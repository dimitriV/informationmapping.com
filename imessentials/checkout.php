<!DOCTYPE html>
<!--[if IE 9]> <html lang="en" class="ie9"> <![endif]-->
<!--[if IE 8]> <html lang="en" class="ie8"> <![endif]-->
<!--[if !IE]><!-->
<html lang="en">
<!--<![endif]-->
	<head>
		<meta charset="utf-8">
		<title>Change The Way You Write</title>
		<meta name="description" content="">
		<meta name="author" content="">

		<!-- Mobile Meta -->
		<meta name="viewport" content="width=device-width, initial-scale=1.0">

		<!-- Favicon -->
		<link rel="shortcut icon" href="images/favicon.ico">

		<!-- Web Fonts -->
		<link href='http://fonts.googleapis.com/css?family=Open+Sans:400italic,700italic,400,700,300&amp;subset=latin,latin-ext' rel='stylesheet' type='text/css'>
		<link href='http://fonts.googleapis.com/css?family=PT+Serif' rel='stylesheet' type='text/css'>

		<!-- Bootstrap core CSS -->
		<link href="bootstrap/css/bootstrap.css" rel="stylesheet">
		<link href="fonts/font-awesome/css/font-awesome.css" rel="stylesheet">
		<link href="fonts/fontello/css/fontello.css" rel="stylesheet">

		<!-- Plugins -->
		<link href="plugins/rs-plugin/css/settings.css" media="screen" rel="stylesheet">
		<link href="plugins/rs-plugin/css/extralayers.css" media="screen" rel="stylesheet">
		<link href="plugins/magnific-popup/magnific-popup.css" rel="stylesheet">
		<link href="css/animations.css" rel="stylesheet">
		<link href="plugins/owl-carousel/owl.carousel.css" rel="stylesheet">

		<!-- iDea core CSS file -->
		<link href="css/style.css" rel="stylesheet">
		<link href="css/information_mapping.css" rel="stylesheet">

		<!-- Custom css -->
		<link href="css/custom.css" rel="stylesheet">

		<!-- HTML5 shim and Respond.js for IE8 support of HTML5 elements and media queries -->
		<!--[if lt IE 9]>
			<script src="https://oss.maxcdn.com/html5shiv/3.7.2/html5shiv.min.js"></script>
			<script src="https://oss.maxcdn.com/respond/1.4.2/respond.min.js"></script>
		<![endif]-->
	</head>

	<!-- body classes: 
			"boxed": boxed layout mode e.g. <body class="boxed">
			"pattern-1 ... pattern-9": background patterns for boxed layout mode e.g. <body class="boxed pattern-1"> 
	-->
	<body class="front no-trans">
		<div class="scrollToTop"><i class="icon-up-open-big"></i></div>

		<div class="page-wrapper">


			<!-- header start classes:
				fixed: fixed navigation mode (sticky menu) e.g. <header class="header fixed clearfix">
				 dark: dark header version e.g. <header class="header dark clearfix">
			================ -->
			<header class="header fixed clearfix">
				<div class="container">
					<div class="row">
						<div class="col-md-3">

							<div class="header-left clearfix">
								<div class="logo">
									<img id="logo" src="images/logo_information_mapping.png" alt="information mapping">
								</div>
							</div>

						</div>
					</div>
				</div>
			</header>
			<!-- header end -->
			
			<!-- main-container start -->
			<!-- ================ -->
			<section class="main-container">
			
				<div class="container">
					
					<div class="row">
						<div class="col-sm-12">
					
							<h1>Pluggable Checkout Demo</h1>
						
							<div id="checkout">
								<button class="btn btn-primary buy buy-premium" autocomplete="off" style=" display: none">Buy premium</button>
								<button class="btn btn-default buy buy-standard" autocomplete="off" style=" display: none">Buy standard</button>
							</div>
						</div>
					</div><!-- row -->
				</div><!-- container -->
				
	
				
			</section>
			<!-- main-container end -->
			

		</div>
		<!-- page-wrapper end -->

		<!-- JavaScript files placed at the end of the document so the pages load faster
		================================================== -->
		<!-- Jquery and Bootstap core js files -->
		<script type="text/javascript" src="plugins/jquery.min.js"></script>
		<script type="text/javascript" src="bootstrap/js/bootstrap.min.js"></script>

		<!-- Modernizr javascript -->
		<script type="text/javascript" src="plugins/modernizr.js"></script>

		<!-- jQuery REVOLUTION Slider  -->
		<script type="text/javascript" src="plugins/rs-plugin/js/jquery.themepunch.tools.min.js"></script>
		<script type="text/javascript" src="plugins/rs-plugin/js/jquery.themepunch.revolution.min.js"></script>

		<!-- Isotope javascript -->
		<script type="text/javascript" src="plugins/isotope/isotope.pkgd.min.js"></script>

		<!-- Owl carousel javascript -->
		<script type="text/javascript" src="plugins/owl-carousel/owl.carousel.js"></script>

		<!-- Magnific Popup javascript -->
		<script type="text/javascript" src="plugins/magnific-popup/jquery.magnific-popup.min.js"></script>

		<!-- Appear javascript -->
		<script type="text/javascript" src="plugins/jquery.appear.js"></script>

		<!-- Count To javascript -->
		<script type="text/javascript" src="plugins/jquery.countTo.js"></script>

		<!-- Parallax javascript -->
		<script src="plugins/jquery.parallax-1.1.3.js"></script>

		<!-- Contact form -->
		<script src="plugins/jquery.validate.js"></script>
		
		<!-- Word rotator of Plugins -->
		<script type="text/javascript" src="js/jquery.simple-text-rotator.min.js"></script>

		<!-- Initialization of Plugins -->
		<script type="text/javascript" src="js/template.js"></script>

		<!-- Custom Scripts -->
		<script type="text/javascript" src="js/pluggablecheckout.js"></script>
		<script type="text/javascript" src="js/custom.js"></script>
		<script>
			jQuery(document).ready(function(){
				AngularCheckout.init('https://staging.informationmapping.com/magento/', function(){
					jQuery('.buy').show();
				},'proxy/index.php');
				
				jQuery('.buy-premium').click(function(e){
					e.preventDefault();
					console.log('.buy-premium clicked');
					jQuery('.buy').prop('disabled', true);
					AngularCheckout.emptyCart();
					AngularCheckout.addProduct('roy-im-fspro2013-subscription-1y-classroom ', 5);
					AngularCheckout.renderInside('#checkout');
				});
				
				jQuery('.buy-standard').click(function(e){
					e.preventDefault();
					console.log('.buy-standard clicked');
					jQuery('.buy').prop('disabled', true);
					AngularCheckout.emptyCart();
					AngularCheckout.addProduct('roy-im-fspro2013-subscription-1y-classroom ', 1);
					AngularCheckout.renderInside('#checkout');
				});
					
					
				//AngularCheckout.addProduct('roy-im-fspro2013-subscription-1y-classroom ', 5);
				//AngularCheckout.addProduct('roy-im-fspro2013-subscription-1y-classroom ', 1);
				//AngularCheckout.renderInside('#checkout');
			});	
		</script>
			
	</body>
</html>
