<!DOCTYPE html>
<!--[if IE 9]> <html lang="en" class="ie9"> <![endif]-->
<!--[if IE 8]> <html lang="en" class="ie8"> <![endif]-->
<!--[if !IE]><!-->
<html lang="en">
<!--<![endif]-->
<head>
    <meta charset="utf-8">
    <title>Change The Way You Write</title>
    <meta name="description" content="">
    <meta name="author" content="">

    <!-- Mobile Meta -->
    <meta name="viewport" content="width=device-width, initial-scale=1.0">

    <!-- Favicon -->
    <link rel="shortcut icon" href="../images/favicon.ico">

    <!-- Bootstrap core CSS -->
    <link href="../bootstrap/css/bootstrap.css" rel="stylesheet">
    <link href="../css/bootstrap-social.css" rel="stylesheet">
    <link href="../fonts/font-awesome/css/font-awesome.css" rel="stylesheet">
    <link href="../fonts/fontello/css/fontello.css" rel="stylesheet">

    <!-- Plugins -->
    <link href="../plugins/rs-plugin/css/settings.css" media="screen" rel="stylesheet">
    <link href="../plugins/rs-plugin/css/extralayers.css" media="screen" rel="stylesheet">
    <link href="../plugins/magnific-popup/magnific-popup.css" rel="stylesheet">
    <link href="../css/animations.css" rel="stylesheet">
    <link href="../plugins/owl-carousel/owl.carousel.css" rel="stylesheet">

    <!-- iDea core CSS file -->
    <link href="../css/style.css" rel="stylesheet">
    <link href="../css/information_mapping.css" rel="stylesheet">

    <!-- Custom css -->
    <link href="../css/custom.css" rel="stylesheet">

    <!-- HTML5 shim and Respond.js for IE8 support of HTML5 elements and media queries -->
    <!--[if lt IE 9]>
    <script src="https://oss.maxcdn.com/html5shiv/3.7.2/html5shiv.min.js"></script>
    <script src="https://oss.maxcdn.com/respond/1.4.2/respond.min.js"></script>
    <![endif]-->


</head>

<!-- body classes:
        "boxed": boxed layout mode e.g. <body class="boxed">
"pattern-1 ... pattern-9": background patterns for boxed layout mode e.g. <body class="boxed pattern-1">
-->
<body class="front no-trans">
<div class="scrollToTop"><i class="icon-up-open-big"></i></div>

<div class="page-wrapper" id="top_of_page">


    <!-- header start classes:
        fixed: fixed navigation mode (sticky menu) e.g. <header class="header fixed clearfix">
dark: dark header version e.g. <header class="header dark clearfix">
    ================ -->
    <header class="header fixed clearfix">
        <div class="container">
            <div class="row">
                <div class="col-md-3">

                    <div class="header-left clearfix">
                        <div class="logo">
                            <a href="../"><img id="logo" src="../images/logo_information_mapping.jpg" alt="information mapping"></a>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </header>
    <!-- section start -->
    <!-- ================ -->
    <div class="section clearfix" id="section3">
        <div class="container">
            <div class="row">
                <div class="col-md-12">
                    <h1 class="text-center">Free <em>module</em></h1>
                    <div class="separator"></div>
                    <p class="lead text-center">Get immediate access to a FREE MODULE of the Information Mapping® Essentials Video Course!</p>
                </div>
                <div class="col-md-12 text-center sample_modal_image">
                        <a href="login-with.php?provider=LinkedIn"><img src="../images/free_module.png" class="img-responsive"/></a>
                    <div class="col-md-3">
                        <a class="btn btn-block btn-social btn-linkedin" href="login-with.php?provider=LinkedIn">Get access with LinkedIn <span><i class="fa fa-linkedin fa-lg"></i></span></a>
                </div>
                </div>
            </div>
        </div>
        <!-- section end -->
        <div class="container">

            <!-- Modal -->
            <div class="modal fade" id="success" role="dialog">
                <div class="modal-dialog">

                    <!-- Modal content-->
                    <div class="modal-content">
                        <div class="modal-header">
                            <button type="button" class="close" data-dismiss="modal">&times;</button>
                            <h4 class="modal-title">FREE MODULE about Blocks and Maps</h4>
                        </div>
                        <div class="modal-body modal-center">
                            <a class="btn btn-info" href="../f238765ece0ebab4141919be58ab650280abc659428f1117eaf53ba1c0e52e71.php" role="button">Proceed to your free module here.</a>
                        </div>
                    </div>

                </div>
            </div>

        </div>

        <!-- section start -->
        <!-- ================ -->
        <div class="section clearfix" id="social">
            <div class="container">
                <div class="row">
                    <div class="col-md-12">
                        <h2 class="text-center"><em>Share</em> with friends</h2>
                        <div class="separator"></div>
                        <ul class="text-center social-links colored circle">
                            <li class="facebook"><a target="_blank" href="https://www.facebook.com/sharer/sharer.php?u=https://www.informationmapping.com/"><i class="fa fa-facebook"></i></a></li>
                            <li class="twitter"><a target="_blank" href="https://twitter.com/home?status=https://www.informationmapping.com/"><i class="fa fa-twitter"></i></a></li>
                            <li class="googleplus"><a target="_blank" href="https://plus.google.com/share?url=https://www.informationmapping.com/"><i class="fa fa-google-plus"></i></a></li>
                            <li class="linkedin"><a target="_blank" href="https://www.linkedin.com/shareArticle?mini=true&url=https://www.informationmapping.com/&title=Information%20Mapping%20Essentiels&summary=Information%20Mapping%20Essentiels%20will%20learn%20you%20how%20to%20create%20easily%20read,%20easily%20understood%20proposals%0Aproduct%20descriptions%0A%20in%20less%20time!&source="><i class="fa fa-linkedin"></i></a></li>
                        </ul>
                    </div>
                </div>
            </div>
        </div>
        <!-- section end -->
        <footer class="footer"></footer>
    </div>
    <!-- section end -->

</div>
<!-- page-wrapper end -->

<!-- JavaScript files placed at the end of the document so the pages load faster
================================================== -->
<!-- Jquery and Bootstap core js files -->
<script type="text/javascript" src="../plugins/jquery.min.js"></script>
<script type="text/javascript" src="../bootstrap/js/bootstrap.min.js"></script>

<!-- Modernizr javascript -->
<script type="text/javascript" src="../plugins/modernizr.js"></script>

<!-- jQuery REVOLUTION Slider  -->
<script type="text/javascript" src="../plugins/rs-plugin/js/jquery.themepunch.tools.min.js"></script>
<script type="text/javascript" src="../plugins/rs-plugin/js/jquery.themepunch.revolution.min.js"></script>

<!-- Isotope javascript -->
<script type="text/javascript" src="../plugins/isotope/isotope.pkgd.min.js"></script>

<!-- Owl carousel javascript -->
<script type="text/javascript" src="../plugins/owl-carousel/owl.carousel.js"></script>

<!-- Magnific Popup javascript -->
<script type="text/javascript" src="../plugins/magnific-popup/jquery.magnific-popup.min.js"></script>

<!-- Appear javascript -->
<script type="text/javascript" src="../plugins/jquery.appear.js"></script>

<!-- Count To javascript -->
<script type="text/javascript" src="../plugins/jquery.countTo.js"></script>

<!-- Parallax javascript -->
<script src="../plugins/jquery.parallax-1.1.3.js"></script>

<!-- Contact form -->
<script src="../plugins/jquery.validate.js"></script>

<!-- Word rotator of Plugins -->
<script type="text/javascript" src="../js/jquery.simple-text-rotator.min.js"></script>

<!-- Initialization of Plugins -->
<script type="text/javascript" src="../js/template.js"></script>

<!-- Custom Scripts -->
<script type="text/javascript" src="../js/pluggablecheckout.js"></script>
<script type="text/javascript" src="../js/custom.js"></script>

<?php if (($_GET['success'])) { ?>
    <script> $('#success').modal('show'); </script>
<?php } ?>
<!-- Google Analytics -->
<script>
    (function(i,s,o,g,r,a,m){i['GoogleAnalyticsObject']=r;i[r]=i[r]||function(){
        (i[r].q=i[r].q||[]).push(arguments)},i[r].l=1*new Date();a=s.createElement(o),
        m=s.getElementsByTagName(o)[0];a.async=1;a.src=g;m.parentNode.insertBefore(a,m)
    })(window,document,'script','//www.google-analytics.com/analytics.js','ga');

    ga('create', 'UA-24028581-1', 'auto', {'allowLinker': true});
    // On every domain.
    ga('require', 'linker');
    // List of every domain to share linker parameters.
    ga('linker:autoLink', ['informationmapping.com', 'informationmapping-webinars.com']);
    ga('send', 'pageview'); // Send hits after initializing the auto-linker plug-in.
    ga('require', 'ecommerce');
</script>
<!-- End Google Analytics -->

<?php //endif; ?>
<script type="text/javascript">
    piAId = '9622';
    piCId = '34568';

    (function() {
        function async_load(){
            var s = document.createElement('script'); s.type = 'text/javascript';
            s.src = ('https:' == document.location.protocol ? 'https://pi' : 'http://cdn') + '.pardot.com/pd.js';
            var c = document.getElementsByTagName('script')[0]; c.parentNode.insertBefore(s, c);
        }
        if(window.attachEvent) { window.attachEvent('onload', async_load); }
        else { window.addEventListener('load', async_load, false); }
    })();
</script>
<script type="text/javascript" src="/templates/imi/javascript/opentrack.js"></script>
<!-- Google-code voor remarketingtag -->
<script type="text/javascript">
    /* <![CDATA[ */
    var google_conversion_id = 963549049;
    var google_custom_params = window.google_tag_params;
    var google_remarketing_only = true;
    /* ]]> */
</script>
<script type="text/javascript" src="//www.googleadservices.com/pagead/conversion.js">
</script>
<noscript>
    <div style="display:inline;">
        <img height="1" width="1" style="border-style:none;" alt="" src="//googleads.g.doubleclick.net/pagead/viewthroughconversion/963549049/?value=0&amp;guid=ON&amp;script=0"/>
    </div>
</noscript>


<!-- begin olark code -->
<script data-cfasync="false" type='text/javascript'>/*<![CDATA[*/window.olark||(function(c){var f=window,d=document,l=f.location.protocol=="https:"?"https:":"http:",z=c.name,r="load";var nt=function(){
        f[z]=function(){
            (a.s=a.s||[]).push(arguments)};var a=f[z]._={
        },q=c.methods.length;while(q--){(function(n){f[z][n]=function(){
            f[z]("call",n,arguments)}})(c.methods[q])}a.l=c.loader;a.i=nt;a.p={
            0:+new Date};a.P=function(u){
            a.p[u]=new Date-a.p[0]};function s(){
            a.P(r);f[z](r)}f.addEventListener?f.addEventListener(r,s,false):f.attachEvent("on"+r,s);var ld=function(){function p(hd){
            hd="head";return["<",hd,"></",hd,"><",i,' onl' + 'oad="var d=',g,";d.getElementsByTagName('head')[0].",j,"(d.",h,"('script')).",k,"='",l,"//",a.l,"'",'"',"></",i,">"].join("")}var i="body",m=d[i];if(!m){
            return setTimeout(ld,100)}a.P(1);var j="appendChild",h="createElement",k="src",n=d[h]("div"),v=n[j](d[h](z)),b=d[h]("iframe"),g="document",e="domain",o;n.style.display="none";m.insertBefore(n,m.firstChild).id=z;b.frameBorder="0";b.id=z+"-loader";if(/MSIE[ ]+6/.test(navigator.userAgent)){
            b.src="javascript:false"}b.allowTransparency="true";v[j](b);try{
            b.contentWindow[g].open()}catch(w){
            c[e]=d[e];o="javascript:var d="+g+".open();d.domain='"+d.domain+"';";b[k]=o+"void(0);"}try{
            var t=b.contentWindow[g];t.write(p());t.close()}catch(x){
            b[k]=o+'d.write("'+p().replace(/"/g,String.fromCharCode(92)+'"')+'");d.close();'}a.P(2)};ld()};nt()})({
        loader: "static.olark.com/jsclient/loader0.js",name:"olark",methods:["configure","extend","declare","identify"]});
    /* custom configuration goes here (www.olark.com/documentation) */
    olark.identify('3504-626-10-9459');/*]]>*/</script><noscript><a href="https://www.olark.com/site/3504-626-10-9459/contact" title="Contact us" target="_blank">Questions? Feedback?</a> powered by <a href="http://www.olark.com?welcome" title="Olark live chat software">Olark live chat software</a></noscript>
<!-- end olark code -->

</body>
</html>
