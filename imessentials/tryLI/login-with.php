<?php
// start a new session (required for Hybridauth)
session_start();

// change the following paths if necessary
$config   = '../vendor/config.php';
require_once( "../vendor/Hybrid/Auth.php" );

if(isset($_GET['provider']))
{
    $provider = $_GET['provider'];

    try
    {
        // create an instance for Hybridauth with the configuration file path as parameter
        $hybridauth = new Hybrid_Auth($config);
        $linkedIn = $hybridauth->authenticate($provider);
        $linkedIn_user_profile = $linkedIn->getUserProfile();
        if($linkedIn_user_profile && isset($linkedIn_user_profile->identifier))
        {

            // set the user as connected and redirect him to a home page or something
            $_SESSION["name"] = $linkedIn_user_profile->displayName;
            $_SESSION["email"] = $linkedIn_user_profile->email;
            $_SESSION["firstName"] = $linkedIn_user_profile->firstName;
            $_SESSION["lastName"] = $linkedIn_user_profile->lastName;
            $_SESSION["photoURL"] = $linkedIn_user_profile->photoURL;
            $_SESSION["phone"] = $linkedIn_user_profile->phone;
            $_SESSION["title"] = $linkedIn_user_profile->title;
            $_SESSION["profile"] = $linkedIn_user_profile->profileURL ;


            header("Location: https://www.informationmapping.com/imessentials/f238765ece0ebab4141919be58ab650280abc659428f1117eaf53ba1c0e52e71.php");
        }
        // $linkedIn->logout();
    } catch (Exception $e)
    {
        // Display the recived error,
        // to know more please refer to Exceptions handling section on the userguide
        switch ($e->getCode())
        {
            case 0 :
                echo "Unspecified error.";
                break;
            case 1 :
                echo "Hybriauth configuration error.";
                break;
            case 2 :
                echo "Provider not properly configured.";
                break;
            case 3 :
                echo "Unknown or disabled provider.";
                break;
            case 4 :
                echo "Missing provider application credentials.";
                break;
            case 5 :
                echo "Authentification failed. "
                    . "The user has canceled the authentication or the provider refused the connection.";
                break;
            case 6 :
                echo "User profile request failed. Most likely the user is not connected "
                    . "to the provider and he should authenticate again.";
                $linkedIn->logout();
                break;
            case 7 :
                echo "User not connected to the provider.";
                $linkedIn->logout();
                break;
            case 8 :
                echo "Provider does not support this feature.";
                break;
        }

        // well, basically your should not display this to the end user, just give him a hint and move on..
        echo "<br /><br /><b>Original error message:</b> " . $e->getMessage();
    }
}
?>