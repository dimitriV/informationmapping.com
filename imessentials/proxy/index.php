<?php
// ==== BEGIN CONFIGURATION ====
$magentoHomeUrl = 'https://www.informationmapping.com/en/shop/';





// ==== END CONFIGURATION - DON'T EDIT BELOW THIS LINE ====

ini_set('display_errors', 1);

set_include_path('classes');
$r = dirname(__FILE__);
define('ROOT_DIR', $r);

require_once ROOT_DIR . '/classes/Zend/Loader.php';
require_once ROOT_DIR . '/classes/Zend/Loader/Autoloader.php';

$autoloader = Zend_Loader_Autoloader::getInstance();
$autoloader->registerNamespace('Storefront_');

date_default_timezone_set('Europe/Brussels');

// Destination URL: Where this proxy leads to.

$magentoHomeUrl = trim($magentoHomeUrl, '/');

$destinationURL = $magentoHomeUrl . '/' . $_GET['path'];

proxy_request($destinationURL);

function proxy_request ($url) {
	$excludeCookies = array(
			'ZS6LIBRARIES',
			'ZDEDebuggerPresent',
			'ZSDEVBAR',
			'ZS6SESSID',
			'ZendDebuggerCookie',
			'debug_host',
			'debug_port',
			'start_debug',
			'send_debug_header',
			'send_sess_end',
			'debug_jit'
	);
	
	$client = new Zend_Http_Client($url, array(
			'timeout' => 30,
			'maxredirects' => 5,
	));
	
	$cookies = array();
	foreach ($_COOKIE as $key => $value) {
		if ($key != 'Array') {
			if (! in_array($key, $excludeCookies)) {
				// $cookies[] = $key . '=' . $value;
				
				$client->setCookie($key, $value);
			}
		}
	}
	
	$method = strtoupper($_SERVER['REQUEST_METHOD']);
	
	if ($method === 'POST') {
		if (count($_POST) > 0) {
			$client->setParameterPost($_POST);
		} else {
			$postData = file_get_contents('php://input');
			$client->setRawData($postData);
		}
	}
	
	$ip = '';
	if (! empty($_SERVER['HTTP_CLIENT_IP'])) {
		$ip = $_SERVER['HTTP_CLIENT_IP'];
	} elseif (! empty($_SERVER['HTTP_X_FORWARDED_FOR'])) {
		$ip = $_SERVER['HTTP_X_FORWARDED_FOR'];
	} else {
		$ip = $_SERVER['REMOTE_ADDR'];
	}
	
	if ($ip) {
		$client->setHeaders('X-Forwarded-For', $ip);
	}
	
	$response = $client->request($method);
	
	$body = $response->getBody();
	
	$headers = $response->getHeaders();
	
	
	$headersToIgnore = array(
			'date',
			'server',
			'x-powered-by',
			'expires',
			'cache-control',
			'pragma',
			'content-length',
			'x-fpc',
			'connection',
			'transfer-encoding'
	);
	
	$cookiesToIgnore = array(
		'ZDEDebuggerPresent'
	);
	
	foreach ($headers as $headerName => $headerValue) {
		
		if (! in_array(strtolower($headerName), $headersToIgnore)) {
			
			if ($headerName == 'Set-cookie'){
				
				
				if(!is_array($headerValue)){
					$headerValue = array($headerValue);
				}
				
				$newCookieValues = array();
				
				foreach($headerValue as $oneHeaderValue){
					// Simplify cookies
					
					$parts = explode(';', $oneHeaderValue);

					$parts = explode('=',$parts[0]);
					
					
					$cookieName = $parts[0];
					$cookieValue = $parts[1];
					
					if(in_array($cookieName, $cookiesToIgnore)){
						
					}else{
						setcookie($cookieName, $cookieValue);
					}
					
					
					/*
					
					if (strpos($oneHeaderValue, 'domain') !== false) {
						
						$cookie = Zend_Http_Cookie::fromString($oneHeaderValue);
						
						$remoteUrlInfo = parse_url($url);
					
						// Example: www.google.com
						$remoteDomain = $remoteUrlInfo['host'];
					
						// Example: localhost
						$myDomain = $_SERVER['HTTP_HOST'];
					
						$newCookieValue = str_replace('domain=' . $remoteDomain, 'domain=' . $myDomain, $oneHeaderValue);
						
						$newCookieValues[] = $newCookieValue;
					}
					*/
				}
// 				$headerValue = implode(',', $newCookieValues);
				
				
				
			}else{
			
				// Not a cookie, set header normally
				$newHeader = $headerName . ': ' . $headerValue;
			
				header($newHeader, false);
			}
		}
	}
	
	$body = $response->getBody();
// 	$body = 'LOL';
	
	$status = $response->getStatus();
	$statusMessage = $response->getMessage();
	
	header('HTTP/1.1 '.$status.' '.$statusMessage);
	
// 	$bytes = mb_strlen($body);
// 	header("Content-Length: $bytes");
	
	echo $body;
	exit();
}
