﻿<!DOCTYPE html>
<!--[if IE 9]> <html lang="en" class="ie9"> <![endif]-->
<!--[if IE 8]> <html lang="en" class="ie8"> <![endif]-->
<!--[if !IE]><!-->
<html lang="en">
<!--<![endif]-->
<script type="text/javascript">
    //<![CDATA[
    var tlJsHost = ((window.location.protocol == "https:") ? "https://secure.comodo.com/" : "http://www.trustlogo.com/");
    document.write(unescape("%3Cscript src='" + tlJsHost + "trustlogo/javascript/trustlogo.js' type='text/javascript'%3E%3C/script%3E"));
    //]]>
</script>
<head>
    <meta charset="utf-8">
    <title>Change The Way You Write</title>
    <meta name="description" content="">
    <meta name="author" content="">

    <!-- Mobile Meta -->
    <meta name="viewport" content="width=device-width, initial-scale=1.0">

    <!-- Favicon -->
    <link rel="shortcut icon" href="images/favicon.ico">

    <!-- Bootstrap Form Helpers -->
    <link href="css/bootstrap-formhelpers.min.css" rel="stylesheet">
    <!-- Bootstrap core CSS -->
    <link href="bootstrap/css/bootstrap.css" rel="stylesheet">
    <link href="fonts/font-awesome/css/font-awesome.css" rel="stylesheet">
    <link href="fonts/fontello/css/fontello.css" rel="stylesheet">

    <!-- Plugins -->
    <link href="plugins/rs-plugin/css/settings.css" media="screen" rel="stylesheet">
    <link href="plugins/rs-plugin/css/extralayers.css" media="screen" rel="stylesheet">
    <link href="plugins/magnific-popup/magnific-popup.css" rel="stylesheet">
    <link href="css/animations.css" rel="stylesheet">
    <link href="plugins/owl-carousel/owl.carousel.css" rel="stylesheet">

    <!-- iDea core CSS file -->
    <link href="css/style.css" rel="stylesheet">
    <link href="css/information_mapping.css" rel="stylesheet">

    <!-- Custom css -->
    <link href="css/custom.css" rel="stylesheet">

    <!-- HTML5 shim and Respond.js for IE8 support of HTML5 elements and media queries -->
    <!--[if lt IE 9]>
    <script src="https://oss.maxcdn.com/html5shiv/3.7.2/html5shiv.min.js"></script>
    <script src="https://oss.maxcdn.com/respond/1.4.2/respond.min.js"></script>
    <![endif]-->

    <script src="js/modernizr.js"></script>

</head>

<!-- body classes:
        "boxed": boxed layout mode e.g. <body class="boxed">
        "pattern-1 ... pattern-9": background patterns for boxed layout mode e.g. <body class="boxed pattern-1">
-->
<body class="front no-trans">
<div class="scrollToTop"><i class="icon-up-open-big"></i></div>

<div class="page-wrapper" id="top_of_page">


    <!-- header start classes:
        fixed: fixed navigation mode (sticky menu) e.g. <header class="header fixed clearfix">
         dark: dark header version e.g. <header class="header dark clearfix">
    ================ -->
    <header class="header fixed clearfix">
        <div class="container">
            <div class="row">
                <div class="col-md-3">

                    <div class="header-left clearfix">
                        <div class="logo">
                            <a href="https://www.informationmapping.com/imessentials/"><img id="logo" src="images/logo_information_mapping.jpg" alt="information mapping"></a>
                        </div>
                    </div>

                </div>
                <div class="col-md-9">

                    <!-- header-right start -->
                    <!-- ================ -->
                    <div class="header-right clearfix">

                        <!-- main-navigation start -->
                        <!-- ================ -->
                        <div id="header" class="main-navigation animated">

                            <!-- navbar start -->
                            <nav class="navbar navbar-default" role="navigation">
                                <div class="container-fluid">
                                    <div class="navbar-header">
                                        <button type="button" class="navbar-toggle" data-toggle="collapse" data-target="#navbar-collapse-1">
                                            <span class="sr-only">Toggle navigation</span>
                                            <span class="icon-bar"></span>
                                            <span class="icon-bar"></span>
                                            <span class="icon-bar"></span>
                                        </button>
                                    </div>
                                    <div class="collapse navbar-collapse scrollspy smooth-scroll" id="navbar-collapse-1">
                                        <ul class="nav navbar-nav navbar-right">
                                            <li class="active"><a href="#top_of_page">Home</a></li>
                                            <li class=""><a href="#section2">Benefits</a></li>
                                            <li class=""><a href="#section3">Free module</a></li>
                                            <li class=""><a href="#section4">Testimonials</a></li>
                                            <li class="highlight"><a href="#buynow">Buy Now</a></li>
                                        </ul>
                                    </div>
                                </div>
                            </nav>
                            <!-- navbar end -->

                        </div>
                        <!-- main-navigation end -->

                    </div>
                    <!-- header-right end -->

                </div>
            </div>
        </div>
    </header>
    <!-- header end -->

    <!-- section end -->
    <div class="container">

        <!-- Modal -->
        <div class="modal fade" id="success" role="dialog">
            <div class="modal-dialog">

                <!-- Modal content-->
                <div class="modal-content">
                    <div class="modal-header">
                        <button type="button" class="close" data-dismiss="modal">&times;</button>
                        <h4 class="modal-title">FREE MODULE about Blocks and Maps</h4>
                    </div>
                    <div class="modal-body">
                        <p>Thank you for requesting your free module.
                            Please check your email to find the link.
                        </p>
                    </div>
                    <div class="modal-footer">
                        <button type="button" class="btn btn-default" data-dismiss="modal">Close</button>
                    </div>
                </div>

            </div>
        </div>

    </div>

    <div id="rest">
        <!-- banner start -->
        <!-- ================ -->
        <div class="banner">

            <!-- slideshow start -->
            <!-- ================ -->
            <div class="slideshow">

                <!-- slider revolution start -->
                <!-- ================ -->
                <div class="slider-banner-container">
                    <div class="slider-banner">
                        <ul>
                            <!-- slide 1 start -->
                            <li data-transition="random" data-slotamount="7" data-masterspeed="500" data-saveperformance="on" data-title="Premium HTML5 template">

                                <!-- main image -->
                                <img src="images/slider-1-slide-1.jpg"  alt="slidebg1" data-bgposition="center top" data-bgfit="cover" data-bgrepeat="no-repeat">

                                <!-- LAYER NR. 1 -->
                                <div class="tp-caption default_bg large sfr tp-resizeme"
                                     data-x="0"
                                     data-y="70"
                                     data-speed="600"
                                     data-start="1200">Information Mapping<sup>&reg;</sup> Essentials&trade;
                                </div>

                                <!-- LAYER NR. 2 -->
                                <div class="tp-caption dark_gray_bg sfl medium tp-resizeme"
                                     data-x="0"
                                     data-y="170"
                                     data-speed="600"
                                     data-start="1600"><i class="icon-check"></i>
                                </div>

                                <!-- LAYER NR. 3 -->
                                <div class="tp-caption light_gray_bg sfb medium tp-resizeme"
                                     data-x="50"
                                     data-y="170"
                                     data-speed="600"
                                     data-start="1600">Learn to write more clearly, effectively and productively
                                </div>

                                <!-- LAYER NR. 4 -->
                                <div class="tp-caption dark_gray_bg sfl medium tp-resizeme"
                                     data-x="0"
                                     data-y="220"
                                     data-speed="600"
                                     data-start="1800"><i class="icon-check"></i>
                                </div>

                                <!-- LAYER NR. 5 -->
                                <div class="tp-caption light_gray_bg sfb medium tp-resizeme"
                                     data-x="50"
                                     data-y="220"
                                     data-speed="600"
                                     data-start="1800">Benefit from the expert guidance of a Master Instructor
                                </div>

                                <!-- LAYER NR. 6 -->
                                <div class="tp-caption dark_gray_bg sfl medium tp-resizeme"
                                     data-x="0"
                                     data-y="270"
                                     data-speed="600"
                                     data-start="2000"><i class="icon-check"></i>
                                </div>

                                <!-- LAYER NR. 7 -->
                                <div class="tp-caption light_gray_bg sfb medium tp-resizeme"
                                     data-x="50"
                                     data-y="270"
                                     data-speed="600"
                                     data-start="2000">Get trained at your own desk, at your own pace
                                </div>

                                <!-- LAYER NR. 8 -->
                                <div class="tp-caption dark_gray_bg sfl medium tp-resizeme"
                                     data-x="0"
                                     data-y="320"
                                     data-speed="600"
                                     data-start="2200"><i class="icon-check"></i>
                                </div>

                                <!-- LAYER NR. 9 -->
                                <div class="tp-caption light_gray_bg sfb medium tp-resizeme"
                                     data-x="50"
                                     data-y="370"
                                     data-speed="600"
                                     data-start="2200">Enjoy this flexible and interactive video course
                                </div>

                                <!-- LAYER NR. 8 -->
                                <div class="tp-caption dark_gray_bg sfl medium tp-resizeme"
                                     data-x="0"
                                     data-y="370"
                                     data-speed="600"
                                     data-start="2400"><i class="icon-check"></i>
                                </div>

                                <!-- LAYER NR. 9 -->
                                <div class="tp-caption light_gray_bg sfb medium tp-resizeme"
                                     data-x="50"
                                     data-y="320"
                                     data-speed="600"
                                     data-start="2400">Join thousands of users worldwide
                                </div>

                                <!-- LAYER NR. 10 -->
                                <div class="tp-caption sfb medium tp-resizeme"
                                     data-x="-10"
                                     data-y="420"
                                     data-speed="600"
                                     data-start="2600"><a href="#buynow" class="btn btn-pink contact buy">Buy now <i class="pl-10 fa fa-shopping-cart"></i></a> or <a class="btn btn-default contact buy" data-toggle="modal" data-target="#myModalSample">Get access to a free module<i class="pl-10 fa fa-info"></i></a>
                                </div>

                                <!-- LAYER NR. 11 -->
                                <div class="tp-caption sfr tp-resizeme"
                                     data-x="650"
                                     data-y="bottom"
                                     data-speed="600"
                                     data-start="2900"><img src="images/slider_front.png" alt="">
                                </div>

                            </li>
                            <!-- slide 1 end -->

                        </ul>
                        <div class="tp-bannertimer tp-bottom"></div>
                    </div>
                </div>
                <!-- slider revolution end -->

            </div>
            <!-- slideshow end -->

        </div>
        <!-- banner end -->

        <!-- page-top start-->
        <!-- ================ -->
        <div class="page-top" id="section1">
            <div class="container">
                <div class="row">
                    <div class="col-md-10 col-md-offset-1">
                        <div class="call-to-action">
                            <h1 class="title">What you'll <em>learn</em></h1>
                            <div class="separator"></div>
                            <p class="lead text-center">You’ll learn how to create easily read, easily understood <span class="red">policies & procedures, SOP’s, manuals, memos, reports, proposals</span> in less time!</p>
                            <p class="lead text-center">Throughout the course, we will also teach you how to present the Information Mapping® techniques in FS Pro for Microsoft Word.</p>
                            <p class="sample_modal_image"><a data-toggle="modal" data-target="#myModal"><img src="images/before_after.jpg" /></a></p>
                            <a class="btn btn-white more" data-toggle="modal" data-target="#myModal">
                                See a Before & After example <i class="pl-10 fa fa-info"></i>
                            </a>

                            <!-- Modal -->
                            <div class="modal fade" id="myModal" tabindex="-1" role="dialog" aria-labelledby="myModalLabel" aria-hidden="true">
                                <div class="modal-dialog">
                                    <div class="modal-content">
                                        <div class="modal-header">
                                            <button type="button" class="close" data-dismiss="modal"><span aria-hidden="true">&times;</span><span class="sr-only">Close</span></button>
                                            <h4 class="modal-title" id="myModalLabel">What you'll learn during our course!</h4>
                                        </div>
                                        <div class="modal-body">
                                            <img src="images/example.jpg" class="img-responsive"/>
                                        </div>
                                        <div class="modal-footer">
                                            <button type="button" class="btn btn-sm btn-default" data-dismiss="modal"><i class="icon-check"></i> Ok</button>
                                        </div>
                                    </div>
                                </div>
                            </div>
                            or
                            <a href="#buynow" class="btn btn-pink contact buy">Buy now <i class="pl-10 fa fa-shopping-cart"></i></a>
                        </div>
                    </div>
                </div>
            </div>
        </div>
        <!-- page-top end -->


        <!-- main-container start -->
        <!-- ================ -->
        <section class="main-container gray-bg" id="section2">
            <!-- page-top start-->
            <!-- ================ -->
            <div class="page-top object-non-visible" data-animation-effect="fadeInUpSmall" data-effect-delay="100">
                <div class="container">
                    <div class="row">
                        <div class="col-md-12">
                            <h1 class="text-center title">How you will <em>benefit</em></h1>
                            <div class="separator"></div>
                            <p class="lead text-center">After this course, you and your organization will:</p>

                            <div class="row grid-space-10">
                                <div class="col-sm-4">
                                    <div class="box-style-2">
                                        <div class="icon-container default-bg">
                                            <i class="glyphicon glyphicon-time"></i>
                                        </div>
                                        <div class="body">
                                            <h2>Increase productivity</h2>
                                            <p>You’ll write more quickly and easily. Independent research shows that you can now double your productivity and effectiveness.</p>
                                        </div>
                                    </div>
                                </div>
                                <div class="col-sm-4">
                                    <div class="box-style-2">
                                        <div class="icon-container default-bg">
                                            <i class="glyphicon glyphicon-thumbs-up"></i>
                                        </div>
                                        <div class="body">
                                            <h2>Follow best practices</h2>
                                            <p>Thousands of people and organizations worldwide successfully use the Information Mapping® Method. You’ll be one of them!</p>
                                        </div>
                                    </div>
                                </div>
                                <div class="col-sm-4">
                                    <div class="box-style-2">
                                        <div class="icon-container default-bg">
                                            <i class="glyphicon glyphicon-exclamation-sign"></i>
                                        </div>
                                        <div class="body">
                                            <h2>Reduce errors</h2>
                                            <p>Reduce errors caused by miscommunication. Did you know that errors are reduced by 54% when using this Method?</p>
                                        </div>
                                    </div>
                                </div>
                            </div>
                            <div class="row grid-space-10">
                                <div class="col-sm-4">
                                    <div class="box-style-2">
                                        <div class="icon-container default-bg">
                                            <i class="glyphicon glyphicon-check"></i>
                                        </div>
                                        <div class="body">
                                            <h2>Upgrade quality</h2>
                                            <p>You and your customers will be amazed by the quality of your written documentation, both offline and online!</p>
                                        </div>
                                    </div>
                                </div>
                                <div class="col-sm-4">
                                    <div class="box-style-2">
                                        <div class="icon-container default-bg">
                                            <i class="glyphicon glyphicon-book"></i>
                                        </div>
                                        <div class="body">
                                            <h2>Comply with regulations</h2>
                                            <p>Auditors and regulators appreciate the clarity and accuracy of Mapped documentation.</p>
                                        </div>
                                    </div>
                                </div>
                                <div class="col-sm-4">
                                    <div class="box-style-2">
                                        <div class="icon-container default-bg">
                                            <i class="glyphicon glyphicon-pencil"></i>
                                        </div>
                                        <div class="body">
                                            <h2>Standardize your output</h2>
                                            <p>Information Mapping is a corporate content standard at organizations around the world. It’s ideal for use by writing teams.</p>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
            <!-- page-top end -->

        </section>
        <!-- main-container end -->

        <!-- section start -->
        <!-- ================ -->
        <div class="section clearfix" id="section3">
            <div class="container">
                <div class="row">
                    <div class="col-md-12">
                        <h1 class="text-center">Free <em>module</em></h1>
                        <div class="separator"></div>
                        <p class="lead text-center">The ‘Information Mapping® Essentials’ Video Course contains a mixture of...</p>
                    </div>
                    <div class="col-md-7">
                        <!-- accordion start -->
                        <div class="panel-group" id="accordion">
                            <div class="panel panel-default">
                                <div class="panel-heading">
                                    <h4 class="panel-title">
                                        <a data-toggle="collapse" data-parent="#accordion" href="#collapseOne">
                                            <i class="fa fa-video-camera"></i>Video Modules
                                        </a>
                                    </h4>
                                </div>
                                <div id="collapseOne" class="panel-collapse collapse in">
                                    <div class="panel-body">
                                        <p>In 15 modules that average 20 minutes each, a highly experienced Information Mapping&reg; Master Instructor will teach you all the secrets of this great Method.</p>
                                        <ul>
                                            <li>Introduction to the Information Mapping&reg; Method (17:02)</li>
                                            <li>Research-based Principles (26:28)</li>
                                            <li>Blocks and Maps (22:45) <?php if(!isset($_GET['s'])||($_GET['s']!='trial')){ ?><a class="" href="" data-toggle="modal" data-target="#myModalSample">(Get access to this free module)</a><?php } else { ?><a class="" href="free-sample.html">(Get access to this free module)</a><?php } ?></li>
                                            <li>Introduction to Information Types (25:34)</li>
                                            <li>Presenting Procedure (Part 1 + Part 2) (22:24 + 16:00)</li>
                                            <li>Presenting Process (20:16)</li>
                                            <li>Presenting Structure (14:38)</li>
                                            <li>Presenting Concept (15:36)</li>
                                            <li>Presenting Principle (18:05)</li>
                                            <li>Presenting Fact (18:16)</li>
                                            <li>Managing large documents (23:02)</li>
                                            <li>Document development process (Part 1 + Part 2) (15:24 + 30:22)</li>
                                            <li>Wrap-up exercise (22:48)</li>
                                        </ul>
                                    </div>
                                </div>
                            </div>
                            <div class="panel panel-default">
                                <div class="panel-heading">
                                    <h4 class="panel-title">
                                        <a data-toggle="collapse" data-parent="#accordion" href="#collapseTwo" class="collapsed">
                                            <i class="fa fa-pencil"></i>Downloadable Exercises
                                        </a>
                                    </h4>
                                </div>
                                <div id="collapseTwo" class="panel-collapse collapse">
                                    <div class="panel-body">
                                        <p>
                                            The course features many hands-on exercises to ensure that you’ll quickly master your new skill set.
                                            The exercises range from fill in the blanks to writing exercises in which you apply the Information Mapping® Method to analyze, organize and present source material.
                                            The final wrap-up exercise will take you through the entire document development process step by step and allow you to create your first complete Mapped document from the ground up.
                                            Karen will guide you by introducing each exercise and providing sample answers.
                                        </p>
                                    </div>
                                </div>
                            </div>
                            <div class="panel panel-default">
                                <div class="panel-heading">
                                    <h4 class="panel-title">
                                        <a data-toggle="collapse" data-parent="#accordion" href="#collapseThree" class="collapsed">
                                            <i class="fa fa-thumbs-o-up"></i>Sample Answers
                                        </a>
                                    </h4>
                                </div>
                                <div id="collapseThree" class="panel-collapse collapse">
                                    <div class="panel-body">
                                        <p>For every exercise, you will be able to download the sample answer. You can compare the sample answer to your own answer,
                                            and Karen will explain where the common pitfalls are for each exercise and highlight the key points of the sample answer.
                                            This way, you will learn by doing, as if you were in a real-life classroom training.
                                        </p>
                                    </div>
                                </div>
                            </div>
                        </div>
                        <!-- accordion end -->
                    </div>
                    <div class="col-md-5 pull-right text-center sample_modal_image">
                        <?php if(!isset($_GET['s'])||($_GET['s']!='trial')){ ?>
                        <a class="" data-toggle="modal" data-target="#myModalSample"><img src="images/free_module.png" class="img-responsive"/></a>
                        <div>
                            <p class="lead text-center">Curious what the course looks like? </p>
                            <p>Get immediate access to the <strong>FREE MODULE</strong> and learn everything about Blocks and Maps!</p>
                        </div>
                        <a class="btn btn-default contact more" data-toggle="modal" data-target="#myModalSample">Sign up for the FREE MODULE<i class="pl-10 fa fa-info"></i>
                            <?php } else { ?>
                            <a class="" href="free-sample.html"><img src="images/free_module.png" class="img-responsive"/></a>
                            <a class="btn btn-white more"  href="free-sample.html">Sign up for the FREE MODULE<i class="pl-10 fa fa-info"></i>
                                <?php }  ?>
                            </a>
                    </div>

                    <!-- Modal -->
                    <div class="modal fade" id="myModalSample" tabindex="-1" role="dialog" aria-labelledby="myModalLabel" aria-hidden="true">
                        <div class="modal-dialog">
                            <div class="modal-content">
                                <div class="modal-header">
                                    <button type="button" class="close" data-dismiss="modal"><span aria-hidden="true">&times;</span><span class="sr-only">Close</span></button>
                                    <h4 class="modal-title" id="myModalLabel">FREE MODULE about Blocks and Maps</h4>
                                </div>
                                <div class="modal-body">
                                    <p>Enter your email below and we'll send you a link to the free module so that you'll learn more about Blocks and Maps.</p>
                                    <form role="form" action="https://go.pardot.com/l/8622/2015-06-15/pj57m" method="post">
                                        <div class="form-group">
                                            <label for="firstname">First Name</label>
                                            <input type="text" class="form-control" id="firstname" name="firstname" placeholder="Enter your first name" required>
                                        </div>
                                        <div class="form-group">
                                            <label for="lastname">Last Name</label>
                                            <input type="text" class="form-control" id="lastname" name="lastname" placeholder="Enter your last name" required>
                                        </div>
                                        <div class="form-group">
                                            <label for="company">Company</label>
                                            <input type="text" class="form-control" id="company" name="company" placeholder="Enter your company name" required>
                                        </div>

                                        <div class="form-group">
                                            <label for="country"><strong>Country</strong></label>
                                            <select class="form-control bfh-countries" data-country="United States"  name="country" required></select>
                                        </div>
                                        <div class="form-group">
                                            <label for="phone">Phone</label>
                                            <input type="text" class="form-control" id="phone" name="phone" placeholder="Enter your phone number" required>
                                        </div>
                                        <div class="form-group">
                                            <label for="email">Email address</label>
                                            <input type="email" class="form-control" id="email" name="email" placeholder="Enter email" required>
                                        </div>

                                        <button type="submit" class="btn btn-default" >Send me the link</button>
                                    </form>
                                </div>
                            </div>
                        </div>
                    </div>


                </div>
            </div>
        </div>

        <!-- section start -->
        <!-- ================ -->
        <div class="section parallax parallax-bg-3" id="section4">
            <div class="container">
                <div class="call-to-action">
                    <div class="row">
                        <div class="col-md-8">
                            <h1 class="title text-center">For a limited time! <em>Just $595</em></h1>
                            <p class="text-center">Save $100 by when you buy <em>today!</em></p>
                        </div>
                        <div class="col-md-4">
                            <div class="text-center">
                                <a href="#buynow" class="btn btn-default btn-lg">Buy now <i class="pl-10 fa fa-shopping-cart"></i></a>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
        <!-- section end -->

        <!-- section start -->
        <!-- ================ -->
        <div class="section clearfix">
            <div class="container">
                <div class="row">
                    <div class="col-md-12">

                        <h1 class="text-center">What people are <em>saying</em> about the course</h1>
                        <div class="separator"></div>
                        <p class="lead text-center">The course has already helped many people worldwide to change the way they write. Check out their reviews!</p>
                    </div>
                </div>
                <div class="row">
                    <div class="col-md-8 col-md-offset-2">
                        <div class="owl-carousel content-slider-with-controls-bottom">
                            <div class="testimonial clearfix ">
                                <div class="testimonial-image">
                                    <img src="images/erwin.jpg" alt="Erwin Chopee" title="Erwin Chopee, anager HSE Technical Services, Hydro One" class="img-circle">
                                </div>
                                <div class="testimonial-body">
                                    <p>Writing with this methodology causes you to think about communications in a structured manner. Productivity is then gained by clearly communicating important issues to the audience with very little clarification required.</p>
                                    <div class="testimonial-info-1">Erwin Chopee</div>
                                    <div class="testimonial-info-2">Manager HSE Technical Services, Hydro One</div>
                                </div>
                            </div>

                            <div class="testimonial clearfix">
                                <div class="testimonial-image">
                                    <img src="images/giselle.jpg" alt="Giselle Mazurat" title="Giselle Mazurat, Technical Writer, Hewlett-Packard" class="img-circle">
                                </div>
                                <div class="testimonial-body">
                                    <p>I love Information Mapping. I don't know how writers can develop technical manuals without it. I'm glad I learned it early in my career.</p>
                                    <div class="testimonial-info-1">Giselle Mazurat</div>
                                    <div class="testimonial-info-2">Technical Writer, Hewlett-Packard</div>
                                </div>
                            </div>

                            <div class="testimonial clearfix">
                                <div class="testimonial-image">
                                    <img src="images/tammy.jpg" alt="Tammy Popper" title="Tammy Popper, HR Specialist, Boehringer Ingelheim" class="img-circle">
                                </div>
                                <div class="testimonial-body">
                                    <p>At first I wasn’t sure how this would apply to my work environment. But after finishing the course I was surprised and excited to start using Information Mapping!  I believe this will improve my productivity and bring a better understanding of procedures, practices, and many other activities on a daily basis.  I could have used this class years ago! Thank you guys, you did a fantastic job!</p>
                                    <div class="testimonial-info-1">Tammy Popper</div>
                                    <div class="testimonial-info-2">HR Specialist, Boehringer Ingelheim</div>
                                </div>
                            </div>

                            <div class="testimonial clearfix">
                                <div class="testimonial-image">
                                    <img src="images/joseph.jpg" alt="Joseph Guevara" title="Joseph Guevara, Member of Technical Staff at Alcatel-Lucent" class="img-circle">
                                </div>
                                <div class="testimonial-body">
                                    <p>This course was very beneficial and pertinent to my job assignment. In addition, I will be able to apply the same principles in my personal life.</p>
                                    <div class="testimonial-info-1">Joseph Guevara</div>
                                    <div class="testimonial-info-2">Member of Technical Staff at Alcatel-Lucent</div>
                                </div>
                            </div>

                            <div class="testimonial clearfix">
                                <div class="testimonial-image">
                                    <img src="images/micheal.jpg" alt="Micheal Price" title="Micheal Price, Store Operations at Charming Charlie" class="img-circle">
                                </div>
                                <div class="testimonial-body">
                                    <p>I wish I had taken this course 5 years ago. I will personally benefit from this course, as I have never been able to use a methodology that allowed me to organize my thoughts so clearly. My communication will become stronger because now I am writing with my reader in mind. Information Mapping has changed the way that I think about communication.</p>
                                    <div class="testimonial-info-1">Micheal Price</div>
                                    <div class="testimonial-info-2">Store Operations at Charming Charlie</div>
                                </div>
                            </div>
                        </div>
                    </div><!-- col-md-8-->
                </div>
            </div>
        </div>
        <!-- section end -->

        <!-- section start -->
        <!-- ================ -->
        <div class="section clearfix gray-bg">
            <div class="container">
                <div class="row">
                    <div class="col-md-12">
                        <h1 class="text-center">About the <em>instructor</em></h1>
                        <div class="separator"></div>
                    </div>
                    <div class="col-md-10 col-md-offset-1">
                        <div class="testimonial clearfix">
                            <div class="testimonial-image">
                                <img src="images/karin.jpg" alt="Karen Krogh" title="Karen Krogh, Information Mapping Master Instructor" class="img-circle">
                            </div>
                            <div class="testimonial-body">
                                <p>Information Mapping Master Instructor <em>Karen Krogh</em> is a language specialist and translator who has taught classes in the Information Mapping® Method at organizations all over the world.</p>
                                <p>Karen’s work with the Information Mapping® Method includes implementation projects for financial organizations, airlines, shipping companies and government agencies.</p>
                                <p>“I’m delighted by how well this video course conveys the learning experience that takes place in an Information Mapping classroom.”</p>
                                <img class="pull-right" src="images/karen_signature.png"/>

                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
        <!-- section end -->
    </div>

    <!-- section start -->
    <!-- ================ -->
    <div class="section clearfix" id="buynow">
        <div class="container">
            <div class="row">
                <div id="checkout">

                    <div class="col-md-12">
                        <h1 class="text-center"><em>Buy</em> the course</h1>
                        <div class="separator"></div>
                    </div>

                    <div class="col-md-12">
                        <!-- pricing tables start -->
                        <div class="pricing-tables white object-non-visible" data-animation-effect="fadeInUpSmall">
                            <div class="row grid-space-0">

                                <!-- pricing table start -->
                                <div class="col-xs-12 col-sm-9 col-sm-offset-2 plan premium_plan">
                                    <div class="header">
                                        <h3>Information Mapping<sup>&reg;</sup> Essentials&trade; Video Course</h3>
                                    </div>
                                    <ul>
                                        <li><i class="fa fa-check"></i> 15 HD Video Modules <i data-toggle="tooltip" data-placement="right" title="90 day full access to the 15 video modules and the supporting materials" class="fa fa-info-circle"></i></li>
                                        <li><i class="fa fa-check"></i> FS Pro Software License <i data-toggle="tooltip" data-placement="right" title="1-year license of FS Pro 2013, software that makes it quick and easy to create clear and structured documents in MS Word." class="fa fa-info-circle"></i></li>
                                        <li><i class="fa fa-check"></i> Certification <i data-toggle="tooltip" data-placement="right" title="Access to the Information Mapping® Professional™ (IMP) Certification Exam" class="fa fa-info-circle"></i></li>
                                        <li class="row buynow_price">
                                            <div class="col-xs-12"><span class="strike_pricing">$695</span> <span class="pricing">Introductory price</span></div>
                                        </li>
                                        <li class="row buynow_price">
                                            <div class="col-xs-12 col-sm-4 bottommargin">
                                                $595<br/>
                                                <a class="btn btn-pink buy buy-premium"><i class="fa fa-shopping-cart pr-10"></i>Buy <span class="larger">1</span></a>
                                            </div>
                                            <div class="col-xs-12 col-sm-4 bottommargin">
                                                $425 per person<br/>
                                                <a class="btn btn-pink buy buy-premium-20"><i class="fa fa-shopping-cart pr-10"></i>Buy <span class="larger">20</span></a>
                                            </div>
                                            <div class="col-xs-12 col-sm-4 bottommargin">
                                                $395 per person<br/>
                                                <a class="btn btn-pink buy buy-premium-50"><i class="fa fa-shopping-cart pr-10"></i>Buy <span class="larger">50</span></a>
                                            </div>
                                        </li>
                                        <li class="row">
                                            <p class="nosoftware">Already have FS Pro? <a href="" class="buy buy-standard">Buy Information Mapping&reg; Essentials&trade; without software</a></p>
                                        </li>
                                    </ul>
                                </div>
                                <!-- pricing table end -->

                            </div>
                            <div class="row grid-space-0">
                                <div class="col-xs-2 col-sm-offset-1 col-sm-2 blue_badge"><img class="img-responsive" src="images/blue_badge.png" /></div>
                                <div class="col-xs-10 col-sm-8 custom_plan">
                                    <div class="row customwrap">
                                        <div class="col-xs-12 col-sm-6 col-md-8">
                                            <div class="header">
                                                <h3>Build your own <strong>custom</strong> course</h3>
                                            </div>
                                        </div>
                                        <div class="col-xs-12 col-sm-6 col-md-4">
                                            <p class="buynow_price"> from $50<br/>
                                                <a class="btn btn-pink  buy buy-modular"><i class="fa fa-shopping-cart pr-10"></i>Buy now</a>
                                            </p>
                                        </div>
                                    </div>


                                </div>
                            </div>
                        </div>
                    </div>
                    <!-- pricing tables end -->
                </div>
            </div>

        </div>
    </div>
    <!-- section start -->
    <!-- ================ -->
    <div class="clearfix" id="secure">
        <div class="container">
            <div class="row">
                <div class="col-md-8">
                    <div class="col-md-4">
                        <script language="JavaScript" type="text/javascript">
                            TrustLogo("https://www.informationmapping.com/imessentials/images/comodo_secure_113x59_white.png", "SC5", "none");
                        </script>
                        <a href="https://ssl.comodo.com/ev-ssl-certificates.php" id="comodoTL">EV SSL Certificate</a>
                        <!--<img src="images/comodo_secure_113x59_white.png" class="img-responsive" alt="Comodo secure"> -->
                    </div>
                    <div class="col-md-4">
                        <img src="images/PP_ogone1.gif" class="img-responsive" alt="Ogone secure">
                    </div>
                    <div class="col-md-4">
                        <img src="images/vseal_Norton.gif" class="img-responsive" alt="Norton seal">
                    </div>
                </div>
            </div>
        </div>
    </div>
    <!-- section end -->
    <!-- section end -->

    <!-- section start -->
    <!-- ================ -->
    <div class="section clearfix" id="social">
        <div class="container">
            <div class="row">
                <div class="col-md-12">
                    <h2 class="text-center"><em>Connect</em> with us</h2>
                    <div class="separator"></div>
                    <ul class="text-center social-links colored circle">
                        <li class="facebook"><a target="_blank" href="https://www.facebook.com/changethewayyouwrite"><i class="fa fa-facebook"></i></a></li>
                        <li class="twitter"><a target="_blank" href="https://twitter.com/infomap"><i class="fa fa-twitter"></i></a></li>
                        <li class="googleplus"><a target="_blank" href="https://plus.google.com/share?url=https://www.informationmapping.com/"><i class="fa fa-google-plus"></i></a></li>
                        <li class="linkedin"><a target="_blank" href="https://www.linkedin.com/grp/home?gid=983927"><i class="fa fa-linkedin"></i></a></li>
                    </ul>
                </div>
            </div>
        </div>
    </div>
    <!-- section end -->

    <!-- section start -->
    <!-- ================ Hide questions section to use olark instead
    <div class="section clearfix">
        <div class="container">
            <div class="row">
                <div class="col-md-12">
                    <h2 class="text-center">Questions?</h2>
                    <div class="separator"></div>
                </div>
                <div class="col-md-8 col-md-offset-2">

                    <div class="alert alert-success hidden" id="MessageSent">
                        We have received your message, we will contact you very soon.
                    </div>
                    <div class="alert alert-danger hidden" id="MessageNotSent">
                        Oops! Something went wrong please refresh the page and try again.
                    </div>
                    <div class="contact-form">
                        <form id="contact-form" role="form">
                            <div class="form-group has-feedback">
                                <label for="name">Your name*</label>
                                <input type="text" class="form-control" id="name" name="name" placeholder="">
                                <i class="fa fa-user form-control-feedback"></i>
                            </div>
                            <div class="form-group has-feedback">
                                <label for="email">Your email*</label>
                                <input type="email" class="form-control" id="email" name="email" placeholder="">
                                <i class="fa fa-envelope form-control-feedback"></i>
                            </div>
                            <div class="form-group has-feedback">
                                <label for="message">Message*</label>
                                <textarea class="form-control" rows="6" id="message" name="message" placeholder=""></textarea>
                                <i class="fa fa-pencil form-control-feedback"></i>
                            </div>
                            <input type="submit" value="Submit" class="submit-button btn btn-default">
                        </form>
                    </div>
                </div>

            </div>
        </div>
    </div>
    -->
</div>
<!-- section end -->

</div>
<!-- page-wrapper end -->

<!-- JavaScript files placed at the end of the document so the pages load faster
================================================== -->
<!-- Jquery and Bootstap core js files -->
<script type="text/javascript" src="plugins/jquery.min.js"></script>
<script type="text/javascript" src="bootstrap/js/bootstrap.min.js"></script>

<!-- Modernizr javascript -->
<script type="text/javascript" src="plugins/modernizr.js"></script>

<!-- jQuery REVOLUTION Slider  -->
<script type="text/javascript" src="plugins/rs-plugin/js/jquery.themepunch.tools.min.js"></script>
<script type="text/javascript" src="plugins/rs-plugin/js/jquery.themepunch.revolution.min.js"></script>

<!-- Isotope javascript -->
<script type="text/javascript" src="plugins/isotope/isotope.pkgd.min.js"></script>

<!-- Owl carousel javascript -->
<script type="text/javascript" src="plugins/owl-carousel/owl.carousel.js"></script>

<!-- Magnific Popup javascript -->
<script type="text/javascript" src="plugins/magnific-popup/jquery.magnific-popup.min.js"></script>

<!-- Appear javascript -->
<script type="text/javascript" src="plugins/jquery.appear.js"></script>

<!-- Count To javascript -->
<script type="text/javascript" src="plugins/jquery.countTo.js"></script>

<!-- Parallax javascript -->
<script src="plugins/jquery.parallax-1.1.3.js"></script>

<!-- Contact form -->
<script src="plugins/jquery.validate.js"></script>

<!-- Word rotator of Plugins -->
<script type="text/javascript" src="js/jquery.simple-text-rotator.min.js"></script>

<!-- Initialization of Plugins -->
<script type="text/javascript" src="js/template.js"></script>

<!-- Bootstrap Form Helpers -->
<script src="js/bootstrap-formhelpers.min.js"></script>
<script src="js/bootstrap-formhelpers-countries.js"></script>

<script type="text/javascript" src="js/pluggablecheckout.js"></script>
<script type="text/javascript" src="js/custom.js"></script>

<?php if (($_GET['success'])) { ?>
    <script> $('#success').modal('show'); </script>
<?php } ?>



<script>

    var loadingCheckoutReady = function(){
// Deze functie zullen we gebruiken wanneer	de checkout tevoorschijn komt
        jQuery('.checkout-loading').hide();
    };

    var startLoading = function(){
        jQuery('.checkout-loading').show();
        jQuery('.buy').hide();
    }

    jQuery(document).ready(function(){

        jQuery('#secure').hide();

// Initialiseer de checkout in de achtergrond.
// Argument 1: De magento installatie
// Argument 2: Deze functie wordt uitgevoerd als de checkout geladen is - in de praktijk wachten we met het tonen van de buttons, tot de checkout er klaar voor is.
// Argument 3: URL naar het proxy script. Dit kan een relatieve URL zijn als het proxy script in een map staat bvb.
// Het proxy script MOET ALTIJD op dezelfde domeinnaam staan als deze pagina.
        AngularCheckout.init('https://www.informationmapping.com/en/shop/', function(){

// Dit wordt uitgevoerd als de checkout geladen is.
// De knoppen zijn aanvankelijk verborgen, en we tonen ze nu pas. Eerder werken ze toch niet.
            jQuery('.buy').show();

        },'proxy/index.php');


// Nu volgt het gedrag voor de 2 knoppen...

        jQuery('.buy-premium').click(function(e){
// Er werd geklikt op de knop "premium"

// Standaard gedrag browser verhinderen
            e.preventDefault();


// Blokkeer alle koop-knoppen, zodat geen 2de keer geklikt kan worden
            startLoading();
            // Hide rest van website bij orders
            jQuery('#social').hide();
            jQuery('#header').hide();
            jQuery('#rest').hide();
            jQuery('#secure').show();


// Maak het mandje leeg...
            AngularCheckout.emptyCart();


// Voeg producten toe op basis van SKU en aantal
            AngularCheckout.addProduct('roy-informationmapping-essentials-plus', 1);


// Toon de checkout op de gewenste plaats
            AngularCheckout.renderInside('#checkout', loadingCheckoutReady);

        });
        jQuery('.buy-premium-20').click(function(e){
// Er werd geklikt op de knop "premium"

// Standaard gedrag browser verhinderen
            e.preventDefault();


// Blokkeer alle koop-knoppen, zodat geen 2de keer geklikt kan worden
            startLoading();
            // Hide rest van website bij orders
            jQuery('#social').hide();
            jQuery('#header').hide();
            jQuery('#rest').hide();
            jQuery('#secure').show();


// Maak het mandje leeg...
            AngularCheckout.emptyCart();


// Voeg producten toe op basis van SKU en aantal
            AngularCheckout.addProduct('roy-informationmapping-essentials-plus-20', 1);


// Toon de checkout op de gewenste plaats
            AngularCheckout.renderInside('#checkout', loadingCheckoutReady);

        });
        jQuery('.buy-premium-50').click(function(e){
// Er werd geklikt op de knop "premium"

// Standaard gedrag browser verhinderen
            e.preventDefault();


// Blokkeer alle koop-knoppen, zodat geen 2de keer geklikt kan worden
            startLoading();
            // Hide rest van website bij orders
            jQuery('#social').hide();
            jQuery('#header').hide();
            jQuery('#rest').hide();
            jQuery('#secure').show();


// Maak het mandje leeg...
            AngularCheckout.emptyCart();


// Voeg producten toe op basis van SKU en aantal
            AngularCheckout.addProduct('roy-informationmapping-essentials-plus-50', 1);


// Toon de checkout op de gewenste plaats
            AngularCheckout.renderInside('#checkout', loadingCheckoutReady);

        });

        jQuery('.buy-standard').click(function(e){
// Er werd geklikt op de knop "standard"

// Standaard gedrag browser verhinderen
            e.preventDefault();


// Blokkeer alle koop-knoppen, zodat geen 2de keer geklikt kan worden
            startLoading();

            // Hide rest van website bij orders
            jQuery('#social').hide();
            jQuery('#header').hide();
            jQuery('#rest').hide();
            jQuery('#secure').show();
// Maak het mandje leeg...
            AngularCheckout.emptyCart();

// Voeg producten toe op basis van SKU en aantal
            AngularCheckout.addProduct('roy-informationmapping-essentials-basic', 1);

// Toon de checkout op de gewenste plaats
            AngularCheckout.renderInside('#checkout', loadingCheckoutReady);

        });

        jQuery('.buy-modular').click(function(e){
// Er werd geklikt op de knop "standard"

// Standaard gedrag browser verhinderen
            e.preventDefault();

// Blokkeer alle koop-knoppen, zodat geen 2de keer geklikt kan worden
            startLoading();

            // Hide rest van website bij orders
            jQuery('#social').hide();
            jQuery('#header').hide();
            jQuery('#rest').hide();
            jQuery('#secure').show();

// Maak het mandje leeg...
            AngularCheckout.emptyCart();

// Duid bundel product aan met items die voorgesteld zullen worden
            AngularCheckout.setProductBundle('roy-informationmapping-essentials-modular');

// Toon de checkout op de gewenste plaats
            AngularCheckout.renderInside('#checkout', loadingCheckoutReady);

        });

    });



</script>


<!-- Google Analytics -->
<script>
    (function(i,s,o,g,r,a,m){i['GoogleAnalyticsObject']=r;i[r]=i[r]||function(){
        (i[r].q=i[r].q||[]).push(arguments)},i[r].l=1*new Date();a=s.createElement(o),
        m=s.getElementsByTagName(o)[0];a.async=1;a.src=g;m.parentNode.insertBefore(a,m)
    })(window,document,'script','//www.google-analytics.com/analytics.js','ga');

    ga('create', 'UA-24028581-1', 'auto', {'allowLinker': true});
    // On every domain.
    ga('require', 'linker');
    // List of every domain to share linker parameters.
    ga('linker:autoLink', ['informationmapping.com', 'informationmapping-webinars.com']);
    ga('send', 'pageview'); // Send hits after initializing the auto-linker plug-in.
    ga('require', 'ecommerce');
</script>
<!-- End Google Analytics -->

<script type="text/javascript">
    piAId = '9622';
    piCId = '34568';

    (function() {
        function async_load(){
            var s = document.createElement('script'); s.type = 'text/javascript';
            s.src = ('https:' == document.location.protocol ? 'https://pi' : 'http://cdn') + '.pardot.com/pd.js';
            var c = document.getElementsByTagName('script')[0]; c.parentNode.insertBefore(s, c);
        }
        if(window.attachEvent) { window.attachEvent('onload', async_load); }
        else { window.addEventListener('load', async_load, false); }
    })();
</script>
<script type="text/javascript" src="/templates/imi/javascript/opentrack.js"></script>
<!-- Google-code voor remarketingtag -->
<script type="text/javascript">
    /* <![CDATA[ */
    var google_conversion_id = 963549049;
    var google_custom_params = window.google_tag_params;
    var google_remarketing_only = true;
    /* ]]> */
</script>
<script type="text/javascript" src="//www.googleadservices.com/pagead/conversion.js">
</script>
<noscript>
    <div style="display:inline;">
        <img height="1" width="1" style="border-style:none;" alt="" src="//googleads.g.doubleclick.net/pagead/viewthroughconversion/963549049/?value=0&amp;guid=ON&amp;script=0"/>
    </div>
</noscript>

<!-- begin olark code -->
<script data-cfasync="false" type='text/javascript'>/*<![CDATA[*/window.olark||(function(c){var f=window,d=document,l=f.location.protocol=="https:"?"https:":"http:",z=c.name,r="load";var nt=function(){
        f[z]=function(){
            (a.s=a.s||[]).push(arguments)};var a=f[z]._={
        },q=c.methods.length;while(q--){(function(n){f[z][n]=function(){
            f[z]("call",n,arguments)}})(c.methods[q])}a.l=c.loader;a.i=nt;a.p={
            0:+new Date};a.P=function(u){
            a.p[u]=new Date-a.p[0]};function s(){
            a.P(r);f[z](r)}f.addEventListener?f.addEventListener(r,s,false):f.attachEvent("on"+r,s);var ld=function(){function p(hd){
            hd="head";return["<",hd,"></",hd,"><",i,' onl' + 'oad="var d=',g,";d.getElementsByTagName('head')[0].",j,"(d.",h,"('script')).",k,"='",l,"//",a.l,"'",'"',"></",i,">"].join("")}var i="body",m=d[i];if(!m){
            return setTimeout(ld,100)}a.P(1);var j="appendChild",h="createElement",k="src",n=d[h]("div"),v=n[j](d[h](z)),b=d[h]("iframe"),g="document",e="domain",o;n.style.display="none";m.insertBefore(n,m.firstChild).id=z;b.frameBorder="0";b.id=z+"-loader";if(/MSIE[ ]+6/.test(navigator.userAgent)){
            b.src="javascript:false"}b.allowTransparency="true";v[j](b);try{
            b.contentWindow[g].open()}catch(w){
            c[e]=d[e];o="javascript:var d="+g+".open();d.domain='"+d.domain+"';";b[k]=o+"void(0);"}try{
            var t=b.contentWindow[g];t.write(p());t.close()}catch(x){
            b[k]=o+'d.write("'+p().replace(/"/g,String.fromCharCode(92)+'"')+'");d.close();'}a.P(2)};ld()};nt()})({
        loader: "static.olark.com/jsclient/loader0.js",name:"olark",methods:["configure","extend","declare","identify"]});
    /* custom configuration goes here (www.olark.com/documentation) */
    olark.identify('3504-626-10-9459');/*]]>*/</script>
<noscript><a href="https://www.olark.com/site/3504-626-10-9459/contact" title="Contact us" target="_blank">Questions? Feedback?</a> powered by <a href="http://www.olark.com?welcome" title="Olark live chat software">Olark live chat software</a></noscript>
<!-- end olark code -->

</body>
</html>
