<?php
session_start()

?>
<!DOCTYPE html>
<!--[if IE 9]> <html lang="en" class="ie9"> <![endif]-->
<!--[if IE 8]> <html lang="en" class="ie8"> <![endif]-->
<!--[if !IE]><!-->
<html lang="en">
<!--<![endif]-->
<script type="text/javascript">
    //<![CDATA[
    var tlJsHost = ((window.location.protocol == "https:") ? "https://secure.comodo.com/" : "http://www.trustlogo.com/");
    document.write(unescape("%3Cscript src='" + tlJsHost + "trustlogo/javascript/trustlogo.js' type='text/javascript'%3E%3C/script%3E"));
    //]]>
</script>
<head>
    <meta charset="utf-8">
    <title></title>
    <meta name="description" content="">
    <meta name="author" content="">

    <!-- Mobile Meta -->
    <meta name="viewport" content="width=device-width, initial-scale=1.0">

    <!-- Favicon -->
    <link rel="shortcut icon" href="images/favicon.ico">

    <!-- Web Fonts -->
    <link href='https://fonts.googleapis.com/css?family=Open+Sans:400italic,700italic,400,700,300&amp;subset=latin,latin-ext' rel='stylesheet' type='text/css'>
    <link href='https://fonts.googleapis.com/css?family=PT+Serif' rel='stylesheet' type='text/css'>

    <!-- Bootstrap core CSS -->
    <link href="bootstrap/css/bootstrap.css" rel="stylesheet">
    <link href="fonts/font-awesome/css/font-awesome.css" rel="stylesheet">
    <link href="fonts/fontello/css/fontello.css" rel="stylesheet">

    <!-- Plugins -->
    <link href="plugins/rs-plugin/css/settings.css" media="screen" rel="stylesheet">
    <link href="plugins/rs-plugin/css/extralayers.css" media="screen" rel="stylesheet">
    <link href="plugins/magnific-popup/magnific-popup.css" rel="stylesheet">
    <link href="css/animations.css" rel="stylesheet">
    <link href="plugins/owl-carousel/owl.carousel.css" rel="stylesheet">

    <!-- iDea core CSS file -->
    <link href="css/style.css" rel="stylesheet">
    <link href="css/information_mapping.css" rel="stylesheet">

    <!-- Custom css -->
    <link href="css/custom.css" rel="stylesheet">

    <!-- HTML5 shim and Respond.js for IE8 support of HTML5 elements and media queries -->
    <!--[if lt IE 9]>
    <script src="https://oss.maxcdn.com/html5shiv/3.7.2/html5shiv.min.js"></script>
    <script src="https://oss.maxcdn.com/respond/1.4.2/respond.min.js"></script>
    <![endif]-->
</head>

<!-- body classes:
        "boxed": boxed layout mode e.g. <body class="boxed">
        "pattern-1 ... pattern-9": background patterns for boxed layout mode e.g. <body class="boxed pattern-1">
-->
<body class="front no-trans">
<div class="scrollToTop"><i class="icon-up-open-big"></i></div>

<div class="page-wrapper">


    <!-- header start classes:
        fixed: fixed navigation mode (sticky menu) e.g. <header class="header fixed clearfix">
         dark: dark header version e.g. <header class="header dark clearfix">
    ================ -->
    <header class="header fixed clearfix">
        <div class="container">
            <div class="row">
                <div class="col-md-3">

                    <div class="header-left clearfix">
                        <div class="logo">
                            <a href="https://www.informationmapping.com/imessentials/"><img id="logo" src="images/logo_information_mapping.png" alt="information mapping"></a>
                        </div>
                    </div>

                </div>
                <div class="col-md-9">

                    <!-- header-right start -->
                    <!-- ================ -->
                    <div class="header-right clearfix">

                        <!-- main-navigation start -->
                        <!-- ================ -->
                        <div id="header" class="main-navigation animated">

                            <!-- navbar start -->
                            <nav class="navbar navbar-default" role="navigation">
                                <div class="container-fluid">
                                    <div class="navbar-header">
                                        <button type="button" class="navbar-toggle" data-toggle="collapse" data-target="#navbar-collapse-1">
                                            <span class="sr-only">Toggle navigation</span>
                                            <span class="icon-bar"></span>
                                            <span class="icon-bar"></span>
                                            <span class="icon-bar"></span>
                                        </button>
                                    </div>
                                    <div class="collapse navbar-collapse scrollspy smooth-scroll" id="navbar-collapse-1">
                                        <ul class="nav navbar-nav navbar-right">
                                              <li class="highlight"><a href="#buynow">BUY NOW</a></li>
                                        </ul>
                                    </div>
                                </div>
                            </nav>
                            <!-- navbar end -->

                        </div>
                        <!-- main-navigation end -->

                    </div>
                    <!-- header-right end -->

                </div>
            </div>
        </div>
    </header>
    <!-- header end -->
    <div id="rest">
        <section class="main-container">

            <div class="container">
                <div class="row">

                    <!-- main start -->
                    <!-- ================ -->
                    <div class="main col-md-12">

                        <!-- page-title start -->
                        <!-- ================ -->
                        <h1 class="page-title margin-top-clear">Free Sample - Chapter 3: Blocks and Maps</h1>
                        <!-- page-title end -->


                        <div class="embed-responsive embed-responsive-16by9">
                            <iframe src="https://player.vimeo.com/video/130336322" width="500" height="281" frameborder="0" webkitallowfullscreen="" mozallowfullscreen="" allowfullscreen=""></iframe>
                        </div

                    </div>

                </div>
                <!-- main end -->

            </div>
    </div>
    </section>
    <!-- main-container end -->

    <!-- section start -->
    <!-- ================ -->
    <div class="section parallax parallax-bg-3" id="section4">
        <div class="container">
            <div class="call-to-action">
                <div class="row">
                    <div class="col-md-12">
                        <h1 class="title text-center">Want to learn more about this video course?</h1>
                        <p class="text-center"><a class="description_link" href="index.php?s=trial">Check the full description</a></p>
                    </div>
                </div>
            </div>
        </div>
    </div>
    <!-- section end -->
</div>

<!-- section start -->
<!-- ================ -->
<div class="section clearfix" id="buynow">
    <div class="container">
        <div class="row">
            <div id="checkout">

                <div class="col-md-12">
                    <h1 class="text-center"><em>Buy</em> the course</h1>
                    <div class="separator"></div>
                </div>

                <div class="col-md-12">
                    <!-- pricing tables start -->
                    <div class="pricing-tables white object-non-visible" data-animation-effect="fadeInUpSmall">
                        <div class="row grid-space-0">

                            <!-- pricing table start -->
                            <div class="col-xs-12 col-sm-9 col-sm-offset-2 plan premium_plan">
                                <div class="header">
                                    <h3>Information Mapping<sup>&reg;</sup> Essentials&trade; Video Course</h3>
                                </div>
                                <ul>
                                    <li><i class="fa fa-check"></i> 15 HD Video Modules <i data-toggle="tooltip" data-placement="right" title="6 months full access to the 15 video modules and the supporting materials" class="fa fa-info-circle"></i></li>
                                    <li><i class="fa fa-check"></i> FS Pro Software License <i data-toggle="tooltip" data-placement="right" title="1-year license of FS Pro 2013, software that makes it quick and easy to create clear and structured documents in MS Word." class="fa fa-info-circle"></i></li>
                                    <li><i class="fa fa-check"></i> Certification <i data-toggle="tooltip" data-placement="right" title="Access to the Information Mapping® Professional™ (IMP) Certification Exam" class="fa fa-info-circle"></i></li>
                                    <li class="row buynow_price">
                                        <div class="col-xs-12"><span class="strike_pricing">$695</span> <span class="pricing">Introductory price</span></div>
                                    </li>
                                    <li class="row buynow_price">
                                        <div class="col-xs-12 col-sm-4 bottommargin">
                                            $595<br/>
                                            <a class="btn btn-pink buy buy-premium"><i class="fa fa-shopping-cart pr-10"></i>Buy <span class="larger">1</span></a>
                                        </div>
                                        <div class="col-xs-12 col-sm-4 bottommargin">
                                            $425 per person<br/>
                                            <a class="btn btn-pink buy buy-premium-20"><i class="fa fa-shopping-cart pr-10"></i>Buy <span class="larger">20</span></a>
                                        </div>
                                        <div class="col-xs-12 col-sm-4 bottommargin">
                                            $395 per person<br/>
                                            <a class="btn btn-pink buy buy-premium-50"><i class="fa fa-shopping-cart pr-10"></i>Buy <span class="larger">50</span></a>
                                        </div>
                                    </li>
                                    <li class="row">
                                        <p class="nosoftware">Already have FS Pro? <a href="" class="buy buy-standard">Buy Information Mapping&reg; Essentials&trade; without software</a></p>
                                    </li>
                                </ul>
                            </div>
                            <!-- pricing table end -->

                        </div>
                        <div class="row grid-space-0">
                            <div class="col-xs-2 col-sm-offset-1 col-sm-2 blue_badge"><img class="img-responsive" src="images/blue_badge.png" /></div>
                            <div class="col-xs-10 col-sm-8 custom_plan">
                                <div class="row customwrap">
                                    <div class="col-xs-12 col-sm-6 col-md-8">
                                        <div class="header">
                                            <h3>Build your own <strong>custom</strong> course</h3>
                                        </div>
                                    </div>
                                    <div class="col-xs-12 col-sm-6 col-md-4">
                                        <p class="buynow_price"> from $50<br/>
                                            <a class="btn btn-pink  buy buy-modular"><i class="fa fa-shopping-cart pr-10"></i>Buy now</a>
                                        </p>
                                    </div>
                                </div>


                            </div>
                        </div>
                    </div>
                </div>
                <!-- pricing tables end -->
            </div>
        </div>

    </div>
</div>
<!-- pricing tables end -->
<!-- section start -->
<!-- ================ -->
<div class="clearfix" id="secure">
    <div class="container">
        <div class="row">
            <div class="col-md-8">
                <div class="col-md-4">
                    <script language="JavaScript" type="text/javascript">
                        TrustLogo("https://www.informationmapping.com/imessentials/images/comodo_secure_113x59_white.png", "SC5", "none");
                    </script>
                    <a href="https://ssl.comodo.com/ev-ssl-certificates.php" id="comodoTL">EV SSL Certificate</a>
                    <!--<img src="images/comodo_secure_113x59_white.png" class="img-responsive" alt="Comodo secure"> -->
                </div>
                <div class="col-md-4">
                    <img src="images/PP_ogone1.gif" class="img-responsive" alt="Ogone secure">
                </div>
                <div class="col-md-4">
                    <img src="images/vseal_Norton.gif" class="img-responsive" alt="Norton seal">
                </div>
            </div>
        </div>
    </div>
</div>
<!-- section end -->
</div>
</div>

</div>
</div>
</div>
<!-- section end -->


</div>
<!-- page-wrapper end -->

<!-- JavaScript files placed at the end of the document so the pages load faster
================================================== -->
<!-- Jquery and Bootstap core js files -->
<script type="text/javascript" src="plugins/jquery.min.js"></script>
<script type="text/javascript" src="bootstrap/js/bootstrap.min.js"></script>

<!-- Modernizr javascript -->
<script type="text/javascript" src="plugins/modernizr.js"></script>

<!-- jQuery REVOLUTION Slider  -->
<script type="text/javascript" src="plugins/rs-plugin/js/jquery.themepunch.tools.min.js"></script>
<script type="text/javascript" src="plugins/rs-plugin/js/jquery.themepunch.revolution.min.js"></script>

<!-- Isotope javascript -->
<script type="text/javascript" src="plugins/isotope/isotope.pkgd.min.js"></script>

<!-- Owl carousel javascript -->
<script type="text/javascript" src="plugins/owl-carousel/owl.carousel.js"></script>

<!-- Magnific Popup javascript -->
<script type="text/javascript" src="plugins/magnific-popup/jquery.magnific-popup.min.js"></script>

<!-- Appear javascript -->
<script type="text/javascript" src="plugins/jquery.appear.js"></script>

<!-- Count To javascript -->
<script type="text/javascript" src="plugins/jquery.countTo.js"></script>

<!-- Parallax javascript -->
<script src="plugins/jquery.parallax-1.1.3.js"></script>

<!-- Contact form -->
<script src="plugins/jquery.validate.js"></script>

<!-- Word rotator of Plugins -->
<script type="text/javascript" src="js/jquery.simple-text-rotator.min.js"></script>

<!-- Initialization of Plugins -->
<script type="text/javascript" src="js/template.js"></script>

<!-- Custom Scripts -->
<script type="text/javascript" src="js/pluggablecheckout.js"></script>
<script type="text/javascript" src="js/custom.js"></script>
<script>

    var loadingCheckoutReady = function(){
// Deze functie zullen we gebruiken wanneer	de checkout tevoorschijn komt
        jQuery('.checkout-loading').hide();
    };

    var startLoading = function(){
        jQuery('.checkout-loading').show();
        jQuery('.buy').hide();
    }

    jQuery(document).ready(function(){

        jQuery('#secure').hide();

// Initialiseer de checkout in de achtergrond.
// Argument 1: De magento installatie
// Argument 2: Deze functie wordt uitgevoerd als de checkout geladen is - in de praktijk wachten we met het tonen van de buttons, tot de checkout er klaar voor is.
// Argument 3: URL naar het proxy script. Dit kan een relatieve URL zijn als het proxy script in een map staat bvb.
// Het proxy script MOET ALTIJD op dezelfde domeinnaam staan als deze pagina.
        AngularCheckout.init('https://www.informationmapping.com/en/shop/', function(){

// Dit wordt uitgevoerd als de checkout geladen is.
// De knoppen zijn aanvankelijk verborgen, en we tonen ze nu pas. Eerder werken ze toch niet.
            jQuery('.buy').show();

        },'proxy/index.php');


// Nu volgt het gedrag voor de 2 knoppen...

        jQuery('.buy-premium').click(function(e){
// Er werd geklikt op de knop "premium"

// Standaard gedrag browser verhinderen
            e.preventDefault();


// Blokkeer alle koop-knoppen, zodat geen 2de keer geklikt kan worden
            startLoading();
            // Hide rest van website bij orders
            jQuery('#social').hide();
            jQuery('#header').hide();
            jQuery('#rest').hide();
            jQuery('#secure').show();


// Maak het mandje leeg...
            AngularCheckout.emptyCart();


// Voeg producten toe op basis van SKU en aantal
            AngularCheckout.addProduct('roy-informationmapping-essentials-plus', 1);


// Toon de checkout op de gewenste plaats
            AngularCheckout.renderInside('#checkout', loadingCheckoutReady);

        });
        jQuery('.buy-premium-20').click(function(e){
// Er werd geklikt op de knop "premium"

// Standaard gedrag browser verhinderen
            e.preventDefault();


// Blokkeer alle koop-knoppen, zodat geen 2de keer geklikt kan worden
            startLoading();
            // Hide rest van website bij orders
            jQuery('#social').hide();
            jQuery('#header').hide();
            jQuery('#rest').hide();
            jQuery('#secure').show();


// Maak het mandje leeg...
            AngularCheckout.emptyCart();


// Voeg producten toe op basis van SKU en aantal
            AngularCheckout.addProduct('roy-informationmapping-essentials-plus-20', 1);


// Toon de checkout op de gewenste plaats
            AngularCheckout.renderInside('#checkout', loadingCheckoutReady);

        });
        jQuery('.buy-premium-50').click(function(e){
// Er werd geklikt op de knop "premium"

// Standaard gedrag browser verhinderen
            e.preventDefault();


// Blokkeer alle koop-knoppen, zodat geen 2de keer geklikt kan worden
            startLoading();
            // Hide rest van website bij orders
            jQuery('#social').hide();
            jQuery('#header').hide();
            jQuery('#rest').hide();
            jQuery('#secure').show();


// Maak het mandje leeg...
            AngularCheckout.emptyCart();


// Voeg producten toe op basis van SKU en aantal
            AngularCheckout.addProduct('roy-informationmapping-essentials-plus-50', 1);


// Toon de checkout op de gewenste plaats
            AngularCheckout.renderInside('#checkout', loadingCheckoutReady);

        });

        jQuery('.buy-standard').click(function(e){
// Er werd geklikt op de knop "standard"

// Standaard gedrag browser verhinderen
            e.preventDefault();


// Blokkeer alle koop-knoppen, zodat geen 2de keer geklikt kan worden
            startLoading();

            // Hide rest van website bij orders
            jQuery('#social').hide();
            jQuery('#header').hide();
            jQuery('#rest').hide();
            jQuery('#secure').show();
// Maak het mandje leeg...
            AngularCheckout.emptyCart();

// Voeg producten toe op basis van SKU en aantal
            AngularCheckout.addProduct('roy-informationmapping-essentials-basic', 1);

// Toon de checkout op de gewenste plaats
            AngularCheckout.renderInside('#checkout', loadingCheckoutReady);

        });

        jQuery('.buy-modular').click(function(e){
// Er werd geklikt op de knop "standard"

// Standaard gedrag browser verhinderen
            e.preventDefault();

// Blokkeer alle koop-knoppen, zodat geen 2de keer geklikt kan worden
            startLoading();

            // Hide rest van website bij orders
            jQuery('#social').hide();
            jQuery('#header').hide();
            jQuery('#rest').hide();
            jQuery('#secure').show();

// Maak het mandje leeg...
            AngularCheckout.emptyCart();

// Duid bundel product aan met items die voorgesteld zullen worden
            AngularCheckout.setProductBundle('roy-informationmapping-essentials-modular');

// Toon de checkout op de gewenste plaats
            AngularCheckout.renderInside('#checkout', loadingCheckoutReady);

        });

    });



</script>

<!-- Google Analytics -->
<script>
    (function(i,s,o,g,r,a,m){i['GoogleAnalyticsObject']=r;i[r]=i[r]||function(){
        (i[r].q=i[r].q||[]).push(arguments)},i[r].l=1*new Date();a=s.createElement(o),
        m=s.getElementsByTagName(o)[0];a.async=1;a.src=g;m.parentNode.insertBefore(a,m)
    })(window,document,'script','//www.google-analytics.com/analytics.js','ga');

    ga('create', 'UA-24028581-1', 'auto', {'allowLinker': true});
    // On every domain.
    ga('require', 'linker');
    // List of every domain to share linker parameters.
    ga('linker:autoLink', ['informationmapping.com', 'informationmapping-webinars.com']);
    ga('send', 'pageview'); // Send hits after initializing the auto-linker plug-in.
    ga('require', 'ecommerce');
</script>
<!-- End Google Analytics -->>

<?php //endif; ?>
<script type="text/javascript">
    piAId = '9622';
    piCId = '34568';

    (function() {
        function async_load(){
            var s = document.createElement('script'); s.type = 'text/javascript';
            s.src = ('https:' == document.location.protocol ? 'https://pi' : 'http://cdn') + '.pardot.com/pd.js';
            var c = document.getElementsByTagName('script')[0]; c.parentNode.insertBefore(s, c);
        }
        if(window.attachEvent) { window.attachEvent('onload', async_load); }
        else { window.addEventListener('load', async_load, false); }
    })();
</script>
<script type="text/javascript" src="/templates/imi/javascript/opentrack.js"></script>
<!-- Google-code voor remarketingtag -->
<script type="text/javascript">
    /* <![CDATA[ */
    var google_conversion_id = 963549049;
    var google_custom_params = window.google_tag_params;
    var google_remarketing_only = true;
    /* ]]> */
</script>
<script type="text/javascript" src="//www.googleadservices.com/pagead/conversion.js">
</script>
<noscript>
    <div style="display:inline;">
        <img height="1" width="1" style="border-style:none;" alt="" src="//googleads.g.doubleclick.net/pagead/viewthroughconversion/963549049/?value=0&amp;guid=ON&amp;script=0"/>
    </div>
</noscript>

<!-- begin olark code -->
<script data-cfasync="false" type='text/javascript'>/*<![CDATA[*/window.olark||(function(c){var f=window,d=document,l=f.location.protocol=="https:"?"https:":"http:",z=c.name,r="load";var nt=function(){
        f[z]=function(){
            (a.s=a.s||[]).push(arguments)};var a=f[z]._={
        },q=c.methods.length;while(q--){(function(n){f[z][n]=function(){
            f[z]("call",n,arguments)}})(c.methods[q])}a.l=c.loader;a.i=nt;a.p={
            0:+new Date};a.P=function(u){
            a.p[u]=new Date-a.p[0]};function s(){
            a.P(r);f[z](r)}f.addEventListener?f.addEventListener(r,s,false):f.attachEvent("on"+r,s);var ld=function(){function p(hd){
            hd="head";return["<",hd,"></",hd,"><",i,' onl' + 'oad="var d=',g,";d.getElementsByTagName('head')[0].",j,"(d.",h,"('script')).",k,"='",l,"//",a.l,"'",'"',"></",i,">"].join("")}var i="body",m=d[i];if(!m){
            return setTimeout(ld,100)}a.P(1);var j="appendChild",h="createElement",k="src",n=d[h]("div"),v=n[j](d[h](z)),b=d[h]("iframe"),g="document",e="domain",o;n.style.display="none";m.insertBefore(n,m.firstChild).id=z;b.frameBorder="0";b.id=z+"-loader";if(/MSIE[ ]+6/.test(navigator.userAgent)){
            b.src="javascript:false"}b.allowTransparency="true";v[j](b);try{
            b.contentWindow[g].open()}catch(w){
            c[e]=d[e];o="javascript:var d="+g+".open();d.domain='"+d.domain+"';";b[k]=o+"void(0);"}try{
            var t=b.contentWindow[g];t.write(p());t.close()}catch(x){
            b[k]=o+'d.write("'+p().replace(/"/g,String.fromCharCode(92)+'"')+'");d.close();'}a.P(2)};ld()};nt()})({
        loader: "static.olark.com/jsclient/loader0.js",name:"olark",methods:["configure","extend","declare","identify"]});
    /* custom configuration goes here (www.olark.com/documentation) */
    olark.identify('3504-626-10-9459');/*]]>*/</script>
<noscript><a href="https://www.olark.com/site/3504-626-10-9459/contact" title="Contact us" target="_blank">Questions? Feedback?</a> powered by <a href="https://www.olark.com?welcome" title="Olark live chat software">Olark live chat software</a></noscript>
<!-- end olark code -->

</body>
</html>