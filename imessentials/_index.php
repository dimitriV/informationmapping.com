﻿<!DOCTYPE html>
<!--[if IE 9]> <html lang="en" class="ie9"> <![endif]-->
<!--[if IE 8]> <html lang="en" class="ie8"> <![endif]-->
<!--[if !IE]><!-->
<html lang="en">
<!--<![endif]-->
<head>
    <meta charset="utf-8">
    <title>Change The Way You Write</title>
    <meta name="description" content="">
    <meta name="author" content="">

    <!-- Mobile Meta -->
    <meta name="viewport" content="width=device-width, initial-scale=1.0">

    <!-- Favicon -->
    <link rel="shortcut icon" href="images/favicon.ico">

    <!-- Bootstrap core CSS -->
    <link href="bootstrap/css/bootstrap.css" rel="stylesheet">
    <link href="fonts/font-awesome/css/font-awesome.css" rel="stylesheet">
    <link href="fonts/fontello/css/fontello.css" rel="stylesheet">

    <!-- Plugins -->
    <link href="plugins/rs-plugin/css/settings.css" media="screen" rel="stylesheet">
    <link href="plugins/rs-plugin/css/extralayers.css" media="screen" rel="stylesheet">
    <link href="plugins/magnific-popup/magnific-popup.css" rel="stylesheet">
    <link href="css/animations.css" rel="stylesheet">
    <link href="plugins/owl-carousel/owl.carousel.css" rel="stylesheet">

    <!-- iDea core CSS file -->
    <link href="css/style.css" rel="stylesheet">
    <link href="css/information_mapping.css" rel="stylesheet">

    <!-- Custom css -->
    <link href="css/custom.css" rel="stylesheet">

    <!-- HTML5 shim and Respond.js for IE8 support of HTML5 elements and media queries -->
    <!--[if lt IE 9]>
    <script src="https://oss.maxcdn.com/html5shiv/3.7.2/html5shiv.min.js"></script>
    <script src="https://oss.maxcdn.com/respond/1.4.2/respond.min.js"></script>
    <![endif]-->

    <script src="js/modernizr.js"></script>

</head>

<!-- body classes:
        "boxed": boxed layout mode e.g. <body class="boxed">
        "pattern-1 ... pattern-9": background patterns for boxed layout mode e.g. <body class="boxed pattern-1">
-->
<body class="front no-trans">
<div class="scrollToTop"><i class="icon-up-open-big"></i></div>

<div class="page-wrapper" id="top_of_page">


    <!-- header start classes:
        fixed: fixed navigation mode (sticky menu) e.g. <header class="header fixed clearfix">
         dark: dark header version e.g. <header class="header dark clearfix">
    ================ -->
    <header class="header fixed clearfix">
        <div class="container">
            <div class="row">
                <div class="col-md-3">

                    <div class="header-left clearfix">
                        <div class="logo">
                            <a href=""><img id="logo" src="images/logo_information_mapping.jpg" alt="information mapping"></a>
                        </div>
                    </div>

                </div>
                <div class="col-md-9">

                    <!-- header-right start -->
                    <!-- ================ -->
                    <div class="header-right clearfix">

                        <!-- main-navigation start -->
                        <!-- ================ -->
                        <div id="header" class="main-navigation animated">

                            <!-- navbar start -->
                            <nav class="navbar navbar-default" role="navigation">
                                <div class="container-fluid">
                                    <div class="navbar-header">
                                        <button type="button" class="navbar-toggle" data-toggle="collapse" data-target="#navbar-collapse-1">
                                            <span class="sr-only">Toggle navigation</span>
                                            <span class="icon-bar"></span>
                                            <span class="icon-bar"></span>
                                            <span class="icon-bar"></span>
                                        </button>
                                    </div>
                                    <div class="collapse navbar-collapse scrollspy smooth-scroll" id="navbar-collapse-1">
                                        <ul class="nav navbar-nav navbar-right">
                                            <li class="active"><a href="#top_of_page">Home</a></li>
                                            <li class=""><a href="#section2">Benefits</a></li>
                                            <li class=""><a href="#section3">Sample</a></li>
                                            <li class=""><a href="#section4">Testimonials</a></li>
                                            <li class="highlight"><a href="#buynow">Purchase</a></li>
                                        </ul>
                                    </div>
                                </div>
                            </nav>
                            <!-- navbar end -->

                        </div>
                        <!-- main-navigation end -->

                    </div>
                    <!-- header-right end -->

                </div>
            </div>
        </div>
    </header>
    <!-- header end -->
    <div id="rest">
        <!-- banner start -->
        <!-- ================ -->
        <div class="banner">

            <!-- slideshow start -->
            <!-- ================ -->
            <div class="slideshow">

                <!-- slider revolution start -->
                <!-- ================ -->
                <div class="slider-banner-container">
                    <div class="slider-banner">
                        <ul>
                            <!-- slide 1 start -->
                            <li data-transition="random" data-slotamount="7" data-masterspeed="500" data-saveperformance="on" data-title="Premium HTML5 template">

                                <!-- main image -->
                                <img src="images/slider-1-slide-1.jpg"  alt="slidebg1" data-bgposition="center top" data-bgfit="cover" data-bgrepeat="no-repeat">

                                <!-- LAYER NR. 1 -->
                                <div class="tp-caption default_bg large sfr tp-resizeme"
                                     data-x="0"
                                     data-y="70"
                                     data-speed="600"
                                     data-start="1200">Information Mapping<sup>&reg;</sup> Essentials&trade;
                                </div>

                                <!-- LAYER NR. 2 -->
                                <div class="tp-caption dark_gray_bg sfl medium tp-resizeme"
                                     data-x="0"
                                     data-y="170"
                                     data-speed="600"
                                     data-start="1600"><i class="icon-check"></i>
                                </div>

                                <!-- LAYER NR. 3 -->
                                <div class="tp-caption light_gray_bg sfb medium tp-resizeme"
                                     data-x="50"
                                     data-y="170"
                                     data-speed="600"
                                     data-start="1600">Learn to write more clearly, effectively and productively
                                </div>

                                <!-- LAYER NR. 4 -->
                                <div class="tp-caption dark_gray_bg sfl medium tp-resizeme"
                                     data-x="0"
                                     data-y="220"
                                     data-speed="600"
                                     data-start="1800"><i class="icon-check"></i>
                                </div>

                                <!-- LAYER NR. 5 -->
                                <div class="tp-caption light_gray_bg sfb medium tp-resizeme"
                                     data-x="50"
                                     data-y="220"
                                     data-speed="600"
                                     data-start="1800">Benefit from the expert guidance of a Master Instructor
                                </div>

                                <!-- LAYER NR. 6 -->
                                <div class="tp-caption dark_gray_bg sfl medium tp-resizeme"
                                     data-x="0"
                                     data-y="270"
                                     data-speed="600"
                                     data-start="2000"><i class="icon-check"></i>
                                </div>

                                <!-- LAYER NR. 7 -->
                                <div class="tp-caption light_gray_bg sfb medium tp-resizeme"
                                     data-x="50"
                                     data-y="270"
                                     data-speed="600"
                                     data-start="2000">Get trained at your own desk, at your own pace
                                </div>

                                <!-- LAYER NR. 8 -->
                                <div class="tp-caption dark_gray_bg sfl medium tp-resizeme"
                                     data-x="0"
                                     data-y="320"
                                     data-speed="600"
                                     data-start="2200"><i class="icon-check"></i>
                                </div>

                                <!-- LAYER NR. 9 -->
                                <div class="tp-caption light_gray_bg sfb medium tp-resizeme"
                                     data-x="50"
                                     data-y="370"
                                     data-speed="600"
                                     data-start="2200">Enjoy this flexible and interactive video course
                                </div>

                                <!-- LAYER NR. 8 -->
                                <div class="tp-caption dark_gray_bg sfl medium tp-resizeme"
                                     data-x="0"
                                     data-y="370"
                                     data-speed="600"
                                     data-start="2400"><i class="icon-check"></i>
                                </div>

                                <!-- LAYER NR. 9 -->
                                <div class="tp-caption light_gray_bg sfb medium tp-resizeme"
                                     data-x="50"
                                     data-y="320"
                                     data-speed="600"
                                     data-start="2400">Join thousands of users worldwide
                                </div>

                                <!-- LAYER NR. 10 -->
                                <div class="tp-caption sfb medium tp-resizeme"
                                     data-x="-10"
                                     data-y="420"
                                     data-speed="600"
                                     data-start="2600"><a href="#buynow" class="btn btn-pink contact buy">Buy now <i class="pl-10 fa fa-shopping-cart"></i></a>
                                </div>

                                <!-- LAYER NR. 11 -->
                                <div class="tp-caption sfr tp-resizeme"
                                     data-x="650"
                                     data-y="bottom"
                                     data-speed="600"
                                     data-start="2900"><img src="images/slider_front.png" alt="">
                                </div>

                            </li>
                            <!-- slide 1 end -->

                        </ul>
                        <div class="tp-bannertimer tp-bottom"></div>
                    </div>
                </div>
                <!-- slider revolution end -->

            </div>
            <!-- slideshow end -->

        </div>
        <!-- banner end -->

        <!-- page-top start-->
        <!-- ================ -->
        <div class="page-top" id="section1">
            <div class="container">
                <div class="row">
                    <div class="col-md-10 col-md-offset-1">
                        <div class="call-to-action">
                            <h1 class="title">What you'll <em>learn</em></h1>
                            <div class="separator"></div>
                            <p class="lead text-center">You’ll learn how to create easily read, easily understood <span class="rotate">memos, reports, proposals, product descriptions</span> in less time!</p>
                            <p class="lead text-center">Throughout the course, we will also teach you how to present the Information Mapping® techniques in FS Pro for Microsoft Word.</p>
                            <p class="sample_modal_image"><a data-toggle="modal" data-target="#myModal"><img src="images/before_after.jpg" /></a></p>
                            <a class="btn btn-white more" data-toggle="modal" data-target="#myModal">
                                See a Before & After example <i class="pl-10 fa fa-info"></i>
                            </a>

                            <!-- Modal -->
                            <div class="modal fade" id="myModal" tabindex="-1" role="dialog" aria-labelledby="myModalLabel" aria-hidden="true">
                                <div class="modal-dialog">
                                    <div class="modal-content">
                                        <div class="modal-header">
                                            <button type="button" class="close" data-dismiss="modal"><span aria-hidden="true">&times;</span><span class="sr-only">Close</span></button>
                                            <h4 class="modal-title" id="myModalLabel">What you'll learn during our course!</h4>
                                        </div>
                                        <div class="modal-body">
                                            <img src="images/example.jpg" class="img-responsive"/>
                                        </div>
                                        <div class="modal-footer">
                                            <button type="button" class="btn btn-sm btn-default" data-dismiss="modal"><i class="icon-check"></i> Ok</button>
                                        </div>
                                    </div>
                                </div>
                            </div>
                            or
                            <a href="#buynow" class="btn btn-pink contact buy">Buy now <i class="pl-10 fa fa-shopping-cart"></i></a>
                        </div>
                    </div>
                </div>
            </div>
        </div>
        <!-- page-top end -->


        <!-- main-container start -->
        <!-- ================ -->
        <section class="main-container gray-bg" id="section2">
            <!-- page-top start-->
            <!-- ================ -->
            <div class="page-top object-non-visible" data-animation-effect="fadeInUpSmall" data-effect-delay="100">
                <div class="container">
                    <div class="row">
                        <div class="col-md-12">
                            <h1 class="text-center title">How you will <em>benefit</em></h1>
                            <div class="separator"></div>
                            <p class="lead text-center">After this course, you and your organization will:</p>

                            <div class="row grid-space-10">
                                <div class="col-sm-4">
                                    <div class="box-style-2">
                                        <div class="icon-container default-bg">
                                            <i class="glyphicon glyphicon-time"></i>
                                        </div>
                                        <div class="body">
                                            <h2>Increase productivity</h2>
                                            <p>You’ll write more quickly and easily. Independent research shows that you can now double your productivity and effectiveness.</p>
                                        </div>
                                    </div>
                                </div>
                                <div class="col-sm-4">
                                    <div class="box-style-2">
                                        <div class="icon-container default-bg">
                                            <i class="glyphicon glyphicon-thumbs-up"></i>
                                        </div>
                                        <div class="body">
                                            <h2>Follow best practices</h2>
                                            <p>Thousands of people and organizations worldwide successfully use the Information Mapping® Method. You’ll be one of them!</p>
                                        </div>
                                    </div>
                                </div>
                                <div class="col-sm-4">
                                    <div class="box-style-2">
                                        <div class="icon-container default-bg">
                                            <i class="glyphicon glyphicon-exclamation-sign"></i>
                                        </div>
                                        <div class="body">
                                            <h2>Reduce errors</h2>
                                            <p>Reduce errors caused by miscommunication. Did you know that errors are reduced by 54% when using this Method?</p>
                                        </div>
                                    </div>
                                </div>
                            </div>
                            <div class="row grid-space-10">
                                <div class="col-sm-4">
                                    <div class="box-style-2">
                                        <div class="icon-container default-bg">
                                            <i class="glyphicon glyphicon-check"></i>
                                        </div>
                                        <div class="body">
                                            <h2>Upgrade quality</h2>
                                            <p>You and your customers will be amazed by the quality of your written documentation, both offline and online!</p>
                                        </div>
                                    </div>
                                </div>
                                <div class="col-sm-4">
                                    <div class="box-style-2">
                                        <div class="icon-container default-bg">
                                            <i class="glyphicon glyphicon-book"></i>
                                        </div>
                                        <div class="body">
                                            <h2>Comply with regulations</h2>
                                            <p>Auditors and regulators appreciate the clarity and accuracy of Mapped documentation.</p>
                                        </div>
                                    </div>
                                </div>
                                <div class="col-sm-4">
                                    <div class="box-style-2">
                                        <div class="icon-container default-bg">
                                            <i class="glyphicon glyphicon-pencil"></i>
                                        </div>
                                        <div class="body">
                                            <h2>Standardize your output</h2>
                                            <p>Information Mapping is a corporate content standard at organizations around the world. It’s ideal for use by writing teams.</p>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
            <!-- page-top end -->

        </section>
        <!-- main-container end -->

        <!-- section start -->
        <!-- ================ -->
        <div class="section clearfix" id="section3">
            <div class="container">
                <div class="row">
                    <div class="col-md-12">
                        <h1 class="text-center">Free <em>sample</em></h1>
                        <div class="separator"></div>
                        <p class="lead text-center">The ‘Information Mapping® Essentials’ Video Course contains a mixture of...</p>
                    </div>
                    <div class="col-md-7">
                        <!-- accordion start -->
                        <div class="panel-group" id="accordion">
                            <div class="panel panel-default">
                                <div class="panel-heading">
                                    <h4 class="panel-title">
                                        <a data-toggle="collapse" data-parent="#accordion" href="#collapseOne">
                                            <i class="fa fa-video-camera"></i>Video Modules
                                        </a>
                                    </h4>
                                </div>
                                <div id="collapseOne" class="panel-collapse collapse in">
                                    <div class="panel-body">
                                        <p>A highly experienced Information Mapping® Master Instructor will teach you all the secrets of this great Method.</p>
                                        <ul>
                                            <li>Introduction to the Information Mapping® Method</li>
                                            <li>Research-based Principles</li>
                                            <li>Blocks and Maps <?php if(!isset($_GET['s'])||($_GET['s']!='trial')){ ?><a class="" href="" data-toggle="modal" data-target="#myModalSample">(free sample)</a><?php } else { ?><a class="" href="free-sample.html">(Preview this chapter in the free sample)</a><?php } ?></li>
                                            <li>Introduction to Information Types</li>
                                            <li>Presenting Procedure (Part 1 + Part 2)</li>
                                            <li>Presenting Process</li>
                                            <li>Presenting Structure</li>
                                            <li>Presenting Concept</li>
                                            <li>Presenting Principle</li>
                                            <li>Presenting Fact</li>
                                            <li>Managing large documents</li>
                                            <li>Document development process (Part 1 + Part 2)</li>
                                            <li>Wrap-up exercise</li>
                                        </ul>
                                    </div>
                                </div>
                            </div>
                            <div class="panel panel-default">
                                <div class="panel-heading">
                                    <h4 class="panel-title">
                                        <a data-toggle="collapse" data-parent="#accordion" href="#collapseTwo" class="collapsed">
                                            <i class="fa fa-pencil"></i>Downloadable Exercises
                                        </a>
                                    </h4>
                                </div>
                                <div id="collapseTwo" class="panel-collapse collapse">
                                    <div class="panel-body">
                                        <p>The course features many hands-on exercises to ensure that you’ll quickly master your new skill set.</p>
                                    </div>
                                </div>
                            </div>
                            <div class="panel panel-default">
                                <div class="panel-heading">
                                    <h4 class="panel-title">
                                        <a data-toggle="collapse" data-parent="#accordion" href="#collapseThree" class="collapsed">
                                            <i class="fa fa-thumbs-o-up"></i>Sample Answers
                                        </a>
                                    </h4>
                                </div>
                                <div id="collapseThree" class="panel-collapse collapse">
                                    <div class="panel-body">
                                        <p>You’ll learn by comparing the results of your exercises with the sample answers of the Instructor.</p>
                                    </div>
                                </div>
                            </div>
                        </div>
                        <!-- accordion end -->
                    </div>
                    <div class="col-md-5 pull-right text-center sample_modal_image">
                        <?php if(!isset($_GET['s'])||($_GET['s']!='trial')){ ?>
                        <a class="" data-toggle="modal" data-target="#myModalSample"><img src="images/sample_video.png" class="img-responsive"/></a>
                        <a class="btn btn-white more" data-toggle="modal" data-target="#myModalSample">Get access to a free sample<i class="pl-10 fa fa-info"></i>
                            <?php } else { ?>
                            <a class="" href="free-sample.html"><img src="images/sample_video.png" class="img-responsive"/></a>
                            <a class="btn btn-white more"  href="free-sample.html">Get access to a free sample<i class="pl-10 fa fa-info"></i>
                                <?php }  ?>
                            </a>
                    </div>

                    <!-- Modal -->
                    <div class="modal fade" id="myModalSample" tabindex="-1" role="dialog" aria-labelledby="myModalLabel" aria-hidden="true">
                        <div class="modal-dialog">
                            <div class="modal-content">
                                <div class="modal-header">
                                    <button type="button" class="close" data-dismiss="modal"><span aria-hidden="true">&times;</span><span class="sr-only">Close</span></button>
                                    <h4 class="modal-title" id="myModalLabel">Get free access to Chapter 3: Blocks and Maps</h4>
                                </div>
                                <div class="modal-body">
                                    <p>Please enter your email in below and we'll send you a link to our download page.</p>
                                    <form role="form" action="http://www2.informationmapping.com/l/8622/2015-06-10/nhsxy" method="post">
                                        <div class="form-group">
                                            <label for="email">Email address</label>
                                            <input type="email" class="form-control" id="email" name="email" placeholder="Enter email">
                                        </div>

                                        <button type="submit" class="btn btn-default">Send me the link</button>
                                    </form>
                                </div>
                            </div>
                        </div>
                    </div>


                </div>
            </div>
        </div>
        <!-- section end -->

        <!-- section start -->
        <!-- ================ -->
        <div class="section parallax parallax-bg-3" id="section4">
            <div class="container">
                <div class="call-to-action">
                    <div class="row">
                        <div class="col-md-8">
                            <h1 class="title text-center">For a limited time! <em>Just $595</em></h1>
                            <p class="text-center">Save $100 by when you buy <em>today!</em></p>
                        </div>
                        <div class="col-md-4">
                            <div class="text-center">
                                <a href="#buynow" class="btn btn-default btn-lg">Buy now <i class="pl-10 fa fa-shopping-cart"></i></a>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
        <!-- section end -->

        <!-- section start -->
        <!-- ================ -->
        <div class="section clearfix">
            <div class="container">
                <div class="row">
                    <div class="col-md-12">

                        <h1 class="text-center">What people are <em>saying</em> about the course</h1>
                        <div class="separator"></div>
                        <p class="lead text-center">The course has already helped many people worldwide to change the way they write. Check out their reviews!</p>
                    </div>
                </div>
                <div class="row">
                    <div class="col-md-8 col-md-offset-2">
                        <div class="owl-carousel content-slider-with-controls-bottom">
                            <div class="testimonial clearfix ">
                                <div class="testimonial-image">
                                    <img src="images/erwin.jpg" alt="Erwin Chopee" title="Erwin Chopee, anager HSE Technical Services, Hydro One" class="img-circle">
                                </div>
                                <div class="testimonial-body">
                                    <p>Writing with this methodology causes you to think about communications in a structured manner. Productivity is then gained by clearly communicating important issues to the audience with very little clarification required.</p>
                                    <div class="testimonial-info-1">Erwin Chopee</div>
                                    <div class="testimonial-info-2">Manager HSE Technical Services, Hydro One</div>
                                </div>
                            </div>

                            <div class="testimonial clearfix">
                                <div class="testimonial-image">
                                    <img src="images/giselle.jpg" alt="Giselle Mazurat" title="Giselle Mazurat, Technical Writer, Hewlett-Packard" class="img-circle">
                                </div>
                                <div class="testimonial-body">
                                    <p>I love Information Mapping. I don't know how writers can develop technical manuals without it. I'm glad I learned it early in my career.</p>
                                    <div class="testimonial-info-1">Giselle Mazurat</div>
                                    <div class="testimonial-info-2">Technical Writer, Hewlett-Packard</div>
                                </div>
                            </div>

                            <div class="testimonial clearfix">
                                <div class="testimonial-image">
                                    <img src="images/tammy.jpg" alt="Tammy Popper" title="Tammy Popper, HR Specialist, Boehringer Ingelheim" class="img-circle">
                                </div>
                                <div class="testimonial-body">
                                    <p>At first I wasn’t sure how this would apply to my work environment. But after finishing the course I was surprised and excited to start using Information Mapping!  I believe this will improve my productivity and bring a better understanding of procedures, practices, and many other activities on a daily basis.  I could have used this class years ago! Thank you guys, you did a fantastic job!</p>
                                    <div class="testimonial-info-1">Tammy Popper</div>
                                    <div class="testimonial-info-2">HR Specialist, Boehringer Ingelheim</div>
                                </div>
                            </div>

                            <div class="testimonial clearfix">
                                <div class="testimonial-image">
                                    <img src="images/joseph.jpg" alt="Joseph Guevara" title="Joseph Guevara, Member of Technical Staff at Alcatel-Lucent" class="img-circle">
                                </div>
                                <div class="testimonial-body">
                                    <p>This course was very beneficial and pertinent to my job assignment. In addition, I will be able to apply the same principles in my personal life.</p>
                                    <div class="testimonial-info-1">Joseph Guevara</div>
                                    <div class="testimonial-info-2">Member of Technical Staff at Alcatel-Lucent</div>
                                </div>
                            </div>

                            <div class="testimonial clearfix">
                                <div class="testimonial-image">
                                    <img src="images/micheal.jpg" alt="Micheal Price" title="Micheal Price, Store Operations at Charming Charlie" class="img-circle">
                                </div>
                                <div class="testimonial-body">
                                    <p>I wish I had taken this course 5 years ago. I will personally benefit from this course, as I have never been able to use a methodology that allowed me to organize my thoughts so clearly. My communication will become stronger because now I am writing with my reader in mind. Information Mapping has changed the way that I think about communication.</p>
                                    <div class="testimonial-info-1">Micheal Price</div>
                                    <div class="testimonial-info-2">Store Operations at Charming Charlie</div>
                                </div>
                            </div>
                        </div>
                    </div><!-- col-md-8-->
                </div>
            </div>
        </div>
        <!-- section end -->

        <!-- section start -->
        <!-- ================ -->
        <div class="section clearfix gray-bg">
            <div class="container">
                <div class="row">
                    <div class="col-md-12">
                        <h1 class="text-center">About the <em>instructor</em></h1>
                        <div class="separator"></div>
                    </div>
                    <div class="col-md-10 col-md-offset-1">
                        <div class="testimonial clearfix">
                            <div class="testimonial-image">
                                <img src="images/karin.jpg" alt="Karen Krogh" title="Karen Krogh, Information Mapping Master Instructor" class="img-circle">
                            </div>
                            <div class="testimonial-body">
                                <p>Information Mapping Master Instructor <em>Karen Krogh</em> is a language specialist and translator who has taught classes in the Information Mapping® Method at organizations all over the world.</p>
                                <p>Karen’s work with the Information Mapping® Method includes implementation projects for financial organizations, airlines, shipping companies and government agencies.</p>
                                <p>“I’m delighted by how well this video course conveys the learning experience that takes place in an Information Mapping classroom.”</p>
                                <img class="pull-right" src="images/karen_signature.png"/>

                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
        <!-- section end -->
    </div>

    <!-- section start -->
    <!-- ================ -->
    <div class="section clearfix" id="buynow">
        <div class="container">
            <div class="row">
                <div class="col-md-12">
                    <h1 class="text-center"><em>Purchase</em> the course</h1>
                    <div class="separator"></div>
                </div>

                <div class="col-md-12">
                    <!-- pricing tables start -->
                    <div id="checkout">
                        <div class="pricing-tables white object-non-visible" data-animation-effect="fadeInUpSmall">
                            <div class="row grid-space-0">
                                <div class="col-sm-3 plan noplan hidden-xs">
                                    <ul>
                                        <li>Video Course <i data-toggle="tooltip" data-placement="right" title="6 months full access to the 15 video modules and the supporting materials" class="fa fa-info-circle"></i></li>
                                        <li>Certification <i data-toggle="tooltip" data-placement="right" title="Access to the Information Mapping® Professional™ (IMP) Certification Exam" class="fa fa-info-circle"></i></li>
                                        <li>Software License <i data-toggle="tooltip" data-placement="right" title="1-year license of FS Pro 2013, software that makes it quick and easy to create clear and structured documents in MS Word." class="fa fa-info-circle"></i></li>
                                        <li>&nbsp;</li>
                                    </ul>
                                </div>

                                <!-- pricing table start -->
                                <div class="col-sm-3 plan standard_plan">
                                    <div class="header">
                                        <h3>Information Mapping<sup>&reg;</sup> Essentials&trade;</h3>
                                    </div>
                                    <ul>
                                        <li class="visible-xs">6 months full access to the 15 video modules and the supporting materials</li>
                                        <li class="visible-xs">Access to the Information Mapping® Professional™ (IMP) Certification Exam</li>

                                        <li class="hidden-xs"><i class="fa fa-check"></i></li>
                                        <li class="hidden-xs"><i class="fa fa-check"></i></li>
                                        <li class="hidden-xs"><i class="fa fa-close"></i></li>
                                        <li class="buynow_price">Introductory price<br/><span class="old_price">$595</span> <span class="new_price">$495</span></li>
                                        <li><button class="btn btn-pink buy buy-standard" autocomplete="off" style=" display: none"><i class="fa fa-shopping-cart pr-10"></i>Buy now</button></li>

                                    </ul>
                                </div>
                                <!-- pricing table end -->

                                <!-- pricing table start -->
                                <div class="col-sm-3 plan premium_plan">
                                    <div class="header">
                                        <h3>Information Mapping<sup>&reg;</sup> Essentials&trade; Plus</h3>
                                        <p class="most_popular"><img src="images/most_popular.png" class="img-responsive"/></p>
                                    </div>
                                    <ul>
                                        <li class="visible-xs">6 months full access to the 15 video modules and the supporting materials</li>
                                        <li class="visible-xs">Access to the Information Mapping® Professional™ (IMP) Certification Exam</li>
                                        <li class="visible-xs">1-year license of FS Pro 2013, software that makes it quick and easy to create clear and structured documents in MS Word.</li>

                                        <li class="hidden-xs"><i class="fa fa-check"></i></li>
                                        <li class="hidden-xs"><i class="fa fa-check"></i></li>
                                        <li class="hidden-xs"><i class="fa fa-check"></i></li>
                                        <li class="buynow_price">Introductory price<br/><span class="old_price">$695</span> <span class="new_price">$595</span></li>
                                        <li><a class="btn btn-pink buy buy-premium"><i class="fa fa-shopping-cart pr-10"></i>Buy now</a></li>
                                    </ul>
                                </div>
                                <!-- pricing table end -->
                                
                                <div class="col-sm-3">
                                    <div class="team">
                                        <div class="header">
                                            <h3>Working in a team?</h3>
                                        </div>
                                        <ul>
                                            <li class="buynow_price">20-pack<br/><span class="new_price">$425 pp</span><br/><a class="btn btn-pink buy buy-premium"><i class="fa fa-shopping-cart pr-10"></i>Buy now</a></li>
                                            <li class="buynow_price">50-pack<br/><span class="new_price">$395 pp</span><br/><a class="btn btn-pink buy buy-premium"><i class="fa fa-shopping-cart pr-10"></i>Buy now</a></li>
                                        </ul>
                                    </div>
                                    
                                </div>

                            </div>
                            <div class="row grid-space-0">
                                <div class="col-xs-2 col-sm-offset-1 col-sm-2 blue_badge"><img class="img-responsive" src="images/blue_badge.png" /></div>
                                <div class="col-xs-10 col-sm-9 custom_plan">
                                    <div class="header">
                                        <h3>Build your own <strong>custom</strong> course</h3>
                                        <p class="buynow_price"> from <span class="new_price">$50</span></p>
                                    </div>
                                    <div class="row">
                                        <div class="col-sm-offset-8 col-sm-4 text-center">
                                            <p><a class="btn btn-pink  buy buy-modular"><i class="fa fa-shopping-cart pr-10"></i>Buy now</a></p>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>
                        <!-- pricing tables end -->
                    </div>
                </div>

            </div>
        </div>
    </div>
    <!-- section end -->

    <!-- section start -->
    <!-- ================ -->
    <div class="section clearfix" id="social">
        <div class="container">
            <div class="row">
                <div class="col-md-12">
                    <h2 class="text-center"><em>Share</em> with friends</h2>
                    <div class="separator"></div>
                    <ul class="text-center social-links colored circle">
                        <li class="facebook"><a target="_blank" href="https://www.facebook.com/sharer/sharer.php?u=https://www.informationmapping.com/"><i class="fa fa-facebook"></i></a></li>
                        <li class="twitter"><a target="_blank" href="https://twitter.com/home?status=https://www.informationmapping.com/"><i class="fa fa-twitter"></i></a></li>
                        <li class="googleplus"><a target="_blank" href="https://plus.google.com/share?url=https://www.informationmapping.com/"><i class="fa fa-google-plus"></i></a></li>
                        <li class="linkedin"><a target="_blank" href="https://www.linkedin.com/shareArticle?mini=true&url=https://www.informationmapping.com/&title=Information%20Mapping%20Essentiels&summary=Information%20Mapping%20Essentiels%20will%20learn%20you%20how%20to%20create%20easily%20read,%20easily%20understood%20proposals%0Aproduct%20descriptions%0A%20in%20less%20time!&source="><i class="fa fa-linkedin"></i></a></li>
                    </ul>
                </div>
            </div>
        </div>
    </div>
    <!-- section end -->

    <!-- section start -->
    <!-- ================ Hide questions section to use olark instead
    <div class="section clearfix">
        <div class="container">
            <div class="row">
                <div class="col-md-12">
                    <h2 class="text-center">Questions?</h2>
                    <div class="separator"></div>
                </div>
                <div class="col-md-8 col-md-offset-2">

                    <div class="alert alert-success hidden" id="MessageSent">
                        We have received your message, we will contact you very soon.
                    </div>
                    <div class="alert alert-danger hidden" id="MessageNotSent">
                        Oops! Something went wrong please refresh the page and try again.
                    </div>
                    <div class="contact-form">
                        <form id="contact-form" role="form">
                            <div class="form-group has-feedback">
                                <label for="name">Your name*</label>
                                <input type="text" class="form-control" id="name" name="name" placeholder="">
                                <i class="fa fa-user form-control-feedback"></i>
                            </div>
                            <div class="form-group has-feedback">
                                <label for="email">Your email*</label>
                                <input type="email" class="form-control" id="email" name="email" placeholder="">
                                <i class="fa fa-envelope form-control-feedback"></i>
                            </div>
                            <div class="form-group has-feedback">
                                <label for="message">Message*</label>
                                <textarea class="form-control" rows="6" id="message" name="message" placeholder=""></textarea>
                                <i class="fa fa-pencil form-control-feedback"></i>
                            </div>
                            <input type="submit" value="Submit" class="submit-button btn btn-default">
                        </form>
                    </div>
                </div>

            </div>
        </div>
    </div>
    -->
</div>
<!-- section end -->

</div>
<!-- page-wrapper end -->

<!-- JavaScript files placed at the end of the document so the pages load faster
================================================== -->
<!-- Jquery and Bootstap core js files -->
<script type="text/javascript" src="plugins/jquery.min.js"></script>
<script type="text/javascript" src="bootstrap/js/bootstrap.min.js"></script>

<!-- Modernizr javascript -->
<script type="text/javascript" src="plugins/modernizr.js"></script>

<!-- jQuery REVOLUTION Slider  -->
<script type="text/javascript" src="plugins/rs-plugin/js/jquery.themepunch.tools.min.js"></script>
<script type="text/javascript" src="plugins/rs-plugin/js/jquery.themepunch.revolution.min.js"></script>

<!-- Isotope javascript -->
<script type="text/javascript" src="plugins/isotope/isotope.pkgd.min.js"></script>

<!-- Owl carousel javascript -->
<script type="text/javascript" src="plugins/owl-carousel/owl.carousel.js"></script>

<!-- Magnific Popup javascript -->
<script type="text/javascript" src="plugins/magnific-popup/jquery.magnific-popup.min.js"></script>

<!-- Appear javascript -->
<script type="text/javascript" src="plugins/jquery.appear.js"></script>

<!-- Count To javascript -->
<script type="text/javascript" src="plugins/jquery.countTo.js"></script>

<!-- Parallax javascript -->
<script src="plugins/jquery.parallax-1.1.3.js"></script>

<!-- Contact form -->
<script src="plugins/jquery.validate.js"></script>

<!-- Word rotator of Plugins -->
<script type="text/javascript" src="js/jquery.simple-text-rotator.min.js"></script>

<!-- Initialization of Plugins -->
<script type="text/javascript" src="js/template.js"></script>

<!-- Custom Scripts -->
<script type="text/javascript" src="js/pluggablecheckout.js"></script>
<script type="text/javascript" src="js/custom.js"></script>
<script>
    jQuery(document).ready(function(){
        AngularCheckout.init('https://staging.informationmapping.com/magento/', function(){
            jQuery('.buy').show();
        },'proxy/index.php');

        jQuery('.buy-premium').click(function(e){
            e.preventDefault();
            console.log('.buy-premium clicked');
            jQuery('.buy').prop('disabled', true);
            jQuery('#social').hide();
            jQuery('#header').hide();
            jQuery('#rest').hide();
            AngularCheckout.emptyCart();
            AngularCheckout.addProduct('roy-im-fspro2013-subscription-1y ', 1);
            AngularCheckout.renderInside('#checkout');
        });

        jQuery('.buy-standard').click(function(e){
            e.preventDefault();
            console.log('.buy-standard clicked');
            jQuery('.buy').prop('disabled', true);
            jQuery('#social').hide();
            jQuery('#header').hide();
            jQuery('#rest').hide();
            AngularCheckout.emptyCart();
            AngularCheckout.addProduct('roy-im-fspro2013-subscription-1y ', 1);
            AngularCheckout.renderInside('#checkout');
        });
        jQuery('.buy-modular').click(function(e){
            e.preventDefault();
            console.log('.buy-modular clicked');
            jQuery('.buy').prop('disabled', true);
            jQuery('#social').hide();
            jQuery('#header').hide();
            jQuery('#rest').hide();
            AngularCheckout.emptyCart();
            AngularCheckout.addProduct('roy-im-fspro2013-subscription-1y ', 1);
            AngularCheckout.renderInside('#checkout');
        });
    });
</script>

<!-- begin olark code -->
<script data-cfasync="false" type='text/javascript'>/*<![CDATA[*/window.olark||(function(c){var f=window,d=document,l=f.location.protocol=="https:"?"https:":"http:",z=c.name,r="load";var nt=function(){
        f[z]=function(){
            (a.s=a.s||[]).push(arguments)};var a=f[z]._={
        },q=c.methods.length;while(q--){(function(n){f[z][n]=function(){
            f[z]("call",n,arguments)}})(c.methods[q])}a.l=c.loader;a.i=nt;a.p={
            0:+new Date};a.P=function(u){
            a.p[u]=new Date-a.p[0]};function s(){
            a.P(r);f[z](r)}f.addEventListener?f.addEventListener(r,s,false):f.attachEvent("on"+r,s);var ld=function(){function p(hd){
            hd="head";return["<",hd,"></",hd,"><",i,' onl' + 'oad="var d=',g,";d.getElementsByTagName('head')[0].",j,"(d.",h,"('script')).",k,"='",l,"//",a.l,"'",'"',"></",i,">"].join("")}var i="body",m=d[i];if(!m){
            return setTimeout(ld,100)}a.P(1);var j="appendChild",h="createElement",k="src",n=d[h]("div"),v=n[j](d[h](z)),b=d[h]("iframe"),g="document",e="domain",o;n.style.display="none";m.insertBefore(n,m.firstChild).id=z;b.frameBorder="0";b.id=z+"-loader";if(/MSIE[ ]+6/.test(navigator.userAgent)){
            b.src="javascript:false"}b.allowTransparency="true";v[j](b);try{
            b.contentWindow[g].open()}catch(w){
            c[e]=d[e];o="javascript:var d="+g+".open();d.domain='"+d.domain+"';";b[k]=o+"void(0);"}try{
            var t=b.contentWindow[g];t.write(p());t.close()}catch(x){
            b[k]=o+'d.write("'+p().replace(/"/g,String.fromCharCode(92)+'"')+'");d.close();'}a.P(2)};ld()};nt()})({
        loader: "static.olark.com/jsclient/loader0.js",name:"olark",methods:["configure","extend","declare","identify"]});
    /* custom configuration goes here (www.olark.com/documentation) */
    olark.identify('3504-626-10-9459');/*]]>*/</script><noscript><a href="https://www.olark.com/site/3504-626-10-9459/contact" title="Contact us" target="_blank">Questions? Feedback?</a> powered by <a href="http://www.olark.com?welcome" title="Olark live chat software">Olark live chat software</a></noscript>
<!-- end olark code -->

</body>
</html>
