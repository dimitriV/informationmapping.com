var AngularCheckout = {

	_html: null,
	_magentoHomeUrl: null,
	_target: null,
	_scriptsToLoad: null,
	_commandQueue: [],
	_readyForCommandQueue: false,
	_queueIsBusy: false,
	_useProxy: true,
	_proxyUrl: null,
	_readyFunction: null,
	_bundleSku: null,

	logError: function(msg){
		alert('AngularCheckout Error'+"\n"+msg);
		console.log('AngularCheckout Error: '+msg);
	},

	_loadNextScript: function(){

		if(AngularCheckout._scriptsToLoad && AngularCheckout._scriptsToLoad.length > 0){
		    var scriptUrl = AngularCheckout._scriptsToLoad.shift();

		    jQuery.getScript(scriptUrl, function(){
				console.log('Loaded: '+scriptUrl);

				AngularCheckout._loadNextScript();
		    });

	    	/*
		    var script = document.createElement( 'script' );
			script.type = 'text/javascript';
			script.src = scriptUrl;
			head.append( script );

			console.log('Loading: '+scriptUrl);
			*/
		}else{
			
			AngularCheckout._runInitSuccess();

			AngularCheckout._readyForCommandQueue = true;
			
			AngularCheckout._runNextCommand();
			
		}
	},
	
	_runInitSuccess: function(){
		AngularCheckout._readyFunction();
	},

	_runNextCommand: function(){

		if(AngularCheckout._readyForCommandQueue && !AngularCheckout._queueIsBusy){
			
			if(AngularCheckout._commandQueue && AngularCheckout._commandQueue.length > 0){

				try{
					// Prevent overlapping calls to this functions
					AngularCheckout._queueIsBusy = true;
					
				    var command = AngularCheckout._commandQueue.shift();
	
					var commandName = command.command;

					AngularCheckout['_'+commandName](command.arguments);
					
				}catch(ex){
					AngularCheckout.logError(ex);
					
					
				}finally{
					AngularCheckout._queueIsBusy = false;
				}
				
			    AngularCheckout._runNextCommand();
			}
		}
		
		
		
	},

	init: function(magentoHomeUrl, readyFunction, proxyUrl){

		if(!magentoHomeUrl){
			AngularCheckout.logError('Missing parameter "magentoHomeUrl" in init() function');
		}
		
		if(magentoHomeUrl.substr(-1) == '/') {
			magentoHomeUrl = magentoHomeUrl.substr(0, magentoHomeUrl.length - 1);
	    }
		
		this._magentoHomeUrl = magentoHomeUrl;
	    this._proxyUrl = proxyUrl;
	    this._readyFunction = readyFunction;
	    
	    
	    // Load pluggable config
	    var cssjsPath = 'angularcheckout/pluggable/cssjs';

		this._proxyGet(cssjsPath,
			function(data, textStatus, jqXHR){
				// Success function
			
				var head = jQuery("head");
				
				var scripts = data.script;
				var css = data.css;

				AngularCheckout._scriptsToLoad = scripts;

				// Add CSS to head
				for (var index = 0; index < css.length; index++) {
					var cssUrl = css[index];
					var headlinklast = head.find("link[rel='stylesheet']:last");
					var linkElement = "<link rel='stylesheet' href='"+cssUrl+"' type='text/css' media='screen'>";
					
					if (headlinklast.length){
						headlinklast.after(linkElement);
					}else {
					   head.append(linkElement);
					};
				}

				AngularCheckout._loadNextScript();
				
		},				
		function(jqXHR, textStatus, errorThrown){
			// Error function
				if(textStatus == 'error'){
					textStatus = 'Did you configure CORS correctly for '+AngularCheckout._magentoHomeUrl+'?';
				}
				AngularCheckout.logError(textStatus);
		});
	},


	// Functions run through the command queue
	emptyCart: function(){
		AngularCheckout._queueCommand('emptyCart');
		AngularCheckout._runNextCommand();
	},

	addProduct: function(sku, qty){
		AngularCheckout._queueCommand('addProduct', arguments);
		AngularCheckout._runNextCommand();
	},
	
	setProductBundle: function(sku){
		AngularCheckout._bundleSku = sku;
	},

	renderInside: function(selector){
		AngularCheckout._queueCommand('renderInside', arguments);
		AngularCheckout._runNextCommand();
	},

	// Internal function for queuing
	_queueCommand: function(cmd, args){
		var command = {command: cmd, arguments: args};
		AngularCheckout._commandQueue.push(command);

		console.log('Queued '+cmd);
	},

	_getProxyUrl: function(path){
		return this._proxyUrl+'?path='+encodeURIComponent(path);
	},
	
	_proxyGet: function(path, successFunction, errorFunction){

		var proxyUrl = AngularCheckout._getProxyUrl(path);
		
		if(!errorFunction){
			errorFunction = function(jqXHR, textStatus, errorThrown){
				// Default error function
				AngularCheckout.logError(errorThrown);
				
				// Stop the queue
				AngularCheckout._readyForCommandQueue = false;
			};
		}
		
		jQuery.ajax({
			url: proxyUrl,
			method: 'GET',
			cache:false,
			async: false,
			dataType: "json",
			success: successFunction,
			error: errorFunction
		});
	},

	// Actual command executing function
	_emptyCart: function(){
		console.log('Emptying the cart');

		var url = 'angularcheckout/pluggable/emptycart/';

		if(AngularCheckout._useProxy){
			this._proxyGet(url, function(data, textStatus, jqXHR){
				console.log('Cart is cleared!');
			});
		}else{
		
		jQuery.ajax({
			url: url,
			method: 'POST',
			cache:false,
			async: false,
			xhrFields: {
				withCredentials: true
			},
			success: function(data, textStatus, jqXHR){
				if(data.status == 'ok'){
					console.log('Cart is cleared!');
				}else{
					AngularCheckout.logError('Error during cart clearing');
				}
			}
		});
		
		}
	},
	
	_addProduct: function(args){
		var sku = args[0];
		var qty = args[1];

		if(AngularCheckout._useProxy){
			var url = 'angularcheckout/pluggable/addproduct/?sku='+encodeURIComponent(sku)+'&qty='+encodeURIComponent(qty);

			this._proxyGet(url, function(data, textStatus, jqXHR){
				console.log('Product "' + sku + '" added to cart!');
			});
			
		}else{

			// NOT IMPLEMENTED
			
			/*
			var url = AngularCheckout._magentoHomeUrl + '/angularcheckout/pluggable/addproduct/?sku='+encodeURIComponent(sku)+'&qty='+encodeURIComponent(qty);
			
			jQuery.ajax({
				url: url,
				method: 'GET',
				crossDomain: true,
				async: false,
				success: function(data, textStatus, jqXHR){
					if(data.status == 'ok'){
						console.log('Product is added!');
					}else{
						AngularCheckout.logError('Error during product adding');
					}
				}
			});
			*/

		}
		
	},
	
	_renderDom: function(selector, onCheckoutReadyFunction){
		AngularCheckout._target = selector;

		// Render HTML
		if(selector){
			var node = jQuery(AngularCheckout._target);
			
			node.empty();

			// Make invisible during rendering
			node.hide();

			// Put the HTML in the document
			node.append(AngularCheckout._html);
			
			
			// Set global JS vars used in Angular in the window namespace
			var jsonUrl = AngularCheckout._getProxyUrl('angularcheckout/checkout/json');
			if(AngularCheckout._bundleSku){
				jsonUrl += '?bundle='+AngularCheckout._bundleSku;
			}
			
			var placeOrderUrl = AngularCheckout._getProxyUrl('angularcheckout/checkout/placeorder');
			
		    window.checkoutJsonUrl = jsonUrl;
		    window.placeOrderUrl = placeOrderUrl;

		    
		    
		    
		    // Register the HTML ready event function
		    if(onCheckoutReadyFunction){
		    	jQuery(document).on( "angularcheckout_rendered", onCheckoutReadyFunction);
		    }
		    
			// Tell Angular to start
			jQuery.event.trigger('startAngularCheckout');

			// Show the checkout after processing
			node.show();
		}
	},

	_renderInside: function(args){
		console.log('Rendering inside');
		
		
		// Load pluggable config
	    var cssjsPath = 'angularcheckout/pluggable/html';
	    
	    if(AngularCheckout._bundleSku){
	    	cssjsPath += '?bundle='+AngularCheckout._bundleSku;
	    }

		this._proxyGet(cssjsPath,
			function(data, textStatus, jqXHR){
				// Success function
			
				AngularCheckout._html = data.html;

				AngularCheckout._renderDom(args[0], args[1]);
				
		});
		
		
	}
};