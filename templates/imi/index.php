<?php
/**
 * @version    $Id: index.php 17268 2010-05-25 20:32:21Z a.radtke $
 * @package    Joomla.Site
 * @subpackage  Templates.IMI
 * @copyright  Copyright (C) 20012 - 2015 Open Source Matters, Inc. All rights reserved.
 * @license    GNU General Public License version 2 or later; see LICENSE.txt
 */
defined('_JEXEC') or die;
include_once(JPATH_SITE.'/templates/'.$this->template.'/vars.php');
?>
<?php
JHtml::_('behavior.framework', true);
$color      = $this->params->get('templatecolor');
$logo      = $this->params->get('logo');
$navposition  = $this->params->get('navposition');
$app      = JFactory::getApplication();
$jinput = JFactory::getApplication()->input;
$Itemid = $jinput->getInt('Itemid');
$doc      = JFactory::getDocument();
$templateparams  = $app->getTemplate(true)->params;

$topline='block';
if ($Itemid == 226) { $topline='none'; }

// echo $topline;

// Added jquery library with no conflict
$doc->addScript($this->baseurl.'/media/jui/js/jquery.min.js', 'text/javascript');
$doc->addScript($this->baseurl.'/media/jui/js/bootstrap.min.js', 'text/javascript');
//$doc->addScript($this->baseurl.'/templates/'.$app->getTemplate().'/javascript/jquery.uniform.min.js', 'text/javascript');
?>
<!DOCTYPE html>
<head>
    <meta charset="utf-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <jdoc:include type="head" />
    <!--<script src="https://cdnjs.cloudflare.com/ajax/libs/twitter-bootstrap/3.3.6/js/bootstrap.min.js"></script>-->
    <link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/twitter-bootstrap/3.3.6/css/bootstrap.min.css">
    <link rel="stylesheet" href="<?php echo $this->baseurl ?>/templates/imi/css/merge.css" type="text/css" />
    <link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/font-awesome/4.4.0/css/font-awesome.min.css">
    <link href="https://gitcdn.github.io/bootstrap-toggle/2.2.0/css/bootstrap-toggle.min.css" rel="stylesheet">
    <script src="https://gitcdn.github.io/bootstrap-toggle/2.2.0/js/bootstrap-toggle.min.js"></script>
    <!-- HTML5 shim and Respond.js for IE8 support of HTML5 elements and media queries -->
    <!-- WARNING: Respond.js doesn't work if you view the page via file:// -->
    <!--[if lt IE 9]>
    <script defer src="https://oss.maxcdn.com/html5shiv/3.7.2/html5shiv.min.js"></script>
    <script defer src="https://oss.maxcdn.com/respond/1.4.2/respond.min.js"></script>
    <![endif]-->
    <?php
    if( JRequest::getCmd('option') == 'com_magebridge' ) { ?>
        <?php
        // only keep scripts necessary for language selection box styling.
        foreach($this->_scripts as $scriptName => $script) {
            if(preg_match('/jquery/', $scriptName)) {
                $uniform_script[$scriptName] = $script;
            }
        }
        $this->_scripts =  $uniform_script;
    }
    ?>
    <script src="https://cdnjs.cloudflare.com/ajax/libs/sweetalert/1.1.0/sweetalert.min.js"></script>
    <!-- Facebook Pixel Code -->
    <script>
        !function(f,b,e,v,n,t,s){if(f.fbq)return;n=f.fbq=function(){n.callMethod?
            n.callMethod.apply(n,arguments):n.queue.push(arguments)};if(!f._fbq)f._fbq=n;
            n.push=n;n.loaded=!0;n.version='2.0';n.queue=[];t=b.createElement(e);t.async=!0;
            t.src=v;s=b.getElementsByTagName(e)[0];s.parentNode.insertBefore(t,s)}(window,
            document,'script','//connect.facebook.net/en_US/fbevents.js');

        fbq('init', '252451421764281');
        fbq('track', "PageView");</script>
    <noscript><img height="1" width="1" style="display:none"
                   src="https://www.facebook.com/tr?id=252451421764281&ev=PageView&noscript=1"
        /></noscript>
    <!-- End Facebook Pixel Code -->
</head>
<body>
<?php // echo $Itemid; ?>
<?php if(class_exists('MageBridgeTemplateHelper') && MageBridgeTemplateHelper::isLoaded()) : ?>
    <!-- start after body start -->
    <jdoc:include type="modules" name="mbafterbodystart" />
    <!-- end after body start -->
<?php endif;
$menu = JSite::getMenu();
$lang = JFactory::getLanguage();
$home = ($menu->getActive() == $menu->getDefault($lang->getTag())) ? '-home' : '';
?>
<header class="row">
    <div class="container">
        <div class="top">
            <jdoc:include type="modules" name="top" />
        </div>
        <div class="wrapper-nav col-xs-12">
            <div class="logo">
                <a href="/"> <img src="<?php echo $this->baseurl ?>/<?php echo htmlspecialchars($logo); ?>" alt="<?php echo htmlspecialchars($templateparams->get('sitetitle'));?>" /></a>
            </div>
            <nav class="navbar navbar-default" style="margin-bottom: 10px !important;">
                <div class="navbar-header">
                    <button type="button" class="navbar-toggle collapsed" data-toggle="collapse" data-target="#imi-collapse">
                        <span class="sr-only">Toggle navigation</span>
                        <span class="icon-bar"></span>
                        <span class="icon-bar"></span>
                        <span class="icon-bar"></span>
                    </button>
                </div>
                <div class="collapse navbar-collapse" id="imi-collapse">
                    <jdoc:include type="modules" name="main-menu" />
                </div>
            </nav>
        </div>
    </div>
</header>

<div class="container">
    <div class="row" id="topline" style="display: <?php echo $topline; ?>">
        <div class="bread">
            <div class="col-xs-12 col-sm-8">
                <jdoc:include type="modules" name="popper" />
                <?php if ($this->countModules('announcement')): ?>
                    <jdoc:include type="modules" name="announcement" />
                <?php else: ?>
                <jdoc:include type="modules" name="breadcrumb"  />
            </div>
            <div class="breadcrumb-magento-menu-wrapper col-xs-12 col-sm-4 login">
                <?php if ($this->countModules('magento-topmenu')): ?>
                    <div class="magento-topmenu">
                        <jdoc:include type="modules" name="magento-topmenu" style="xhtml" />
                    </div>
                <?php endif; ?>
            </div>
            <?php if ($this->countModules('banner')): ?>
                <div id="banner-container">
                    <jdoc:include type="modules" name="banner" />
                </div>
                <div class="clear-fix-top-gap"></div>
            <?php endif; ?>
        </div>
    </div>
    <jdoc:include type="message" />
</div>

<!-- Position the DJ Image slider -->
<?php if ($this->countModules('djslider')): ?>
    <div class="djslider-topmenu">
        <?php if ($this->countModules('magento-topmenu')): ?>
            <div class="container" style="z-index: 99999; position: relative;">
                <div class="magento-topmenu">
                    <jdoc:include type="modules" name="magento-topmenu" style="xhtml" />
                </div>
            </div>
        <?php endif; ?>        
        <div style="top: -41px; position: relative;">
            <jdoc:include type="modules" name="djslider" style="XHTML"  />
        </div>
    </div>
<?php endif; ?>
<!-- Position the DJ Image slider -->
<!-- MAIN WRAPPER -->
<?php if ($midPosition1Exist): ?>
    <div class="container-fluid hero">
        <?php if ($this->countModules('mid-position1')): ?>
            <div>
                <jdoc:include type="modules" name="mid-position1" style="XHTML"  />
            </div>
        <?php endif; ?>
    </div>
<?php endif; ?>
<div class="container">
    <!-- LEFT WRAPPER -->
    <?php if ($positionLeftExist || $positionLleftExist ): ?>
        <div class="col-md-3" id="content">
            <!-- LEFT MODULES-->
            <?php if ($this->countModules('position-left')): ?>
                <div class="col-md-12">
                    <?php echo $leftModuleContent;?>
                </div>
            <?php endif; ?>
            <?php if ($this->countModules('position-lleft')): ?>
                <div class="col-md-12">
                    <jdoc:include type="modules" name="position-lleft" style="XHTML"  />
                </div>
            <?php endif; ?>
        </div>
    <?php endif; ?>
    <!--MID WRAPPERS  | MID WRAPPER TOP-->
    <?php if ( !($positionLeftExist || $positionLleftExist || $positionRightExist || $positionRrightExist) ): ?>
    <div class="col-md-12">
        <?php elseif ( ($positionLeftExist || $positionLleftExist) && !($positionRightExist || $positionRrightExist)): ?>
        <div id="check-out" class="col-md-9">
            <?php elseif ( !($positionLeftExist || $positionLleftExist) && ($positionRightExist || $positionRrightExist)): ?>
            <div class="col-md-8">
                <?php else: ?>
                <div class="col-md-6">
                    <?php endif; ?>

                    <jdoc:include type="modules" name="store-banner" style="XHTML"  />
                    <?php if ($midPosition1Exist || $midPosition2Exist || $midPosition3Exist || $midPosition4Exist): ?>
                        <div class="mid-top-wrapper">
                            <?php if ($midPosition2Exist || $midPosition3Exist): ?>
                                <div class="row">
                                    <?php if ($this->countModules('mid-position2')): ?>
                                        <div class="col-md-6 col-xs-12">
                                            <jdoc:include type="modules" name="mid-position2" style="XHTML"  />
                                        </div>
                                    <?php endif; ?>
                                    <?php if ($this->countModules('mid-position3')): ?>
                                        <div class="col-md-6 col-xs-12">
                                            <jdoc:include type="modules" name="mid-position3" style="XHTML"  />
                                        </div>
                                    <?php endif; ?>
                                </div>
                            <?php endif; ?>
                            <?php if ($this->countModules('mid-position4')): ?>
                                <div class="col-xs-12">
                                    <jdoc:include type="modules" name="mid-position4" style="XHTML"  />
                                </div>
                            <?php endif; ?>
                        </div>
                    <?php endif; ?>
                    <div>
                        <jdoc:include type="component" />
                    </div>
                    <!--MID WRAPPERS  | MID WRAPPER BOTTOM-->
                    <?php if ($midPosition5Exist || $midPosition6Exist || $midPosition7Exist || $midPosition8Exist): ?>
                        <div class="mid-bottom-wrapper">
                            <?php if ($this->countModules('mid-position5')): ?>
                                <div class="col-xs-12">
                                    <jdoc:include type="modules" name="mid-position5" style="XHTML"  />
                                </div>
                            <?php endif; ?>
                            <?php if ($midPosition6Exist || $midPosition7Exist): ?>
                                <div class="row">
                                    <?php if ($this->countModules('mid-position6')): ?>
                                        <div class="col-md-6 col-xs-12">
                                            <jdoc:include type="modules" name="mid-position6" style="XHTML"  />
                                        </div>
                                    <?php endif; ?>
                                    <?php if ($this->countModules('mid-position7')): ?>
                                        <div class="col-md-6 col-xs-12">
                                            <jdoc:include type="modules" name="mid-position7" style="XHTML"  />
                                        </div>
                                    <?php endif; ?>
                                </div>
                            <?php endif; ?>
                            <?php if ($this->countModules('mid-position8')): ?>
                                <div class="col-xs-12">
                                    <jdoc:include type="modules" name="mid-position8" style="XHTML"  />
                                </div>
                            <?php endif; ?>
                        </div>
                    <?php endif; ?>
                </div>
                <?php endif; ?>
                <!-- RIGHT WRAPPER -->
                <?php if (!($positionLeftExist || $positionLleftExist) && $positionRightExist || $positionRrightExist ): ?>
                <div class="col-md-4">
                    <?php elseif ( $positionLeftExist || $positionLleftExist || $positionRightExist || $positionRrightExist): ?>
                    <div class="col-md-3">
                        <?php endif; ?>
                        <?php if ($this->countModules('position-right')): ?>
                            <div class="col-md-12">
                                <jdoc:include type="modules" name="position-right" style="XHTML"  />
                            </div>
                        <?php endif; ?>
                        <?php if ($this->countModules('position-rright')): ?>
                            <div class="col-md-12">
                                <jdoc:include type="modules" name="position-rright" style="XHTML"  />
                            </div>
                        <?php endif; ?>
                    </div>
                </div>
            </div>
        </div>
        <?php if ($this->countModules('payment')): ?>
            <div class="row bottom-payment">
                <div class="col-xs-12">
                    <jdoc:include type="modules" name="payment" style="XHTML"  />
                </div>
            </div>
        <?php endif; ?>
        <?php if ($this->countModules('footer')): ?>
            <footer class="row">
                <jdoc:include type="modules" name="footer" style="xhtml" />
            </footer>
        <?php endif; ?>
        <?php if ($this->countModules('bottom')): ?>
            <div class="row bottom-footer">
                <div class="col-xs-12">
                    <jdoc:include type="modules" name="bottom" style="XHTML"  />
                </div>
            </div>
        <?php endif; ?>
        <?php if(class_exists('MageBridgeTemplateHelper') && MageBridgeTemplateHelper::isLoaded()) : ?>
            <jdoc:include type="modules" name="mbbeforebodyend" />
        <?php endif; ?>
        <jdoc:include type="modules" name="debug" />
        <script>
            jQuery("#myModal").on("click",'.close', function() { jQuery("#myModal iframe").attr("src", jQuery("#myModal iframe").attr("src")); });
            jQuery("#myModal").on("click",'.btn', function() { jQuery("#myModal iframe").attr("src", jQuery("#myModal iframe").attr("src")); });
        </script>
        <script>
            jQuery("#myModal2").on("click",'.close', function() { jQuery("#myModal2 iframe").attr("src", jQuery("#myModal2 iframe").attr("src")); });
            jQuery("#myModal2").on("click",'.btn', function() { jQuery("#myModal2 iframe").attr("src", jQuery("#myModal2 iframe").attr("src")); });
        </script>
        <script type="text/javascript">
            ( function($) {
                $(document).ready(function(){
                    if($(".magebridge-checkout").length) {
                        $("#content").hide();
                        $("#check-out").removeClass("col-md-9").addClass("col-md-12");
                    };
                });
                $(document).ready(function(){
                    if($("#magebridge-customer_account_navigation").length) {
                        $(".menu").addClass(" hidden");
                        $(".imi-jump-menu").addClass(" hidden");
                    };
                });
            } ) ( jQuery );
        </script>
        <script type="text/javascript">
            WebFontConfig = {
                google: { families: [ 'Open+Sans::latin', 'Tauri::latin' ] }
            };
            (function() {
                var wf = document.createElement('script');
                wf.src = ('https:' == document.location.protocol ? 'https' : 'http') +
                    '://ajax.googleapis.com/ajax/libs/webfont/1/webfont.js';
                wf.type = 'text/javascript';
                wf.async = 'true';
                var s = document.getElementsByTagName('script')[0];
                s.parentNode.insertBefore(wf, s);
            })(); </script>
        <script>
            var cb = function() {
                var l = document.createElement('link'); l.rel = 'stylesheet';
                l.href = 'https://cdnjs.cloudflare.com/ajax/libs/sweetalert/1.1.0/sweetalert.min.css';
                var h = document.getElementsByTagName('head')[0]; h.parentNode.insertBefore(l, h);
            };
            var raf = requestAnimationFrame || mozRequestAnimationFrame ||
                webkitRequestAnimationFrame || msRequestAnimationFrame;
            if (raf) raf(cb);
            else window.addEventListener('load', cb);
        </script>
        <!-- Facebook Pixel Code -->
        <script>
            !function(f,b,e,v,n,t,s){if(f.fbq)return;n=f.fbq=function(){n.callMethod?
                n.callMethod.apply(n,arguments):n.queue.push(arguments)};if(!f._fbq)f._fbq=n;
                n.push=n;n.loaded=!0;n.version='2.0';n.queue=[];t=b.createElement(e);t.async=!0;
                t.src=v;s=b.getElementsByTagName(e)[0];s.parentNode.insertBefore(t,s)}(window,
                document,'script','//connect.facebook.net/en_US/fbevents.js');

            fbq('init', '546303298879910');
            fbq('track', "PageView");</script>
        <noscript><img height="1" width="1" style="display:none"
                       src="https://www.facebook.com/tr?id=546303298879910&ev=PageView&noscript=1"
            /></noscript>
        <!-- End Facebook Pixel Code -->


        <!-- begin olark code -->
        <script data-cfasync="false" type='text/javascript'>/*<![CDATA[*/window.olark||(function(c){var f=window,d=document,l=f.location.protocol=="https:"?"https:":"http:",z=c.name,r="load";var nt=function(){
                f[z]=function(){
                    (a.s=a.s||[]).push(arguments)};var a=f[z]._={
                },q=c.methods.length;while(q--){(function(n){f[z][n]=function(){
                    f[z]("call",n,arguments)}})(c.methods[q])}a.l=c.loader;a.i=nt;a.p={
                    0:+new Date};a.P=function(u){
                    a.p[u]=new Date-a.p[0]};function s(){
                    a.P(r);f[z](r)}f.addEventListener?f.addEventListener(r,s,false):f.attachEvent("on"+r,s);var ld=function(){function p(hd){
                    hd="head";return["<",hd,"></",hd,"><",i,' onl' + 'oad="var d=',g,";d.getElementsByTagName('head')[0].",j,"(d.",h,"('script')).",k,"='",l,"//",a.l,"'",'"',"></",i,">"].join("")}var i="body",m=d[i];if(!m){
                    return setTimeout(ld,100)}a.P(1);var j="appendChild",h="createElement",k="src",n=d[h]("div"),v=n[j](d[h](z)),b=d[h]("iframe"),g="document",e="domain",o;n.style.display="none";m.insertBefore(n,m.firstChild).id=z;b.frameBorder="0";b.id=z+"-loader";if(/MSIE[ ]+6/.test(navigator.userAgent)){
                    b.src="javascript:false"}b.allowTransparency="true";v[j](b);try{
                    b.contentWindow[g].open()}catch(w){
                    c[e]=d[e];o="javascript:var d="+g+".open();d.domain='"+d.domain+"';";b[k]=o+"void(0);"}try{
                    var t=b.contentWindow[g];t.write(p());t.close()}catch(x){
                    b[k]=o+'d.write("'+p().replace(/"/g,String.fromCharCode(92)+'"')+'");d.close();'}a.P(2)};ld()};nt()})({
                loader: "static.olark.com/jsclient/loader0.js",name:"olark",methods:["configure","extend","declare","identify"]});
            /* custom configuration goes here (www.olark.com/documentation) */
            olark.identify('3504-626-10-9459');/*]]>*/</script><noscript><a href="https://www.olark.com/site/3504-626-10-9459/contact" title="Contact us" target="_blank">Questions? Feedback?</a> powered by <a href="http://www.olark.com?welcome" title="Olark live chat software">Olark live chat software</a></noscript>
        <!-- end olark code -->
        <!-- Google Analytics -->
        <script>
            (function(i,s,o,g,r,a,m){i['GoogleAnalyticsObject']=r;i[r]=i[r]||function(){
                    (i[r].q=i[r].q||[]).push(arguments)},i[r].l=1*new Date();a=s.createElement(o),
                m=s.getElementsByTagName(o)[0];a.async=1;a.src=g;m.parentNode.insertBefore(a,m)
            })(window,document,'script','https://www.google-analytics.com/analytics.js','ga');
            ga('create', 'UA-24028581-1', 'auto');
            ga('require', 'displayfeatures');
            <?php
            $user = JFactory::getUser();
            if (!$user->guest) {
                $user_id= $user->id;
                $gacode = "ga('set', 'userId', '%s');";
                echo sprintf($gacode, $user_id);
                $gacode2 = "ga('set', 'dimension1', '%s');";
                echo sprintf($gacode2, $user_id);
            }
            ?>
            ga('send', 'pageview');

        </script>
        <!-- End Google Analytics -->
</body>
</html>
