<?php
/**
 * @version		$Id: component.php 16465 2010-04-26 01:46:24Z eddieajau $
 * @package		Joomla.Site
 * @subpackage	Templates.beez5
 * @copyright	Copyright (C) 2005 - 2011 Open Source Matters, Inc. All rights reserved.
 * @license		GNU General Public License version 2 or later; see LICENSE.txt
 */

// No direct access.
defined('_JEXEC') or die;

include (dirname(__FILE__).'/index.php');