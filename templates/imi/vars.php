<?php
/**

 * @copyright        Copyright (C) 2005 - 2012 IT OFFSHORE NEPAL PVT. LTD. All rights reserved.

 */

// No direct access.
defined('_JEXEC') or die;



$document = &JFactory::getDocument();
$renderer = $document->loadRenderer('modules');
$leftModuleContent = $renderer->render('position-left',array('style'=>'XHTML'),null);

// check modules
$positionRightExist = $this->countModules('position-right');
$positionRrightExist = $this->countModules('position-rright');
$positionLeftExist = ($this->countModules('position-left') && !empty($leftModuleContent)) ? true : false;
$positionLleftExist = $this->countModules('position-lleft');
$midPosition1Exist = $this->countModules('mid-position1');
$midPosition2Exist = $this->countModules('mid-position2');
$midPosition3Exist = $this->countModules('mid-position3');
$midPosition4Exist = $this->countModules('mid-position4');
$midPosition5Exist = $this->countModules('mid-position5');
$midPosition6Exist = $this->countModules('mid-position6');
$midPosition7Exist = $this->countModules('mid-position7');
$midPosition8Exist = $this->countModules('mid-position8');
$bottomExist = $this->countModules('bottom');
$paymentExist = $this->countModules('payment');

?>