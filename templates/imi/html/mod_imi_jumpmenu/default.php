<?php
/**
 * @version		$Id: default.php 20974 2011-03-16 14:14:03Z chdemko $
 * @package		Joomla.Site
 * @subpackage	mod_jumpmenu
 * @copyright	Copyright (C) 2005 - 2009 Open Source Matters, Inc. All rights reserved.
 * @license		GNU General Public License version 2 or later; see LICENSE.txt
 */

defined('_JEXEC') or die;
?>
<script>
function redirect(url)
{
    document.location.replace(url);
}
</script>
<div class="imi-jump-menu<?php echo $moduleclass_sfx; ?>">
	<span class="imi-jump-menu-title"><?php echo $title; ?></span>
	<?php if(count($list)): ?>
		<span class="imi-jump-menu-list">
<?php
 //echo $_SERVER['REQUEST_URI'];
 
 $url_store_wim = $_SERVER['REQUEST_URI'];
 $url_delen_wim = explode("/", $url_store_wim);
 $aantal_delen_wim = count($url_delen_wim);


// default is /us/shop/ of en/en-store/, dit zijn 3 delen

 ?>

		    <select name="jumpMenuList" onchange="redirect(this.value)" class="inputbox styled">
				<?php
				foreach($list as $item):
				    if($item['type'] == 'alias'){
				    	$item_params = json_decode($item['params']);
						$link = JRoute::_('index.php?Itemid='.$item_params->aliasoptions);
					}
					elseif($item['type'] == 'url'){
						$link = $item['link'];
					}
					else{
						$link = JRoute::_('index.php?Itemid='.$item['id'], false);
					}
				    ?>
<?php 
$product_wim = null;
if($aantal_delen_wim == "4") { $product_wim = $url_delen_wim[3];  }
if($aantal_delen_wim == "5") { $product_wim = $url_delen_wim[3] . "/" . $url_delen_wim[4];  }
if($aantal_delen_wim == "6") { $product_wim = $url_delen_wim[3] . "/" . $url_delen_wim[4] . "/" . $url_delen_wim[5];  }

//echo $product_wim;
 ?>

					<option value="<?php echo $link . "/" . $product_wim; ?>" <?php if($Itemid==$item['id']) echo 'selected="selected"'?>>
				        <?php echo $item['title']; ?>
				    </option>
				<?php endforeach; ?>
			</select>
		</span>
	<?php endif; ?>
</div>
