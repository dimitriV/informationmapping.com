<?php 
/**
* @package   JE Ajax Event Calendar
* @copyright Copyright (C) 2009 - 2010 Open Source Matters. All rights reserved.
* @license   http://www.gnu.org/licenses/lgpl.html GNU/LGPL, see LICENSE.php
* Contact to : emailtohardik@gmail.com, joomextensions@gmail.com
* Visit : http://www.joomlaextensions.co.in/
**/

defined('_JEXEC') or die('Restricted access');
$mosConfig_live_site = JURI :: base();
$doc =& JFactory::getDocument();
$Itemid = JRequest::getVar('Itemid','','','int');
$option = JRequest::getVar('option','','request','string');
$event_id = JRequest::getVar( 'event_id','','','int');
$uri =& JURI::getInstance();
$url= $uri->root();
$doc->addStyleSheet("components/".$option."/assets/css/style.css");
$doc->addScript("components/".$option."/assets/js/event_detail.js");
$model = $this->getModel ( 'alleventlist_more' );
$user 		= clone(JFactory::getUser());
$insert_user= $user->id;
$eimg_path	= $url."components/".$option."/assets/event/images/";
$dynemic_img_path	= $url."components/".$option."/assets/images/";

// ============================= Event hits code ============================ //
	$model->eventhits($event_id);
// ============================= Event hits code ============================ //

// =========================================== GOOGLE MAP CODE =========================================================
	$db	= & JFactory::getDBO();
	$this->_table_prefix = '#__jeajx_';
	$event_id	= JRequest::getVar('event_id','','','int');
	$sqlapi = "SELECT gmap_api,gmap_width,gmap_height,gmap_display FROM ".$this->_table_prefix."event_configration WHERE id =1";
	$db->setQuery($sqlapi);
	$googleapi = $db->LoadObject();
	$tempt_setting=$model->gettempsetting();
	$tempt_allevent_more = $tempt_setting->alleventlist_more_tempt;
	$template_gmap = preg_match("{google_map_tab}",$tempt_allevent_more);
	
	$exploded_date = explode('-',$this->alleventlist_more[0]->start_date);
	if($exploded_date[1]=='01')
		$month = JText::_('JANUARY');
	if($exploded_date[1]=='02')
		$month = JText::_('FEBRUARY');
	if($exploded_date[1]=='03')
		$month = JText::_('MARCH');
	if($exploded_date[1]=='04')
		$month = JText::_('APRIL');
	if($exploded_date[1]=='05')
		$month = JText::_('MAY');
	if($exploded_date[1]=='06')
		$month = JText::_('JUNE');
	if($exploded_date[1]=='07')
		$month = JText::_('JULY');
	if($exploded_date[1]=='08')
		$month = JText::_('AUGUST');
	if($exploded_date[1]=='09')
		$month = JText::_('SEPTEMBER');
	if($exploded_date[1]=='10')
		$month = JText::_('OCTOBER');
	if($exploded_date[1]=='11')
		$month = JText::_('NOVEMBER');
	if($exploded_date[1]=='12')
		$month = JText::_('DECEMBER');
	
	$event_date = $exploded_date[2].' '.$month.' '.$exploded_date[0];
	$exploded_enddate = explode('-',$this->alleventlist_more[0]->end_date);
	
	if($exploded_enddate[1]=='01')
		$emonth = JText::_('JANUARY');
	if($exploded_enddate[1]=='02')
		$emonth = JText::_('FEBRUARY');
	if($exploded_enddate[1]=='03')
		$emonth = JText::_('MARCH');
	if($exploded_enddate[1]=='04')
		$emonth = JText::_('APRIL');
	if($exploded_enddate[1]=='05')
		$emonth = JText::_('MAY');
	if($exploded_enddate[1]=='06')
		$emonth = JText::_('JUNE');
	if($exploded_enddate[1]=='07')
		$emonth = JText::_('JULY');
	if($exploded_enddate[1]=='08')
		$emonth = JText::_('AUGUST');
	if($exploded_enddate[1]=='09')
		$emonth = JText::_('SEPTEMBER');
	if($exploded_enddate[1]=='10')
		$emonth = JText::_('OCTOBER');
	if($exploded_enddate[1]=='11')
		$emonth = JText::_('NOVEMBER');
	if($exploded_enddate[1]=='12')
		$emonth = JText::_('DECEMBER');
	
	$end_date	= $exploded_enddate[2].' '.$emonth.' '.$exploded_enddate[0];
	$link_start_date = JRoute::_('index.php?option=com_jeajaxeventcalendar&view=event_list&Itemid='.$Itemid.'&ey='.$exploded_date[0].'&em='.$exploded_date[1].'&ed='.$exploded_date[2]);
	$date_link = '<a href="'.$link_start_date.'">'.$event_date.'</a>';
	
	// ========================================== Code for Event Data ================================================ //
	$event_title = $this->alleventlist_more[0]->title; 
	$event_photoes	= $model->geteventphoto($event_id); 
	$emain_photoes	= $model->eventmainphoto($event_id);
	if(count($event_photoes)!=0) {
		if(count($emain_photoes)!=0) {
			$emain_img	= $eimg_path.'thumb_'.$emain_photoes->image;
			$eventimg	= $eimg_path.$emain_photoes->image;
		} else {
			$emain_img	= $eimg_path.'thumb_'.$event_photoes[0]->image;
			$eventimg	= $eimg_path.$event_photoes[0]->image;
		}
	} else {
		$emain_img	= $eimg_path.'noimage.png';
		$eventimg	= $eimg_path.'noimage.png';
	}
	// ========================================== EOF Code for Event Data ============================================ //

 	$description = $this->alleventlist_more[0]->desc;
	
	$address = array();
	if($this->alleventlist_more[0]->street){
		$address[] = $this->alleventlist_more[0]->street;
	}
	if($this->alleventlist_more[0]->city){
		$address[] = $this->alleventlist_more[0]->city;
	}
	$address = count($address) ? implode(', ', $address) : '';

	// ======== Design comes from database ============================================================
	$title = $this->alleventlist_more[0]->title;

	if($googleapi->gmap_display==1 && $template_gmap==1 )
	{
		$location	= $this->alleventlist_more[0]->street;
		$add1		= $this->alleventlist_more[0]->city;
		$add3		= $this->alleventlist_more[0]->country_name;
		$add4		= $this->alleventlist_more[0]->postcode;
		
		$gmap_address	= "";
		$gmap_address	= $location;
		$gmap_address  .= ','.$add1;
		if($add3!="")
			$gmap_address .= ','.$add3;
		if($add4!="")
			$gmap_address .= ','.$add4;
		
		$pp = '<div style="width: 210px;padding-right:10px"><table border="0" style="margin-left:20px;"><tr><td width="40px"><img src="'.$emain_img.'" width="40" height="40"></td><td width="30px" valign="top">'.$address.'</td></tr></table></div>';
?>
<script src="http://maps.google.com/maps?file=api&v=1&sensor=true&key=<?php echo $googleapi->gmap_api; ?>" type="text/javascript"></script>
<script type="text/javascript">
	var map = null;
	var geocoder = null;
	var WINDOW_HTML = '<?php echo $pp; ?>';
	function initialize() {
		if (GBrowserIsCompatible()) {
			var mapOptions = {
				googleBarOptions : {
				style : "new"
				}
			}
			map = new GMap2(document.getElementById("map_canvas"));
			map.setCenter(new GLatLng(41.393294, -77.189941), 13);
			map.setUIToDefault();
			geocoder = new GClientGeocoder();
		}
	}

	function showAddress(address) {
		if (geocoder) {
			geocoder.getLatLng(
			address,
			function(point) {
				if (!point) {
					alert(address + " not found");
				} else {
					map.setCenter(point, 13);
					var marker = new GMarker(point);
					map.addOverlay(marker);
					GEvent.addListener(marker, "click", function() {
					marker.openInfoWindowHtml(WINDOW_HTML);
			});
		marker.openInfoWindowHtml(WINDOW_HTML);	
		}
	}  ); } }

	window.onload=function(){
		initialize();
		showAddress("<?php echo $gmap_address?>");
		document.getElementById('event_gmap_div').style.display= 'none';
	}
	window.onunload=function(){
		GUnload();
	}
</script>
<?php  
		$google_map = '<div id="map_canvas" style="width:'.$googleapi->gmap_width.'px; height:'.$googleapi->gmap_height.'px"></div>';
	} else {
		$google_map ='';
	}
//===================================================== End of GOOGLE MAP =============================================== 
?>

<div class="componentheading" >

<!--<h2 class="event-title"><?php echo $this->escape("$event_title"); ?></h2>--></div>
<form action="<?php echo JRoute::_('index.php?option='.$option); ?>" method="post" name="adminForm" enctype="multipart/form-data" >
<?php 	
		$event_photo	= '<img src="'.$emain_img.'" width="472" height="148" alt="Information Mapping Roadshow" class="roadshowimg" >';
if(stristr($_SERVER['REQUEST_URI'], 'resources') == FALSE )
{
echo str_replace('thumb_','',$event_photo);
}
//echo "<span style=\"font-weight: bold; font-size: 18px;\">" . $this->escape("$event_title") . "</span>"; 
		$tempt_allevent_more = str_replace('{title}',$title,$tempt_allevent_more);
		$tempt_allevent_more = str_replace('{event_date}',$event_date,$tempt_allevent_more);
		$tempt_allevent_more = str_replace('{end_date}',$end_date,$tempt_allevent_more);
		$tempt_allevent_more = str_replace('{description}',$description,$tempt_allevent_more);
		$tempt_allevent_more = str_replace('{address}',$address,$tempt_allevent_more);
		//$tempt_allevent_more = str_replace('{google_map}',$google_map,$tempt_allevent_more);
		$tempt_allevent_more = str_replace('{event_photo}',$event_photo,$tempt_allevent_more);
		$tempt_allevent_more = str_replace('{date_link}',$date_link,$tempt_allevent_more);
		
		$event_detail_tab = preg_match("{event_detail_tab}",$tempt_allevent_more);
		$event_video_tab = preg_match("{event_video_tab}",$tempt_allevent_more);
		$event_photo_tab = preg_match("{event_photo_tab}",$tempt_allevent_more);
		$google_map_tab = preg_match("{google_map_tab}",$tempt_allevent_more);
		
		// =========================== Hits Display Code ============================== //
			$eventhits_tag = preg_match('{hits}',$tempt_allevent_more);
			if($eventhits_tag==1) {
				$event_hits	= JText::_('HITS').':'.$this->alleventlist_more[0]->ehits;
				$tempt_allevent_more = str_replace('{hits}',$event_hits,$tempt_allevent_more);
			}
		// =========================== Hits Display Code ============================== //
		
		$related_event_tab = preg_match("{related_event}",$tempt_allevent_more);
		if($related_event_tab==1) {
			$tempt_allevent_more = str_replace('{related_event}','',$tempt_allevent_more);
			$relatedev	= 1;
		} else
			$relatedev	= '';
		
		if($event_detail_tab==1)
			$tempt_allevent_more = str_replace('{event_detail_tab}','',$tempt_allevent_more);
		if($event_video_tab==1)
			$tempt_allevent_more = str_replace('{event_video_tab}','',$tempt_allevent_more);
		if($event_photo_tab==1)
			$tempt_allevent_more = str_replace('{event_photo_tab}','',$tempt_allevent_more);
		if($google_map_tab==1)
			$tempt_allevent_more = str_replace('{google_map_tab}','',$tempt_allevent_more);
			
		
		$outlook_icon 	= preg_match('{outlook}',$tempt_allevent_more);
		$icalendar_icon = preg_match('{ical}',$tempt_allevent_more);
		$gcalendar_icon = preg_match('{gcal}',$tempt_allevent_more);	
		
		if($outlook_icon==1) {
		// ============================= Code for OutLook Calendar =============================================================//
			$link_outlook = JRoute::_("index.php?option=".$option."&view=mycalender&task=generate_outlook&Itemid=".$Itemid.'&event_id='.$this->alleventlist_more[0]->id);
			$outlook = '<a href="'.$link_outlook.'" ><img src="'.$dynemic_img_path.'outlook.png" border="0" ></a>';
			$tempt_allevent_more = str_replace('{outlook}',$outlook,$tempt_allevent_more);
		// ============================= EOF Code for OutLook Calendar =========================================================//
		}
		if($icalendar_icon==1) {
		// ============================= Code for Ical Calendar ===============================================================//
			$link_ical = JRoute::_("index.php?option=".$option."&view=mycalender&task=generate_ical&Itemid=".$Itemid.'&event_id='.$this->alleventlist_more[0]->id);
			$ical = '<a href="'.$link_ical.'" ><img src="'.$dynemic_img_path.'ical.png" border="0" ></a>';
			$tempt_allevent_more = str_replace('{ical}',$ical,$tempt_allevent_more);
		// ============================= EOF Code for Ical Calendar ===========================================================//
		}
		if($gcalendar_icon==1) {
		// ============================= Code for Google Calendar =============================================================//
			$link_gcal = JRoute::_("index.php?option=".$option."&view=mycalender&task=gcalendar&Itemid=".$Itemid.'&event_id='.$this->alleventlist_more[0]->id);
			$gcal = '<a href="'.$link_gcal.'" ><img src="'.$dynemic_img_path.'gcalendar.png" border="0" ></a>';
			$tempt_allevent_more = str_replace('{gcal}',$gcal,$tempt_allevent_more);
		// ============================= EOF Code for Google Calendar =========================================================//
		}
		
			
		
		////////////////////////////////////////////////////////// Dynamic Field Data /////////////////////////////////////////
		$fields1=$model->getfields(2);
		////////////////////////////////////////////////////////// EOF Dynamic Field Data /////////////////////////////////////
		$extra_field_data	= '';
		for($f=0;$f<count($fields1);$f++)
		{
			// =============== Getting Dynemic Fields value ===================	//		
			$fieldsvalue1=$model->fieldsvalue($fields1[$f]->field_id,$this->alleventlist_more[0]->id);	
			// ================================================================	//
			$fieldname= '{'.$fields1[$f]->field_name.'}';
			// ============ Image Field =========================================================================
			if($fields1[$f]->field_type==9)
			{
				$myfile	= $fieldsvalue1->data_txt;
				$myfile	= strtolower($myfile);
				$fileext = strstr($myfile, '.');
				if($fileext==".jpeg" || $fileext==".jpg" || $fileext==".png" || $fileext==".gif" || $fileext==".bmp" || $fileext==".tif" || $fileext==".psd") {
					$extra_field_data	= '<img src="'.$dynemic_img_path.$myfile.'" height="100px" width="100px;" />';
				} else {
					$extra_field_data	= '<a href="'.$dynemic_img_path.$fieldsvalue1->data_txt.'" />'.$fieldsvalue1->data_txt.'</a>';
				}
			} else if($fields1[$f]->field_type==7) {
				$country_id	= $fieldsvalue1->data_txt;
				$country1	= $model->getcountry($country_id);
				$extra_field_data	= $country1->country_name;
			} else {
				$extra_field_data	= ($fields1[$f]->field_name == 'register') ? '<a href="'.$fieldsvalue1->data_txt.'">'.$fieldsvalue1->data_txt.'</a>' : $fieldsvalue1->data_txt;
			}
			$tempt_allevent_more = str_replace($fieldname,$extra_field_data,$tempt_allevent_more); 	
		}
		
		$google_map_icon = preg_match("{google_map_icon}",$tempt_allevent_more);
		if($googleapi->gmap_display==1 && $google_map_icon==1)
		{
			// ================== This code for GOOGLE MAP ===================================================
			$gmaplink 	= JRoute::_('index.php?tmpl=component&option='.$option.'&view=google_map&Itemid='.$Itemid.'&eventid='.$this->alleventlist_more[0]->id);
			$gmapicon = $url.'components/com_jeajaxeventcalendar/assets/images/google-map-icon.png';
			$gmap_icon = "<a  style=\"color:#0099FF;\"  href='".$gmaplink."' title=\"Google MAP\" onclick=\"window.open(this.href,'win2','status=no,toolbar=no,scrollbars=yes,titlebar=no,menubar=no,resizable=yes,width=".$googleapi->gmap_width.",height=".$googleapi->gmap_height.",directories=no,location=no'); return false;\" rel=\"nofollow\"><img src='".$gmapicon."' height=\"16px\" width=\"15px\" border=\"0\"></a>";
			// ===============================================================================================
		} else
			$gmap_icon ="";		
		$tempt_allevent_more = str_replace('{google_map_icon}',$gmap_icon,$tempt_allevent_more);	

		echo $tempt_allevent_more;
		
		$youtubelink	= $this->alleventlist_more[0]->youtubelink;
		$googlelink	= $this->alleventlist_more[0]->googlelink;
		if($event_detail_tab==1 || $event_video_tab==1 || $event_photo_tab==1 || $google_map_tab==1) {  	
?>
  <table width="100%" border="0">
    <tr>
      	<td><ul class="event_tab">
			<?php if($event_detail_tab==1) { ?>	
				<li id="event" class="active" style="background:#D9EBFF; color:#000000;" onclick="showdiv('event_div',this.id)"><?php echo JText::_('EVENT_DETAIL'); ?></li>
				<input type="hidden" id="showevent" value="event" />
			<?php } if($event_video_tab==1 && ($youtubelink!='' || $googlelink!='')) { ?>
				<li id="event_video" onclick="showdiv('event_video_div',this.id)"><?php echo JText::_('EVENT_VIDEO'); ?></li> 
				<input type="hidden" id="showevent" value="noevent" />	
			<?php } if($event_photo_tab==1 && count($event_photoes) > 0) { ?>
				<li id="event_photo" onclick="showdiv('event_photo_div',this.id)"><?php echo JText::_('PHOTO_GALLARY'); ?></li>
				<input type="hidden" id="showevent" value="noevent" />	
			<?php } if($google_map_tab==1) { ?>
				<li id="event_map" onclick="showdiv('event_gmap_div',this.id)"><?php echo JText::_('MAP'); ?></li>
				<input type="hidden" id="showevent" value="noevent" />	
		<?php } ?>
			</ul>
		</td>
    </tr>
  </table>
  <?php } 	?>

<?php 	if($event_detail_tab==1) { 
			////////////////////////////////////////////////////////// Dynamic Field Data /////////////////////////////////////////
			$fields=$model->getfields(2);
			////////////////////////////////////////////////////////// EOF Dynamic Field Data /////////////////////////////////////
			$extra_field_data	= '';
			for($j=0;$j<count($fields);$j++)
			{
				// =============== Getting Dynemic Fields value ===================	//		
				$fieldsvalue=$model->fieldsvalue($fields[$j]->field_id,$this->alleventlist_more[0]->id);	
				// ================================================================	//
				if($fields[$j]->field_type==10) {
					$extra_field_data	.= '<tr><td colspan="3"><hr></td></tr>';
				} else {
					$extra_field_data	.= '<tr><td width="150" class="detail-info-title"><b>'.$fields[$j]->field_title.'</b></td>';
					$extra_field_data	.='</td><td width="3" class="detail-info-sep">:</td>';
				// ============ Image Field =========================================================================
					if($fields[$j]->field_type==9)
					{
						$myfile	= $fieldsvalue->data_txt;
						$myfile	= strtolower($myfile);
						$fileext = strstr($myfile, '.');
						if($fileext==".jpeg" || $fileext==".jpg" || $fileext==".png" || $fileext==".gif" || $fileext==".bmp" || $fileext==".tif" || $fileext==".psd") {
							$extra_field_data	.= '<td class="detail-info"><img src="'.$dynemic_img_path.$myfile.'" height="100px" width="100px;" /></td>';
						} else {
							$extra_field_data	.= '<td class="detail-info"><a href="'.$dynemic_img_path.$fieldsvalue->data_txt.'" />'.$fieldsvalue->data_txt.'</a></td>';
						}
						$image_path=$dynemic_img_path.$fieldsvalue->data_txt;
					} else if($fields[$j]->field_type==7) {
						$country_id	= $fieldsvalue->data_txt;
						$country	= $model->getcountry($country_id);
						$extra_field_data	.= '<td class="detail-info">'.$country->country_name.'</td></tr>';
					} else {
						$extra_field_data	.= '<td class="detail-info">'.$fieldsvalue->data_txt.'</td></tr>';
					}
				} 
			}
?>
  
  <div id="event_div" >
    <table width="100%" border="0" cellpadding="0" cellspacing="0">
      <tr>
        <td width="150" class="detail-info-title"><b><?php echo JText::_('EVENT'); ?></b></td>
        <td width="3" class="detail-info-sep">:</td>
        <td class="detail-info"><?php echo $this->alleventlist_more[0]->title; ?></td>
      </tr>
      <tr>
        <td width="85" class="detail-info-title"><b><?php echo JText::_('START_DATE'); ?></b></td>
        <td width="3" class="detail-info-sep">:</td>
        <td class="detail-info"><?php echo $this->alleventlist_more[0]->start_date; ?></td>
      </tr>
      <tr>
        <td width="85" class="detail-info-title"><b><?php echo JText::_('END_DATE'); ?></b></td>
        <td width="3" class="detail-info-sep">:</td>
        <td class="detail-info"><?php echo $this->alleventlist_more[0]->end_date; ?></td>
      </tr>
      <tr>
        <td width="85" class="detail-info-title"><b><?php echo JText::_('ORGANIZED_AT'); ?></b></td>
        <td width="3" class="detail-info-sep">:</td>
        <td class="detail-info"><?php echo $address; ?></td>
      </tr>
      <tr>
        <td width="85" valign="top" class="detail-info-title"><b><?php echo JText::_('DESCRIPTION'); ?></b></td>
        <td valign="top" width="3" class="detail-info-sep">:</td>
        <td class="detail-info"><?php echo $this->alleventlist_more[0]->desc; ?></td>
      </tr>
	  <?php echo $extra_field_data; ?>
    </table>
  </div>
<?php } ?> 
<?php // added by wvd
if(stristr($_SERVER['REQUEST_URI'], 'resources') == FALSE  )
{
echo "<a href=\"http://www.informationmapping.com/training-roadshow/event.php?id=$event_id\" target=\"_blank\" class=\"eventlinkmore\">LEARN MORE <em class=\"icon-expand\"></em></a>";
}

?>
  <div id="event_video_div" style="display:none;">
<?php 	$video_w	= '100%';
		$video_h	= 350;
		if($youtubelink!='') { 
			$tempstr = strstr($youtubelink, '='); 
		$ext = substr($tempstr, 1);
?>
    <object width="<?php echo $video_w; ?>" height="<?php echo $video_h; ?>px">
      <param name="movie" value="http://www.youtube.com/v/<?php echo $ext; ?>?fs=1&amp;hl=en_US">
      </param>
      <param name="allowFullScreen" value="true">
      </param>
      <param name="allowscriptaccess" value="always">
      </param>
      <embed src="http://www.youtube.com/v/<?php echo $ext; ?>?fs=1&amp;hl=en_US" type="application/x-shockwave-flash" allowscriptaccess="always" allowfullscreen="true" width="<?php echo $video_w; ?>px" height="<?php echo $video_h; ?>px"> </embed>
    </object>
<?php 	}	
		if($googlelink!='') {
			$tempstr = strrchr($googlelink, '=');
		$ext = substr($tempstr, 1); 
?>
    <object width="<?php echo $video_w; ?>px" height="<?php echo $video_h; ?>">
      <param name="movie" value="http://video.google.com/googleplayer.swf?docId=<?php echo $ext; ?>&hl=en">
      </param>
      <param name="allowFullScreen" value="true">
      </param>
      <param name="allowscriptaccess" value="always">
      </param>
      <embed src="http://video.google.com/googleplayer.swf?docId=<?php echo $ext; ?>&hl=en" type="application/x-shockwave-flash" allowscriptaccess="always" allowfullscreen="true" width="<?php echo $video_w; ?>px" height="<?php echo $video_h; ?>px"> </embed>
    </object>
    <?php 	}	?>
  </div>
<script type="text/javascript" language="javascript">
function show_photo_div(img)
{
	var path = document.getElementById("path").value;
	var img_main = path + img ;
	document.getElementById("img_tag").src = img_main;
}
</script>

  <div id="event_photo_div" style="display:none">
	<div id="mainphoto_div"><img src="<?php echo $eventimg; ?>" id="img_tag"  /></div>
	<div class="thumbnails-block">
    	<ul class="list">
		<?php for($j=0;$j<count($event_photoes);$j++) { ?>
          	<li><img src="<?php echo $eimg_path.'thumb_'.$event_photoes[$j]->image; ?>" height="50" width="50" onclick="show_photo_div('<?php echo $event_photoes[$j]->image;  ?>')"/></li>
		<?php } ?>
		</ul>
	</div>
  </div>
  <?php if($google_map): ?>
	  <div id="event_gmap_div" >
	    <?php echo $google_map; ?>
	  </div>
  <?php endif; ?>
<?php	$r=1;
		if($relatedev==1) { 
			$relatedevent	= $model->relatedevents($this->alleventlist_more[0]->category);
?>
		<div id="relatedevent_div" style="margin-top:2%;">
			<table border="0" cellpadding="0" cellspacing="0">
				<tr><td colspan="8"><b><?php echo JText::_('RELATED_EVENTS'); ?></b></td></tr>
				<tr>
<?php 
		for($k=0;$k<count($relatedevent);$k++) {
			$link_edetail = JRoute::_('index.php?option=com_jeajaxeventcalendar&view=alleventlist_more&Itemid='.$Itemid.'&event_id='.$relatedevent[$k]->id);
			$event_photoes	= $model->geteventphoto($relatedevent[$k]->id); 
			$emain_photoes	= $model->eventmainphoto($relatedevent[$k]->id);
			if(count($event_photoes)!=0) {
				if(count($emain_photoes)!=0) {
					$emain_img	= $eimg_path.'thumb_'.$emain_photoes->image;
					$eventimg	= $eimg_path.$emain_photoes->image;
				} else {
					$emain_img	= $eimg_path.'thumb_'.$event_photoes[0]->image;
					$eventimg	= $eimg_path.$event_photoes[0]->image;
				}
				} else {
					$emain_img	= $eimg_path.'noimage.png';
					$eventimg	= $eimg_path.'noimage.png';
				}
?>
				<td valign="top">
					<table border="0">
						<tr><td width="60px"><a href="<?php echo $link_edetail; ?>"><img src="<?php echo $emain_img; ?>" height="60" width="60" border="0" /></a></td></tr>
						<tr><td width="60px"><a href="<?php echo $link_edetail; ?>"><?php echo $relatedevent[$k]->title; ?></a></td></tr>
					</table>
		<?php 	if($r%8==0) {
					echo '</td><tr>';	
				} ?>
				
<?php 	$r++;	} 
?>
			</tr>
			</table>
		</div>
<?php 	}
?>  
  <span class="article_separator">&nbsp;</span>
  <div style="clear:both;"></div>
  <input type="hidden" id="path" name="path" value="<?php echo $eimg_path; ?>" />
  <input type="hidden" name="Itemid" value="<?php echo $Itemid; ?>" />
  <input type="hidden" name="option" value="<?php echo $option;?>">
  <input type="hidden" name="view" value="alleventlist_more" />
  <input type="hidden" name="task" value="" />
  <input type="hidden" name="boxchecked" value="0" />
  <input type="hidden" name="filter_order" value="<?php echo $this->lists['order']; ?>" />
  <input type="hidden" name="filter_order_Dir" value="<?php echo $this->lists['order_Dir']; ?>" />
</form>
