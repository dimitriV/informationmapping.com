<?php
/**
 * @package   JE Ajax Event Calendar
 * @copyright Copyright (C) 2009 - 2010 Open Source Matters. All rights reserved.
 * @license   http://www.gnu.org/licenses/lgpl.html GNU/LGPL, see LICENSE.php
 * Contact to : emailtohardik@gmail.com, joomextensions@gmail.com
 * Visit : http://www.joomlaextensions.co.in/
 **/

defined('_JEXEC') or die('Restricted access');

$model = $this->getModel('categoryevent');
$Itemid = JRequest::getVar('Itemid','','request','int');

?>

<div class="events-wrapper">
	<?php foreach($this->categoryevent as $categoryevent): ?>
		<div class="category-block">
			<h2><?php echo $categoryevent->ename; ?></h2>
			<?php if($categoryevent->edesc != ''): ?>
				<div class="category-desc">
					<?php echo $categoryevent->edesc; ?>
				</div>
			<?php endif; ?>

			<?php
			$events = $model->getevents($categoryevent->id);
			if(count($events)):
				foreach($events as $event):
					//$event_link = JRoute::_('index.php?option=com_jeajaxeventcalendar&view=alleventlist_more&event_id='.$event->id.'&Itemid='.$Itemid);
					$event_link = 'http://www.informationmapping.com/index.php?option=com_jeajaxeventcalendar&view=alleventlist_more&event_id='.$event->id.'&Itemid='.$Itemid;
					$location = array();
					if($event->city != ''){
						$location[] = $event->city;
					}
					if($event->country_name != ''){
						$location[] = $event->country_name;
					}
					?>
					<div class="events-row">
						<h3><?php echo $event->title; ?></h3>
						<ul class="event-info">
							<li><?php echo JText::_('START_DATE'); ?>: <?php echo $event->start_date; ?></li>
							<li><?php echo JText::_('END_DATE'); ?>: <?php echo $event->end_date; ?></li>
							<?php if(count($location)): ?>
								<li><?php echo JText::_('VENUE'); ?>: <?php echo implode(', ', $location); ?></li>
							<?php endif; ?>
						</ul>
						<a class="more" href="<?php echo $event_link; ?>"><?php echo JText::_('READ_MORE'); ?></a>
					</div>
					<?php
				endforeach;
			endif;
			?>
		</div>
	<?php endforeach; ?>

	<div class="pagination">
		<p class="counter">
			<?php echo $this->pagination->getPagesCounter(); ?>
		</p>
		<?php echo $this->pagination->getPagesLinks(); ?>
	</div>
</div>
<div style="clear:both;"></div>