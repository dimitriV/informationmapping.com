<?php
/**
* @package   JE Ajax Event Calendar
* @copyright Copyright (C) 2009 - 2010 Open Source Matters. All rights reserved.
* @license   http://www.gnu.org/licenses/lgpl.html GNU/LGPL, see LICENSE.php
* Contact to : emailtohardik@gmail.com, joomextensions@gmail.com
* Visit : http://www.joomlaextensions.co.in/
**/ 

defined('_JEXEC') or die('Restricted access');
JHTML::_('behavior.tooltip');
JHTMLBehavior::modal();
$uri =& JURI::getInstance();
$url= $uri->root();
$editor =& JFactory::getEditor();
JHTML::_('behavior.calendar');
$doc =& JFactory::getDocument();
$option = JRequest::getVar('option','','','string');
$Itemid = JRequest::getVar('Itemid','','','int');
$user = clone(JFactory::getUser());

//================= Extrafield Validation Java Script =====================
	$doc->addScript("components/".$option."/assets/js/formvalidation.js");
//=========================================================================
// =========================== Bring dynemic fields value ===========================
	$res=new extra_field();
	$fields= $res->list_all_field(2,$this->detail->id);
	$extra=explode("`",$fields);
// =====================================================================================

$model 	= $this->getModel ( 'eventcrlist_detail' );
//$link	= JRoute::_("index.php?option=com_jeajaxeventcalendar&view=eventcrlist_detail_mspp&Itemid=".$Itemid."&task=save");
$link	= JRoute::_("index.php?option=com_jeajaxeventcalendar&view=eventcrlist_detail_mspp&task=save");
$event_setting = $model->getconfigration();


//====================================== New captcha code =============================================== //
$doc->addScript($url.'components/'.$option.'/assets/js/ajax.js' );
$doc->addScript($url.'components/'.$option.'/assets/js/fields1.js' );
//====================================== New captcha code =============================================== //

?>
<script language="javascript" type="text/javascript">
	function form_cancel() {
		history.go(-1); return false;	
	}
</script>

<form action="<?php echo $link; ?>" method="post" name="adminForm" id="adminForm" enctype="multipart/form-data" onsubmit="return validatefrm(adminForm)">
  <div class="col50">
    <fieldset class="adminform">
    <h2><?php echo JText::_( 'DETAILS' ); ?></h2>
    <table width="100%" border="0" cellpadding="0" cellspacing="0" class="admintable" >
      <tr>
        <td align="right" class="key" width="100"><label for="name"> <?php echo JText::_( 'EVENT_TITLE' ); ?> </label>
        </td>
        <td><input class="text_area" type="text" name="title" id="title" size="32" maxlength="250" value="<?php echo $this->detail->title;?>" />
        </td>
      </tr>
      <tr>
        <td align="right" class="key" width="100"><label for="name"> <?php echo JText::_( 'CATEGORY' ); ?> </label>
        </td>
        <td><div class="custom-dropdown"><?php echo $this->lists['category']; ?></div> </td>
      </tr>
      <?php 

		$useraccess = key($user->groups);

		if($useraccess=="Super Users") {

?>
      <tr>
        <td width="100" align="right" class="key" ><label for="name"><?php echo JText::_( 'USER' );?>: </label>
        </td>
        <td><?php echo $this->lists['usr']; ?> </td>
      </tr>
      <?php 	} else {	?>
      <input type="hidden" name="usr[]" value="<?php echo $this->lists['usr']; ?>" />
      <?php	}

?>
	<tr>
		<td width="100" align="right" valign="top">
				<label for="name"><?php echo JText::_( 'REPEAT_EVENT' ); ?>:</label>
		</td>
			<td><div class="custom-dropdown"><?php echo $this->lists['erepeat']; ?></div></td>
	</tr>
	</table>
	
    <?php	if($this->detail->erepeat==0 || $this->detail->erepeat==1 || $this->detail->erepeat==2 || $this->detail->erepeat==3)
				$style='display:block;';
			else
				$style='display:none;';
	?>		
		<div id="norepeat_div" style="<?php echo $style; ?>" >
		<table class="admintable" id="event_table" border="0" cellpadding="0" cellspacing="0" width="100%"  >
		<tr>
			<td width="100" align="right"  valign="top">
				<label for="name"><?php echo JText::_( 'START_DATE' ); ?>:</label>
			</td>
			<td width="162">
      			<input type="text" name="start_date" id="start_date" value="<?php echo $this->detail->start_date; ?>"/>
				
                </td>
                <td width="851">
                <img class="calendar" src="media/system/images/calendar.png" alt="calendar" id="intro_date_img" />

      			<script type="text/javascript">
					Calendar.setup(
					  {
						inputField  : "start_date",         // ID of the input field
						ifFormat    : "%Y-%m-%d",    // the date format
						button      : "intro_date_img"       // ID of the button
					  }
					);
			
		      </script>

   			</td>
		</tr>
		<tr>
			<td width="100" align="right"  valign="top">
				<label for="name"><?php echo JText::_( 'END_DATE' ); ?>:</label>
			</td>
			<td>
      			<input type="text" name="end_date" id="end_date" value="<?php echo $this->detail->end_date; ?>"/>
				</td>
                <td>
                <img class="calendar" src="media/system/images/calendar.png" alt="calendar" id="intro_date_img1" />
	
      			<script type="text/javascript">
					Calendar.setup(
					  {
						inputField  : "end_date",         // ID of the input field
						ifFormat    : "%Y-%m-%d",    // the date format
						button      : "intro_date_img1"       // ID of the button
					  }
					);
				</script>
			</td>
		</tr>
		</table>
		</div>
	<?php	//} 	?>
	
	<?php	if($this->detail->erepeat==4)
				$style1='display:block;';
			else
				$style1='display:none;';
	?>	
	<div id="dialyevent_div" style="<?php echo $style1; ?>">
		<table id="event_table" border="0" cellpadding="0" cellspacing="0" width="100%" class="admintable" >
			<tr>
				<td width="100" align="right" valign="top">&nbsp;</td>
					<td>
						<table cellpadding="0" cellspacing="0" id="extradate_table" border="0">
							<tr>
								<td><table border="0" cellpadding="0" cellspacing="0">
								  <tr>
								    <td ><?php echo JText::_( 'START_DATE' ); ?>&nbsp;</td>
								    <td >&nbsp;</td>
								    <td><?php echo JText::_( 'END_DATE' ); ?>&nbsp;</td>
								    <td >&nbsp;</td>  <td >&nbsp;</td>
							      </tr>
								  <tr>
								    <td ><input type="text" name="daily_sdate"  id="daily_sdate" readonly="true" /></td>
								    <td><img class="calendar" src="templates/system/images/calendar.png" alt="calendar" id="daily_sdate_img" /></td>
								    <script type="text/javascript">
												Calendar.setup(
												{
													inputField  : "daily_sdate",         // ID of the input field
													ifFormat    : "%Y-%m-%d",    // the date format
													button      : "daily_sdate_img"       // ID of the button
												}
												);
													
											</script>
								    <td ><input type="text" name="daily_edate"  id="daily_edate" readonly="true" /></td>
								    <td><img class="calendar" src="templates/system/images/calendar.png" alt="calendar" id="daily_edate_img" /></td>
								    <script type="text/javascript">
												Calendar.setup(
												{
													inputField  : "daily_edate",         // ID of the input field
													ifFormat    : "%Y-%m-%d",    // the date format
													button      : "daily_edate_img"       // ID of the button
												}
												);
													
											</script>
								    <td><input type="button" name="adddatevalue" id="adddatevalue" class="button"  value="<?php echo JText::_( 'ADD_NEW_DATE' ); ?>" onclick="addNewDateRow('extradate_table');" /></td>
							      </tr>
							    </table></td>
							</tr>
							<?php	$alldate_event	= $model->getdateevent($this->detail->id);
									for($m=0;$m<count($alldate_event);$m++) {
							?>
										<tr><td><div id="del_dailydate_div<?php echo $alldate_event[$m]->id; ?>">
											<table cellpadding="0" cellspacing="0">
												<tr>
													<td><input type="text" name="dailysdate[]" value="<?php echo $alldate_event[$m]->dailysdate; ?>" id="dailysdate<?php echo $alldate_event[$m]->id; ?>" ></td>
													<td width="18">&nbsp;</td>
													<td><input type="text" name="dailyedate[]" value="<?php echo $alldate_event[$m]->dailyedate; ?>" id="dailyedate<?php echo $alldate_event[$m]->id; ?>" >&nbsp;<input type="button" value="<?php echo JText::_('DELETE'); ?>" class="button" onclick="delete_dailyevent(<?php echo $alldate_event[$m]->id; ?>,this)" /></td>
												</tr>										
											</table>	
												</div></td></tr>
							<?php	}
							?>	
					</table>
			  </td>
			</tr>
		</table>
	</div>
		
	<table id="event_table" class="admintable" border="0" cellpadding="5" cellspacing="0" width="100%" >
      <tr>
        <td width="100" align="right"  valign="top"><label for="name"> <?php echo JText::_( 'IMAGE' ); ?>: </label>
        </td>
        <td><div class="col50" id="field_data" >
            <table cellpadding="0" cellspacing="0">
              <tr>
                <td><?php echo JText::_( 'USE_THIS_BUTTON_TO_ADD_NEW_PHOTO' ); ?></td>
                <td>
				 <input type="button" name="addvalue" id="addvalue" class="event-add-button" onclick="addNewRow('extra_table');" />
                </td>
              </tr>
            </table>
            <table cellpadding="0" cellspacing="0" border="0" id="extra_table">
            <?php
			if($this->eventphoto){
			?>
              <tr>
                <td><?php
					
						$j=0;
						for($k=0;$k<count($this->eventphoto);$k++)
						{
							$j++; 
?>
		                  <div id="<?php echo 'divphoto'.$k; ?>" >
		                    <table cellpadding="0" cellspacing="0">
		                      <tr>
		                        <td><?php
									$image_dir	= $url."components/".$option."/assets/event/images/";
									echo '<a class="modal" href="'.$image_dir.$this->eventphoto[$k]->image.'"><img src="'.$image_dir."thumb_".$this->eventphoto[$k]->image.'" border="0" ></a>';
		?>
		                        <div class="event-browse-wrapper-img">
		                        	  <input type="file" name="extra_name[]"  id="extra_name[]" style="border:none; height:auto;" />
			                          <?php if($this->eventphoto[$k]->main_image==1) {    ?>
				                          <input type="radio" name="mainphoto" id="mainphoto"  checked="checked" value="<?php echo $j; ?>"   />
				                          <span><?php echo JText::_(' Mainphoto'); ?></span>
			                          <?php } else{  ?>
				                          <input type="radio" name="mainphoto" id="mainphoto"  value="<?php echo $j; ?>"   />
				                          <span><?php echo JText::_(' Mainphoto'); ?></span>
			                          <?php } ?>
			                          <input value="Delete"  title="<?php echo 'divphoto'.$k; ?>" name="<?php echo $this->eventphoto[$k]->image; ?>"  onclick="deleteRow1(this.name,this.title)" class="event-delete-button" type="button" />
			                          <input type="hidden" name="value_id[]" id="value_id[]" value="<?php echo $this->eventphoto[$k]->image; ?>" />
			                        </div>
								  </td>
		                      </tr>
		                    </table>
		                  </div>
                  <?php 	} ?>
				  </td>
              </tr>
              <?php		} else {

						$k=1;  ?>
              <tr>
                <td>
                	<div class="event-browse-wrapper">
                 	  <input type="file" name="extra_name[]" value="field_temp_opt_1" id="extra_name[]" />
	                  <input type="radio" name="mainphoto" id="mainphoto" value="<?php echo $k;?>" checked="checked"/>
	                  <span><?php echo JText::_(' Mainphoto'); ?></span>
						<input type="hidden" name="value_id[]" id="value_id[]" />
	                </div>
				</td>
              </tr>
              <?php		} ?>
            </table>
        </div></td>
      </tr>
	  </table>
	  
	 <table id="event_table" class="admintable" border="0" cellpadding="0" cellspacing="0" width="100%" >
      <tr>
        <td align="right" class="key" valign="top" width="100"><label for="name"> <?php echo JText::_( 'DESCRIPTION' ); ?> </label>
        </td>
        <td><?php echo $editor->display("desc",$this->detail->desc,300,100,'50','20','0');	?> </td>
      </tr>
      <tr valign="top">
        <td width="100" align="right" class="key"><label for="name"><?php echo JText::_( 'STREET' ); ?>:</label>
        </td>
        <td><input class="text_area" type="text" name="street" id="street" size="32" maxlength="250" value="<?php echo $this->detail->street;?>" />
        </td>
      </tr>
      <tr valign="top">
        <td width="100" align="right" class="key"><label for="name"><?php echo JText::_( 'POSTCODE' ); ?>:</label>
        </td>
        <td><input class="text_area" type="text" name="postcode" id="postcode" size="32" maxlength="250" value="<?php echo $this->detail->postcode;?>" />
        </td>
      </tr>
      <tr>
        <td width="100" align="right" class="key"><label for="name"> <?php echo JText::_( 'CITY' ); ?>: </label>
        </td>
        <td><input class="text_area" type="text" name="city" id="city" size="32" maxlength="250" value="<?php echo $this->detail->city;?>" />
        </td>
      </tr>
      <tr>
        <td width="100" align="right" class="key"><label for="name"> <?php echo JText::_( 'COUNTRY' ); ?>: </label>
        </td>
        <td><div class="custom-dropdown">
<select name="country">
<?php
$q_c = mysql_query("SELECT * FROM imi_jeajx_country");
while($row_c = mysql_fetch_array($q_c))
{
echo "<option value=\"" . $row_c['country_id'] . "\"";
$c2 = $this->detail->country;
$c_id = $row_c['country_id'];
if($c2 == $c_id) { echo " selected"; }

echo ">";
echo $row_c['country_name'] . "</option>\n";
}

?>
</select>
</div>
        </td>
      </tr>
<?php	$field_show =count($model->getfields()); 
		if($field_show!=0) {
?>
      <tr>
        <td colspan="2">
<?php  		//=============== Display dynemic field ============
			echo $extra[0];
			//================================================
?>
    		</table>
    	</td>
    </tr>
<?php 	} ?>
    </tr>
    <tr>
      <td align="right" class="key" width="100"></td>
      <td>
      
       <div class="event-blue-btn">
			<button type="submit" name="submit" value="Submit" class="button" >Submit</button>
		</div>
      <div class="event-gray-btn">
			<button type="submit" name="submit" value="Cancel" class="button" onclick="return form_cancel()" >Cancel</button>
		</div>
      </td>
    </tr>
    </table>
    </fieldset>
  </div>
  <div class="clr"></div>
  <input type="hidden" name="cid[]" value="<?php echo $this->detail->id; ?>" />
  <input type="hidden" name="task" value="save" />
  <input type="hidden" name="view" value="eventcrlist_detail" />
<?php 	if($this->detail->id!=0 || $event_setting->autopub==1)
			$published=1;
		else
			$published=0;
?>
  <input type="hidden" name="published" value="<?php echo $published; ?>" />
  <input type="hidden" name="Itemid" value="<?php echo $Itemid; ?>" />
  <input type="hidden" name="insert_user" value="<?php echo $user->id; ?>" />
  <input type="hidden" name="v11" value="<?php echo base64_encode($ResultStr);?>" />
  <input type="hidden" name="rec" id="rec" value="<?php echo $extra[1]; ?>" />
  <input type="hidden" name="rec" id="rec1" value="<?php echo $extra[2]; ?>" />
  <input type="hidden" name="rec" id="rec2" value="<?php echo $extra[3]; ?>" />
  <input type="hidden" name="elive_url" id="elive_url" value="<?php echo $url; ?>" />
  <input type="hidden" value="<?php echo $k;?>" name="total_extra" id="total_extra">
  <input type="hidden" name="youtubelink" id="youtubelink" value="" />
  <input type="hidden" name="googlelink" id="googlelink" value="" />
  <input type="hidden" name="bgcolor" id="bgcolor" value="" />
  <input type="hidden" name="txtcolor" id="txtcolor" value="" />
  <?php echo JHTML::_( 'form.token' ); ?>
</form>
