<?php
/**
 * @package   JE Ajax Event Calendar
 * @copyright Copyright (C) 2009 - 2010 Open Source Matters. All rights reserved.
 * @license   http://www.gnu.org/licenses/lgpl.html GNU/LGPL, see LICENSE.php
 * Contact to : emailtohardik@gmail.com, joomextensions@gmail.com
 * Visit : http://www.joomlaextensions.co.in/
 **/

defined('_JEXEC') or die('Restricted access');
JHTML::_('behavior.tooltip');
JHTMLBehavior::modal();
$uri =& JURI::getInstance();
$url= $uri->root();
$editor =& JFactory::getEditor();
JHTML::_('behavior.calendar');
$doc =& JFactory::getDocument();
$option = JRequest::getVar('option','','','string');
$Itemid = JRequest::getVar('Itemid','','','int');
$user = clone(JFactory::getUser());

//================= Extrafield Validation Java Script =====================
$doc->addScript("components/".$option."/assets/js/formvalidation.js");
//=========================================================================
// =========================== Bring dynemic fields value ===========================
$res=new extra_field();
$fields= $res->list_all_field(2,$this->detail->id);
$extra=explode("`",$fields);
// =====================================================================================

$model 	= $this->getModel ( 'eventcrlist_detail' );
$link	= JRoute::_("index.php?option=com_jeajaxeventcalendar&view=eventcrlist_detail&Itemid=".$Itemid."&task=save");
$event_setting = $model->getconfigration();


//====================================== New captcha code =============================================== //
$doc->addScript($url.'components/'.$option.'/assets/js/ajax.js' );
$doc->addScript($url.'components/'.$option.'/assets/js/fields1.js' );
//====================================== New captcha code =============================================== //

?>
<script language="javascript" type="text/javascript">
    function form_cancel() {
        history.go(-1); return false;
    }
</script>
<form class="form-horizontal" action="<?php echo $link; ?>" method="post" name="adminForm" id="adminForm" enctype="multipart/form-data" onsubmit="return validatefrm(adminForm)">
    <div class="col-md-12">
        <fieldset class="adminform">
            <h2>Event Details</h2>
            <div class="form-group">
                <label for="name" class="col-sm-2 control-label"> <?php echo JText::_( 'COM_JEAJAXEVENTCALENDAR_FORM_EVENT_FIELD_TEMPLATE' ); ?> </label>
                <div class="col-sm-10">
                    <?php echo $this->lists['template']; ?>
                </div>
            </div>
            <div class="form-group">
                <label for="name" class="col-sm-2 control-label"> <?php echo JText::_( 'EVENT_TITLE' ); ?></label>
                <div class="col-sm-10">
                    <input  type="text" name="title" id="title" maxlength="250" class="form-control" value="<?php echo $this->detail->title;?>" />
                </div>
            </div>
            <div class="form-group">
                <label for="name" class="col-sm-2 control-label"> <?php echo JText::_( 'CATEGORY' ); ?> </label>
                <div class="col-sm-10"><?php echo $this->lists['category']; ?>
                </div>
            </div>
            <?php
            $useraccess = key($user->groups);
            if($useraccess=="Super Users") {
                ?>
                <tr>
                    <td width="100" align="right" class="key" ><label for="name"><?php echo JText::_( 'USER' );?>: </label>
                    </td>
                    <td><?php echo $this->lists['usr']; ?> </td>
                </tr>
            <?php 	} else {	?>
                <input type="hidden" name="usr[]" value="<?php echo $this->lists['usr']; ?>" />
            <?php	}

            ?>
            <div class="form-group">
                <label for="name" class="col-sm-2 control-label"><?php echo JText::_( 'REPEAT_EVENT' ); ?>:</label>
                <div class="col-sm-10"><?php echo $this->lists['erepeat']; ?>
                </div>
            </div>

            <?php	if($this->detail->erepeat==0 || $this->detail->erepeat==1 || $this->detail->erepeat==2 || $this->detail->erepeat==3)
                $style='display:block;';
            else
                $style='display:none;';
            ?>
            <div id="norepeat_div" style="<?php echo $style; ?>" >
                <div class="form-group">
                    <label for="name" class="col-sm-2 control-label"><?php echo JText::_( 'START_DATE' ); ?>:</label>
                    <div class="col-sm-9 input-group datebsfix">
                        <input class="form-control" type="text" name="start_date" id="start_date" value="<?php echo $this->detail->start_date; ?>" readonly/>
                    <span class="input-group-btn">
                    <a class="btn"><i class="fa fa-calendar fa-lg" id="intro_date_img" ></i></a>
                        </span>
                        <script type="text/javascript">
                            Calendar.setup(
                                {
                                    inputField  : "start_date",         // ID of the input field
                                    ifFormat    : "%Y-%m-%d",    // the date format
                                    button      : "intro_date_img"       // ID of the button
                                }
                            );
                        </script>
                    </div>
                </div>
                <div class="form-group">
                    <label for="name" class="col-sm-2 control-label"><?php echo JText::_( 'END_DATE' ); ?>:</label>
                    <div class="col-sm-9 input-group datebsfix">
                        <input class="form-control" type="text" name="end_date" id="end_date" value="<?php echo $this->detail->end_date; ?>" readonly/>
                    <span class="input-group-btn">
                    <a class="btn"><i class="fa fa-calendar fa-lg" id="intro_date_img1" ></i></a>
                    </span>
                        <script type="text/javascript">
                            Calendar.setup(
                                {
                                    inputField  : "end_date",         // ID of the input field
                                    ifFormat    : "%Y-%m-%d",    // the date format
                                    button      : "intro_date_img1"       // ID of the button
                                }
                            );
                        </script>
                    </div>
                </div>
            </div>
            <?php	//} 	?>

            <?php	if($this->detail->erepeat==4)
                $style1='display:block;';
            else
                $style1='display:none;';
            ?>
            <div id="dialyevent_div" style="<?php echo $style1; ?>">
                <div class="form-group">
                    <label for="name" class="col-sm-2 control-label"><?php echo JText::_( 'START_DATE' ); ?></label>
                    <div class="col-sm-9 input-group datebsfix">
                        <input class="form-control"  type="text" name="daily_sdate"  id="daily_sdate" readonly/>
                        <span class="input-group-btn">
                    <a class="btn"><i class="fa fa-calendar fa-lg" id="daily_sdate_img" ></i></a>
                    </span>
                        <script type="text/javascript">
                            Calendar.setup(
                                {
                                    inputField  : "daily_sdate",         // ID of the input field
                                    ifFormat    : "%Y-%m-%d",    // the date format
                                    button      : "daily_sdate_img"       // ID of the button
                                }
                            );

                        </script>
                    </div>
                </div>
                <div class="form-group">
                    <label for="name" class="col-sm-2 control-label"><?php echo JText::_( 'END_DATE' ); ?></label>
                    <div class="col-sm-9 input-group datebsfix">
                        <input class="form-control" type="text" name="daily_edate"  id="daily_edate" readonly/>
                    <span class="input-group-btn">
                    <a class="btn"><i class="fa fa-calendar fa-lg" id="daily_edate_img" ></i></a>
                    </span>
                        <script type="text/javascript">
                            Calendar.setup(
                                {
                                    inputField  : "daily_edate",         // ID of the input field
                                    ifFormat    : "%Y-%m-%d",    // the date format
                                    button      : "daily_edate_img"       // ID of the button
                                }
                            );

                        </script>
                    </div>
                </div>
                <div class="form-group">
                    <div class="col-sm-2 input-group datebsfix">
                        <input class="btn btn-blue" type="button" name="adddatevalue" id="adddatevalue"  value="<?php echo JText::_( 'ADD_NEW_DATE' ); ?>" onclick="addNewDateRow('extradate_table');" />
                    </div>
                </div>
                <div id="extradate_table">
                </div>
                <?php	$alldate_event	= $model->getdateevent($this->detail->id);
                for($m=0;$m<count($alldate_event);$m++) {
                    ?>
                    <div id="del_dailydate_div<?php echo $alldate_event[$m]->id; ?>">
                        <div class="form-group">
                            <div class="col-sm-6">
                                <input class="form-control" type="text" name="dailysdate[]" value="<?php echo $alldate_event[$m]->dailysdate; ?>" id="dailysdate<?php echo $alldate_event[$m]->id; ?>" >
                            </div>
                            <div class="col-sm-6">
                                <input class="form-control" type="text" name="dailyedate[]" value="<?php echo $alldate_event[$m]->dailyedate; ?>" id="dailyedate<?php echo $alldate_event[$m]->id; ?>" >
                            </div>
                        </div>
                        <input type="button" value="<?php echo JText::_('DELETE'); ?>" class="btn btn-danger" onclick="delete_dailyevent(<?php echo $alldate_event[$m]->id; ?>,this)" />

                    </div>
                <?php	}
                ?>
            </div>

            <table id="event_table" class="admintable" border="0" cellpadding="5" cellspacing="0" width="100%" >
                <tr>
                    <td width="100" align="right"  valign="top"><label for="name"> <?php echo JText::_( 'IMAGE' ); ?>: </label>
                    </td>
                    <td><div class="col50" id="field_data" >
                            <table cellpadding="0" cellspacing="0">
                                <tr>
                                    <td><?php echo JText::_( 'USE_THIS_BUTTON_TO_ADD_NEW_PHOTO' ); ?></td>
                                    <td>
                                        <input type="button" name="addvalue" id="addvalue" class="event-add-button" onclick="addNewRow('extra_table');" />
                                    </td>
                                </tr>
                            </table>
                            <table cellpadding="0" cellspacing="0" border="0" id="extra_table">
                                <?php
                                if($this->eventphoto){
                                    ?>
                                    <tr>
                                        <td><?php

                                            $j=0;
                                            for($k=0;$k<count($this->eventphoto);$k++)
                                            {
                                                $j++;
                                                ?>
                                                <div id="<?php echo 'divphoto'.$k; ?>" >
                                                    <table cellpadding="0" cellspacing="0">
                                                        <tr>
                                                            <td><?php
                                                                $image_dir	= $url."components/".$option."/assets/event/images/";
                                                                echo '<a class="modal" href="'.$image_dir.$this->eventphoto[$k]->image.'"><img src="'.$image_dir."thumb_".$this->eventphoto[$k]->image.'" border="0" ></a>';
                                                                ?>
                                                                <div class="event-browse-wrapper-img">
                                                                    <input type="file" name="extra_name[]"  id="extra_name[]" style="border:none; height:auto;" />
                                                                    <?php if($this->eventphoto[$k]->main_image==1) {    ?>
                                                                        <input type="radio" name="mainphoto" id="mainphoto"  checked="checked" value="<?php echo $j; ?>"   />
                                                                        <span><?php echo JText::_(' Mainphoto'); ?></span>
                                                                    <?php } else{  ?>
                                                                        <input type="radio" name="mainphoto" id="mainphoto"  value="<?php echo $j; ?>"   />
                                                                        <span><?php echo JText::_(' Mainphoto'); ?></span>
                                                                    <?php } ?>
                                                                    <input value="Delete"  title="<?php echo 'divphoto'.$k; ?>" name="<?php echo $this->eventphoto[$k]->image; ?>"  onclick="deleteRow1(this.name,this.title)" class="event-delete-button" type="button" />
                                                                    <input type="hidden" name="value_id[]" id="value_id[]" value="<?php echo $this->eventphoto[$k]->image; ?>" />
                                                                </div>
                                                            </td>
                                                        </tr>
                                                    </table>
                                                </div>
                                            <?php 	} ?>
                                        </td>
                                    </tr>
                                <?php		} else {

                                    $k=1;  ?>
                                    <tr>
                                        <td>
                                            <div class="event-browse-wrapper">
                                                <input type="file" name="extra_name[]" value="field_temp_opt_1" id="extra_name[]" />
                                                <input type="radio" name="mainphoto" id="mainphoto" value="<?php echo $k;?>" checked="checked"/>
                                                <span><?php echo JText::_(' Mainphoto'); ?></span>
                                                <input type="hidden" name="value_id[]" id="value_id[]" />
                                            </div>
                                        </td>
                                    </tr>
                                <?php		} ?>
                            </table>
                        </div></td>
                </tr>
            </table>

            <div class="form-group">
                <label class="col-sm-2 control-label" for="name"> <?php echo JText::_( 'DESCRIPTION' ); ?> </label>
                <div class="col-sm-9">
                    <?php echo $editor->display("desc",$this->detail->desc,300,100,'50','20','0');	?>
                </div>
            </div>
            <div class="form-group">
                <label class="col-sm-2 control-label"  for="name"><?php echo JText::_( 'STREET' ); ?>:</label>
                <div class="col-sm-9">
                    <input  class="form-control" type="text" name="street" id="street" size="32" maxlength="250" value="<?php echo $this->detail->street;?>" />
                </div>
            </div>
            <div class="form-group">
                <label class="col-sm-2 control-label"  for="name"><?php echo JText::_( 'POSTCODE' ); ?>:</label>
                <div class="col-sm-9">
                    <input class="form-control" type="text" name="postcode" id="postcode" size="32" maxlength="250" value="<?php echo $this->detail->postcode;?>" />
                </div>
            </div>
            <div class="form-group">
                <label  class="col-sm-2 control-label" for="name"> <?php echo JText::_( 'CITY' ); ?>: </label>
                <div class="col-sm-9">
                    <input class="form-control" type="text" name="city" id="city" size="32" maxlength="250" value="<?php echo $this->detail->city;?>" />
                </div>
            </div>
            <div class="form-group">
                <label  class="col-sm-2 control-label"  for="name"> <?php echo JText::_( 'COUNTRY' ); ?>: </label>
                <div class="col-sm-9">
                    <select name="country" class="form-control">
                        <?php
                        $q_c = mysql_query("SELECT * FROM imi_jeajx_country");
                        while($row_c = mysql_fetch_array($q_c))
                        {
                            echo "<option value=\"" . $row_c['country_id'] . "\"";
                            $c2 = $this->detail->country;
                            $c_id = $row_c['country_id'];
                            if($c2 == $c_id) { echo " selected"; }

                            echo ">";
                            echo $row_c['country_name'] . "</option>\n";
                        }

                        ?>
                    </select>
                </div>
            </div>
            <?php	$field_show =count($model->getfields());
            if($field_show!=0) {
            ?>
            <tr>
                <td colspan="2">
                    <?php  		//=============== Display dynemic field ============
                    echo $extra[0];
                    //================================================
                    ?>
                    </table>


                    <?php 	} ?>
            </tr>
            <div class="form-group">
                <div class="col-md-offset-4 col-sm-3">
                    <button type="submit" name="submit" value="Cancel" class="btn btn-grey" onclick="return form_cancel()" >Cancel</button>
                    <button type="submit" name="submit" value="Submit" class="btn btn-blue" >Submit</button>
                </div>
            </div>
        </fieldset>
    </div>
    <div class="clr"></div>
    <input type="hidden" name="cid[]" value="<?php echo $this->detail->id; ?>" />
    <input type="hidden" name="task" value="save" />
    <input type="hidden" name="view" value="eventcrlist_detail" />
    <?php 	if($this->detail->id!=0 || $event_setting->autopub==1)
        $published=1;
    else
        $published=0;
    ?>
    <input type="hidden" name="published" value="<?php echo $published; ?>" />
    <input type="hidden" name="Itemid" value="<?php echo $Itemid; ?>" />
    <input type="hidden" name="insert_user" value="<?php echo $user->id; ?>" />
    <input type="hidden" name="v11" value="<?php echo base64_encode($ResultStr);?>" />
    <input type="hidden" name="rec" id="rec" value="<?php echo $extra[1]; ?>" />
    <input type="hidden" name="rec" id="rec1" value="<?php echo $extra[2]; ?>" />
    <input type="hidden" name="rec" id="rec2" value="<?php echo $extra[3]; ?>" />
    <input type="hidden" name="elive_url" id="elive_url" value="<?php echo $url; ?>" />
    <input type="hidden" value="<?php echo $k;?>" name="total_extra" id="total_extra">
    <input type="hidden" name="youtubelink" id="youtubelink" value="" />
    <input type="hidden" name="googlelink" id="googlelink" value="" />
    <input type="hidden" name="bgcolor" id="bgcolor" value="" />
    <input type="hidden" name="txtcolor" id="txtcolor" value="" />
    <?php echo JHTML::_( 'form.token' ); ?>
</form>
