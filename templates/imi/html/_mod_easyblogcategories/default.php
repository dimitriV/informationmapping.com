<?php
/**
 * @package		EasyBlog
 * @copyright	Copyright (C) 2010 Stack Ideas Private Limited. All rights reserved.
 * @license		GNU/GPL, see LICENSE.php
 *
 * EasyBlog is free software. This version may have been modified pursuant
 * to the GNU General Public License, and as distributed it includes or
 * is derivative of works licensed under the GNU General Public License or
 * other free or open source software licenses.
 * See COPYRIGHT.php for copyright notices and details.
 */
defined('_JEXEC') or die('Restricted access');
?>

<div id="ezblog-categories" class="ezb-mod eblog-module-categories">
	<?php if(!empty($categories)){ ?>
		<?php echo accessNestedCategories( $categories , $selected , $params ); ?>
	<?php } else { ?>
			<?php echo JText::_('MOD_EASYBLOGCATEGORIES_NO_CATEGORY'); ?>
	<?php } ?>
</div>

<?php
function accessNestedCategories( &$categories , $selected , $params , $level = 0 )
{
	foreach($categories as $category)
	{
		$active = ($category->id == $selected) ? ' active' : '';
		
		if( $params->get( 'layouttype' ) == 'tree' )
		{
			$category->level	-= 1;
			$padding	= $category->level * 30;
		}
		
?>
		<div class="ezitem ezcf"<?php echo ( $params->get( 'layouttype' ) == 'tree' ) ? ' style="padding-left: ' . $padding . 'px;"' : '';?>>
	 		<div class="eztc<?php echo $active; ?>">
				<a href="<?php echo EasyBlogRouter::_('index.php?option=com_easyblog&view=categories&layout=listings&id='.$category->id );?>">
	 				<span class="eztc-catname"><?php echo $category->title; ?></span>
					<span class="eztc-postcounter"><?php echo $category->cnt;?></span>
				</a>
			 </div>
		</div>
<?php
		if( $params->get( 'layouttype' ) == 'tree' || $params->get( 'layouttype' ) == 'flat' )
		{
			if( isset( $category->childs ) && is_array( $category->childs ) )
			{
			    accessNestedCategories( $category->childs , $selected, $params ,  $level );
			}
		}

		if( empty( $category->childs ) )
		{
		    $level  = 0;
		}
	}
}
?>