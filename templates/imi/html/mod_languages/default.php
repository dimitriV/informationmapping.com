<?php
/**
 * @version		$Id: default.php 22372 2011-11-09 16:47:59Z github_bot $
 * @package		Joomla.Site
 * @subpackage	mod_languages
 * @copyright	Copyright (C) 2005 - 2011 Open Source Matters, Inc. All rights reserved.
 * @license		GNU General Public License version 2 or later; see LICENSE.txt
 */

// no direct access
defined('_JEXEC') or die('Restricted access');
//JHtml::_('stylesheet', 'mod_languages/template.css', array(), true);
?>
<div class="country col-xs-6 col-sm-3 col-md-2">
<?php if ($headerText) : ?>
	<div class="pretext"><p><?php echo $headerText; ?></p></div>
<?php endif; ?>


<script type="text/javascript">

function changeFunc() {
    var selectBox = document.getElementById("selectBox");
    var selectedValue = selectBox.options[selectBox.selectedIndex].value;

    if (selectedValue == "/jp/") {
        window.location.assign("http://www.informationmapping.com/en/find-a-partner/partner/12");
    }
    else if (selectedValue == "/aus/") {
        window.location.assign("http://www.informationmapping.com/en/find-a-partner/partner/6");
    }
    else if (selectedValue == "/be/") {
        window.location.assign("http://www.informationmapping.com/en/find-a-partner/partner/2");
    }
    else if (selectedValue == "/fr/") {
        window.location.assign("http://www.informationmapping.com/en/find-a-partner/partner/9");
    }
    else if (selectedValue == "/id/") {
        window.location.assign("http://www.informationmapping.com/en/find-a-partner/partner/18");
    }
    else if (selectedValue == "/it/") {
        window.location.assign("http://www.informationmapping.com/en/find-a-partner/partner/11");
    }
    else if (selectedValue == "/my/") {
        window.location.assign("http://www.informationmapping.com/en/find-a-partner/partner/21");
    }
    else if (selectedValue == "/mx/") {
        window.location.assign("http://www.informationmapping.com/en/find-a-partner/partner/13");
    }
    else if (selectedValue == "/nz/") {
        window.location.assign("http://www.informationmapping.com/en/find-a-partner/partner/6");
    }
    else if (selectedValue == "/pl/") {
        window.location.assign("http://www.informationmapping.com/en/find-a-partner/partner/23");
    }
    else if (selectedValue == "/ru/") {
        window.location.assign("http://www.informationmapping.com/en/find-a-partner/partner/24");
    }
    else if (selectedValue == "/sa/") {
        window.location.assign("http://www.informationmapping.com/en/find-a-partner/partner/25");
    }
    else if (selectedValue == "/tr/") {
        window.location.assign("http://www.informationmapping.com/en/find-a-partner/partner/22");
    }
    else if (selectedValue == "/uk/") {
        window.location.assign("http://www.informationmapping.com/en/find-a-partner/partner/4");
    }
    else if (selectedValue == "/ca/") {
        window.location.assign("http://www.informationmapping.com/us/");
    }

    else {
        window.location.assign(selectedValue);
    }

}
</script>



<?php if ($params->get('dropdown',1)) : ?>
	<form name="lang" method="post" action="" class="form-inline text-right">
	<select id="selectBox" class="form-control" onchange="changeFunc();" >


<option dir="ltr" value="/en/">Global</option>
<option dir="ltr" value="/aus/">Australia</option>
<option dir="ltr" value="/be/">Belgium</option>
<option dir="ltr" value="/ca/">Canada</option>
<option dir="ltr" value="/fr/">France</option>
<option dir="ltr" value="/in/"<?php if(stristr($_SERVER['REQUEST_URI'], '/in/') == TRUE)
{ echo " selected=\"selected\""; } ?>>India</option>
<option dir="ltr" value="/id/">Indonesia</option>
<option dir="ltr" value="/it/">Italy</option>
<option dir="ltr" value="/jp/">Japan</option>
<option dir="ltr" value="/my/">Malaysia</option>
<option dir="ltr" value="/mx/">Mexico</option>
<option dir="ltr" value="/aus/">New Zealand</option>
<option dir="ltr" value="/pl/">Poland</option>
<option dir="ltr" value="/ru/">Russia</option>
<option dir="ltr" value="/sa/">South Africa</option>
<option dir="ltr" value="/tr/">Turkey</option>
<option dir="ltr" value="/uk/">United Kingdom</option>
<option dir="ltr" value="/us/"<?php if(stristr($_SERVER['REQUEST_URI'], '/us/') == TRUE)
{ echo " selected=\"selected\""; } ?>>United States</option>
	</select>
	</form>
<?php else : ?>
	<ul class="<?php echo $params->get('inline', 1) ? 'lang-inline' : 'lang-block';?>">
	<?php foreach($list as $language):?>
		<?php if ($params->get('show_active', 0) || !$language->active):?>
			<li class="<?php echo $language->active ? 'lang-active' : '';?>" dir="<?php echo JLanguage::getInstance($language->lang_code)->isRTL() ? 'rtl' : 'ltr' ?>">
			<a href="<?php echo $language->link;?>">
			<?php if ($params->get('image', 1)):?>
				<?php echo JHtml::_('image', 'mod_languages/'.$language->image.'.gif', $language->title_native, array('title'=>$language->title_native), true);?>
			<?php else : ?>
				<?php echo $params->get('full_name', 1) ? $language->title_native : strtoupper($language->sef);?>
			<?php endif; ?>
			</a>
			</li>
		<?php endif;?>
	<?php endforeach;?>
	</ul>
<?php endif; ?>

<?php if ($footerText) : ?>
	<div class="posttext"><p><?php echo $footerText; ?></p></div>
<?php endif; ?>
</div>
