<?php
/**
 * Joomla! module MageBridge Login 
 *
 * @author Yireo (info@yireo.com)
 * @package MageBridge
 * @copyright Copyright 2011
 * @license GNU Public License
 * @link http://www.yireo.com/
 */
        
// No direct access
defined('_JEXEC') or die('Restricted access');
?>

<?php if($type == 'logout_link') : ?>
    <form class="mod-login-form" action="<?php echo $component_url; ?>" method="post" name="mod-magebridge-logout" id="login-form">
        <?php if( $params->get('greeting') ) : ?>
	        <div class="login-greeting">
				<?php echo JText::sprintf($params->get('greeting'), $name); ?>
			</div>
        <?php endif; ?>
	    <div class="logout-button">
	        <input type="submit" name="Submit" class="button" value="<?php echo JText::_('JLOGOUT'); ?>" />
	        <input type="hidden" name="option" value="<?php echo $component ?>" />
	        <input type="hidden" name="task" value="<?php echo $task_logout ?>" />
	        <input type="hidden" name="return" value="<?php echo $return_url ?>" />
	        <?php echo JHTML::_( 'form.token' ); ?>
	    </div>
    </form>
<?php else : ?> 
	<form class="mod-login-form" action="<?php echo $component_url; ?>" method="post" name="mod-magebridge-login" id="login-form">
	    <?php if( $params->get('text') ) : ?>
		    <div class="pretext">
				<p><?php echo JText::sprintf($params->get('text'), $name); ?></p>
			</div>
	    <?php endif; ?>
	    <fieldset class="userdata">
		    <p id="form-login-username">
		        <label for="modlgn-username"><?php echo JText::_('Email') ?></label>
		        <input class="inputbox" type="text" id="modlgn-username" size="18" name="username" />
		    </p>
		    <p id="form-login-password">
		        <label for="modlgn-passwd"><?php echo JText::_('Password') ?></label>
		        <input type="password" class="inputbox" id="modlgn-passwd" size="18" name="<?php echo $password_field; ?>" />
		    </p>
		    <?php if(JPluginHelper::isEnabled('system', 'remember')) : ?>
		    <p id="form-login-remember">
		        <label for="modlgn-remember"><?php echo JText::_('Remember me') ?></label>
		        <input type="checkbox" name="remember" id="modlgn-remember" value="yes" checked="checked" class="inputbox" />
		    </p>
		    <?php endif; ?>
		    <input type="submit" value="<?php echo JText::_('Login') ?>" class="button" name="<?php echo JText::_('JLOGIN') ?>" class="button" />
		    <input type="hidden" name="option" value="<?php echo $component ?>" />
	        <input type="hidden" name="task" value="<?php echo $task_login ?>" />
	        <input type="hidden" name="return" value="<?php echo $return_url ?>" />
	        <?php echo JHTML::_( 'form.token' ); ?>
	    </fieldset>
	    <ul>
            <li><a href="<?php echo $forgotpassword_url; ?>"><?php echo JText::_('Forgot your password'); ?></a></li>
            <li><a href="<?php echo $createnew_url; ?>"><?php echo JText::_('Create an account'); ?></a></li>
        </ul>
	</form>
<?php endif; ?>
