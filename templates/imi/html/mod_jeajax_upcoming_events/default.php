<?php 
/**
* @package   JE Ajax Upcoming Events
* @copyright Copyright (C) 2011 IT Offshore Nepal. All rights reserved.
**/
defined('_JEXEC') or die('Restricted access'); 

$moduleclass_sfx = $params->get('moduleclass_sfx','');
$details_text = $params->get('details_text', 'See details');
$more_events_text = $params->get('more_events_text', 'More events');
$menuitem	= $params->get('menuitem', 0);
$order_by 	= $params->get('order', 'ordering');
$order_dir 	= $params->get('direction', 'asc');

if($order_by == 'start_date' && $order_dir == 'desc'){
	$events = array_reverse($events);
}
?>

<div class="events-wrapper<?php echo $moduleclass_sfx; ?>">
<?php
// als er geen enkel event is, moet wel de Evergreen webinar getoond worden
/* if(!count($events)){ 
echo "<div class=\"month-block\">";
echo "<div class=\"event-block\">";
echo "Every Tuesday and Thursday<br />";
echo "<a href=\"http://www.informationmapping-webinars.com/webinar1/webinar-register.php?trackingID1=home&trackingID2=all_events&landingpage=default&expiration=default\" target=\"_blank\"><strong>";
echo "Information Mapping: What is it? Why use it?</strong></a>";
echo "</div>";
echo "</div>";
}*/

?>
<?php
// als er in de huidige maand geen event is, moet deze maand wel toegevoegd worden om het Evergreen webinar in te tonen
if($event->event_month != date(F)) 
{  
echo "<div class=\"month-block\">";
echo "<h2>" . JText::_(strtoupper(date(F))) . "</h2><br /><br style=\"font-size: 2px\" />";
echo "<div class=\"event-block\">";
echo "Every Tuesday and Thursday<br />";
echo "<a href=\"http://www.informationmapping-webinars.com/webinar1/webinar-register.php?trackingID1=home&trackingID2=all_events&landingpage=default&expiration=default\" target=\"_blank\"><strong>";
echo "Information Mapping: What is it? Why use it?</strong></a>";
echo "</div>";


}
?>



<?php if(count($events)){ ?>
	<div class="month-block">

<?php	
	$event_month = '';
	$i = 0;
	foreach($events as $event){
		$start_date = explode('-', $event->start_date);
		$end_date = explode('-', $event->end_date);
		$location = array();
		if($event->city != ''){
			$location[] = $event->city;
		}
		if($event->country_name != ''){
			$location[] = $event->country_name;
		}
		$location = implode(', ', $location);
		$location = ($location != '') ? '('.$location.')' : '';
	
		if($event_month != $event->event_month){
			$event_month = $event->event_month;
			if($i > 0){
			?>
				</div><div class="month-block">
			<?php
			}
			?>
			<h2><?php echo JText::_(strtoupper($event_month)); ?></h2><br /><br style="font-size: 2px" />




<?php
// show evergreen webinar
if(($start_date[2] == $end_date[2]) && ($event->event_month == date(F))) {
echo "<div class=\"event-block\">";
echo "Every Tuesday and Thursday<br />";
echo "<a href=\"http://www.informationmapping-webinars.com/webinar1/webinar-register.php?trackingID1=home&trackingID2=all_events&landingpage=default&expiration=default\" target=\"_blank\"><strong>";
echo "Information Mapping: What is it? Why use it?</strong></a>";
echo "</div>";
}
?>


			<?php
		}
		$i++;
	?>


		<div class="event-block">
			<!--<a href="<?php echo JRoute::_( 'index.php?option=com_jeajaxeventcalendar&view=alleventlist_more&event_id='.$event->id.'&Itemid='.$menuitem ); ?>">-->
			<?php 
			if($start_date[2] == $end_date[2]) {
			//echo $event_month . " " . $start_date[2].' '.$location;	
echo $event_month . " " . $start_date[2];	
			} else
			{
			//echo $event_month . " " . $start_date[2].'-'.$end_date[2].' '.$location; 
			echo $event_month . " " . $start_date[2].'-'.$end_date[2]; 
			}
			?><!--</a>--><br />
			<a href="<?php echo JRoute::_( 'index.php?option=com_jeajaxeventcalendar&view=alleventlist_more&event_id='.$event->id.'&Itemid='.$menuitem ); ?>">
			<strong><?php echo $event->title; ?></strong></a>
            
			<!--<a href="<?php echo JRoute::_( 'index.php?option=com_jeajaxeventcalendar&view=alleventlist_more&event_id='.$event->id.'&Itemid='.$menuitem ); ?>"><?php echo $details_text; ?></a>-->
		</div>
	<?php } ?>

	</div>



	<!--<a class="read-more" href="<?php echo JRoute::_( 'index.php?Itemid='.$menuitem ); ?>"><?php echo $more_events_text; ?></a>-->
<?php
$url_training_wim = $_SERVER['REQUEST_URI'];

if(strpos($url_training_wim, '/ca/') !== false)
{
$url_show = "https://www.informationmapping.com/training-roadshow/?country=38";
}

if(strpos($url_training_wim, '/in/') !== false)
{
$url_show = "ttps://www.informationmapping.com/training-roadshow/?country=99";
}

if(strpos($url_training_wim, '/us/') !== false)
{
$url_show = "https://www.informationmapping.com/training-roadshow/?country=223";
}

if(strpos($url_training_wim, '/en/') !== false)
{
$url_show = "https://www.informationmapping.com/training-roadshow/";
}
?>
<a class="read-more" href="<?php echo $url_show; ?>" target="_blank"><?php echo $more_events_text; ?></a>


	<?php } ?>
</div>
