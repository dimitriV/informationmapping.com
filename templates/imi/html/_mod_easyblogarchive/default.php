<?php
/*
 * @package		mod_easyblogarchive
 * @copyright	Copyright (C) 2010 Stack Ideas Private Limited. All rights reserved.
 * @license		GNU/GPL, see LICENSE.php
 *
 * EasyBlog is free software. This version may have been modified pursuant
 * to the GNU General Public License, and as distributed it includes or
 * is derivative of works licensed under the GNU General Public License or
 * other free or open source software licenses.
 * See COPYRIGHT.php for copyright notices and details.
 */
defined('_JEXEC') or die('Restricted access');
?>
<div id="eblog-module-archive" class="ezb-mod eblog-module-archive">
<?php if(!empty($year)): ?>
<ul class="archive-container">
	<?php for($i = $year['maxyear']; $i >= $year['minyear']; $i--) : ?>
			<?php
			    $showEmpty = $params->get( 'showempty', 1);

				for($m = 1; $m < 13; $m++)
				{
					$curDate    	= JFactory::getDate( $i . '-' . $m . '-01' );

					// Do not show future months or empty months
					if( $i < $currentYear || ($i == $currentYear && $m <= $currentMonth && !$params->get( 'showfuture') ) || $params->get( 'showfuture' )  )
					{
					    if( ( $showEmpty ) || (!$showEmpty && !empty( $postCounts->$i->$m ) ) )
					    {
          					$monthSEF	= ( strlen($m) < 2) ? '0' . $m : $m;
          					$post_counter = empty($postCounts->$i->$m) ? 0 : $postCounts->$i->$m;
          					$active = ($defaultYear == $i && $defaultMonth == $m) ? ' active' : '';
			?>
				<li class="archive-item<?php echo $active; ?>">
					<a href="<?php echo EasyBlogRouter::_('index.php?option=com_easyblog&view=archive&archiveyear='.$i.'&archivemonth='.$monthSEF.$modItemId ); ?>">
						<span class="archive-date"><?php echo $curDate->toFormat('%B %Y'); ?></span>
						<span class="archive-postcounter"><?php echo $post_counter; ?></span>
					</a>
				</li>
			<?php
						}//end if
					}//end if
				}//end for
			?>
	<?php endfor; ?>
</ul>
<?php else: ?>
	<?php echo JText::_('MOD_EASYBLOGARCHIVE_NO_POST'); ?>
<?php endif; ?>
</div>

