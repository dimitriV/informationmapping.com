<body style="font-family: monospace;">
<?php
	include_once("minifier.php");
	
	/* FILES ARRAYs
	 * Keys as input, Values as output */ 
	
	$js = array(
		"../javascript/jquery.uniform.js" 			=> "../javascript/jquery.uniform.min.js"
	);
	
	$css = array(
		"merge.css"	=> "merge.min.css",
	);
	
	minifyJS($js);
	minifyCSS($css);
?>
</body>
