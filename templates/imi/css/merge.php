<?php
$files = array(
    '../../system/css/system.css',
    'framework.css',
    'general.css',
    'uniform.aristo.css'
);
$mergeCss = dirname(__FILE__).'/merge.css';

file_put_contents($mergeCss, null);
foreach($files as $file) {
    $file = dirname(__FILE__).'/'.$file;
    $content = file_get_contents($file);

    $datauri = true;
    if ($datauri && preg_match_all('/url\(([^\(]+)\)/i', $content, $matches)) {
        foreach ($matches[1] as $index => $match) {

            $match = str_replace('\'', '', $match);
            $match = str_replace('"', '', $match);

            if (stristr($match, '.css')) continue;
            if (preg_match('/^(http|https):\/\//', $match)) continue;
            if (preg_match('/^\/\//', $match)) continue;
            if (preg_match('/^data\:/', $match)) continue;

            if (preg_match('/^\.\.\/images\//', $match)) {
                $image = dirname(__FILE__).'/'.$match;
                if(file_exists($image) && filesize($image) < 1000) {
                    $imageContent = file_get_contents($image);
                    if(!empty($imageContent)) {
                        if (preg_match('/\.gif$/i', $image)) {
                            $mimetype = 'image/gif';
                        } else if (preg_match('/\.png$/i', $image)) {
                            $mimetype = 'image/png';
                        } else if (preg_match('/\.webp$/i', $image)) {
                            $mimetype = 'image/webp';
                        } else if (preg_match('/\.(jpg|jpeg)$/i', $image)) {
                            $mimetype = 'image/jpg';
                        }
                        $imageBase64 = 'data:'.$mimetype.';base64,'.base64_encode($imageContent);
                        $content = str_replace($match, $imageBase64, $content);
                    }
                }
            }
        }
    }    

    file_put_contents($mergeCss, $content."\n", FILE_APPEND);
echo "<p>Rebuild CSS merged file done</p>";
}
