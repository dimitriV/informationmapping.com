var alreadyLoaded = false;
var useDebug = false;
var debug = null;
var CustomerID = 'GM-InfoMap';
var DatabaseID = null;
var Campaign = escape('Web Site Browse');
var baseURL = 'http://www.intelliclicksoftware.net/ClickTrack2/click.aspx?ActionType=CreateHistory&EntryType=Browse&CustomerID=' + escape(CustomerID) + '&UserID=CLICKS';

if (document.addEventListener) {
	document.addEventListener("DOMContentLoaded", pageLoaded, false);
} else {
	//alert('attaching the onLoad event');
	window.attachEvent('onload', pageLoaded);
}

function trackURL(URL) {
	if (useDebug) { 
		debug = document.getElementById('openTrackDebug'); 
	}
	if (useDebug) { 
		debug.innerHTML = debug.innerHTML + 'trackURL called.<BR>'; 
	}
	var ICData = getCookie('ICData');
	var ICCampaignData = getCookie('ICCampaignData');
	var CID = null;
	var EMail = getCookie('ICEmail');
	var Name = getCookie('ICName');
	var NoHistory = getCookie('ICNoHistory');
	if (ICData == null) {
		if (useDebug) { 
			debug.innerHTML = debug.innerHTML + 'Getting CID from URL.<BR>'; 
		}
		if (queryString('CID') != 'false' && queryString('CID') != null && queryString('CID') != false) { 
			CID = queryString('CID'); 
			setCookie('ICData', CID, 30, '/'); 
		}
	} else {
		if (queryString('CID') != 'false' && queryString('CID') != null && queryString('CID') != false) { 
			if (useDebug) { 
				debug.innerHTML = debug.innerHTML + 'Getting CID from URL despite having a cookie value and resetting cookie.<BR>'; 
			} 
			CID = queryString('CID'); 
			setCookie('ICData', CID, 30, '/'); 
		} else { 
			if (useDebug) { 
				debug.innerHTML = debug.innerHTML + 'Getting CID from cookie and refreshing cookie.<BR>'; 
			} 
			CID = ICData; 
			setCookie('ICData', CID, 30, '/'); 
		}
	}
	if (NoHistory == null) {
		if (useDebug) { 
			debug.innerHTML = debug.innerHTML + 'Getting NoHistory from URL.<BR>'; 
		}
		if (queryString('NoHistory') != 'false' && queryString('NoHistory') != null && queryString('NoHistory') != false) { 
			NoHistory = queryString('NoHistory'); 
			setCookie('ICNoHistory', NoHistory, 30, '/'); 
		}
	} else {
		if (queryString('NoHistory') != 'false' && queryString('NoHistory') != null && queryString('NoHistory') != false) { 
			if (useDebug) { 
				debug.innerHTML = debug.innerHTML + 'Getting NoHistory from URL despite having a cookie value and resetting cookie.<BR>'; 
			} 
			NoHistory = queryString('NoHistory'); 
			setCookie('ICNoHistory', NoHistory, 30, '/'); 
		} else { 
			if (useDebug) { 
				debug.innerHTML = debug.innerHTML + 'Using NoHistory from cookie and refreshing cookie.<BR>'; 
			} 
			setCookie('ICNoHistory', NoHistory, 30, '/'); 
		}
	}
	if (ICCampaignData == null) {
		if (useDebug) { 
			debug.innerHTML = debug.innerHTML + 'Getting Campaign from URL.<BR>'; 
		}
		if (queryString('Campaign') != 'false' && queryString('Campaign') != null && queryString('Campaign') != false) { 
			Campaign = queryString('Campaign');
			setCookie('ICCampaignData', Campaign, 30, '/'); 
		}
	} else {
		if (queryString('Campaign') != 'false' && queryString('Campaign') != null && queryString('Campaign') != false) { 
			if (useDebug) { 
				debug.innerHTML = debug.innerHTML + 'Getting Campaign from URL despite having a cookie value and resetting cookie.<BR>'; 
			} 
			Campaign = queryString('Campaign'); 
			setCookie('ICCampaignData', Campaign, 30, '/'); 
		} else { 
			if (useDebug) { 
				debug.innerHTML = debug.innerHTML + 'Getting Campaign from cookie and refreshing cookie.<BR>'; 
			} 
			Campaign = ICCampaignData; 
			setCookie('ICCampaignData', Campaign, 30, '/'); 
		}
	}
	if (EMail == null) {
		if (useDebug) { 
			debug.innerHTML = debug.innerHTML + 'Getting EMail from URL.<BR>'; 
		}
		if (queryString('Email') != 'false' && queryString('Email') != null && queryString('Email') != false) { 
			EMail = queryString('Email'); 
			setCookie('ICEmail', EMail, 1); 
		}
	} else {
		if (queryString('Email') != 'false' && queryString('Email') != null && queryString('Email') != false) { 
			if (useDebug) { 
				debug.innerHTML = debug.innerHTML + 'Getting EMail from URL despite having a cookie value and resetting cookie.<BR>'; 
			} 
			EMail = queryString('Email'); 
			setCookie('ICEMail', EMail, 30, '/'); 
		} else { 
			if (useDebug) { 
				debug.innerHTML = debug.innerHTML + 'Getting EMail from cookie and refreshing cookie.<BR>'; 
			} 
			setCookie('ICEMail', EMail, 30, '/'); 
		}
	}
	if (Name == null) {
		if (useDebug) { 
			debug.innerHTML = debug.innerHTML + 'Getting Name from URL.<BR>'; 
		}
		if (queryString('Name') != 'false' && queryString('Name') != null && queryString('Name') != false) { 
			Name = queryString('Name'); 
			setCookie('ICName', Name, 1); 
		}
	} else {
		if (queryString('Name') != 'false' && queryString('Name') != null && queryString('Name') != false) { 
			if (useDebug) { 
				debug.innerHTML = debug.innerHTML + 'Getting Name from URL despite having a cookie value and resetting cookie.<BR>'; 
			} 
			Name = queryString('Name'); 
			setCookie('ICName', Name, 1); 
		} else { 
			if (useDebug) { 
				debug.innerHTML = debug.innerHTML + 'Getting Name from cookie and refreshing cookie.<BR>'; 
			} 
			setCookie('ICName', Name, 1); 
		}
	}
	if (useDebug) { debug.innerHTML = debug.innerHTML + 'CID:  ' + CID + '<BR>'; }
	if (CID != null && CID != false && CID != 'false') {
		var url = baseURL + '&ParentRecordID=' + escape(CID) + '&URL=' + escape(URL) + '&Campaign=' + escape(Campaign) + '&Name=' + escape(Name) + '&Email=' + escape(EMail) + '&NoHistory=' + escape(NoHistory);
		if (useDebug) { debug.innerHTML = debug.innerHTML + 'Submitting iFrame request to URL <a href="' + url + '">' + url + '</a><BR>'; }
		callToServer(url);
		window.location = url;
	}
}

function  pageLoaded() {
	if (alreadyLoaded == false) {
		alreadyLoaded = true;
		//alert('pageLoaded entered');
		if (getCookie('lastURL') != location.protocol + '//' + location.host + location.pathname) {
			if (useDebug) { 
				debug = document.getElementById('openTrackDebug'); 
			}
			if (useDebug) { 
				debug.innerHTML = debug.innerHTML + 'Page Loaded<BR>'; 
			}
			var ICData = getCookie('ICData');
			var ICCampaignData = getCookie('ICCampaignData');
			var CID = null;
			var EMail = getCookie('ICEmail');
			var Name = getCookie('ICName');
			var NoHistory = getCookie('ICNoHistory');
			if (ICData == null) {
				if (useDebug) { 
					debug.innerHTML = debug.innerHTML + 'Getting CID from URL.<BR>'; 
				}
				if (queryString('CID') != 'false' && queryString('CID') != null && queryString('CID') != false) { 
					CID = queryString('CID'); 
					setCookie('ICData', CID, 30, '/'); 
				}
			} else {
				if (queryString('CID') != 'false' && queryString('CID') != null && queryString('CID') != false) { 
					if (useDebug) { 
						debug.innerHTML = debug.innerHTML + 'Getting CID from URL despite having a cookie value and resetting cookie.<BR>'; 
					} 
					CID = queryString('CID'); 
					setCookie('ICData', CID, 30, '/'); 
				} else { 
					if (useDebug) { 
						debug.innerHTML = debug.innerHTML + 'Getting CID from cookie and refreshing cookie.<BR>'; 
					} 
					CID = ICData; 
					setCookie('ICData', CID, 30, '/'); 
				}
			}
			if (NoHistory == null) {
				if (useDebug) { 
					debug.innerHTML = debug.innerHTML + 'Getting NoHistory from URL.<BR>'; 
				}
				if (queryString('NoHistory') != 'false' && queryString('NoHistory') != null && queryString('NoHistory') != false) { 
					NoHistory = queryString('NoHistory'); 
					setCookie('ICNoHistory', NoHistory, 30, '/'); 
				}
			} else {
				if (queryString('NoHistory') != 'false' && queryString('NoHistory') != null && queryString('NoHistory') != false) { 
					if (useDebug) { 
						debug.innerHTML = debug.innerHTML + 'Getting NoHistory from URL despite having a cookie value and resetting cookie.<BR>'; 
					} 
					NoHistory = queryString('NoHistory'); 
					setCookie('ICNoHistory', NoHistory, 30, '/'); 
				} else { 
					if (useDebug) { 
						debug.innerHTML = debug.innerHTML + 'Using NoHistory from cookie and refreshing cookie.<BR>'; 
					} 
					setCookie('ICNoHistory', NoHistory, 30, '/'); 
				}
			}
			if (ICCampaignData == null) {
				if (useDebug) { 
					debug.innerHTML = debug.innerHTML + 'Getting Campaign from URL.<BR>'; 
				}
				if (queryString('Campaign') != 'false' && queryString('Campaign') != null && queryString('Campaign') != false) { 
					Campaign = queryString('Campaign');
					setCookie('ICCampaignData', Campaign, 30, '/'); 
				}
			} else {
				if (queryString('Campaign') != 'false' && queryString('Campaign') != null && queryString('Campaign') != false) { 
					if (useDebug) { 
						debug.innerHTML = debug.innerHTML + 'Getting Campaign from URL despite having a cookie value and resetting cookie.<BR>'; 
					} 
					Campaign = queryString('Campaign'); 
					setCookie('ICCampaignData', Campaign, 30, '/'); 
				} else { 
					if (useDebug) { 
						debug.innerHTML = debug.innerHTML + 'Getting Campaign from cookie and refreshing cookie.<BR>'; 
					} 
					Campaign = ICCampaignData; 
					setCookie('ICCampaignData', Campaign, 30, '/'); 
				}
			}
			if (EMail == null) {
				if (useDebug) { 
					debug.innerHTML = debug.innerHTML + 'Getting EMail from URL.<BR>'; 
				}
				if (queryString('Email') != 'false' && queryString('Email') != null && queryString('Email') != false) { 
					EMail = queryString('Email'); 
					setCookie('ICEmail', EMail, 30, '/'); 
				}
			} else {
				if (queryString('Email') != 'false' && queryString('Email') != null && queryString('Email') != false) { 
					if (useDebug) { 
						debug.innerHTML = debug.innerHTML + 'Getting EMail from URL despite having a cookie value and resetting cookie.<BR>'; 
					} 
					EMail = queryString('Email'); 
					setCookie('ICEMail', EMail, 30, '/'); 
				} else { 
					if (useDebug) { 
						debug.innerHTML = debug.innerHTML + 'Getting EMail from cookie and refreshing cookie.<BR>'; 
					} 
					setCookie('ICEMail', EMail, 30, '/'); 
				}
			}
			if (Name == null) {
				if (useDebug) { 
					debug.innerHTML = debug.innerHTML + 'Getting Name from URL.<BR>'; 
				}
				if (queryString('Name') != 'false' && queryString('Name') != null && queryString('Name') != false) { 
					Name = queryString('Name'); 
					setCookie('ICName', Name, 30, '/'); 
				}
			} else {
				if (queryString('Name') != 'false' && queryString('Name') != null && queryString('Name') != false) { 
					if (useDebug) { 
						debug.innerHTML = debug.innerHTML + 'Getting Name from URL despite having a cookie value and resetting cookie.<BR>'; 
					} 
					Name = queryString('Name'); 
					setCookie('ICName', Name, 30, '/'); 
				} else { 
					if (useDebug) { 
						debug.innerHTML = debug.innerHTML + 'Getting Name from cookie and refreshing cookie.<BR>'; 
					} 
					setCookie('ICName', Name, 30, '/'); 
				}
			}
			if (useDebug) { debug.innerHTML = debug.innerHTML + 'CID:  ' + CID + '<BR>'; }
			if (useDebug) { debug.innerHTML = debug.innerHTML + 'Campaign:  ' + Campaign + '<BR>'; }
			if (CID != null && CID != false && CID != 'false') {
				var url = baseURL + '&ParentRecordID=' + escape(CID).replace('+', '%2B') + '&Campaign=' + escape(Campaign) + '&Name=' + escape(Name) + '&Email=' + escape(EMail) + '&NoHistory=' + escape(NoHistory) + '&URL=' + escape(location.protocol + '//' + location.host + location.pathname);
				if (useDebug) { debug.innerHTML = debug.innerHTML + 'Submitting iFrame request to URL <a href="' + url + '">' + url + '</a><BR>'; }
				setCookie('lastURL', location.protocol + '//' + location.host + location.pathname, .000057);
				callToServer(url);
			}
		}
	}
}

function callToServer(URL) {
	if (!document.createElement) {return true};
	var IFrameDoc;
	var IFrameObj;
	if (!IFrameObj && document.createElement) {
		try {
			var tempIFrame=document.createElement('iframe');
			tempIFrame.setAttribute('id','RSIFrame');
			tempIFrame.style.border='0px';
			tempIFrame.style.width='0px';
			tempIFrame.style.height='0px';
			IFrameObj = document.body.appendChild(tempIFrame);
      
			if (document.frames) {
				IFrameObj = document.frames['RSIFrame'];
			}
		} catch(exception) {
			iframeHTML='\<iframe id="RSIFrame" style="';
			iframeHTML+='border:0px;';
			iframeHTML+='width:0px;';
			iframeHTML+='height:0px;';
			iframeHTML+='"><\/iframe>';
			document.body.innerHTML+=iframeHTML;
			IFrameObj = new Object();
			IFrameObj.document = new Object();
			IFrameObj.document.location = new Object();
			IFrameObj.document.location.iframe = document.getElementById('RSIFrame');
			IFrameObj.document.location.replace = function(location) {
				this.iframe.src = location;
			}
		}
	}
  
	if (navigator.userAgent.indexOf('Gecko') !=-1 && !IFrameObj.contentDocument) {
		setTimeout('callToServer()',10);
		return false;
	}
  
	if (IFrameObj.contentDocument) {
		IFrameDoc = IFrameObj.contentDocument; 
	} else if (IFrameObj.contentWindow) {
		IFrameDoc = IFrameObj.contentWindow.document;
	} else if (IFrameObj.document) {
		IFrameDoc = IFrameObj.document;
	} else {
		return true;
	}
  
	IFrameDoc.location.replace(URL);
	return false;
}


function PageQuery(q) {
	if (q.length > 1) { this.q = q.substring(1, q.length); } else { this.q = null; }

	this.keyValuePairs = new Array();

	if (q) {
		for (var i=0; i < this.q.split("&").length; i++) {
			this.keyValuePairs[i] = this.q.split("&")[i];
		}
	}

	this.getKeyValuePairs = function() { return this.keyValuePairs; }
	this.getValue = function(s) { for (var j=0; j < this.keyValuePairs.length; j++) { if (this.keyValuePairs[j].split("=")[0] == s) { return this.keyValuePairs[j].split("=")[1]; } } return false; }
	this.getParameters = function() { var a = new Array(this.getLength()); for (var j=0; j < this.keyValuePairs.length; j++) { a[j] = this.keyValuePairs[j].split("=")[0]; } return a; }
	this.getLength = function() { return this.keyValuePairs.length; }
}

function queryString(key) {
	var page = new PageQuery(window.location.search);
	return unescape(page.getValue(key));
}
function displayItem(key) {
	if (queryString(key) == 'false') { document.write("you didn't enter a ?name=value querystring item."); } else { document.write(queryString(key)); }
}

function setCookie(name, value, expires, path, domain, secure) {
	var today = new Date();
	today.setTime(today.getTime());
	if (expires) {
		expires = expires * 1000 * 60 * 60 * 24;
	}
	var expires_date = new Date(today.getTime() + (expires));
	document.cookie = name + "=" + escape(value) + ((expires) ? ";expires=" + expires_date.toGMTString() : "") + ((path) ? ";path=" + path : "") + ((domain) ? ";domain=" + domain : "") + ((secure) ? ";secure" : "");
}

function getCookie(check_name) {
	var a_all_cookies = document.cookie.split( ';' );
	var a_temp_cookie = '';
	var cookie_name = '';
	var cookie_value = '';
	var b_cookie_found = false;
	
	for ( i = 0; i < a_all_cookies.length; i++ )
	{
		a_temp_cookie = a_all_cookies[i].split('=');
		cookie_name = a_temp_cookie[0].replace(/^\s+|\s+$/g, '');
		if (cookie_name == check_name) {
			b_cookie_found = true;
			if (a_temp_cookie.length > 1) {
				cookie_value = unescape(a_temp_cookie[1].replace(/^\s+|\s+$/g, ''));
			}
			return cookie_value;
			break;
		}
		a_temp_cookie = null;
		cookie_name = '';
	}
	if (!b_cookie_found)
	{
		return null;
	}
}

function deleteCookie(name, path, domain) {
	if (getCookie(name)) {
		document.cookie = name + "=" + ((path) ? ";path=" + path : "") + ((domain) ? ";domain=" + domain : "") + ";expires=Thu, 01-Jan-1970 00:00:01 GMT";
	}
}