<?php
header('X-UA-Compatible: IE=edge,chrome=1');
include_once 'quick_enroll.php';
include_once 'connect.php';
include_once('ENV.php');
?>
<!DOCTYPE html>
<html>
<head>
    <meta charset="utf-8">
    <title><?php echo $elearning->__('Manage your e-learning access'); ?></title>
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <meta name="description" content="Start with FS Pro 2013 and increase your productivity today">
    <meta name="keywords" content="writing technical documentation, proposal letter template, technical writing software, template for proposal, business document template, proposal software, writing software, documentation software" />
    <link href="css/bootstrap.min.css" rel="stylesheet" media="screen">
    <link rel="stylesheet" href="//maxcdn.bootstrapcdn.com/font-awesome/4.3.0/css/font-awesome.min.css">
    <link href="css/imi.css" rel="stylesheet" media="screen">
    <link rel="stylesheet" href="assets/lib/datatables/3/dataTables.bootstrap.css">
    <!-- HTML5 shim and Respond.js IE8 support of HTML5 elements and media queries -->
    <!--[if lt IE 9]>
    <script src="js/html5shiv.js"></script>
    <script src="js/respond.min.js"></script>
    <![endif]-->
    <script src="assets/lib/jquery/jquery.min.js"></script>
    <script src="assets/lib/datatables/jquery.dataTables.js"></script>
    <script src="assets/lib/datatables/3/dataTables.bootstrap.js"></script>
    <script src="js/bootstrap.min.js"></script>
</head>
<body>
<header>
    <div class="container">
        <!-- Brand and toggle get grouped for better mobile display -->
        <div class="navbar-header">
            <button type="button" class="navbar-toggle collapsed" data-toggle="collapse" data-target="#bs-example-navbar-collapse-1" aria-expanded="false">
                <span class="sr-only">Toggle navigation</span>
                <span class="icon-bar"></span>
                <span class="icon-bar"></span>
                <span class="icon-bar"></span>
            </button>
            <a class="navbar-brand" href="../" title="Home">
                <img alt="Logo" src="../images/logo.gif">
            </a>
        </div>
        <!-- Collect the nav links, forms, and other content for toggling -->
        <div class="collapse navbar-collapse" id="bs-example-navbar-collapse-1">
            <ul class="nav navbar-nav navbar-right">
                <!--<li><a href="../reporting-new/elearning/extend_enrollments.php" class="submit-a-request">Extend Enrollment</a></li>-->
            </ul>
        </div><!-- /.navbar-collapse -->
    </div>
</header>
<?php

if($_POST['validate'])
{
    $pass = isset($_POST['pass']) ? trim($_POST['pass']) : '';
    $_SESSION['wachtwoord'] = $pass;
}
if(empty($_SESSION['wachtwoord']))
{
    include('login.php');
}elseif(!empty($_SESSION['wachtwoord']) && $_SESSION['wachtwoord'] != $password)
{
    include('loginIncorrect.php');
}

if($_SESSION['wachtwoord'] == $password)
{

    ?>
    <div class="container content">
        <div class="row">
            <div class="col-sm-12">

                <?php if(!empty($session_messages)) : ?>
                    <?php foreach($session_messages as $session_message) : ?>
                        <?php if($session_message[0] == 'error') : ?>
                            <div class="alert alert-warning" role="alert">
                                <?php echo $elearning->__($session_message[1]); ?>
                            </div>
                        <?php else: ?>
                            <div class="alert alert-success" role="alert">
                                <?php echo $elearning->__($session_message[1]); ?>
                            </div>
                        <?php endif; ?>
                    <?php endforeach; ?>
                <?php endif; ?>

                <?php if($error == 'order_unknown') : ?>
                    <div class="alert alert-warning" role="alert">
                        <?php echo $elearning->__('This order is not known.'); ?>
                    </div>

                <?php elseif($error == 'order_not_complete') : ?>
                    <div class="alert alert-warning" role="alert">
                        <?php echo $elearning->__('Please come back to this page once payment has been completed.'); ?>
                    </div>

                <?php elseif($error == 'order_no_slots') : ?>
                    <div class="alert alert-warning" role="alert">
                        <?php echo $elearning->__('Please add the number of desired slots.'); ?>
                    </div>

                <?php else: ?>
                <div class="row">
                    <div class="col-sm-12" id="toptitle">
                        <h1>Add Student Slots</h1>
                    </div>
                </div>
                <div class="container">
                    <form class="form-horizontal" action="<?php echo $_SERVER['REQUEST_URI']; ?>" method="post">
                        <div class="form-group">
                            <label for="newslots" class="col-sm-2 control-label">Number of Students :</label>
                            <div class="col-sm-4">
                                <input type="text" class="form-control" id="newslots" name="newslots" required>
                            </div>
                        </div>
                        <div class="form-group">
                            <label for="elearning_products" class="col-sm-2 control-label">Elearning course :</label>
                            <div class="col-sm-4">
                                <select name="products" id="products" class="form-control">
                                    <?php
                                    $query = mysql_query("SELECT * FROM elearning_products order by id asc" , $db);
                                    while($row = mysql_fetch_array($query))
                                    {
                                        echo "<option value='" . $row['id'] . "'>" . $row['product_title'] . "</option>";
                                    }
                                    ?>
                                </select>
                            </div>
                        </div>
                        <div class="form-group">
                            <div class="col-sm-offset-2 col-sm-10">
                                <button type="submit" name="save" class="btn btn-blue">Add E-learning slots</button>
                            </div>
                        </div>
                    </form>
                </div>

                <div class="row">
                    <div class="col-sm-12" id="toptitle">
                        <h1>Enroll Students To Course</h1>
                    </div>
                </div>
                <div class="container">

                    <?php if(empty($product)): ?>
                        <?php $slots = $elearning->getEnrollmentPurchasedSlotsByID($order);

                        $importedSlots = (isset($_SESSION['enroll_slots_imported'])) ? $_SESSION['enroll_slots_imported'] : null;
                        if(!empty($importedSlots))
                        {
                            $i = 0;
                            foreach ($slots as $slotId => $slot)
                            {
                                $email = trim($importedSlots[$i][0]);
                                $firstname = trim($importedSlots[$i][1]);
                                $lastname = trim($importedSlots[$i][2]);
                                $lastname = preg_replace('/\;$/', '', $lastname);
                                if ($slot->getLocked() == 0 && !empty($email))
                                {
                                    $slot->setCustomerEmail($email);
                                    $slot->setCustomerFirstname($firstname);
                                    $slot->setCustomerLastname($lastname);
                                }
                                $i ++;
                            }
                            unset($_SESSION['enroll_slots_imported']);
                        }

                        ?>
                        <form class="form-horizontal" action="<?php echo $_SERVER['REQUEST_URI']; ?>" method="post">
                            <table class="table table-bordered table-condensed table-hover" id="dataTables-slots">
                                <thead>
                                <tr>
                                    <th><?php echo $elearning->__('Email'); ?></th>
                                    <th><?php echo $elearning->__('Firstname'); ?></th>
                                    <th><?php echo $elearning->__('Lastname'); ?></th>
                                    <th><?php echo $elearning->__('Course'); ?></th>
                                    <th><?php echo $elearning->__('Remove'); ?></th>
                                </tr>
                                </thead>
                                <tbody>
                                <?php $locked = true; ?>
                                <?php foreach($slots as $slot): ?>
                                    <?php $customer = Mage::getModel('customer/customer')->load($slot->getCustomerId()); ?>
                                    <?php $prodID =$slot->getProductID(); ?>
                                    <?php if($slot->getLocked() == 0) $locked = false; ?>
                                    <tr>
                                        <td>
                                            <?php if($slot->getLocked() == 1) : ?>
                                                <?php echo $slot->getCustomerEmail(); ?>
                                            <?php else: ?>
                                                <input type="email" class="form-control" name="slot[<?php echo $slot->getId(); ?>][email]" value="<?php echo $slot->getCustomerEmail(); ?>" />
                                            <?php endif; ?>
                                        </td>
                                        <td>
                                            <?php if($slot->getLocked() == 1) : ?>
                                                <?php echo $slot->getCustomerFirstname(); ?>
                                            <?php else: ?>
                                                <input type="input" class="form-control" name="slot[<?php echo $slot->getId(); ?>][firstname]" value="<?php echo $slot->getCustomerFirstname(); ?>" />
                                            <?php endif; ?>
                                        </td>
                                        <td>
                                            <?php if($slot->getLocked() == 1) : ?>
                                                <?php echo $slot->getCustomerLastname(); ?>
                                            <?php else: ?>
                                                <input type="input" class="form-control" name="slot[<?php echo $slot->getId(); ?>][lastname]" value="<?php echo $slot->getCustomerLastname(); ?>" />
                                            <?php endif; ?>
                                        </td>
                                        <td>
                                            <?php if($slot->getLocked() == 1) : ?>
                                                <?php echo $elearning->getEnrollmentProductTitle($prodID); ?>
                                            <?php else: ?>
                                                <?php echo $elearning->getEnrollmentProductTitle($prodID); ?>
                                            <?php endif; ?>
                                        </td>
                                        <td class="center">
                                            <input type="checkbox" value="<?php echo $slot->getID();  ?>" name="deleteCheck[]" >
                                        </td>
                                    </tr>
                                <?php endforeach; ?>
                                </tbody>
                            </table>
                            <div class="center form-group">
                                <p class="col-sm-offset-4 col-sm-4">Date format: mm/dd/yyy <br> Example: 12/30/2015</p>
                            </div>
                            <div class="form-group period">
                                <label for="period" class="col-sm-offset-1 col-sm-4 control-label">Choose Expiry date :</label>
                                <div class="col-sm-4">
                                    <input type="date" class="form-control" id="period" name="period">
                                </div>
                            </div>
                            <?php if($locked == false) : ?>
                                <div class="form-group">
                                    <div class="col-sm-offset-5 col-sm-2">
                                        <input type="submit" value="Enroll now!" name="enroll" class="btn btn-blue" />
                                    </div>
                                    <div class="col-sm-offset-3 col-sm-2">
                                        <input type="submit" value="Remove selected" name="remove" class="btn btn-default" />
                                    </div>
                                </div>

                            <?php endif; ?>
                        </form>
                    <?php endif; ?>
                    <?php endif; ?>
                </div>

                <?php if($locked == false && $upload == true && count($slots) >= $uploadSlots) : ?>
                    <div class="row">
                        <div class="col-sm-12" id="toptitle">
                            <h1>Upload Using CSV</h1>
                        </div>
                    </div>
                    <div class="container">
                        <form class="form-horizontal" action="<?php echo $_SERVER['REQUEST_URI']; ?>" method="post" enctype="multipart/form-data">
                            <div class="form-group center">
                                <p class="col-sm-offset-4 col-sm-4">Please upload a CSV with the following format: <br />"john@example.com","John","Doe"</p>
                            </div>
                            <div class="form-group ">
                                <label class="col-sm-offset-3 col-sm-2 control-label" for="importfile"><?php echo $elearning->__('CSV import :'); ?></label>
                                <div class="col-sm-4">
                                    <input type="file" name="importfile" id="importfile" />
                                </div>
                            </div>
                            <div class="form-group">
                                <div class="col-sm-offset-5 col-sm-7">
                                    <input type="submit" class="btn btn-blue" value="Import" name="import" />
                                </div>
                            </div>
                        </form>
                    </div>
                <?php endif; ?>
            </div>
        </div>

        <script>
            $(document).ready(function() {
                $('#dataTables-slots').dataTable();
            });
        </script>
    </div>
<?php } ?>
<footer class="row">
    <div class="col-sm-12" id="footertext">
        <p class="imi-footer">&copy; <?php echo date("Y"); ?> Information Mapping International</p>
        <p class="imi-footer icons"> <a href="https://twitter.com/infomap"><i class="fa fa-twitter-square fa-lg"></i></a> <a href="https://www.facebook.com/iminv"><i class="fa fa-facebook-official fa-lg"></i></a> <a href="http://www.linkedin.com/groups?gid=983927"><i class="fa fa-linkedin-square fa-lg"></i></a> <a href="http://www.youtube.com/user/InformationMapping"><i class="fa fa-youtube-square fa-lg"></i></a> </p>
    </div>
</footer>
</body>
</html>
