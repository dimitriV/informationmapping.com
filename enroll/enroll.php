<?php
ini_set('display_errors', 1);
session_start();
// Check GET variables
$storeId = (isset($_GET['s'])) ? (int)$_GET['s'] : 0;
$orderId = (isset($_GET['o'])) ? base64_decode($_GET['o']) : null;

if($_SERVER['REMOTE_ADDR'] == '84.86.237.157') {
    $orderId = 500004186;
    $storeId = 5;
}

// Instantiante Magento
require_once '../magento/app/Mage.php';

require_once '../magento/app/code/local/Storefront/VendorLoader/Model/Loader.php';
Storefront_VendorLoader_Model_Loader::enableLoader();

Mage::app();

// Check for valid store
try {
    $store = Mage::app()->getStore($storeId)->getCode();
    Mage::app()->setCurrentStore($storeId);
} catch(Exception $e) {}

// Error handling
$error = null;
$order = Mage::getModel('sales/order')->loadByIncrementId($orderId);
if(!$order->getId() > 0) {
    $error = 'order_unknown';

// Check for the order state
} elseif($order->getState() != Mage_Sales_Model_Order::STATE_COMPLETE) {
    $error = 'order_not_complete';

} else {

    // Init enrolment
    Mage::helper('elearning')->initEnrollment($order);

    // Check for the purchased slots
    $slots = Mage::helper('elearning')->getEnrollmentPurchasedSlots($order);
    if(empty($slots)) {
        $error = 'order_no_slots';
    }
}

// Other variables
$elearning = Mage::helper('elearning');
$products = $elearning->getOrderElearningProducts($order);

// Upload functionality
$upload = true;
$uploadSlots = 10;
$mimes = array('application/vnd.ms-excel','text/plain','text/csv','text/tsv');

if($upload && !empty($_FILES)) {
    $importError = null;

    if (in_array($_FILES['importfile']['error'], array(UPLOAD_ERR_INI_SIZE, UPLOAD_ERR_FORM_SIZE))) {
        $importError = 'Imported file is too large';

    } elseif (in_array($_FILES['importfile']['error'], array(UPLOAD_ERR_PARTIAL, UPLOAD_ERR_CANT_WRITE, UPLOAD_ERR_NO_TMP_DIR))) {
        $importError = 'File could not be imported';

    } elseif($_FILES['importfile']['error'] != UPLOAD_ERR_NO_FILE) {

        if(!is_readable($_FILES['importfile']['tmp_name'])) {
            $importError = 'Imported file could not be read';

        } elseif(empty($_FILES['importfile']['size'])) {
            $importError = 'Imported file has no size';

        } elseif(!in_array($_FILES['importfile']['type'],$mimes)) {
            $importError = 'Imported file is not a CSV file';

        } else {
            if (($handle = fopen($_FILES['importfile']['tmp_name'], 'r')) !== FALSE) {

                $importLines = array();
                while (($line = fgetcsv($handle, 1000, ',')) !== FALSE) {
                    $importLines[] = $line;
                }

                if(empty($importLines)) {
                    $importError = 'Could not import CSV lines';
                } else {
                    $_SESSION['enroll_slots_imported'] = $importLines;
                }

                $elearning->setMessage('notice', 'CSV lines imported');
                session_write_close();
                header('Location: '.$_SERVER['REQUEST_URI']);
                exit;

            } else {
                $importError = 'Imported file has no content';
            }
        }
    }

    if(!empty($importError)) {
        $elearning->setMessage('error', $importError);
        session_write_close();
        header('Location: '.$_SERVER['REQUEST_URI']);
        exit;
    }
}

// Check for other POST-data
if(empty($error) && !empty($_POST['slot'])) {
    $rt = $elearning->postEnrollmentPurchasedSlots($_POST['slot'], $order);
    if($rt == true) $elearning->setMessage('success', 'Enrollment saved successfully.');
    session_write_close();
    header('Location: '.$_SERVER['REQUEST_URI']);
    exit;
}

// Session messages
$session_messages = array();
$password_links = array();

// Add the enroll message
if(!empty($_SESSION['enroll_messages'])) {
    foreach($_SESSION['enroll_messages'] as $session_message) {
        $session_messages[] = $session_message;
    }
    unset($_SESSION['enroll_messages']);
}

// Add the password link message
if(!empty($_SESSION['password_link'])) {
    foreach($_SESSION['password_link'] as $session_password_link) {
        $password_links[] = $session_password_link;
    }
    unset($_SESSION['password_link']);
}

// Continue to index.php
