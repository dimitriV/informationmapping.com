<?php include_once('ENV.php'); ?>
<div class="container">
    <div class="row">
        <div class="col-sm-12">
            <h1><?= $title ?> Back-End Application</h1>
        </div>
    </div>
    <form class="form-horizontal" role="form" action="" method="post">
        <div class="form-group">
            <div class="col-sm-6">
                <label for="pass">Enter your password please:</label>
                <div class="input-group">
                    <span class="input-group-addon"><i class="fa fa-key fa-fw"></i></span>
                    <input class="form-control" type="password" placeholder="Password" name="pass" >
                </div>
            </div>
        </div>
            <div class="form-group">
                <div class="col-sm-6">
            <div class="alert alert-danger" role="alert">
                <span class="sr-only">Error:</span>
Invalid Password, please try again
</div>
                    </div>
                </div>

        <div class="form-group">
            <div class="col-sm-6">
                <button type="submit" class="btn btn-primary" name="validate" value="Login">Login!</button>
            </div>
        </div>
    </form>
</div>