<?php
header('X-UA-Compatible: IE=edge,chrome=1');
include_once 'enroll.php';
?>
<!DOCTYPE html>
<html>
<head>
    <meta charset="utf-8">
    <title><?php echo $elearning->__('Manage your e-learning access'); ?></title>
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <meta name="description" content="Start with FS Pro 2013 and increase your productivity today">
    <meta name="keywords" content="writing technical documentation, proposal letter template, technical writing software, template for proposal, business document template, proposal software, writing software, documentation software" />
    <link href="css/bootstrap.min.css" rel="stylesheet" media="screen">
    <link rel="stylesheet" href="//maxcdn.bootstrapcdn.com/font-awesome/4.3.0/css/font-awesome.min.css">
    <link rel="stylesheet" href="assets/lib/datatables/3/dataTables.bootstrap.css">
    <link rel="stylesheet" type="text/css" href="/enroll/css/sweetalert.css">
    <link href="css/imi.css" rel="stylesheet" media="screen">
    <!-- HTML5 shim and Respond.js IE8 support of HTML5 elements and media queries -->
    <!--[if lt IE 9]>
    <script src="js/html5shiv.js"></script>
    <script src="js/respond.min.js"></script>
    <![endif]-->
    <script src="/enroll/js/sweetalert.min.js"></script>
    <script src="assets/lib/jquery/jquery.min.js"></script>
    <script src="assets/lib/datatables/jquery.dataTables.js"></script>
    <script src="assets/lib/datatables/3/dataTables.bootstrap.js"></script>
    <script src="/enroll/js/bootstrap.min.js"></script>
</head>
<body>

<header>
    <div class="container">
        <!-- Brand and toggle get grouped for better mobile display -->
        <div class="navbar-header">
            <button type="button" class="navbar-toggle collapsed" data-toggle="collapse" data-target="#bs-example-navbar-collapse-1" aria-expanded="false">
                <span class="sr-only">Toggle navigation</span>
                <span class="icon-bar"></span>
                <span class="icon-bar"></span>
                <span class="icon-bar"></span>
            </button>
            <a class="navbar-brand" href="../" title="Home">
                <img alt="Logo" src="../images/logo.gif">
            </a>
        </div>
        <!-- Collect the nav links, forms, and other content for toggling -->
        <div class="collapse navbar-collapse" id="bs-example-navbar-collapse-1">
            <ul class="nav navbar-nav navbar-right">
                <!--<li><a href="../reporting-new/elearning/extend_enrollments.php" class="submit-a-request">Extend Enrollment</a></li>-->
            </ul>
        </div><!-- /.navbar-collapse -->
    </div>
</header>
<div class="container">
    <div class="row">
        <div class="col-sm-12" id="toptitle">
            <h1>Student Manager</h1>
        </div>
    </div>
    <div class="row">
        <div class="col-sm-12">

            <?php if(!empty($session_messages)) : ?>
                <?php foreach($session_messages as $session_message) : ?>
                    <?php if($session_message[0] == 'error') : ?>
                        <div class="alert alert-warning" role="alert">
                            <?php echo $elearning->__($session_message[1]); ?>
                        </div>
                    <?php elseif($session_message[0] == 'success'): ?>
                        <div class="alert alert-success" role="alert">
                            <?php echo $elearning->__($session_message[1]); ?>
                        </div>
                    <?php endif; ?>
                <?php endforeach; ?>
            <?php endif; ?>

            <?php if($error == 'order_unknown') : ?>
                <div class="alert alert-warning" role="alert">
                    <?php echo $elearning->__('This order is not known.'); ?>
                </div>

            <?php elseif($error == 'order_not_complete') : ?>
                <div class="alert alert-warning" role="alert">
                    <?php echo $elearning->__('Please come back to this page once payment has been completed.'); ?>
                </div>

            <?php elseif($error == 'order_no_slots') : ?>
                <div class="alert alert-warning" role="alert">
                    <?php echo $elearning->__('This order contains no available e-learning slots.'); ?>
                </div>

            <?php else: ?>
                <p><?php echo $elearning->__('Please complete the form below to define who should get access to the e-learning course(s) or download(s). The end users will be automatically notified by email.'); ?></p>
                <?php foreach($products as $product) : ?>
                    <?php $slots = $elearning->getEnrollmentPurchasedSlotsByProduct($order, $product); ?>
                    <?php if(empty($slots)) continue; ?>
                    <h2><?php echo $product->getName(); ?> - #<?php echo $order->getIncrementId(); ?></h2>
                    <form action="<?php echo $_SERVER['REQUEST_URI']; ?>" method="post">
                        <table width="100%">
                            <thead>
                            <tr>
                                <th><div class="col-md-12"><?php echo $elearning->__('Email'); ?></div></th>
                                <th><div class="col-md-12"><?php echo $elearning->__('Firstname'); ?></div></th>
                                <th><div class="col-md-12"><?php echo $elearning->__('Lastname'); ?></div></th>
                            </tr>
                            </thead>
                            <tbody>
                            <?php $locked = true; ?>
                            <?php foreach($slots as $slot): ?>
                                <?php $customer = Mage::getModel('customer/customer')->load($slot->getCustomerId()); ?>
                                <?php if($slot->getLocked() == 0) $locked = false; ?>
                                <tr>
                                    <td>
                                        <?php if($slot->getLocked() == 1) : ?>
                                            <div class="col-md-12">
                                                <input type="input" class="form-control locked" disabled="disabled" value="<?php echo $slot->getCustomerEmail(); ?>" />
                                            </div>
                                        <?php else: ?>
                                            <div class="col-md-12">
                                                <input type="email" class="form-control" name="slot[<?php echo $slot->getId(); ?>][email]" value="<?php echo $slot->getCustomerEmail(); ?>" />
                                            </div>
                                        <?php endif; ?>
                                    </td>
                                    <td>
                                        <?php if($slot->getLocked() == 1) : ?>
                                            <div class="col-md-12">
                                                <input type="input" class="form-control locked" disabled="disabled" value="<?php echo $slot->getCustomerFirstname(); ?>" />
                                            </div>
                                        <?php else: ?>
                                            <div class="col-md-12">
                                                <input type="input" class="form-control" name="slot[<?php echo $slot->getId(); ?>][firstname]" value="<?php echo $slot->getCustomerFirstname(); ?>" />
                                            </div>
                                        <?php endif; ?>
                                    </td>
                                    <td>
                                        <?php if($slot->getLocked() == 1) : ?>
                                            <div class="col-md-12">
                                                <input type="input" class="form-control locked" disabled="disabled" value="<?php echo $slot->getCustomerLastname(); ?>" />
                                            </div>
                                        <?php else: ?>
                                            <div class="col-md-12">
                                                <input type="input" class="form-control" name="slot[<?php echo $slot->getId(); ?>][lastname]" value="<?php echo $slot->getCustomerLastname(); ?>" />
                                            </div>
                                        <?php endif; ?>
                                    </td>
                                </tr>
                            <?php endforeach; ?>
                            </tbody>
                        </table>

                        <?php if($locked == false) : ?>
                            <br>
                            <p>
                                <input type="submit" class="btn btn-blue" value="Enroll now!" name="enroll" />
                            </p>
                        <?php endif; ?>
                    </form>

                    <?php if($locked == false && $upload == true && count($slots) >= $uploadSlots) : ?>
                        <p>Please upload a CSV with the following format: <br />"john@example.com","John","Doe"<br />
                            "jane@example.com","Jane","Smith"</p>
                        <form action="<?php echo $_SERVER['REQUEST_URI']; ?>" method="post" enctype="multipart/form-data">
                            <p>
                                <label for="importfile"><?php echo $elearning->__('CSV import'); ?></label>
                                <input type="file" name="importfile" id="importfile" />
                            </p>
                            <p>
                                <input type="submit" value="Import" name="import" />
                            </p>
                        </form>
                    <?php endif; ?>
                <?php endforeach; ?>
            <?php endif; ?>
        </div>
    </div>

    <div class="row">
        <div class="col-sm-6">
            <h3>Frequently Asked Questions</h3>
            <div class="panel-group" id="accordion">
                <div class="panel panel-default">
                    <div class="panel-heading">
                        <h4 class="panel-title">
                            <a data-toggle="collapse" data-parent="#accordion" href="#collapseOne">
                                Do I have to enter information for all users right away?
                            </a>
                        </h4>
                    </div>
                    <div id="collapseOne" class="panel-collapse collapse">
                        <div class="panel-body">
                            No. If you wish, you can give access to some users now by entering their information (including email address, first name and last name) and enter information for additional users later. (For example, if you have purchased e-learning for 5 users you can get immediate access for 2 of them by entering their information now, and you can visit this page again later to enter information for the other 3 users.

                        </div>
                    </div>
                </div>
                <div class="panel panel-default">
                    <div class="panel-heading">
                        <h4 class="panel-title">
                            <a data-toggle="collapse" data-parent="#accordion" href="#collapseTwo">
                                Can I use dummy names and email addresses?
                            </a>
                        </h4>
                    </div>
                    <div id="collapseTwo" class="panel-collapse collapse">
                        <div class="panel-body">
                            No. You must always use real names and email addresses. This is because if you provided a dummy name, that name would appear on the certificate of completion of the course. If you provided a dummy email address, the email with the login instructions could not be delivered.
                        </div>
                    </div>
                </div>
                <div class="panel panel-default">
                    <div class="panel-heading">
                        <h4 class="panel-title">
                            <a data-toggle="collapse" data-parent="#accordion" href="#collapseThree">
                                Can I change a user's details once they have been saved?
                            </a>
                        </h4>
                    </div>
                    <div id="collapseThree" class="panel-collapse collapse">
                        <div class="panel-body">
                            No. Once you have clicked on the Save button, a user's details cannot be changed. If you have made an error, contact us at <a href="mailto:info@informationmapping.com">info@informationmapping.com</a> to update the record.
                        </div>
                    </div>
                </div>
                <div class="panel panel-default">
                    <div class="panel-heading">
                        <h4 class="panel-title">
                            <a data-toggle="collapse" data-parent="#accordion" href="#collapseFour">
                                How can I get back to this page?
                            </a>
                        </h4>
                    </div>
                    <div id="collapseFour" class="panel-collapse collapse">
                        <div class="panel-body">
                            On your order confirmation email,  you will find a hyperlink to this page. You can also bookmark this page in your browser for easy retrieval.
                        </div>
                    </div>
                </div>
                <div class="panel panel-default">
                    <div class="panel-heading">
                        <h4 class="panel-title">
                            <a data-toggle="collapse" data-parent="#accordion" href="#collapseFive">
                                How long is the access period for end users?
                            </a>
                        </h4>
                    </div>
                    <div id="collapseFive" class="panel-collapse collapse">
                        <div class="panel-body">
                            End user access is valid for 90 days.
                        </div>
                    </div>
                </div>
                <div class="panel panel-default">
                    <div class="panel-heading">
                        <h4 class="panel-title">
                            <a data-toggle="collapse" data-parent="#accordion" href="#collapseSix">
                                When does the access period begin?
                            </a>
                        </h4>
                    </div>
                    <div id="collapseSix" class="panel-collapse collapse">
                        <div class="panel-body">
                            End user access begins on the date when the user's details are saved on this page. If you save multiple end users on multiple dates, each user will have a different start date.
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>
<?php if(!empty($session_messages)) :
    foreach($session_messages as $session_message) :
        if($session_message[0] == 'count') :
            $count = $session_message[1];
        endif;
    endforeach;
endif; ?>


<?php if($locked == true && $count == 1 ) : ?>
    <script type="text/javascript">
        swal({
            title: "To access your materials, proceed as follows:",
            text: "<div class='left'><ol><li> Go to <a href='http://www.informationmapping.com/en/im-academy' target='_blank' >http://www.informationmapping.com/en/im-academy</a></li><li> Log in with the following credentials:<hr><p>E-mail Address: <b><?php echo $slot->getCustomerEmail(); ?></b></p><br><p>Password: <?php if(!empty($password_links)) : foreach($password_links as $session_password_link) :  ?><a href='<?php echo $elearning->__($session_password_link[1]); ?>' target='_blank'' >Choose your password</a><?php endforeach; else: ?><b>your own password</b><?php endif; ?></li></ol></div>",
            type: "success",
            customClass: 'checkout',
            html: true,
            confirmButtonText: 'Okay'
        });
    </script>
<?php endif; ?>
<footer class="row">
    <div class="col-sm-12" id="footertext">
        <p class="imi-footer">&copy; <?php echo date("Y"); ?> Information Mapping International</p>
        <p class="imi-footer icons"> <a href="https://twitter.com/infomap"><i class="fa fa-twitter-square fa-lg"></i></a> <a href="https://www.facebook.com/iminv"><i class="fa fa-facebook-official fa-lg"></i></a> <a href="http://www.linkedin.com/groups?gid=983927"><i class="fa fa-linkedin-square fa-lg"></i></a> <a href="http://www.youtube.com/user/InformationMapping"><i class="fa fa-youtube-square fa-lg"></i></a> </p>
    </div>
</footer>
<!-- Google Analytics -->
<script>
    (function(i,s,o,g,r,a,m){i['GoogleAnalyticsObject']=r;i[r]=i[r]||function(){
        (i[r].q=i[r].q||[]).push(arguments)},i[r].l=1*new Date();a=s.createElement(o),
        m=s.getElementsByTagName(o)[0];a.async=1;a.src=g;m.parentNode.insertBefore(a,m)
    })(window,document,'script','//www.google-analytics.com/analytics.js','ga');

    ga('create', 'UA-24028581-1', 'auto', {'allowLinker': true});
    // On every domain.
    ga('require', 'linker');
    // List of every domain to share linker parameters.
    ga('linker:autoLink', ['informationmapping.com', 'informationmapping-webinars.com']);
    ga('send', 'pageview'); // Send hits after initializing the auto-linker plug-in.
    ga('require', 'ecommerce');
</script>
<!-- End Google Analytics -->
<script type="text/javascript">
    piAId = '9622';
    piCId = '36422';

    (function() {
        function async_load(){
            var s = document.createElement('script'); s.type = 'text/javascript';
            s.src = ('https:' == document.location.protocol ? 'https://pi' : 'http://cdn') + '.pardot.com/pd.js';
            var c = document.getElementsByTagName('script')[0]; c.parentNode.insertBefore(s, c);
        }
        if(window.attachEvent) { window.attachEvent('onload', async_load); }
        else { window.addEventListener('load', async_load, false); }
    })();
</script>
<script type="text/javascript">
    /* <![CDATA[ */
    var google_conversion_id = 963549049;
    var google_custom_params = window.google_tag_params;
    var google_remarketing_only = true;
    /* ]]> */
</script>
<script type="text/javascript" src="//www.googleadservices.com/pagead/conversion.js">
</script>
<noscript>
    <div style="display:inline;"> <img height="1" width="1" style="border-style:none;" alt="" src="//googleads.g.doubleclick.net/pagead/viewthroughconversion/963549049/?value=0&amp;guid=ON&amp;script=0"/> </div>
</noscript>
</body>
</html>
