<?php
ini_set('display_errors', 1);
session_start();
// Check GET variables
$storeId = 5;
$order = 1336;

// Instantiante Magento
require_once '../magento/app/Mage.php';

require_once '../magento/app/code/local/Storefront/VendorLoader/Model/Loader.php';
Storefront_VendorLoader_Model_Loader::enableLoader();

Mage::app();

// Check for valid store
try {
    $store = Mage::app()->getStore($storeId)->getCode();
    Mage::app()->setCurrentStore($storeId);
} catch(Exception $e) {}

// Error handling
$error = null;


// Check for the purchased slots
$slots = Mage::helper('elearning')->getEnrollmentPurchasedSlotsByID($order);
if(empty($slots)) {
    $error = 'order_no_slots';
}


// Other variables
$elearning = Mage::helper('elearning');


// Upload functionality
$upload = true;
$uploadSlots = 5;
$mimes = array('application/vnd.ms-excel','text/plain','text/csv','text/tsv');

if($upload && !empty($_FILES)) {
    $importError = null;

    if (in_array($_FILES['importfile']['error'], array(UPLOAD_ERR_INI_SIZE, UPLOAD_ERR_FORM_SIZE))) {
        $importError = 'Imported file is too large';

    } elseif (in_array($_FILES['importfile']['error'], array(UPLOAD_ERR_PARTIAL, UPLOAD_ERR_CANT_WRITE, UPLOAD_ERR_NO_TMP_DIR))) {
        $importError = 'File could not be imported';

    } elseif($_FILES['importfile']['error'] != UPLOAD_ERR_NO_FILE) {

        if(!is_readable($_FILES['importfile']['tmp_name'])) {
            $importError = 'Imported file could not be read';

        } elseif(empty($_FILES['importfile']['size'])) {
            $importError = 'Imported file has no size';

        } elseif(!in_array($_FILES['importfile']['type'],$mimes)) {
            $importError = 'Imported file is not a CSV file';

        } else {
            if (($handle = fopen($_FILES['importfile']['tmp_name'], 'r')) !== FALSE) {

                $importLines = array();
                while (($line = fgetcsv($handle, 1000, ',')) !== FALSE) {
                    $importLines[] = $line;
                }

                if(empty($importLines)) {
                    $importError = 'Could not import CSV lines';
                } else {
                    $_SESSION['enroll_slots_imported'] = $importLines;
                }

                $elearning->setMessage('notice', 'CSV lines imported');
                session_write_close();
                header('Location: '.$_SERVER['REQUEST_URI']);
                exit;

            } else {
                $importError = 'Imported file has no content';
            }
        }
    }

    if(!empty($importError)) {
        $elearning->setMessage('error', $importError);
        session_write_close();
        header('Location: '.$_SERVER['REQUEST_URI']);
        exit;
    }
}

if(!empty($_POST['products']) && !empty($_POST['newslots']))
{
    $product = $_POST['products'];
    $qty = $_POST['newslots'];
    $rt = $elearning->createEnrollmentPurchasedSlotsByID($order, $product, $qty);
    if($rt == true) $elearning->setMessage('success', 'Slots created successfully.');
    session_write_close();
    header('Location: '.$_SERVER['REQUEST_URI']);
    exit;
};

if(!empty($_POST['deleteCheck']) && isset($_POST['remove'])){
    foreach($_POST['deleteCheck'] as $slotID)
    {
        echo $slotID;
        $dl = $elearning->deleteEnrollmentPurchasedSlotsID($slotID);
    }
    session_write_close();
    header('Location: '.$_SERVER['REQUEST_URI']);
    exit;

}

// Check for other POST-data
if(empty($error) && !empty($_POST['slot'])) {


    $now = time(); // or your date as well
    $period= strtotime($_POST['period']);
    $diff = $period -$now;
    $days= floor($diff/(60*60*24));
    $rt = $elearning->postEnrollmentPurchasedSlotsByID($_POST['slot'], $order, $days);
    if($rt == true) $elearning->setMessage('success', 'Enrollment saved successfully. Student enrolled for a period of '.$days.' days.');
    session_write_close();
    header('Location: '.$_SERVER['REQUEST_URI']);
    exit;
}

// Session messages
$session_messages = array();

// Add the enroll message
if(!empty($_SESSION['enroll_messages'])) {
    foreach($_SESSION['enroll_messages'] as $session_message) {
        $session_messages[] = $session_message;
    }
    unset($_SESSION['enroll_messages']);
}

// Continue to index.php
