<?php
header('X-UA-Compatible: IE=edge,chrome=1');
include_once 'imp_enroll.php';
include_once 'connect.php';
include_once('ENV.php');
?>
<!DOCTYPE html>
<html>
<head>
    <meta charset="utf-8">
    <title><?php echo $elearning->__('Manage your e-learning access'); ?></title>
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <meta name="description" content="Start with FS Pro 2013 and increase your productivity today">
    <meta name="keywords" content="writing technical documentation, proposal letter template, technical writing software, template for proposal, business document template, proposal software, writing software, documentation software" />
    <link href="css/bootstrap.min.css" rel="stylesheet" media="screen">
    <link rel="stylesheet" href="//maxcdn.bootstrapcdn.com/font-awesome/4.3.0/css/font-awesome.min.css">
    <link href="css/imi.css" rel="stylesheet" media="screen">
    <link rel="stylesheet" href="assets/lib/datatables/3/dataTables.bootstrap.css">
    <!-- HTML5 shim and Respond.js IE8 support of HTML5 elements and media queries -->
    <!--[if lt IE 9]>
    <script src="js/html5shiv.js"></script>
    <script src="js/respond.min.js"></script>
    <![endif]-->
    <script src="assets/lib/jquery/jquery.min.js"></script>
    <script src="assets/lib/datatables/jquery.dataTables.js"></script>
    <script src="assets/lib/datatables/3/dataTables.bootstrap.js"></script>
    <script src="js/bootstrap.min.js"></script>
</head>
<body>
<header>
    <div class="container">
        <!-- Brand and toggle get grouped for better mobile display -->
        <div class="navbar-header">
            <button type="button" class="navbar-toggle collapsed" data-toggle="collapse" data-target="#bs-example-navbar-collapse-1" aria-expanded="false">
                <span class="sr-only">Toggle navigation</span>
                <span class="icon-bar"></span>
                <span class="icon-bar"></span>
                <span class="icon-bar"></span>
            </button>
            <a class="navbar-brand" href="../" title="Home">
                <img alt="Logo" src="../images/logo.gif">
            </a>
        </div>
        <!-- Collect the nav links, forms, and other content for toggling -->
        <div class="collapse navbar-collapse" id="bs-example-navbar-collapse-1">
            <ul class="nav navbar-nav navbar-right">
                <!--<li><a href="../reporting-new/elearning/extend_enrollments.php" class="submit-a-request">Extend Enrollment</a></li>-->
            </ul>
        </div><!-- /.navbar-collapse -->
    </div>
</header>
<?php

if($_POST['validate'])
{
    $pass = isset($_POST['pass']) ? trim($_POST['pass']) : '';
    $_SESSION['wachtwoord'] = $pass;
}
if(empty($_SESSION['wachtwoord']))
{
    include('login.php');
}elseif(!empty($_SESSION['wachtwoord']) && $_SESSION['wachtwoord'] != $password)
{
    include('loginIncorrect.php');
}

if($_SESSION['wachtwoord'] == $password)
{

    ?>
    <div class="container content">
        <div class="row">
            <div class="col-sm-12">

                <?php if(!empty($session_messages)) : ?>
                    <?php foreach($session_messages as $session_message) : ?>
                        <?php if($session_message[0] == 'error') : ?>
                            <div class="alert alert-warning" role="alert">
                                <?php echo $elearning->__($session_message[1]); ?>
                            </div>
                        <?php else: ?>
                            <div class="alert alert-success" role="alert">
                                <?php echo $elearning->__($session_message[1]); ?>
                            </div>
                        <?php endif; ?>
                    <?php endforeach; ?>
                <?php endif; ?>

                <?php if($error == 'order_unknown') : ?>
                    <div class="alert alert-warning" role="alert">
                        <?php echo $elearning->__('This order is not known.'); ?>
                    </div>

                <?php elseif($error == 'order_not_complete') : ?>
                    <div class="alert alert-warning" role="alert">
                        <?php echo $elearning->__('Please come back to this page once payment has been completed.'); ?>
                    </div>

                <?php elseif($error == 'order_no_slots') : ?>
                    <div class="alert alert-warning" role="alert">
                        <?php echo $elearning->__('Please add the number of desired slots.'); ?>
                    </div>

                <?php else: ?>
                <div class="row">
                    <div class="col-sm-12" id="toptitle">
                        <div class="form-group">
                            <div class="col-md-10">
                                <h1>Enroll Students To IMP Certification</h1>
                            </div>
                            <div class="col-md-2">
                                <form class="form-horizontal" action="<?php echo $_SERVER['REQUEST_URI']; ?>" method="post">
                                    <button type="submit" name="add" id="add" value="1" class="btn btn-pink"><i class="fa fa-2x fa-plus-square"></i></button>
                                </form>
                            </div>
                        </div>
                    </div>
                </div>
                <div class="container">

                    <?php if(empty($product)): ?>
                        <?php $slots = $elearning->getEnrollmentPurchasedSlotsByID($order);

                        $importedSlots = (isset($_SESSION['enroll_slots_imported'])) ? $_SESSION['enroll_slots_imported'] : null;
                        if(!empty($importedSlots))
                        {
                            $i = 0;
                            foreach ($slots as $slotId => $slot)
                            {
                                $email = trim($importedSlots[$i][0]);
                                $firstname = trim($importedSlots[$i][1]);
                                $lastname = trim($importedSlots[$i][2]);
                                $lastname = preg_replace('/\;$/', '', $lastname);
                                if ($slot->getLocked() == 0 && !empty($email))
                                {
                                    $slot->setCustomerEmail($email);
                                    $slot->setCustomerFirstname($firstname);
                                    $slot->setCustomerLastname($lastname);
                                }
                                $i ++;
                            }
                            unset($_SESSION['enroll_slots_imported']);
                        }

                        ?>
                        <form class="form-horizontal" action="<?php echo $_SERVER['REQUEST_URI']; ?>" method="post">
                            <table class="table table-bordered table-condensed table-hover" id="dataTables-slots">
                                <thead>
                                <tr>
                                    <th><?php echo $elearning->__('Email'); ?></th>
                                    <th><?php echo $elearning->__('Firstname'); ?></th>
                                    <th><?php echo $elearning->__('Lastname'); ?></th>
                                    <th><?php echo $elearning->__('Course'); ?></th>
                                    <th><?php echo $elearning->__('Remove'); ?></th>
                                </tr>
                                </thead>
                                <tbody>
                                <?php $locked = true; ?>
                                <?php foreach($slots as $slot): ?>
                                    <?php $customer = Mage::getModel('customer/customer')->load($slot->getCustomerId()); ?>
                                    <?php $prodID =$slot->getProductID(); ?>
                                    <?php if($slot->getLocked() == 0) $locked = false; ?>
                                    <tr>
                                        <td>
                                            <?php if($slot->getLocked() == 1) : ?>
                                                <?php echo $slot->getCustomerEmail(); ?>
                                            <?php else: ?>
                                                <input type="email" class="form-control" name="slot[<?php echo $slot->getId(); ?>][email]" value="<?php echo $slot->getCustomerEmail(); ?>" />
                                            <?php endif; ?>
                                        </td>
                                        <td>
                                            <?php if($slot->getLocked() == 1) : ?>
                                                <?php echo $slot->getCustomerFirstname(); ?>
                                            <?php else: ?>
                                                <input type="input" class="form-control" name="slot[<?php echo $slot->getId(); ?>][firstname]" value="<?php echo $slot->getCustomerFirstname(); ?>" />
                                            <?php endif; ?>
                                        </td>
                                        <td>
                                            <?php if($slot->getLocked() == 1) : ?>
                                                <?php echo $slot->getCustomerLastname(); ?>
                                            <?php else: ?>
                                                <input type="input" class="form-control" name="slot[<?php echo $slot->getId(); ?>][lastname]" value="<?php echo $slot->getCustomerLastname(); ?>" />
                                            <?php endif; ?>
                                        </td>
                                        <td>
                                            <?php if($slot->getLocked() == 1) : ?>
                                                <?php echo $elearning->getEnrollmentProductTitle($prodID); ?>
                                            <?php else: ?>
                                                <?php echo $elearning->getEnrollmentProductTitle($prodID); ?>
                                            <?php endif; ?>
                                        </td>
                                        <td class="center">
                                           <?php echo "<a href=\"imp_enroll.php?id=". $slot->getID()."\" class='btn btn-default' />Cancel Enrollment </a>" ?>
                                        </td>
                                    </tr>
                                <?php endforeach; ?>
                                </tbody>
                            </table>
                            <!--
                            <div class="center form-group">
                                <p class="col-sm-offset-4 col-sm-4">Date format: mm/dd/yyy <br> Example: 12/30/2015</p>
                            </div>
                            <div class="form-group period">
                                <label for="period" class="col-sm-offset-1 col-sm-4 control-label">Choose Expiry date :</label>
                                <div class="col-sm-4">
                                    <input type="date" class="form-control" id="period" name="period">
                                </div>
                            </div>
                            -->
                            <?php if($locked == false) : ?>
                                <div class="form-group">
                                    <div class="col-sm-offset-5 col-sm-2">
                                        <input type="submit" value="Enroll now!" name="enroll" class="btn btn-blue" />
                                    </div>
                                </div>

                            <?php endif; ?>
                        </form>
                    <?php endif; ?>
                    <?php endif; ?>
                </div>

                <?php if($locked == false && $upload == true && count($slots) >= $uploadSlots) : ?>
                    <div class="row">
                        <div class="col-sm-12" id="toptitle">
                            <h1>Upload Using CSV</h1>
                        </div>
                    </div>
                    <div class="container">
                        <form class="form-horizontal" action="<?php echo $_SERVER['REQUEST_URI']; ?>" method="post" enctype="multipart/form-data">
                            <div class="form-group center">
                                <p class="col-sm-offset-4 col-sm-4">Please upload a CSV with the following format: <br />"john@example.com","John","Doe"</p>
                            </div>
                            <div class="form-group ">
                                <label class="col-sm-offset-3 col-sm-2 control-label" for="importfile"><?php echo $elearning->__('CSV import :'); ?></label>
                                <div class="col-sm-4">
                                    <input type="file" name="importfile" id="importfile" />
                                </div>
                            </div>
                            <div class="form-group">
                                <div class="col-sm-offset-5 col-sm-7">
                                    <input type="submit" class="btn btn-blue" value="Import" name="import" />
                                </div>
                            </div>
                        </form>
                    </div>
                <?php endif; ?>
            </div>
        </div>

        <br>
        <br>
        <hr>
        <br>
        <div class="row">
            <div class="col-md-12">
                <div class="panel panel-content panel-default">
                    <div class="panel-heading">
                        <h3 class="panel-title">Search Quiz attempts<span class="label label-info"></h3>
                    </div>
                    <div class="panel-body">
                        <form class="form-horizontal" method="post" role="form">
                            <div class="form-group">
                                <label for="inputLicense" class="col-sm-2 control-label">Email address:</label>
                                <div class="col-sm-10">
                                    <input type="text" class="form-control" name="email" id="inputEmail" >
                                </div>
                            </div>
                            <div class="form-group">
                                <div class="col-sm-offset-2 col-sm-10">
                                    <button type="submit" class="btn btn-primary">Search</button>
                                </div>
                            </div>
                        </form>

                    </div>
                    <?php
                    $email = isset($_POST['email']) ? trim($_POST['email']) : '';

                    if (isset($_POST['email']))
                    {
                        if(empty($email))
                        {
                            echo "Please provide a search string.";

                        }
                        else
                        {
                            $db2 = new mysqli("localhost","prod_academy","KD8VDqKZYCrqYtcJ", "imi_prod_academy");
                            $sql = "SELECT
                                                       u.firstname, u.lastname, u.email, q.name, a.attempt, q.attempts, TRUNCATE(a.sumgrades,2) as grade, TRUNCATE(q.sumgrades,2) as sumgrades, FROM_UNIXTIME(a.timestart,'%Y-%m-%d') as quizDate
                                                FROM
                                                    imi_prod_academy.mdl_quiz_attempts as a
                                                        LEFT JOIN
                                                    imi_prod_academy.mdl_user AS u ON u.id = a.userid
                                                    LEFT JOIN
                                                    imi_prod_academy.mdl_quiz as q on q.id=a.quiz
                                                WHERE
                                                    u.email LIKE '%$email%'
                                                    AND a.quiz=1";
                            $attempts = $db2->query($sql);

                            ?>
                            <div class="panel-footer">
                                <table class="table table-bordered table-condensed table-hover table-striped" id="dataTables-attempts">
                                    <thead>
                                    <tr>
                                        <th>Name</th>
                                        <th>Quiz</th>
                                        <th>Attempts</th>
                                        <th>Grade</th>
                                        <th>Date</th>
                                        <th>Email</th>
                                    </tr>
                                    </thead>
                                    <tbody>
                                    <?php
                                    while($row_item = $attempts->fetch_array()) {
                                        $totalAttempts = $row_item['attempts'];
                                        if($totalAttempts == 0){
                                            $total="unlimited";}
                                        else $total=$totalAttempts
                                        ?>
                                        <tr>
                                            <td><?php echo $row_item['firstname']." ".$row_item['lastname'] ; ?></td>
                                            <td><?php echo $row_item['name'] ; ?></td>
                                            <td><?php echo $row_item['attempt']."/".$total ; ?></td>
                                            <td><?php echo $row_item['grade']. "/" . $row_item['sumgrades'] ; ?></td>
                                            <td><?php echo $row_item['quizDate'] ; ?></td>
                                            <td class="email"><?php echo $row_item['email'] ; ?></td>
                                        </tr>
                                    <?php } ?>
                                    </tbody>
                                </table>
                            </div>
                        <?php } } ?>
                </div>
            </div>
        </div><!-- /.inner -->
        <script>
            $(document).ready(function() {
                $('#dataTables-slots').dataTable();
            });
        </script>
    </div>
<?php } ?>
<footer class="row">
    <div class="col-sm-12" id="footertext">
        <p class="imi-footer">&copy; <?php echo date("Y"); ?> Information Mapping International</p>
        <p class="imi-footer icons"> <a href="https://twitter.com/infomap"><i class="fa fa-twitter-square fa-lg"></i></a> <a href="https://www.facebook.com/iminv"><i class="fa fa-facebook-official fa-lg"></i></a> <a href="http://www.linkedin.com/groups?gid=983927"><i class="fa fa-linkedin-square fa-lg"></i></a> <a href="http://www.youtube.com/user/InformationMapping"><i class="fa fa-youtube-square fa-lg"></i></a> </p>
    </div>
</footer>
</body>
</html>
