<?php
$installer = $this;
/* @var $installer Mage_Core_Model_Resource_Setup */

$installer->startSetup();

$installer->run("

CREATE TABLE IF NOT EXISTS `{$this->getTable('dblog/message')}` (
  `id` int(10) unsigned NOT NULL auto_increment,
  `message` TEXT,
  `priority` TINYINT,
  `context` VARCHAR(100),
  `timestamp` timestamp NOT NULL default CURRENT_TIMESTAMP,
  
  PRIMARY KEY  (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

");

$installer->endSetup();
