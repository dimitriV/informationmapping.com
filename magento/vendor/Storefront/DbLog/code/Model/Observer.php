<?php
class Storefront_DbLog_Model_Observer{
	
	public function autoCleanDaily($observer = null){
		
		$isCleaningEnabled = Mage::getStoreConfigFlag('system/log/enabled');
		
		if($isCleaningEnabled){
			$daysToKeep = (int) Mage::getStoreConfig('system/log/clean_after_day');
			
			if($daysToKeep > 0){
				$table = Mage::helper('dblog')->getTableName();
				
				$sql = 'delete from '.$table.' where timestamp < DATE_SUB(SYSDATE(), INTERVAL '.$daysToKeep.' DAY)';
				
				$resource = Mage::getSingleton('core/resource');
				/* @var $db Magento_Db_Adapter_Pdo_Mysql */
				$db = $resource->getConnection('core_write');
				
				$db->query($sql);
				
				Mage::log('Cleaned DbLog table', null, 'dblog.log');
			}
		}
	}
	
}