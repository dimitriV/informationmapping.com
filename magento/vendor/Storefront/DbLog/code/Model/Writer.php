<?php

class Storefront_DbLog_Model_Writer extends Zend_Log_Writer_Stream {

	protected $_context;

	

	public function __construct ($logFile) {
		$parts = explode(DS, $logFile);
		
		$context = $parts[count($parts) - 1];
		
		$this->_context = $context;
		
		parent::__construct($logFile);
	}

	protected function _write ($event) {
		parent::_write($event);
		
		try {
			/*
			 * EXAMPLE DATA ============ $event	Array [4] timestamp	(string:25) 2015-04-27T16:09:23+00:00 message	(string:12) Test message priority	(int) 7 priorityName	(string:5) DEBUG
			 */
			$resource = Mage::getSingleton('core/resource');
			/* @var $db Magento_Db_Adapter_Pdo_Mysql */
			$db = $resource->getConnection('core_write');
			
			unset($event['priorityName']);
			$event['context'] = $this->_context;
			
			$table = Mage::helper('dblog')->getTableName();
			
			$db->insert($table, $event);
			
		} catch (Exception $e) {}
	}

	

}