<?php
class Storefront_DbLog_Model_Resource_Message extends Mage_Core_Model_Resource_Db_Abstract {

	protected function _construct () {
		$this->_init('dblog/message', 'id');
	}
}