<?php
class Storefront_DbLog_Block_Adminhtml_Message_Grid extends Mage_Adminhtml_Block_Widget_Grid{
	/**
	 * Initialize Grid settings
	 */
	public function __construct () {
		parent::__construct();

		$this->setId ( 'dblog_log_grid' );
		$this->setDefaultSort ( 'id' );
		$this->setDefaultDir ( 'DESC' );
		$this->setSaveParametersInSession ( true );
	}
	
	protected function _prepareCollection() {
		$collection = Mage::getModel ( 'dblog/message' )->getResourceCollection ();
		$this->setCollection ( $collection );
		return parent::_prepareCollection ();
	}
	
	protected function _prepareColumns() {
		$helper = Mage::helper ( 'dblog' );
		
		$this->addColumn ( 'id', array ('header' => $helper->__ ( 'ID' ), 'width' => '30px', 'index' => 'id' ) );
		$this->addColumn ( 'timestamp', array ('header' => $helper->__ ( 'Date' ), 'type' => 'datetime', 'width' => '170px', 'index' => 'timestamp' ) );
		$this->addColumn ( 'message', array ('header' => $helper->__ ('Message'), 'index' => 'message' ) );
		$this->addColumn ( 'context', array ('header' => $helper->__ ('Context'), 'width' => '130px', 'index' => 'context' ) );
// 		$this->addColumn ( 'actions', array ('header' => 'Actions', 'renderer' => 'dblog/adminhtml_postsale_column_actions' ) );
		
		return parent::_prepareColumns ();
	}
	
	public function getRowUrl($row) {
		return '#'.$row->getId();
	}
	
}