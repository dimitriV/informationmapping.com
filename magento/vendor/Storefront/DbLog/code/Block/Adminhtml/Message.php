<?php

class Storefront_DbLog_Block_Adminhtml_Message extends Mage_Adminhtml_Block_Widget_Grid_Container {

	/**
	 * Initialize container block settings
	 */
	public function __construct () {
		$this->_controller = 'adminhtml_message';
		$this->_blockGroup = 'dblog';
		$this->_headerText = Mage::helper('dblog')->__('Log Messages');
		parent::__construct();
		
		$this->_removeButton('add');
	}
}