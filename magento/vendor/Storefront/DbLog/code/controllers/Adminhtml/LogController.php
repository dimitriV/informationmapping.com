<?php
class Storefront_DbLog_Adminhtml_LogController extends Storefront_DbLog_Adminhtml_CrudController{
	
	protected $_activeMenu = 'system';
	protected $_helperName = 'dblog';
	protected $_objectNamePlural = 'Log Messages';
	protected $_objectName = 'Log Message';
	protected $_modelGroupedName = 'dblog/message';
	protected $_modelPk = 'id';
	protected $_modelLabelField = 'id';
	protected $_allowStoreSwitching = false;
	
	
	
}