<?php

class Storefront_DbLog_Helper_Data extends Storefront_VendorLoader_Helper_Abstract {
	
	protected $_tableName;
	
	public function getModuleName () {
		return 'Storefront_DbLog';
	}

	
	public function getTableName () {
		if ($this->_tableName === null) {
			/* @var $res Storefront_DbLog_Model_Resource_Message */
			$res = Mage::getResourceModel('dblog/message');
				
			$this->_tableName = $res->getTable('dblog/message');
		}
	
		return $this->_tableName;
	}
}