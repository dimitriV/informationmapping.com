<?php
class Storefront_PayOgoneAlias_Adminhtml_AliasController extends Storefront_PayOgoneAlias_Adminhtml_CrudController{
	
	protected $_activeMenu = 'customer';
	protected $_helperName = 'payogonealias';
	protected $_objectNamePlural = 'Ogone Aliases';
	protected $_objectName = 'Ogone Alias';
	protected $_modelGroupedName = 'payogonealias/alias';
	protected $_modelPk = 'id';
	protected $_modelLabelField = 'alias';
	protected $_allowStoreSwitching = false;
	
}