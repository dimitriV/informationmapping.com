<?php
abstract class Storefront_PayOgoneAlias_Adminhtml_CrudController extends Mage_Adminhtml_Controller_Action {
	
	// Version 1.6
	// Last checked 17 april 2015
	
	// Version history:
	// 1.1 - Initial version
	// 1.2 - Added support for multiselect fields, which are posted as an array
	// 1.3 - Added support for serializing or not serializing arrays on save
	// 1.4 - No longer requires a layout XML
	// 1.5 - Allows store switching for EAV types
	// 1.6 - Added massDelete support
	
	protected $_activeMenu;
	protected $_helperName;
	protected $_objectNamePlural;
	protected $_objectName;
	protected $_modelGroupedName;
	protected $_modelPk;
	protected $_modelLabelField = 'name';
	protected $_allowStoreSwitching = false;
	
	
	protected $_noLongerExistsErrorString = 'This entry no longer exists.';
	protected $_saveSuccesString = 'Saved Successfully.';
	protected $_saveErrorString = 'An error occurred while saving. Please review the log and try again.';
	protected $_deleteSuccessString = 'Deleted Successfully.';
	protected $_deleteErrorString = 'Error while deleting. Please try again.';
	protected $_deleteNotFoundString = 'The item that should be deleted could not be found.';
	
	protected $_editBlockName = 'model_edit';
	
	protected $_serializeArraysOnSave = false;
	
	protected function _initAction() {
		$title = Mage::helper ( $this->_helperName )->__ ( $this->_objectNamePlural );
		
		$this->loadLayout ();
		$this->_setActiveMenu ( $this->_activeMenu );
		$this->_addBreadcrumb ( $title, $title );
		return $this;
	}
	
	public function getCurrentModel(){
		return Mage::registry($this->_getRegistryKeyForModel());
	}
	
	protected function _getRegistryKeyForModel() {
		$key = 'current_' . str_replace ( '/', '_', $this->_modelGroupedName );
		return $key;
	}
	
	public function indexAction() {
		$this->_initAction ();
		$this->_title ( $this->__ ( $this->_objectNamePlural ) )->_title ( $this->__ ( 'Manage ' . $this->_objectNamePlural ) );
		
		$layout = $this->getLayout();
		$contentBlock = $layout->getBlock('content');
		
		$groupedName = $this->_modelGroupedName;
		$groupedName = str_replace('/', '/adminhtml_', $groupedName);
		
		$indexBlock = $layout->createBlock($groupedName, 'index');
		$gridBlock = $layout->createBlock($groupedName.'_grid', 'grid');
		
		if($this->_allowStoreSwitching){
			/* @var $storeSwitcher Mage_Adminhtml_Block_Store_Switcher */
			$storeSwitcher = $layout->createBlock('adminhtml/store_switcher', 'store_switcher');
			$storeSwitcher->setUseConfirm(0);
			
			$contentBlock->append($storeSwitcher);
			// WOUTER: This solutions is not visually perfect. The store switcher is above the title, but to change this we would need separate templates for every CRUD model. It works just the same.
		}

		
		$indexBlock->append($gridBlock);
		$contentBlock->append($indexBlock);
		
		$this->_beforeRender('index');
		$this->renderLayout ();
	}
	
	public function newAction() {
		$this->_forward('edit');
	}
	
	public function editAction() {
		$this->_title ( $this->__ ( $this->_objectNamePlural ) )->_title ( $this->__ ( 'Edit ' . $this->_objectName ) );
		
		$id = $this->getRequest ()->getParam ( 'id' );
		$model = Mage::getModel ( $this->_modelGroupedName );
		
		if ($id) {
			if($this->_allowStoreSwitching){
				$storeId = $this->getRequest ()->getParam ( 'store' );
				if($storeId){
					$model->setStoreId($storeId);
				}
			}
			$model->load ( $id );
			if (! $model->getId ()) {
				Mage::getSingleton ( 'adminhtml/session' )->addError ( Mage::helper ( $this->_helperName )->__ ( $this->_noLongerExistsErrorString ) );
				$this->_redirect ( '*/*' );
				return;
			}
		}
		
		$this->_title ( $model->getId () ? $model->getData($this->_modelLabelField) : $this->__ ( 'New ' . $this->_objectName ) );
		
		// set entered data if was error when we do save
		$data = $this->_getEditData();
		if (! empty ( $data )) {
			$model->addData ( $data );
		}
		//$model->getConditions()->setJsFormObject('rule_conditions_fieldset');
		
		$this->_registerModel($model);
		
		$layout = $this->_initAction ()->getLayout ();
		
		
		/*
		<faq_adminhtml_faq_edit>
			<reference name="head">
				<action method="setCanLoadExtJs">
					<flag>1</flag>
				</action>
				<action method="addItem"><type>skin_js</type><name>js/cmssommelier.js</name></action>
			</reference>
			
			<reference name="left">
				<block type="cmssommelier/adminhtml_sommelier_edit_tabs" name="edit_tabs" />
			</reference>
			
			<reference name="content">
				<block type="cmssommelier/adminhtml_sommelier_edit" name="model_edit" />
			</reference>
		</faq_adminhtml_faq_edit>
		*/
		
		$headBlock = $layout->getBlock('head');
		$headBlock->setCanLoadExtJs(1);
		
		// TODO add custom JS
		
		$leftBlock = $layout->getBlock('left');
		$contentBlock = $layout->getBlock('content');
		
		$groupedName = $this->_modelGroupedName;
		$groupedName = str_replace('/', '/adminhtml_', $groupedName);
		
		$tabsBlock = $layout->createBlock($groupedName.'_edit_tabs', 'edit_tabs');
		$editBlock = $layout->createBlock($groupedName.'_edit', 'model_edit');
		
		if($this->_allowStoreSwitching){
			$storeSwitcher = $layout->createBlock('adminhtml/store_switcher', 'store_switcher');
			$leftBlock->append($storeSwitcher);
		}
		
		
		$leftBlock->append($tabsBlock);
		$contentBlock->append($editBlock);
		
		
		$block = $layout->getBlock ( $this->_editBlockName );
		$block->setData ( 'action', $this->getUrl ( '*/*/save' ) );
		
		
		$title = $id ? Mage::helper ( $this->_helperName )->__ ( 'Edit ' . $this->_objectName ) : Mage::helper ( $this->_helperName )->__ ( 'New ' . $this->_objectName );
		
		$this->_addBreadcrumb ( $title, $title );
		$this->_beforeRender('edit');
		$this->renderLayout ();
	}
	
	protected function _getEditData(){
		$data = Mage::getSingleton ( 'adminhtml/session' )->getPageData ( true );
		return $data;
	}
	
	public function saveAction() {
		if ($this->getRequest ()->getPost ()) {
			try {
				$model = Mage::getModel ( $this->_modelGroupedName );
				Mage::dispatchEvent ( 'controller_' . $this->_getRegistryKeyForModel () . '_prepare_save', array ('request' => $this->getRequest () ) );
				$data = $this->getRequest ()->getPost ();
				
				$id = null;
				if(array_key_exists($this->_modelPk, $data)){
					$id = $data [$this->_modelPk];
				}
				
				//$data = $this->_filterDates($data, array('from_date', 'to_date'));
				//if ($id = $this->getRequest()->getParam('id')) {
				
				if ($id) {
					// update existing model
					$isNewModel = false;
					
					if($this->_allowStoreSwitching){
						$storeId = $this->getRequest ()->getParam ( 'store' );
						if($storeId){
							$model->setStoreId($storeId);
						}
					}
					
					$model->load ( $id );
					if ($id != $model->getId ()) {
						Mage::throwException ( Mage::helper ( $this->_helperName )->__ ( 'Wrong ' . $this->_objectName . ' specified.' ) );
					}
				}else{
					// Create new
					$isNewModel = true;
				}
				
				$validateResult = $model->validateData ( new Varien_Object ( $data ) );
				if ($validateResult !== true) {
					foreach ( $validateResult as $errorMessage ) {
						$this->_getSession ()->addError ( $errorMessage );
					}
					// This clears all submitted data that is not an array (arrays are used for images and the Image Element renderer cant handle it
					foreach($data as $key => $value){
						if(is_array($value)){
							unset($data[$key]);
						}
					}
					
					$this->_getSession ()->setPageData ( $data );
					
					$this->_redirect ( '*/*/edit', array ('id' => $model->getId () ) );
					return;
				}
				
				//                $data['conditions'] = $data['rule']['conditions'];
				//                unset($data['rule']);
				//
				//                if (!empty($data['auto_apply'])) {
				//                    $autoApply = true;
				//                    unset($data['auto_apply']);
				//                } else {
				//                    $autoApply = false;
				//                }
				

				$data = $this->_processFileUploads ( $data );
				
				if($this->_serializeArraysOnSave){
					foreach($data as $key => $value){
						if(is_array($value)){
							$data[$key] = implode(',', $value);
						}
					}
				}
				
				$model->loadPost ( $data );
				
				Mage::getSingleton ( 'adminhtml/session' )->setPageData ( $model->getData () );
				
				$model->save ();
				
				Mage::dispatchEvent ( 'controller_' . $this->_getRegistryKeyForModel () . '_after_save', array ('request' => $this->getRequest (), 'model' => $model, 'data' => $data ) );
				
				Mage::getSingleton ( 'adminhtml/session' )->addSuccess ( Mage::helper ( $this->_helperName )->__ ( $this->_saveSuccesString ) );
				Mage::getSingleton ( 'adminhtml/session' )->setPageData ( false );
				$this->_redirectAfterSave($model, $isNewModel);
				return;
			} catch ( Mage_Core_Exception $e ) {
				$this->_getSession ()->addError ( $e->getMessage () );
			} catch ( Exception $e ) {
				$this->_getSession ()->addError ( Mage::helper ( $this->_helperName )->__ ( $this->_saveErrorString ) );
				Mage::logException ( $e );
				Mage::getSingleton ( 'adminhtml/session' )->setPageData ( $data );
				$this->_redirect ( '*/*/edit', array ('id' => $this->getRequest ()->getParam ( 'id' ) ) );
				return;
			}
		}
		$this->_redirectAfterSave($model);
	}
	
	protected function _redirectAfterSave($model, $isNewModel){
		$this->_redirect ( '*/*/' );
	}
	
	protected function _processFileUploads($data) {
		return $data;
	}
	
	protected function _storeUploadedImage(&$data, $fieldName, $destFolder) {
		return $this->_storeUploadedFile($data, $fieldName, $destFolder, array('jpg','jpeg','gif','png'));
	}
	
	protected function _storeUploadedFile(&$data, $fieldName, $destFolder, $allowedExtensions = null) {
		if (isset ( $_FILES [$fieldName] ['name'] ) && $_FILES [$fieldName] ['name'] != '' && (!isset($_POST[$fieldName]['delete']) || $_POST[$fieldName]['delete'] !== '1')) {
	
			$uploader = new Varien_File_Uploader ( $fieldName );
			if(is_array($allowedExtensions)){
				$uploader->setAllowedExtensions ( $allowedExtensions );
			}
			$uploader->setAllowRenameFiles ( false );
			$uploader->setFilesDispersion ( false );
	
			$path = Mage::getBaseDir ( 'media' ) . DS . $destFolder . DS;
			if(!file_exists($path)){
				mkdir($path, 0777, true);
			}
			$fileName = $_FILES [$fieldName] ['name'];
			$destination = strtolower( $fileName);
	
			$uploader->save ( $path, $destination );
	
			$data [$fieldName] = $destFolder . DS . $uploader->getUploadedFileName ();
	
	
		}elseif(isset($_POST[$fieldName]['delete']) && $_POST[$fieldName]['delete'] === '1'){
			$data [$fieldName] = '';
		} else {
			unset ( $data [$fieldName] );
		}
	}
	
	public function deleteAction() {
		if ($id = $this->getRequest ()->getParam ( 'id' )) {
			try {
				$model = Mage::getModel ( $this->_modelGroupedName );
				$model->load ( $id );
				$this->_beforeDelete($model);
				$model->delete ();
				$this->_afterDelete();
				Mage::getSingleton ( 'adminhtml/session' )->addSuccess ( Mage::helper ( $this->_helperName )->__ ( $this->_deleteSuccessString ) );
				$this->_redirectAfterDelete();
				return;
			} catch ( Mage_Core_Exception $e ) {
				$this->_getSession ()->addError ( $e->getMessage () );
			} catch ( Exception $e ) {
				$this->_getSession ()->addError ( Mage::helper ( $this->_helperName )->__ ( $this->_deleteErrorString ) );
				Mage::logException ( $e );
				$this->_redirect ( '*/*/edit', array ('id' => $this->getRequest ()->getParam ( 'id' ) ) );
				return;
			}
		}
		Mage::getSingleton ( 'adminhtml/session' )->addError ( Mage::helper ( $this->_helperName )->__ ( $this->_deleteNotFoundString ) );
		$this->_redirectAfterDelete();
	}
	
	protected function _isAjax() {
		return ( bool ) $this->getRequest ()->getParam ( 'isAjax' );
	}
	
	protected function _ajaxAction(){
		$model = Mage::getModel ( $this->_modelGroupedName );
		if ($id = $this->getRequest()->getParam('id')) {
			$model->load($id);
		}
		if($this->_isAjax()){
			$extraData = $_REQUEST;
			$model->addData($extraData);
		}
	
		$this->_registerModel($model);
	
		$this->loadLayout();
		$this->renderLayout();
	}
	
	protected function _registerModel($model){
		Mage::register ( $this->_getRegistryKeyForModel (), $model );
		Mage::register ( '_current_model', $model );
	}
	
	protected function _redirectAfterDelete(){
		$this->_redirect ( '*/*/' );
	}
	
	protected function _beforeDelete($model){
		
	}
	
	protected function _afterDelete(){
		
	}
	
	protected function _beforeRender($actionName){
		
	}
	
	public function massDeleteAction(){
		$ids = $this->getRequest()->getParam('ids');
		if(!is_array($ids)) {
			Mage::getSingleton('adminhtml/session')->addError(Mage::helper('adminhtml')->__('Please select one or more items.'));
		} else {
			try {
				foreach ($ids as $id) {
					$model = Mage::getModel($this->_modelGroupedName)->load($id);
					$model->delete();
				}
				Mage::getSingleton('adminhtml/session')->addSuccess(
					Mage::helper('adminhtml')->__('Total of %d record(s) were deleted', count($ids))
				);
			} catch (Exception $e) {
				Mage::getSingleton('adminhtml/session')->addError($e->getMessage());
			}
		}
		
		$this->_redirect('*/*/index');
	}

}