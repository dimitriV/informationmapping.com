<?php

class Storefront_PayOgoneAlias_Helper_Data extends Storefront_VendorLoader_Helper_Abstract {

	public function getModuleName () {
		return 'Storefront_PayOgoneAlias';
	}

	public function isAliasEnabled ($paymentMethodString) {
		// Example: ogone_visa
		return Mage::getStoreConfigFlag('payment/' . $paymentMethodString . '/collect_alias');
	}

	public function getAliasesForCustomer ($customerId = null) {
		if ($customerId === null) {
			/* @var $helper Mage_Customer_Helper_Data */
			$helper = Mage::helper('customer');
			
			$customerId = $helper->getCurrentCustomer()->getId();
		}
		$pspid = $this->getPspid();
		$environment = $this->getEnvironment();
		
		/* @var $col Storefront_PayOgoneAlias_Model_Resource_Alias_Collection */
		$col = Mage::getResourceModel('payogonealias/alias_collection');
		$col->addFieldToFilter('customer_id', $customerId);
		$col->addFieldToFilter('pspid', $pspid);
		$col->addFieldToFilter('environment', $environment);
		
		$col->addOrder('is_primary', 'desc');
		$col->addOrder('last_used_at', 'desc');
		
		return $col;
	}

	public function getPrimaryAliasForCustomer ($customerId = null) {
		$aliases = $this->getAliasesForCustomer($customerId);
		
		// TODO move primary logic to "saved cards" module
		
		// $aliases->addFieldToFilter('is_primary', '1');
		if ($aliases->count() > 0) {
			$primaries = array();
			foreach ($aliases as $alias) {
				if ($alias->getIsPrimary()) {
					$primaries[] = $alias;
				}
			}
			
			if (count($primaries) == 0) {
				// mark one as primary
				$first = $aliases->getFirstItem();
				$first->makePrimary();
				return $first;
				
			} elseif (count($primaries) == 1) {
				// okay
				return $primaries[0];
			} else {
				// removes all primaries but keeps first
				$primaries[0]->makePrimary();
				
				return $primaries[0];
			}
		} else {
			return null;
		}
	}
	
	public function getPspid($storeId = null){
		return Mage::getStoreConfig('ogone/test_account/pspid', $storeId);
	}
	public function getEnvironment($storeId = null){
		return Mage::getStoreConfig('ogone/settings/environment', $storeId);
	}
	
	public function getDirectLinkShaIn($storeId = null){
		$key = 'test';
		if($this->getEnvironment($storeId) === 'prod'){
			$key = 'production';
		}
		
		return Mage::getStoreConfig('ogone/'.$key.'_account/secret_key_directlink', $storeId);
	}
	
	public function getDirectLinkUrl($storeId = null){
		if($this->getEnvironment($storeId) === 'prod'){
			return 'https://secure.ogone.com/ncol/prod/orderdirect.asp';
		}else{
			return 'https://secure.ogone.com/ncol/test/orderdirect.asp';
		}
	}
	
	public function getDirectLinkApiUser($storeId = null){
		$key = 'test';
		if($this->getEnvironment($storeId) === 'prod'){
			$key = 'production';
		}
		return Mage::getStoreConfig('ogone/'.$key.'_account/api_userid', $storeId);
	}
	
	public function getDirectLinkShaSecret($storeId = null){
		$key = 'test';
		if($this->getEnvironment($storeId) === 'prod'){
			$key = 'production';
		}
		return Mage::getStoreConfig('ogone/'.$key.'_account/secret_key_directlink', $storeId);
	}
	
	public function getDirectLinkApiPass($storeId = null){
		$key = 'test';
		if($this->getEnvironment($storeId) === 'prod'){
			$key = 'production';
		}
		return Mage::getStoreConfig('ogone/'.$key.'_account/api_pswd', $storeId);
	}
	
	public function createAlias($postsaleData, $customerId, $storeId){
		if(!isset($postsaleData['CARDNO']) || !$postsaleData['CARDNO']){
			Mage::throwException('Missing parameter "CARDNO" during Ogone postsale alias save');
		}
		
		if(!isset($postsaleData['ED']) || !$postsaleData['ED']){
			Mage::throwException('Missing parameter "ED" during Ogone postsale alias save');
		}
		
		if(!isset($postsaleData['ALIAS']) || !$postsaleData['ALIAS']){
			Mage::throwException('Missing parameter "ALIAS" during Ogone postsale alias save');
		}
		
		/* @var $helper Storefront_PayOgoneAlias_Helper_Data */
		$helper = Mage::helper('payogonealias');
		
		$alias = $postsaleData['ALIAS'];
		$pspid = $helper->getPspid($storeId);
		$environment = $helper->getEnvironment($storeId);
		
		$card = $postsaleData['CARDNO'];
		
		$expiryDate = $postsaleData['ED'];
		// Example value = 0216 -> february 2016
		
		// Reformat expiry date to 'yyyy-mm-dd'
		$expiryDate = '20'.substr($expiryDate, 2, 4).'-'.substr($expiryDate, 0, 2).'-01';
		
		$col = Mage::getResourceModel('payogonealias/alias_collection');
		$col->addFieldToFilter('alias', $alias);
		$col->addFieldToFilter('environment', $environment);
		$col->addFieldToFilter('pspid', $pspid);
		
		$nowString = date('Y-m-d H:i:s');
		
		if($col->count() > 0){
			// Alias already exists
			$aliasObj = $col->getFirstItem();
				
		}else{
			// Create new alias
			
			/* @var $aliasObj Storefront_PayOgoneAlias_Model_Alias */
			$aliasObj = Mage::getModel('payogonealias/alias');
				
			$aliasObj->setAlias($alias);
			$aliasObj->setPspid($pspid);
			$aliasObj->setEnvironment($environment);
			$aliasObj->setCreatedAt($nowString);
		}
		
		$parts = explode('-', $alias);
		$pm = $parts[0];
		
		$aliasObj->setPaymentMethod($pm);
		$aliasObj->setLastUsedAt($nowString);
		$aliasObj->setCustomerId($customerId);
		$aliasObj->setCard($card);
		$aliasObj->setExpiryDate($expiryDate);
		
		$aliasObj->save();
		
	}
}