<?php
$installer = $this;
/* @var $installer Mage_Core_Model_Resource_Setup */

$installer->startSetup();

$installer->run("

CREATE TABLE IF NOT EXISTS `{$this->getTable('payogonealias/alias')}` (
  `id` int(10) unsigned NOT NULL AUTO_INCREMENT,
  `payment_method` VARCHAR(45) NULL DEFAULT NULL,
  `alias` varchar(100) DEFAULT NULL,
  `customer_id` int(10) DEFAULT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `last_used_at` timestamp NULL DEFAULT NULL,
  `pspid` varchar(100) DEFAULT NULL,
  `environment` enum('test','prod') DEFAULT NULL,
  PRIMARY KEY (`id`),
  UNIQUE KEY `UNQ` (`alias`,`pspid`,`environment`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8

");

$installer->endSetup();
