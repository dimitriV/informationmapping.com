<?php
$installer = $this;
/* @var $installer Mage_Core_Model_Resource_Setup */

$installer->startSetup();


$installer->run("
ALTER TABLE `{$this->getTable('payogonealias/alias')}`
ADD COLUMN `card` VARCHAR(20) NULL DEFAULT NULL AFTER `alias`
");

$installer->run("
ALTER TABLE `{$this->getTable('payogonealias/alias')}`
ADD COLUMN `is_primary` TINYINT(1) UNSIGNED NOT NULL DEFAULT 0 AFTER `environment`
");




$installer->endSetup();
