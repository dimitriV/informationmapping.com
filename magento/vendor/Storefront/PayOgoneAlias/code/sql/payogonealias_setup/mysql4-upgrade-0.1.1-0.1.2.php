<?php
$installer = $this;
/* @var $installer Mage_Core_Model_Resource_Setup */

$installer->startSetup();


$installer->run("
ALTER TABLE `{$this->getTable('payogonealias/alias')}`
ADD COLUMN `expiry_date` DATE NULL DEFAULT NULL AFTER `card`
");


$installer->endSetup();
