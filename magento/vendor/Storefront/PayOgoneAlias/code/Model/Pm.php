<?php

class Storefront_PayOgoneAlias_Model_Pm extends Mage_Payment_Model_Method_Abstract {

	protected $_code = 'ogone_alias';

	protected $_formBlockType = 'payogonealias/form';
	
	/* Ogone specific features for Magento */
	protected $_canAuthorize = TRUE;
	protected $_canCapture = TRUE;

	protected $_canRefund = FALSE;

	protected $_canUseForMultishipping = FALSE;

	protected $_canSaveCc = FALSE;
	
	protected $_canUseInternal = true;
	
	// Needed so that capture will be called
	protected $_isInitializeNeeded      = false;

	public function getTitle () {
		
		/* @var $helper Storefront_PayOgoneAlias_Helper_Data */
		$helper = Mage::helper('payogonealias');
		
		if (Mage::app()->getStore()->isAdmin()) {
			// Admin
			return $helper->__('My saved credit card');
		} else {
			// Frontend
			
			if (Mage::helper('customer')->isLoggedIn()) {
				$alias = $helper->getPrimaryAliasForCustomer();
				
				if ($alias) {
					$cardNumber = $alias->getCard();
					return $helper->__('My saved credit card: %s', $cardNumber);
				}
			}
			
			return $helper->__('My saved credit card');
		}
	}

	public function capture(Varien_Object $payment, $amount){
		$isTesting = false;
		
		$order = $payment->getOrder();
		
		$storeId = $order->getStoreId();
		
		/* @var $helper Storefront_PayOgoneAlias_Helper_Data */
		$helper = Mage::helper('payogonealias');
		
		/* @var $ogoneHelper Storefront_PayOgone_Helper_Data */
		$ogoneHelper = Mage::helper('ogone');
		
		$merchantRef = $order->getIncrementId();
		$customerId = $order->getCustomerId();
		if (! $customerId) {
			Mage::throwException('Cannot pay with Ogone alias. Customer is unknown');
		}
		
		$alias = $helper->getPrimaryAliasForCustomer($customerId);
		
		if (! ($alias && $alias->getAlias())) {
			Mage::throwException('Customer "' . $customerId . '" does not have an Ogone alias.');
		}
		
		$amount = $order->getGrandTotal();
		
		/* @var $ba Mage_Sales_Model_Order_Address */
		$ba = $order->getBillingAddress();
		
		$firstName = $ba->getFirstname();
		$lastName = $ba->getLastname();
		$email = $order->getCustomerEmail();
		$customerAddress = trim($ba->getStreet1() . ' ' . $ba->getStreet2() . ' ' . $ba->getStreet3() . ' ' . $ba->getStreet4());
		$postalCode = $ba->getPostcode();
		$city = $ba->getCity();
		$countryCode = $ba->getCountry();
		$phone = $ba->getTelephone();
		
		$locale = Mage::getStoreConfig('general/locale/code', $storeId);
		$localeParts = explode('_', $locale);
		$langCode = strtolower($localeParts[0]);
		
		$pspid = $helper->getPspid($storeId);
		$shaPass = $helper->getDirectLinkShaIn($storeId);
		
		$directLinkUser = $helper->getDirectLinkApiUser($storeId);
		$directLinkPass = $helper->getDirectLinkApiPass($storeId);
		
		$customerFullName = trim($firstName . ' ' . $lastName);
		
		// Collect fields
		$fields = array();
		
		// We need to add a random part, because if something external goes wrong, 
		// Magento is stuck on the same order number for the cart. The customer cannot continue... 
		$fields["ORDERID"] = 'alias-'.mt_rand(0, 999). '-'.$order->getIncrementId();
		
		$fields["PSPID"] = $pspid;
		$fields['ALIAS'] = $alias->getAlias();
		$fields['ECI'] = '9';
		$fields["AMOUNT"] = round($amount * 100, 0);
		$fields["CURRENCY"] = $order->getOrderCurrencyCode();
		$fields["LANGUAGE"] = $langCode;
		$fields["COMPLUS"] = '';
		$fields["CN"] = $customerFullName;
		$fields["EMAIL"] = $email;
		
		$fields["OWNERZIP"] = $postalCode;
		$fields["OWNERADDRESS"] = $customerAddress;
		$fields["OWNERTOWN"] = $city;
		$fields["OWNERCTY"] = $countryCode;
		if ($phone){
			$fields["OWNERTELNO"] = $phone;
		}
		
		// addValueForSha("PM", getOgonePaymentPm(o), shaKeys);
		// addValueForSha("BRAND", getOgonePaymentBrand(o), shaKeys);
		
		$fields['WITHROOT'] = "Y";
		$fields['OPERATION'] = "SAL";
		
		$fields['USERID'] = $directLinkUser;
		$fields['PSWD'] = $directLinkPass;
		
		// Add signature at the end
		$directLinkShaIn = $helper->getDirectLinkShaSecret($storeId);
		$fields['SHASIGN'] = $ogoneHelper->generateShaSignature($fields, $directLinkShaIn);
		
		
		
		
		// HTTP request
		$ogoneUrl = $helper->getDirectLinkUrl($storeId);
		$client = new Zend_Http_Client($ogoneUrl);
		$client->setMethod('POST');
		
		$client->setParameterPost($fields);
		
		
		/*
		 * 
		 * // Book dummy payment during testing...
			$payid = 'dummy';
			$xmlString = '<dummy></dummy>';
			$ogoneStatus = '9';
			
			$payment->setTransactionId($payid);
			$payment->setIsTransactionClosed(1);
			$payment->setTransactionAdditionalInfo(Mage_Sales_Model_Order_Payment_Transaction::RAW_DETAILS, array(
					'xml' =>  $xmlString,
					'ogone_status' => $ogoneStatus,
						
			));
			
		 */
		
			
			Mage::log('Sending data to Ogone Directlink: '.$ogoneUrl. "\n".print_r($fields,true), null, $this->getCode() . '.log');
			
			
			
			$response = $client->request();
			
			$xmlString = $response->getBody();
			
			$xml = simplexml_load_string($xmlString);
			
			Mage::log('Received XML from Ogone: '.$xmlString, null, $this->getCode() . '.log');
		
		
			
			// dict.put("OgoneTargetUrl", dlOrderUrl); // Ogone endpoint URL
			// dict.put("OgoneParameters", OgoneKeys); // Data sent to Ogone
			// dict.put("OgoneShaIn", shaPass); // SHA sent to Ogone
			// dict.put("OgoneXml", xml); // Response from Ogone
			
			/*
			 * EXAMPLE RESPONSE XML FROM OGONE: <?xml version="1.0"?> <ogone> <ncresponse orderID="WOUTER1" PAYID="34692993" NCSTATUS="0" NCERROR="50001113" ACCEPTANCE="test123" STATUS="0" amount="30" currency="EUR" PM="CreditCard" BRAND="MasterCard" ALIAS="MasterCard-5000000273" NCERRORPLUS="This order has already been processed by Ogone DirectLink" > </ncresponse> </ogone>
			 */
			
			//Example success
			/*
			 * (string:1042) <?xml version="1.0"?><ogone><ncresponse
	orderID="100000026"
	PAYID="40125624"
	NCSTATUS="0"
	NCERROR="0"
	ACCEPTANCE="test123"
	STATUS="9"
	IPCTY="99"
	CCCTY="US"
	ECI="9"
	CVCCheck="NO"
	AAVCheck="NO"
	VC="NO"
	AAVZIP="NO"
	AAVADDRESS="NO"
	AAVNAME="NO"
	AAVPHONE="NO"
	AAVMAIL="NO"
	amount="40"
	currency="EUR"
	PM="CreditCard"
	BRAND="VISA"
	ALIAS="ogone_visa-8086-6672"
	ECOM_BILLTO_POSTAL_CITY="Menen"
	ECOM_BILLTO_POSTAL_COUNTRYCODE="NL"
	ECOM_BILLTO_POSTAL_STATEDESC=""
	ECOM_BILLTO_POSTAL_STREET_LINE1="Henri Dunantlaan 84"
	ECOM_BILLTO_POSTAL_STREET_LINE2=""
	ECOM_BILLTO_POSTAL_POSTALCODE="8930"
	CN="Wouter Samaey"
	ECOM_BILLTO_POSTAL_NAME_LAST=""
	ECOM_BILLTO_POSTAL_NAME_FIRST="Wouter Samaey"
	ECOM_BILLTO_TELECOM_PHONE_NUMBER="0472728307"
	ECOM_SHIPTO_POSTAL_CITY=""
	ECOM_SHIPTO_POSTAL_COUNTRYCODE=""
	ECOM_SHIPTO_POSTAL_STATEDESC=""
	ECOM_SHIPTO_POSTAL_STREET_LINE1=""
	ECOM_SHIPTO_POSTAL_STREET_LINE2=""
	ECOM_SHIPTO_POSTAL_POSTALCODE=""
	ECOM_SHIPTO_POSTAL_NAME_LAST="Wouter Samaey"
	NCERRORPLUS="!">
	</ncresponse></ogone>
			 */
			
			$ogoneStatus = ''.$xml->ncresponse->attributes()->STATUS;
			$errorCode = ''.$xml->ncresponse->attributes()->NCSTATUS;
			$errorMsg = trim(''.$xml->ncresponse->attributes()->NCERRORPLUS);
			$payid = ''.$xml->ncresponse->attributes()->PAYID;

			if ($ogoneStatus == '9') {
				
				$nowString = date('Y-m-d H:i:s');
				$alias->setLastUsedAt($nowString);
				$alias->save();
				
				$payment->setTransactionId($payid);
				$payment->setIsTransactionClosed(1);
				$payment->setTransactionAdditionalInfo(Mage_Sales_Model_Order_Payment_Transaction::RAW_DETAILS, array(
						'xml' =>  $xmlString,
						'ogone_status' => $ogoneStatus,
							
				)); // use this in case you want to add some extra information
				
				
				
				
			}else{
				// Example error: 
				
				$aliasInvalidError = 'Alias '.$alias->getAlias().' not Found';
				
				if($errorMsg === $aliasInvalidError){
					// The alias does not exist
					// Deleting the alias doesnt work because we are in a transaction
	
					//$alias->delete();
					
					Mage::throwException('Alias not known by Ogone: '.$alias->getId());
					
				}else{
                    if($errorCode == '3'){
                        //Authorisation declined
                        Mage::throwException($helper->__('We\'re sorry, but your saved credit card is not accepted. Please use a different card.'));
                    }else{
                        Mage::throwException('Ogone alias direct link error: '.$errorCode.' - '. $errorMsg);
                    }

				}

			}

		return $this;
	}



// 	public function assignData ($data) {
// 		if (! ($data instanceof Varien_Object)) {
// 			$data = new Varien_Object($data);
// 		}
		
// 		$info = $this->getInfoInstance();
// 		//$info->setCcOwner($data->getCcOwner())->setCcType($data->getCcOwner())->setCcNumber($data->getCcNumber())->setCcCid($data->getCcCid())->setCcExpMonth($data->getCcExpMonth())->setCcExpYear($data->getCcExpYear())->setOgoneIssuerPm($data->getOgoneIssuerPm())->setOgoneIssuerBrand($data->getOgoneIssuerBrand());
// 		parent::assignData($data);
// 		return $this;
// 	}
	
	public function isAvailable($quote = null){
		$customerId = $quote->getCustomerId();
		
		if(!$customerId){
			return false;
		}
		
		/* @var $helper Storefront_PayOgoneAlias_Helper_Data */
		$helper = Mage::helper('payogonealias');
		$alias = $helper->getPrimaryAliasForCustomer($customerId);
		
		if($alias && $alias->getAlias()){
			return parent::isAvailable($quote);
		}else{
			return false;
		}
		
	}

/**
 * Prepare info instance for save
 *
 * @return Mage_Payment_Model_Abstract
 */
	// public function prepareSave()
	// {
	// $info = $this->getInfoInstance();
	
	// Mage::getSingleton('core/session')->setCcNumberEnc($info->encrypt($info->getCcNumber()));
	// Mage::getSingleton('core/session')->setCcCidEnc($info->encrypt($info->getCcCid()));
	// Mage::getSingleton('core/session')->setCcExpMonthEnc($info->encrypt($info->getCcExpMonth()));
	// Mage::getSingleton('core/session')->setCcExpYearEnc($info->encrypt($info->getCcExpYear()));
	
	// $info->setCcNumber(null)->setCcCid(null);
	
	// return $this;
	// }
}
