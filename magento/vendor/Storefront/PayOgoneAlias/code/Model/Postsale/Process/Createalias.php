<?php

class Storefront_PayOgoneAlias_Model_Postsale_Process_Createalias extends Storefront_PayOgone_Model_Postsale_Process_Abstract {

	public function process (Storefront_PayOgone_Model_Postsale_Request $postsale) {
		$status = $postsale->getStatus();
		
		if ($postsale->isPaymentOkay() || $postsale->isAuthorizeOkay()) {
			$orderId = $postsale->getOrderId();
			
			// Expected format: $orderId = 'create-alias-'.$customer->getId().'-'.$storeId.'-'.time().'-'.sprintf('%\'.05d', mt_rand(0, 99999));
			
			// Extra security is not needed because the orderID cannot be tampered with without breaking the postsale SHA.
			
			$matches = array();
			
			if(preg_match('#^create\-alias\-([0-9]+)\-([0-9]+)-#is', $orderId, $matches)){
				$customerId = $matches[1];
				$storeId = $matches[2];
				
				/* @var $helper Storefront_PayOgoneAlias_Helper_Data */
				$helper = Mage::helper('payogonealias');
				
				try{
					$helper->createAlias($postsale->getData(), $customerId, $storeId);
					
					$postsale->setProcessed(true);
					
				}catch(Exception $e){
					
					Mage::log('EXCEPTION: '. $e->getMessage(), null, 'wouter.log');
					$postsale->addError($e->getMessage());
				}
			}
		}
	}

	
}