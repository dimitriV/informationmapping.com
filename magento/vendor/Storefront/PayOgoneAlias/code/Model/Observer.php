<?php
class Storefront_PayOgoneAlias_Model_Observer{
	
	public function addAliasFormFields($observer){
		/* @var $order Mage_Sales_Model_Order */
		$order = $observer->getOrder();
		$formFields = $observer->getFormFields();
		
		
		// Example: $paymentMethod = 'ogone_visa'
		$paymentMethod = $order->getPayment()->getMethod();
		
		/* @var $helper Storefront_PayOgoneAlias_Helper_Data */
		$helper = Mage::helper('payogonealias');
		$isEnabled = $helper->isAliasEnabled($paymentMethod);
		
		if($isEnabled){
			$alias = $paymentMethod.'-'.$order->getId().'-'.mt_rand(0, 9999);
			$aliasUsage = $helper->__('Remember this card for future use'); // This is an extra line of text, but we won't use it.
			
			$formFields->setData('ALIAS', $alias);
			$formFields->setData('ALIASUSAGE', $aliasUsage);
		}
	}
	
	public function saveAliasOnCreateAliasPostSale($observer){
		// Save the alias when a card was added - 0 amount transaction
		
		$postsale = $observer->getPostsale();
		
		/* @var $processor Storefront_PayOgoneAlias_Model_Postsale_Process_Createalias */
		$processor = Mage::getSingleton('payogonealias/postsale_process_createalias');
		
		$processor->process($postsale);
		
	}
	
	public function saveAliasOnOrderPostSale($observer){
		
		// Save the alias when an order was placed
		
		/* @var $order Mage_Sales_Model_Order */
		$order = $observer->getOrder();
		
		$params = $observer->getParams();
		
		if(isset($params['ALIAS'])){
			
			$customerId = $order->getCustomerId();
			
			if($customerId){
				
				$storeId = $order->getStoreId();
				
				/* @var $helper Storefront_PayOgoneAlias_Helper_Data */
				$helper = Mage::helper('payogonealias');
				
				$helper->createAlias($params, $customerId, $storeId);
			}
			
		}
	}
	
	public function loadOgoneAliasCardsForCustomer($observer){
		$customerId = $observer->getCustomerId();
		$cards = $observer->getCards();
		
		/* @var $helper Storefront_PayOgoneAlias_Helper_Data */
		$helper = Mage::helper('payogonealias');
		
		$aliases = $helper->getAliasesForCustomer($customerId);
		
		foreach($aliases as $alias){
			$cards[] = $alias;
		}
	}
	
	/**
	 * Delete an ogone alias if it could not be used during subscription renewal
	 * @param unknown $observer
	 */
	public function subscriptionRenewFailed($observer){
		
		$e = $observer->getException();
		
		// Intercept invalid aliasses
		$unknownAliasPrefix = 'Alias not known by Ogone: ';
		$message = $e->getMessage();
		
		if(strlen($message) > strlen($unknownAliasPrefix) && substr($message, 0, strlen($unknownAliasPrefix)) === $unknownAliasPrefix ){
			$aliasId = (int) substr($message, strlen($unknownAliasPrefix));
			$alias = Mage::getModel('payogonealias/alias')->load($aliasId);
			$alias->delete();
		}
	}


    public function sendInvoiceEmailAfterAliasPayment($observer){
        /* @var $order Mage_Sales_Model_Order */
        $order = $observer->getOrder();

        $payment = $order->getPayment();

        $pm = new Storefront_PayOgoneAlias_Model_Pm();
        $isAlias = ($payment->getMethod() === $pm->getCode());
        $sendInvoiceEnabled = Mage::getStoreConfigFlag('sales_email/invoice/enabled', $order->getStoreId());

        if($sendInvoiceEnabled && $isAlias){

            $invoices = $order->getInvoiceCollection();


            foreach($invoices as $invoice){
                /* @var $invoice Mage_Sales_Model_Order_Invoice */

                if($invoice->getIsPaid() && !$invoice->getEmailSent()){
                    $invoice->sendEmail();
                    $invoice->setEmailSent(1);
                    $invoice->save();
                }

            }






        }
    }
	
	
}