<?php
class Storefront_PayOgoneAlias_Model_Alias extends Mage_Core_Model_Abstract {

	protected function _construct () {
		$this->_init('payogonealias/alias');
	}
	
	/**
	 * Make this alias the primary one, and reset all other primaries
	 */
	public function makePrimary(){
		$customerId = $this->getCustomerId();
		$aliases = Mage::helper('payogonealias')->getAliasesForCustomer($customerId);
		
		$aliases->addFieldToFilter('is_primary', '1');
		
		// In order to prevent the auto-primary election to misbehave, we will first set the new primary, and then remove the old ones...
		
		// Save this model as the new primary
		$this->setIsPrimary('1')->save();
		
		// Remove all other primaries
		foreach ($aliases as $alias) {
			if($alias->getId() != $this->getId()){
				$alias->setIsPrimary('0');
				$alias->save();
			}
		}
		
		
	}
	
	protected function _afterSave(){
		$customerId = $this->getCustomerId();
		
		// TODO add store view data separation...
		
		Mage::dispatchEvent('saved_cards_changed_for_customer', array('customer_id' => $customerId));
	}
	
	protected function _afterDelete(){
		$customerId = $this->getCustomerId();
		
		// TODO add store view data separation...
		
		Mage::dispatchEvent('saved_cards_changed_for_customer', array('customer_id' => $customerId));
	}
}