<?php
class Storefront_PayOgoneAlias_Model_Resource_Alias extends Mage_Core_Model_Resource_Db_Abstract {

	protected function _construct () {
		$this->_init('payogonealias/alias', 'id');
	}
}