<?php
class Storefront_PayOgoneAlias_Block_Form extends Mage_Payment_Block_Form {

	protected function _construct () {
		parent::_construct();
		$this->setTemplate('payogonealias/form.phtml');
	}
	
	/**
	 * @return Storefront_PayOgoneAlias_Model_Alias
	 */
	public function getAlias(){
		return Mage::helper('payogonealias')->getPrimaryAliasForCustomer();
	}

}
