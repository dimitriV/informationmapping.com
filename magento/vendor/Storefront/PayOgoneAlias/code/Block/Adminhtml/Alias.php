<?php

class Storefront_PayOgoneAlias_Block_Adminhtml_Alias extends Mage_Adminhtml_Block_Widget_Grid_Container {

	/**
	 * Initialize container block settings
	 */
	public function __construct () {
		$this->_controller = 'adminhtml_alias';
		$this->_blockGroup = 'payogonealias';
		$this->_headerText = Mage::helper('payogonealias')->__('Ogone Aliases (Saved Cards)');
		parent::__construct();
		
		$this->_removeButton('add');
	}
}