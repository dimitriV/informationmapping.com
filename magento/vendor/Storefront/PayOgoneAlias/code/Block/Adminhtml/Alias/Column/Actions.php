<?php

class Storefront_PayOgoneAlias_Block_Adminhtml_Alias_Column_Actions extends Mage_Adminhtml_Block_Widget_Grid_Column_Renderer_Abstract {

	public function render (Varien_Object $row) {
		
		$html = '<a href="'.$this->getUrl('payogonealias/adminhtml_alias/delete', array('id' => $row->getId())).'">'.$this->__('Delete').'</a>';
		
		return $html;
	}
}