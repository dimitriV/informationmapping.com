<?php
class Storefront_PayOgoneAlias_Block_Adminhtml_Alias_Grid extends Mage_Adminhtml_Block_Widget_Grid{
	/**
	 * Initialize Grid settings
	 */
	public function __construct () {
		parent::__construct();

		$this->setId ( 'payogonealias_alias_grid' );
		$this->setDefaultSort ( 'id' );
		$this->setDefaultDir ( 'DESC' );
		$this->setSaveParametersInSession ( true );
	}
	
	protected function _prepareCollection() {
		$collection = Mage::getModel ( 'payogonealias/alias' )->getResourceCollection ();
		$this->setCollection ( $collection );
		return parent::_prepareCollection ();
	}
	
	protected function _prepareColumns() {
		$helper = Mage::helper ( 'ogone' );
		
		$this->addColumn ( 'id', array ('header' => $helper->__ ( 'ID' ), 'width' => '30px', 'index' => 'id' ) );
		$this->addColumn ( 'created_at', array ('header' => $helper->__ ( 'Date' ), 'type' => 'datetime', 'width' => '170px', 'index' => 'created_at' ) );
		
		$this->addColumn ( 'payment_method', array ('header' => $helper->__ ( 'Payment Method' ), 'width' => '120px', 'index' => 'payment_method' ) );
		
		$this->addColumn ( 'alias', array ('header' => $helper->__('Alias'), 'index' => 'alias' ) );
		$this->addColumn ( 'card', array ('header' => $helper->__('Card'), 'width' => '170px', 'index' => 'card' ) );
		$this->addColumn ( 'customer_id', array ('header' => $helper->__('Customer ID'), 'width' => '170px', 'index' => 'customer_id' ) );
		
		$this->addColumn ( 'expiry_date', array ('header' => $helper->__('Expiry'), 'width' => '170px', 'index' => 'expiry_date' ) );
		$this->addColumn ( 'is_primary', array ('header' => $helper->__('Primary'), 'width' => '170px', 'index' => 'is_primary' ) );
		$this->addColumn ( 'environment', array ('header' => $helper->__('Environment'), 'width' => '170px', 'index' => 'environment' ) );
		$this->addColumn ( 'pspid', array ('header' => $helper->__('PSPID'), 'width' => '170px', 'index' => 'pspid' ) );
		
		$this->addColumn ( 'actions', array ('header' => 'Actions', 'renderer' => 'payogonealias/adminhtml_alias_column_actions' ) );
		
		return parent::_prepareColumns ();
	}
	
	public function getRowUrl($row) {
		return '#'.$row->getId();
	}
	
}