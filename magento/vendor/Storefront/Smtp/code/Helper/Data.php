<?php

class Storefront_Smtp_Helper_Data extends Storefront_VendorLoader_Helper_Abstract
{

    const LOG_FILE = 'smtp.log';

    public function getTransport($storeId = null)
    {
        $option = Mage::getStoreConfig('smtp/general/option', $storeId);
        return Mage::getModel("smtp/transports_$option")->getTransport($storeId);
    }


    public function log($m)
    {
        if ($this->isDebugLoggingEnabled()) {
            Mage::log($m, null, self::LOG_FILE);
        }
    }

    public function logEmailSent($to, $template, $subject, $email, $isHtml)
    {
        if ($this->isLogEnabled()) {
            $log = Mage::getModel('smtp/email_log')
                ->setEmailTo($to)
                ->setTemplate($template)
                ->setSubject($subject)
                ->setEmailBody($isHtml ? $email : nl2br($email))
                ->save();
        }
        return $this;
    }

    // General config
    public function isEnabled($storeId = null)
    {
        return Mage::getStoreConfig('smtp/general/option', $storeId) != "disabled";
    }

    public function isGoogleAppsEnabled($storeId = null)
    {
        return Mage::getStoreConfig('smtp/general/option', $storeId) == "google";
    }

    public function isAmazonSESEnabled($storeId = null)
    {
        return Mage::getStoreConfig('smtp/general/option', $storeId) == "ses";
    }

    public function isSMTPEnabled($storeId = null)
    {
        return Mage::getStoreConfig('smtp/general/option', $storeId) == "smtp";
    }

    public function isSendGridEnabled($storeId = null)
    {
        return Mage::getStoreConfig('smtp/general/option', $storeId) == "sendgrid";
    }


    // logging config
    public function isLogCleaningEnabled($storeId = null)
    {
        return Mage::getStoreConfigFlag('smtp/debug/cleanlog', $storeId);
    }

    public function getLogLifetimeDays($storeId = null)
    {
        return Mage::getStoreConfig('smtp/debug/cleanlog_after_days', $storeId);
    }

    public function isLogEnabled($storeId = null)
    {
        //return Mage::getStoreConfigFlag('smtp/debug/logenabled', $storeId);
        return true;
    }

    public function isDebugLoggingEnabled($storeId = null)
    {
        return Mage::getIsDeveloperMode();
    }

    // transport config
    public function getAmazonSESAccessKey($storeId = null)
    {
        return Mage::getStoreConfig('smtp/general/ses_access_key', $storeId);
    }

    public function getAmazonSESPrivateKey($storeId = null)
    {
        return Mage::getStoreConfig('smtp/general/ses_private_key', $storeId);
    }

    public function getGoogleAppsEmail($storeId = null)
    {
        return Mage::getStoreConfig('smtp/general/googleapps_email', $storeId);
    }

    public function getGoogleAppsPassword($storeId = null)
    {
        return Mage::getStoreConfig('smtp/general/googleapps_gpassword', $storeId);
    }


    public function getSendGridEmail($storeId = null)
    {
        return Mage::getStoreConfig('smtp/general/sendgrid_email', $storeId);
    }

    public function getSendGridPassword($storeId = null)
    {
        return Mage::getStoreConfig('smtp/general/sendgrid_password', $storeId);
    }

    public function getMailUpUsername($storeId = null)
    {
        return Mage::getStoreConfig('smtp/general/mailup_email', $storeId);
    }

    public function getMailUpPassword($storeId = null)
    {
        return Mage::getStoreConfig('smtp/general/mailup_password', $storeId);
    }

    public function getSMTPSettingsHost($storeId = null)
    {
        return trim(Mage::getStoreConfig('smtp/general/smtp_host', $storeId));
    }

    public function getSMTPSettingsPort($storeId = null)
    {
        return Mage::getStoreConfig('smtp/general/smtp_port', $storeId);
    }

    public function getSMTPSettingsUsername($storeId = null)
    {
        return Mage::getStoreConfig('smtp/general/smtp_username', $storeId);
    }

    public function getSMTPSettingsPassword($storeId = null)
    {
        return Mage::getStoreConfig('smtp/general/smtp_password', $storeId);
    }

    public function getSMTPSettingsSSL($storeId = null)
    {
        return Mage::getStoreConfig('smtp/general/smtp_ssl', $storeId);
    }

    public function getSMTPSettingsAuthentication($storeId = null)
    {
        return Mage::getStoreConfig('smtp/general/smtp_authentication', $storeId);
    }

    public function getQueueUsage($storeId = null)
    {
        return Mage::getStoreConfig('smtp/queue/usage', $storeId);
    }
    public function isQueueBypassed($storeId = null)
    {
        if(Mage::getIsDeveloperMode()){
            // Never use the queue during development
            return true;
        }else {
            return Mage::getStoreConfig('smtp/queue/usage', $storeId) == "never";
        }
    }
    public function getQueuePerCron($storeId = null)
    {
        return Mage::getStoreConfig('smtp/queue/percron', $storeId);
    }
    public function getQueuePause($storeId = null)
    {
        return Mage::getStoreConfig('smtp/queue/pause', $storeId);
    }
    
	public function getModuleName () {
		return 'Storefront_Smtp';
	}


}
