<?php

/**
 * Controller for the Log viewing operations
 *
 *
 * 
 */


class Storefront_Smtp_Smtp_LogController
	extends Mage_Adminhtml_Controller_Action {

    protected function _initAction() {
        // load layout, set active menu and breadcrumbs
        $this->loadLayout()
            ->_setActiveMenu('system')
            ->_addBreadcrumb(Mage::helper('adminhtml')->__('System'), Mage::helper('adminhtml')->__('System'))
//             ->_addBreadcrumb(Mage::helper('adminhtml')->__('Tools'), Mage::helper('adminhtml')->__('Tools'))
            ->_addBreadcrumb(Mage::helper('adminhtml')->__('Email Log'), Mage::helper('adminhtml')->__('Email Log'));
        return $this;
    }	
		
	public function indexAction() {
		
		  $this->_initAction()
            ->_addContent($this->getLayout()->createBlock('smtp/log'))
            ->renderLayout();
		
	}	
	
	public function viewAction() {
		
		  $this->_initAction()
            ->_addContent($this->getLayout()->createBlock('smtp/log_view'))
            ->renderLayout();

	}

    /**
     * Show the last email sent
     */
    public function lastAction(){
        $col = Mage::getModel('smtp/email_log')->getCollection();
        $col->setOrder('email_id','DESC');

        $mail = $col->getFirstItem();

        die($mail->getEmailBody());
    }
	
} 
