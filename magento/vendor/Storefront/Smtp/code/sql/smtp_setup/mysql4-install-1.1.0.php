<?php
/**
 *
 *
 * 
 */


$installer = $this;

$installer->startSetup();

Mage::helper('smtp/mysql4_install')->prepareForDb();

Mage::helper('smtp/mysql4_install')->attemptQuery($installer, "
    CREATE TABLE IF NOT EXISTS `{$this->getTable('smtp_email_log')}` (
      `email_id` int(10) unsigned NOT NULL auto_increment,
      `log_at` timestamp NOT NULL default CURRENT_TIMESTAMP on update CURRENT_TIMESTAMP,
      `to` varchar(255) NOT NULL default '',
      `template` varchar(255) NULL,
      `subject` varchar(255) NULL,
      `email_body` text,
      PRIMARY KEY  (`email_id`),
      KEY `log_at` (`log_at`)
    ) ENGINE=InnoDB DEFAULT CHARSET=utf8;
");

$installer->endSetup();
