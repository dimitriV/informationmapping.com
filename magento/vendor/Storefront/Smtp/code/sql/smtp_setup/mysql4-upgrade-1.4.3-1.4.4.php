<?php
$installer = $this;

$installer->startSetup();

Mage::helper('smtp/mysql4_install')->attemptQuery($installer, "
    ALTER TABLE `{$this->getTable('smtp_email_log')}` 
        CHANGE `to` `email_to` VARCHAR(255)  NOT NULL  DEFAULT '';
");

$installer->endSetup();