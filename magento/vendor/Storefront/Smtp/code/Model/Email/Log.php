<?php
/**
 * 
 *
 * 
 */

class Storefront_Smtp_Model_Email_Log extends Mage_Core_Model_Abstract
{
    
    /**
     * Model initialization
     *
     */
    protected function _construct()
    {
        $this->_init('smtp/email_log');
    }
    
    /**
     * clean up log table
     * 
     * @param int|null $lifetime Lifetime of entries in days
     */
    public function clean($lifetime = null)
    {
        $this->getResource()->clean($lifetime);
        return $this;
    }
}