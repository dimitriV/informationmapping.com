<?php


class Storefront_Smtp_Model_System_Config_Source_Smtp_Option extends Varien_Object
{
	
    public function toOptionArray()
    {
        $options = array(
            "phpmail"   => Mage::helper('smtp')->__('PHP mail() function'),
            "google"   => Mage::helper('smtp')->__('Google Apps or Gmail'),
            "smtp"   => Mage::helper('smtp')->__('SMTP'),
//             "sendgrid"   => Mage::helper('smtp')->__('SendGrid'),
//             "mailup"   => Mage::helper('smtp')->__('MailUp'),
//             "ses"   => Mage::helper('smtp')->__('Amazon SES'),
            "disabled"   => Mage::helper('smtp')->__('Magento default (disable SMTP module)')

        );
        return $options;
    }
}