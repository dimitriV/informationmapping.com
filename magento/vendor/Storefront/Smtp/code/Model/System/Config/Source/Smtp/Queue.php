<?php

/**
 * Queue usage options
 */
class Storefront_Smtp_Model_System_Config_Source_Smtp_Queue {

	public function toOptionArray () {
		return array(
				'default' => Mage::helper('adminhtml')->__('Default'),
				'never' => Mage::helper('adminhtml')->__('Never')
		);
	}
}