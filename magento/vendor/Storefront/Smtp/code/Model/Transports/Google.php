<?php


class Storefront_Smtp_Model_Transports_Google extends Storefront_Smtp_Model_Transports_Basesmtp {

    public function getName($storeId) {
        return "Google Apps/Gmail";
    }
    public function getEmail($storeId) {
        return Mage::helper('smtp')->getGoogleAppsEmail($storeId);
    }
    public function getPassword($storeId) {
        return Mage::helper('smtp')->getGoogleAppsPassword($storeId);
    }
    public function getHost($storeId) {
        return "smtp.gmail.com";
    }
    public function getPort($storeId) {
        return 587;
    }
    public function getAuth($storeId) {
        return 'login';
    }
    public function getSsl($storeId) {
        return 'tls';
    }
}
