<?php
/**
 *
 * Amazon SES API Transport
 *
 * Big thanks to Christopher Valles
 * https://github.com/christophervalles/Amazon-SES-Zend-Mail-Transport
 *
 * 
 */

class Storefront_Smtp_Model_Transports_Ses {


    public function getTransport($storeId) {

        $_helper = Mage::helper('smtp');
        $_helper->log("Getting Amazon SES Transport");

        $path = Mage::getModuleDir('', 'Storefront_Smtp');
        include_once $path . '/lib/AmazonSES.php';

        $emailTransport = new App_Mail_Transport_AmazonSES(
            array(
                'accessKey' => $_helper->getAmazonSESAccessKey($storeId),
                'privateKey' => $_helper->getAmazonSESPrivateKey($storeId)
            )
        );

        return $emailTransport;
    }
}
