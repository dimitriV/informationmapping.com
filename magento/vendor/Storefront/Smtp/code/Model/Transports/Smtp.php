<?php


class Storefront_Smtp_Model_Transports_Smtp extends Storefront_Smtp_Model_Transports_Basesmtp {

    public function getName($storeId) {
        return "Custom SMTP";
    }
    public function getEmail($storeId) {
        return Mage::helper('smtp')->getSMTPSettingsUsername($storeId);
    }
    public function getPassword($storeId) {
        return Mage::helper('smtp')->getSMTPSettingsPassword($storeId);
    }
    public function getHost($storeId) {
        return Mage::helper('smtp')->getSMTPSettingsHost($storeId);
    }
    public function getPort($storeId) {
        return Mage::helper('smtp')->getSMTPSettingsPort($storeId);
    }
    public function getAuth($storeId) {
        return Mage::helper('smtp')->getSMTPSettingsAuthentication($storeId);
    }
    public function getSsl($storeId) {
        return Mage::helper('smtp')->getSMTPSettingsSSL($storeId);
    }
}