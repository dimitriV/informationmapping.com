<?php


class Storefront_Smtp_Model_Transports_Mailup extends Storefront_Smtp_Model_Transports_Basesmtp {

    public function getName($storeId) {
        return "MailUp";
    }
    public function getEmail($storeId) {
        return Mage::helper('smtp')->getMailUpUsername($storeId);
    }
    public function getPassword($storeId) {
        return Mage::helper('smtp')->getMailUpPassword($storeId);
    }
    public function getHost($storeId) {
        return "in.smtpok.com";
    }
    public function getPort($storeId) {
        return 587;
    }
    public function getAuth($storeId) {
        return 'login';
    }
    public function getSsl($storeId) {
        return 'tls';
    }
}