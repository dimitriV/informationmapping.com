<?php


class Storefront_Smtp_Model_Transports_Sendgrid extends Storefront_Smtp_Model_Transports_Basesmtp {

    public function getName($storeId) {
        return "Sendgrid";
    }
    public function getEmail($storeId) {
        return Mage::helper('smtp')->getSendGridEmail($storeId);
    }
    public function getPassword($storeId) {
        return Mage::helper('smtp')->getSendGridPassword($storeId);
    }
    public function getHost($storeId) {
        return "smtp.sendgrid.net";
    }
    public function getPort($storeId) {
        return 587;
    }
    public function getAuth($storeId) {
        return 'login';
    }
    public function getSsl($storeId) {
        return 'tls';
    }
}