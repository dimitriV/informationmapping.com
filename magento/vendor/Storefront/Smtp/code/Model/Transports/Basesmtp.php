<?php
/**
 *
 * Abstract base class for all SMTP-based transports
 *
 * 
 */

class Storefront_Smtp_Model_Transports_Basesmtp {


    public function getTransport($storeId) {

        $_helper = Mage::helper('smtp');

        $name = $this->getName($storeId);
        $email = $this->getEmail($storeId);
        $password = $this->getPassword($storeId);
        $host = $this->getHost($storeId);
        $port = $this->getPort($storeId);
        $auth = $this->getAuth($storeId);
        $ssl = $this->getSsl($storeId);

        $_helper->log("Using $name Transport.");

        $config = array();

        if ($auth != "none") {
            $config['auth'] = $auth;
            $config['username'] = $email;
            $config['password'] = $password;
        }

        if ($port) {
            $config['port'] = $port;
        }

        if ($ssl != "none" ) {
            $config['ssl'] = $ssl;
        }

        $emailTransport = new Zend_Mail_Transport_Smtp($host, $config);
        return $emailTransport;
    }
}
