<?php
/**
 * This class wraps the Basic email sending functionality
 * If SMTP is enabled it will send emails using the given
 * configuration.
 */

class Storefront_Smtp_Model_Email extends Mage_Core_Model_Email {


     public function send() {
        /* @var $_helper Storefront_Smtp_Helper_Data */
        $_helper = Mage::helper('smtp');

        // If it's not enabled, just return the parent result.
        if (!$_helper->isEnabled()) {
            return parent::send();
        }

        if (Mage::getStoreConfigFlag('system/smtp/disable')) {
            return $this;
        }

        $mail = new Zend_Mail('utf-8');

        if (strtolower($this->getType()) == 'html') {
            $mail->setBodyHtml($this->getBody());
        } else {
            $mail->setBodyText($this->getBody());
        }

        $mail->setFrom($this->getFromEmail(), $this->getFromName())
            ->addTo($this->getToEmail(), $this->getToName())
            ->setSubject($this->getSubject());

        $transport = new Varien_Object(); // for observers to set if required
        Mage::dispatchEvent('smtp_before_send', array(
            'mail' => $mail,
            'email' => $this,
            'transport' => $transport
        ));

        if ($transport->getTransport()) { // if set by an observer, use it
            $mail->send($transport->getTransport());
        } else {
            $mail->send();
        }

        Mage::dispatchEvent('smtp_after_send', array(
            'to' => $this->getToName(),
            'subject' => $this->getSubject(),
            'template' => "n/a",
            'html' => (strtolower($this->getType()) == 'html'),
            'email_body' => $this->getBody()));

        return $this;
    }
}
