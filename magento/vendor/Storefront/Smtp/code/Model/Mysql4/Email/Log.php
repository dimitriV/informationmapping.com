<?php
/**
 *
 *
 * 
 */

class Storefront_Smtp_Model_Mysql4_Email_Log extends Mage_Core_Model_Mysql4_Abstract {
    /**
     * Resource model initialization
     */
    protected function _construct() {
        $this->_init('smtp/email_log', 'email_id');
    }

    /**
     * Clean up log table.
     * 
     * @param int|null $lifetime Lifetime of entries in days
     * @return Storefront_Smtp_Model_Mysql4_Email_Log
     */
    public function clean($lifetime = null) {
        if (!Mage::helper('smtp')->isLogCleaningEnabled()) {
            return $this;
        }
        if (is_null($lifetime)) {
            $lifetime = Mage::helper('smtp')->getLogLifetimeDays();
        }
        $cleanTime = $this->formatDate(time() - $lifetime * 3600 * 24, false);

        $readAdapter    = $this->_getReadAdapter();
        $writeAdapter   = $this->_getWriteAdapter();

        while (true) {
            $select = $readAdapter->select()
                ->from(
                    $this->getMainTable(),
                    $this->getIdFieldName()
                )
                ->where('log_at < ?', $cleanTime)
                ->order('log_at ASC')
                ->limit(100);

            $logIds = $readAdapter->fetchCol($select);
            
            if (!$logIds) {
                break;
            }

            $condition = array($this->getIdFieldName() . ' IN (?)' => $logIds);

            // remove email log entries
            $writeAdapter->delete($this->getMainTable(), $condition);
        }
        
        return $this;
    }
}