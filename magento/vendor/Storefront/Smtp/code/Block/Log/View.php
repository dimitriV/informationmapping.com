<?php


class Storefront_Smtp_Block_Log_View extends Mage_Catalog_Block_Product_Abstract {
	 
    public function __construct() {
        parent::__construct();
        $this->setTemplate('smtp/view.phtml');
        $this->setEmailId($this->getRequest()->getParam('email_id', false));
    }


    public function getEmailData() {
        if( $this->getEmailId()) {
	        return Mage::getModel('smtp/email_log')
	        			->load($this->getEmailId());
        } else {
        	throw new Exception("No Email Id given");
        }
    }

}