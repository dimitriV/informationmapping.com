<?php
class Storefront_Commercebug_Block_Html extends Mage_Core_Block_Template {
	
	
	public function fetchView($fileName) {
		$scriptPath = realpath ( dirname ( __FILE__ ) . DS.'..'.DS.'static' );
		$this->_viewDir = $scriptPath;	    
		return parent::fetchView ( $this->getTemplate () );
	}
	
	public function getPathSkin() {
// 		$url = Mage::getUrl('');
		
// 		$useStoreCodeInUrl = Mage::getStoreConfigFlag('web/url/use_store');
// 		if($useStoreCodeInUrl){
// 			$index = strrpos($url, '/', -2);
// 			$url = substr($url, 0, $index).'/';
// 		}
		
		$url = '/magento/vendor/Storefront/Commercebug/skin/frontend/commercebug';
		return $url;
	}


	public function fetchUseKeyboardShortcuts() {
		return Mage::getStoreConfig ( 'commercebug/options/keyboard_shortcuts' );
	}


	public function getLayout() {
		return Mage::getSingleton ( 'core/layout' );
	}
}