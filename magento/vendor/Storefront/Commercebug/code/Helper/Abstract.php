<?php
abstract class Storefront_Commercebug_Helper_Abstract extends Storefront_VendorLoader_Helper_Abstract {
	public function isModuleOutputEnabled($moduleName = null) {
		if (is_callable ( array (Mage::helper ( 'core' ), 'isModuleOutputEnabled' ) )) {
			return parent::isModuleOutputEnabled ();
		}
		
		if ($moduleName === null) {
			$moduleName = $this->_getModuleName ();
		}
		if (Mage::getStoreConfigFlag ( 'advanced/modules_disable_output/' . $moduleName )) {
			return false;
		}
		return true;
	}
	public function getModuleName() {
		return 'Storefront_Commercebug';
	}
}