<?php

class Storefront_GuestToCustomer_Helper_Data extends Storefront_VendorLoader_Helper_Abstract {

    public function getModuleName(){
        return 'Storefront_GuestToCustomer';
    }

    public function isGuestToCustomerEnabled($storeId = 0){
        $r = Mage::getStoreConfigFlag('customer/guest_to_customer/enabled',$storeId);
        return $r;
    }

    public function getNewCustomerPassword($email){
        $password = md5('imi'.$email);
        return $password;
    }

}