<?php
class Storefront_GuestToCustomer_Model_Cron {


    public function createAccountForGuest() {
        /* @var $helper Storefront_GuestToCustomer_Helper_Data */
        $helper = Mage::helper('guesttocustomer');

        $oneWeekAgo = new Zend_Date();
        $oneWeekAgo->sub(1, Zend_Date::WEEK);
        $oneWeekAgo = $oneWeekAgo->get('YYYY-MM-dd HH:mm:ss');

        $orderCollection = Mage::getSingleton('sales/order')->getCollection();
        $orderCollection->setPageSize(5);
        $orderCollection->addAttributeToSelect('*');
        $orderCollection->addFieldToFilter('customer_id', array('null' => true));
        $orderCollection->addFieldToFilter('state', 'complete');
       /* $orderCollection->addFieldToFilter(
            array('customer_email'), // columns
            array( // conditions
                array( // conditions for field_1
                    array('in' => array('mkr@ikadan.dk',
                        'Christine.Kolentsis@peelpolice.ca',
                        'jshearer@praesidiuminc.com',
                        'smeadows@focushomehealth.com',
                        'amy.sigler@roche.com',
                        'licensing@softwareone.com',
                        'lindsey.goularte@roberthalf.com',
                        'mcox.wallace@gmail.com',
                        'carol.tewes@acegroupllc.com',
                        'elio.berardocco@uantwerpen.be',
                        'CLIAV@INTRACOM-TELECOM.COM',
                        'license.dk@crayon.com',
                        'saragunderman@vpi-inc.com',
                        'meyerr@careoregon.org',

                    )
                    )
                )
            )
        ); */


        //$orderCollection->addFieldToFilter('customer_email', 'cbristol@praesidiuminc.com');
        $orderCollection->addFieldToFilter('created_at', array('gt' => $oneWeekAgo));
        $orderCollection->addAttributeToSort('entity_id', 'desc');

        foreach ($orderCollection as $order) {
            /* @var $order Mage_Sales_Model_Order */

            $incrementId = $order->getIncrementId();
            $postsale = Mage::getModel('ogone/postsale_request')->load($incrementId, 'orderID');

            if(!$helper->isGuestToCustomerEnabled($order->getStoreId())){
                return;
            }

            $sendNewCustomerEmail = false;

            $email = $order->getCustomerEmail();
            $password = $helper->getNewCustomerPassword($email);

            try {
                $storeId = $order->getStoreId();
                $websiteId = Mage::app()->getStore($storeId)->getWebsiteId();



                /* @var $customer Mage_Customer_Model_Customer */
                $customer = Mage::getModel('customer/customer');
                $customer->setWebsiteId($websiteId);
                $customer->loadByEmail($email);

                if (!$customer->getId()) {
                    // Create new customer

                    $customer->setStore($order->getStore());
                    $customer->setEmail($email);
                    $customer->setFirstname($order->getBillingAddress()->getFirstname());
                    $customer->setLastname($order->getBillingAddress()->getLastname());
                    $customer->setPassword($password);

                    $customer->setPartner(5); // 5 = None

                    $customer->save();

                    $sendNewCustomerEmail = true;

                    // Add Ogone Alias to new Customer
                    if($postsale->getData('ALIAS') !== 'null') {
                        $expiryDate = $postsale->getData('ED');

                        // Reformat expiry date to 'yyyy-mm-dd'
                        $expiryDate = '20' . substr($expiryDate, 2, 4) . '-' . substr($expiryDate, 0, 2) . '-01';
                        $nowString = date('Y-m-d H:i:s');
                        $pspid = Mage::getStoreConfig('ogone/test_account/pspid', $storeId);
                        $environment = Mage::getStoreConfig('ogone/settings/environment', $storeId);
                        $alias = Mage::getModel('payogonealias/alias');
                        $alias->setIsPrimary('1');
                        $alias->setEnvironment($environment);
                        $alias->setPspid($pspid);
                        $alias->setLastUsedAt($nowString);
                        $alias->setCreatedAt($nowString);
                        $alias->setCustomerId($customer->getId());
                        $alias->setExpiryDate($expiryDate);
                        $alias->setCard($order->getPayment()->getCcNumberEnc());
                        $alias->setPaymentMethod($order->getPayment()->getMethod());
                        $alias->setAlias($postsale->getData('ALIAS'));

                        $alias->save();

                        $message = $helper->__('Customer found with ALIAS: %s', $postsale->getData('ALIAS'));
                        Mage::log($message, null, 'guest-to-customer.log');
                    }

                }

                $order->setCustomerId($customer->getId());
                $order->save();


                // Check or attach billing/shipping address
                if (!$customer->getDefaultBillingAddress() || !$customer->getDefaultShippingAddress()) {

                    $defaultBilling = '0';
                    if (!$customer->getDefaultBillingAddress()) {
                        $defaultBilling = '1';
                    }

                    $defaultShipping = '0';
                    if (!$customer->getDefaultShippingAddress()) {
                        $defaultShipping = '1';
                    }

                    $billingAddress = $order->getBillingAddress();

                    // Create a new address
                    $address = Mage::getModel("customer/address");
                    $address->setCustomerId($customer->getId());
                    $address->setFirstname($billingAddress->getFirstname());
                    $address->setMiddleName($billingAddress->getMiddlename());
                    $address->setLastname($billingAddress->getLastname());
                    $address->setCountryId($billingAddress->getCountryId());
                    $address->setRegionId($billingAddress->getRegionId());
                    $address->setPostcode($billingAddress->getPostcode());
                    $address->setCity($billingAddress->getCity());
                    $address->setTelephone($billingAddress->getTelephone());
                    $address->setFax($billingAddress->getFax());
                    $address->setCompany($billingAddress->getCompany());
                    $address->setVatId($billingAddress->getVatId());
                    $address->setStreet($billingAddress->getStreet());
                    $address->setIsDefaultBilling($defaultBilling);
                    $address->setIsDefaultShipping($defaultShipping);
                    $address->setSaveInAddressBook('1');

                    $address->save();
                }

                if($sendNewCustomerEmail){
                    $this->_sendNewAccountEmail($customer);
                }

                $message = $helper->__('Customer successfully attached to order: %s. Customer found with email: %s and ID: %s', $order->getId(), $email, $customer->getId());
                Mage::log($message, null, 'guest-to-customer.log');


            } catch (Exception $ex) {
                Mage::logException($ex);
            }


        }
    }

    protected function _sendNewAccountEmail(Mage_Customer_Model_Customer $customer){
        //$customer->sendNewAccountEmail('registered', '', $storeId);

        $store = $customer->getStore();
        $storeId = $store->getId();

        $appEmulation = Mage::getSingleton('core/app_emulation');

        //Start environment emulation of the specified store
        $initialEnvironmentInfo = $appEmulation->startEnvironmentEmulation($store->getId());

        $templateVars = array(
            'customer' => $customer,
            'store' => $store,
            'create_password_url' => $this->_getPasswordResetLink($customer),
        );

        /* @var $mailTemplate Mage_Core_Model_Email_Template */
        $mailTemplate = Mage::getModel('core/email_template');
        $mailTemplate->setDesignConfig(array(
            'area' => 'frontend'
        ));

        $sender = 'support';

        if (Mage::getIsDeveloperMode()) {
            $emailTo = 'wouter.samaey@storefront.be';
        } else {
            $emailTo = $customer->getEmail();
        }


        $template = Mage::getStoreConfig('customer/guest_to_customer/email_template', $storeId);
        if(!$template){
            $template = 'guest_to_customer_new_customer';
        }

        $bcc = trim(Mage::getStoreConfig('customer/guest_to_customer/bcc', $storeId));
        if($bcc){
            $bcc = explode(';',$bcc);
            $mailTemplate->addBcc($bcc);
        }
        $mailTemplate->sendTransactional($template, $sender, $emailTo, null, $templateVars);


        //Stop environment emulation and restore original store
        $appEmulation->stopEnvironmentEmulation($initialEnvironmentInfo);

        if (!$mailTemplate->getSentSuccess()) {
            Mage::throwException('Could not send new customer from guest order email for customer ' . $customer->getId());
        }

    }

    protected function _getPasswordResetLink(Mage_Customer_Model_Customer $customer) {
        /* @var $customerHelper Mage_Customer_Helper_Data */
        $customerHelper = Mage::helper('customer');
        $newResetPasswordLinkToken =  $customerHelper->generateResetPasswordLinkToken();
        $customer->changeResetPasswordLinkToken($newResetPasswordLinkToken);
        $customer->save();

        $url = Mage::getUrl("customer/account/resetpassword/", array('_store' => $customer->getStoreId())).'?id='.urlencode($customer->getId()).'&token=' . urlencode($customer->getRpToken());
        return $url;
    }


}