<?php

class Storefront_Subscriptions_Adminhtml_RenewalController extends Storefront_Subscriptions_Adminhtml_CrudController {

    protected $_activeMenu = 'subscriptions';
    protected $_helperName = 'subscriptions';
    protected $_objectNamePlural = 'Renewals';
    protected $_objectName = 'Renewal';
    protected $_modelGroupedName = 'subscriptions/renewal';
    protected $_modelPk = 'id';


    protected function _redirectAfterSave($model, $isNewModel){
        $back = $this->getRequest()->getParam('back');

        if($back){
            $this->_redirect ( '*/*/'.$back, array('id' => $model->getData($this->_modelPk)) );
        }else{
            $this->_redirect ( 'subscriptions/adminhtml_subscription/edit', array('id' => $model->getSubscriptionId()) );
        }
    }

    public function activateAction(){



        /* @var $renewal Storefront_Subscriptions_Model_Renewal */
        $renewal = $this->_initModel();

        try{
            $renewal->startRenew();
            Mage::getSingleton ( 'adminhtml/session' )->addSuccess ( Mage::helper ( $this->_helperName )->__ ( 'Renewal %s has been activated.', $renewal->getId() ) );

        }catch(Exception $ex){
            Mage::logException($ex);
            Mage::getSingleton ( 'adminhtml/session' )->addError ( $ex->getMessage() );
        }

        $this->_redirectAfterSave($renewal, false);
    }

}