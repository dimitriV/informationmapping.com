<?php

class Storefront_Subscriptions_Adminhtml_SubscriptionController extends Storefront_Subscriptions_Adminhtml_CrudController {

    protected $_activeMenu = 'subscriptions';
    protected $_helperName = 'subscriptions';
    protected $_objectNamePlural = 'Subscriptions';
    protected $_objectName = 'Subscription';
    protected $_modelGroupedName = 'subscriptions/subscription';
    protected $_modelPk = 'id';


    protected function _renewSubscription($id){
        $subscription = Mage::getModel($this->_modelGroupedName)->load($id);
        if ($subscription->getId()) {

            /* @var $subscription Storefront_Subscriptions_Model_Subscription */
            $errors = $subscription->getAutoRenewValidationErrors();
            if (count($errors) > 0) {
                $errorMsg = implode(', ', $errors);
                Mage::getSingleton('adminhtml/session')->addError($this->__('Cannot renew subscription %s: %s', $id, $errorMsg));
            } else {
                try {
                    $subscription->renew();
                    Mage::getSingleton('adminhtml/session')->addSuccess(Mage::helper('adminhtml')->__('Subscription %s was succesfully renewed', $id));

                } catch (Exception $e) {
                    Mage::getSingleton('adminhtml/session')->addError($this->__('Cannot renew subscription %s: %s', $id, $e->getMessage()));
                }
            }

        } else {
            Mage::getSingleton('adminhtml/session')->addError($this->__('Subscription %s does not exist.', $id));
        }
    }

    public function renewalsgridAction() {
        $this->loadLayout();
        $this->renderLayout();
    }

    public function renewAction() {
        $id = $this->getRequest()->getParam('id');
        $this->_renewSubscription($id);
        $this->_redirect('*/*/index');
    }

    public function setautorenewAction() {
        $flag = (bool)$this->getRequest()->getParam('value');
        $subscriptionId = $this->getRequest()->getParam('id');

        $subscription = Mage::getModel('subscriptions/subscription')->load($subscriptionId);

        $session = Mage::getSingleton('adminhtml/session');


        if ($subscription->getId()) {
            $subscription->setAutoRenew($flag);
            $subscription->save();
            if ($flag) {
                $session->addSuccess($this->__('Subscription ' . $subscriptionId . ' is set to auto-renew'));
            } else {
                $session->addSuccess($this->__('Subscription ' . $subscriptionId . ' is set to not auto-renew'));
            }
        } else {
            $session->addError($this->__('Cannot find subscription ' . $subscriptionId));
        }

        $this->_redirect('*/*/');

    }

    public function massEnableAutoRenewAction() {
        $this->_massChangeAutoRenew(1);
    }

    public function massDisableAutoRenewAction() {
        $this->_massChangeAutoRenew(0);
    }

    protected function _massChangeAutoRenew($flag) {
        $ids = $this->getRequest()->getParam('ids');
        if (!is_array($ids)) {
            Mage::getSingleton('adminhtml/session')->addError(Mage::helper('adminhtml')->__('Please select one or more items.'));
        } else {
            try {
                foreach ($ids as $id) {
                    $model = Mage::getModel($this->_modelGroupedName)->load($id);
                    $model->setAutoRenew($flag);
                    $model->save();
                }
                Mage::getSingleton('adminhtml/session')->addSuccess(Mage::helper('adminhtml')->__('%d subscriptions set to auto renew', count($ids)));
            } catch (Exception $e) {
                Mage::getSingleton('adminhtml/session')->addError($e->getMessage());
            }
        }

        $this->_redirect('*/*/index');
    }

    public function massRenewAction() {
        $ids = $this->getRequest()->getParam('ids');
        if (!is_array($ids)) {
            Mage::getSingleton('adminhtml/session')->addError(Mage::helper('adminhtml')->__('Please select one or more items.'));
        } else {

            foreach ($ids as $id) {
                $this->_renewSubscription($id);
            }


        }

        $this->_redirect('*/*/index');
    }

    public function massSendRenewReminderEmailAction(){
        $ids = $this->getRequest()->getParam('ids');
        if (!is_array($ids)) {
            Mage::getSingleton('adminhtml/session')->addError(Mage::helper('adminhtml')->__('Please select one or more items.'));
        } else {

            $numSent = 0;
            $numErrors = 0;

            foreach ($ids as $id) {
                $subscription = Mage::getModel($this->_modelGroupedName)->load($id);
                if ($subscription->getId()) {
                    /* @var $subscription Storefront_Subscriptions_Model_Subscription */

                    try{
                        $subscription->sendRenewReminderEmail();
                        $numSent++;
                    }catch(Exception $ex){
                        Mage::logException($ex);
                        $numErrors++;
                    }
                }
            }

            if($numSent > 0) {
                Mage::getSingleton('adminhtml/session')->addSuccess(Mage::helper('adminhtml')->__('%d renew reminder emails sent', $numSent));
            }
            if($numErrors > 0){
                Mage::getSingleton('adminhtml/session')->addSuccess(Mage::helper('adminhtml')->__('%d renew reminder emails count not be sent', $numErrors));
            }


        }

        $this->_redirect('*/*/index');
    }

    public function massDisableAction() {
        $ids = $this->getRequest()->getParam('ids');
        if (!is_array($ids)) {
            Mage::getSingleton('adminhtml/session')->addError(Mage::helper('adminhtml')->__('Please select one or more items.'));
        } else {
            try {
                foreach ($ids as $id) {
                    $model = Mage::getModel($this->_modelGroupedName)->load($id);
                    $model->setDisabled('1');
                    $model->save();
                }
                Mage::getSingleton('adminhtml/session')->addSuccess(Mage::helper('adminhtml')->__('%d subscriptions are now hidden', count($ids)));
            } catch (Exception $e) {
                Mage::getSingleton('adminhtml/session')->addError($e->getMessage());
            }
        }

        $this->_redirect('*/*/index');
    }

}