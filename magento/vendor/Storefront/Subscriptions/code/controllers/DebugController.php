<?php

class Storefront_Subscriptions_DebugController extends Mage_Core_Controller_Front_Action {

    public function showrenewablesubscriptionsAction(){
        /* @var $helper Storefront_Subscriptions_Helper_Data */
        $helper = Mage::helper('subscriptions');

        $allStores = Mage::app()->getStores();



        $body = '';

        foreach ($allStores as $storeId => $store) {
            if (Mage::getStoreConfigFlag('subscriptions/general/enable_auto_renew', $storeId)) {
                $subscriptions = $helper->getSubscriptionsForImmediateAutoRenew($storeId);

                foreach($subscriptions as $subscription){
                    $body .= $subscription->getId()."\n";
                }
            }
        }

        $this->getResponse()->setHeader('Content-Type', 'text/plain');
        $this->getResponse()->setBody($body);

    }

    public function emailreminderAction() {
        /* @var $cron Storefront_Subscriptions_Model_Cron */
        $cron = Mage::getSingleton('subscriptions/cron');

        $cron->sendRenewalEmails();

        die('DONE');
    }

    public function renewAction() {
        /* @var $cron Storefront_Subscriptions_Model_Cron */
        $cron = Mage::getSingleton('subscriptions/cron');

        $cron->autoRenewOnExpiryDay();

        die('DONE');
    }

    public function refreshstatusAction() {
        $col = Mage::getResourceModel('subscriptions/subscription_collection');

        foreach ($col as $subscription) {
            $subscription->refreshStatus();
            $subscription->save();
        }

        die('DONE');
    }


    public function senderrormailAction() {
        $subscriptionId = $this->getRequest()->getParam('id');

        /* @var $helper Storefront_Subscriptions_Helper_Data */
        $helper = Mage::helper('subscriptions');

        $subscription = $helper->getSubscriptionById($subscriptionId);

        if ($subscription && $subscription->getId()) {

            $subscription->sendErrorMailToCustomer();
            try {
                Mage::throwException('Dummy exception for testing');
            } catch (Exception $e) {
                $subscription->sendErrorMailToShopkeeper($e);
            }

            die('EMAILS SENT');
        }
    }
}
