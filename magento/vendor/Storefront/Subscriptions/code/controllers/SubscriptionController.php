<?php

class Storefront_Subscriptions_SubscriptionController extends Mage_Core_Controller_Front_Action {

    /**
     * Action predispatch
     *
     * Check customer authentication for some actions
     */
    public function preDispatch() {
        parent::preDispatch();
        if (!Mage::getSingleton('customer/session')->authenticate($this)) {
            $this->setFlag('', 'no-dispatch', true);
        }
    }

    public function indexAction() {

// 		$this->_title('');
        $this->_title($this->__('My Subscriptions'));

        /* @var $helper Storefront_Subscriptions_Helper_Data */
        $helper = Mage::helper('subscriptions');
        $subscriptions = $helper->getSubscriptions();

        if ($subscriptions->count() < 2) {
            // Forward to detail view, there are not enough subscriptions to use this list view
            $this->_redirect('*/*/view');
        } else {

            $this->loadLayout();
            $this->_initLayoutMessages('customer/session');
            $this->renderLayout();

        }
    }

    public function viewAction() {

        $this->_title($this->__('My Subscriptions'));
        $this->loadLayout();

        /* @var $helper Storefront_Subscriptions_Helper_Data */
        $helper = Mage::helper('subscriptions');

        $breadcrumbs = $this->getLayout()->getBlock('breadcrumbs');
        if ($this->getRequest()->getParam('id')) {
            // Only 1 is shown
            $breadcrumbs->addCrumb('subscriptions', array(
                'label' => $helper->__('Subscriptions'),
                'title' => $helper->__('Subscriptions'),
                'link' => $breadcrumbs->getUrl('subscriptions/subscription/')
            ));
            $breadcrumbs->addCrumb('subscriptions', array(
                'label' => $helper->__('Subscription Details'),
                'title' => $helper->__('Subscription Details')
            ));
        } else {
            // Show subscription list
            $breadcrumbs->addCrumb('subscriptions', array(
                'label' => $helper->__('Subscriptions'),
                'title' => $helper->__('Subscriptions')
            ));
        }

        $this->_initLayoutMessages('customer/session');
        $this->renderLayout();
    }

    public function licensehistoryAction() {
        $this->_title($this->__('My Subscriptions'));
        $this->_title($this->__('License History'));


        $subscriptionId = $this->getRequest()->getParam('id');
        $helper = Mage::helper('subscriptions');

        if ($subscriptionId) {
            // Show 1
            $subscription = $helper->getSubscriptionById($subscriptionId);
            if ($subscription->getId()) {
                Mage::register('current_subscription', $subscription);
                $this->loadLayout();

                $this->renderLayout();

                return;
            }
        }

        $this->_redirect('*/*/');

    }

    public function confirmAction() {

        $subscriptionId = $this->getRequest()->getParam('id');

        /* @var $helper Storefront_Subscriptions_Helper_Data */
        $helper = Mage::helper('subscriptions');

        $subscription = $helper->getSubscriptionById($subscriptionId);

        /* @var $session Mage_Customer_Model_Session */
        $session = Mage::getSingleton('customer/session');

        if ($subscription && $subscription->getId()) {


            if ($subscription->canRenew()) {
                Mage::register('current_subscription', $subscription);

                $this->_title($this->__('Confirm renewal for subscription'));
                $this->loadLayout();
                $this->renderLayout();
            } else {
                $session->addError($this->__('Subscription "%s" cannot be renewed.', $subscriptionId));
                $this->_redirect('subscriptions/subscription/view', array('id' => $subscriptionId));
            }

        } else {

            $session->addError($this->__('Subscription "%s" not found.', $subscriptionId));
            $this->_redirect('subscriptions/subscription/');
        }

    }

    protected function _handleChangeAutoRenewStatus($flag) {
        $subscriptionId = $this->getRequest()->getParam('id');

        /* @var $helper Storefront_Subscriptions_Helper_Data */
        $helper = Mage::helper('subscriptions');

        $subscription = $helper->getSubscriptionById($subscriptionId);

        /* @var $session Mage_Customer_Model_Session */
        $session = Mage::getSingleton('customer/session');

        if ($subscription && $subscription->getId()) {

            $subscription->setAutoRenew((int)$flag);
            $subscription->save();
            if ($flag) {
                $session->addSuccess($this->__('Auto-renew is now enabled for %s', $subscription->getName()));
            } else {
                $session->addSuccess($this->__('Auto-renew is now disabled for %s', $subscription->getName()));
            }

            $this->_redirect('subscriptions/subscription/');
        } else {
            $this->_forward('noRoute');
        }
    }

    public function disableautorenewAction() {
        $this->_handleChangeAutoRenewStatus(false);
    }

    public function enableautorenewAction() {
        $this->_handleChangeAutoRenewStatus(true);
    }

    public function renewedsuccessAction() {
        $subscriptionId = $this->getRequest()->getParam('id');

        /* @var $helper Storefront_Subscriptions_Helper_Data */
        $helper = Mage::helper('subscriptions');

        $subscription = $helper->getSubscriptionById($subscriptionId);

        /* @var $session Mage_Customer_Model_Session */
        $session = Mage::getSingleton('customer/session');

        if ($subscription) {
            $this->_initLayoutMessages('customer/session');

            $this->loadLayout();
            $this->renderLayout();
        } else {
            /* @var $session Mage_Customer_Model_Session */
            $session = Mage::getSingleton('customer/session');
            $session->addError($this->__('Subscription "%s" not found.', $subscriptionId));
            $this->_redirect('subscriptions/subscription/');
        }
    }


    public function renewAction() {
        $subscriptionId = $this->getRequest()->getParam('id');

        /* @var $helper Storefront_Subscriptions_Helper_Data */
        $helper = Mage::helper('subscriptions');

        $subscription = $helper->getSubscriptionById($subscriptionId);

        /* @var $session Mage_Customer_Model_Session */
        $session = Mage::getSingleton('customer/session');


        if ($subscription && $subscription->getId()) {

            $product = $subscription->getRenewProduct();

            if ($product && $product->getId()) {

                try {
                    $order = $subscription->renew();

                    if ($order) {
                        $session->addSuccess($this->__('Your subscription to %s has been extended.', $subscription->getName()));

                        $this->_redirect('subscriptions/subscription/renewedsuccess', array('id' => $subscription->getId()));
                    }

                } catch (Exception $e) {
                    // If auto-renew doesn't work, send to cart
                    // $this->_clearCart();
                    $this->_fillCartWithProduct($product);
                    $this->_redirect('checkout/cart');
                }

            } else {
                $session->addError($this->__('Your subscription cannot be renewed. A renewal is not available for purchase.'));
                $this->_redirect('subscriptions/subscription/');
            }

        } else {
            $session->addError($this->__('Your subscription cannot be found.'));
            $this->_redirect('subscriptions/subscription/');
        }
    }

    /**
     *
     * @return Mage_Sales_Model_Quote
     */
    protected function _getQuote() {
        /* @var $checkoutHelper Mage_Checkout_Helper_Data */
        $checkoutHelper = Mage::helper('checkout');

        $quote = $checkoutHelper->getQuote();

        return $quote;
    }

    protected function _fillCartWithProduct(Mage_Catalog_Model_Product $product) {
        $this->_clearCart();
        $this->_addProductToCart($product);
    }

    protected function _addProductToCart(Mage_Catalog_Model_Product $product) {
        // Get customer session
        $session = Mage::getSingleton('customer/session');

        /* @var $cart Mage_Checkout_Model_Cart */
        // $cart = Mage::getSingleton('checkout/cart');

        // $cart->getQuote()->delete();

        $cart = Mage::getSingleton('checkout/cart');

        $cart->init();

        $cart->addProduct($product, array('qty' => '1'));



        $cart->save();

        // // Add a product with custom options
        // $productInstance = Mage::getModel('catalog/product')->load($productId);
        // $param = array(
        // 'product' => $productInstance->getId(),
        // 'qty' => 1,
        // 'options' => array(
        // 234 => 'A value' // Custom option with id: 234
        // )
        // );
        // $request = new Varien_Object();
        // $request->setData($param);
        // $cart->addProduct($productInstance, $request);

        // Set shipping method
        // $quote = $cart->getQuote();
        // $shippingAddress = $quote->getShippingAddress();
        // $shippingAddress->setShippingMethod('flatrate_flatrate')->save();

        // update session
        // $session = Mage::getSingleton('customer/session');
        $session->setCartWasUpdated(true);


        Mage::dispatchEvent('checkout_cart_add_product_complete', array(
                'product' => $product,
                'request' => $this->getRequest(),
                'response' => $this->getResponse()
            ));

        // save the cart
        $cart->save();
//
//        $quote = Mage::getSingleton('checkout/session')->getQuote();
//        $quote->addProduct($product, $qty);
//
//        $quote->collectTotals()->save();

    }






    /**
     * Get one page checkout model
     *
     * @return Mage_Checkout_Model_Type_Onepage
     */
    protected function _getOnepage() {
        return Mage::getSingleton('checkout/type_onepage');
    }

    protected function _clearCart(Mage_Sales_Model_Order $order = null) {

        if ($order === null) {
            // Reset current cart
            /* @var $checkoutHelper Mage_Checkout_Helper_Data */
            $checkoutHelper = Mage::helper('checkout/cart');
            $quote = $checkoutHelper->getQuote();
            $quote->setIsActive(0)->save();
            Mage::getSingleton('checkout/session')->clear();
        }else{
            $quoteId = $order->getQuoteId();
            $quote = Mage::getModel('sales/quote')->load($quoteId);
            $quote->setIsActive(FALSE)->save();
        }
    }


    public function activationsavailableAction() {

        $this->_title($this->__('My Available Activations'));
        $this->loadLayout();

        /* @var $helper Storefront_Subscriptions_Helper_Data */
        $helper = Mage::helper('subscriptions');

        $breadcrumbs = $this->getLayout()->getBlock('breadcrumbs');

        $breadcrumbs->addCrumb('subscriptions', array(
            'label' => $helper->__('Subscriptions'),
            'title' => $helper->__('Subscriptions'),
            'link' => $breadcrumbs->getUrl('subscriptions/subscription/')
        ));
        $breadcrumbs->addCrumb('activations', array(
            'label' => $helper->__('My Available Activations'),
            'title' => $helper->__('My Available Activations')
        ));

        $this->renderLayout();
    }

    public function setautorenewAction(){
        $subscriptionId = $this->getRequest()->getParam('id');

        $subscription = Mage::getModel('subscriptions/subscription')->load($subscriptionId);

        $r = array();
        $success = true;
        if($subscription->getId()){

            // Check current user has access to this subscription
            /* @var $helper Storefront_Subscriptions_Helper_Data */
            $helper = Mage::helper('subscriptions');

            $mySubscriptions = $helper->getSubscriptions();

            $found = false;
            foreach($mySubscriptions as $mySubscription){
                if($mySubscription->getId() == $subscription->getId()){
                    $found = true;
                }
            }

            if(isset($_GET) && ($_GET['enable'] === '0' || $_GET['enable'] === '1')){
                $enable = $_GET['enable'];

                if($found){
                    $subscription->setAutoRenew((int) $enable);
                    $subscription->save();
                    $r['success'] = 'OK';
                }else{
                    $success = false;
                    $r['error'] = 'Access denied to this subscription';
                }
            }else{
                $success = false;
                $r['error'] = 'Missing or invalid parameter "enable"';
            }


        }else{
            $success = false;
            $r['error'] = 'Subscription not found';
        }

        if(!$success){
            $this->getResponse()->setHttpResponseCode(500);
        }

        $this->getResponse()->setHeader('Content-Type', 'application/json');
        $this->getResponse()->setBody(Zend_Json::encode($r));
    }
}
