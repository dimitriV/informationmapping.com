<?php
class Storefront_Subscriptions_Model_Subscription extends Mage_Core_Model_Abstract {

    const STATUS_EXPIRED = 'EXPIRED';
    const STATUS_WAIT_MANUAL_ACTIVATION_BY_ADMIN = 'WAIT_MANUAL_ACTIVATION_BY_ADMIN';
    const STATUS_WAIT_MANUAL_ACTIVATION_BY_CUSTOMER = 'WAIT_MANUAL_ACTIVATION_BY_CUSTOMER';
    const STATUS_WAIT_ON_INVOICE = 'WAIT_ON_INVOICE';
    const STATUS_ACTIVE = 'ACTIVE';

    // These fields are editable in Magento admin and need to be timezone managed
    protected $_userEnteredDateTimeFields = array('expires_on', 'started_on');

    protected $_product = null;

    public function _construct() {
        parent::_construct();
        $this->_init('subscriptions/subscription');
    }

    /**
     * Same as getData(), for datetime is converted to local timezone
     */
    public function getDataInLocalTimezone(){
        $data = $this->getData();

        $dateFields = $this->getUserEnteredDateTimeFields();

        if (is_array($dateFields) && count($dateFields)) {
            //$data           = $this->_filterDateTime($data, $dateFields);

            $gmt_timezone   = new DateTimeZone('Europe/London');
            $store_timezone = new DateTimeZone(Mage::getStoreConfig('general/locale/timezone'));

            foreach ($dateFields as $key) if (isset($data[$key])) {
                $dateTime = new DateTime($data[$key], $gmt_timezone);
                $dateTime->setTimezone($store_timezone);
                $data[$key] = $dateTime->format('Y-m-d H:i:s');
            }
        }

        return $data;
    }


    public function getUserEnteredDateTimeFields(){
        return $this->_userEnteredDateTimeFields;
    }

    public function getStoreId() {
        return $this->getOrderItem()->getStoreId();
    }

    public function getStore() {
        return Mage::app()->getStore($this->getStoreId());
    }

    /**
     * @return Mage_Sales_Model_Order_Item
     */
    public function getOrderItem() {
        $id = $this->getOrderItemId();
        $r = Mage::getModel('sales/order_item')->load($id);

        if ($r->getId()) {
            return $r;
        } else {
            return null;
        }
    }

    public function isDisabled() {
        return (bool)$this->getData('disabled');
    }

    public static function getStatuses() {
        $helper = Mage::helper('subscriptions');
        return array(
            self::STATUS_EXPIRED => $helper->__('Expired'),
            self::STATUS_WAIT_MANUAL_ACTIVATION_BY_ADMIN => $helper->__('Waiting activation by admin'),
            self::STATUS_WAIT_MANUAL_ACTIVATION_BY_CUSTOMER => $helper->__('Waiting activation by customer'),
            self::STATUS_ACTIVE => $helper->__('Active'),
        );
    }

    public function getProduct() {
        if($this->_product === null){
            $oi = $this->getOrderItem();
            $p = $oi->getProduct();

            // Reload the product, so we have ALL ATTRIBUTES!
            $this->_product = Mage::getModel('catalog/product')->setStoreId($this->getStoreId())->load($p->getId());
        }

        return $this->_product;
    }

    public function isExpired() {
        $expDate = $this->getExpiresOn();

        if ($expDate === null) {
            // Expiry is unknown, this is not the same as expired
            return false;
        } else {
            // String comparison is fine here
            if ($expDate < date('Y-m-d H:i:s')) {
                return true;
            }
        }
        return false;
    }

    /**
     * Start the subscription right now.
     * This is run "on invoice" or during "manual start"
     * @throws Exception
     */
    public function startSubscription() {
        $helper = Mage::helper('subscriptions');

        if($this->getStatus() === self::STATUS_ACTIVE){
            Mage::throwException($helper->__('Subscription %s is already active, so it cannot be activated again.', $this->getId()));

        }else if($this->getExpiryDate() !== null){
            Mage::throwException($helper->__('Subscription %s already has an expiry date, so it cannot be activated again.', $this->getId()));

        }else if($this->getStartDate() !== null){
            Mage::throwException($helper->__('Subscription %s already has a start date, so it cannot be activated again.', $this->getId()));
        }

        $this->setStatus(self::STATUS_ACTIVE);
        $this->setStartedOn(Varien_Date::now());

        $product = $this->getProduct();

        if(!$product->getSubscriptionStartFrom() || $product->getSubscriptionStartFrom() == Storefront_Subscriptions_Model_Source_Subscription_Startfrom::START_FROM_ACTIVATION_DATE) {
            // No value => default to "start from activation date"
            // From activation date = now

            $expiresDate = new Zend_Date();

        }elseif($product->getSubscriptionStartFrom() == Storefront_Subscriptions_Model_Source_Subscription_Startfrom::START_FROM_ORDER_DATE){
            $expiresDate = $this->getOrderItem()->getOrder()->getCreatedAtDate();

        }else{
            Mage::throwException($helper->__('Unknown "subscription_start_from" value (%s) for product %s', $product->getSubscriptionStartFrom(), $product->getId()));
        }

        if(!$product->getSubscriptionPeriod()){
            Mage::throwException($helper->__('Unknown "subscription_period" value (%s) for product %s', $product->getSubscriptionPeriod(), $product->getId()));
        }

        // Add period to get the real expiry
        $expiresDate->addMonth($product->getSubscriptionPeriod());
        $expiresString = $expiresDate->getIso();

        // Set value
        $this->setExpiresOn($expiresString);

        Mage::dispatchEvent('subscriptions_start_subscription', array('subscription' => $this));

        $this->save();
    }

    /**
     *
     * @return Mage_Customer_Model_Customer
     */
    public function getCustomer() {

        if ($this->hasData('customer_id')) {
            // If the model was loaded through a collection, this field will be available
            $customerId = $this->getCustomerId();
        } else {
            // If the modal was loaded directly, we need to look the customer up
            $order = $this->getOrderItem()->getOrder();
            $customerId = $order->getCustomerId();
        }

        if ($customerId) {
            $customer = Mage::getModel('customer/customer')->load($customerId);

            if ($customer->getId()) {
                return $customer;
            }
        }
        return null;

    }


//    public function getStatusCode () {
//        $now = new Zend_Date();
//        if ($now->isLater($this->getStartDate()) && $now->isEarlier($this->getExpiryDate())) {
//            return self::STATUS_ACTIVE;
//        } else {
//            return self::STATUS_EXPIRED;
//        }
//    }

    /**
     *
     * @return Zend_Date
     */
    public function getExpiryDate() {
        $isoString = $this->_getData('expires_on');

        if ($isoString) {
            $r = new Zend_Date($isoString, Zend_Date::ISO_8601);
        } else {
            $r = null;
        }
        return $r;
    }

    /**
     *
     * @return Zend_Date
     */
    public function getStartDate() {
        $isoString = $this->_getData('started_on');

        if ($isoString) {
            $r = new Zend_Date($isoString, Zend_Date::ISO_8601);
        } else {
            $r = null;
        }
        return $r;
    }

    public function isAutoRenew() {
        return (bool)$this->getAutoRenew();
    }
    
    public function canRenew() {
        $errors = $this->getRenewValidationErrors();
        return (count($errors) === 0);
    }

    public function canAutoRenew() {
        $errors = $this->getAutoRenewValidationErrors();
        return (count($errors) === 0);
    }

    /**
     * Check if this subscription can be renewed right now (mostly depending on payment method).
     * Does not take auto-renew into account. Use getAutoRenewValidationErrors() to check for auto-renew possibility.
     * Returns an array of strings with errors.
     */
    public function getRenewValidationErrors() {
        $r = array();

        $helper = Mage::helper('subscriptions');

        if ($this->isDisabled()) {
            $r[] = $helper->__('This subscription is disabled.');

        } else {

            if ($this->getRenewProductId() && $this->getRenewProduct() && $this->getRenewProduct()->getId()) {

                $customer = $this->getCustomer();
                if ($customer) {

                } else {
                    $r[] = $helper->__('Subscription %s does not have a customer ID.', $this->getId());
                }
            } else {
                $r[] = $helper->__('There is no renewal product available for purchase.');
            }

            if ($this->hasRenewalsAwaitingManualActivation()) {
                $r[] = $helper->__('This subscription already has a renewal, but it is not activated yet.');
            }
        }

        return $r;
    }

    /**
     * Check if this subscription can be auto-renewed right now (mostly depending on payment method).
     * Returns an array of strings with errors.
     */
    public function getAutoRenewValidationErrors() {
        $r = $this->getRenewValidationErrors();

        if (!$this->isDisabled()) {
            if ($this->getCustomer()) {
               $customer = $this->getCustomer();
                $customerId = $customer->getId();

                /* @var $savedCardHelper Storefront_SavedCards_Helper_Data */
                $savedCardHelper = Mage::helper('savedcards');

                $card = $savedCardHelper->getPrimaryCard($customerId);

                if ($card && $card->getId()) {
                    // Everything ok

                } else {
                    $r[] = $savedCardHelper->__('Customer %s does not have a primary saved card.', $customerId);
                }
            }
        }

        return $r;
    }

    public function disable() {
        $this->setDisabled(1);
        $this->save();
    }

    public function enable() {
        $this->setDisabled(0);
        $this->save();
    }

    public function getName() {
        return $this->getProduct()->getName();
    }

    /**
     *
     * @return Mage_Catalog_Model_Product
     */
    public function getRenewProductId() {

        $origProduct = $this->getProduct();
        $renewalProductId = $origProduct->getSubscriptionRenewalProduct();

        return $renewalProductId;

    }

    /**
     *
     * @return Mage_Catalog_Model_Product
     */
    public function getRenewProduct() {
        $productId = $this->getRenewProductId();
        if ($productId) {
            $product = Mage::getModel('catalog/product');
            $product->load($productId);

            if ($product->getId()) {
                return $product;
            }
        }
        return null;
    }


    /**
     * Create a renew order for this subscription.
     *
     * @return Mage_Sales_Model_Order
     * @throws Exception
     * @throws Mage_Core_Exception
     */
    public function renew() {
        $helper = Mage::helper('subscriptions');

        if ($this->canAutoRenew()) {
            $product = $this->getRenewProduct();

            $customer = $this->getCustomer();
            try {
                $order = $this->_createOrderForProduct($customer, $product, 1);

                Mage::dispatchEvent('subscription_after_renew_success', array(
                    'order' => $order,
                    'subscription' => $this
                ));

                $this->sendRenewSuccessEmail($order);

                Mage::log('Subscription "' . $this->getId() . '" renewed successfully', null, 'subscriptions.log');

                return $order;

            } catch (Exception $e) {

                Mage::log('Could not renew subscription "' . $this->getId() . '": ' . $e->getMessage(), null, 'subscriptions.log');

                Mage::dispatchEvent('subscription_after_renew_failed', array(
                    'subscription' => $this,
                    'exception' => $e
                ));

                throw $e;
            }
        } else {
            $errors = $this->getAutoRenewValidationErrors();
            Mage::throwException($helper->__('Your subscription cannot be renewed: %s',implode($errors)));
        }
    }

    public function sendRenewSuccessEmail(Mage_Sales_Model_Order $order) {
        $order->sendNewOrderEmail();
    }

    public function sendRenewErrorEmail(Exception $e = null) {
        $this->sendErrorMailToShopkeeper($e);
        $this->sendErrorMailToCustomer();
    }

    public function sendErrorMailToShopkeeper(Exception $e = null) {
        $senderName = Mage::getStoreConfig('trans_email/ident_general/name');
        $senderEmail = Mage::getStoreConfig('trans_email/ident_general/email');

        $helper = Mage::helper('subscriptions');

        $emailTo = $senderEmail;

        $subject = $helper->__('Could not auto-renew your subscription for %s', $this->getName());

        $body = $subject;
        $body .= "\n\n";
        $body .= $e->getMessage();

        $body .= "\n\n";
        $body .= $e->getTraceAsString();

        $mail = Mage::getModel('core/email');
        $mail->setToName($senderName);
        $mail->setToEmail($emailTo);
        $mail->setBody($body);
        $mail->setSubject($subject);
        $mail->setFromEmail($senderEmail);
        $mail->setFromName($senderName);
        $mail->setType('text');

        try {
            $mail->send();
        } catch (Exception $e) {
            Mage::logException($e);
        }
    }

    public function sendErrorMailToCustomer() {
        $customer = $this->getCustomer();

        $mailData = array(
            'subscription' => $this,
            'store' => $this->getStore()
        );

        /* @var $mailTemplate Mage_Core_Model_Email_Template */
        $mailTemplate = Mage::getModel('core/email_template');
        $mailTemplate->setDesignConfig(array(
            'area' => 'frontend'
        ));

        $sender = 'support';

        $emailTo = $customer->getEmail();


        $template = Mage::getStoreConfig('subscriptions/auto_renew_failed_email/template', $this->getStoreId());
        if(!$template){
            $template = 'subscription_auto_renew_failed';
        }

        $bcc = trim(Mage::getStoreConfig('subscriptions/auto_renew_failed_email/bcc', $this->getStoreId()));
        if($bcc){
            $bcc = explode(';',$bcc);
            $mailTemplate->addBcc($bcc);
        }
        $mailTemplate->sendTransactional($template, $sender, $emailTo, null, $mailData);

        if (!$mailTemplate->getSentSuccess()) {
            Mage::throwException('Could not send the subscription auto-renew error email for subscription ' . $this->getId());
        }
    }


    public function sendRenewReminderEmail() {

        $store = $this->getStore();
        $storeId = $store->getId();

        $appEmulation = Mage::getSingleton('core/app_emulation');

        //Start environment emulation of the specified store
        $initialEnvironmentInfo = $appEmulation->startEnvironmentEmulation($store->getId());

        $dateFormat = 'medium';
        $coreHelper = Mage::helper('core');

        $customer = $this->getCustomer();
        $order = $this->getOrderItem()->getOrder();
        $orderdate = $order->getCreatedAtFormated($dateFormat);
        $subscription_end_date = $coreHelper->formatDate($this->getExpiryDate(), $dateFormat, false);

        $subscriptionUrl = Mage::getUrl('subscriptions/subscription/view', array(
            '_store' => $storeId,
            'id' => $this->getId()
        ));
        $allSubscriptionsUrl = Mage::getUrl('subscriptions/subscription/', array('_store' => $storeId));

        $subscriptionRenewalUrl =  "http://www.informationmapping.com/subscription-renewal/?key=". $this->getSerial();

        $product = $this->getProduct();

        $templateVars = array(
            'order' => $order,
            'orderdate' => $orderdate,
            'firstname' => $customer->getFirstname(),
            'lastname' => $customer->getLastname(),
            'email'=> $customer->getEmail(),
            'ordernumber' => $order->getIncrementId(),
            'product' => $product,
            'logo_url' => Mage::getDesign()->getSkinUrl('images/logo.gif', array('_store' => $storeId)),
            'store_url' => Mage::getUrl('', array('_store' => $storeId)),
            'subscription_end_date' => $subscription_end_date,
            'store' => $store,
            'subscription' => $this,
        );

        if($customer && $customer->getId()){
            $customerId = $customer->getId();

            /* @var $savedCardHelper Storefront_SavedCards_Helper_Data */
            $savedCardHelper = Mage::helper('savedcards');
            $primaryCard = $savedCardHelper->getPrimaryCard($customerId);

            if ($this->canAutoRenew()) {
                $templateVars['can_auto_renew'] = true;
                $templateVars['primary_card_number'] = $primaryCard->getCard();
                $templateVars['primary_card_type'] = $primaryCard->getCardType();
                $templateVars['primary_card_expiration_date'] = $primaryCard->getExpiryDate();
                $templateVars['edit_card_url'] = Mage::getUrl('savedcards/card', array('_store' => $storeId));
                $templateVars['my_account_url'] = Mage::getUrl('customer/account/', array('_store' => $storeId));
            }else{
                $templateVars['can_auto_renew'] = false;
            }

            if ($this->getSerial()){
                $templateVars['has_license_key'] = true;
                $templateVars['license_key'] = $this->getSerial();
                $templateVars['subscriptions_renewal_url'] = $subscriptionRenewalUrl;
            }else{
                $templateVars['has_license_key'] = false;
            }

            $templateVars['subscription_url'] = $subscriptionUrl;
            $templateVars['all_subscriptions_url'] = $allSubscriptionsUrl;
        }


        /* @var $mailTemplate Mage_Core_Model_Email_Template */
        $mailTemplate = Mage::getModel('core/email_template');
        $mailTemplate->setDesignConfig(array(
            'area' => 'frontend'
        ));

        $sender = 'support';

        if (Mage::getIsDeveloperMode()) {
            $emailTo = 'wouter.samaey@storefront.be';
        } else {
            $emailTo = $customer->getEmail();
        }


        $template = Mage::getStoreConfig('subscriptions/renew_reminder_email/template', $storeId);
        if(!$template){
            $template = 'subscription_auto_renew_coming_up';
        }

        $bcc = trim(Mage::getStoreConfig('subscriptions/renew_reminder_email/bcc', $storeId));
        if($bcc){
            $bcc = explode(';',$bcc);
            $mailTemplate->addBcc($bcc);
        }
        $mailTemplate->sendTransactional($template, $sender, $emailTo, null, $templateVars);


        //Stop environment emulation and restore original store
        $appEmulation->stopEnvironmentEmulation($initialEnvironmentInfo);

        if (!$mailTemplate->getSentSuccess()) {
            Mage::throwException('Could not send the renew reminder email for subscription ' . $this->getId());
        }

    }

    protected function _createOrderForProduct(Mage_Customer_Model_Customer $customer, Mage_Catalog_Model_Product $product, $qty = 1) {

        /* @var $savedCardHelper Storefront_SavedCards_Helper_Data */
        $savedCardHelper = Mage::helper('savedcards');

        $card = $savedCardHelper->getPrimaryCard($customer->getId());

        if ($card && $card->getId()) {

            // Use the store where the customer was created
            $store = $customer->getStore();

            /* @var $quote Mage_Sales_Model_Quote */
            $quote = Mage::getModel('sales/quote')->setStoreId($store->getId());

            // Set customer on quote
            $quote->assignCustomer($customer);

            // Add product
            $item = $quote->addProduct($product, new Varien_Object(array(
                'qty' => $qty
            )));

            $item->setRenewSubscriptionId($this->getId());

            $customerBillingAddress = $customer->getDefaultBillingAddress();
            $customerShippingAddress = $customer->getDefaultShippingAddress();

            $quoteBillingAddress = $quote->getBillingAddress();
            $quoteShippingAddress = $quote->getShippingAddress();

            /* @var $addressForm Mage_Customer_Model_Form */
            $addressForm = Mage::getModel('customer/form');
            $addressForm->setFormCode('customer_address_edit')->setEntityType('customer_address');

            $quoteBillingAddress->importCustomerAddress($customerBillingAddress)->setSaveInAddressBook(0);
            $addressForm->setEntity($quoteBillingAddress);

            $addressErrors = $addressForm->validateData($quoteBillingAddress->getData());
            if ($addressErrors !== true) {
                Mage::throwException('Cannot create order due to billing address errors: '.print_r($addressErrors, true));
            }

            if (!$quote->isVirtual()) {
                /**
                 * Billing address using otions
                 */
                $usingCase = 1;

                switch ($usingCase) {
                    case 0:
                        $shipping = $this->getQuote()->getShippingAddress();
                        $shipping->setSameAsBilling(0);
                        break;
                    case 1:
                        $billing = clone $quoteBillingAddress;
                        $billing->unsAddressId()->unsAddressType();
                        $shipping = $quote->getShippingAddress();
                        $shippingMethod = $shipping->getShippingMethod();

                        // Billing address properties that must be always copied to shipping address
                        $requiredBillingAttributes = array(
                            'customer_address_id'
                        );

                        // don't reset original shipping data, if it was not changed by customer
                        foreach ($shipping->getData() as $shippingKey => $shippingValue) {
                            if (!is_null($shippingValue) && !is_null($billing->getData($shippingKey)) && !isset($data[$shippingKey]) && !in_array($shippingKey, $requiredBillingAttributes)) {
                                $billing->unsetData($shippingKey);
                            }
                        }
                        $shipping->addData($billing->getData())->setSameAsBilling(1)->setSaveInAddressBook(0)->setShippingMethod($shippingMethod)->setCollectShippingRates(true);

                        break;
                }

                $quoteShippingAddress->setCollectShippingRates(1);
                $quoteShippingAddress->collectShippingRates();
                $quoteShippingAddress->save();

                $shippingRates = $this->_getFlatShippingRates($quote);

                foreach ($shippingRates as $shippingRate) {
                    /* @var $shippingRate Mage_Sales_Model_Quote_Address_Rate */

                    $selectedShippingMethod = 'flatrate_flatrate';
                }

                $rate = $quoteShippingAddress->getShippingRateByCode($selectedShippingMethod);
                if (!$rate) {
                    Mage::throwException(Mage::helper('checkout')->__('Invalid shipping method.'));
                }
                $quoteShippingAddress->setShippingMethod($selectedShippingMethod);

                Mage::dispatchEvent('checkout_controller_onepage_save_shipping_method', array(
                    'request' => Mage::app()->getRequest(),
                    'quote' => $quote
                ));
            }

            // foreach ($paymentHelper->getStoreMethods($storeId, $quote) as $method) {
            // /* @var $method Mage_Payment_Model_Method_Abstract */
            // if ($method->isApplicableToQuote($quote, Mage_Payment_Model_Method_Abstract::CHECK_ZERO_TOTAL)) {
            // $method->setInfoInstance($quote->getPayment());

            // // // $paymentOptionInfo = array();

            // $paymentCode = $method->getCode();
            // // // $paymentOptionInfo['title'] = $method->getTitle();

            // // // $paymentOptions[] = $paymentOptionInfo;

            // $h = '';

            // }
            // }

            $pm = $card->getPaymentMethodCode();

            // Payment method
            $payment = $quote->getPayment();
            $payment->setQuote($quote);
            $payment->setMethod($pm);

            // Save quote first, because payment references quote_id
            $quote->save();

            $payment->save();

            $quote->collectTotals();

            $quote->save();

            // Create Order From Quote
            /* @var $service Mage_Sales_Model_Service_Quote */
            $service = Mage::getModel('sales/service_quote', $quote);

            $service->submitAll();

            $order = $service->getOrder();

            return $order;
        } else {
            // Card not found
            Mage::throwException($savedCardHelper->__('Customer %s does not have a primary saved card.', $customer->getId()));

        }
    }

    protected function _getFlatShippingRates(Mage_Sales_Model_Quote $quote) {
        $r = array();

        $shippingAddress = $quote->getShippingAddress();
        $shippingRates = $shippingAddress->getGroupedAllShippingRates();

        foreach ($shippingRates as $shippingRateGroup) {
            foreach ($shippingRateGroup as $shippingRate) {
                $r[] = $shippingRate;
            }
        }

        return $r;
    }


    public function lastLicenseKeyByLicenseId($id) {
        $license_keys = Mage::getResourceModel('licensemanager/license_collection')->addFieldToSelect('serial')->addFieldToFilter('license_id', $id)->setOrder('id', 'desc')->getFirstItem();
        return $license_keys->getLicenseKey();
    }


    public function getProductSkuByLicenseKeyId($id) {
        $license_key = Mage::getResourceModel('licensemanager/license_collection')->addFieldToSelect('product_sku')->addFieldToFilter('id', $id)->getFirstItem();
        return $license_key->getProductSku();
    }

    private function _getAllActiveLicenseKeys($license_id) {
        $license_keys = Mage::getResourceModel('licensemanager/keys_collection')->addFieldToSelect('id')->addFieldToFilter('license_id', $license_id)->addFieldToFilter('expiration_date', array('gteq' => date('Y-m-d')))->addFieldToFilter('licensekey_status', array('neq' => '0'));
        return $license_keys;
    }

    public function getExpiringLicenseKeys($type) {
        $resource = Mage::getSingleton('core/resource');
        $readConnection = $resource->getConnection('core_read');
        $license_keysTable = $resource->getTableName('subscriptions_subscription');
        $license_keys_renewalTable = $resource->getTableName('subscriptions_renewal');

        $filter_sql = ($type > 0) ? 'lkr.sma_2m_b4_enddate' : 'lkr.sma_enddate';
        $query = "SELECT 
                    lk.id
                    FROM " . $license_keysTable . " lk
                    INNER JOIN " . $license_keys_renewalTable . " lkr ON lk.id = lkr.keyid
                  WHERE
                    lk.licensekey_status <> ?
                    AND CURDATE() >  " . $filter_sql;
        return $readConnection->fetchAll($query, 0);
    }

    public function lastLicenseKeyStatusByLicenseId($id) {
        $license_key = Mage::getResourceModel('licensemanager/keys_collection')->addFieldToSelect('licensekey_status')->addFieldToFilter('license_id', $id)->setOrder('id', 'desc')->getFirstItem();
        return $license_key->getLicensekeyStatus();
    }

    public function getLicensekeyByKeyid($id) {
        $licensekeyInfo = Mage::getResourceModel('licensemanager/keys_collection')->addFieldToSelect('serial')->addFieldToFilter('id', $id)->getFirstItem();
        return $licensekeyInfo->getLicenseKey();
    }

    public function validateLicenseKeys(array $licensekeys, $qty, $license_type) {
        $isValid = false;
        $license_keys = Mage::getResourceModel('licensemanager/keys_collection')->addFieldToSelect('id')->addFieldToFilter('serial', array('in' => $licensekeys));
        //echo $license_keys->printLogQuery(true);
        $existing_licensekeys = $license_keys->count();

        if (strcasecmp($license_type, 'Single License') == 0 && $existing_licensekeys >= $qty) {
            $isValid = true;
        } elseif (strcasecmp($license_type, 'Volume License') == 0 && $existing_licensekeys >= 1) {
            $isValid = true;
        }

        return $isValid;
    }

    public function getLicensekeyInfoByKey($key) {
        $licensekeyInfo = Mage::getResourceModel('licensemanager/keys_collection')->addFieldToSelect('*')->addFieldToFilter('serial', $key)->setOrder('id', 'desc')->getFirstItem();
        return $licensekeyInfo;
    }

    public function setCurrentLicensekeysExpired($currentKeys) {
        $currentKeys = array_map(array(
            $this,
            'enclose_with_quotes'
        ), $currentKeys);
        $currentKeys = implode(",", $currentKeys);

        $resource = Mage::getSingleton('core/resource');
        $writeConnection = $resource->getConnection('core_write');
        $license_keysTable = $resource->getTableName('subscriptions_subscription');
        $license_keysRenewalTable = $resource->getTableName('subscriptions_renewal');
        $status = 0;

        $updateLicensekey = "UPDATE " . $license_keysTable . " SET licensekey_status = ? WHERE serial IN(" . $currentKeys . ")";
        $writeConnection->query($updateLicensekey, $status);

        $updateLicensekeyRenewal = "UPDATE " . $license_keysRenewalTable . " SET renewal_status = ? WHERE keyid IN( SELECT id FROM " . $license_keysTable . " WHERE serial IN(" . $currentKeys . "))";
        $writeConnection->query($updateLicensekeyRenewal, $status);

        return true;
    }

    private function enclose_with_quotes($value) {
        return "'" . $value . "'";
    }

    public function getRenewalCollection() {
        /* @var $col Storefront_Subscriptions_Model_Resource_Renewal_Collection */
        $col = Mage::getResourceModel('subscriptions/renewal_collection');
        $col->addFieldToFilter('subscription_id', $this->getId());

        return $col;
    }

    public function getRenewalsAwaitingManualActivation() {
        // Show renewals that may need activation
        $col = $this->getRenewalCollection();
        $col->addFieldToFilter('status', array(
            'in' => array(
                Storefront_Subscriptions_Model_Renewal::STATUS_WAIT_MANUAL_ACTIVATION_BY_ADMIN,
                Storefront_Subscriptions_Model_Renewal::STATUS_WAIT_MANUAL_ACTIVATION_BY_CUSTOMER
            )
        ));

        return $col;
    }

    public function hasRenewalsAwaitingManualActivation() {
        return $this->getRenewalsAwaitingManualActivation()->count() > 0;
    }

    /**
     * Check if the status is correct.
     * We want to prevent having "active" status with an expired date or an "expired" status with a later expiry date
     */
    public function refreshStatus() {
        $status = $this->getStatus();
        $expiryDate = $this->getExpiryDate();

        if ($expiryDate) {
            $isPastExpiry = $expiryDate->isEarlier(new Zend_Date());

            if ($status == self::STATUS_ACTIVE && $isPastExpiry) {
                $this->setStatus(self::STATUS_EXPIRED);

            } elseif ($status == self::STATUS_EXPIRED && !$isPastExpiry) {
                $this->setStatus(self::STATUS_ACTIVE);

            } elseif (($status == self::STATUS_WAIT_MANUAL_ACTIVATION_BY_ADMIN || $status == self::STATUS_WAIT_MANUAL_ACTIVATION_BY_CUSTOMER) && $isPastExpiry) {
                $this->setStatus(self::STATUS_EXPIRED);
            }
        }
    }

    /**
     * Update the status based on expiry date
     * @return Mage_Core_Model_Abstract
     */
    protected function _beforeLoad($id, $field = null) {
        $r = parent::_beforeLoad($id, $field);
        $this->refreshStatus();
        return $r;
    }

    /**
     * Update the status based on expiry date
     * @return Mage_Core_Model_Abstract
     */
    protected function _beforeSave() {
        $this->refreshStatus();

        return parent::_beforeSave();
    }

    public function isReadOnly() {
        return false;
    }

    public function isDeleteable() {
        return !$this->isReadOnly();
    }

    public function validateData(Varien_Object $data) {
        return true;
    }

    /**
     * Bulk set data before save, used in CRUD admin controller
     * @param $data
     * @return $this
     */
    public function loadPost($data) {
        foreach ($data as $key => $value) {
            $this->setData($key, $value);
        }
        return $this;
    }

    /**
     * Get the currently available statusses an admin can set this subscription to.
     * @return array
     */
    public function getAvailableStatuses() {
        $r = self::getStatuses();

        $expiryDate = $this->getExpiryDate();
        if ($expiryDate) {
            $isExpired = $expiryDate->isEarlier(new Zend_Date());

            if ($isExpired) {
                // Cannot set to anything if expiry date is past
                unset($r[self::STATUS_ACTIVE]);
                unset($r[self::STATUS_WAIT_MANUAL_ACTIVATION_BY_ADMIN]);
                unset($r[self::STATUS_WAIT_MANUAL_ACTIVATION_BY_CUSTOMER]);
            } else {
                // Cannot set to "expired" if expiry date is not reached yet
                unset($r[self::STATUS_EXPIRED]);
            }
        }

        return $r;
    }
}