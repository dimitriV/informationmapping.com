<?php

class Storefront_Subscriptions_Model_Cron {

    public function sendRenewalEmails($cronjob = null) {
        $sendMailXDaysBeforeExpiry = Mage::getStoreConfig('subscriptions/renew_reminder_email/days_before_expiry');
        $numEmailsPerRun = 20;

        /* @var $helper Storefront_Subscriptions_Helper_Data */
        $helper = Mage::helper('subscriptions');
        $storeIdsWithSubscriptions = $helper->getStoreIdsWithSubscriptions();

        // Filter out stores that have the emails turned off
        $storeIdsToSendEmailsFor = array();
        foreach($storeIdsWithSubscriptions as $storeId) {
            if (Mage::getStoreConfigFlag('subscriptions/general/enable_auto_renew', $storeId)) {
                $storeIdsToSendEmailsFor[] = $storeId;
            }
        }

        $storeIdsConcat = implode(', ', $storeIdsToSendEmailsFor);

        $sql = <<<TEXT
SELECT
    `main_table`.id,
    `sales_flat_order`.`store_id`
FROM
    `subscriptions_subscription` AS `main_table`
        INNER JOIN
    `sales_flat_order_item` ON sales_flat_order_item.item_id = main_table.order_item_id
        INNER JOIN
    `sales_flat_order` ON sales_flat_order.entity_id = sales_flat_order_item.order_id
WHERE
    (last_email_sent IS NULL OR last_email_sent < DATE_SUB(NOW(), INTERVAL $sendMailXDaysBeforeExpiry DAY))
    AND
    (expires_on IS NOT NULL AND NOW() > DATE_SUB(expires_on, INTERVAL $sendMailXDaysBeforeExpiry DAY) AND NOW() < expires_on)
    AND
    (sales_flat_order.store_id IN ($storeIdsConcat))
    LIMIT $numEmailsPerRun
TEXT;

        $resource = Mage::getSingleton('core/resource');
        $db = $resource->getConnection('core_write');


        $ids = $db->fetchCol($sql);

        /* @var $col Storefront_Subscriptions_Model_Resource_Subscription_Collection */
        $col = Mage::getResourceModel('subscriptions/subscription_collection');

        $col->addFieldToFilter('id', array('in' => $ids));

        foreach ($col->getItems() as $subscription) {
            /* @var $subscription Storefront_Subscriptions_Model_Subscription */
            try {
                $subscription->sendRenewReminderEmail();

                $subscription->setLastEmailSent(date('Y-m-d H:i:s'));
                $subscription->save();

            } catch (Exception $ex) {
                Mage::logException($ex);
            }


        }
    }


    /**
     * Runs every 15 minutes
     * @param null $observer
     */
    public function autoRenewOnExpiryDay($observer = null) {
        /* @var $helper Storefront_Subscriptions_Helper_Data */
        $helper = Mage::helper('subscriptions');

        $allStores = Mage::app()->getStores();

        foreach ($allStores as $storeId => $store) {
            if (Mage::getStoreConfigFlag('subscriptions/general/enable_auto_renew', $storeId)) {
                $numToProcess = 10;

                $subscriptions = $helper->getSubscriptionsForImmediateAutoRenew($storeId);

                $numProcessed = 0;
                $nonAutoRenewableCount = 0;

                if (count($subscriptions) > 0) {
                    Mage::log($helper->__('Found %s subscriptions that need renewal. Will process a batch of %s now.', count($subscriptions), $numToProcess), null, 'subscriptions.log');

                    foreach ($subscriptions as $subscription) {
                        /* @var $subscription Storefront_Subscriptions_Model_Subscription */

                        try {
                            if ($numProcessed >= $numToProcess) {
                                break;
                            }

                            $order = $subscription->renew();

                        } catch (Exception $e) {
                            Mage::logException($e);

                            $subscription->setLastAutoRenewFailedOn(date('Y-m-d H:i:s'));
                            $subscription->save();

                            $subscription->sendRenewErrorEmail($e);
                        }
                        $numProcessed++;

                    }
                    Mage::log($helper->__('Finished subscription renew batch. %s subscriptions were processed. %s were not auto-renewable.', $numProcessed, $nonAutoRenewableCount), null, 'subscriptions.log');
                } else {
                    Mage::log($helper->__('There are no subscriptions that need renewal.'), null, 'subscriptions.log');
                }

            } else {
                Mage::log($helper->__('Cannot auto-renew. It is turned off in Magento admin.'), null, 'subscriptions.log');
            }
        }


    }
}