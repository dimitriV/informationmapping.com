<?php

class Storefront_Subscriptions_Model_Entity_Setup extends Mage_Eav_Model_Entity_Setup {

    /**
     * Default entities and attributes
     *
     * @return array
     */
    public function getDefaultEntities() {
        $group = 'Subscription';

        return array(
            'catalog_product' => array(
                'entity_model' => 'catalog/product',
                'attribute_model' => 'catalog/resource_eav_attribute',
                'table' => 'catalog/product',
                'additional_attribute_table' => 'catalog/eav_attribute',
                'entity_attribute_collection' => 'catalog/product_attribute_collection',
                'attributes' => array(
                    'subscription_behaviour' => array(
                        'attribute_set' => 'default',
                        'group' => $group,
                        'type' => 'int',
                        'backend' => '',
                        'frontend' => '',
                        'label' => 'Subscription behaviour',
                        'source' => 'subscriptions/source_subscription_behaviour',
                        'global' => Mage_Catalog_Model_Resource_Eav_Attribute::SCOPE_GLOBAL,
                        'input' => 'select',
                        'class' => '',
                        // 'visible' => false,
                        'required' => false,
                        'user_defined' => true,
                        'default' => '0',
                        'searchable' => false,
                        'filterable' => false,
                        'comparable' => false,
                        'visible_on_front' => false,
                        'unique' => false,
                        'used_in_product_listing' => false,
                        'configurable' => false
                    ),
                    'subscription_period' => array(
                        'attribute_set' => 'default',
                        'group' => $group,
                        'type' => 'int',
                        'backend' => '',
                        'frontend' => '',
                        'label' => 'Subscription period',
                        'source' => 'subscriptions/source_subscription_period',
                        'global' => Mage_Catalog_Model_Resource_Eav_Attribute::SCOPE_GLOBAL,
                        'input' => 'select',
                        'class' => '',
                        // 'visible' => false,
                        'required' => false,
                        'user_defined' => true,
                        'default' => '1',
                        'searchable' => false,
                        'filterable' => false,
                        'comparable' => false,
                        'visible_on_front' => false,
                        'unique' => false,
                        'used_in_product_listing' => false,
                        'configurable' => false
                    ),
                    'subscription_start_from' => array(
                        'attribute_set' => 'default',
                        'group' => $group,
                        'type' => 'int',
                        'backend' => '',
                        'frontend' => '',
                        'label' => 'Subscription start from',
                        'source' => 'subscriptions/source_subscription_startfrom',
                        'global' => Mage_Catalog_Model_Resource_Eav_Attribute::SCOPE_GLOBAL,
                        'input' => 'select',
                        'class' => '',
                        // 'visible' => false,
                        'required' => false,
                        'user_defined' => true,
                        'default' => '1',
                        'searchable' => false,
                        'filterable' => false,
                        'comparable' => false,
                        'visible_on_front' => false,
                        'unique' => false,
                        'used_in_product_listing' => false,
                        'configurable' => false
                    ),
                    'subscription_renewal_product' => array(
                        'attribute_set' => 'default',
                        'group' => $group,
                        'type' => 'int',
                        'backend' => '',
                        'frontend' => '',
                        'label' => 'Renewal product',
                        'source' => 'subscriptions/source_subscription_renewal',
                        'global' => Mage_Catalog_Model_Resource_Eav_Attribute::SCOPE_GLOBAL,
                        'input' => 'select',
                        'class' => '',
                        // 'visible' => false,
                        'required' => false,
                        'user_defined' => true,
                        'default' => '1',
                        'searchable' => false,
                        'filterable' => false,
                        'comparable' => false,
                        'visible_on_front' => false,
                        'unique' => false,
                        'used_in_product_listing' => false,
                        'configurable' => false
                    ),
                    'subscription_auto_renew' => array(
                        'attribute_set' => 'default',
                        'group' => $group,
                        'type' => 'int',
                        'backend' => '',
                        'frontend' => '',
                        'label' => 'Auto-renew new subscriptions',
                        'source' => 'eav/entity_attribute_source_boolean',
                        'global' => Mage_Catalog_Model_Resource_Eav_Attribute::SCOPE_GLOBAL,
                        'input' => 'select',
                        'class' => '',
                        'note' => 'Newly created subscriptions will be set to auto-renew by default. The customer can change this later.',
                        // 'visible' => false,
                        'required' => false,
                        'user_defined' => true,
                        'default' => '0',
                        'searchable' => false,
                        'filterable' => false,
                        'comparable' => false,
                        'visible_on_front' => false,
                        'unique' => false,
                        'used_in_product_listing' => false,
                        'configurable' => false
                    ),
                    'subscription_info' => array(
                        'attribute_set' => 'default',
                        'group' => $group,
                        'type' => 'int',
                        'backend' => '',
                        'frontend' => '',
                        'label' => ' ',
                        'source' => 'subscriptions/source_subscription_period',
                        'global' => Mage_Catalog_Model_Resource_Eav_Attribute::SCOPE_GLOBAL,
                        'input' => 'subscription_info',
                        'class' => '',
                        // 'visible' => false,
                        'required' => false,
                        'user_defined' => true,
                        'default' => '',
                        'searchable' => false,
                        'filterable' => false,
                        'comparable' => false,
                        'visible_on_front' => false,
                        'unique' => false,
                        'used_in_product_listing' => false,
                        'configurable' => false
                    )



                )
            )
        );
    }
}