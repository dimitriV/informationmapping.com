<?php

class Storefront_Subscriptions_Model_Adminhtml_Observer {

	/**
	 * After saving a template, we may need to continue to the configure products step.
	 */
	public function loadSubscriptionFormElement(Varien_Event_Observer $observer){

        $types = $observer->getResponse()->getTypes();

        $types['subscription_info'] = 'Storefront_Subscriptions_Block_Adminhtml_Element_Subscription';

        $observer->getResponse()->setTypes($types);

    }
		
}