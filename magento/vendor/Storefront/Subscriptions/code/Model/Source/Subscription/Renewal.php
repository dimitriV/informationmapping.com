<?php
class Storefront_Subscriptions_Model_Source_Subscription_Renewal extends Mage_Eav_Model_Entity_Attribute_Source_Abstract {

   public function getAllOptions () {
        if ($this->_options === null) {

            /* @var $helper Storefront_Subscriptions_Helper_Data */
            $helper = Mage::helper('subscriptions');

            $products = $helper->getAllRenewalProducts();

            $r = array('' => '---');
            foreach($products as $product){
                $r[$product->getId()] = $product->getSku();
            }

            $this->_options = $r;
        }

        return $this->_options;
    }

}