<?php
class Storefront_Subscriptions_Model_Source_Subscription_Startfrom extends Mage_Eav_Model_Entity_Attribute_Source_Abstract {

    const START_FROM_ORDER_DATE = '10';
    const START_FROM_ACTIVATION_DATE = '20';


    public function getAllOptions () {
        if ($this->_options === null) {

            $helper = Mage::helper('subscriptions');

            $r = array(
                self::START_FROM_ORDER_DATE => $helper->__('Start period from order date'),
                self::START_FROM_ACTIVATION_DATE => $helper->__('Start period from activation date')
            );

            $this->_options = $r;
        }

        return $this->_options;
    }
}