<?php
class Storefront_Subscriptions_Model_Source_Subscription_Behaviour extends Mage_Eav_Model_Entity_Attribute_Source_Abstract {

    const SUBSCRIPTION_NONE = '0';
    const SUBSCRIPTION_START_ON_INVOICE = '10';
    const SUBSCRIPTION_START_ON_ACTIVATION_BY_ADMIN = '15';
    const SUBSCRIPTION_START_ON_ACTIVATION_BY_CUSTOMER = '16';
    const SUBSCRIPTION_RENEW_ON_INVOICE = '20';
    const SUBSCRIPTION_RENEW_ON_ACTIVATION_BY_ADMIN = '25';
    const SUBSCRIPTION_RENEW_ON_ACTIVATION_BY_CUSTOMER = '26';


    public function getAllOptions () {
        if ($this->_options === null) {

            $helper = Mage::helper('subscriptions');

            $r = array(
                self::SUBSCRIPTION_NONE => '---',
                self::SUBSCRIPTION_START_ON_INVOICE => $helper->__('Start a new subscription on invoice'),
                self::SUBSCRIPTION_START_ON_ACTIVATION_BY_ADMIN => $helper->__('Start a new subscription after activation by admin'),
                self::SUBSCRIPTION_START_ON_ACTIVATION_BY_CUSTOMER => $helper->__('Start a new subscription after activation by customer'),
                self::SUBSCRIPTION_RENEW_ON_INVOICE => $helper->__('Renew subscription on invoice'),
                self::SUBSCRIPTION_RENEW_ON_ACTIVATION_BY_ADMIN => $helper->__('Renew subscription after activation by admin'),
                self::SUBSCRIPTION_RENEW_ON_ACTIVATION_BY_CUSTOMER => $helper->__('Renew subscription after activation by customer'),
            );

            $this->_options = $r;
        }

        return $this->_options;
    }

    public static function getStartOnInvoiceValues(){
        return array(self::SUBSCRIPTION_START_ON_INVOICE, self::SUBSCRIPTION_RENEW_ON_INVOICE);
    }

    public static function getStartManualValues(){
        return array(self::SUBSCRIPTION_START_ON_ACTIVATION_BY_ADMIN, self::SUBSCRIPTION_START_ON_ACTIVATION_BY_CUSTOMER, self::SUBSCRIPTION_RENEW_ON_ACTIVATION_BY_ADMIN, self::SUBSCRIPTION_RENEW_ON_ACTIVATION_BY_CUSTOMER);
    }

//    public static function getStartManualByAdminValues(){
//        return array(self::SUBSCRIPTION_START_ON_ACTIVATION_BY_ADMIN, self::SUBSCRIPTION_RENEW_ON_ACTIVATION_BY_ADMIN);
//    }
//
//    public static function getStartManualByCustomerValues(){
//        return array(self::SUBSCRIPTION_START_ON_ACTIVATION_BY_CUSTOMER, self::SUBSCRIPTION_RENEW_ON_ACTIVATION_BY_CUSTOMER);
//    }

    public static function getStartSubscriptionValues(){
        return array(self::SUBSCRIPTION_START_ON_INVOICE, self::SUBSCRIPTION_START_ON_ACTIVATION_BY_ADMIN, self::SUBSCRIPTION_START_ON_ACTIVATION_BY_CUSTOMER);

    }

    public static function getRenewalValues(){
        return array(self::SUBSCRIPTION_RENEW_ON_INVOICE, self::SUBSCRIPTION_RENEW_ON_ACTIVATION_BY_ADMIN, self::SUBSCRIPTION_RENEW_ON_ACTIVATION_BY_CUSTOMER);
    }
}