<?php
class Storefront_Subscriptions_Model_Source_Subscription_Period extends Mage_Eav_Model_Entity_Attribute_Source_Abstract {

    const PERIOD_NOT_SET = '';
    const PERIOD_1MONTH = '1';
    const PERIOD_3MONTH = '3';
    const PERIOD_6MONTH = '6';
    const PERIOD_1YEAR = '12';
    const PERIOD_2YEAR = '24';
    const PERIOD_3YEAR = '36';
    const PERIOD_PERPETUAL = '9999';

    const PERIOD_PERPETUAL_DATE = '2050-01-01 00:00:00';


    public function getAllOptions () {
        if ($this->_options === null) {

            $helper = Mage::helper('subscriptions');

            $r = array(
                self::PERIOD_NOT_SET => '---',
                self::PERIOD_1MONTH => $helper->__('1 month'),
                self::PERIOD_3MONTH => $helper->__('3 months'),
                self::PERIOD_6MONTH => $helper->__('6 months'),
                self::PERIOD_1YEAR => $helper->__('1 year'),
                self::PERIOD_2YEAR => $helper->__('2 years'),
                self::PERIOD_3YEAR => $helper->__('3 years'),
                self::PERIOD_PERPETUAL => $helper->__('Perpetual')
            );

            $this->_options = $r;
        }

        return $this->_options;
    }
}