<?php

class Storefront_Subscriptions_Model_Resource_Renewal_Collection extends Mage_Core_Model_Resource_Db_Collection_Abstract
{

    protected $_isOrderItemJoined = false;
    protected $_isOrderJoined = false;

	public function _construct()
	{
		parent::_construct();
		$this->_init('subscriptions/renewal');
	}


    public function joinOrderItem() {
        if (!$this->_isOrderItemJoined) {
            // $this->getSelect()->joinLeft('subscriptions_subscription','subscriptions_subscription.pool_id = main_table.id', array('subscription_id' => 'subscriptions_subscription.id'));
            $fields = array(
                'product_id' => 'sales_flat_order_item.product_id',
                'product_name' => 'sales_flat_order_item.name',
                'order_id' => 'sales_flat_order_item.order_id'
            );
            $this->getSelect()->joinInner('sales_flat_order_item', 'sales_flat_order_item.item_id = main_table.order_item_id', $fields);

            $this->_isOrderItemJoined = true;
        }
    }

    public function joinOrder() {

        $this->joinOrderItem();

        if (!$this->_isOrderJoined) {
            // $this->getSelect()->joinLeft('subscriptions_subscription','subscriptions_subscription.pool_id = main_table.id', array('subscription_id' => 'subscriptions_subscription.id'));

            $fields = array(
                'store_id' => 'sales_flat_order.store_id',
                'customer_id' => 'sales_flat_order.customer_id',
                'customer_firstname' => 'sales_flat_order.customer_firstname',
                'customer_lastname' => 'sales_flat_order.customer_lastname',
                'customer_email' => 'sales_flat_order.customer_email'
            );

            $this->getSelect()->joinInner('sales_flat_order', 'sales_flat_order.entity_id = sales_flat_order_item.order_id', $fields);
            $this->_isOrderJoined = true;
        }
    }

    public function getSerialNumberArray() {
        $serialNumbers = array();
        foreach ($this as $renewal) {
            if($renewal->getSerial()){
                $serialNumbers[] = $renewal->getSerial();
            }
        }
        return $serialNumbers;
    }
	
}