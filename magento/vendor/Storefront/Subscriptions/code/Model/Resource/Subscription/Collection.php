<?php

class Storefront_Subscriptions_Model_Resource_Subscription_Collection extends Mage_Core_Model_Resource_Db_Collection_Abstract {

    protected $_isOrderItemJoined = false;
    protected $_isOrderJoined = false;

    public function _construct() {
        parent::_construct();
        $this->_init('subscriptions/subscription');
    }

    protected function _initSelect() {
        $this->addFilterToMap('status', 'main_table.status');
        $this->addFilterToMap('product_name', 'sales_flat_order_item.name');
        $this->addFilterToMap('store_id', 'sales_flat_order.store_id');
        //$this->addFilterToMap('days_to_expiry', 'days_to_expiry');

        return parent::_initSelect();
    }

    public function addDaysToExpiry(){
        $select = $this->getSelect();
        $select->columns(array('days_to_expiry'=> new Zend_Db_Expr('DATEDIFF(expires_on, now())')));
    }

    public function getSerialNumberArray() {
        $serialNumbers = array();
        foreach ($this as $subscription) {
            if($subscription->getSerial()){
                $serialNumbers[] = $subscription->getSerial();
            }
        }
        return $serialNumbers;
    }

    public function joinOrderItem() {
        if (!$this->_isOrderItemJoined) {
            // $this->getSelect()->joinLeft('subscriptions_subscription','subscriptions_subscription.pool_id = main_table.id', array('subscription_id' => 'subscriptions_subscription.id'));
            $fields = array(
                'product_id' => 'sales_flat_order_item.product_id',
                'product_name' => 'sales_flat_order_item.name',
                'order_id' => 'sales_flat_order_item.order_id'
            );
            $this->getSelect()->joinInner('sales_flat_order_item', 'sales_flat_order_item.item_id = main_table.order_item_id', $fields);

            $this->_isOrderItemJoined = true;
        }
    }

    public function joinOrder() {

        $this->joinOrderItem();

        if (!$this->_isOrderJoined) {
            // $this->getSelect()->joinLeft('subscriptions_subscription','subscriptions_subscription.pool_id = main_table.id', array('subscription_id' => 'subscriptions_subscription.id'));

            $fields = array(
                'store_id' => 'sales_flat_order.store_id',
                'customer_id' => 'sales_flat_order.customer_id',
                'customer_firstname' => 'sales_flat_order.customer_firstname',
                'customer_lastname' => 'sales_flat_order.customer_lastname',
                'customer_email' => 'sales_flat_order.customer_email',
                'increment_id' => 'sales_flat_order.increment_id'
            );

            $this->getSelect()->joinInner('sales_flat_order', 'sales_flat_order.entity_id = sales_flat_order_item.order_id', $fields);
            $this->_isOrderJoined = true;
        }
    }


}