<?php

class Storefront_Subscriptions_Model_Renewal extends Mage_Core_Model_Abstract {

    const STATUS_WAIT_MANUAL_ACTIVATION_BY_ADMIN = 'WAIT_ON_ACTIVATION_BY_ADMIN';
    const STATUS_WAIT_MANUAL_ACTIVATION_BY_CUSTOMER = 'WAIT_ON_ACTIVATION_BY_CUSTOMER';
    const STATUS_WAIT_ON_INVOICE = 'WAIT_ON_INVOICE';
    const STATUS_ACTIVE = 'ACTIVE';

    // These fields are editable in Magento admin and need to be timezone managed
    protected $_userEnteredDateTimeFields = array('started_on');


    protected function _construct() {
        parent::_construct();
        $this->_init('subscriptions/renewal');
    }

    /**
     * Same as getData(), for datetime is converted to local timezone
     */
    public function getDataInLocalTimezone(){
        $data = $this->getData();

        $dateFields = $this->getUserEnteredDateTimeFields();

        if (is_array($dateFields) && count($dateFields)) {
            //$data           = $this->_filterDateTime($data, $dateFields);

            $gmt_timezone   = new DateTimeZone('Europe/London');
            $store_timezone = new DateTimeZone(Mage::getStoreConfig('general/locale/timezone'));

            foreach ($dateFields as $key) if (isset($data[$key])) {
                $dateTime = new DateTime($data[$key], $gmt_timezone);
                $dateTime->setTimezone($store_timezone);
                $data[$key] = $dateTime->format('Y-m-d H:i:s');
            }
        }

        return $data;
    }


    public function getUserEnteredDateTimeFields(){
        return $this->_userEnteredDateTimeFields;
    }

    /**
     * @return Mage_Sales_Model_Order_Item
     */
    public function getOrderItem() {
        $id = $this->getOrderItemId();
        $r = Mage::getModel('sales/order_item')->load($id);

        if ($r->getId()) {
            return $r;
        } else {
            return null;
        }
    }

    public function startRenew($expiresOn = null) {

        if ($this->getStatus() !== self::STATUS_ACTIVE) {

            // Update subscription with new expiry date
            $subscription = $this->getSubscription();
            if ($subscription) {

                if ($expiresOn !== null) {
                    // The expiry date was given as input
                    $newExpiresOn = $expiresOn;
                } else {
                    // Calculate the new expiry date - it was not given as input
                    $product = $this->getOrderItem()->getProduct();
                    $period = $product->getSubscriptionPeriod();

                    if ($period) {
                        if ($period == Storefront_Subscriptions_Model_Source_Subscription_Period::PERIOD_PERPETUAL) {
                            $newExpiresOn = Storefront_Subscriptions_Model_Source_Subscription_Period::PERIOD_PERPETUAL_DATE;
                        } else {
                            $months = $period;

                            $currentExpiryDate = $subscription->getExpiryDate();

                            if ($currentExpiryDate) {
                                $expiryDate = $currentExpiryDate;
                            } else {
                                $expiryDate = new Zend_Date();
                            }

                            $expiryDate->addMonth($months);

                            $newExpiresOn = $expiryDate->getIso();
                        }
                    } else {
                        $newExpiresOn = null;
                    }
                }

                // Start subscription
                $subscription->setExpiresOn($newExpiresOn);
                $subscription->setStatus(Storefront_Subscriptions_Model_Subscription::STATUS_ACTIVE);


                // Start renewal
                $this->setStatus(self::STATUS_ACTIVE);
                $this->setStartedOn(date('Y-m-d H:i:s'));


                $subscription->save();
            }

            // Check the current status
            $this->setStatus(self::STATUS_ACTIVE);

            // Set the start date
            $this->setStartedOn(Varien_Date::now());

            Mage::dispatchEvent('subscriptions_start_renewal', array('renewal' => $this));

            $this->save();
        } else {
            $helper = Mage::helper('subscriptions');
            Mage::throwException($helper->__('Renewal %s is already active. Cannot activate it again.', $this->getId()));
        }
    }

    public function getSubscription() {
        $subscriptionId = $this->getSubscriptionId();
        /* @var $subscription Storefront_Subscriptions_Model_Subscription */
        $subscription = Mage::getModel('subscriptions/subscription');
        $subscription->load($subscriptionId);

        if ($subscription->getId()) {
            return $subscription;
        } else {
            return null;
        }

    }

    public static function getStatuses() {
        $helper = Mage::helper('subscriptions');
        return array(
            self::STATUS_WAIT_ON_INVOICE => $helper->__('Waiting on invoice'),
            self::STATUS_WAIT_MANUAL_ACTIVATION_BY_ADMIN => $helper->__('Waiting activation by admin'),
            self::STATUS_WAIT_MANUAL_ACTIVATION_BY_CUSTOMER => $helper->__('Waiting activation by customer'),
            self::STATUS_ACTIVE => $helper->__('Active'),
        );
    }


    /**
     *
     * @return Zend_Date
     */
    public function getStartedDate() {
        $isoString = $this->_getData('started_on');

        if ($isoString) {
            $r = new Zend_Date($isoString, Zend_Date::ISO_8601);
        } else {
            $r = null;
        }
        return $r;
    }

//    public function getLicenseRenewalQty($renewalid)
//    {
//        $resource = Mage::getSingleton('core/resource');
//        $readConnection = $resource->getConnection('core_read');
//        $license_keys_renewalTable = $resource->getTableName('license_keys_renewal');
//
//        $query = "SELECT qty FROM ". $license_keys_renewalTable ." WHERE renewal_id = ?";
//        return $readConnection->fetchOne($query, $renewalid);
//    }

    public function isReadOnly() {
        return false;
    }

    public function isDeleteable() {
        return !$this->isReadOnly();
    }

    public function validateData(Varien_Object $data) {
        return true;
    }

    public function loadPost($data) {
        foreach ($data as $key => $value) {
            $this->setData($key, $value);
        }
        return $this;
    }


}