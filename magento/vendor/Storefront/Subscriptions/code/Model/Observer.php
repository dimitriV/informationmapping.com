<?php

class Storefront_Subscriptions_Model_Observer {




    /**
     * When an invoice is created, we need to create or renew a subscription.
     * Depending on the product settings, the subscription is activated or not.
     * @param $observer
     * @return bool
     */
    public function onBeforeInvoiceSave($observer) {

        /* @var $invoice Mage_Sales_Model_Order_Invoice */
        $invoice = $observer->getEvent()->getInvoice();

        // Make sure this is a new invoice
        if ($invoice->getId()) {
            return false;
        }

        /* @var $subscriptionHelper Storefront_Subscriptions_Helper_Data */
        $subscriptionHelper = Mage::helper('subscriptions');

        if ($subscriptionHelper->isEnabled($invoice->getStoreId())) {

            foreach ($invoice->getAllItems() as $invoiceItem) {
                /* @var $invoiceItem Mage_Sales_Model_Order_Invoice_Item */
                $item = $invoiceItem->getOrderItem();


                if ($item->getProductType() === 'bundle') {
                    //Do nothing for bundle product
                    continue;
                }


                // Load the product with full data
                $product = Mage::getModel('catalog/product')->load($item->getProductId());

                if($subscriptionHelper->isSubscriptionProduct($product)){

                    $qtyNeeded = $this->_getQty($invoiceItem);

                    $isStartNewSubscription = $subscriptionHelper->isSubscriptionStartProduct($product);
                    $isRenewProduct = $subscriptionHelper->isSubscriptionRenewProduct($product);

                    for ($i = 0; $i < $qtyNeeded; $i++) {

                        if ($isStartNewSubscription) {
                            // Create a new subscription
                            $subscription = $subscriptionHelper->createNewSubscription($item);

                            if ($subscriptionHelper->isStartOnInvoice($product)) {
                                // This is an automatically starting subscription (not manual activation)
                                $subscription->startSubscription();
                            }

                            $subscription->save();

                        } elseif($isRenewProduct) {
                            // Create a renewal, but it is not linked to the original subscription.
                            $renew = $subscriptionHelper->createRenewal($item);

                            if ($subscriptionHelper->isStartOnInvoice($product)) {
                                // This is an automatically starting renew (not manual activation)
                                $renew->startRenew();
                            }

                            $renew->save();

                        }
                    }
                }
            }
        }

    }

    protected function _getQty(Mage_Sales_Model_Order_Invoice_Item $invoiceItem) {

        // TODO: check bundle qty

        $r = $invoiceItem->getQty();

        return $r;
    }

    public function rememberRenewIdOnaddToCart($observer){

        if(isset($_POST['subscription_id']) && $_POST['subscription_id']){
            /* @var $item Mage_Sales_Model_Quote_Item */
            $item = $observer->getQuoteItem();

            $subscriptionId = $_POST['subscription_id'];

            /* @var $subscription Storefront_Subscriptions_Model_Subscription */
            $subscription = Mage::getModel('subscriptions/subscription')->load($subscriptionId);
            $subscriptionStartProduct = $subscription->getProduct();

            /* @var $helper Storefront_Subscriptions_Helper_Data */
            $helper = Mage::helper('subscriptions');

            $startingProducts = $helper->getSubscriptionStartProductsForRenewal($item->getProduct());

            foreach($startingProducts as $startingProduct){
                echo "\n<br/>".$subscriptionStartProduct->getId() .' vs '. $startingProduct->getId();

                if($subscriptionStartProduct->getId() == $startingProduct->getId()){
                    // This renewal is correct for this subscription

                    $item->setRenewSubscriptionId($subscriptionId);
                    $item->save();
                }
            }

            die('END');


        }

    }

//    protected function _processSingleLicense(Mage_Sales_Model_Order_Invoice $invoice, Mage_Sales_Model_Order_Item $item) {
//
//
//
//        return;
//
//        // OLD CODE BELOW
//        $availableProductSkus = Mage::helper('licensemanager')->getAvailableProductSkus();
//        $productId = $item->getProductId();
//
//        $product = Mage::getModel('catalog/product')->load($productId);
//        $productType = $product->getAttributeText('product_type');
//        $licenseType = $product->getAttributeText('license_type');
//
//        $order = $item->getOrder();
//
//        //Make sure there are serials available to assign to this product
//        $licenseKeysInItem = $item->getCurrentLicensekey();
//        $licenseKeysInItem = array_map('trim', explode(",", trim($licenseKeysInItem)));
//        $licenseKeysInItem = array_filter($licenseKeysInItem);
//
//        if (isset($availableProductSkus[$productId]) && count($licenseKeysInItem) === 0) {
//            //Create licenseid and add license keys if the keys are available
//            if ($availableProductSkus[$productId] < $qtyNeeded) {
//                Mage::throwException("There are not enough license keys remaining for this product. Please contact administrator.");
//            }
//
//            for ($i = 0; $i < $qtyNeeded; $i++) {
//                $parentLicenseId = 0;
//
//                /* Make sure we don't assign too many license keys to a product */
//                $serialCollection = Mage::getResourceModel('licensemanager/license_pool_collection')->addFieldToFilter('order_item_id', $orderItemId)->load();
//                $assignedCount = $serialCollection->count();
//
//                if ($assignedCount >= $qtyNeeded) {
//                    // WOUTER there is already a license assigned to this order item id.
//                    // => Cancel the existing license and continue
//
//                    $this->cancelLicensKeys($orderItemId);
//
//                    // Mage::throwException("sales_order_invoice_save_before 1: Can not add license key to ($orderItemId) since it already has ($assignedCount) license keys associated with it.");
//                }
//
//                $serial_product = Mage::getModel('licensemanager/license_pool');
//                $serialNumber = $serial_product->assignSerialNumber($orderItemId, $productId, $order);
//
//                //Insert license information into license manager tables
//                $this->insertLicenseInformation($serialNumber, $item, $parentLicenseId);
//            }
//            Mage::getSingleton('adminhtml/session')->addSuccess(Mage::helper('licensemanager')->__('Successfully assigned %d license key(s)!', $qtyNeeded));
//
//        } elseif (count($licenseKeysInItem) > 0) {
//
//            if (!isset($availableProductSkus[$productId])) {
//                foreach ($licenseKeysInItem as $upgradeLicensekey) {
//                    //If current licensekey not found continue with next
//                    if (!$upgradeLicensekey) {
//                        continue;
//                    }
//
//                    $licenseKeyInfo = Mage::getModel('licensemanager/pool')->getLicensekeyInfoByKey($upgradeLicensekey);
//                    $parentLicenseId = ($licenseKeyInfo->getLicenseId()) ? $licenseKeyInfo->getLicenseId() : 0;
//
//                    /* Make sure we don't assign too many license keys to a product */
//                    $serialCollection = Mage::getResourceModel('licensemanager/license_pool_collection')->addFieldToFilter('order_item_id', $item->getId())->load();
//                    $assignedCount = count($serialCollection);
//
//                    if ($assignedCount >= $qtyNeeded) {
//                        Mage::throwException("sales_order_invoice_save_before 2: Can not add license key to ($orderItemId) since it already has ($assignedCount) license keys associated with it.");
//                    }
//
//                    $serial_product = Mage::getModel('licensemanager/licensekeys');
//                    $serial_product->updateSerialNumberAssociation($orderItemId, $productId, $order, $upgradeLicensekey);
//                    //Insert license information into license manager tables
//
//                    $this->updateLicenseUpgradeInformation($productType, $licenseType, $upgradeLicensekey, $order, $item->getSku(), $orderItem['expDate'], $orderItem['smaType'], $orderItem['smaOut'], $parentLicenseId);
//                }
//            } else {
//
//                if ($availableProductSkus[$productId] < count($licenseKeysInItem)) {
//                    Mage::throwException("There are not enough license keys remaining for this product.  Please contact administrator.");
//                }
//
//                $response = Mage::getModel('licensemanager/keys')->setCurrentLicensekeysExpired($licenseKeysInItem);
//                if ($response) {
//                    foreach ($licenseKeysInItem as $cLicensekey) {
//                        if ($cLicensekey) {
//                            $licenseKeyInfo = Mage::getModel('licensemanager/license')->getLicensekeyInfoByKey($cLicensekey);
//                            $parentLicenseId = $licenseKeyInfo->getLicenseId();
//                        }
//                        $parentLicenseId = ($parentLicenseId) ? $parentLicenseId : 0;
//
//                        /* Make sure we don't assign too many license keys to a product */
//                        $serialCollection = Mage::getResourceModel('licensemanager/license_pool_collection')->addFieldToFilter('order_item_id', $orderItemId)->load();
//                        $assignedCount = count($serialCollection);
//
//                        if ($assignedCount >= sizeof($licenseKeysInItem)) {
//                            Mage::throwException("sales_order_invoice_save_before 3 : Can not add license key to ($orderItemId) since it already has ($assignedCount) license keys associated with it.");
//                        }
//
//                        $serial_product = Mage::getModel('licensemanager/licensekeys');
//                        $serial = $serial_product->assignSerialNumber($orderItemId, $productId, $order);
//                        $upgradeLicensekey = $serial['serial'];
//
//                        //Insert license information into license manager tables
//                        $this->updateLicenseUpgradeInformation($productType, $licenseType, $cLicensekey, $order, $item->getSku(), $orderItem['expDate'], $orderItem['smaType'], $orderItem['smaOut'], $parentLicenseId, $upgradeLicensekey, true);
//                    }
//                }
//            }
//        } else {
//            Mage::throwException("There are no license keys remaining for this product.");
//        }
//
//    }
}