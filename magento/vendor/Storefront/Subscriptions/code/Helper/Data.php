<?php

class Storefront_Subscriptions_Helper_Data extends Storefront_VendorLoader_Helper_Abstract {

    public function getModuleName() {
        return 'Storefront_Subscriptions';
    }

    public function isEnabled($storeId = null) {
        return Mage::getStoreConfigFlag('subscriptions/general/enabled', $storeId);
    }

    public function isAutoRenewEnabled($storeId = null) {
        return Mage::getStoreConfigFlag('subscriptions/general/enable_auto_renew', $storeId);
    }

    /**
     * Load all subscriptions that should be auto-renew right now
     *
     * @param number $inNumberOfDays
     * @param number $limit
     */
    public function getSubscriptionsForImmediateAutoRenew($storeId) {
        $r = array();

        if ($this->isAutoRenewEnabled($storeId)) {

            /* @var $col Storefront_Subscriptions_Model_Resource_Subscription_Collection */
            $col = Mage::getResourceModel('subscriptions/subscription_collection');

            $giveUpAutoRenewAfterDays = Mage::getStoreConfig('subscriptions/general/give_up_auto_renew_days');
            $renewRetryDelayDays = Mage::getStoreConfig('subscriptions/general/auto_renew_retry_days');

            $expiryDateMin = date('Y-m-d', time() - ($giveUpAutoRenewAfterDays * 24 * 60 * 60)) . ' 00:00:00';
            $expiryDateMax = date('Y-m-d') . ' 23:59:59';

            $renewThresholdDate = date('Y-m-d H:i:s', time() - ($renewRetryDelayDays * 24 * 60 * 60));

            $col->joinOrder();
            $col->addFieldToFilter('store_id', $storeId);
            $col->addFieldToFilter('auto_renew', '1');
            $col->addFieldToFilter('last_auto_renew_failed_on', array(
                array('null' => true),
                array('lteq' => $renewThresholdDate)
            ));
            $col->addFieldToFilter('disabled', '0');
            $col->addFieldToFilter('expires_on', array('gteq' => $expiryDateMin));
            $col->addFieldToFilter('expires_on', array('lteq' => $expiryDateMax));
            $col->addFieldToFilter('status', array(
                'in' => array(
                    Storefront_Subscriptions_Model_Subscription::STATUS_ACTIVE,
                    Storefront_Subscriptions_Model_Subscription::STATUS_EXPIRED
                )
            ));

            $sql = '' . $col->getSelect();

            foreach ($col as $subscription) {
                /* @var $subscription Storefront_Subscriptions_Model_Subscription */

                if (!$subscription->hasRenewalsAwaitingManualActivation()) {
                    $r[] = $subscription;
                }

            }
        }
        return $r;

    }

    protected function _reloadProduct(Mage_Catalog_Model_Product $product) {
        $product = Mage::getModel('catalog/product')->setStoreId($product->getStoreId())->load($product->getId());
        return $product;
    }

    /**
     * Is this a product that had subscription functionality?
     * @param Mage_Catalog_Model_Product $product
     * @return bool
     */
    public function isSubscriptionProduct(Mage_Catalog_Model_Product $product){
        // Some attributes will be missing
        $product = $this->_reloadProduct($product);

        $b = $product->getSubscriptionBehaviour();

        if($b){
            return true;
        }else{
            return false;
        }
    }

    /**
     * Is the product a subscription starting product?
     * @param Mage_Catalog_Model_Product $product
     * @return bool
     */
    public function isSubscriptionStartProduct(Mage_Catalog_Model_Product $product) {
        // Some attributes will be missing
        $product = $this->_reloadProduct($product);

        $b = $product->getSubscriptionBehaviour();

        if ($b === null) {
            Mage::throwException($this->__('Subscription behaviour is not set for product ID %s', $product->getId()));
        } else {

            $subscriptionStartValues = Storefront_Subscriptions_Model_Source_Subscription_Behaviour::getStartSubscriptionValues();

            if (in_array($b, $subscriptionStartValues)) {
                return true;
            }
            return false;
        }
    }

    /**
     * Is the product a subscription renew product?
     * @param Mage_Catalog_Model_Product $product
     * @return bool
     */
    public function isSubscriptionRenewProduct(Mage_Catalog_Model_Product $product) {
        // Some attributes will be missing
        $product = $this->_reloadProduct($product);

        $b = $product->getSubscriptionBehaviour();

        if ($b === null) {
            Mage::throwException($this->__('Subscription behaviour is not set for product ID %s', $product->getId()));
        } else {
            $values = Storefront_Subscriptions_Model_Source_Subscription_Behaviour::getRenewalValues();

            if (in_array($b, $values)) {
                return true;
            }
            return false;
        }
    }

    /**
     * Get all products that could have started a subcription this product renews
     * @param Mage_Catalog_Model_Product $renewProduct
     * @return Mage_Catalog_Model_Resource_Product_Collection
     */
    public function getSubscriptionStartProductsForRenewal(Mage_Catalog_Model_Product $renewProduct) {

        $startBehaviours = Storefront_Subscriptions_Model_Source_Subscription_Behaviour::getStartSubscriptionValues();

        /* @var $col Mage_Catalog_Model_Resource_Product_Collection */
        $col = Mage::getResourceModel('catalog/product_collection');

        $attributes = Mage::getSingleton('catalog/config')->getProductAttributes();

        $col->addAttributeToSelect($attributes);

        $col->addAttributeToSelect('subscription_period');
        $col->addAttributeToFilter('subscription_behaviour', array('in' => $startBehaviours));
        $col->addAttributeToFilter('subscription_renewal_product', $renewProduct->getId());

        return $col;

    }

    /**
     * Get the renewal product for the subscription this product started.
     * @param Mage_Catalog_Model_Product $product
     * @return Mage_Core_Model_Abstract|null
     * @throws Mage_Core_Exception
     */
    public function getRenewalProductFor(Mage_Catalog_Model_Product $product) {
        if ($this->isSubscriptionStartProduct($product)) {

            //$renewalbehaviours = Storefront_Subscriptions_Model_Source_Subscription_Behaviour::getRenewalValues();

            $productId = $product->getSubscriptionRenewalProduct();

            $renewalProduct = Mage::getModel('catalog/product')->load($productId);

            if ($renewalProduct->getId()) {
                return $renewalProduct;
            } else {
                return null;
            }

        } else {
            Mage::throwException('Product ' . $product->getId() . ' is not a subscription product and cannot have a renewal product.');
        }
    }

    /**
     * Get all products that renew an existing subscription
     * @return Mage_Catalog_Model_Resource_Product_Collection
     */
    public function getAllRenewalProducts() {
        /* @var $col Mage_Catalog_Model_Resource_Product_Collection */
        $col = Mage::getResourceModel('catalog/product_collection');

        $attributes = Mage::getSingleton('catalog/config')->getProductAttributes();
        $col->addAttributeToSelect($attributes);

        $col->addAttributeToFilter('subscription_behaviour', array('in' => Storefront_Subscriptions_Model_Source_Subscription_Behaviour::getRenewalValues()));

        return $col;
    }

    /**
     * Get all products that start a subscription
     * @return Mage_Catalog_Model_Resource_Product_Collection
     */
    public function getAllSubscriptionStartProducts() {
        /* @var $col Mage_Catalog_Model_Resource_Product_Collection */
        $col = Mage::getResourceModel('catalog/product_collection');

        $attributes = Mage::getSingleton('catalog/config')->getProductAttributes();
        $col->addAttributeToSelect($attributes);

        $col->addAttributeToFilter('subscription_behaviour', array('in' => Storefront_Subscriptions_Model_Source_Subscription_Behaviour::getStartSubscriptionValues()));

        return $col;
    }

    public function getSubscriptions($customerId = null) {
        if ($customerId === null) {
            $customerId = Mage::helper('customer')->getCurrentCustomer()->getId();
        }

        /* @var $col Storefront_Subscriptions_Model_Resource_Subscription_Collection */
        $col = Mage::getResourceModel('subscriptions/subscription_collection');
        $col->joinOrder();

        $col->addFieldToFilter('customer_id', $customerId);
        $col->addFieldToFilter('disabled', '0');

        $col->setOrder('expires_on', 'ASC');

        return $col;
    }

    /**
     *
     * @param int $subscriptionId
     * @return Storefront_Subscriptions_Model_Subscription
     */
    public function getSubscriptionById($subscriptionId) {
        $col = $this->getSubscriptions();
        $col->addFieldToFilter('id', $subscriptionId);

        if ($col->count() > 0) {
            return $col->getFirstItem();
        } else {
            return null;
        }
    }

    public function isStartOnInvoice(Mage_Catalog_Model_Product $product) {
        if (in_array($product->getSubscriptionBehaviour(), Storefront_Subscriptions_Model_Source_Subscription_Behaviour::getStartOnInvoiceValues())) {
            return true;
        }
        return false;
    }

    public function createNewSubscription(Mage_Sales_Model_Order_Item $item) {
        /* @var $subscription Storefront_Subscriptions_Model_Subscription */
        $subscription = Mage::getModel('subscriptions/subscription');

        $subscription->setOrderItemId($item->getId());

        $product = $subscription->getProduct();
        $subscription->setAutoRenew($product->getSubscriptionAutoRenew());

        switch ($product->getSubscriptionBehaviour()) {
            case Storefront_Subscriptions_Model_Source_Subscription_Behaviour::SUBSCRIPTION_START_ON_ACTIVATION_BY_CUSTOMER:
                $subscription->setStatus($subscription::STATUS_WAIT_MANUAL_ACTIVATION_BY_CUSTOMER);
                break;
            case Storefront_Subscriptions_Model_Source_Subscription_Behaviour::SUBSCRIPTION_START_ON_ACTIVATION_BY_ADMIN:
                $subscription->setStatus($subscription::STATUS_WAIT_MANUAL_ACTIVATION_BY_ADMIN);
                break;
            case Storefront_Subscriptions_Model_Source_Subscription_Behaviour::SUBSCRIPTION_START_ON_INVOICE:
                $subscription->setStatus($subscription::STATUS_WAIT_ON_INVOICE);
                break;
            default:
                $subscription->setStatus($subscription::STATUS_WAIT_MANUAL_ACTIVATION_BY_ADMIN);
                break;
        }

        Mage::dispatchEvent('subscriptions_created_subscription', array('subscription' => $subscription));

        return $subscription;
    }

    public function createRenewal(Mage_Sales_Model_Order_Item $item) {
        /* @var $renew Storefront_Subscriptions_Model_Renewal */
        $renew = Mage::getModel('subscriptions/renewal');
        //$renew->setActivated(0);
        $renew->setOrderItemId($item->getId());

        $subscriptionId = $item->getRenewSubscriptionId();
        $product = $item->getProduct();


        $behaviour = $product->getSubscriptionBehaviour();

        if ($behaviour == Storefront_Subscriptions_Model_Source_Subscription_Behaviour::SUBSCRIPTION_RENEW_ON_ACTIVATION_BY_ADMIN) {
            $renew->setStatus(Storefront_Subscriptions_Model_Renewal::STATUS_WAIT_MANUAL_ACTIVATION_BY_ADMIN);

        } elseif ($behaviour == Storefront_Subscriptions_Model_Source_Subscription_Behaviour::SUBSCRIPTION_RENEW_ON_ACTIVATION_BY_CUSTOMER) {
            $renew->setStatus(Storefront_Subscriptions_Model_Renewal::STATUS_WAIT_MANUAL_ACTIVATION_BY_CUSTOMER);

        } else {
            $renew->setStatus(Storefront_Subscriptions_Model_Renewal::STATUS_WAIT_ON_INVOICE);
        }

        if ($subscriptionId) {
            $renew->setSubscriptionId($subscriptionId);

        } else {
            // No link available to original subscription

            // IMI: We are not always able to detect the original subscription at this point in time.
            // The renewal may be created without a link to the subscription, but only if this is a manual renewal.
            // The linking of the renewal to the subscription should happen as a separate process

          /*  if (!$this->isManualStart($product)) {
                $order = $item->getOrder();
                Mage::throwException($this->__('Renewal order item "%s" in order %s is not linked to a subscription.', $item->getName(), $order->getIncrementId()));
            }*/
        }

        Mage::dispatchEvent('subscriptions_renewal_created', array('renewal' => $renew));

        return $renew;
    }

    public function isManualStart(Mage_Catalog_Model_Product $product) {
        $b = $product->getSubscriptionBehaviour();
        if (in_array($b, Storefront_Subscriptions_Model_Source_Subscription_Behaviour::getStartManualValues())) {
            return true;
        }
        return false;
    }

//    public function isManualStartByAdmin(Mage_Catalog_Model_Product $product) {
//        $b = $product->getSubscriptionBehaviour();
//        if (in_array($b, Storefront_Subscriptions_Model_Source_Subscription_Behaviour::getStartManualByAdminValues())) {
//            return true;
//        }
//        return false;
//    }
//
//    public function isManualStartByCustomer(Mage_Catalog_Model_Product $product) {
//        $b = $product->getSubscriptionBehaviour();
//        if (in_array($b, Storefront_Subscriptions_Model_Source_Subscription_Behaviour::getStartManualByCustomerValues())) {
//            return true;
//        }
//        return false;
//    }

    public function getTimeInputFormat() {
//        $r = Mage::app()->getLocale()->getDateTimeFormat(Mage_Core_Model_Locale::FORMAT_TYPE_SHORT);
//
//        if (strpos($r, '-yy ') !== false) {
//            // The 2 digit year part can cause problems
//            $r = str_replace('-yy ', '-yyyy ', $r);
//        }
//
//        $r = 'yyyy-MM-dd HH:mm:ss';

        $r = Mage::app()->getLocale()->getDateTimeFormat(Mage_Core_Model_Locale::FORMAT_TYPE_SHORT);

        return $r;
    }


    public function getSubscriptionsNeedingCustomerActivation($customerId = null) {
        /* @var $col Storefront_Subscriptions_Model_Resource_Subscription_Collection */
        $col = Mage::getResourceModel('subscriptions/subscription_collection');

        $col->joinOrder();

        if ($customerId === null) {
            $customerId = Mage::getSingleton('customer/session')->getId();
            $col->addFieldToFilter('customer_id', $customerId);
        }

        //$col->addFieldToFilter('serial', array('neq' => ''));
        $col->addFieldToFilter('main_table.status', array('in' => Storefront_Subscriptions_Model_Subscription::STATUS_WAIT_MANUAL_ACTIVATION_BY_CUSTOMER));
        $col->addFieldToFilter('state', 'complete');
        $col->addFieldToFilter('disabled', '0');

        $sql = ''.$col->getSelect();

        return $col;
    }

    public function getRenewalsNeedingCustomerActivation($customerId = null) {
        /* @var $col Storefront_Subscriptions_Model_Resource_Renewal_Collection */
        $col = Mage::getResourceModel('subscriptions/renewal_collection');

        $col->joinOrder();

        if ($customerId === null) {
            $customerId = Mage::getSingleton('customer/session')->getId();
            $col->addFieldToFilter('customer_id', $customerId);
        }

        //$col->addFieldToFilter('serial', array('neq' => ''));
        $col->addFieldToFilter('main_table.status', array('in' => Storefront_Subscriptions_Model_Renewal::STATUS_WAIT_MANUAL_ACTIVATION_BY_CUSTOMER));
        $col->addFieldToFilter('state', 'complete');

        return $col;
    }

    public function getStoreIdsWithSubscriptions() {
        $allStoreIds = Mage::app()->getStores();

        $r = array();

        foreach ($allStoreIds as $store) {
            if ($this->isEnabled($store->getId())) {
                $r[] = $store->getId();
            }
        }

        return $r;
    }
}