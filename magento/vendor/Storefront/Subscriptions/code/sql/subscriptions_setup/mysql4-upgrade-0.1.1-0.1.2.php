<?php
$installer = $this;
/* @var $installer Storefront_Subscriptions_Model_Entity_Setup */

$installer->startSetup();

// Cleanup table license_skus
$db = $installer->getConnection();

$db->beginTransaction();

$rows = $db->fetchAll('SELECT * FROM license_skus');

$subscriptionBehaviourAttributeId = $db->fetchOne('SELECT attribute_id FROM eav_attribute where attribute_code = "subscription_behaviour"');
$renewalProductIdAttributeId = $db->fetchOne('SELECT attribute_id FROM eav_attribute where attribute_code = "subscription_renewal_product"');

// Example: $subscriptionBehaviourAttributeId = 580
// Example: $renewalForAttributeId = 583

foreach ($rows as $row) {

    $subscriptionStartProductId = $row['license_product_id'];
    $renewProductId = $row['activation_product_id'];

    // Set start subscription behaviour on subscription start product
    $newData = array(
        'entity_type_id' => 4,
        'attribute_id' => $subscriptionBehaviourAttributeId,
        'store_id' => 0,
        'entity_id' => $subscriptionStartProductId,
        'value' => Storefront_Subscriptions_Model_Source_Subscription_Behaviour::SUBSCRIPTION_START_ON_INVOICE
    );

    $db->insertOnDuplicate('catalog_product_entity_int', $newData, array_keys($newData));


    // Set renew behaviour on subscription renewal product
    $newData = array(
        'entity_type_id' => 4,
        'attribute_id' => $subscriptionBehaviourAttributeId,
        'store_id' => 0,
        'entity_id' => $renewProductId,
        'value' => Storefront_Subscriptions_Model_Source_Subscription_Behaviour::SUBSCRIPTION_RENEW_ON_INVOICE
    );
    $db->insertOnDuplicate('catalog_product_entity_int', $newData, array_keys($newData));


    // Link renewal product to original subscription product
    $newData = array(
        'entity_type_id' => 4,
        'attribute_id' => $renewalProductIdAttributeId,
        'store_id' => 0,
        'entity_id' => $subscriptionStartProductId,
        'value' => $renewProductId
    );
    $db->insertOnDuplicate('catalog_product_entity_int', $newData, array_keys($newData));

    //$db->delete('license_skus', 'id = "' . $row['id'] . '"');
}

$db->commit();

$installer->run("ALTER TABLE `license_skus` RENAME TO  `_license_skus` ");


//$installer->installEntities();

$installer->endSetup();
