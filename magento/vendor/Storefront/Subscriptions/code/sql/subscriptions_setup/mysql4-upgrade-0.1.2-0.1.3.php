<?php
$installer = $this;
/* @var $installer Storefront_Subscriptions_Model_Entity_Setup */

$installer->startSetup();

$installer->run("ALTER TABLE `sales_flat_quote_item` ADD COLUMN `renew_subscription_id` INT UNSIGNED NULL DEFAULT NULL;");
$installer->run("ALTER TABLE `sales_flat_order_item` ADD COLUMN `renew_subscription_id` INT UNSIGNED NULL DEFAULT NULL;");


//$installer->installEntities();

$installer->endSetup();
