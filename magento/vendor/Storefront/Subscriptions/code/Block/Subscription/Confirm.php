<?php
class Storefront_Subscriptions_Block_Subscription_Confirm extends Storefront_Subscriptions_Block_Subscription_Abstract{
	
	protected $_template = 'subscriptions/subscription/confirm.phtml';
	
	/**
	 * @return Storefront_Subscriptions_Model_Subscription
	 */
	public function getSubscription(){
		return Mage::registry('current_subscription');
	}
	
	/**
	 * @return Storefront_SavedCards_Model_Card_Abstract
	 */
	public function getCard(){
		return Mage::helper('savedcards')->getPrimaryCard();
	}
	
	public function getPriceHtml(Mage_Catalog_Model_Product $product = null){
		$priceHtml = parent::getPriceHtml($product);
		$priceHtml = str_replace('div', 'span', $priceHtml);
		return $priceHtml;
	}
	
}