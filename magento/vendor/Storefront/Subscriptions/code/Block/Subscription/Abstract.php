<?php
abstract class Storefront_Subscriptions_Block_Subscription_Abstract extends Mage_Core_Block_Template{
	
	
	public function getPriceHtml(Mage_Catalog_Model_Product $product = null){
		if($product === null){
			return '';
		}
	
		if($this->_priceBlock === null){
			$this->_priceBlock = $this->getLayout()->createBlock('catalog/product_price', 'subsciption.product.price');
			$this->_priceBlock->setTemplate('catalog/product/price.phtml');
		}
	
		$block = $this->_priceBlock;
	
		/* @var $block Mage_Catalog_Block_Product_Price */
		$block->setProduct($product);
	
		return $block->toHtml();
	}
	
}