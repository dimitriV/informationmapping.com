<?php
class Storefront_Subscriptions_Block_Subscription_Renewed extends Mage_Core_Block_Template{
	protected $_template = 'subscriptions/subscription/renewed.phtml';
	
	/**
	 * @return Storefront_Subscriptions_Model_Subscription_Abstract
	 */
	public function getSubscription(){
		
		/* @var $helper Storefront_Subscriptions_Helper_Data */
		$helper = Mage::helper('subscriptions');
		
		$subscriptionId = $this->getRequest()->getParam('id');
		
		return $helper->getSubscriptionById($subscriptionId);
	}
	
	
}