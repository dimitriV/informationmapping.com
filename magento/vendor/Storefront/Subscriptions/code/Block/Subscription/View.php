<?php
class Storefront_Subscriptions_Block_Subscription_View extends Storefront_Subscriptions_Block_Subscription_Abstract{
	protected $_template = 'subscriptions/subscription/view.phtml';

    /**
     * @return Storefront_Subscriptions_Model_Subscription
     * @throws Exception
     */
	public function getSubscriptions() {
        $subscriptionId = $this->getRequest()->getParam('id');
        $helper = Mage::helper('subscriptions');

        if($subscriptionId){
            // Show 1
            $subscription = $helper->getSubscriptionById($subscriptionId);
            if($subscription->getId()){
                return array($subscription);
            }
        }

        // Show all
        $subscriptions = $helper->getSubscriptions();
        return $subscriptions;
    }


	
	
}