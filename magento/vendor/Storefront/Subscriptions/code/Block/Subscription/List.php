<?php
class Storefront_Subscriptions_Block_Subscription_List extends Storefront_Subscriptions_Block_Subscription_Abstract{
	protected $_template = 'subscriptions/subscription/list.phtml';

	public function getSubscriptions(){
		
		/* @var $helper Storefront_Subscriptions_Helper_Data */
		$helper = Mage::helper('subscriptions');
		
		return $helper->getSubscriptions();
	}

}