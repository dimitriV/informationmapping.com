<?php
class Storefront_Subscriptions_Block_Subscription_Activationsavailablemessage extends Storefront_Subscriptions_Block_Subscription_Activationsavailable{
    protected $_template = 'subscriptions/subscription/activationsavailablemessage.phtml';

    public function getNumKeys(){

       return $this->getRenewalsAvailable()->count() + $this->getSubscriptionsAvailable()->count();

    }
}