<?php
class Storefront_Subscriptions_Block_Subscription_Activationsavailable extends Mage_Core_Block_Template{
    protected $_template = 'subscriptions/subscription/activationsavailable.phtml';

    public function getRenewalsAvailable(){

        /* @var $helper Storefront_Subscriptions_Helper_Data */
        $helper = Mage::helper('subscriptions');

        return $helper->getRenewalsNeedingCustomerActivation();

    }

    public function getSubscriptionsAvailable(){

        /* @var $helper Storefront_Subscriptions_Helper_Data */
        $helper = Mage::helper('subscriptions');

        return $helper->getSubscriptionsNeedingCustomerActivation();

    }
}