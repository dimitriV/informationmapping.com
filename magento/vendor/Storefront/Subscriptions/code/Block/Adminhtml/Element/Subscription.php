<?php
class Storefront_Subscriptions_Block_Adminhtml_Element_Subscription extends Varien_Data_Form_Element_Abstract{


    public function getElementHtml() {
        $r = '';

        /* @var $product Mage_Catalog_Model_Product */
        $product = $this->getContainer()->getContainer()->getDataObject();

        /* @var $helper Storefront_Subscriptions_Helper_Data */
        $helper = Mage::helper('subscriptions');


        $r .= '<div class="subscriptions">';

        $r .= <<<TEXT
<style>
.subscriptions h3{
    font-size: 10pt;
}

.subscriptions table{
    border: 1px solid #CCC;
    border-collapse: collapse;
    background: #FFF;
}

.subscriptions table th,
.subscriptions table td{
    border: 1px solid #CCC;
    padding: 5px;
}

</style>
TEXT;
        try{
            if($helper->isSubscriptionStartProduct($product)){
                $renewProduct = $helper->getRenewalProductFor($product);

                if($renewProduct){

                    $r .= '<h3>This subscription can be renewed by:</h3>';
                    $r .= $this->_getTableHtml($renewProduct);
                }

            }elseif($helper->isSubscriptionRenewProduct($product)){

                $r .= <<<TEXT
    <style>
    #subscription_renewal_product{
        display: none;
    }
    </style>
TEXT;

                $col = $helper->getSubscriptionStartProductsForRenewal($product);
                $r .= '<h3>This product is a renewal for:</h3>';

                $r .= $this->_getTableHtml($col);

            }
        }catch(Exception $ex){
        }

        $r .= '</div>';

        return $r;
    }

    protected function _getTableHtml($products){

        if($products instanceof Mage_Catalog_Model_Product){
            $products = array($products);
        }

        /* @var $helper Storefront_Subscriptions_Helper_Data */
        $helper = Mage::helper('subscriptions');

        $adminHelper = Mage::helper("adminhtml");

        $r = '';
        $r .= '<table>';
        $r .= '<thead>';
        $r .= '<tr>';
        $r .= '<th>'.$helper->__('ID').'</th>';
        $r .= '<th>'.$helper->__('SKU').'</th>';
        $r .= '<th>'.$helper->__('Name').'</th>';
        $r .= '<th>'.$helper->__('Period').'</th>';
        $r .= '</tr>';
        $r .= '</thead>';
        $r .= '<tbody>';

        foreach($products as $product){
            $editUrl = $adminHelper->getUrl('adminhtml/catalog_product/edit', array('id' => $product->getId()));

            $r .= '<tr>';

            $r .= '<td>'.$product->getId().'</td>';
            $r .= '<td>'.$product->getSku().'</td>';

            $r .= '<td>';
            $r .= '<a href="'.$editUrl.'">'.$product->getName().'</a>';
            $r .= '</td>';

            $r .= '<td>';
            $r .= $product->getAttributeText('subscription_period');
            $r .= '</td>';


            $r .= '</tr>';
        }

        $r .= '</tbody>';
        $r .= '</table>';

        return $r;
    }

}