<?php
class Storefront_Subscriptions_Block_Adminhtml_Element_Renew extends Varien_Data_Form_Element_Abstract{


    public function getElementHtml() {
        $r = '';

        /* @var $subscription Storefront_Subscriptions_Model_Subscription */
        $subscription = $this->getContainer()->getContainer()->getDataObject();

        /* @var $helper Storefront_Subscriptions_Helper_Data */
        $helper = Mage::helper('subscriptions');

        $adminHelper = Mage::helper("adminhtml");


        $r .= '<div class="renew">';

        $r .= <<<TEXT
<style>

</style>


TEXT;

        $errors = $subscription->getAutoRenewValidationErrors();

        if($errors){
            $r.= '<ul>';
            foreach($errors as $error){
                $r .= '<li style="color: #C00">'. $error.'</li>';
            }
            $r.= '</ul>';

        }else{

            $r .= '<span style="color: #009900">'.$helper->__('Renewal is possible (if credit card is accepted)').'</span>';

        }
        return $r;
    }


}