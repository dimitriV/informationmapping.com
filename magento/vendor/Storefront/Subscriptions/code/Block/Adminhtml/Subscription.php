<?php

class Storefront_Subscriptions_Block_Adminhtml_Subscription extends Mage_Adminhtml_Block_Template {

    public function __construct() {
        parent::__construct();
        $this->setTemplate('subscriptions/subscription/list.phtml');
    }

    protected function _prepareLayout() {
        $this->setChild('grid', $this->getLayout()->createBlock('subscriptions/adminhtml_subscription_grid', 'subscription.grid'));
        return parent::_prepareLayout();
    }

    public function getAddNewButtonHtml() {
        return $this->getChildHtml('add_new_button');
    }

    public function getGridHtml() {
        return $this->getChildHtml('grid');
    }

    public function getHeaderText(){
        return $this->__('Subscriptions');
    }

}