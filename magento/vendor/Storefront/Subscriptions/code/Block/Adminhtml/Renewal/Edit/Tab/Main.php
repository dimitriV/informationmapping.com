<?php
class Storefront_Subscriptions_Block_Adminhtml_Renewal_Edit_Tab_Main
    extends Mage_Adminhtml_Block_Widget_Form
    implements Mage_Adminhtml_Block_Widget_Tab_Interface
{
    protected function _prepareForm()
    {
        /* @var $model Storefront_Subscriptions_Model_Renewal */
        $model = Mage::registry('current_subscriptions_renewal');

        $form = new Varien_Data_Form();

        $form->setHtmlIdPrefix('renewal_');

        /* @var $helper Storefront_Subscriptions_Helper_Data */
        $helper = Mage::helper('subscriptions');

        $fieldset = $form->addFieldset('base_fieldset', array('legend'=> $helper->__('Renewal Details')));

        // Make sure we submit the ID too
        if ($model->getId()) {
            $fieldset->addField('id', 'hidden', array(
                'name' => 'id',
            ));
        }

        Mage::dispatchEvent('subscriptions_admin_renewal_edit_tab_main_prepare_form_before', array('form' => $form, 'fieldset' => $fieldset));
//
//        $fieldset->addField('title', 'text', array(
//            'name'      => 'title',
//            'label'     => Mage::helper('cms')->__('Page Title'),
//            'title'     => Mage::helper('cms')->__('Page Title'),
//            'required'  => true,
//        ));
//
//

        $fieldset->addField('subscription_id', 'text', array(
            'label'     => $helper->__('Subscription ID'),
            'title'     => $helper->__('Subscription ID'),
            'name'      => 'subscription_id',
            'required'  => true,
            'disabled'=> true
        ));


        $dateFormat = $helper->getTimeInputFormat();

        $startedDate = $model->getExpiryDate();
        if($startedDate){
            $startedDateString = $startedDate->get($dateFormat);
        }else{
            $startedDateString = null;
        }

        $fieldset->addField('started_on', 'date', array(
            'name' => 'started_on',
            'label' => $helper->__('Started'),
            'title' => $helper->__('Started'),
            'format' => $dateFormat,
            'style'     => 'width:120px',
            'image' => $this->getSkinUrl('images/grid-cal.gif'),
            'no_span'   => false,
            'time' => true,
            'value' => $startedDateString,
            'disabled'=> true
        ));


        $statusData = array(
            'label'     => $helper->__('Status'),
            'title'     => $helper->__('Status'),
            'name'      => 'status',
            'required'  => true,
//            'disabled' => true,
            'options'   => $model->getStatuses()
        );
        if($model->getStatus() !== Storefront_Subscriptions_Model_Renewal::STATUS_ACTIVE){
            $statusData['note'] = $helper->__('Use the "Activate" button to change the status.');
        }

        $fieldset->addField('status', 'select', $statusData);

        $fieldset->addField('order_item_id', 'text', array(
            'label'     => $helper->__('Order Item ID'),
            'title'     => $helper->__('Order Item ID'),
            'name'      => 'order_item_id',
            'required'  => true,
            'disabled'=> true
        ));

//        $dateFormat = $helper->getTimeInputFormat();
//
//        $startDate = $model->getStartDate();
//        if($startDate){
//            $startDateString = $startDate->get($dateFormat);
//        }else{
//            $startDateString = null;
//        }
//
//        $fieldset->addField('started_on', 'date', array(
//            'name' => 'started_on',
//            'label' => $helper->__('Start date'),
//            'title' => $helper->__('Start date'),
//            'required' => true,
//            'format' => $dateFormat,
//            'style'     => 'width:120px',
//            'image' => $this->getSkinUrl('images/grid-cal.gif'),
//            'no_span'   => false,
//            'time' => true,
//            'value' => $startDateString
//        ));
//
//        $expiryDate = $model->getExpiryDate();
//        if($expiryDate){
//            $expiryDateString = $expiryDate->get($dateFormat);
//        }else{
//            $expiryDateString = null;
//        }
//
//        $fieldset->addField('expires_on', 'date', array(
//            'name' => 'expires_on',
//            'label' => $helper->__('Expiry date'),
//            'title' => $helper->__('Expiry date'),
//            'required' => true,
//            'format' => $dateFormat,
//            'style'     => 'width:120px',
//            'image' => $this->getSkinUrl('images/grid-cal.gif'),
//            'no_span'   => false,
//            'time' => true,
//            'value' => $expiryDateString
//        ));
//
//        $fieldset->addField('auto_renew', 'select', array(
//            'label'     => $helper->__('Auto-renew'),
//            'title'     => $helper->__('Auto-renew'),
//            'name'      => 'auto_renew',
//            'required'  => true,
//            'options'   => array('0' => 'No', '1'=>'Yes')
//        ));
//
//        $fieldset->addField('disabled', 'select', array(
//            'label'     => $helper->__('Disabled'),
//            'title'     => $helper->__('Disabled'),
//            'name'      => 'disabled',
//            'required'  => true,
//            'options'   => array('0' => 'No', '1'=>'Yes'),
//            'note'     => $helper->__('A disabled subscription is invisible to the customer and cannot be renewed.')
//        ));
//
//        $fieldset->addType('renew', 'Storefront_Subscriptions_Block_Adminhtml_Element_Renew');
//        $fieldset->addField('renew', 'renew', array(
//            'label'     => $helper->__('Problems preventing renewal'),
//            'title'     => $helper->__('Problems preventing renewal'),
//            'name'      => 'renew'
//        ));

        Mage::dispatchEvent('subscriptions_admin_renewal_edit_tab_main_prepare_form_after', array('form' => $form, 'fieldset' => $fieldset));

        $form->setDataObject($model);
        //$form->setValues($model->getData());
        $form->setValues($model->getDataInLocalTimezone());

        $this->setForm($form);

        return parent::_prepareForm();
    }

    /**
     * Prepare label for tab
     *
     * @return string
     */
    public function getTabLabel()
    {
        return Mage::helper('subscriptions')->__('Renewal Information');
    }

    /**
     * Prepare title for tab
     *
     * @return string
     */
    public function getTabTitle()
    {
        return Mage::helper('subscriptions')->__('Renewal Information');
    }

    /**
     * Returns status flag about this tab can be shown or not
     *
     * @return true
     */
    public function canShowTab()
    {
        return true;
    }

    /**
     * Returns status flag about this tab hidden or not
     *
     * @return true
     */
    public function isHidden()
    {
        return false;
    }


}
