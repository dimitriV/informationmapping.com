<?php
class Storefront_Subscriptions_Block_Adminhtml_Renewal_Edit extends Mage_Adminhtml_Block_Widget_Form_Container
{
    /**
     * Initialize cms page edit block
     *
     * @return void
     */
    public function __construct()
    {
        $this->_objectId = 'id';
        $this->_controller = 'adminhtml_renewal';
        $this->_blockGroup = 'subscriptions';
        $this->_mode = 'edit';

        parent::__construct();

        $adminHelper = Mage::helper('adminhtml');
        $helper = Mage::helper('subscriptions');

        // Back should lead to subscription edit, not renewal grid (this does not exist)
        $this->removeButton('back');
        $this->_addButton('back', array(
            'label'     => $helper->__('Back to subscription'),
            'onclick'   => 'setLocation(\'' . $this->getBackUrl() . '\')',
            'class'     => 'back',
        ), -1);


        // Save and continue edit
        $this->_addButton('saveandcontinue', array(
            'label'     => Mage::helper('adminhtml')->__('Save and Continue Edit'),
            'onclick'   => 'saveAndContinueEdit()',
            'class'     => 'save',
        ), -100);
        $this->_formScripts[] = "
            function saveAndContinueEdit(){
                editForm.submit($('edit_form').action+'back/edit/');
            }
        ";


        $renewal = $this->_getRenewal();
        if($renewal->getStatus() !== Storefront_Subscriptions_Model_Renewal::STATUS_ACTIVE){
            // Activate renewal
            $this->_addButton('activate', array(
                'label'     => $helper->__('Activate renewal'),
                'onclick'   => "setLocation('".$adminHelper->getUrl('subscriptions/adminhtml_renewal/activate', array('id' => $renewal->getId())) ."')",
                'class'     => 'save',
            ), 0);
            }
    }

    /**
     * Get URL for back (reset) button
     *
     * @return string
     */
    public function getBackUrl()
    {
        return $this->getUrl('subscriptions/adminhtml_subscription/edit', array('id' => $this->_getRenewal()->getSubscriptionId()));
    }


    protected function _getRenewal(){
        $renewal = Mage::registry('current_subscriptions_renewal');
        return $renewal;
    }

    /**
     * Retrieve text for header element depending on loaded page
     *
     * @return string
     */
    public function getHeaderText()
    {
        $renewal = $this->_getRenewal();
        $helper = Mage::helper('subscriptions');

        if ($renewal->getId()) {
            return $helper->__("Edit renewal '%s'", $this->escapeHtml($renewal->getId()));
        }
        else {
            return $helper->__('New Renewal');
        }
    }



    /**
     * Getter of url for "Save and Continue" button
     * tab_id will be replaced by desired by JS later
     *
     * @return string
     */
    protected function _getSaveAndContinueUrl()
    {
        return $this->getUrl('*/*/save', array(
            '_current'   => true,
            'back'       => 'edit',
            'active_tab' => '{{tab_id}}'
        ));
    }

    /**
     * Prepare layout
     *
     * @return Mage_Core_Block_Abstract
     */
    protected function _prepareLayout()
    {
//        $layout = $this->getLayout();
//        $tabsBlock = $layout->getBlock('edit_tabs');
//        if ($tabsBlock) {
//            $tabsBlockJsObject = $tabsBlock->getJsObjectName();
//            $tabsBlockPrefix   = $tabsBlock->getId() . '_';
//
//            $this->_formScripts[] = "
//            function saveAndContinueEdit(urlTemplate) {
//                var tabsIdValue = " . $tabsBlockJsObject . ".activeTab.id;
//                var tabsBlockPrefix = '" . $tabsBlockPrefix . "';
//                if (tabsIdValue.startsWith(tabsBlockPrefix)) {
//                    tabsIdValue = tabsIdValue.substr(tabsBlockPrefix.length)
//                }
//                var template = new Template(urlTemplate, /(^|.|\\r|\\n)({{(\w+)}})/);
//                var url = template.evaluate({tab_id:tabsIdValue});
//                editForm.submit(url);
//            }
//        ";
//        }


        return parent::_prepareLayout();
    }
}
