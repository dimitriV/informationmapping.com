<?php

class Storefront_Subscriptions_Block_Adminhtml_Renewal_Edit_Tabs extends Mage_Adminhtml_Block_Widget_Tabs {

    public function __construct() {
        parent::__construct();
        $this->setId('renewal_tabs');
        $this->setDestElementId('edit_form');
        $this->setTitle(Mage::helper('subscriptions')->__('Renewal Information'));


    }

    protected function _prepareLayout() {
        parent::_prepareLayout();

        /* @var $helper Storefront_Subscriptions_Helper_Data */
        $helper = Mage::helper('subscriptions');

        $tab = $this->getRequest()->getParam('tab', '');

        $this->addTab('main', array(
            'label' => $helper->__('Renewal details'),
            'content' => $this->getLayout()->createBlock('subscriptions/adminhtml_renewal_edit_tab_main')->toHtml()
        ));




    }
}
