<?php

class Storefront_Subscriptions_Block_Adminhtml_Renewal_Grid_Column_Actions extends Mage_Adminhtml_Block_Widget_Grid_Column_Renderer_Abstract {

    public function render(Varien_Object $row) {
        $html = '';
        if ($row instanceof Storefront_Subscriptions_Model_Renewal) {
            $renewal = $row;

            $htmlParts[] = '<a href="' . $this->getUrl('subscriptions/adminhtml_renewal/edit', array('id' => $renewal->getId())) . '">' . $this->__('Edit') . '</a>';

            $html = implode(' - ',$htmlParts);

        }
        return $html;
    }
}