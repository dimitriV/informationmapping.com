<?php

class Storefront_Subscriptions_Block_Adminhtml_Subscription_Grid_Column_Status extends Mage_Adminhtml_Block_Widget_Grid_Column_Renderer_Abstract {

    public function render(Varien_Object $row) {
        $html = '';
        if ($row instanceof Storefront_Subscriptions_Model_Subscription) {
            $subscription = $row;

            $statusses = $subscription->getStatuses();

            if(isset($statusses[$subscription->getStatus()])){
                $html = $statusses[$subscription->getStatus()];

                switch($subscription->getStatus()){
                    case Storefront_Subscriptions_Model_Subscription::STATUS_ACTIVE:
                        $color = '#00AA00';
                        break;
                    case Storefront_Subscriptions_Model_Subscription::STATUS_EXPIRED:
                        $color = '#AA0000';
                        break;

                    case Storefront_Subscriptions_Model_Subscription::STATUS_WAIT_MANUAL_ACTIVATION_BY_ADMIN:
                    case Storefront_Subscriptions_Model_Subscription::STATUS_WAIT_MANUAL_ACTIVATION_BY_CUSTOMER:
                        $color = '#AAAA00';
                        break;
                }

                $html = '<span style="color: '.$color.'">'.$html.'</span>';

                $col = $subscription->getRenewalsAwaitingManualActivation();

                if($col->count() > 0){
                    /* @vra $helper Storefront_Subscriptions_Helper_Data */
                    $helper = Mage::helper('subscriptions');

                    $html .= ' <br/>';

                    if($col->count() == 1){
                        $html .= $helper->__('1 renewal is awaiting manual activation');
                    }else{
                        $html .= $helper->__('%s renewals are awaiting manual activation',$col->count());
                    }

                }
            }

        }
        return $html;
    }
}