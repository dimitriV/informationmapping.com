<?php
class Storefront_Subscriptions_Block_Adminhtml_Subscription_Edit_Tab_Main
    extends Mage_Adminhtml_Block_Widget_Form
    implements Mage_Adminhtml_Block_Widget_Tab_Interface
{
    protected function _prepareForm()
    {
        /* @var $model Storefront_Subscriptions_Model_Subscription */
        $model = Mage::registry('current_subscriptions_subscription');


        $form = new Varien_Data_Form();

        $form->setHtmlIdPrefix('subscription');

        /* @var $helper Storefront_Subscriptions_Helper_Data */
        $helper = Mage::helper('subscriptions');

        $fieldset = $form->addFieldset('base_fieldset', array('legend'=> $helper->__('Subscription Details')));

        // Make sure we submit the ID too
        if ($model->getId()) {
            $fieldset->addField('id', 'hidden', array(
                'name' => 'id',
            ));
        }

        Mage::dispatchEvent('subscriptions_admin_subscription_edit_tab_main_prepare_form_before', array('form' => $form, 'fieldset' => $fieldset));
//
//        $fieldset->addField('title', 'text', array(
//            'name'      => 'title',
//            'label'     => Mage::helper('cms')->__('Page Title'),
//            'title'     => Mage::helper('cms')->__('Page Title'),
//            'required'  => true,
//        ));
//
//
        $fieldset->addField('status', 'select', array(
            'label'     => $helper->__('Status'),
            'title'     => $helper->__('Status'),
            'name'      => 'status',
            'required'  => true,
            'options'   => $model->getAvailableStatuses(),
        ));

        $dateFormat = $helper->getTimeInputFormat();


        $startDateString = $model->getStartedOn();
//        if($startDate){
//            $startDateString = $startDate->get($dateFormat);
//        }else{
//            $startDateString = null;
//        }

        $fieldset->addField('started_on', 'date', array(
            'name' => 'started_on',
            'label' => $helper->__('Start date'),
            'title' => $helper->__('Start date'),
            'required' => true,
            'format' => $dateFormat,
            'style'     => 'width:120px',
            'image' => $this->getSkinUrl('images/grid-cal.gif'),
            'no_span'   => false,
            'time' => true,
            'value' => $startDateString
        ));

        $expiryDate = $model->getExpiryDate();
        if($expiryDate){
            $expiryDateString = $expiryDate->get($dateFormat);
        }else{
            $expiryDateString = null;
        }

        $fieldset->addField('expires_on', 'date', array(
            'name' => 'expires_on',
            'label' => $helper->__('Expiry date'),
            'title' => $helper->__('Expiry date'),
            'required' => true,
            'format' => $dateFormat,
            'style'     => 'width:120px',
            'image' => $this->getSkinUrl('images/grid-cal.gif'),
            'no_span'   => false,
            'time' => true,
            'value' => $expiryDateString
        ));

        $fieldset->addField('auto_renew', 'select', array(
            'label'     => $helper->__('Auto-renew'),
            'title'     => $helper->__('Auto-renew'),
            'name'      => 'auto_renew',
            'required'  => true,
            'options'   => array('0' => 'No', '1'=>'Yes')
        ));

        $fieldset->addField('disabled', 'select', array(
            'label'     => $helper->__('Hidden'),
            'title'     => $helper->__('Hidden'),
            'name'      => 'disabled',
            'required'  => true,
            'options'   => array('0' => 'No', '1'=>'Yes'),
            'note'     => $helper->__('A hidden subscription is invisible to the customer and cannot be renewed.')
        ));

        $fieldset->addType('renew', 'Storefront_Subscriptions_Block_Adminhtml_Element_Renew');
        $fieldset->addField('renew', 'renew', array(
            'label'     => $helper->__('Problems preventing renewal'),
            'title'     => $helper->__('Problems preventing renewal'),
            'name'      => 'renew'
        ));

        Mage::dispatchEvent('subscriptions_admin_subscription_edit_tab_main_prepare_form_after', array('form' => $form, 'fieldset' => $fieldset));

        $form->setDataObject($model);

        // getAdminFormData() will return datetime in the GMT timezone!
        $form->setValues($model->getDataInLocalTimezone());
//        $form->setValues($model->getData());
        $this->setForm($form);

        return parent::_prepareForm();
    }

    /**
     * Prepare label for tab
     *
     * @return string
     */
    public function getTabLabel()
    {
        return Mage::helper('cms')->__('Page Information');
    }

    /**
     * Prepare title for tab
     *
     * @return string
     */
    public function getTabTitle()
    {
        return Mage::helper('cms')->__('Page Information');
    }

    /**
     * Returns status flag about this tab can be shown or not
     *
     * @return true
     */
    public function canShowTab()
    {
        return true;
    }

    /**
     * Returns status flag about this tab hidden or not
     *
     * @return true
     */
    public function isHidden()
    {
        return false;
    }


}
