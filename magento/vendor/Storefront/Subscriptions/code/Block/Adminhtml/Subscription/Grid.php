<?php

class Storefront_Subscriptions_Block_Adminhtml_Subscription_Grid extends Mage_Adminhtml_Block_Widget_Grid {

    public function __construct() {
        parent::__construct();
        $this->setId('id');
        $this->setDefaultSort('id');
        $this->setDefaultDir('desc');
        $this->setSaveParametersInSession(true);
        //$this->setUseAjax(true);
        //$this->setRowClickCallback(false);
    }

    public function _prepareCollection() {
        /* @var $col Storefront_Subscriptions_Model_Resource_Subscription_Collection */
        $col = Mage::getResourceModel('subscriptions/subscription_collection');
        $col->joinOrder();

        $this->setCollection($col);

        parent::_prepareCollection();

        return $this;
    }

    public function _prepareColumns() {

        $showStartedOn = Mage::getStoreConfigFlag('subscriptions/subscriptions_grid/show_started_on');
        $showFirstName = Mage::getStoreConfigFlag('subscriptions/subscriptions_grid/show_firstname');
        $showLastName = Mage::getStoreConfigFlag('subscriptions/subscriptions_grid/show_lastname');
        $showCustomerEmail = Mage::getStoreConfigFlag('subscriptions/subscriptions_grid/show_customer_email');

        $this->addColumn('id', array(
                'header' => $this->__('ID'),
                'width' => '100px',
                'index' => 'id',
                'type' => 'number'
            ));

        $this->addColumn('status', array(
            'header' => $this->__('Status'),
            'index' => 'status',
            'width' => '100px',
            'type' => 'options',
            'options' => Storefront_Subscriptions_Model_Subscription::getStatuses(),
            'renderer' => 'subscriptions/adminhtml_subscription_grid_column_status'
        ));

//        $this->addColumn('days_to_expiry', array(
//            'header' => $this->__('Days to expiry'),
//            'index' => 'days_to_expiry',
//            'width' => '100px',
//            'type' => 'number',
//          //  'filter_condition_callback' => array($this, '_filterDaysToExpiry'),
//        ));

        $yesNoSource = new Mage_Adminhtml_Model_System_Config_Source_Yesno();
        $yesNoOptions = $yesNoSource->toArray();
        $this->addColumn('auto_renew', array(
            'header' => $this->__('Auto renew'),
            'index' => 'auto_renew',
            'type' => 'options',
            'options' => $yesNoOptions
        ));

        $this->addColumn('disabled', array(
            'header' => $this->__('Hidden'),
            'index' => 'disabled',
            'type' => 'options',
            'options' => $yesNoOptions
        ));

        if($showStartedOn) {
            $this->addColumn('started_on', array(
                'header' => $this->__('Started'),
                'index' => 'started_on',
                'type' => 'datetime'
            ));
        }

        $this->addColumn('expires_on', array(
            'header' => $this->__('Expires'),
            'index' => 'expires_on',
            'type' => 'datetime'
        ));

        if($showFirstName){
            $this->addColumn('customer_firstname', array(
                'header' => $this->__('First name'),
                'index' => 'customer_firstname'
            ));
        }

        if($showLastName) {
            $this->addColumn('customer_lastname', array(
                'header' => $this->__('Last name'),
                'index' => 'customer_lastname'
            ));
        }
        if($showCustomerEmail) {
            $this->addColumn('customer_email', array(
                'header' => $this->__('Customer Email'),
                'index' => 'customer_email',
                'renderer' => 'subscriptions/adminhtml_subscription_grid_column_customeremail'
            ));
        }
        $this->addColumn('increment_id', array(
            'header' => $this->__('# Order'),
            'index' => 'increment_id',
            'renderer' => 'subscriptions/adminhtml_subscription_grid_column_incrementid'
        ));

        $this->addColumn('activation_email', array(
            'header' => $this->__('Email'),
            'index' => 'activation_email'
        ));

        // company is not available. Needs more joins...
//        $this->addColumn('customer_company', array(
//            'header' => $this->__('Company'),
//            'index' => 'customer_company'
//        ));

//        $this->addColumn('product_id', array(
//            'header' => $this->__('Product ID'),
//            'index' => 'product_id',
//            'type' => 'number'
//        ));

        $this->addColumn('product_name', array(
            'header' => $this->__('Product Name'),
            'index' => 'product_name',
            'width' => '200px',
            'renderer' => 'subscriptions/adminhtml_subscription_grid_column_productname'
        ));

        $this->addColumn('renewal_product_name', array(
            'header' => $this->__('Renewal Product'),
            'index' => 'renewal_product_name',
            'width' => '200px',
            'renderer' => 'subscriptions/adminhtml_subscription_grid_column_renewalproductname',
            'filter' => false,
            'sortable' => false
        ));

        $this->addColumn('actions', array(
            'header' => $this->__('Actions'),
            'width' => '200px',
            'filter' => false,
            'sortable' => false,
            'renderer' => 'subscriptions/adminhtml_subscription_grid_column_actions'
        ));

        Mage::dispatchEvent('subscriptions_admin_subscription_grid_prepare_columns', array('grid' => $this));

        return parent::_prepareColumns();
    }

    /*
    protected function _filterDaysToExpiry($collection, $column){
        if (!$value = $column->getFilter()->getValue()) {
            return $this;
        }

        $this->getCollection()->getSelect()->where("days_to_expiry > ".$value['from']);

        return $this;
    }
    */

    public function _prepareMassaction() {
        $this->setMassactionIdField('ids');
        $this->getMassactionBlock()->setFormFieldName('ids');


        $this->getMassactionBlock()->addItem('disable', array(
            'label' => $this->__('Hide subscriptions'),
            'url' => $this->getUrl('*/*/massDisable'),
            'confirm' => $this->__('Are you sure you want to hide these subscriptions? (this action cannot be undone)')
        ));

        $this->getMassactionBlock()->addItem('enableautorenew', array(
            'label' => $this->__('Enable Auto-Renew'),
            'url' => $this->getUrl('*/*/massEnableAutoRenew'),
            'confirm' => $this->__('Are you sure you want to enable auto-renew for these subscriptions? (this action cannot be undone)')
        ));

        $this->getMassactionBlock()->addItem('disableautorenew', array(
            'label' => $this->__('Disable Auto-Renew'),
            'url' => $this->getUrl('*/*/massDisableAutoRenew'),
            'confirm' => $this->__('Are you sure you want to disable auto-renew for these subscriptions? (this action cannot be undone)')
        ));

        $this->getMassactionBlock()->addItem('send_renew_reminder_email', array(
            'label' => $this->__('Send renew reminder email'),
            'url' => $this->getUrl('*/*/massSendRenewReminderEmail')
        ));

        $this->getMassactionBlock()->addItem('renew', array(
            'label' => $this->__('Renew now'),
            'url' => $this->getUrl('*/*/massRenew'),
            'confirm' => $this->__('Are you sure you want to renew these subscriptions? Customers will be billed automatically. (this action cannot be undone)')
        ));

//        $this->getMassactionBlock()->addItem('delete', array(
//            'label' => $this->__('Delete'),
//            'url' => $this->getUrl('*/*/massDelete'),
//            'confirm' => $this->__('Are you sure you want to delete these subscriptions? (this action cannot be undone)')
//        ));



        return $this;
    }
//
//    public function getGridUrl() {
//        return $this->getUrl('*/*/grid', array('_current' => true));
//    }

    public function getRowUrl($row) {
        return $this->getUrl('*/*/edit', array('id' => $row->getId()));
    }
}