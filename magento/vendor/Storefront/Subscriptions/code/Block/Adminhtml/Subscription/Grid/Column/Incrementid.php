<?php

class Storefront_Subscriptions_Block_Adminhtml_Subscription_Grid_Column_Incrementid extends Mage_Adminhtml_Block_Widget_Grid_Column_Renderer_Abstract {

    protected static $_output = array();

    public function render(Varien_Object $row) {
        $html = '';
        if ($row instanceof Storefront_Subscriptions_Model_Subscription) {
            $subscription = $row;

            $incrementId = $subscription->getOrderId();
            if($incrementId){
                $html .= '<a href="'.$this->getUrl('adminhtml/sales_order/view/order_id/', array('order_id' => $incrementId)).'">';
            }

            $html .= $subscription->getIncrementId();

            if($incrementId) {
                $html .= '</a>';
            }

        }
        return $html;
    }
}