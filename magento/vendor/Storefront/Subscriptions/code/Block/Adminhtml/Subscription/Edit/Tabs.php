<?php

class Storefront_Subscriptions_Block_Adminhtml_Subscription_Edit_Tabs extends Mage_Adminhtml_Block_Widget_Tabs {

    public function __construct() {
        parent::__construct();
        $this->setId('subscription_tabs');
        $this->setDestElementId('edit_form');
        $this->setTitle(Mage::helper('subscriptions')->__('Subscription Information'));


    }

    protected function _prepareLayout() {
        parent::_prepareLayout();

        /* @var $helper Storefront_Subscriptions_Helper_Data */
        $helper = Mage::helper('subscriptions');

        $tab = $this->getRequest()->getParam('tab', '');

        $this->addTab('main', array(
            'label' => $helper->__('Subscription details'),
            'content' => $this->getLayout()->createBlock('subscriptions/adminhtml_subscription_edit_tab_main')->toHtml()
        ));

        $this->addTab('renewals', array(
            'label' => $helper->__('Renewals'),
            'content' => $this->getLayout()->createBlock('subscriptions/adminhtml_subscription_edit_tab_renewals')->toHtml()
        ));

//         $this->addTab('overview', array(
//                 'label'     => $helper->__('Overview'),
//                 'active'    => ($tab == ''),
//         		'class'	=> 'ajax',
//         		'url' => $this->getUrl('*/*/maintab', array('_current' => true))
// 		));


    }
}
