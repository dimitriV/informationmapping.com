<?php

class Storefront_Subscriptions_Block_Adminhtml_Subscription_Grid_Column_Renewalproductname extends Mage_Adminhtml_Block_Widget_Grid_Column_Renderer_Abstract {

    protected static $_output = array();

    public function render(Varien_Object $row) {
        $html = '';
        if ($row instanceof Storefront_Subscriptions_Model_Subscription) {

            $subscription = $row;

            $productId = $subscription->getProductId();

            if(!array_key_exists($productId, self::$_output)){

                $renewalProduct = $subscription->getRenewProduct();
                if($renewalProduct){
                    $productId = $renewalProduct->getId();
                    $productName = $renewalProduct->getName();

                    $html = '<a href="'.$this->getUrl('adminhtml/catalog_product/edit', array('id' => $productId)).'">'.$productName.'</a>';
                }

                self::$_output[$productId] = $html;
            }else{
                $html = self::$_output[$productId];
            }

        }
        return $html;
    }
}