<?php

class Storefront_Subscriptions_Block_Adminhtml_Subscription_Edit_Tab_Renewals extends Mage_Adminhtml_Block_Widget_Grid implements Mage_Adminhtml_Block_Widget_Tab_Interface{

    protected $_defaultSort = 'id';
    protected $_subscription;

    /**
     * Initialize Grid
     *
     */
    public function __construct() {
        parent::__construct();
        $this->setId('renewalsGrid');
        $this->setUseAjax(true);
        $this->_parentTemplate = $this->getTemplate();
        $this->setTemplate('subscriptions/subscription/tab/renewals.phtml');
        $this->setEmptyText(Mage::helper('customer')->__('No Items Found'));
    }

    /**
     * @return Storefront_Subscriptions_Model_Subscription
     */
    protected function _getSubscription() {
        if ($this->_subscription === null) {
            $subscription = Mage::registry('current_subscriptions_subscription');
            if (!$subscription) {
                $subscriptionId = $this->getRequest()->getParam('id');
                $subscription = Mage::getModel('subscriptions/subscription')->load($subscriptionId);
            }

            $this->_subscription = $subscription;
        }
        return $this->_subscription;
    }

    public function getTabUrl(){
        return '#renewals';
    }

    protected function _createCollection() {
        $subscription = $this->_getSubscription();

        $renewals = $subscription->getRenewalCollection();

        return $renewals;
    }


    protected function _prepareCollection() {
        $collection = $this->_createCollection();
        $collection->addFieldToFilter('subscription_id', $this->_getSubscription()->getId());
        $this->setCollection($collection);

        return parent::_prepareCollection();
    }


    protected function _prepareColumns() {
        $helper = Mage::helper('subscriptions');
        $adminHelper = Mage::helper('adminhtml');

        // We use a prefix "r_" to prevent duplicate form names with other tabs
        $this->addColumn('r_id', array(
            'header' => $helper->__('ID'),
            'index' => 'id',
            'type' => 'number'
        ));

        $this->addColumn('r_status', array(
            'header' => $helper->__('Status'),
            'index' => 'status',
            'type' => 'options',
            'options' => Storefront_Subscriptions_Model_Renewal::getStatuses(),
        ));

        $this->addColumn('r_started_on', array(
            'header' => $helper->__('Started'),
            'index' => 'started_on',
            'type' => 'datetime'
        ));

//        $this->addColumn('description', array(
//            'header' => Mage::helper('wishlist')->__('User Description'),
//            'index' => 'description',
//            'renderer' => 'adminhtml/customer_edit_tab_wishlist_grid_renderer_description'
//        ));
//
//        $this->addColumn('qty', array(
//            'header' => Mage::helper('catalog')->__('Qty'),
//            'index' => 'qty',
//            'type' => 'number',
//            'width' => '60px'
//        ));
//
//        if (!Mage::app()->isSingleStoreMode()) {
//            $this->addColumn('store', array(
//                'header' => Mage::helper('wishlist')->__('Added From'),
//                'index' => 'store_id',
//                'type' => 'store',
//                'width' => '160px'
//            ));
//        }
//
//        $this->addColumn('added_at', array(
//            'header' => Mage::helper('wishlist')->__('Date Added'),
//            'index' => 'added_at',
//            'gmtoffset' => true,
//            'type' => 'date'
//        ));
//
//        $this->addColumn('days', array(
//            'header' => Mage::helper('wishlist')->__('Days in Wishlist'),
//            'index' => 'days_in_wishlist',
//            'type' => 'number'
//        ));


        $this->addColumn('r_order_item_id', array(
            'header' => $helper->__('Order Item ID'),
            'index' => 'order_item_id',
            'type' => 'number'
        ));

        $this->addColumn('r_action', array(
            'header' => Mage::helper('customer')->__('Action'),
            'index' => 'wishlist_item_id',
            'renderer' => 'subscriptions/adminhtml_renewal_grid_column_actions',
            'filter' => false,
            'sortable' => false
        ));

        Mage::dispatchEvent('subscriptions_admin_renewal_grid_prepare_columns', array('grid' => $this));


        return parent::_prepareColumns();
    }

    /**
     * Retrieve Grid URL
     *
     * @return string
     */
    public function getGridUrl() {
        return $this->getUrl('*/*/renewalsgrid', array('_current' => true));
    }

    /**
     * Retrieve Grid Parent Block HTML
     *
     * @return string
     */
    public function getGridParentHtml() {
        $templateName = Mage::getDesign()->getTemplateFilename($this->_parentTemplate, array('_relative' => true));
        return $this->fetchView($templateName);
    }

    /**
     * Retrieve Row click URL
     *
     * @return string
     */
    public function getRowUrl($row) {
        return $this->getUrl('*/adminhtml_renewal/edit', array('id' => $row->getId()));
    }


    /**
     * Return Tab label
     *
     * @return string
     */
    public function getTabLabel() {
        return 'label';
    }

    /**
     * Return Tab title
     *
     * @return string
     */
    public function getTabTitle() {
        return 'title';
    }

    /**
     * Can show tab in tabs
     *
     * @return boolean
     */
    public function canShowTab() {
     return true;
    }

    /**
     * Tab is hidden
     *
     * @return boolean
     */
    public function isHidden() {
        return false;
    }
}
