<?php

class Storefront_Subscriptions_Block_Adminhtml_Subscription_Grid_Column_Customeremail extends Mage_Adminhtml_Block_Widget_Grid_Column_Renderer_Abstract {

    public function render(Varien_Object $row) {
        $html = '';
        if ($row instanceof Storefront_Subscriptions_Model_Subscription) {
            $subscription = $row;

            $customerId = $subscription->getCustomerId();
            if($customerId){
                $html .= '<a href="'.$this->getUrl('adminhtml/customer/edit', array('id' => $customerId)).'">';
            }

            $html .= $subscription->getCustomerEmail();

            if($customerId) {
                $html .= '</a>';
            }

        }
        return $html;
    }
}