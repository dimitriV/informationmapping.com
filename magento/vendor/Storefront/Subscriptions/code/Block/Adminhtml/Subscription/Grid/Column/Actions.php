<?php

class Storefront_Subscriptions_Block_Adminhtml_Subscription_Grid_Column_Actions extends Mage_Adminhtml_Block_Widget_Grid_Column_Renderer_Abstract {

    public function render(Varien_Object $row) {
        $html = '';
        if ($row instanceof Storefront_Subscriptions_Model_Subscription) {
            $subscription = $row;

            if ($row->getAutoRenew()) {
                $htmlParts[] = '<a href="' . $this->getUrl('*/*/setautorenew', array(
                        'id' => $row->getId(),
                        'value' => '0'
                    )) . '">' . $this->__('Disable auto-renew') . '</a>';
            } else {
                $htmlParts[] = '<a href="' . $this->getUrl('*/*/setautorenew', array(
                        'id' => $row->getId(),
                        'value' => '1'
                    )) . '">' . $this->__('Enable auto-renew') . '</a>';
            }

            if ($subscription->canAutoRenew() === true) {
                $htmlParts[] = '<a href="' . $this->getUrl('*/*/renew', array('id' => $row->getId())) . '">' . $this->__('Renew now') . '</a>';
            }

            $html = implode('<br />',$htmlParts);

        }
        return $html;
    }
}