<?php

class Storefront_Subscriptions_Block_Adminhtml_Subscription_Grid_Column_Productname extends Mage_Adminhtml_Block_Widget_Grid_Column_Renderer_Abstract {

    public function render(Varien_Object $row) {
        $html = '';
        if ($row instanceof Storefront_Subscriptions_Model_Subscription) {

            $subscription = $row;

            $productName = $subscription->getProductName();

            $html = '<a href="'.$this->getUrl('adminhtml/catalog_product/edit', array('id' => $row->getProductId())).'">'.$productName.'</a>';

        }
        return $html;
    }
}