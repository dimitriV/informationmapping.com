<?php

class Storefront_Subscriptions_Block_Adminhtml_Product extends Mage_Adminhtml_Block_Template {

    public function __construct() {
        parent::__construct();
        $this->setTemplate('subscriptions/product/list.phtml');
    }

    protected function _prepareLayout() {
        $this->setChild('grid', $this->getLayout()->createBlock('subscriptions/adminhtml_product_grid', 'product.grid'));
        return parent::_prepareLayout();
    }

    public function getGridHtml() {
        return $this->getChildHtml('grid');
    }

    public function getHeaderText(){
        return $this->__('Subscription products');
    }

}