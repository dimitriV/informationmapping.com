<?php

class Storefront_Subscriptions_Block_Adminhtml_Product_Grid extends Mage_Adminhtml_Block_Widget_Grid {

    public function __construct() {
        parent::__construct();
        $this->setId('id');
        $this->setDefaultSort('id');
        $this->setDefaultDir('desc');
        $this->setSaveParametersInSession(true);
        //$this->setUseAjax(true);
        //$this->setRowClickCallback(false);
    }

    public function _prepareCollection() {
        /* @var $col Mage_Catalog_Model_Resource_Product_Collection */
        $col = Mage::getResourceModel('catalog/product_collection');

        $col->addAttributeToSelect(array(
            'name',
            'subscription_behaviour',
            'subscription_period',
            'subscription_renewal_product',
            'subscription_auto_renew',
            'subscription_start_from'
        ));

        // Only show subscription products
        $col->addFieldToFilter('subscription_behaviour', array('notnull' => true));

        $this->setCollection($col);

        parent::_prepareCollection();

        return $this;
    }

    public function _prepareColumns() {

        $this->addColumn('entity_id', array(
            'header' => $this->__('ID'),
            'width' => '100px',
            'index' => 'entity_id',
            'type' => 'number'
        ));

        $this->addColumn('name', array(
            'header' => $this->__('Name'),
            'index' => 'name'
        ));

        $this->addColumn('sku', array(
            'header' => $this->__('SKU'),
            'index' => 'sku',
            'width' => '100px'
        ));

        $behaviourModel = new Storefront_Subscriptions_Model_Source_Subscription_Behaviour();
        $this->addColumn('subscription_behaviour', array(
            'header' => $this->__('Behaviour'),
            'index' => 'subscription_behaviour',
            'width' => '100px',
            'type' => 'options',
            'options' => $behaviourModel->getAllOptions()

        ));

        $periodModel = new Storefront_Subscriptions_Model_Source_Subscription_Period();
        $this->addColumn('subscription_period', array(
            'header' => $this->__('Period'),
            'index' => 'subscription_period',
            'width' => '100px',
            'type' => 'options',
            'options' => $periodModel->getAllOptions()
        ));


        $this->addColumn('subscription_renewal_product', array(
            'header' => $this->__('Renewal product'),
            'index' => 'subscription_renewal_product',
            'width' => '100px',
            'renderer' => 'subscriptions/adminhtml_product_grid_column_renewalproductname'
        ));

        $yesNoSource = new Mage_Adminhtml_Model_System_Config_Source_Yesno();
        $yesNoOptions = $yesNoSource->toArray();
        $this->addColumn('subscription_auto_renew', array(
            'header' => $this->__('Auto-renew by default'),
            'index' => 'subscription_auto_renew',
            'width' => '50px',
            'type' => 'options',
            'options' => $yesNoOptions
        ));

        $startFromModel = new Storefront_Subscriptions_Model_Source_Subscription_Startfrom();
        $this->addColumn('subscription_start_from', array(
            'header' => $this->__('Start from'),
            'index' => 'subscription_start_from',
            'width' => '100px',
            'type' => 'options',
            'options' => $startFromModel->getAllOptions()
        ));


        Mage::dispatchEvent('subscriptions_admin_product_grid_prepare_columns', array('grid' => $this));

        return parent::_prepareColumns();
    }

    /*
    protected function _filterDaysToExpiry($collection, $column){
        if (!$value = $column->getFilter()->getValue()) {
            return $this;
        }

        $this->getCollection()->getSelect()->where("days_to_expiry > ".$value['from']);

        return $this;
    }
    */

//
//    public function getGridUrl() {
//        return $this->getUrl('*/*/grid', array('_current' => true));
//    }

    public function getRowUrl($row) {
        return $this->getUrl('adminhtml/catalog_product/edit', array('id' => $row->getId()));
    }
}