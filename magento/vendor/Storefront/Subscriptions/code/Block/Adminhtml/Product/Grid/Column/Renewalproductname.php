<?php

class Storefront_Subscriptions_Block_Adminhtml_Product_Grid_Column_Renewalproductname extends Mage_Adminhtml_Block_Widget_Grid_Column_Renderer_Abstract {

    public function render(Varien_Object $row) {
        $html = '';
        if ($row instanceof Mage_Catalog_Model_Product) {

            $product = $row;

            $renewalProductId = $product->getSubscriptionRenewalProduct();

            $renewalProduct = Mage::getModel('catalog/product')->load($renewalProductId);
            $renewalProductName = $renewalProduct->getName();

            $html = '<a href="'.$this->getUrl('adminhtml/catalog_product/edit', array('id' => $renewalProduct->getId())).'">'.$renewalProductName.'</a>';

        }
        return $html;
    }
}