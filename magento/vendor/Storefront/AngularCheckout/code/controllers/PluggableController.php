<?php

class Storefront_AngularCheckout_PluggableController extends Mage_Core_Controller_Front_Action {

	public function cssjsAction () {
		$cacheKey = 'angularcheckout_pluggable_cssjs_' . Mage::app()->getStore()->getId();
		
		$cache = Mage::app()->getCache();
		
		$useCache = Mage::app()->useCache('block_html');
		if ($useCache) {
			$jsonString = $cache->load($cacheKey);
		} else {
			$jsonString = false;
		}
		
		if ($jsonString === false) {
			
			$handles = array(
					'angularcheckout_checkout_index'
			);
			$this->loadLayout($handles);
			
			/* @var $checkoutBlock Storefront_AngularCheckout_Block_Checkout */
			$checkoutBlock = $this->getLayout()->getBlock('angularcheckout');
			
			$scriptUrls = array();
			foreach ($checkoutBlock->getExtraSkinJs() as $js) {
				$scriptUrls[] = $checkoutBlock->getSkinUrl($js);
			}
			
			$cssUrls = array();
			foreach ($checkoutBlock->getExtraSkinCss() as $css) {
				$cssUrls[] = $checkoutBlock->getSkinUrl($css);
			}
			
			$data = array();
			$data['script'] = $scriptUrls;
			$data['css'] = $cssUrls;
			
			$jsonString = Zend_Json::encode($data);
			
			if ($useCache) {
				$cache->save($jsonString, $cacheKey, array(
						Mage_Core_Block_Abstract::CACHE_GROUP,
						'layout',
						'config',
						'translate'
				), 24 * 60 * 60);
			}
		}
		
		$this->_renderJson($jsonString);
	}

	public function htmlAction () {
		$cacheKey = 'angularcheckout_pluggable_html_' . Mage::app()->getStore()->getId();
		
		$bundleProduct = null;
		$bundleSku = $this->getRequest()->getParam('bundle');
		if ($bundleSku) {
			
			$bundleProduct = Mage::getModel("catalog/product");
			$productId = $bundleProduct->getIdBySku($bundleSku);
			
			$bundleProduct->load($productId);
			
			if ($bundleProduct->getId()) {
				$cacheKey .= '-' . $bundleSku;
			} else {
				Mage::throwException('Bundle SKU "' . $bundleSku . '" not found');
			}
		}
		
		$cache = Mage::app()->getCache();
		
		$useCache = Mage::app()->useCache('block_html');
		if ($useCache) {
			$jsonString = $cache->load($cacheKey);
		} else {
			$jsonString = false;
		}
		
		if ($jsonString === false) {
			
			$handles = array(
					'angularcheckout_checkout_index'
			);
			$this->loadLayout($handles);
			
			/* @var $checkoutBlock Storefront_AngularCheckout_Block_Pluggablecheckout */
			$checkoutBlock = $this->getLayout()->getBlock('angularcheckout');
			
			$checkoutBlock->disableJsonInHtml();
			$checkoutBlock->setUseAsPluggable(true);
			$checkoutBlock->setUseBootstrapIcons(true);
			
			if ($bundleProduct) {
				$checkoutBlock->setBundleProduct($bundleProduct);
			}
			
			$data = array();
			$data['html'] = $checkoutBlock->toHtml();
			
			$jsonString = Zend_Json::encode($data);
			
			if ($useCache) {
				$cache->save($jsonString, $cacheKey, array(
						Mage_Core_Block_Abstract::CACHE_GROUP,
						'layout',
						'config',
						'translate'
				), 24 * 60 * 60);
			}
		}
		
		$this->_renderJson($jsonString);
	}

	public function seproductbundleAction () {
		$sku = $this->getRequest()->getParam('sku');
		
		$col = Mage::getModel('catalog/product')->getCollection()->addAttributeToFilter('sku', $sku);
		
		if ($col->count() > 0) {
			$productId = $col->getFirstItem()->getId();
			
			// TODO add check if this is a bundle type
			
			/* @var $jsonModel Storefront_AngularCheckout_Model_Json */
			$jsonModel = Mage::getSingleton('angularcheckout/json');
			
			$jsonModel->getSession()->setBundleProductId($productId);
			
			$data = array(
					'status' => 'ok'
			);
			
		} else {
			$data = array(
					'status' => 'error',
					'error_msg' => $this->__('Product not found')
			);
			
			$this->getResponse()->setHeader('HTTP/1.1', '404 Product not found');
			// $this->getResponse()->setHeader('Status','404 File not found');
		}
		
		$this->_renderJson(Zend_Json::encode($data));
	}

	public function emptycartAction () {
		foreach (Mage::getSingleton('checkout/session')->getQuote()->getItemsCollection() as $item) {
			Mage::getSingleton('checkout/cart')->removeItem($item->getId())->save();
		}
		
		$this->_renderJson(Zend_Json::encode(array(
				'status' => 'ok'
		)));
	}

	public function addproductAction () {
		$sku = $this->getRequest()->getParam('sku');
		$qty = $this->getRequest()->getParam('qty');
		
		$col = Mage::getModel('catalog/product')->getCollection()->addAttributeToFilter('sku', $sku);
		
		if ($col->count() > 0) {
			$productId = $col->getFirstItem()->getId();
			$product = Mage::getModel('catalog/product')->load($productId);
			
			// /* @var $quote Mage_Sales_Model_Quote */
			// $quote = Mage::getSingleton('checkout/session')->getQuote();
			
			$session = Mage::getSingleton('customer/session');
			
			/* @var $checkoutSession Mage_Checkout_Model_Session */
			// $checkoutSession = Mage::getSingleton('checkout/session');
			
			// $quote = $checkoutSession->getQuote();
			
			/* @var $cart Mage_Checkout_Model_Cart */
			$cart = Mage::getSingleton('checkout/cart');
			$cart->init();
			
			// Add product
			$qtyObj = new Varien_Object(array(
					'qty' => $qty
			));
			
			$quoteItem = $cart->addProduct($product, $qtyObj);
			
			// if (is_string($r)) {
			// // Error message
			// $data = array(
			// 'status' => 'error',
			// 'error_msg' => $r
			// );
			// } else {
			
			// $quoteItem->save();
			$cart->save();
			
			$session->setCartWasUpdated(true);
			
			// save the cart
			
			$data = array(
					'status' => 'ok'
			);
			
			// }
		} else {
			$data = array(
					'status' => 'error',
					'error_msg' => $this->__('Product not found')
			);
			
			$this->getResponse()->setHeader('HTTP/1.1', '404 Product not found');
			// $this->getResponse()->setHeader('Status','404 File not found');
		}
		
		$this->_renderJson(Zend_Json::encode($data));
	}

	protected function _renderJson ($jsonString) {
		$response = $this->getResponse();
		$response->setHeader('Content-Type', 'application/json');
		$response->setBody($jsonString);
	}
	
}