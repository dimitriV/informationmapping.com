<?php

class Storefront_AngularCheckout_CheckoutController extends Mage_Core_Controller_Front_Action {

	public function indexAction () {
		// if (!Mage::helper('checkout')->canOnepageCheckout()) {
		// Mage::getSingleton('checkout/session')->addError($this->__('The onepage checkout is disabled.'));
		// $this->_redirect('checkout/cart');
		// return;
		// }
		
		// Check if cart is not empty
		$quote = $this->_getQuote();
		if (! $quote->hasItems() || $quote->getHasError()) {
			$this->_redirect('checkout/cart');
			return;
		}
		
		
		
		
		// Check if minimum amount is met
		if (! $quote->validateMinimumAmount()) {
			$error = Mage::getStoreConfig('sales/minimum_order/error_message') ? Mage::getStoreConfig('sales/minimum_order/error_message') : Mage::helper('checkout')->__('Subtotal must exceed minimum order amount');
			
			Mage::getSingleton('checkout/session')->addError($error);
			$this->_redirect('checkout/cart');
			return;
		}
		
		Mage::getSingleton('checkout/session')->setCartWasUpdated(false);
		Mage::getSingleton('customer/session')->setBeforeAuthUrl(Mage::getUrl('*/*/*', array(
				'_secure' => true
		)));
		
		// $this->getOnepage()->initCheckout();
		
		// Init quote
		$customerSession = Mage::getSingleton('customer/session');
		$customer = $customerSession->getCustomer();
		if (! $quote->getCustomerId() && $customer) {
			// Quote does not have a customer assigned yet
			$quote->assignCustomer($customer);
		}
		
		$this->loadLayout();
		$this->_initLayoutMessages('customer/session');
		$this->getLayout()->getBlock('head')->setTitle($this->__('Checkout'));
		$this->renderLayout();
	}

	public function jsonAction () {
		
		
		//die(print_r($_POST, true));
		
		$response = $this->getResponse();
		
		$isError = true;
		
		try {
			$this->_processPost();
			
			/* @var $jsonModel Storefront_AngularCheckout_Model_Json */
			$jsonModel = Mage::getSingleton('angularcheckout/json');
			
			$bundleSku = $this->getRequest()->getParam('bundle');
			if($bundleSku){
			
				$bundleProduct = Mage::getModel("catalog/product");
				$productId = $bundleProduct->getIdBySku( $bundleSku );
					
				$bundleProduct->load($productId);
					
				if($bundleProduct->getId()){
					$jsonModel->setBundleProduct($bundleProduct);
				}
			
			}
			
			
			$data = $jsonModel->getJsonData();
			
			
			$isError = false;
		} catch (Exception $e) {
			Mage::logException($e);
			
			$data = array(
					'error' => true,
					'message' => $e->getMessage()
			);
			if (Mage::getIsDeveloperMode()) {
				$data['stacktrace'] = $e->getTraceAsString();
			}
			
			$response->setHttpResponseCode(500);
		}
		
		$fullJson = Zend_Json::encode($data);
		$resultJson = $fullJson;
		
		// TODO we are still using the server DIFF technique. But, we should replace it with DIFF at client side. Currently not working due to JS error.
		$cacheJson = false;
		
		if ($cacheJson) {
			
			$post = $this->_getPostData();
			
			if (! $isError && isset($post['version']) && isset($data['version'])) {
				
				// Save version to cache
				
				$cacheLifetime = 5 * 60;
				
				$version = $data['version'];
				
				$cache = Mage::app()->getCache();
				$cache->save($fullJson, 'angularcheckout-' . $version, array(), $cacheLifetime);
				
				if (isset($post['version'])) {
					$previousVersion = $post['version'];
					$previousJson = $cache->load('angularcheckout-' . $previousVersion);
					
					if ($previousJson !== false) {
						
						$previousJson = Zend_Json::decode($previousJson);
						
						$diff = $this->_arrayRecursiveDiff($data, $previousJson);
						
						$resultJson = Zend_Json::encode($diff);
					}
				}
			}
		}
		
		$response->setHeader('Content-Type', 'application/json');
		$response->setBody($resultJson);
	}

	public function placeorderAction () {
		$response = $this->getResponse();
		
		$data = array();
		$errorMessages = array();
		
		$redirectUrl = false;
		
		try {
			$data['success'] = true;
			
			$quote = $this->_getQuote();
			
			// $requiredAgreements = Mage::helper('checkout')->getRequiredAgreementIds();
			// if ($requiredAgreements) {
			// $postedAgreements = array_keys($this->getRequest()->getPost('agreement', array()));
			// $diff = array_diff($requiredAgreements, $postedAgreements);
			// if ($diff) {
			// $data['success'] = false;
			// $data['error'] = true;
			// $data['error_messages'] = $this->__('Please agree to all the terms and conditions before placing the order.');
			
			// }
			// }
			
			/* @var $jsonModel Storefront_AngularCheckout_Model_Json */
			$jsonModel = Mage::getSingleton('angularcheckout/json');
			
			/* @var $customerSession Mage_Customer_Model_Session */
			$customerSession = Mage::getSingleton('customer/session');
			
			// Create account logic
			if ($jsonModel->isCreateAccount() && (! $customerSession->isLoggedIn()) && $jsonModel->isPasswordValid() && $jsonModel->isPassword2Valid()) {
				// We should create an account!
				// If processing fails in the next step, this won't matter because the user will be logged in by then.
				
				if (! $jsonModel->customerAlreadyExists()) {
					
					try {
						$jsonModel->createCustomer();
					} catch (Exception $e) {
						Mage::logException($e);
					}
				}
			}
			
			// Subscribe logic
			if ($jsonModel->getSubscribe()) {
				try {
					$email = $jsonModel->getCustomerEmail();
					
					/* @var $subscriber Mage_Newsletter_Model_Subscriber */
					$subscriber = Mage::getModel('newsletter/subscriber');
					
					$subscriber->subscribe($email);
					
				} catch (Exception $e) {
					Mage::logException($e);
				}
			}

			// Process order
			$quote->collectTotals();
			
			// $data = $this->getRequest()->getPost('payment', array());
			// if ($data) {
			// $data['checks'] = Mage_Payment_Model_Method_Abstract::CHECK_USE_CHECKOUT | Mage_Payment_Model_Method_Abstract::CHECK_USE_FOR_COUNTRY | Mage_Payment_Model_Method_Abstract::CHECK_USE_FOR_CURRENCY | Mage_Payment_Model_Method_Abstract::CHECK_ORDER_TOTAL_MIN_MAX | Mage_Payment_Model_Method_Abstract::CHECK_ZERO_TOTAL;
			// $this->_getOnepage()->getQuote()->getPayment()->importData($data);
			// }
			
			$this->_getOnepage()->saveOrder();
			
			$redirectUrl = $this->_getOnepage()->getCheckout()->getRedirectUrl();
			
			// TODO should we update the session here?
			
			// Save to session for the next page
			$session = Mage::getSingleton('checkout/type_onepage')->getCheckout();
			
			/* @var $order Mage_Sales_Model_Order */
			$order = Mage::registry('current_order');
			
			if(!$order){
				Mage::throwException('Don\'t know which order was placed during Angular Checkout');
			}
			
			$session->setLastSuccessQuoteId($order->getQuoteId());
			$session->setLastQuoteId($order->getQuoteId());
			$session->setLastOrderId($order->getId());
			$session->setLastRealOrderId($order->getIncrementId());
			
			$data['success'] = true;
			$data['error'] = false;
			
		} catch (Mage_Core_Exception $e) {
			Mage::logException($e);
			
			Mage::helper('checkout')->sendPaymentFailedEmail($this->_getOnepage()->getQuote(), $e->getMessage());
			
			$data['success'] = false;
			$data['error'] = true;
			$errorMessages[] = $e->getMessage();
			
			$gotoSection = $this->_getOnepage()->getCheckout()->getGotoSection();
			if ($gotoSection) {
				$data['goto_section'] = $gotoSection;
				$this->_getOnepage()->getCheckout()->setGotoSection(null);
			}
			$updateSection = $this->_getOnepage()->getCheckout()->getUpdateSection();
			if ($updateSection) {
				if (isset($this->_sectionUpdateFunctions[$updateSection])) {
					$updateSectionFunction = $this->_sectionUpdateFunctions[$updateSection];
					$data['update_section'] = array(
							'name' => $updateSection,
							'html' => $this->$updateSectionFunction()
					);
				}
				$this->_getOnepage()->getCheckout()->setUpdateSection(null);
			}
			
			if (Mage::getIsDeveloperMode()) {
				$data['stacktrace'] = $e->getTraceAsString();
			}
			
			$response->setHttpResponseCode(500);
		} catch (Exception $e) {
			Mage::logException($e);
			Mage::helper('checkout')->sendPaymentFailedEmail($this->_getOnepage()->getQuote(), $e->getMessage());
			$data['success'] = false;
			$data['error'] = true;
			$errorMessages[] = $this->__('There was an error processing your order. Please contact us or try again later.');
			
			if (Mage::getIsDeveloperMode()) {
				$data['stacktrace'] = $e->getTraceAsString();
			}
		}
		
		$this->_getOnepage()->getQuote()->save();
		
		if ($data['error'] || count($errorMessages) > 0) {
			$response->setHttpResponseCode(500);
			$data['error_messages'] = $errorMessages;
		}
		
		/* @var $checkoutSession Mage_Checkout_Model_Session */
		$checkoutSession = Mage::getSingleton('checkout/session');
		if ($checkoutSession->getLastOrderId()) {
			$orderId = $checkoutSession->getLastOrderId();
			/* @var $order Mage_Sales_Model_Order */
			$order = Mage::getModel('sales/order')->load($orderId);
			
			if ($order->getId()) {
				$orderIncrement = $order->getIncrementId();
				
				$data['order_id'] = $orderId;
				$data['order_increment'] = $orderIncrement;
				
				if ($redirectUrl) {
					$data['redirect'] = $redirectUrl;
				} else {
					
					if (Mage::helper('core')->isModuleEnabled('Storefront_BootstrapTheme')) {
						$data['redirect'] = Mage::helper('bootstraptheme')->getUrlToOrderConfirmationPage($order);
					} else {
						$data['redirect'] = Mage::getUrl('checkout/onepage/success');
						//Mage::throwException('Don\'t know where to redirect to after placing order. Feature not implemented.');
					}
				}
			} else {
				Mage::throwException('Order was placed, but session contains invalid data?!?');
			}
		}
		
		$json = Zend_Json::encode($data);
		
		$response->setHeader('Content-Type', 'application/json');
		$response->setBody($json);
	}

	protected function _arrayRecursiveDiff ($aArray1, $aArray2) {
		$aReturn = array();
		
		foreach ($aArray1 as $mKey => $mValue) {
			if (array_key_exists($mKey, $aArray2)) {
				if (is_array($mValue)) {
					$aRecursiveDiff = $this->_arrayRecursiveDiff($mValue, $aArray2[$mKey]);
					if (count($aRecursiveDiff)) {
						$aReturn[$mKey] = $aRecursiveDiff;
					}
				} else {
					if ($mValue !== $aArray2[$mKey]) {
						$aReturn[$mKey] = $mValue;
					}
				}
			} else {
				$aReturn[$mKey] = $mValue;
			}
		}
		return $aReturn;
	}

	protected function _getPostData () {
		if (count($_POST) == 0) {
			$post = Zend_Json::decode(file_get_contents('php://input'));
		} else {
			$post = $_POST;
		}
		return $post;
	}

	protected function _processPost ($post = null) {
		$this->_modelsToSave = array();
		
		if ($post === null) {
			$post = $this->_getPostData();
		}
		
		// Ignore this field
		unset($post['version']);
		
		if (count($post) > 0) {
			$updateSingle = (bool) (isset($post['update_single']) && $post['update_single']);
			unset($post['update_single']);
			
			/* @var $checkoutHelper Mage_Checkout_Helper_Data */
			$checkoutHelper = Mage::helper('checkout');
			
			/* @var $jsonModel Storefront_AngularCheckout_Model_Json */
			$jsonModel = Mage::getSingleton('angularcheckout/json');
			
			$quote = $checkoutHelper->getQuote();
			
			$billingAddressChanged = false;
			$shippingAddressChanged = false;
			
			/* @var $customerHelper Mage_Customer_Helper_Data */
			$customerHelper = Mage::helper('customer');
			$isLoggedIn = $customerHelper->isLoggedIn();
			
			$customer = $this->_getCustomer();
			
			if (isset($post['is_b2b']) && ! $post['is_b2b']) {
				// Empty company / vat_id if this is not a b2b order
				
				$post['billing_info.company'] = '';
				$post['billing_info.vat_id'] = '';
			}
			
			if (isset($post['shipping_info.ship_to_different_address'])) {
				// Changing ship to different address
				
				if ($post['shipping_info.ship_to_different_address']) {
					// Enable ship to different address
					
					$jsonModel->enableShipToDifferentAddress();
				} else {
					// Ship to billing address
					
					$jsonModel->disableShipToDifferentAddress();
				}
			}
			
			foreach ($post as $key => $value) {
				
				$value = trim($value);
				$keyParts = explode('.', $key);
				
				switch ($keyParts[0]) {
					
					case 'set_cart_qty_product_id':
						$productId = $value;
						$qty = $post['set_cart_qty'];
						
						$jsonModel->setProductCartQty($productId, $qty);
					
						break;
						
					case 'remove_from_cart_product_id':
						$productId = $value;
						
						$jsonModel->removeProductFromCart($productId);
						
						break;
					
					case 'item_id':
						if (isset($post['qty'])) {
							$itemId = $value;
							$qty = $post['qty'];
							
							$quoteItem = $quote->getItemById($itemId);
							$quoteItem->setQty($qty);
							
							$jsonModel->flagModelsToSave($quoteItem);
						}
						
						break;
					
					case 'email':
						$jsonModel->setEmail($value);
						
						break;
					
					case 'po_number':
						$quote->setPoNumber($value);
						$jsonModel->flagModelsToSave($quote);
						break;
					
					case 'is_b2b':
						$value = (int) $value;
						
						$quote->setIsB2b($value);
						$jsonModel->flagModelsToSave($quote);
						break;
					
					case 'subscribe':
						$value = (bool) $value;
						$jsonModel->setSubscribe($value);
						break;
					
					case 'create_account':
						$value = (int) $value;
						$jsonModel->setCreateAccount($value);
						break;
					
					case 'password':
						$jsonModel->setPassword($value);
						break;
					
					case 'password2':
						$jsonModel->setPassword2($value);
						break;
					
					case 'billing_info':
						
						$quoteAddress = $quote->getBillingAddress();
						
						$addressId = $quoteAddress->getId();
						$customerAddressId = $quoteAddress->getCustomerAddressId();
						
						$customerDefaultBillingAddress = null;
						
						if ($isLoggedIn) {
							if ($customerAddressId === null) {
								// Customer doesnt have a saved address yet
								
								/* @var $newAddress Mage_Customer_Model_Address */
								
								$newAddress = Mage::getModel('customer/address');
								$newAddress->setCustomer($quote->getCustomer());
								
								$newAddress->setIsBillingDefault(true);
								
								// This is a quote address field... not a customer address field
								// $newAddress->setSameAsBilling(1);
								
								$newAddress->save();
								
								$customerAddressId = $newAddress->getId();
								
								$quote->setCustomerAddressId($customerAddressId);
								
								$customerDefaultBillingAddress = $newAddress;
								
								// /* @var $newQuoteAddress Mage_Sales_Model_Quote_Address */
								// $newQuoteAddress = Mage::getModel('sales/quote_address');
								
								// $newQuoteAddress = new Mage_Sales_Model_Quote_Address();
								// $newQuoteAddress->setData($newAddress->getData());
								// //$newQuoteAddress->save();
								
								// $quote->setBillingAddress($newQuoteAddress);
								// $quote->setShippingAddress($newQuoteAddress);
							}
							
							// Do not set this to 1, or we will be creating extra addresses!!
							// $quoteAddress->setSaveInAddressBook(0);
							$customerDefaultBillingAddress = Mage::getModel('customer/address')->load($customerAddressId);
						}
						
						/* @var $addressForm Mage_Customer_Model_Form */
						// $addressForm = Mage::getModel('customer/form');
						// $addressForm->setFormCode('customer_address_edit')->setEntityType('customer_address')->setIsAjaxRequest(Mage::app()->getRequest()->isAjax());
						
						$billingAddressChanged = true;
						
						switch ($keyParts[1]) {
							case 'first_name':
								$quote->setCustomerFirstname($value);
								$jsonModel->flagModelsToSave($quote);
								
								$quoteAddress->setFirstname($value);
								$jsonModel->flagModelsToSave($quoteAddress);
								
								if ($customer) {
									$customer->setFirstname($value);
									$jsonModel->flagModelsToSave($customer);
								}
								
								if ($customerDefaultBillingAddress) {
									$customerDefaultBillingAddress->setFirstname($value);
									$jsonModel->flagModelsToSave($customerDefaultBillingAddress);
								}
								
								break;
							
							case 'last_name':
								$quote->setCustomerLastname($value);
								$jsonModel->flagModelsToSave($quote);
								
								$quoteAddress->setLastname($value);
								$jsonModel->flagModelsToSave($quoteAddress);
								
								if ($customer) {
									$customer->setLastname($value);
									$jsonModel->flagModelsToSave($customer);
								}
								
								if ($customerDefaultBillingAddress) {
									$customerDefaultBillingAddress->setLastname($value);
									$jsonModel->flagModelsToSave($customerDefaultBillingAddress);
								}
								
								break;
							
							case 'phone':
								$quoteAddress->setTelephone($value);
								
								$jsonModel->flagModelsToSave($quoteAddress);
								
								if ($customerDefaultBillingAddress) {
									$customerDefaultBillingAddress->setTelephone($value);
									$jsonModel->flagModelsToSave($customerDefaultBillingAddress);
								}
								
								break;
							
							case 'street':
								$jsonModel->setStreet($quoteAddress, 1, $value);
								
								if ($customerDefaultBillingAddress) {
									$jsonModel->setStreet($customerDefaultBillingAddress, 1, $value);
								}
								break;
							
							case 'housenumber':
								
								$jsonModel->setStreet($quoteAddress, 2, $value);
								if ($customerDefaultBillingAddress) {
									$jsonModel->setStreet($customerDefaultBillingAddress, 2, $value);
								}
								break;
							
							// TODO add support for postal box
							// case 'postalbox':
							// $quoteAddress->setStreet3($value);
							// $this->_flagModelsToSave($address);
							// break;
							
							case 'postalcode':
								$quoteAddress->setPostcode($value);
								$jsonModel->flagModelsToSave($quoteAddress);
								
								if ($customerDefaultBillingAddress) {
									$customerDefaultBillingAddress->setPostcode($value);
									$jsonModel->flagModelsToSave($customerDefaultBillingAddress);
								}
								
								break;
							
							case 'city':
								$quoteAddress->setCity($value);
								$jsonModel->flagModelsToSave($quoteAddress);
								
								if ($customerDefaultBillingAddress) {
									$customerDefaultBillingAddress->setCity($value);
									$jsonModel->flagModelsToSave($customerDefaultBillingAddress);
								}
								break;
							
							case 'country':
								$quoteAddress->setCountryId($value);
								$quoteAddress->setRegion('');
								$jsonModel->flagModelsToSave($quoteAddress);
								
								if ($customerDefaultBillingAddress) {
									$customerDefaultBillingAddress->setCountryId($value);
									$jsonModel->flagModelsToSave($customerDefaultBillingAddress);
								}
								break;
							
							case 'state':
								$quoteAddress->setRegion($value);
								$jsonModel->flagModelsToSave($quoteAddress);
								
								if ($customerDefaultBillingAddress) {
									$customerDefaultBillingAddress->setRegionId($value);
									$jsonModel->flagModelsToSave($customerDefaultBillingAddress);
								}
								break;
							
							case 'company':
								$quoteAddress->setCompany($value);
								$jsonModel->flagModelsToSave($quoteAddress);
								
								if ($customerDefaultBillingAddress) {
									$customerDefaultBillingAddress->setCompany($value);
									$jsonModel->flagModelsToSave($customerDefaultBillingAddress);
								}
								break;
							
							case 'vat_id':
								$country = $quoteAddress->getCountry();
								$value = $this->_sanitizeVatNumber($country, $value);
								
								$quoteAddress->setVatId($value);
								
								$jsonModel->flagModelsToSave($quoteAddress);
								
								if ($customerDefaultBillingAddress) {
									$customerDefaultBillingAddress->setVatId($value);
									$jsonModel->flagModelsToSave($customerDefaultBillingAddress);
								}
								
								break;
							
							default:
								Mage::throwException('Unknown key: ' . $key);
						}
						break;
					
					case 'shipping_info':
						
						$quoteShippingAddress = $quote->getShippingAddress();
// 						$customerShippingAddress = $customer->getDefaultShippingAddress();
						
						
						// We dont need this anymore because it was handled in:
						// enableShipToDifferentAddress() and disableShipToDifferentAddress()
						
						
// 						if ($isLoggedIn && ! $customerShippingAddress) {
// 							// Customer doesnt have a saved shipping address yet
							
// 							/* @var $quoteShippingAddress Mage_Customer_Model_Address */
							
// 							$customerShippingAddress = Mage::getModel('customer/address');
// 							$customerShippingAddress->setCustomer($quote->getCustomer());
// 							$customerShippingAddress->setSameAsBilling(1);
// 							$customerShippingAddress->save();
							
// 							$jsonModel->applyCustomerAddressToQuote($customerShippingAddress, false);
							
// 							$quoteShippingAddress = $quote->getShippingAddress();
// 						}
						
						$shippingAddressChanged = true;
						
						switch ($keyParts[1]) {
							
							case 'selected_address_id':
								$jsonModel->setShippingAddressId($value);
								break;
							
							case 'first_name':
								$jsonModel->setShippingFirstName($value);
								break;
							
							case 'last_name':
								$jsonModel->setShippingLastName($value);
								break;
								
							case 'company':
								$jsonModel->setShippingCompany($value);
								break;
							
							case 'street':
								$jsonModel->setStreet($quoteShippingAddress, 1, $value);
								break;
							
							case 'housenumber':
								$jsonModel->setStreet($quoteShippingAddress, 2, $value);
								break;
							
							// TODO add support for postal box
							// case 'postalbox':
							// $quoteAddress->setStreet3($value);
							// $this->_flagModelsToSave($address);
							// break;
							
							case 'postalcode':
								$quoteShippingAddress->setPostcode($value);
								$jsonModel->flagModelsToSave($quoteShippingAddress);
								break;
							
							case 'city':
								$quoteShippingAddress->setCity($value);
								$jsonModel->flagModelsToSave($quoteShippingAddress);
								break;
								
							case 'state':
								// TODO Not implemented state editing yet on shipping address
								Mage::throwException('Not implemented state editing yet on shipping address');
								break;
							
							case 'country':
								$quoteShippingAddress->setCountryId($value);
								
								$jsonModel->flagModelsToSave($quoteShippingAddress);
								break;
						}
						break;
					
					case 'shipping_method':
						$jsonModel->setShippingMethod($value);
						
						break;
					
					case 'payment_method':
						$jsonModel->setPaymentMethod($value);
						break;
				}
			}
			
			$billingAddress = $quote->getBillingAddress();
			
			// There is a chance the billing address is missing some fields
			if ($customer && ! $billingAddress->getFirstname()) {
				$billingAddress->setFirstname($customer->getFirstname());
			}
			if ($customer && ! $billingAddress->getLastname()) {
				$billingAddress->setLastname($customer->getLastname());
			}
			
			// If the billing address is saved and it is also used for shipping, copy the data
			if ($billingAddressChanged && $billingAddress->getSameAsBilling()) {
				
				// TODO is this really needed anymore??
				
				$shippingAddress = $quote->getShippingAddress();
				
				$billingData = $quote->getBillingAddress()->getData();
				unset($billingData['address_id']);
				unset($billingData['address_type']);
				$shippingAddress->setData($billingData);
				
				$jsonModel->flagModelsToSave($shippingAddress);
			}
			
			$jsonModel->saveModels();
			
			// $checkoutHelper->getQuote()->collectTotals();
			// $checkoutHelper->getQuote()->save();
			
			// $quote->getShippingAddress()->setCollectShippingRates(true);
		}
	}

	/**
	 *
	 * @return Mage_Sales_Model_Quote
	 */
	protected function _getQuote () {
		/* @var $checkoutHelper Mage_Checkout_Helper_Data */
		$checkoutHelper = Mage::helper('checkout');
		
		$quote = $checkoutHelper->getQuote();
		
		return $quote;
	}

	/**
	 * Check whether VAT ID validation is enabled
	 *
	 * @param Mage_Core_Model_Store|string|int $store        	
	 * @return bool
	 */
	protected function _isVatValidationEnabled ($store = null) {
		return Mage::helper('customer/address')->isVatValidationEnabled($store);
	}

	/**
	 * Get one page checkout model
	 *
	 * @return Mage_Checkout_Model_Type_Onepage
	 */
	protected function _getOnepage () {
		return Mage::getSingleton('checkout/type_onepage');
	}

	protected function _sanitizeVatNumber ($countryCode, $vatNumber) {
		$vatNumber = str_replace(' ', '', $vatNumber);
		$vatNumber = str_replace('.', '', $vatNumber);
		
		if (stripos($vatNumber, $countryCode) === 0) {
			$vatNumber = substr($vatNumber, 2);
		}
		
		if ($countryCode == 'BE' && strlen($vatNumber) == 9) {
			// Example: 837434751 should have a prefix "0"
			$vatNumber = '0' . $vatNumber;
		}
		
		return $vatNumber;
	}

	protected function _getCustomer () {
		/* @var $customer Mage_Customer_Model_Customer */
		$customer = $this->_getQuote()->getCustomer();
		if (! $customer->getId()) {
			$customer = null;
		}
		return $customer;
	}
}