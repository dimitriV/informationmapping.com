<?php
/* @var $installer Mage_Sales_Model_Resource_Setup */
$installer = $this;
$installer->startSetup();

$installer->addAttribute('order', 'po_number', array(
		'type' => 'varchar'
));
$installer->addAttribute('quote', 'po_number', array(
		'type' => 'varchar'
));


$installer->addAttribute('order', 'is_b2b', array(
		'type' => 'int'
));
$installer->addAttribute('quote', 'is_b2b', array(
		'type' => 'int'
));

$installer->endSetup();