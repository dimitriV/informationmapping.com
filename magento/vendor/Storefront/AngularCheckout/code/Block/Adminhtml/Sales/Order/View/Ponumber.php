<?php
class Storefront_AngularCheckout_Block_Adminhtml_Sales_Order_View_Ponumber extends Mage_Adminhtml_Block_Sales_Order_Abstract{
	
	protected $_template = 'angularcheckout/sales/order/view/purchaseorder.phtml';
	
	public function getPoNumber(){
		return $this->getOrder()->getPoNumber();
	}
	
}