<?php
class Storefront_AngularCheckout_Block_Checkout_Step extends Storefront_AngularCheckout_Block_Checkout_Abstract{
	
	public function getTermsAndConditionsUrl(){
		
		$termsUrl = trim(Mage::getStoreConfig('checkout/options/override_terms_url'));
		
		if(!$termsUrl){
		
			if(Mage::helper('core')->isModuleEnabled('Storefront_BootstrapTheme')){
				$termsUrl = Mage::helper('bootstraptheme')->getTermsAndConditionsUrl();
			}else{
				$termsUrl = null;
			}
		}
		
		return $termsUrl;
	}
	
	public function getInputHtml($id, $name, $title, $ngModel, $updateSingle = false, $useRemoteValidation = true, $type = 'text'){
		$fullNgModel = 'data.checkout.'.$ngModel;
		$helpBlockModel = 'data.checkout.form.help[\''.$ngModel.'\']';
		
		if($updateSingle){
			$updateSingleBoolean = 'true';
		}else{
			$updateSingleBoolean = 'false';
		}
		
		$remoteValidationString = '';
		if($useRemoteValidation){
			$remoteValidationString = ' remote-validation ';
		}
		
		if($this->getUseBootstrapIcons()){
			$validIconClass = array('glyphicon', 'glyphicon-ok');
			$invalidIconClass = array('glyphicon', 'glyphicon-remove');
			
		}else{
			$validIconClass = array('fa', 'fa-check-circle');
			$invalidIconClass = array('fa', 'fa-times-circle');
		}
		$loadingIconClass = $this->_getLoadingIconClasses();
		
		$html = '';
		$html .= '<input '.$remoteValidationString.' ng-disabled="isFieldBeingUpdated(\''.$ngModel.'\')" id="'.$id.'" type="'.$type.'" class="form-control" placeholder="'. $this->escapeHtml($this->__($title)) .'" name="'.$name.'" ng-model="'.$fullNgModel.'" ng-change="updateCheckout(\''.$ngModel.'\', '.$updateSingleBoolean.')" ng-model-options="{updateOn: \'blur\'}" />';
		$html .= '<i ng-hide="isFieldBeingUpdated(\''.$ngModel.'\')" class="'.implode(' ',$validIconClass).' valid form-control-feedback" aria-hidden="true"></i>';
		$html .= '<i ng-hide="isFieldBeingUpdated(\''.$ngModel.'\')" class="'.implode(' ',$invalidIconClass).' invalid form-control-feedback" aria-hidden="true"></i>';
		$html .= '<i ng-show="isFieldBeingUpdated(\''.$ngModel.'\')" class="'.implode(' ',$loadingIconClass).' loading form-control-feedback" aria-hidden="true"></i>';
  		$html .= '<span class="sr-only">('.$this->escapeHtml($this->__('Success')).')</span>';
  		
  		// Help block
  		$html .= '<p ng-show="'.$helpBlockModel.'" class="help-block">{{ '.$helpBlockModel.' }}</p>';
  		
		return $html;
	}
	
	
	
	public function getSelectHtml($id, $name, $title, $ngModel, $ngOptions, $useRemoteValidation = true){
		$fullNgModel = 'data.checkout.'.$ngModel;
	
// 		if($updateSingle){
// 			$updateSingleBoolean = 'true';
// 		}else{
			$updateSingleBoolean = 'false';
// 		}
	
		$remoteValidationString = '';
		if($useRemoteValidation){
			$remoteValidationString = ' remote-validation ';
		}
		
		$html = '';
// 		$html .= '<input ng-disabled="isFieldBeingUpdated(\''.$ngModel.'\')" id="'.$id.'" type="text" class="form-control" placeholder="'. $this->escapeHtml($this->__($title)) .'" name="'.$name.'" ng-model="'.$fullNgModel.'" ng-change="updateCheckout(\''.$ngModel.'\', '.$updateSingleBoolean.')" ng-model-options="{updateOn: \'blur\'}" />';
		$html .= '<select '.$remoteValidationString.' ng-options="'.$ngOptions.'" ng-disabled="isFieldBeingUpdated(\''.$ngModel.'\')" id="'.$id.'" name="'.$name.'" class="form-control" ng-model="'.$fullNgModel.'" ng-options="country.code as country.label for country in data.checkout.allowed_billing_countries" ng-change="updateCheckout(\''.$ngModel.'\', '.$updateSingleBoolean.')"></select>';
		return $html;
	}
	
	
}