<?php
abstract class Storefront_AngularCheckout_Block_Checkout_Abstract extends Mage_Core_Block_Template{
	
	public function getUseBootstrapIcons(){
		
		$checkout = $this->_getCheckoutBlock();
		
		if($checkout){
			return $checkout->getUseBootstrapIcons();
		}
		
		return false;
	}
	
	protected function _getCheckoutBlock(){
		$parent = $this->getParentBlock();
		
		while($parent && !$parent instanceof Storefront_AngularCheckout_Block_Checkout){
			$parent = $parent->getParentBlock();
		}
		
		if($parent instanceof Storefront_AngularCheckout_Block_Checkout){
			/* @var $parent Storefront_AngularCheckout_Block_Pluggablecheckout */
			return $parent;
		}
		
		return null;
	}
	
	public function isPluggableCheckout(){
		$checkout = $this->_getCheckoutBlock();
		
		if($checkout){
			return $checkout->getUseAsPluggable();
		}
		
		return false;
	}
	
	public function getLoadingIconHtml(){
		$r = '<i class="'.implode(' ', $this->_getLoadingIconClasses()).'"></i>';
		return $r;
	}
	
	protected function _getLoadingIconClasses(){
		if($this->getUseBootstrapIcons()){
			return array('glyphicon', 'glyphicon-repeat', 'spin');
		}else{
			// TODO spinning is not centered
			//return array('fa', 'fa-refresh', 'fa-spin');
			return array('');
		}
	}
	
	
}