<?php
class Storefront_AngularCheckout_Block_Payment_Info_Purchaseorder extends Mage_Payment_Block_Info_Purchaseorder{

	protected function _construct()
	{
		parent::_construct();
		$this->setTemplate('angularcheckout/payment/info/purchaseorder.phtml');
	}
	
	public function toPdf()
	{
		$this->setTemplate('angularcheckout/payment/info/pdf/purchaseorder.phtml');
		return $this->toHtml();
	}
	
}