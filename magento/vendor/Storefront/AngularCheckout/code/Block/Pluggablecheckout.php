<?php

class Storefront_AngularCheckout_Block_Pluggablecheckout extends Storefront_AngularCheckout_Block_Checkout {

	protected $_bundleProduct = null;	

	public function setBundleProduct(Mage_Catalog_Model_Product $product){
		$this->_bundleProduct = $product;
	}

	public function getSuggestedProducts(){
		if($this->_bundleProduct){
			
			$r = array();
			
			$bundle = $this->_bundleProduct;
			
			$x = $bundle->getTypeInstance(true)->getChildrenIds($bundle->getId(), false);
			
			return $r;
			
			
		}
		
		return false;
	}
	
}