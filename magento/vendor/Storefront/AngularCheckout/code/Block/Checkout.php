<?php

class Storefront_AngularCheckout_Block_Checkout extends Mage_Checkout_Block_Onepage_Abstract {

	protected $_template = 'angularcheckout/checkout.phtml';
	protected $_includeJsonInHtml = true;
	protected $_useBootstrapIcons = false;
	protected $_useAsPluggable = false;
	
	
	public function getJsonUrl(){
		/*
		if(Mage::helper('core')->isModuleEnabled('Yireo_MageBridge')){
			$baseUrl = Mage::getBaseUrl('skin');
			$baseUrl = substr($baseUrl, 0, -5);
			
			$r = $baseUrl . 'angularcheckout/checkout/json';
			
		}else{
			$r = Mage::getUrl('angularcheckout/checkout/json');
		}
		*/
		
		$r = Mage::getUrl('angularcheckout/checkout/json');
		
		return $r;
	}
	
	public function getPlaceOrderUrl(){
		
// 		if(Mage::helper('core')->isModuleEnabled('Yireo_MageBridge')){
// 			$baseUrl = Mage::getBaseUrl('skin');
// 			$baseUrl = substr($baseUrl, 0, -5);
				
// 			$r = $baseUrl . 'angularcheckout/checkout/placeorder';
				
// 		}else{
			$r = Mage::getUrl('angularcheckout/checkout/placeorder');
// 		}
		return $r;
	}
	
	public function setUseBootstrapIcons($flag){
		$this->_useBootstrapIcons = (bool) $flag;
	}
	
	public function getUseBootstrapIcons(){
		return $this->_useBootstrapIcons;
	}
	
	public function getUseAsPluggable(){
		return $this->_useAsPluggable;
	}
	
	public function getIncludeJsonInHtml(){
		return $this->_includeJsonInHtml;
	}
	
	public function setUseAsPluggable($value){
		$this->_useAsPluggable = (bool) $value;
	}
	
	public function disableJsonInHtml(){
		$this->_includeJsonInHtml = false;
	}
	
	public function getJsonString () {
		/* @var $helper Storefront_AngularCheckout_Helper_Data */
		$helper = Mage::getSingleton('angularcheckout/json');
		$data = $helper->getJsonData();
		
		return Zend_Json::encode($data);
	}
	
	public function getExtraSkinCss(){
		return array('css/angularcheckout.css');
	}
	
	public function getExtraSkinJs(){
		$r = array();
		
		$r[] = 'js/angular.js';
		$r[] = 'js/angular-sanitize.js';
		
		$r[] = 'js/angular-input-modified.min.js';
		$r[] = 'js/angularcheckout.js';
		
		return $r;
	}

	protected function _isBootstrapTheme(){
		return Mage::helper('core')->isModuleEnabled('Storefront_BootstrapTheme');
	}
	
	protected function _prepareLayout () {
		if($this->_isBootstrapTheme()){
			$helper = Mage::helper('bootstraptheme');
			
			$helper->enableAngular();
			
			// Bootstrap Theme has font awesome bundled!
			$this->setUseBootstrapIcons(false);
			
		}else{
			//$this->setUseBootstrapIcons(true);
		}
		
		$layout = $this->getLayout();
		
		/* @var $headBlock Mage_Page_Block_Html_Head */
		$headBlock = $layout->getBlock('head');
		
		if($headBlock){
			foreach($this->getExtraSkinJs() as $skinJs){
				$headBlock->addItem('skin_js', $skinJs);
			}
			foreach($this->getExtraSkinCss() as $skinCss){
				$headBlock->addItem('skin_css', $skinCss);
			}
		}
		
		return parent::_prepareLayout();
	}
	
	public function getStepHtml($step){
		/* @var $steps Mage_Core_Block_Text_List */
		$steps = $this->getChild('steps');
		
		return $steps->getChildHtml('checkout.steps.'.$step);
	}

	
}