<?php

class Storefront_AngularCheckout_Model_Json {
	
	// Caching vars
	protected $_statesOptions = null;

	protected $_cart = null;

	protected $_addressModels = array();
	
	// Functional vars
	protected $_modelsToSave = array();

	protected $_bundleProduct = null;

	public function getPickupShippingCodes () {
		return array(
				'simplepickup_pickup'
		);
	}

	public function autofillShippingMethod () {
		$quote = $this->_getQuote();
		$shippingAddress = $quote->getShippingAddress();
		
		if (! $shippingAddress->getShippingMethod()) {
			// Calculate shipping rates
			
			if (! $shippingAddress->getCountryId()) {
				// Country is needed to collect rates
				
				$billingCountryId = $quote->getBillingAddress()->getCountryId();
				if ($billingCountryId) {
					$shippingAddress->setCountryId($billingCountryId);
				}
			}
			
			$shippingAddress->setCollectShippingRates(1);
			$shippingAddress->collectShippingRates();
			$shippingAddress->save();
			
			$shippingRates = $this->_getFlatShippingRates();
			
			if (count($shippingRates) == 1) {
				// If only 1 option, select it!
				
				/* @var $shippingRate Mage_Sales_Model_Quote_Address_Rate */
				$shippingRate = reset($shippingRates);
				
				$this->setShippingMethod($shippingRate->getCode(), true);
			}
		}
	}

	/**
	 *
	 * @return Mage_Sales_Model_Quote
	 */
	protected function _getQuote () {
		/* @var $checkoutHelper Mage_Checkout_Helper_Data */
		$checkoutHelper = Mage::helper('checkout');
		
		$quote = $checkoutHelper->getQuote();
		
		return $quote;
	}

	protected function _autoChooseDefaultBillingAddress () {
		$customer = $this->_getCustomer();
		if ($customer) {
			$ba = $customer->getDefaultBillingAddress();
			
			if (! $ba) {
				
				$allAddresses = $customer->getAddressesCollection();
				$allAddresses->addOrder('created_at', 'asc');
				
				if ($allAddresses->count() > 0) {
					// Mark the oldest address as default for billing
					
					/* @var $firstAddress Mage_Customer_Model_Address */
					$firstAddress = $allAddresses->getFirstItem();
					
					$firstAddress->setIsDefaultBilling(true);
					
					// We dont need to create an extra address
					$firstAddress->setSaveInAddressBook(0);
					
					$firstAddress->save();
					
					// Reset already loaded addresses
					$customer->cleanAllAddresses();
					
					$quote = $this->_getQuote();
					$this->applyCustomerAddressToQuote($firstAddress, true);
				}
			}
		}
	}

	protected function _checkQuoteAddresses () {
		$quote = $this->_getQuote();
		
		$qba = $quote->getBillingAddress();
		$qsa = $quote->getShippingAddress();
		
		if ($this->_isLoggedIn() && $qba->getCustomerAddressId() === null && $qsa->getCustomerAddressId() === null) {
			// The quote is not complete, init it
			$customer = $this->_getCustomer();
			$quote->assignCustomerWithAddressChange($customer);
			$quote->save();
		}
		
		$h = '';
	}

	protected function _autofillPaymentMethod () {
		
		// TODO this throws an integrigty contraint
		
		/*
		 * "SQLSTATE[23000]: Integrity constraint violation: 1452 Cannot add or update a child row: a foreign key constraint fails (`grafimon`.`sales_flat_quote_payment`, CONSTRAINT `FK_SALES_FLAT_QUOTE_PAYMENT_QUOTE_ID_SALES_FLAT_QUOTE_ENTITY_ID` FOREIGN KEY (`quote_id`) REFERENCES `sales_flat_quote` (`entity_id`) ON DELE), query was: INSERT INTO `sales_flat_quote_payment` (`created_at`, `updated_at`, `method`, `additional_information`) VALUES ('2015-06-09 13:50:57', '2015-06-09 13:50:57', ?, ?)"
		 */
		
		// if (! $this->_getQuote()->getPayment()->getMethod()) {
		// $paymentOptions = $this->_getPaymentOptions();
		
		// if (count($paymentOptions) === 1) {
		// // Select the only option
		// $pm = $paymentOptions[0]['code'];
		// $this->setPaymentMethod($pm, true);
		// }
		// }
	}
	
	protected function _autoAddFreeProductFromProductSelector(){
		$products = $this->_getSuggestedProducts();
		
		if($products){
			
			foreach($products as $product){
				if($product->getFinalPrice() == 0){
				
					if(!$this->_isProductInCart($product->getId())){
						// add free product to cart
						
						$this->addProductToCart($product, 1);
						
					}
				}
			}
			
		}
	}

	public function getJsonData () {
		$this->_autoAddFreeProductFromProductSelector();
		$this->_autoChooseDefaultBillingAddress();
		$this->_checkQuoteAddresses();
		$this->autofillShippingMethod();
		$this->_autofillPaymentMethod();
		
		$session = Mage::getSingleton('checkout/session');
		$session->setCartWasUpdated(true);
		
		$dangerMessages = array();
		
		$data = array();
		
		/* @var $checkoutHelper Mage_Checkout_Helper_Data */
		$checkoutHelper = Mage::helper('checkout');
		
		/* @var $customerHelper Mage_Customer_Helper_Data */
		$customerHelper = Mage::helper('customer');
		
		$quote = $checkoutHelper->getQuote();
		
		$customer = $customerHelper->getCustomer();
		$customerEmail = $this->getCustomerEmail();
		
		/* @var $helper Storefront_AngularCheckout_Helper_Data */
		$helper = Mage::helper('angularcheckout');
		
		/* @var $customerSession Mage_Customer_Model_Session */
		$customerSession = Mage::getSingleton('customer/session');
		
		if ($helper->isModuleEnabled('Storefront_AngularCart')) {
			/* @var $cartJsonModel Storefront_AngularCart_Model_Json */
			$cartJsonModel = Mage::getSingleton('angularcart/json');
		} else {
			/* @var $cartJsonModel Storefront_AngularCheckout_Model_Cart_Json */
			$cartJsonModel = Mage::getSingleton('angularcheckout/cart_json');
		}
		$data['cart'] = $cartJsonModel->getCartData();
		
		$allowedBillingCountries = array();
		
		$allowedCountryCodes = $this->_getAllowedCountryCodes();
		
		foreach ($allowedCountryCodes as $allowedCountryCode) {
			$label = Mage::app()->getLocale()->getCountryTranslation($allowedCountryCode);
			$allowedBillingCountries[] = array(
					'code' => $allowedCountryCode,
					'label' => $label
			);
		}
		
		$billingAddress = $quote->getBillingAddress();
		$shippingAddress = $quote->getShippingAddress();
		
		// Setting defaults
		
		if (count($allowedCountryCodes) == 1) {
			// Only 1 country code allowed - set it automatically
			if ($billingAddress->getCountryId() !== $allowedCountryCodes[0]) {
				$billingAddress->setCountryId($allowedCountryCodes[0]);
				$billingAddress->save();
			}
		}
		
		$checkoutData = array();
		$checkoutData['is_logged_in'] = $customerHelper->isLoggedIn();
		$checkoutData['order_button_label'] = $this->_getOrderButtonLabel();
		$checkoutData['allow_guest_checkout'] = $this->_isAllowGuestCheckout();
		$checkoutData['allow_login'] = $this->_isAllowLogin();
		$checkoutData['can_pickup'] = $this->_canPickup();
		$checkoutData['can_home_delivery'] = $this->_canHomeDelivery();
		$checkoutData['can_create_account'] = $this->_canCreateAccount();
		$checkoutData['create_account'] = $this->isCreateAccount();
		$checkoutData['password'] = $this->_getPassword();
		$checkoutData['password2'] = $this->_getPassword2();
		
		// Billing address data
		$checkoutData['billing_info'] = $this->_collectAddressData($billingAddress, true);
		
		// Shipping address
		$selectedShippingAddressId = $this->_getSelectedShippingAddressId();
		$shippingInfo = array();
		
		$shippingInfo = $this->_collectAddressData($quote->getShippingAddress(), false);
		
		$shippingInfo['ship_to_different_address'] = $this->_isShipToDifferentAddress();
		$shippingInfo['selected_address_id'] = $selectedShippingAddressId;
		
		$allAddressCol = Mage::getResourceModel('customer/address_collection');
		$allAddressCol->setCustomerFilter($customer);
		$allAddressCol->addAttributeToSelect('*');
		
		$allShippingAddresses = array();
		
		foreach ($allAddressCol as $oneAddress) {
			
			$customerBillingAddressId = $billingAddress->getCustomerAddressId();
			
			// Must be a different address than billing, otherwise the checkbox doesnt make sense
			if ($oneAddress->getId() != $customerBillingAddressId && $this->_isAddressDifferent($oneAddress, $billingAddress)) {
				
				if ($this->_isAddressComplete($oneAddress)) {
					$allShippingAddresses[] = $this->_collectAddressData($oneAddress, false);
				}
			}
		}
		
		$shippingInfo['all_addresses'] = $allShippingAddresses;
		
		$checkoutData['shipping_info'] = $shippingInfo;
		
		// Shipping methods
		$shippingMethodInfo = array();
		$shippingOptions = array();
		
		$shippingAddress->setCollectShippingRates(1);
		$shippingAddress->collectShippingRates();
		$shippingAddress->save();
		
		$shippingRates = $this->_getFlatShippingRates();
		
		foreach ($shippingRates as $shippingRate) {
			/* @var $shippingRate Mage_Sales_Model_Quote_Address_Rate */
			// $rateLabel = $shippingRate->getCarrierTitle();
			
			$priceInclVat = $shippingRate->getPrice();
			$priceExclVat = $priceInclVat;
			
			$option = array();
			$option['rate_id'] = $shippingRate->getRateId();
			$option['carrier_title'] = $shippingRate->getCarrierTitle();
			$option['method_title'] = $shippingRate->getMethodTitle();
			$option['code'] = $shippingRate->getCode();
			$option['price_incl_vat'] = $priceInclVat;
			$option['price_incl_vat_formatted'] = $this->_formatMoney($priceInclVat);
			$option['price_excl_vat'] = $priceExclVat;
			$option['price_excl_vat_formatted'] = $this->_formatMoney($priceExclVat);
			
			$shippingOptions[] = $option;
		}
		
		$selectedShippingMethod = $shippingAddress->getShippingMethod();
		
		$shippingMethodInfo['options'] = $shippingOptions;
		$shippingMethodInfo['selected'] = $selectedShippingMethod;
		
		$checkoutData['shipping_method'] = $shippingMethodInfo;
		
		$paymentMethodInfo = array();
		$paymentOptions = array();
		
		$paymentHelper = Mage::helper('payment');
		$storeId = $quote->getStoreId();
		
		$paymentOptions = $this->_getPaymentOptions();
		$paymentMethodInfo['options'] = $paymentOptions;
		$paymentMethodInfo['selected'] = $quote->getPayment()->getMethod();
		
		$checkoutData['payment_method'] = $paymentMethodInfo;
		
		$checkoutData['email'] = $customerEmail;
		$checkoutData['po_number'] = $quote->getPoNumber();
		$checkoutData['show_po_number'] = Mage::getStoreConfigFlag('checkout/options/show_po_number');
		$checkoutData['is_b2b'] = $this->_isB2b();
		$checkoutData['can_edit_email'] = $this->_canEditEmail();
		$checkoutData['allowed_billing_countries'] = $allowedBillingCountries;
		$checkoutData['allow_other_shipping_address'] = $this->_canShipToOtherAddress();
		
		$checkoutData['can_subscribe'] = $this->_canSubscribe();
		$checkoutData['is_double_optin'] = $this->_isDoubleOptin();
		$checkoutData['subscribe'] = $this->getSubscribe();
		$checkoutData['can_place_order'] = $this->_canPlaceOrder();
		$checkoutData['is_first_order'] = $this->_isFirstOrder($customerSession);
		
		$checkoutData['messages'] = array();
		$checkoutData['messages']['danger'] = $dangerMessages;
		
		$isVatValid = $this->_isVatValid();
		
		// Validation states
		$validFieldsArray = array(
				'email' => $this->_isEmailValid(),
				'po_number' => $this->_isPoNumberValid(),
				'billing_info.vat_id' => $isVatValid,
				'billing_info.first_name' => $this->_isBillingFirstnameValid(),
				'billing_info.last_name' => $this->_isBillingLastnameValid(),
				'billing_info.phone' => $this->_isPhoneValid(),
				'billing_info.street' => $this->_isBillingStreetValid(),
				'billing_info.housenumber' => $this->_isBillingHouseNumberValid(),
				'billing_info.postalcode' => $this->_isBillingPostalcodeValid(),
				'billing_info.city' => $this->_isBillingCityValid(),
				'billing_info.company' => $this->_isBillingCompanyValid(),
				'billing_info.state' => $this->_isBillingStateValid(),
				'password' => $this->isPasswordValid(),
				'password2' => $this->isPassword2Valid(),
				'billing_info.country' => $this->_isBillingCountryValid(),
				'shipping_info.first_name' => $this->_isShippingFirstnameValid(),
				'shipping_info.last_name' => $this->_isShippingLastnameValid(),
				'shipping_info.company' => $this->_isShippingCompanyValid(),
				'shipping_info.street' => $this->_isShippingStreetValid(),
				'shipping_info.housenumber' => $this->_isShippingHousenumberValid(),
				'shipping_info.postalcode' => $this->_isShippingPostalcodeValid(),
				'shipping_info.city' => $this->_isShippingCityValid(),
				'shipping_info.country' => $this->_isShippingCountryValid(),
				'shipping_info.state' => $this->_isShippingStateValid()
		);
		
		// Validation advice
		$helpFieldsArray = array();
		if (! $isVatValid) {
			$helpFieldsArray['billing_info.vat_id'] = $helper->__('Please enter a valid VAT number');
		}
		
		$poNumber = $quote->getPoNumber();
		if (! $poNumber) {
			$helpFieldsArray['po_number'] = $helper->__('If your company uses PO numbers, you can enter it here.');
		}
		
		if ($this->_isShipToDifferentAddress() && ! $selectedShippingAddressId) {
			$helpFieldsArray['shipping_info.selected_address_id'] = $helper->__('Please choose a delivery address.');
		}
		
		$checkoutData['form']['is_valid'] = $validFieldsArray;
		$checkoutData['form']['help'] = $helpFieldsArray;
		
		$checkoutData['form']['states'] = $this->_getStatesOptions();
		$checkoutData['form']['use_2_address_lines'] = $this->_getUse2AddressLines();
		
		
		$agreements = array();
		
// 		$agreementIds = $checkoutHelper->getRequiredAgreementIds();
		
// 		if (count($agreementIds) > 0) {
// 			/* @var $agreementCol Mage_Checkout_Model_Resource_Agreement_Collection */
// 			$agreementCol = Mage::getResourceModel('checkout/agreement_collection');
// 			$agreementCol->addFieldToFilter('agreement_id', array(
// 					'in' => $agreementIds
// 			));
			
// 			foreach ($agreementCol as $agreement) {
// 				$content = $agreement->getContent();
// 				if (! $agreement->getIsHtml()) {
// 					$content = '<p>' . $content . '</p>';
// 				}
// 				$agreementData = array();
// 				$agreementData['id'] = $agreement->getId();
// 				$agreementData['checkbox_text'] = $agreement->getCheckboxText();
// 				$agreementData['content_html'] = $content;
				
// 				$agreements[] = $agreementData;
// 			}
// 		}
		
		
		$checkoutData['agreements'] = $agreements;
		
		$checkoutData['suggested_products'] = $this->_getSuggestedProductsData();
		
		$data['checkout'] = $checkoutData;
		
		if (Mage::helper('core')->isModuleEnabled('Storefront_BootstrapTheme')) {
			/* @var $bootstrapHelper Storefront_BootstrapTheme_Helper_Data */
			$bootstrapHelper = Mage::helper('bootstraptheme');
			$showPricesInclVat = $bootstrapHelper->showPricesInclVat();
		} else {
			$taxDisplay = (int) Mage::getStoreConfig('tax/display/type');
			switch ($taxDisplay) {
				case 1:
					$showPricesInclVat = false;
					break;
				default:
					$showPricesInclVat = true;
					break;
			}
		}
		$data['show_prices_incl_vat'] = $showPricesInclVat;
		
		// The version is random and used to compare between client and server states
		$version = sha1($quote->getId() . '-' . time() . '-' . mt_rand(0, 999999));
		
		$data['version'] = $version;
		
		return $data;
	}

	protected function _isAddressDifferent ($a, $b) {
		$dataSetA = array(
				$a->getFirstname(),
				$a->getLastname(),
				$a->getStreet(),
				$a->getCity(),
				$a->getPostcode(),
				$a->getCountryId()
		);
		$dataSetB = array(
				$b->getFirstname(),
				$b->getLastname(),
				$b->getStreet(),
				$b->getCity(),
				$b->getPostcode(),
				$b->getCountryId()
		);
		
		foreach ($dataSetA as $key => $valueA) {
			
			if (isset($dataSetB[$key])) {
				$valueB = $dataSetB[$key];
			} else {
				return true;
			}
			
			if (is_array($valueA)) {
				$valueA = implode("\n", $valueA);
			}
			
			if (is_array($valueB)) {
				$valueB = implode("\n", $valueB);
			}
			
			$valueA = '' . $valueA;
			$valueB = '' . $valueB;
			
			$valueA = trim($valueA);
			$valueB = trim($valueB);
			
			if ($valueA !== $valueB) {
				return true;
			}
		}
		
		return false;
	}

	protected function _getPaymentOptions () {
		$r = array();
		
		$quote = $this->_getQuote();
		$storeId = $quote->getStoreId();
		
		$paymentHelper = Mage::helper('payment');

        $partner ='';
        if(Mage::getSingleton('customer/session')->isLoggedIn()) {
            $customerData = Mage::getSingleton('customer/session')->getCustomer();
            $partner = $customerData->getId();
        }
		
		foreach ($paymentHelper->getStoreMethods($storeId, $quote) as $method) {
			/* @var $method Mage_Payment_Model_Method_Abstract */
			if ($this->_canUsePaymentMethod($method) && $method->isApplicableToQuote($quote, Mage_Payment_Model_Method_Abstract::CHECK_ZERO_TOTAL) || $partner=='8419'|| $partner=='12086') {
				$method->setInfoInstance($quote->getPayment());
				
				$paymentOptionInfo = array();
				
				$paymentOptionInfo['code'] = $method->getCode();
				$paymentOptionInfo['title'] = $method->getTitle();
				
				$r[] = $paymentOptionInfo;
			}
		}
		
		return $r;
	}

	protected function _isDoubleOptin () {
		// TODO add real look-up
		return true;
	}

	protected function _getSession () {
		/* @var $session Mage_Checkout_Model_Session */
		$session = Mage::getSingleton('checkout/session');
		
		return $session;
	}

	public function setPassword ($pass) {
		$this->_getSession()->setPassword($pass);
	}

	public function setPassword2 ($pass) {
		$this->_getSession()->setPassword2($pass);
	}

	public function getSubscribe () {
		return $this->_getSession()->getSubscribe();
	}

	public function setSubscribe ($flag) {
		$this->_getSession()->setSubscribe((bool) $flag);
	}

	protected function _getPassword () {
		return $this->_getSession()->getPassword();
	}

	protected function _getPassword2 () {
		return $this->_getSession()->getPassword2();
	}

	public function setCreateAccount ($flag) {
		$this->_getSession()->setCreateAccount((bool) $flag);
	}

	public function createCustomer () {
		$websiteId = Mage::app()->getWebsite()->getId();
		$store = Mage::app()->getStore();
		$quote = $this->_getQuote();
		
		$qba = $quote->getBillingAddress();
		$qsa = $quote->getShippingAddress();
		
		$email = $this->getCustomerEmail();
		
		// $email = 'wouter.samaey+random'.rand(0,999999).'@storefront.be';
		
		$password = $this->_getPassword();
		
		$customer = Mage::getModel("customer/customer");
		$customer->setWebsiteId($websiteId);
		$customer->setStore($store);
		$customer->setFirstname($qba->getFirstname());
		$customer->setLastname($qba->getLastname());
		$customer->setEmail($email);
		$customer->setPassword($password);
		
		$customer->save();
		
		// Create billing address for customer
		
		/* @var $ba Mage_Customer_Model_Address */
		$ba = Mage::getModel("customer/address");
		
		$ba->setCustomerId($customer->getId());
		$ba->setFirstname($customer->getFirstname());
		$ba->setLastname($customer->getLastname());
		$ba->setCountryId($qba->getCountryId());
		$ba->setRegionId($qba->getRegionId());
		$ba->setPostcode($qba->getPostcode());
		$ba->setCity($qba->getCity());
		$ba->setTelephone($qba->getTelephone());
		$ba->setCompany($qba->getCompany());
		$ba->setStreet($qba->getStreet());
        $ba->setVatId($qba->getVatId());
		$ba->setIsDefaultBilling('1');
		
		if (! $this->_isShipToDifferentAddress()) {
			// Use this address for both billing and shipping
			$ba->setIsDefaultShipping('1');
		}
		
		$ba->setSaveInAddressBook('1');
		
		$ba->save();
		
		if ($this->_isShipToDifferentAddress()) {
			// Create a separate shipping address
			/* @var $sa Mage_Customer_Model_Address */
			$sa = Mage::getModel("customer/address");
			
			$sa->setCustomerId($customer->getId());
			$sa->setFirstname($qsa->getFirstname());
			$sa->setLastname($qsa->getLastname());
			$sa->setCountryId($qsa->getCountryId());
			$sa->setRegionId($qsa->getRegionId());
			$sa->setPostcode($qsa->getPostcode());
			$sa->setCity($qsa->getCity());
			$sa->setTelephone($qsa->getTelephone());
			$sa->setCompany($qsa->getCompany());
			$sa->setStreet($qsa->getStreet());
			$sa->setIsDefaultBilling('0');
			$sa->setIsDefaultShipping('1');
			
			$sa->setSaveInAddressBook('1');
			
			$sa->save();
		}
		
		// Log in as this customer
		
		/* @var $session Mage_Customer_Model_Session */
		$session = Mage::getSingleton('customer/session');
		$session->setCustomerAsLoggedIn($customer);
		$session->login($email, $password);
		
		// Assign this customer to the quote
		$quote->setCustomer($customer);
		$quote->save();
	}

	public function isPasswordValid () {
		if ($this->isCreateAccount()) {
			$pass = trim($this->_getPassword());
			if ($pass) {
				return true;
			} else {
				return false;
			}
		} else {
			// Not creating an account
			return true;
		}
	}

	public function isPassword2Valid () {
		if ($this->isCreateAccount()) {
			$pass1 = trim($this->_getPassword());
			$pass2 = trim($this->_getPassword2());
			
			if ($pass1 && $pass2 && $pass1 === $pass2) {
				return true;
			} else {
				return false;
			}
		} else {
			// Not creating an account
			return true;
		}
	}

	protected function _getAllowedCountryCodes () {
		$r = explode(',', (string) Mage::getStoreConfig('general/country/allow'));
		return $r;
	}

	protected function _countryRequiresState ($countryCode) {
		$options = $this->_getStatesOptions();
		if (isset($options[$countryCode])) {
			return true;
		}
		
		return false;
	}

	protected function _getUse2AddressLines () {
		return Mage::getStoreConfigFlag('checkout/options/use_2_address_lines');
	}

	protected function _canCreateAccount () {
		if ($this->_isLoggedIn()) {
			return false;
		}
		
		if ($this->_isAllowLogin() && $this->_isGuest()) {
			return true;
		}
		return false;
	}

	public function isCreateAccount () {
		$session = $this->_getSession();
		$r = (bool) $session->getCreateAccount();
		
		return $r;
	}

	protected function _getStatesOptions () {
		if ($this->_statesOptions === null) {
			
			$r = array();
			
			/* @var $helper Mage_Directory_Helper_Data */
			$helper = Mage::helper('directory');
			
			$regionData = $helper->getRegionJson();
			$regionData = Zend_Json::decode($regionData);
			
			$allowedCountries = $this->_getAllowedCountryCodes();
			
			// Filter only usefull countries
			
			foreach ($allowedCountries as $countryCode) {
				if (isset($regionData[$countryCode])) {
					
					foreach ($regionData[$countryCode] as $regionItem) {
						$regionCode = $regionItem['code'];
						$regionName = $regionItem['name'];
						
						$r[$countryCode][] = array(
								'code' => $regionCode,
								'label' => $regionName
						);
					}
				}
			}
			
			$this->_statesOptions = $r;
		}
		
		return $this->_statesOptions;
	}

	protected function _collectAddressData ($address, $isBilling) {
		$r = array();
		
		/* @var $address Mage_Sales_Model_Quote_Address */
		
		$r['id'] = $address->getId();
		$r['customer_address_id'] = $address->getCustomerAddressId();
		$r['first_name'] = $address->getFirstname();
		$r['last_name'] = $address->getLastname();
		$r['phone'] = $address->getTelephone();
		$r['street'] = $address->getStreet1();
		$r['housenumber'] = $address->getStreet2();
		$r['postalbox'] = $address->getStreet3();
		$r['postalcode'] = $address->getPostcode();
		$r['city'] = $address->getCity();
		$r['state'] = $address->getRegionCode();
		$r['country'] = $address->getCountryId();
		$r['country_name'] = $address->getCountryModel()->getName();
		$r['company'] = $address->getCompany();
		
		if ($isBilling) {
			$r['vat_id'] = $address->getVatId();
			$r['request_invoice'] = (bool) $address->getCompany();
		}
		
		return $r;
	}

	protected function _isAddressComplete (Mage_Customer_Model_Address $address) {
		$requiredFields = array(
				'firstname',
				'lastname',
				'city',
				'postcode',
				'street',
				'country_id'
		);
		
		$data = $address->getData();
		
		foreach ($requiredFields as $requiredField) {
			if (! isset($data[$requiredField])) {
				return false;
			} else {
				
				$value = $data[$requiredField];
				$value = trim($value);
				
				if (! $value) {
					return false;
				}
			}
		}
		
		return true;
	}

	protected function _isShipToDifferentAddress () {
		$quote = $this->_getQuote();
		
		$cba = $quote->getBillingAddress()->getCustomerAddressId();
		$csa = $quote->getShippingAddress()->getCustomerAddressId();
		
		if ($cba && $csa) {
			if ($cba === $csa) {
				return false;
			} else {
				return true;
			}
		}
		
		// TODO this function may have room for improvement for guest checkout, but not sure if it is needed...
		
		$sa = $quote->getShippingAddress();
		$r = ! ((boolean) $sa->getSameAsBilling());
		
		return $r;
	}

	protected function _canPickup () {
		if ($this->_getQuote()->isVirtual()) {
			return false;
		}
		
		$flatShippingRates = $this->_getFlatShippingRates();
		
		$pickupCodes = $this->getPickupShippingCodes();
		
		foreach ($flatShippingRates as $rate) {
			$code = $rate->getCode();
			
			if (in_array($code, $pickupCodes)) {
				return true;
			}
		}
		
		return false;
	}

	protected function _canHomeDelivery () {
		if ($this->_getQuote()->isVirtual()) {
			return false;
		}
		
		return ! $this->_canPickup();
	}

	protected function _getFlatShippingRates () {
		$r = array();
		
		$quote = $this->_getQuote();
		$shippingAddress = $quote->getShippingAddress();
		$shippingRates = $shippingAddress->getGroupedAllShippingRates();
		
		foreach ($shippingRates as $shippingRateGroup) {
			foreach ($shippingRateGroup as $shippingRate) {
				$r[] = $shippingRate;
			}
		}
		
		return $r;
	}

	protected function _getPaymentMethod () {
		return $this->_getQuote()->getPayment()->getMethod();
	}

	protected function _getOrderButtonLabel () {
		$pm = $this->_getPaymentMethod();
		
		$helper = Mage::helper('angularcheckout');
		
		if (strpos($pm, 'ogone') !== false) {
			return $helper->__('Pay online');
		} else 
			if (strpos($pm, 'adyen') !== false) {
				return $helper->__('Pay online');
			}
		
		return $helper->__('Place order');
	}

	protected function _isAllowLogin () {
		$r = true;
		
		$helper = Mage::helper('core');
		if ($helper->isModuleEnabled('Storefront_GuestOnly')) {
			
			/* @var $guestOnlyHelper Storefront_GuestOnly_Helper_Data */
			$guestOnlyHelper = Mage::helper('guestonly');
			
			if ($guestOnlyHelper->isBlockAccounts()) {
				$r = false;
			}
		}
		
		return $r;
	}

	public function customerAlreadyExists () {
		$email = $this->getCustomerEmail();
		
		$customer = Mage::getModel('customer/customer');
		$customer->setWebsiteId(Mage::app()->getWebsite()->getId());
		
		$customer->loadByEmail($email);
		if ($customer->getId()) {
			return true;
		}
		return false;
	}

	protected function _isAllowGuestCheckout () {
		return Mage::getStoreConfigFlag('checkout/options/guest_checkout');
	}

	protected function _canEditEmail () {
		return ! Mage::getStoreConfigFlag('customer/create_account/confirm');
	}

	protected function _isVatValid () {
		$billingAddress = $this->_getQuote()->getBillingAddress();
		$company = $billingAddress->getCompany();
		$vatId = $billingAddress->getVatId();
		
		$isB2b = $this->_isB2b();
		
		if ($isB2b) {
			
			$countryCode = $billingAddress->getCountry();
			
			if ($countryCode) {
				
				if(Mage::helper('core')->isCountryInEU($countryCode)){
				
					$success = $this->_validateVatNumber($countryCode, $vatId);
					
					if ($success === true) {
						return true;
					} else {
						return false;
					}
				} else {
					// Country outside EU - no VAT number needed
					return true;
				}
			}else{
				// TODO what if country is empty?
				return true;
			}
			
			
		} else {
			// Must be empty
			if ($vatId) {
				return false;
			} else {
				return true;
			}
		}
	}

	protected function _isBillingFirstnameValid () {
		$firstname = $this->_getQuote()->getBillingAddress()->getFirstname();
		
		$r = false;
		
		$firstname = trim($firstname);
		if ($firstname) {
			$r = true;
		}
		
		return $r;
	}

	protected function _isBillingLastnameValid () {
		$lastname = $this->_getQuote()->getBillingAddress()->getLastname();
		
		$r = false;
		
		$lastname = trim($lastname);
		if ($lastname) {
			$r = true;
		}
		
		return $r;
	}

	protected function _isPhoneValid () {
		$r = false;
		
		$phone = $this->_getQuote()->getBillingAddress()->getTelephone();
		
		// TODO improve phone validation
		
		if (preg_match('#^[0-9\.\-\/\+ \(\)]+$#is', $phone)) {
			if (strlen($phone) > 7) {
				$r = true;
			}
		}
		
		return $r;
	}

	protected function _isBillingStreetValid () {
		if ($this->_getUse2AddressLines()) {
			$street = trim($this->_getQuote()->getBillingAddress()->getStreet1());
			if ($street) {
				return true;
			} else {
				return false;
			}
		} else {
			$street = $this->_getQuote()->getBillingAddress()->getStreet1();
			
			$r = Zend_Validate::is($street, 'Alpha', array(
					'allowWhiteSpace' => true
			));
			
			return $r;
		}
	}

	protected function _isBillingHouseNumberValid () {
		if ($this->_getUse2AddressLines()) {
			return true;
		} else {
			
			$houseNumber = $this->_getQuote()->getBillingAddress()->getStreet2();
			
			$r = Zend_Validate::is($houseNumber, 'Alnum');
			
			return $r;
		}
	}

	protected function _isBillingStateValid () {
		$r = false;
		
		$ba = $this->_getQuote()->getBillingAddress();
		$countryCode = $ba->getCountryId();
		
		if ($this->_countryRequiresState($countryCode)) {
			
			$state = trim($ba->getRegionCode());
			
			if ($state) {
				return true;
			} else {
				return false;
			}
		} else {
			return true;
		}
	}

	protected function _isBillingPostalcodeValid () {
		$r = false;
		
		$address = $this->_getQuote()->getBillingAddress();
		
		$postcode = $address->getPostcode();
		$countryCode = $address->getCountryId();
		if ($countryCode == 'BE') {
			if (preg_match('#^[0-9]{4}$#is', $postcode)) {
				$r = true;
			}
		} else {
			$r = Zend_Validate::is($postcode, 'Alnum');
		}
		
		return $r;
	}

	protected function _isBillingCityValid () {
		$r = false;
		
		$city = $this->_getQuote()->getBillingAddress()->getCity();
		
		$city = trim($city);
		if ($city) {
			$r = true;
		}
		
		return $r;
	}

	protected function _isB2b () {
		$quote = $this->_getQuote();
		
		$value = $quote->getIsB2b();
		
		if ($value === null) {
			$company = $quote->getBillingAddress()->getCompany();
			$vatId = $quote->getBillingAddress()->getVatId();
			if ($company || $vatId) {
				$newValue = true;
			} else {
				$newValue = false;
			}
			
			$quote->setIsB2b((int) $newValue);
			
			$this->flagModelsToSave($quote);
		}
		
		$r = (bool) $quote->getIsB2b();
		
		return $r;
	}

	protected function _isBillingCompanyValid () {
		if ($this->_isB2b()) {
			$company = $this->_getQuote()->getBillingAddress()->getCompany();
			$company = trim($company);
			if ($company) {
				return true;
			} else {
				return false;
			}
		} else {}
		
		return true;
	}

	protected function _isShippingCompanyValid () {
		if ($this->_getQuote()->isVirtual()) {
			return true;
		}
		
		if ($this->_isB2b()) {
			$company = $this->_getQuote()->getShippingAddress()->getCompany();
			$company = trim($company);
			if ($company) {
				return true;
			} else {
				return false;
			}
		} else {}
		
		return true;
	}

	protected function _isBillingCountryValid () {
		return (bool) trim($this->_getQuote()->getBillingAddress()->getCountryId());
	}

	protected function _isShippingFirstnameValid () {
		if ($this->_getQuote()->isVirtual()) {
			return true;
		}
		
		$firstname = $this->_getQuote()->getShippingAddress()->getFirstname();
		
		$r = false;
		
		$firstname = trim($firstname);
		if ($firstname) {
			$r = true;
		}
		
		return $r;
	}

	protected function _isShippingLastnameValid () {
		if ($this->_getQuote()->isVirtual()) {
			return true;
		}
		
		$lastname = $this->_getQuote()->getShippingAddress()->getLastname();
		
		$r = false;
		
		$lastname = trim($lastname);
		if ($lastname) {
			$r = true;
		}
		
		return $r;
	}

	protected function _isShippingStreetValid () {
		if ($this->_getQuote()->isVirtual()) {
			return true;
		}
		
		if ($this->_getUse2AddressLines()) {
			$street = trim($this->_getQuote()->getShippingAddress()->getStreet1());
			if ($street) {
				return true;
			} else {
				return false;
			}
		} else {
			$street = $this->_getQuote()->getShippingAddress()->getStreet1();
			
			$r = Zend_Validate::is($street, 'Alpha', array(
					'allowWhiteSpace' => true
			));
			
			return $r;
		}
	}

	protected function _isShippingHousenumberValid () {
		if ($this->_getQuote()->isVirtual()) {
			return true;
		}
		
		if ($this->_getUse2AddressLines()) {
			return true;
		} else {
			
			$houseNumber = $this->_getQuote()->getShippingAddress()->getStreet2();
			
			$r = Zend_Validate::is($houseNumber, 'Alnum');
			
			return $r;
		}
	}

	protected function _isShippingPostalcodeValid () {
		if ($this->_getQuote()->isVirtual()) {
			return true;
		}
		
		$r = false;
		
		$address = $this->_getQuote()->getShippingAddress();
		
		$postcode = $address->getPostcode();
		$countryCode = $address->getCountryId();
		if ($countryCode == 'BE') {
			if (preg_match('#^[0-9]{4}$#is', $postcode)) {
				$r = true;
			}
		} else {
			$r = Zend_Validate::is($postcode, 'Alnum');
		}
		
		return $r;
	}

	protected function _isShippingCityValid () {
		if ($this->_getQuote()->isVirtual()) {
			return true;
		}
		
		$r = false;
		
		$city = $this->_getQuote()->getShippingAddress()->getCity();
		
		$city = trim($city);
		if ($city) {
			$r = true;
		}
		
		return $r;
	}

	protected function _isShippingCountryValid () {
		if ($this->_getQuote()->isVirtual()) {
			return true;
		}
		
		return (bool) trim($this->_getQuote()->getShippingAddress()->getCountryId());
	}

	protected function _isShippingStateValid () {
		if ($this->_getQuote()->isVirtual()) {
			return true;
		}
		
		$r = false;
		
		$sa = $this->_getQuote()->getShippingAddress();
		$countryCode = $sa->getCountryId();
		
		if ($this->_countryRequiresState($countryCode)) {
			
			$state = trim($sa->getRegionCode());
			
			if ($state) {
				return true;
			} else {
				return false;
			}
		} else {
			return true;
		}
	}

	protected function _validateVatNumber ($countryCode, $vatNumber) {
		
		// TODO The VIES webservice does return company and address information. We could use this to prefill...
		
		// TODO if selling to an exotic country is enabled + auto-grouping, the checkout will be extremely slow due to too much VAT checking
		
		/* @var $helper Mage_Customer_Helper_Data */
		$helper = Mage::helper('customer');
		$result = $helper->checkVatNumber($countryCode, $vatNumber);
		
		// The service may be down OR the user may have an exotic country outside EU... Not doing anything with this yet...
		$isWebserviceOffline = ! $result->getRequestSuccess();
		
		if ($result->getIsValid()) {
			return true;
		} else {
			return false;
		}
	}

	public function setStreet (Mage_Customer_Model_Address_Abstract $address, $lineNo, $value) {
		$lines = $address->getStreet();
		
		if (! is_array($lines)) {
			$lines = explode("\n", $lines);
		}
		$lines[$lineNo - 1] = $value;
		
		$address->setStreet($lines);
		
		$this->flagModelsToSave($address);
	}

	public function setShippingMethod ($value, $forceSaveImmediately = false) {
		$quote = $this->_getQuote();
		$shippingAddress = $quote->getShippingAddress();
		$rate = $shippingAddress->getShippingRateByCode($value);
		if (! $rate) {
			Mage::throwException(Mage::helper('checkout')->__('Invalid shipping method.'));
		}
		$shippingAddress->setShippingMethod($value);
		
		Mage::dispatchEvent('checkout_controller_onepage_save_shipping_method', array(
				'request' => Mage::app()->getRequest(),
				'quote' => $quote
		));
		
		$quote->collectTotals();
		
		if ($forceSaveImmediately) {
			$shippingAddress->save();
		} else {
			$this->flagModelsToSave($shippingAddress);
		}
	}

	protected function _canUsePaymentMethod ($method) {
		return $method->isApplicableToQuote($this->_getQuote(), Mage_Payment_Model_Method_Abstract::CHECK_USE_FOR_COUNTRY | Mage_Payment_Model_Method_Abstract::CHECK_USE_FOR_CURRENCY | Mage_Payment_Model_Method_Abstract::CHECK_ORDER_TOTAL_MIN_MAX);
	}

	protected function _formatMoney ($amount, $allowFreeText = true) {
		$helper = Mage::helper('angularcheckout');
		
		if ($amount == 0) {
			if ($allowFreeText) {
				return $helper->__('Free');
			}
		}
		return Mage::app()->getStore()->formatPrice($amount, false);
	}

	protected function _canPlaceOrder () {
		$quote = $this->_getQuote();
		$quoteService = Mage::getModel('sales/service_quote', $quote);
		
		$canPlaceOrder = true;
		try {
			$quoteService->validate();
		} catch (Exception $e) {
			$canPlaceOrder = false;
		}
		
		return $canPlaceOrder;
	}

	protected function _canShipToOtherAddress () {
		return ! $this->_getQuote()->isVirtual();
	}

	protected function _isLoggedIn () {
		$customerHelper = Mage::helper('customer');
		return $customerHelper->isLoggedIn();
	}

	protected function _isGuest () {
		$customerHelper = Mage::helper('customer');
		return ! $customerHelper->isLoggedIn();
	}

	protected function _canSubscribe () {
		$showCheckbox = Mage::getStoreConfigFlag('checkout/options/allow_guest_subscribe');
		
		if ($showCheckbox) {
			
			if (Mage::helper('core')->isModuleEnabled('Storefront_SubscribeFirstOrder')) {
				/* @var $subscribeFirstOrderHelper Storefront_SubscribeFirstOrder_Helper_Data */
				$subscribeFirstOrderHelper = Mage::helper('subscribefirstorder');
				
				if ($subscribeFirstOrderHelper->isEnabled()) {
					// No need to subscribe. Customers are auto-subscribed!
					return false;
				}
			}
			
			if ($this->_isGuest()) {
				$guestCanSubscribe = Mage::getStoreConfigFlag('newsletter/subscription/allow_guest_subscribe');
				
				if (! $guestCanSubscribe) {
					return false;
				}
			}
			
			$isSubscribed = false;
			
			$customerEmail = $this->_getQuote()->getCustomerEmail();
			
			if ($customerEmail && Zend_Validate::is($customerEmail, 'EmailAddress')) {
				$subscriber = Mage::getModel('newsletter/subscriber')->loadByEmail($customerEmail);
				if ($subscriber->getId()) {
					$isSubscribed = true;
				}
			}
			
			return ! $isSubscribed;
		} else {
			return false;
		}
	}

	protected function _isFirstOrder (Mage_Customer_Model_Session $session) {
		// TODO cache results for a few minutes?
		if ($session->isLoggedIn()) {
			$customer = $session->getCustomer();
			
			$storeId = Mage::app()->getStore()->getId();
			
			/* @var $orders Mage_Sales_Model_Resource_Order_Collection */
			$orders = Mage::getResourceModel('sales/order_collection');
			
			$orders->addFieldToFilter('store_id', $storeId);
			$orders->addFieldToFilter('customer_id', $customer->getId());
			$orders->addFieldToFilter('state', array(
					'neq' => 'new'
			));
			
			$c = $orders->count();
			if ($c > 0) {
				return false;
			}
		}
		return true;
	}

	public function setEmail ($value) {
		
		/* @var $customerHelper Mage_Customer_Helper_Data */
		$customerHelper = Mage::helper('customer');
		
		if ($customerHelper->isLoggedIn()) {
			$customer = $customerHelper->getCurrentCustomer();
			$customer->setEmail($value);
			
			$this->flagModelsToSave($customer);
		}
		
		$quote = $this->_getQuote();
		$quote->setCustomerEmail($value);
		
		$ba = $quote->getBillingAddress();
		$ba->setEmail($value);
		
		$this->flagModelsToSave($quote);
		$this->flagModelsToSave($ba);
	}

	public function setPaymentMethod ($pm, $saveImmediately = false) {
		$quote = $this->_getQuote();
		$payment = $quote->getPayment();
		$payment->setMethod($pm);
		
		// if ($quote->isVirtual()) {
		// $address = $quote->getBillingAddress();
		// } else {
		// $address = $quote->getShippingAddress();
		// }
		
		// $address->setPaymentMethod($pm);
		
		if (! $quote->isVirtual() && $quote->getShippingAddress()) {
			$quote->getShippingAddress()->setCollectShippingRates(true);
		}
		
		// Save
		if (! $saveImmediately) {
			$this->flagModelsToSave($quote->getBillingAddress());
			$this->flagModelsToSave($quote->getShippingAddress());
			$this->flagModelsToSave($payment);
		} else {
			$quote->getBillingAddress()->save();
			$quote->getShippingAddress()->save();
			$payment->save();
		}
	}

	public function flagModelsToSave (Mage_Core_Model_Abstract $model) {
		$found = false;
		
		foreach ($this->_modelsToSave as $m) {
			if (get_class($m) === get_class($model) && $m->getId() === $model->getId()) {
				// Already marked for save
				$found = true;
				break;
			}
		}
		
		if (! $found) {
			$this->_modelsToSave[] = $model;
		}
	}

	public function saveModels () {
		foreach ($this->_modelsToSave as $key => $model) {
			$model->save();
			unset($this->_modelsToSave[$key]);
		}
	}

	protected function _isPoNumberValid () {
		return true;
	}

	protected function _isEmailValid () {
		$email = $this->getCustomerEmail();
		
		return Zend_Validate::is($email, 'EmailAddress');
	}

	public function getCustomerEmail () {
		return $this->_getQuote()->getCustomerEmail();
	}

	public function enableShipToDifferentAddress () {
		$quote = $this->_getQuote();
		
		if ($this->_isLoggedIn()) {
			// Logged in
			
			$customer = $this->_getCustomer();
			
			$billingAddressId = $customer->getDefaultBillingAddress()->getId();
			$newShippingAddress = $customer->getDefaultShippingAddress();
			
			if ($newShippingAddress && $newShippingAddress->getId() && $newShippingAddress->getId() !== $billingAddressId) {
				// Customer has a default shipping address that is different from the billing address
				// We're good!
			} else {
				
				$makeDefaultForShipping = false;
				
				if (! $newShippingAddress) {
					// Customer does not have a default for shipping yet, so we will make a default now
					$makeDefaultForShipping = true;
				}
				
				$addressesAvailableForShipping = $this->_getOtherAddressesAvailableForShipping();
				
				if (count($addressesAvailableForShipping) == 0) {
					// Customer does not have an address for shipping => create a new address
					
					$newShippingAddress = Mage::getModel('customer/address');
					$newShippingAddress->setCustomer($quote->getCustomer());
					
					if ($quote->getBillingAddress()->getCountryId()) {
						// Most likely the new shipping address will be in the same country
						$newShippingAddress->setCountryId($quote->getBillingAddress()->getCountryId());
					}
					
					if ($makeDefaultForShipping) {
						$newShippingAddress->setIsDefaultShipping(true);
					}
					
					// Save the new address
					$newShippingAddress->save();
				} else {
					// Select the first of the options - the customer can pick another one if they like
					$newShippingAddress = $addressesAvailableForShipping[0];
					
					if ($makeDefaultForShipping) {
						$newShippingAddress->setIsDefaultShipping(true);
						$newShippingAddress->save();
					}
				}
				
				// Reset address cache after setting a new default address
				if ($makeDefaultForShipping) {
					$customer->cleanAllAddresses();
				}
			}
			
			$this->applyCustomerAddressToQuote($newShippingAddress, false);
			
			$ba = $quote->getBillingAddress();
			$sa = $quote->getShippingAddress();
			
			$ba->setSameAsBilling(0);
			$sa->setSameAsBilling(0);
			
			// TODO check if we need this...
			$sa->setSaveInAddressBook(0);
			
			$this->flagModelsToSave($quote);
			$this->flagModelsToSave($ba);
			$this->flagModelsToSave($sa);
		} else {
			
			// TODO support guest checkout with delivery to different address
			Mage::throwException('Not implemented guest checkout with delivery to different address');
		}
	}

	/**
	 * Get an array of addresses that can be used for delivery to other address
	 */
	protected function _getOtherAddressesAvailableForShipping () {
		$r = array();
		
		$customer = $this->_getCustomer();
		if ($customer) {
			
			$billingAddress = $customer->getDefaultBillingAddress();
			
			$col = $customer->getAddressCollection();
			$col->setCustomerFilter($customer);
			$col->addAttributeToSelect('*');
			$col->setOrder('updated_at', 'DESC');
			
			foreach ($col as $address) {
				/* @var $address Mage_Customer_Model_Address */
				if ($address->getId() != $billingAddress->getId()) {
					$r[] = $address;
				}
			}
		}
		
		return $r;
	}

	public function disableShipToDifferentAddress () {
		$quote = $this->_getQuote();
		
		// $quote->setShippingAddress($quote->getBillingAddress());
		
		$ba = $quote->getBillingAddress();
		$sa = $quote->getShippingAddress();
		
		// Copy billing data to shipping
		$baData = $ba->getData();
		
		$keysNotToCopy = array(
				'address_id',
				'created_at',
				'updated_at',
				'address_type'
		);
		foreach ($keysNotToCopy as $key) {
			unset($baData[$key]);
		}
		
		$sa->addData($baData);
		
		// Always 0 for billing address...
		$ba->setSameAsBilling(0);
		
		// Shipping address is what counts!
		$sa->setSameAsBilling(1);
		
		// TODO check if we need this...
		$sa->setSaveInAddressBook(0);
		
		$this->flagModelsToSave($quote);
		$this->flagModelsToSave($ba);
		$this->flagModelsToSave($sa);
	}

	protected function _getCustomer () {
		/* @var $customer Mage_Customer_Model_Customer */
		$customer = $this->_getQuote()->getCustomer();
		if (! $customer->getId()) {
			$customer = null;
		}
		return $customer;
	}

	protected function _getSelectedShippingAddressId () {
		$r = null;
		if ($this->_isShipToDifferentAddress()) {
			$r = $this->_getQuote()->getShippingAddress()->getCustomerAddressId();
		}
		return $r;
	}

	public function applyCustomerAddressToQuote (Mage_Customer_Model_Address $address, $isBilling) {
		$quote = $this->_getQuote();
		
		if ($isBilling) {
			$qa = $quote->getBillingAddress();
		} else {
			$qa = $quote->getShippingAddress();
		}
		
		$originalId = $qa->getId();
		
		$data = $address->getData();
		unset($data['address_id']);
		unset($data['address_type']);
		unset($data['entity_id']);
		unset($data['entity_type_id']);
		unset($data['increment_id']);
		unset($data['attribute_set_id']);
		unset($data['created_at']);
		unset($data['updated_at']);
		unset($data['is_active']);
		
		// $data['address_id'] = $originalId;
		$data['customer_address_id'] = $address->getId();
		
		// TODO for some reason, emptying this won't persist on the second request
		
		// Empty original address before adding new data
		// $qa->unsetData('firstname');
		// $qa->unsetData('lastname');
		// $qa->unsetData('telephone');
		// $qa->unsetData('email');
		// $qa->unsetData('company');
		// $qa->unsetData('vat_id');
		// $qa->unsetData('street');
		// $qa->unsetData('postcode');
		// $qa->unsetData('city');
		// $qa->unsetData('region_id');
		// $qa->unsetData('country_id');
		
		$qa->addData($data);
		
		// $qa->save();
		
		$this->flagModelsToSave($qa);
	}

	/**
	 * Assigns a customer address to the quote for shipping, based on customer address ID
	 *
	 * @param int $value        	
	 */
	public function setShippingAddressId ($customerAddressId) {
		$quote = $this->_getQuote();
		
		$customer = $this->_getCustomer();
		
		$col = $customer->getAddressCollection();
		$col->setCustomerFilter($customer);
		$col->addAttributeToSelect('*');
		$col->addAttributeToFilter('entity_id', $customerAddressId);
		
		if ($col->count() == 1) {
			/* @var $custShippingAddress Mage_Customer_Model_Address */
			$custShippingAddress = $col->getFirstItem();
			
			$quote->getShippingAddress()->importCustomerAddress($custShippingAddress);
			
			// $session = Mage::getSingleton('checkout/session');
			// $session->setCartWasUpdated(true);
			
			// $quote->collectTotals();
		} else {
			Mage::throwException('Address ID "' . $customerAddressId . '" does not belong to the current customer');
		}
	}

	protected function _getCustomerAddresById ($id) {
		if (! array_key_exists($id, $this->_addressModels)) {
			
			$customerAddress = Mage::getModel('customer/address')->load($id);
			
			if ($customerAddress->getId()) {
				$this->_addressModels[$id] = $customerAddress;
			} else {
				$this->_addressModels[$id] = null;
			}
		}
		
		return $this->_addressModels[$id];
	}

	public function setShippingFirstName ($value) {
		// Change the name in th quote address
		$sa = $this->_getQuote()->getShippingAddress();
		$sa->setFirstname($value);
		
		$addressId = $sa->getCustomerAddressId();
		
		$customerAddress = $this->_getCustomerAddresById($addressId);
		if ($customerAddress) {
			$customerAddress->setFirstname($value);
		}
		
		// if the shipping address is changed and diverges from the billing address, it becomes an address on it's own
		// $sa->setCustomerAddressId(null);
		
		$this->flagModelsToSave($sa);
		$this->flagModelsToSave($customerAddress);
	}

	public function setShippingLastName ($value) {
		// Change the name in th quote address
		$sa = $this->_getQuote()->getShippingAddress();
		$sa->setLastname($value);
		
		$addressId = $sa->getCustomerAddressId();
		$customerAddress = $this->_getCustomerAddresById($addressId);
		if ($customerAddress) {
			$customerAddress->setLastname($value);
		}
		
		$this->flagModelsToSave($sa);
		$this->flagModelsToSave($customerAddress);
	}

	public function setShippingCompany ($value) {
		$sa = $this->_getQuote()->getShippingAddress();
		
		$sa->setCompany($value);
		
		$addressId = $sa->getCustomerAddressId();
		$customerAddress = $this->_getCustomerAddresById($addressId);
		if ($customerAddress) {
			$customerAddress->setCompany($value);
		}
		
		$this->flagModelsToSave($sa);
		$this->flagModelsToSave($customerAddress);
	}

	public function setBundleProduct (Mage_Catalog_Model_Product $bundleProduct) {
		$this->_bundleProduct = $bundleProduct;
	}

	
	/**
	 * Long way to get cart items, preventing item cache
	 *
	 * @return Mage_Sales_Model_Resource_Quote_Item_Collection
	 */
	protected function _getCartItems () {
		$items = Mage::getModel('sales/quote_item')->getCollection();
		$items->setQuote($this->_getQuote());
		return $items;
	}
	
	protected function _isProductInCart ($productId) {
		foreach ($this->_getCartItems() as $item) {
			/* @var $item Mage_Sales_Model_Quote_Item */
			if ($item->getProductId() === $productId) {
				return true;
			}
		}
		return false;
	}
	
	protected function _getSuggestedProducts(){
		if($this->_isSuggestProductsByBundle()){
			$bundleProduct = $this->_bundleProduct;
				
			/* @var $type Mage_Bundle_Model_Product_Type */
			$type = $bundleProduct->getTypeInstance(true);
				
			$products = $type->getSelectionsCollection($type->getOptionsIds($bundleProduct), $bundleProduct);
				
			return $products;
		}
		return null;
	}
	
	protected function _isSuggestProductsByBundle(){
		return (bool) $this->_bundleProduct;
	}

	protected function _getSuggestedProductsData () {
		
		if($this->_isSuggestProductsByBundle()){
			$bundleProduct = $this->_bundleProduct;
			
			/* @var $typeInctance Mage_Bundle_Model_Product_Type */
			$typeInstance = $bundleProduct->getTypeInstance(true);
			
			/* @var $selectionCollection Mage_Bundle_Model_Resource_Selection_Collection */
			$selectionCollection = $typeInstance->getSelectionsCollection(
					$typeInstance->getOptionsIds($bundleProduct), $bundleProduct
			);
			
			$optionCollection = $typeInstance->getOptionsCollection($bundleProduct);
			
			$_options = $optionCollection->appendSelections($selectionCollection, false,
					Mage::helper('catalog/product')->getSkipSaleableCheck()
			);
			
			$r = array();
			
			foreach($_options as $group){
				
				$groupData = array();
				
				$groupData['title'] = $group->getDefaultTitle();
				
				foreach($group->getSelections() as $product){
					
					$isChecked = $this->_isProductInCart($product->getId());
					
					$productData = array();
					$productData['id'] = $product->getId();
					$productData['name'] = $product->getName();
					
					$productData['price'] = $product->getFinalPrice();
					$productData['price_formatted'] = $this->_formatMoney($product->getFinalPrice());
					
					$productData['old_price'] = $product->getPrice();
					$productData['old_price_formatted'] = $this->_formatMoney($product->getPrice());
					
					$productData['checked'] = $isChecked;
					$productData['qty'] = (int) $product->getSelectionQty();
					
					$groupData['products'][] = $productData;
					
				}
				
				
				$r[] = $groupData;
			}
			
			
			return $r;
			
		}
		
		return array();
	}

	protected function _getCart () {
		if ($this->_cart === null) {
			
			/* @var $cart Mage_Checkout_Model_Cart */
			$cart = Mage::getSingleton('checkout/cart');
			$cart->init();
			
			$this->_cart = $cart;
		}
		
		return $this->_cart;
	}
	
	public function removeProductFromCart($productId){
		foreach($this->_getCartItems() as $item){
			if($item->getProductId() == $productId){
				Mage::getSingleton('checkout/cart')->removeItem($item->getId())->save();
			}
		}
	}

	public function addProductToCart ($product, $qty = 1) {
		$product = $this->_checkProductArgument($product);
		
		$cart = $this->_getCart();
		
		// Add product
		$qtyObj = new Varien_Object(array(
				'qty' => $qty
		));
		
		$quoteItem = $cart->addProduct($product, $qtyObj);
		
		$cart->save();
		
		$this->_getSession()->setCartWasUpdated(true);
		
	}
	
	public function setProductCartQty ($product, $qty = 1) {
		$product = $this->_checkProductArgument($product);
		
		if($this->_isProductInCart($product)){
			// Change qty
			foreach($this->_getCartItems() as $item){
				if($item->getProductId() == $product->getId()){
					$item->setQty($qty);
					$item->save();
				}
			}
			
		}else{
			// Product is not in cart, add it
			$this->addProductToCart($product,$qty);
			
		}
	}
		
	protected function _checkProductArgument($product){
		if(is_numeric($product)){
			$product = Mage::getModel('catalog/product')->load($product);
		}
		
		if(!$product instanceof Mage_Catalog_Model_Product){
			Mage::throwException('Expected an object of type Mage_Catalog_Model_Product.');
		}
		
		if(!$product->getId()){
			Mage::throwException('Product is empty, cannot continue');
		}
		
		return $product;
	}
}