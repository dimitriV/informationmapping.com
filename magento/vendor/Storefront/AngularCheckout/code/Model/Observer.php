<?php
class Storefront_AngularCheckout_Model_Observer{
	
	
	public function interceptOnepageCheckout(){
		//controller_action_predispatch_checkout_onepage_index
		
		$url = Mage::getUrl('angularcheckout/checkout/');
		
		header('Location:' .$url);
		
		exit;
	}
	
	public function rememberOrderWhenCreating($observer){
		$order = $observer->getOrder();
		Mage::register('current_order', $order, true);
	}
	
}