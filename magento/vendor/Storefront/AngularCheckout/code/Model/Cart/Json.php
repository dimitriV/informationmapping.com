<?php

class Storefront_AngularCheckout_Model_Cart_Json {
	
	const THUMBNAIL_SIZE = 250;

	protected function _getQuote(){
		/* @var $cart Mage_Checkout_Model_Cart */
		$cart = Mage::getSingleton('checkout/cart');
		
		$quote = $cart->getQuote();
		
		return $quote;
	}
	
	public function getCartData ($includeCrossSells = false) {
		
		$helper = Mage::helper('angularcheckout');
		
		// Build JSON output
		$data = array();
		
		$quote = $this->_getQuote();
		
		// Needed for promotions and free shipping! Otherwise these will be forgotten
		$quote->collectTotals();
		
		/* @var $items Mage_Sales_Model_Resource_Quote_Item_Collection */
		// $items = $quote->getItemsCollection(false);
		
		$items = $this->_getCartItems($quote);
		
		/* @var $cartBlock Mage_Checkout_Block_Cart */
		$cartBlock = Mage::app()->getLayout()->createBlock('checkout/cart', 'tmpcart');
		
		$itemsOutput = array();
		foreach ($items as $item) {
			/* @var $item Mage_Sales_Model_Quote_Item */
			
			if($item->getParentItemId()){
				// This is a child item (possibly an item in a bundle)
				continue;
			}
			
			$oneItem = array();
			
			/* @var $renderer Mage_Checkout_Block_Cart_Item_Renderer */
			$renderer = $cartBlock->getItemRenderer($item->getProductType());
			$renderer->setItem($item);
			
			$thumbnail = $renderer->getProductThumbnail();
			if (is_string($thumbnail)) {
				$thumbnailUrl = $thumbnail;
			} else {
				$thumbnailUrl = $thumbnail->resize(self::THUMBNAIL_SIZE);
			}
			
			$oneItem['item_id'] = $item->getId();
			$oneItem['product_id'] = $item->getProductId();
			
			$oneItem['sku'] = $item->getSku();
			
			$oneItem['unit_price_incl_tax'] = $item->getPriceInclTax();
			$oneItem['unit_price_incl_tax_formatted'] = $this->_formatMoney($oneItem['unit_price_incl_tax']);
			$oneItem['unit_price_excl_tax'] = $item->getPrice();
			$oneItem['unit_price_excl_tax_formatted'] = $this->_formatMoney($oneItem['unit_price_excl_tax']);
			
			$oneItem['row_total_incl_tax'] = $item->getRowTotalInclTax();
			$oneItem['row_total_incl_tax_formatted'] = $this->_formatMoney($oneItem['row_total_incl_tax']);
			$oneItem['row_total_excl_tax'] = $item->getRowTotal();
			$oneItem['row_total_excl_tax_formatted'] = $this->_formatMoney($oneItem['row_total_excl_tax']);
			
			$oneItem['name'] = $renderer->getProductName();
			
			$oneItem['product_url'] = $renderer->getProductUrl();
			$oneItem['thumbnail_url'] = '' . $thumbnailUrl;
			
			// $oneItem['additional_html'] = $renderer->getProductAdditionalInformationBlock()->toHtml();
			
			$oneItem['qty'] = $renderer->getQty();
			
			// TODO add support for max and minimum qty
			// $oneItem['max_qty'] = false;
			// $oneItem['min_qty'] = false;
			
			// TODO check errors
			// $oneItem['has_error']
			
			// TODO add support for wishlist
			
			// TODO add support for minimum order value
			
			$itemsOutput[] = $oneItem;
		}
		$data['items'] = $itemsOutput;
		
		$totalOutput = array();
		
		$totalInclTax = 0;
		$totalTax = 0;
		
		foreach ($quote->getTotals() as $total) {
			/* @var $total Mage_Sales_Model_Quote_Address_Total */
			
			if ($total->getCode() == 'subtotal') {
				// Skip subtotal
				continue;
			}
			
			$oneTotal = array();
			
			$cssClasses = array(
					'total-' . $total->getCode()
			);
			
			if ($total->getValue() == 0) {
				$cssClasses[] = 'free';
			}
			
			$oneTotal['title'] = $total->getTitle();
			$oneTotal['class'] = implode(' ', $cssClasses);
			$oneTotal['value'] = $total->getValue();
			$oneTotal['value_formatted'] = $this->_formatMoney($oneTotal['value'], false);
			
			if ($total->getCode() == 'grand_total') {
				$totalInclTax = $total->getValue();
				$oneTotal['title'] = $helper->__('Total incl Tax');
			} else 
				if ($total->getCode() == 'tax') {
					$totalTax = $total->getValue();
					$oneTotal['title'] = $helper->__('VAT');
				}
			
			$totalOutput[] = $oneTotal;
		}
		
		// Adding grand total excl tax
		
		$totalExclTax = $totalInclTax - $totalTax;
		$oneTotal = array();
		$oneTotal['title'] = $helper->__('Total excl tax');
		
		$cssClasses = array('total_excl_tax');
		if ($totalExclTax == 0) {
			$cssClasses[] = 'free';
		}
		$oneTotal['class'] = implode(' ', $cssClasses);
		
		$oneTotal['value'] = $totalExclTax;
		$oneTotal['value_formatted'] = $this->_formatMoney($totalExclTax, false);
		
		array_unshift($totalOutput, $oneTotal);
		
		$data['totals'] = $totalOutput;
		
		$isVatExempt = $this->_isVatExempt();
		
		$data['is_vat_exempt'] = $isVatExempt;
		
		$data['has_error'] = $quote->getHasError();
		
		if ($includeCrossSells) {
			$crosssells = $this->getCrossSellsData();
			$c = array();
			foreach ($crosssells as $crosssell) {
				$productId = $crosssell->getProductId();
				/* @var $product Mage_Catalog_Model_Product */
				$product = Mage::getModel('catalog/product')->load($productId);
				
				$qty = $crosssell->getQty();
				
				// WOUTER: This does not use the cart item renderer - should we?
				
				$productImageSrc = ''.Mage::helper('catalog/image')->init($product, 'thumbnail')->resize(self::THUMBNAIL_SIZE);
				
				$addToCartUrl = Mage::getUrl('checkout/cart/add').'?product='.$productId.'&qty='.$qty;
				$addToCartLabel = $crosssell->getAddToCartLabel();
				
				if(!$addToCartLabel){
					$addToCartLabel = Mage::helper('catalog')->__('Add to Cart');
				}
				
				$item = array(
						'product_id' => $productId,
						'qty' => $qty,
						'reason_title' => $crosssell->getReasonTitle(),
						'reason_text' => $crosssell->getReasonText(),
						'product_name' => $product->getName(),
						'product_description' => $product->getDescription(),
						'product_image' => $productImageSrc,
						'product_url' => $product->getProductUrl(),
						'reason_title' => $crosssell->getReasonTitle(),
						'reason_subtitle' => $crosssell->getReasonSubtitle(),
						'reason_text' => $crosssell->getReasonText(),
						'add_to_cart_label' => $addToCartLabel,
						'add_to_cart_url' => $addToCartUrl
				);
				
				$c[] = $item;
			}
			$data['crosssells'] = $c;
		}
		
		// $methodsOutput = array();
		// $methods = $cartBlock->getMethods('top_methods');
		// foreach($methods as $blockName){
		// $block = $layout->getBlock($blockName);
		
		// $methodHtml = trim($block->toHtml());
		
		// if($methodHtml){
		
		// //$methodsOutput[] = array('url' => $methodUrl, 'title' => $methodTitle);
		// }
		// }
		
		// $data['methods'] = $methodsOutput;
		
		return $data;
	}

	public function getCrossSellsData ($numCrossSells = 4) {
		return array();

	}

	/**
	 * Long way to get cart items, preventing item cache
	 *
	 * @return Mage_Sales_Model_Resource_Quote_Item_Collection
	 */
	protected function _getCartItems (Mage_Sales_Model_Quote $quote) {
		$items = Mage::getModel('sales/quote_item')->getCollection();
		$items->setQuote($quote);
		return $items;
	}

	protected function _isVatExempt(){
		$quote = $this->_getQuote();
		$totals = $quote->getTotals();
		
		
		if(isset($totals['tax']) && $totals['tax']->getValue() == 0){
			
			$ba = $quote->getBillingAddress();
			
			if($ba->getVatId()){
			
				$customerCountry = $ba->getCountryId();
				$sellerCountry = Mage::getStoreConfig('shipping/origin/country_id', $quote->getStoreId());
				
				if($customerCountry === $sellerCountry){
					return true;
				}
			
			}
			
		}
			
		return false;
		
	}
	
	protected function _formatMoney ($amount, $allowFreeText = true) {
		$helper = Mage::helper('angularcheckout');
		
		if ($amount == 0) {
			if ($allowFreeText) {
				return $helper->__('Free');
			}
		}
		return Mage::app()->getStore()->formatPrice($amount, false);
	}
}