// ngAnimate

var checkoutApp = angular.module('checkoutApp', ['ngInputModified']).controller('CheckoutController', function($sce, $scope, $timeout, $http) {

	$scope.data = null;
	$scope.temp = {};
	$scope.updateRequestCount = 0;
	
	/* Local data */
	$scope.local = {
		isRenderingReady: false
	};
	
	$scope.init = function(){
		
		if(typeof(json) !== 'undefined'){
			// Load data from in-page
			$scope._processData(json);
			
		}else{
		
			// Load data from remote
			$scope._loadJson();
		}
	}
	
	$scope.isLoggedIn = function(){
		if($scope.data && $scope.data.checkout && $scope.data.checkout.is_logged_in){
			return true;
		}
		return false
	}
	
	$scope.isPaymentMethodSelected = function(){
		if($scope.data && $scope.data.checkout && $scope.data.checkout.payment_method && $scope.data.checkout.payment_method.options.length > 0){
			return true;
		}
		return false;
	}
	
	$scope._loadJson = function(){
		
		$scope.data = null;
		
		$http.get(checkoutJsonUrl).success(function(data, status, headers, config){
			$scope._processData(data);
			
		}).error(function(data, status, headers, config){
			$scope._handleError(data, status);
		});
	}
	
	$scope._processData = function(data){
		if(data && data.checkout){
			
			// Data is in valid format!
			
			$scope.data = data;
			
			// TODO set all "p.help-block" with advise
			
			var body = jQuery('body');
			if($scope.data.show_prices_incl_vat){
				body.removeClass('prices-excl-vat');
				body.addClass('prices-incl-vat');
			}else{
				body.removeClass('prices-incl-vat');
				body.addClass('prices-excl-vat');
			}
			
			//console.log('broadcasting "checkout-loaded-validity"');
			
			$scope.$broadcast('checkout-loaded-validity');
			
			// Dispatch event when HTML rendering is done
			// Timeout makes sure it is run last
			if(!$scope.local.isRenderingReady){
				
				$scope.local.isRenderingReady = true;
				
				$timeout(function () {
					jQuery(document).trigger( "angularcheckout_rendered");
					//console.log('Dispatched event "angularcheckout_rendered"');
					
	            }, 0, false);
			}
			
		}else{
			
			$scope._handleError(data);
			
		}
	}
	
	$scope._handleError = function(data, status){
		if(data && data.error){
			
			$scope.error = data;
			// TODO error handling - show modal with error message
		}else{
			alert('ERROR: '+data);
		}
		//$scope.data = null;
	}
	
	$scope.getTrustedHtml = function(html){
		return $sce.trustAsHtml(html);
	}
	
	$scope.markFieldBeingUpdated = function(fieldName){
		$scope.temp[fieldName+'_updating'] = true;
		$scope.temp['field_updating'] = true;
		
		if(!$scope.temp.fieldsBeingUpdated){
			$scope.temp.fieldsBeingUpdated = [];
		}
		
		$scope.temp.fieldsBeingUpdated.push(fieldName);
	}
	
	$scope.markFieldUpdateReady = function(fieldName){
		delete $scope.temp[fieldName+'_updating'];
		delete $scope.temp['field_updating'];
		
		var index = $scope.temp.fieldsBeingUpdated.indexOf(fieldName);
		if (index > -1) {
			$scope.temp.fieldsBeingUpdated.splice(index, 1);
		}
	}
	
	$scope.markAllFieldsUpdateReady = function(){
		
		while($scope.temp.fieldsBeingUpdated.length > 0){
			// The array gets shorter with every run
			var field = $scope.temp.fieldsBeingUpdated[0];
			$scope.markFieldUpdateReady(field);
		}
	}
	
	$scope.isFieldBeingUpdated = function(fieldName){
		if($scope.temp && $scope.temp[fieldName+'_updating']){
			return true;
		}
		return false;
	}
	
	$scope.isAnyFieldBeingUpdated = function(){
		if($scope.temp && $scope.temp['field_updating']){
			return true;
		}
		return false;
	},
	
	$scope.isBillingEditMode = function(){
		var dataLoaded = (typeof $scope.data !== 'undefined') && (typeof $scope.data.checkout !== 'undefined');
		var isBillingEditClicked = $scope.local.edit_billing;
		var isFirstOrder = $scope.data.checkout.is_first_order;
		
		var validationData = $scope.data.checkout.form.is_valid;
		var isBillingAddressValid = validationData['billing_info.first_name'] && 
			validationData["billing_info.vat_id"] &&
			validationData["billing_info.first_name"] &&
			validationData["billing_info.last_name"] &&
			validationData["billing_info.phone"] &&
			validationData["billing_info.street"] &&
			validationData["billing_info.housenumber"] &&
			validationData["billing_info.postalcode"] &&
			validationData["billing_info.city"] &&
			validationData["billing_info.company"] &&
			validationData["billing_info.state"] &&
			validationData["billing_info.country"]
		
		var isBillingAddressInvalid = !isBillingAddressValid;
		
		/*
		console.log('dataLoaded = '+dataLoaded);
		console.log('isBillingEditClicked = '+isBillingEditClicked);
		console.log('isFirstOrder = '+isFirstOrder);
		console.log('isBillingAddressInvalid = '+isBillingAddressInvalid);
		*/
		var isEditMode = dataLoaded && (isBillingEditClicked || isFirstOrder || isBillingAddressInvalid);
		
		/*
		console.log('isEditMode = '+isEditMode);
		*/
		return isEditMode;
	}
	
	$scope.isShippingEditMode = function(){
		
		if($scope.data && $scope.data.checkout.shipping_info.all_addresses){
			
			// We are using a local var.
			// If the local var is not set yet, we will choose the value based on the number of address options
			
			if (typeof $scope.local.edit_shipping_address === 'undefined') {
				$scope.local.edit_shipping_address = ($scope.data.checkout.shipping_info.all_addresses.length == 0);
			}
			
			return $scope.local.edit_shipping_address;
			
		}
		
		return false;
	}
	
	
	$scope.openEditBilling = function(){
		$scope.local.edit_billing = true;
		
		// The form is changed, so we need to process validity again...
		$scope.$broadcast('checkout-loaded-validity');
	}
	
	$scope.openEditShipping = function(shipping_address){
		$scope.local.edit_shipping_address = shipping_address;
		
		// The form is changed, so we need to process validity again...
		$scope.$broadcast('checkout-loaded-validity');
	}
	
	$scope.isCheckoutUpdating = function(){
		return $scope.isUpdatingQty() || $scope.isAnyFieldBeingUpdated();
	}
	
	$scope.toggleProductInCart = function(product){
		
		if($scope.canToggleProduct(product)){
			
			var productId = product.id;
			var qty = product.qty;
			
			// This uses a pseudo-fieldname so we can reuse code
			var dummyFieldName = 'toggle_product_'+productId;
			$scope.markFieldBeingUpdated(dummyFieldName);
			
			// Build data object
			var dataObj = {};
			
			if($scope.isProductInCart(productId)){
				// Remove from cart
				//console.log('Remove product id "'+productId+'" from cart');
				
				dataObj['remove_from_cart_product_id'] = productId;
				
			}else{
				// Add to cart
				//console.log('Add product id "'+productId+'" to cart');
				
				dataObj['set_cart_qty_product_id'] = productId;
				dataObj['set_cart_qty'] = qty;
	
			}
			
			$scope._sendJsonData(dataObj);
		}
		
	}
	
	$scope.isProductInCart = function(productId){
		if($scope.data){
			for(var i = 0; i < $scope.data.cart.items.length; i++){
				
				var product = $scope.data.cart.items[i];
				if(product.product_id == productId){
					return true;
				}
				
			}
		}
		return false;
	}
	
	$scope._sendJsonData = function(dataObj){
		dataObj['version'] = $scope.data.version;
		
		$scope.updateRequestCount++;
		
		$http.post(checkoutJsonUrl, dataObj).success(function(data, status, headers, config){
			
			$scope.updateRequestCount--;
			
			// Only update the display when no other AJAX calls are running to prevent "flickering".
			if($scope.updateRequestCount == 0){
				if(data){
					$scope._processData(data);
				}
				$scope.markAllFieldsUpdateReady();
			}
			
		}).error(function(data, status, headers, config){
			
			$scope.updateRequestCount--;
			
			$scope._handleError(data, status);
		});
	}
	
	$scope.isProductBeingUpdated = function(productId){
		// This uses a pseudo-fieldname so we can reuse code
		var dummyFieldName = 'toggle_product_'+productId;
		return $scope.isFieldBeingUpdated(dummyFieldName);
	}
	
	$scope.updateCheckout = function(fieldName, updateSingle){
		
		// Get value to send
		var expr = "$scope.data.checkout." + fieldName;
		var value = eval(expr);
		
		// Mark as being updated
		$scope.markFieldBeingUpdated(fieldName);
		
		var dataObj = {};
		if(typeof value == 'undefined'){
			dataObj[fieldName] = '';
		}else{
			dataObj[fieldName] = value;
		}
		
		$scope._sendJsonData(dataObj);
	}
	
	$scope.isDataLoaded = function(){
		return $scope.data;
	}
	
	$scope.canEnterCheckoutSteps = function(){
		if(!$scope.isDataLoaded()){
			return false;
		}
		
		var isGuestCheckout = !$scope.data.checkout.is_logged_in && $scope.data.checkout.allow_guest_checkout;
		var isLoggedInCheckout = $scope.data.checkout.is_logged_in;
		
		// TODO removed this criteria... do we need it?? guess not
		// !$scope.data.checkout.allow_login
		
		var showSteps = isLoggedInCheckout || isGuestCheckout;
		
		return showSteps && $scope.data.checkout && $scope.data.cart && $scope.data.cart.items && $scope.data.cart.items.length > 0;
	}
	
	$scope.markUpdatingQty = function(item){
		$scope.temp['updating_qty'] = true;
	}
	
	$scope.markUpdatingQtyFinished = function(item){
		$scope.temp['updating_qty'] = false;
	}
	
	$scope.setQty = function(item){
		var newQty = item.qty;
		$scope._changeQty(item, newQty);
	}
	
	$scope._changeQty = function(item, newQty){
		$scope.markUpdatingQty(item);
		
		var dataObj = {
			item_id: item.item_id,
			qty: newQty
		};
		
		$http.post(checkoutJsonUrl, dataObj).success(function(data, status, headers, config){
			if(data){
				$scope._processData(data);
			}
			$scope.markUpdatingQtyFinished(item);
			
		}).error(function(data, status, headers, config){
			$scope._handleError(data, status);
		});
	} 
	
	$scope.increaseQty = function(item){
		$scope._changeQty(item, item.qty + 1);
	}
	
	$scope.decreaseQty = function(item){
		$scope._changeQty(item, item.qty - 1);
	}
	
	$scope.isUpdatingQty = function(){
		if($scope.temp['updating_qty']){
			return $scope.temp['updating_qty'];
		}
		return false;
	}
	
	$scope.isStateRequired = function(){
		
		
		if($scope.data && $scope.data.checkout){
			
			var countryCode = $scope.data.checkout.billing_info.country;
			
			if($scope.data.checkout.form.states[countryCode]){
				return true;
			}
			
		}
		
		return false;
	}
	
	$scope.submitOrder = function(){
		
		// Apply validation from JSON
		$scope.$broadcast('checkout-loaded-validity');
		
		// Show all validation states
		$scope.$broadcast('show-errors-check-validity');
		
		if ($scope.checkoutForm.$invalid) {
			
			// The form has errors
			var errors = $scope.checkoutForm.$error;
			
			console.log('VALIDATION ERRORS...');
			console.log(errors);
			
		}else{
			
			$scope.temp['order_being_placed'] = true;
		
			$http.post(placeOrderUrl).success(function(data, status, headers, config){
				if(data.success){
					window.location = data.redirect;
					
					// Don't release order button
					
				}else{
					alert('Error');
					
					// Reset form
					$scope._loadJson();
					
					// Release order button
					$scope.temp['order_being_placed'] = false;
				}
				
				
			}).error(function(data, status, headers, config){
				$scope._handleError(data, status);
				
				$scope.temp['order_being_placed'] = false;
			});
		}
	}
	
	$scope.getStatesOptionsForBilling = function(){
		if($scope.data){
			var countryCode = $scope.data.checkout.billing_info.country;
			
			if(countryCode){
				var r = $scope.data.checkout.form.states[countryCode];
				return r;
			}
		}
		
		return false;
	}
	
	$scope.getStatesOptionsForShipping = function(){
		if($scope.data){
			var countryCode = $scope.data.checkout.shipping_info.country;
			
			if(countryCode){
				var r = $scope.data.checkout.form.states[countryCode];
				return r;
			}
		}
		
		return false;
	}
	
	
	
	$scope.isOrderBeingPlaced = function(){
		if ($scope.temp['order_being_placed']){
			return true;
		}
		return false;
	}
	
	$scope.canToggleProduct = function(product){
		if(product.price === 0){
			return false;
		}
		return true;
	}
	
	
	
	/*
	$scope.changeQty = function(item){
		var newQty = item.qty;
		var itemId = item.item_id;
		
		var dataObj = { item_id : itemId, qty: newQty };
		
		item.loading = true;
		$scope.cart.disable_checkout = true;
		
		$http.post(cartJsonUrl, dataObj).success(function(data, status, headers, config){
			$scope._processData(data);
		});
	}
	
	$scope.removeItem = function(item){
		item.qty = 0;
		$scope.changeQty(item);
	}
	*/
	
	// Init
	$scope.init();

}).
  directive('showErrors', ['$timeout', function($timeout) {
    return {
      restrict: 'A',
      require:  '^form',
      link: function (scope, el, attrs, formCtrl) {
        
    	  
    	  // The timeout is used to make sure this is run later.
    	  // Radio buttons inside a ng-repeat do not exist yet, so we can only run this code after all ng-repeats.
    	  
    	  $timeout(function(){
    	  
    	  
        	//find the text box element, which has the 'name' attribute
	        var inputEl   = el[0].querySelector("[name]");
	        
	        if(inputEl == null){        	
	        	// input not found :(
	        	
	        }else{
	        	
		        // convert the native text box element to an angular element
		        var inputNgEl = angular.element(inputEl);
		        
		        // get the name on the text box so we know the property to check
		        // on the form controller
		        var inputName = inputNgEl.attr('name');
		
		        // only apply the has-error class after the user leaves the text box
		        
		        inputNgEl.bind('focus', function() {
		        	var t = jQuery(this);
		        	var valueOnFocus = t.val();
		        	t.data('value-on-focus', valueOnFocus);
		        });
		        
		        
		        inputNgEl.bind('blur', function() {
		        	
		        	var usesRemoteValidation = (inputNgEl.attr('remote-validation') === '');
		        	
		        	var isValueChanged = false;
		        	var valueOnFocus = inputNgEl.data('value-on-focus');
		        	var currentValue = inputNgEl.val();
		        	
		        	var isPristine = formCtrl[inputName].$pristine;
		        	var isEmpty = (inputNgEl.val() === '');
		        	
		        	if(isPristine && isEmpty){
		        		
		        		// The field has not been used really
		        		el.toggleClass('has-feedback', false);
		        		el.toggleClass('has-error', false);
			        	el.toggleClass('has-success', false);
			        	
		        	}else{
		        	
			        	if(currentValue !== valueOnFocus){
			        		isValueChanged = true;
			        	}
			        	
			        	if(usesRemoteValidation && isValueChanged){
			        		// Remote validation will set the correct values later
			        		el.toggleClass('has-feedback', true); // true is needed to position the loading icon
			        		el.toggleClass('has-error', false);
				        	el.toggleClass('has-success', false);
				        	
			        	}else{
			        		var invalid = formCtrl[inputName].$invalid;
			        		
			        		el.toggleClass('has-feedback', true);
			        		el.toggleClass('has-error', invalid);
				        	el.toggleClass('has-success', ! invalid);
	
			        	}
		        	
		        	}
		        	
		        	
		        	
		        });
		        
		        
		        // Run when event is dispatched - example: submit button was clicked
		        scope.$on('show-errors-check-validity', function() {
		        	var invalid = formCtrl[inputName].$invalid

		        	el.toggleClass('has-error', invalid);
		        	el.toggleClass('has-success', ! invalid);
		            
		            el.toggleClass('has-feedback', true);
		            
		            //console.log(inputName + ' is '+invalid);
		        });
		        
		        
		        // When JSON data is loaded, we need to update the dirty fields
		        scope.$on('checkout-loaded-validity', function(that) {
		        	
		        	//TODO this should also be triggered when becoming visible.
	        		// For now we have a workaround in openEditBilling() that will trigger this event
	        		
		        	var isPristine = formCtrl[inputName].$pristine;
		        	var isDirty = formCtrl[inputName].$dirty;
		        	var invalid = formCtrl[inputName].$invalid;
		        	
		        	
		        	
		        	// Uses a module to provide "modified", comparing original form state to current value
		        	
		        	// TODO this is not perfect. If you change the value a couple of times and then set back to the initial value, it will not be modified
		        	// To reproduce: Empty the po number, then re-enter. The field will NOT have validation due to wrong "touched"
		        	var modified = formCtrl[inputName].modified;
		        	
		        	// Don't use "touched" - it will trigger fields that have focus but should not be validated yet
		        	
		        	if(isDirty || !isPristine || modified){
		        		el.toggleClass('has-error', invalid);
		                el.toggleClass('has-success', ! invalid);
		                
		                el.toggleClass('has-feedback', true);
		        	}
		        	
		        });
	        
	        }
	        
    	  }, 1);
        
      }
    }
  }]).
  directive('remoteValidation', function() {
	    return {
	      restrict: 'A',
	      require: '?ngModel', // get a hold of NgModelController
	      link: function(scope, element, attrs, ngModel) {
	          if (!ngModel || !attrs || !attrs.ngModel){
	        	  return; // do nothing if missing requirements
	          }
	        
	         
	    	
	        var ngModelString = attrs.ngModel;
	        //var ngModelValue = eval('$scope.'+ngModelString);
	        var parts = ngModelString.split('.');
	        
	        var validationPath = '';
	        for(var i = 2; i < parts.length; i++){
	        	validationPath += parts[i];
	        	if(i < parts.length - 1){
	        		validationPath += '.';
	        	}
	        }
	        
	        
	    	  
	        	scope.$on('checkout-loaded-validity', function(that) {
	        		var isValid = eval('scope.data.checkout.form.is_valid[\''+validationPath+'\']');
	    		  
	        		//console.log(validationPath + ' is valid? ' + isValid);
	    		  
	        		if(typeof isValid == 'undefined'){
	        			// Not set => valid, but log
	        			isValid = true;
	        			console.log('No validation state set for '+validationPath);
	        		}
	        		
	        		// Use boolean from JSON for validity state
	        		ngModel.$setValidity("remoteCheckoutValidation", isValid);
	        		
	    		  	
		        });
	        
	      }
	    }
	  });;


var AngularApp = {
	init: function(){
		var angularRoot = jQuery('[ng-controller=CheckoutController]');
		angular.bootstrap(angularRoot, ['checkoutApp']);
	}
}

jQuery(document).ready(function(){
	// Init Angular
	jQuery.event.trigger('startAngularCheckout');
});

jQuery(document).bind('startAngularCheckout', function(){
	AngularApp.init();
});

function getDiff(prev, now) {
	
	if(prev === null){
		return now;
	}
	
    var changes = {};
    for (var prop in now) {
        if (!prev || prev[prop] !== now[prop]) {
            if (typeof now[prop] == "object") {
                var c = getDiff(prev[prop], now[prop]);
                if (c.length > 0 ){
                    changes[prop] = c;
                }
            } else {
                changes[prop] = now[prop];
            }
        }
    }
    return changes;
}