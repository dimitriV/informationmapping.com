<?php

class Storefront_PayOgone_Block_Payment extends Mage_Core_Block_Template {

	protected function _construct () {
		parent::_construct();
		$this->setTemplate('payogone/payment.phtml');
		return $this;
	}
}
