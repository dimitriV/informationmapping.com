<?php

class Storefront_PayOgone_Block_Info extends Mage_Payment_Block_Info_Cc {

	protected function _construct () {
		parent::_construct();
		$this->setTemplate('payogone/info.phtml');
	}

	/**
	 * Return AlertPay PDF info html
	 *
	 * @return html
	 */
	public function toPdf () {
		$this->setTemplate('payogone/pdf/info.phtml');
		return $this->toHtml();
	}

	/**
	 * Get the label for the Ogone status.
	 * If the code argument is null, the last Ogone status code is used.
	 *
	 * @param $statusCode string        	
	 */
	public function getOgoneStatusLabel ($statusCode = null) {
		if ($statusCode === null) {
			$payment = $this->getInfo();
			/* @var $payment Mage_Sales_Model_Order_Payment */
			
			$statusCode = $payment->getAdditionalInformation('ogone_last_status');
		}
		$ogoneHelper = Mage::helper('ogone');
		$statusLabel = $ogoneHelper->getOgoneStatusLabel($statusCode);
		return $statusLabel . ' (' . $ogoneHelper->__('Code %s', $statusCode) . ')';
	}

	public function _getOgoneStatusHistoryHtml () {
		$ogoneHelper = Mage::helper('ogone');
		$payment = $this->getInfo();
		/* @var $payment Mage_Sales_Model_Order_Payment */
		
		$ogoneStatusHistory = $payment->getAdditionalInformation('ogone_status_history');
		if ($ogoneStatusHistory === null) {
			return '';
		} else {
			$r = '<div style="color: #999; border-top: 1px solid #D6D6D6; border-bottom: 1px solid #D6D6D6; font-size: 85%;">';
			$r .= '<p>' . $ogoneHelper->__('This shows the history of Ogone messages as they have reached Magento.') . '</p>';
			$r .= '<ul>';
			$ogoneStatusHistory = array_reverse($ogoneStatusHistory);
			foreach ($ogoneStatusHistory as $historyItem) {
				$timestamp = $historyItem['timestamp'];
				$status = $historyItem['status'];
				$zendDate = new Zend_Date($timestamp);
				$formattedTimestamp = Mage::helper('core')->formatDate($zendDate, 'short', true);
				
				$r .= '<li>';
				$r .= $formattedTimestamp . ': ' . $this->getOgoneStatusLabel($status);
				
				if (isset($historyItem['params']['NCERRORPLUS'])) {
					$r .= ' - ' . $historyItem['params']['NCERRORPLUS'];
				}
				$r .= '</li>';
			}
			$r .= '</ul>';
			
			$r .= '</div>';
		}
		return $r;
	}
}
