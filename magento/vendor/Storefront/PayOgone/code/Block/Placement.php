<?php

class Storefront_PayOgone_Block_Placement extends Mage_Core_Block_Template {

	public function __construct () {}

	/**
	 * Return checkout session
	 *
	 * @return Mage_Checkout_Model_Session
	 */
	public function getCheckout () {
		return Mage::getSingleton('checkout/session');
	}

	/**
	 * Return Ogone payment API instance
	 *
	 * @return Storefront_PayOgone_Model_Api
	 */
	protected function _getApi () {
		return Mage::getSingleton('ogone/api');
	}

	/**
	 * Return order instance by last increment id
	 *
	 * @return Mage_Sales_Model_Order
	 */
	protected function _getOrder () {
		if ($this->getOrder()) {
			$order = $this->getOrder();
		} elseif ($this->getCheckout()->getLastRealOrderId()) {
			$order = Mage::getModel('sales/order')->loadByIncrementId($this->getCheckout()->getLastRealOrderId());
		} else {
			return null;
		}
		return $order;
	}

	/**
	 * Return Ogone payment form data by Ogone API
	 *
	 * @return array
	 */
	public function getFormData () {
		return $this->_getApi()->getFormFields($this->_getOrder());
	}

	/**
	 * Return gateway path from admin settings
	 *
	 * @return string
	 */
	public function getFormAction () {
		return $this->_getApi()->getConfig()->getGatewayPath();
	}
}
