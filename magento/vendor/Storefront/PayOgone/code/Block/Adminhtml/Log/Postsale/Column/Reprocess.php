<?php

class Storefront_PayOgone_Block_Adminhtml_Log_Postsale_Column_Reprocess extends Mage_Adminhtml_Block_Widget_Grid_Column_Renderer_Abstract {

	public function render (Varien_Object $row) {
		$data = $row->getPostData();
		
		$stores = Mage::app()->getStores();
		$firstStore = reset($stores);
		$firstStoreId = $firstStore->getId();
		
		$html = '<form action="'.Mage::getUrl('ogone/api/postBack', array('_store' => $firstStoreId)).'" method="post">';
		
		foreach($data as $key => $value){
			$html .= '<input name="'.$key.'" value="'.$this->escapeHtml($value).'" type="hidden" />';
		}
		$html .= '<button type="submit"><span><span><span>'.$this->__('Reprocess').'</span></span></span></button>';
		$html .= '</form>';
		return $html;
	}
}