<?php
class Storefront_PayOgone_Block_Adminhtml_Log_Postsale_Grid extends Mage_Adminhtml_Block_Widget_Grid{
	/**
	 * Initialize Grid settings
	 */
	public function __construct () {
		parent::__construct();

		$this->setId ( 'payogone_postsale_grid' );
		$this->setDefaultSort ( 'created_at' );
		$this->setDefaultDir ( 'DESC' );
		$this->setSaveParametersInSession ( true );
	}
	
	protected function _prepareCollection() {
		$collection = Mage::getModel ( 'ogone/log_postsale_request' )->getResourceCollection ();
		$this->setCollection ( $collection );
		return parent::_prepareCollection ();
	}
	
	protected function _prepareColumns() {
		$helper = Mage::helper ( 'ogone' );
		
// 		$this->addColumn ( 'id', array ('header' => $helper->__ ( 'ID' ), 'width' => '30px', 'index' => 'id' ) );
		$this->addColumn ( 'created_at', array ('header' => $helper->__ ( 'Date' ), 'width' => '170px', 'index' => 'created_at' ) );
		$this->addColumn ( 'orderID', array ('header' => $helper->__ ( 'Order ID' ), 'width' => '120px', 'index' => 'orderID' ) );
		$this->addColumn ( 'CN', array ('header' => $helper->__('Customer'), 'width' => '170px', 'index' => 'CN' ) );
		$this->addColumn ( 'STATUS', array ('header' => $helper->__('Status'), 'width' => '50px', 'index' => 'STATUS' ) );
		
		
		$this->addColumn ( 'PAYID', array ('header' => 'PAYID', 'width' => '100px', 'index' => 'PAYID' ) );
		$this->addColumn ( 'SUBPAYID', array ('header' => 'SUBPAYID', 'width' => '80px', 'index' => 'SUBPAYID' ) );
		
		$this->addColumn ( 'PM', array ('header' => 'PM', 'width' => '130px', 'index' => 'PM' ) );
		$this->addColumn ( 'BRAND', array ('header' => 'BRAND', 'width' => '170px', 'index' => 'BRAND' ) );
		$this->addColumn ( 'CARDNO', array ('header' => 'CARDNO', 'width' => '170px', 'index' => 'CARDNO' ) );
		
		$this->addColumn ( 'response_body', array ('header' => 'Response', 'index' => 'response_body' ) );
		$this->addColumn ( 'actions', array ('header' => 'Actions', 'renderer' => 'ogone/adminhtml_log_postsale_column_reprocess' ) );
		
		return parent::_prepareColumns ();
	}
	
	public function getRowUrl($row) {
		return '#'.$row->getId();
	}
	
}