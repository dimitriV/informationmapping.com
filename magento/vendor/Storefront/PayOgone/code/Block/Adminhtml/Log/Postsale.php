<?php

class Storefront_PayOgone_Block_Adminhtml_Log_Postsale extends Mage_Adminhtml_Block_Widget_Grid_Container {

	/**
	 * Initialize container block settings
	 */
	public function __construct () {
		$this->_controller = 'adminhtml_log_postsale';
		$this->_blockGroup = 'ogone';
		$this->_headerText = Mage::helper('ogone')->__('Ogone Postsale Log');
		parent::__construct();
		
		$this->_removeButton('add');
	}
}