<?php

class Storefront_PayOgone_Block_Adminhtml_Postsale_Column_Actions extends Mage_Adminhtml_Block_Widget_Grid_Column_Renderer_Abstract {

	public function render (Varien_Object $row) {
		/*
		$data = $row->getPostData();
		
		$stores = Mage::app()->getStores();
		$firstStore = reset($stores);
		$firstStoreId = $firstStore->getId();
		
		$html = '<form action="'.Mage::getUrl('ogone/api/postBack', array('_store' => $firstStoreId)).'" method="post">';
		
		foreach($data as $key => $value){
			$html .= '<input name="'.$key.'" value="'.$this->escapeHtml($value).'" type="hidden" />';
		}
		$html .= '<button type="submit"><span><span><span>'.$this->__('Reprocess').'</span></span></span></button>';
		$html .= '</form>';
		*/
		
		$html = '<a href="'.$this->getUrl('payogone/adminhtml_postsale/reprocess', array('id' => $row->getId())).'">'.$this->__('Reprocess').'</a>';
		$html .= ' - ';
		$html .= '<a href="'.$this->getUrl('payogone/adminhtml_postsale/export', array('id' => $row->getId())).'">'.$this->__('Export').'</a>';
		
		
		return $html;
	}
}