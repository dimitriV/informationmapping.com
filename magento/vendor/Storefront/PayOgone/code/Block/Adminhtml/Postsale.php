<?php

class Storefront_PayOgone_Block_Adminhtml_Postsale extends Mage_Adminhtml_Block_Widget_Grid_Container {

	/**
	 * Initialize container block settings
	 */
	public function __construct () {
		$this->_controller = 'adminhtml_postsale';
		$this->_blockGroup = 'ogone';
		$this->_headerText = Mage::helper('ogone')->__('Ogone Postsale Requests');
		parent::__construct();
		
		$this->_removeButton('add');
	}
}