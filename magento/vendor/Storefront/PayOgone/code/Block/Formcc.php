<?php
class Storefront_PayOgone_Block_Formcc extends Mage_Payment_Block_Form_Cc {

	protected function _construct () {
		parent::_construct();
		$this->setTemplate('payogone/form/standard.phtml');
	}

	/**
	 * Return Ogone payment Api instance
	 *
	 * @return Storefront_PayOgone_Model_Api
	 */
	protected function _getApi () {
		return Mage::getSingleton('ogone/api');
	}
}
