<?php
class Storefront_PayOgone_Block_Formideal extends Mage_Payment_Block_Form {

	protected function _construct () {
		parent::_construct();
		$this->setTemplate('payogone/form/ideal.phtml');
	}
}
