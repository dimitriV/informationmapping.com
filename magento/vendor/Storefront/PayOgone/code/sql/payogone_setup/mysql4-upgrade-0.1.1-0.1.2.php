<?php
$installer = $this;
/* @var $installer Storefront_PayOgone_Model_Resource_Setup */

$installer->startSetup();

$logPostsaleTable = $this->getTable('ogone/postsale_request');

$installer->run("
ALTER TABLE `$logPostsaleTable`
	CHANGE `ORDERID` `orderID` VARCHAR( 30 ) CHARACTER SET utf8 COLLATE utf8_general_ci NULL DEFAULT NULL,
	CHANGE `AAVCHECK` `AAVCheck` CHAR( 2 ) CHARACTER SET utf8 COLLATE utf8_general_ci NULL DEFAULT NULL ,
	CHANGE `AMOUNT` `amount` DECIMAL( 15, 2 ) NULL DEFAULT NULL ,
	CHANGE `CURRENCY` `currency` CHAR( 3 ) CHARACTER SET utf8 COLLATE utf8_general_ci NULL DEFAULT NULL ,
	CHANGE `CVCCHECK` `CVCCheck` CHAR( 2 ) CHARACTER SET utf8 COLLATE utf8_general_ci NULL DEFAULT NULL
");

$installer->endSetup();