<?php
$installer = $this;
/* @var $installer Storefront_PayOgone_Model_Resource_Setup */

$installer->startSetup();

$logPostsaleTable = $this->getTable('ogone/postsale_request');

$installer->run("

DROP TABLE IF EXISTS `$logPostsaleTable`;

CREATE TABLE `$logPostsaleTable` (
		`id` INT UNSIGNED NULL AUTO_INCREMENT ,
		`ogone_data` TEXT NULL ,
		`response_body` TEXT NULL,
		`response_code` SMALLINT UNSIGNED NULL,
		`response_redirect` VARCHAR(400) NULL,
		`created_at` TIMESTAMP DEFAULT CURRENT_TIMESTAMP, 
		
		`AAVADDRESS` CHAR(2) NULL,
		`AAVCHECK` CHAR(2) NULL,
		`AAVZIP` CHAR(2) NULL,
		`ACCEPTANCE` VARCHAR(16) NULL ,
		`ACCEPTURL` VARCHAR(200) NULL ,
		`ALIAS` VARCHAR(50) NULL ,
		`ALIASOPERATION` VARCHAR(12) NULL ,
		`ALIASUSAGE`  VARCHAR(255) NULL ,
		`AMOUNT` DECIMAL(15,2) NULL ,
		`BACKURL` VARCHAR(200) NULL ,
		`BIN` VARCHAR(6) NULL ,
		`BRAND` VARCHAR(25) NULL ,
		`CARDNO` VARCHAR(21) NULL ,
		`CCCTY` CHAR(2) NULL ,
		`CN` VARCHAR(35) NULL ,
		`COMPLUS` VARCHAR(1000) NULL ,
		`CURRENCY` CHAR(3) NULL,
		`CVCCHECK` CHAR(2) NULL,
		`DIGESTCARDNO` VARCHAR(128) NULL ,
		`ED`  CHAR(7) NULL ,
		`EMAIL` VARCHAR(50) NULL ,
		`EXCEPTIONURL` VARCHAR(200) NULL ,
		`HTML_ANSWER` TEXT NULL ,
		`IPCTY` CHAR(2) NULL ,
		`LANGUAGE` CHAR(5) NULL ,
		`NCERROR` VARCHAR(8) NULL ,
		`NCERRORPLUS` VARCHAR(255) NULL ,
		`NCSTATUS` VARCHAR(4) NULL ,
		`ORDERID` VARCHAR(30) NULL ,
		`PARAMPLUS` VARCHAR(1000) NULL ,
		`PAYID` VARCHAR(20) NULL ,
		`PAYIDSUB` VARCHAR(10) NULL ,
		`PM` VARCHAR(25) NULL ,
		`SCO_CATEGORY` CHAR(1) NULL ,
		`SCORING`  VARCHAR(4) NULL ,
		`SHASIGN`  CHAR(128) NULL ,
		`STATUS`  CHAR(2) NULL ,
		`SUBSCRIPTION_ID` VARCHAR(50) NULL ,
		`TRXDATE` DATE NULL,
		`VC` VARCHAR(3) NULL ,
		
	PRIMARY KEY (`id`), INDEX `orderid_idx` (`ORDERID`) 
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

");




/*
FIELDS TO CHECK - SUPPOSEDLY ONLY INPUT:
----------------------------------------
ACCEPTURL
EMAIL
BACKURL
EXCEPTIONURL
LANGUAGE



FIELDS NOT INCLUDED:
--------------------

DCC_COMMPERCENTAGE 	eDCC commission percentage 	- 	V 	N 	 	 
DCC_CONVAMOUNT 	eDCC converted amount 	- 	V 	N 	 	 
DCC_CONVCCY 	eDCC destination currency 	- 	V 	AN 	3 	EUR 
DCC_EXCHRATE 	eDCC exchange rate 	- 	V 	N 	 	 
DCC_EXCHRATESOURCE 	eDCC source of conversion rate 	- 	V 	N 	50 	 
DCC_EXCHRATETS 	eDCC timestamp of the conversion rate 	- 	V 	N 	 	 
DCC_INDICATOR 	eDCC indicator 	- 	V 	N 	 	 
DCC_MARGINPERCENTAGE 	eDCC margin percentage 	- 	V 	N 	 	 
DCC_VALIDHOURS 	eDCC validity (in hours) of the conversion rate, starting from the date/time defined in DCC_EXCHRATETS 	- 	V 	N 	 	 

 */

$installer->endSetup();