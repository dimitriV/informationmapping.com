<?php
$installer = $this;
/* @var $installer Storefront_PayOgone_Model_Resource_Setup */

$installer->startSetup();

$installer->run("

DROP TABLE IF EXISTS `{$this->getTable('ogone/api_debug')}`;
CREATE TABLE `{$this->getTable('ogone/api_debug')}` (
  `debug_id` int(10) unsigned NOT NULL auto_increment,
  `dir` enum('in', 'out'),
  `debug_at` timestamp NOT NULL default CURRENT_TIMESTAMP on update CURRENT_TIMESTAMP,
  `url` varchar(255),
  `data` text,
  PRIMARY KEY  (`debug_id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

");

$installer->endSetup();

$installer->addAttribute('quote_payment', 'ogone_issuer_pm', array());
$installer->addAttribute('quote_payment', 'ogone_issuer_brand', array());
$installer->addAttribute('quote_payment', 'ogone_issuer_ideal', array());

$installer->addAttribute('order_payment', 'ogone_issuer_pm', array());
$installer->addAttribute('order_payment', 'ogone_issuer_brand', array());
$installer->addAttribute('order_payment', 'ogone_issuer_ideal', array());
