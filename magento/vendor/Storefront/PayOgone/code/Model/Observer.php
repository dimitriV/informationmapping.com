<?php

class Storefront_PayOgone_Model_Observer {

	/**
	 * Convert specific attributes from Quote Payment to Order Payment
	 *
	 * @param Varien_Object $observer        	
	 *
	 */
	public function convertPayment ($observer) {
		$orderPayment = $observer->getEvent()->getOrderPayment();
		$quotePayment = $observer->getEvent()->getQuotePayment();
		
		if (strpos($quotePayment->getMethod(), 'ogone') !== false) {
			$orderPayment->setOgoneIssuerPm($quotePayment->getOgoneIssuerPm());
			$orderPayment->setOgoneIssuerBrand($quotePayment->getOgoneIssuerBrand());
			$orderPayment->setOgoneIssuerIdeal($quotePayment->getOgoneIssuerIdeal());
		}
		return $this;
	}
	
	public function rememberOrderWhenCreating($observer){
		$order = $observer->getOrder();
		Mage::register('current_order', $order, true);
	}
	
	public function processPaymentConfirmationPostsale($observer){
		$postsale = $observer->getPostsale();
		
		/* @var $processor Storefront_PayOgone_Model_Postsale_Process_Paymentconfirmation */
		$processor = Mage::getSingleton('ogone/postsale_process_paymentconfirmation');
		
		$processor->process($postsale);
	}
	
	public function processPaymentDeclinedPostsale($observer){
		$postsale = $observer->getPostsale();
	
		/* @var $processor Storefront_PayOgone_Model_Postsale_Process_Paymentdeclined */
		$processor = Mage::getSingleton('ogone/postsale_process_paymentdeclined');
	
		$processor->process($postsale);
	}
	
}