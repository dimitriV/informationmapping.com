<?php

class Storefront_PayOgone_Model_Api extends Mage_Payment_Model_Method_Abstract {

	protected $_code = 'ogone';

	protected $_formBlockType = 'ogone/form';

	protected $_infoBlockType = 'ogone/info';

	protected $_config = null;
	
	/* Ogone default features for Magento */
	protected $_isGateway = FALSE;

	protected $_canAuthorize = TRUE;

	protected $_canCapture = FALSE;

	protected $_canCapturePartial = FALSE;

	protected $_canRefund = FALSE;

	protected $_canVoid = FALSE;

	protected $_canUseInternal = FALSE;

	protected $_canUseCheckout = TRUE;

	protected $_canUseForMultishipping = FALSE;

	protected $_shouldInactivateQuote = FALSE;
	
	/* Ogone templates */
	const TEMPLATE_OGONE = 'ogone';

	const TEMPLATE_MAGENTO = 'magento';
	
	/* Ogone signature methods: pre May 2010, post May 2010 */
	const OGONE_SIGNATURE_PRE = 'pre';

	const OGONE_SIGNATURE_POST = 'post';
	
	/* Ogone order statuses */
	const OGONE_STATUS_PENDING = 'ogone_pending';

	const OGONE_STATUS_CANCEL = 'ogone_cancel';

	const OGONE_STATUS_DECLINE = 'ogone_decline';

	const OGONE_STATUS_PROCESSING = 'ogone_processing';

	const OGONE_STATUS_PROCESSED = 'ogone_processed';

	const OGONE_STATUS_AUTH = 'ogone_auth';
	
	/* Ogone response statuses */
	const OGONE_INVALID = 0;

	const OGONE_PAYMENT_INCOMPLETE = 1;

	const OGONE_AUTH_REFUSED = 2;

	const OGONE_AUTHORIZED = 5;

	const OGONE_PAYMENT_REQUESTED = 9;

	const OGONE_IDENT_WAITING = 46;

	const OGONE_AUTH_WAITING = 51;

	const OGONE_AUTH_UNKNOWN = 52;

	const OGONE_PAYMENT_UNKNOWN = 91;

	const OGONE_PAYMENT_UNCERTAIN = 92;

	const OGONE_TECH_PROBLEM = 93;
	
	/* Request Timeout in seconds, must be between 30 - 90 */
	const OGONEDIRECT_REQUEST_TIMEOUT = 60;
	
	/* OgoneDirect response statuses */
	const OGONEDIRECT_AUTHORIZED = 5;

	const OGONEDIRECT_PAYMENT_REQUESTED = 9;

	const OGONEDIRECT_INVALID = 0;

	const OGONEDIRECT_AUTH_REFUSED = 2;

	const OGONEDIRECT_IDENT_WAITING = 46;

	const OGONEDIRECT_AUTH_WAITING = 51;

	const OGONEDIRECT_AUTH_UNKNOWN = 52;

	const OGONEDIRECT_PAYMENT_UNKNOWN = 91;
	
	/* Electronic Commerce Indicator */
	const OGONEDIRECT_ECI_SWIPED = 0;

	const OGONEDIRECT_ECI_MANUAL_MOTO = 1;

	const OGONEDIRECT_ECI_RECURRING_MOTO = 2;

	const OGONEDIRECT_ECI_INSTALLMENT = 3;

	const OGONEDIRECT_ECI_MANUAL = 4;

	const OGONEDIRECT_ECI_ECOMMERCE = 7;

	const OGONEDIRECT_ECI_RECURRING = 9;
	
	/* Ogone payment actions */
	const OGONE_AUTHORIZE_ACTION = 'RES';

	const OGONE_AUTHORIZE_CAPTURE_ACTION = 'SAL';
	
	/* Ogone hash algorithms */
	const OGONE_SHA1 = 'sha1';

	const OGONE_SHA256 = 'sha256';

	const OGONE_SHA512 = 'sha512';
	
	/* Ogone 3-D secure mode */
	const OGONE_MAINW = 'MAINW';

	const OGONE_POPUP = 'POPUP';

	const OGONE_POPIX = 'POPIX';

	/**
	 * Return Ogone API instance
	 *
	 * @return Storefront_PayOgone_Model_Api
	 */
	public function __construct () {
		$this->_config = Mage::getSingleton('ogone/config');
		return $this;
	}

	/**
	 * Return ogone configuration instance
	 *
	 * @return Storefront_PayOgone_Model_Config
	 */
	public function getConfig () {
		return $this->_config;
	}

	/**
	 * Return debug flag by storeConfig
	 *
	 * @param
	 *        	int storeId
	 * @return bool
	 */
	public function getDebug ($storeId = null) {
		return true;
		// return $this->getConfig()->getConfigData('ogone/payment/debug_flag', $storeId);
	}

	/**
	 * Flag to prevent automatic invoice creation
	 *
	 * @return bool
	 */
	public function isInitializeNeeded () {
		return TRUE;
	}

	/**
	 * s
	 * Ogone redirect URL for placement form
	 *
	 * @return string
	 */
	public function getOrderPlaceRedirectUrl () {
		if ($this->getConfig()->getDirectLinkEnabledAndCompatible() == TRUE) {
			return Mage::getUrl('ogone/api/directLink', array(
					'_secure' => TRUE
			));
		} else {
			
			// This was registered just before this code started...
			
			$order = Mage::registry('current_order'); 
			$orderId = $order->getId();
			
			return Mage::getUrl('ogone/api/placement', array(
					'_secure' => TRUE,
					'order_id' => $orderId
			));
		}
	}

	/**
	 * Ogone payment action configuration (Direct Sale, Authorize)
	 *
	 * @return string
	 */
	public function getPaymentAction () {
		return $this->getConfig()->getConfigData('ogone/payment/payment_action');
	}

	/**
	 * Return Signature Method
	 *
	 * @return Storefront_PayOgone_Model_Api
	 */
	public function getSignatureMethod () {
		return $this->getConfig()->getConfigData('ogone/settings/signature_method');
	}

	/**
	 * Function to get the real IP address from visitors
	 *
	 * @return string Ip Address, if available
	 */
	public function getRealIpAddr () {
		$ip = '';
		if (! empty($_SERVER['HTTP_CLIENT_IP'])) {
			$ip = $_SERVER['HTTP_CLIENT_IP'];
		} elseif (! empty($_SERVER['HTTP_X_FORWARDED_FOR'])) {
			$ip = $_SERVER['HTTP_X_FORWARDED_FOR'];
		} else {
			$ip = $_SERVER['REMOTE_ADDR'];
		}
		return $ip;
	}
	
	/*
	 * Decide currency code type
	 */
	public function _getCurrencyCode () {
		if ($this->getConfig()->getBaseCurrency()) {
			return Mage::app()->getStore()->getBaseCurrencyCode();
		} else {
			return Mage::app()->getStore()->getCurrentCurrencyCode();
		}
	}
	
	/*
	 * Decide grand total type @return Mage_Sales_Model_Order
	 */
	public function _getGrandTotal ($order) {
		if ($this->getConfig()->getBaseCurrency()) {
			return $order->getBaseGrandTotal();
		} else {
			return $order->getGrandTotal();
		}
	}

	protected function _getRedirectLocationFromHeader (Mage_Core_Controller_Response_Http $response) {
		$r = null;
		foreach ($response->getHeaders() as $header) {
			if ($header['name'] === 'Location' && ($r === null || $header['replace'] === true)) {
				$r = $header['value'];
			}
		}
		return $r;
	}

	protected function _log ($msg) {
		Mage::log($msg, null, 'payogone.log');
	}

	/**
	 * Rrepare parameters array to send it to the Ogone gateway page via POST
	 *
	 * @param
	 *        	Mage_Sales_Model_Order
	 * @return array
	 */
	public function getFormFields ($order) {
		if (empty($order)) {
			if (! ($order = $this->getOrder())) {
				return array();
			}
		}
		/* @var $order Mage_Sales_Model_Order */
		
		$billingAddress = $order->getBillingAddress();
		$paymentAction = $this->_getOgonePaymentOperation();
		
		$formFields = array();
		if ($paymentAction) {
			$formFields['operation'] = $paymentAction;
		}
		
		$formFields['amount'] = round($this->_getGrandTotal($order) * 100);
		$formFields['currency'] = $this->_getCurrencyCode();
		$formFields['orderID'] = $this->getConfig()->generateOrderId($order);
		$formFields['PSPID'] = $this->getConfig()->getPSPID();
		$formFields['USERID'] = $this->getConfig()->getUSERID();
		$formFields['language'] = Mage::app()->getLocale()->getLocaleCode();
		$formFields['CN'] = $billingAddress->getFirstname() . ' ' . $billingAddress->getLastname();
		$formFields['EMAIL'] = $order->getCustomerEmail();
		
		// address fields
		$formFields['ownerZIP'] = $billingAddress->getPostcode();
		$formFields['ownercty'] = $billingAddress->getCountry();
		$formFields['ownertown'] = $billingAddress->getCity();
		$formFields['COM'] = str_replace('"', '', $this->_getOrderDescription($order));
		$formFields['ownertelno'] = $billingAddress->getTelephone();
		$formFields['owneraddress'] = str_replace("\n", ' ', $billingAddress->getStreet(- 1));
		
		// return urls
		$storeId = $order->getStoreId();
		$formFields['homeurl'] = $this->getConfig()->getHomeUrl($storeId);
		$formFields['catalogurl'] = $this->getConfig()->getHomeUrl($storeId);
		$formFields['accepturl'] = $this->getConfig()->getAcceptUrl($storeId, $order);
		$formFields['declineurl'] = $this->getConfig()->getDeclineUrl($storeId);
		$formFields['exceptionurl'] = $this->getConfig()->getExceptionUrl($storeId);
		$formFields['cancelurl'] = $this->getConfig()->getCancelUrl($storeId);
		
		// 3-D Secure
		if ($this->getConfig()->getConfigData('ogone/security/flag3d') == 1) {
			$formFields['FLAG3D'] = 'Y';
			$formFields['WIN3DS'] = $this->getConfig()->getConfigData('ogone/security/win3ds');
			$formFields['HTTP_ACCEPT'] = $_SERVER['HTTP_ACCEPT'];
			$formFields['HTTP_USER_AGENT'] = $_SERVER['HTTP_USER_AGENT'];
		}
		
		// Required for Fraud Detection Module
		if ($this->getConfig()->getConfigData('ogone/security/fraud_detection') == 1) {
			$formFields['REMOTE_ADDR'] = $this->getRealIpAddr();
		}
		
		// Payment selected by user
		// including fix for popular OneStepCheckout
		$method = $order->getPayment()->getMethod();
		$pm = $this->getConfig()->getPm($method);
		$formFields['PM'] = $pm['PM'];
		$formFields['BRAND'] = $pm['BRAND'];
		
		// iDEAL Issuer ID
		if ($formFields['BRAND'] == 'iDEAL') {
			$formFields['ISSUERID'] = $order->getPayment()->getOgoneIssuerIdeal();
		}
		
		// ogone template
		if ($this->getConfig()->getConfigData('ogone/template/template') == 'ogone') {
			$formFields['TP'] = '';
		} else {
			$formFields['TP'] = $this->getConfig()->getPaymentTemplate();
		}
		
		// DirectLink
		if ($this->getConfig()->getDirectLinkEnabledAndCompatible() == TRUE) {
			
			// translate UTF-8 to ISO (DirectLink does not support UTF-8)
			$formFields['ownertown'] = $this->_translate($formFields['ownertown']);
			$formFields['owneraddress'] = $this->_translate($formFields['owneraddress']);
			$formFields['COM'] = $this->_translate($formFields['COM']);
			
			$formFields['USERID'] = $this->getConfig()->getApiUser();
			$formFields['PSWD'] = $this->getConfig()->getApiPswd();
			$formFields['RTIMEOUT'] = Storefront_PayOgone_Model_Api::OGONEDIRECT_REQUEST_TIMEOUT;
			$formFields['ECI'] = $this->getConfig()->getEcIndicator();
			
			// credit card data
			$ccType = $order->getPayment()->getCcType();
			if ($ccType) {
				$info = $this->getInfo();
				$month = $this->getConfig()->getCcExpMonthDec();
				$mm = (string) $month < 10 ? '0' . $month : $month;
				$yy = substr((string) $this->getConfig()->getCcExpYearDec(), 2, 2);
				
				// Payment selected by user
				$formFields['CN'] = $this->_translate($order->getPayment()->getCcOwner());
				$formFields['CARDNO'] = $this->getConfig()->getCcNumberDec();
				$formFields['ED'] = $mm . '/' . $yy;
				$formFields['CVC'] = $this->getConfig()->getCcCidDec();
				
				// Ecom
				$formFields['Ecom_Payment_Card_Verification'] = $this->getConfig()->getCcCidDec();
			}
		}
		
		// get correct SHA-1 signature
// 		if ($this->getConfig()->getDirectLinkEnabledAndCompatible() == TRUE) {
// 			$secretCode = $this->getConfig()->getDirectLinkShaInCode();
// 		} else {
			$secretCode = $this->getConfig()->getShaInCode();
// 		}
		
		$formFieldsObj = new Varien_Object($formFields);
		
		$eventData = array();
		$eventData['form_fields'] = $formFieldsObj;
		$eventData['order'] = $order;
				
		Mage::dispatchEvent('ogone_form_fields_prehash', $eventData);
		
		$formFields = $formFieldsObj->getData();

		/* @var $ogoneHelper Storefront_PayOgone_Helper_Data */
		$ogoneHelper = Mage::helper('ogone');
		
		// must come last for logic above
		$shaSecret = $this->getConfig()->getShaInCode($storeId);
		$formFields['SHASign'] = $ogoneHelper->generateShaSignature($formFields, $shaSecret);
		
		return $formFields;
	}

	/**
	 * to translate UTF 8 to ISO 8859-1
	 * Ogone system is only compatible with iso-8859-1 and does not (yet) fully support the utf-8
	 */
	protected function _translate ($text) {
		return htmlentities(iconv("UTF-8", "ISO-8859-1//TRANSLIT", $text));
	}

	/**
	 * Return Ogone payment mode value
	 *
	 * @param
	 *        	string
	 * @return string
	 */
	protected function _getOgonePaymentOperation () {
		$value = $this->getPaymentAction();
		if ($value == Mage_Payment_Model_Method_Abstract::ACTION_AUTHORIZE) {
			$value = Storefront_PayOgone_Model_Api::OGONE_AUTHORIZE_ACTION;
		} elseif ($value == Mage_Payment_Model_Method_Abstract::ACTION_AUTHORIZE_CAPTURE) {
			$value = Storefront_PayOgone_Model_Api::OGONE_AUTHORIZE_CAPTURE_ACTION;
		}
		return $value;
	}

	/**
	 * Return order description
	 *
	 * @param
	 *        	Mage_Sales_Model_Order
	 * @return string
	 */
	protected function _getOrderDescription ($order) {
		$invoiceDesc = '';
		$lengs = 0;
		foreach ($order->getAllItems() as $item) {
			if ($item->getParentItem()) {
				continue;
			}
			// COM filed can only handle max 100
			if (Mage::helper('core/string')->strlen($invoiceDesc . $item->getName()) > 100) {
				break;
			}
			$invoiceDesc .= $item->getName() . ', ';
		}
		// form fields can't have double quotes "
		return str_replace('"', "'", Mage::helper('core/string')->substr($invoiceDesc, 0, - 2));
	}
}