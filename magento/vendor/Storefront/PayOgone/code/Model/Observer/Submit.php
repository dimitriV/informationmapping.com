<?php

class Storefront_PayOgone_Model_Observer_Submit {
	/*
	 * Keep cart after placing order
	 */
	public function sales_model_service_quote_submit_after (Varien_Event_Observer $observer) {
		$method = $observer->getEvent()->getOrder()->getPayment()->getMethod();
		
		// check if chosen method is a member of 'ogone_' prefix
		if (strpos($method, 'ogone_') !== FALSE) {
			$observer->getQuote()->setIsActive(TRUE);
		}
	}
}