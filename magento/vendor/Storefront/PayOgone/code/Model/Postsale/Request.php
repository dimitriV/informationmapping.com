<?php

class Storefront_PayOgone_Model_Postsale_Request extends Mage_Core_Model_Abstract {

	protected $_processed = false;

	protected $_errors = array();

	protected function _construct () {
		$this->_init('ogone/postsale_request');
	}

	public function initFromRequest () {
		$ogoneData = array_merge($_GET, $_POST);
		$this->setOgoneData($ogoneData);
	}

	public function getOgoneData ($key = null) {
		$r = unserialize($this->getData('ogone_data'));
		
		if ($key === null) {
			return $r;
		} else {
			if (isset($r[$key])) {
				return $r[$key];
			} else {
				return null;
			}
		}
	}

	public function setOgoneData ($data) {
		$this->setData('ogone_data', serialize($data));
	}

	public function isProcessed () {
		return $this->_processed;
	}

	public function setProcessed ($flag) {
		$this->_processed = (bool) $flag;
	}

	public function getStatus () {
		return $this->getOgoneData('STATUS');
	}

	public function getOrderId () {
		return $this->getOgoneData('orderID');
	}

	public function getPayId () {
		return $this->getOgoneData('PAYID');
	}

	public function getBrand () {
		return $this->getOgoneData('BRAND');
	}

	public function addError ($message) {
		$this->_errors[] = $message;
	}

	public function getErrors(){
		return $this->_errors;
	}
	
	public function hasErrors () {
		return (count($this->_errors) > 0);
	}

	public function isValid () {
		$params = $this->getOgoneData();
		
		if (! isset($params['SHASIGN'])) {
			return false;
		}
		
		$secureKey = $this->_getApi()->getConfig()->getShaOutCode();
		
		/* @var $helper Storefront_PayOgone_Helper_Data */
		$helper = Mage::helper('ogone');
		
		$secureSet = $helper->generateToBeEncryptedString($params, $secureKey);
		
		// if ($this->_getApi()->getDebug()) {
		// $debug = Mage::getModel('ogone/api_debug')->setDir('in')->setUrl($this->getRequest()->getPathInfo())->setData('data', http_build_query($this->_getParams()))->save();
		// }
		
		
		$myHash = $helper->generateShaSignature($params, $secureKey);
		
		if (Mage::getIsDeveloperMode()) {
			$debugShaMessage = 'PARAMS = ' . print_r($params, true) . "\n\nKEY = " . $secureKey . "\n\nUNENCR = " . $secureSet . "\n\nSHA TO MATCH = " . $params['SHASIGN'] . "\nMY HASH = " . $myHash;
			// mail('wouter.samaey@storefront.be','VALIDATE OGONE DATA - TEST SHA OUT', $debugShaMessage);
			Mage::log($debugShaMessage, null, 'payogone-sha-validation.log');
		}
		
		if ($myHash === $params['SHASIGN']) {
			// $this->_getCheckout()->addError($this->__('The payment request SHA signature does not match. Please check the SHA IN and OUT codes.'));
			return FALSE;
		}
		
		return TRUE;
	}

	public function run () {
		Mage::dispatchEvent('payogone_postsale_process', array(
				'postsale' => $this
		));
		
		if(!$this->isProcessed()){
			$this->addError('Not processed');
		}
	}
	
	public function applyToResponse(Mage_Core_Controller_Response_Http $response){
		if($this->hasErrors()){
			$errors = implode("\n", $this->getErrors());
			$this->setResponseCode(500);
			$this->setResponseBody($errors);
		}else{
			$this->setResponseCode(200);
			$this->setResponseBody('OK');
		}
		
		if($this->getResponseCode() == 200){
			// Leave default
		}else{
			$response->setHeader("Status", "500 Internal Server Error");
		}
		
		$response->setBody($this->getResponseBody());
	}

	public function isAuthorizeOkay(){
		$status = $this->getStatus();
		switch ($status) {
			case Storefront_PayOgone_Model_Api::OGONE_AUTHORIZED:
				return true;
		}
		return false;
	}
	
	public function isPaymentOkay () {
		$status = $this->getStatus();
		switch ($status) {
			case Storefront_PayOgone_Model_Api::OGONE_PAYMENT_REQUESTED:
				return true;
		}
		return false;
	}

	public function isPaymentDeclined () {
		$status = $this->getStatus();
		switch ($status) {
			case Storefront_PayOgone_Model_Api::OGONE_INVALID:
			case Storefront_PayOgone_Model_Api::OGONE_PAYMENT_INCOMPLETE:
			case Storefront_PayOgone_Model_Api::OGONE_TECH_PROBLEM:
			case Storefront_PayOgone_Model_Api::OGONE_AUTH_REFUSED:
				return true;
		}
		return false;
	}

	/**
	 * Return Ogone payment API instance
	 *
	 * @return Storefront_PayOgone_Model_Api
	 */
	protected function _getApi () {
		return Mage::getSingleton('ogone/api');
	}
	
	public function getExportString(){
		$data = $this->getData();
		
		unset($data['id']);
		
		return base64_encode(serialize($data));
	}
	
	public function restoreFromExport($string){
		$data = unserialize(base64_decode($string));
		$this->setData($data);
	}
}