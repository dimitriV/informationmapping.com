<?php

class Storefront_PayOgone_Model_Postsale_Process_Paymentconfirmation extends Storefront_PayOgone_Model_Postsale_Process_Abstract{

	public function process (Storefront_PayOgone_Model_Postsale_Request $postsale) {
		$status = $postsale->getStatus();
		
		$paymentOk = $postsale->isPaymentOkay();
		
		if ($paymentOk) {
			$order = $this->_getOrder($postsale);
			
			if ($order && $order->getId()) {
				
				if ($order->getInvoiceCollection()->getSize() > 0) {
					// $this->_updateOgoneStatusHistory();
					
					$postsale->addError('Order already invoiced');
				}
				
				$params = $postsale->getOgoneData();
				
				// Credit card info
				$this->_prepareCCInfo($postsale);
				
				// Generic payment info
				$payment = $order->getPayment();
				$payment->setTransactionId($postsale->getPayId());
				$payment->setLastTransId($postsale->getPayId());
				
				try {
					if ($this->_getApi()->getPaymentAction() == Mage_Payment_Model_Method_Abstract::ACTION_AUTHORIZE_CAPTURE) {
						$this->_processDirectSale($postsale);
					} else {
						// $this->_processAuthorize();
						Mage::throwException('Authorize is not supported by this Ogone module');
					}
					
					// Legacy event - could be removed and replaced with "payogone_postsale_process"
					Mage::dispatchEvent('payogone_postsale_order_paid', array(
							'order' => $order,
							'payment_action' => 'direct_sale',
							'params' => $postsale->getData()
					));
					
					$postsale->setProcessed(true);
					
				} catch (Exception $e) {
					$postsale->addError(Mage::helper('ogone')->__('Order could not be saved.'));
					Mage::logException($e);
				}
			}
		}
	}


	/**
	 * Ogone credit card info processing
	 *
	 * @param Mage_Sales_Model_Order $order        	
	 * @param array $ccInfo        	
	 *
	 * @return Storefront_PayOgone_ApiController
	 */
	protected function _prepareCCInfo (Storefront_PayOgone_Model_Postsale_Request $postsale) {
		$order = $this->_getOrder($postsale);
		$ccInfo = $postsale->getData();
		
		if (isset($ccInfo['CARDNO']) && isset($ccInfo['ED'])) {
			$order->getPayment()->setCcOwner($ccInfo['CN']);
			$order->getPayment()->setCcNumberEnc($ccInfo['CARDNO']);
			$order->getPayment()->setCcLast4(substr($ccInfo['CARDNO'], - 4));
			$order->getPayment()->setCcExpMonth(substr($ccInfo['ED'], 0, 2));
			$order->getPayment()->setCcExpYear(substr($ccInfo['ED'], 2, 2));
		}
	}

	/**
	 * Ogone payment mode: Direct Sale
	 * We create an invoice if the order status changes 'pending' to 'processeed'
	 */
	protected function _processDirectSale (Storefront_PayOgone_Model_Postsale_Request $postsale) {
		$order = $this->_getOrder($postsale);
		$status = $postsale->getStatus();
		
		try {
			
			// order status = ogone_pending
			// order state = pending_payment
			
			// 1. Ogone status: Waiting for auth
			if ($status == Storefront_PayOgone_Model_Api::OGONE_AUTH_WAITING) {
				$order->setState(Mage_Sales_Model_Order::STATE_PROCESSING, Storefront_PayOgone_Model_Api::OGONE_STATUS_AUTH, Mage::helper('ogone')->__('Waiting for authorization by Ogone') . ' (PAYID: ' . $postsale->getPayId() . ')');
				$order->save();
			}			

			// 2. Order status: Pending
			elseif ($order->getState() == Mage_Sales_Model_Order::STATE_PENDING_PAYMENT) {

				$comment = Mage::helper('ogone')->__('Processed by Ogone') . ' (PAYID: ' . $postsale->getPayId() . ')' . '<br />' . Mage::helper('ogone')->__('Paid with %s', $postsale->getBrand());
				
				if ($order->getInvoiceCollection()->getSize() == 0) {
					
					if ($order->getStatus() != Mage_Sales_Model_Order::STATE_PENDING_PAYMENT) {
						
						// Order is paid. Set the order status
						$order->setState(Mage_Sales_Model_Order::STATE_PROCESSING, Storefront_PayOgone_Model_Api::OGONE_STATUS_PROCESSED, $comment);
					}

					if ($this->_getApi()->getConfig()->getInvoiceCreate()) {
						
						$invoice = $order->prepareInvoice();
						$invoice->register();
						$invoice->setState(Mage_Sales_Model_Order_Invoice::STATE_PAID);
						$invoice->getOrder()->setIsInProcess(TRUE);
						
						Mage::getModel('core/resource_transaction')->addObject($invoice)->addObject($invoice->getOrder())->save();
						
						// Send invoice email
// 						if (! $invoice->getOrder()->getEmailSent() && $this->_getApi()->getConfig()->getInvoiceEmail()) {
							
							Mage::log('Sending invoice after ogone postsale', null, 'ogone.log');
							
							$invoice->sendEmail()->setEmailSent(TRUE);
// 						}

					} else {

						$order->setIsInProcess(TRUE);
						Mage::getModel('core/resource_transaction')->addObject($order)->save();
					}
					
					// Send order email
					if (! $order->getEmailSent() && $this->_getApi()->getConfig()->getOrderEmail($order->getStoreId())) {
						$order->sendNewOrderEmail();
					}
				}
			} else {
				$order->save();
			}
			// $this->_getCheckout()->setLastSuccessQuoteId($order->getQuoteId());
			
			// if (is_object($this->_request)) {
			// $this->_redirect('checkout/onepage/success', array(
			// '_store' => $order->getStoreId()
			// ));
			// } else {
			// header("Location: " . Mage::getUrl('checkout/onepage/success', array(
			// '_store' => $order->getStoreId()
			// )));
			// }
			
			
			
		} catch (Exception $e) {
			$postsale->addError(Mage::helper('ogone')->__('Test Order could not be saved.'));
			
			Mage::logException($e);
		}
	}
	
}