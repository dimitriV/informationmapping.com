<?php

abstract class Storefront_PayOgone_Model_Postsale_Process_Abstract{

	protected $_order;

	abstract public function process (Storefront_PayOgone_Model_Postsale_Request $postsale);
	
	/**
	 * Return Ogone payment API instance
	 * 
	 * @return Storefront_PayOgone_Model_Api
	 */
	protected function _getApi () {
		return Mage::getSingleton('ogone/api');
	}

	/**
	 * Return order instance
	 * 
	 * @return Mage_Sales_Model_Order
	 */
	protected function _getOrder (Storefront_PayOgone_Model_Postsale_Request $postsale) {
		if ($this->_order === null) {
			$ogoneOrderId = $postsale->getOrderId();
			$this->_order = $this->_getApi()->getConfig()->getMagentoOrderIncrementFromOgoneOrderId($ogoneOrderId);
		}
		return $this->_order;
	}


	
	
}