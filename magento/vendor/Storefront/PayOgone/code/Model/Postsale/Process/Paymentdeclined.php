<?php

class Storefront_PayOgone_Model_Postsale_Process_Paymentdeclined extends Storefront_PayOgone_Model_Postsale_Process_Abstract{

	public function process (Storefront_PayOgone_Model_Postsale_Request $postsale) {
		$status = $postsale->getStatus();
		
		if ($postsale->isPaymentDeclined()) {
			$order = $this->_getOrder();
			
			if ($order && $order->getId()) {
				
				$status = Storefront_PayOgone_Model_Api::OGONE_STATUS_DECLINE;
				$comment = Mage::helper('ogone')->__('Payment declined by Ogone.');
				//$this->_getCheckout()->addError(Mage::helper('ogone')->__('Transaction declined by Ogone.'));
				
				try {
					$order->cancel();
					$order->setState(Mage_Sales_Model_Order::STATE_CANCELED, $status, $comment . ' (PAYID: ' . $postsale->getPayId() . ')');
					$order->save();
					
					$postsale->setProcessed(true);
					
				} catch (Exception $e) {
					$postsale->addError(Mage::helper('ogone')->__('Order can not be canceled.'));
				}
				
			}
		}
	}

	
}