<?php
class Storefront_PayOgone_Model_Api_Ideal extends Storefront_PayOgone_Model_Api {

	protected $_code = 'ogone_ideal';

	protected $_formBlockType = 'ogone/formideal';
	
	/* Ogone specific features for Magento */
	protected $_canRefund = FALSE;

	protected $_canUseForMultishipping = FALSE;

	protected $_canSaveCc = FALSE;
}