<?php
class Storefront_PayOgone_Model_Api_Intersolve extends Storefront_PayOgone_Model_Api {

	protected $_code = 'ogone_intersolve';

	protected $_formBlockType = 'ogone/form';
	
	/* Ogone specific features for Magento */
	protected $_canRefund = FALSE;

	protected $_canUseForMultishipping = FALSE;

	protected $_canSaveCc = FALSE;
}