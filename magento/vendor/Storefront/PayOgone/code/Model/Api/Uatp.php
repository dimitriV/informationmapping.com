<?php
class Storefront_PayOgone_Model_Api_Uatp extends Storefront_PayOgone_Model_Api {

	protected $_code = 'ogone_uatp';

	protected $_formBlockType = 'ogone/formcc';
	
	/* Ogone specific features for Magento */
	protected $_canRefund = FALSE;

	protected $_canUseForMultishipping = FALSE;

	protected $_canSaveCc = FALSE;

	/**
	 * Assign data to info model instance
	 *
	 * @param mixed $data        	
	 * @return Mage_Payment_Model_Info
	 */
	public function assignData ($data) {
		if (! ($data instanceof Varien_Object)) {
			$data = new Varien_Object($data);
		}
		
		$info = $this->getInfoInstance();
		$info->setCcOwner($data->getCcOwner())->setCcType($data->getCcOwner())->setCcNumber($data->getCcNumber())->setCcCid($data->getCcCid())->setCcExpMonth($data->getCcExpMonth())->setCcExpYear($data->getCcExpYear())->setOgoneIssuerPm($data->getOgoneIssuerPm())->setOgoneIssuerBrand($data->getOgoneIssuerBrand());
		
		return $this;
	}

	/**
	 * Prepare info instance for save
	 *
	 * @return Mage_Payment_Model_Abstract
	 */
	public function prepareSave () {
		$info = $this->getInfoInstance();
		
		Mage::getSingleton('core/session')->setCcNumberEnc($info->encrypt($info->getCcNumber()));
		Mage::getSingleton('core/session')->setCcCidEnc($info->encrypt($info->getCcCid()));
		Mage::getSingleton('core/session')->setCcExpMonthEnc($info->encrypt($info->getCcExpMonth()));
		Mage::getSingleton('core/session')->setCcExpYearEnc($info->encrypt($info->getCcExpYear()));
		
		$info->setCcNumber(null)->setCcCid(null);
		
		return $this;
	}
}