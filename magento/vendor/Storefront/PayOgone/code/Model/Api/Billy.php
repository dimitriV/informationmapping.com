<?php
/**
 * 
 * 
 *
 * @extension   Ogone
 * @type        Payment method
 *
 * NOTICE OF LICENSE
 *
 * This source file is subject to the Open Software License (OSL 3.0)
 * that is bundled with this package in the file LICENSE.txt.
 * It is also available through the world-wide-web at this URL:
 * http://opensource.org/licenses/osl-3.0.php
 * If you did not receive a copy of the license and are unable to
 * obtain it through the world-wide-web, please send an email
 * to license@magentocommerce.com so we can send you a copy immediately.
 *
 * 
 * @package     Storefront_PayOgone
 * 
 * 
 */

class Storefront_PayOgone_Model_Api_Billy extends Storefront_PayOgone_Model_Api
{
  protected $_code          = 'ogone_billy';
  protected $_formBlockType = 'ogone/form';

  /* Ogone specific features for Magento */
  protected $_canRefund                  = FALSE;
  protected $_canUseForMultishipping     = FALSE;
  protected $_canSaveCc                  = FALSE;
}
