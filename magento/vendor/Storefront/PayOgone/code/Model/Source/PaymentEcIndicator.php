<?php
class Storefront_PayOgone_Model_Source_PaymentEcIndicator {

	public function toOptionArray () {
		return array(
				array(
						'value' => Storefront_PayOgone_Model_Api::OGONEDIRECT_ECI_SWIPED,
						'label' => Mage::helper('ogone')->__('Swiped')
				),
				array(
						'value' => Storefront_PayOgone_Model_Api::OGONEDIRECT_ECI_MANUAL_MOTO,
						'label' => Mage::helper('ogone')->__('Manually keyed (MOTO) (card not present)')
				),
				array(
						'value' => Storefront_PayOgone_Model_Api::OGONEDIRECT_ECI_RECURRING_MOTO,
						'label' => Mage::helper('ogone')->__('Recurring (from MOTO)')
				),
				array(
						'value' => Storefront_PayOgone_Model_Api::OGONEDIRECT_ECI_INSTALLMENT,
						'label' => Mage::helper('ogone')->__('Installment payments')
				),
				array(
						'value' => Storefront_PayOgone_Model_Api::OGONEDIRECT_ECI_MANUAL,
						'label' => Mage::helper('ogone')->__('Manually keyed, card present')
				),
				array(
						'value' => Storefront_PayOgone_Model_Api::OGONEDIRECT_ECI_ECOMMERCE,
						'label' => Mage::helper('ogone')->__('E-commerce with SSL encryption')
				),
				array(
						'value' => Storefront_PayOgone_Model_Api::OGONEDIRECT_ECI_RECURRING,
						'label' => Mage::helper('ogone')->__('Recurring (from e-commerce)')
				)
		);
	}
}