<?php
class Storefront_PayOgone_Model_Source_PaymentAction {

	public function toOptionArray () {
		return array(
				array(
						'value' => '',
						'label' => Mage::helper('ogone')->__('Ogone default operation')
				),
				array(
						'value' => Mage_Payment_Model_Method_Abstract::ACTION_AUTHORIZE,
						'label' => Mage::helper('ogone')->__('Authorization')
				),
				array(
						'value' => Mage_Payment_Model_Method_Abstract::ACTION_AUTHORIZE_CAPTURE,
						'label' => Mage::helper('ogone')->__('Direct Sale')
				)
		);
	}
}