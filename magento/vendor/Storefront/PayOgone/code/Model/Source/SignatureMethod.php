<?php
class Storefront_PayOgone_Model_Source_SignatureMethod {

	public function toOptionArray () {
		return array(
				array(
						'value' => Storefront_PayOgone_Model_Api::OGONE_SIGNATURE_PRE,
						'label' => Mage::helper('ogone')->__('Old style (accounts before May 2010)')
				),
				array(
						'value' => Storefront_PayOgone_Model_Api::OGONE_SIGNATURE_POST,
						'label' => Mage::helper('ogone')->__('New style (accounts after May 2010)')
				)
		);
	}
}
