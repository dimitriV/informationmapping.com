<?php
class Storefront_PayOgone_Model_Source_Template {

	public function toOptionArray () {
		return array(
				array(
						'value' => Storefront_PayOgone_Model_Api::TEMPLATE_OGONE,
						'label' => Mage::helper('ogone')->__('Ogone')
				),
				array(
						'value' => Storefront_PayOgone_Model_Api::TEMPLATE_MAGENTO,
						'label' => Mage::helper('ogone')->__('Magento')
				)
		);
	}
}
