<?php
class Storefront_PayOgone_Model_Source_Environment
{
  public function toOptionArray()
  {
    return array(
      array('value' => 'test', 'label' => Mage::helper('ogone')->__('Test')),
      array('value' => 'prod', 'label' => Mage::helper('ogone')->__('Production')),
    );
  }
}
