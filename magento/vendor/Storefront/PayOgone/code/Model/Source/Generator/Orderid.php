<?php
class Storefront_PayOgone_Model_Source_Generator_Orderid {

	public function toOptionArray () {
		$r = array();
		
		$root = Mage::getConfig ()->getNode ( Storefront_PayOgone_Helper_Data::XML_PATH_OGONE_GENERATORS_ORDERID );
		$types = array ();
			
		foreach ( $root->children () as $generatorCode => $generatorNode ) {
			
			// Convert to string to get XML value
			$groupedName = ''.$generatorNode;
			$model = Mage::getSingleton($groupedName);
			
			if($model instanceof Storefront_PayOgone_Model_Generator_Orderid_Abstract && $model->isAvailable()){
				$r[$generatorCode] = $model->getName();
			}
		}
		
		return $r;
	}
}