<?php
class Storefront_PayOgone_Model_Source_PaymentSecure {

	public function toOptionArray () {
		return array(
				array(
						'value' => Storefront_PayOgone_Model_Api::OGONE_MAINW,
						'label' => Mage::helper('ogone')->__('Main window (recommended)')
				),
				array(
						'value' => Storefront_PayOgone_Model_Api::OGONE_POPUP,
						'label' => Mage::helper('ogone')->__('Popup and return to main window')
				),
				array(
						'value' => Storefront_PayOgone_Model_Api::OGONE_POPIX,
						'label' => Mage::helper('ogone')->__('Popup and stay in popup')
				)
		);
	}
}