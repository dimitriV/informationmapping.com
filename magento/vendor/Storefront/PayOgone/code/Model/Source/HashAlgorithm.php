<?php

class Storefront_PayOgone_Model_Source_HashAlgorithm {

	public function toOptionArray () {
		return array(
				array(
						'value' => Storefront_PayOgone_Model_Api::OGONE_SHA1,
						'label' => Mage::helper('ogone')->__('SHA-1')
				),
				array(
						'value' => Storefront_PayOgone_Model_Api::OGONE_SHA256,
						'label' => Mage::helper('ogone')->__('SHA-256')
				),
				array(
						'value' => Storefront_PayOgone_Model_Api::OGONE_SHA512,
						'label' => Mage::helper('ogone')->__('SHA-512')
				)
		);
	}
}
