<?php
class Storefront_PayOgone_Model_Mysql4_Log_Postsale_Request_Collection extends Mage_Core_Model_Mysql4_Collection_Abstract {
	protected function _construct() {
		$this->_init ( 'ogone/log_postsale_request' );
	}
}