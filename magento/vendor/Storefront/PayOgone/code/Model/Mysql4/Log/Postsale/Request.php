<?php
class Storefront_PayOgone_Model_Mysql4_Log_Postsale_Request extends Mage_Core_Model_Mysql4_Abstract {

	protected function _construct () {
		$this->_init('ogone/log_postsale_request', 'id');
	}
}