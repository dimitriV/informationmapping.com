<?php
class Storefront_PayOgone_Model_Mysql4_Setup extends Mage_Sales_Model_Mysql4_Setup{
	
	public function setupOgoneStati(){
		$this->_setupOgoneStatus('ogone_auth','Ogone Waiting for Authorization','pending_payment');
		$this->_setupOgoneStatus('ogone_cancel','Ogone Canceled','canceled');
		$this->_setupOgoneStatus('ogone_decline','Ogone Declined','canceled');
		$this->_setupOgoneStatus('ogone_pending','Ogone Pending','pending_payment');
		$this->_setupOgoneStatus('ogone_processed','Ogone Processed Payment','processing');
		$this->_setupOgoneStatus('ogone_processing','Ogone Processing Payment','processing');
	}
	
	/**
	 * Creates or updates a custom order status
	 * 
	 * @param string $statusCode
	 * @param string $statusLabel
	 * @param string $state
	 * @param string $isDefault
	 */
	protected function _setupOgoneStatus($statusCode, $statusLabel, $state, $isDefault = false){
		/* @var $status Mage_Sales_Model_Order_Status */
		$status = Mage::getModel('sales/order_status');
	
		$status->setStatus($statusCode);
		$status->setLabel($statusLabel);
		$status->assignState($state, $isDefault);
	
		$status->save();
	}
}
