<?php

class Storefront_PayOgone_Model_Log_Postsale_Request extends Mage_Core_Model_Abstract {

	protected function _construct () {
		$this->_init('ogone/log_postsale_request');
	}
	
	public function getPostData(){
		$serialized = $this->getData('post_data');
		return unserialize($serialized);
	}
	
	public function setPostData($dataArray){
		if(!is_array($dataArray)){
			Mage::throwException('Data must be an array.');
		}
		$this->setData('post_data', serialize($dataArray));
		return $this;
	}
}