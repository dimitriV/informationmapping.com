<?php
class Storefront_PayOgone_Model_Resource_Postsale_Request extends Mage_Core_Model_Resource_Db_Abstract {

	protected function _construct () {
		$this->_init('ogone/postsale_request', 'id');
	}
	
	protected function _extractSerializedOgoneData($object){
		$data = $object->getData();
		
		$ogoneData = $object->getOgoneData();
		
		$combined = array_merge($ogoneData, $data);
		
		$object->setData($combined);
	}
	
	protected function _beforeSave(Mage_Core_Model_Abstract $object){
		$this->_extractSerializedOgoneData($object);
		return parent::_beforeSave($object);
	}
	
	protected function _afterLoad(Mage_Core_Model_Abstract $object){
		$this->_extractSerializedOgoneData($object);
		return parent::_afterLoad($object);
	}
}