<?php
class Storefront_PayOgone_Model_Resource_Postsale_Request_Collection extends Mage_Core_Model_Resource_Db_Collection_Abstract {
	protected function _construct() {
		$this->_init ( 'ogone/postsale_request' );
	}
}