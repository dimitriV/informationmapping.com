<?php

class Storefront_PayOgone_Model_Resource_Api_Debug extends Mage_Core_Model_Resource_Db_Abstract {

	protected function _construct () {
		$this->_init('ogone/api_debug', 'debug_id');
	}
}