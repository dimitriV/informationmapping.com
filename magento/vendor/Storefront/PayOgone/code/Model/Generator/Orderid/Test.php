<?php
class Storefront_PayOgone_Model_Generator_Orderid_Test extends Storefront_PayOgone_Model_Generator_Orderid_Abstract{
	
	/**
	 * Generate the orderID to be sent to Ogone
	 * This function is the opposite of reverseOgoneOrderId()
	 * @return string
	 */
	public function generateOgoneOrderId(Mage_Sales_Model_Order $order){
		return 'test'.$order->getIncrementId();
	}
	
	/**
	 * Get the Magento order based on the Ogone orderID
	 * This function is the opposite of generateOgoneOrderId()
	 */
	public function reverseOgoneOrderId($orderID){
		$orderID = substr($orderID, 4);
		
		$order = Mage::getModel('sales/order')->loadByIncrementId($orderID);
		if($order->getId()){
			return $order;
		}else{
			return null;
		}
	}
	
	public function getName(){
		return 'Use Magento\'s order number prefixed with "test" for testing';
	}
	
	public function isAvailable(){
		return Mage::getIsDeveloperMode();
	}
	
}