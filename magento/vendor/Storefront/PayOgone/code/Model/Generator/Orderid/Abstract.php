<?php
abstract class Storefront_PayOgone_Model_Generator_Orderid_Abstract{
	
	/**
	 * Generate the orderID to be sent to Ogone
	 * This function is the opposite of reverseOgoneOrderId()
	 * @return string
	 */
	abstract public function generateOgoneOrderId(Mage_Sales_Model_Order $order);
	
	/**
	 * Get the Magento order based on the Ogone orderID
	 * This function is the opposite of generateOgoneOrderId()
	 */
	abstract public function reverseOgoneOrderId($orderID);
	
	/**
	 * Get a user-friendly name for this generator
	 * @return string
	 */
	abstract public function getName();
	
	abstract public function isAvailable();
}