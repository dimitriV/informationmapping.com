<?php
class Storefront_PayOgone_Model_Generator_Orderid_Incrementid extends Storefront_PayOgone_Model_Generator_Orderid_Abstract{
	
	/**
	 * Generate the orderID to be sent to Ogone
	 * This function is the opposite of reverseOgoneOrderId()
	 * @return string
	 */
	public function generateOgoneOrderId(Mage_Sales_Model_Order $order){
		return $order->getIncrementId();
	}
	
	/**
	 * Get the Magento order based on the Ogone orderID
	 * This function is the opposite of generateOgoneOrderId()
	 */
	public function reverseOgoneOrderId($orderID){
		$order = Mage::getModel('sales/order')->loadByIncrementId($orderID);
		if($order->getId()){
			return $order;
		}else{
			return null;
		}
	}
	
	public function getName(){
		return 'Use Magento\'s order number';
	}
	
	public function isAvailable(){
		return true;
	}
	
}