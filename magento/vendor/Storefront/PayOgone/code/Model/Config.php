<?php
class Storefront_PayOgone_Model_Config extends Mage_Payment_Model_Config {

	/**
	 * Return an array of accepted payment methods for selection
	 *
	 * @return array of payment methods, brands, labels and image filenames
	 */
	public function getPm ($_code = FALSE) {
		if (! $_code) {
			return;
		}
		
		$_pmList = array(
				'ogone_airplus' => array(
						'key' => 'airplus',
						'PM' => 'CreditCard',
						'BRAND' => 'AIRPLUS',
						'label' => Mage::helper('ogone')->__('CreditCard - AIRPLUS'),
						'img' => 'AIRPLUS_choice.gif'
				),
				'ogone_americanexpress' => array(
						'key' => 'american_express',
						'PM' => 'CreditCard',
						'BRAND' => 'American Express',
						'label' => Mage::helper('ogone')->__('CreditCard - American Express'),
						'img' => 'American Express_choice.gif'
				),
				'ogone_aurora' => array(
						'key' => 'aurora',
						'PM' => 'CreditCard',
						'BRAND' => 'Aurora',
						'label' => Mage::helper('ogone')->__('CreditCard - Aurora'),
						'img' => 'Aurora_choice.gif'
				),
				'ogone_aurore' => array(
						'key' => 'aurore',
						'PM' => 'CreditCard',
						'BRAND' => 'Aurore',
						'label' => Mage::helper('ogone')->__('CreditCard - Aurore'),
						'img' => 'Aurore_choice.gif'
				),
				'ogone_billy' => array(
						'key' => 'billy',
						'PM' => 'CreditCard',
						'BRAND' => 'Billy',
						'label' => Mage::helper('ogone')->__('CreditCard - Billy'),
						'img' => 'Billy_choice.gif'
				),
				'ogone_cb' => array(
						'key' => 'cb',
						'PM' => 'CreditCard',
						'BRAND' => 'CB',
						'label' => Mage::helper('ogone')->__('CreditCard - CB'),
						'img' => 'Carte%20bleue_choice.gif'
				),
				'ogone_cofinoga' => array(
						'key' => 'cofinoga',
						'PM' => 'CreditCard',
						'BRAND' => 'Cofinoga',
						'label' => Mage::helper('ogone')->__('CreditCard - Cofinoga'),
						'img' => 'Cofinoga_choice.gif'
				),
				'ogone_dankort' => array(
						'key' => 'dankort',
						'PM' => 'CreditCard',
						'BRAND' => 'Dankort',
						'label' => Mage::helper('ogone')->__('CreditCard - Dankort'),
						'img' => 'Dankort_choice.gif'
				),
				'ogone_dinersclub' => array(
						'key' => 'diners_club',
						'PM' => 'CreditCard',
						'BRAND' => 'Diners Club',
						'label' => Mage::helper('ogone')->__('CreditCard - Diners Club'),
						'img' => 'Diners Club_choice.gif'
				),
				'ogone_jcb' => array(
						'key' => 'jcb',
						'PM' => 'CreditCard',
						'BRAND' => 'JCB',
						'label' => Mage::helper('ogone')->__('CreditCard - JCB'),
						'img' => 'JCB_choice.gif'
				),
				'ogone_maestrouk' => array(
						'key' => 'maestrouk',
						'PM' => 'CreditCard',
						'BRAND' => 'MaestroUK',
						'label' => Mage::helper('ogone')->__('CreditCard - MaestroUK'),
						'img' => 'Maestro_choice.gif'
				),
				'ogone_mastercard' => array(
						'key' => 'mastercard',
						'PM' => 'CreditCard',
						'BRAND' => 'MasterCard',
						'label' => Mage::helper('ogone')->__('CreditCard - MasterCard'),
						'img' => 'Eurocard_choice.gif'
				),
				'ogone_solo' => array(
						'key' => 'solo',
						'PM' => 'CreditCard',
						'BRAND' => 'Solo',
						'label' => Mage::helper('ogone')->__('CreditCard - Solo'),
						'img' => 'Solo_choice.gif'
				),
				'ogone_uatp' => array(
						'key' => 'uatp',
						'PM' => 'CreditCard',
						'BRAND' => 'UATP',
						'label' => Mage::helper('ogone')->__('CreditCard - UATP'),
						'img' => 'UATP_choice.gif'
				),
				'ogone_visa' => array(
						'key' => 'visa',
						'PM' => 'CreditCard',
						'BRAND' => 'VISA',
						'label' => Mage::helper('ogone')->__('CreditCard - VISA'),
						'img' => 'VISA_choice.gif'
				),
				'ogone_bcmc' => array(
						'key' => 'bcmc',
						'PM' => 'CreditCard',
						'BRAND' => 'BCMC',
						'label' => Mage::helper('ogone')->__('CreditCard - BCMC'),
						'img' => 'BCMC_choice.gif'
				),
				'ogone_maestro' => array(
						'key' => 'maestro',
						'PM' => 'CreditCard',
						'BRAND' => 'Maestro',
						'label' => Mage::helper('ogone')->__('CreditCard - Maestro'),
						'img' => 'Maestro_choice.gif'
				),
				'ogone_postfinancecard' => array(
						'key' => 'postfinance_card',
						'PM' => 'PostFinance Card',
						'BRAND' => 'PostFinance + card',
						'label' => Mage::helper('ogone')->__('PostFinance Card'),
						'img' => ''
				),
				'ogone_netreserve' => array(
						'key' => 'netreserve',
						'PM' => 'CreditCard',
						'BRAND' => 'NetReserve',
						'label' => Mage::helper('ogone')->__('NetReserve'),
						'img' => ''
				),
				'ogone_privilege' => array(
						'key' => 'privilege',
						'PM' => 'CreditCard',
						'BRAND' => 'PRIVILEGE',
						'label' => Mage::helper('ogone')->__('PRIVILEGE'),
						'img' => 'PRIVILEGE_choice.gif'
				),
				'ogone_uneurocom' => array(
						'key' => 'uneurocom',
						'PM' => 'UNEUROCOM',
						'BRAND' => 'UNEUROCOM',
						'label' => Mage::helper('ogone')->__('CreditCard - UNEUROCOM'),
						'img' => 'UNEUROCOM_choice.gif'
				),
				'ogone_cbconline' => array(
						'key' => 'cbc_online',
						'PM' => 'CBC Online',
						'BRAND' => 'CBC Online',
						'label' => Mage::helper('ogone')->__('CreditCard - CBC Online'),
						'img' => 'CBC Online_choice.gif'
				),
				'ogone_centeaonline' => array(
						'key' => 'centea_online',
						'PM' => 'CENTEA Online',
						'BRAND' => 'CENTEA Online',
						'label' => Mage::helper('ogone')->__('CreditCard - CENTEA Online'),
						'img' => 'CENTEA Online_choice.gif'
				),
				'ogone_belfiusdirectnet' => array(
						'key' => 'belfius_direct_net',
						'PM' => 'Belfius Direct Net',
						'BRAND' => 'Belfius Direct Net',
						'label' => Mage::helper('ogone')->__('Belfius Direct Net'),
						'img' => 'Belfius_choice.gif'
				),
				'ogone_sofortueberweisung' => array(
						'key' => 'sofort_ueberweisung',
						'PM' => 'DirectEbanking',
						'BRAND' => 'Sofort Uberweisung',
						'label' => Mage::helper('ogone')->__('Sofort Überweisung'),
						'img' => 'DirectEbanking_choice.gif'
				),
				'ogone_edankort' => array(
						'key' => 'edankort',
						'PM' => 'eDankort',
						'BRAND' => 'eDankort',
						'label' => Mage::helper('ogone')->__('eDankort'),
						'img' => 'eDankort_choice.gif'
				),
				'ogone_eps' => array(
						'key' => 'eps',
						'PM' => 'EPS',
						'BRAND' => 'EPS',
						'label' => Mage::helper('ogone')->__('EPS'),
						'img' => 'EPS_choice.gif'
				),
				'ogone_fortispaybutton' => array(
						'key' => 'fortis_pay_button',
						'PM' => 'Fortis Pay Button',
						'BRAND' => 'Fortis Pay Button',
						'label' => Mage::helper('ogone')->__('Fortis Pay Button'),
						'img' => 'Fortis Pay Button_choice.gif'
				),
				'ogone_giropay' => array(
						'key' => 'giropay',
						'PM' => 'giropay',
						'BRAND' => 'giropay',
						'label' => Mage::helper('ogone')->__('giropay'),
						'img' => 'giropay_choice.gif'
				),
				'ogone_ideal' => array(
						'key' => 'ideal',
						'PM' => 'iDEAL',
						'BRAND' => 'iDEAL',
						'label' => Mage::helper('ogone')->__('iDEAL'),
						'img' => 'iDEAL_choice.gif'
				),
				'ogone_inghomepay' => array(
						'key' => 'ing_homepay',
						'PM' => 'ING HomePay',
						'BRAND' => 'ING HomePay',
						'label' => Mage::helper('ogone')->__('ING HomePay'),
						'img' => 'ING_choice.gif'
				),
				'ogone_kbconline' => array(
						'key' => 'kbc_online',
						'PM' => 'KBC Online',
						'BRAND' => 'KBC Online',
						'label' => Mage::helper('ogone')->__('KBC Online'),
						'img' => 'KBC Online_choice.gif'
				),
				'ogone_mpass' => array(
						'key' => 'mpass',
						'PM' => 'MPASS',
						'BRAND' => 'MPASS',
						'label' => Mage::helper('ogone')->__('MPASS'),
						'img' => 'MPASS_choice.gif'
				),
				'ogone_paysafecard' => array(
						'key' => 'paysafecard',
						'PM' => 'paysafecard',
						'BRAND' => 'paysafecard',
						'label' => Mage::helper('ogone')->__('paysafecard'),
						'img' => 'paysafecard_choice.gif'
				),
				'ogone_postfinanceefinance' => array(
						'key' => 'postfinance_e_finance',
						'PM' => 'PostFinance e-finance',
						'BRAND' => 'PostFinance e-finance',
						'label' => Mage::helper('ogone')->__('PostFinance e-finance'),
						'img' => ''
				),
				'ogone_directdebitsat' => array(
						'key' => 'direct_debits_at',
						'PM' => 'Direct Debits AT',
						'BRAND' => 'Direct Debits AT',
						'label' => Mage::helper('ogone')->__('Direct Debits AT'),
						'img' => 'Direct Debits AT_choice.gif'
				),
				'ogone_directdebitsde' => array(
						'key' => 'direct_debits_de',
						'PM' => 'Direct Debits DE',
						'BRAND' => 'Direct Debits DE',
						'label' => Mage::helper('ogone')->__('Direct Debits DE'),
						'img' => 'Direct Debits DE_choice.gif'
				),
				'ogone_directdebitsnl' => array(
						'key' => 'direct_debits_nl',
						'PM' => 'Direct Debits NL',
						'BRAND' => 'Direct Debits NL',
						'label' => Mage::helper('ogone')->__('Direct Debits NL'),
						'img' => 'Direct Debits NL_choice.gif'
				),
				'ogone_acceptgiro' => array(
						'key' => 'acceptgiro',
						'PM' => 'Acceptgiro',
						'BRAND' => 'Acceptgiro',
						'label' => Mage::helper('ogone')->__('Acceptgiro'),
						'img' => 'Acceptgiro_choice.gif'
				),
				'ogone_banktransfer' => array(
						'key' => 'bank_transfer',
						'PM' => 'Bank transfer',
						'BRAND' => 'Bank transfer',
						'label' => Mage::helper('ogone')->__('Bank transfer'),
						'img' => 'Bank transfer_choice.gif'
				),
				'ogone_paymentondelivery' => array(
						'key' => 'payment_on_delivery',
						'PM' => 'Payment on Delivery',
						'BRAND' => 'Payment on Delivery',
						'label' => Mage::helper('ogone')->__('Payment on Delivery'),
						'img' => 'Payment on Delivery_choice.gif'
				),
				'ogone_intersolve' => array(
						'key' => 'intersolve',
						'PM' => 'InterSolve',
						'BRAND' => 'InterSolve',
						'label' => Mage::helper('ogone')->__('InterSolve'),
						'img' => 'InterSolve_choice.gif'
				),
				'ogone_minitix' => array(
						'key' => 'minitix',
						'PM' => 'MiniTix',
						'BRAND' => 'MiniTix',
						'label' => Mage::helper('ogone')->__('MiniTix'),
						'img' => 'MiniTix_choice.gif'
				),
				'ogone_pingping' => array(
						'key' => 'pingping',
						'PM' => 'PingPing',
						'BRAND' => 'PingPing',
						'label' => Mage::helper('ogone')->__('PingPing'),
						'img' => 'PingPing_choice.gif'
				),
				'ogone_tunz' => array(
						'key' => 'tunz',
						'PM' => 'TUNZ',
						'BRAND' => 'TUNZ',
						'label' => Mage::helper('ogone')->__('TUNZ'),
						'img' => 'TUNZ_choice.gif'
				),
				'ogone_paypal' => array(
						'key' => 'paypal',
						'PM' => 'PAYPAL',
						'BRAND' => 'PAYPAL',
						'label' => Mage::helper('ogone')->__('PAYPAL'),
						'img' => 'PAYPAL_choice.gif'
				),
				'ogone_wallie' => array(
						'key' => 'wallie',
						'PM' => 'Wallie',
						'BRAND' => 'Wallie',
						'label' => Mage::helper('ogone')->__('Wallie'),
						'img' => 'Wallie_choice.gif'
				)
		);
		
		return $_pmList[$_code];
	}

	/**
	 * Return boolean is PM directlink compatible TRUE/FALSE?
	 */
	public function getDirectLinkCompatible ($_code) {
		return in_array($_code, array(
				'ogone_airplus',
				'ogone_americanexpress',
				'ogone_aurore',
				'ogone_cofinoga',
				'ogone_dinersclub',
				'ogone_jcb',
				'ogone_mastercard',
				'ogone_uatp',
				'ogone_visa'
		));
	}

	/**
	 * Get iDEAL Issuers
	 */
	public function getIdealIssuers () {
		return array(
				'' => Mage::helper('ogone')->__(' - Please choose your bank - '),
				'0031' => Mage::helper('ogone')->__('ABN AMRO'),
				'0761' => Mage::helper('ogone')->__('ASN Bank'),
				'0091' => Mage::helper('ogone')->__('Friesland Bank'),
				'0721' => Mage::helper('ogone')->__('ING'),
				'0021' => Mage::helper('ogone')->__('Rabobank'),
				'0771' => Mage::helper('ogone')->__('RegioBank'),
				'0751' => Mage::helper('ogone')->__('SNS Bank'),
				'0511' => Mage::helper('ogone')->__('Triodos Bank'),
				'0161' => Mage::helper('ogone')->__('Van Lanschot Bankiers')
		);
	}

	/**
	 * Return ogone payment config information
	 *
	 * @param string $path        	
	 * @param int $storeId        	
	 * @return Simple_Xml
	 */
	public function getConfigData ($path, $storeId = null) {
		if (! empty($path)) {
			return Mage::getStoreConfig($path, $storeId);
		}
		return FALSE;
	}

	/**
	 * Return SHA1-IN secret key from configuration
	 *
	 * @param int $storeId        	
	 * @return string
	 */
	public function getShaInCode ($storeId = null) {
		if ($this->getIsTestEnvironment($storeId)) {
			$r = $this->getConfigData('ogone/test_account/secret_key_in', $storeId);
		} else {
			$r = $this->getConfigData('ogone/production_account/secret_key_in', $storeId);
		}
		
		// $r = Mage::helper('core')->decrypt($r);
		
		return $r;
	}

	/**
	 * Return SHA1-IN secret key from configuration
	 *
	 * @param int $storeId        	
	 * @return string
	 */
	public function getDirectLinkShaInCode ($storeId = null) {
		return $this->getConfigData('ogone/direct/secret_key_directlink', $storeId);
	}

	/**
	 * Return SHA1-OUT secret key from configuration
	 *
	 * @param int $storeId        	
	 * @return string
	 */
	public function getShaOutCode ($storeId = null) {
		if ($this->getIsTestEnvironment($storeId)) {
			$r = $this->getConfigData('ogone/test_account/secret_key_out', $storeId);
		} else {
			$r = $this->getConfigData('ogone/production_account/secret_key_out', $storeId);
		}
		
		// $r = Mage::helper('core')->decrypt($r);
		
		return $r;
	}

	/**
	 * Get CcNumber decrypted
	 *
	 * @param int $storeId        	
	 * @return string
	 */
	public function getCcNumberDec () {
		$session = Mage::getSingleton('core/session')->getCcNumberEnc();
		
		// kill the CC Number session
		Mage::getSingleton('core/session')->setCcNumberEnc(null);
		return Mage::helper('core')->decrypt($session);
	}

	/**
	 * Get CcCid decrypted & destroy session
	 * We are not allowed to permanently store the CVN/CVV2
	 *
	 * @param int $storeId        	
	 * @return string
	 */
	public function getCcCidDec () {
		$session = Mage::getSingleton('core/session')->getCcCidEnc();
		
		// kill the CC CVC session
		Mage::getSingleton('core/session')->setCcCidEnc(null);
		return Mage::helper('core')->decrypt($session);
	}

	/**
	 * Get CcCid decrypted & destroy session
	 * We are not allowed to permanently store the CVN/CVV2
	 *
	 * @param int $storeId        	
	 * @return string
	 */
	public function getCcExpMonthDec () {
		$session = Mage::getSingleton('core/session')->getCcExpMonthEnc();
		
		// kill the CC CVC session
		Mage::getSingleton('core/session')->getCcExpMonthEnc(null);
		return Mage::helper('core')->decrypt($session);
	}

	/**
	 * Get CcCid decrypted & destroy session
	 * We are not allowed to permanently store the CVN/CVV2
	 *
	 * @param int $storeId        	
	 * @return string
	 */
	public function getCcExpYearDec () {
		$session = Mage::getSingleton('core/session')->getCcExpYearEnc();
		
		// kill the CC CVC session
		Mage::getSingleton('core/session')->getCcExpYearEnc(null);
		return Mage::helper('core')->decrypt($session);
	}

	/**
	 * Return EcIndicator
	 *
	 * @param int $storeId        	
	 * @return string
	 */
	public function getEcIndicator ($storeId = null) {
		return $this->getConfigData('ogone/direct/eci', $storeId);
	}

	/**
	 * Return Hash Algorithm
	 *
	 * @param int $storeId        	
	 * @return string
	 */
	public function getHashAlgorithm ($storeId = null) {
		return $this->getConfigData('ogone/security/hash_algorithm', $storeId);
	}

	/**
	 * Return SHA1-OUT secret key from configuration
	 *
	 * @param int $storeId        	
	 * @return string
	 */
	public function getDirectLinkEnabled ($storeId = null) {
		return $this->getConfigData('ogone/direct/directlink', $storeId);
	}

	/**
	 * Return SHA1-OUT secret key from configuration
	 *
	 * @param int $storeId        	
	 * @return string
	 */
	public function getDirectLinkEnabledAndCompatible ($storeId = null) {
		$_enabled = FALSE;
		$_compatible = FALSE;
		
		// DirectLink enabled and compatible PM Brand?
		if ($_enabled = $this->getConfigData('ogone/direct/directlink', $storeId)) {
			try {
				$_code = Mage::getSingleton('checkout/session')->getQuote()->getPayment()->getMethodInstance()->getCode();
				$_compatible = $this->getDirectLinkCompatible($_code);
			} catch (Exception $e) {}
		}
		
		return ($_enabled && $_compatible) ? TRUE : FALSE;
	}

	/**
	 * Return the Order Email setting (0/1)
	 *
	 * @param int $storeId        	
	 * @return string
	 */
	public function getOrderEmail ($storeId = null) {
		return $this->getConfigData('ogone/payment/order_email', $storeId);
	}

	/**
	 * Get the generator model for Ogone's orderID
	 * 
	 * @return Storefront_PayOgone_Model_Generator_Orderid_Abstract
	 */
	protected function _getOrderIdGenerator () {
		$generatorCode = Mage::getStoreConfig('ogone/payment/generator_orderid');
		
		if (empty($generatorCode)) {
			$generatorCode = 'incrementid';
		}
		
		/* @var $helper Storefront_PayOgone_Helper_Data */
		$helper = Mage::helper('ogone');
		
		$generator = $helper->getOrderGenerator($generatorCode);
		
		return $generator;
	}

	public function generateOrderId (Mage_Sales_Model_Order $order) {
		$gen = $this->_getOrderIdGenerator();
		return $gen->generateOgoneOrderId($order);
	}

	/**
	 * The opposite of generateOrderId.
	 * Get the original Magento order
	 */
	public function getMagentoOrderIncrementFromOgoneOrderId ($ogoneOrderId) {
		$gen = $this->_getOrderIdGenerator();
		return $gen->reverseOgoneOrderId($ogoneOrderId);
	}

	/**
	 * Return the Create Invoice setting (0/1)
	 *
	 * @param int $storeId        	
	 * @return string
	 */
	public function getInvoiceCreate ($storeId = null) {
		return $this->getConfigData('ogone/payment/invoice_create', $storeId);
	}

	/**
	 * Return the Invoice Email setting (0/1)
	 *
	 * @param int $storeId        	
	 * @return string
	 */
	public function getInvoiceEmail ($storeId = null) {
		return $this->getConfigData('ogone/payment/invoice_email', $storeId);
	}

	/**
	 * Return the Base Currency setting (0/1)
	 *
	 * @param int $storeId        	
	 * @return string
	 */
	public function getBaseCurrency ($storeId = null) {
		return $this->getConfigData('ogone/payment/base_currency', $storeId);
	}

	/**
	 * Return SHA1-OUT secret key from configuration
	 *
	 * @param int $storeId        	
	 * @return string
	 */
	public function getShowLogos ($storeId = null) {
		return $this->getConfigData('ogone/payment/logos', $storeId);
	}

	/**
	 * Return gateway path, get from confing.
	 * Setup on admin place.
	 *
	 * @param int $storeId        	
	 * @return string
	 */
	public function getDirectLinkGatewayPath ($storeId = null) {
		if ($this->getConfigData('ogone/settings/environment') === 'prod') {
			$url = $this->getConfigData('ogone/direct/gateway_prod');
		} else {
			$url = $this->getConfigData('ogone/direct/gateway_test');
		}
		return $url;
	}

	/**
	 * Return gateway path, get from config.
	 * Setup on admin place.
	 *
	 * @param int $storeId        	
	 * @return string
	 */
	public function getGatewayPath ($storeId = null) {
		if ($this->getConfigData('ogone/settings/environment') === 'prod') {
			$url = $this->getConfigData('ogone/settings/gateway_prod', $storeId);
		} else {
			$url = $this->getConfigData('ogone/settings/gateway_test', $storeId);
		}
		return $url;
	}

	/**
	 * Return gateway path, get from confing.
	 * Setup on admin place.
	 *
	 * @param int $storeId        	
	 * @return string
	 */
	public function getApiPswd ($storeId = null) {
		return $this->getConfigData('ogone/direct/api_pswd', $storeId);
	}

	/**
	 * Return gateway path, get from confing.
	 * Setup on admin place.
	 *
	 * @param int $storeId        	
	 * @return string
	 */
	public function getApiUser ($storeId = null) {
		return $this->getConfigData('ogone/direct/api_userid', $storeId);
	}

	/**
	 * Return PSPID; the PSPID is case sensitive
	 *
	 * @param int $storeId        	
	 * @return string
	 */
	public function getPSPID ($storeId = null) {
		if ($this->getIsTestEnvironment($storeId)) {
			$r = $this->getConfigData('ogone/test_account/pspid', $storeId);
		} else {
			$r = $this->getConfigData('ogone/production_account/pspid', $storeId);
		}
		return $r;
	}

	public function getIsTestEnvironment ($storeId = null) {
		$env = $this->getConfigData('ogone/settings/environment', $storeId);
		if ($env == 'test') {
			return true;
		} else {
			return false;
		}
	}

	/**
	 * Return USERID; the USERID is case sensitive
	 *
	 * @param int $storeId        	
	 * @return string
	 */
	public function getUSERID ($storeId = null) {
		if ($this->getIsTestEnvironment($storeId)) {
			$r = $this->getConfigData('ogone/test_account/userid', $storeId);
		} else {
			$r = $this->getConfigData('ogone/production_account/userid', $storeId);
		}
		return $r;
	}

	/**
	 * Return payment page template for Magento
	 *
	 * @return string
	 */
	public function getPaymentTemplate () {
		return Mage::getUrl('ogone/api/template', array(
				'_forced_secure' => true
		));
	}

	/**
	 * Return accept URL
	 *
	 * @return string
	 */
	public function getAcceptUrl ($storeId = 'default', Mage_Sales_Model_Order $order) {
		return Mage::getUrl('ogone/api/accept', array(
				'_store' => $storeId,
				'order' => $order->getIncrementId(),
				'secret' => $this->getOrderSecretKey($order)
		));
	}

	public function getOrderSecretKey (Mage_Sales_Model_Order $order) {
		return sha1($order->getId() . $order->getSecret() . $order->getIncrementId(), false);
	}

	/**
	 * Return decline URL
	 *
	 * @return string
	 */
	public function getDeclineUrl ($storeId = 'default') {
		return Mage::getUrl('ogone/api/decline', array(
				'_store' => $storeId
		));
	}

	/**
	 * Return exception URL
	 *
	 * @return string
	 */
	public function getExceptionUrl ($storeId = 'default') {
		return Mage::getUrl('ogone/api/exception', array(
				'_store' => $storeId
		));
	}

	/**
	 * Return cancel URL
	 *
	 * @return string
	 */
	public function getCancelUrl ($storeId = 'default') {
		return Mage::getUrl('ogone/api/cancel', array(
				'_store' => $storeId
		));
	}

	/**
	 * Return home URL
	 *
	 * @return string
	 */
	public function getHomeUrl ($storeId = 'default') {
		return Mage::getUrl('checkout/cart', array(
				'_store' => $storeId
		));
	}
}
