<?php

class Storefront_PayOgone_ApiController extends Mage_Core_Controller_Front_Action {
	
	/*
	 * @param Mage_Sales_Model_Order
	 */
	protected $_order = null;

	/**
	 *
	 * @var Mage_DirectLink_Model_Config
	 */
	protected $_config = null;

	/**
	 * DirectLink info instance
	 *
	 * @var Mage_DirectLink_Model_Info
	 */
	protected $_info = null;

	/**
	 * DirectLink request data
	 *
	 * @var array
	 */
	protected $_request = array();

	/**
	 * Collected debug information
	 *
	 * @var array
	 */
	protected $_debugData = array();

	/**
	 * Return checkout session
	 *
	 * @return Mage_Checkout_Model_Session
	 */
	protected function _getCheckout () {
		return Mage::getSingleton('checkout/session');
	}

	/**
	 * Pseudo getParam() for normal versus DirectLink
	 *
	 * @return string
	 */
	protected function _getParam ($param) {
		$params = $this->_getParams();
		
		if(isset($params[$param])){
			return $params[$param];
		}else{
			return null;
		}
	}

	/**
	 * Pseudo getParam() for normal versus DirectLink
	 *
	 * @return string
	 */
	protected function _getParams () {
		return array_merge($_GET, $_POST);
	}


	/**
	 * Placement action to load and submit placement form to Ogone via POST
	 * ogone/api/placement
	 */
	public function placementAction () {
		
		/* @var $order Mage_Sales_Model_Order */
		$order = Mage::getModel('sales/order');
		
		$orderId = $this->getRequest()->getParam('order_id');
		if($orderId){
			$order->load($orderId);
		}
		
		if(!$order->getId()){
			$lastIncrementId = $this->_getCheckout()->getLastRealOrderId();
			
			if($lastIncrementId){
				$order->loadByIncrementId($lastIncrementId);
			}
		}
		
		
		if ($order->getId()) {
			
			// TODO improve check. The order may have a different state if we have an unusual order flow.
			// We need to check if there is an amount due for the order, and send the right amount to ogone.
			// This amount may differ from the total amount if we already have a partial payment or credit note.
			$isPaid = ($order->getInvoiceCollection()->count() > 0);
			
			
			if (!$isPaid) {
				
				// The order can continue to Ogone
				
				$order->setState(Mage_Sales_Model_Order::STATE_PENDING_PAYMENT, Storefront_PayOgone_Model_Api::OGONE_STATUS_PENDING, Mage::helper('ogone')->__('Started Ogone payment process.'));
				$order->save();
				
				$ogoneData = $this->_getApi()->getFormFields($order);
				
				// Debug
				if ($this->_getApi()->getDebug()) {
					$debug = Mage::getModel('ogone/api_debug')->setDir('out')->setUrl($this->getRequest()->getPathInfo())->setData('data', http_build_query($ogoneData))->save();
				}
				Mage::log($ogoneData, null, 'payogone-form.log');
				
				$this->_getCheckout()->setOgoneQuoteId($this->_getCheckout()->getQuoteId());
				$this->_getCheckout()->setOgoneLastSuccessQuoteId($this->_getCheckout()->getLastSuccessQuoteId());
				
				$this->loadLayout();
				
				/* @var $placementBlock Storefront_PayOgone_Block_Placement */
				$placementBlock = $this->getLayout()->getBlock('ogone_placement');
				
				$placementBlock->setOrder($order);
				
				$this->renderLayout();
				
			} else {
				$this->_redirect('checkout/cart');
			}
		} else {
			$this->_redirect('checkout/cart');
		}
	}

	/**
	 * Payment action to load Ogone payment form
	 * ogone/api/payment
	 */
	public function templateAction () {
		$this->loadLayout();
		$this->renderLayout();
	}


	/**
	 * Ogone offline process action to start Ogone background process
	 * ogone/api/offlineProcess
	 */
	public function offlineProcessAction () {
		$this->postBackAction();
	}

	/**
	 * Ogone postback action to start processing
	 * ogone/api/postBack
	 */
	public function postBackAction () {
		if (count($_POST) == 0 && count($_GET) == 0) {
			$this->getResponse()->setHeader("Status", "500 Internal Server Error");
			$this->getResponse()->setBody('No data submitted');
			return;
		}
		
		
		/* @var $postsale Storefront_PayOgone_Model_Postsale_Request */
		$postsale = Mage::getModel('ogone/postsale_request');
		
		try {
			$postsale->initFromRequest();
			$postsale->save();
			
			if (($this->_isValidatePostsaleEnabled() && $postsale->isValid()) || !$this->_isValidatePostsaleEnabled()) {
				$postsale->run();
				
			}else{
				// Invalid request - save but don't run!
				$postsale->addError('Invalid request');
			}
			
			
		} catch (Exception $e) {
			Mage::logException($e);
			mail('wouter.samaey@storefront.be', 'Can\'t log incoming Ogone postsale request' . "\n" . print_r($_POST, true));
			
			$postsale->addError('Exception: '.$e);
			
		}
		
		$postsale->applyToResponse($this->getResponse());
		
		$postsale->save();
	}

	protected function _isValidatePostsaleEnabled () {
		$r = Mage::getStoreConfig('ogone/security/validate_postsale_flag');
		return $r;
	}

	/**
	 * Decision tree to process Ogone statuses
	 */
	protected function _UNUSED_ogoneProcess () {
		
		// Moved this to the top
		$this->_updateOgoneStatusHistory();
		
		try {
			$paymentStatus = $this->_getParam('STATUS');
			$errorMessage = Mage::helper('ogone')->__('Error') . ': ' . Mage::helper('ogone')->__($this->_getParam('NCERRORPLUS')) . ' (NCERROR: ' . $this->_getParam('NCERROR') . ')';
			
			switch ($paymentStatus) {
				
				
				
				// declined payment
				case Storefront_PayOgone_Model_Api::OGONE_INVALID:
					// intentionally do not flush cart
// 					$this->_getCheckout()->addError(Mage::helper('ogone')->__('Invalid payment.'));
// 					$this->_getCheckout()->addError($errorMessage);
					$this->_declineProcess();
					break;
				
				// incomplete payment
				case Storefront_PayOgone_Model_Api::OGONE_PAYMENT_INCOMPLETE:
// 					$this->_clearCart();
// 					$this->_getCheckout()->addError(Mage::helper('ogone')->__('Incomplete payment! Please contact the store owner to resolve the matter.'));
// 					$this->_getCheckout()->addError($errorMessage);
					$this->_declineProcess();
					break;
				
				// technical problem
				case Storefront_PayOgone_Model_Api::OGONE_TECH_PROBLEM:
					// intentionally do not flush cart
// 					$this->_getCheckout()->addError(Mage::helper('ogone')->__('Technical problem.'));
// 					$this->_getCheckout()->addError($errorMessage);
					$this->_declineProcess();
					break;
				
				// auth refused
				case Storefront_PayOgone_Model_Api::OGONE_AUTH_REFUSED:
					// intentionally do not flush cart
// 					$this->_getCheckout()->addError(Mage::helper('ogone')->__('Authorization refused.'));
// 					$this->_getCheckout()->addError($errorMessage);
					$this->_declineProcess();
					break;
				
				// auth unknown
				case Storefront_PayOgone_Model_Api::OGONE_AUTH_UNKNOWN:
// 					$this->_clearCart();
// 					$this->_getCheckout()->addError(Mage::helper('ogone')->__('Unknown authorization.'));
// 					$this->_getCheckout()->addError($errorMessage);
					$this->_exceptionProcess();
					break;
				
				// payment unknown
				case Storefront_PayOgone_Model_Api::OGONE_PAYMENT_UNKNOWN:
				case Storefront_PayOgone_Model_Api::OGONE_PAYMENT_UNCERTAIN:
// 					$this->_clearCart();
// 					$this->_getCheckout()->addError(Mage::helper('ogone')->__('Could not determine payment status. Please contact the store owner to resolve the matter.'));
// 					$this->_getCheckout()->addError($errorMessage);
					$this->_exceptionProcess();
					break;
				
				default:
					$this->_exceptionProcess();
			}
		} catch (Mage_Core_Exception $e) {
			throw $e;
		}
		
		// Moved this to the top
		// $this->_updateOgoneStatusHistory();
	}

	protected function _updateOgoneStatusHistory () {
		// $status = $this->_getParam('STATUS');
		
		// $order = $this->_getOrder();
		// $payment = $order->getPayment();
		// $payment->setAdditionalInformation('ogone_last_status', $status);
		
		// $ogoneStatusHistory = $payment->getAdditionalInformation('ogone_status_history');
		// if ($ogoneStatusHistory === null) {
		// $ogoneStatusHistory = array();
		// }
		// $timestamp = Mage::getSingleton('core/date')->gmtTimestamp();
		// $ogoneStatusHistory[] = array(
		// 'timestamp' => $timestamp,
		// 'status' => $status,
		// 'params' => $this->_getParams()
		// );
		// $payment->setAdditionalInformation('ogone_status_history', $ogoneStatusHistory);
		
		// $payment->save();
	}

	/**
	 * Ogone payment action: accepted
	 * ogone/api/accept
	 */
	public function acceptAction () {
		$orderIncrement = $this->getRequest()->getParam('order');
		
		//die('PAYMENT ACCEPTED for '.$orderIncrement);
		
		/* @var $order Mage_Sales_Model_Order */
		$order = Mage::getModel('sales/order');
		$order->loadByIncrementId($orderIncrement);
		
		$error = true;
		if ($order->getId()) {
			/* @var $config Storefront_PayOgone_Model_Config */
			$config = Mage::getSingleton('ogone/config');
			
			$actualSecret = $config->getOrderSecretKey($order);
			$inputSecret = $this->getRequest()->getParam('secret');
			
			if ($actualSecret === $inputSecret) {
				$error = false;
			}
		}
		
		if ($error) {
			$this->_redirect('checkout/cart');
			
		} else {
			// Save to session for the next page
			$session = Mage::getSingleton('checkout/type_onepage')->getCheckout();
			
			$session->setLastSuccessQuoteId($order->getQuoteId());
			$session->setLastQuoteId($order->getQuoteId());
			$session->setLastOrderId($order->getId());
			$session->setLastRealOrderId($order->getIncrementId());
			
			$response = $this->getResponse();
			
			$response->setHeader('X-LastSuccessQuoteId', $session->getLastSuccessQuoteId());
			$response->setHeader('X-LastQuoteId', $session->getLastQuoteId());
			$response->setHeader('X-LastOrderId', $session->getLastOrderId());
			$response->setHeader('X-LastRealOrderId', $session->getLastRealOrderId());
			
			// Empty the cart
			$this->_clearCart($order);
			
			// TODO check the order status to see if it was paid or not
			
			$this->_redirect('checkout/onepage/success');
		}
	}

	
	protected function _clearCart ($order) {
		$quoteId = $order->getQuoteId();
		$quote = Mage::getModel('sales/quote')->load($quoteId);
		$quote->setIsActive(FALSE)->save();
	}
	
	/**
	 * Ogone payment mode: Authorize
	 *
	 * Authorization means we place the order without creating an invoice
	 */
	// protected function _processAuthorize () {
	// $order = $this->_getOrder();
	// $status = $this->_getParam('STATUS');
	// try {
	// if ($status == Storefront_PayOgone_Model_Api::OGONE_AUTH_WAITING) {
	// $order->setState(Mage_Sales_Model_Order::STATE_PROCESSING, Storefront_PayOgone_Model_Api::OGONE_STATUS_AUTH, Mage::helper('ogone')->__('Waiting for authorization by Ogone') . ' (PAYID: ' . $this->_getParam('PAYID') . ')');
	// } else {
	
	// // to send new order email only when state is pending payment
	// if ($order->getState() == Mage_Sales_Model_Order::STATE_PENDING_PAYMENT) {
	
	// // Send order email
	// if (! $order->getEmailSent() && $this->_getApi()->getConfig()->getOrderEmail()) {
	// $order->sendNewOrderEmail();
	// }
	// }
	// $order->setState(Mage_Sales_Model_Order::STATE_PROCESSING, Storefront_PayOgone_Model_Api::OGONE_STATUS_PROCESSED, Mage::helper('ogone')->__('Processed by Ogone') . ' (PAYID: ' . $this->_getParam('PAYID') . ')');
	// }
	// $order->save();
	// $this->_getCheckout()->setLastSuccessQuoteId($order->getQuoteId());
	// is_object($this->_request) ? $this->_redirect('checkout/onepage/success') : header("Location: " . Mage::getUrl('checkout/onepage/success'));
	// return;
	// } catch (Exception $e) {
	// $this->_getCheckout()->addError(Mage::helper('ogone')->__('Order could not be saved.'));
	// is_object($this->_request) ? $this->_redirect('checkout/cart') : header("Location: " . Mage::getUrl('checkout/cart'));
	// return;
	// }
	// }
	
	/**
	 * Ogone payment action: exception
	 *
	 * The payment result is uncertain. Exception status can be 52 or 92.
	 */
	public function exceptionAction () {
		$this->_exceptionProcess();
	}

	/**
	 * Ogone payment process: exception
	 */
	public function _exceptionProcess () {
// 		$this->_clearCart();
		
		$params = $this->_getParams();
		$order = $this->_getOrder();
		
		$exception = '';
		switch ($this->_getParam('STATUS')) {
			case Storefront_PayOgone_Model_Api::OGONE_PAYMENT_UNCERTAIN:
				$exception = Mage::helper('ogone')->__('Payment uncertain: a technical problem arose during the payment process. The payment status could not be determined.');
				break;
			
			case Storefront_PayOgone_Model_Api::OGONE_AUTH_UNKNOWN:
				$exception = Mage::helper('ogone')->__('Authorization unknown: a technical problem arose during the authorization process. The payment status could not be determined.');
				break;
			
			default:
				$exception = Mage::helper('ogone')->__('Unknown exception: Payment status could not be determined. Please check your bank statements manually before shipping any goods.');
		}
		
		if (! empty($exception)) {
			
			$this->_getCheckout()->addError($exception);
			
			try {
				$this->_getCheckout()->setLastSuccessQuoteId($order->getQuoteId());
				// $this->_prepareCCInfo($order, $params);
				$order->getPayment()->setLastTransId($this->_getParam('PAYID'));
				
				// to send new order email only when state is pending payment
				if ($order->getState() == Mage_Sales_Model_Order::STATE_PENDING_PAYMENT) {
					
					// Send order email
					if (! $order->getEmailSent() && $this->_getApi()->getConfig()->getOrderEmail()) {
						$order->sendNewOrderEmail();
					}
					$order->setState(Mage_Sales_Model_Order::STATE_PROCESSING, Storefront_PayOgone_Model_Api::OGONE_STATUS_PROCESSING, $exception . ' (PAYID: ' . $this->_getParam('PAYID') . ')');
				} else {
					$order->addStatusToHistory(Storefront_PayOgone_Model_Api::OGONE_STATUS_PROCESSING, $exception);
				}
				$order->save();
			} catch (Exception $e) {
				$this->_getCheckout()->addError(Mage::helper('ogone')->__('Order could not be saved.'));
				is_object($this->_request) ? $this->_redirect('checkout/cart') : header("Location: " . Mage::getUrl('checkout/cart'));
			}
		} else {
			$this->_getCheckout()->addError(Mage::helper('ogone')->__('Exception unknown.'));
			is_object($this->_request) ? $this->_redirect('checkout/cart') : header("Location: " . Mage::getUrl('checkout/cart'));
		}
		
		is_object($this->_request) ? $this->_redirect('checkout/onepage/success') : header("Location: " . Mage::getUrl('checkout/onepage/success'));
	}

	/**
	 * Ogone payment action: decline
	 *
	 * Ogone has canceled the payment. The order status is set to 'canceled' and
	 * the customer is redirected back to the shopping cart.
	 *
	 * @return Storefront_PayOgone_ApiController
	 */
	public function declineAction () {		
		$errorMessage = Mage::helper('ogone')->__('Error') . ': ' . Mage::helper('ogone')->__($this->_getParam('NCERRORPLUS')) . ' (NCERROR: ' . $this->_getParam('NCERROR') . ')';
		
		$this->_getCheckout()->addError(Mage::helper('ogone')->__('Payment was declined by Ogone. Please choose a different payment method.'));
		$this->_getCheckout()->addError($errorMessage);
		$this->_getCheckout()->setQuoteId($this->_getCheckout()->getOgoneQuoteId());
		
		$status = Storefront_PayOgone_Model_Api::OGONE_STATUS_DECLINE;
		$comment = Mage::helper('ogone')->__('Payment declined by Ogone.');
		$this->_getCheckout()->addError(Mage::helper('ogone')->__('Transaction declined by Ogone.'));
		$this->_cancelOrder($status, $comment);
		
		return $this;
	}


	/**
	 * Ogone payment action: cancel
	 *
	 * Customer canceled the payment. The order status is set to 'canceled' and
	 * the customer is redirected back to the shopping cart
	 *
	 * @return Storefront_PayOgone_ApiController
	 */
	public function cancelAction () {
// 		if (! $this->_validateOgoneData()) {
// 			is_object($this->_request) ? $this->_redirect('checkout/cart') : header("Location: " . Mage::getUrl('checkout/cart'));
// 			return;
// 		}
		
// 		$errorMessage = Mage::helper('ogone')->__('Error') . ': ' . Mage::helper('ogone')->__($this->_getParam('NCERRORPLUS')) . ' (NCERROR: ' . $this->_getParam('NCERROR') . ')';
		
// 		$this->_getCheckout()->addError(Mage::helper('ogone')->__('Payment canceled. Please try again.'));
// 		$this->_getCheckout()->addError($errorMessage);
// 		$this->_getCheckout()->setQuoteId($this->_getCheckout()->getOgoneQuoteId());
		
// 		$this->_cancelProcess();

		$this->_redirect('checkout/cart');
		
		return $this;
	}

	/**
	 * Ogone payment process: cancel
	 *
	 * @return Storefront_PayOgone_ApiController
	 */
	public function _cancelProcess () {
		$status = Storefront_PayOgone_Model_Api::OGONE_STATUS_CANCEL;
		$comment = Mage::helper('ogone')->__('Payment canceled by customer.');
		$this->_cancelOrder($status, $comment);
		return $this;
	}

	/**
	 * Ogone process: cancel and redirect to cart
	 *
	 * @return Storefront_PayOgone_ApiController
	 */
	protected function _cancelOrder ($status, $comment = '') {
		$order = $this->_getOrder();
		
		try {
			$order->cancel();
			$order->setState(Mage_Sales_Model_Order::STATE_CANCELED, $status, $comment . ' (PAYID: ' . $this->_getParam('PAYID') . ')');
			$order->save();
		} catch (Exception $e) {
			$this->_getCheckout()->addError(Mage::helper('ogone')->__('Order can not be canceled.'));
		}
		
		is_object($this->_request) ? $this->_redirect('checkout/cart') : header("Location: " . Mage::getUrl('checkout/cart'));
		return $this;
	}

	

	/**
	 * Return Ogone payment API instance
	 *
	 * @return Storefront_PayOgone_Model_Api
	 */
	protected function _getApi () {
		return Mage::getSingleton('ogone/api');
	}
}
