<?php
class Storefront_PayOgone_Adminhtml_PostsaleController extends Mage_Adminhtml_Controller_Action{
	
	
	public function indexAction(){
		$this->_title($this->__('Ogone Postsale Requests'));
		
		$helper = Mage::helper('ogone');
		
		$this->loadLayout();
		
		$block = $this->getLayout()->createBlock('ogone/adminhtml_postsale');
		
		$this->_setActiveMenu('system/ogone_postsale')
		->_addBreadcrumb($helper->__('Ogone Postsale Requests'), $helper->__('Ogone Postsale Requests'));
		
		$this->renderLayout();
	}
	
	public function reprocessAction(){
		$id = $this->getRequest()->getParam('id');
		
		/* @var $postsale Storefront_PayOgone_Model_Postsale_Request */
		$postsale = Mage::getModel('ogone/postsale_request')->load($id);
		
		$postsale->run();
		
		$postsale->applyToResponse($this->getResponse());
		
		$postsale->save();
	}
	
	public function exportAction(){
		$id = $this->getRequest()->getParam('id');
	
		/* @var $postsale Storefront_PayOgone_Model_Postsale_Request */
		$postsale = Mage::getModel('ogone/postsale_request')->load($id);
	
		$export = $postsale->getExportString();
		die($export);
	}
	
	public function importAction(){
		$data = $this->getRequest()->getParam('data');
	
		/* @var $postsale Storefront_PayOgone_Model_Postsale_Request */
		$postsale = Mage::getModel('ogone/postsale_request');
	
		$postsale->restoreFromExport($data);
		$postsale->save();
		
		$this->_redirect('payogone/adminhtml_postsale/');
	}
	
}