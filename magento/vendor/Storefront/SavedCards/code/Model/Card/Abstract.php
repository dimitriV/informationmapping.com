<?php
abstract class Storefront_SavedCards_Model_Card_Abstract{
	
	const CARD_TYPE_VISA = 'visa';
	const CARD_TYPE_MASTERCARD = 'mc';
	const CARD_TYPE_AMEX = 'amex';
	
	
	abstract public function getId();
	
	/**
	 * Get the formatted card number
	 */
	abstract public function getCard();
	abstract public function getCardType();	
	abstract public function getIsPrimary();
	abstract public function getExpiryDate();
	
	/**
	 * Get the Magento payment method code used in order payments
	 */
	abstract public function getPaymentMethodCode();
	
	/**
	 * @return Zend_Date
	 */
	abstract public function getLastUsedDate();
	
	abstract public function makePrimary();
	
	abstract public function delete();
}