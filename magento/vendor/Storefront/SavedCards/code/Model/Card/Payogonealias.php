<?php
class Storefront_SavedCards_Model_Card_Payogonealias extends Storefront_SavedCards_Model_Card_Abstract{
	
	private $_alias = null;
	
	public function __construct($args){
		$this->_setAlias($args[0]);
	}
	
	protected function _setAlias(Storefront_PayOgoneAlias_Model_Alias $alias){
		$this->_alias = $alias;
	}
	
	/**
	 * @return Storefront_PayOgoneAlias_Model_Alias
	 */
	public function getAlias(){
		return $this->_alias;
	}
	
	public function getId(){
		return 'payogonealias-'.$this->getAlias()->getId();
	}
	
	/**
	 * Get the formatted card number
	 */
	public function getCard(){
		$card = $this->getAlias()->getCard();
		
		$pm = $this->getAlias()->getPaymentMethod();
		
		switch($pm){
			case 'ogone_visa':
			case 'ogone_mastercard':
				// Parse VISA and MasterCard to more readable format
				$r = substr($card, 0, 4).'-'.substr($card, 4, 4).'-'.substr($card, 8, 4).'-'.substr($card, 12, 4);
				break;
				
			default:
				// Leave American Express as standard format
				$r = $card;
		}
		
		return $r;
	}
	
	public function getCardType(){
		$pm = $this->getAlias()->getPaymentMethod();
		
		switch($pm){
			case 'ogone_visa':
				return self::CARD_TYPE_VISA;
			case 'ogone_mastercard':
				return self::CARD_TYPE_MASTERCARD;
			case 'ogone_americanexpress':
				return self::CARD_TYPE_AMEX;
			default:
				Mage::throwException('Unknown card type "'.$pm.'"');
		}
	}
	
	public function getIsPrimary(){
		return $this->getAlias()->getIsPrimary();
	}
	
	public function getExpiryDate(){
		return substr($this->getAlias()->getExpiryDate(), 0, 7);
	}
	

	public function getLastUsedDate(){
		$dateString = $this->getAlias()->getLastUsedAt();
		return new Zend_Date($dateString, Zend_Date::ISO_8601);
	}
	
	public function makePrimary(){
		// TODO move this away from PayOgoneAlias into SavedCards module
		return $this->getAlias()->makePrimary();
	}
	
	public function delete(){
		$this->getAlias()->delete();
	}
	
	/**
	 * Get the Magento payment method code used in order payments
	 */
	public function getPaymentMethodCode(){
		return 'ogone_alias';
	}
}