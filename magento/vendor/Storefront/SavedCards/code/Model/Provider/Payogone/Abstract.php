<?php

abstract class Storefront_SavedCards_Model_Provider_Payogone_Abstract extends Storefront_SavedCards_Model_Provider_Abstract {

	abstract protected function _getOgonePm ();

	abstract protected function _getOgoneBrand ();

	abstract protected function _getMagentoPaymentMethod ();

	public function getAddCardHtml () {
		$r = '';
		
		$storeId = Mage::app()->getStore()->getId();
		
		/* @var $config Storefront_PayOgone_Model_Config */
		$config = Mage::getSingleton('ogone/config');
		
		/* @var $customerHelper Mage_Customer_Helper_Data */
		$customerHelper = Mage::helper('customer');
		$customer = $customerHelper->getCustomer();
		
		$orderId = 'create-alias-' . $customer->getId() . '-' . $storeId . '-' . time() . '-' . sprintf('%\'.05d', mt_rand(0, 99999));
		
		$acceptUrl = Mage::getUrl('savedcards/card/success');
		$declineUrl = Mage::getUrl('savedcards/card/decline');
		$cancelUrl = Mage::getUrl('savedcards/card/cancel');
		$exceptionUrl = Mage::getUrl('savedcards/card/exception');
		
		$formFields = array();
		
		// Authorize mode instead of Sale
		$formFields['operation'] = 'RES';
		
		$formFields['amount'] = 0;
		$formFields['currency'] = $this->_getCurrencyCode();
		$formFields['orderID'] = $orderId;
		$formFields['PSPID'] = $config->getPSPID();
		$formFields['USERID'] = $config->getUSERID();
		$formFields['language'] = Mage::app()->getLocale()->getLocaleCode();
		$formFields['CN'] = $customer->getFirstname() . ' ' . $customer->getLastname();
		$formFields['EMAIL'] = $customer->getEmail();
		// $formFields['homeurl'] = $config->getHomeUrl($storeId);
		// $formFields['catalogurl'] = $config->getHomeUrl($storeId);
		
		$formFields['accepturl'] = $acceptUrl;
		$formFields['declineurl'] = $declineUrl;
		$formFields['exceptionurl'] = $exceptionUrl;
		$formFields['cancelurl'] = $cancelUrl;
		
		// Example: 'ogone_visa'
		$paymentMethod = $this->_getMagentoPaymentMethod();
		
		$helper = Mage::helper('savedcards');
		
		$alias = $paymentMethod . '-'.$customer->getId().'-'. mt_rand(0, 99999);
		$aliasUsage = $helper->__('This card will be added to your account for future use. You will not be charged for this.');
		
		$formFields['ALIAS'] = $alias;
		$formFields['ALIASUSAGE'] = $aliasUsage;
		
		$formFields['PM'] = $this->_getOgonePm();
		$formFields['BRAND'] = $this->_getOgoneBrand();
		
		// ogone template
		if ($config->getConfigData('ogone/template/template') == 'ogone') {
			$formFields['TP'] = '';
		} else {
			$formFields['TP'] = $config->getPaymentTemplate();
		}
		
		/* @var $ogoneHelper Storefront_PayOgone_Helper_Data */
		$ogoneHelper = Mage::helper('ogone');
		
		// must come last for logic above
		$shaSecret = $config->getShaInCode($storeId);
		$formFields['SHASign'] = $ogoneHelper->generateShaSignature($formFields, $shaSecret, $storeId);
		
		$coreHelper = Mage::helper('core');
		
		$ogoneUrl = $config->getGatewayPath($storeId);
		// $ogoneUrl = 'https://secure.ogone.com/ncol/prod/alias_gateway.asp';
		
		$r .= '<div class="btn"><form action="' . $coreHelper->escapeHtml($ogoneUrl) . '" method="post" enctype="application/x-www-form-urlencoded">';
		
		foreach ($formFields as $name => $value) {
			
			$escapedName = $coreHelper->escapeHtml($name);
			$escapedValue = $coreHelper->escapeHtml($value);
			
			$r .= '<input type="hidden" name="' . $escapedName . '" value="' . $escapedValue . '" />' . "\n";
		}
		
		$buttonLabel = $this->_getButtonLabel();
		
		$r .= '<button class="btn btn-default pull-left" type="submit">' . $coreHelper->escapeHtml($buttonLabel) . '</button>';
		
		$r .= '</form></div>';
		
		return $r;
	}

	protected function _getCurrencyCode () {
		/* @var $config Storefront_PayOgone_Model_Config */
		$config = Mage::getSingleton('ogone/config');
		
		if ($config->getBaseCurrency()) {
			return Mage::app()->getStore()->getBaseCurrencyCode();
		} else {
			return Mage::app()->getStore()->getCurrentCurrencyCode();
		}
	}
}