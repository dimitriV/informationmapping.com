<?php
class Storefront_SavedCards_Model_Provider_Payogone_Mastercard extends Storefront_SavedCards_Model_Provider_Payogone_Abstract{
	
	protected function _getOgonePm(){
		return 'CreditCard';
	}
	
	protected function _getOgoneBrand(){
		return 'MASTERCARD';
	}
	
	protected function _getButtonLabel(){
		return Mage::helper('savedcards')->__('Add MasterCard');
	}
	
	protected function _getMagentoPaymentMethod(){
		return 'ogone_mastercard';
	}
	
}