<?php
class Storefront_SavedCards_Model_Provider_Payogone_Amex extends Storefront_SavedCards_Model_Provider_Payogone_Abstract{
	
	protected function _getOgonePm(){
		return 'CreditCard';
	}
	
	protected function _getOgoneBrand(){
		return 'AMEX';
	}
	
	protected function _getButtonLabel(){
		return Mage::helper('savedcards')->__('Add American Express Card');
	}
	
	protected function _getMagentoPaymentMethod(){
		return 'ogone_americanexpress';
	}
	
}