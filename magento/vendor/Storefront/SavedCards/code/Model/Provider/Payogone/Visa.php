<?php
class Storefront_SavedCards_Model_Provider_Payogone_Visa extends Storefront_SavedCards_Model_Provider_Payogone_Abstract{
	
	protected function _getOgonePm(){
		return 'CreditCard';
	}
	
	protected function _getOgoneBrand(){
		return 'VISA';
	}
	
	protected function _getButtonLabel(){
		return Mage::helper('savedcards')->__('Add VISA Card');
	}
	
	protected function _getMagentoPaymentMethod(){
		return 'ogone_visa';
	}
	
}