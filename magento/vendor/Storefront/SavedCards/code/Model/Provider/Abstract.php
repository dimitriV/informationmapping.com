<?php
abstract class Storefront_SavedCards_Model_Provider_Abstract {

	abstract public function getAddCardHtml ();
	
	abstract protected function _getButtonLabel(); 
}