<?php

class Storefront_SavedCards_Model_Observer {

	public function electPrimarySavedCard ($observer) {
		$customerId = $observer->getCustomerId();
		
		/* @var $helper Storefront_SavedCards_Helper_Data */
		$helper = Mage::helper('savedcards');
		
		$allCards = $helper->getCards($customerId);
		
		if (count($allCards) > 0) {
			
			$found = false;
			foreach ($allCards as $card) {
				if ($card->getIsPrimary()) {
					$found = true;
				}
				break;
			}
			
			// There is no primary card...
			if (! $found) {
				
				$firstCard = $allCards[0];
				$firstCard->makePrimary();
			}
		}
	}
}