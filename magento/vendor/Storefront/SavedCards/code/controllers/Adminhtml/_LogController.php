<?php
class Storefront_PayOgone_Adminhtml_LogController extends Mage_Adminhtml_Controller_Action{
	
	
	public function postsaleAction(){
		$this->_title($this->__('Ogone Postsale Logs'));
		
		$helper = Mage::helper('ogone');
		
		$this->loadLayout();
		
		$block = $this->getLayout()->createBlock('ogone/adminhtml_log_postsale');
		
		$this->_setActiveMenu('system/ogone_logs')
		->_addBreadcrumb($helper->__('Ogone Postsale Logs'), $helper->__('Ogone Postsale Logs'));
		
		$this->renderLayout();
	}
	
}