<?php

class Storefront_SavedCards_CardController extends Mage_Core_Controller_Front_Action {
	
	/**
	 * Action predispatch
	 *
	 * Check customer authentication for some actions
	 */
	public function preDispatch()
	{
		parent::preDispatch();
		if (!Mage::getSingleton('customer/session')->authenticate($this)) {
			$this->setFlag('', 'no-dispatch', true);
		}
	}
	
	public function indexAction(){
		$this->_title($this->__('My Credit Cards'));
		$this->loadLayout();
		$this->_initLayoutMessages('customer/session');
		
		$this->renderLayout();
	}
	
	
	public function makeprimaryAction(){
		$cardId = $this->getRequest()->getParam('id');
		
		$allCards = Mage::helper('savedcards')->getCards();
		
		$found = false;
		
		foreach($allCards as $card){
			if($card->getId() == $cardId){
				$card->makePrimary();
				$found = true;
				break;
			}
		}
		
		if($found){
			
			// TODO add success message
			
			$this->_redirect('savedcards/card/');
		}else{
			$this->_forward('noRoute');
		}
	}
	
	public function deleteAction(){
		$cardId = $this->getRequest()->getParam('id');
	
		$allCards = Mage::helper('savedcards')->getCards();
	
		$found = false;
		$cardIsPrimary = false;
		
		foreach($allCards as $card){
			if($card->getId() == $cardId){
				
				if($card->getIsPrimary()){
					$cardIsPrimary = true;
				}
				$card->delete();
				$found = true;
				break;
			}
		}
		
		// If we deleted the primary, we need to set a new primary
		if($cardIsPrimary){
			$allCards = Mage::helper('savedcards')->getCards();
			
			$hasPrimary = false;
			foreach($allCards as $card){
				if($card->getIsPrimary()){
					$hasPrimary = true;
					break;
				}
			}
			
			if(!$hasPrimary && isset($allCards[0])){
				$allCards[0]->makePrimary();
			}
		}
		
	
		if($found){
			$this->_redirect('savedcards/card/');
		}else{
			$this->_forward('noRoute');
		}
	}
	
	public function successAction(){
		$this->_redirect('*/*/');
	}
}
