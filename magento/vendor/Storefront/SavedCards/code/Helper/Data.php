<?php

class Storefront_SavedCards_Helper_Data extends Storefront_VendorLoader_Helper_Abstract {

	public function getModuleName () {
		return 'Storefront_SavedCards';
	}
	
	public function getSavedCardProviders(){
		$r = array();
		
		$r[] = Mage::getSingleton('savedcards/provider_payogone_visa');
		$r[] = Mage::getSingleton('savedcards/provider_payogone_mastercard');
		$r[] = Mage::getSingleton('savedcards/provider_payogone_amex');
		
		return $r;
	}
	
	public function getPrimaryCard($customerId = null){
		if($customerId === null){
			$customerId = Mage::helper('customer')->getCurrentCustomer()->getId();
		}
		
		$cards = $this->getCards($customerId);
		if(count($cards) > 0){
			
			$primaryCard = null;
			
			foreach($cards as $card){
				/* @var $card Storefront_SavedCards_Model_Card_Abstract */
				
				if($primaryCard && $card->getIsPrimary()){
					// Found a 2nd primary card - reset the original is only primary
					$primaryCard->makePrimary();
					
				}else{
					if($card->getIsPrimary()){
						$primaryCard = $card;
					}
				}
			}
			
			if($primaryCard == null){
				$firstCard = reset($cards);
				$firstCard->makePrimary();
				return $firstCard;
			}else{
				return $primaryCard;
			}
			
		}
		return false;
	}
	
	public function getCards($customerId = null){
		if($customerId === null){
			$customerId = Mage::helper('customer')->getCurrentCustomer()->getId();
		}
		$cards = new ArrayObject();
	
		Mage::dispatchEvent('saved_cards_load_for_customer', array('cards' => $cards, 'customer_id' => $customerId));
	
		$r = array();
		foreach($cards as $card){
			if(get_class($card) == 'Storefront_PayOgoneAlias_Model_Alias'){
				$r[] = Mage::getModel('savedcards/card_payogonealias', array($card));
			}
		}
	
		return $r;
	}

}