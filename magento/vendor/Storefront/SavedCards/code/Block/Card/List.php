<?php
class Storefront_SavedCards_Block_Card_List extends Mage_Core_Block_Template{
	protected $_template = 'savedcards/card/list.phtml';
	
	public function getCards(){
		return Mage::helper('savedcards')->getCards();
	}
	
	public function getAddCardHtml(){
		/* @var $helper Storefront_SavedCards_Helper_Data */
		$helper = Mage::helper('savedcards');
		
		$savedCardProviders = $helper->getSavedCardProviders();
		
		$r = '';
		
		foreach($savedCardProviders as $savedCardProvider){
			/* @var $savedCardProvider Storefront_SavedCards_Model_Provider_Abstract */
			$r .= $savedCardProvider->getAddCardHtml();
		}
		
		return $r;
		
	}
}