<?php
class Storefront_ImiSubscriptions_Model_Observer{
	
	public function loadSubscriptionsForCustomer($observer){
		$customerId = $observer->getCustomerId();
		$allSubscriptions = $observer->getSubscriptions();
		
		
		
		/* @var $helper Storefront_ImiSubscriptions_Helper_Data */
		$helper = Mage::helper('imisubscriptions');
		
		$mySubscriptions = $helper->getSubscriptions($customerId);
		
		foreach($mySubscriptions as $subscription){
			$allSubscriptions[] = $subscription;
		}
	}
	
	public function loadSubscriptionById($observer){
		$subscriptionId = $observer->getSubscriptionId();
		$resultArray = $observer->getResult();
		
		/* @var $helper Storefront_ImiSubscriptions_Helper_Data */
		$helper = Mage::helper('imisubscriptions');
		
		// In IMI, we don't use the prefix.
		$subscriptionId = substr($subscriptionId, 4);
	
		$foundSubscription = $helper->getSubscriptionById($subscriptionId);
	
		if($foundSubscription){
			$resultArray[] = $foundSubscription;
		}
	}
	
	public function loadExpiredSubscriptions($observer){
		$allSubscriptions = $observer->getSubscriptions();
		
		/* @var $helper Storefront_ImiSubscriptions_Helper_Data */
		$helper = Mage::helper('imisubscriptions');
		
		$mySubscriptions = $helper->getExpiredSubscriptions();
		
		foreach($mySubscriptions as $subscription){
			$allSubscriptions[] = $subscription;
		}
		
	}
	
	public function onRenewSuccess($observer){
		
		$subscription = $observer->getSubscription();
		
		if($subscription instanceof Storefront_ImiSubscriptions_Model_Subscription_Imi){
			/* @var $subscription Storefront_ImiSubscriptions_Model_Subscription_Imi */
			$sql = 'update license_info set auto_renewed_at = NOW() where license_id = ? limit 1';
			
			$resource = Mage::getSingleton('core/resource');
			/* @var $readConnection Magento_Db_Adapter_Pdo_Mysql */
			$readConnection = $resource->getConnection('core_read');
			
			$stmt = $readConnection->query($sql, array($subscription->getOriginalId()));
			
			
		}
		
	}
	
}