<?php

class Storefront_ImiSubscriptions_Helper_Data extends Storefront_VendorLoader_Helper_Abstract {

	public function getModuleName () {
		return 'Storefront_ImiSubscriptions';
	}


	/**
	 * Load all expired subscriptions, but give up after 7 days so not to keep processing old records 
	 * @return array
	 */
	public function getExpiredSubscriptions(){

        Mage::throwException('need to re-implement getExpiredSubscriptions()');

//		$giveUpDays = 7;
//
//		$sql = 'select * from (
//				SELECT datediff(li.expiration_date, now()) days_til_expiration, lk.license_keyid, li.auto_renew, li.license_id, o.customer_id, ls.activation_sku renew_product_sku, lk.product_sku original_product_sku, license_key, li.initial_purchase_date, li.expiration_date
//FROM license_info li
//inner join license_orders lo on li.license_id = lo.license_id
//inner join sales_flat_order o on lo.order_id = o.increment_id
//inner join license_keys lk on li.license_id = lk.license_id
//inner join license_skus ls on lk.product_sku = ls.license_sku
//where li.product_type=\'subscription\' and (li.auto_renewed_at is null or li.auto_renewed_at < li.expiration_date)
//) q1 where q1.days_til_expiration <= 0 and q1.days_til_expiration > -'.$giveUpDays;
//
//		$resource = Mage::getSingleton('core/resource');
//		/* @var $readConnection Magento_Db_Adapter_Pdo_Mysql */
//		$readConnection = $resource->getConnection('core_read');
//
//		$rows = $readConnection->fetchAll($sql);
//
//		$r = $this->_parseRows($rows);
//
//		return $r;
	}
	
	public function getSubscriptionById($id){

        /* @var $col Storefront_Subscriptions_Model_Resource_Subscription_Collection */
        $col = Mage::getResourceModel('subscriptions/subscription_collection');

        $col->addFieldToFilter('id', $id);

        return $col;


//		$sql = 'SELECT lk.license_keyid, li.auto_renew, li.license_id, o.customer_id, ls.activation_sku renew_product_sku, lk.product_sku original_product_sku, license_key, li.initial_purchase_date, li.expiration_date
//FROM license_info li
//inner join license_orders lo on li.license_id = lo.license_id
//inner join sales_flat_order o on lo.order_id = o.increment_id
//inner join license_keys lk on li.license_id = lk.license_id
//inner join license_skus ls on lk.product_sku = ls.license_sku
//where li.product_type=\'subscription\'
//and li.license_id = ? limit 1';
//
//		$resource = Mage::getSingleton('core/resource');
//		/* @var $readConnection Magento_Db_Adapter_Pdo_Mysql */
//		$readConnection = $resource->getConnection('core_read');
//
//		$rows = $readConnection->fetchAll($sql, array($id));
//
//		$r = $this->_parseRows($rows);
//
//		if(count($r) > 0){
//			return $r[0];
//		}else{
//			return null;
//		}
		
	}
	
	public function getSubscriptions($customerId){

        /* @var $col Storefront_Subscriptions_Model_Resource_Subscription_Collection */
        $col = Mage::getResourceModel('subscriptions/subscription_collection');

        $col->joinOrder();
        $col->addFieldToFilter('customer_id', $customerId);

        return $col;

//		$sql = 'SELECT lk.license_keyid, li.auto_renew, li.license_id, o.customer_id, ls.activation_sku renew_product_sku, lk.product_sku original_product_sku, license_key, li.initial_purchase_date, li.expiration_date
//FROM license_info li
//inner join license_orders lo on li.license_id = lo.license_id
//inner join sales_flat_order o on lo.order_id = o.increment_id
//inner join license_keys lk on li.license_id = lk.license_id
//inner join license_skus ls on lk.product_sku = ls.license_sku
//where li.product_type=\'subscription\'
//and o.customer_id = ?';
//
//		$resource = Mage::getSingleton('core/resource');
//		/* @var $readConnection Magento_Db_Adapter_Pdo_Mysql */
//		$readConnection = $resource->getConnection('core_read');
//
//		$rows = $readConnection->fetchAll($sql, array($customerId));
//
//		$r = $this->_parseRows($rows);
//
//		return $r;
	}

}