<?php
// Set INI variables and error reporting
error_reporting(E_ALL);
ini_set('display_errors', 1);
ini_set("soap.wsdl_cache_enabled", 0);
ini_set("session.auto_start", 0);

require("../app/Mage.php");
$app = Mage::app();

$baseUrl = Mage::getBaseUrl(Mage_Core_Model_Store::URL_TYPE_WEB);
$wsdl    = $baseUrl . 'ws/?wsdl';

if (isset($_GET['wsdl'])) {
	header("Content-type: text/xml;");
	echo file_get_contents("wsdl.wsdl");
	die();
}

if (isset($_GET['phpinfo'])) {
	phpinfo();
	die();
}

/**
 * The class to perform required server side actions of the web service.
 *
 * @package 	AutoInsliteService
 * @author 	    Narendra Raj Ojha <narendraraj.ojha@itoffshorenepal.com>
 * @copyright 	ITOffshore Nepal
 * @version 	1.0
 */
class AutoInsliteService
{
    private $username, $password;
    
	public function __construct()
	{
		$this->username = 'imiadmin';
        $this->password = 'vlinder467';
	}

	/**
	 * The function To get License Information
	 *
	 * @param string $data, $filters
	 *
	 * @return string
	 */
	public function customwebserviceApiLicenseinfo($data, $filters = null)
	{
		$userinfo = json_decode($data);
        $user     = $userinfo->user;
		$pwd      = $userinfo->pass;
        
        if ($user === $this->username && $pwd === $this->password) {
			$serialCollections = Mage::getModel('serialproduct/serial')->getCollection(); /* Old table */
            //$serialCollections = Mage::getModel('licensemanager/licensekeys')->getCollection(); /* New table */
            if(count($filters) && $filters !== null){
                $filters = json_decode($filters);
                foreach($filters as $key => $value){
                    $serialCollections->addFieldToFilter($key, $value);
                }
            }
            
            $data = $serialCollections->getData();
            return json_encode($data);
		} else {
			return json_encode('Incorrect login!');
		}
	}
    
    /**
	 * The function To get SMA Subscription Information
	 *
	 * @param string $data, $filters
	 *
	 * @return string
	 */
	public function customwebserviceApiSmainfo($data, $filters = null)
	{
		$userinfo = json_decode($data);
        $user     = $userinfo->user;
		$pwd      = $userinfo->pass;
        
        if ($user === $this->username && $pwd === $this->password) {
			$smaCollections = Mage::getModel('smasubscription/smasubscription')->getCollection(); /* Old table */
            //$smaCollections = Mage::getModel('licensemanager/licenserenewal')->getCollection(); /* New table */
            if(count($filters) && $filters !== null){
                $filters = json_decode($filters);
                foreach($filters as $key => $value){
                    $smaCollections->addFieldToFilter($key, $value);
                }
            }
            
            $data = $smaCollections->getData();
            return json_encode($data);
		} else {
			return json_encode('Incorrect login!');
		}
	}	
}

$server = new SoapServer($wsdl);
$server->setClass('AutoInsliteService');
$server->handle();