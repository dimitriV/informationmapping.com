<?php
// Set INI variables and error reporting
error_reporting(E_ALL);
ini_set('display_errors', 1);
ini_set("soap.wsdl_cache_enabled", 0);
ini_set("session.auto_start", 0);

require("../../app/Mage.php");
$app = Mage::app();


$baseUrl = Mage::getBaseUrl(Mage_Core_Model_Store::URL_TYPE_WEB);
$wsdl    = $baseUrl . 'ws/?wsdl';

/**
 * make a request to the web service server
 */
$client = new SoapClient($wsdl, array("trace" => 1, "exception" => 1));

$userinfo       = json_encode(array('user' => 'imiadmin', 'pass' => 'vlinder467'));
$licensefilters = json_encode(array('product_id' => 23, 'customer_id' => 9, 'order_id' => 330));
$smafilters     = json_encode(array('order_no' => '100000225'));
/**
 * Call the web service now.
 */
try {
	$licensedata = $client->customwebserviceApiLicenseinfo($userinfo, $licensefilters);
    $smadata = $client->customwebserviceApiSmainfo($userinfo);
    echo "<pre>";
    print_r(json_decode($licensedata)); 
    print_r(json_decode($smadata));
    echo "</pre>";
} catch (Exception $e) {
	echo $e->getMessage();
}