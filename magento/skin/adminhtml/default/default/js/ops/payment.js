responseIframe = null;
Event.observe(window, 'load', function() {
    payment.toggleOpsDirectDebitInputs = function(country) {
        var bankcode = 'ops_directdebit_bank_code';
        var showInput = function(id) {
            $$('#' + id)[0].up().show();
            $(id).addClassName('required-entry');
        };
        var hideInput = function(id) {
            $$('#' + id)[0].up().hide();
            $(id).removeClassName('required-entry');
        };
        if ('NL' == country) {
            hideInput(bankcode);
        }
        if ('DE' == country || 'AT' == country) {
            showInput(bankcode);
        }
    };

    payment.switchMethod = payment.switchMethod.wrap(function (originalSwitchMethod, method) {
        var iframe = $('ops_iframe_ops_cc');
        var doc = null;

        if(iframe != null && iframe.contentDocument) {
            doc = iframe.contentDocument;
        } else if(iframe != null && iframe.contentWindow) {
            doc = iframe.contentWindow.document;
        } else if(iframe != null && iframe.document) {
            doc = iframe.document;
        }
        if (method == 'ops_cc' && (iframe == null || doc.body.innerHTML.length == 0)) {
            document.location.reload();
        }
       return originalSwitchMethod(method);
    });

    order.loadArea = order.loadArea.wrap (function (originalLoadArea, area, indicator, params) {

        if (order.paymentMethod && order.paymentMethod.substr(0,3) == 'ops') {
            var billing_methodIndex = area.indexOf('billing_method');
            if (billing_methodIndex > -1) {
                area.splice(billing_methodIndex, 1);
            }
        }
        originalLoadArea(area, indicator, params);
    });

    order.submit = order.submit.wrap(function (originalSubmitMethod) {
        if ('ops_cc' != order.paymentMethod) {
            console.log('running original method');
            return originalSubmitMethod();
        }
        order.originalSubmitMethod = originalSubmitMethod;
        var iframe = $('ops_iframe_ops_cc');
        var doc = null;

        if(iframe.contentDocument) {
            doc = iframe.contentDocument;
        } else if(iframe.contentWindow) {
            doc = iframe.contentWindow.document;
        } else if(iframe.document) {
            doc = iframe.document;
        }
        var form = doc.forms[0];

        order.opsCcFormData["CN"]                = form['CN'].value;
        order.opsCcFormData["CARDNO"]            = form['CARDNO'].value;
        order.opsCcFormData["CVC"]               = form['CVC'].value;
        order.opsCcFormData["ED_MONTH_SELECTOR"] = form['ED_MONTH_SELECTOR'].value;
        order.opsCcFormData["ED_YEAR_SELECTOR"]  = form['ED_YEAR_SELECTOR'].value;

        new Ajax.Request(opsHashUrl, {
            method: 'get',
            parameters: {
                orderid:   form["ORDERID"].value,
                paramplus: form["PARAMPLUS"].value,
                alias:     form["ALIAS"].value,
                storeId:   storeId,
                isAdmin:   1
            },
            onSuccess: function(transport) {
                var data = transport.responseText.evalJSON();
                form["SHASIGN"].value = data.hash;
                form["ED"].value = form["ED_MONTH_SELECTOR"].value + form["ED_YEAR_SELECTOR"].value;
                form.removeChild(form["ED_MONTH_SELECTOR"]);
                form.removeChild(form["ED_YEAR_SELECTOR"]);

                iframe.alreadySet = 'true';

                form.submit();
                iframe.style.visibility = "hidden";
                doc.body.innerHTML = '{ "result" : "waiting" }';
                setTimeout("order.processOpsResponse(2000)", 2000);//500
            }
        });
    });

    order.opsCcFormData=[];

    order.processOpsResponse = function(timeOffset) {
        try {
            var responseIframe = $('ops_iframe_ops_cc');
            var responseResult;

            /* payment fails after 30s without response */
            var maxOffset = 50000;

            if(responseIframe.contentDocument) {
                responseResult = responseIframe.contentDocument;
            } else if(responseIframe.contentWindow) {
                responseResult = responseIframe.contentWindow.document;
            } else if(responseIframe.document) {
                responseResult = responseIframe.document;
            }

            //Remove links in JSON response
            //can happen f.e. on iPad <a href="tel:0301125679">0301125679</a> if alias is interpreted as a phone number
            var htmlResponse = responseResult.body.innerHTML.replace(/<a\b[^>]*>/i, '');
            htmlResponse = htmlResponse.replace(/<\/a>/i, '');

            if ("undefined" == typeof(responseResult)) {
                currentStatus = '{ "result" : "waiting" }'.evalJSON();
            } else {
                var currentStatus = htmlResponse.evalJSON();
                if ("undefined" == typeof(currentStatus) || "undefined" == typeof(currentStatus.result)) {
                    currentStatus = '{ "result" : "waiting" }'.evalJSON();
                }
            }
        } catch (e) {
            currentStatus = '{ "result" : "waiting" }'.evalJSON();
        }

        if ('waiting' == currentStatus.result && timeOffset <= maxOffset) {
            setTimeout("order.processOpsResponse(" + (2000+timeOffset) + ")", 2000);//500
            return false;
        } else if ('success' == currentStatus.result) {
            /* show form again, just to have it ready if some other validation fails */
            responseIframe.style.visibility = "visible";
            ops_iframe_ops_cc_prepare(order.opsCcFormData);

            /* submit order */
            order.originalSubmitMethod();

            return true;
        } else {
            responseIframe.style.visibility = "visible";
            ops_iframe_ops_cc_prepare(order.opsCcFormData);
        }

        /*alert(Translator.translate('Payment failed. Please review your input or select another payment method.'));
        return false;*/
    };

    order.dataLoaded = function() {
        this.dataShow();
        if (responseIframe) {
            responseIframe.style.visibility = "visible";
            setTimeout("ops_iframe_ops_cc_prepare(order.opsCcFormData)", 2000);//500
        }
    }
});

