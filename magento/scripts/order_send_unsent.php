<?php
/**
 * Yireo cronjob for Magento to send any order-emails that have not been sent yet
 *
 * @author      Yireo (http://www.yireo.com/)
 * @copyright   Copyright (c) 2014 Yireo (http://www.yireo.com/)
 * @license     Open Source License
 */

require_once dirname(__FILE__).'/../app/Mage.php';

Mage::app();
Mage::app()->setCurrentStore(Mage_Core_Model_App::ADMIN_STORE_ID);

$from_date = date('Y-m-d', time() - (60*60*24));
$orders = Mage::getModel('sales/order')->getCollection()
    ->addAttributeToFilter('created_at', array('from' => $from_date))
    ->addAttributeToFilter('email_sent', array('null' => true))
;

foreach($orders as $order) {
    if($order->getCanSendNewEmailFlag() != true) continue;
    if(in_array($order->getStatus(), array('canceled', 'refunded', 'closed'))) continue;
    if($order->getEmailSent() == true) continue;
    $order->sendNewOrderEmail();
    $order->setEmailSent(1);
    $order->save();
}

