<?php
/**
 * Yireo cronjob for Magento to send any order-emails that have not been sent yet
 *
 * @author      Yireo (http://www.yireo.com/)
 * @copyright   Copyright (c) 2014 Yireo (http://www.yireo.com/)
 * @license     Open Source License
 */

exit;
require_once dirname(__FILE__).'/../app/Mage.php';

Mage::app();
Mage::app()->setCurrentStore(Mage_Core_Model_App::ADMIN_STORE_ID);

$from_date = date('Y-m-d', time() - (60*60*24));
$invoices = Mage::getModel('sales/order_invoice')->getCollection()
    ->addAttributeToFilter('created_at', array('from' => $from_date))
    ->addAttributeToFilter('email_sent', array('null' => true))
;

foreach($invoices as $invoice) {
    $order = $invoice->getOrder();
    if($order->getStatus() != 'complete') continue;
    if($invoice->getEmailSent() == true) continue;
    $invoice->sendEmail(true);
}

