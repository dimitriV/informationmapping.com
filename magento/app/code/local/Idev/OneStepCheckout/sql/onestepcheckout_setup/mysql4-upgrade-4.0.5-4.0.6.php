<?php
$installer = $this;
$installer->startSetup();

$table = $this->getTable('sales_flat_order');
$query = 'ALTER TABLE `' . $table . '` ADD COLUMN `imi_customer_email` VARCHAR(100) DEFAULT NULL, ADD COLUMN `imi_customer_firstname` VARCHAR(100) DEFAULT NULL';
$connection = Mage::getSingleton('core/resource')->getConnection('core_write');
$connection->query($query);

$installer->endSetup();
?>