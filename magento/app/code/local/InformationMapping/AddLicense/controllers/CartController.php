<?php
/**
 * InformationMapping AddLicense
 *
 * @package     InformationMapping_AddLicense
 * @author      InformationMapping (http://www.information-mapping/)
 * @copyright   Copyright (c) 2013 InformationMapping (http://www.information-mapping/)
 * @license     Open Source License
 */

/**
 * AddLicense cart-controller
 */
class InformationMapping_AddLicense_CartController extends Mage_Core_Controller_Front_Action
{
    public function addAction()
    {
        $id = (int)$_REQUEST['id'];
        $key = trim($_REQUEST['key']);

        // Redirect to cart
        $this->_redirect('checkout/cart/add', array('product' => $id, 'key' => $key));
    }
}
