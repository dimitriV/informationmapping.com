<?php
/**
 * InformationMapping AddLicense for Magento 
 *
 * @package     InformationMapping_AddLicense
 * @author      InformationMapping (http://www.information-mapping.com/)
 * @copyright   Copyright (c) 2013 InformationMapping (http://www.information-mapping.com/)
 * @license     Open Source License
 */

/**
 * AddLicense helper
 */
class InformationMapping_AddLicense_Helper_Data extends Mage_Core_Helper_Abstract
{
    public function isValidLicense($licenseKey)
    {
        $licenseKey = addslashes($licenseKey);
        $resource = Mage::getSingleton('core/resource');

        /* @var $db Magento_Db_Adapter_Pdo_Mysql */
        $db = $resource->getConnection('core_read');

        $query = 'SELECT `serial` FROM `subscriptions_subscription` WHERE `serial` = ?';
        $result = $db->fetchOne($query, array($licenseKey));

        if(!empty($result)) {
            return true;            
        }
        return false;
    }

    public function getActivations($licenseKey)
    {
        $resource = Mage::getSingleton('core/resource');
        $db = $resource->getConnection('core_read');

        $query = 'SELECT `qty_ordered` FROM `subscriptions_subscription` l INNER JOIN sales_flat_order_item oi ON oi.item_id = l.order_item_id WHERE l.serial = ?';
        $row = $db->fetchOne($query, array($licenseKey));
        if(!empty($row)) {
            return (int)$row;
        }
        return 0;
    }
}
