<?php
/**
 * AddLicense plugin for Magento 
 *
 * @package     InformationMapping_AddLicense
 * @author      InformationMapping (http://www.information-mapping.com/)
 * @copyright   Copyright (c) 2013 InformationMapping (http://www.information-mapping.com/)
 * @license     Open Source License
 */

class InformationMapping_AddLicense_Model_Observer
{
//    /*
//     * Method fired on the event <checkout_cart_add_product_complete>
//     *
//     * @access public
//     * @param Varien_Event_Observer $observer
//     * @return InformationMapping_AddLicense_Model_Observer
//     */
//    public function checkoutCartAddProductComplete($observer)
//    {
//        $product = $observer->getEvent()->getProduct();
//        $quote = Mage::getModel('checkout/cart')->getQuote();
//        $licenseKey = Mage::app()->getRequest()->getParam('key');
//
//        foreach($quote->getAllItems() as $quoteItem) {
//            if($quoteItem->getProductId() == $product->getId()) {
//                $this->saveLicenseQuoteItem($quoteItem->getId(), $quote->getId(), null, null, $licenseKey);
//            }
//        }
//
//        return $this;
//    }
//
//    /*
//     * Method fired on the event <sales_order_place_after>
//     *
//     * @access public
//     * @param Varien_Event_Observer $observer
//     * @return InformationMapping_AddLicense_Model_Observer
//     */
//    public function salesOrderPlaceAfter($observer)
//    {
//        $order = $observer->getEvent()->getOrder();
//
//        $quote = Mage::getModel('sales/quote')->load($order->getQuoteId());
//        if(!$quote->getId() > 0) {
//            return $this;
//        }
//
//        foreach($order->getAllItems() as $orderItem) {
//            foreach($quote->getAllItems() as $quoteItem) {
//                if($quoteItem->getProductId() == $orderItem->getProductId()) {
//                    $orderItem = Mage::getModel('sales/order_item')->load($quoteItem->getId(), 'quote_item_id');
//                    $this->saveLicenseQuoteItem($quoteItem->getId(), $quote->getId(), $orderItem->getId(), $order->getId());
//                }
//            }
//        }
//
//        return $this;
//    }
//
//    /*
//     * Method fired on the event <sales_order_save_after>
//     *
//     * @access public
//     * @param Varien_Event_Observer $observer
//     * @return InformationMapping_AddLicense_Model_Observer
//     */
//    public function salesOrderSaveAfter($observer)
//    {
//        $order = $observer->getEvent()->getOrder();
//        if($order->getState() != Mage_Sales_Model_Order::STATE_COMPLETE) {
//            return $this;
//        }
//
//        // Try to clean the license_quote_item-table first
//        $this->cleanLicenseQuoteItem();
//
//        foreach($order->getAllItems() as $orderItem) {
//            $licenseKey = $this->getLicenseQuoteItem($orderItem->getId());
//            if(!empty($licenseKey)) {
//
//                // Check whether the keys already exist
//                if($this->hasRenewHistoryLog($orderItem->getId())) {
//                    continue;
//                }
//
//                // Add data to the license_renew_history-table
//                $activationKeys = $this->getLicenseSerialNumbers($orderItem->getId());
//                foreach($activationKeys as $activationKey) {
//                    $activationKey = $activationKey['serial_number'];
//                    $this->addRenewHistoryLog($order->getId(), $order->getIncrementId(), $orderItem->getId(),
//                        $orderItem->getProductId(), $orderItem->getSku(), $licenseKey, $activationKey,
//                        $order->getCustomerId(), $order->getCustomerFirstname(), $order->getCustomerLastname(), $order->getCustomerEmail());
//                }
//
//                // Update the as_unique_key2013-table
//                $this->updateUniqueKey($licenseKey);
//
//                // Remove this entry in license_quote_item-table
//                $this->deleteLicenseQuoteItem($orderItem->getId());
//            }
//        }
//
//        return $this;
//    }
//
//    protected function hasRenewHistoryLog($orderItemId)
//    {
//        $resource = Mage::getSingleton('core/resource');
//        $db = $resource->getConnection('core_write');
//        $query = "SELECT `license_key` FROM `license_renew_history` WHERE `order_item_id`='$orderItemId'";
//        $result = $db->fetchOne($query);
//        if(!empty($result)) {
//            return true;
//        }
//        return false;
//    }
//
//    protected function addRenewHistoryLog($orderId, $orderIncrementId, $orderItemId, $productId, $productSku, $licenseKey, $activationKey, $customerId, $customerFirstname, $customerLastname, $customerEmail)
//    {
//        $resource = Mage::getSingleton('core/resource');
//        $db = $resource->getConnection('core_write');
//
//        $values = "`order_id`='$orderId',";
//        $values .= "`order_increment_id`='$orderIncrementId',";
//        $values .= "`order_item_id`='$orderItemId',";
//        $values .= "`product_id`='$productId',";
//        $values .= "`product_sku`='$productSku',";
//        $values .= "`license_key`='$licenseKey',";
//        $values .= "`activation_key`='$activationKey',";
//        $values .= "`customer_id`='$customerId',";
//        $values .= "`customer_firstname`='$customerFirstname',";
//        $values .= "`customer_lastname`='$customerLastname',";
//        $values .= "`customer_email`='$customerEmail'";
//
//        $query = "INSERT INTO `license_renew_history` SET $values";
//        //Mage::log('[InformationMapping_AddLicense] renew: '.$query);
//        $db->query($query);
//    }
//
//    protected function updateUniqueKey($licenseKey)
//    {
//        $resource = Mage::getSingleton('core/resource');
//        $db = $resource->getConnection('core_write');
//        $query = "UPDATE `as_unique_key2013` SET `renewed`='1' WHERE `lickey` = '$licenseKey'";
//
//        try {
//            $db->query($query);
//        } catch(Exception $e) {
//            // do nothing
//        }
//    }
//
//    protected function getLicenseSerialNumbers($orderItemId)
//    {
//        $resource = Mage::getSingleton('core/resource');
//        $db = $resource->getConnection('core_write');
//        $query = "SELECT `serial_number` FROM `license_serialnumbers` WHERE `status` = 'ASSIGNED' AND `order_item_id` = '$orderItemId'";
//        //Mage::log('[InformationMapping_AddLicense] license: '.$query);
//        $results = $db->fetchAll($query);
//        return $results;
//    }
//
//    protected function getLicenseQuoteItem($value, $name = 'order_item_id')
//    {
//        $resource = Mage::getSingleton('core/resource');
//        $db = $resource->getConnection('core_write');
//        $query = "SELECT `license_key` FROM `license_quote_item` WHERE `$name`='$value'";
//        $result = $db->fetchOne($query);
//        return $result;
//    }
//
//    protected function saveLicenseQuoteItem($quoteItemId = null, $quoteId = null, $orderItemId = null, $orderId = null, $licenseKey = null)
//    {
//        $resource = Mage::getSingleton('core/resource');
//        $db = $resource->getConnection('core_write');
//        $query = "SELECT `license_key` FROM `license_quote_item` WHERE `quote_item_id`='$quoteItemId'";
//        $result = $db->fetchOne($query);
//
//        $values = array();
//        if(!empty($quoteItemId)) $values[] = "`quote_item_id`='$quoteItemId'";
//        if(!empty($quoteId)) $values[] = "`quote_id`='$quoteId'";
//        if(!empty($orderItemId)) $values[] = "`order_item_id`='$orderItemId'";
//        if(!empty($orderId)) $values[] = "`order_id`='$orderId'";
//        if(!empty($licenseKey)) $values[] = "`license_key`='$licenseKey'";
//
//        if(empty($result)) {
//            $query = "INSERT INTO `license_quote_item` SET ".implode(',', $values);
//        } else {
//            $query = "UPDATE `license_quote_item` SET ".implode(',', $values)." WHERE `quote_item_id`='$quoteItemId'";
//        }
//        $db->query($query);
//    }
//
//    protected function deleteLicenseQuoteItem($value, $name = 'order_item_id')
//    {
//        $resource = Mage::getSingleton('core/resource');
//        $db = $resource->getConnection('core_write');
//        $query = "DELETE FROM `license_quote_item` WHERE `$name`='$value'";
//        $db->query($query);
//    }
//
//    protected function cleanLicenseQuoteItem()
//    {
//        $resource = Mage::getSingleton('core/resource');
//        $db = $resource->getConnection('core_write');
//        $query = "DELETE FROM `license_quote_item` WHERE `license_key` IS NULL";
//        $db->query($query);
//    }
}
