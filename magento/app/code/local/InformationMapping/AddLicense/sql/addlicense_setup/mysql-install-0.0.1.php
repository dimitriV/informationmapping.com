<?php
/**
 * InformationMapping AddLicense for Magento 
 *
 * @package     InformationMapping_AddLicense
 * @author      InformationMapping (http://www.information-mapping.com/)
 * @copyright   Copyright (c) 2013 InformationMapping (http://www.information-mapping.com/)
 * @license     Open Source License
 */

$installer = $this;
$installer->startSetup();
$installer->run("
CREATE TABLE IF NOT EXISTS `license_renew_history` (
    `id` int(10) unsigned default NULL AUTO_INCREMENT,
    `order_id` int(10) unsigned default NULL,
    `order_increment_id` int(20) unsigned default NULL,
    `order_item_id` int(10) unsigned default NULL,
    `product_id` int(10) unsigned default NULL,
    `product_sku` varchar(255) default NULL,
    `license_key` varchar(255) default NULL,
    `activation_key` varchar(255) default NULL,
    `customer_id` int(10) unsigned default NULL,
    `customer_firstname` varchar(255) default NULL,
    `customer_lastname` varchar(255) default NULL,
    `customer_email` varchar(255) default NULL,
    PRIMARY KEY  (`id`)
) ENGINE=MyISAM DEFAULT CHARSET=utf8;
");

$installer->run("
CREATE TABLE IF NOT EXISTS `license_quote_item` (
    `id` int(10) unsigned default NULL AUTO_INCREMENT,
    `quote_item_id` int(10) unsigned default NULL,
    `quote_id` int(10) unsigned default NULL,
    `order_item_id` int(10) unsigned default NULL,
    `order_id` int(10) unsigned default NULL,
    `license_key` varchar(255) default NULL,
    PRIMARY KEY  (`id`)
) ENGINE=MyISAM DEFAULT CHARSET=utf8;
");

$installer->endSetup();
