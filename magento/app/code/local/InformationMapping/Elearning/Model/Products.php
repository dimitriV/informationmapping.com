<?php
/**
 * Elearning plugin for Magento 
 *
 * @package     InformationMapping_Elearning
 * @author      InformationMapping (http://www.information-mapping.com/)
 * @copyright   Copyright (c) 2014 InformationMapping (http://www.information-mapping.com/)
 * @license     Open Source License
 */

class InformationMapping_Elearning_Model_Products extends Mage_Core_Model_Abstract
{
    /**
     * Constructor
     */
    protected function _construct()
    {
        parent::_construct();
        $this->_init('elearning/products');
    }
}
