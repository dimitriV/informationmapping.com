<?php

/**
 * Elearning plugin for Magento
 *
 * @package     InformationMapping_Elearning
 * @author      InformationMapping (http://www.information-mapping.com/)
 * @copyright   Copyright (c) 2014 InformationMapping (http://www.information-mapping.com/)
 * @license     Open Source License
 */
class InformationMapping_Elearning_Model_Observer {
    /*
     * Method fired on the event <sales_order_save_after>
     *
     * @access public
     * @param Varien_Event_Observer $observer
     * @return InformationMapping_Elearning_Model_Observer
     */
    public function salesOrderSaveAfter($observer) {
        $order = $observer->getEvent()->getOrder();
        if (!in_array($order->getData('state'), array(
            'complete',
            'processing'
        ))
        ) {
            return $this;
        }

        $debug = false;
        //if($order->getStoreId() == 17) $debug = true;
        //if($debug) Mage::log('[InformationMapping_Elearning] observer: '.$order->getId());

        $helper = Mage::helper('elearning');

        $orderItems = $order->getAllItems();
        $slots = array();
        foreach ($orderItems as $orderItem) {

            $product = $orderItem->getProduct();
            $qty = (int)$orderItem->getQtyInvoiced();

            if ($helper->isElearningOrderItem($orderItem)) {
                if (empty($slots[$product->getId()])) $slots[$product->getId()] = 0;
                $slots[$product->getId()] += $qty;
            }
        }

        $helper->deleteEnrollmentPurchasedSlots($order);
        foreach ($slots as $productId => $qty) {
            $product = Mage::getModel('catalog/product')->load($productId);
            $helper->createEnrollmentPurchasedSlots($order, $product, $qty);
        }

        return $this;
    }

    /**
     * After password reset, check if we need to redirect elsewhere using the custom "forward_to_url" parameter.
     */
    public function afterPasswordReset($observer) {
        /* @var $controller Mage_Customer_AccountController */
        $controller = $observer->getControllerAction();

        if ($this->_wasPasswordResetSuccessfully()) {
            // Password reset was a success!

            $customerId = (int) $controller->getRequest()->getQuery('id');

            /* @var $customer Mage_Customer_Model_Customer */
            $customer = Mage::getModel('customer/customer')->load($customerId);

            /* @var $session Mage_Customer_Model_Session */
            $session = Mage::getSingleton('customer/session');

            if ($customer->getId() && !$session->isLoggedIn()) {
                /* @var $session Mage_Customer_Model_Session */
                $session = Mage::getSingleton('customer/session');

                $session->loginById($customerId);
            }

            // Should we forward the used to a custom URL?
            $forwardToUrl = $controller->getRequest()->getParam('forward_to_url');
            if ($forwardToUrl) {
                // Resetting the password has been a success, but we want to send the user elsewhere...
                $response = $controller->getResponse();
                $response->setRedirect($forwardToUrl);
            }
        }

    }

    protected function _wasPasswordResetSuccessfully() {
        /* @var $session Mage_Customer_Model_Session */
        $session = Mage::getSingleton('customer/session');

        /* @var $messages Mage_Core_Model_Message_Collection */
        $messages = $session->getMessages();

        foreach ($messages->getItems() as $message) {

            if ($message instanceof Mage_Core_Model_Message_Success) {
                /* @var $message Mage_Core_Model_Message_Success */

                if ($message->getCode() === Mage::helper('customer')->__('Your password has been updated.')) {
                    return true;
                }
            }
        }

        return false;
    }
}
