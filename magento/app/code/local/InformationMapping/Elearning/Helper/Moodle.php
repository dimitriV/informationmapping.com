<?php
/**
 * InformationMapping Elearning for Magento 
 *
 * @package     InformationMapping_Elearning
 * @author      InformationMapping (http://www.information-mapping.com/)
 * @copyright   Copyright (c) 2014 InformationMapping (http://www.information-mapping.com/)
 * @license     Open Source License
 */

/**
 * Elearning Moodle helper
 */
class InformationMapping_Elearning_Helper_Moodle extends Mage_Core_Helper_Abstract
{
    public function getMoodleDbConnection()
    {
        $resource = Mage::getSingleton('core/resource');
        $db = $resource->getConnection('moodle_setup');
        return $db;
    }

    public function enrolUserToCourse($userid, $enrolid, $days)
    {

        // Add enrollment
        // Setup the values
        $status = 0;
       // $days = 91;
        $values = array(
            'enrolid' => $enrolid,
            'userid' => $userid,
            'status' => $status,
            'timestart' => time(),
            'timeend' => time() + (60*60*24*(int)$days),
            'timecreated' => time(),
            'timemodified' => time(),
        );

        $moodle = $this->getMoodleDbConnection();

        $query = 'SELECT `id` FROM `mdl_user_enrolments` WHERE `enrolid`='.(int)$enrolid.' AND `userid`='.(int)$userid;
        $id = $moodle->fetchOne($query);
        if(empty($id)) {
            $query = 'INSERT INTO `mdl_user_enrolments` SET '.$this->arrayToSqlValues($values);
        } else {
            $query = 'UPDATE `mdl_user_enrolments` SET '.$this->arrayToSqlValues($values)
                . ' WHERE `enrolid`='.(int)$enrolid.' AND `userid`='.(int)$userid;
        }

        $rt = $moodle->query($query);


        // Add student role
        $moodle = $this->getMoodleDbConnection();
        $queryrole = 'SELECT
                        con.id
                    FROM
                        `mdl_user_enrolments` ue
                            LEFT JOIN
                        `mdl_enrol` en ON en.id = ue.enrolid
                            LEFT JOIN
                        `mdl_course` c ON en.courseid = c.id
                            LEFT JOIN
                        `mdl_context` con ON con.instanceid = c.id
                    WHERE
                        `enrolid` ='.(int)$enrolid.'  AND `userid` = '.(int)$userid.'
                            AND con.depth = 3;';
        $contextid = $moodle->fetchOne($queryrole);
        // Setup the values for student role
        $values2 = array(
            'roleid' => 5,
            'contextid' => $contextid,
            'userid' => $userid,
            'timemodified' => time(),
            'modifierid' => 2,
        );
        if(empty($contexid)) {

            $queryrole = 'INSERT INTO `mdl_role_assignments` SET '.$this->arrayToSqlValues($values2);
        } else {
            $queryrole = 'UPDATE `mdl_role_assignments` SET '.$this->arrayToSqlValues($values2)
                . ' WHERE `contextid`='.(int)$contextid.' AND `userid`='.(int)$userid;
        }
        $rt = $moodle->query($queryrole);

    }

    public function enrolCustomerToProductId($customer, $productId)
    {
        $userid = $this->getUseridByCustomer($customer);
        $enrolid = $this->getEnrolidByProductId($productId);
        $days = $this->getEnrolPeriodByProductId($productId);
        return $this->enrolUserToCourse($userid, $enrolid, $days);
    }
    public function enrolCustomerToProductIdDays($customer, $productId, $days)
    {
        $userid = $this->getUseridByCustomer($customer);
        $enrolid = $this->getEnrolidByProductId($productId);
        return $this->enrolUserToCourse($userid, $enrolid, $days);
    }

    public function getUseridByCustomer($customer)
    {
        $moodle = $this->getMoodleDbConnection();
        $query = 'SELECT `id` FROM `mdl_user` WHERE `email`='.$moodle->quote($customer->getEmail());
        $id = $moodle->fetchOne($query);

        if(empty($id)) {
            $query = 'INSERT INTO `mdl_user` SET `auth`="manual", `confirmed`="1", `policyagreed`="0", `mnethostid`="1", '
                . '`username`='.$moodle->quote($customer->getId()).', `idnumber`='.$moodle->quote($customer->getId()).', '
		. '`email`='.$moodle->quote($customer->getEmail()).', '
		. '`password`="'.md5($customer->getEmail().$customer->getFirstname()).'", '
                . '`firstname`='.$moodle->quote($customer->getFirstname()).', `lastname`='.$moodle->quote($customer->getLastname()).', `timecreated`=UNIX_TIMESTAMP('.$moodle->quote($customer->getCreatedAt()).')'
            ;
            $moodle->query($query);

            $query = 'SELECT `id` FROM `mdl_user` WHERE `email`='.$moodle->quote($customer->getEmail());
            $id = $moodle->fetchOne($query);
        }

        return $id;
    }

    public function getEnrolidByProductId($productId)
    {
        $moodle = $this->getMoodleDbConnection();
        $query = 'SELECT `enrol_id` FROM `mdl_enrolments_products` WHERE `product_id`='.$moodle->quote($productId);
        $id = $moodle->fetchOne($query);
        return $id;
    }
     public function getEnrolPeriodByProductId($productId)
    {
        $moodle = $this->getMoodleDbConnection();
        $query = 'SELECT `enroll_period` FROM `mdl_enrolments_products` WHERE `product_id`='.$moodle->quote($productId);
        $days = $moodle->fetchOne($query);
        return $days;
    }

    public function arrayToSqlValues($array)
    {
        $moodle = $this->getMoodleDbConnection();
        $sql = array();
        foreach($array as $name => $value) {
            $sql[] = '`'.$name.'`='.$moodle->quote($value);
        }
        return implode(', ', $sql);
    }
}
