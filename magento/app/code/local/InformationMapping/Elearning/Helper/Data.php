<?php
/**
 * InformationMapping Elearning for Magento
 *
 * @package     InformationMapping_Elearning
 * @author      InformationMapping (http://www.information-mapping.com/)
 * @copyright   Copyright (c) 2014 InformationMapping (http://www.information-mapping.com/)
 * @license     Open Source License
 */

/**
 * Elearning helper
 */
class InformationMapping_Elearning_Helper_Data extends Mage_Core_Helper_Abstract
{
    protected $_bcc = array(
        array('name' => 'Sofian Mourabit', 'email' => 'smourabit@informationmapping.com'),
    );

    public function getOrderElearningProducts($order)
    {
        $products = array();
        $orderItems = $order->getAllItems();
        foreach($orderItems as $orderItem) {

            // Load product details
            $product = $orderItem->getProduct();
            $sku = $product->getSku();

            // Check the product itself
            if($this->isElearningProduct($product)) {
                $products[$sku] = $product;
                continue;
            }

            // Check child items
            if($orderItem->getHasChildren() > 0) {
                $childOrderItems = $orderItems;
                foreach($childOrderItems as $childOrderItem) {

                    if($orderItem->getQtyOrdered() == 0) continue;
                    if($orderItem->getQtyInvoiced() > 0 && $orderItem->getQtyInvoiced() == $orderItem->getQtyRefunded()) continue;

                    if($childOrderItem->getParentItemId() == $orderItem->getItemId()) {
                        $childProduct = $childOrderItem->getProduct();

                        if($this->isElearningProduct($childProduct)) {
                            $products[$sku] = $product;
                        }
                    }
                }
            }
        }

        return $products;
    }

    public function initEnrollment($order)
    {
        // Check whether there are any slots yet
        $slots = $this->getEnrollmentPurchasedSlots($order);
        if(empty($slots) || $slots->count() == 0) {
            $order->addStatusToHistory($order->getStatus(), 'Accessing enrolment', false);
            $order->save();
        }
        return true;
    }

    public function orderContainsElearning($order)
    {
        $orderItems = Mage::getResourceModel('sales/order_item_collection')
            ->setOrderFilter($order);

        foreach($orderItems as $orderItem) {
            if($this->isElearningOrderItem($orderItem)) {
                return true;
            }
        }

        return false;
    }

    public function isElearningOrderItem($orderItem)
    {
        // Skip orderItems that are contained within a parent item
        if($orderItem->getParentItemId() > 0) {
            return false;
        }

        // Check for product
        $product = $orderItem->getProduct();
        if($this->isElearningProduct($product)) {
            return true;
        }

        // Check for children
        if($orderItem->getHasChildren() > 0) {
            $childOrderItems = $orderItem->getOrder()->getAllItems();
            foreach($childOrderItems as $childOrderItem) {

                if($orderItem->getQtyOrdered() == 0) continue;
                if($orderItem->getQtyInvoiced() > 0 && $orderItem->getQtyInvoiced() == $orderItem->getQtyRefunded()) continue;

                if($childOrderItem->getParentItemId() == $orderItem->getItemId()) {
                    $childProduct = $childOrderItem->getProduct();

                    if($this->isElearningProduct($childProduct)) {
                        return true;
                    }
                }
            }
        }

        return false;
    }

    public function isElearningProduct($product)
    {
        return (bool)$product->getData('elearning');
    }

    public function getEnrollmentUrl($order)
    {
        $storeId = Mage::app()->getStore()->getStoreId();
        $orderId = base64_encode($order->getIncrementId());
        if(isset($_SERVER['HTTP_HOST'])) {
            return 'http://'.$_SERVER['HTTP_HOST'].'/enroll/?s='.(int)$storeId.'&o='.$orderId;
        } else {
            return 'http://www.informationmapping.com/enroll/?s='.(int)$storeId.'&o='.$orderId;
        }
    }

    public function deleteEnrollmentPurchasedSlots($order)
    {
        $slots = Mage::getModel('elearning/slot')
            ->getCollection()
            ->addFieldToFilter('order_id', $order->getId())
        ;

        foreach($slots as $slot) {
            $slot->delete();
        }
    }

    public function deleteEnrollmentPurchasedSlotsID($slotID)
    {
        $delete = Mage::getModel('elearning/slot');
        $delete->load($slotID)
               ->delete();
    }

    public function postEnrollmentPurchasedSlots($postSlots, $order, $modifyLocked = false)
    {
        // Set the return value
        $rt = true;

        // Fetch the available slots for this order
        $allowedSlots = $this->getEnrollmentPurchasedSlots($order);
        //Mage::log('[InformationMapping_Elearning] post purchased slots - '.$order->getId());

        // Loop through the POST data
        foreach($postSlots as $postSlotId => $postSlot) {

            // Load the slot record
            $slot = Mage::getModel('elearning/slot')->load($postSlotId);
            //Mage::log('[InformationMapping_Elearning] slot - '.$postSlotId);

            // Invalid, so continue
            if($slot->getId() == 0) continue;

            // Locked, so continue
            if($modifyLocked == false && $slot->getLocked() == 1) continue;

            // Prepare the POST data
            $email = trim($postSlot['email']);
            $firstname = trim($postSlot['firstname']);
            $lastname = trim($postSlot['lastname']);

            // No valid POST data, so continue
            if(empty($email) && empty($firstname) && empty($lastname)) {
                continue;
            }

            if(empty($email) || empty($firstname) || empty($lastname)) {
                $this->setMessage('error', 'You need to fill in Email, Firstname and Lastname to save this enrollment.');
                $rt = false;
                continue;
            }

            // Load the customer record or create a new record if needed
            $customer = Mage::getModel('customer/customer');
            $customer->setWebsiteId(Mage::app()->getWebsite()->getId());
            $customer->loadByEmail($email);

            if($customer->getId() > 0) {
                $customerNew = 0;
                $customerId = $customer->getId();
            } else {
                $customerNew = 1;
                $customerId = $this->createCustomerRecord($email, $firstname, $lastname);
                $customer->load($customerId);
            }

            // Determine the locked status
            $locked = true;

            // Save the data
            $slot->setCustomerEmail($email);
            $slot->setCustomerFirstname($firstname);
            $slot->setCustomerLastname($lastname);
            $slot->setCustomerId($customerId);
            $slot->setCustomerNew($customerNew);
            $slot->setLocked((int)$locked);
            $slot->setModified(date('Y-m-d H:i:s'));
            $slot->save();

            // Don't do anything else if the lock is not set yet
            if($locked == false) {
                continue;
            }

            // Load additional variables
            $product = Mage::getModel('catalog/product')->load($slot->getProductId());
            $helper = Mage::helper('elearning/moodle');

            // Save to Moodle
            $subProducts = $this->getSubProducts($product, $slot->getOrderId());
            if(!empty($subProducts)) {
                foreach($subProducts as $subProductId => $subProduct) {
                    $helper->enrolCustomerToProductId($customer, $subProductId);
                }
            }

            // Send the mail
            if($customerNew) {
                Mage::log('[InformationMapping_Elearning] mail new customer: '.$customer->getEmail());
                $this->sendNewCustomerEmail($customer, $product);
            } else {
                Mage::log('[InformationMapping_Elearning] mail existing customer: '.$customer->getEmail());
                $this->sendExistingCustomerEmail($customer, $product);
            }
        }

        return $rt;
    }
    public function postEnrollmentPurchasedSlotsByID($postSlots, $order, $days, $modifyLocked = false)
    {
        // Set the return value
        $rt = true;

        // Fetch the available slots for this order
        $allowedSlots = $this->getEnrollmentPurchasedSlotsByID($order);
        //Mage::log('[InformationMapping_Elearning] post purchased slots - '.$order->getId());

        // Loop through the POST data
        foreach($postSlots as $postSlotId => $postSlot) {

            // Load the slot record
            $slot = Mage::getModel('elearning/slot')->load($postSlotId);
            //Mage::log('[InformationMapping_Elearning] slot - '.$postSlotId);

            // Invalid, so continue
            if($slot->getId() == 0) continue;

            // Locked, so continue
            if($modifyLocked == false && $slot->getLocked() == 1) continue;

            // Prepare the POST data
            $email = trim($postSlot['email']);
            $firstname = trim($postSlot['firstname']);
            $lastname = trim($postSlot['lastname']);

            // No valid POST data, so continue
            if(empty($email) && empty($firstname) && empty($lastname)) {
                continue;
            }

            if(empty($email) || empty($firstname) || empty($lastname)) {
                $this->setMessage('error', 'You need to fill in Email, Firstname and Lastname to save this enrollment.');
                $rt = false;
                continue;
            }

            // Load the customer record or create a new record if needed
            $customer = Mage::getModel('customer/customer');
            $customer->setWebsiteId(Mage::app()->getWebsite()->getId());
            $customer->loadByEmail($email);

            if($customer->getId() > 0) {
                $customerNew = 0;
                $customerId = $customer->getId();
            } else {
                $customerNew = 1;
                $customerId = $this->createCustomerRecord($email, $firstname, $lastname);
                $customer->load($customerId);
            }

            // Determine the locked status
            $locked = true;

            // Save the data
            $slot->setCustomerEmail($email);
            $slot->setCustomerFirstname($firstname);
            $slot->setCustomerLastname($lastname);
            $slot->setCustomerId($customerId);
            $slot->setCustomerNew($customerNew);
            $slot->setLocked((int)$locked);
            $slot->setModified(date('Y-m-d H:i:s'));
            $slot->save();

            // Don't do anything else if the lock is not set yet
            if($locked == false) {
                continue;
            }

            // Load additional variables
            $product = Mage::getModel('catalog/product')->load($slot->getProductId());
            $helper = Mage::helper('elearning/moodle');

            // Save to Moodle
            $subProductId = $slot->getProductId();
            $helper->enrolCustomerToProductIdDays($customer, $subProductId, $days);

            // Send the mail
            if($customerNew) {
                Mage::log('[InformationMapping_Elearning] mail new customer: '.$customer->getEmail());
                $this->sendNewCustomerEmail($customer, $product);
            } else {
                Mage::log('[InformationMapping_Elearning] mail existing customer: '.$customer->getEmail());
                $this->sendExistingCustomerEmail($customer, $product);
            }
        }

        return $rt;
    }

    public function getSubProducts($product, $order)
    {
        if(is_numeric($product)) $product = Mage::getModel('catalog/product')->load($product);
        if(is_numeric($order)) $order = Mage::getModel('sales/order')->load($order);

        $subProducts = array();
        $orderItems = $order->getAllItems();
        foreach($orderItems as $orderItem) {
            if($orderItem->getProductId() == $product->getId() && $orderItem->getHasChildren() == 0) {
                if($this->isElearningProduct($product)) {
                    $subProducts[$product->getId()] = $product;
                }
                break;
            }

            $childOrderItems = $orderItems;
            foreach($childOrderItems as $childOrderItem) {
                if($childOrderItem->getParentItemId() == $orderItem->getItemId()) {
                    $childProduct = $childOrderItem->getProduct();
                    if($this->isElearningProduct($childProduct)) {
                        $subProducts[$childProduct->getId()] = $childProduct;
                    }
                }
            }
        }

        return $subProducts;
    }

    public function createCustomerRecord($email, $firstname, $lastname)
    {
        $customer = Mage::getModel('customer/customer');
        $customer->setWebsiteId(Mage::app()->getWebsite()->getId());
        $customer->loadByEmail($email);
        if($customer->getId() > 0) {
            return $customer->getId();
        }

        $password = Mage::helper('core')->getRandomString(8);
        $customer->setEmail($email);
        $customer->setFirstname($firstname);
        $customer->setLastname($lastname);
        $customer->setPassword($password);
        //$customer->setIsSubscribed(1);
        //$customer->setGroupId($groupId); // @todo: Implement group?

        try {
            $customer->save();
            $customer->setConfirmation(null);
            $customer->save();
        } catch (Exception $ex) {}

        return $customer->getId();
    }

    /**
     * The e-Learning URL is outside of Magento, so generating the URL is not obvious.
     */
    public function getElearningUrl($storeId = null){
        $params = array();
        if($storeId !== null){
            $params['_store'] = $storeId;
        }

        $eLearningUrl = Mage::getUrl('im-academy', $params);
        $eLearningUrl = str_replace('/shop/','/' , $eLearningUrl);

        return $eLearningUrl;
    }

    public function sendNewCustomerEmail($customer, $product)
    {
        $newResetPasswordLinkToken = Mage::helper('customer')->generateResetPasswordLinkToken();
        $customer->changeResetPasswordLinkToken($newResetPasswordLinkToken);
        $customer->save();
        $customer->load($customer->getId());

        $eLearningUrl = $this->getElearningUrl($customer->getStoreId());
        
        $reset_link = Mage::getUrl('customer/account/resetpassword',
            array('_query' => array('id' => $customer->getId(), 'token' => $newResetPasswordLinkToken, 'forward_to_url' => $eLearningUrl))
        );

        $this->setPasswordLink('success', $reset_link);

        $variables = array(
            'customer' => $customer,
            'product' => $product,
            'reset_link' => $reset_link,
        );

        return $this->sendTemplate('elearning_slot_new_customer', $customer, $variables);
    }

    public function sendExistingCustomerEmail($customer, $product)
    {
        $variables = array(
            'customer' => $customer,
            'product' => $product,
        );

        return $this->sendTemplate('elearning_slot_existing_customer', $customer, $variables);
    }

    public function sendTemplate($template, $customer, $variables)
    {
        $translate = Mage::getSingleton('core/translate');
        $translate->setTranslateInline(false);

        $storeId = Mage::app()->getStore()->getStoreId();
        $mail = Mage::getModel('core/email_template');
        $mail->setDesignConfig(array('area' => 'frontend', 'store' => $storeId));

        $senderName = Mage::getStoreConfig('trans_email/ident_general/name');
        $senderEmail = Mage::getStoreConfig('trans_email/ident_general/email');
        $sender = array('name' => $senderName, 'email' => $senderEmail);

        if(!empty($this->_bcc)) {
            foreach($this->_bcc as $bcc) {
                $mail->addBcc($bcc);
            }
        }
        $mail->sendTransactional($template, $sender, $customer->getEmail(), $customer->getName(), $variables);

        $translate->setTranslateInline(true);
        return $mail->getSentSuccess();
    }

    public function createEnrollmentPurchasedSlots($order, $product, $qty = 1)
    {
        for($i = 0; $i < $qty; $i++) {
            $slot = Mage::getModel('elearning/slot');
            $slot->setId(null);
            $slot->setProductId($product->getId());
            $slot->setOrderId($order->getId());
            $slot->save();
        }
    }

    public function createEnrollmentPurchasedSlotsByID($order, $product, $qty = 1)
    {
        for($i = 0; $i < $qty; $i++) {
            $slot = Mage::getModel('elearning/slot');
            $slot->setId(null);
            $slot->setProductId($product);
            $slot->setOrderId($order);
            $slot->save();
        }
    }

    public function getEnrollmentPurchasedSlotsByProduct($order, $product)
    {
        if(is_object($product)) $product = $product->getId();
        $slots = $this->getEnrollmentPurchasedSlots($order);
        $slotsByProduct = array();
        foreach($slots as $slot) {
            if($slot->getProductId() == $product) {
                $slotsByProduct[] = $slot;
            }
        }

        if(count($slotsByProduct) == 0) {
            return array();
        }

        if(count($slotsByProduct) == 1) {
            $this->setMessage('count', 1);
        }


        $importedSlots = (isset($_SESSION['enroll_slots_imported'])) ? $_SESSION['enroll_slots_imported'] : null;
        if(!empty($importedSlots)) {
            $i = 0;
            foreach($slotsByProduct as $slotId => $slot) {
                $email = trim($importedSlots[$i][0]);
                $firstname = trim($importedSlots[$i][1]);
                $lastname = trim($importedSlots[$i][2]);
                $lastname = preg_replace('/\;$/', '', $lastname);
                if($slot->getLocked() == 0 && !empty($email)) {
                    $slot->setCustomerEmail($email);
                    $slot->setCustomerFirstname($firstname);
                    $slot->setCustomerLastname($lastname);
                }
                $i++;
            }
            unset($_SESSION['enroll_slots_imported']);

            return $slotsByProduct;
        }

        foreach($slotsByProduct as $slotId => $slot) {
            $email = $slot->getCustomerEmail();
            if(empty($email)) {
                $address = $order->getBillingAddress();
                $slot->setCustomerEmail($address->getEmail());
                $slot->setCustomerFirstname($address->getFirstname());
                $slot->setCustomerLastname($address->getLastname());
            }
            break;

        }






        return $slotsByProduct;
    }

    public function getEnrollmentPurchasedSlots($order)
    {
        $slots = Mage::getModel('elearning/slot')
            ->getCollection()
            ->addFieldToFilter('order_id', $order->getId());

        return $slots;
    }
    public function getEnrollmentPurchasedSlotsByID($order)
    {

        $slots = Mage::getModel('elearning/slot')
            ->getCollection()
            ->addFieldToFilter('order_id', $order)
            ->setOrder('id', 'DESC');

        return $slots;
    }
    public function getEnrollmentProductTitle($prodId)
    {
        $title = Mage::getModel('elearning/products')->load($prodId);

        return $title->getProductTitle();
    }

    public function setMessage($type, $message)
    {
        if(empty($_SESSION['enroll_messages'])) $_SESSION['enroll_messages'] = array();
        $hash = md5($type.$message);
        $_SESSION['enroll_messages'][$hash] = array($type, $message);
    }
    public function setPasswordLink($type, $message)
    {
        if(empty($_SESSION['password_link'])) $_SESSION['password_link'] = array();
        $hash = md5($type.$message);
        $_SESSION['password_link'][$hash] = array($type, $message);
    }
}
