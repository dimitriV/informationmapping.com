<?php
class Mage_Core_Model_Config extends _Mage_Core_Model_Config {
	
	private $_vendorModuleNames;
	
	/**
	 * Load modules configuration
	 *
	 * @return Mage_Core_Model_Config
	 */
	public function loadModules() {
		$r = parent::loadModules ();

		Mage::dispatchEvent ( 'vendor_modules_loaded', array ('config' => $this ) );
		
		return $r;
	}

	
	/**
	 * Get module directory by directory type
	 *
	 * @param   string $type
	 * @param   string $moduleName
	 * @return  string
	 */
	public function getModuleDir($type, $moduleName) {
		$codePool = ( string ) $this->getModuleConfig ( $moduleName )->codePool;
		

		if ($codePool === 'vendor' && Storefront_VendorLoader_Model_Loader::isModuleAllowed($moduleName)) {
			
			$dir = $this->getOptions ()->getBaseDir () . DS . $codePool . DS . uc_words ( $moduleName, DS ) . DS . 'code';
			
			switch ($type) {
				case 'etc' :
					$dir .= DS . 'etc';
					break;
				
				case 'controllers' :
					$dir .= DS . 'controllers';
					break;
				
				case 'sql' :
					$dir .= DS . 'sql';
					break;
				
				case 'locale' :
					$dir .= DS . 'locale';
					break;
			}
			
			$dir = str_replace ( '/', DS, $dir );
			return $dir;
		} else {
			return parent::getModuleDir ( $type, $moduleName );
		}
	}
	
	/**
	 * Get model class instance.
	 *
	 * Example:
	 * $config->getModelInstance('catalog/product')
	 *
	 * Will instantiate Mage_Catalog_Model_Mysql4_Product
	 *
	 * @param string $modelClass
	 * @param array|object $constructArguments
	 * @return Mage_Core_Model_Abstract
	 */
	public function getModelInstance($modelClass = '', $constructArguments = array()) {
		$className = $this->getModelClassName ( $modelClass );
		
		// Check if it's a vendor module class
		$this->_loadVendorClass ( $className );
		
		return parent::getModelInstance ( $modelClass, $constructArguments );
	}
	
	/**
	 * Retrieve block class name
	 *
	 * @param   string $blockType
	 * @return  string
	 */
	public function getBlockClassName($blockType) {
		$className = parent::getBlockClassName ( $blockType );
		
		// If we also load the block class here,
		// the rest of Mage_Core_Model_Layout::_getBlockInstance will also run
		$this->_loadVendorClass ( $className );
		
		return $className;
	}
	
	/**
	 * Retrieve helper class name
	 *
	 * @param   string $name
	 * @return  string
	 */
	public function getHelperClassName($helperName) {
		$className = parent::getHelperClassName ( $helperName );
		
		// If we also load the block class here,
		// the rest of Mage::helper will also run
		$this->_loadVendorClass ( $className );
		
		return $className;
	}
	
	/**
	 * Loads a vendor model or block class based on the class name
	 * @param unknown_type $className
	 * @throws Exception
	 */
	private function _loadVendorClass($className) {
		if (! class_exists ( $className, false )) {
			// Class does not exist yet, so try the vendor modules
			$parts = explode ( '_', $className );
			if (count ( $parts ) > 2) {
				$moduleName = $parts [0] . '_' . $parts [1];
				
				if ($this->_areModulesLoaded ()) {
					$vendorModules = $this->_getVendorModuleNames ();
					
					if (in_array ( $moduleName, $vendorModules )) {
						// This is a model of a vendor module
						$this->_loadVendorClassFile ( $className, $moduleName, $parts );
					}
					
				} else {
					//config was not loaded yet
					$this->_loadVendorClassFile ( $className, $moduleName, $parts );
				
				}
			}
		}
	}
	
	private function _getVendorModuleNames() {
		if ($this->_vendorModuleNames === null) {
			$modules = $this->getNode ( 'modules' )->children ();
			
			$vendorModuleNames = array ();
			foreach ( $modules as $moduleName => $moduleConfig ) {
				if ($moduleConfig->codePool == 'vendor' && $moduleConfig->active == 'true') {
					$vendorModuleNames [] = $moduleName;
				}
			}
			$this->_vendorModuleNames = $vendorModuleNames;
		}
		return $this->_vendorModuleNames;
	}
	
	private function _loadVendorClassFile($className, $moduleName, $parts) {
		if ($parts [2] == 'Model' || $parts [2] == 'Block' || $parts [2] == 'Helper' || $parts [2] == 'Controller') {
			$subDir = $parts [2];
		} else {
			throw new Exception ( 'Error loading vendor model. Part 3 of class name was not "Model" or "Block".' );
		}
		unset ( $parts [0], $parts [1], $parts [2] );
		
		$classFile = implode ( DIRECTORY_SEPARATOR, $parts ). '.php';
		
		$dir = $this->getOptions ()->getBaseDir () . DS . 'vendor' . DS . uc_words ( $moduleName, DS ) . DS . 'code';
		
		$path = $dir . DS . $subDir . DS . $classFile;
		if (file_exists ( $path )) {
			include $path;
		}
	}
	
	/**
	 * Force loading a vendor class - only works when modules haven't been loaded yet.
	 * 
	 */
	public function loadVendorClass($className) {
		if (! $this->_areModulesLoaded ()) {
			$this->_loadVendorClass ( $className );
		}
	}
	
	private function _areModulesLoaded() {
		$configNode = $this->getNode ( 'modules' );
		return ($configNode !== false);
	}

	
	/**
	 * Imports XML file - now with support for IonCube
	 *
	 * @param string $filePath
	 * @return boolean
	 */
	public function loadFile($filePath)
	{
		if (!is_readable($filePath)) {
			//throw new Exception('Can not read xml file '.$filePath);
			return false;
		}
		
		if(function_exists('ioncube_read_file')){
			$fileData = ioncube_read_file($filePath);
			$fileData = $this->processFileData($fileData);
			return $this->loadString($fileData, $this->_elementClass);
		}else{
			return parent::loadFile($filePath);
		}
	}
}