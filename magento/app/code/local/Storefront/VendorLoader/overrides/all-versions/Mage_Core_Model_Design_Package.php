<?php
class Mage_Core_Model_Design_Package extends _Mage_Core_Model_Design_Package {
	
	private static $_templateFilenameRewrites;
	
	/**
	 * Use this one to get existing file name with fallback to default
	 *
	 * $params['_type'] is required
	 *
	 * @param string $file
	 * @param array $params
	 * @return string
	 */
	public function getFilename($file, array $params) {
		$filename = parent::getFilename ( $file, $params );
		
		if (! isset ( $params ['_relative'] ) || (isset ( $params ['_relative'] ) && $params ['_relative'] == false)) {
			// FALL BACK TO VENDOR MODULE - If the requested file is found, nothing will happen.
			$filename = $this->fallbackVendor ( $filename );
		}
		
		return $filename;
	}
	
	/**
	 * Finds an alternative file in a vendor module, if the given file doesn't exist
	 * 
	 * @param unknown_type $filename
	 */
	public function fallbackVendor($absFilename) {
		// Example: /Users/wouter/Business/Workspace/natuurhuis/app/design/frontend/base/default/layout/testmodule.xml
		

		if (! file_exists ( $absFilename )) {
			
			$matches = array ();
			if (preg_match ( '#/app/(design|locale)/(.*)$#is', $absFilename, $matches )) {
				$relPath = $matches [2];
				$folder = $matches [1];
				// Example: frontend/base/default/locale/nl_NL/translate.csv
				$helper = Mage::helper ( 'vendorloader' );
				$vendorModules = $helper->getVendorModulePaths ();
				
				//TODO build complete fallback for vendor modules too
				foreach ( $vendorModules as $moduleName => $vendorPath ) {
					$vendorFilename = $vendorPath . $folder . DS . $relPath;
					if (file_exists ( $vendorFilename )) {
						return $vendorFilename;
					}
				}
			}
		}
		return $absFilename;
	}
	
	public function getTemplateFilename($file, array $params = array()) {
		$file = $this->_rewriteTemplateFilename ( $file );
		
		$filename = parent::getTemplateFilename ( $file, $params );
		return $filename;
	}
	
	private function _rewriteTemplateFilename($file) {
		if (self::$_templateFilenameRewrites === null) {
			$this->_loadTemplateFilenameRewrites ();
		}
		if (array_key_exists ( $file, self::$_templateFilenameRewrites )) {
			$file = self::$_templateFilenameRewrites [$file];
		}
		return $file;
	}
	
	private function _loadTemplateFilenameRewrites() {
		$rewrites = new ArrayObject ();
		
		$rootNodes = array();
		$rootNodes[] = Mage::getConfig ()->getNode ( 'global/template/rewrites' );
		$rootNodes[] = Mage::getConfig ()->getNode ( $this->getArea().'/template/rewrites' );
		
		// Old rewriting no longer supported
		$oldRootNode = Mage::getConfig ()->getNode ( 'global/template/'.$this->getArea().'/rewrites' );
		if($oldRootNode !== false){
			Mage::throwException('This old template rewriting solution is no longer supported!');
		}
		
		foreach($rootNodes as $root){
			if($root !== false){
				foreach ( $root->children () as $groupNodeName => $rewriteNode ) {
					$from = $rewriteNode->from;
					if (isset ( $from [0] )) {
						$from = reset ( $from );
					}
					
					$to = $rewriteNode->to;
					if (isset ( $to [0] )) {
						$to = reset ( $to );
					}
					
					if (is_string ( $from ) && is_string ( $to )) {
						$rewrites [$from] = $to;
					}
				}
			}
		}
		
		
		Mage::dispatchEvent ( 'after_load_template_filename_rewrites', array ('rewrites' => $rewrites ) );
		
		$rewrites = ( array ) $rewrites;
		
		self::$_templateFilenameRewrites = $rewrites;
	}
	
	/**
	 * Get skin file url
	 *
	 * @param string $file
	 * @param array $params
	 * @return string
	 */
	public function getSkinUrl($file = null, array $params = array()) {
		$url = parent::getSkinUrl ( $file, $params );
		
		//    	if (empty($params['_type'])) {
		//            $params['_type'] = 'skin';
		//        }
		//        if (empty($params['_default'])) {
		//            $params['_default'] = false;
		//        }
		//        $this->updateParamDefaults($params);
		//    	$baseUrl = Mage::getBaseUrl('skin', isset($params['_secure'])?(bool)$params['_secure']:null).$params['_area'].'/';
		

		$url = $this->_fallbackVendorSkin ( $file, $params, $url );
		
		return $url;
	}
	
	protected function _fallbackVendorSkin($file, $params, $url) {
		$matches = array ();
		if (preg_match ( '#/skin/(.*?/base/default/.*)$#is', $url, $matches )) {
			$relPath = $matches [1];
			
			if (empty ( $params ['_type'] )) {
				$params ['_type'] = 'skin';
			}
			if (empty ( $params ['_default'] )) {
				$params ['_default'] = false;
			}
			$this->updateParamDefaults ( $params );
			
			$helper = Mage::helper ( 'vendorloader' );
			$vendorModules = $helper->getVendorModulePaths ();
			
			//TODO build complete fallback for vendor modules too
			foreach ( $vendorModules as $moduleName => $vendorPath ) {
				$vendorFilename = $vendorPath . 'skin' . DS . $relPath;
				if (file_exists ( $vendorFilename )) {
					
					// Using the skin dir helps to find the root for shops that do not have the standard dir layout (example MageBridge with Joomla)
					$baseUrl = Mage::getBaseUrl ( 'skin' );
					$baseUrl = substr($baseUrl, 0, -5);
					
					if(defined('SUBDIR')){
						$baseUrl = str_replace('/'.SUBDIR.'/', '/', $baseUrl);
					}
					
					$url = $baseUrl . 'vendor/' . str_replace ( '_', '/', $moduleName ) . '/skin/' . $relPath;
					return $url;
				}
			}
		}
		return $url;
	}
}