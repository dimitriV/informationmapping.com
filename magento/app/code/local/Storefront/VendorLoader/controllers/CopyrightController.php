<?php
class Storefront_VendorLoader_CopyrightController extends Mage_Core_Controller_Front_Action {
	
	public function indexAction(){
		$this->loadLayout();
		
		$layout = $this->getLayout();
		$copyBlock = $layout->createBlock('vendorloader/copyright');
		/* @var $copyBlock Storefront_VendorLoader_Block_Copyright */
		
		$contentBlock = $layout->getBlock('content');
		/* @var $contentBlock Mage_Core_Block_Text_List */
		
		$contentBlock->append($copyBlock);
		
		$headBlock = $layout->getBlock('head');
		/* @var $headBlock Storefront_AwCore_Block_Page_Html_Head */
		
		$headBlock->setRobotsFollow(false);
		$headBlock->setRobotsIndex(false);
		
		$layout->getBlock('root')->setTemplate('page/1column.phtml');
		
		$this->renderLayout();
	}
}