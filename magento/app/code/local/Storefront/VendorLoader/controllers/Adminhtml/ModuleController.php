<?php
class Storefront_VendorLoader_Adminhtml_ModuleController extends Mage_Adminhtml_Controller_Action {

	public function indexAction(){
		$this->loadLayout();
		
		$layout = $this->getLayout();
		
		$contentBlock = $layout->getBlock('content');
		
		$block = $layout->createBlock('vendorloader/adminhtml_modules', 'modules');
		$contentBlock->append($block);
		
		$this->renderLayout();
	}
	
	

}