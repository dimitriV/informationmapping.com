<?php
class Storefront_VendorLoader_DebugController extends Mage_Core_Controller_Front_Action {
	
	public function preDispatch()
	{
		if(!Mage::getIsDeveloperMode()){
			header("HTTP/1.0 403 Forbidden");
			echo 'Access denied';
			exit;

		}else{
			return parent::preDispatch();
		}
	}
	
	public function licenseupdateAction(){
		
		Storefront_VendorLoader_Model_Loader::updateLicense();
		
		
	}
}