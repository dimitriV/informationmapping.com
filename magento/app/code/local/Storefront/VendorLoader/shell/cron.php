<?php
define('MAGENTO_ROOT', realpath(dirname(__FILE__).'/../../../../../../'));

require MAGENTO_ROOT.'/app/Mage.php';

if (!Mage::isInstalled()) {
	echo "Application is not installed yet, please complete install wizard first.";
	exit;
}

umask(0);

// Load VendorLoader
require_once MAGENTO_ROOT.'/app/code/local/Storefront/VendorLoader/Model/Loader.php';
Storefront_VendorLoader_Model_Loader::enableLoader();


// Only for urls
// Don't remove this
$_SERVER['SCRIPT_NAME'] = str_replace(basename(__FILE__), 'index.php', $_SERVER['SCRIPT_NAME']);
$_SERVER['SCRIPT_FILENAME'] = str_replace(basename(__FILE__), 'index.php', $_SERVER['SCRIPT_FILENAME']);

Mage::app('admin')->setUseSessionInUrl(false);

try {
	Mage::getConfig()->init()->loadEventObservers('crontab');
	Mage::app()->addEventArea('crontab');
	Mage::dispatchEvent('default');
} catch (Exception $e) {
	Mage::printException($e);
}
