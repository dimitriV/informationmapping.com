<?php
class Storefront_VendorLoader_Block_Adminhtml_Html extends Mage_Core_Block_Template {
	
	public function fetchView($fileName) {
		$scriptPath = realpath ( dirname ( __FILE__ ).DS.'..'.DS.'..'.DS.'templates' );
		$this->_viewDir = $scriptPath;	    
		return parent::fetchView ( $this->getTemplate () );
	}
	
	public function getPathSkin() {
		$url = Mage::helper('vendorloader')->getVendorBaseUrl();
		$url .= 'vendor/Storefront/Commercebug/skin/frontend/commercebug';
		return $url;
	}

	public function getLayout() {
		return Mage::getSingleton ( 'core/layout' );
	}
}