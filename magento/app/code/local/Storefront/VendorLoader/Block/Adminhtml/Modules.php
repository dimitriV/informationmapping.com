<?php
class Storefront_VendorLoader_Block_Adminhtml_Modules extends Storefront_VendorLoader_Block_Adminhtml_Html {
	
	protected $_template = 'modules.phtml';
	
	
	public function getModules(){
		$modules = Mage::getConfig()->getNode('modules')->asArray();
		$codes = array();
		foreach($modules as $moduleName => $moduleNode){
			if(stripos($moduleName, 'Storefront_') === 0 && $moduleName !== 'Storefront_VendorLoader'){
				$codes[] = $moduleName;
			}
		}
		
		natcasesort($codes);
		return $codes;
	}
}