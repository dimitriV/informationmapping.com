<?php
class Storefront_VendorLoader_Block_Copyright extends Mage_Core_Block_Abstract{
	
	protected function _toHtml(){
		$coreHelper = Mage::helper('core');
		$html = '<h1>'.$coreHelper->__('Copyrights and Legal').'</h1>';
		$html .= '<ul>';
		foreach($this->_getInstalledModules() as $module){
			if(preg_match('#(Storefront_)|(Allwebsites_)#is',$module)){
				$html .= '<li>';
				$html .= '<h2>'.$coreHelper->escapeHtml($module).'</h2>';
				$html .= '<p>'.$coreHelper->__('Copyright Storefront BVBA, Belgium. All rights reserved. http://www.storefront.be').'</p>';
				$html .= '</li>';
			}
		}
		$html .= '</ul>';
		return $html;
	}
	
	protected function _getInstalledModules(){
		$modules = array_keys((array)Mage::getConfig()->getNode('modules')->children());
		return $modules;
	}
	
}