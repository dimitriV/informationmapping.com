<?php
class Storefront_VendorLoader_Helper_Data extends Mage_Core_Helper_Abstract {
	
	protected $_vendorModulePaths;
	
	
	/**
	 * Checks if the vendor loader was installed properly, including Magento core modifications
	 * @return boolean
	 */
	public function isInstalledProperly() {
		return Storefront_VendorLoader_Model_Loader::isLoaderEnabled();
	}
	
	/**
	 * Returns an associative array with the module names and paths to the installed modules
	 * @return array
	 */
	public function getVendorModulePaths() {
		if($this->_vendorModulePaths === null){
			$modules = Mage::app ()->getConfig ()->getNode ( 'modules' )->children ();
			$vendorModules = array ();
			
			
			
			foreach ( $modules as $moduleName => $moduleConfig ) {
				if ($moduleConfig->codePool == 'vendor' && $moduleConfig->active == 'true') {
					$moduleNameParts = explode ( '_', $moduleName );
					
					if(Storefront_VendorLoader_Model_Loader::isModuleAllowed($moduleName)){
						$vendorModules [$moduleName] = Mage::getBaseDir () . DS . 'vendor' . DS . $moduleNameParts [0] . DS . $moduleNameParts [1]. DS;
					}
				}
			}
			$this->_vendorModulePaths = $vendorModules;
		}
		return $this->_vendorModulePaths;
	}
	
	
	/**
	 * Get the base URL for use with vendor URL's
	 * This is the same are the normal Base URL, except it also works when store codes are in the URL.
	 * @return string
	 */
	public function getVendorBaseUrl(){
		$url = Mage::getBaseUrl ();
		
		$useStoreCodes = Mage::getStoreConfigFlag('web/url/use_store');
		if($useStoreCodes){
			// Store code is included in URL
			$storeCode = Mage::app()->getStore()->getCode();
			$url = str_replace ( $storeCode.'/', '', $url );
		}
		
		$url = str_replace ( 'index.php', '', $url );
		return $url;
	}
	
	
	
}