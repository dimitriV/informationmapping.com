<?php
abstract class Storefront_VendorLoader_Helper_Abstract extends Mage_Core_Helper_Abstract {
	
	abstract public function getModuleName();
	
	public function __construct(){
		if(!Storefront_VendorLoader_Model_Loader::isLoaderEnabled()){
			Mage::throwException('The Storefront Vendor Loader module is not loaded.');
		}
	}
	
	public function getVendorSkinUrl($relativeSkinPath = '') {
		$module = $this->getModuleName();
		$pathParts = explode('_', $module);
		
		$url = Mage::helper('vendorloader')->getVendorBaseUrl();
		$url .= 'vendor/'.$pathParts[0].'/'.$pathParts[1].'/skin/'.$relativeSkinPath;
		return $url;
	}
}