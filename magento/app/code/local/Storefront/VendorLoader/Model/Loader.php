<?php
/**
 * 
 * @author wouter
 * @no_encode
 */
final class Storefront_VendorLoader_Model_Loader {

	private static $_customizedClasses = array(
			'Mage_Core_Model_Config',
			'Mage_Core_Block_Template',
			'Mage_Core_Model_Design_Package'
	);

	private static $_supportedVersions = array(
			'Enterprise_1.12.0.0',
			'Enterprise_1.13.1.0',
			'Community_1.5.1.0',
			'Community_1.6.0.0',
			'Community_1.6.2.0',
			'Community_1.7.0.0',
			'Community_1.7.0.2',
			'Community_1.8.1.0',
			'Community_1.9.0.1',
			'Community_1.9.1.0'
	);
	
	private static $_isHelperAvailable = false;

	private static $_isAutoloaderLoaded = false;

	private static $_isModuleLoaded = false;

	protected static $_instance;

	private static $_autoLoaded = array();

	/**
	 * Singleton pattern implementation
	 *
	 * @return Varien_Autoload
	 */
	static public function instance () {
		if (! self::$_instance) {
			self::$_instance = new Storefront_VendorLoader_Model_Loader();
		}
		return self::$_instance;
	}

	/**
	 * Register SPL autoload function
	 */
	private static function _registerAutoloader () {
		if (version_compare(phpversion(), '5.3.0', '<') === true) {
			// Older than 5.3 has only 2 arguments
			throw new Exception('PHP version must be at least 5.3 or later for Vendorloader module.');
		} else {
			// PHP 5.3 or newer
			spl_autoload_register(array(
					self::instance(),
					'autoloadVendorClass'
			), true, true);

			// Load helper - not needed on PHP 5.3, but may cause problems on PHP 5.4
			$helperPath = realpath(dirname(__FILE__).DS.'..'.DS.'Helper'.DS.'Data.php');
			require_once $helperPath;
			
			// Mark autoloader installed
			self::$_isAutoloaderLoaded = true;
		}
	}

	public static function enableLoader () {
		/**
		 * Register SPL autoload function
		 */
		if (! self::isLoaderEnabled()) {
			self::_registerAutoloader();
		}
	}

	public function isMagentoVersionSupported () {
		$key = self::getMagentoEdition().'_'.Mage::getVersion();
		return in_array($key, self::$_supportedVersions);
	}
	
	/**
	 * Older Magento versions don't have the Mage::getEdition() function
	 * @return string
	 */
	public static function getMagentoEdition(){
		if(method_exists('Mage','getEdition')){
			return Mage::getEdition();
		}else{
			return 'Community';
		}
	}

	public static function isLoaderEnabled () {
		return self::$_isAutoloaderLoaded;
	}

	public static function autoloadVendorClass ($class) {
		// Special cases
		
		if($class === 'Varien_Cache_Core' && file_exists(MAGENTO_ROOT.DS.'vendor'.DS.'Storefront'.DS.'CachePlus'.DS.'lib'.DS.'Varien'.DS.'Cache'.DS.'Core.php')){
			include MAGENTO_ROOT.DS.'vendor'.DS.'Storefront'.DS.'CachePlus'.DS.'lib'.DS.'Varien'.DS.'Cache'.DS.'Core.php';
			return;
		}
		
		
		if (in_array($class, self::$_customizedClasses)) {
			
			// Load customized version of an existing class
			$version = Mage::getVersion();
			$edition = self::getMagentoEdition();
			
			$overrideFilePath1 = dirname(__FILE__) . DS . '..' . DS . 'overrides' . DS . $edition . DS. $version . DS . 'original' . DS . $class . '.php';
			
			if (file_exists($overrideFilePath1)) {
				// Load original
				include_once $overrideFilePath1;
			}
			
			$overrideFilePath2 = dirname(__FILE__) . DS . '..' . DS . 'overrides' . DS . $edition . DS. $version . DS . $class . '.php';
			if (file_exists($overrideFilePath2)) {
				// Load adaptation of original
				include_once $overrideFilePath2;
				return;
			}
			
			$overrideFilePath3 = dirname(__FILE__) . DS . '..' . DS . 'overrides' . DS . 'all-versions' . DS . $class . '.php';
			if (file_exists($overrideFilePath3)) {
				// Load adaptation of original
				include_once $overrideFilePath3;
				return;
			}
		}
		
		$company = substr($class, 0, strpos($class, '_'));
		if (($company === 'Mage') || ($company === 'Varien') || ($company === 'Zend') || ($company === 'Enterprise')) {
			// Never autoload Magento classes using the vendor system
			return;
		}
		
		if(!self::_isVendorModuleLoaded()){
			// Class may be a bootstrap class
			
			$classParts = explode('_', $class);
			if(count($classParts) > 3 && $classParts[2] === 'Bootstrap'){
				$classFile = str_replace(' ', DS, ucwords(str_replace('_', ' ', $class)));
				$classFile = str_replace('Bootstrap', 'code'.DS.'Bootstrap', $classFile);
				$classFile = Mage::getBaseDir() . DS . 'vendor' . DS . $classFile . '.php';
				
				include $classFile;
				return;
			}
		}
		
		// These classes are used here, so we cannot start another autoload
		$classesNotToAutoload = array(
				'Mage_Core_Helper_Abstract',
				'Storefront_VendorLoader_Helper_Data'
		);
		
		if (self::_isVendorModuleLoaded() && !in_array($class, $classesNotToAutoload) && !in_array($class, self::$_autoLoaded)) {
			self::$_autoLoaded[] = $class;
			
			/* @var $vendorLoaderHelper Storefront_VendorLoader_Helper_Data */
			$vendorLoaderHelper = Mage::helper('vendorloader');
			
			$vendorModulePaths = $vendorLoaderHelper->getVendorModulePaths();
			foreach ($vendorModulePaths as $vendorModuleName => $vendorModulePath) {
				if (substr($class, 0, strlen($vendorModuleName)) === $vendorModuleName) {
					// The class is part of this vendor module
					
					$parts = explode('_', $class);
					
					if (strlen($class) > 10 && substr($class, - 10) == 'Controller') {
						// load controller PHP file
						
						if (count($parts) >= 3) {
							$classFile = $parts[0] . DS . $parts[1] . DS . 'code' . DS . 'controllers' . DS;
							unset($parts[0], $parts[1]);
							$classFile .= implode(DS, $parts);
							$classFile = Mage::getBaseDir() . DS . 'vendor' . DS . $classFile . '.php';
						} else {
							continue;
						}
					} elseif (count($parts) >= 3 && $parts[2] == 'Controller') {
						
						$classFile = $parts[0] . DS . $parts[1] . DS . 'code' . DS;
						unset($parts[0], $parts[1]);
						$classFile .= implode(DS, $parts);
						$classFile = Mage::getBaseDir() . DS . 'vendor' . DS . $classFile . '.php';
					} else {
						// load other file
						$codeReplacements = array(
								'Model',
								'Block',
								'Helper'
						);
						
						$classFile = str_replace(' ', DS, ucwords(str_replace('_', ' ', $class)));
						foreach ($codeReplacements as $folder) {
							$classFile = str_replace(DS . $folder . DS, DS . 'code' . DS . $folder . DS, $classFile);
						}
						
						$classFile = Mage::getBaseDir() . DS . 'vendor' . DS . $classFile . '.php';
					}
					
					include $classFile;
					break;
				}
			}
		}
	}
	

	
	protected static function _isVendorModuleLoaded () {
		if (! self::$_isModuleLoaded && Mage::getConfig() !== null) {
			$enabled = (bool) Mage::getConfig()->getNode('modules/Storefront_VendorLoader/active');
			self::$_isModuleLoaded = $enabled;
		}
		return self::$_isModuleLoaded;
	}
	
	final public static function isModuleAllowed ($moduleName) {
			// TODO check license
			// $key = 'Jf$oO+DaE±xq,?pWr7Ux-g]k_Fx7v;4D"39y!p*Yx§\ |~]-#Pby+&f..K&';
			return true;
		}

		final private static function _formatLicenseRequest () {
			$websiteData = array();

			foreach (Mage::app()->getWebsites() as $website) {

				$storeData = array();
				foreach ($website->getStores() as $store) {
					$storeId = $store->getId();

					$storeData['store'.$storeId] = array(
							'id' => $storeId,
							'name' => $store->getName(),
							'company_name' => Mage::getStoreConfig('general/store_information/name', $store->getId()),
							'url' => Mage::getUrl('', array(
									'_store' => $storeId
							)),
							'locale' => Mage::getStoreConfig('general/locale/code', $storeId),
							'phone' => Mage::getStoreConfig('general/store_information/phone', $storeId),
							'country' => Mage::getStoreConfig('general/store_information/merchant_country', $storeId),
							'vat' => Mage::getStoreConfig('general/store_information_merchant/vat_number', $storeId),
							'address' => Mage::getStoreConfig('general/store_information/address', $storeId),
							'general_email' => Mage::getStoreConfig('trans_email/ident_general/email', $storeId),
							'general_name' => Mage::getStoreConfig('trans_email/ident_general/name', $storeId),
							'sales_email' => Mage::getStoreConfig('trans_email/ident_sales/email', $storeId),
							'sales_name' => Mage::getStoreConfig('trans_email/ident_sales/name', $storeId),
							'support_email' => Mage::getStoreConfig('trans_email/ident_support/email', $storeId),
							'support_name' => Mage::getStoreConfig('trans_email/ident_support/name', $storeId),
							'custom1_email' => Mage::getStoreConfig('trans_email/ident_custom1/email', $storeId),
							'custom1_name' => Mage::getStoreConfig('trans_email/ident_custom1/name', $storeId),
							'custom2_email' => Mage::getStoreConfig('trans_email/ident_custom2/email', $storeId),
							'custom2_name' => Mage::getStoreConfig('trans_email/ident_custom2/name', $storeId),
							'contact_email' => Mage::getStoreConfig('contacts/email/recipient_email', $storeId)
					);
				}

				$websiteData['website' . $website->getId()] = array(
						'id' => $website->getId(),
						'name' => $website->getName(),
						'stores' => $storeData
				);
			}

			$moduleData = array();
			$module_names = array_keys((array) Mage::getConfig()->getNode('modules')->children());
			foreach ($module_names as $module) {
				$moduleData[$module] = array(
						'version' => (string) Mage::getConfig()->getNode()->modules->{$module}->version
				);
			}

			$data = array(
					'identity' => array(
							'version' => Mage::getVersion(),
							'edition' => Mage::getEdition(),
							'admin_url' => Mage::getUrl('', array(
									'_store' => 0
							)),
							'company_name' => Mage::getStoreConfig('general/store_information/name'),
							'timezone' => Mage::getStoreConfig('general/locale/timezone'),
							'locale' => Mage::getStoreConfig('general/locale/code'),
							'phone' => Mage::getStoreConfig('general/store_information/phone'),
							'country' => Mage::getStoreConfig('general/store_information/merchant_country'),
							'vat' => Mage::getStoreConfig('general/store_information_merchant/vat_number'),
							'address' => Mage::getStoreConfig('general/store_information/address'),
							'general_email' => Mage::getStoreConfig('trans_email/ident_general/email'),
							'general_name' => Mage::getStoreConfig('trans_email/ident_general/name'),
							'sales_email' => Mage::getStoreConfig('trans_email/ident_sales/email'),
							'sales_name' => Mage::getStoreConfig('trans_email/ident_sales/name'),
							'support_email' => Mage::getStoreConfig('trans_email/ident_support/email'),
							'support_name' => Mage::getStoreConfig('trans_email/ident_support/name'),
							'custom1_email' => Mage::getStoreConfig('trans_email/ident_custom1/email'),
							'custom1_name' => Mage::getStoreConfig('trans_email/ident_custom1/name'),
							'custom2_email' => Mage::getStoreConfig('trans_email/ident_custom2/email'),
							'custom2_name' => Mage::getStoreConfig('trans_email/ident_custom2/name'),
							'contact_email' => Mage::getStoreConfig('contacts/email/recipient_email')
					),
					'websites' => $websiteData,
					'modules' => $moduleData
			);
			$xml = '<license_request>'.self::_convertArrayToXml($data).'</license_request>';

			return $xml;
		}

		final public static function updateLicense () {
			if(Mage::getIsDeveloperMode()){
				$server = 'http://localhost:10088/storefront-license/';
			}else{
				$server = 'https://license.storefront.be/';
			}

			$xml = self::_formatLicenseRequest();

			$client = new Zend_Http_Client($server);
			$client->setParameterPost('xml', $xml);

			$result = $client->request('POST');
			if ($result->getStatus() == 200) {
				$bin = $result->getBody();

				$licensePath = self::_getLicensePath();

				file_put_contents($licensePath, $bin);

				return true;
			}
		}

		private static function _getLicensePath () {
			return Mage::getBaseDir('var') . 'storefront.license';
		}

		private static function _escapeXml ($value) {
			return htmlentities(trim($value));
		}

		private static function _convertArrayToXml ($a, $level = 0) {
			$r = '';
			foreach ($a as $node => $value) {

				$node = preg_replace('#([0-9]+)$#is', '', $node);

				$r .= str_repeat("\t", $level) . '<' . self::_escapeXml($node) . '>';
				if (is_array($value)) {
					$r .= "\n" . self::_convertArrayToXml($value, $level + 1) . "\n";
				} else {
					$r .= self::_escapeXml($value);
				}
				$r .= '</' . self::_escapeXml($node) . '>' . "\n";
			}
			return $r;
			}
}