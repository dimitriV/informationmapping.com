<?php
class Storefront_VendorLoader_Model_Core_Translate extends Mage_Core_Model_Translate {
	
    /**
     * Retrieve translation file for module
     *
     * @param   string $module
     * @return  string
     */
    protected function _getModuleFilePath($module, $fileName)
    {
        $filePath = parent::_getModuleFilePath($module, $fileName);
    	
        $package = Mage::getSingleton('core/design_package');
		/* @var $package Mage_Core_Model_Design_Package */
        
        if(method_exists($package, 'fallbackVendor')){
        	$filePath = $package->fallbackVendor($filePath);
        	return $filePath;
        }else{
        	Mage::throwException('Mage_Core_Model_Design_Package is not overruled by the VendorLoader module');
        }
        
    }


}