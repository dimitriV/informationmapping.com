<?php
class Storefront_VendorLoader_Model_Core_Layout extends Mage_Core_Model_Layout{
	
	/**
	 * Block Factory
	 *
	 * @param     string $type
	 * @param     string $blockName
	 * @param     array $attributes
	 * @return    Mage_Core_Block_Abstract
	 */
	public function createBlock($type, $name='', array $attributes = array())
	{
		$r = parent::createBlock($type,$name,$attributes);
		if($r === false){
			// An exception occurred
			Mage::throwException(Mage::helper('core')->__('Invalid block type: %s', $type));
		}
		return $r;
	}
}