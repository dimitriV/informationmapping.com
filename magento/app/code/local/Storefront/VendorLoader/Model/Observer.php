<?php
class Storefront_VendorLoader_Model_Observer{
	
	private static $_wasBeforeReadingTemplateFileFired = false;
	
	public function beforeReadingTemplateFile(Varien_Event_Observer $observer) {
		self::$_wasBeforeReadingTemplateFileFired = true;
		
		$templateOptions = $observer->getTemplateOptions();
		$file = $templateOptions->includeFilePath;
		
		if(!file_exists($file)){
			$package = Mage::getSingleton('core/design_package');
			/* @var $package Storefront_VendorLoader_Model_Core_Design_Package */
			
			$file = $package->fallbackVendor($file);
			$templateOptions->includeFilePath = $file;
		}
		
		return $this;
	}

	
	/**
	 * This is a function that is used to determine if the module was installed properly.
	 * The event should always be fired, and this function should return true.
	 * @return boolean
	 */
	public function wasTemplateEventFired(){
		return self::$_wasBeforeReadingTemplateFileFired;
	}
	
	public function checkInstallation(){
		
		$loader = Mage::getSingleton('vendorloader/loader');
		/* @var $loader Storefront_VendorLoader_Model_Loader */
		
		if (!$loader->isMagentoVersionSupported()) {
			Mage::getSingleton ( 'adminhtml/session' )->addError ( Mage::helper('vendorloader')->__('Module VendorLoader does not support Magento version: %s', Mage::getVersion()));
		}
		
		if(! Mage::helper('vendorloader')->isInstalledProperly()){
			Mage::getSingleton ( 'adminhtml/session' )->addError ( Mage::helper('vendorloader')->__('Some Magento modules, developed by Storefront BVBA, may not be loaded due to a problem with the installation.') );
		}
	}
	
	public function checkInstalledForCron(Varien_Event_Observer $observer){
// 		if(! Mage::helper('vendorloader')->isInstalledProperly()){
// 			$msg = "Vendorloader is not installed properly for Cron.\n\nFile: ".__FILE__;
// 			mail('wouter.samaey@storefront.be', $msg, $msg);
// 		}
	}
	
	
}