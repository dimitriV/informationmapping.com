<?php
class Storefront_VendorLoader_Model_Compiler_Process extends Mage_Compiler_Model_Process {
	
	/**
	 * Copy files from all include directories to one.
	 * Lib files and controllers files will be copied as is
	 *
	 * @return Mage_Compiler_Model_Process
	 */
	protected function _collectFiles() {
		$r = parent::_collectFiles ();
		
		$vendorHelper = Mage::helper ( 'vendorloader' );
		
		$destDir = $this->_includeDir;
		$vendorModules = $vendorHelper->getVendorModulePaths ();
		
		$paths = array ();
		
		foreach ( $vendorModules as $vendorModule ) {
			$paths [] = $vendorModule . 'code';
		}
		
		foreach ( $paths as $path ) {
			//TODO controllers
			//$this->_controllerFolders = array ();
			$this->_copyVendor ( $path, $destDir );
			//$this->_copyControllers ( $path );
		}
		
		return $r;
	}
	
    /**
     * Copy directory
     *
     * @param   string $source
     * @param   string $target
     * @return  Mage_Compiler_Model_Process
     */
    protected function _copyVendor($source, $target, $firstIteration = true)
    {
        if (is_dir($source)) {
            $dir = dir($source);
            while (false !== ($file = $dir->read())) {
                if (($file[0] == '.')) {
                    continue;
                }
                $sourceFile = $source . DS . $file;
                if ($file == 'controllers') {
                    $this->_controllerFolders[] = $sourceFile;
                    continue;
                }

                if ($firstIteration) {
                    $targetFile = $target . DS . $file;
                } else {
                    $targetFile = $target . '_' . $file;
                }
                $this->_copyVendor($sourceFile, $targetFile, false);
            }
        } else {
            if (strpos(str_replace($this->_includeDir, '', $target), '-') || !in_array(substr($source, strlen($source)-4, 4), array('.php'))) {
                return $this;
            }
            
            $matches = array();
            $pattern = '#'.DS.'vendor'.DS.'(.*?)'.DS.'(.*?)'.DS.'code'.DS.'#';
            preg_match($pattern, $source, $matches);
            if(count($matches) === 3){
            	$moduleName = $matches[1].'_'.$matches[2];
            	
            	$target = str_replace($this->_includeDir. DS, '', $target);
            	$target = $this->_includeDir. DS. $moduleName .'_'. $target;
            	
            	copy($source, $target);
            }
        }
        return $this;
    }
}