<?php
class Storefront_PurchaseOrder_Model_Payment_Method_Purchaseorder extends Mage_Payment_Model_Method_Purchaseorder{
	
	protected $_canUseInternal = true;
	protected $_canUseCheckout = true;
	
	protected $_code  = 'purchaseorder';
	protected $_formBlockType = 'payment/form_purchaseorder';
	protected $_infoBlockType = 'payment/info_purchaseorder';
	
	/**
	 * Assign data to info model instance
	 *
	 * @param   mixed $data
	 * @return  Mage_Payment_Model_Method_Purchaseorder
	 */
	public function assignData($data)
	{
		if (!($data instanceof Varien_Object)) {
			$data = new Varien_Object($data);
		}
	
		$this->getInfoInstance()->setPoNumber($data->getPoNumber());
		return $this;
	}
	
}