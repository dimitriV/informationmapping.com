<?php
class Storefront_ImiTheme_Block_Cms_Page extends Mage_Cms_Block_Page{
	
	public function getCacheLifetime(){
		return 24 * 60 * 60;
	}
	
	public function getCacheKeyInfo()
	{
		$r = parent::getCacheKeyInfo();
		$r[] = 'store'.Mage::app()->getStore()->getId();
		$r[] = 'url'.$_SERVER['REQUEST_URI'];
		return $r;
	}
	
	public function getCacheTags(){
		$r = parent::getCacheTags();
		$r[] = Mage_Cms_Model_Page::CACHE_TAG;
		$r[] = Mage_Cms_Model_Page::CACHE_TAG.$this->getPage()->getId();
		return $r;
	}
	
}