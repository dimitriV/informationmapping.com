<?php
class Storefront_ImiTheme_Block_Review_Product_View_List extends Mage_Review_Block_Product_View_List{
	
	public function getCacheLifetime(){
		return 24 * 60 * 60;
	}
	
	public function getCacheKeyInfo()
	{
		$r = parent::getCacheKeyInfo();
		$r[] = 'url'.$_SERVER['REQUEST_URI'];
		return $r;
	}
	
}