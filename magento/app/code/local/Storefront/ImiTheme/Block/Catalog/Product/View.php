<?php
class Storefront_ImiTheme_Block_Catalog_Product_View extends Mage_Catalog_Block_Product_View{
	
	public function getCacheLifetime(){
		return 24 * 60 * 60;
	}
	
	public function getCacheKeyInfo()
	{
		$r = parent::getCacheKeyInfo();
		$r[] = 'url'.$_SERVER['REQUEST_URI'];
		return $r;
	}
	
	public function getCacheTags(){
		$r = parent::getCacheTags();
		$r[] = Mage_Catalog_Model_Product::CACHE_TAG;
		$r[] = Mage_Catalog_Model_Product::CACHE_TAG.$this->getProduct()->getId();
		return $r;
	}
	
}