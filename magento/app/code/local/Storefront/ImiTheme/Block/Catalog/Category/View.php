<?php
class Storefront_ImiTheme_Block_Catalog_Category_View extends Mage_Catalog_Block_Category_View{

	public function getCacheLifetime(){
		return 24 * 60 * 60;
	}

	public function getCacheKeyInfo()
	{
		$r = parent::getCacheKeyInfo();
		$r[] = 'url'.$_SERVER['REQUEST_URI'];
		return $r;
	}

	public function getCacheTags(){
		$r = parent::getCacheTags();
		$r[] = Mage_Catalog_Model_Category::CACHE_TAG;
		$r[] = Mage_Catalog_Model_Category::CACHE_TAG.$this->getCurrentCategory()->getId();
		return $r;
	}
}
