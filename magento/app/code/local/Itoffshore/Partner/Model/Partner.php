<?php

class Itoffshore_Partner_Model_Partner extends Mage_Core_Model_Abstract
{
    public function _construct()
    {
        parent::_construct();
        $this->_init('partner/partner');
    }
}