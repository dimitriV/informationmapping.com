<?php

class Itoffshore_Partner_Model_Mysql4_Partner extends Mage_Core_Model_Mysql4_Abstract
{
    public function _construct()
    {    
        // Note that the partner_id refers to the key field in your database table.
        $this->_init('partner/partner', 'partner_id');
    }
}