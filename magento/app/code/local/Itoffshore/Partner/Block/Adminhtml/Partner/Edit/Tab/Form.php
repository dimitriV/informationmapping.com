<?php

class Itoffshore_Partner_Block_Adminhtml_Partner_Edit_Tab_Form extends Mage_Adminhtml_Block_Widget_Form
{
  protected function _prepareForm()
  {
      $form = new Varien_Data_Form();
      $this->setForm($form);
      $fieldset = $form->addFieldset('partner_form', array('legend'=>Mage::helper('partner')->__('Item information')));
     
      $fieldset->addField('name', 'text', array(
          'label'     => Mage::helper('partner')->__('Name'),
          'class'     => 'required-entry',
          'required'  => true,
          'name'      => 'name',
      ));
      $fieldset->addField('email', 'text', array(
          'label'     => Mage::helper('partner')->__('Email'),         
          'required'  => false,
          'name'      => 'email',
      ));
      $fieldset->addField('address', 'editor', array(
          'name'      => 'address',
          'label'     => Mage::helper('partner')->__('Address'),
          'title'     => Mage::helper('partner')->__('Address'),
          'style'     => 'width:276px; height:100px;',
          'wysiwyg'   => false,
          'required'  => false,
      ));
        
      /*$fieldset->addField('filename', 'file', array(
          'label'     => Mage::helper('partner')->__('File'),
          'required'  => false,
          'name'      => 'filename',
	  ));*/
		
      $fieldset->addField('status', 'select', array(
          'label'     => Mage::helper('partner')->__('Status'),
          'name'      => 'status',
          'values'    => array(
              array(
                  'value'     => 1,
                  'label'     => Mage::helper('partner')->__('Enabled'),
              ),

              array(
                  'value'     => 2,
                  'label'     => Mage::helper('partner')->__('Disabled'),
              ),
          ),
      ));
     
      
     
      if ( Mage::getSingleton('adminhtml/session')->getPartnerData() )
      {
          $form->setValues(Mage::getSingleton('adminhtml/session')->getPartnerData());
          Mage::getSingleton('adminhtml/session')->setPartnerData(null);
      } elseif ( Mage::registry('partner_data') ) {
          $form->setValues(Mage::registry('partner_data')->getData());
      }
      return parent::_prepareForm();
  }
}