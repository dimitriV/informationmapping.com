<?php
class Itoffshore_Partner_Block_Adminhtml_Partner extends Mage_Adminhtml_Block_Widget_Grid_Container
{
  public function __construct()
  {
    $this->_controller = 'adminhtml_partner';
    $this->_blockGroup = 'partner';
    $this->_headerText = Mage::helper('partner')->__('Information Mapping Partner Manager');
    $this->_addButtonLabel = Mage::helper('partner')->__('Add Partner');
    parent::__construct();
  }
}