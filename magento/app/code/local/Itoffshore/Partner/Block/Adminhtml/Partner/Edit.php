<?php

class Itoffshore_Partner_Block_Adminhtml_Partner_Edit extends Mage_Adminhtml_Block_Widget_Form_Container
{
    public function __construct()
    {
        parent::__construct();
                 
        $this->_objectId = 'id';
        $this->_blockGroup = 'partner';
        $this->_controller = 'adminhtml_partner';
        
        $this->_updateButton('save', 'label', Mage::helper('partner')->__('Save Item'));
        $this->_updateButton('delete', 'label', Mage::helper('partner')->__('Delete Item'));
		
        $this->_addButton('saveandcontinue', array(
            'label'     => Mage::helper('adminhtml')->__('Save And Continue Edit'),
            'onclick'   => 'saveAndContinueEdit()',
            'class'     => 'save',
        ), -100);

        $this->_formScripts[] = "
            function toggleEditor() {
                if (tinyMCE.getInstanceById('partner_content') == null) {
                    tinyMCE.execCommand('mceAddControl', false, 'partner_content');
                } else {
                    tinyMCE.execCommand('mceRemoveControl', false, 'partner_content');
                }
            }

            function saveAndContinueEdit(){
                editForm.submit($('edit_form').action+'back/edit/');
            }
        ";
    }

    public function getHeaderText()
    {
        if( Mage::registry('partner_data') && Mage::registry('partner_data')->getId() ) {
            return Mage::helper('partner')->__("Edit Item '%s'", $this->htmlEscape(Mage::registry('partner_data')->getName()));
        } else {
            return Mage::helper('partner')->__('Add Item');
        }
    }
}