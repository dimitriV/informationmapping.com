<?php
class Itoffshore_Partner_Block_Partner extends Mage_Core_Block_Template
{
	public function _prepareLayout()
    {
		return parent::_prepareLayout();
    }
    
     public function getPartner()     
     { 
        if (!$this->hasData('partner')) {
            $this->setData('partner', Mage::registry('partner'));
        }
        return $this->getData('partner');
        
    }
}