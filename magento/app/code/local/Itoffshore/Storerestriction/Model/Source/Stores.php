<?php
class Itoffshore_Storerestriction_Model_Source_Stores{
    
    public function toOptionArray(){        	
    	$stores   = Mage::getModel('core/store_group')
                    ->getResourceCollection()				
                    ->setLoadDefault(true)    				
                    ->load();					
    				
    	$stores = $stores->toOptionArray();				
    	return $stores;

    }
}
?>