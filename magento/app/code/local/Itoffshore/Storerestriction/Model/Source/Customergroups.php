<?php
class Itoffshore_Storerestriction_Model_Source_Customergroups{
    
    public function toOptionArray()
    {
        $customer_groups = Mage::getResourceModel('customer/group_collection')
                /* ->setRealGroupsFilter() */
                ->loadData()
                ->toOptionArray();        
        return $customer_groups;
    }
}
?>