<?php

class Itoffshore_Importer_Model_Mysql4_Importer extends Mage_Core_Model_Mysql4_Abstract
{
    public function _construct()
    {    
        // Note that the importer_id refers to the key field in your database table.
        $this->_init('importer/importer', 'importer_id');
    }
}