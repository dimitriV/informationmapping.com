<?php

class Itoffshore_Importer_Adminhtml_ImporterController extends Mage_Adminhtml_Controller_action
{
    private $fields = array();
    
	protected function _initAction() {
		$this->loadLayout()
			->_setActiveMenu('importer/items')
			->_addBreadcrumb(Mage::helper('adminhtml')->__('Items Manager'), Mage::helper('adminhtml')->__('Item Manager'));
		
		return $this;
	}   
 
	public function indexAction() {
		$this->_initAction()
			->renderLayout();
	}

	public function editAction() {
		$id     = $this->getRequest()->getParam('id');
		$model  = Mage::getModel('importer/importer')->load($id);

		if ($model->getId() || $id == 0) {
			$data = Mage::getSingleton('adminhtml/session')->getFormData(true);
			if (!empty($data)) {
				$model->setData($data);
			}

			Mage::register('importer_data', $model);

			$this->loadLayout();
			$this->_setActiveMenu('importer/items');

			$this->_addBreadcrumb(Mage::helper('adminhtml')->__('Item Manager'), Mage::helper('adminhtml')->__('Item Manager'));
			$this->_addBreadcrumb(Mage::helper('adminhtml')->__('Item News'), Mage::helper('adminhtml')->__('Item News'));

			$this->getLayout()->getBlock('head')->setCanLoadExtJs(true);

			$this->_addContent($this->getLayout()->createBlock('importer/adminhtml_importer_edit'))
				->_addLeft($this->getLayout()->createBlock('importer/adminhtml_importer_edit_tabs'));

			$this->renderLayout();
		} else {
			Mage::getSingleton('adminhtml/session')->addError(Mage::helper('importer')->__('Item does not exist'));
			$this->_redirect('*//*/');
		}
	}
    /************CSV Utilities Start **************/
    /************CSV Utilities Start **************/
    /************CSV Utilities Start **************/
    private function getFieldIndex($field)
    {
    	return array_search($field, $this->fields);
    }
	function importOrderData($_data){
	   $data = array(
            /* Customer Info */
            'buyer_email'               => trim(htmlentities($_data[$this->getFieldIndex('buyer_email')], ENT_QUOTES))
            ,'buyer_firstname'          => $_data[$this->getFieldIndex('buyer_firstname')]
            ,'buyer_lastname'           => $_data[$this->getFieldIndex('buyer_lastname')]
            ,'buyer_street1'            => $_data[$this->getFieldIndex('buyer_street1')]
            ,'buyer_street2'            => $_data[$this->getFieldIndex('buyer_street2')]
            ,'buyer_city'               => $_data[$this->getFieldIndex('buyer_city')]
            ,'buyer_state'              => $_data[$this->getFieldIndex('buyer_state')]
            ,'buyer_zip'                => $_data[$this->getFieldIndex('buyer_zip')]
            ,'buyer_phone_number'       => $_data[$this->getFieldIndex('buyer_phone_number')]
            ,'buyer_country'            => $_data[$this->getFieldIndex('buyer_country')]
            
            /* Order Info */
            ,'order_created_date'       => $_data[$this->getFieldIndex('order_created_date')]
            ,'sku'                      => 'fspro-update-privileges-1y' //$_data[$this->getFieldIndex('sku')]
            ,'price'                    => $_data[$this->getFieldIndex('price')]
            ,'shipping_price'           => $_data[$this->getFieldIndex('shipping_price')]
            ,'tax'                      => $_data[$this->getFieldIndex('tax')]
            ,'quantity_purchased'       => $_data[$this->getFieldIndex('quantity_purchased')]
            
            /* Payment Method */
            ,'payment_method'           => $_data[$this->getFieldIndex('payment_method')]
            
            /* Shipping Method */
            ,'shipping_method'          => $_data[$this->getFieldIndex('shipping_method')]
            ,'currency'                 => $_data[$this->getFieldIndex('currency')] 
            ,'code'                     => $_data[$this->getFieldIndex('code')]           
        );
        //echo 'Datas<pre>'; print_r($data); 
        //preparing data        
        $storeId    = $this->_getStoreIdFromCode($data['code']);
        $storeId    = !empty($storeId) ? $storeId : 1; 
        Mage::log('STORE ID::'.$storeId);
        //check if the date format is correct or not
        if($this->_checkDateFormat($data['order_created_date']) === true){            
            $data['order_created_date'] = $this->_formatCsvDate($data['order_created_date']);
        }else{
            return false;
        }
        
        //get customer id from email, also check if the email already exists
    	if (!$this->checkCustomersExists($data['buyer_email'])) {
    		$this->createCustomer($data);
            #echo ('Customer email address was added.');
    	}
       
        $customer = $this->getCustomerByEmail($data['buyer_email']);
        $customerAddressId = 0;              
        $customerAddresses  = $customer->getAddresses();
        foreach ($customer->getAddresses() as $address) {
            $customerAddressId = $address->getId();
        }
        $billingAddress     = (isset($customerAddresses[$customerAddressId])) ? $this->getOrderAddress($customerAddresses[$customerAddressId]) : Mage::getModel('sales/order_address');
        $shippingAddress    = (isset($customerAddresses[$customerAddressId])) ? $this->getOrderAddress($customerAddresses[$customerAddressId]) : Mage::getModel('sales/order_address');
        #echo '##Step1##';
        $order = Mage::getModel('sales/order')
                     ->setIncrementId(Mage::getSingleton('eav/config')->getEntityType('order')->fetchNewIncrementId($storeId))
                     ->setStoreId($storeId)                     
                     ->setBillingAddress($billingAddress)
                     ->setShippingAddress($shippingAddress)
                     ->setCreatedAt($data['order_created_date'])
                     ->setCustomerId($customer->getId())
                     ->setCustomerGroupId($customer->getGroupId())
                     ->setCustomerFirstname($customer->getFirstname())
                     ->setCustomerLastname($customer->getLastname())
    	             ->setCustomerIsGuest(0)
    	             ->setCustomerEmail($data['buyer_email']);
        #echo '##Step2##';             
        $payment = Mage::getModel('sales/order_payment');
    	$payment->setMethod($data['payment_method']);        
    	$order->setPayment($payment);
        #echo '##Step3##';
        
        $order->setShippingMethod($data['shipping_method']);
        
        $currency = $data["currency"];		
		$order->setBaseCurrencyCode($currency);
		$order->setStoreCurrencyCode($currency);
		$order->setOrderCurrencyCode($currency);
        
        /***********************************************
        * ADD ITEMS TO ORDER - START
        ***********************************************/
        $catalog        = Mage::getModel('catalog/product');
        $sku            = $data['sku'];
        $productId      = $catalog->getIdBySku($sku);
        $product        = $catalog->load($productId);

        if (!$product->getId()) {
            //
        }
		$item = Mage::getModel('sales/order_item')->setProductId($product->getId())
            ->setSku($product->getSku())
            ->setName($product->getName())
            ->setWeight($product->getWeight())
            ->setTaxClassId($product->getTaxClassId())
            ->setCost($product->getCost())
            ->setOriginalPrice($product->getPrice())
            ->setIsQtyDecimal($product->getIsQtyDecimal());

        if($product->getSuperProduct()) {
            $item->setSuperProductId($product->getSuperProduct()->getId());
            if ($product->getSuperProduct()->isConfigurable()) {
                $item->setName($product->getSuperProduct()->getName());
            }
        }
        #echo '##Step4##';
        $price      = $data['price'];        
        $shipping   = $data['shipping_price'];    	
        $tax        = $data['tax'];
    	
		$rate         = (((float) ($price + $tax)) * 100 ) / $price;
		$rate         = round($rate - 100, 3);

        $item->setProduct($product)
        		->setPrice($price)
        		->setQtyOrdered($data['quantity_purchased'])
        		->setTaxAmount($tax)
        		->setTaxPercent($rate)
        		->setRowTotal((float)$price * (float)$data['quantity_purchased'])
        		->setRowWeight((float)$product->getWeight() * (float)$data['quantity_purchased']);
        
    	$order->addItem($item);
        #echo '##Step5##';
        
    	// Update totals
    	$order->setSubtotal($order->getSubtotal() + $item->getRowTotal())
    			->setBaseSubtotal($order->getBaseSubtotal() + $item->getRowTotal())
    			->setGrandTotal($order->getGrandTotal() + $item->getRowTotal() +
    									(float)$shipping + (float)$tax)
				->setBaseGrandTotal($order->getBaseGrandTotal() + $item->getRowTotal() +
    									(float)$shipping + (float)$tax)
    			->setShippingAmount($order->getShippingAmount() +
    									(float)$shipping)
    			->setTaxAmount($order->getTaxAmount() +
    									(float)$tax);

    	$order->setTotalPaid($order->getGrandTotal());
        /***********************************************
        * ADD ITEMS TO ORDER - END
        ***********************************************/
        
        $order->setStatus('complete');
		$order->addStatusToHistory('complete');

		#these fields are used on reports
		$order->setStoreToBaseRate('1.0000');
		$order->setStoreToOrderRate('1.0000');
        
        $order->save();
        return $order->getId();        
        #echo '##Step6##';        
        #print_r($order);
        #echo '##THE END##';
    }
    
    private function _checkDateFormat($date){
        $date = explode('/', $date);
        
        $return = false;
        if(!empty($date)){
            if(checkdate($date[1], $date[0], substr($date[2], 0, 4))){
                $return = true;
            }
        }        
        return $return;
    }
    
    private function _formatCsvDate($date){
         $date = explode('/', $date);
         $ymd = substr($date[2], 0, 4) . '-' . $date[1] . '-' . $date[0] . ' ' . date('H:i:s');
         Mage::log('YMD:'.$ymd);
         return $ymd;
         /*Mage::log($date);
         
         $timestamp = mktime(0, 0, 0, $date[1], $date[0], $date[2]);
         Mage::log('timestamp:'.$timestamp);
         $timestamp = Mage::getModel('core/date')->timestamp($timestamp); //making magento compatible
         return $timestamp;*/
         #$output = Mage::getModel('core/date')->date('Y-m-d' . ' 00:00:00', $timestamp);
         #Mage::log('date:'.$output);
         #return $output;        
         #return $formatted_date;
         #return date('Y-m-d', mktime(0, 0, 0, $date[1], $date[0], $date[2])); 
    }
    
    private function getOrderAddress($address) 
    {
        return Mage::getModel('sales/order_address')
                ->setStoreId($address->getStoreId())
                ->setAddressType($address->getAddressType())
                ->setCustomerId($address->getCustomerId())
                ->setCustomerAddressId($address->getCustomerAddressId())
                ->setFirstname($address->getFirstname())
                ->setLastname($address->getLastname())
                ->setCompany($address->getCompany())
                ->setStreet($address->getStreet(-1))
                ->setCity($address->getCity())
                ->setRegion($address->getRegion())
                ->setRegionId($address->getRegionId())
                ->setPostcode($address->getPostcode())
                ->setCountryId($address->getCountryId())
                ->setTelephone($address->getTelephone())
                ->setFax($address->getFax());
    }
            
    private function checkCustomersExists($email)
    {
    	$customer = Mage::getModel('customer/customer');
    	if ($customer->getSharingConfig()->isWebsiteScope()) {
            $customer->setWebsiteId(Mage::app()->getStore()->getWebsiteId());
        }
    	$customer->loadByEmail($email);
    	return ($customer->getId());
    }

    private function getCustomerByEmail($email)
    {
    	$customer = Mage::getModel('customer/customer');
    	if ($customer->getSharingConfig()->isWebsiteScope()) {
            $customer->setWebsiteId(Mage::app()->getStore()->getWebsiteId());
        }
    	$customer->loadByEmail($email);
    	return $customer;
    }
    
    private function createCustomer($data)
    {
    	$buyer_email        = $data['buyer_email'];        
        $buyer_firstname    = $data['buyer_firstname'];
        $buyer_lastname     = $data['buyer_lastname'];
        $buyer_street1      = $data['buyer_street1'];
        $buyer_street2      = $data['buyer_street2'];
        $buyer_city         = $data['buyer_city'];
        $buyer_state        = $data['buyer_state'];
        $buyer_zip          = $data['buyer_zip'];
        $buyer_phone_number = $data['buyer_phone_number'];
        $buyer_country      = $data['buyer_country'];
        
    	$customer = $this->getCustomerByEmail($buyer_email);
    	#$customer->isDeleted(false);
    	#$customer->cleanAllAddresses();
        $customer->setForceConfirmed(true);
    	
    	$customer->setFirstname($buyer_firstname);
    	$customer->setLastname($buyer_lastname);
        
    	$customer_address = Mage::getModel('customer/address');

    	$customer_address->setFirstname($buyer_firstname)
    	                 ->setLastname($buyer_lastname)
  	                     ->setStreet($buyer_street1 . "\n" . $buyer_street2)
    	                 ->setCity($buyer_city)
  	                     ->setRegion($buyer_state)   	
    	                 ->setPostcode($buyer_zip)
    	                 ->setTelephone($buyer_phone_number)
    	                 ->setCountryId($buyer_country)                        
                		 ->setIsDefaultBilling('1')
                		 ->setIsDefaultShipping('1')
                		 ->setSaveInAddressBook('1');

                
    	$customer->setEmail($buyer_email);
		$customer->addAddress($customer_address);
    	$customer->save();		
    }
    
    private function _importToSmaSubscription($data){
        $order_id   = $data['order_id'];
        $order      = Mage::getModel('sales/order')->load($order_id);        
                                
        if($order->getId()){
            //get sku, currently only one is supported            
            /*$skus           = array();  
            $oitems         = array();       
            $items          = $order->getAllItems();   
            foreach($items as $item){           
                $oitems[$item->getSku()] = $item->getQtyOrdered();               
            }  
            $skus           = array_keys($oitems);*/   
            //$sku            = 'sma-subscription-1y';
            //$sku            = 'fspro-update-privileges-1y';
            $sku    = $data['sku'];
                                
            $access_code                    = $this->_generateAccessCode($order->getId(), $sku);
            $sma_start_date                 = $order->getCreatedAt();
            /* # DONE TO IMPORT REMAINING PRODUCTS OF IMPORT V5.CSV
            if($data['createdat'] != '') {
                $sma_start_date                 = $data['createdat'];   
            } else {
                $sma_start_date                 = $order->getCreatedAt();    
            } */           
            $sma_onemonth_before_end_date   = $this->_getFutureDate($sma_start_date, '1', '1');
            $sma_twomonth_before_end_date   = $this->_getFutureDate($sma_start_date, '1', '2');
            $sma_end_date                   = $this->_getFutureDate($sma_start_date, '1', '0');  
            $sma_two_week_after_end_date    = $this->_getFutureDate($sma_start_date, '1', '-1'); 
            
            /* # DONE TO IMPORT REMAINING PRODUCTS OF IMPORT V5.CSV
            if($data['createdat'] != '') {
                $sma_onemonth_before_end_date   = '2010-10-16';                
                $isOnemonthBeforeEmailNotified = '0'; 
            } else {
                $sma_onemonth_before_end_date   = $this->_getFutureDate($sma_start_date, '1', '1');                
                $isOnemonthBeforeEmailNotified = $this->_checkIfDateIsOlder($sma_onemonth_before_end_date);
            }*/            
            //for notification status
            $isOnemonthBeforeEmailNotified = $this->_checkIfDateIsOlder($sma_onemonth_before_end_date);
            $isTwomonthBeforeEmailNotified = $this->_checkIfDateIsOlder($sma_twomonth_before_end_date);   
            $isEndmonthEmailNotified       = $this->_checkIfDateIsOlder($sma_end_date);
            
            if($isOnemonthBeforeEmailNotified && $isTwomonthBeforeEmailNotified && $isEndmonthEmailNotified){
                $is_renewed = 1;
            }else{
                $is_renewed = 0;
            }      
            
            $qty                            = $data['quantity_purchased'];
                                    
            $model = Mage::getModel('smasubscription/smasubscription');
            $model->setCustomerId($order->getCustomerId());
            $model->setOrderId($order->getId());
            $model->setOrderNo($order->getIncrementId());
            $model->setProductSku($sku);
            $model->setQty($qty);
            $model->setSmaType('1');
            $model->setSmaStartDate($sma_start_date);
            $model->setSmaOnemonthBeforeEndDate($sma_onemonth_before_end_date);
            $model->setSmaTwomonthBeforeEndDate($sma_twomonth_before_end_date);
            $model->setSmaEndDate($sma_end_date);
            $model->setSmaTwoWeekAfterEndDate($sma_two_week_after_end_date);
            $model->setIsRenewed($is_renewed);
            $model->setIsOnemonthBeforeEmailNotified($isOnemonthBeforeEmailNotified);
            $model->setIsTwomonthBeforeEmailNotified($isTwomonthBeforeEmailNotified);
            $model->setIsEndmonthEmailNotified($isEndmonthEmailNotified);
            $model->setAccessCode($access_code);    
            try{
                $model->save();          
                #Mage::helper('smasubscription')->log('ORDER IMPORT::1 YEAR SMA SAVE, ID::'.$model->getId()); 
            }catch(Exception $e){
                Mage::helper('smasubscription')->log('ORDER IMPORT::1 YEAR SMA ERROR ::'.$e->getMessage());
                exit(); 
            }          
                    
        }        
            
    }
    
    private function _getFutureDate($order_created_date, $for, $type){
        $connection = Mage::getSingleton('core/resource')->getConnection('core_read');
        $interval_sql = '';
        $double_add = false;
        
        # for 1 year sma
        if($for == '1'){
            if($type == '1'){
                $interval_sql = '11 MONTH';
            }
            if($type == '2'){
               $interval_sql = '10 MONTH';
            }
            if($type == '0'){
                $interval_sql = '12 MONTH';
            }
            if($type == '-1'){
                $interval_sql = '12 MONTH';                
                $double_add = true;
            }
        }
        
        # for 2 year sma
        if($for == '2'){
            if($type == '1'){
                $interval_sql = '23 MONTH';
            }
            if($type == '2'){
               $interval_sql = '22 MONTH';
            }
            if($type == '0'){
                $interval_sql = '24 MONTH';
            }
            if($type == '-1'){
                $interval_sql = '24 MONTH';
                $double_add = true;
            }
        }
        
        # for 3 year sma
        if($for == '3'){
            if($type == '1'){
                $interval_sql = '35 MONTH';
            }
            if($type == '2'){
               $interval_sql = '34 MONTH';
            }
            if($type == '0'){
                $interval_sql = '36 MONTH';
            }
            if($type == '-1'){
                $interval_sql = '36 MONTH';
                $double_add = true;
            }
        }       
        
        if($double_add === true){
            $sql        = "SELECT DATE_FORMAT(DATE_ADD(DATE_ADD('$order_created_date', INTERVAL ".$interval_sql."), INTERVAL 2 WEEK), '%Y-%m-%d %H:%i:%s') AS future_date";
        }else{
            $sql        = "SELECT DATE_FORMAT(DATE_ADD('$order_created_date', INTERVAL ".$interval_sql."), '%Y-%m-%d %H:%i:%s') AS future_date";
        }
        
        Mage::helper('smasubscription')->log('SQL:: '.$sql);
        return $connection->fetchOne($sql);
    }
    
    private function _generateAccessCode($order_id, $sku){        
        return md5(uniqid(rand().$order_id.$sku, true));
    }
    
    private function _checkIfDateIsOlder($sma_date){
        $connection = Mage::getSingleton('core/resource')->getConnection('core_read');
        $sql        = "SELECT IF(NOW() > '$sma_date', 1, 0) AS date_check";        
        return $connection->fetchOne($sql);
    }   
    
    private function _getStoreIdFromCode($code){
        $connection = Mage::getSingleton('core/resource')->getConnection('core_read');
        $sql        = "SELECT store_id FROM core_store WHERE `code` = '".$code."' LIMIT 1";        
        return $connection->fetchOne($sql);
    }     
    /************CSV Utilities End **************/
    /************CSV Utilities End **************/
    /************CSV Utilities End **************/
    
    public function newAction() {
		$this->_forward('edit');
	}
	
    public function saveAction() {
        set_time_limit(0);
		if ($data = $this->getRequest()->getPost()) {			
           
			if(isset($_FILES['filename']['name']) && $_FILES['filename']['name'] != '') {
				try {	
					/* Starting upload */	
					$uploader = new Varien_File_Uploader('filename');
					
					// Any extention would work
	           		$uploader->setAllowedExtensions(array('csv'));
					$uploader->setAllowRenameFiles(false);
					
					// Set the file upload mode 
					// false -> get the file directly in the specified folder
					// true -> get the file in the product like folders 
					//	(file.jpg will go in something like /media/f/i/file.jpg)
					$uploader->setFilesDispersion(false);
							
					// We set media as the upload dir
					$path = Mage::getBaseDir('media') . DS . 'order_import' . DS;
					$uploader->save($path, $_FILES['filename']['name'] );
                    
                    // Customer/Order Import for Uploaded CSV
                    $csvObject  = new Varien_File_Csv();
                    $csvObject->setDelimiter(",");
                    $csvData = $csvObject->getData($path. $uploader->getUploadedFileName(), 0, 1);
                    $this->fields = $csvData[0];
                    array_shift($csvData);
                    #print_r($csvData);
                    $output = '<h1>Import Results</h1>';
                    //Looping the data
                    if(count($csvData) > 0){
                        $count = 1;
                        foreach($csvData as $key => $data){ 
                            
                            try{
                                $order_id = $this->importOrderData($data);
                                
                                if(!empty($order_id) && $order_id > 0){
                                    //Import into the SMA Subscription Table
                                    $data['order_id'] = $order_id;
                                    $data['quantity_purchased'] = $data[$this->getFieldIndex('quantity_purchased')];
                                    $data['sku']      = $data[$this->getFieldIndex('sku')];
                                    $this->_importToSmaSubscription($data);                                                                
                                    
                                 }
                                 $status_message = 'Success';
                                 //print_r($data); exit();
                            }catch(Exception $e){
                                 $status_message = 'Error::Customer:'.$data[$this->getFieldIndex('buyer_email')].'::'.$e->getMessage();  
                            }
                                                                                  
                            $output .= 'Importing <strong>Row:</strong> '.($key + 1). ', Result: '.$status_message.'<br />'; 
                            
                            /*if($count == 100){
                                break;
                            }*/
                            
                            $count++;                           
                        }
                    }                   
				} catch (Exception $e) {
		              Mage::getSingleton('adminhtml/session')->addError($e->getMessage());
                      Mage::getSingleton('adminhtml/session')->setFormData($data);
                      $this->_redirect('*/*/edit', array('id' => $this->getRequest()->getParam('id')));
                      return;
		        }
	        
		        //this way the name is saved in DB
	  			$data['filename'] = $_FILES['filename']['name'];
			}
	  			
	  			
			$model = Mage::getModel('importer/importer');		
			$model->setData($data)
				  ->setId($this->getRequest()->getParam('id'));
			
			try {
				if ($model->getCreatedTime == NULL || $model->getUpdateTime() == NULL) {
					$model->setCreatedTime(now())
						->setUpdateTime(now());
				} else {
					$model->setUpdateTime(now());
				}	
				
				$model->save();
				Mage::getSingleton('adminhtml/session')->addSuccess(Mage::helper('importer')->__($output));
				Mage::getSingleton('adminhtml/session')->setFormData(false);

				if ($this->getRequest()->getParam('back')) {
					$this->_redirect('*/*/edit', array('id' => $model->getId()));
					return;
				}
				$this->_redirect('*/*/');
				return;
            } catch (Exception $e) {
                Mage::getSingleton('adminhtml/session')->addError($e->getMessage());
                Mage::getSingleton('adminhtml/session')->setFormData($data);
                $this->_redirect('*/*/edit', array('id' => $this->getRequest()->getParam('id')));
                return;
            }
        }
        Mage::getSingleton('adminhtml/session')->addError(Mage::helper('importer')->__('Unable to find item to save'));
        $this->_redirect('*/*/');
	}
 
	public function deleteAction() {
		if( $this->getRequest()->getParam('id') > 0 ) {
			try {
				$model = Mage::getModel('importer/importer');
				 
				$model->setId($this->getRequest()->getParam('id'))
					->delete();
					 
				Mage::getSingleton('adminhtml/session')->addSuccess(Mage::helper('adminhtml')->__('Item was successfully deleted'));
				$this->_redirect('*/*/');
			} catch (Exception $e) {
				Mage::getSingleton('adminhtml/session')->addError($e->getMessage());
				$this->_redirect('*/*/edit', array('id' => $this->getRequest()->getParam('id')));
			}
		}
		$this->_redirect('*/*/');
	}

    public function massDeleteAction() {
        $importerIds = $this->getRequest()->getParam('importer');
        if(!is_array($importerIds)) {
			Mage::getSingleton('adminhtml/session')->addError(Mage::helper('adminhtml')->__('Please select item(s)'));
        } else {
            try {
                foreach ($importerIds as $importerId) {
                    $importer = Mage::getModel('importer/importer')->load($importerId);
                    $importer->delete();
                }
                Mage::getSingleton('adminhtml/session')->addSuccess(
                    Mage::helper('adminhtml')->__(
                        'Total of %d record(s) were successfully deleted', count($importerIds)
                    )
                );
            } catch (Exception $e) {
                Mage::getSingleton('adminhtml/session')->addError($e->getMessage());
            }
        }
        $this->_redirect('*/*/index');
    }
    
    public function smaimportAction(){
        $path       = Mage::getBaseDir('media') . DS . 'order_import' . DS . 'Import v6.csv';//'Import v5.csv';
        $csvObject  = new Varien_File_Csv();
        $csvObject->setDelimiter(",");
        $csvData    = $csvObject->getData($path, 0, 1);
        $this->fields = array();
        $this->fields = $csvData[0];
        array_shift($csvData);
        
        $output = '<h1>SMA Import Results</h1>';
        
        //Looping the data
        if(count($csvData) > 0){
            $count = 1;
            foreach($csvData as $key => $data){ 
                
                try{
                    $order_number   = $data[$this->getFieldIndex('order_number')];  
                    $order_obj      = Mage::getModel('sales/order')->loadByIncrementId($order_number);
                    if($order_obj->getId()){                        
                        $feed_array             = array();
                        $feed_array['order_id'] = $order_obj->getId();
                        $feed_array['quantity_purchased'] = $data[$this->getFieldIndex('quantity_purchased')];
                        $feed_array['sku']      = $data[$this->getFieldIndex('sku')];
                        
                        /* # DONE TO IMPORT REMAINING PRODUCTS OF IMPORT V5.CSV
                        if($count <= 113 ) {
                            $feed_array['createdat'] = '2009-11-02';
                        } else {
                            $feed_array['createdat'] = '';
                        }
                        */
                        //echo $order_number.'<pre>'; print_r($feed_array); exit();
                        $this->_importToSmaSubscription($feed_array);
                        $status_message = 'Success::ORDER #:'.$data[$this->getFieldIndex('order_number')]; 
                    }else{
                        $status_message = 'Error::ORDER #:'.$data[$this->getFieldIndex('order_number')].' (Such order Not found in the system)'; 
                    }                                                                       
                    
                    
                }catch(Exception $e){
                     $status_message = 'Error::ORDER #:'.$data[$this->getFieldIndex('order_number')].'::'.$e->getMessage();  
                }
                                                                      
                $output .= 'Importing <strong>Row:</strong> '.($key + 1). ', Result: '.$status_message.'<br />'; 
                
                /*if($count == 100){
                    break;
                }*/
                
                $count++;                           
            }
        }
        Mage::getSingleton('adminhtml/session')->addSuccess(Mage::helper('importer')->__($output));
        $this->_redirect('smasubscription/adminhtml_smasubscription/index/');
    }
    
    public function updateorderstoreAction(){
        $path       = Mage::getBaseDir('media') . DS . 'order_import' . DS . 'order-store-update.csv';
        $csvObject  = new Varien_File_Csv();
        $csvObject->setDelimiter(",");
        $csvData    = $csvObject->getData($path, 0, 1);
        $this->fields = array();
        $this->fields = $csvData[0];
        array_shift($csvData);
        $output = '<h1>Order Store Update Results</h1>';
        
        //Looping the data
        if(count($csvData) > 0){
            $count = 1;
            foreach($csvData as $key => $data){ 
                
                try{
                    $order_number   = $data[$this->getFieldIndex('order_number')];  
                    $order_obj      = Mage::getModel('sales/order')->loadByIncrementId($order_number);                    
                    if($order_obj->getId()){ 
                        $storeCode = $data[$this->getFieldIndex('code')];
                        $storeId = $this->_getStoreIdFromCode($storeCode);//Mage::getModel('core/store')->load($storeCode)->getId();
                        $order_obj->setStoreId($storeId);
                        $order_obj->save();
                        $status_message = 'Success::ORDER #:'.$data[$this->getFieldIndex('order_number')]; 
                    }else{
                        $status_message = 'Error::ORDER #:'.$data[$this->getFieldIndex('order_number')].' (Such order Not found in the system)'; 
                    }                                                                       
                    
                    
                }catch(Exception $e){
                     $status_message = 'Error::ORDER #:'.$data[$this->getFieldIndex('order_number')].'::'.$e->getMessage();  
                }
                                                                      
                $output .= 'Updating <strong>Row:</strong> '.($key + 1). ', Result: '.$status_message.'<br />'; 
                
                /*if($count == 10){
                    break;
                }*/
                
                $count++;                           
            }
        }
        Mage::getSingleton('adminhtml/session')->addSuccess(Mage::helper('importer')->__($output));
        $this->_redirect('smasubscription/adminhtml_smasubscription/index/');
    }
	    
}