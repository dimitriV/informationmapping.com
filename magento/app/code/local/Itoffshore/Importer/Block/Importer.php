<?php
class Itoffshore_Importer_Block_Importer extends Mage_Core_Block_Template
{
	public function _prepareLayout()
    {
		return parent::_prepareLayout();
    }
    
     public function getImporter()     
     { 
        if (!$this->hasData('importer')) {
            $this->setData('importer', Mage::registry('importer'));
        }
        return $this->getData('importer');
        
    }
}