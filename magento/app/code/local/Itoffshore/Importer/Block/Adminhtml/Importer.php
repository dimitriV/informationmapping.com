<?php
class Itoffshore_Importer_Block_Adminhtml_Importer extends Mage_Adminhtml_Block_Widget_Grid_Container
{
  public function __construct()
  {
    $this->_controller = 'adminhtml_importer';
    $this->_blockGroup = 'importer';
    $this->_headerText = Mage::helper('importer')->__('Customer/Order CSV Importer');
    $this->_addButtonLabel = Mage::helper('importer')->__('Upload CSV File');
    parent::__construct();
  }
}