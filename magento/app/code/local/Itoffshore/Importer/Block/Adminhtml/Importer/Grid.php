<?php

class Itoffshore_Importer_Block_Adminhtml_Importer_Grid extends Mage_Adminhtml_Block_Widget_Grid
{
  public function __construct()
  {
      parent::__construct();
      $this->setId('importerGrid');
      $this->setDefaultSort('created_time');
      $this->setDefaultDir('DESC');
      $this->setSaveParametersInSession(true);
  }

  protected function _prepareCollection()
  {
      $collection = Mage::getModel('importer/importer')->getCollection();
      $this->setCollection($collection);
      return parent::_prepareCollection();
  }

  protected function _prepareColumns()
  {
      $this->addColumn('importer_id', array(
          'header'    => Mage::helper('importer')->__('ID'),
          'align'     =>'right',
          'width'     => '50px',
          'index'     => 'importer_id',
      ));

      /*$this->addColumn('title', array(
          'header'    => Mage::helper('importer')->__('Title'),
          'align'     =>'left',
          'index'     => 'title',
      ));
      */  
      $this->addColumn('created_time', array(
          'header'    => Mage::helper('importer')->__('Imported Date'),
          'align'     =>'left',
          'index'     => 'created_time',
      ));
                        
	  /*
      $this->addColumn('content', array(
			'header'    => Mage::helper('importer')->__('Item Content'),
			'width'     => '150px',
			'index'     => 'content',
      ));
	  */

      /*$this->addColumn('status', array(
          'header'    => Mage::helper('importer')->__('Status'),
          'align'     => 'left',
          'width'     => '80px',
          'index'     => 'status',
          'type'      => 'options',
          'options'   => array(
              1 => 'Enabled',
              2 => 'Disabled',
          ),
      ));
	  */
       /* $this->addColumn('action',
            array(
                'header'    =>  Mage::helper('importer')->__('Action'),
                'width'     => '100',
                'type'      => 'action',
                'getter'    => 'getId',
                'actions'   => array(
                    array(
                        'caption'   => Mage::helper('importer')->__('Edit'),
                        'url'       => array('base'=> '*//*/edit'),
                        'field'     => 'id'
                    )
                ),
                'filter'    => false,
                'sortable'  => false,
                'index'     => 'stores',
                'is_system' => true,
        ));
		*/
		#$this->addExportType('*/*/exportCsv', Mage::helper('importer')->__('CSV'));
		#$this->addExportType('*/*/exportXml', Mage::helper('importer')->__('XML'));
	  
      return parent::_prepareColumns();
  }

    protected function _prepareMassaction()
    {
        $this->setMassactionIdField('importer_id');
        $this->getMassactionBlock()->setFormFieldName('importer');

        $this->getMassactionBlock()->addItem('delete', array(
             'label'    => Mage::helper('importer')->__('Delete'),
             'url'      => $this->getUrl('*/*/massDelete'),
             'confirm'  => Mage::helper('importer')->__('Are you sure?')
        ));

        /*$statuses = Mage::getSingleton('importer/status')->getOptionArray();

        array_unshift($statuses, array('label'=>'', 'value'=>''));
        $this->getMassactionBlock()->addItem('status', array(
             'label'=> Mage::helper('importer')->__('Change status'),
             'url'  => $this->getUrl('*//*/massStatus', array('_current'=>true)),
             'additional' => array(
                    'visibility' => array(
                         'name' => 'status',
                         'type' => 'select',
                         'class' => 'required-entry',
                         'label' => Mage::helper('importer')->__('Status'),
                         'values' => $statuses
                     )
             )
        ));*/
        return $this;
    }

  public function getRowUrl($row)
  {
      return $this->getUrl('*/*/edit', array('id' => $row->getId()));
  }

}