<?php

class Itoffshore_Importer_Block_Adminhtml_Importer_Edit_Tab_Form extends Mage_Adminhtml_Block_Widget_Form
{
  protected function _prepareForm()
  {
      $form = new Varien_Data_Form();
      $this->setForm($form);
      $fieldset = $form->addFieldset('importer_form', array('legend'=>Mage::helper('importer')->__('Item information')));
     
      /*$fieldset->addField('title', 'text', array(
          'label'     => Mage::helper('importer')->__('Title'),
          'class'     => 'required-entry',
          'required'  => true,
          'name'      => 'title',
      ));*/

      $fieldset->addField('filename', 'file', array(
          'label'     => Mage::helper('importer')->__('CSV File'),
          'required'  => false,
          'name'      => 'filename',
          'required'  => true,
	  ));
		
      /*$fieldset->addField('status', 'select', array(
          'label'     => Mage::helper('importer')->__('Status'),
          'name'      => 'status',
          'values'    => array(
              array(
                  'value'     => 1,
                  'label'     => Mage::helper('importer')->__('Enabled'),
              ),

              array(
                  'value'     => 2,
                  'label'     => Mage::helper('importer')->__('Disabled'),
              ),
          ),
      ));
     */
     /* $fieldset->addField('content', 'editor', array(
          'name'      => 'content',
          'label'     => Mage::helper('importer')->__('Content'),
          'title'     => Mage::helper('importer')->__('Content'),
          'style'     => 'width:700px; height:500px;',
          'wysiwyg'   => false,
          'required'  => true,
      ));
     */
      if ( Mage::getSingleton('adminhtml/session')->getImporterData() )
      {
          $form->setValues(Mage::getSingleton('adminhtml/session')->getImporterData());
          Mage::getSingleton('adminhtml/session')->setImporterData(null);
      } elseif ( Mage::registry('importer_data') ) {
          $form->setValues(Mage::registry('importer_data')->getData());
      }
      return parent::_prepareForm();
  }
}