<?php

class Itoffshore_Importer_Block_Adminhtml_Importer_Edit_Tabs extends Mage_Adminhtml_Block_Widget_Tabs
{

  public function __construct()
  {
      parent::__construct();
      $this->setId('importer_tabs');
      $this->setDestElementId('edit_form');
      $this->setTitle(Mage::helper('importer')->__('Item Information'));
  }

  protected function _beforeToHtml()
  {
      $this->addTab('form_section', array(
          'label'     => Mage::helper('importer')->__('Item Information'),
          'title'     => Mage::helper('importer')->__('Item Information'),
          'content'   => $this->getLayout()->createBlock('importer/adminhtml_importer_edit_tab_form')->toHtml(),
      ));
     
      return parent::_beforeToHtml();
  }
}