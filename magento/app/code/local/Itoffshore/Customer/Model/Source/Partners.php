<?php
class Itoffshore_Customer_Model_Source_Partners extends Mage_Eav_Model_Entity_Attribute_Source_Abstract
{
    public function getAllOptions()
    {
        if (!$this->_options) {
            $connection = Mage::getSingleton('core/resource')->getConnection('core_read');
            $sql        = "SELECT * FROM partner WHERE status = '1' ORDER BY name";
            $rows       = $connection->fetchAll($sql);
            
            if(!empty($rows)){
                foreach ($rows as $row) {                            
                    $this->_options[] = array(
                        'label' => $row['name'],
                        'value' => $row['partner_id']);                
                }
            }
        }
        return $this->_options;        
    }
	
    public function getPartnerById($partner_id)
    {
	$partner_name = '';
        if($partner_id) {
            $connection = Mage::getSingleton('core/resource')->getConnection('core_read');
            $sql        = "SELECT name FROM partner WHERE partner_id = ".$partner_id;
            $partner_name = $connection->fetchOne($sql);
        }
        
        if(!$partner_name) {
            $partner_name = 'Unknown Partner';
        }
        return $partner_name;
    }
}