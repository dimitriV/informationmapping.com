<?php
//@note: No longer been used
class Itoffshore_Customer_Model_Observer
{
    public function checkForPartner($observer){
        #$customer = $observer->getEvent()->getCustomer();         
        if(Mage::getSingleton('customer/session')->isLoggedIn()) {
            $customer = Mage::getSingleton('customer/session')->getCustomer();
            
            if($customer->getPartner() > 0){            
                Mage::log('Observer after '.get_class($observer->getEvent()).':Partner ID:'.print_r($customer->getData(), true));
            }else{
                Mage::log('Observer after '.get_class($observer->getEvent()).':No Partner ID:'.print_r($customer->getData(), true));
                Mage::getSingleton('core/session')->addError('In order to proceed, you must select your Information Mapping Partner. Go <a href="'.Mage::getUrl("customer/account/edit").'">here</a> to edit.');            
                Mage::app()->getResponse()->setRedirect(Mage::getUrl("customer/account/edit"));            
            }          
        }
        
    }    
}