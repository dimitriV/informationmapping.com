<?php
/**
 * 
 * @author Narendra Raj Ojha <narendraraj.ojha@itoffshorenepal.com>
 * @package SOAP V2 [ITOffshore]
 * 
 */
 
class Itoffshore_Customwebservice_Model_Api_V2 extends Mage_Api_Model_Server_V2_Adapter_Soap 
{

    /**
     * method Name
     *
     * @param arry $license_key
     * @return xml/array
     */
    public function licenseinfo($filters=null) 
    {
        try {
            $serialCollections = Mage::getModel('serialproduct/serial')->getCollection(); /* Old table */
            //$serialCollections = Mage::getModel('licensemanager/licensekeys')->getCollection(); /* New table */
            if(count($filters) && $filters !== null){
                $filters = json_decode($filters);
                foreach($filters as $key => $value){
                    $serialCollections->addFieldToFilter($key, $value);
                }
            }
            
        } catch (Mage_Core_Exception $e) {
            $this->_fault('filters_invalid', $e->getMessage());
        }

        return json_encode($serialCollections->getData());
    }

    /**
     * method Name
     *
     * @param arry $order_no
     * @return xml/array
     */
    public function smainfo($filters=null) 
    {
        try {
            $smaCollections = Mage::getModel('smasubscription/smasubscription')->getCollection(); /* Old table */
            //$smaCollections = Mage::getModel('licensemanager/licenserenewal')->getCollection(); /* New table */
            if(count($filters) && $filters !== null){
                $filters = json_decode($filters);
                foreach($filters as $key => $value){
                    $smaCollections->addFieldToFilter($key, $value);
                }
            }
        } catch (Mage_Core_Exception $e) {
            $this->_fault('filters_invalid', $e->getMessage());
        }
        return json_encode($smaCollections->getData());
    }
}

?>