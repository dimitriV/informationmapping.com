<?php

class Itoffshore_Customwebservice_Model_Objectmodel_Api extends Mage_Api_Model_Resource_Abstract 
{

    /**
     * method Name
     *
     * @param arry $license_key
     * @return xml/array
     */
    public function licenseInfo($filters) 
    {

        try {
            $serialCollection = Mage::getModel('licensemanager/licensekeys')->load($filters['license_key'], 'serial_number');
        } catch (Mage_Core_Exception $e) {
            $this->_fault('filters_invalid', $e->getMessage());
        }
        return $serialCollection->getData();
        //$xml = new SimpleXMLElement('<license/>');
        //array_walk_recursive(array_flip($serialCollection->getData()), array($xml, 'addChild'));
        //return $xml->asXML();
        //return $this->array_to_xml($serialCollection->getData(), $xml)->asXML();
    }

    /**
     * method Name
     *
     * @param arry $license_key
     * @return xml/array
     */
    public function licenseLists($filters =  null) 
    {
        try {
            $serialCollections = Mage::getModel('licensemanager/licensekeys')->getCollection();
            if(count($filters)){
                foreach($filters as $key => $value){
                    $serialCollections->addFieldToFilter($key,$value);
                }
            }
            
        } catch (Mage_Core_Exception $e) {
            $this->_fault('filters_invalid', $e->getMessage());
        }

        return $serialCollections->getData();
        //$xml = new SimpleXMLElement('<license/>');
        //array_walk_recursive(array_flip($serialCollections->getData()), array($xml, 'addChild'));
        //return $xml->asXML();
        //return $this->array_to_xml($serialCollections->getData(), $xml)->asXML();
    }

    /**
     * method Name
     *
     * @param arry $order_no
     * @return xml/array
     */
    public function smaInfo($filters) 
    {

        try {
            $smaCollection = Mage::getModel('licensemanager/licenserenewal')->load($filters['order_no'], 'order_no');
        } catch (Mage_Core_Exception $e) {
            $this->_fault('filters_invalid', $e->getMessage());
        }
        return $smaCollection->getData();
        //$xml = new SimpleXMLElement('<sma/>');
        //array_walk_recursive(array_flip($smaCollection->getData()), array($xml, 'addChild'));
        //return $xml->asXML();
        //return $this->array_to_xml($smaCollection->getData(), $xml)->asXML();
    }

    /**
     * method Name
     *
     * @param arry $order_no
     * @return xml/array
     */
    public function smaLists($filters = null) 
    {
        try {
            $smaCollections = Mage::getModel('licensemanager/licenserenewal')->getCollection();
            if(count($filters)){
                foreach($filters as $key => $value){
                    $smaCollections->addFieldToFilter($key,$value);
                }
            }
        } catch (Mage_Core_Exception $e) {
            $this->_fault('filters_invalid', $e->getMessage());
        }
        return $smaCollections->getData();
        //$xml = new SimpleXMLElement('<sma_list/>');
        //array_walk_recursive(array_flip($smaCollections->getData()), array($xml, 'addChild'));
        //return $xml->asXML();
        //return $this->array_to_xml($smaCollections->getData(), $xml)->asXML();
    }
    
    public function array_to_xml(array $arr, SimpleXMLElement $xml)
    {
        foreach ($arr as $k => $v) {
            is_array($v)
                ? $this->array_to_xml($v, $xml->addChild($k))
                : $xml->addChild($k, $v);
        }
        return $xml;
    }

}

?>