<?php

class Itoffshore_Sales_Model_Order extends Mage_Sales_Model_Order
{
    /**
     * Checking either reverse charge vat information is applicable or not for a order
     * @param $_order : is order object
     * @return boolen
     */
    public function reverseChargeVAT($_order)
    {    

        /* check for applicable of reverse charge */
                    
        $customer_countries = array();
                    
        $reverseCharge = false;
        /* 
        * haystack array for test condition 
        * only country listed on this array are applicable country
        */
        
        $applicable_countries = array(
            'Austria', 'Ireland', 'Slovakia', 'Portugal',
            'Hungary', 'Romania', 'Sweden', 'Greece', 'Poland',
            'Denmark', 'Germany', 'Malta', 'Spain', 'Finland',
            'Luxembourg', 'United Kingdom', 'Estonia', 'Lithuania',
            'Netherlands');
                    
        /* get customer id or buyer id*/
        $customer_id = $_order->getCustomerId();
                    
        /*get csutomer taxvat*/
        $customer_taxvat = $_order->getCustomerTaxvat();
                    
        /* get customer object */
        $customer_data = Mage::getModel('customer/customer')->load($customer_id);
                    
        /* list down customer address */
        foreach($customer_data->getAddresses() as $address) {
            $countryName = Mage::getModel('directory/country')->load($address->getCountryId())->getName();
            $customer_countries[] = $countryName;
        }
        /* check if customer country is in applicable country list or not */
        $mached = array();
        $mached = array_intersect($applicable_countries, $customer_countries);
        
        if((count($mached)) && ($customer_taxvat)) {
            return true;
        }
        
        return false;
    }

    /**
     * Sending email with order data
     *
     * @return Mage_Sales_Model_Order
     */
    public function sendNewOrderEmail()
    {
        if (!Mage::helper('sales')->canSendNewOrderEmail($this->getStore()->getId())) {
            return $this;
        }

        $translate = Mage::getSingleton('core/translate');
        /* @var $translate Mage_Core_Model_Translate */
        $translate->setTranslateInline(false);

        $paymentBlock = Mage::helper('payment')->getInfoBlock($this->getPayment())
            ->setIsSecureMode(true);

        $paymentBlock->getMethod()->setStore($this->getStore()->getId());

        $mailTemplate = Mage::getModel('core/email_template');
        /* @var $mailTemplate Mage_Core_Model_Email_Template */
        $copyTo = $this->_getEmails(self::XML_PATH_EMAIL_COPY_TO);
        $copyMethod = Mage::getStoreConfig(self::XML_PATH_EMAIL_COPY_METHOD, $this->getStoreId());
        if ($copyTo && $copyMethod == 'bcc') {
            foreach ($copyTo as $email) {
                $mailTemplate->addBcc($email);
            }
        }
        
        /**********************************************************/
        //tweak the email template
        //get email template id based on product id
        /**********************************************************/
        $items      = $this->getAllItems();
        $counter    = 0;
        $product_id = 0;
        $template   = 0;
        foreach($items as $item){
            #if($counter == 0){
                $product_id = $item->getProductId();
                $_template   = $this->_getEmailTemplateByProductId($product_id);
                if($_template > 0){
                    $template = $_template;
                }
                
            #}
            $counter++;
        }
       
        /**********************************************************/
        //END
        /**********************************************************/
        
        if ($this->getCustomerIsGuest()) {
            //default magento configuration
            if(empty($template)){
                $template = Mage::getStoreConfig(self::XML_PATH_EMAIL_GUEST_TEMPLATE, $this->getStoreId());    
            }            
            $customerName = $this->getBillingAddress()->getName();
        } else {
            //default magento configuration
            if(empty($template)){
                $template = Mage::getStoreConfig(self::XML_PATH_EMAIL_TEMPLATE, $this->getStoreId());    
            } 
            $customerName = $this->getCustomerName();
        }

        $sendTo = array(
            array(
                'email' => $this->getCustomerEmail(),
                'name'  => $customerName
            )
        );
        if ($copyTo && $copyMethod == 'copy') {
            foreach ($copyTo as $email) {
                $sendTo[] = array(
                    'email' => $email,
                    'name'  => null
                );
            }
        }
        
        
        /**********************************************************/
        //finally add email-receipants of printing house associated with the related partner
        /**********************************************************/

        $productIds = array();
        foreach ($this->getAllItems() as $item) {
            /* @var $item Mage_Sales_Model_Order_Item */
            $productIds[] = $item->getProductId();
        }
        Mage::log('$product ids:'.print_r($productIds, true), null, 'order-printing.log');

        $is_product_under_training_material = false;

        //Categorie Training Materials Partner Store EUR, Partner Store USD
        $children_categories1 = Mage::getModel('catalog/category')->load(12)->getAllChildren(true);
        Mage::log('$children_categories1:'.print_r($children_categories1, true), null, 'order-printing.log');
        $children_categories2 = Mage::getModel('catalog/category')->load(17)->getAllChildren(true);
        Mage::log('$children_categories2:'.print_r($children_categories2, true), null, 'order-printing.log');

        $children_categories  = array_merge($children_categories1, $children_categories2);

        foreach($productIds as $_pid){
            $productObj = Mage::getModel('catalog/product');

            // Load the product in the correct store context, i.e. for product translation, price, etc...
            $productObj->setStoreId($this->getStoreId());

            $productObj->load($_pid);

            if($productObj->getId()){
                $categoryIds = $productObj->getCategoryIds();
                Mage::log('category-ids:'.print_r($categoryIds, true), null, 'order-printing.log');

                $result_array = array_intersect($categoryIds, $children_categories);

                if(count($result_array) > 0){
                    $is_product_under_training_material = true;
                    break;
                }
            }else{
                // Product does not exist anymore... what to do?
            }
        }
        
        
        if($is_product_under_training_material){

            $storeId = $this->getStoreId();
            $store = Mage::app()->getStore($storeId);
            $websiteId = $store->getWebsiteId();

            $customerId = $this->getCustomerId();
            $customer = Mage::getModel('customer/customer');

            // The customer should be loaded from the correct website context
            $customer->setWebsiteId($websiteId);

            $customer->load($customerId);
            $partnerId = $customer->getPartner();

            if($partnerId){
                $extra_email_receipants = $this->_getPrintingHouseEmailByPartnerId($partnerId);
                Mage::log('Extra Email Reciever:'.print_r($extra_email_receipants, true), null, 'order-printing.log');

                if(count($extra_email_receipants)){
                    $sendTo[] = array(
                            'email' => $extra_email_receipants['email'],
                            'name'  => $extra_email_receipants['name']
                    );
                }
            }
        }
        /**********************************************************/
        //END
        /**********************************************************/

        Mage::log('SendTo Emails:'.print_r($sendTo, true), null, 'order-printing.log');

        
        foreach ($sendTo as $recipient) {
            $mailTemplate->setDesignConfig(array('area'=>'frontend', 'store'=>$this->getStoreId()))
                ->addBcc('smourabit@informationmapping.com')
                ->sendTransactional(
                    $template,
                    Mage::getStoreConfig(self::XML_PATH_EMAIL_IDENTITY, $this->getStoreId()),
                    $recipient['email'],
                    $recipient['name'],
                    array(
                        'order'         => $this,
                        'billing'       => $this->getBillingAddress(),
                        'payment_html'  => $paymentBlock->toHtml(),
                    )
                );
        }

        $translate->setTranslateInline(true);
        

        $this->_getResource()->saveAttribute($this, 'email_sent');
        $this->setEmailSent(true);
        $this->save();

        return $this;
    }    
    
    protected function _getEmailTemplateByProductId($product_id){
        $connection = Mage::getSingleton('core/resource')->getConnection('core_read');
        $sql        = "SELECT email_template_id FROM orderemail WHERE product_id = ? LIMIT 1";
        return $connection->fetchOne($sql, $product_id);
    }
    
    protected function _getPrintingHouseEmailByPartnerId($partnerId){
        $connection = Mage::getSingleton('core/resource')->getConnection('core_read');
        $sql        = "SELECT 
                            p2.name
                        	,p2.email
                        FROM
                        	associater a INNER JOIN partner p1 ON a.partner_id = p1.partner_id
                        	INNER JOIN printers p2 ON a.printers_id = p2.printers_id
                        WHERE 
                        	p1.partner_id = ?";
        return $connection->fetchRow($sql, $partnerId);
    }
}
