<?php
class Itoffshore_Autoorder_IndexController extends Mage_Core_Controller_Front_Action
{
    public function indexAction()
    {
        $dont_login_customers = array('allisong@infomap.com', 'rdexters@infomap.com', 'jamia@infomap.com', 'hhaggerty@infomap.com'/*, 'prajwol.bajracharya@itoffshorenepal.com'*/);
        
        $access_code    = $this->getRequest()->getParam('access_code'); 
        
        //Mage::getSingleton('customer/session')->setSmaSkuCode();
         
        $licensekey_renewal    = $this->_getSmaDetailsByAccessCode($access_code);
        $total_renewal = count($licensekey_renewal);        
        //Create an array of all license renewal ids
        $renewals = '';
        foreach($licensekey_renewal as $renewal) {
            $renewals .= $renewal['renewal_id'].',';
        }
        $renewals = rtrim($renewals, ",");
        
        if(empty($licensekey_renewal)){
            die('Either the Access Code is invalid or SMA is already renewed.');
        } 
        
        $licensekey_renewal = $licensekey_renewal[0];
        
        $customer_id    = $licensekey_renewal['customer_id'];
        $customer_email = Mage::getModel('customer/customer')->load($customer_id)->getEmail();
        
        // Get default store alias
        $default_store_alias = Mage::getModel('storecodealias/storecodealias')->load(0, 'store_id')->getJoomlaMenuAlias();
        //$us_store_view_url = Mage::getModel('core/store')->load('us_store_view', 'code')->getBaseUrl(); 
        
        //echo 'Special offer URL: '.Mage::getUrl('special-offer').'<br />';
        // Check if the renewal link has expired.
        $is_auto_renewal_valid = $this->_checkIfAutoRenewalIsValid($licensekey_renewal['sma_2w_after_enddate']);
        
        if(!$is_auto_renewal_valid){
			header('Location: '.Mage::getUrl('special-offer'));
            exit();       
        }
        $order_no      = $licensekey_renewal['order_no'];        
        $order         = Mage::getModel('sales/order')->loadByIncrementId($order_no);
        
        if(!$order->getId()){
            die('Invalid Access Code.');        
        }
		
		$store_id = $order->getStoreId();
		if($store_id){
			Mage::app()->setCurrentStore($store_id);
			$store_alias = Mage::getModel('storecodealias/storecodealias')->load($store_id, 'store_id')->getJoomlaMenuAlias();			
		}
		else{
			$store_alias = '';
		}
        
        //get notification details 
        $product_sku = $licensekey_renewal['product_sku'];
        $product_id  = Mage::getModel('catalog/product')->getIdBySku($product_sku); 
        
        if($total_renewal != 1) {
            $product_qty = $total_renewal;
        }
        else {
            $product_qty = Mage::getModel('licensemanager/license_renewal')->getLicenseRenewalQty($renewals);
        }  
                
        if(empty($product_id)){
            die('Product could not be found.');
        }                         
        
        //if customer is already logged in then logout
        if(Mage::getSingleton('customer/session')->isLoggedIn()){
            Mage::getSingleton('customer/session')->logout();                
        }        
        
        if(!$customer_id) {         
        	die('Such Customer does not exist in our store.');
        }
              
        try {
            //Auto login only in case of certain customers
            if(!in_array($customer_email, $dont_login_customers)){
                Mage::getSingleton('customer/session')->loginById($customer_id);
            }
        	
        }  catch (Exception $ex) {
        	echo '<hr /> Customer Error:<br />' . Zend_Debug::dump($ex->getMessage());
            exit();
        }
         
        $cart = Mage::getSingleton('checkout/cart');        
        $cart->truncate();
        $cart->save();
        $cart->getItems()->clear()->save();
        
        //set access code later updates
        Mage::getSingleton('checkout/session')->setSmaAccessCode($access_code);
        Mage::getSingleton('checkout/session')->setRenewalIds($renewals);        
        Mage::getSingleton('customer/session')->setSmaAccessCode($access_code);
        Mage::getSingleton('customer/session')->setSmaSkuCode($product_sku);
              
        $redirect_url = Mage::getUrl('checkout/cart/add', array('product'  => $product_id, 'qty' => $product_qty));
		
        header('Location: '. $redirect_url); 
        exit();
    }        
      
    private function _getSmaDetailsByAccessCode($access_code)
    {
        $connection = Mage::getSingleton('core/resource')->getConnection('core_read');
        $sql        = "SELECT *
                        FROM license_keys_renewal
                        WHERE access_code = '$access_code'
                        AND is_renewed = '0'
                        AND '1' IN (sma_2m_b4_email_notified, sma_1m_b4_email_notified, sma_enddate_email_notified)";        
        return $connection->fetchAll($sql);
    }
    
    private function _checkIfAutoRenewalIsValid($sma_2w_after_enddate)
    {
        $connection = Mage::getSingleton('core/resource')->getConnection('core_read');
        $sql        = "SELECT IF(NOW() > '" . $sma_2w_after_enddate . "', 0, 1) AS is_current_date_greater";        
        return $connection->fetchOne($sql);        
    }
    
}