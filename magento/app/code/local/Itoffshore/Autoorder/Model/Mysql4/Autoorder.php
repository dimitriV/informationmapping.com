<?php

class Itoffshore_Autoorder_Model_Mysql4_Autoorder extends Mage_Core_Model_Mysql4_Abstract
{
    public function _construct()
    {    
        // Note that the autoorder_id refers to the key field in your database table.
        $this->_init('autoorder/autoorder', 'autoorder_id');
    }
}