<?php

class Itoffshore_Autoorder_Model_Mysql4_Autoorder_Collection extends Mage_Core_Model_Mysql4_Collection_Abstract
{
    public function _construct()
    {
        parent::_construct();
        $this->_init('autoorder/autoorder');
    }
}