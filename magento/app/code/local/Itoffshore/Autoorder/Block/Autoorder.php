<?php
class Itoffshore_Autoorder_Block_Autoorder extends Mage_Core_Block_Template
{
	public function _prepareLayout()
    {
		return parent::_prepareLayout();
    }
    
     public function getAutoorder()     
     { 
        if (!$this->hasData('autoorder')) {
            $this->setData('autoorder', Mage::registry('autoorder'));
        }
        return $this->getData('autoorder');
        
    }
}