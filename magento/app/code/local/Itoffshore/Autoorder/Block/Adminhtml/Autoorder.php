<?php
class Itoffshore_Autoorder_Block_Adminhtml_Autoorder extends Mage_Adminhtml_Block_Widget_Grid_Container
{
  public function __construct()
  {
    $this->_controller = 'adminhtml_autoorder';
    $this->_blockGroup = 'autoorder';
    $this->_headerText = Mage::helper('autoorder')->__('Item Manager');
    $this->_addButtonLabel = Mage::helper('autoorder')->__('Add Item');
    parent::__construct();
  }
}