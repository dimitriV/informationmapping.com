<?php

class Itoffshore_Autoorder_Block_Adminhtml_Autoorder_Edit extends Mage_Adminhtml_Block_Widget_Form_Container
{
    public function __construct()
    {
        parent::__construct();
                 
        $this->_objectId = 'id';
        $this->_blockGroup = 'autoorder';
        $this->_controller = 'adminhtml_autoorder';
        
        $this->_updateButton('save', 'label', Mage::helper('autoorder')->__('Save Item'));
        $this->_updateButton('delete', 'label', Mage::helper('autoorder')->__('Delete Item'));
		
        $this->_addButton('saveandcontinue', array(
            'label'     => Mage::helper('adminhtml')->__('Save And Continue Edit'),
            'onclick'   => 'saveAndContinueEdit()',
            'class'     => 'save',
        ), -100);

        $this->_formScripts[] = "
            function toggleEditor() {
                if (tinyMCE.getInstanceById('autoorder_content') == null) {
                    tinyMCE.execCommand('mceAddControl', false, 'autoorder_content');
                } else {
                    tinyMCE.execCommand('mceRemoveControl', false, 'autoorder_content');
                }
            }

            function saveAndContinueEdit(){
                editForm.submit($('edit_form').action+'back/edit/');
            }
        ";
    }

    public function getHeaderText()
    {
        if( Mage::registry('autoorder_data') && Mage::registry('autoorder_data')->getId() ) {
            return Mage::helper('autoorder')->__("Edit Item '%s'", $this->htmlEscape(Mage::registry('autoorder_data')->getTitle()));
        } else {
            return Mage::helper('autoorder')->__('Add Item');
        }
    }
}