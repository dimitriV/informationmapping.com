<?php

class Itoffshore_Autoorder_Block_Adminhtml_Autoorder_Edit_Tab_Form extends Mage_Adminhtml_Block_Widget_Form
{
  protected function _prepareForm()
  {
      $form = new Varien_Data_Form();
      $this->setForm($form);
      $fieldset = $form->addFieldset('autoorder_form', array('legend'=>Mage::helper('autoorder')->__('Item information')));
     
      $fieldset->addField('title', 'text', array(
          'label'     => Mage::helper('autoorder')->__('Title'),
          'class'     => 'required-entry',
          'required'  => true,
          'name'      => 'title',
      ));

      $fieldset->addField('filename', 'file', array(
          'label'     => Mage::helper('autoorder')->__('File'),
          'required'  => false,
          'name'      => 'filename',
	  ));
		
      $fieldset->addField('status', 'select', array(
          'label'     => Mage::helper('autoorder')->__('Status'),
          'name'      => 'status',
          'values'    => array(
              array(
                  'value'     => 1,
                  'label'     => Mage::helper('autoorder')->__('Enabled'),
              ),

              array(
                  'value'     => 2,
                  'label'     => Mage::helper('autoorder')->__('Disabled'),
              ),
          ),
      ));
     
      $fieldset->addField('content', 'editor', array(
          'name'      => 'content',
          'label'     => Mage::helper('autoorder')->__('Content'),
          'title'     => Mage::helper('autoorder')->__('Content'),
          'style'     => 'width:700px; height:500px;',
          'wysiwyg'   => false,
          'required'  => true,
      ));
     
      if ( Mage::getSingleton('adminhtml/session')->getAutoorderData() )
      {
          $form->setValues(Mage::getSingleton('adminhtml/session')->getAutoorderData());
          Mage::getSingleton('adminhtml/session')->setAutoorderData(null);
      } elseif ( Mage::registry('autoorder_data') ) {
          $form->setValues(Mage::registry('autoorder_data')->getData());
      }
      return parent::_prepareForm();
  }
}