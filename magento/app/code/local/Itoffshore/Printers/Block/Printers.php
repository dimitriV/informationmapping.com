<?php
class Itoffshore_Printers_Block_Printers extends Mage_Core_Block_Template
{
	public function _prepareLayout()
    {
		return parent::_prepareLayout();
    }
    
     public function getPrinters()     
     { 
        if (!$this->hasData('printers')) {
            $this->setData('printers', Mage::registry('printers'));
        }
        return $this->getData('printers');
        
    }
}