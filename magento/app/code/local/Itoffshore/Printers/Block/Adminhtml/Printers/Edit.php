<?php

class Itoffshore_Printers_Block_Adminhtml_Printers_Edit extends Mage_Adminhtml_Block_Widget_Form_Container
{
    public function __construct()
    {
        parent::__construct();
                 
        $this->_objectId = 'id';
        $this->_blockGroup = 'printers';
        $this->_controller = 'adminhtml_printers';
        
        $this->_updateButton('save', 'label', Mage::helper('printers')->__('Save Item'));
        $this->_updateButton('delete', 'label', Mage::helper('printers')->__('Delete Item'));
		
        $this->_addButton('saveandcontinue', array(
            'label'     => Mage::helper('adminhtml')->__('Save And Continue Edit'),
            'onclick'   => 'saveAndContinueEdit()',
            'class'     => 'save',
        ), -100);

        $this->_formScripts[] = "
            function toggleEditor() {
                if (tinyMCE.getInstanceById('printers_content') == null) {
                    tinyMCE.execCommand('mceAddControl', false, 'printers_content');
                } else {
                    tinyMCE.execCommand('mceRemoveControl', false, 'printers_content');
                }
            }

            function saveAndContinueEdit(){
                editForm.submit($('edit_form').action+'back/edit/');
            }
        ";
    }

    public function getHeaderText()
    {
        if( Mage::registry('printers_data') && Mage::registry('printers_data')->getId() ) {
            return Mage::helper('printers')->__("Edit Item '%s'", $this->htmlEscape(Mage::registry('printers_data')->getName()));
        } else {
            return Mage::helper('printers')->__('Add Item');
        }
    }
}