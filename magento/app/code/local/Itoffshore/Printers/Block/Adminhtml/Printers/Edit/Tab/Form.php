<?php

class Itoffshore_Printers_Block_Adminhtml_Printers_Edit_Tab_Form extends Mage_Adminhtml_Block_Widget_Form
{
  protected function _prepareForm()
  {
      $form = new Varien_Data_Form();
      $this->setForm($form);
      $fieldset = $form->addFieldset('printers_form', array('legend'=>Mage::helper('printers')->__('Item information')));
     
      $fieldset->addField('name', 'text', array(
          'label'     => Mage::helper('partner')->__('Name'),
          'class'     => 'required-entry',
          'required'  => true,
          'name'      => 'name',
      ));
      $fieldset->addField('email', 'text', array(
          'label'     => Mage::helper('partner')->__('Email'),         
          'class'     => 'required-entry validate-email',
          'required'  => true,
          'name'      => 'email',
      ));
      $fieldset->addField('address', 'editor', array(
          'name'      => 'address',
          'label'     => Mage::helper('partner')->__('Address'),
          'title'     => Mage::helper('partner')->__('Address'),
          'style'     => 'width:276px; height:100px;',
          'wysiwyg'   => false,
          'required'  => false,
      ));
		
      $fieldset->addField('status', 'select', array(
          'label'     => Mage::helper('printers')->__('Status'),
          'name'      => 'status',
          'values'    => array(
              array(
                  'value'     => 1,
                  'label'     => Mage::helper('printers')->__('Enabled'),
              ),

              array(
                  'value'     => 2,
                  'label'     => Mage::helper('printers')->__('Disabled'),
              ),
          ),
      ));
     
          
      if ( Mage::getSingleton('adminhtml/session')->getPrintersData() )
      {
          $form->setValues(Mage::getSingleton('adminhtml/session')->getPrintersData());
          Mage::getSingleton('adminhtml/session')->setPrintersData(null);
      } elseif ( Mage::registry('printers_data') ) {
          $form->setValues(Mage::registry('printers_data')->getData());
      }
      return parent::_prepareForm();
  }
}