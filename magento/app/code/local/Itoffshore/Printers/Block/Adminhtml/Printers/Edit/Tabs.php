<?php

class Itoffshore_Printers_Block_Adminhtml_Printers_Edit_Tabs extends Mage_Adminhtml_Block_Widget_Tabs
{

  public function __construct()
  {
      parent::__construct();
      $this->setId('printers_tabs');
      $this->setDestElementId('edit_form');
      $this->setTitle(Mage::helper('printers')->__('Item Information'));
  }

  protected function _beforeToHtml()
  {
      $this->addTab('form_section', array(
          'label'     => Mage::helper('printers')->__('Item Information'),
          'title'     => Mage::helper('printers')->__('Item Information'),
          'content'   => $this->getLayout()->createBlock('printers/adminhtml_printers_edit_tab_form')->toHtml(),
      ));
     
      return parent::_beforeToHtml();
  }
}