<?php
class Itoffshore_Printers_Block_Adminhtml_Printers extends Mage_Adminhtml_Block_Widget_Grid_Container
{
  public function __construct()
  {
    $this->_controller = 'adminhtml_printers';
    $this->_blockGroup = 'printers';
    $this->_headerText = Mage::helper('printers')->__('Printing House Manager');
    $this->_addButtonLabel = Mage::helper('printers')->__('Add Printing House');
    parent::__construct();
  }
}