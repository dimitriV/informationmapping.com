<?php

class Itoffshore_Printers_Model_Mysql4_Printers extends Mage_Core_Model_Mysql4_Abstract
{
    public function _construct()
    {    
        // Note that the printers_id refers to the key field in your database table.
        $this->_init('printers/printers', 'printers_id');
    }
}