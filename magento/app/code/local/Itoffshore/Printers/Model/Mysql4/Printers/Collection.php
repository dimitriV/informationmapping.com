<?php

class Itoffshore_Printers_Model_Mysql4_Printers_Collection extends Mage_Core_Model_Mysql4_Collection_Abstract
{
    public function _construct()
    {
        parent::_construct();
        $this->_init('printers/printers');
    }
}