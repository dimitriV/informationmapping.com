<?php

class Itoffshore_Printers_Model_Printers extends Mage_Core_Model_Abstract
{
    public function _construct()
    {
        parent::_construct();
        $this->_init('printers/printers');
    }
}