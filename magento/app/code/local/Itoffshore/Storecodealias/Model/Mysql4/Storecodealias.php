<?php

class Itoffshore_Storecodealias_Model_Mysql4_Storecodealias extends Mage_Core_Model_Mysql4_Abstract
{
    public function _construct()
    {    
        // Note that the storecodealias_id refers to the key field in your database table.
        $this->_init('storecodealias/storecodealias', 'storecodealias_id');
    }
}