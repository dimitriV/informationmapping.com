<?php

$installer = $this;

$installer->startSetup();

$installer->run("

-- DROP TABLE IF EXISTS {$this->getTable('storecodealias')};
CREATE TABLE {$this->getTable('storecodealias')} (
  `storecodealias_id` int(11) unsigned NOT NULL auto_increment,
  `joomla_menu_alias` varchar(255) NOT NULL default '',
  `store_id` int(11) NOT NULL default '0',
  `status` smallint(6) NOT NULL default '0',
  `created_time` datetime NULL,
  `update_time` datetime NULL,
  PRIMARY KEY (`storecodealias_id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

    ");

$installer->endSetup(); 