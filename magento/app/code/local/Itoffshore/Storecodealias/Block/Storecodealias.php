<?php
class Itoffshore_Storecodealias_Block_Storecodealias extends Mage_Core_Block_Template
{
	public function _prepareLayout()
    {
		return parent::_prepareLayout();
    }
    
     public function getStorecodealias()     
     { 
        if (!$this->hasData('storecodealias')) {
            $this->setData('storecodealias', Mage::registry('storecodealias'));
        }
        return $this->getData('storecodealias');
        
    }
}