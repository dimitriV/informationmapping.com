<?php

class Itoffshore_Storecodealias_Block_Adminhtml_Storecodealias_Edit_Tab_Form extends Mage_Adminhtml_Block_Widget_Form
{
  protected function _prepareForm()
  {
      $form = new Varien_Data_Form();
      $this->setForm($form);
      $fieldset = $form->addFieldset('storecodealias_form', array('legend'=>Mage::helper('storecodealias')->__('Item information')));
     
      $fieldset->addField('store_id', 'select', array(
			'name'      => 'store_id',
			'label'     => Mage::helper('cms')->__('Store View'),
			'title'     => Mage::helper('cms')->__('Store View'),
			'required'  => true,
			'values'    => Mage::getSingleton('adminhtml/system_store')->getStoreValuesForForm(false, true),
		));
	  
	  $fieldset->addField('joomla_menu_alias', 'text', array(
          'label'     => Mage::helper('storecodealias')->__('Joomla Menu Alias'),
          'class'     => 'required-entry',
          'required'  => true,
          'name'      => 'joomla_menu_alias',
      ));
		
      $fieldset->addField('status', 'select', array(
          'label'     => Mage::helper('storecodealias')->__('Status'),
          'name'      => 'status',
          'values'    => array(
              array(
                  'value'     => 1,
                  'label'     => Mage::helper('storecodealias')->__('Enabled'),
              ),

              array(
                  'value'     => 2,
                  'label'     => Mage::helper('storecodealias')->__('Disabled'),
              ),
          ),
      ));
     
      if ( Mage::getSingleton('adminhtml/session')->getStorecodealiasData() )
      {
          $form->setValues(Mage::getSingleton('adminhtml/session')->getStorecodealiasData());
          Mage::getSingleton('adminhtml/session')->setStorecodealiasData(null);
      } elseif ( Mage::registry('storecodealias_data') ) {
          $form->setValues(Mage::registry('storecodealias_data')->getData());
      }
      return parent::_prepareForm();
  }
}