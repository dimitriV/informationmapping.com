<?php

class Itoffshore_Storecodealias_Block_Adminhtml_Storecodealias_Grid extends Mage_Adminhtml_Block_Widget_Grid
{
  public function __construct()
  {
      parent::__construct();
      $this->setId('storecodealiasGrid');
      $this->setDefaultSort('storecodealias_id');
      $this->setDefaultDir('ASC');
      $this->setSaveParametersInSession(true);
  }

  protected function _prepareCollection()
  {
      $collection = Mage::getModel('storecodealias/storecodealias')->getCollection();
      $this->setCollection($collection);
      return parent::_prepareCollection();
  }

  protected function _prepareColumns()
  {
      $this->addColumn('storecodealias_id', array(
          'header'    => Mage::helper('storecodealias')->__('ID'),
          'align'     =>'right',
          'width'     => '50px',
          'index'     => 'storecodealias_id',
      ));

      $this->addColumn('joomla_menu_alias', array(
          'header'    => Mage::helper('storecodealias')->__('Joomla Menu Alias'),
          'align'     =>'left',
          'index'     => 'joomla_menu_alias',
      ));

      $this->addColumn('status', array(
          'header'    => Mage::helper('storecodealias')->__('Status'),
          'align'     => 'left',
          'width'     => '80px',
          'index'     => 'status',
          'type'      => 'options',
          'options'   => array(
              1 => 'Enabled',
              2 => 'Disabled',
          ),
      ));
	  
        $this->addColumn('action',
            array(
                'header'    =>  Mage::helper('storecodealias')->__('Action'),
                'width'     => '100',
                'type'      => 'action',
                'getter'    => 'getId',
                'actions'   => array(
                    array(
                        'caption'   => Mage::helper('storecodealias')->__('Edit'),
                        'url'       => array('base'=> '*/*/edit'),
                        'field'     => 'id'
                    )
                ),
                'filter'    => false,
                'sortable'  => false,
                'index'     => 'stores',
                'is_system' => true,
        ));
		
		$this->addExportType('*/*/exportCsv', Mage::helper('storecodealias')->__('CSV'));
		$this->addExportType('*/*/exportXml', Mage::helper('storecodealias')->__('XML'));
	  
      return parent::_prepareColumns();
  }

    protected function _prepareMassaction()
    {
        $this->setMassactionIdField('storecodealias_id');
        $this->getMassactionBlock()->setFormFieldName('storecodealias');

        $this->getMassactionBlock()->addItem('delete', array(
             'label'    => Mage::helper('storecodealias')->__('Delete'),
             'url'      => $this->getUrl('*/*/massDelete'),
             'confirm'  => Mage::helper('storecodealias')->__('Are you sure?')
        ));

        $statuses = Mage::getSingleton('storecodealias/status')->getOptionArray();

        array_unshift($statuses, array('label'=>'', 'value'=>''));
        $this->getMassactionBlock()->addItem('status', array(
             'label'=> Mage::helper('storecodealias')->__('Change status'),
             'url'  => $this->getUrl('*/*/massStatus', array('_current'=>true)),
             'additional' => array(
                    'visibility' => array(
                         'name' => 'status',
                         'type' => 'select',
                         'class' => 'required-entry',
                         'label' => Mage::helper('storecodealias')->__('Status'),
                         'values' => $statuses
                     )
             )
        ));
        return $this;
    }

  public function getRowUrl($row)
  {
      return $this->getUrl('*/*/edit', array('id' => $row->getId()));
  }

}