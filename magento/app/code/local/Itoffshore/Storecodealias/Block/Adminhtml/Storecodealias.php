<?php
class Itoffshore_Storecodealias_Block_Adminhtml_Storecodealias extends Mage_Adminhtml_Block_Widget_Grid_Container
{
  public function __construct()
  {
    $this->_controller = 'adminhtml_storecodealias';
    $this->_blockGroup = 'storecodealias';
    $this->_headerText = Mage::helper('storecodealias')->__('Item Manager');
    $this->_addButtonLabel = Mage::helper('storecodealias')->__('Add Item');
    parent::__construct();
  }
}