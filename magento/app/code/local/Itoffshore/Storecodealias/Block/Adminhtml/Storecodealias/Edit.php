<?php

class Itoffshore_Storecodealias_Block_Adminhtml_Storecodealias_Edit extends Mage_Adminhtml_Block_Widget_Form_Container
{
    public function __construct()
    {
        parent::__construct();
                 
        $this->_objectId = 'id';
        $this->_blockGroup = 'storecodealias';
        $this->_controller = 'adminhtml_storecodealias';
        
        $this->_updateButton('save', 'label', Mage::helper('storecodealias')->__('Save Item'));
        $this->_updateButton('delete', 'label', Mage::helper('storecodealias')->__('Delete Item'));
		
        $this->_addButton('saveandcontinue', array(
            'label'     => Mage::helper('adminhtml')->__('Save And Continue Edit'),
            'onclick'   => 'saveAndContinueEdit()',
            'class'     => 'save',
        ), -100);

        $this->_formScripts[] = "
            function toggleEditor() {
                if (tinyMCE.getInstanceById('storecodealias_content') == null) {
                    tinyMCE.execCommand('mceAddControl', false, 'storecodealias_content');
                } else {
                    tinyMCE.execCommand('mceRemoveControl', false, 'storecodealias_content');
                }
            }

            function saveAndContinueEdit(){
                editForm.submit($('edit_form').action+'back/edit/');
            }
        ";
    }

    public function getHeaderText()
    {
        if( Mage::registry('storecodealias_data') && Mage::registry('storecodealias_data')->getId() ) {
            return Mage::helper('storecodealias')->__("Edit Item '%s'", $this->htmlEscape(Mage::registry('storecodealias_data')->getTitle()));
        } else {
            return Mage::helper('storecodealias')->__('Add Item');
        }
    }
}