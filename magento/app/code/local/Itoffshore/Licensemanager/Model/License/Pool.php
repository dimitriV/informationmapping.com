<?php

class Itoffshore_Licensemanager_Model_License_Pool extends Mage_Core_Model_Abstract
{
	const STATUS_ASSIGNED = 'ASSIGNED';
	const STATUS_UNASSIGNED = 'UNASSIGNED';
	const STATUS_CANCELLED = "CANCELLED";

	protected function _construct()
	{
		parent::_construct();
		$this->_init('licensemanager/license_pool');
	}	
    


    
    public function lowSerialCountNotify($product_id, Mage_Sales_Model_Order $order, $currentCount)
    {
		if($email = Mage::getStoreConfig('licensemanager/options/license_notify_email')){
			$productName = Mage::getSingleton('catalog/product')->load($product_id)->getName();
			$message = "You are running low on serial numbers for $productName, the product ID is : $product_id .\nYou currently have $currentCount serials remaining.";
			mail($email, Mage::app()->getStore()->getName() . ' - Low License Key Notification', $message);
		}
  	}    

    public function noSerialCountNotify($product_id, Mage_Sales_Model_Order $order)
    {
		if($email = Mage::getStoreConfig('licensemanager/options/license_notify_email')){
			$productName = Mage::getSingleton('catalog/product')->load($product_id)->getName();
			$message = "You have run out of serial numbers for $productName, the product ID is : $product_id \nYou currently have NO serials remaining.  Please log into the Magento admin and add more serial numbers.";
			mail($email, Mage::app()->getStore()->getName() . ' - No License Key Notification', $message);
		}
  	}

  	public function cancel()
  	{
  		//Change status of unlinked product
  		$this->setStatus(self::STATUS_CANCELLED)->save();
  		return true;
  	}

    public function loadByOrderItemId($orderItemId)
    {
    	$this->setData($this->getResource()->loadByOrderItemId($orderItemId));
    	return $this;
    }
}