<?php

class Itoffshore_Licensemanager_Model_Mysql4_License_Pool extends Mage_Core_Model_Mysql4_Abstract {
    public function _construct() {
        $this->_init('licensemanager/license_pool', 'id');
    }


    public function getAvailableProductSkus() {
        $select = $this->_getReadAdapter()->select()->from($this->getMainTable(), array(
            'product_id',
            'count' => 'COUNT(*)'
        ))->where('status = (?)', Itoffshore_Licensemanager_Model_License_Pool::STATUS_UNASSIGNED)->group('product_id');
        $result = $this->_getReadAdapter()->fetchPairs($select);
        return $result;
    }


    public function loadByOrderItemId($orderItemId) {
        $select = $this->_getReadAdapter()->select()->from($this->getTable('license'))->where('order_item_id=:order_item_id');
        $result = $this->_getReadAdapter()->fetchRow($select, array('order_item_id' => $orderItemId));
        return $result;
    }
}