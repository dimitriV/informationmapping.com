<?php

class Itoffshore_Licensemanager_Model_Mysql4_License_Pool_Collection extends Mage_Core_Model_Mysql4_Collection_Abstract
{
	public function _construct()
	{
		parent::_construct();
		$this->_init('licensemanager/license_pool');
	}

    protected function _initSelect() {
        $this->addFilterToMap('id', 'main_table.id');
        $this->addFilterToMap('renewal_id', 'subscriptions_renewal.id');
        $this->addFilterToMap('subscription_id', 'subscriptions_subscription.id');
        $this->addFilterToMap('serial', 'main_table.serial');

        return parent::_initSelect();
    }

    public function joinSubscriptions(){
        $this->getSelect()->joinLeft('subscriptions_subscription','subscriptions_subscription.pool_id = main_table.id', array('subscription_id' => 'subscriptions_subscription.id'));
        $this->getSelect()->joinLeft('subscriptions_renewal','subscriptions_renewal.pool_id = main_table.id', array('renewal_id' => 'subscriptions_renewal.id'));

	        //$sql = ''.$this->getSelect();

        return $this;
    }


	
	public function loadByOrderItemId($orderItemId, $status = Itoffshore_Licensemanager_Model_License_Pool::STATUS_ASSIGNED)
	{
		$this->addFieldToFilter('order_item_id', $orderItemId);
		
		if($status !== false)
			$this->addFieldToFilter('status', $status);
			
		return $this;
	}
	
	public function getSerialNumberArray()
	{
		$serialNumbers = array();
		foreach($this as $pool){
			$serialNumbers[] = $pool->getSerial();
		}
		return $serialNumbers;
	}

	
}