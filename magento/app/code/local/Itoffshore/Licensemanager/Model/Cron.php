<?php

class Itoffshore_Licensemanager_Model_Cron {

    public function processLicenseLog($x = null) {
        /* @var $db Magento_Db_Adapter_Pdo_Mysql */
        $helper = Mage::helper('licensemanager');

        $db = Mage::getSingleton('core/resource')->getConnection('core_write');

        // Clean up license log first
        // 1. Remove all new trial enteries
        $db->query('DELETE FROM `license_log` WHERE `LastActivationDate`="1899-12-30 00:00:00" and `ProcessedOn` is null and `Keys`=""');
        // 2. Remove all the old log data
        $db->query('DELETE d FROM `license_log` d INNER JOIN `license_log` d2 ON  d.`Keys` = d2.`Keys` AND d.LogDate < d2.LogDate');

        // Process oldest rows first, so new data is applied last!
        $rows = $db->fetchAll('select * from license_log where ProcessedOn is null order by LogDate');



        foreach ($rows as $row) {
            // Find the subscription or renewal by using the serial.
            $subscription = null;
            $renewal = null;

            $keys = explode(';', $row['Keys']);
            foreach ($keys as $key) {
                $subOrRenewal = $this->_findBySerial($key);
                //Loging
               $message = "Processing key: " . $key;
               Mage::log($helper->__($message), null, 'licenselog.log');

                if ($subOrRenewal instanceof Storefront_Subscriptions_Model_Subscription) {
                    $subscription = $subOrRenewal;
                    $renewal = null;
                    break;
                } else if ($subOrRenewal instanceof Storefront_Subscriptions_Model_Renewal) {
                    $subscription = null;
                    $renewal = $subOrRenewal;
                    break;
                } else {
                    // Continue to next row
                    continue 2;
                }
            }

            $expiresOn = $row['ExpiresOn'];

            $foundKey = 0;

            if ($renewal) {
                // This license log record represents an activation for a renewal
                $subscription = $renewal->getSubscription();

                if ($subscription) {
                    $foundKey = 1;

                    if ($renewal->getStatus() !== Storefront_Subscriptions_Model_Renewal::STATUS_ACTIVE) {
                        $renewal->startRenew($expiresOn);

                    } else {
                        // Renewal is already active, but the expiry date may be different. Let's accept it.
                        $subscription->setExpiresOn($expiresOn);
                        $subscription->save();
                    }
                }

            } elseif ($subscription) {
                // This license log record represents a new license activation

                $subscription->setActivationEmail($row['RegisteredEmail']);

                if ($subscription->getStatus() !== 'ACTIVE' && $subscription->getExpiryDate() == null && $subscription->getStartDate() == null) {
                    $subscription->startSubscription();
                }

                // Override the expiry date to the date from the license_log table
                $subscription->setExpiresOn($expiresOn);

                $subscription->save();

                $foundKey = 1;
            }

            // Mark row as processed so it is not processed again.
            $db->query('update license_log set ProcessedOn = now(), KeyFound = ? where Id = ? limit 1', array(
                $foundKey,
                $row['Id']
            ));

            $message = 'Update License Log: ' . $foundKey;
            Mage::log($helper->__($message), null, 'licenselog.log');

        }


    }

    /**
     * Look for a subscription or a renewal by using the serial.
     * We're not sure what the result will be: a subcription or a renewal.
     * @param $serial
     */
    protected function _findBySerial($serial) {
        // Try renewal
        $renewCol = Mage::getResourceModel('subscriptions/renewal_collection');
        $renewCol->addFieldToFilter('serial', $serial);

        if ($renewCol->count() > 0) {
            return $renewCol->getFirstItem();
        }

        // Try subscription
        $subsCol = Mage::getResourceModel('subscriptions/subscription_collection');
        $subsCol->addFieldToFilter('serial', $serial);

        if ($subsCol->count() > 0) {
            return $subsCol->getFirstItem();
        }

        // Serial not found
        return null;
    }



}