<?php

class Itoffshore_Licensemanager_Model_Sales_Order_Item extends Mage_Sales_Model_Order_Item {

    public function getDescription() {
        $orderItem = $this;

        $description = $this->getData('description');
        $itemId = $orderItem->getId();

        if ($orderItem->getParentItemId()) {
            $itemId = $orderItem->getParentItemId();
        }

        /* @var $subscriptionCollection Storefront_Subscriptions_Model_Resource_Subscription_Collection */
        $subscriptionCollection = Mage::getResourceModel('subscriptions/subscription_collection');
        $subscriptionCollection->addFieldToFilter('order_item_id', $itemId);
//        $subscriptionCollection->addFieldToFilter('status', array(
//            'in',
//            array(
//                Itoffshore_Licensemanager_Model_License_Pool::STATUS_ASSIGNED,
//                Itoffshore_Licensemanager_Model_License_Pool::STATUS_CANCELLED
//            )
//        ));

        if ($subscriptionCollection->count() === 0) {
            // If there are no subscriptions on this order item, they may be on the child items!

            $childItems = Mage::getResourceModel('sales/order_item_collection');
            $childItems->addFieldToFilter('parent_item_id', $itemId)->load();

            $childIds = array();
            foreach ($childItems as $child) {
                $childIds[] = $child->getId();
            }
            

            $subscriptionCollection = Mage::getResourceModel('subscriptions/subscription_collection');
            $subscriptionCollection->addFieldToFilter('order_item_id', array('in' => $childIds));
            //$subscriptionCollection->addFieldToFilter('expires_on', array('in' => $childIds));
//            $subscriptionCollection->addFieldToFilter('status', array(
//                'in',
//                array(
//                    Itoffshore_Licensemanager_Model_License_Pool::STATUS_ASSIGNED,
//                    Itoffshore_Licensemanager_Model_License_Pool::STATUS_CANCELLED
//                )
//            ))->load(false);
        }

        if($subscriptionCollection->count() === 0 ){

            // Check for renewals
            /* @var $subscriptionCollection Storefront_Subscriptions_Model_Resource_Renewal_Collection */
            $renewalCollection = Mage::getResourceModel('subscriptions/renewal_collection');
            $renewalCollection->addFieldToFilter('order_item_id', $itemId);

            if ($renewalCollection->count() === 0) {

                $childItems = Mage::getResourceModel('sales/order_item_collection');
                $childItems->addFieldToFilter('parent_item_id', $itemId)->load();

                $childIds = array();
                foreach ($childItems as $child) {
                    $childIds[] = $child->getId();
                }
                if (!count($childIds)) {
                    $childIds[] = $orderItem->getId();
                }

                $renewalCollection = Mage::getResourceModel('subscriptions/renewal_collection');
                $renewalCollection->addFieldToFilter('order_item_id', array('in' => $childIds));

            }

        }


        $cancelledOrderItem = false;
        foreach ($subscriptionCollection as $subscription) {
            if ($subscription->isExpired()) {
                $cancelledOrderItem = true;
                break;
            }
        }

        $serialNumbers = array();
        if($subscriptionCollection->count() > 0){
            $serialNumbers = $subscriptionCollection->getSerialNumberArray();
        }elseif($renewalCollection->count() > 0){
            $serialNumbers = $renewalCollection->getSerialNumberArray();
        }


        if ($cancelledOrderItem) {
            $description .= "Serial number(s) removed because order was cancelled";

        } else if (count($serialNumbers) == 1) {
            $description = Mage::helper('licensemanager')->__(' Your FS Pro Key: ') . implode(', ', $serialNumbers) . $description;

        } else if (count($serialNumbers) > 1) {
            $description = Mage::helper('licensemanager')->__(' Your FS Pro Keys: ') . implode(', ', $serialNumbers) . $description;

        } else if ($this->getOrder()->getState() == 'new' || $this->getOrder()->getState() == 'pending' || $this->getOrder()->getState() == 'processing') {
            //$description .= "\n<span class='serial-number'>" . Mage::helper('serialproduct')->__('License Key(s) Pending') . '</span>';

        } else {
            // Mage::log('CLS_Serialproduct_Model_Sales_Order_Item::getDescription() - Could not load product serial number.  Sales Order Item ID: ' . print_r($orderItem->getId(), true));
            // $description .= "\n<span class='serial-number-error'> ".$orderItem->getId()."</span>"; 
        }



        return $description;
    }
}