<?php

class  Itoffshore_Licensemanager_Model_Sales_Order_Invoice_Item extends Mage_Sales_Model_Order_Invoice_Item 
{
	public function getDescription() {
		return Mage::getSingleton('sales/order_item')->load($this->getOrderItemId())->getDescription();
	}
}