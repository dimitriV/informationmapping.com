<?php
class Itoffshore_Licensemanager_Model_Cron
{

    //TODO delete this file??


    const XML_PATH_EMAIL_IDENTITY = 'sales_email/order/identity';
    
    private $_debugEmailDomains = array(
    		'@storefront.be',
    		'@informationmapping.com'
    );

//    private function _sendNotificationEmails($monthsBefore)
//    {
//    	//$isTesting = true;
//    	$isTesting = false;
//
//    	$notifications  = $this->getDataForSmaNotification($monthsBefore);
//
//        if(count($notifications) == 0){
//            return false;
//        }
//
//        /*********************************************************
//        * SENDING NOTIFICATION EMAILS - START
//        **********************************************************/
//        foreach($notifications as $recipient){
//        	$customerId = $recipient['customer_id'];
//        	$customer     = Mage::getModel('customer/customer')->load($customerId);
//        	$communicationEmail     = (trim($recipient['email'])) ? trim($recipient['email']) : $customer->getEmail();
//
//        	if(Mage::getIsDeveloperMode()){
//        		// Debug mode
//
//        		$canEmail = false;
//        		foreach($this->_debugEmailDomains as $emailDomain){
//        			if(stripos($communicationEmail, $emailDomain) !== false){
//        				// The customer email address is not blocked during debugging...
//        				$canEmail = true;
//        			}
//        		}
//
//        		if(!$canEmail){
//        			continue;
//        		}
//
//        	}
//
//        	/* @var $savedCardHelper Storefront_SavedCards_Helper_Data */
//        	$savedCardHelper = Mage::helper('savedcards');
//        	$cards = $savedCardHelper->getCards($customerId);
//        	$primaryCard = $savedCardHelper->getPrimaryCard($customerId);
//
//        	$hasPrimaryCard = ((bool) $primaryCard);
//
//
//        	$communicationFirstname = (trim($recipient['firstname'])) ? trim($recipient['firstname']) : $customer->getFirstname();
//        	$licenseId              = $recipient['license_id'];
//
//
////         	if($isTesting && $licenseId != '16583'){
//        		// Only run for this license...
////         		continue;
////         	}
//
//
//        	if(!$recipient['product_type']) {
//                $product_type = 'sma';
//            }
//            else {
//                $product_type = $recipient['product_type'];
//            }
//
//            $canAutoRenew = false;
//
//            if($product_type == 'subscription' && $hasPrimaryCard){
//
//	            /* @var $subscriptionHelper Storefront_Subscriptions_Helper_Data */
//	            $subscriptionHelper = Mage::helper('subscriptions');
//
//	            $subscriptionId = 'imi_'.$recipient['license_id'];
//
//	            $subscription = $subscriptionHelper->getSubscriptionById($subscriptionId);
//	            if($subscription && $subscription->isAutoRenew()){
//	            	$canAutoRenew = true;
//	            }
//            }
//
//
//            $all_ids = explode(",", $recipient['renewal_ids']);
//            $access_code = $recipient['access_code'];
//
//            $storeId    = Mage::app()->getStore()->getId();
//            $translate  = Mage::getSingleton('core/translate');
//            /* @var $translate Mage_Core_Model_Translate */
//            $translate->setTranslateInline(false);
//
//            $mailTemplate   = Mage::getModel('core/email_template');
//            $template       = $this->_getEmailTemplate($product_type, $monthsBefore, $canAutoRenew);
//
//
//            $order        = Mage::getModel('sales/order')->loadByIncrementId($recipient['order_no']);
//
//            $product      = Mage::getModel('catalog/product')->loadByAttribute('sku', $recipient['product_sku']);
//
//            //$product->setPrice(Mage::helper('core')->currency($product->getPrice(), true, false));
//
//            $customer_email = $customer->getEmail();
//            $dont_login_customers = array('allisong@infomap.com', 'rdexters@infomap.com', 'jamia@infomap.com', 'hhaggerty@infomap.com'/*, 'narendraraj.ojha@itoffshorenepal.com'*/);
//
//            $store_id = $order->getStoreId();
//            if(empty($store_id) || in_array($customer_email, $dont_login_customers)) {
//				$store_id = 0;
//				Mage::app()->setCurrentStore($storeId);
//			}
//			else {
//				Mage::app()->setCurrentStore($store_id);
//			}
//
//			$store_alias = Mage::getModel('storecodealias/storecodealias')->load($store_id, 'store_id')->getJoomlaMenuAlias();
//
//            $format                 = 'long';
//            $orderdate              = Mage::helper('core')->formatDate($order->getCreatedAt(), $format);
//            $subscription_end_date  = Mage::helper('core')->formatDate($recipient['sma_enddate'], $format);
//			$renewal_link			= str_replace('magento', $store_alias, Mage::getBaseUrl(Mage_Core_Model_Store::URL_TYPE_WEB)).'autoorder?access_code=' . $access_code;
//
//            $templateVars = array(
//                'order'       => $order,
//                'orderdate'   => $orderdate,
//                'firstname'   => $communicationFirstname,
//                'lastname'    => $customer->getLastname(),
//                'ordernumber' => $order->getIncrementId(),
//                'product'     => $product,
//                'logo_url'    => Mage::getDesign()->getSkinUrl('images/logo_email.gif'),
//                'store_url'   => Mage::getUrl(),
//                'subscription_end_date' => $subscription_end_date,
//				'renewal_link'	=> $renewal_link,
//            );
//
//
//            if($canAutoRenew){
//            	$templateVars['primary_card_number'] = $primaryCard->getCard();
//            	$templateVars['primary_card_type'] = $primaryCard->getCardType();
//            	$templateVars['primary_card_expiration_date'] = $primaryCard->getExpiryDate();
//            	$templateVars['edit_card_url'] = Mage::getUrl('savedcards/card', array('_store' => $store_id));
//            	$templateVars['all_subscriptions_url'] = Mage::getUrl('subscriptions/subscription', array('_store' => $store_id));
//            }
//
//
//            if($isTesting){
//            	// Force to my email for testing...
//            	$communicationEmail = 'wouter.samaey@storefront.be';
//            }
//
//
//            //Finally Send Emails & Update the DB
//            $mailTemplate->setDesignConfig(array('area' => 'frontend', 'store' => $storeId))
//                         ->sendTransactional(
//                            $template,
//                            Mage::getStoreConfig(self::XML_PATH_EMAIL_IDENTITY, $storeId),
//                            $communicationEmail,
//                            $communicationFirstname,
//                            $templateVars
//                         );
//
//            			/*$mailTemplate->setDesignConfig(array('area' => 'frontend', 'store' => $storeId))
//                         ->sendTransactional(
//                            $template,
//                            Mage::getStoreConfig(self::XML_PATH_EMAIL_IDENTITY, $storeId),
//                            "fvanlerberghe@informationmapping.com",
//                            "fvanlerberghe@informationmapping.com",
//                            $templateVars
//                         );*/
//
//
//            if($isTesting){
//            	// Only send 1 email and don't update the status
//            	return;
//            }
//
//
//            if ($mailTemplate->getSentSuccess()) {
//                //Update DB
//                $this->_updateEmailNotifiedFields($all_ids, $access_code, $monthsBefore);
//                $this->_updateLicenseLastCommunicationDate($licenseId);
//                $this->_insertCommunicationData($monthsBefore, $communicationEmail, $communicationFirstname, $licenseId);
//
//                $success_message = 'SUCCESS:: Renewal ID = ' . implode(",", $all_ids) . ', Customer Email=' . $customer->getEmail();
//                Mage::log($success_message, null, 'sma_notifications.log');
//
//            }else{
//                //log the error
//                $error_message = 'ERROR::  Renewal ID = ' . implode(",", $all_ids) . ', Customer Email=' . $customer->getEmail();
//                Mage::log($error_message, null, 'sma_notifications.log');
//            }
//
//            $translate->setTranslateInline(true);
//
//
//
//        }
//        /*********************************************************
//        * SENDING NOTIFICATION EMAILS - END
//        **********************************************************/
//        return true;
//    }
    
    private function getDataForSmaNotification($type)
    {
        $filter_date_sql = $filter_flag_sql = '';
        
        if($type == 2) {
            $filter_date_sql = 'lkr.sma_2m_b4_enddate';
            $filter_flag_sql = "AND lkr.sma_2m_b4_email_notified='0'";
        }
        elseif($type == 1) {
            $filter_date_sql = 'lkr.sma_1m_b4_enddate';
            $filter_flag_sql = "AND lkr.sma_2m_b4_email_notified='1' AND lkr.sma_1m_b4_email_notified='0'";
        }
        elseif($type == 0) {
            $filter_date_sql = 'lkr.sma_enddate';
            $filter_flag_sql = "AND lkr.sma_2m_b4_email_notified='1' AND lkr.sma_1m_b4_email_notified='1' AND lkr.sma_enddate_email_notified='0'";
        }
        
        $resource = Mage::getSingleton('core/resource');
        $readConnection = $resource->getConnection('core_read');
        $license_keys_renewalTable = $resource->getTableName('license_keys_renewal');
        $license_keysTable = $resource->getTableName('license_license');
        $license_infoTable = $resource->getTableName('license_info');
        
        $sql = "SELECT
                    GROUP_CONCAT(lkr.renewal_id) AS renewal_ids,
                    li.license_id, li.product_type, li.email, li.firstname, lkr.customer_id, 
                    lkr.order_no, lkr.product_sku, lkr.access_code, lkr.keyid, lkr.sma_enddate 
                FROM 
                    ". $license_keys_renewalTable ." lkr 
                    INNER JOIN ". $license_keysTable ." lk ON lkr.keyid = lk.id
                    INNER JOIN ". $license_infoTable ." li ON lk.license_id = li.license_id
                WHERE
                    NOW() >= " . $filter_date_sql . " 
                    AND li.license_type <> 'volume'
                    AND lkr.is_renewed = '0' AND lkr.renewal_status='1' AND lk.licensekey_status != '0'
                    " . $filter_flag_sql ."
                    GROUP BY lkr.product_sku, li.product_type, li.license_type, lkr.sma_enddate, li.email, li.firstname
                
                UNION
                    
                SELECT
                    lkr.renewal_id AS renewal_ids,
                    li.license_id, li.product_type, li.email, li.firstname, lkr.customer_id, 
                    lkr.order_no, lkr.product_sku, lkr.access_code, lkr.keyid, lkr.sma_enddate 
                FROM 
                    ". $license_keys_renewalTable ." lkr 
                    INNER JOIN ". $license_keysTable ." lk ON lkr.keyid = lk.id
                    INNER JOIN ". $license_infoTable ." li ON lk.license_id = li.license_id
                WHERE
                    NOW() >= " . $filter_date_sql . "
                    AND li.license_type = 'volume' 
                    AND lkr.is_renewed = '0' AND lkr.renewal_status='1' AND lk.licensekey_status != '0'
                    " . $filter_flag_sql ."
                ";
        return $readConnection->fetchAll($sql);        
    }
    
    private function _updateEmailNotifiedFields($ids, $access_code, $type)
    {
        $connection  = Mage::getSingleton('core/resource');
        $write = $connection->getConnection('core_write');
        $keysRenewalTable = $connection->getTableName('license_keys_renewal');
        
        if($type == 2) {
            $notify_field = 'sma_2m_b4_email_notified';
        }
        else if($type == 1) {
            $notify_field = 'sma_1m_b4_email_notified';
        }
        else if($type == 0) {
            $notify_field = 'sma_enddate_email_notified';
        }
        
        if(!$notify_field) { 
            return; 
        }
        
        $all_ids = implode(",", $ids); 
        $updateQuery = "UPDATE ". $keysRenewalTable . " SET ". $notify_field ." = 1, access_code = ? WHERE renewal_id IN (" . $all_ids . ")";
        $write->query($updateQuery, array($access_code));
    }
    
    private function _updateLicenseLastCommunicationDate($licenseId)
    {
        $connection  = Mage::getSingleton('core/resource');
        $write = $connection->getConnection('core_write');
        $licenseinfoTable = $connection->getTableName('license_info');
        
        $today = date('Y-m-d');
        $updateQuery = "UPDATE ". $licenseinfoTable . " SET last_communication_date = ? WHERE license_id = ?";
        $write->query($updateQuery, array($today, $licenseId));
    }
    
    private function _insertCommunicationData($type, $email, $firstname, $licenseId)
    {
        $connection  = Mage::getSingleton('core/resource');
        $write = $connection->getConnection('core_write');
        $communicationTable = $connection->getTableName('license_mail');
        
        $insertQuery = "INSERT INTO ". $communicationTable . " SET mailed_at = ?, communication_type = ?, email = ?, firstname = ?, license_id = ?";
        $write->query($insertQuery, array(date('Y-m-d'), $type, $email, $firstname, $licenseId));
    }
    
    private function _getEmailTemplate($prodType, $subscriptionType, $canAutoRenew)
    {
    	$storeId = null;
        $template = '';
        
        
        if(strcasecmp($prodType, 'sma') == 0) {
        	
        	if($subscriptionType == 2) {
                $template = Mage::getStoreConfig('smaconfiguration/smaemails/et2monthsbefore', $storeId);
            }
            elseif($subscriptionType == 1) {
                $template = Mage::getStoreConfig('smaconfiguration/smaemails/et1monthsbefore', $storeId);
            }
            elseif($subscriptionType == 0) {
                $template = Mage::getStoreConfig('smaconfiguration/smaemails/etendofsma', $storeId);
            } 

        	
        }
        else {
        	// Subscription
        	 
        	if($canAutoRenew){
        		$template = Mage::getStoreConfig('smaconfiguration/subemailtemplates/autorenew', $storeId);
        		 
        	}else{
        		 
	            if($subscriptionType == 2) {
	                $template = Mage::getStoreConfig('smaconfiguration/subemailtemplates/subet2monthsbefore', $storeId);
	            }
	            elseif($subscriptionType == 1) {
	                $template = Mage::getStoreConfig('smaconfiguration/subemailtemplates/subet1monthsbefore', $storeId);
	            }
	            elseif($subscriptionType == 0) {
	                $template = Mage::getStoreConfig('smaconfiguration/subemailtemplates/subetendofsma', $storeId);
	            } 
        	}
        }
        
        
        return $template;
    }


}