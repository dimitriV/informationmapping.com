<?php

class Itoffshore_Licensemanager_Model_Observer {

    public function addSubscriptionGridColumns($observer) {
        /* @var $grid Storefront_Subscriptions_Block_Adminhtml_Subscription_Grid */
        $grid = $observer->getGrid();

        $helper = Mage::helper('licensemanager');

        $grid->addColumnAfter('serial', array(
            'header' => $helper->__('Serial'),
            'index' => 'serial'
        ), 'id');

        $grid->addColumnAfter('activation_email', array(
            'header' => $helper->__('Activation Email'),
            'index' => 'activation_email'
        ), 'customer_email');

    }

    public function addRenewalGridColumns($observer) {
        /* @var $grid Storefront_Subscriptions_Block_Adminhtml_Subscription_Grid */
        $grid = $observer->getGrid();

        $helper = Mage::helper('licensemanager');


        $grid->addColumnAfter('r_serial', array(
            'header' => $helper->__('Serial'),
            'index' => 'serial',
        ), 'r_id');

        $grid->addColumnAfter('r_pool_id', array(
            'header' => $helper->__('Pool ID'),
            'index' => 'pool_id',
            'type' => 'number'
        ), 'r_order_item_id');


    }

    public function addSerialToSubscriptionForm($observer) {

        $helper = Mage::helper('licensemanager');

        /* @var $fieldset Varien_Data_Form_Element_Fieldset */
        $fieldset = $observer->getFieldset();

        $fieldset->addField('serial', 'text', array(
            'name' => 'serial',
            'label' => $helper->__('Serial key'),
            'title' => $helper->__('Serial key'),
            'required' => false,
        ));


    }

    public function addSerialToRenewalForm($observer) {

        $helper = Mage::helper('licensemanager');

        /* @var $fieldset Varien_Data_Form_Element_Fieldset */
        $fieldset = $observer->getFieldset();

        $fieldset->addField('serial', 'text', array(
            'name' => 'serial',
            'label' => $helper->__('Serial key'),
            'title' => $helper->__('Serial key'),
            'required' => false,
        ));

        $fieldset->addField('pool_id', 'text', array(
            'name' => 'pool_id',
            'label' => $helper->__('Pool ID'),
            'title' => $helper->__('Pool ID'),
            'required' => false,
            'disabled'=> true
        ));
    }
//
//
//    public function getPoolItem($productId){
//        return $this->_getPoolItem($productId);
//    }
    protected function _getPoolItem($productId) {
        /* @var $poolCol Itoffshore_Licensemanager_Model_Mysql4_License_Pool_Collection */
        $poolCol = Mage::getResourceModel('licensemanager/license_pool_collection');
        $poolCol->joinSubscriptions();
        $poolCol->addFieldToFilter('product_id', $productId);
        $poolCol->getSelect()->where('subscriptions_subscription.id is null');
        $poolCol->getSelect()->where('subscriptions_renewal.id is null');
        $poolCol->getSelect()->limit(1);

        $sql = $poolCol->getSelect().'';

        return $poolCol->getFirstItem();
    }

    public function onCreateSubscription($observer) {
        /* @var $subscription Storefront_Subscriptions_Model_Subscription */
        $subscription = $observer->getSubscription();

        // Assign key from pool

        $item = $subscription->getOrderItem();

        $productId = $item->getProductId();

        $poolItem = $this->_getPoolItem($productId);

        $subscription->setPoolId($poolItem->getId());
        $subscription->setSerial($poolItem->getSerial());

    }

    public function onRenewCreated($observer) {
        /* @var $renewal Storefront_Subscriptions_Model_Renewal */
        $renewal = $observer->getRenewal();

        // Assign key from pool

        $item = $renewal->getOrderItem();

        $productId = $item->getProductId();

        $poolItem = $this->_getPoolItem($productId);

        $renewal->setPoolId($poolItem->getId());
        $renewal->setSerial($poolItem->getSerial());
    }


//
//
//    protected function _processRenewal($renewal_ids, $orderItem) {
//        //Handle renewal of the product
//        $renewal_data = Mage::getModel('licensemanager/licenserenewal')->getLicenseRenewalsByRenewalids($renewal_ids);
//        $status = 1;
//
//        $order = $orderItem->getOrder();
//        $orderItemId = $orderItem->getId();
//
//        $availableProductSkus = $this->_getAvailableProductSkus();
//
//        foreach ($renewal_data as $renewal) {
//            $renewal_id = $renewal['renewal_id'];
//            $renewalSku = $renewal['product_sku'];
//            $renewalKeyid = $renewal['keyid'];
//
//            $orderId = $order->getId();
//            $orderIncrementId = $order->getIncrementId();
//            $customerId = $order->getCustomerId();
//
//            if ($customerId) {
//                $customer = Mage::getModel('customer/customer')->load($customerId);
//                $customerName = $customer->getFirstname() . ' ' . $customer->getLastname();
//            } else {
//                $customerName = '';
//            }
//            $orderDate = date('Y-m-d', strtotime($order->getCreatedAt()));
//
//            $licenseKey = Mage::getModel('licensemanager/keys')->getLicensekeyByKeyid($renewalKeyid);
//
//            $communication = $this->_getCommunicationData($order);
//
//            if (!array_key_exists($orderItem['renewProductId'], $availableProductSkus)) {
//                $productSku = $orderItem['smaOut'];
//                $productExpDate = $orderItem['expDate'];
//                $productSmaType = $orderItem['smaType'];
//                $productSmaOut = $orderItem['smaOut'];
//
//                $licenseKeyInfo = Mage::getModel('licensemanager/keys')->getLicensekeyInfoByKey($licenseKey);
//                $licenseId = $licenseKeyInfo->getLicenseId();
//                $licenseKeyId = $licenseKeyInfo->getLicenseKeyid();
//                $lastLicenseKey = $licenseKey;
//
//                $this->insertOrderInfo($orderIncrementId, $customerName, $orderDate, $licenseId);
//                $this->_updateLicenseInfo($productExpDate, $lastLicenseKey, $status, $licenseId, null, $communication);
//                $this->_updateLicenseKey($productSku, $orderDate, $productExpDate, $status, $licenseKeyId);
//                $this->_updateLicenseRenewalInfo($customerId, $renewalSku, $orderId, $orderIncrementId, $licenseKeyId, $productSmaType, $productSmaOut, $renewal_id);
//
//            } else {
//
//                $prevLicenseKeyInfo = Mage::getModel('licensemanager/keys')->getLicensekeyInfoByKey($licenseKey);
//                $licenseId = $prevLicenseKeyInfo->getLicenseId();
//
//                $serial_product = Mage::getModel('licensemanager/licensekeys');
//                $serial_number = $serial_product->assignSerialNumber($orderItemId, $orderItem['renewProductId'], $order);
//                $licenseKey = $serial_number['serial'];
//
//                $productSku = $orderItem['renewProductSku'];
//                $smaData = Mage::getModel('licensemanager/licensemanager')->getSMAConfigData($productSku, date("Y-m-d"), $orderItem['productType']);
//                $productSmaType = $smaData['sma_type'];
//                $productExpDate = $smaData['exp_date'];
//                $productSmaOut = $smaData['sma_out'];
//                $lastLicenseKey = $licenseKey;
//                $currentDate = $orderItem['todayDate'];
//
//                $this->insertOrderInfo($orderIncrementId, $customerName, $orderDate, $licenseId);
//                $this->_updateLicenseInfo($productExpDate, $lastLicenseKey, $status, $licenseId, null, $communication);
//                $licenseKeyId = $this->insertLicensekeysInfo($licenseKey, $productSku, $currentDate, $status, $productExpDate, $licenseId);
//                $this->insertLicenseRenewalInfo($customerId, $productSku, $orderId, $orderIncrementId, $licenseKeyId, $productSmaType, $productSmaOut);
//
//            }
//        }
//    }


//    public function cancelLicensKeys($orderItemId) {
//        $serialCollection = Mage::getResourceModel('licensemanager/license_pool_collection')->loadByOrderItemId($orderItemId);
//
//        $cancelCount = 0;
//        foreach ($serialCollection as $serial) {
//            $serial->setStatus(Itoffshore_Licensemanager_Model_Licensekeys::STATUS_UNASSIGNED)->setCustomerId(NULL)->setProductId(NULL)->setOrderId(NULL)->setOrderItemId(NULL)->setDateAssigned(NULL)->save();
//
//            $cancelCount++;
//        }
//        return $cancelCount;
//    }


//    public function sales_order_item_cancel($event) {
//        /* @var $orderItem Mage_Sales_Model_Order_Item */
//        $orderItem = $event->getItem();
//        $product = Mage::getModel('catalog/product')->load($orderItem->getProductId());
//
//        $cancelCount = $this->cancelLicensKeys($orderItem->getId());
//
//        if ($cancelCount > 0) {
//            Mage::getSingleton('adminhtml/session')->addSuccess(Mage::helper('licensemanager')->__('%d license key(s) successfully changed from "Assigned" to "Unassigned"', $cancelCount));
//        } else {
//            Mage::getSingleton('adminhtml/session')->addWarning(Mage::helper('licensemanager')->__('No license keys were removed in the cancelling of this order'));
//        }
//    }

//    private function insertLicenseInformation($serial_number, $productType, $licenseType, $order, $productSku, $productExpDate, $productSmaType, $productSmaOut, $parentLicenseId) {
//        $productType = ($productType == '') ? 'none' : strtolower($productType);
//        $licenseType = $this->getLicenseTypeValue($licenseType);
//        $currentDate = Mage::getModel('core/date')->date('Y-m-d');
//        $licenseKey = $serial_number['serial'];
//        $status = 1;
//
//        $orderId = $order->getId();
//        $orderIncrementId = $order->getIncrementId();
//        $customerId = $order->getCustomerId();
//
//        // For registered customers
//        if ($customerId) {
//            $customer = Mage::getModel('customer/customer')->load($customerId);
//            $customerName = $customer->getFirstname() . ' ' . $customer->getLastname();
//        } // Guest customers
//        else {
//            $customerName = '';
//        }
//
//        // These values are always filled up, in Data.php
//        $imiCustomerEmail = $order->getImiCustomerEmail();
//        $imiCustomerFirstname = $order->getImiCustomerFirstname();
//
//        $orderDate = date('Y-m-d', strtotime($order->getCreatedAt()));
//
//        $licenseId = $this->insertLicenseInfo($productType, $licenseType, $currentDate, $licenseKey, $status, $imiCustomerEmail, $imiCustomerFirstname, $productExpDate, $parentLicenseId);
//        $ordersId = $this->insertOrderInfo($orderIncrementId, $customerName, $orderDate, $licenseId);
//        $licenseKey = $this->insertLicensekeysInfo($licenseKey, $productSku, $currentDate, $status, $productExpDate, $licenseId);
//        $renewalId = $this->insertLicenseRenewalInfo($customerId, $productSku, $orderId, $orderIncrementId, $licenseKey, $productSmaType, $productSmaOut);
//
//
//    }

//    private function updateLicenseUpgradeInformation($productType, $licenseType, $licenseKey, $order, $productSku, $productExpDate, $productSmaType, $productSmaOut, $parentLicenseId, $newLicense = '', $hasLicense = false) {
//        $productType = ($productType == '') ? 'none' : strtolower($productType);
//        $licenseType = $this->getLicenseTypeValue($licenseType);
//
//        $currentDate = Mage::getModel('core/date')->date('Y-m-d');
//        $status = 1;
//
//        $orderId = $order->getId();
//        $orderIncrementId = $order->getIncrementId();
//        $customerId = $order->getCustomerId();
//        if ($customerId) {
//            $customer = Mage::getModel('customer/customer')->load($customerId);
//            $customerName = $customer->getFirstname() . ' ' . $customer->getLastname();
//        } else {
//            $customerName = '';
//        }
//
//        $orderDate = date('Y-m-d', strtotime($order->getCreatedAt()));
//
//        $licenseKeyInfo = Mage::getModel('licensemanager/keys')->getLicensekeyInfoByKey($licenseKey);
//        $licenseId = $licenseKeyInfo->getLicenseId();
//        $licenseKeyId = $licenseKeyInfo->getLicenseKeyid();
//        if ($newLicense == '') {
//            $lastLicenseKey = $licenseKey;
//        } else {
//            $licenseKey = $newLicense;
//            $lastLicenseKey = $newLicense;
//        }
//
//        $this->insertOrderInfo($orderIncrementId, $customerName, $orderDate, $licenseId);
//
//        $communication = $this->_getCommunicationData($order);
//        $this->_updateLicenseInfo($productExpDate, $lastLicenseKey, $status, $licenseId, $parentLicenseId, $communication);
//
//        if ($hasLicense == true) {
//            $this->insertLicensekeysInfo($licenseKey, $productSku, $currentDate, $status, $productExpDate, $licenseId);
//            $this->insertLicenseRenewalInfo($customerId, $productSku, $orderId, $orderIncrementId, $licenseKey, $productSmaType, $productSmaOut);
//        } else {
//            $this->_updateLicenseKey($productSku, $orderDate, $productExpDate, $status, $licenseKeyId);
//            $this->_updateLicenseRenewalInfo($customerId, $productSku, $orderId, $orderIncrementId, $licenseKeyId, $productSmaType, $productSmaOut);
//        }
//    }
//
//
//    private function _processVolumeLicense($productType, $licenseType, $order, $productSku, $productExpDate, $currentLicensekeys, $productSmaType, $productSmaOut, $orderedQty) {
//        $productType = ($productType == '') ? 'none' : strtolower($productType);
//        $licenseType = $this->getLicenseTypeValue($licenseType);
//
//        $currentDate = Mage::getModel('core/date')->date('Y-m-d');
//        $licenseKey = null;
//        $status = 1;
//
//        // These values are always filled up, in Data.php
//        $imiCustomerEmail = $order->getImiCustomerEmail();
//        $imiCustomerFirstname = $order->getImiCustomerFirstname();
//
//        $orderId = $order->getIncrementId();
//        $customerId = $order->getCustomerId();
//        if ($customerId) {
//            $customer = Mage::getModel('customer/customer')->load($customerId);
//            $customerName = $customer->getFirstname() . ' ' . $customer->getLastname();
//        } else {
//            $customerName = '';
//        }
//
//        $orderDate = date('Y-m-d', strtotime($order->getCreatedAt()));
//
//        if (trim($currentLicensekeys) == '') {
//            $licenseId = $this->insertLicenseInfo($productType, $licenseType, $currentDate, $licenseKey, $status, $imiCustomerEmail, $imiCustomerFirstname, null, $parentLicenseId);
//        } else {
//            $licenseKeyInfo = Mage::getModel('licensemanager/keys')->getLicensekeyInfoByKey($currentLicensekeys);
//            $licenseId = $licenseKeyInfo->getLicenseId();
//            $licenseKeyId = $licenseKeyInfo->getLicenseKeyid();
//            $lastLicenseKey = $currentLicensekeys;
//
//            $communication = $this->_getCommunicationData($order);
//            $this->_updateLicenseInfo($productExpDate, $lastLicenseKey, $status, $licenseId, null, $communication);
//
//            $this->_updateLicenseKey($productSku, $orderDate, $productExpDate, $status, $licenseKeyId);
//            $this->_updateLicenseRenewalInfo($customerId, $productSku, $orderId, $orderId, $licenseKeyId, $productSmaType, $productSmaOut);
//        }
//        $orderId = $this->insertOrderInfo($orderId, $customerName, $orderDate, $licenseId, $orderedQty);
//
//    }
//
//    private function getLicenseTypeValue($type) {
//        if ($type == '') {
//            $licenseType = 'none';
//        } elseif (strcasecmp($type, 'Volume License') == 0) {
//            $licenseType = 'volume';
//        } else {
//            $licenseType = 'single';
//        }
//        return $licenseType;
//    }
//
//    private function insertLicenseInfo($productType, $licenseType, $currentDate, $licenseKey, $status, $customerEmail, $customerFirstname, $productExpDate, $parentLicenseId) {
//        $connection = Mage::getSingleton('core/resource');
//        $write = $connection->getConnection('core_write');
//        $tableName = $connection->getTableName('license_info');
//
//        if ($parentLicenseId == 0) {
//            $insertQuery = "INSERT INTO " . $tableName . " SET product_type = ?, license_type = ?, initial_purchase_date = ?,
//                            expiration_date = ?, last_license_key = ?, status = ?, overwrite_communication = ?, email = ?, firstname = ?";
//            $write->query($insertQuery, array(
//                $productType,
//                $licenseType,
//                $currentDate,
//                $productExpDate,
//                $licenseKey,
//                $status,
//                1,
//                $customerEmail,
//                $customerFirstname
//            ));
//            return $write->lastInsertId();
//        } else {
//            $overwrite_communication = Mage::getModel('licensemanager/info')->getOverwriteCommunication($parentLicenseId);
//            if ($customerEmail && $customerFirstname && $overwrite_communication) {
//                $updateQuery = "UPDATE " . $tableName . " SET expiration_date = ?, last_license_key = ?, status = ?, email = ?, firstname = ? WHERE license_id = ?";
//                $write->query($updateQuery, array(
//                    $productExpDate,
//                    $licenseKey,
//                    1,
//                    $customerEmail,
//                    $customerFirstname,
//                    $parentLicenseId
//                ));
//            } else {
//                $updateQuery = "UPDATE " . $tableName . " SET expiration_date = ?, last_license_key = ?, status = ? WHERE license_id = ? ";
//                $write->query($updateQuery, array(
//                    $productExpDate,
//                    $licenseKey,
//                    1,
//                    $parentLicenseId
//                ));
//            }
//            return $parentLicenseId;
//        }
//    }

//    public function insertOrderInfo($orderId, $customerName, $orderDate, $licenseId, $orderedQty = 1) {
//        $connection = Mage::getSingleton('core/resource');
//        $write = $connection->getConnection('core_write');
//        $tableName = $connection->getTableName('license_orders');
//
//        $insertQuery = "INSERT INTO " . $tableName . " SET order_id = ?, customer_name = ?, order_date = ?,
//                        number_of_activations = ?, license_id = ?";
//        $write->query($insertQuery, array(
//            $orderId,
//            $customerName,
//            $orderDate,
//            $orderedQty,
//            $licenseId
//        ));
//        return $write->lastInsertId();
//    }
//
//    private function insertLicensekeysInfo($licenseKey, $productSku, $renewalDate, $licensekeyStatus, $productExpDate, $licenseId) {
//        $connection = Mage::getSingleton('core/resource');
//        $write = $connection->getConnection('core_write');
//        $licenseKeysTableName = $connection->getTableName('license_license');
//
//        $insertQuery = "INSERT INTO " . $licenseKeysTableName . " SET key = ?, product_sku = ?, last_renewal_date = ?,
//                        expiration_date = ?, number_of_activations = ?, licensekey_status = ?, license_id = ?";
//        $write->query($insertQuery, array(
//            $licenseKey,
//            $productSku,
//            $renewalDate,
//            $productExpDate,
//            1,
//            $licensekeyStatus,
//            $licenseId
//        ));
//        return $write->lastInsertId();
//    }
//
//    public function insertLicenseRenewalInfo($customerId, $prodSku, $orderId, $orderIncrementId, $licenseKey, $productSmaType, $productSmaOut) {
//        $connection = Mage::getSingleton('core/resource');
//        $write = $connection->getConnection('core_write');
//        $renewTableName = $connection->getTableName('license_keys_renewal');
//
//        $today_date = date('Y-m-d');
//
//        $sma_1m_b4_enddate = Mage::getModel('licensemanager/licensemanager')->getFutureDate($today_date, $productSmaType, 1);
//        $sma_2m_b4_enddate = Mage::getModel('licensemanager/licensemanager')->getFutureDate($today_date, $productSmaType, 2);
//        $sma_enddate = Mage::getModel('licensemanager/licensemanager')->getFutureDate($today_date, $productSmaType, 0);
//        $sma_2w_after_enddate = Mage::getModel('licensemanager/licensemanager')->getFutureDate($today_date, $productSmaType, '-1');
//        $access_code = Mage::getModel('licensemanager/licensemanager')->generateAccessCode($licenseKey, $prodSku);
//
//        $insertQuery = "INSERT INTO " . $renewTableName . " SET customer_id = ?, order_id = ?, order_no = ?, product_sku = ?, sma_type = ?,
//        sma_1m_b4_enddate = ?, sma_2m_b4_enddate = ?, sma_enddate = ?, sma_2w_after_enddate = ?, access_code = ?, keyid = ?";
//        $write->query($insertQuery, array(
//            $customerId,
//            $orderId,
//            $orderIncrementId,
//            $productSmaOut,
//            $productSmaType,
//            $sma_1m_b4_enddate,
//            $sma_2m_b4_enddate,
//            $sma_enddate,
//            $sma_2w_after_enddate,
//            $access_code,
//            $licenseKey
//        ));
//        return $write->lastInsertId();
//    }
//
//    private function _updateLicenseRenewalInfo($customerId, $productSku, $orderId, $orderIncrementId, $licenseKeyId, $productSmaType, $productSmaOut, $renewal_id = null) {
//        $connection = Mage::getSingleton('core/resource');
//        $write = $connection->getConnection('core_write');
//        $renewTableName = $connection->getTableName('license_keys_renewal');
//
//        $today_date = date('Y-m-d');
//
//        $sma_1m_b4_enddate = Mage::getModel('licensemanager/licensemanager')->getFutureDate($today_date, $productSmaType, 1);
//        $sma_2m_b4_enddate = Mage::getModel('licensemanager/licensemanager')->getFutureDate($today_date, $productSmaType, 2);
//        $sma_enddate = Mage::getModel('licensemanager/licensemanager')->getFutureDate($today_date, $productSmaType, 0);
//        $sma_2w_after_enddate = Mage::getModel('licensemanager/licensemanager')->getFutureDate($today_date, $productSmaType, '-1');
//        $access_code = Mage::getModel('licensemanager/licensemanager')->generateAccessCode($licenseKeyId, $productSku);
//
//        if ($renewal_id === null) {
//            $updateQuery = "UPDATE " . $renewTableName . " SET customer_id = ?, order_id = ?, order_no = ?, product_sku = ?, sma_type = ?,
//            sma_1m_b4_enddate = ?, sma_2m_b4_enddate = ?, sma_enddate = ?, sma_2w_after_enddate = ?, access_code = ? WHERE keyid = ?
//            ORDER BY renewal_id DESC LIMIT 1";
//            $write->query($updateQuery, array(
//                $customerId,
//                $orderId,
//                $orderIncrementId,
//                $productSmaOut,
//                $productSmaType,
//                $sma_1m_b4_enddate,
//                $sma_2m_b4_enddate,
//                $sma_enddate,
//                $sma_2w_after_enddate,
//                $access_code,
//                $licenseKeyId
//            ));
//        } else {
//            $updateQuery = "UPDATE " . $renewTableName . " SET is_renewed = ?, next_order_number = ?, renewal_status = ? WHERE renewal_id = ?";
//            $write->query($updateQuery, array(
//                1,
//                $orderIncrementId,
//                0,
//                $renewal_id
//            ));
//
//            $insertQuery = "INSERT INTO " . $renewTableName . " SET customer_id = ?, order_id = ?, order_no = ?, product_sku = ?, sma_type = ?,
//            sma_1m_b4_enddate = ?, sma_2m_b4_enddate = ?, sma_enddate = ?, sma_2w_after_enddate = ?, access_code = ?, keyid = ?";
//            $write->query($insertQuery, array(
//                $customerId,
//                $orderId,
//                $orderIncrementId,
//                $productSmaOut,
//                $productSmaType,
//                $sma_1m_b4_enddate,
//                $sma_2m_b4_enddate,
//                $sma_enddate,
//                $sma_2w_after_enddate,
//                $access_code,
//                $licenseKeyId
//            ));
//        }
//    }

    public function saveLicenseFieldsInformation($observer) {
        $item = $observer->getQuoteItem();

        //Set current license key for upgrade products
        $licenseKey = $observer->getEvent()->getLicensekey();
        if ($licenseKey) {
            $item->setCurrentLicensekey($licenseKey);
        }

        //Set renewal license ids for group order checkout
        $renewals = Mage::getModel('checkout/session')->getRenewalIds();
        if (trim($renewals) != '') {
            $item->setRenewalLicenseIds($renewals);
        }
    }

//    private function _updateLicenseInfo($expDate, $lastKey, $keyStatus, $licenseId, $parentLicense, $communication) {
//        if (trim($licenseId) == '') {
//            return;
//        }
//        $connection = Mage::getSingleton('core/resource');
//        $write = $connection->getConnection('core_write');
//        $licenseInfoTable = $connection->getTableName('license_info');
//
//        //Communication override for license upgrade and renewal
//        $override_communication = Mage::getModel('licensemanager/info')->getOverwriteCommunication($licenseId);
//        if ($override_communication) {
//            if ($parentLicense == null) {
//                $updateQuery = "UPDATE " . $licenseInfoTable . " SET expiration_date = ?, number_of_renewals = number_of_renewals+1, last_license_key = ?, status = ? WHERE license_id = ?";
//            } else {
//                $updateQuery = "UPDATE " . $licenseInfoTable . " SET expiration_date = ?, last_license_key = ?, status = ? WHERE license_id = ?";
//            }
//            $write->query($updateQuery, array(
//                $expDate,
//                $lastKey,
//                $keyStatus,
//                $licenseId
//            ));
//        } else {
//            $email = $communication['customerEmail'];
//            $firstname = $communication['customerFirstname'];
//
//            if ($parentLicense == null) {
//                $updateQuery = "UPDATE " . $licenseInfoTable . " SET expiration_date = ?, number_of_renewals = number_of_renewals+1, last_license_key = ?, status = ?, email = ?, firstname = ? WHERE license_id = ?";
//            } else {
//                $updateQuery = "UPDATE " . $licenseInfoTable . " SET expiration_date = ?, last_license_key = ?, status = ?, email = ?, firstname = ? WHERE license_id = ?";
//            }
//            $write->query($updateQuery, array(
//                $expDate,
//                $lastKey,
//                $keyStatus,
//                $email,
//                $firstname,
//                $licenseId
//            ));
//        }
//    }
//
//    private function _updateLicenseKey($productSku, $orderDate, $productExpDate, $status, $licenseKeyId) {
//        if (trim($licenseKeyId) == '') {
//            return;
//        }
//        $connection = Mage::getSingleton('core/resource');
//        $write = $connection->getConnection('core_write');
//        $licenseKeysTable = $connection->getTableName('license_license');
//
//        $updateQuery = "UPDATE " . $licenseKeysTable . " SET product_sku = ?, last_renewal_date = ?, expiration_date = ?, licensekey_status = ? WHERE id = ?";
//        $write->query($updateQuery, array(
//            $productSku,
//            $orderDate,
//            $productExpDate,
//            $status,
//            $licenseKeyId
//        ));
//    }
//
//    private function _getCommunicationData($order) {
//        // These values are always filled up, in Data.php
//        $imiCustomerEmail = $order->getImiCustomerEmail();
//        $imiCustomerFirstname = $order->getImiCustomerFirstname();
//
//        return array(
//            "customerEmail" => $customerEmail,
//            "$imiCustomerEmail" => $imiCustomerFirstname
//        );
//    }

    /* Save customer email and firstname to sales_flat_order and on the license level */
    public function setIMIAdditionalFields($observer) {

        $request = Mage::app()->getRequest();
        $order = $observer->getEvent()->getOrder();

        $imiCustomerFirstname = trim($request->getPost('imi_customer_firstname'));
        $imiCustomerEmail = trim($request->getPost('imi_customer_email'));

        if (($imiCustomerFirstname == '') || ($imiCustomerEmail == '')) {
            $order = $observer->getEvent()->getOrder();
            $address = $order->getBillingAddress();
            $imiCustomerFirstname = ($imiCustomerFirstname == '') ? $address->getFirstname() : $imiCustomerFirstname;

            // There is no emailaddress provided on a new billing address from a partner, so get it from the customer object.
            $emailaddress = ($address->getEmail() == '') ? $order->getCustomer()->getEmail() : $address->getEmail();
            $imiCustomerEmail = ($imiCustomerEmail == '') ? $emailaddress : $imiCustomerEmail;

        }

        $order->setImiCustomerFirstname($imiCustomerFirstname);
        $order->setImiCustomerEmail($imiCustomerEmail);
    }

}