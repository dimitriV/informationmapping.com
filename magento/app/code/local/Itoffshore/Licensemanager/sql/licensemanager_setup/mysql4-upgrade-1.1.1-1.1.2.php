<?php

$installer = $this;

$installer->startSetup();

// Allow NULL in pool_id on renewal table - needed for foreign key checks. The pool_id is saved later...
$installer->run("ALTER TABLE `subscriptions_renewal`
DROP FOREIGN KEY `renewal_fk2`;
ALTER TABLE `subscriptions_renewal` 
CHANGE COLUMN `pool_id` `pool_id` INT(10) UNSIGNED NULL COMMENT '' ;
ALTER TABLE `subscriptions_renewal` 
ADD CONSTRAINT `renewal_fk2`
  FOREIGN KEY (`pool_id`)
  REFERENCES `license_pool` (`id`)
  ON DELETE NO ACTION
  ON UPDATE CASCADE;
");


$installer->endSetup(); 