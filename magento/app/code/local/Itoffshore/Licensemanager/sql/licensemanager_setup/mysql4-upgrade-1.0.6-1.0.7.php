<?php

$installer = $this;

$installer->startSetup();

// Add subscription start date (needed for manual activations)
$installer->run("ALTER TABLE `subscriptions_subscription` ADD COLUMN `started_on` TIMESTAMP NULL DEFAULT NULL COMMENT '' AFTER `_license_id`;");



$installer->endSetup(); 