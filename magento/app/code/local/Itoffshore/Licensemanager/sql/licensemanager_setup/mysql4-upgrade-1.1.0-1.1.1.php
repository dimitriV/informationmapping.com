<?php

$installer = $this;

$installer->startSetup();

// Default auto_renew to 0, not NULL
$installer->run("ALTER TABLE `subscriptions_subscription` CHANGE COLUMN `auto_renew` `auto_renew` TINYINT(1) UNSIGNED NOT NULL DEFAULT 0");


$installer->endSetup(); 