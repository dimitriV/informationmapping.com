<?php

$installer = $this;

$installer->startSetup();

//Alter license_keys table to set foreign key
$installer->run("
    ALTER TABLE license_keys ADD CONSTRAINT keys_fk1 FOREIGN KEY(license_id) 
    REFERENCES license_info(license_id) ON DELETE CASCADE
");

//Alter license_orders table to set foreign key
$installer->run("
    ALTER TABLE license_orders ADD CONSTRAINT orders_fk1 FOREIGN KEY(license_id) 
    REFERENCES license_info(license_id) ON DELETE CASCADE
");

//Alter license_communication table to set foreign key
$installer->run("
    ALTER TABLE license_communication ADD CONSTRAINT communication_fk1 FOREIGN KEY(license_id) 
    REFERENCES license_info(license_id) ON DELETE CASCADE
");
$installer->endSetup(); 