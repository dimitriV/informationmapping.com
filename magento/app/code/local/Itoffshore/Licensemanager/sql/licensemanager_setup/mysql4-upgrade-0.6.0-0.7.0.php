<?php

$installer = $this;

$installer->startSetup();

//Create license_info table
$installer->run("

-- DROP TABLE IF EXISTS {$this->getTable('license_keys_renewal')};
    CREATE TABLE {$this->getTable('license_keys_renewal')} (
        renewal_id INT UNSIGNED AUTO_INCREMENT PRIMARY KEY,
        customer_id int DEFAULT NULL,
        order_id int DEFAULT NULL,
        order_no int DEFAULT NULL,
        qty int DEFAULT NULL,
        sma_type ENUM('1','2','3','0') DEFAULT '0',
        sma_1m_b4_enddate DATE,
        sma_2m_b4_enddate DATE,
        sma_enddate DATE,
        sma_2w_after_enddate DATE,
        is_renewed ENUM('1','0') DEFAULT '0',
        next_order_number VARCHAR(255),
        sma_1m_b4_email_notified ENUM('1','0') DEFAULT '0',
        sma_2m_b4_email_notified ENUM('1','0') DEFAULT '0',
        sma_enddate_email_notified ENUM('1','0') DEFAULT '0',
        access_code VARCHAR(255),
        ogone_alias VARCHAR(255),
        keyid int(10) NOT NULL 
    );
");

$installer->endSetup(); 