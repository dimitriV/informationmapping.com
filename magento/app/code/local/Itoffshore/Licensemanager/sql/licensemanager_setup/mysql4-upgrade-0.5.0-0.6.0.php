<?php
$installer = $this;
$installer->startSetup();

$table = $this->getTable('sales_flat_quote_item');
$query = 'ALTER TABLE `' . $table . '` ADD COLUMN `current_licensekey` text';
$connection = Mage::getSingleton('core/resource')->getConnection('core_write');
$connection->query($query);

$installer->endSetup();