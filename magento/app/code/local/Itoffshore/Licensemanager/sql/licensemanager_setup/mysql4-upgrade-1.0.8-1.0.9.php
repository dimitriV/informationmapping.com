<?php

$installer = $this;

$installer->startSetup();

// Renamed column, no longer needed...
$installer->run("ALTER TABLE `license_pool` CHANGE COLUMN `assigned_on` `_assigned_on` TIMESTAMP NULL DEFAULT NULL");


$installer->endSetup(); 