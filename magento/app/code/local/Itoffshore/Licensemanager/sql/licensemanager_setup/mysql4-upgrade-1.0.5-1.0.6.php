<?php

$installer = $this;

$installer->startSetup();

// Rename column license_id => subscription_id in subscriptions_renewal
$installer->run("ALTER TABLE subscriptions_renewal
DROP FOREIGN KEY `renewal_fk1`;
ALTER TABLE `subscriptions_renewal`
CHANGE COLUMN `license_id` `subscription_id` INT(10) UNSIGNED NULL DEFAULT NULL COMMENT '' ;
ALTER TABLE `subscriptions_renewal`
ADD CONSTRAINT `renewal_fk1`
  FOREIGN KEY (`subscription_id`)
  REFERENCES `subscriptions_subscription` (`id`)");



// Add value to enum in subscriptions_subscription
$installer->run("ALTER TABLE `subscriptions_subscription` CHANGE COLUMN `status` `status` ENUM('WAIT_MANUAL_ACTIVATION', 'EXPIRED', 'ACTIVE') NOT NULL DEFAULT 'EXPIRED' COMMENT ''");


$installer->endSetup(); 