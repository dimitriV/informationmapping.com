<?php 

$installer = $this;
/* @var $installer Mage_Core_Model_Resource_Setup */

$installer->startSetup();

$installer->run("
ALTER TABLE `" . $installer->getTable('license_keys_renewal') . "` 
	ADD COLUMN `product_sku` varchar(255) AFTER order_no;
");

$installer->endSetup();