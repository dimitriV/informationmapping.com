<?php

$installer = $this;

$installer->startSetup();

// We don't need this columns anymore
$installer->run("ALTER TABLE `subscriptions_renewal` DROP COLUMN `activated` ");


$installer->endSetup(); 