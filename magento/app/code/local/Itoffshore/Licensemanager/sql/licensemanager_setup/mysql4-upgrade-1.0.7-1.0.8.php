<?php

$installer = $this;

$installer->startSetup();

// Add status and start date for renewals (needed for manual activations)
$installer->run("ALTER TABLE `subscriptions_renewal` ADD COLUMN `status` ENUM('ACTIVE', 'WAIT_ON_ACTIVATION') NULL DEFAULT 'ACTIVE'");
$installer->run("ALTER TABLE `subscriptions_renewal` ADD COLUMN `started_on` TIMESTAMP NULL DEFAULT NULL");


$installer->endSetup(); 