<?php

$installer = $this;

$installer->startSetup();

// Add a disabled flag
$installer->run("
ALTER TABLE `subscriptions_renewal`
CHANGE COLUMN `status` `status` ENUM('ACTIVE', 'WAIT_ON_INVOICE', 'WAIT_ON_ACTIVATION_BY_ADMIN', 'WAIT_ON_ACTIVATION_BY_CUSTOMER') NULL DEFAULT 'ACTIVE' COMMENT '' ;
");

$installer->endSetup(); 