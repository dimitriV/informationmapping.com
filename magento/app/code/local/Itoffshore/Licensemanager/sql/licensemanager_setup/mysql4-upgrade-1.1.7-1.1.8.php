<?php

$installer = $this;

$installer->startSetup();

// Add a disabled flag
$installer->run("
ALTER TABLE `subscriptions_subscription`
ADD COLUMN `activation_email` VARCHAR(255) NULL DEFAULT NULL AFTER `disabled`;
");

$installer->endSetup(); 