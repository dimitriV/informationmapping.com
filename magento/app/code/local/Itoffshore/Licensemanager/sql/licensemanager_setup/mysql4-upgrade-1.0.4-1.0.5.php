<?php

$installer = $this;

$installer->startSetup();

$installer->run("ALTER TABLE `license_license` RENAME TO  `subscriptions_subscription`");
$installer->run("ALTER TABLE `license_renewal` RENAME TO  `subscriptions_renewal`");
$installer->run("ALTER TABLE `license_mail` RENAME TO  `subscriptions_mail`");

$installer->endSetup(); 