<?php 

$installer = $this;
/* @var $installer Mage_Core_Model_Resource_Setup */

$installer->startSetup();

$installer->run("
ALTER TABLE `" . $installer->getTable('license_serialnumbers') . "` 
	MODIFY COLUMN `status` enum('ASSIGNED','UNASSIGNED','CANCELLED') NOT NULL default 'UNASSIGNED';
");

$installer->endSetup();