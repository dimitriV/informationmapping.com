<?php

$installer = $this;

$installer->startSetup();

$installer->run("
    ALTER TABLE {$this->getTable('license_keys_renewal')}  MODIFY COLUMN qty int DEFAULT 1;
");

$installer->endSetup(); 