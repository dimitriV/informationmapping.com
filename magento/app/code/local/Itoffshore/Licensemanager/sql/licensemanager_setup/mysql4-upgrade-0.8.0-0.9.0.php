<?php
$installer = $this;
$installer->startSetup();

$table = $this->getTable('sales_flat_quote_item');
$query = 'ALTER TABLE `' . $table . '` ADD COLUMN `renewal_license_ids` VARCHAR(255)';
$connection = Mage::getSingleton('core/resource')->getConnection('core_write');
$connection->query($query);

$installer->endSetup();