<?php

$installer = $this;

$installer->startSetup();

// Add a disabled flag
$installer->run("
ALTER TABLE `license_log`
ADD COLUMN `KeyFound` TINYINT(1) UNSIGNED NOT NULL DEFAULT 0;
");

$installer->endSetup(); 