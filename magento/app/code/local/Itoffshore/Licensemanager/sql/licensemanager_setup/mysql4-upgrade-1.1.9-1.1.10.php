<?php

$installer = $this;

$installer->startSetup();

// Add a disabled flag
$installer->run("
ALTER TABLE `subscriptions_subscription`
ADD COLUMN `last_email_sent` VARCHAR(45) NULL DEFAULT NULL;
");

$installer->endSetup(); 