<?php

$installer = $this;

$installer->startSetup();

// Add a disabled flag
$installer->run("
ALTER TABLE `subscriptions_subscription`
ADD COLUMN `disabled` TINYINT(1) UNSIGNED NULL DEFAULT 0 AFTER `started_on`
");


$installer->endSetup(); 