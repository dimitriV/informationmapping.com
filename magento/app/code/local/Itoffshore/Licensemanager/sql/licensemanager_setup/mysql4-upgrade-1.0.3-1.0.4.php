<?php

$installer = $this;

$installer->startSetup();

$installer->run('
ALTER TABLE `license_key_pool`
RENAME TO `license_pool` ;
');

$installer->endSetup(); 