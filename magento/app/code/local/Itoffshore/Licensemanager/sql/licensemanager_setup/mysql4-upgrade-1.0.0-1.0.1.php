<?php

$installer = $this;

$installer->startSetup();

$installer->run("
    ALTER TABLE {$this->getTable('license_keys_renewal')}  ADD COLUMN renewal_status ENUM('1', '0') NOT NULL DEFAULT '1';
");

$installer->endSetup(); 