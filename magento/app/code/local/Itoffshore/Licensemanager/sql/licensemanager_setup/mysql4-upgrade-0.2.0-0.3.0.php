<?php

$installer = $this;

$installer->startSetup();

$installer->run("
    ALTER TABLE {$this->getTable('license_keys')} CHANGE STATUS licensekey_status ENUM('1', '2', '0') NOT NULL DEFAULT '1';
");

$installer->endSetup(); 