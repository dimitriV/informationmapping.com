<?php

$installer = $this;

$installer->startSetup();


$installer->run('
ALTER TABLE `as_customerinfo`
RENAME TO  `_as_customerinfo` ;
');

$installer->run('
ALTER TABLE `as_lic_files`
RENAME TO  `_as_lic_files` ;
');

$installer->run('
ALTER TABLE `as_license_usage`
RENAME TO  `_as_license_usage` ;
');
$installer->run('
ALTER TABLE `as_links`
RENAME TO  `_as_links` ;
');
$installer->run('
ALTER TABLE `as_locked`
RENAME TO  `_as_locked` ;
');
$installer->run('
ALTER TABLE `as_notice`
RENAME TO  `_as_notice` ;
');
$installer->run('
ALTER TABLE `as_tries`
RENAME TO  `_as_tries` ;
');
$installer->run('
ALTER TABLE `as_unique_key2013`
RENAME TO  `_as_unique_key2013` ;
');
$installer->run('
ALTER TABLE `license_renew_history`
RENAME TO  `_license_renew_history` ;
');
$installer->run('
ALTER TABLE `license_quote_item`
RENAME TO  `_license_quote_item` ;
');
$installer->run("
ALTER TABLE `license_communication`
CHANGE COLUMN `license_id` `license_id` INT(100) UNSIGNED NULL DEFAULT NULL COMMENT '' AFTER `id`,
CHANGE COLUMN `communication_date` `mailed_at` TIMESTAMP NULL DEFAULT NULL COMMENT '' ,
CHANGE COLUMN `communication_type` `_communication_type` ENUM('0','1','2','3') NULL DEFAULT NULL COMMENT '' ,
CHANGE COLUMN `email` `_email` VARCHAR(255) NULL DEFAULT NULL COMMENT '' ,
CHANGE COLUMN `firstname` `_firstname` VARCHAR(100) NULL DEFAULT NULL COMMENT '' , RENAME TO  `license_mail` ;
");
$installer->run('
ALTER TABLE `license_serialnumbers`
ENGINE = InnoDB ;
');
$installer->run('
ALTER TABLE `license_serialnumbers`
DROP INDEX `idx_license_serialnumbers_order_id` ;
');
$installer->run('
ALTER TABLE `license_serialnumbers`
RENAME TO  `license_key_pool` ;
');
$installer->run("
ALTER TABLE `license_key_pool`
CHANGE COLUMN `product_id` `product_id` INT(11) NULL DEFAULT NULL COMMENT '' AFTER `key`,
CHANGE COLUMN `date_assigned` `assigned_on` TIMESTAMP NULL COMMENT '' AFTER `product_id`,
CHANGE COLUMN `serial_number` `key` TEXT NOT NULL COMMENT '' ,
CHANGE COLUMN `customer_id` `_customer_id` INT(11) NULL COMMENT '' ,
CHANGE COLUMN `order_id` `_order_id` INT(11) NULL DEFAULT NULL COMMENT '' ,
CHANGE COLUMN `order_item_id` `_order_item_id` INT(11) NULL COMMENT '' ,
CHANGE COLUMN `date_added` `_date_added` TIMESTAMP NOT NULL DEFAULT CURRENT_TIMESTAMP COMMENT '' ;
");

$installer->run("
ALTER TABLE `license_key_pool`
ADD COLUMN `type` VARCHAR(45) NULL COMMENT '' AFTER `key`;
");

$installer->run("
DELETE FROM `license_key_pool` WHERE `id`='21972';
DELETE FROM `license_key_pool` WHERE `id`='18999';
DELETE FROM `license_key_pool` WHERE `id`='21129';
DELETE FROM `license_key_pool` WHERE `id`='16809';
DELETE FROM `license_key_pool` WHERE `id`='21130';
DELETE FROM `license_key_pool` WHERE `id`='17672';
DELETE FROM `license_key_pool` WHERE `id`='17673';
DELETE FROM `license_key_pool` WHERE `id`='17696';
DELETE FROM `license_key_pool` WHERE `id`='17674';
DELETE FROM `license_key_pool` WHERE `id`='17697';
DELETE FROM `license_key_pool` WHERE `id`='17675';
DELETE FROM `license_key_pool` WHERE `id`='17698';
");

$installer->run("
ALTER TABLE `license_key_pool`
CHANGE COLUMN `key` `key` VARCHAR(45) NOT NULL COMMENT '' ,
ADD UNIQUE INDEX `key_UNIQUE` (`key` ASC)  COMMENT '';

");

$installer->run('
ALTER TABLE `license_keys`
ENGINE = InnoDB ;
');

$installer->run("
ALTER TABLE `license_keys`
CHANGE COLUMN `expiration_date` `expiration_on` TIMESTAMP NULL DEFAULT NULL COMMENT '' AFTER `key`,
CHANGE COLUMN `licensekey_status` `status` ENUM('1','2','0') NOT NULL DEFAULT '1' COMMENT '' AFTER `expiration_on`,
CHANGE COLUMN `license_keyid` `id` INT(10) UNSIGNED NOT NULL AUTO_INCREMENT COMMENT '' ,
CHANGE COLUMN `license_key` `key` TEXT NULL COMMENT '' ,
CHANGE COLUMN `product_sku` `_product_sku` VARCHAR(255) NULL DEFAULT NULL COMMENT '' ,
CHANGE COLUMN `last_renewal_date` `_last_renewal_date` DATE NULL DEFAULT NULL COMMENT '' ,
CHANGE COLUMN `number_of_activations` `_number_of_activations` INT(11) NULL DEFAULT '1' COMMENT '' ,
CHANGE COLUMN `comments` `_comments` TEXT NULL DEFAULT NULL COMMENT '' ,
CHANGE COLUMN `license_id` `_license_id` INT(11) NULL COMMENT '' ;

");

$installer->run('
ALTER TABLE `license_keys`
RENAME TO  `license_license` ;
');

$installer->run('
DELETE n1 FROM license_license n1, license_license n2 WHERE n1.id > n2.id AND n1.`key` = n2.`key`;
');

$installer->run("
ALTER TABLE `license_license`
CHANGE COLUMN `key` `key` VARCHAR(45) NULL COMMENT '' ,
ADD UNIQUE INDEX `key_UNIQUE` (`key` ASC)  COMMENT '';

");

$installer->run("
ALTER TABLE `license_license`
CHANGE COLUMN `status` `status` ENUM('EXPIRED', 'ACTIVE') NOT NULL DEFAULT 'EXPIRED' COMMENT '' ,
ADD COLUMN `order_item_id` INT(10) UNSIGNED NULL COMMENT '' AFTER `status`,
ADD COLUMN `pool_id` INT(10) UNSIGNED NULL COMMENT '' AFTER `order_item_id`,
ADD COLUMN `auto_renew` TINYINT(1) UNSIGNED NULL COMMENT '' AFTER `pool_id`;

");


$installer->run('
ALTER TABLE `license_info`
RENAME TO  `_license_info` ;

');


$installer->run('

ALTER TABLE `license_orders`
RENAME TO  `_license_orders` ;
');


$installer->run('
ALTER TABLE `license_keys_renewal`
RENAME TO  `_license_keys_renewal` ;
');


$installer->run("
CREATE TABLE `license_renewal` (
  `id` INT(11) NOT NULL AUTO_INCREMENT COMMENT '' ,
  `license_id` INT(10) UNSIGNED NULL COMMENT '',
  `key` VARCHAR(45) NULL COMMENT '',
  `order_item_id` INT UNSIGNED NULL COMMENT '',
  `activated` TINYINT(1) UNSIGNED NULL COMMENT '',
  `pool_id` INT(10) UNSIGNED NOT NULL COMMENT '',
  PRIMARY KEY (`id`)  COMMENT '')
ENGINE = InnoDB
DEFAULT CHARACTER SET = latin1;
");

$installer->run("
ALTER TABLE `license_renewal`
ADD INDEX `renewal_fk2_idx` (`pool_id` ASC)  COMMENT '';
ALTER TABLE `license_renewal`
ADD CONSTRAINT `renewal_fk1`
FOREIGN KEY (`license_id`)
REFERENCES `license_license` (`id`),
ADD CONSTRAINT `renewal_fk2`
FOREIGN KEY (`pool_id`)
REFERENCES `license_key_pool` (`id`)
ON DELETE RESTRICT
ON UPDATE CASCADE;
");

$installer->run("
ALTER TABLE `license_license`
ADD INDEX `license_fk1_idx` (`pool_id` ASC)  COMMENT '';
ALTER TABLE `license_license`
ADD CONSTRAINT `license_fk1`
FOREIGN KEY (`pool_id`)
REFERENCES `license_key_pool` (`id`)
ON DELETE RESTRICT
ON UPDATE RESTRICT;
");


$installer->run("
ALTER TABLE `license_mail`
ENGINE = InnoDB ,
DROP INDEX `communication_fk1` ,
ADD INDEX `mail_fk1` (`license_id` ASC)  COMMENT '';
");


$installer->run('
TRUNCATE `license_mail`;
');


$installer->run('
ALTER TABLE `license_mail`
ADD CONSTRAINT `mail_fk1`
FOREIGN KEY (`license_id`)
REFERENCES `license_license` (`id`)
ON DELETE RESTRICT
ON UPDATE RESTRICT;
');


$installer->run("
ALTER TABLE `license_license`
CHANGE COLUMN `expiration_on` `expires_on` TIMESTAMP NULL DEFAULT NULL COMMENT '' ;
");


$installer->run("
UPDATE license_license SET expires_on = '';
");

$installer->run("
ALTER TABLE `license_key_pool`
CHANGE COLUMN `status` `_status` ENUM('ASSIGNED','UNASSIGNED','CANCELLED') NOT NULL DEFAULT 'UNASSIGNED' COMMENT '' AFTER `assigned_on`;

");
$installer->run("
update license_key_pool k
inner join catalog_product_entity p on
k.product_id = p.entity_id
set k.type = CASE
WHEN p.sku like '%renewal%' THEN
'renewal'
ELSE
'license'
END;
");
$installer->run("
INSERT INTO license_renewal (`key`,order_item_id,pool_id)
  select p.`key`,p._order_item_id,p.id
  from license_key_pool p
  where p.`type` like 'renewal' and p._status like 'ASSIGNED';
");
$installer->run("
DELETE FROM `license_key_pool` WHERE `product_id` is null;
");
$installer->run("
ALTER TABLE `license_license`
CHANGE COLUMN `expires_on` `expires_on` DATETIME NULL DEFAULT NULL COMMENT '' ;

")
;$installer->run("
ALTER TABLE `license_log`
CHANGE COLUMN `ExpiresOn` `ExpiresOn` DATETIME NOT NULL COMMENT '' ;

");

$installer->run("
update license_license l
inner join license_log log on
l.`key` = log.serialnumber
set l.expires_on = log.ExpiresOn;

");

$installer->run("
update license_renewal r
inner join license_key_pool p on
r.`key` = p.`key`
set r.order_item_id= p._order_item_id;

")
;$installer->run("
update license_license l
inner join license_key_pool p on
l.`key` = p.`key`
set l.order_item_id= p._order_item_id;

");
;$installer->run("
update license_license l
inner join license_key_pool p on
l.`key` = p.`key`
set l.pool_id= p.id;
");
;$installer->run("
update license_renewal l
inner join license_key_pool p on
l.`key` = p.`key`
set l.pool_id= p.id;
");
$installer->run("
DELETE l.* FROM license_license l
inner join license_renewal r on
l.pool_id=r.pool_id
WHERE l.`key`=r.`key`;
");

$installer->run("
DELETE FROM license_license where pool_id is null ;
");
$installer->run("
ALTER TABLE `license_renewal`
CHANGE COLUMN `key` `serial` VARCHAR(45) NULL DEFAULT NULL COMMENT '' ;

");
$installer->run("
ALTER TABLE `license_license`
CHANGE COLUMN `key` `serial` VARCHAR(45) NULL DEFAULT NULL COMMENT '' ;

");
$installer->run("
ALTER TABLE `license_key_pool`
CHANGE COLUMN `key` `serial` VARCHAR(45) NOT NULL COMMENT '' ;

");

$installer->endSetup();