<?php

$installer = $this;

$installer->startSetup();

// Add a disabled flag
$installer->run("
ALTER TABLE `subscriptions_subscription` ADD COLUMN `last_auto_renew_failed_on` TIMESTAMP NULL DEFAULT NULL;
");

$installer->endSetup(); 