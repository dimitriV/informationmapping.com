<?php 

$installer = $this;
/* @var $installer Mage_Core_Model_Resource_Setup */

$installer->startSetup();

$installer->run("
CREATE TABLE `" . $installer->getTable('license_serialnumbers') . "` (
  `id` int(10) unsigned NOT NULL auto_increment,
  `status` enum('ASSIGNED','UNASSIGNED') NOT NULL default 'UNASSIGNED',
  `serial_number` text NOT NULL,
  `customer_id` int(11) default NULL,
  `product_id` int(11) default NULL,
  `order_id` int(11) default NULL,
  `order_item_id` int(11) default NULL,
  `date_assigned` timestamp NULL default NULL,
  `date_added` timestamp NOT NULL default CURRENT_TIMESTAMP,
  PRIMARY KEY  (`id`)
) ENGINE=MyISAM AUTO_INCREMENT=1 DEFAULT CHARSET=latin1;
");

$installer->endSetup(); 