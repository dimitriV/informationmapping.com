<?php

$installer = $this;

$installer->startSetup();

// Add a timestamp for keeping track of progress in license_log table
$installer->run("
ALTER TABLE `license_log`
ADD COLUMN `ProcessedOn` VARCHAR(45) NULL DEFAULT NULL AFTER `SerialNumber`;
");


$installer->endSetup(); 