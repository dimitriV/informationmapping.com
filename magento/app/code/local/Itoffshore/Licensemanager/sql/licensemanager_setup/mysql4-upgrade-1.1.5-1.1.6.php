<?php

$installer = $this;

$installer->startSetup();

// Add a disabled flag
$installer->run("
ALTER TABLE `subscriptions_subscription`
CHANGE COLUMN `status` `status` ENUM('WAIT_ON_INVOICE','WAIT_MANUAL_ACTIVATION_BY_ADMIN', 'WAIT_MANUAL_ACTIVATION_BY_CUSTOMER', 'EXPIRED', 'ACTIVE') NOT NULL DEFAULT 'EXPIRED' COMMENT ''
");

$installer->endSetup(); 