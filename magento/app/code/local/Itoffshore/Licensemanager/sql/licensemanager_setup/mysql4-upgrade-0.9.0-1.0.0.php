<?php
$installer = $this;
$installer->getConnection()->addColumn(
    $installer->getTable('sales_flat_order'), 'imi_customer_email', 'varchar(100)'
);

$installer->getConnection()->addColumn(
    $installer->getTable('sales_flat_order'), 'imi_customer_firstname', 'varchar(100)'
);
