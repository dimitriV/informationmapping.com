<?php

$installer = $this;

$installer->startSetup();

//Create license_info table
$installer->run("

-- DROP TABLE IF EXISTS {$this->getTable('license_info')};
CREATE TABLE {$this->getTable('license_info')} (
    license_id INT UNSIGNED AUTO_INCREMENT PRIMARY KEY,
    product_type ENUM('perpetual', 'sma','subscription','none') DEFAULT 'none',
    license_type ENUM('single','volume','none') DEFAULT 'none',
    initial_purchase_date DATE,
    expiration_date DATE,
    number_of_renewals INT DEFAULT 0,
    last_license_key TEXT DEFAULT NULL,
    license_status ENUM('1', '2', '0') DEFAULT '1',
    overwrite_communication ENUM('1', '0') NOT NULL DEFAULT '1',
    last_communication_date DATE DEFAULT NULL,
    email VARCHAR(255) DEFAULT NULL,
    firstname VARCHAR(100) DEFAULT NULL
);

");

//Create license_keys table
$installer->run("

-- DROP TABLE IF EXISTS {$this->getTable('license_keys')};
CREATE TABLE {$this->getTable('license_keys')} (
    license_keyid INT UNSIGNED AUTO_INCREMENT PRIMARY KEY,
    license_key TEXT DEFAULT NULL,
    product_sku VARCHAR(255) DEFAULT NULL,
    last_renewal_date DATE DEFAULT NULL,
    expiration_date DATE DEFAULT NULL,
    number_of_activations INT DEFAULT 1,
    comments TEXT,
    STATUS ENUM('1', '2', '0') NOT NULL DEFAULT '1',
    license_id INT DEFAULT NULL
);

");

//Create license_orders table
$installer->run("

-- DROP TABLE IF EXISTS {$this->getTable('license_orders')};
CREATE TABLE {$this->getTable('license_orders')} (
    id INT UNSIGNED AUTO_INCREMENT PRIMARY KEY,
    order_id INT,
    customer_name VARCHAR(255) DEFAULT NULL,
    order_date DATE DEFAULT NULL,
    number_of_activations INT DEFAULT 1,
    license_id INT DEFAULT NULL
);

");

//Create license_communication table
$installer->run("

-- DROP TABLE IF EXISTS {$this->getTable('license_communication')};
CREATE TABLE {$this->getTable('license_communication')} (
    id INT UNSIGNED AUTO_INCREMENT PRIMARY KEY,
    communication_date DATE DEFAULT NULL,
    communication_type ENUM('0', '1', '2','3'),
    email VARCHAR(255) DEFAULT NULL,
    firstname VARCHAR(100) DEFAULT NULL,
    license_id INT(100) DEFAULT NULL
);

");
$installer->endSetup(); 