<?php
require_once MAGENTO_ROOT.DS.'app'.DS.'code'.DS.'core'.DS.'Mage'.DS.'Checkout'.DS.'controllers'.DS.'CartController.php';
class Itoffshore_Licensemanager_CartController extends Mage_Checkout_CartController
{
    

    /**
     * Add product to shopping cart action, include license key validation
     */
    public function addAction()
    {
        $cart               = $this->_getCart();
        $params             = $this->getRequest()->getParams();
        $licensekey         = (isset($params['licensekey'])) ? trim($params['licensekey']) : '';
                      
        try {
            if (isset($params['qty'])) {
                $filter = new Zend_Filter_LocalizedToNormalized(
                    array('locale' => Mage::app()->getLocale()->getLocaleCode())
                );
                $params['qty'] = $filter->filter($params['qty']);
            }

            $licensekeys_string = null;

            $product = $this->_initProduct();
                        
            /* Validate license key for upgrade products */
            if($product->getIsLicenseMandatory() == 1) {
                $backUrl = $this->_getRefererUrl();
                
                if(trim($licensekey) == '') {
                    $message = Mage::helper('licensemanager')->__('Please enter your current license key(s).');
                    $this->_getSession()->addError($message);
                    $this->_redirectUrl($backUrl);
                    return;
                }
                
                //cut out a whole bunch of nepalese sauce right here
                $license_type = $product->getAttributeText('license_type');
                
                $licensekey_count   = count(explode('<br />', nl2br($licensekey)));
                $licensekeys_string = implode(',', explode('<br />', nl2br($licensekey))); 
                $licensekeys_array  = explode('<br />', nl2br($licensekey));       
                $licensekeys_clean  = array_map('trim', $licensekeys_array);
                
                if(strcasecmp($license_type, 'Single License') == 0) {
                    if($params['qty'] != $licensekey_count) {
                        $message = Mage::helper('licensemanager')->__('Number of license keys and quantity do not match.');
                        $this->_getSession()->addError($message);
                        $this->_redirectUrl($backUrl);
                        return;
                    }
                }
                elseif(strcasecmp($license_type, 'Volume License') == 0) {
                    if($licensekey_count != 1) {
                        $message = Mage::helper('licensemanager')->__('Invalid number of license key for volume product.');
                        $this->_getSession()->addError($message);
                        $this->_redirectUrl($backUrl);
                        return;
                    }
                }
                else {
                    $message = Mage::helper('licensemanager')->__('License type is not defined for upgrade product.');
                    $this->_getSession()->addError($message);
                    $this->_redirectUrl($backUrl);
                    return;
                }
                
                $isLicensekeysValid = Mage::getModel('licensemanager/license')->validateLicenseKeys($licensekeys_clean, $params['qty'], $license_type);
                if($isLicensekeysValid == false) {
                    $message = Mage::helper('licensemanager')->__('One or more license keys are invalid.');
                    $this->_getSession()->addError($message);
                    $this->_redirectUrl($backUrl); 
                    return;
                }
            }
            /* Validate license key for upgrade products end */
            
            $related = $this->getRequest()->getParam('related_product');

            /**
             * Check product availability
             */
            if (!$product) {
                $this->_goBack();
                return;
            }

            $cart->addProduct($product, $params, $licensekeys_string);
            if (!empty($related)) {
                $cart->addProductsByIds(explode(',', $related));
            }

            $cart->save();

            $this->_getSession()->setCartWasUpdated(true);

            /**
             * @todo remove wishlist observer processAddToCart
             */
            Mage::dispatchEvent('checkout_cart_add_product_complete',
                array('product' => $product, 'request' => $this->getRequest(), 'response' => $this->getResponse())
            );

            if (!$this->_getSession()->getNoCartRedirect(true)) {
                if (!$cart->getQuote()->getHasError()){
                    $message = $this->__('%s was added to your shopping cart.', Mage::helper('core')->escapeHtml($product->getName()));
                    $this->_getSession()->addSuccess($message);
                }
                $this->_goBack();
            }
        } catch (Mage_Core_Exception $e) {
            
            if ($this->_getSession()->getUseNotice(true)) {
                $this->_getSession()->addNotice(Mage::helper('core')->escapeHtml($e->getMessage()));
            } else {
                $messages = array_unique(explode("\n", $e->getMessage()));
                foreach ($messages as $message) {
                    $this->_getSession()->addError(Mage::helper('core')->escapeHtml($message));
                }
            }

            $url = $this->_getSession()->getRedirectUrl(true);
            if ($url) {
                $this->getResponse()->setRedirect($url);
            } else {
                $this->_redirectReferer(Mage::helper('checkout/cart')->getCartUrl());
            }
        } catch (Exception $e) {
            $this->_getSession()->addException($e, $this->__('Cannot add the item to shopping cart.'));
            Mage::logException($e);
            $this->_goBack();
        }
    }
    
    /**
     * Update customer's shopping cart
     */
    protected function _updateShoppingCart()
    {
        try {
            $cartData = $this->getRequest()->getParam('cart');
            
            $renewal_ids = $this->getRequest()->getPost('renewal');
            
            $renewal_data = array();
            if(is_array($renewal_ids) && sizeof($renewal_ids)) {
                foreach($renewal_ids as $renewal) {
                    $renewal_value      = explode(":", $renewal);
                    $key                = $renewal_value[0];
                    $values[]           = $renewal_value[1];
                    $renewal_data[$key] = $values;
                }
            }
            
            if (is_array($cartData)) {
                $filter = new Zend_Filter_LocalizedToNormalized(
                    array('locale' => Mage::app()->getLocale()->getLocaleCode())
                );
                
                foreach ($cartData as $index => $data) {
                    if(array_key_exists($index, $renewal_data) && sizeof($renewal_ids)) {
                        $cartData[$index]['qty'] = count($renewal_data[$index]);
                        $renewalData[$index]     = implode(",", $renewal_data[$index]);
                    }
                    elseif (isset($data['qty'])) {
                        $cartData[$index]['qty'] = $filter->filter(trim($data['qty']));
                    }
                }
                
                $cart = $this->_getCart();
                if (! $cart->getCustomerSession()->getCustomer()->getId() && $cart->getQuote()->getCustomerId()) {
                    $cart->getQuote()->setCustomerId(null);
                }

                $cartData = $cart->suggestItemsQty($cartData);
                $cart->updateItems($cartData)
                    ->save();
                //Update renewal license ids on cart update
                foreach($renewalData as $itemId => $renewal_licenses) {
                    $this->_updateRenewalLicenseIds($itemId, $renewal_licenses);
                }
            }
            $this->_getSession()->setCartWasUpdated(true);
        } catch (Mage_Core_Exception $e) {
            $this->_getSession()->addError(Mage::helper('core')->escapeHtml($e->getMessage()));
        } catch (Exception $e) {
            $this->_getSession()->addException($e, $this->__('Cannot update shopping cart.'));
            Mage::logException($e);
        }
    }
    
    private function _updateRenewalLicenseIds($itemId, $licenses)
    {
        if(!$itemId) {
            return;
        }
        $resource = Mage::getSingleton('core/resource');
        $writeConnection = $resource->getConnection('core_write');
        $quoteItemTable = $resource->getTableName('sales_flat_quote_item');
        
        $query = "UPDATE ". $quoteItemTable ." SET renewal_license_ids = '". mysql_escape_string($licenses) ."' WHERE item_id = ". $itemId;
        $writeConnection->query($query);
        
        Mage::getSingleton('checkout/session')->setRenewalIds($licenses);
    }
}