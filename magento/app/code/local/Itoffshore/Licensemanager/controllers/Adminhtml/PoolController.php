<?php
class Itoffshore_Licensemanager_Adminhtml_PoolController extends Mage_Adminhtml_Controller_Action
{
	public function indexAction()
	{
		$this->loadLayout();

        $this->_title('Manage License Keys');
		$this->_setActiveMenu('licensemanager');

        $this->_addContent(
            $this->getLayout()->createBlock('licensemanager/adminhtml_license_pool')
        );
		$this->renderLayout();
	}
	
	public function gridAction()
    {
        $this->loadLayout();
        $this->getResponse()->setBody(
            $this->getLayout()->createBlock('licensemanager/adminhtml_license_pool_grid')->toHtml()
        );
    }
	
	public function editAction()
	{
		$this->loadLayout();
		$this->_setActiveMenu('licensemanager');
		
		$modelId = $this->getRequest()->getParam('id');
		
		$model = Mage::getModel('licensemanager/license_pool')->load($modelId);
		
		
		if(!$model->getId()){
			Mage::getSingleton('adminhtml/session')->addError(Mage::helper('licensemanager')->__('This serial number could not be found'));
			$this->_redirect('*/*/');
		} else {
		    Mage::register('licensemanager_license_pool',$model);
		}
        
        $this->_addContent(
            $this->getLayout()->createBlock('licensemanager/edit', 'licensekeys.edit')
        );
        
        $this->renderLayout();	
	}


	public function massDeleteAction()
	{
		$serialNumbers = $this->getRequest()->getPost('license_pool', array());

		$deleteCount = 0;
		foreach($serialNumbers as $serialNumber){
			$serialModel = Mage::getModel('licensemanager/license_pool')->load((int) $serialNumber);
			$serialModel->delete();
			$deleteCount++;
		}
		Mage::getSingleton('adminhtml/session')->addSuccess(Mage::helper('licensemanager')->__('%d license key(s) successfully deleted', $deleteCount));
		$this->_redirect('*/*/');
	}

    public function newAction()
    {
        $this->loadLayout();
        $this->_setActiveMenu('licensemanager');

        $this->_title('Add License Keys');

        $this->_addContent(
            $this->getLayout()->createBlock('licensemanager/adminhtml_license_pool_add', 'licensemanager.addlicensekeys')
        );

        $this->renderLayout();
    }

    public function saveAction()
    {
        try {
            $dataFormat = $this->getRequest()->getPost('data_format');

            $serialNumberPost = $this->getRequest()->getParam('serial');
            $productId = $this->getRequest()->getParam('product_id');

            switch($dataFormat){

                case 'comma_delimited':
                    $serialNumbers = explode(',',$serialNumberPost);
                    if(count($serialNumbers) <= 0)
                        throw new Exception($this->__('You must enter a list of comma delimited serial numbers'));
                    break;

                case 'linebreak_delimited':
                    $serialNumbers = explode("\n", $serialNumberPost);
                    if(count($serialNumbers) <= 0)
                        throw new Exception($this->__('You must enter a list of serial numbers seperated by new line'));
                    break;

                default:
                    Mage::throwException("You must select one of the data format options");
            }

            $added_serials = 0;
            foreach($serialNumbers as $serial)
            {
                $serial = trim($serial);
                if(strlen($serial) <= 0)
                    continue;
                $serial_model = Mage::getModel('licensemanager/license_pool');
                $serial_model->setSerial($serial);
                $serial_model->setProductId($productId);
                $serial_model->save();
                $added_serials++;
            }

            Mage::getSingleton('adminhtml/session')->addSuccess($this->__($added_serials . " license key(s) added successfully!"));

        }
        catch (Exception $e)
        {
            Mage::getSingleton('adminhtml/session')->setData('licensemanager_licensekeys_data', $this->getRequest()->getParams());
            Mage::getSingleton('adminhtml/session')->addError($e->getMessage());
        }
        $this->_redirect('*/*');

    }
}