<?php

class Itoffshore_Licensemanager_DebugController extends Mage_Core_Controller_Front_Action {

    public function cronAction() {
        /* @var $cron Itoffshore_Licensemanager_Model_Cron */
        $cron = Mage::getSingleton('licensemanager/cron');
        $cron->processLicenseLog();
    }

    public function fixdbAction() {
        $this->_fixOrderItemIdsForBundles();
    }


    /**
     * In subscriptions_subscription, we link to sales_flat_order_item, but always to the bundle item, not the underlying item, which is what we really need.
     */
    protected function _fixOrderItemIdsForBundles() {

        $subscriptionsFixed = 0;

        /* @var $subscriptions Storefront_Subscriptions_Model_Resource_Subscription_Collection */
        $subscriptions = Mage::getResourceModel('subscriptions/subscription_collection');

        $subscriptions->joinOrderItem();

        $subscriptions->addFieldToFilter('product_type', 'bundle');
        $subscriptions->setPageSize(500);
        $subscriptions->setOrder('id', 'desc');

        foreach ($subscriptions as $subscription) {
            /* @var $subscription Storefront_Subscriptions_Model_Subscription */
            if ($subscription->getSerial()) {
                $poolCol = Mage::getResourceModel('licensemanager/license_pool_collection');
                $poolCol->addFieldToFilter('serial', $subscription->getSerial());

                if ($poolCol->count() > 0) {
                    $poolItem = $poolCol->getFirstItem();

                    if ($poolItem->getProductId()) {
                        $productId = $poolItem->getProductId();

                        if ($productId) {
                            //Load the current bundle order item
                            $orderItem = $subscription->getOrderItem();

                            // This line does not work!
                            $itemsInBundle = $orderItem->getChildrenItems();

                            $itemsInBundle = Mage::getResourceModel('sales/order_item_collection')->addFieldToFilter('parent_item_id', $orderItem->getId());

                            foreach ($itemsInBundle as $itemInBundle) {
                                if ($itemInBundle->getProductId() == $productId) {
                                    $newOrderItemId = $itemInBundle->getId();

                                    $subscription->setOrderItemId($newOrderItemId);
                                    $subscription->save();

                                    $subscriptionsFixed++;
                                    break;
                                }
                            }


                        }
                    }

                }
            }

        }

        die('Fixed ' . $subscriptionsFixed . ' subscriptions. Keep running this script to reach 0.');
    }

//    public function testAction(){
//        $observer = Mage::getSingleton('licensemanager/observer');
//        $observer->getPoolItem(389);
//    }

    public function guesttocustomerAction() {
        /* @var $cron Itoffshore_Licensemanager_Model_Cron */
        $cron = Mage::getSingleton('licensemanager/cron');

        $orderIds = array(
            9559,
            9558
        );

        foreach ($orderIds as $orderId) {
            $order = Mage::getModel('sales/order')->load($orderId);
            $order->setCustomerId(null);

            $email = $order->getCustomerEmail();
            $emailParts = explode('_',$email);
            if(count($emailParts) > 1){
                array_shift($emailParts);
            }
            $newEmail = date('H.i.s') .'_' . implode('_',$emailParts);

            $order->setCustomerEmail($newEmail);
            $order->save();
        }


        $cron->createAccountForGuest();
    }
}

