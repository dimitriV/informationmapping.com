<?php

class Itoffshore_Licensemanager_Block_Adminhtml_License_Pool_Grid_Column_Productname extends Mage_Adminhtml_Block_Widget_Grid_Column_Renderer_Abstract {

    public function render(Varien_Object $row) {
        $html = '';
        if ($row instanceof Itoffshore_Licensemanager_Model_License_Pool) {

            $pool = $row;

            $productId = $pool->getProductId();
            $productName = Mage::getModel('catalog/product')->load($productId)->getName();

            $html = '<a href="'.$this->getUrl('adminhtml/catalog_product/edit', array('id' => $productId)).'">'.$productName.'</a>';

        }
        return $html;
    }
}