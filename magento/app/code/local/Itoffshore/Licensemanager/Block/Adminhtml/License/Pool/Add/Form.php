<?php

class Itoffshore_Licensemanager_Block_Adminhtml_License_Pool_Add_Form extends Mage_Adminhtml_Block_Widget_Form
{
    public function __construct()
    {
        parent::__construct();
        $this->setId('licensemanager_add_form');
        $this->setTitle(Mage::helper('licensemanager')->__('Add New License Keys'));
    }
    
    public function _prepareForm()
    {
        $form = new Varien_Data_Form(array('id' => 'edit_form', 'action' => $this->getUrl('*/*/save'), 'method' => 'post'));
        
        $form->setHtmlIdPrefix('block_');
        
        $fieldset = $form->addFieldset('base_fieldset', array('legend' => 'Add New License Keys', 'class' => 'fieldset-wide'));
        
        $fieldset->addField('serial', 'editor', array(
            'name'      => 'serial',
            'label'     => 'License Keys',
            'title'     => 'License Keys',
            'required'  => true,
            'class'     => 'required-entry'
        ));
        
        
        $fieldset->addField('product_id', 'select', array(
            'name'      => 'product_id',
            'label'     => 'Associate with this product',
            'label'     => 'Associate with this product',
            'required'  => true,
            'options'   => $this->_getProductList()
        ));
        
        $fieldset->addField('data_format', 'select', array(
            'name'      => 'data_format',
            'label'     => 'Data Format',
            'label'     => 'Data Format',
            'required'  => true,
            'options'   => array(
                'comma_delimited'   => "Comma Delimited",
                'linebreak_delimited' => "Line-break Delimited"
            )
        ));

        if($values = Mage::getSingleton('adminhtml/session')->getData('licensemanager_licensekeys_form_data', true)) {
            $form->setValues($values);
        }
        
        $form->setUseContainer(true);
        $this->setForm($form);
        return $this;
    }
    
    protected function _getProductList()
    {
        $products = Mage::getModel('catalog/product')
            ->getCollection()
            ->addAttributeToSelect('*')
            ->addAttributeToFilter('type_id', 'virtual')
            ->addAttributeToFilter('status', '1')
            ->addAttributeToSort('id', 'ASC')
            ->load();
            
        $product_list = array();
        foreach($products as $product)
        {
            $product_list[$product->getId()] = 'Product '.$product->getId().' : '.$product->getName();
        }
        return $product_list;   
    }
    
}