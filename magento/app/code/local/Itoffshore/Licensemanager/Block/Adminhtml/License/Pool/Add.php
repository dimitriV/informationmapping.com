<?php

class Itoffshore_Licensemanager_Block_Adminhtml_License_Pool_Add extends Mage_Adminhtml_Block_Widget_Form_Container
{

    public function __construct()
    {
        parent::__construct();
        
        $this->_updateButton('save', 'label', $this->__('Save License Key'));
    }
    
    
    public function _prepareLayout()
    {
        
        $this->_objectId = 'block_id';
        $this->_controller = 'licensemanager';
        
        $this->_updateButton('save', 'label', $this->__('Add License Keys'));

        $this->setChild('form', $this->getLayout()->createBlock('licensemanager/adminhtml_license_pool_add_form'));
        
        $this->setChild('save_button',
            $this->getLayout()->createBlock('adminhtml/widget_button')
                ->setData(
                    array(
                        'label'   => Mage::helper('licensemanager')->__('Add License Keys'),
                        'onclick' => 'editForm.submit();',
                        'class' => 'save'
                    )
                )
        );
      $this->_removeButton('delete');
//        parent::_prepareLayout();
    }
    
    public function getHeaderText()
    {
        return Mage::helper('licensemanager')->__('Add License Keys');
    }
}