<?php
class Itoffshore_Licensemanager_Block_Adminhtml_Licensemanager_Renderer_Lastlicensekey extends Mage_Adminhtml_Block_Widget_Grid_Column_Renderer_Abstract
{
    public function render(Varien_Object $row)
    {
        $id =  $row->getId();
        $licensekey = Mage::getModel('licensemanager/keys')->getCollection()
                      ->addFieldToFilter('license_id', $id)
                      ->setOrder('id', 'desc')
                      ->getFirstItem();
        return $licensekey->getLicenseKey();
    }
}
?>