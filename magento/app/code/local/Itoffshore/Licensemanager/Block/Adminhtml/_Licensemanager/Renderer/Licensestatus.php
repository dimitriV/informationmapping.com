<?php
class Itoffshore_Licensemanager_Block_Adminhtml_Licensemanager_Renderer_Licensestatus extends Mage_Adminhtml_Block_Widget_Grid_Column_Renderer_Abstract
{
    public function render(Varien_Object $row)
    {
        $id =  $row->getId();
        $licensekey = Mage::getModel('licensemanager/keys')->getCollection()
                      ->addFieldToSelect(array('serial', 'licensekey_status'))
                      ->addFieldToFilter('license_id', $id)
                      ->setOrder('id', 'desc')
                      ->getFirstItem();
        if(!$licensekey->getLicenseKey()) {
            return;     
        }
        else if($licensekey->getLicensekeyStatus() == 1) {
            return Mage::helper('licensemanager')->__('Active');
        }
        else {
            return Mage::helper('licensemanager')->__('Expired');
        }
    }
}
?>