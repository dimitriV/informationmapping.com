<?php
class Itoffshore_Licensemanager_Block_Adminhtml_Licensemanager_Renderer_Expirationdate extends Mage_Adminhtml_Block_Widget_Grid_Column_Renderer_Abstract
{
    public function render(Varien_Object $row)
    {
        $id =  $row->getId();
        $licensekey = Mage::getModel('licensemanager/keys')->getCollection()
                      ->addFieldToSelect(array('expiration_date', 'licensekey_status'))
                      ->addFieldToFilter('license_id', $id)
                      ->setOrder('id', 'desc')
                      ->getFirstItem();
        if(!$licensekey->getExpirationDate()) {
            return;     
        }
        else {
            $expiration_date = $licensekey->getExpirationDate();
            return date('M d, Y', strtotime($expiration_date));
        }
    }
}
?>