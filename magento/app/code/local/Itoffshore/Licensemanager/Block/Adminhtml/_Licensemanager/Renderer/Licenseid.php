<?php
class Itoffshore_Licensemanager_Block_Adminhtml_Licensemanager_Renderer_Licenseid extends Mage_Adminhtml_Block_Widget_Grid_Column_Renderer_Abstract
{
    public function render(Varien_Object $row)
    {
        $id =  $row->getId();
        $licensekeys  = Mage::getModel('licensemanager/license')->load($id);
        $serialnumber = $licensekeys->getSerialNumber();
        
        if(!$serialnumber || $licensekeys->getStatus() === 'UNASSIGNED' || $licensekeys->getStatus() === 'CANCELLED') {
            return;
        }
        
        $license = Mage::getModel('licensemanager/license')->getCollection()
                      ->addFieldToSelect('id')
                      ->addFieldToFilter('serial', $serialnumber)
                      ->setOrder('id', 'desc')
                      ->getFirstItem();
        return $license->getLicenseId();
    }
}
?>