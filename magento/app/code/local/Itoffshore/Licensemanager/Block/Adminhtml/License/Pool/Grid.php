<?php

class Itoffshore_Licensemanager_Block_Adminhtml_License_Pool_Grid extends Mage_Adminhtml_Block_Widget_Grid {

    public function __construct() {
        parent::__construct();
        $this->setId('id');
        $this->setDefaultSort('id');
        $this->setDefaultDir('desc');
        $this->setSaveParametersInSession(true);
        $this->setUseAjax(true);
        $this->setRowClickCallback(false);
    }

    public function _prepareCollection() {
        /* @var $collection Itoffshore_Licensemanager_Model_Mysql4_License_Pool_Collection */
        $collection = Mage::getModel('licensemanager/license_pool')->getCollection();

        $collection->joinSubscriptions();


        // Filter out the UNLINKED and DELETED items
        //	$collection->addFieldToFilter('status',
        //		array(
        //			CLS_Serialproduct_Model_Serial::STATUS_UNASSIGNED,
        //			CLS_Serialproduct_Model_Serial::STATUS_ASSIGNED,
        //			CLS_Serialproduct_Model_Serial::STATUS_CANCELLED
        //		)
        //	);


        $this->setCollection($collection);

        return parent::_prepareCollection();
    }

    public function _prepareColumns() {
        $this->addColumn('id', array(
                'header' => $this->__('ID'),
                'width' => '100',
                'index' => 'id',
                'type' => 'number'
            ));

        $this->addColumn('serial', array(
                'header' => $this->__('License Key'),
                'index' => 'serial'
            ));

        $this->addColumn('product_id', array(
                'header' => $this->__('Product ID'),
                'index' => 'product_id',
                'type' => 'number'
            ));

        $this->addColumn('product_name', array(
            'header' => $this->__('Product Name'),
            'index' => 'product_name',
            'width' => '300px',
            'sortable'=> false,
            'filter' => false,
            'renderer' => 'licensemanager/adminhtml_license_pool_grid_column_productname'
        ));

        $this->addColumn('subscription_id', array(
                'header' => $this->__('Linked to subscription'),
                'index' => 'subscription_id',
                'width' => '170px',
                'type' => 'number'
            ));

        $this->addColumn('renewal_id', array(
                'header' => $this->__('Linked to renewal'),
                'index' => 'renewal_id',
                'width' => '170px',
                'type' => 'number'
            ));

//
//		$this->addColumn('assigned_on',
//			array(
//				'header'	=>	$this->__('Date Assigned'),
//				'index'		=>	'assigned_on'	,
//         	   'type' => 'datetime'
//			)
//		);
        return parent::_prepareColumns();
    }

    public function _prepareMassaction() {
        $this->setMassactionIdField('id');
        $this->getMassactionBlock()->setFormFieldName('license_pool');

        $this->getMassactionBlock()->addItem('delete', array(
            'label' => $this->__('Delete'),
            'url' => $this->getUrl('*/*/massDelete'),
            'confirm' => $this->__('Are you sure you want to delete all of the selected serial numbers?  (this action cannot be undone)')
        ));

        return $this;
    }

    public function getGridUrl() {
        return $this->getUrl('*/*/grid', array('_current' => true));
    }

    public function getRowUrl($row) {
        return false;
    }
}