<?php 
class Itoffshore_Licensemanager_Block_Adminhtml_License_Pool extends Mage_Adminhtml_Block_Template
{
    public function __construct()
    {
        parent::__construct();
        $this->setTemplate('licensemanager/license/pool.phtml');


    }

    protected function _prepareLayout()
    {
        $this->setChild('grid', $this->getLayout()->createBlock('licensemanager/adminhtml_license_pool_grid', 'license.pool.grid'));
        return parent::_prepareLayout();
    }

    public function getAddNewButtonHtml()
    {
        return $this->getChildHtml('add_new_button');
    }

    public function getGridHtml()
    {
        return $this->getChildHtml('grid');
    }

    public function getLicensekeysHtml(Itoffshore_Licensemanager_Model_Licensekeys $license_product)
    {
    	return "<p><a href='http://www.informationmapping.com/downloads/FSPro2013.zip'>Download FS Pro 2013</a></p><span class='serial-number'>Your FS Pro Key: " . $license_product->getSerialNumber();
    }
}