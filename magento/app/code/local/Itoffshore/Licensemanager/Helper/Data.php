<?php

class Itoffshore_Licensemanager_Helper_Data extends Mage_Core_Helper_Abstract {
    public function getCurrentLicenseKeysByQuoteId($quoteId) {
        $resource = Mage::getSingleton('core/resource');
        $readConnection = $resource->getConnection('core_read');
        $quote_itemTable = $resource->getTableName('sales_flat_quote_item');
        $query = "SELECT current_licensekey FROM " . $quote_itemTable . " WHERE quote_id = ? AND current_licensekey !=''";
        $r = $readConnection->fetchOne($query, $quoteId);

        return $r;
    }

    public function getAvailableProductSkus() {
        // Get a list of all product skus for which license keys are used
        $poolResource = Mage::getResourceModel('licensemanager/license_pool');

        $availableProductSkus = $poolResource->getAvailableProductSkus();

        return $availableProductSkus;
    }

    /**
     * Get an array of serials from an order
     * @param Mage_Sales_Model_Order $order
     */
    public function getSerialNumbersFromOrder(Mage_Sales_Model_Order $order) {
        $orderItemIds = array();
        $items = $order->getItemsCollection();

        foreach ($items as $item) {
            $orderItemIds[] = $item->getId();
        }

        $serials = array();

        // Subscriptions
        /* @var $subscriptionsCol Storefront_Subscriptions_Model_Resource_Subscription_Collection */
        $subscriptionsCol = Mage::getResourceModel('subscriptions/subscription_collection');
        $subscriptionsCol->addFieldToFilter('order_item_id', array('in' => $orderItemIds));

        // Renewals
        /* @var $renewalsCol Storefront_Subscriptions_Model_Resource_Renewal_Collection */
        $renewalsCol = Mage::getResourceModel('subscriptions/renewal_collection');
        $renewalsCol->addFieldToFilter('order_item_id', array('in' => $orderItemIds));

        foreach ($subscriptionsCol as $subscription) {
            if ($subscription->getSerial()) {
                $serials[] = $subscription->getSerial();
            }
        }

        foreach ($renewalsCol as $renewal) {
            if ($renewal->getSerial()) {
                $serials[] = $renewal->getSerial();
            }
        }

        return $serials;
    }

   
}