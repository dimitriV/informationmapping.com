<?php

class Itoffshore_Agreement_Helper_Data extends Mage_Checkout_Helper_Data
{
    

    public function getRequiredAgreementIds()
    {
        if (is_null($this->_agreements)) {
            if (!Mage::getStoreConfigFlag('checkout/options/enable_agreements')) {
                $this->_agreements = array();
            } else {
                /*$this->_agreements = Mage::getModel('checkout/agreement')->getCollection()
                    ->addStoreFilter(Mage::app()->getStore()->getId())
                    ->addFieldToFilter('is_active', 1)
                    ->getAllIds();*/
                    
                $agreements = Mage::getModel('checkout/agreement')->getCollection()
                    //->addStoreFilter(Mage::app()->getStore()->getId())
                    ->addFieldToFilter('is_active', 1);
                    
                /*************Tweaking the Query ***********/
                            
                $product_ids = Mage::getModel('checkout/cart')->getProductIds();                
                  
                $agreements->getSelect()
                            ->join( array('a' => 'agreement'), 'main_table.agreement_id = a.checkout_agreement_id', array('a.product_id'))
                            ->join( array('cpe' => 'catalog_product_entity'), 'a.product_id = cpe.entity_id', array('cpe.entity_id'))             
                            ->where("1=1
                                    AND a.status = '1'
                            	    AND a.product_id IN (".implode(',', $product_ids).")")
                            ->group('a.checkout_agreement_id');
                                    
                $db_agreement_ids = array();
                foreach($agreements as $agreement){
                    $data = $agreement->getData();
                    $db_agreement_ids[] = $data['agreement_id'];
                }
                $this->_agreements = $db_agreement_ids;                
            }
        }
        
        #Mage::log(print_r($this->_agreements, true));
        return $this->_agreements;
    }
}