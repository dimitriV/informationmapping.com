<?php

class Itoffshore_Agreement_Block_Adminhtml_Agreement_Grid extends Mage_Adminhtml_Block_Widget_Grid
{
  public function __construct()
  {
      parent::__construct();
      $this->setId('agreementGrid');
      $this->setDefaultSort('agreement_id');
      $this->setDefaultDir('DESC');
      $this->setSaveParametersInSession(true);
  }

  protected function _prepareCollection()
  {
      $collection = Mage::getModel('agreement/agreement')->getCollection();
      $collection->getSelect()
                 ->join( array('cpe' => 'catalog_product_entity'), 'main_table.product_id = cpe.entity_id', array('cpe.sku'))
                 ->join( array('cpev' => 'catalog_product_entity_varchar'), 'cpe.entity_id = cpev.entity_id', array('cpev.value'))                 
                 ->join( array('ca' => 'checkout_agreement'), 'main_table.checkout_agreement_id = ca.agreement_id', array('ca.name'))
                  ->where("1=1
                    	AND cpev.entity_type_id = '4'
                    	AND cpev.attribute_id = '56'");  
      $this->setCollection($collection);
      return parent::_prepareCollection();
  }

  protected function _prepareColumns()
  {
      $this->addColumn('agreement_id', array(
          'header'    => Mage::helper('agreement')->__('ID'),
          'align'     =>'right',
          'width'     => '50px',
          'index'     => 'agreement_id',
      ));

      $this->addColumn('product_id', array(
          'header'    => Mage::helper('agreement')->__('Product'),
          'align'     =>'left',
          'index'     => 'value',
      ));
      
      $this->addColumn('checkout_agreement_id', array(
          'header'    => Mage::helper('agreement')->__('Assigned Checkout Agreement'),
          'align'     =>'left',
          'index'     => 'name',
      ));

	  /*
      $this->addColumn('content', array(
			'header'    => Mage::helper('agreement')->__('Item Content'),
			'width'     => '150px',
			'index'     => 'content',
      ));
	  */

      $this->addColumn('status', array(
          'header'    => Mage::helper('agreement')->__('Status'),
          'align'     => 'left',
          'width'     => '80px',
          'index'     => 'status',
          'type'      => 'options',
          'options'   => array(
              1 => 'Enabled',
              2 => 'Disabled',
          ),
      ));
	  
        $this->addColumn('action',
            array(
                'header'    =>  Mage::helper('agreement')->__('Action'),
                'width'     => '100',
                'type'      => 'action',
                'getter'    => 'getId',
                'actions'   => array(
                    array(
                        'caption'   => Mage::helper('agreement')->__('Edit'),
                        'url'       => array('base'=> '*/*/edit'),
                        'field'     => 'id'
                    )
                ),
                'filter'    => false,
                'sortable'  => false,
                'index'     => 'stores',
                'is_system' => true,
        ));
		
		#$this->addExportType('*/*/exportCsv', Mage::helper('agreement')->__('CSV'));
		#$this->addExportType('*/*/exportXml', Mage::helper('agreement')->__('XML'));
	  
      return parent::_prepareColumns();
  }

    protected function _prepareMassaction()
    {
        $this->setMassactionIdField('agreement_id');
        $this->getMassactionBlock()->setFormFieldName('agreement');

        $this->getMassactionBlock()->addItem('delete', array(
             'label'    => Mage::helper('agreement')->__('Delete'),
             'url'      => $this->getUrl('*/*/massDelete'),
             'confirm'  => Mage::helper('agreement')->__('Are you sure?')
        ));

        $statuses = Mage::getSingleton('agreement/status')->getOptionArray();

        array_unshift($statuses, array('label'=>'', 'value'=>''));
        $this->getMassactionBlock()->addItem('status', array(
             'label'=> Mage::helper('agreement')->__('Change status'),
             'url'  => $this->getUrl('*/*/massStatus', array('_current'=>true)),
             'additional' => array(
                    'visibility' => array(
                         'name' => 'status',
                         'type' => 'select',
                         'class' => 'required-entry',
                         'label' => Mage::helper('agreement')->__('Status'),
                         'values' => $statuses
                     )
             )
        ));
        return $this;
    }

  public function getRowUrl($row)
  {
      return $this->getUrl('*/*/edit', array('id' => $row->getId()));
  }

}