<?php

class Itoffshore_Agreement_Block_Adminhtml_Agreement_Edit_Tab_Form extends Mage_Adminhtml_Block_Widget_Form
{
  protected function _prepareForm()
  {
      $form = new Varien_Data_Form();
      $this->setForm($form);
      $fieldset = $form->addFieldset('agreement_form', array('legend'=>Mage::helper('agreement')->__('Item information')));
     
      //get product list according to edit it
      if($this->getRequest()->getParam('id') > 0){
        $product_options =  $this->_getProductList(false); 
        $disabled        = true;        
      }else{
        $product_options =  $this->_getProductList(true);
        $disabled        = false;
      }
      
      $fieldset->addField('product_id', 'select', array(
            'name'      => 'product_id',
            'label'     => 'Select Product',            
            'required'  => true,
            'disabled'  => $disabled,
            'options'   => $product_options
      ));
      
      $fieldset->addField('checkout_agreement_id', 'select', array(
            'name'      => 'checkout_agreement_id',
            'label'     => 'Select Checkout Agreement',            
            'required'  => true,
            'options'   => $this->_getCheckoutAgreementList()
        ));  
		
      $fieldset->addField('status', 'select', array(
          'label'     => Mage::helper('agreement')->__('Status'),
          'name'      => 'status',
          'values'    => array(
              array(
                  'value'     => 1,
                  'label'     => Mage::helper('agreement')->__('Enabled'),
              ),

              array(
                  'value'     => 2,
                  'label'     => Mage::helper('agreement')->__('Disabled'),
              ),
          ),
      ));
     
     
      if ( Mage::getSingleton('adminhtml/session')->getAgreementData() )
      {
          $form->setValues(Mage::getSingleton('adminhtml/session')->getAgreementData());
          Mage::getSingleton('adminhtml/session')->setAgreementData(null);
      } elseif ( Mage::registry('agreement_data') ) {
          $form->setValues(Mage::registry('agreement_data')->getData());
      }
      return parent::_prepareForm();
  }
  
  protected function _getProductList($exclude_assigned = true)
    {
        $products = Mage::getModel('catalog/product')
            ->getCollection()
            ->addAttributeToSelect('*')
            /*->addAttributeToFilter('is_serial_product')*/
            ->load();
            
        $product_list = array();
        $assigned_product_list = $this->_getAssignedCheckoutAgreementProductList();
        foreach($products as $product)
        {
            if($exclude_assigned == true){
                if(!in_array($product->getId(), $assigned_product_list)){
                    $product_list[$product->getId()] = $product->getName(); 
                }
            }else{
                $product_list[$product->getId()] = $product->getName(); 
            }
            
        }
        
        return $product_list;   
    }
    
    protected function _getCheckoutAgreementList(){
        $connection = Mage::getSingleton('core/resource')->getConnection('core_read');
        $sql        = "SELECT * FROM checkout_agreement";
        $rows       = $connection->fetchAll($sql);
        
        $options = array();
        
        if(!empty($rows)){
            foreach ($rows as $row) {
                $options[$row['agreement_id']] = $row['name'];
            }
        }
    
        return $options;
    }
    
    protected function _getAssignedCheckoutAgreementProductList(){
        $connection = Mage::getSingleton('core/resource')->getConnection('core_read');
        $sql        = "SELECT agreement_id, product_id FROM agreement";
        $rows       = $connection->fetchAll($sql);
        
        $options = array();
        
        if(!empty($rows)){
            foreach ($rows as $row) {
                $options[] = $row['product_id'];
            }
        }
    
        return $options;
    }
}