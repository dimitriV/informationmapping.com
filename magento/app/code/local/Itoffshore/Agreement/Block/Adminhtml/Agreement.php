<?php
class Itoffshore_Agreement_Block_Adminhtml_Agreement extends Mage_Adminhtml_Block_Widget_Grid_Container
{
  public function __construct()
  {
    $this->_controller = 'adminhtml_agreement';
    $this->_blockGroup = 'agreement';
    $this->_headerText = Mage::helper('agreement')->__('Product Checkout Agreement Manager');
    $this->_addButtonLabel = Mage::helper('agreement')->__('Add Product Agreement');
    parent::__construct();
  }
}