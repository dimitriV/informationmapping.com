<?php

class Itoffshore_Agreement_Block_Adminhtml_Agreement_Edit extends Mage_Adminhtml_Block_Widget_Form_Container
{
    public function __construct()
    {
        parent::__construct();
                 
        $this->_objectId = 'id';
        $this->_blockGroup = 'agreement';
        $this->_controller = 'adminhtml_agreement';
        
        $this->_updateButton('save', 'label', Mage::helper('agreement')->__('Save Item'));
        $this->_updateButton('delete', 'label', Mage::helper('agreement')->__('Delete Item'));
		
        $this->_addButton('saveandcontinue', array(
            'label'     => Mage::helper('adminhtml')->__('Save And Continue Edit'),
            'onclick'   => 'saveAndContinueEdit()',
            'class'     => 'save',
        ), -100);

        $this->_formScripts[] = "
            function toggleEditor() {
                if (tinyMCE.getInstanceById('agreement_content') == null) {
                    tinyMCE.execCommand('mceAddControl', false, 'agreement_content');
                } else {
                    tinyMCE.execCommand('mceRemoveControl', false, 'agreement_content');
                }
            }

            function saveAndContinueEdit(){
                editForm.submit($('edit_form').action+'back/edit/');
            }
        ";
    }

    public function getHeaderText()
    {
        if( Mage::registry('agreement_data') && Mage::registry('agreement_data')->getId() ) {
            return Mage::helper('agreement')->__("Edit Item", $this->htmlEscape(Mage::registry('agreement_data')->getTitle()));
        } else {
            return Mage::helper('agreement')->__('Add Item');
        }
    }
}