<?php

class Itoffshore_Agreement_Block_Agreement extends Mage_Checkout_Block_Agreements
{
    public function getAgreements()
    {
        if (!$this->hasAgreements()) {
            if (!Mage::getStoreConfigFlag('checkout/options/enable_agreements')) {
                $agreements = array();
            } else {
                $agreements = Mage::getModel('checkout/agreement')->getCollection()
                    //->addStoreFilter(Mage::app()->getStore()->getId())
                    ->addFieldToFilter('is_active', 1);
                    
                /*************Tweaking the Query ***********/
                //get product ids in a cart
                /*$items      = Mage::getSingleton('checkout/session')->getQuote()->getAllItems();                        
                $product_ids = array();                
                foreach($items as $item){            
                    $product_ids[] = (int)$item->getProductId();        
                }*/
                $product_ids = Mage::getModel('checkout/cart')->getProductIds();
                #print_r($product_ids);
                  
                $agreements->getSelect()
                            ->join( array('a' => 'agreement'), 'main_table.agreement_id = a.checkout_agreement_id', array('a.product_id'))
                            ->join( array('cpe' => 'catalog_product_entity'), 'a.product_id = cpe.entity_id', array('cpe.entity_id'))             
                            ->where("1=1
                                    AND a.status = '1'
                            	    AND a.product_id IN (".implode(',', $product_ids).")")
                            ->group('a.checkout_agreement_id');
                /*************Tweaking the Query ***********/
            }
            $this->setAgreements($agreements);
        }
        
        return $this->getData('agreements');
    }
       
}