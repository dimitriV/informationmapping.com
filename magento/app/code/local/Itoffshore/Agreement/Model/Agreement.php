<?php

class Itoffshore_Agreement_Model_Agreement extends Mage_Core_Model_Abstract
{
    public function _construct()
    {
        parent::_construct();
        $this->_init('agreement/agreement');
    }
}