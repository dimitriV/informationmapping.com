<?php

class Itoffshore_Agreement_Model_Mysql4_Agreement extends Mage_Core_Model_Mysql4_Abstract
{
    public function _construct()
    {    
        // Note that the agreement_id refers to the key field in your database table.
        $this->_init('agreement/agreement', 'agreement_id');
    }
}