<?php

$installer = $this;

$installer->startSetup();

$installer->run("

-- DROP TABLE IF EXISTS {$this->getTable('partnershipping')};
CREATE TABLE {$this->getTable('partnershipping')} (                                  
                   `partnershipping_id` INT(11) UNSIGNED NOT NULL AUTO_INCREMENT,  
                   `partner_id` INT(11) NOT NULL,                                  
                   `custom_shipping_ids` VARCHAR(255) NOT NULL DEFAULT '',         
                   `status` SMALLINT(6) NOT NULL DEFAULT '0',                      
                   `created_time` DATETIME DEFAULT NULL,                           
                   `update_time` DATETIME DEFAULT NULL,                            
                   PRIMARY KEY  (`partnershipping_id`)                             
                 ) ENGINE=INNODB DEFAULT CHARSET=utf8 

    ");

$installer->endSetup(); 