<?php
class Itoffshore_Partnershipping_Block_Partnershipping extends Mage_Core_Block_Template
{
	public function _prepareLayout()
    {
		return parent::_prepareLayout();
    }
    
     public function getPartnershipping()     
     { 
        if (!$this->hasData('partnershipping')) {
            $this->setData('partnershipping', Mage::registry('partnershipping'));
        }
        return $this->getData('partnershipping');
        
    }
}