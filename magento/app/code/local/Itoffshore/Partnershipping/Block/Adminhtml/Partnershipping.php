<?php
class Itoffshore_Partnershipping_Block_Adminhtml_Partnershipping extends Mage_Adminhtml_Block_Widget_Grid_Container
{
  public function __construct()
  {
    $this->_controller = 'adminhtml_partnershipping';
    $this->_blockGroup = 'partnershipping';
    $this->_headerText = Mage::helper('partnershipping')->__('Item Manager');
    $this->_addButtonLabel = Mage::helper('partnershipping')->__('Associate Method');
    parent::__construct();
  }
}