<?php

class Itoffshore_Partnershipping_Block_Adminhtml_Partnershipping_Edit_Tab_Form extends Mage_Adminhtml_Block_Widget_Form
{
  protected function _prepareForm()
  {
      $form = new Varien_Data_Form();
      $this->setForm($form);
      $fieldset = $form->addFieldset('partnershipping_form', array('legend'=>Mage::helper('partnershipping')->__('Item information')));
     
      //get product list according to edit it
      if($this->getRequest()->getParam('id') > 0){
        $partner_options =  $this->_getPartnersList(false); 
        $disabled        = true;        
      }else{
        $partner_options =  $this->_getPartnersList(true);
        $disabled        = false;
      }        
     
      $fieldset->addField('partner_id', 'select', array(
          'label'     => Mage::helper('partnershipping')->__('IM Partner'),
          'class'     => 'required-entry',
          'required'  => true,
          'name'      => 'partner_id',
          'disabled'  => $disabled,
          'options'   => $partner_options
      ));
            
      $fieldset->addField('custom_shipping_ids', 'multiselect', array(
          'label'     => Mage::helper('partnershipping')->__('Custom Shipping Methods'),
          'class'     => 'required-entry',
          'required'  => true,
          'name'      => 'custom_shipping_ids',          
          'values'   => $this->_getShippingMethodsList(),          
      ));
      
		
      $fieldset->addField('status', 'select', array(
          'label'     => Mage::helper('partnershipping')->__('Status'),
          'name'      => 'status',
          'values'    => array(
              array(
                  'value'     => 1,
                  'label'     => Mage::helper('partnershipping')->__('Enabled'),
              ),

              array(
                  'value'     => 2,
                  'label'     => Mage::helper('partnershipping')->__('Disabled'),
              ),
          ),
      ));     
      
     
      if ( Mage::getSingleton('adminhtml/session')->getPartnershippingData() )
      {
          $form->setValues(Mage::getSingleton('adminhtml/session')->getPartnershippingData());
          Mage::getSingleton('adminhtml/session')->setPartnershippingData(null);
      } elseif ( Mage::registry('partnershipping_data') ) {
          $form->setValues(Mage::registry('partnershipping_data')->getData());
      }
      return parent::_prepareForm();
  }
  
  private function _getShippingMethodsList(){    
    $connection = Mage::getSingleton('core/resource')->getConnection('core_read');
    $sql        = "SELECT * FROM customshipping";
    $rows       = $connection->fetchAll($sql);
    
    $options = array();
        
    if(!empty($rows)){
        foreach ($rows as $row) {
            $options[] = array(
                'value' => $row['customshipping_id'],
                'label' => $row['title']
            );            
        }
    }

    return $options;
  }
  
   protected function _getPartnersList($exclude_assigned = true){
        $connection = Mage::getSingleton('core/resource')->getConnection('core_read');
        $sql        = "SELECT * FROM partner";
        $rows       = $connection->fetchAll($sql);
        
        $options                = array();
        $assigned_partner_list  = $this->_getAssignedPartnerList();
        if(!empty($rows)){
            foreach ($rows as $row) {
                if($exclude_assigned === true){                    
                    if(!in_array($row['partner_id'], $assigned_partner_list)){
                        $options[$row['partner_id']] = $row['name'];
                    }    
                }else{            
                    $options[$row['partner_id']] = $row['name'];
                }
            }
        }
    
        return $options;
    }
    
    protected function _getAssignedPartnerList(){
        $connection = Mage::getSingleton('core/resource')->getConnection('core_read');
        $sql        = "SELECT partner_id FROM partnershipping";
        $rows       = $connection->fetchAll($sql);
        
        $options = array();
        
        if(!empty($rows)){
            foreach ($rows as $row) {
                $options[] = $row['partner_id'];
            }
        }
    
        return $options;
    }
  
  
}