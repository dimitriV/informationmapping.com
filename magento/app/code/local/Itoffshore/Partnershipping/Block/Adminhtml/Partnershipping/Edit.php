<?php

class Itoffshore_Partnershipping_Block_Adminhtml_Partnershipping_Edit extends Mage_Adminhtml_Block_Widget_Form_Container
{
    public function __construct()
    {
        parent::__construct();
                 
        $this->_objectId = 'id';
        $this->_blockGroup = 'partnershipping';
        $this->_controller = 'adminhtml_partnershipping';
        
        $this->_updateButton('save', 'label', Mage::helper('partnershipping')->__('Save Item'));
        $this->_updateButton('delete', 'label', Mage::helper('partnershipping')->__('Delete Item'));
		
        $this->_addButton('saveandcontinue', array(
            'label'     => Mage::helper('adminhtml')->__('Save And Continue Edit'),
            'onclick'   => 'saveAndContinueEdit()',
            'class'     => 'save',
        ), -100);

        $this->_formScripts[] = "
            function toggleEditor() {
                if (tinyMCE.getInstanceById('partnershipping_content') == null) {
                    tinyMCE.execCommand('mceAddControl', false, 'partnershipping_content');
                } else {
                    tinyMCE.execCommand('mceRemoveControl', false, 'partnershipping_content');
                }
            }

            function saveAndContinueEdit(){
                editForm.submit($('edit_form').action+'back/edit/');
            }
        ";
    }

    public function getHeaderText()
    {
        if( Mage::registry('partnershipping_data') && Mage::registry('partnershipping_data')->getId() ) {
            return Mage::helper('partnershipping')->__("Edit Item '%s'", $this->htmlEscape(Mage::registry('partnershipping_data')->getTitle()));
        } else {
            return Mage::helper('partnershipping')->__('Add Item');
        }
    }
}