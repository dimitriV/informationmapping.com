<?php

class Itoffshore_Partnershipping_Block_Adminhtml_Partnershipping_Grid extends Mage_Adminhtml_Block_Widget_Grid
{
  public function __construct()
  {
      parent::__construct();
      $this->setId('partnershippingGrid');
      $this->setDefaultSort('partnershipping_id');
      $this->setDefaultDir('ASC');
      $this->setSaveParametersInSession(true);
  }

  protected function _prepareCollection()
  {
      $collection = Mage::getModel('partnershipping/partnershipping')->getCollection();
      $collection->getSelect()->join(array('p' => 'partner'), 'main_table.partner_id=p.partner_id', array('p.name'));
      #echo $collection->getSelect()->__toString();
      $this->setCollection($collection);
      return parent::_prepareCollection();
  }

  protected function _prepareColumns()
  {
      $this->addColumn('partnershipping_id', array(
          'header'    => Mage::helper('partnershipping')->__('ID'),
          'align'     =>'right',
          'width'     => '50px',
          'index'     => 'partnershipping_id',
      ));

      $this->addColumn('partner_id', array(
          'header'    => Mage::helper('partnershipping')->__('IM Partner'),
          'align'     =>'left',
          'index'     => 'name',
      ));

	  $this->addColumn('custom_shipping_ids', array(
          'header'    => Mage::helper('partnershipping')->__('Associated Shipping Methods'),
          'align'     =>'left',
          'index'     => 'custom_shipping_ids',
      ));

      $this->addColumn('status', array(
          'header'    => Mage::helper('partnershipping')->__('Status'),
          'align'     => 'left',
          'width'     => '80px',
          'index'     => 'status',
          'type'      => 'options',
          'options'   => array(
              1 => 'Enabled',
              2 => 'Disabled',
          ),
      ));
	  
        $this->addColumn('action',
            array(
                'header'    =>  Mage::helper('partnershipping')->__('Action'),
                'width'     => '100',
                'type'      => 'action',
                'getter'    => 'getId',
                'actions'   => array(
                    array(
                        'caption'   => Mage::helper('partnershipping')->__('Edit'),
                        'url'       => array('base'=> '*/*/edit'),
                        'field'     => 'id'
                    )
                ),
                'filter'    => false,
                'sortable'  => false,
                'index'     => 'stores',
                'is_system' => true,
        ));
		
		$this->addExportType('*/*/exportCsv', Mage::helper('partnershipping')->__('CSV'));
		$this->addExportType('*/*/exportXml', Mage::helper('partnershipping')->__('XML'));
	  
      return parent::_prepareColumns();
  }

    protected function _prepareMassaction()
    {
        $this->setMassactionIdField('partnershipping_id');
        $this->getMassactionBlock()->setFormFieldName('partnershipping');

        $this->getMassactionBlock()->addItem('delete', array(
             'label'    => Mage::helper('partnershipping')->__('Delete'),
             'url'      => $this->getUrl('*/*/massDelete'),
             'confirm'  => Mage::helper('partnershipping')->__('Are you sure?')
        ));

        $statuses = Mage::getSingleton('partnershipping/status')->getOptionArray();

        array_unshift($statuses, array('label'=>'', 'value'=>''));
        $this->getMassactionBlock()->addItem('status', array(
             'label'=> Mage::helper('partnershipping')->__('Change status'),
             'url'  => $this->getUrl('*/*/massStatus', array('_current'=>true)),
             'additional' => array(
                    'visibility' => array(
                         'name' => 'status',
                         'type' => 'select',
                         'class' => 'required-entry',
                         'label' => Mage::helper('partnershipping')->__('Status'),
                         'values' => $statuses
                     )
             )
        ));
        return $this;
    }

  public function getRowUrl($row)
  {
      return $this->getUrl('*/*/edit', array('id' => $row->getId()));
  }

}