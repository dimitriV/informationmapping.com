<?php

class Itoffshore_Partnershipping_Model_Mysql4_Partnershipping_Collection extends Mage_Core_Model_Mysql4_Collection_Abstract
{
    public function _construct()
    {
        parent::_construct();
        $this->_init('partnershipping/partnershipping');
    }
}