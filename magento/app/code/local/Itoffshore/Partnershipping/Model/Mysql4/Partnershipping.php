<?php

class Itoffshore_Partnershipping_Model_Mysql4_Partnershipping extends Mage_Core_Model_Mysql4_Abstract
{
    public function _construct()
    {    
        // Note that the partnershipping_id refers to the key field in your database table.
        $this->_init('partnershipping/partnershipping', 'partnershipping_id');
    }
}