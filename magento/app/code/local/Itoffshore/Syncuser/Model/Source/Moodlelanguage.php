<?php
class Itoffshore_Syncuser_Model_Source_Moodlelanguage extends Mage_Eav_Model_Entity_Attribute_Source_Abstract {
    
    public function getAllOptions() {
         if ($this->_options === null) {
            $this->_options = array();
            $this->_options[] = array(
                    'value' => 'en',
                    'label' => 'English'
            );
            $this->_options[] = array(
                    'value' => 'nl',
                    'label' => 'Dutch'
            );
            $this->_options[] = array(
                    'value' => 'fr',
                    'label'  => 'French'
            );
            $this->_options[] = array(
                    'value' => 'it',
                    'label'  => 'Italian'
            );
            $this->_options[] = array(
                      'value' => 'es',
                      'label'  => 'Spanish'
              );
            $this->_options[] = array(
                      'value' => 'dk',
                      'label'  => 'Danish'
              );
            $this->_options[] = array(
                      'value' => 'de',
                      'label'  => 'German'
            );
        }
        
        return $this->_options;        
    }
}   