<?php
    class Itoffshore_Syncuser_Model_Observer extends Mage_Core_Model_Abstract
    {
        /*
         * Method fired on the event <address_save_after>
         *
         * @access public
         * @param Varien_Event_Observer $observer
         * @return Itoffshore_Syncuser_Model_Observer
         */
        public function customerAddressSaveAfter($observer)
        {
            $customerAddress    = $observer->getEvent()->getCustomerAddress();
            
            $customerId         = Mage::getSingleton('customer/session')->getCustomer()->getId();
            $customerEmail      = Mage::getSingleton('customer/session')->getCustomer()->getEmail();
            $customerLanguage   = Mage::getSingleton('customer/session')->getCustomer()->getMoodlelanguage();
            $customerFirstname  = $customerAddress->getFirstname();
            $customerLastname   = $customerAddress->getLastname();
            $customerCountry    = $customerAddress->getCountryId();
            $customerCity       = $customerAddress->getCity();
            
            //Call moodle webservice method to create/update user
            $userinfo = array(
                                'idnumber'  => $customerId,
                                'email'     => $customerEmail,
                                'firstname' => $customerFirstname,
                                'lastname'  => $customerLastname,
                                'lang'      => $customerLanguage,
                                'country'   => $customerCountry,
                                'city'      => $customerCity
                             );
            
            $this->createUpdateMoodleUser($userinfo);
            
            return $this;
        }
        
        /*
         * Method fired on the event <adminhtml_customer_save_after>
         *
         * @access public
         * @param Varien_Event_Observer $observer
         * @return Itoffshore_Syncuser_Model_Observer
         */
        public function adminhtmlCustomerSaveAfter($observer)
        {
            $customer           = $observer->getEvent()->getCustomer();
            $customerId         = $customer->getId(); 
            $customerEmail      = $customer->getEmail();
            $customerFirstname  = $customer->getFirstname();
            $customerLastname   = $customer->getLastname();
            $customerLanguage   = $customer->getMoodlelanguage();
            
            $customerCountry = $customerCity = '';
            foreach($customer->getAddresses() as $address) {
                $customerCity       = $address->getCity();
                $customerCountry    = $address->getCountryId();
                break;
            }
            
            //Call moodle webservice method to create/update user
            $userinfo = array(
                                'idnumber'  => $customerId,
                                'email'     => $customerEmail,
                                'firstname' => $customerFirstname,
                                'lastname'  => $customerLastname,
                                'lang'      => $customerLanguage,
                                'country'   => $customerCountry,
                                'city'      => $customerCity
                             );
            $this->createUpdateMoodleUser($userinfo);
            
            return $this;
        }
        
        /*
         * Method fired on the event <customer_save_after>
         *
         * @access public
         * @param Varien_Event_Observer $observer
         * @return Itoffshore_Syncuser_Model_Observer
         */
        public function customerSaveAfter($observer)
        {
            $customer = $observer->getEvent()->getCustomer();
            $customerId         = $customer->getId();
            $customerEmail      = $customer->getEmail();
            $customerFirstname  = $customer->getFirstname();
            $customerLastname   = $customer->getLastname();
            $customerLanguage   = $customer->getMoodlelanguage();
            
            $customerCountry = $customerCity = '';
            foreach($customer->getAddresses() as $address) {
                $customerCity       = $address->getCity();
                $customerCountry    = $address->getCountryId();
                break;
            }
            
            //Call moodle webservice method to create/update user
            $userinfo = array(
                                'idnumber'  => $customerId,
                                'email'     => $customerEmail,
                                'firstname' => $customerFirstname,
                                'lastname'  => $customerLastname,
                                'lang'      => $customerLanguage,
                                'country'   => $customerCountry,
                                'city'      => $customerCity
                             );
            $this->createUpdateMoodleUser($userinfo);
            
            return $this;
        }
        
        /*
         * Method fired on the event <customer_delete_after>
         *
         * @access public
         * @param Varien_Event_Observer $observer
         * @return Itoffshore_Syncuser_Model_Observer
         */
        public function customerDeleteAfter($observer)
        {
            /**
             * @todo Perform delete customer from moodle database
            */
            
            return $this;
        }
        
        /*
         * Method to create update user on moodle
         *
         * @access private
         * @param array userinfo
         * @return Itoffshore_Syncuser_Model_Observer
         */
        private function createUpdateMoodleUser($userinfo)
        {
            
            $cli = new SoapClient('http://www.informationmapping.com/academy/local/service/soapserver.php?wsdl');    
            try {
                if($userinfo['idnumber'] > 0) {
                    //Mage::log(print_r($userinfo, true), null, 'userinfo.log');
                    $response = $cli->UpdateUser((object) $userinfo); 
                }         
            }
            catch(exception $e) {
                Mage::log($e->getMessage() .' for user: '. print_r($userinfo, true), null, 'moodle_syncuser_error.log'); 
            }
        }
    }
?>
