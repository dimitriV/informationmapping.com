<?php
$installer = $this;
$installer->startSetup();

$setup = new Mage_Eav_Model_Entity_Setup('core_setup'); 

/*
// remove language from orders & quote (staging only
$tablequote = $this->getTable('sales/quote_address');
$installer->run("ALTER TABLE  $tablequote DROP  `language`");
$tablequote = $this->getTable('sales/order_address');
$installer->run("ALTER TABLE  $tablequote DROP  `language`");

// remove old language attribute from address
$entityType         = Mage::getModel('eav/config')->getEntityType('customer_address');
$entityTypeId       = $entityType->getEntityTypeId();
$setup->removeAttribute($entityTypeId, 'language');
*/
//$setup->removeAttribute('customer', 'moodlelanguage');

$attribute_options   = array(
    'type' => 'varchar',
    'input' => 'select',
    'label' => 'Language',
    'global' => 1,
    'visible' => 1,
    'required' => 1,
    'user_defined' => 1,
    'default' => 'en',
    'visible_on_front' => 1,
    'source' => 'syncuser/source_moodlelanguage',
);
$setup->addAttribute('customer', 'moodlelanguage', $attribute_options);


if (version_compare(Mage::getVersion(), '1.6.0', '<='))
{
      $customer = Mage::getModel('customer/customer');
      $attrSetId = $customer->getResource()->getEntityType()->getDefaultAttributeSetId();
      $setup->addAttributeToSet('customer', $attrSetId, 'General', 'moodlelanguage');
}
if (version_compare(Mage::getVersion(), '1.4.2', '>='))
{
    Mage::getSingleton('eav/config')
    ->getAttribute('customer', 'moodlelanguage')
    ->setData('used_in_forms', array('adminhtml_customer','customer_account_create','customer_account_edit','checkout_register'))
    ->save();
}

$installer->endSetup();