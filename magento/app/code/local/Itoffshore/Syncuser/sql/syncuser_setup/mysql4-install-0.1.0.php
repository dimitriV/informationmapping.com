<?php
/**
* Reference URL: http://www.excellencemagentoblog.com/magento-adding-custom-field-to-customer-address
*/
$installer = $this;
 
$installer->startSetup();

$setup              = new Mage_Eav_Model_Entity_Setup('core_setup'); 
$entityType         = Mage::getModel('eav/config')->getEntityType('customer_address');
$entityTypeId       = $entityType->getEntityTypeId();
$attrbute_options   = array(
                        'type' => 'varchar',
                        'input' => 'text',
                        'label' => 'Language',
                        'global' => 1,
                        'visible' => 1,
                        'required' => 0,
                        'user_defined' => 1,
                        'visible_on_front' => 1
                      );
$setup->addAttribute($entityTypeId, 'language', $attrbute_options);

Mage::getSingleton('eav/config')
    ->getAttribute('customer_address', 'language')
    ->setData('used_in_forms', array('customer_register_address','customer_address_edit','adminhtml_customer_address'))
    ->save();
    
$installer->endSetup(); 