<?php
/**
 * Product list toolbar
 *
 * @category    Itoffshore
 * @package     Itoffshore_Override
 * @author      Itoffshore Core Team <narendraraj.ojha@itoffshorenepal.com>
 */
class Itoffshore_Override_Block_Product_List_Toolbar extends Mage_Catalog_Block_Product_List_Toolbar
{
    //This function is not found in core file!
    public function isLastPage()
    {
        return $this->getCurrentPage() == $this->getLastPageNum();
    }    
}
