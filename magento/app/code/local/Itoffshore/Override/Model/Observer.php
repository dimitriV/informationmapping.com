<?php
    class Itoffshore_Override_Model_Observer
    {
        public function updateQuoteItemPrice(Varien_Event_Observer $observer)
        {
            $quoteItem = $observer->getEvent()->getItem();
            
            if ($quoteItem->getItemId() && $quoteItem->getParentItemId()) {
                $data = $quoteItem->getQty().",".$quoteItem->getPrice().",".$quoteItem->getBaseRowTotal();
                //Mage::log($data, null, 'quoteinfo.log');  
            }
            else {
                $data = $quoteItem->getQty().",".$quoteItem->getPrice().",".$quoteItem->getBaseRowTotal();
                //Mage::log($data, null, 'quoteinfo_else.log'); 
            }
            
            return $this;
        }
    }
?>
