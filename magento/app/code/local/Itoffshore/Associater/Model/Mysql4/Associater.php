<?php

class Itoffshore_Associater_Model_Mysql4_Associater extends Mage_Core_Model_Mysql4_Abstract
{
    public function _construct()
    {    
        // Note that the associater_id refers to the key field in your database table.
        $this->_init('associater/associater', 'associater_id');
    }
}