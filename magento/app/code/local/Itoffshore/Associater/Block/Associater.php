<?php
class Itoffshore_Associater_Block_Associater extends Mage_Core_Block_Template
{
	public function _prepareLayout()
    {
		return parent::_prepareLayout();
    }
    
     public function getAssociater()     
     { 
        if (!$this->hasData('associater')) {
            $this->setData('associater', Mage::registry('associater'));
        }
        return $this->getData('associater');
        
    }
}