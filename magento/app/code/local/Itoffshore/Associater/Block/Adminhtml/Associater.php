<?php
class Itoffshore_Associater_Block_Adminhtml_Associater extends Mage_Adminhtml_Block_Widget_Grid_Container
{
  public function __construct()
  {
    $this->_controller = 'adminhtml_associater';
    $this->_blockGroup = 'associater';
    $this->_headerText = Mage::helper('associater')->__('Associated Partners');
    $this->_addButtonLabel = Mage::helper('associater')->__('Associate');    
    parent::__construct();
  }
}