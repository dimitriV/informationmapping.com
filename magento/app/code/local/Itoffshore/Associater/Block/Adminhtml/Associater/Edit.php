<?php

class Itoffshore_Associater_Block_Adminhtml_Associater_Edit extends Mage_Adminhtml_Block_Widget_Form_Container
{
    public function __construct()
    {
        parent::__construct();
                 
        $this->_objectId = 'id';
        $this->_blockGroup = 'associater';
        $this->_controller = 'adminhtml_associater';
        
        $this->_updateButton('save', 'label', Mage::helper('associater')->__('Save Item'));
        $this->_updateButton('delete', 'label', Mage::helper('associater')->__('Delete Item'));
		
        $this->_addButton('saveandcontinue', array(
            'label'     => Mage::helper('adminhtml')->__('Save And Continue Edit'),
            'onclick'   => 'saveAndContinueEdit()',
            'class'     => 'save',
        ), -100);

        $this->_formScripts[] = "
            function toggleEditor() {
                if (tinyMCE.getInstanceById('associater_content') == null) {
                    tinyMCE.execCommand('mceAddControl', false, 'associater_content');
                } else {
                    tinyMCE.execCommand('mceRemoveControl', false, 'associater_content');
                }
            }

            function saveAndContinueEdit(){
                editForm.submit($('edit_form').action+'back/edit/');
            }
        ";
    }

    public function getHeaderText()
    {
        if( Mage::registry('associater_data') && Mage::registry('associater_data')->getId() ) {
            return Mage::helper('associater')->__("Edit Item");
        } else {
            return Mage::helper('associater')->__('Add Item');
        }
    }
}