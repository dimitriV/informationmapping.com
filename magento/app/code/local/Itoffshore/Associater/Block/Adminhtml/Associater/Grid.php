<?php

class Itoffshore_Associater_Block_Adminhtml_Associater_Grid extends Mage_Adminhtml_Block_Widget_Grid
{
  public function __construct()
  {
      parent::__construct();
      $this->setId('associaterGrid');
      $this->setDefaultSort('associater_id');
      $this->setDefaultDir('ASC');
      $this->setSaveParametersInSession(true);
  }

  protected function _prepareCollection()
  {
      $collection = Mage::getModel('associater/associater')->getCollection();
      $collection->getSelect()
                ->join(array('p1' => 'partner'), 'main_table.partner_id = p1.partner_id', array('p1.name AS partner_name'))
                ->join(array('p2' => 'printers'), 'main_table.printers_id = p2.printers_id', array('p2.name AS printer_name'));
      $this->setCollection($collection);
      return parent::_prepareCollection();
  }

  protected function _prepareColumns()
  {
      $this->addColumn('associater_id', array(
          'header'    => Mage::helper('associater')->__('ID'),
          'align'     =>'right',
          'width'     => '50px',
          'index'     => 'associater_id',
      ));

      /*$this->addColumn('title', array(
          'header'    => Mage::helper('associater')->__('Title'),
          'align'     =>'left',
          'index'     => 'title',
      ));*/
      
      $this->addColumn('partner_id', array(
          'header'    => Mage::helper('associater')->__('Information Mapping Partner'),
          'align'     =>'left', 
          //'filter_index'  => 'sku',         
          'index'     => 'partner_name',
      ));
      
      $this->addColumn('printers_id', array(
          'header'    => Mage::helper('associater')->__('Printing House'),
          'align'     =>'left',
          'index'     => 'printer_name',
      ));   

	  /*
      $this->addColumn('content', array(
			'header'    => Mage::helper('associater')->__('Item Content'),
			'width'     => '150px',
			'index'     => 'content',
      ));
	  */

      $this->addColumn('status', array(
          'header'    => Mage::helper('associater')->__('Status'),
          'align'     => 'left',
          'width'     => '80px',
          'index'     => 'status',
          'type'      => 'options',
          'options'   => array(
              1 => 'Enabled',
              2 => 'Disabled',
          ),
      ));
	  
        $this->addColumn('action',
            array(
                'header'    =>  Mage::helper('associater')->__('Action'),
                'width'     => '100',
                'type'      => 'action',
                'getter'    => 'getId',
                'actions'   => array(
                    array(
                        'caption'   => Mage::helper('associater')->__('Edit'),
                        'url'       => array('base'=> '*/*/edit'),
                        'field'     => 'id'
                    )
                ),
                'filter'    => false,
                'sortable'  => false,
                'index'     => 'stores',
                'is_system' => true,
        ));
		
		$this->addExportType('*/*/exportCsv', Mage::helper('associater')->__('CSV'));
		$this->addExportType('*/*/exportXml', Mage::helper('associater')->__('XML'));
	  
      return parent::_prepareColumns();
  }

    protected function _prepareMassaction()
    {
        $this->setMassactionIdField('associater_id');
        $this->getMassactionBlock()->setFormFieldName('associater');

        $this->getMassactionBlock()->addItem('delete', array(
             'label'    => Mage::helper('associater')->__('Delete'),
             'url'      => $this->getUrl('*/*/massDelete'),
             'confirm'  => Mage::helper('associater')->__('Are you sure?')
        ));

        $statuses = Mage::getSingleton('associater/status')->getOptionArray();

        array_unshift($statuses, array('label'=>'', 'value'=>''));
        $this->getMassactionBlock()->addItem('status', array(
             'label'=> Mage::helper('associater')->__('Change status'),
             'url'  => $this->getUrl('*/*/massStatus', array('_current'=>true)),
             'additional' => array(
                    'visibility' => array(
                         'name' => 'status',
                         'type' => 'select',
                         'class' => 'required-entry',
                         'label' => Mage::helper('associater')->__('Status'),
                         'values' => $statuses
                     )
             )
        ));
        return $this;
    }

  public function getRowUrl($row)
  {
      return $this->getUrl('*/*/edit', array('id' => $row->getId()));
  }

}