<?php

class Itoffshore_Associater_Block_Adminhtml_Associater_Edit_Tab_Form extends Mage_Adminhtml_Block_Widget_Form
{
  protected function _prepareForm()
  {
      $form = new Varien_Data_Form();
      $this->setForm($form);
      $fieldset = $form->addFieldset('associater_form', array('legend'=>Mage::helper('associater')->__('Item information')));
     
      /*$fieldset->addField('title', 'text', array(
          'label'     => Mage::helper('associater')->__('Title'),
          'class'     => 'required-entry',
          'required'  => true,
          'name'      => 'title',
      ));*/

      //get product list according to edit it
      if($this->getRequest()->getParam('id') > 0){
        $partner_options =  $this->_getPartnersList(false); 
        $disabled        = true;        
      }else{
        $partner_options =  $this->_getPartnersList(true);
        $disabled        = false;
      }
      
      $fieldset->addField('partner_id', 'select', array(
            'name'      => 'partner_id',
            'label'     => 'Select Partner',            
            'required'  => true,
            'disabled'  => $disabled,
            'options'   => $partner_options
      ));
      
      $fieldset->addField('printers_id', 'select', array(
            'name'      => 'printers_id',
            'label'     => 'Select Printing House',            
            'required'  => true,
            'options'   => $this->_getPrintersList()
        ));    
      		
      $fieldset->addField('status', 'select', array(
          'label'     => Mage::helper('associater')->__('Status'),
          'name'      => 'status',
          'values'    => array(
              array(
                  'value'     => 1,
                  'label'     => Mage::helper('associater')->__('Enabled'),
              ),

              array(
                  'value'     => 2,
                  'label'     => Mage::helper('associater')->__('Disabled'),
              ),
          ),
      ));
     
           
      if ( Mage::getSingleton('adminhtml/session')->getAssociaterData() )
      {
          $form->setValues(Mage::getSingleton('adminhtml/session')->getAssociaterData());
          Mage::getSingleton('adminhtml/session')->setAssociaterData(null);
      } elseif ( Mage::registry('associater_data') ) {
          $form->setValues(Mage::registry('associater_data')->getData());
      }
      return parent::_prepareForm();
  }
  
  protected function _getPartnersList($exclude_assigned = true){
        $connection = Mage::getSingleton('core/resource')->getConnection('core_read');
        $sql        = "SELECT * FROM partner";
        $rows       = $connection->fetchAll($sql);
        
        $options                = array();
        $assigned_partner_list  = $this->_getAssignedPartnerList();
        if(!empty($rows)){
            foreach ($rows as $row) {
                if($exclude_assigned === true){                    
                    if(!in_array($row['partner_id'], $assigned_partner_list)){
                        $options[$row['partner_id']] = $row['name'];
                    }    
                }else{            
                    $options[$row['partner_id']] = $row['name'];
                }
            }
        }
    
        return $options;
    }
    
    protected function _getPrintersList(){
        $connection = Mage::getSingleton('core/resource')->getConnection('core_read');
        $sql        = "SELECT * FROM printers";
        $rows       = $connection->fetchAll($sql);
        
        $options = array();
        
        if(!empty($rows)){
            foreach ($rows as $row) {
                $options[$row['printers_id']] = $row['name'];
            }
        }
    
        return $options;
    }
  
  protected function _getAssignedPartnerList(){
        $connection = Mage::getSingleton('core/resource')->getConnection('core_read');
        $sql        = "SELECT partner_id, printers_id FROM associater";
        $rows       = $connection->fetchAll($sql);
        
        $options = array();
        
        if(!empty($rows)){
            foreach ($rows as $row) {
                $options[] = $row['partner_id'];
            }
        }
    
        return $options;
    }
}