<?php

class Itoffshore_Customshipping_Block_Adminhtml_Customshipping_Edit_Tab_Form extends Mage_Adminhtml_Block_Widget_Form
{
  protected function _prepareForm()
  {
      $form = new Varien_Data_Form();
      $this->setForm($form);
      $fieldset = $form->addFieldset('customshipping_form', array('legend'=>Mage::helper('customshipping')->__('Item information')));
     
      $fieldset->addField('title', 'text', array(
          'label'     => Mage::helper('customshipping')->__('Shipping Title'),
          'class'     => 'required-entry',
          'required'  => true,
          'name'      => 'title',
      ));
    
     $fieldset->addField('shipping_fee', 'text', array(
          'label'     => Mage::helper('customshipping')->__('Shipping Fee'),
          'class'     => 'required-entry validate-number',
          'required'  => true,
          'name'      => 'shipping_fee',
      ));
      		
      $fieldset->addField('status', 'select', array(
          'label'     => Mage::helper('customshipping')->__('Status'),
          'name'      => 'status',
          'values'    => array(
              array(
                  'value'     => 1,
                  'label'     => Mage::helper('customshipping')->__('Enabled'),
              ),

              array(
                  'value'     => 2,
                  'label'     => Mage::helper('customshipping')->__('Disabled'),
              ),
          ),
      ));         
     
      if ( Mage::getSingleton('adminhtml/session')->getCustomshippingData() )
      {
          $form->setValues(Mage::getSingleton('adminhtml/session')->getCustomshippingData());
          Mage::getSingleton('adminhtml/session')->setCustomshippingData(null);
      } elseif ( Mage::registry('customshipping_data') ) {
          $form->setValues(Mage::registry('customshipping_data')->getData());
      }
      return parent::_prepareForm();
  }
}