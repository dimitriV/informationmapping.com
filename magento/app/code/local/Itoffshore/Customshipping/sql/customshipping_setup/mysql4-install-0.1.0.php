<?php

$installer = $this;

$installer->startSetup();

$installer->run("

-- DROP TABLE IF EXISTS {$this->getTable('customshipping')};
CREATE TABLE IF NOT EXISTS {$this->getTable('customshipping')}(
  `customshipping_id` int(11) unsigned NOT NULL auto_increment,
  `title` varchar(255) NOT NULL default '',
  `shipping_fee` double(10,2) default NULL,  
  `status` smallint(6) NOT NULL default '0',
  `created_time` datetime default NULL,
  `update_time` datetime default NULL,
  PRIMARY KEY  (`customshipping_id`)
) ENGINE=InnoDB  DEFAULT CHARSET=utf8;

    ");

$installer->endSetup(); 