<?php

class Itoffshore_Orderemail_Model_Mysql4_Orderemail extends Mage_Core_Model_Mysql4_Abstract
{
    public function _construct()
    {    
        // Note that the orderemail_id refers to the key field in your database table.
        $this->_init('orderemail/orderemail', 'orderemail_id');
    }
}