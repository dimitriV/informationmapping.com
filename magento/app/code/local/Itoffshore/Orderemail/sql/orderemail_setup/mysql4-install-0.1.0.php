<?php

$installer = $this;

$installer->startSetup();

$installer->run("

-- DROP TABLE IF EXISTS {$this->getTable('orderemail')};
CREATE TABLE {$this->getTable('orderemail')} (
  `orderemail_id` INT(11) UNSIGNED NOT NULL AUTO_INCREMENT,
  `product_id` INT(11) DEFAULT NULL,
  `email_template_id` INT(11) DEFAULT NULL,
  `title` VARCHAR(255) NOT NULL DEFAULT '',
  `filename` VARCHAR(255) NOT NULL DEFAULT '',
  `content` TEXT NOT NULL,
  `status` SMALLINT(6) NOT NULL DEFAULT '0',
  `created_time` DATETIME DEFAULT NULL,
  `update_time` DATETIME DEFAULT NULL,
  PRIMARY KEY  (`orderemail_id`)
) ENGINE=INNODB  DEFAULT CHARSET=utf8 AUTO_INCREMENT=3 ;

    ");

$installer->endSetup(); 