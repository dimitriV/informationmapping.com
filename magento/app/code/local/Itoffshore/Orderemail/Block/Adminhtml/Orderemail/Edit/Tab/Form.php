<?php

class Itoffshore_Orderemail_Block_Adminhtml_Orderemail_Edit_Tab_Form extends Mage_Adminhtml_Block_Widget_Form
{
  protected function _prepareForm()
  {
      $form = new Varien_Data_Form();
      $this->setForm($form);
      $fieldset = $form->addFieldset('orderemail_form', array('legend'=>Mage::helper('orderemail')->__('Item information')));
     
      /*$fieldset->addField('title', 'text', array(
          'label'     => Mage::helper('orderemail')->__('Title'),
          'class'     => 'required-entry',
          'required'  => true,
          'name'      => 'title',
      ));*/
      
      //get product list according to edit it
      if($this->getRequest()->getParam('id') > 0){
        $product_options =  $this->_getProductList(false); 
        $disabled        = true;        
      }else{
        $product_options =  $this->_getProductList(true);
        $disabled        = false;
      }
      
      $fieldset->addField('product_id', 'select', array(
            'name'      => 'product_id',
            'label'     => 'Select Product',            
            'required'  => true,
            'disabled'  => $disabled,
            'options'   => $product_options
      ));
      
      $fieldset->addField('email_template_id', 'select', array(
            'name'      => 'email_template_id',
            'label'     => 'Select Email Template',            
            'required'  => true,
            'options'   => $this->_getEmailTemplatesList()
        ));  
        
      /*$fieldset->addField('filename', 'file', array(
          'label'     => Mage::helper('orderemail')->__('File'),
          'required'  => false,
          'name'      => 'filename',
	  ));*/
		
      $fieldset->addField('status', 'select', array(
          'label'     => Mage::helper('orderemail')->__('Status'),
          'name'      => 'status',
          'values'    => array(
              array(
                  'value'     => 1,
                  'label'     => Mage::helper('orderemail')->__('Enabled'),
              )/*,

              array(
                  'value'     => 2,
                  'label'     => Mage::helper('orderemail')->__('Disabled'),
              ),*/
          ),
      ));
     
      /*$fieldset->addField('content', 'editor', array(
          'name'      => 'content',
          'label'     => Mage::helper('orderemail')->__('Content'),
          'title'     => Mage::helper('orderemail')->__('Content'),
          'style'     => 'width:700px; height:500px;',
          'wysiwyg'   => false,
          'required'  => true,
      ));*/
     
      if ( Mage::getSingleton('adminhtml/session')->getOrderemailData() )
      {
          $form->setValues(Mage::getSingleton('adminhtml/session')->getOrderemailData());
          Mage::getSingleton('adminhtml/session')->setOrderemailData(null);
      } elseif ( Mage::registry('orderemail_data') ) {
          $form->setValues(Mage::registry('orderemail_data')->getData());
      }
      return parent::_prepareForm();
  }
  
    protected function _getProductList($exclude_assigned = true)
    {
        $products = Mage::getModel('catalog/product')
            ->getCollection()
            ->addAttributeToSelect('*')
            ->addAttributeToFilter('status', array('eq' => Mage_Catalog_Model_Product_Status::STATUS_ENABLED))
            ->load();
            
        $product_list = array();
        $assigned_product_list = $this->_getAssignedEmailTemplateProductList();
        foreach($products as $product)
        {
            if($exclude_assigned == true){
                if(!in_array($product->getId(), $assigned_product_list)){
                    $product_list[$product->getId()] = $product->getName(); 
                }
            }else{
                $product_list[$product->getId()] = $product->getName(); 
            }
            
        }
        
        return $product_list;   
    }
    
    protected function _getEmailTemplatesList(){
        $connection = Mage::getSingleton('core/resource')->getConnection('core_read');
        $sql        = "SELECT * FROM core_email_template";
        $rows       = $connection->fetchAll($sql);
        
        $options = array();
        
        if(!empty($rows)){
            foreach ($rows as $row) {
                $options[$row['template_id']] = $row['template_code'];
            }
        }
    
        return $options;
    }
    
    protected function _getAssignedEmailTemplateProductList(){
        $connection = Mage::getSingleton('core/resource')->getConnection('core_read');
        $sql        = "SELECT orderemail_id, product_id FROM orderemail";
        $rows       = $connection->fetchAll($sql);
        
        $options = array();
        
        if(!empty($rows)){
            foreach ($rows as $row) {
                $options[] = $row['product_id'];
            }
        }
    
        return $options;
    }
    
}