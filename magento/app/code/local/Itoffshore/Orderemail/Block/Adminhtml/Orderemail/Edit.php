<?php

class Itoffshore_Orderemail_Block_Adminhtml_Orderemail_Edit extends Mage_Adminhtml_Block_Widget_Form_Container
{
    public function __construct()
    {
        parent::__construct();
                 
        $this->_objectId = 'id';
        $this->_blockGroup = 'orderemail';
        $this->_controller = 'adminhtml_orderemail';
        
        $this->_updateButton('save', 'label', Mage::helper('orderemail')->__('Save Item'));
        $this->_updateButton('delete', 'label', Mage::helper('orderemail')->__('Delete Item'));
		
        $this->_addButton('saveandcontinue', array(
            'label'     => Mage::helper('adminhtml')->__('Save And Continue Edit'),
            'onclick'   => 'saveAndContinueEdit()',
            'class'     => 'save',
        ), -100);

        $this->_formScripts[] = "
            function toggleEditor() {
                if (tinyMCE.getInstanceById('orderemail_content') == null) {
                    tinyMCE.execCommand('mceAddControl', false, 'orderemail_content');
                } else {
                    tinyMCE.execCommand('mceRemoveControl', false, 'orderemail_content');
                }
            }

            function saveAndContinueEdit(){
                editForm.submit($('edit_form').action+'back/edit/');
            }
        ";
    }

    public function getHeaderText()
    {
        if( Mage::registry('orderemail_data') && Mage::registry('orderemail_data')->getId() ) {
            return Mage::helper('orderemail')->__("Edit Item", $this->htmlEscape(Mage::registry('orderemail_data')->getTitle()));
        } else {
            return Mage::helper('orderemail')->__('Add Item');
        }
    }
}