<?php

class Itoffshore_Orderemail_Block_Adminhtml_Orderemail_Edit_Tabs extends Mage_Adminhtml_Block_Widget_Tabs
{

  public function __construct()
  {
      parent::__construct();
      $this->setId('orderemail_tabs');
      $this->setDestElementId('edit_form');
      $this->setTitle(Mage::helper('orderemail')->__('Item Information'));
  }

  protected function _beforeToHtml()
  {
      $this->addTab('form_section', array(
          'label'     => Mage::helper('orderemail')->__('Item Information'),
          'title'     => Mage::helper('orderemail')->__('Item Information'),
          'content'   => $this->getLayout()->createBlock('orderemail/adminhtml_orderemail_edit_tab_form')->toHtml(),
      ));
     
      return parent::_beforeToHtml();
  }
}