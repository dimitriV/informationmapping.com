<?php
class Itoffshore_Orderemail_Block_Adminhtml_Orderemail extends Mage_Adminhtml_Block_Widget_Grid_Container
{
  public function __construct()
  {
    $this->_controller = 'adminhtml_orderemail';
    $this->_blockGroup = 'orderemail';
    $this->_headerText = Mage::helper('orderemail')->__('Product Email Manager');
    $this->_addButtonLabel = Mage::helper('orderemail')->__('Add Product Email');
    parent::__construct();
  }
}