<?php

class Itoffshore_Orderemail_Block_Adminhtml_Orderemail_Grid extends Mage_Adminhtml_Block_Widget_Grid
{
  public function __construct()
  {
      parent::__construct();
      $this->setId('orderemailGrid');
      $this->setDefaultSort('orderemail_id');
      $this->setDefaultDir('DESC');
      $this->setSaveParametersInSession(true);
  }

  protected function _prepareCollection()
  {
      $collection = Mage::getModel('orderemail/orderemail')->getCollection();
      $collection->getSelect()
                  ->join( array('cpe' => 'catalog_product_entity'), 'main_table.product_id = cpe.entity_id', array('cpe.sku'))
                  ->join( array('cpev' => 'catalog_product_entity_varchar'), 'cpe.entity_id = cpev.entity_id', array('cpev.value'))     
                  ->join(array('cet' => 'core_email_template'), 'main_table.email_template_id = cet.template_id', array('cet.template_code'))
                  ->where("1=1
                    	AND cpev.entity_type_id = '4'
                    	AND cpev.attribute_id = '56'");    
      $this->setCollection($collection);
      return parent::_prepareCollection();
  }

  protected function _prepareColumns()
  {
      $this->addColumn('orderemail_id', array(
          'header'    => Mage::helper('orderemail')->__('ID'),
          'align'     =>'right',
          'width'     => '50px',
          'index'     => 'orderemail_id',
      ));

      
      
      /*$this->addColumn('title', array(
          'header'    => Mage::helper('orderemail')->__('Title'),
          'align'     =>'left',
          'index'     => 'title',
      ));*/
        
      $this->addColumn('product_id', array(
          'header'    => Mage::helper('orderemail')->__('Product'),
          'align'     =>'left', 
          //'filter_index'  => 'sku',         
          'index'     => 'value',
      ));
      
      $this->addColumn('email_template_id', array(
          'header'    => Mage::helper('orderemail')->__('Assigned Email Template'),
          'align'     =>'left',
          'index'     => 'template_code',
      ));   
      
	  /*
      $this->addColumn('content', array(
			'header'    => Mage::helper('orderemail')->__('Item Content'),
			'width'     => '150px',
			'index'     => 'content',
      ));
	  */

      $this->addColumn('status', array(
          'header'    => Mage::helper('orderemail')->__('Status'),
          'align'     => 'left',
          'width'     => '80px',
          'index'     => 'status',
          'type'      => 'options',
          'options'   => array(
              1 => 'Enabled',
              /*2 => 'Disabled',*/
          ),
      ));
	  
        $this->addColumn('action',
            array(
                'header'    =>  Mage::helper('orderemail')->__('Action'),
                'width'     => '100',
                'type'      => 'action',
                'getter'    => 'getId',
                'actions'   => array(
                    array(
                        'caption'   => Mage::helper('orderemail')->__('Edit'),
                        'url'       => array('base'=> '*/*/edit'),
                        'field'     => 'id'
                    )
                ),
                'filter'    => false,
                'sortable'  => false,
                'index'     => 'stores',
                'is_system' => true,
        ));
		
		#$this->addExportType('*/*/exportCsv', Mage::helper('orderemail')->__('CSV'));
		#$this->addExportType('*/*/exportXml', Mage::helper('orderemail')->__('XML'));
	  
      return parent::_prepareColumns();
  }

    protected function _prepareMassaction()
    {
        $this->setMassactionIdField('orderemail_id');
        $this->getMassactionBlock()->setFormFieldName('orderemail');

        $this->getMassactionBlock()->addItem('delete', array(
             'label'    => Mage::helper('orderemail')->__('Delete'),
             'url'      => $this->getUrl('*/*/massDelete'),
             'confirm'  => Mage::helper('orderemail')->__('Are you sure?')
        ));

        $statuses = Mage::getSingleton('orderemail/status')->getOptionArray();

        array_unshift($statuses, array('label'=>'', 'value'=>''));
        /*$this->getMassactionBlock()->addItem('status', array(
             'label'=> Mage::helper('orderemail')->__('Change status'),
             'url'  => $this->getUrl('*//*/massStatus', array('_current'=>true)),
             'additional' => array(
                    'visibility' => array(
                         'name' => 'status',
                         'type' => 'select',
                         'class' => 'required-entry',
                         'label' => Mage::helper('orderemail')->__('Status'),
                         'values' => $statuses
                     )
             )
        ));*/
        return $this;
    }

  public function getRowUrl($row)
  {
      return $this->getUrl('*/*/edit', array('id' => $row->getId()));
  }

}