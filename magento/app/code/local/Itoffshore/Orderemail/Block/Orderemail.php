<?php
class Itoffshore_Orderemail_Block_Orderemail extends Mage_Core_Block_Template
{
	public function _prepareLayout()
    {
		return parent::_prepareLayout();
    }
    
     public function getOrderemail()     
     { 
        if (!$this->hasData('orderemail')) {
            $this->setData('orderemail', Mage::registry('orderemail'));
        }
        return $this->getData('orderemail');
        
    }
}