<?php

class Itoffshore_Shipping_Model_Carrier_Infoshipping
    extends Mage_Shipping_Model_Carrier_Abstract
    implements Mage_Shipping_Model_Carrier_Interface
{

    protected $_code = 'infoshipping';

    /**
     * Collects the shipping rates
     *
     * @param Mage_Shipping_Model_Rate_Request $data
     * @return Mage_Shipping_Model_Rate_Result
     */
    public function collectRates(Mage_Shipping_Model_Rate_Request $request)
    {
        if (!$this->getConfigFlag('active')) {
            return false;
        }

        $result = Mage::getModel('shipping/rate_result');
        
        $custom_methods = $this->_getAllCustomShippingMethods();        
        foreach($custom_methods as $custom_method){
            $method = Mage::getModel('shipping/rate_result_method');

            $method->setCarrier('infoshipping');
            $method->setCarrierTitle($this->getConfigData('title'));

            $method->setMethod('infomethod_' . $custom_method['pk']);
            $method->setMethodTitle($custom_method['name']);

			$price = $custom_method['handling_fee'];
		          
            $method->setPrice($this->getFinalPriceWithHandlingFee($price));
            $method->setCost($custom_method['handling_fee']);

            $result->append($method);          
        }

        return $result;
    }
    
    public function _getAllCustomShippingMethods(){
        $collection = Mage::getModel('customshipping/customshipping')->getCollection();
        $collection->addFieldToFilter('status', 1);
        $collection->setOrder('title', 'ASC');
        $shipping_methods = $collection->toArray();
        
        if($shipping_methods['totalRecords'] > 0){
            $methods_array = array();
            foreach($shipping_methods['items'] as $method){
                $partner_shipping_ids = $this->_getImPartnerShippingsByCurrentCustomer();
                if(in_array($method['customshipping_id'], $partner_shipping_ids)){
                    $methods_array[] = array('name' => $method['title'], 'pk' => $method['customshipping_id'], 'handling_fee' => $method['shipping_fee']);    
                }                
            }
            return $methods_array;
        }else{
            return;
        }
    }   
    
    protected function _getImPartnerShippingsByCurrentCustomer(){
        $_customer  = Mage::getSingleton('customer/session')->isLoggedIn() ? Mage::getSingleton('customer/session')->getCustomer() : null;
        $partner_id = $_customer->getPartner();
        
        $connection = Mage::getSingleton('core/resource')->getConnection('core_read');
        $sql        = "SELECT custom_shipping_ids FROM partnershipping WHERE partner_id = ?";
        Mage::log($partner_id, null, 'customshipping.log');
        $custom_shipping_ids = $connection->fetchOne($sql, $partner_id);        
        if(empty($custom_shipping_ids)){
            return array();
        }else{
            return explode(',', $custom_shipping_ids);
        }
    } 
   
    
    /**
     * Get allowed shipping methods
     *
     * @return array
     */
    public function getAllowedMethods()
    {
        return array('infomethod' => $this->getConfigData('title'));
    }

}
