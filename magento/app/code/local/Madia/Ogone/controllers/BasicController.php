<?php
/**
 * Madia
 *
 * @category   Madia
 * @package    Madia_Ogone
 * @copyright  Copyright (c) 2008 Madia BV (http://www.madia.nl)
 */

/**
 * Ogone Basic Front Controller
 *
 * @category   Madia
 * @package    Madia_Ogone
 * @name       Madia_Ogone_BasicController
 * @author     Han Leenders <info@madia.nl>
 */
class Madia_Ogone_BasicController extends Mage_Core_Controller_Front_Action {
	/**
	 *  Return order instance for last real order ID (stored in session)
	 *
	 *  @param    none
	 *  @return	  Mage_Sales_Model_Entity_Order object
	 */
	protected function _getOrder() {
		$order = Mage::getModel('sales/order');
		$order->load(Mage::getSingleton('checkout/session')->getLastOrderId());
		
		if (!$order->getId()) {
			return false;
		}
		
		return $order;
	}
	
	
	/**
	 *  Return debug flag
	 *
	 *  @return  boolean
	 */
	public function getDebug() {
		return Mage::getSingleton('ogone/config')->getDebug();
	}
	

	public function postsaleAction()
	{

		$request = $this->getRequest();
		
		if ($request->getParam('SHASIGN') == Mage::getModel('ogone/basic')->getShaOutSignature($request)) {
			
			$orderId = $request->getParam('orderID');
			$order = Mage::getModel('sales/order');
	        $order->loadByIncrementId($orderId);
	        
	        $ogoneStatus = $request->getParam('STATUS');
	  		$pm = $request->getParam('PM');
	        $brand = $request->getParam('BRAND');
	        $ogonePayId = $request->getParam('PAYID');
	        	        
	        if ($order->getId() > 0 && $order->getPayment()->getOgoneStatus() != $ogoneStatus) {
	        	
	        	if ($ogoneStatus == 5 || $ogoneStatus == 9) {
                	$order->setState(Mage_Sales_Model_Order::STATE_PROCESSING, true);
                	$this->_saveInvoice($order);
	        	}
	        		        	
	        	$order->addStatusToHistory($order->getStatus(), 
	        		Mage::helper('ogone')->__('Transaction Status Update<br/>Ogone status: ' . $ogoneStatus . ' - ' . Mage::getModel('ogone/basic')->getOgoneStatusString($ogoneStatus) . '<br/>Ogone payId: ' . $ogonePayId . '<br/>Payment details: ' . $pm . '-' . $brand));
	  			
	        	//$order->getPayment()->setOgonePostsaleReceived(1);
	        	$order->getPayment()->setOgoneStatus($ogoneStatus);	
	        	$order->getPayment()->setOgonePaymentBrand($pm.' '.$brand);	
	        	$order->save();        	
	        }		
		} else {
			$this->norouteAction();
		}
	}
	
	/**
	 * When a customer chooses Ogone on Checkout/Payment page
	 *
	 */
	public function redirectAction() {
		$session = Mage::getSingleton('checkout/session');
		$session->setOgoneBasicQuoteId($session->getQuoteId());
		$session->setOgoneBasicOrderId($session->getLastOrderId());
		
		if (!($order = $this->_getOrder())) {
			$this->norouteAction();
			return;
		}
		$order->addStatusToHistory ( 
			$order->getStatus(), 
			$this->__ ('Customer was redirected to Ogone. Please, check the status of a transaction via the Ogone administration module before delivery of the goods purchased.')
		);
		$order->save();
		
		$this->getResponse()->setBody($this->getLayout()->createBlock('ogone/basic_redirect')->setOrder($order)->toHtml());
		$session->unsQuoteId();
		$session->unsLastOrderId();
	
	}
	
	/**
	 *  Accept response from Ogone
	 * 
	 *  @return void
	 */
	public function acceptAction() 
	{
		$request = $this->getRequest();	
		
        $session = Mage::getSingleton('checkout/session');
        $session->setLastOrderId($session->getOgoneBasicOrderId(true));
             

        if ( !($order = $this->_getOrder()) ) {
        	// order check bases on session has not succeeded, try based on the return data of Ogone
        	        	
        	if ($request->getParam('SHASIGN') == Mage::getModel('ogone/basic')->getShaOutSignature($request)) {
				
				$orderId = $request->getParam('orderID');

				$session->setLastOrderId($orderId);				
				$session->setOgoneBasicQuoteId($session->getQuoteId());
				$session->setOgoneBasicOrderId($session->getLastOrderId());				
				
				$order = Mage::getModel('sales/order');
		        $order->loadByIncrementId($orderId);
		        
        	} else {
        		$this->_redirect('checkout/onepage/success');
            	return;
        	} 	
        }

        $session->setQuoteId($session->getOgoneBasicQuoteId(true));

        $order->addStatusToHistory(
            $order->getStatus(),
            $this->__('Customer successfully returned from Ogone')
        );

        $order->sendNewOrderEmail();
        $this->_saveInvoice($order);
        $order->save();

		/**
         * set the quote as inactive
         */
		Mage::getSingleton('checkout/session')->getQuote()->setIsActive(false)->save();
	
		$this->_redirect('checkout/onepage/success');
	
	}
	
	/**
	 *  Decline response from Ogone
	 *
	 *  @return void
	 */
	public function declineAction() {
		$errorMsg = Mage::helper('ogone')->__('The payment has not been accepted.');
	
		if ( !($order = $this->_getOrder()) ) {
			$this->_redirect('checkout/cart');
			return;
		}
		
		if ($order instanceof Mage_Sales_Model_Order && $order->getId()) {
			$order->addStatusToHistory($order->getStatus(), $errorMsg);
			$order->cancel();
			$order->save();
		}
		
		$this->loadLayout();
		$this->renderLayout();
		
		Mage::getSingleton('checkout/session' )->unsLastRealOrderId();
		Mage::getSingleton('checkout/session')->addError($this->__('Your payment was not accepted. Please try again later.'));
	}
	
	/**
	 * Exception response from Ogone
	 *
	 *  @return void
	 */
	public function exceptionAction() {
		$session = Mage::getSingleton('checkout/session');
		$session->setLastOrderId($session->getOgoneBasicOrderId(true));
		
		if ( !($order = $this->_getOrder()) ) {
			$this->_redirect('checkout/cart');
			return false;
		}
		
		$session->setQuoteId($session->getOgoneBasicQuoteId(true));
		
		$order->addStatusToHistory($order->getStatus(), $this->__ ('Exception response from Ogone, check the payment.'));
		
		$order->sendNewOrderEmail();
		
		$this->_saveInvoice($order);
		
		$order->save();
		
		$this->_redirect('checkout/onepage/success');
	}
	

	/**
	 * Cancel response from Ogone
	 *
	 */
	public function cancelAction() {
		$session = Mage::getSingleton('checkout/session');
		$session->setLastOrderId($session->getOgoneBasicOrderId(true));
		
		if ( !($order = $this->_getOrder()) ) {
			$this->_redirect('checkout/cart');
			return false;
		}
		
		$order->cancel();
		$history = $this->__('Payment was canceled by customer');
		$order->addStatusToHistory($order->getStatus(), $history);
		$order->save();
		$session->setQuoteId($session->getOgoneBasicQuoteId(true));
		
		Mage::getSingleton('customer/session')->setLastOrderId(null);
        Mage::getSingleton('checkout/session')->addError($this->__('Your payment was cancelled. Please try again later'));
		
		$this->_redirect('checkout/cart');
	}
	
	/**
	 *  Save invoice for order
	 *
	 *  @param    Mage_Sales_Model_Order $order
	 *  @return	  boolean Can save invoice or not
	 */
	protected function _saveInvoice(Mage_Sales_Model_Order $order) {		
        //$newOrderState = Mage_Sales_Model_Order::STATE_PROCESSING;
		//$newOrderStatus = 'processing';
		if ($order->canInvoice()) {
			//need to convert from order into invoice
			$invoice = $order->prepareInvoice();
			$invoice->register()->capture();
			Mage::getModel('core/resource_transaction')
			   ->addObject($invoice)
			   ->addObject($invoice->getOrder())
			   ->save();
			//$statusMessage = $this->__('Your payment has been received - order will be submitted for shipping.') . "<br/>" . $this->__('Invoice #%s created', $invoice->getIncrementId());
			
            //$order->setState($newOrderState, $newOrderStatus, $statusMessage, true);
            //$order->save();
			// fire a success event for completed bank transfer (allows shipping module to take action)
		    //Mage::dispatchEvent('checkout_onepage_controller_success_action');
            // Added by Erik Hansen @ CLS
			$invoice->sendEmail(true);
            return true;
		} 
        return false;        
	}

}