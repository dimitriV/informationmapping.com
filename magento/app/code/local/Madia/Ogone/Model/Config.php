<?php
/**
 * Madia
 *
 * @category   Madia
 * @package    Madia_Ogone
 * @copyright  Copyright (c) 2008 Madia BV (http://www.madia.nl)
 */

/**
 * Ogone Configuration Model
 *
 * @category   Madia
 * @package    Madia_Ogone
 * @name       Madia_Ogone_Model_Config
 * @author     Han Leenders <info@madia.nl>
 */

class Madia_Ogone_Model_Config extends Varien_Object
{
	const MODE_TEST         = 'TEST';
    const MODE_LIVE         = 'LIVE';
	
    /**
     *  Return config var
     *
     *  @param    string Var key
     *  @param    string Default value for non-existing key
     *  @return	  mixed
     */
    public function getConfigData($key, $default=false)
    {
        if (!$this->hasData($key)) {
            $value = Mage::getStoreConfig('payment/ogone_basic/'.$key);
            if (is_null($value) || false===$value) {
             $value = $default;
            }
            $this->setData($key, $value);
        }
        return $this->getData($key);
    }

     /**
     *  Return new order status
     *
     *  @return	  string New order status
     */
    public function getNewOrderStatus ()
    {
        return $this->getConfigData('order_status');
    }

    /**
     *  Return debug flag
     *
     *  @return	  boolean Debug flag (0/1)
     */
    public function getDebug ()
    {
        return $this->getConfigData('debug_flag');
    }

    /**
     *  Return accepted currency
     *
     *  @param    none
     *  @return	  string Currenc
     */
    public function getCurrency ()
    {
        return $this->getConfigData('currency');
    }

    /**
     *  Return client interface language
     *
     *  @param    none
     *  @return	  string(2) Accepted language
     */
    public function getLanguage ()
    {
        return $this->getConfigData('language');
    }

    /**
     * Return Psp id
     *
     * @return string
     */
	public function getPspid()
	{
		return $this->getConfigData('pspid');
	}
	
	/**
	 * Return Sha1 sign key
	 * 
	 * @return string
	 */
	public function getSha1SignKey()
	{
		return $this->getConfigData('sha1_key');
	}
	
	/**
	 * Return payment methodes by brand
	 *
	 * @return array
	 */
	public function getPaymentMethodesByBrand()
	{
        $methodes = array();
        foreach (Mage::getConfig()->getNode('global/payment/methodes')->asArray() as $data) {
            $methodes[$data['brand']] = $data['name'];
        }
        return $methodes;
	}
	
	public function getPaymentMethodesByType()
	{
		$methodes = array();
        foreach (Mage::getConfig()->getNode('global/payment/methodes')->asArray() as $data) {
            $methodes[$data['brand']] = $data['pm'];
        }
        return $methodes;
	}
	
	/**
	 * Return layout title
	 *
	 * @return string
	 */
	public function getLayoutTitle()
	{
		return $this->getConfigData('layout_title');
	}
	
	public function getLayoutBgColor()
	{
		return $this->getConfigData('layout_bgcolor', 'white');
	}
	
	public function getLayoutTxtColor()
	{
		return $this->getConfigData('layout_txtcolor', 'black');
	}
	
	public function getLayoutTblBgColor()
	{
		return $this->getConfigData('layout_tblbgcolor', 'white');
	}
	
	public function getLayoutTblTxtColor()
	{
		return $this->getConfigData('layout_tbltxtcolor', 'black');
	}
	
	public function getLayoutButtonBgColor()
	{
		return $this->getConfigData('layout_buttonbgcolor');	
	}
	
	public function getLayoutButtonTxtColor()
	{
		return $this->getConfigData('layout_buttontxtcolor', 'black');
	}
	
	public function getLayoutLogo()
	{
		return $this->getConfigData('layout_logo');
	}
	
	public function getLayoutFontType()
	{
		return $this->getConfigData('layout_fonttype', 'verdana');
	}
	
	public function getMode()
	{
		return $this->getConfigData('mode');
	}
	
	public function getDynamicTemplate()
	{
		return $this->getConfigData('dynamic_template');
	}
}