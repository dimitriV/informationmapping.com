<?php
/**
 * Madia
 *
 * @category   Madia
 * @package    Magdia_Ogone
 * @copyright  Copyright (c) 2004-2007 Irubin Consulting Inc. DBA Varien (http://www.varien.com)
 * @license    http://opensource.org/licenses/osl-3.0.php  Open Software License (OSL 3.0)
 */

/**
 * Ogone module observer
 *
 * @category    Madia
 * @package     Madia_Ogone
 */
class Madia_Ogone_Model_Observer
{
    /**
     * Convert specific attributes from Quote Payment to Order Payment
     *
     * @param Varien_Object $observer
     * @return Madia_Ogone_Model_Observer
     */
    public function convertPayment($observer)
    {
        $orderPayment = $observer->getEvent()->getOrderPayment();
        $quotePayment = $observer->getEvent()->getQuotePayment();
        
       
        try {
        
        	$orderPayment->setOgonePaymentBrand($quotePayment->getOgonePaymentBrand());
        	
        	Mage::getSingleton('checkout/session')->setOgonePaymentBrand($quotePayment->getOgonePaymentBrand());

        } catch (Exception  $e) {
        	error_log($e->getMessage());
        }
        
        return $this;
    }
}