<?php
/**
 * Madia
 *
 * @category   Madia
 * @package    Madia_Ogone
 * @copyright  Copyright (c) 2008 Madia BV (http://www.madia.nl)
 */

/**
 *
 * Payflow Payment Action Dropdown source
 *
 * @author      Han Leenders <info@madia.nl>
 */
class Madia_Ogone_Model_Source_PaymentMethodes
{
	public function toOptionArray()
	{
        $options =  array();

        foreach (Mage::getSingleton('ogone/config')->getPaymentMethodesByBrand() as $brand => $name) {
            $options[] = array(
             'value' => $brand,
             'label' => $name 
            );
        }

        return $options;
	}
}