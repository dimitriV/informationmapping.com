<?php
/**
 * Madia
 *
 * @category   Madia
 * @package    Madia_Ogone
 * @copyright  Copyright (c) 2008 Madia BV (http://www.madia.nl)
 */

/**
 * Ogone Modes Resource
 *
 * @category   Mage
 * @package    Mage_Ogone
 * @name       Mage_Ogone_Model_Source_ModeAction
 */

class Madia_Ogone_Model_Source_ModeAction
{
    public function toOptionArray()
    {
        return array(
            array('value' => Madia_Ogone_Model_Config::MODE_TEST, 'label' => Mage::helper('ogone')->__('Test')),
            array('value' => Madia_Ogone_Model_Config::MODE_LIVE, 'label' => Mage::helper('ogone')->__('Live')),
        );
    }
}



