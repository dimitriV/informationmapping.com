<?php
/**
 * Madia
 *
 * @category   Madia
 * @package    Madia_Ogone
 * @copyright  Copyright (c) 2008 Madia BV (http://www.madia.nl)
 */

/**
 * Ogone Allowed languages Resource
 *
 * @category   Madia
 * @package    Madia_Ogone
 * @name       Madia_Ogone_Model_Source_Language
 * @author     Han Leenders <info@madia.nl>
 */

class Madia_Ogone_Model_Source_Language
{
    public function toOptionArray()
    {
        return array(
            array('value' => 'en_US', 'label' => Mage::helper('ogone')->__('English')),
            array('value' => 'nl_NL', 'label' => Mage::helper('ogone')->__('Dutch')),
            array('value' => 'fr_FR', 'label' => Mage::helper('ogone')->__('French'))
            
        );
    }
}



