<?php
/**
 * Madia
 *
 * @category   Madia
 * @package    Madia_Ogone
 * @copyright  Copyright (c) 2004-2007 Madia BB (http://www.madia.nl)
 */

/**
 * Ogone API Debug Resource
 *
 * @category   Madia
 * @package    Madia_Ogone
 * @name       Madia_Ogone_Model_Mysql4_Api_Debug
 * @author     Magento Core Team <core@magentocommerce.com>
 */

class Madia_Ogone_Model_Mysql4_Api_Debug extends Mage_Core_Model_Mysql4_Abstract
{
    protected function _construct()
    {
        $this->_init('ogone/api_debug', 'debug_id');
    }
}