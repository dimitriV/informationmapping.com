<?php
/**
 * Madia
 *
 * @category   Madia
 * @package    Madia_Ogone
 * @copyright  Copyright (c) 2004-2007 Madia BB (http://www.madia.nl)
 */

/**
 * Ogone API Debug Resource Collection
 *
 * @category   Madia
 * @package    Madia_Ogone
 * @name       Madia_Ogone_Model_Mysql4_Api_Debug_Collection
 * @author     Han Leenders <info@madia.nl>
 */
class Madia_Ogone_Model_Mysql4_Api_Debug_Collection extends Mage_Core_Model_Mysql4_Collection_Abstract
{
    protected function _construct()
    {
        $this->_init('ogone/api_debug');
    }
}