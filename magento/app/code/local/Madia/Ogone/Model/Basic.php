<?php
/**
 * Madia
 *
 * @category   Madia
 * @package    Madia_Ogone
 * @copyright  Copyright (c) 2008 Madia BV (http://www.madia.nl)
 */

/**
 * Ogone Basic Model
 *
 * @category   Madia
 * @package    Madia_Ogone
 * @name       Madia_Ogone_Model_Basic
 * @author     Han Leenders <info@madia.nl>
 */
class Madia_Ogone_Model_Basic extends Mage_Payment_Model_Method_Abstract
{
    protected $_code  = 'ogone_basic';
    protected $_formBlockType = 'ogone/basic_form';

    protected $_isGateway               = false;
    protected $_canAuthorize            = true;
    protected $_canCapture              = true;
    protected $_canCapturePartial       = false;
    protected $_canRefund               = false;
    protected $_canVoid                 = false;
    protected $_canUseInternal          = false;
    protected $_canUseCheckout          = true;
    protected $_canUseForMultishipping  = false;

    protected $_order = null;
    
    protected $_allowCurrencyCode = array('AED', 'ANG', 'ARS', 'AUD', 'AWG', 'BGN', 'BRL', 'BYR', 'CAD', 'CHF', 'CNY', 'CZK', 'DKK', 'EEK', 'EGP', 'EUR', 'GBP', 'GEL', 'HKD', 'HRK', 'HUF', 'ILS', 'ISK', 'JPY', 'KRW', 'LTL', 'LVL', 'MAD', 'MXN', 'NOK', 'NZD', 'PLN', 'RON', 'RUB', 'SEK', 'SGD', 'SKK', 'THB', 'TRY', 'UAH', 'USD', 'XAF', 'XOF', 'XPF', 'ZAR');


    /**
     * Get Config model
     *
     * @return object Mage_Ogone_Model_Config
     */
    public function getConfig()
    {
        return Mage::getSingleton('ogone/config');
    }
    
    /**
     * Get API Model
     *
     * @return Madia_Ogone_Api_Basic
     */
    public function getApi()
    {
        return Mage::getSingleton('ogone/api_basic');
    }
    
    public function canUseForCurrency($currencyCode)
    {
        if (!in_array($currencyCode, $this->_allowCurrencyCode)) {
            return false;
        }
        return true;
    }

   
    /**
     * Payment validation
     *
     * @param   none
     * @return  Mage_Ogone_Model_Standard
     */
    public function validate()
    {
        parent::validate();
        $paymentInfo = $this->getInfoInstance();
        if ($paymentInfo instanceof Mage_Sales_Model_Order_Payment) {
            $currencyCode = $paymentInfo->getOrder()->getBaseCurrencyCode();
        } else {
            $currencyCode = $paymentInfo->getQuote()->getBaseCurrencyCode();
        }
        if (!$this->canUseForCurrency($currencyCode)) {
            Mage::throwException(Mage::helper('ogone')->__('Selected currency code ('.$currencyCode.') is not compatabile with Ogone'));
        }
        return $this;
    }

    /**
     *  Returns Target URL
     *
     *  @return	  string Target URL
     */
    public function getOgoneUrl ()
    {
        switch ($this->getConfig()->getMode()) {
            case Madia_Ogone_Model_Config::MODE_LIVE:
                $url = 'https://secure.ogone.com/ncol/prod/orderstandard.asp';
                break;
            default: // test mode
                $url = 'https://secure.ogone.com/ncol/test/orderstandard.asp';
                break;
        }  	
        return $url;
    }

    /**
     *  Form block description
     *
     *  @return	 object
     */
    public function createFormBlock($name)
    {
        $block = $this->getLayout()->createBlock('ogone/form_basic', $name);
        $block->setMethod($this->_code);
        $block->setPayment($this->getPayment());

        return $block;
    }
    
    public function getOgonePaymentBrand()
    {    
    	return Mage::getSingleton('checkout/session')->getOgonePaymentBrand();
    }
    
    /**
     * Returns the payment type; PM
     *
     * @return string
     */
    public function getOgonePaymentMethod()
    {
    	$brand = Mage::getSingleton('checkout/session')->getOgonePaymentBrand();
    	$types = $this->getConfig()->getPaymentMethodesByType();
        if (array_key_exists($brand,$types)) {
			return $types[$brand];
    	}
    	return false;
    }

    /**
     *  Return Order Place Redirect URL
     *
     *  @return	  string Order Redirect URL
     */
    public function getOrderPlaceRedirectUrl()
    {
        return Mage::getUrl('ogone/basic/redirect');
    }
        
    /**
     * Return URL for Ogone accept response
     * Possible Ogone status: 5 = authorized
     * 						  9 = accepted
     *                        51,91 = wait for acceptance
     *
     * @return string
     */
    public function getAcceptUrl()
    {
    	return Mage::getUrl('ogone/basic/accept', array('_secure' => true));
    }
    
    /**
     * Return URL for Ogone decline response
     * Ogone status: 2 = authorization is declined by acquirer
     *
     * @return string
     */
    public function getDeclineUrl()
    {
    	return Mage::getUrl('ogone/basic/decline');
    }
    
    /**
     * Return URL for Ogone exception response
     * Ogone status: 52, 92 = successfull payment is not reliable
     *
     * @return string
     */
    public function getExceptionUrl()
    {
    	return Mage::getUrl('ogone/basic/exception');
    }
    
    /**
     * Return URL for Ogone cancel response
     * Ogone status 1 = payment has been cancelled by customer
     *
     * @return string
     */
    public function getCancelUrl()
    {
    	return Mage::getUrl('ogone/basic/cancel');
    }
    
    public function getHomeUrl()
    {
    	return Mage::getUrl();
    }
    
    public function getCurrency()
    {
        return Mage::app()->getStore()->getCurrentCurrencyCode();
    }    
    
    public function getAmountForOgone()
    {
    	$order = $this->getOrder();
    	$amount = round($order->getBaseGrandTotal(),2) * 100;
    	return $amount;
    }
    
    public function getDynamicTemplate()
    {
    	$dynamicTemplateUrl = $this->getConfig()->getDynamicTemplate(); // PSPID
    	if (!empty($dynamicTemplateUrl)) {
    		return $dynamicTemplateUrl;
    	}
    	return false;
    }
    
    public function getShaSign()
    {
    	$order = $this->getOrder();
    	
    	$str = $order->getRealOrderId();
    	$str .= $this->getAmountForOgone(); //$this->getAmount();    	
    	$str .= $this->getCurrency(); // Currency
    	$str .= $this->getConfig()->getPspid(); // PSPID
        
        //add
	$str .= $order->getIncrementId();
	$str .= trim(Mage::getStoreConfig('payment/ogone_basic/AliasUsage'));
        
    	$str .= $this->getConfig()->getSha1SignKey();
        return bin2hex(hash("sha1", $str, true));
		//return bin2hex(mhash(MHASH_SHA1, $str));
    }
    
    public function getShaOutSignature($request)
    {
    	// check shasign
		$shaOutSignature = $request->getParam('orderID');
		$shaOutSignature .= $request->getParam('currency');
		$shaOutSignature .= $request->getParam('amount');
		$shaOutSignature .= $request->getParam('PM');
		$shaOutSignature .= $request->getParam('ACCEPTANCE');
		$shaOutSignature .= $request->getParam('STATUS');
		$shaOutSignature .= $request->getParam('CARDNO');
		$shaOutSignature .= $request->getParam('PAYID');
		$shaOutSignature .= $request->getParam('NCERROR');
		$shaOutSignature .= $request->getParam('BRAND');
		$shaOutSignature .=  $this->getConfig()->getSha1SignKey();
        return strtoupper(bin2hex(hash("sha1", $shaOutSignature, true)));
		//return strtoupper(bin2hex(mhash(MHASH_SHA1, $shaOutSignature)));
    }
    
	/**
     *  Return Standard Checkout Form Fields for request to Ogone
     *
     *  @return	  array Array of hidden form fields
     */
    public function getStandardCheckoutFormFields()
    {
        $order = $this->getOrder();
        if (!($order instanceof Mage_Sales_Model_Order)) {
            Mage::throwException($this->_getHelper()->__('Cannot retrieve order object'));
        }
        
        $billingAddress = $order->getBillingAddress();

        $streets = $billingAddress->getStreet();
        $street = isset($streets[0]) && $streets[0] != ''
                  ? $streets[0]
                  : (isset($streets[1]) && $streets[1] != '' ? $streets[1] : '');

        if ($this->getConfig()->getDescription()) {
            $transDescription = $this->getConfig()->getDescription();
        } else {
            $transDescription = Mage::helper('ogone')->__('Order #%s', $order->getRealOrderId());
        }

        if ($order->getCustomerEmail()) {
            $email = $order->getCustomerEmail();
        } elseif ($billingAddress->getEmail()) {
            $email = $billingAddress->getEmail();
        } else {
            $email = '';
        }

        $fields = array(
        				// general parameters
        				'PSPID'				=> $this->getConfig()->getPspid(),
        				'orderID'			=> $order->getRealOrderId(),
        				'amount'			=> $this->getAmountForOgone(),
        				'currency'  		=> $this->getCurrency(),
        				'language'			=> $this->getConfig()->getLanguage(),
        				// optional parameters
        				'CN'				=> $billingAddress->getFirstname().' '.$billingAddress->getLastname(),
        				'EMAIL'				=> $email,
        				'ownerZIP'			=> $billingAddress->getPostcode(),
        				'owneraddress'		=> $street,
        				'ownercty'			=> $billingAddress->getCountry(),
        				'ownertown'			=> $billingAddress->getCity(),
        				'ownertelno'		=> $billingAddress->getTelephone(),
        				'COM'				=> 'webshop order',
        				//  check before the payment        
        				'SHASign'			=> $this->getShaSign(),
                        
                        //Aliasing
                        'Alias' => $order->getIncrementId(),
                        'AliasUsage' => trim(Mage::getStoreConfig('payment/ogone_basic/AliasUsage')),
                        
        				// layout information
        				'TITLE'				=> $this->getConfig()->getLayoutTitle(),
        				'BGCOLOR'			=> $this->getConfig()->getLayoutBgColor(),
        				'TXTCOLOR'			=> $this->getConfig()->getLayoutTxtColor(),
        				'TBLBGCOLOR'		=> $this->getConfig()->getLayoutTblBgColor(),
        				'TBLTXTCOLOR'		=> $this->getConfig()->getLayoutTblTxtColor(),
        				'BUTTONBGCOLOR' 	=> $this->getConfig()->getLayoutButtonBgColor(),
        				'BUTTONTXTCOLOR'	=> $this->getConfig()->getLayoutButtonTxtColor(),
        				'LOGO'				=> $this->getConfig()->getLayoutLogo(),
        				'FONTTYPE'			=> $this->getConfig()->getLayoutFontType(),
        				// post payment
        				'accepturl' 		=> $this->getAcceptUrl(),
        				'declineurl'		=> $this->getDeclineUrl(),
        				'exceptionurl'  	=> $this->getExceptionUrl(),
        				'cancelurl' 		=> $this->getCancelUrl(),
        				'homeurl'			=> $this->getHomeUrl(),
        				// PM type
        				'PM'				=> $this->getOgonePaymentMethod(),
        				'BRAND'				=> $this->getOgonePaymentBrand(),
        				'TP'				=> $this->getDynamicTemplate()
        				);
        				
        if ($this->getConfig()->getDebug()) {
            $debug = Mage::getModel('ogone/api_debug')
                ->setRequestBody($this->getOgoneUrl()."\n".print_r($fields,1))
                ->save();
            $fields['cs2'] = $debug->getId();
        }

        return $fields;
    }
    
    public function getOgoneStatusString($statusId)
    {
    	return $this->getApi()->getOgoneStatusString($statusId);
    }

    /**
     *  Validate Response from Ogone
     *
     *  @param    array Post data returned from Ogone
     *  @return	  mixed
     */
    public function validateResponse ($data)
    {
        $order = $this->getOrder();

        if (!($order instanceof Mage_Sales_Model_Order)) {
            Mage::throwException($this->_getHelper()->__('Cannot retrieve order object'));
        }

        try {
            $ok = is_array($data)
                && isset($data['transaction_type']) && $data['transaction_type'] != ''
                && isset($data['customer_id']) && $data['customer_id'] != ''
                && isset($data['site_id']) && $data['site_id'] != ''
                && isset($data['product_id']) && $data['product_id'] != '';

            if (!$ok) {
                throw new Exception('Cannot restore order or invalid order ID');
            }

            // validate site ID
            if ($this->getConfig()->getSiteId() != $data['site_id']) {
                throw new Exception('Invalid site ID');
            }

            // validate product ID
            if ($this->getConfig()->getProductId() != $data['product_id']) {
                throw new Exception('Invalid product ID');
            }

            // Successful transaction type
            if (!in_array($data['transaction_type'], array('initial', 'onetime'))) {
                throw new Exception('Transaction is not successful');
            }

        } catch (Exception $e) {
            return $e;
        }
    }
    
}