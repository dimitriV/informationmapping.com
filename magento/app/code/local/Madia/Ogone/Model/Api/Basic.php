<?php
class Madia_Ogone_Model_Api_Basic extends Varien_Object
{
	/**
	 * Status of the payment.
	 *	0	Incomplete or invalid
	 *	1	Cancelled by client
	 *  2	Authorization refused
	 *  4	Order stored
	 * 41	Waiting client payment
	 *  5	Authorized
	 * 51	Authorization waiting
	 * 52	Authorization not known
	 * 59	Author. to get manually
	 *  6	Authorized and canceled
	 * 61	Author. deletion waiting
	 * 62	Author. deletion uncertain
	 * 63	Author. deletion refused
	 *  7	Payment deleted
	 * 71	Payment deletion pending
	 * 72	Payment deletion uncertain
	 * 73	Payment deletion refused
	 * 74	Payment deleted (not accepted)
	 * 75	Deletion processed by merchant
	 *  8	Refund
	 * 81	Refund pending
	 * 82	Refund uncertain
	 * 83	Refund refused
	 * 84	Payment declined by the acquirer (will be debited)
	 * 85	Refund processed by merchant
	 * 	9	Payment requested
	 * 91	Payment processing
	 * 92	Payment uncertain
	 * 93	Payment refused
	 * 94	Refund declined by the acquirer
	 * 95	Payment processed by merchant
	 * 97-99 Being processed (intermediate technical status)
	 */
	public function getOgoneStatusString($statusId)
	{
		$status = array();
		$status[0] 	= 'Incomplete or invalid';
		$status[1]  = 'Cancelled by client';
		$status[2]  = 'Authorization refused';
		$status[4]  = 'Order stored';
		$status[41] = 'Waiting client payment';
		$status[5]  = 'Authorized';
		$status[51] = 'Authorization waiting';
		$status[52] = 'Authorization not known';
		$status[59] = 'Author. to get manually';
		$status[6]  = 'Authorized and canceled';
		$status[61] = 'Author. deletion waiting';
		$status[62] = 'Author. deletion uncertain';
		$status[63] = 'Author. deletion refused';
	 	$status[7]  = 'Payment deleted';
	 	$status[71] = 'Payment deletion pending';
	 	$status[72] = 'Payment deletion uncertain';
	 	$status[73] = 'Payment deletion refused';
	 	$status[74] = 'Payment deleted (not accepted)';
	 	$status[75] = 'Deletion processed by merchant';
		$status[8]  = 'Refund';
		$status[81] = 'Refund pending';
		$status[82] = 'Refund uncertain';
		$status[83] = 'Refund refused';
		$status[84] = 'Payment declined by the acquirer (will be debited)';
		$status[85] = 'Refund processed by merchant';
		$status[9]  = 'Payment requested';
		$status[91] = 'Payment processing';
		$status[92] = 'Payment uncertain';
		$status[93] = 'Payment refused';
		$status[94] = 'Refund declined by the acquirer';
		$status[95] = 'Payment processed by merchant';
		$status[97] = 'Being processed (intermediate technical status)';		
		
		if (array_key_exists($statusId, $status)) {
			return $status[$statusId];
		}
		return false;
	}
}
