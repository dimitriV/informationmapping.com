<?php
/**
 * Madia
 *
 * @category   Madia
 * @package    Madia_Ogone
 * @copyright  Copyright (c) 2004-2007 Madia BB (http://www.madia.nl)
 */

/**
 * Api Debug
 *
 * @category   Madia
 * @package    Madia_Ogone
 * @name       Madia_Ogone_Model_Api_Debug
 * @author     Han Leenders <info@madia.nl>
 */
class Madia_Ogone_Model_Api_Debug extends Mage_Core_Model_Abstract
{
    protected function _construct()
    {
        $this->_init('ogone/api_debug');
    }
}