<?php
/**
 * Madia
 *
 * @category   Madia
 * @package    Madia_Ogone
 * @copyright  Copyright (c) 2008 Madia BV (http://www.madia.nl)
 */

/**
 * Redirect to Ogone
 *
 * @category    Madia
 * @package     Madia_Ogone
 * @name        Madia_Ogone_Block_Basic_Redirect
 * @author      Han Leenders <info@madia.nl>
 */

class Madia_Ogone_Block_Basic_Redirect extends Mage_Core_Block_Abstract
{

    protected function _toHtml()
    {
        $basic = Mage::getModel('ogone/basic');
        $form = new Varien_Data_Form();
        $form->setAction($basic->getOgoneUrl())
            ->setId('ogone_basic_checkout')
            ->setName('ogone_basic_checkout')
            ->setMethod('POST')
            ->setUseContainer(true);
            
        foreach ($basic->setOrder($this->getOrder())->getStandardCheckoutFormFields() as $field => $value) {
            $form->addField($field, 'hidden', array('name' => $field, 'value' => $value));
        }
        
        $html = '';
        
        if ($templateUrl = $basic->getDynamicTemplate()) {
        	
        	try {
        		$templateHtml = file_get_contents($templateUrl);
        	
        	
	        	$content = '<div align="center">';
	        	$content.= $this->__('You will be redirected in a few seconds.');
		        $content.= $form->toHtml();
		        $content.= '<script type="text/javascript">document.getElementById("ogone_basic_checkout").submit();</script>';
	        	$content.= '</div>';
		        
	        	$html = str_replace('$$$PAYMENT ZONE$$$', $content, $templateHtml);     	
        	} catch (Exception $e) {
        		//
        	}
        	
        }
        
        // fall back
        if (empty($html)) {
        
	        $html = '<html><body>';
	        $html.= $this->__('You will be redirected in a few seconds.');
	        $html.= $form->toHtml();
	        $html.= '<script type="text/javascript">document.getElementById("ogone_basic_checkout").submit();</script>';
	        $html.= '</body></html>';
        
        }

        return $html;
      
    }
    
}