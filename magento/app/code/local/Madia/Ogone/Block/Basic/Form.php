<?php
/**
 * Madia
 *
 * @category   Madia
 * @package    Madia_Ogone
 * @copyright  Copyright (c) 2008 Madia BV (http://www.madia.nl)
 */

/**
 * Form Block
 *
 * @category    Madia
 * @package     Madia_Ogone
 * @name        Madia_Ogone_Block_Standard_Form
 * @author      Han Leenders <info@madia.nl>
 */

class Madia_Ogone_Block_Basic_Form extends Mage_Payment_Block_Form
{
    protected function _construct()
    {
        $this->setTemplate('ogone/basic/form.phtml');
        parent::_construct();
    }
    
    /**
     * Retrieve payment configuration object
     *
     * @return Mage_Spplus_Model_Config
     */
    protected function _getConfig()
    {
        return Mage::getSingleton('ogone/config');
    }
    
    /**
     * Retrieve availables payment methodes
     *
     * @return array
     */
    public function getAvailablePaymentMethodes()
    {
        $types = $this->_getConfig()->getPaymentMethodesByBrand();

        if ($method = $this->getMethod()) {
        	
            $availableTypes = $method->getConfigData('paymentmethodes');
           
            if ($availableTypes) {
                $availableTypes = explode(',', $availableTypes);
                foreach ($types as $brand=>$name) {
                    if (!in_array($brand, $availableTypes)) {
                        unset($types[$brand]);
                    }
                }
            }
        }
        return $types;
    }
}