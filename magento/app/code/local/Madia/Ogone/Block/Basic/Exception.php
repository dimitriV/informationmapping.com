<?php
/**
 * Madia
 *
 * @category   Madia
 * @package    Madia_Ogone
 * @copyright  Copyright (c) 2008 Madia BV (http://www.madia.nl)
 */
/**
 * Failure Response from Ogone
 *
 * @category   Madia
 * @package    Madia_Ogone
 * @name       Madia_Ogone_Block_Basic_Failure
 * @author     Han Leenders <info@madia.nl>
*/

class Madia_Ogone_Block_Basic_Exception extends Mage_Core_Block_Template
{
}