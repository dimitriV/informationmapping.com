<?php
/**
 * Madia
 *
 * @category   Madia
 * @package    Madia_Ogone
 * @copyright  Copyright (c) 2008 Madia BV (http://www.madia.nl)
 */

/**
 * Data Helper
 *
 * @category   Madia
 * @package    Madia_Ogone
 * @name       Madia_Ogone_Helper_Data
 * @author     Han Leenders (Madia)
 */
class Madia_Ogone_Helper_Data extends Mage_Core_Helper_Abstract
{

}
