<?php
/**
 * Madia
 *
 * @category   Madia
 * @package    Madia_Ogone
 * @copyright  Copyright (c) 2008 Madia BV (http://www.madia.nl)
 */


$installer = $this;
/* @var $installer Madia_Ogone_Model_Mysql4_Setup */

$installer->addAttribute('order_payment', 'ogone_postsale_received', array('type'=>'int'));