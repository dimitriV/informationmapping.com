<?php
/**
 * Madia
 *
 * @category   Madia
 * @package    Madia_Ogone
 * @copyright  Copyright (c) 2008 Madia BV (http://www.madia.nl)
 */


$installer = $this;
/* @var $installer Madia_Ogone_Model_Mysql4_Setup */

$installer->addAttribute('order_payment', 'ogone_payment_brand', array('type'=>'varchar'));
$installer->addAttribute('order_payment', 'ogone_status', array('type'=>'varchar'));
$installer->addAttribute('order_payment', 'ogone_pay_id', array('type'=>'varchar'));