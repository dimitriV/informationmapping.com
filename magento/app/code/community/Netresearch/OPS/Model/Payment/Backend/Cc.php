<?php
/**
 * Netresearch_OPS_Model_Payment_Backend_Cc
 *
 * @package
 * @copyright 2012 Netresearch App Factory AG <http://www.netresearch.de>
 * @author    Thomas Birke <thomas.birke@netresearch.de>
 * @license   OSL 3.0
 */
class Netresearch_OPS_Model_Payment_Backend_Cc
    extends Netresearch_OPS_Model_Payment_Cc
{
    /** @var bool $_canUseInternal capture directly from the backend */
    protected $_canUseInternal = true;

    /** @var bool $_canUseCheckout disable backend payment method for checkout */
    protected $_canUseCheckout = false;

    /** @var string $_formBlockType define a specific form block */
    protected $_formBlockType = 'ops/form_cc';
}
