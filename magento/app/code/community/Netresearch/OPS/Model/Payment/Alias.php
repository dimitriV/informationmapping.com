<?php
/**
 * @category   OPS
 * @package    Netresearch_OPS
 * @author     Thomas Birke <thomas.birke@netresearch.de>
 * @author     Michael Lühr <michael.luehr@netresearch.de>
 * @copyright  Copyright (c) 2012 Netresearch GmbH & Co. KG
 * @license    http://opensource.org/licenses/osl-3.0.php  Open Software License (OSL 3.0)
 */

/**
 * Netresearch_OPS_Model_Payment_Alias
 *
 * @author     Thomas Birke <thomas.birke@netresearch.de>
 * @author     Michael Lühr <michael.luehr@netresearch.de>
 * @copyright  Copyright (c) 2012 Netresearch GmbH & Co. KG
 * @license    http://opensource.org/licenses/osl-3.0.php  Open Software License (OSL 3.0)
 */
class Netresearch_OPS_Model_Payment_Alias
    extends Netresearch_OPS_Model_Payment_Abstract
{
    /** payment code */
    protected $_code = 'ops_alias';

    protected $_formBlockType = 'ops/form_alias';

    protected $_infoBlockType = 'ops/info_alias';

    public function isAvailable($payment=null)
    {
        return parent::isAvailable() && $this->hasCustomerAliases();
    }

    public function hasCustomerAliases()
    {
        if (Mage::getSingleton('customer/session')->isLoggedIn()) {
            $customerId = Mage::getSingleton('customer/session')->getCustomer()->getId();
            $aliases = Mage::helper('ops/payment')->getAliasesForAddresses(
                $customerId,
                Mage::getSingleton('checkout/session')->getQuote()->getBillingAddress(),
                Mage::getSingleton('checkout/session')->getQuote()->getShippingAddress()
            );
            return 0 < $aliases->count();
        }
        return false;
    }

    /**
     * PM value is not used for payments with Alias Manager
     *
     * @param Mage_Sales_Model_Quote_Payment|null Payment
     * @return null
     */
    public function getOpsCode($payment=null)
    { }

    /**
     * BRAND value is not used for payments with Alias Manager
     *
     * @param Mage_Sales_Model_Quote_Payment|null Payment
     * @return null
     */
    public function getOpsBrand($payment=null)
    { }

    /**
     * Only redirect to Ogone if Alias requires 3DSecure redirect
     *
     * @param Mage_Sales_Model_Quote_Payment|null $payment
     * @return void
     */
    public function getOrderPlaceRedirectUrl($payment=null)
    {
        /*
         * Alias payments have always Interface support -> Inline method
         * f HTML_ANSWER exists and redirect to Ogone is necessary
         */
        if ('' == $this->getOpsHtmlAnswer($payment)) {
            return false; // Prevent redirect on cc payment
        } else {
            return Mage::getModel('ops/config')->get3dSecureRedirectUrl();
        }
    }
}

