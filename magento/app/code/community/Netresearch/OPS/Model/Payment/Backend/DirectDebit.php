<?php
/**
 * Netresearch_OPS_Model_Payment_Backend_DirectDebit
 *
 * @package   OPS
 * @copyright 2012 Netresearch App Factory AG <http://www.netresearch.de>
 * @author    Thomas Birke <thomas.birke@netresearch.de>
 * @license   OSL 3.0
 */
class Netresearch_OPS_Model_Payment_Backend_DirectDebit
    extends Netresearch_OPS_Model_Payment_DirectDebit
{
    /** Check if we can capture directly from the backend */
    protected $_canUseInternal = true;

    /** disable backend payment method for checkout */
    protected $_canUseCheckout = false;

    /* define a specific form block */
    protected $_formBlockType = 'ops/form_directDebit';
}

