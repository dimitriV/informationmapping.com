<?php

class Netresearch_OPS_Test_Block_Form_CcTest extends EcomDev_PHPUnit_Test_Case
{

    public function testGetAliasBrands()
    {

        $aliasBrands = array(
            'American Express',
            'Billy',
            'Diners Club',
            'MaestroUK',
            'MasterCard',
            'VISA',
        );

        $ccAliasInterfaceEnabledTypesMock = $this->getModelMock('ops/source_cc_aliasInterfaceEnabledTypes', array('getAliasInterfaceCompatibleTypes'));
        $ccAliasInterfaceEnabledTypesMock->expects($this->any())
            ->method('getAliasInterfaceCompatibleTypes')
            ->will($this->returnValue($aliasBrands));
        $this->replaceByMock('model', 'ops/source_cc_aliasInterfaceEnabledTypes', $ccAliasInterfaceEnabledTypesMock);
        $ccForm = Mage::app()->getLayout()->getBlockSingleton('ops/form_cc');
        $ccAliases = $ccForm->getAliasBrands();
        $this->assertEquals($aliasBrands, $ccAliases);
    }
}
