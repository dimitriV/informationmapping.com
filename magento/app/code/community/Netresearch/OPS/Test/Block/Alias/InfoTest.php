<?php
/**
 * @category   OPS
 * @package    Netresearch_OPS
 * @author     Thomas Birke <thomas.birke@netresearch.de>
 * @author     Michael Lühr <michael.luehr@netresearch.de>
 * @copyright  Copyright (c) 2012 Netresearch GmbH & Co. KG
 * @license    http://opensource.org/licenses/osl-3.0.php  Open Software License (OSL 3.0)
 */

/**
 * Netresearch_OPS_Test_Block_Alias_InfoTest
 *
 * @author     Thomas Birke <thomas.birke@netresearch.de>
 * @author     Michael Lühr <michael.luehr@netresearch.de>
 * @copyright  Copyright (c) 2012 Netresearch GmbH & Co. KG
 * @license    http://opensource.org/licenses/osl-3.0.php  Open Software License (OSL 3.0)
 */
class Netresearch_OPS_Test_Block_Alias_InfoTest
    extends EcomDev_PHPUnit_Test_Case
{
    protected function getBlock()
    {
        return Mage::app()->getLayout()->getBlockSingleton('ops/alias_info');
    }

    public function testOutputIfDisabledAndLoggedOut()
    {
        $this->setOutputEnabled(false);
        $this->setCustomerId(null);
        $this->assertNull($this->getOutput(), 'expected no output');
    }

    public function testOutputIfDisabledAndLoggedIn()
    {
        $this->setOutputEnabled(false);
        $this->setCustomerId(123);
        $this->assertNull($this->getOutput(), 'expected no output');
    }

    public function testOutputIfEnabledAndLoggedIn()
    {
        $this->setOutputEnabled(true);
        $this->setCustomerId(123);
        $this->assertNull($this->getOutput(), 'expected no output');
    }

    public function testOutputIfEnabledAndLoggedOut()
    {
        $blockMock = $this->getBlockMock('ops/alias_info', array('renderView'));
        $blockMock->expects($this->any())
            ->method('renderView')
            ->will($this->returnValue('foo'));
        $this->replaceByMock('block', 'ops/alias_info', $blockMock);
        $this->setOutputEnabled(true);
        $this->setCustomerId(null);
        $this->assertTrue(0 < strlen($blockMock->toHtml()), 'expected output');
    }

    protected function setOutputEnabled($value)
    {
        $config = $this->getModelMock('ops/config', array('isAliasInfoBlockEnabled'));
        $config->expects($this->any())
            ->method('isAliasInfoBlockEnabled')
            ->will($this->returnValue($value));
        $this->replaceByMock('model', 'ops/config', $config);
    }

    protected function getOutput()
    {
        return $this->getToHtmlMethod()->invoke($this->getBlock());
    }

    protected function getToHtmlMethod()
    {
        $method = new ReflectionMethod($this->getBlock(), '_toHtml');
        $method->setAccessible(true);
        return $method;
    }

    protected function setCustomerId($value)
    {
        $customer = $this->getModelMock('customer/customer', array('getId'));
        $customer->expects($this->any())
            ->method('getId')
            ->will($this->returnValue($value));

        $customerHelper = $this->getHelperMock('customer/data', array('getCustomer'));
        $customerHelper->expects($this->any())
            ->method('getCustomer')
            ->will($this->returnValue($customer));
        $this->replaceByMock('helper', 'customer/data', $customerHelper);
    }
}

