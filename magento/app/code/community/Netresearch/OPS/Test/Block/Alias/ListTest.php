<?php
/**
 * @category   OPS
 * @package    Netresearch_OPS
 * @author     Thomas Birke <thomas.birke@netresearch.de>
 * @author     Michael Lühr <michael.luehr@netresearch.de>
 * @copyright  Copyright (c) 2012 Netresearch GmbH & Co. KG
 * @license    http://opensource.org/licenses/osl-3.0.php  Open Software License (OSL 3.0)
 */

/**
 * Netresearch_OPS_Test_Block_Alias_ListTest
 *
 * @author     Thomas Birke <thomas.birke@netresearch.de>
 * @author     Michael Lühr <michael.luehr@netresearch.de>
 * @copyright  Copyright (c) 2012 Netresearch GmbH & Co. KG
 * @license    http://opensource.org/licenses/osl-3.0.php  Open Software License (OSL 3.0)
 */
class Netresearch_OPS_Test_Block_Alias_ListTest
    extends EcomDev_PHPUnit_Test_Case
{
    private $_block;

    public function setUp()
    {
        parent::setup();
        $this->_block = Mage::app()->getLayout()->getBlockSingleton('ops/alias_list');
    }

    public function testGetAliases()
    {
        $this->markTestIncomplete();
    }

    public function testGetMethodName()
    {
        $this->assertNull($this->_block->getMethodName('something_stupid'));

        Mage::app()->getStore()->setConfig('payment/ops_cc/title', 'OPS Credit Card');
        $this->assertEquals(
            'OPS Credit Card',
            $this->_block->getMethodName('ops_cc')
        );
    }
    
}

