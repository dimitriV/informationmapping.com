<?php
class Netresearch_OPS_Test_Block_FormTest extends EcomDev_PHPUnit_Test_Case
{
    private $_block;

    public function setUp()
    {
        parent::setup();
        $this->_block = Mage::app()->getLayout()->getBlockSingleton('ops/form');
    }

    public function testGetCcBrands()
    {
        $this->assertInternalType('array', $this->_block->getCcBrands());
    }

    public function testGetDirectDebitCountryIds()
    {
        $this->assertInternalType('array', $this->_block->getDirectDebitCountryIds());
    }

    public function testIsAliasPMAvailable()
    {
        $modelMock = $this->getModelMock('ops/payment_alias', array('isAvailable'));
        $modelMock->expects($this->once())
            ->method('isAvailable')
            ->will($this->returnValue(true));
        $this->replaceByMock('model', 'ops/payment_alias', $modelMock);
        $this->assertTrue($this->_block->isAliasPMAvailable());

        $modelMock = $this->getModelMock('ops/payment_alias', array('isAvailable'));
        $modelMock->expects($this->once())
            ->method('isAvailable')
            ->will($this->returnValue(false));
        $this->replaceByMock('model', 'ops/payment_alias', $modelMock);
        $this->assertFalse($this->_block->isAliasPMAvailable());
    }

}
