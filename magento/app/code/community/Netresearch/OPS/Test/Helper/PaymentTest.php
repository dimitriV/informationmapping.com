<?php
class Netresearch_OPS_Test_Helper_PaymentTest extends EcomDev_PHPUnit_Test_Case
{
    private $_helper;
    private $store;

    public function setUp()
    {
        parent::setup();
        $this->_helper = Mage::helper('ops/payment');
        $this->store  = Mage::app()->getStore(0)->load(0);
    }

    public function testIsPaymentAuthorizeType()
    {
        $this->assertTrue($this->_helper->isPaymentAuthorizeType(Netresearch_OPS_Model_Payment_Abstract::OPS_AUTHORIZED));
        $this->assertTrue($this->_helper->isPaymentAuthorizeType(Netresearch_OPS_Model_Payment_Abstract::OPS_AUTHORIZED_WAITING));
        $this->assertTrue($this->_helper->isPaymentAuthorizeType(Netresearch_OPS_Model_Payment_Abstract::OPS_AUTHORIZED_UNKNOWN));
        $this->assertTrue($this->_helper->isPaymentAuthorizeType(Netresearch_OPS_Model_Payment_Abstract::OPS_AWAIT_CUSTOMER_PAYMENT));
        $this->assertFalse($this->_helper->isPaymentAuthorizeType(0));
    }

    public function testIsPaymentCaptureType()
    {
        $this->assertTrue($this->_helper->isPaymentCaptureType(Netresearch_OPS_Model_Payment_Abstract::OPS_PAYMENT_REQUESTED));
        $this->assertTrue($this->_helper->isPaymentCaptureType(Netresearch_OPS_Model_Payment_Abstract::OPS_PAYMENT_PROCESSING));
        $this->assertTrue($this->_helper->isPaymentCaptureType(Netresearch_OPS_Model_Payment_Abstract::OPS_PAYMENT_UNCERTAIN));
        $this->assertFalse($this->_helper->isPaymentCaptureType(0));
    }

    /**
     * send no invoice mail if it is denied by configuration
     */
    public function testSendNoInvoiceToCustomerIfDenied()
    {
        $this->store->setConfig('payment_services/ops/send_invoice', 0);
        $this->assertFalse(Mage::getModel('ops/config')->getSendInvoice());
        $sentInvoice = $this->getModelMock('sales/order_invoice', array('getEmailSent', 'sendEmail'));
        $sentInvoice->expects($this->any())
            ->method('getEmailSent')
            ->will($this->returnValue(false));
        $sentInvoice->expects($this->never())
            ->method('sendEmail');
        $this->_helper->sendInvoiceToCustomer($sentInvoice);
    }

    /**
     * send no invoice mail if it was already sent
     */
    public function testSendNoInvoiceToCustomerIfAlreadySent()
    {
        $this->store->setConfig('payment_services/ops/send_invoice', 1);
        $this->assertTrue(Mage::getModel('ops/config')->getSendInvoice());
        $someInvoice = $this->getModelMock('sales/order_invoice', array('getEmailSent', 'sendEmail'));
        $someInvoice->expects($this->any())
            ->method('getEmailSent')
            ->will($this->returnValue(true));
        $someInvoice->expects($this->never())
            ->method('sendEmail');
        $this->_helper->sendInvoiceToCustomer($someInvoice);
    }

    /**
     * send invoice mail
     */
    public function testSendInvoiceToCustomerIfEnabled()
    {
        $this->store->setConfig('payment_services/ops/send_invoice', 1);
        $this->assertTrue(Mage::getModel('ops/config')->getSendInvoice());
        $anotherInvoice = $this->getModelMock('sales/order_invoice', array('getEmailSent', 'sendEmail'));
        $anotherInvoice->expects($this->any())
            ->method('getEmailSent')
            ->will($this->returnValue(false));
        $anotherInvoice->expects($this->once())
            ->method('sendEmail')
            ->with($this->equalTo(true));
        $this->_helper->sendInvoiceToCustomer($anotherInvoice);
    }

    public function testPrepareParamsAndSort()
    {
        $params = array(
            'CVC'          => '123',
            'CARDNO'       => '4111111111111111',
            'CN'           => 'JohnSmith',
            'PSPID'        => 'test1',
            'ED'           => '1212',
            'ACCEPTURL'    => 'https=//www.myshop.com/ok.html',
            'EXCEPTIONURL' => 'https=//www.myshop.com/nok.html',
            'BRAND'        => 'VISA',
        );
        $sortedParams = array(
            'ACCEPTURL'    => array('key' => 'ACCEPTURL',    'value' => 'https=//www.myshop.com/ok.html'),
            'BRAND'        => array('key' => 'BRAND',        'value' => 'VISA'),
            'CARDNO'       => array('key' => 'CARDNO',       'value' => '4111111111111111'),
            'CN'           => array('key' => 'CN',           'value' => 'JohnSmith'),
            'CVC'          => array('key' => 'CVC',          'value' => '123'),
            'ED'           => array('key' => 'ED',           'value' => '1212'),
            'EXCEPTIONURL' => array('key' => 'EXCEPTIONURL', 'value' => 'https=//www.myshop.com/nok.html'),
            'PSPID'        => array('key' => 'PSPID',        'value' => 'test1'),
        );
        $secret = 'Mysecretsig1875!?';
        $shaInSet = 'ACCEPTURL=https=//www.myshop.com/ok.htmlMysecretsig1875!?BRAND=VISAMysecretsig1875!?'
            . 'CARDNO=4111111111111111Mysecretsig1875!?CN=JohnSmithMysecretsig1875!?CVC=123Mysecretsig1875!?'
            . 'ED=1212Mysecretsig1875!?EXCEPTIONURL=https=//www.myshop.com/nok.htmlMysecretsig1875!?'
            . 'PSPID=test1Mysecretsig1875!?';
        $key = 'a28dc9fe69b63fe81da92471fefa80aca3f4851a';
        $this->assertEquals($sortedParams, $this->_helper->prepareParamsAndSort($params));
        $this->assertEquals($shaInSet, $this->_helper->getSHAInSet($params, $secret));
        $this->assertEquals($key, $this->_helper->shaCrypt($shaInSet, $secret));
    }

    public function testSaveAliasIfCustomerIsNotLoggedIn()
    {
        $quote = Mage::getModel('sales/quote');
        $this->assertEquals(null, $this->_helper->saveAlias(array('OrderID' => 4711)));
    }

    public function testSaveAliasIfCustomerIsLoggedIn()
    {
        $quote = new Varien_Object();
        $quote->setBillingAddress($this->getAddressData());
        $quote->setShippingAddress($this->getAddressData());
        $quote->setId(4711);
        $customer = new Varien_Object();
        $customer->setId(1);
        $aliasData['OrderID'] = 4711;
        $aliasData['Alias'] = 4711;
        $aliasData['Brand'] = 'Visa';
        $aliasData['CardNo'] = 'xxxx0815';
        $aliasData['ED'] = '1212';
        $payment = new Varien_Object();
        $payment->setMethod('CreditCard');
        $payment->setAdditionalInformation(array('saveOpsAlias' => '1'));
        $quote->setCustomer($customer);
        $quote->setPayment($payment);
        
        $quoteMock = $this->getModelMock('sales/quote', array('load'));
        $quoteMock->expects($this->any())
            ->method('load')
            ->will($this->returnValue($quote));
        $this->replaceByMock('model', 'sales/quote', $quoteMock);
        $alias = $this->_helper->saveAlias($aliasData);
        $this->assertEquals('4711', $alias->getAlias());
        $this->assertEquals('Visa', $alias->getBrand());
        $this->assertEquals('xxxx0815', $alias->getPseudoAccountOrCcNo());
        $this->assertEquals('1212', $alias->getExpirationDate());
        $this->assertEquals('1b9ecdf409e240717f04b7155712658ab09116bb', $alias->getBillingAddressHash());
        $this->assertEquals('1b9ecdf409e240717f04b7155712658ab09116bb', $alias->getShippingAddressHash());
        $this->assertEquals('CreditCard', $alias->getPaymentMethod());
        $this->assertEquals(1, $alias->getCustomerId());

        $oldAliasId = $alias->getId();

        $alias = $this->_helper->saveAlias($aliasData, $quote);
        $this->assertEquals($oldAliasId, $alias->getId());
    }

    public function testGenerateAddressHash()
    {
        $address = $this->getAddressData();
        $this->assertEquals('1b9ecdf409e240717f04b7155712658ab09116bb', $this->_helper->generateAddressHash($address));
    }

    protected function getAddressData()
    {
        $address = new Mage_Sales_Model_Quote_Address();
        $address->setFirstname('foo');
        $address->setLastname('bert');
        $address->setStreet1('bla street 1');
        $address->setZipcode('4711');
        $address->setCity('Cologne');
        $address->setCountry_id(1);
        return $address;
    }

    /**
     * @loadFixture ../../../var/fixtures/aliases.yaml
     */
    public function testGetAliasesForCustomer()
    {

        $aliasesCollection = $this->_helper->getAliasesForCustomer(1);
        $this->assertEquals(4, count($aliasesCollection));

        $aliasesCollection = $this->_helper->getAliasesForCustomer(2);
        $this->assertEquals(1, count($aliasesCollection));

        $aliasesCollection = $this->_helper->getAliasesForCustomer(3);
        $this->assertEquals(0, count($aliasesCollection));

        $aliasesCollection = $this->_helper->getAliasesForCustomer(null);
        $this->assertEquals(0, count($aliasesCollection));
    }

    /**
     * @loadFixture ../../../var/fixtures/aliases.yaml
     */
    public function testIsAliasValidForAddresses()
    {
        $billingAddress = Mage::getModel('sales/quote_address');
        $shippingAddress = Mage::getModel('sales/quote_address');
        $this->assertFalse($this->_helper->isAliasValidForAddresses(1, '4711', $shippingAddress, $billingAddress));

        $billingAddress = $this->getAddressData();
        $shippingAddress = $this->getAddressData();

        $this->assertTrue($this->_helper->isAliasValidForAddresses(1, '4711', $shippingAddress, $billingAddress));

        $this->assertFalse($this->_helper->isAliasValidForAddresses(2, '4711', $shippingAddress, $billingAddress));
    }
    
    
    public function testHandleUnknownStatus()
    {
        $order = $this->getModelMock('sales/order', array('save'));
        $order->expects($this->any())
            ->method('save')
            ->will($this->returnValue(true));
        $order->setState(
                Mage_Sales_Model_Order::STATE_NEW,
                Mage_Sales_Model_Order::STATE_NEW
                );
        $statusHistoryCount = $order->getStatusHistoryCollection()->count();
        Mage::helper('ops/payment')->handleUnknownStatus($order);
        $this->assertEquals(Mage_Sales_Model_Order::STATE_PENDING_PAYMENT, $order->getState());
        $this->assertTrue($statusHistoryCount < $order->getStatusHistoryCollection()->count());
        $statusHistoryCount = $order->getStatusHistoryCollection()->count();
        $order->setState(
                Mage_Sales_Model_Order::STATE_PROCESSING,
                Mage_Sales_Model_Order::STATE_PROCESSING
                );
        
        Mage::helper('ops/payment')->handleUnknownStatus($order);
        $this->assertEquals(Mage_Sales_Model_Order::STATE_PROCESSING, $order->getState());
        $this->assertTrue($statusHistoryCount < $order->getStatusHistoryCollection()->count());
    }

    /**
     * @loadFixture ../../../var/fixtures/orders.yaml
     * @loadFixture ../../../var/fixtures/quotes.yaml
     */
    public function testGetBaseGrandTotalFromSalesObject()
    {
        $helper = Mage::helper('ops/payment');
        $order = Mage::getModel('sales/order')->load(14);
        $amount = $helper->getBaseGrandTotalFromSalesObject($order);
        $this->assertEquals($order->getBaseGrandTotal(), $amount);
        $order = Mage::getModel('sales/order')->load(15);
        $amount = $helper->getBaseGrandTotalFromSalesObject($order);
        $this->assertEquals($order->getBaseGrandTotal(), $amount);
        $quote = Mage::getModel('sales/quote')->load(1);
        $amount = $helper->getBaseGrandTotalFromSalesObject($quote);
        $this->assertEquals($quote->getBaseGrandTotal(), $amount);
        $quote = Mage::getModel('sales/quote')->load(2);
        $amount = $helper->getBaseGrandTotalFromSalesObject($quote);
        $this->assertEquals($quote->getBaseGrandTotal(), $amount);
        $someOtherObject = new Varien_Object();
        $this->setExpectedException('Mage_Core_Exception');
        $helper->getBaseGrandTotalFromSalesObject($someOtherObject);
    }
    
}
