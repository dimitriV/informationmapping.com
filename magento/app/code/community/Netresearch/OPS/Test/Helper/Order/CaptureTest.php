<?php
class Netresearch_OPS_Test_Helper_Order_CaptureTest extends EcomDev_PHPUnit_Test_Case
{

    /**
     * @loadFixture ../../../../var/fixtures/orders.yaml
     */
    public function testDetermineIsPartial()
    {
        $helper = Mage::helper('ops/order_capture');
        $payment = new Varien_Object();
        $order = Mage::getModel('sales/order')->load(11);

        $payment->setOrder($order);
        $amount = $order->getBaseGrandTotal();
        $this->assertFalse($helper->determineIsPartial($payment, $amount));
        $amount = $amount - 0.01;
        $this->assertTrue($helper->determineIsPartial($payment, $amount));
    }

}