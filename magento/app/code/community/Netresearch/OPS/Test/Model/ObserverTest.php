<?php
class Netresearch_OPS_Test_Model_ObserverTest extends EcomDev_PHPUnit_Test_Case
{
    private $_model;
 
    public function setUp()
    {
        parent::setup();
        $this->_model = Mage::getModel('ops/observer');
    }
    
    public function testType()
    {
        $this->assertInstanceOf('Netresearch_OPS_Model_Observer', $this->_model);
    }

    public function testIsCheckoutWithAliasOrDd()
    {
        if (version_compare(PHP_VERSION, '5.3.2') >= 0) {
            $class = new ReflectionClass('Netresearch_OPS_Model_Observer');
            $method = $class->getMethod('isCheckoutWithAliasOrDd');
            $method->setAccessible(true);
         
            $this->assertTrue($method->invokeArgs($this->_model,array('ops_cc')));
            $this->assertTrue($method->invokeArgs($this->_model,array('ops_directDebit')));
            $this->assertFalse($method->invokeArgs($this->_model,array('checkmo')));
        }
    }
    
    
    public function testPerformDirectLinkRequestWithUnknownResponse()
    {
        $quote = new Varien_Object();
        $payment = $this->getModelMock('sales/quote_payment', array('save'));
        $payment->expects($this->any())
              ->method('save')
              ->will($this->returnValue(null));
        $this->replaceByMock('model', 'sales/quote_payment', $payment);
        $quote->setPayment($payment);
        $response = null;
        $directLinkMock = $this->getModelMock('ops/api_directlink', array('performRequest'));
        $directLinkMock->expects($this->any())
            ->method('performRequest')
            ->will($this->returnValue($response));
        $this->replaceByMock('model', 'ops/api_directlink', $directLinkMock);
        $observer = Mage::getModel('ops/observer');
        $observer->performDirectLinkRequest($quote, array());
        $this->assertFalse($this->setExpectedException('PHPUnit_Framework_ExpectationFailedException'));
        $this->assertTrue(array_key_exists('ops_response', $quote->getPayment()->getAdditionalInformation()));
    }
    
    public function testPerformDirectLinkRequestWithInvalidResponse()
    {
        $quote = new Varien_Object();
        $payment = $this->getModelMock('sales/quote_payment', array('save'));
        $payment->expects($this->any())
              ->method('save')
              ->will($this->returnValue(null));
        $this->replaceByMock('model', 'sales/quote_payment', $payment);
        $quote->setPayment($payment);
        $response = '';
        $directLinkMock = $this->getModelMock('ops/api_directlink', array('performRequest'));
        $directLinkMock->expects($this->any())
            ->method('performRequest')
            ->will($this->returnValue($response));
        $this->replaceByMock('model', 'ops/api_directlink', $directLinkMock);
        $observer = Mage::getModel('ops/observer');
        $this->assertTrue($this->setExpectedException('PHPUnit_Framework_ExpectationFailedException'));
        $observer->performDirectLinkRequest($quote, array());
        $this->assertFalse(array_key_exists('ops_response', $quote->getPayment()->getAdditionalInformation()));
    }
    
    public function testPerformDirectLinkRequestWithValidResponse()
    {
        $quote = new Varien_Object();
        $payment = $this->getModelMock('sales/quote_payment', array('save'));
        $payment->expects($this->any())
              ->method('save')
              ->will($this->returnValue(null));
        $this->replaceByMock('model', 'sales/quote_payment', $payment);
        $quote->setPayment($payment);
        $response = array('STATUS' => Netresearch_OPS_Model_Payment_Abstract::OPS_AUTHORIZED);
        $directLinkMock = $this->getModelMock('ops/api_directlink', array('performRequest'));
        $directLinkMock->expects($this->any())
            ->method('performRequest')
            ->will($this->returnValue($response));
        $this->replaceByMock('model', 'ops/api_directlink', $directLinkMock);
        $observer = Mage::getModel('ops/observer');
        $this->assertFalse($this->setExpectedException('PHPUnit_Framework_ExpectationFailedException'));
        $observer->performDirectLinkRequest($quote, array());
        $this->assertTrue(array_key_exists('ops_response', $quote->getPayment()->getAdditionalInformation()));
    }
    
    public function testPerformDirectLinkRequestWithValidResponseButInvalidStatus()
    {
        $quote = new Varien_Object();
        $payment = $this->getModelMock('sales/quote_payment', array('save'));
        $payment->expects($this->any())
              ->method('save')
              ->will($this->returnValue(null));
        $this->replaceByMock('model', 'sales/quote_payment', $payment);
        $quote->setPayment($payment);
        $response = array('STATUS' => Netresearch_OPS_Model_Payment_Abstract::OPS_AUTH_REFUSED);
        $directLinkMock = $this->getModelMock('ops/api_directlink', array('performRequest'));
        $directLinkMock->expects($this->any())
            ->method('performRequest')
            ->will($this->returnValue($response));
        $this->replaceByMock('model', 'ops/api_directlink', $directLinkMock);
        $observer = Mage::getModel('ops/observer');
        $this->assertTrue($this->setExpectedException('PHPUnit_Framework_ExpectationFailedException'));
        $observer->performDirectLinkRequest($quote, array());
        $this->assertFalse(array_key_exists('ops_response', $quote->getPayment()->getAdditionalInformation()));
    }
    
}

