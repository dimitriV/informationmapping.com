<?php
/**
 * @category   OPS
 * @package    Netresearch_OPS
 * @author     Thomas Birke <thomas.birke@netresearch.de>
 * @author     Michael Lühr <michael.luehr@netresearch.de>
 * @copyright  Copyright (c) 2012 Netresearch GmbH & Co. KG
 * @license    http://opensource.org/licenses/osl-3.0.php  Open Software License (OSL 3.0)
 */

/**
 * Netresearch_OPS_Test_Model_Payment_AliasTest
 *
 * @author     Thomas Birke <thomas.birke@netresearch.de>
 * @author     Michael Lühr <michael.luehr@netresearch.de>
 * @copyright  Copyright (c) 2012 Netresearch GmbH & Co. KG
 * @license    http://opensource.org/licenses/osl-3.0.php  Open Software License (OSL 3.0)
 */
class Netresearch_OPS_Test_Model_Payment_AliasTest extends EcomDev_PHPUnit_Test_Case_Config
{
    public function testClassExists()
    {
        $this->assertModelAlias('ops/payment_alias', 'Netresearch_OPS_Model_Payment_Alias');
        $this->assertTrue(Mage::getModel('ops/payment_alias') instanceof Netresearch_OPS_Model_Payment_Alias);
        $this->assertTrue(Mage::getModel('ops/payment_alias') instanceof Netresearch_OPS_Model_Payment_Abstract);
    }

    public function testMethodConfig()
    {
        $this->assertConfigNodeValue('default/payment/ops_alias/model', 'ops/payment_alias');
    }

    public function testOpsCode()
    {
        $this->assertNull(Mage::getModel('ops/payment_alias')->getOpsCode());
    }

    public function testOpsBrand()
    {
        $this->assertNull(Mage::getModel('ops/payment_alias')->getOpsBrand());
    }
}

