<?php
class Netresearch_OPS_Test_Model_Payment_AbstractTest extends EcomDev_PHPUnit_Test_Case_Controller
{
    /**
     * @test
     */
    public function _getOrderDescription()
    {
        $items = array(
            new Varien_Object(array(
                'parent_item' => false,
                'name'       => 'abc'
            )),
            new Varien_Object(array(
                'parent_item' => true,
                'name'       => 'def'
            )),
            new Varien_Object(array(
                'parent_item' => false,
                'name'       => 'ghi'
            )),
            new Varien_Object(array(
                'parent_item' => false,
                'name'       => 'Dubbelwerkende cilinder Boring ø70 Stang ø40 3/8'
            )),
            new Varien_Object(array(
                'parent_item' => false,
                'name'       => '0123456789012345678901234567890123456789012xxxxxx'
            )),
        );

        $order = $this->getModelMock('sales/order', array('getAllItems'));
        $order->expects($this->any())
            ->method('getAllItems')
            ->will($this->returnValue($items));

        $result = Mage::getModel('ops/payment_abstract')->_getOrderDescription($order);
        $this->assertEquals(
            'abc, ghi, Dubbelwerkende cilinder Boring 70 Stang 40 3/8, 012345678901234567890123456789012345678901',
            $result
        );
    }

    /**
     * check if payment methods return correct BRAND and PM values
     *
     * @loadExpectation paymentMethods
     * @test
     */
    public function shouldReturnCorrectBrandAndPMValues()
    {
        $paymentMethods = array(
            'acceptgiro',
            //'bankTransfer',      -> to be tested separately
            'cashU',
            'cbcOnline',
            //'cc',                -> to be tested separately
            'centeaOnline',
            'cod',
            //'dexiaDirectNet',    -> seems not to supported anymore
            //'directDebit',       -> to be tested separately
            //'directEbanking',    -> to be tested separately
            'eDankort',
            'eps',
            'fortisPayButton',
            'giroPay',
            'iDeal',
            'ingHomePay',
            //'interSolve',        -> to be tested separately
            'kbcOnline',
            //'miniTix',           -> seems not to supported anymore
            'openInvoiceDe',
            'openInvoiceNl',
            'paypal',
            'paysafecard',
            'pingPing',
            'postFinanceEFinance',
            'tunz',
            'wallie',
        );
        foreach ($paymentMethods as $methodIdentifier) {
            $method = Mage::getModel('ops/payment_' . $methodIdentifier);
            $this->assertEquals(
                $this->expected('ops_' . $methodIdentifier)->getBrand(),
                $method->getOpsBrand(null)
            );
            $this->assertEquals(
                $this->expected('ops_' . $methodIdentifier)->getPm(),
                $method->getOpsCode()
            );
        }
    }

    /**
     * check if payment method BankTransfer returns correct BRAND and PM values
     *
     * @loadExpectation paymentMethods
     * @test
     */
    public function shouldReturnCorrectBrandAndPMValuesForBankTransfer()
    {
        $method  = Mage::getModel('ops/payment_bankTransfer');

        $payment = $this->getModelMock('sales/quote_payment', array('getId'));
        $payment->expects($this->any())
            ->method('getId')
            ->will($this->returnValue('1'));
        $this->replaceByMock('model', 'sales/quote_payment', $payment);
        $quote  = $this->getModelMock('sales/quote', array('getPayment', 'getQuoteId'));
        $quote->expects($this->any())
            ->method('getQuoteId')
            ->will($this->returnValue('321'));
        $quote->expects($this->any())
            ->method('getPayment')
            ->will($this->returnValue($payment));

        $session = Mage::getSingleton(
            'checkout/session',
            array(
                'last_real_order_id' => '123',
                'quote'              => $quote
            )
        );

        $method = Mage::getModel('ops/payment_bankTransfer');
        try {
            $method->assignData(array('country_id' => 'DE'));
        } catch (Mage_Core_Exception $e) {
            if ('Cannot retrieve the payment information object instance.' != $e->getMessage()) {
                throw $e;
            }
        }
        $this->assertEquals(
            $this->expected('ops_bankTransferDe')->getBrand(),
            $method->getOpsBrand(null)
        );
        $reflectedMethod = new ReflectionMethod($method, 'getOpsCode');
        $reflectedMethod->setAccessible(true);
        $this->assertEquals(
            $this->expected('ops_bankTransferDe')->getPm(),
            $reflectedMethod->invoke($method)
        );
    }

    /**
     * @test
     * @loadFixture ../../../../var/fixtures/orders.yaml
     */
    public function testCanCancelManually()
    {
        $opsAbstractPayment = new Netresearch_OPS_Model_Payment_Abstract();
        
        //Check for successful can cancel (pending_payment and payment status 0)
        $order = Mage::getModel("sales/order")->load(11);
        $this->assertTrue($opsAbstractPayment->canCancelManually($order));
        
        //Check for successful cancel (pending_payment and payment status null/not existing)
        $order = Mage::getModel("sales/order")->load(14);
        $this->assertTrue($opsAbstractPayment->canCancelManually($order));
        
        //Check for denied can cancel (pending_payment and payment status 5)
        $order = Mage::getModel("sales/order")->load(12);
        $this->assertFalse($opsAbstractPayment->canCancelManually($order));
        
        //Check for denied can cancel (processing and payment status 0)
        $order = Mage::getModel("sales/order")->load(13);
        $this->assertFalse($opsAbstractPayment->canCancelManually($order));
    }
}
