<?php
class Netresearch_OPS_Test_Model_AliasTest extends EcomDev_PHPUnit_Test_Case
{
    public function testType()
    {
       $this->assertInstanceOf(
           'Netresearch_OPS_Model_Alias',
           Mage::getModel('ops/alias')
       );
    }
    
    /**
     * @loadFixture ../../../var/fixtures/aliases.yaml
     */
    public function testGetAliasesForCustomer()
    {
        
        $aliasesCollection = Mage::getModel('ops/alias')->getAliasesForCustomer(1);
        $this->assertEquals(4, count($aliasesCollection));
        
        $aliasesCollection = Mage::getModel('ops/alias')->getAliasesForCustomer(2);
        $this->assertEquals(1, count($aliasesCollection));
        
        $aliasesCollection = Mage::getModel('ops/alias')->getAliasesForCustomer(3);
        $this->assertEquals(0, count($aliasesCollection));
        
        $aliasesCollection = Mage::getModel('ops/alias')->getAliasesForCustomer(null);
        $this->assertEquals(0, count($aliasesCollection));
    }
}
