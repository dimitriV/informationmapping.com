<?php
/**
 * Netresearch_OPS_Block_Form_DirectDebit
 *
 * @package   OPS
 * @copyright 2012 Netresearch App Factory AG <http://www.netresearch.de>
 * @author    Thomas Birke <thomas.birke@netresearch.de>
 * @license   OSL 3.0
 */
class Netresearch_OPS_Block_Form_DirectDebit extends Netresearch_OPS_Block_Form
{
    protected function _construct()
    {
        parent::_construct();
        $this->setTemplate('ops/form/directDebit.phtml');
    }

    /**
     * get ids of supported countries
     *
     * @return array
     */
    public function getDirectDebitCountryIds()
    {
        return explode(',', $this->getConfig()->getDirectDebitCountryIds());
    }
}
