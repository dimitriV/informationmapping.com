<?php
/**
 * @category   OPS
 * @package    Netresearch_OPS
 * @author     Thomas Birke <thomas.birke@netresearch.de>
 * @author     Michael Lühr <michael.luehr@netresearch.de>
 * @copyright  Copyright (c) 2012 Netresearch GmbH & Co. KG
 * @license    http://opensource.org/licenses/osl-3.0.php  Open Software License (OSL 3.0)
 */

/**
 * Netresearch_OPS_Block_Alias_List
 *
 * @author     Thomas Birke <thomas.birke@netresearch.de>
 * @author     Michael Lühr <michael.luehr@netresearch.de>
 * @copyright  Copyright (c) 2012 Netresearch GmbH & Co. KG
 * @license    http://opensource.org/licenses/osl-3.0.php  Open Software License (OSL 3.0)
 */
class Netresearch_OPS_Block_Alias_List
    extends Mage_Core_Block_Template
{
    public function getAliases()
    {
        $aliases = array();
        $customer = Mage::helper('customer')->getCustomer();
        if (0 < $customer->getId()) {
            $aliasesCollection = Mage::helper('ops/payment')->getAliasesForCustomer($customer->getId());
            foreach ($aliasesCollection as $alias) {
                $aliases[] = $alias;
            }
        }
        return $aliases;
    }

    /**
     * get human readable name of payment method
     *
     * @param string $methodCode Code of payment method
     * @return string Name of payment method
     */
    public function getMethodName($methodCode)
    {
        $instance = Mage::helper('payment')->getMethodInstance($methodCode);
        if ($instance) {
            return $instance->getTitle();
        }
    }
}
