<?php
/**
 * @category   OPS
 * @package    Netresearch_OPS
 * @author     Thomas Birke <thomas.birke@netresearch.de>
 * @author     Michael Lühr <michael.luehr@netresearch.de>
 * @copyright  Copyright (c) 2012 Netresearch GmbH & Co. KG
 * @license    http://opensource.org/licenses/osl-3.0.php  Open Software License (OSL 3.0)
 */

/**
 * Netresearch_OPS_Block_Alias_Info
 *
 * @author     Thomas Birke <thomas.birke@netresearch.de>
 * @author     Michael Lühr <michael.luehr@netresearch.de>
 * @copyright  Copyright (c) 2012 Netresearch GmbH & Co. KG
 * @license    http://opensource.org/licenses/osl-3.0.php  Open Software License (OSL 3.0)
 */
class Netresearch_OPS_Block_Alias_Info
    extends Mage_Core_Block_Template
{
    protected $_template = "ops/alias_info.phtml";

    protected function _toHtml()
    {
        $customer = Mage::helper('customer/data')->getCustomer();
        if (Mage::getModel('ops/config')->isAliasInfoBlockEnabled()
            && (is_null($customer->getId()))
        ) {
            return parent::_toHtml();
        }
    }
}
