<?php
/**
 * Netresearch_OPS_AdminController
 * 
 * @package   OPS
 * @copyright 2012 Netresearch
 * @author    Thomas Birke <thomas.birke@netresearch.de> 
 * @license   OSL 3.0
 */
class Netresearch_OPS_Adminhtml_AdminController extends Mage_Adminhtml_Controller_Action
{
    protected function _isAllowed()
    {
        return true;
        return Mage::getSingleton('admin/session')
            ->isAllowed('sales/shipment');
    }

    public function saveAliasAction()
    {
        $alias = $this->_request->getParam('alias');
        $quote = Mage::getSingleton('admin/session_quote');
        if (0 < strlen($alias)) {
            $payment = $quote->getPayment();
            $payment->setAdditionalInformation('alias', $alias);
            Mage::log($this->_request->getParams(), null, 'ops_alias.log');
            Mage::log($quote->getId(), null, 'ops_alias.log');
            Mage::log($quote->getPayment()->getQuoteId(), null, 'ops_alias.log');
            Mage::log($quote->getPayment()->getAdditionalInformation(), null, 'ops_alias.log');
            Mage::helper('ops')->log('saved alias ' . $alias . ' for quote #' . $quote->getId());
            $payment->setDataChanges(true);
            $payment->save();
        } else {
            Mage::log('did not save alias due to empty alias:', null, 'ops_alias.log');
            Mage::log($this->_request->getParams(), null, 'ops_alias.log');
        }
    }
}
