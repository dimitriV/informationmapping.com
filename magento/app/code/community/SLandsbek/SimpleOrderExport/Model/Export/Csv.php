<?php
/**
 * NOTICE OF LICENSE
 *
 * The MIT License
 *
 * Copyright (c) 2009 Stefan Landsbek (slandsbek@gmail.com)
 *
 * Permission is hereby granted, free of charge, to any person obtaining a copy
 * of this software and associated documentation files (the "Software"), to deal
 * in the Software without restriction, including without limitation the rights
 * to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
 * copies of the Software, and to permit persons to whom the Software is
 * furnished to do so, subject to the following conditions:
 * 
 * The above copyright notice and this permission notice shall be included in
 * all copies or substantial portions of the Software.
 * 
 * THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
 * IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
 * FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
 * AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
 * LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
 * OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN
 * THE SOFTWARE.
 *
 * @package    SLandsbek_SimpleOrderExport
 * @copyright  Copyright (c) 2009 Stefan Landsbek (slandsbek@gmail.com)
 * @license    http://opensource.org/licenses/mit-license.php  The MIT License
 */

/**
 * Exports orders to csv file. If an order contains multiple ordered items, each item gets
 * added on a separate row.
 * 
 * @author Stefan Landsbek (slandsbek@gmail.com)
 */
class SLandsbek_SimpleOrderExport_Model_Export_Csv extends SLandsbek_SimpleOrderExport_Model_Export_Abstract
{
    const ENCLOSURE = '"';
    const DELIMITER = ',';
    const LICENSE_DELIMITER = ' | ';

    /**
     * Concrete implementation of abstract method to export given orders to csv file in var/export.
     *
     * @param $orders List of orders of type Mage_Sales_Model_Order or order ids to export.
     * @return String The name of the written csv file in var/export
     */
    public function exportOrders($orders) 
    {
        $fileName = 'order_export_'.date("Ymd_His").'.csv';
        $fp = fopen(Mage::getBaseDir('export').'/'.$fileName, 'w');

        $this->writeHeadRow($fp);
        foreach ($orders as $order) {
            $order = Mage::getModel('sales/order')->load($order);
            $this->writeOrder($order, $fp);
        }
        #exit;
        fclose($fp);

        return $fileName;
    }

    /**
	 * Writes the head row with the column names in the csv file.
	 * 
	 * @param $fp The file handle of the csv file
	 */
    protected function writeHeadRow($fp) 
    {
        fputcsv($fp, $this->getHeadRowValues(), self::DELIMITER, self::ENCLOSURE);
    }

    /**
	 * Writes the row(s) for the given order in the csv file.
	 * A row is added to the csv file for each ordered item. 
	 * 
	 * @param Mage_Sales_Model_Order $order The order to write csv of
	 * @param $fp The file handle of the csv file
	 */
    protected function writeOrder($order, $fp) 
    {
        $records = $this->getCommonOrderValues($order);
        
        foreach($records as $record){
            fputcsv($fp, $record, self::DELIMITER, self::ENCLOSURE);    
        }                
    }

    /**
	 * Returns the head column names.
	 * 
	 * @return Array The array containing all column names
	 */
    protected function getHeadRowValues() 
    {
        return array(
            'Order Number',
            /*'Orderitem ID',*/
            'Customer ID',
            'Information Mapping Partner',
            'Order Date',
            /*'Order Status',*/
            'License Keys',
            'Order Purchased From',
            /*'Order Payment Method',
            'Order Shipping Method',*/
            'Order Subtotal',
            'Order Tax',
            'Order Shipping',
            'Order Discount',
            'Order Grand Total',
            /*'Order Paid',
            'Order Refunded',
            'Order Due',*/
            'Total Qty Items Ordered',  
            
            'Item Price',
            'Item Qty Ordered',          
            'Item Subtotal',            
            
            'Customer Name',
            'Customer Email',
            /*'Shipping Name',
            'Shipping Company',
            'Shipping Street',
            'Shipping Zip',
            'Shipping City',
        	'Shipping State',
            'Shipping State Name',
            'Shipping Country',
            'Shipping Country Name',
            'Shipping Phone Number',*/
    		'Billing Name',
            'Billing Company',
            'Billing Street',
            'Billing Zip',
            'Billing City',
        	/*'Billing State',*/
            'Billing State Name',
            /*'Billing Country',*/
            'Billing Country Name',
            'Billing Phone Number',
            /*'Order Item Increment',*/
    		'Item Name',
            /*'Item Status',*/
            'Item SKU',
			'SMA Expiry Date',
            'Is Renewed',
            'Next Order Number',
            /*'Item Options',*/
            /*'Item Original Price',*/    	
        	/*'Item Qty Invoiced',
        	'Item Qty Shipped',
        	'Item Qty Canceled',
            'Item Qty Refunded',
            'Item Tax',
            'Item Discount',
    		'Item Total'*/
    	);
        
    }

    /**
	 * Returns the values which are identical for each row of the given order. These are
	 * all the values which are not item specific: order data, shipping address, billing
	 * address and order totals.
	 * 
	 * @param Mage_Sales_Model_Order $order The order to get values from
	 * @return Array The array containing the non item specific values
	 */
    protected function getCommonOrderValues($order) 
    {
        $shippingAddress = !$order->getIsVirtual() ? $order->getShippingAddress() : null;
        $billingAddress = $order->getBillingAddress();
		
	
        $orderItems = $order->getItemsCollection();
        $itemInc = 1;
        $no_of_items = 0;
        $order_array = array();
        foreach ($orderItems as $_item)
        {
            if (!$_item->isDummy()) 
            {
                $no_of_items += 1;
            }
        }
        foreach ($orderItems as $item)
        {
            if (!$item->isDummy()) 
            {
                #print_r($order->getData());
                #print_r($item->getData());
                $subtotal           = ($itemInc == $no_of_items) ? $this->formatPrice($order->getData('subtotal'), $order)  : '';
                $taxamount          = ($itemInc == $no_of_items) ? $this->formatPrice($order->getData('tax_amount'), $order) : '';
                $shipping_amount    = ($itemInc == $no_of_items) ? $this->formatPrice($order->getData('shipping_amount'), $order)  : '';
                $discount_amount    = ($itemInc == $no_of_items) ? $this->formatPrice($order->getData('discount_amount'), $order)  : '';
                $grand_total        = ($itemInc == $no_of_items) ? $this->formatPrice($order->getData('grand_total'), $order)  : '';
                $total_qty_ordered  = ($itemInc == $no_of_items) ? $this->getTotalQtyItemsOrdered($order)  : '';
                $item_subtotal      =  $item->getQtyOrdered() * $item->getData('price');
                $order_row = array(
                    $order->getRealOrderId(),
                    /*$order->getId(),*/
                    $order->getCustomerId(),
                    $this->_getPartnerNameByCustomerId($order->getCustomerId()),
                    Mage::helper('core')->formatDate($order->getCreatedAt(), 'medium', true),
                    /*$order->getStatus(),*/
                    $this->_getLicenseKeys($order->getId(), $item->getId()),
                    $this->getStoreName($order),
                    /*$this->getPaymentMethod($order),
                    $this->getShippingMethod($order),*/
                    $subtotal,
                    $taxamount,
                    $shipping_amount,
                    $discount_amount,
                    $grand_total,
                    /*$this->formatPrice($order->getData('total_paid'), $order),
                    $this->formatPrice($order->getData('total_refunded'), $order),
                    $this->formatPrice($order->getData('total_due'), $order),*/
                    $total_qty_ordered,
                    
                    $this->formatPrice($item->getData('price'), $order),
                    (int)$item->getQtyOrdered(),
                    $this->formatPrice($item_subtotal, $order),
                    
                    $order->getCustomerName(),
                    $order->getCustomerEmail(),
                    /*$shippingAddress ? $shippingAddress->getName() : '',
                    $shippingAddress ? $shippingAddress->getData("company") : '',
                    $shippingAddress ? str_replace("\n", " - ", $shippingAddress->getData("street")) : '',
                    $shippingAddress ? $shippingAddress->getData("postcode") : '',
                    $shippingAddress ? $shippingAddress->getData("city") : '',
                    $shippingAddress ? $shippingAddress->getRegionCode() : '',
                    $shippingAddress ? $shippingAddress->getRegion() : '',
                    $shippingAddress ? $shippingAddress->getCountry() : '',
                    $shippingAddress ? $shippingAddress->getCountryModel()->getName() : '',
                    $shippingAddress ? $shippingAddress->getData("telephone") : '',*/
                    $billingAddress->getName(),
                    $billingAddress->getData("company"),
                    str_replace("\n", " - ", $billingAddress->getData("street")),
                    $billingAddress->getData("postcode"),
                    $billingAddress->getData("city"),
                    /*$billingAddress->getRegionCode(),*/
                    $billingAddress->getRegion(),
                    /*$billingAddress->getCountry(),*/
                    $billingAddress->getCountryModel()->getName(),
                    $billingAddress->getData("telephone"),
                    $item->getName(),
                    $this->getItemSku($item),
					$this->_getSmaExpiryDate($order->getId(), $this->getItemSku($item)),
                    $this->_getSmaIsRenewed($order->getId(), $this->getItemSku($item)),
                    $this->_getNextOrderNumber($order->getId(), $this->getItemSku($item))	
                );                 
                $order_array[] =  $order_row; 
                $itemInc++;
            }
        }
        return $order_array;
    }

    /**
	 * Returns the item specific values.
	 * 
	 * @param Mage_Sales_Model_Order_Item $item The item to get values from
	 * @param Mage_Sales_Model_Order $order The order the item belongs to
	 * @return Array The array containing the item specific values
	 */
    protected function getOrderItemValues($item, $order, $itemInc=1) 
    {
        return array(
            /*$itemInc,*/
            $item->getName(),
            /*$item->getStatus(),*/
            $this->getItemSku($item),
            /*$this->getItemOptions($item),*/
            /*$this->formatPrice($item->getOriginalPrice(), $order),*/
            $this->formatPrice($item->getData('price'), $order),
            (int)$item->getQtyOrdered()
            /*(int)$item->getQtyInvoiced(),
            (int)$item->getQtyShipped(),
            (int)$item->getQtyCanceled(),
        	(int)$item->getQtyRefunded(),
            $this->formatPrice($item->getTaxAmount(), $order),
            $this->formatPrice($item->getDiscountAmount(), $order),
            $this->formatPrice($this->getItemTotal($item), $order)*/
        );
    }
    
    protected function _getPartnerNameByCustomerId($customer_id){        
        $_customer  = Mage::getModel('customer/customer')->load($customer_id);
        $partner_id = $_customer->getPartner();
        Mage::log('Parnter_ID: '.$partner_id);
        $connection = Mage::getSingleton('core/resource')->getConnection('core_read');
        $sql        = "SELECT 
                            name
                        FROM
                        	partner
                        WHERE 
                        	partner_id = ?";
        return $connection->fetchOne($sql, $partner_id);    
    }
    
    protected function _getLicenseKeys($order_id, $order_item_id){
        $serialNumbersCollection = Mage::getModel('serialproduct/serial')
            ->getCollection()
			->addFieldToFilter('order_id', $order_id)
            ->addFieldToFilter('order_item_id', $order_item_id)
			->addFieldToFilter('status', array('in', array(
					CLS_Serialproduct_Model_Serial::STATUS_ASSIGNED,
					/*CLS_Serialproduct_Model_Serial::STATUS_CANCELLED*/
			)));
        $serialNumbers = array();    
        foreach($serialNumbersCollection as $serialNumber){
            $serialNumbers[] = $serialNumber->getSerialNumber();
        }    
        $serialNumbersString = implode(self::LICENSE_DELIMITER, $serialNumbers);
        return $serialNumbersString;        
    }
	
	protected function _getSmaExpiryDate($order_id, $item_sku){
	    $expiry_date = "";
	    if(1 == Mage::getStoreConfig('smasubscription/option/active'))
        {
            if($this->_getSMAConfigsAll($order_id))
            {
                $sma_configs = $this->_getSMAConfigsAll($order_id);                
                $smaout_sku = $sma_configs[0];
                $sma_type = $sma_configs[1];
                $expiry_date = $this->_getSmaExpiryDateValue($order_id, $smaout_sku, $sma_type);
            }
        } 
        return $expiry_date;       
	}
    
    protected function _getSmaExpiryDateValue($order_id, $item_sku, $type)
	{
	   $connection = Mage::getSingleton('core/resource')->getConnection('core_read');
        
        $sql        = "SELECT 
                            sma_end_date
                        FROM
                        	smasubscription
                        WHERE 
                        	order_id = ?
							AND product_sku = ?
                            AND sma_type = ?";			
        return $connection->fetchOne($sql, array($order_id, $item_sku, $type));
	}
    
    protected function _getSmaIsRenewed($order_id, $item_sku){
	    $is_renewed = 0;
        //$real_order_id = Mage::getModel('sales/order')->load($order_id)->getRealOrderId();
        $_smasubscription = Mage::getModel('smasubscription/smasubscription')->load($order_id,'order_id');
        $is_renewed = $_smasubscription->getIsRenewed();
        
	    /*if(1 == Mage::getStoreConfig('smasubscription/option/active'))
        {
            Mage::helper('smasubscription')->log('Info:'.print_r($this->_getSMAConfigsAll($order_id), true));
            if($this->_getSMAConfigsAll($order_id))
            {
                $sma_configs = $this->_getSMAConfigsAll($order_id);
                
                $smaout_sku = $sma_configs[0];
                $sma_type = $sma_configs[1];
                $is_renewed = $this->_getSmaIsRenewedValue($order_id, $smaout_sku, $sma_type);
            }
        }*/ 
        return $is_renewed;       
	}
    
    protected function _getSmaIsRenewedValue($order_id, $item_sku, $sma_type)
    {
        $connection = Mage::getSingleton('core/resource')->getConnection('core_read');
        $sql        = "SELECT 
                            is_renewed
                        FROM
                        	smasubscription
                        WHERE 
                        	order_id = ?
							AND product_sku = ?
                            AND sma_type = ?
                            ORDER BY is_renewed DESC";
        return $connection->fetchOne($sql, array($order_id, $item_sku, $sma_type));
    }
    
    protected function _getNextOrderNumber($order_id, $item_sku)
    {
        $next_order = "";
	    if(1 == Mage::getStoreConfig('smasubscription/option/active'))
        {
            if($this->_getSMAConfigsAll($order_id))
            {
                $sma_configs = $this->_getSMAConfigsAll($order_id);
                $smaout_sku = $sma_configs[0];
                $sma_type = $sma_configs[1];
                $next_order = $this->_getNextOrderNumberValue($order_id, $smaout_sku, $sma_type);
            }
        }
        if(!$next_order) $next_order = 0; 
        else {
            $_smaInfo = Mage::getModel('smasubscription/smasubscription')->load($order_id,'order_id');
            if(!$_smaInfo->getNextOrderNumber()) 
            $_smaInfo->setNextOrderNumber($next_order)->save();
        }
        return $next_order;   
    }
    
    protected function _getNextOrderNumberValue($order_id, $item_sku, $sma_type)
    {
        $connection = Mage::getSingleton('core/resource')->getConnection('core_read');
        $sql        = "SELECT 
                            id, customer_id
                        FROM
                        	smasubscription
                        WHERE 
                        	order_id = ?
							AND product_sku = ?
                            AND sma_type = ?
                            AND is_renewed = 1";
        $result = $connection->fetchRow($sql, array($order_id, $item_sku, $sma_type));
        if(sizeof($result) > 0)
        {
            $sql_next_order = "SELECT 
                                order_no
                           FROM
                            	smasubscription
                           WHERE 
                                customer_id = ?
                                AND product_sku = ?
                                AND id > ?
                           ORDER BY id ASC
                           ";
            return $connection->fetchOne($sql_next_order, array($result['customer_id'], $item_sku, $result['id']));
        }
        else
            return 0;
        
    }
    
    public function _getSMAConfigsAll($order_id)
    {
        $order = Mage::getModel('sales/order');
		$order->load($order_id);
        
        //Check if the order has the items listed in SMA Subscription configuration        
        $skus           = array();  
        $oitems         = array();       
        $items          = $order->getAllItems();   
        foreach($items as $item){           
            $oitems[$item->getSku()] = $item->getQtyOrdered();               
        }  
        $skus           = array_keys($oitems);      
        //get skus from configuration
        $oneyear_sma_skus   = array();
        $twoyear_sma_skus   = array();
        $threeyear_sma_skus = array();
        /*$oneyear_sma_skus_out = array();
        $twoyear_sma_skus_out = array();
        $threeyear_sma_skus_out = array();*/
        
        if(1 == Mage::getStoreConfig('smasubscription/option/active')){
            //one year sma skus
            $oneyear_sma_skus_string = Mage::getStoreConfig('smasubscription/option/oneyearsma');
            if(!empty($oneyear_sma_skus_string)){
                $oneyear_sma_skus = array_map('trim', explode(',', $oneyear_sma_skus_string));
            } 
            //Mage::helper('smasubscription')->log('$oneyear_sma_skus:'.print_r($oneyear_sma_skus, true));
            $oneyear_sma_skus = array_unique(array_merge($oneyear_sma_skus, array(Mage::getStoreConfig('smasubscription/option/oneyearsmaout'))));  
            //Mage::helper('smasubscription')->log('$oneyear_sma_skus_merged:'.print_r($oneyear_sma_skus, true));         
            
            //two year sma skus
            $twoyear_sma_skus_string = Mage::getStoreConfig('smasubscription/option/twoyearsma');
            if(!empty($twoyear_sma_skus_string)){
                $twoyear_sma_skus = array_map('trim', explode(',', $twoyear_sma_skus_string));
            } 
            //Mage::helper('smasubscription')->log('$twoyear_sma_skus:'.print_r($twoyear_sma_skus, true)); 
            $twoyear_sma_skus = array_unique(array_merge($twoyear_sma_skus, array(Mage::getStoreConfig('smasubscription/option/twoyearsmaout'))));
            //Mage::helper('smasubscription')->log('$twoyear_sma_skus_merged:'.print_r($twoyear_sma_skus, true)); 
            
            //three year sma skus
            $threeyear_sma_skus_string = Mage::getStoreConfig('smasubscription/option/threeyearsma');
            if(!empty($threeyear_sma_skus_string)){                
                $threeyear_sma_skus = array_map('trim', explode(',', $threeyear_sma_skus_string));
            } 
            //Mage::helper('smasubscription')->log('$threeyear_sma_skus:'.print_r($threeyear_sma_skus, true)); 
            $threeyear_sma_skus = array_unique(array_merge($threeyear_sma_skus, array(Mage::getStoreConfig('smasubscription/option/threeyearsmaout'))));   
            //Mage::helper('smasubscription')->log('$threeyear_sma_skus_merged:'.print_r($threeyear_sma_skus, true));         
            //Mage::helper('smasubscription')->log('DB SKUS::'.print_r(array_merge($oneyear_sma_skus, $twoyear_sma_skus, $threeyear_sma_skus), true));                      
        }
        
        $merged_sma_skus    = array_unique(array_merge($oneyear_sma_skus, $twoyear_sma_skus, $threeyear_sma_skus));     
        $result_skus        = array_intersect($skus, $merged_sma_skus);
        
        if(!empty($result_skus)){
            $oneyear_sma_skus = array_intersect($oneyear_sma_skus, $result_skus);
            //Mage::helper('smasubscription')->log('INTERSECT $oneyear_sma_skus::'.print_r($oneyear_sma_skus, true));  
            if(count($oneyear_sma_skus) > 0){
                $_oneyear_sma_sku = Mage::getStoreConfig('smasubscription/option/oneyearsmaout');
                return array($_oneyear_sma_sku, 1);
            }
            
            $twoyear_sma_skus = array_intersect($twoyear_sma_skus, $result_skus);
            //Mage::helper('smasubscription')->log('INTERSECT $twoyear_sma_skus::'.print_r($twoyear_sma_skus, true)); 
            if(count($twoyear_sma_skus) > 0){
                $_twoyear_sma_sku = Mage::getStoreConfig('smasubscription/option/twoyearsmaout');
                return array($_twoyear_sma_sku, 2);
            }
            
            $threeyear_sma_skus = array_intersect($threeyear_sma_skus, $result_skus);
            //Mage::helper('smasubscription')->log('INTERSECT $threeyear_sma_skus::'.print_r($threeyear_sma_skus, true));
            if(count($threeyear_sma_skus) > 0){
                $_threeyear_sma_sku = Mage::getStoreConfig('smasubscription/option/threeyearsmaout');
                return array($_threeyear_sma_sku, 3);
            }   
            
        }
        return false;   
    }
}
?>