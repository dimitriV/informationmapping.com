<?php
/**
 * MageBridge
 *
 * @author Yireo
 * @package MageBridge
 * @copyright Copyright 2011
 * @license Yireo EULA (www.yireo.com)
 * @link http://www.yireo.com/
 */

/*
 * MageBridge model for XML-RPC client-calls
 */
class Yireo_MageBridge_Model_Client_Xmlrpc extends Yireo_MageBridge_Model_Client
{
    /*
     * Method to call a XML-RPC method
     *
     * @access public
     * @param string $method
     * @param array $params
     * @return mixed
     */
    public function makeCall($url, $method, $params = array())
    {
        // Get the authentication data
        $auth = $this->getAPIAuthArray();

        // If these values are not set, we are unable to continue
        if(empty($url ) || $auth == false) {
            return false;
        }

        // Add the $auth-array as first 
        array_unshift( $params, $auth );

        // Include the Zend-clients
        if(!class_exists('Zend_Log')) @include_once 'Zend/Log.php';
        if(!class_exists('Zend_Log')) return false;
        if(!class_exists('Zend_XmlRpc_Client')) @include_once 'Zend/XmlRpc/Client.php';
        if(!class_exists('Zend_XmlRpc_Client')) return false;

        // Initialize the XML-RPC client
        $client = new Zend_XmlRpc_Client($url);
        $client->setSkipSystemLookup(true);

        // Call the XML-RPC server
        try {

            // Call the API method with or without arguments
            if($params == null) {
                $result = $client->call($method);
            } else {
                $result = $client->call($method, $params);
            }

            // Parse the API result
            if(is_string($result)) {

                $parsed_result = @unserialize($result);
                if(!empty($parsed_result)) $result = $parsed_result;

                //Mage::getSingleton('magebridge/debug')->trace('XML-RPC result', $result);
                return $result;

            } else {
                return $result;
            }

        } catch (Exception $e) {

            $error = 'XML-RPC call to "'.$url.$method.'" failed: '.$e->getMessage();
            $this->setErrorMsg($error);
            Mage::getSingleton('magebridge/debug')->warning($error);
            //Mage::getSingleton('magebridge/debug')->trace('Method arguments', $params);
        }

        return false;
    }
} 

