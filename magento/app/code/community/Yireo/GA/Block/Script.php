<?php
/**
 * GoogleAnalytics extension for Magento 
 *
 * @package     Yireo_GA
 * @author      Yireo (http://www.yireo.com/)
 * @copyright   Copyright (C) 2014 Yireo (http://www.yireo.com/)
 * @license     Open Source License (OSL v3)
 */

/**
 * GoogleAnalytics Page Block
 */
class Yireo_GA_Block_Script extends Mage_Core_Block_Template
{
    /**
     * Retrieve Order Data HTML
     *
     * @return string
     */
    public function getOrderHtml()
    {
        $session = Mage::getSingleton('checkout/session');
        $order = Mage::getModel('sales/order')->load($session->getLastOrderId());
        if (!$order->getId() > 0) return null;

        $address = $order->getBillingAddress();
        $orderId = ($order->getIncrementId() != '') ? $order->getIncrementId() : $order->getId();
        $fields = array(
            $orderId,
            $order->getAffiliation(),
            $order->getBaseGrandTotal(),
            $order->getBaseTaxAmount(),
            $order->getBaseShippingAmount(),
            $address->getCity(),
            $address->getRegion(),
            $address->getCountry(),
        );

        $html = null;
        $html .= "_gaq.push(['_addTrans',".implode(',', $this->quoteArray($fields))."]);"."\n";

        // Loop through the order items (the products) to add them to the script
        foreach ($order->getAllItems() as $item) {

            if ($item->getParentItemId()) continue;

            //Get Category with highest ID
            $cProduct = Mage::getModel('catalog/product');
            $cProductId = $cProduct->getIdBySku($item->getSku());
            $cProduct->load($cProductId);
            $category_list = "";
            $cats = $cProduct->getCategoryCollection()->exportToArray();
            foreach($cats as $cat){
                $category_list = Mage::getModel('catalog/category')->load($cat['entity_id'])->getName();
            }

            $fields = array(
                $order->getIncrementId(),
                $item->getSku(),
                $item->getName(),
                $category_list,
                $item->getBasePrice(),
                $item->getQtyOrdered(),
            );

            $html .= "_gaq.push(['_addItem',".implode(',', $this->quoteArray($fields))."]);"."\n";
        }

        $html .= "_gaq.push(['_trackTrans']);\n";
        return $html;
    }

    /**
     * Retrieve current page URL
     *
     * @return string
     */
    public function getPageName()
    {
        if (!$this->hasData('page_name')) {
            $baseurl = preg_replace('/\/$/', '', Mage::getBaseUrl());
            $current = $baseurl . Mage::app()->getRequest()->getServer('REQUEST_URI');
            $current = preg_replace('/(http:|https:)\/\/([^\/]+)\//', '', $current);
            $this->setPageName($current);
        }
        return $this->getData('page_name');
    }

    /**
     * Get the currrent category
     *
     * @return string
     */
    public function getCategory()
    {
        if(is_object(Mage::registry('current_category'))){
            $_category = Mage::registry('current_category')->getName();
        } else {
            $_category = "direct|search";
        }

        return $_category;
    }

    /**
     * Get the currrent product
     *
     * @return string
     */
    public function getProduct()
    {
        if(is_object(Mage::registry('current_product'))){
            $_product = Mage::registry('current_product')->getName();
        } else {
            $_product = "unknown product";
        }

        return $_product;
    }

    /**
     * Get the Google Analytics script-location
     *
     * @return string
     */
    public function getScript()
    {
        $script = "(function() {\n";
        $script .= "var ga = document.createElement('script'); ga.type = 'text/javascript'; ga.async = true;\n";
        $script .= "ga.src = ('https:' == document.location.protocol ? 'https://ssl' : 'http://www') + '.google-analytics.com/ga.js';\n";
        $script .= "var s = document.getElementsByTagName('script')[0]; s.parentNode.insertBefore(ga, s);\n";
        $script .= "})();\n";

        return $script;
    }

    /**
     * Get extra environment variables
     *
     * @return string
     */
    public function getCustomVars()
    {
        // Initialize the extra variables
        $vars = array();

        // Add whether the customer is logged in
        if(Mage::getSingleton('customer/session')->isLoggedIn()) {
            $vars['Register Type'] = 'logged in';
        } else {
            $vars['Guest Type'] = 'not logged in';
        }

        // Add customer items
        $vars['Cart Items'] = (int)Mage::helper('checkout/cart')->getSummaryCount(); 

        // Construct the output
        $output = null;
        $index = 1;
        foreach($vars as $name => $value) {
            $output .= "_gaq.push(['_setCustomVar', ".$index.', "'.$name.'", "'.$value.'", 1]);'."\n";
            $index++;
        }

        return $output;
    }

    /**
     * Retrieve Google Account Identifier
     *
     * @return string
     */
    public function getTracker()
    {
    }

    /**
     * Retrieve Google Account Identifier
     *
     * @return string
     */
    public function getAccount()
    {
        if (!$this->hasData('account')) {
            $this->setAccount(Mage::getStoreConfig('google/analytics/account'));
        }
        return $this->getData('account');
    }

    protected function quoteArray($fields = array())
    {
        foreach($fields as $index => $field) {
            $fields[$index] = '"'.$this->jsQuoteEscape($field, '"').'"';
        }

        return $fields;
    }

    public function getGoogleCheckoutHtml()
    {
        if ($this->getGoogleCheckout()) {
            $protocol = Mage::app()->getStore()->isCurrentlySecure() ? 'https' : 'http';
            return '<script src="'.$protocol.'://checkout.google.com/files/digital/ga_post.js" type="text/javascript"></script>';
        }
    }

    public function getSearchTrack()
    {
        $number = Mage::getSingleton('catalogsearch/layer')->getProductCollection()->getSize();
        switch($number) {
            case 0:
                $track = 'no-results';
                break;
            case 1:
                $track = 'results-1';
                break;
            case 2:
                $track = 'results-2';
                break;
            default:
                $track = 'und';
                if($number >= 3 && $number <= 5) {
                    $track = 'results-3';
                } elseif($number <= 10) {
                    $track = 'results-6';
                } elseif($number > 10) {
                    $track = 'results-10';
                }
                break;
        }

        return $track;
    }
}
