<?php
require_once 'app/Mage.php';

require_once 'app/code/local/Storefront/VendorLoader/Model/Loader.php';
Storefront_VendorLoader_Model_Loader::enableLoader();


Mage::app('admin')->setUseSessionInUrl(false);
//replace your own orders numbers here:
$remove_order_ids=array(
    '500005726'
);
foreach($remove_order_ids as $id){
    try{
        Mage::getModel('sales/order')->loadByIncrementId($id)->delete();
        echo "order #".$id." is removed".PHP_EOL;
    }catch(Exception $e){
        echo "order #".$id." could not be remvoved: ".$e->getMessage().PHP_EOL;
    }
}
echo "complete.";