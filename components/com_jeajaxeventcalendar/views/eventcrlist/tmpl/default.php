<?php 
/**
* @package   JE Ajax Event Calendar
* @copyright Copyright (C) 2009 - 2010 Open Source Matters. All rights reserved.
* @license   http://www.gnu.org/licenses/lgpl.html GNU/LGPL, see LICENSE.php
* Contact to : emailtohardik@gmail.com, joomextensions@gmail.com
* Visit : http://www.joomlaextensions.co.in/
**/ 

defined('_JEXEC') or die('Restricted access');

JHTML::_('behavior.tooltip');
JHTMLBehavior::modal();
$doc =& JFactory::getDocument();
$option = JRequest::getVar('option','','','string');
$doc->addStyleSheet("components/".$option."/assets/css/style.css");
$Itemid = JRequest::getVar('Itemid','','request','int');
$filter = JRequest::getVar('filter'); 
$model = $this->getModel ( 'eventcrlist' );
$uri =& JURI::getInstance();
$url= $uri->root(); 
$img_path=$url.'components/'.$option.'/assets/images/';
$catid	= JRequest::getVar('catid','','request','int');
$lnk_item_add 	= JRoute::_( 'index.php?option='.$option.'&view=eventcrlist_detail&cid[]=0&Itemid='.$Itemid);
$user	= clone(JFactory::getuser());

$tempt_setting=$model->gettempsetting();
$tempt_eventcrlist_tempt = $tempt_setting->eventcrlist_tempt;

$event_id_tag 		= preg_match('{event_id}',$tempt_eventcrlist_tempt);
$event_title_tag 	= preg_match('{event_title}',$tempt_eventcrlist_tempt);
$event_photo_tag 	= preg_match('{event_photo}',$tempt_eventcrlist_tempt);
$catname_tag 		= preg_match('{catname}',$tempt_eventcrlist_tempt);
$start_date_tag 	= preg_match('{start_date}',$tempt_eventcrlist_tempt);
$end_date_tag 		= preg_match('{end_date}',$tempt_eventcrlist_tempt);
$repeat_event_tag 	= preg_match('{repeat_event}',$tempt_eventcrlist_tempt);
$edit_tag 			= preg_match('{edit}',$tempt_eventcrlist_tempt);
$delete_tag 		= preg_match('{delete}',$tempt_eventcrlist_tempt);
$description_tag 	= preg_match('{description}',$tempt_eventcrlist_tempt);
$address_tag 		= preg_match('{address}',$tempt_eventcrlist_tempt);

?>

<table width="100%" border="0">
	<tr>
		<td class="componentheading"><?php echo JText::_('MY_EVENTS'); ?></td>
		<td align="right"><a href="<?php echo $lnk_item_add  ?>" style="text-decoration:none;"><img src="<?php echo $img_path.'new_button.png'; ?>" /><br />New</a></td>
	</tr>
</table>

<form action="<?php echo 'index.php?option='.$option; ?>" method="post" name="adminForm" >
<?php 
	
	$eimg_path	= $url."components/".$option."/assets/event/images/";
	for ($i=0, $n=count( $this->eventcrlist ); $i < $n; $i++)
	{ 
		$lnk_item_edit 	= JRoute::_( 'index.php?option='.$option.'&view=eventcrlist_detail&cid[]='. $this->eventcrlist[$i]->id.'&Itemid='.$Itemid);
		$lnk_item_del 	= JRoute::_( 'index.php?option='.$option.'&view=eventcrlist&task=delete&cid[]='. $this->eventcrlist[$i]->id.'&Itemid='.$Itemid);
				
		$edit = '<a href="'.$lnk_item_edit.'" >'.JText::_('EDITE').'</a>';
		$delete = '<a href="'.$lnk_item_del.'" >'.JText::_('DELETE').'</a>';
		
		$address	= $this->eventcrlist[$i]->city.','.$this->eventcrlist[$i]->country;
		$event_title= '<a href="'.$lnk_item_edit.'" >'.$this->eventcrlist[$i]->title.'</a>';		
		// ======== Design comes from database ============================================================
		$tempt_setting1=$model->gettempsetting();
		$tempt_eventcrlist_tempt1 = $tempt_setting1->eventcrlist_tempt;
				
		$eventphoto	= $model->eventmainphoto($this->eventcrlist[$i]->id);
		if(count($eventphoto)!=0)
			$event_mainphoto = $eimg_path.'thumb_'.$eventphoto->image; 
		else
			$event_mainphoto = $eimg_path.'noimage.png';
			
		$event_photo = '<a href="'.$lnk_item_edit.'" ><img src="'.$event_mainphoto.'" border="0" ></a>';
		if($this->eventcrlist[$i]->erepeat==0)
			$event_repeat	= JText::_('NO_REPEAT');
		if($this->eventcrlist[$i]->erepeat==1)
			$event_repeat	= JText::_('YEAR');
		if($this->eventcrlist[$i]->erepeat==2)
			$event_repeat	= JText::_('MONTH');
		if($this->eventcrlist[$i]->erepeat==3)
			$event_repeat	= JText::_('WEEK');
		if($this->eventcrlist[$i]->erepeat==4)
			$event_repeat	= JText::_('MULTIPLE_DATES');
			
		$action	= $edit.' | '.$delete;
					
		if($event_id_tag==1)
			$tempt_eventcrlist_tempt1 = str_replace('{event_id}',$this->eventcrlist[$i]->id,$tempt_eventcrlist_tempt1);
			
		if($event_title_tag==1)
			$tempt_eventcrlist_tempt1 = str_replace('{event_title}',$event_title,$tempt_eventcrlist_tempt1);
				
		if($event_photo_tag==1)
			$tempt_eventcrlist_tempt1 = str_replace('{event_photo}',$event_photo,$tempt_eventcrlist_tempt1);
				
		if($catname_tag==1)
			$tempt_eventcrlist_tempt1 = str_replace('{catname}',$this->eventcrlist[$i]->catname,$tempt_eventcrlist_tempt1);
				
		if($start_date_tag==1)
			$tempt_eventcrlist_tempt1 = str_replace('{start_date}',$this->eventcrlist[$i]->start_date,$tempt_eventcrlist_tempt1);
					
		if($end_date_tag==1)
			$tempt_eventcrlist_tempt1 = str_replace('{end_date}',$this->eventcrlist[$i]->end_date,$tempt_eventcrlist_tempt1);
				
		if($repeat_event_tag==1)
			$tempt_eventcrlist_tempt1 = str_replace('{repeat_event}',$event_repeat,$tempt_eventcrlist_tempt1);
			
		if($edit_tag==1)
			$tempt_eventcrlist_tempt1 = str_replace('{edit}',$edit,$tempt_eventcrlist_tempt1);
		
		if($delete_tag==1)
			$tempt_eventcrlist_tempt1 = str_replace('{delete}',$delete,$tempt_eventcrlist_tempt1);
		
		if($description_tag==1)
			$tempt_eventcrlist_tempt1 = str_replace('{description}',$this->eventcrlist[$i]->desc,$tempt_eventcrlist_tempt1);
		
		if($address_tag==1)
			$tempt_eventcrlist_tempt1 = str_replace('{address}',$address,$tempt_eventcrlist_tempt1);			
										
		echo $tempt_eventcrlist_tempt1;		
	}
?>	
	
<span class="article_separator">&nbsp;</span>
<?php //=============================== Code for pagination ============================================================// ?>	
	<table cellpadding="0" cellspacing="0"   align="center" width="100%">
	<tr>
		<td valign="top" align="center">
			<?php echo $this->pagination->getPagesLinks(); ?>
			<br /><br />
		</td>
	</tr>
	<tr>
		<td valign="top" align="center">
			<?php echo $this->pagination->getPagesCounter(); ?>
		</td>
	</tr>
	</table>
<?php //=================================================================================================================//  ?>	
	<div style="clear:both;"></div>
<input type="hidden" name="view" value="eventcrlist" />
<input type="hidden" name="task" value="" />
<input type="hidden" name="boxchecked" value="0" />
<input type="hidden" name="Itemid" value="<?php echo $Itemid; ?>" />
</form>
