<?php 
/**
* @package   JE Ajax Event Calendar
* @copyright Copyright (C) 2009 - 2010 Open Source Matters. All rights reserved.
* @license   http://www.gnu.org/licenses/lgpl.html GNU/LGPL, see LICENSE.php
* Contact to : emailtohardik@gmail.com, joomextensions@gmail.com
* Visit : http://www.joomlaextensions.co.in/
**/ 

defined('_JEXEC') or die('Restricted access');
$mosConfig_live_site = JURI :: base();
$doc =& JFactory::getDocument();
$uri =& JURI::getInstance();
$url= $uri->root();
$model = $this->getModel ( 'event_list' );
$user 		= clone(JFactory::getUser());
$option = JRequest::getVar('option','','request','string');
$Itemid = JRequest::getVar('Itemid','','','int');

$insert_user= $user->id;
$doc->addStyleSheet("components/".$option."/assets/css/style.css");

global $context;
$mainframe = JFactory::getApplication();
$myevent_dates	= $mainframe->getUserStateFromRequest( $context.'myevent_dates', 'myevent_dates','0');


$eimg_path	= $url."components/".$option."/assets/event/images/";
$dynemic_img_path	= $url."components/".$option."/assets/images/";

// ======================== Date Convert Calss ================== //
$mydate_formate	= new gencalendar();
// ==================== EOF Date Convert Calss ================== //

?>
<style type="text/css">
.row1 {
	background-color:#fffbcb;
}
.row2 {
	background-color:#d7ebd7;
}
</style>

<form action="<?php echo 'index.php?option='.$option; ?>" method="post" name="adminForm" enctype="multipart/form-data" >
<?php 	$date_exp = explode('-',$myevent_dates);
		$y = $date_exp[0];
		$m = $date_exp[1];
		$d = $date_exp[2];
	// ============================= Convert month ===================== //		
		$month	= $mydate_formate->gen_formatmonth($date_exp[1]);
	// =========================== EOF Convert month =================== //	
		$day_date = date("l", mktime(0, 0, 0, $m, $d, $y)); 
					
	// ============================= Convert month ===================== //		
		$day	= $mydate_formate->gen_formatday($day_date);
	// =========================== EOF Convert month =================== //			
		$date = $day.' '.$date_exp[2].' '.$month.' '.$date_exp[0];
?>
<div>
<h3><?php echo $date;?></h3>

<?php 

	//$tempt_setting		=	$model->gettempsetting();
	//$dateevent_tempt 	= $tempt_setting->dateevent_tempt;
	
	for($i=0;$i<count($this->eventlist);$i++){
		$row =$this->eventlist[$i]; 
		$link_edetail	= JRoute::_("index.php?option=".$option."&view=alleventlist_more&Itemid=".$Itemid."&event_id=".$row->id);
		
		
		$tempt_setting		=	$model->gettempsetting();
		$dateevent_tempt 	= $tempt_setting->dateevent_tempt;
		
		$eventmainphoto	= $model->eventmainphoto($row->id);
		if(count($eventmainphoto)!=0)
			$event_img = '<a href="'.$link_edetail.'" ><img src="'.$eimg_path.'thumb_'.$eventmainphoto->image.'"></a>';
		else
			$event_img = '<a href="'.$link_edetail.'" ><img src="'.$eimg_path.'noimage.png"></a>';

		
		
		$eventcategory	= $model->eventcategory($row->category);
		
		
		$event_photo_tag = preg_match('{event_photo}',$dateevent_tempt);
		if($event_photo_tag==1) {
			$dateevent_tempt = str_replace('{event_photo}',$event_img,$dateevent_tempt);
		}
		
		$address_tag = preg_match('{address}',$dateevent_tempt);
		if($address_tag==1) {
			$address	= $row->city.','.$row->country;
			$dateevent_tempt = str_replace('{address}',$address,$dateevent_tempt);
		}
		
		$description_tag = preg_match('{description}',$dateevent_tempt);
		if($description_tag==1) {
			$description	= $row->desc;
			
			if(strlen($description)>100) { 
				$description = substr($description,0,100);
				$description = $description.' <a href="'.$link_edetail.'" >(Read more)</a>';
			} else {
				$description = $description.' <a href="'.$link_edetail.'" ></a>';
			}
			
			
			$dateevent_tempt = str_replace('{description}',$description,$dateevent_tempt);
		}
		
		$event_title_tag = preg_match('{event_title}',$dateevent_tempt);
		if($event_title_tag==1) {
			$event_title	= '<a href="'.$link_edetail.'" >'.$row->title.'</a>';
			$dateevent_tempt = str_replace('{event_title}',$event_title,$dateevent_tempt);
		}
		
		$catname_tag = preg_match('{catname}',$dateevent_tempt);
		if($catname_tag==1) {
			if($eventcategory!='')
			{
			$catname	= $eventcategory->ename;
			} else {
			$catname	= '';
			}
			$dateevent_tempt = str_replace('{catname}',$catname,$dateevent_tempt);
		}
		
		// =========================== Hits Display Code ============================== //
		$eventhits_tag = preg_match('{hits}',$dateevent_tempt);
		if($eventhits_tag==1) {
			$event_hits	= JText::_('HITS').':'.$row->ehits;
			$dateevent_tempt = str_replace('{hits}',$event_hits,$dateevent_tempt);
		}
		// =========================== Hits Display Code ============================== //
		
		$outlook_icon 	= preg_match('{outlook}',$dateevent_tempt);
		$icalendar_icon = preg_match('{ical}',$dateevent_tempt);
		$gcalendar_icon = preg_match('{gcal}',$dateevent_tempt);
		
		if($outlook_icon==1) {
		// ============================= Code for OutLook Calendar =============================================================//
			$link_outlook = JRoute::_("index.php?option=".$option."&view=mycalender&task=generate_outlook&Itemid=".$Itemid.'&event_id='.$row->id);
			$outlook = '<a href="'.$link_outlook.'" ><img src="'.$dynemic_img_path.'outlook.png" border="0" ></a>';
			$dateevent_tempt = str_replace('{outlook}',$outlook,$dateevent_tempt);
		// ============================= EOF Code for OutLook Calendar =========================================================//
		}
		if($icalendar_icon==1) {
		// ============================= Code for Ical Calendar ===============================================================//
			$link_ical = JRoute::_("index.php?option=".$option."&view=mycalender&task=generate_ical&Itemid=".$Itemid.'&event_id='.$row->id);
			$ical = '<a href="'.$link_ical.'" ><img src="'.$dynemic_img_path.'ical.png" border="0" ></a>';
			$dateevent_tempt = str_replace('{ical}',$ical,$dateevent_tempt);
		// ============================= EOF Code for Ical Calendar ===========================================================//
		}
		if($gcalendar_icon==1) {
		// ============================= Code for Google Calendar =============================================================//
			$link_gcal = JRoute::_("index.php?option=".$option."&view=mycalender&task=gcalendar&Itemid=".$Itemid.'&event_id='.$row->id);
			$gcal = '<a href="'.$link_gcal.'" ><img src="'.$dynemic_img_path.'gcalendar.png" border="0" ></a>';
			$dateevent_tempt = str_replace('{gcal}',$gcal,$dateevent_tempt);
		// ============================= EOF Code for Google Calendar =========================================================//
		}
		
		echo $dateevent_tempt;
	}
	
		
 ?>

<table cellpadding="0" cellspacing="0"   align="center" width="100%">
<tr>
	<td valign="top" align="center">
		<?php echo $this->pagination->getPagesLinks(); ?>
		<br /><br />
	</td>
</tr>
<tr>
	<td valign="top" align="center">
		<?php echo $this->pagination->getPagesCounter(); ?>
	</td>
</tr>
</table>
</div>
<div style="clear:both;"></div>  
<input type="hidden" name="Itemid" value="<?php echo $Itemid; ?>" />				
<input type="hidden" name="option" value="<?php echo $option;?>">
<input type="hidden" name="view" value="event_list" />
<input type="hidden" name="task" value="" />
<input type="hidden" name="boxchecked" value="0" />
<input type="hidden" name="filter_order" value="<?php echo $this->lists['order']; ?>" />
<input type="hidden" name="filter_order_Dir" value="<?php echo $this->lists['order_Dir']; ?>" />
</form>