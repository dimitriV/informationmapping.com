<?php 
/**
* @package   JE Ajax Event Calendar
* @copyright Copyright (C) 2009 - 2010 Open Source Matters. All rights reserved.
* @license   http://www.gnu.org/licenses/lgpl.html GNU/LGPL, see LICENSE.php
* Contact to : emailtohardik@gmail.com, joomextensions@gmail.com
* Visit : http://www.joomlaextensions.co.in/
**/ 

defined('_JEXEC') or die('Restricted access');

JHTML::_('behavior.tooltip');
JHTMLBehavior::modal();

$mosConfig_live_site = JURI :: base();
$doc =& JFactory::getDocument();
$db = JFactory::getDbo();
$option = JRequest::getVar('option','','request','string');

$model = $this->getModel ( 'categoryevent' );
$user 		= clone(JFactory::getUser());
$insert_user= $user->id;
$uri =& JURI::getInstance();
$url= $uri->root();
$Itemid = JRequest::getVar('Itemid','','request','int');
// =============== For light box ===================================================== //
$doc->addStyleSheet("components/".$option."/assets/css/style.css");
$doc->addScript($url."components/".$option."/assets/js/sortable.js");
// =================================================================================== //
$eimg_path	= $url."components/".$option."/assets/event/images/";
$dynemic_img_path	= $url."components/".$option."/assets/images/";

?>

<script language="javascript" type="text/javascript">
var image_path = "<?php echo $dynemic_img_path;  ?>";
var image_up = "arrow-up.gif";
var image_down = "arrow-down.gif";
var image_none = "arrow-none.gif";
var europeandate = true;
var alternate_row_colors = true;
</script>

<form action="<?php echo 'index.php?option='.$option; ?>" method="post" name="adminForm" enctype="multipart/form-data" >
<table border="0" cellpadding="2" cellspacing="0" width="100%">
<?php  
	for($i=0;$i<count($this->categoryevent);$i++) {
		$categoryevent	= $this->categoryevent[$i];
		
		$allevent_data	= $model->getevents($categoryevent->id);
?>	<tr>
		<td>
			<table>
				<tr><td height="20"><b><?php echo $categoryevent->ename; ?></b></td></tr>
				<tr><td></td></tr>
				<tr>
				<td valign="top"><img src="<?php echo $eimg_path.'thumb_'.$categoryevent->cimage; ?>" /></td>
				<td valign="top"><?php echo $categoryevent->edesc; ?></td>
				</tr>
			</table>
		</td>
	</tr>
<?php if(count($allevent_data)!=0) { ?>	
	<tr><td align="center" height="20" class="event_header"><?php echo JText::_('EVENTS'); ?></td></tr>
	<tr>
		<td>
			<table border="0" cellpadding="0" cellspacing="0" width="100%" class="sortable" id="sortable_example">
			<tr>
				<th width="30%"><?php echo JText::_('TITLE'); ?></th>
				<th><?php echo JText::_('DATE'); ?></th>
				<th><?php echo JText::_('VENUE'); ?></th>
				<th><?php echo JText::_('CATEGORY'); ?></th>
				<th width="5%" align="center" class="last"><?php echo JText::_('HITS'); ?></th>
			</tr>
<?php	
		$k=1;
		for($j=0;$j<count($allevent_data);$j++) {
			$allevents	= $allevent_data[$j];
			$venue	= $allevents->city;
			$link_edetail = JRoute::_('index.php?option=com_jeajaxeventcalendar&view=alleventlist_more&Itemid='.$Itemid.'&event_id='.$allevents->id);
			if($k%2==0) 
				$edata_class	= 'event_even';
			else
				$edata_class	= 'event_odd';
			
?>	
			<tr >
				<td><a href="<?php echo $link_edetail; ?>"><?php echo $allevents->title; ?></a></td>
				<td><?php echo $allevents->start_date; ?></td>
				<td><a href="<?php echo $link_edetail; ?>"><?php echo $venue; ?></a></td>
				<td><?php echo $categoryevent->ename; ?></td>
				<td width="5%" align="center" class="last"><?php echo $allevents->ehits; ?></td>
			</tr>
<?php	$k++;
		}
	}	
?>			</table>
		</td>
	</tr>
	<tr><td height="10"></td></tr>
<?php
	}
?>
</table>


<table cellpadding="0" cellspacing="0"   align="center" width="100%">
<tr>
	<td valign="top" align="center">
		<?php echo $this->pagination->getPagesLinks(); ?>
		<br /><br />
	</td>
</tr>
<tr>
	<td valign="top" align="center">
		<?php echo $this->pagination->getPagesCounter(); ?>
	</td>
</tr>
</table>

<div style="clear:both;"></div>  
<input type="hidden" name="Itemid" value="<?php echo $Itemid; ?>"/>
<input type="hidden" name="option" value="<?php echo $option;?>">
<input type="hidden" name="view" value="categoryevent" />
<input type="hidden" name="task" value="" />
<input type="hidden" name="boxchecked" value="0" />
<input type="hidden" name="filter_order" value="<?php echo $this->lists['order']; ?>" />
<input type="hidden" name="filter_order_Dir" value="<?php echo $this->lists['order_Dir']; ?>" />
</form>