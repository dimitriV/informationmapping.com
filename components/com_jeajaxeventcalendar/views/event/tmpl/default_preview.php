<?php	
/**
* @package   JE Ajax Event Calendar
* @copyright Copyright (C) 2009 - 2010 Open Source Matters. All rights reserved.
* @license   http://www.gnu.org/licenses/lgpl.html GNU/LGPL, see LICENSE.php
* Contact to : emailtohardik@gmail.com, joomextensions@gmail.com
* Visit : http://www.joomlaextensions.co.in/
**/

defined ('_JEXEC') or die ('restricted access');
$uri =& JURI::getInstance();
$url= $uri->root();
$document = &JFactory::getDocument();
$my =& JFactory::getUser();
$month = JRequest::getVar('month',date("m"));
$year = JRequest::getVar('year',date("Y"));
$event_id	= JRequest::getVar('event_id','','','int');
$Itemid = JRequest::getVar('Itemid','','','int');
$model = $this->getModel ( 'event' );
$data=$model->event_desc($event_id);
$option = JRequest::getVar('option','','','string');
$document->addStyleSheet ( 'components/'.$option.'/assets/css/style.css' ); 
$db	= & JFactory::getDBO();
// =========================================== GOOGLE MAP CODE =========================================================
 $this->_table_prefix = '#__jeajx_';
 $sqlapi = "SELECT gmap_api,gmap_width,gmap_height,gmap_display FROM ".$this->_table_prefix."event_configration WHERE id =1";
 $db->setQuery($sqlapi);
 $googleapi = $db->LoadObject();
 if($googleapi->gmap_display==1)
 {
	$sql ="SELECT e.street,e.postcode,e.city,e.country FROM ".$this->_table_prefix."event AS e WHERE e.id = ".$event_id;
	$db->setQuery($sql);
	$statelist = $db->LoadObject();
	$location 	= $statelist->street;
	$add1 		= $statelist->city;
	$add3 		= $statelist->country;
	$add4 		= $statelist->postcode;
	$address = "";
	$linkadd = "";
	$address = $location;
	$linkadd = $location;
	$address .= ','.$add1;
	$linkadd .= ','.$add1;
	if($add3!="")
		$address .= ','.$add3;
	$linkadd .= ',<br>'.$add3;
	if($add4!="")
		$address .= ','.$add4;
	$linkadd .= ','.$add4;
	$pp = '<div style="width: 210px;padding-right: 10px"><table><tr><td width="60px">'.$linkadd.'</td></tr></table></div>';
?>
<script src="http://maps.google.com/maps?file=api&amp;v=2&amp;key=<?php echo $googleapi->gmap_api ;?>&sensor=true" type="text/javascript"></script>

<script type="text/javascript">
	var map = null;
    var geocoder = null;
	var WINDOW_HTML = '<?php echo $pp; ?>';
	
	function initialize() {
      if (GBrowserIsCompatible()) {

	  	var mapOptions = {
            googleBarOptions : {
              style : "new"
            }
          }

        map = new GMap2(document.getElementById("map_canvas"));
        map.setCenter(new GLatLng(41.393294, -77.189941), 13);
	
		 map.setUIToDefault();
     
        geocoder = new GClientGeocoder();
      }
    }

    function showAddress(address) {
	 
      if (geocoder) {
        geocoder.getLatLng(
          address,
          function(point) {
            if (!point) {
              alert(address + " not found");
            } else {
              map.setCenter(point, 13);
              var marker = new GMarker(point);
              map.addOverlay(marker);
			  GEvent.addListener(marker, "click", function() {
	marker.openInfoWindowHtml(WINDOW_HTML);
	  });
	marker.openInfoWindowHtml(WINDOW_HTML);	
             
            }
          }
        );
      }
    }
	//initialize();

	window.onload=function(){
		initialize();
		showAddress("<?php echo $address?>");
	}
	
	window.onunload=function(){
		GUnload();
	}
	
</script>
<?php //===================================================== End of GOOGLE MAP =============================================== 
}
?>

<div style="margin:0px; padding:0px;">
  <table width="100%"  cellpadding="2" cellspacing="2" id="one-column-emphasis" class="table table-striped" >
    <colgroup>
    <col class="oce-first" />
    </colgroup>
    <tr valign="top">
      <th ><?php echo JText::_( 'EVENT_TITLE' );?></th>
      <td><?php echo $data->title;?></td>
    </tr>
    <tr valign="top">
      <th width="27%"  ><?php echo JText::_( 'EVENT_START_DATE' );?></th>
      <td width="73%"><?php echo $data->start_date;?></td>
    </tr>
    <tr valign="top">
      <th  ><?php echo JText::_( 'EVENT_END_DATE' );?></th>
      <td><?php echo $data->end_date;?></td>
    </tr>
    <tr valign="top">
      <th  ><?php echo JText::_( 'EVENT_DESC' );?></th>
      <td ><?php echo $data->desc;?></td>
    </tr>
    <tr valign="top">
      <th  ><?php echo JText::_( 'CITY' );?></th>
      <td ><?php echo $data->city;?></td>
    </tr>
    <tr valign="top">
      <th  ><?php echo JText::_( 'COUNTRY' );?></th>
      <td ><?php echo $data->country;?></td>
    </tr>
    <tr>
      <td colspan="2"><div id="map_canvas" style="width: 500px; height: 460px"></div></td>
    </tr>
  </table>
</div>
