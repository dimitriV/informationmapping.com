<?php	
/**
* @package   JE Ajax Event Calendar
* @copyright Copyright (C) 2009 - 2010 Open Source Matters. All rights reserved.
* @license   http://www.gnu.org/licenses/lgpl.html GNU/LGPL, see LICENSE.php
* Contact to : emailtohardik@gmail.com, joomextensions@gmail.com
* Visit : http://www.joomlaextensions.co.in/
**/

defined ('_JEXEC') or die ('restricted access');
JHTML::_('behavior.tooltip');
$uri =& JURI::getInstance();
$url= $uri->root();
$mainframe = JFactory::getApplication();
JHTML::_('behavior.calendar');
$document = &JFactory::getDocument();
$my =& JFactory::getUser();
$option = JRequest::getVar('option','','','string');
$document->addStyleSheet ('components/'.$option.'/assets/css/calendar_style.css' ); 
$document->addStyleSheet ('components/'.$option.'/assets/flatbox/floatbox.css' );
$document->addScript('components/'.$option.'/assets/flatbox/floatbox.js' );
$Itemid = JRequest::getVar('Itemid','','','int');
$load_imgpath	= $url.'components/'.$option.'/assets/images/loading.gif';
?> 
<div id="ajaxcal_loading_img" style="margin-left:50%; display:none;">
<img src="<?php echo $load_imgpath; ?>" />
</div>
<br />
</br>
<script language="javascript" type="text/javascript">
var liv_path="<?php echo $url;?>";
var liv_path_component="<?php echo $url.'components/'.$option.'/assets';?>";</script>
<script language="javascript" type="text/javascript" src="<?php echo $url.'components/'.$option.'/assets/flatbox/floatbox.js'; ?>"></script>
<div id="calback" style="width: 100%; height: 100%;">
	<div id="calendar" style="width:100%;"></div>
</div>
<input type="hidden" name="Itemid" value="<?php echo $Itemid; ?>" />