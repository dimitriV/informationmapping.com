<?php
/**
* @package   JE Ajax Event Calendar
* @copyright Copyright (C) 2009 - 2010 Open Source Matters. All rights reserved.
* @license   http://www.gnu.org/licenses/lgpl.html GNU/LGPL, see LICENSE.php
* Contact to : emailtohardik@gmail.com, joomextensions@gmail.com
* Visit : http://www.joomlaextensions.co.in/
**/

defined('_JEXEC') or die ('restricted access');
jimport('joomla.application.component.view');

class eventViewevent extends JViewLegacy
{ 
	function display ($tpl=null)
	{ 
		$post = JRequest::get ( 'post' );
		$mainframe = JFactory::getApplication();

// ========================= Page title code ================================== //	
		$mainframe 	= JFactory::getApplication();
		$document 	= & JFactory::getDocument();
		$pagetitle 	= $document->getTitle();
		if($mainframe->getCfg( 'sitename_pagetitles' )==2) // After page title
			$document->setTitle($pagetitle.' - '.$mainframe->getCfg( 'sitename' ) );
		else if($mainframe->getCfg( 'sitename_pagetitles' )==1) // Before page title
			$document->setTitle($mainframe->getCfg( 'sitename' ).' - '.$pagetitle );
		else // no page title
			$document->setTitle($pagetitle );
// ======================= EOF Page title code ================================ //			$option	= JRequest::getVar('option', 'com_jeajaxeventcalendar','','string');
		$event_id	= JRequest::getVar('event_id','','','int');
		$res	=& $this->get('Category');
		$sel_res = array();
		$sel_res[0]->value="0";
		$sel_res[0]->text=JText::_('--SELECT_SECTION--');
		$res1=@array_merge($sel_res,$res);
		$lists['res'] 	= JHTML::_('select.genericlist',$res1,'id', 'class="inputtext" onchange="navigate(\'\',\'\',this.value)" ', 'value', 'text' );

		if($event_id!="")
		{
			$tpl="preview"; 
		}
		$this->assignRef('lists',	$lists);
		parent::display($tpl);
	}
}
?>