<?php
 /**
* @package   JE Ajax Event Calendar
* @copyright Copyright (C) 2009 - 2010 Open Source Matters. All rights reserved.
* @license   http://www.gnu.org/licenses/lgpl.html GNU/LGPL, see LICENSE.php
* Contact to : emailtohardik@gmail.com, joomextensions@gmail.com
* Visit : http://www.joomlaextensions.co.in/
**/ 

defined( '_JEXEC' ) or die( 'Restricted access' );
jimport( 'joomla.application.component.view' );

class alleventlistViewalleventlist extends JView
{
	function __construct( $config = array())
	{
		parent::__construct( $config );
	}

	function display($tpl = null)
	{	
		global $context;
		$mainframe = JFactory::getApplication();
		$uri	=& JFactory::getURI();
		$alleventlist = & $this->get( 'Data');
		// ========= For paginaion =========================================
		$limit		= JRequest::getVar('limit', 10, '', 'int');
		$limitstart	= JRequest::getVar('limitstart', 0, '', 'int');
		$total =& $this->get('total');
		jimport('joomla.html.pagination'); 
		$this->pagination = new JPagination($total, $limitstart, $limit);
		// =================================================================
		
		$options[] = JHtml::_('select.option', '1', JText::_('DATE')); 
		$options[] = JHtml::_('select.option', '2', JText::_('EVENT_TITLE')); 
		
		$selected_data	= JRequest::getInt('mysortdata','1');
		
		//echo $selected_data;
		
		$lists['mysortdata'] 	= JHTML::_('select.genericlist',$options,'mysortdata', 'class="inputtext" onchange="sorteventdata(this.value)" ', 'value', 'text',$selected_data);
		
		$this->assignRef('alleventlist',$alleventlist);
		$this->assignRef('lists',$lists); 
		parent::display($tpl);
	}
}
?>