<?php 
/**
* @package   JE Ajax Event Calendar
* @copyright Copyright (C) 2009 - 2010 Open Source Matters. All rights reserved.
* @license   http://www.gnu.org/licenses/lgpl.html GNU/LGPL, see LICENSE.php
* Contact to : emailtohardik@gmail.com, joomextensions@gmail.com
* Visit : http://www.joomlaextensions.co.in/
**/

defined('_JEXEC') or die('Restricted access');
$mosConfig_live_site = JURI :: base();
$doc =& JFactory::getDocument();
$db = JFactory::getDbo();
$option = JRequest::getVar('option','','request','string');
$doc->addStyleSheet("components/".$option."/assets/css/style.css");
$model = $this->getModel ( 'alleventlist' );
$user 		= clone(JFactory::getUser());
$insert_user= $user->id;
$uri =& JURI::getInstance();
$url= $uri->root();
$Itemid = JRequest::getVar('Itemid','','request','int');
// =============== For light box ===================================================== //
$doc->addStyleSheet($url."components/".$option."/assets/lightbox/css/mylightbox.css");
$doc->addScript($url."components/".$option."/assets/lightbox/js/jquery.min.js");
$doc->addScript($url."components/".$option."/assets/lightbox/js/jquery-ui-1.8.2.custom.min.js");
$doc->addScript($url."components/".$option."/assets/lightbox/js/pirobox_extended.js");
// =================================================================================== //
$eimg_path	= $url."components/".$option."/assets/event/images/";
$dynemic_img_path	= $url."components/".$option."/assets/images/";

// ======================== Date Convert Calss ================== //
$mydate_formate	= new gencalendar();
// ==================== EOF Date Convert Calss ================== //
$sqlapi = "SELECT * FROM #__jeajx_event_configration WHERE id =1";
$db->setQuery($sqlapi);
$googleapi = $db->LoadObject();

?>

<script type="text/javascript"  language="javascript">
	function sorteventdata() {
		//alert('Bhavesh');
		document.adminForm.submit();
	}
</script>

<!-- =============================================== Light Box Js =========================================================  -->
<script type="text/javascript">
$(document).ready(function() {
	$().piroBox_ext({
	piro_speed : 700,
	bg_alpha : 0.5,
	piro_scroll : true // pirobox always positioned at the center of the page
	});
});
</script>
<!-- =============================================== EOF Light Box Js =====================================================  -->

<?php 	
		$mainframe	= JFactory::getApplication();
		$rss_imgpath	= $url.'components/'.$option.'/assets/images/rss.png';	
		$redconfig 	= &$mainframe->getParams();
		$category_id=$redconfig->get('cat_id','0');
		$rss_link	= JRoute::_('index.php?option=com_jeajaxeventcalendar&view=event&task=rss_feed&Itemid='.$Itemid.'&category='.$category_id);
		
		
		$selected_data	= JRequest::getInt('mysortdata','1');
		$event_orderby	= JRequest::getWord('eorder','a');
		if($event_orderby=='a') {
			$eorder_link	= JRoute::_('index.php?option=com_jeajaxeventcalendar&view=alleventlist&eorder=d&mysortdata='.$selected_data.'&Itemid='.$Itemid);
			$eorder_image	= 'sort_asc.png';
			$atitle	= JText::_('ASCENDING_ORDER');
			$eorder_value	= 'd';
		} else {
			$eorder_link	= JRoute::_('index.php?option=com_jeajaxeventcalendar&view=alleventlist&eorder=a&mysortdata='.$selected_data.'&Itemid='.$Itemid);
			$eorder_image	= 'sort_desc.png';
			$eorder_value	= 'a';
			$atitle	= JText::_('DECENDING_ORDER');
		}
?>
<form action="<?php echo JRoute::_('index.php?option='.$option); ?>" method="post" name="adminForm" enctype="multipart/form-data" >
<div class="componentheading" >
	<table width="100%">
		<tr>
			<td><?php echo JText::_('ALL_EVENTS'); ?></td>
			<td align="right">
				<table>
					<tr><td><?php echo $this->lists['mysortdata']; ?></td>
						<td><a href="<?php echo $eorder_link; ?>" title="<?php echo $atitle; ?>"><img src="<?php echo $dynemic_img_path.$eorder_image; ?>" border="0" /></a><input type="hidden" name="eorder" id="eorder" value="<?php echo $event_orderby; ?>" /></td>
					</tr>
				</table>
			</td>
<?php	if($googleapi->show_rss==1) { ?>		
			<td align="right"><a href="<?php echo $rss_link; ?>" ><img src="<?php echo $rss_imgpath; ?>" height="20" width="20" border="0" /></a></td>
<?php	}	 ?>
		</tr>
	</table>
</div>
<br />

  <?php  	
		
		$fields=$model->getfields(2);
		for($i=0;$i<count($this->alleventlist);$i++) { 
			$link_edetail = JRoute::_('index.php?option=com_jeajaxeventcalendar&view=alleventlist_more&Itemid='.$Itemid.'&event_id='.$this->alleventlist[$i]->id);
			//$gmaplink = JRoute::_('index.php?option=com_jeajaxeventcalendar&view=google_map&Itemid='.$Itemid.'&event_id='.$this->alleventlist[$i]->id);
			$description = $this->alleventlist[$i]->desc;
			if(strlen($description)>100) { 
				$description = substr($description,0,100);
				$description = $description.' <a href="'.$link_edetail.'" >(Read more)</a>';
			} else {
				$description = $description.' <a href="'.$link_edetail.'" ></a>';
			}
			$date_exp = explode('-',$this->alleventlist[$i]->start_date);
			$y = $date_exp[0];
			$m = $date_exp[1];
			$d = $date_exp[2];
				
			// ============================= Convert month ===================== //		
			$month	= $mydate_formate->gen_formatmonth($date_exp[1]);
			// =========================== EOF Convert month =================== //
			
			$date = $date_exp[2].' '.$month.' '.$date_exp[0];
			$date_l = date("Y-m-d", mktime(0, 0, 0, $m, $d, $y));
			
			$date_endexp = explode('-',$this->alleventlist[$i]->end_date);
			$endy = $date_endexp[0];
			$endd = $date_endexp[2];
			
			// ============================= Convert month ===================== //	
			$endm	= $mydate_formate->gen_formatmonth($date_endexp[1]);
			// =========================== EOF Convert month =================== //
			
			$enddate = $endd.' '.$endm.'-'.$endy;
			
			if($googleapi->gmap_display==1)
			{

 				// ================== This code for GOOGLE MAP ===================================================
				$gmaplink 	= JRoute::_('index.php?tmpl=component&option='.$option.'&view=google_map&Itemid='.$Itemid.'&eventid='.$this->alleventlist[$i]->id);
				$gmapicon = $url.'components/com_jeajaxeventcalendar/assets/images/google-map-icon.png';
				$gmap_icon = "<a  style=\"color:#0099FF;\"  href='".$gmaplink."' title=\"Google MAP\" onclick=\"window.open(this.href,'win2','status=no,toolbar=no,scrollbars=yes,titlebar=no,menubar=no,resizable=yes,width=".$googleapi->gmap_width.",height=".$googleapi->gmap_height.",directories=no,location=no'); return false;\" rel=\"nofollow\"><img src='".$gmapicon."' height=\"16px\" width=\"15px\" border=\"0\"></a>";
				// ===============================================================================================
			} else
				$gmap_icon ="";	

			// ======== Design comes from database ============================================================
			$tempt_setting=$model->gettempsetting();
			$tempt_allevent = $tempt_setting->alleventlist_tempt;
			$eventphoto	= $model->eventmainphoto($this->alleventlist[$i]->id);
			if(count($eventphoto)!=0)
				$event_mainphoto = $eimg_path.'thumb_'.$eventphoto->image; 
			else
				$event_mainphoto = $eimg_path.'noimage.png';
			$event_photo = '<a href="'.$link_edetail.'" ><img src="'.$event_mainphoto.'" border="0" ></a>';
			$address	= $this->alleventlist[$i]->city.','.$this->alleventlist[$i]->country;
			$eventcategory	= $model->eventcategory($this->alleventlist[$i]->category);
			$catname	= $eventcategory->ename;
			$link_edetail_date = JRoute::_('index.php?option=com_jeajaxeventcalendar&view=event_list&Itemid='.$Itemid.'&ey='.$date_exp[0].'&em='.$date_exp[1].'&ed='.$date_exp[2]);
			$date1 = '<a href="'.$link_edetail_date.'" >'.$date.'</a>';
			$title = '<a href="'.$link_edetail.'" >'.$this->alleventlist[$i]->title.'</a>';
			$gmap_icon = $gmap_icon;
			// +++++++++++++++++++++++++++++++++++ Video Light Box ++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++ //		
			$video_w	= 500;
			$video_h	= 350;
			$link_lightbox = "";
			if($this->alleventlist[$i]->youtubelink!="") { 
				$f_name = base64_encode($this->alleventlist[$i]->youtubelink);
				$link_lightbox = JRoute::_("index.php?tmpl=component&option=".$option."&view=lightbox_dis&youtube=1&Itemid=".$Itemid.'&f_name='.$f_name.'&video_w='.$video_w.'&video_h='.$video_h);
			} else if($this->alleventlist[$i]->googlelink!="") {
				$f_name = base64_encode($this->alleventlist[$i]->googlelink);
				$link_lightbox = JRoute::_("index.php?tmpl=component&option=".$option."&view=lightbox_dis&glink=1&Itemid=".$Itemid.'&f_name='.$f_name.'&video_w='.$video_w.'&video_h='.$video_h);
			}
			
			if($link_lightbox!="")
				$video_link = '<a href="'.$url.$link_lightbox.'" rel="iframe-530-370"  class="pirobox_gall1">'.JText::_('WATCH_VIDEO').'</a>';
			else
				$video_link = '';
			// +++++++++++++++++++++++++++++++++++ EOF Video Light Box ++++++++++++++++++++++++++++++++++++++++++++++++++++++++ //	
			$tempt_allevent = str_replace('{date_link}',$date1,$tempt_allevent);
			$tempt_allevent = str_replace('{event_date}',$date,$tempt_allevent);
			$tempt_allevent = str_replace('{end_date}',$enddate,$tempt_allevent);
			$tempt_allevent = str_replace('{title}',$title,$tempt_allevent);
			$tempt_allevent = str_replace('{gmap_icon}',$gmap_icon,$tempt_allevent);
			$tempt_allevent = str_replace('{description}',$description,$tempt_allevent);
			$tempt_allevent = str_replace('{video_link}',$video_link,$tempt_allevent);
			$tempt_allevent = str_replace('{event_photo}',$event_photo,$tempt_allevent);
			$tempt_allevent = str_replace('{address}',$address,$tempt_allevent);
			$tempt_allevent = str_replace('{catname}',$catname,$tempt_allevent);
		
		$outlook_icon 	= preg_match('{outlook}',$tempt_allevent);
		$icalendar_icon = preg_match('{ical}',$tempt_allevent);
		$gcalendar_icon = preg_match('{gcal}',$tempt_allevent);	
		// =========================== Hits Display Code ============================== //
			$eventhits_tag = preg_match('{hits}',$tempt_allevent);
			if($eventhits_tag==1) {
				$event_hits	= JText::_('HITS').':'.$this->alleventlist[$i]->ehits;
				$tempt_allevent = str_replace('{hits}',$event_hits,$tempt_allevent);
			}
		// =========================== Hits Display Code ============================== //
		
		if($outlook_icon==1) {
		// ============================= Code for OutLook Calendar =============================================================//
			$link_outlook = JRoute::_("index.php?option=".$option."&view=mycalender&task=generate_outlook&Itemid=".$Itemid.'&event_id='.$this->alleventlist[$i]->id);
			$outlook = '<a href="'.$link_outlook.'" ><img src="'.$dynemic_img_path.'outlook.png" border="0" ></a>';
			$tempt_allevent = str_replace('{outlook}',$outlook,$tempt_allevent);
		// ============================= EOF Code for OutLook Calendar =========================================================//
		}
		if($icalendar_icon==1) {
		// ============================= Code for Ical Calendar ===============================================================//
			$link_ical = JRoute::_("index.php?option=".$option."&view=mycalender&task=generate_ical&Itemid=".$Itemid.'&event_id='.$this->alleventlist[$i]->id);
			$ical = '<a href="'.$link_ical.'" ><img src="'.$dynemic_img_path.'ical.png" border="0" ></a>';
			$tempt_allevent = str_replace('{ical}',$ical,$tempt_allevent);
		// ============================= EOF Code for Ical Calendar ===========================================================//
		}
		if($gcalendar_icon==1) {
		// ============================= Code for Google Calendar =============================================================//
			$link_gcal = JRoute::_("index.php?option=".$option."&view=mycalender&task=gcalendar&Itemid=".$Itemid.'&event_id='.$this->alleventlist[$i]->id);
			$gcal = '<a href="'.$link_gcal.'" ><img src="'.$dynemic_img_path.'gcalendar.png" border="0" ></a>';
			$tempt_allevent = str_replace('{gcal}',$gcal,$tempt_allevent);
		// ============================= EOF Code for Google Calendar =========================================================//
		}	
			
			for($j=0;$j<count($fields);$j++)
			{
				// =============== Getting Dynemic Fields value ===================	//		
				$fieldsvalue=$model->fieldsvalue($fields[$j]->field_id,$this->alleventlist[$i]->id);
				// ================================================================	//
				if(count($fieldsvalue)!=0) {
					$fieldname= '{'.$fields[$j]->field_name.'}';
					// ============ Image Field =========================================================================
					if($fields[$j]->field_type==9)
					{
						$myfile	= $fieldsvalue->data_txt;
						if($myfile!='') {
							$myfile	= strtolower($myfile);
							$fileext = strstr($myfile, '.');
							$filedata = '';
							if($fileext==".jpeg" || $fileext==".jpg" || $fileext==".png" || $fileext==".gif" || $fileext==".bmp" || $fileext==".tif" || $fileext==".psd") {
								$filedata	.= '<img src="'.$dynemic_img_path.$myfile.'" height="100px" width="100px;" />';
							} else {
								$filedata	.= '<a href="'.$dynemic_img_path.$fieldsvalue->data_txt.'" />'.$myfile.'</a>';
							}
						} else {
							$filedata	= '';
						}
						$tempt_allevent = str_replace($fieldname,$filedata,$tempt_allevent); 
					} else if($fields[$j]->field_type==7) {
						$country_id	= $fieldsvalue->data_txt;
						$country	= $model->getcountry($country_id);
						$tempt_allevent = str_replace($fieldname,$country->country_name,$tempt_allevent);
					} else if($fields[$j]->field_type==10) {
						$hrtag	= '<hr>';
						$tempt_allevent = str_replace($fieldname,$hrtag,$tempt_allevent);
					} else {
						$tempt_allevent = str_replace($fieldname,$fieldsvalue->data_txt,$tempt_allevent); 
					}
				}
			}
		echo $tempt_allevent;
	// ===================================================================================================
  }  ?>
  <table cellpadding="0" cellspacing="0"   align="center" width="100%">
    <tr>
      <td valign="top" align="center"><?php echo $this->pagination->getPagesLinks(); ?> <br />
        <br />
      </td>
    </tr>
    <tr>
      <td valign="top" align="center"><?php echo $this->pagination->getPagesCounter(); ?> </td>
    </tr>
  </table>
  <div style="clear:both;"></div>
  <input type="hidden" name="Itemid" value="<?php echo $Itemid; ?>"/>
  <input type="hidden" name="option" value="<?php echo $option;?>">
  <input type="hidden" name="view" value="alleventlist" />
  <input type="hidden" name="task" value="" />
  <input type="hidden" name="boxchecked" value="0" />
  <input type="hidden" name="filter_order" value="<?php echo $this->lists['order']; ?>" />
  <input type="hidden" name="filter_order_Dir" value="<?php echo $this->lists['order_Dir']; ?>" />
</form>
