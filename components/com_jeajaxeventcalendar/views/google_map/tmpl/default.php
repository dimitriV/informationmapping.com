<?php
/**
* @package   JE Ajax Event Calendar
* @copyright Copyright (C) 2009 - 2010 Open Source Matters. All rights reserved.
* @license   http://www.gnu.org/licenses/lgpl.html GNU/LGPL, see LICENSE.php
* Contact to : emailtohardik@gmail.com, joomextensions@gmail.com
* Visit : http://www.joomlaextensions.co.in/
**/

defined('_JEXEC') or die('Restricted access');
$eventid = JRequest::getVar('eventid','','','int');
$db	= & JFactory::getDBO();
$this->_table_prefix = '#__jeajx_';
$sqlapi = "SELECT gmap_api,gmap_width,gmap_height,gmap_display FROM #__jeajx_event_configration WHERE id =1";
$db->setQuery($sqlapi);
$googleapi = $db->LoadObject();
$sql ="SELECT e.street,e.postcode,e.city,e.country FROM ".$this->_table_prefix."event AS e WHERE e.id = ".$eventid;
$db->setQuery($sql);
$statelist = $db->LoadObject();
$location 	= $statelist->street;
$add1 		= $statelist->city;
$add3 		= $statelist->country;
$add4 		= $statelist->postcode;
$address = "";
$linkadd = "";
$address = $location;
$linkadd = $location;
$address .= ','.$add1;
$linkadd .= ','.$add1;
if($add3!="")
	$address .= ','.$add3;
$linkadd .= ',<br>'.$add3;
if($add4!="")
	$address .= ','.$add4;
$linkadd .= ','.$add4;

$pp = '<div style="width:210px;padding-right:10px; background:#FFFFFF; "><table><tr><td width="60px">'.$linkadd.'</td></tr></table></div>';
?>
<script src="http://maps.google.com/maps?file=api&amp;v=2&amp;key=<?php echo $googleapi->gmap_api ;?>&sensor=true" type="text/javascript"></script>

<script type="text/javascript">

	

    var map = null;

    var geocoder = null;

	var WINDOW_HTML = '<?php echo $pp; ?>';

	

	function initialize() {

      if (GBrowserIsCompatible()) {



	  	var mapOptions = {

            googleBarOptions : {

              style : "new"

            }

          }



        map = new GMap2(document.getElementById("map_canvas"));

        map.setCenter(new GLatLng(41.393294, -77.189941), 13);

	//	map.addOverlay(new GLatLng(37.4419, -122.1419);

		 map.setUIToDefault();

      //    map.enableGoogleBar();

        geocoder = new GClientGeocoder();

      }

    }



    function showAddress(address) {

	 //alert(address);

      if (geocoder) {

        geocoder.getLatLng(

          address,

          function(point) {

		  

            if (!point) {

              alert(address + " not found");

            } else {

              map.setCenter(point, 6);

			  

              var marker = new GMarker(point);

              map.addOverlay(marker);

			  GEvent.addListener(marker, "click", function() {

	//marker.openInfoWindowHtml(WINDOW_HTML);

	  });

	marker.openInfoWindowHtml(WINDOW_HTML);	

             

            }

          }

        );

      }

    }

	//initialize();



	window.onload=function(){

		initialize();

		

		showAddress("<?php echo $address?>");

		/*showAddress("surat");

		showAddress("visnagar");*/

	}

	

	window.onunload=function(){

		GUnload();

	}

</script>

<div id="map_canvas" style="width: <?php echo $googleapi->gmap_width;?>px; height: <?php echo $googleapi->gmap_height;?>px; "></div>
