<?php
/**
* @package   JE Ajax Event Calendar
* @copyright Copyright (C) 2009 - 2010 Open Source Matters. All rights reserved.
* @license   http://www.gnu.org/licenses/lgpl.html GNU/LGPL, see LICENSE.php
* Contact to : emailtohardik@gmail.com, joomextensions@gmail.com
* Visit : http://www.joomlaextensions.co.in/
**/

defined( '_JEXEC' ) or die( 'Restricted access' );
jimport( 'joomla.application.component.view' );

class lightbox_disViewlightbox_dis extends JViewLegacy
{
	function __construct( $config = array())
	{
		parent::__construct( $config );
	}

	function display($tpl = null)
	{	
// ========================= Page title code ================================== //	
		$mainframe 	= JFactory::getApplication();
		$document 	= & JFactory::getDocument();
		$pagetitle 	= $document->getTitle();
		if($mainframe->getCfg( 'sitename_pagetitles' )==2) // After page title
			$document->setTitle($pagetitle.' - '.$mainframe->getCfg( 'sitename' ) );
		else if($mainframe->getCfg( 'sitename_pagetitles' )==1) // Before page title
			$document->setTitle($mainframe->getCfg( 'sitename' ).' - '.$pagetitle );
		else // no page title
			$document->setTitle($pagetitle );
// ======================= EOF Page title code ================================ //	
		$uri	=& JFactory::getURI();
		parent::display($tpl);
	}
}
?>
