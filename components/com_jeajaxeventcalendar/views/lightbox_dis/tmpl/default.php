<?php 
/**
* @package   JE Ajax Event Calendar
* @copyright Copyright (C) 2009 - 2010 Open Source Matters. All rights reserved.
* @license   http://www.gnu.org/licenses/lgpl.html GNU/LGPL, see LICENSE.php
* Contact to : emailtohardik@gmail.com, joomextensions@gmail.com
* Visit : http://www.joomlaextensions.co.in/
**/

defined('_JEXEC') or die('Restricted access');
$doc =& JFactory::getDocument();
$option = JRequest::getVar('option','','request','string');
$Itemid = JRequest::getVar('Itemid','','request','int');		
$user 		= clone(JFactory::getUser());
$uri =& JURI::getInstance();
$url= $uri->root();
$f_name = JRequest::getVar('f_name','','request','string');
$youtube = JRequest::getVar('youtube','','request','int');
$glink = JRequest::getVar('glink','','request','int');
$video_w = JRequest::getVar('video_w','','request','int');
$video_h = JRequest::getVar('video_h','','request','int');
if($youtube==1) { 
	$video_w = $video_w-10;
	$video_h = $video_h-10;
	$file_name = base64_decode($f_name);
	// ============== Function for find the last (=) sign in the string =================== //
	$tempstr = strstr($file_name, '='); 
	// ====================================================================================	//
	// =========== Return the allt he string eccept the first letter of the string ========	//	
	$ext = substr($tempstr, 1);
	// ====================================================================================	//		
?>

<table>
  <tr>
    <td><object width="<?php echo $video_w; ?>px" height="<?php echo $video_h; ?>px">
        <param name="movie" value="http://www.youtube.com/v/<?php echo $ext; ?>?fs=1&amp;hl=en_US">
        </param>
        <param name="allowFullScreen" value="true">
        </param>
        <param name="allowscriptaccess" value="always">
        </param>
        <embed src="http://www.youtube.com/v/<?php echo $ext; ?>?fs=1&amp;hl=en_US" type="application/x-shockwave-flash" allowscriptaccess="always" allowfullscreen="true" width="<?php echo $video_w; ?>px" height="<?php echo $video_h; ?>px"> </embed>
      </object>
    </td>
  </tr>
</table>
<?php } else if($glink==1) { 
		$video_w = $video_w-10;
		$video_h = $video_h-10;
		$file_name = base64_decode($f_name);
		// ============== Function for find the last (=) sign in the string =================== //
		$tempstr = strrchr($file_name, '=');
		// ==================================================================================== //
		// =========== Return the allt he string eccept the first letter of the string ========	//	
		$ext = substr($tempstr, 1);
		// ====================================================================================	//
?>
<table>
  <tr>
    <td><object width="<?php echo $video_w; ?>px" height="<?php echo $video_h; ?>">
        <param name="movie" value="http://video.google.com/googleplayer.swf?docId=<?php echo $ext; ?>&hl=en">
        </param>
        <param name="allowFullScreen" value="true">
        </param>
        <param name="allowscriptaccess" value="always">
        </param>
        <embed src="http://video.google.com/googleplayer.swf?docId=<?php echo $ext; ?>&hl=en" type="application/x-shockwave-flash" allowscriptaccess="always" allowfullscreen="true" width="<?php echo $video_w; ?>px" height="<?php echo $video_h; ?>px"> </embed>
      </object>
    </td>
  </tr>
</table>
<?php }  ?>