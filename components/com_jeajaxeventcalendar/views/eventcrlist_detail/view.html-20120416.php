<?php
/**
* @package   JE Ajax Event Calendar
* @copyright Copyright (C) 2009 - 2010 Open Source Matters. All rights reserved.
* @license   http://www.gnu.org/licenses/lgpl.html GNU/LGPL, see LICENSE.php
* Contact to : emailtohardik@gmail.com, joomextensions@gmail.com
* Visit : http://www.joomlaextensions.co.in/
**/

defined( '_JEXEC' ) or die( 'Restricted access' );
jimport( 'joomla.application.component.view' );

class eventcrlist_detailVieweventcrlist_detail extends JView
{
	function display($tpl = null)
	{
		$document = & JFactory::getDocument();
		$document->setTitle( JText::_('EVENT_MANAGEMENT') );
		$uri 		=& JFactory::getURI();
		$option = JRequest::getVar('option','','request','string');
		// ============= Color picker field ===============================================================
		$document->addScript('components/'.$option.'/assets/color_picker/js/jquery.js');
		$document->addScript('components/'.$option.'/assets/color_picker/js/colorpicker.js');
		$document->addScript('components/'.$option.'/assets/color_picker/js/eye.js');
		$document->addScript('components/'.$option.'/assets/color_picker/js/utils.js');
		$document->addScript('components/'.$option.'/assets/color_picker/js/layout.js?ver=1.0.2');
		$document->addScript('components/'.$option.'/assets/js/validation.js');
		$document->addStyleSheet('components/'.$option.'/assets/color_picker/css/colorpicker.css');
		// ================================================================================================
		$this->setLayout('default');
		$lists = array();
		$detail	=& $this->get('data');
		$category	=& $this->get('category');
		
		$sel_category = array();
		$sel_category[0]->value="0";
		$sel_category[0]->text=JText::_('SELECT_CATEGORY');
		$category=@array_merge($sel_category,$category);
		$lists['category'] 	= JHTML::_('select.genericlist',$category,'category1', 'class="inputtext" ', 'value', 'text', $detail->category ); 

		$loginuser	= clone(JFactory::getuser());
		$useraccess = key($loginuser->groups);
		if($useraccess == "Super Users") {
			$user =& $this->get('user');
			$sel_user = array();
			$sel_user[0]->value = '0';
			$sel_user[0]->text = JText::_('ALL');
			$user = @array_merge($sel_user,$user);
			
			if($detail->usr!="")
				$exp_user = explode('`',$detail->usr);
			else
				$exp_user = 0;	
			
			$lists['usr'] 	= JHTML::_('select.genericlist',$user,'usr[]', ' multiple="multiple" ', 'value', 'text',$exp_user);
		} else {
			$lists['usr']	= 0;
		}	
		
		$event_repeat = array();
		$event_repeat[]   = JHTML::_('select.option', '0"checked"',JText::_('NO_REPEAT'));
		$event_repeat[]   = JHTML::_('select.option', '1', JText::_('YEAR'));
		$event_repeat[]   = JHTML::_('select.option', '2', JText::_('MONTH'));
		$event_repeat[]   = JHTML::_('select.option', '3', JText::_('WEEK'));
		$event_repeat[]   = JHTML::_('select.option', '4', JText::_('MULTIPLE_DATES'));
		
		$lists['erepeat'] 		= JHTML::_('select.genericlist',$event_repeat,'erepeat', 'class="inputbox" onchange="select_repeat(this.value)" size="1" ', 'value', 'text', $detail->erepeat );
		
		$eventphoto	= & $this->get('eventphoto');
	
		$this->assignRef('eventphoto',		$eventphoto);
		$this->assignRef('lists',		$lists);
		$this->assignRef('detail',		$detail);
		$this->assignRef('request_url',	$uri->toString());
		parent::display($tpl);
	}
}
?>