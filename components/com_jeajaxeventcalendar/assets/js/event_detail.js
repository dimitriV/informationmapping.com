var old_divid='';
var old_liid='';
function showdiv(divid,id) {
	if(old_divid=='') {
		var default_div	= document.getElementById("showevent").value;
		if(default_div=="event") {
			document.getElementById("event_div").style.display='none';
			document.getElementById("event").style.backgroundColor = "#438DE0";
			document.getElementById("event").style.color = "#FFFFFF";
		}
		document.getElementById(id).style.color = "#000000";
		document.getElementById(id).style.backgroundColor = "#D9EBFF";
		document.getElementById(divid).style.display='block';
		old_divid = divid;
		old_liid = id;
	} else {
		document.getElementById(old_divid).style.display='none';
		document.getElementById(old_liid).style.backgroundColor = "#438DE0";
		document.getElementById(id).style.backgroundColor = "#D9EBFF";
		
		document.getElementById(old_liid).style.color = "#FFFFFF";
		document.getElementById(id).style.color = "#000000";
		
		document.getElementById(divid).style.display='block';
		old_divid = divid;
		old_liid = id;
	}
}
var old_pid ='image_0';

function showphoto(pname,pid) {
	document.getElementById(old_pid).style.borderColor ='#fff';
	document.getElementById(pid).style.borderColor ='#B6EDED';
	old_pid = pid;
	document.getElementById("main_photo").src = pname;
}

function show_div(divid) {
	document.getElementById(divid).style.display='block';
}

function hidediv(divid) {
	document.getElementById(divid).style.display='none';
}