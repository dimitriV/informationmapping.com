<?php
/**
* @package   JE Auto
* @copyright Copyright (C) 2009 - 2010 Open Source Matters. All rights reserved.
* @license   http://www.gnu.org/licenses/lgpl.html GNU/LGPL, see LICENSE.php
* Contact to : emailtohardik@gmail.com, joomextensions@gmail.com
**/
    
if( !defined( '_VALID_MOS' ) && !defined( '_JEXEC' ) ) die( 'Direct Access to '.basename(__FILE__).' is not allowed.' );

class gencalendar {
	
	function outlook() {
		$db 	= JFactory::getDbo();
		$option 	= JRequest::getVar('option','','request','string');
		$event_id 	= JRequest::getInt('event_id');
		$query1 	= "SELECT * FROM #__jeajx_event WHERE id=".$event_id;
		$db->setQuery($query1);
		$event_data	= $db->loadObject();
		
		$exp_startdate	= $event_data->start_date;
		$exp_sdate		= explode('-',$exp_startdate);
		$startdateonly 	= $exp_sdate[0].$exp_sdate[1].$exp_sdate[2];
		$starttimeonly 	= '000000';
		$startdate		= $startdateonly."T".$starttimeonly;
		
		$exp_enddate	= $event_data->end_date;
		$exp_edate		= explode('-',$exp_enddate);
		$enddateonly 	= $exp_edate[0].$exp_edate[1].$exp_edate[2];
		$endtimeonly 	= '000000';
		$enddate		= $enddateonly."T".$endtimeonly;
		//$ctimestamp = mktime(date("H") ,date("i"),date("s") ,date("n") ,date("j") ,date("Y") );
		$category_id	= $event_data->category;
		$query2 = "SELECT id,ename FROM #__jeajx_cal_category WHERE id=".$category_id;
		$db->setQuery($query2);
		$category_data	= $db->loadObject();
		
		$description	= $event_data->desc;
		
		$search = array ("'<script[^>]*?>.*?</script>'si",  // Strip out javascript
                 "'<[/!]*?[^<>]*?>'si",          // Strip out HTML tags
               /*  "'([rn])[s]+'",*/                // Strip out white space
                 "'&(quot|#34);'i",                // Replace HTML entities
                 "'&(amp|#38);'i",
                 "'&(lt|#60);'i",
                 "'&(gt|#62);'i",
                 "'&(nbsp|#160);'i",
                 "'&(iexcl|#161);'i",
                 "'&(cent|#162);'i",
                 "'&(pound|#163);'i",
                 "'&(copy|#169);'i",
                 "'&#(d+);'e");                    // evaluate as php

		$replace = array ("",
                 "",
                /* "\1",*/
                 "\"",
                 "&",
                 "<",
                 ">",
                 " ",
                 chr(161),
                 chr(162),
                 chr(163),
                 chr(169),
                 "chr(\1)");
				 
		$description = preg_replace($search,$replace,$description);
		$description = mysql_escape_string($description);
		$description = str_replace('\r\n', '=0D=0A', $description);
		
		$location	= $event_data->street.','.$event_data->city.','.$event_data->country;
		$data = "BEGIN:VCALENDAR
VERSION:1.0
BEGIN:VEVENT
CATEGORIES: ".$category_data->ename."
STATUS:NEEDS ACTION
DTSTART:".$startdate."Z
DTEND:".$enddate."Z
SUMMARY:".$event_data->title."
DESCRIPTION;ENCODING=QUOTED-PRINTABLE:".$description."
LOCATION:".$location."
CLASS:PRIVATE
PRIORITY:3
END:VEVENT
END:VCALENDAR";

		echo $data;
		$file_name =$event_data->title.'.vcs';
		header("Content-Type: text/x-vCalendar");	
		header('Content-type: application/vcs');
		header('Content-Disposition: attachment; filename="'.$file_name.'"');
		exit;	
	}
	
	function icalendar() {
		$uri =& JURI::getInstance();
		$url= $uri->root();
		$db 	= JFactory::getDbo();
		$option 	= JRequest::getVar('option','','request','string');
		$event_id 	= JRequest::getInt('event_id');
		$query1 	= "SELECT * FROM #__jeajx_event WHERE id=".$event_id;
		$db->setQuery($query1);
		$event_data	= $db->loadObject();
		
		$exp_startdate	= $event_data->start_date;
		$exp_sdate		= explode('-',$exp_startdate);
		$startdateonly 	= $exp_sdate[0].$exp_sdate[1].$exp_sdate[2];
		$starttimeonly 	= '000000';
		$startdate		= $startdateonly."T".$starttimeonly;
		
		$exp_enddate	= $event_data->end_date;
		$exp_edate		= explode('-',$exp_enddate);
		$enddateonly 	= $exp_edate[0].$exp_edate[1].$exp_edate[2];
		$endtimeonly 	= '000000';
		$enddate		= $enddateonly."T".$endtimeonly;
		//$ctimestamp = mktime(date("H") ,date("i"),date("s") ,date("n") ,date("j") ,date("Y") );
		$category_id	= $event_data->category;
		$query2 = "SELECT id,ename FROM #__jeajx_cal_category WHERE id=".$category_id;
		$db->setQuery($query2);
		$category_data	= $db->loadObject();
		
		$description	= $event_data->desc;
		
		$search = array ("'<script[^>]*?>.*?</script>'si",  // Strip out javascript
                 "'<[/!]*?[^<>]*?>'si",          // Strip out HTML tags
               /*  "'([rn])[s]+'",*/                // Strip out white space
                 "'&(quot|#34);'i",                // Replace HTML entities
                 "'&(amp|#38);'i",
                 "'&(lt|#60);'i",
                 "'&(gt|#62);'i",
                 "'&(nbsp|#160);'i",
                 "'&(iexcl|#161);'i",
                 "'&(cent|#162);'i",
                 "'&(pound|#163);'i",
                 "'&(copy|#169);'i",
                 "'&#(d+);'e");                    // evaluate as php

		$replace = array ("",
                 "",
                /* "\1",*/
                 "\"",
                 "&",
                 "<",
                 ">",
                 " ",
                 chr(161),
                 chr(162),
                 chr(163),
                 chr(169),
                 "chr(\1)");
				 
			 

		$description = preg_replace($search,$replace,$description);
		$description = mysql_escape_string($description);
		$description = str_replace('\r\n', '\n', $description);
		
		$location	= $event_data->street.','.$event_data->city.','.$event_data->country;
		$ical_data	= "BEGIN:VCALENDAR
PRODID:--//".$url."//NONSGML iCalcreator 2.6//EN
VERSION:2.0
METHOD:PUBLISH
X-MS-OLK-FORCEINSPECTOROPEN:TRUE
BEGIN:VEVENT
CLASS:PUBLIC
CREATED:".$startdate."Z
DESCRIPTION:".$description."
DTEND:".$enddate."Z
DTSTAMP:".$startdate."Z
CATEGORIES:".$category_data->ename."
DTSTART:".$startdate."Z
LOCATION:".$location."
PRIORITY:5
SEQUENCE:0
SUMMARY;LANGUAGE=en-us:".$event_data->title."
TRANSP:OPAQUE
END:VEVENT
END:VCALENDAR";

		
		echo $ical_data;
		$file_name	= $event_data->title.'.ics';
		header("Content-Type: text/x-iCalendar");
		header('Content-type: application/ics');
		header('Content-Disposition: attachment; filename="'.$file_name.'"');
		exit;
		
	}
	
	function gen_formatmonth($mymonths) {
		//$date_exp = explode('-',$mydates);
		if($mymonths=='01')
			$month = JText::_('JANUARY');
		if($mymonths=='02')
			$month = JText::_('FEBRUARY');
		if($mymonths=='03')
			$month = JText::_('MARCH');
		if($mymonths=='04')
			$month = JText::_('APRIL');
		if($mymonths=='05')
			$month = JText::_('MAY');
		if($mymonths=='06')
			$month = JText::_('JUNE');
		if($mymonths=='07')
			$month = JText::_('JULY');
		if($mymonths=='08')
			$month = JText::_('AUGUST');
		if($mymonths=='09')
			$month = JText::_('SEPTEMBER');
		if($mymonths=='10')
			$month = JText::_('OCTOBER');
		if($mymonths=='11')
			$month = JText::_('NOVEMBER');
		if($mymonths=='12')
			$month = JText::_('DECEMBER');
		return $month;
	}
	
	function gen_formatday($mydates) {
		if($mydates=='Monday')
			$day = JText::_('MONDAY');
		if($mydates=='Tuesday')
			$day = JText::_('TUESDAY');
		if($mydates=='Wednesday')
			$day = JText::_('WEDNESDAY');
		if($mydates=='Thursday')
			$day = JText::_('THURSDAY');
		if($mydates=='Friday')
			$day = JText::_('FRIDAY');
		if($mydates=='Saturday')
			$day = JText::_('SATURDAY');
		if($mydates=='Sunday')
			$day = JText::_('SUNDAY');	
		
		return $day;
	}
}


?>