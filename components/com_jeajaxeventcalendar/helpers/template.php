<?php
defined ('_JEXEC') or die ('Restricted access');

class eventcrlistHelperTemplate 
{
    public function getTemplateSelect()
    {
        $db = JFactory::getDBO();
        $query = 'SELECT `id`,`label`,`default` FROM `#__jeajx_form_templates` WHERE `published`=1 ORDER BY `ordering`';
        $db->setQuery($query);
        $templates = $db->loadObjectList();

        $query = 'SELECT `id`,`form_template_id`,`name`,`value` FROM `#__jeajx_form_template_values` WHERE `published`=1';
        $db->setQuery($query);
        $fieldValues = $db->loadObjectList();

        $formTemplates = array();
        foreach($fieldValues as $fieldValue) {
            $value = (string)$fieldValue->value;
	    $name = $fieldValue->name;
            if(in_array($name, array('title'))) $value = htmlspecialchars($value);
            if(!isset($formTemplates[$fieldValue->form_template_id])) $formTemplates[$fieldValue->form_template_id] = array();
            $formTemplates[$fieldValue->form_template_id][$name] = $value;
        }

        $html = '<script>';
        $html .= 'var form_templates = new Object;';
        foreach($formTemplates as $formTemplateId => $formTemplate) {
            $html .= 'form_templates["t'.$formTemplateId.'"] = '.json_encode($formTemplate).';'."\n";
        }

        $html .= 'function changeFormTemplate(element) {
    var form_template_id = "t" + jQuery(element).val();
    var form_template = form_templates[form_template_id];
    for (var key in form_template) {
      if (form_template.hasOwnProperty(key)) {
        form_value = form_template[key];
        formField = jQuery(\'[name="\' + key + \'"]\');
        if(formField) {
	    //if(formField.attr(\'type\') == \'text\') {
            	formField.val(form_value);
            //}
            if(tinyMCE) {
                formFieldId = formField.attr(\'id\');
                var editor = tinyMCE.get(formFieldId);
                if(editor) {
                    editor.focus();
                    editor.setContent(form_value);
                }
            }
            jQuery("#title").focus();
        }
      }
    }
}
';
        $html .= '</script>';

        $options = array();
        $options[] = JHTML::_('select.option', 0, JText::_('COM_JEAJAXEVENTCALENDAR_FORM_EVENT_FIELD_TEMPLATE_SELECT'));
        foreach($templates as $template) {
            $options[] = JHTML::_('select.option', $template->id, $template->label);
        }
        $html .= JHTML::_('select.genericlist', $options, 'form_template', 'onChange="return changeFormTemplate(this);" class="form-control"', 'value', 'text', $default);

        return $html;
    }
}
