<?php
/**
* @package   JE Section Finder 
* @copyright Copyright (C) 2009 - 2010 Open Source Matters. All rights reserved.
* @license   http://www.gnu.org/licenses/lgpl.html GNU/LGPL, see LICENSE.php
* Contact to : emailtohardik@gmail.com, joomextensions@gmail.com
**/

function jeajaxeventcalendarBuildRoute(&$query)
{ 
    $segments = array();     
    
    if(isset($query['view']))
    {
        $segments[] = $query['view'];
        
        if( (array_key_exists('event_id',$query)) && $query['view']=='alleventlist_more' && $query['view']!='mycalender' && $query['view']!='google_map') {
            $eventname = jeajaxeventcalendarGetevent($query['event_id']);
            $segments[] = $query['event_id'].':'.JFilterOutput::stringURLSafe($eventname);
            unset($query['event_id']);
        }
        
        if( (array_key_exists('event_id',$query)) && $query['view']=='mycalender' && $query['view']!='alleventlist_more' && $query['view']!='google_map') {
            $eventname = jeajaxeventcalendarGetevent($query['event_id']);
            $segments[] = $query['event_id'].':'.JFilterOutput::stringURLSafe($eventname);
            if($query['task']=='gcalendar')
                $query['task']    = 'My Google Calendar';
            if($query['task']=='generate_ical')
                $query['task']    = 'My ICalendar';
            if($query['task']=='generate_outlook')
                $query['task']    = 'My Outlook';        
            $segments[]        = $query['task'];
            unset($query['event_id']);
            unset($query['task']);
        }
        
        if( array_key_exists('eventid',$query)) {
            $eventname = jeajaxeventcalendarGetevent($query['eventid']);
            $segments[] = $query['eventid'].':'.JFilterOutput::stringURLSafe($eventname);
            $segments[] = 'My Component';
            unset($query['eventid']);
            unset($query['tmpl']);
        }
        
        if($query['view']=='eventcrlist') {
            if(!empty($query['cid'][0])) {
                $eventname = jeajaxeventcalendarGeteventuser($query['cid'][0]);
                $segments[] = $query['cid'][0].':'.JFilterOutput::stringURLSafe($eventname);
                $query['task'] = 'Delete '.$eventname;
                $segments[] = $query['task'];
                unset($query['cid']);
                unset($query['task']);
            }
        }
        
        if($query['view']=='eventcrlist_detail') {
            if(!empty($query['cid'][0])) {
                if($query['cid'][0]==0) {
                    $segments[] = 'Add Event';
                } else {
                    $eventname = jeajaxeventcalendarGeteventuser($query['cid'][0]);
                    $segments[] = 'Edit '.$eventname;
                }
            }
            unset($query['cid']);
        }
        
        if($query['view']=='event_list') {
        }
        
        if($query['view']=='event' && $query['task']=='rss_feed') {
            $segments[] = 'MyRss';
            if($query['category']==0)
                $segments[] = 'All Category';
            else {
                $mycatname = jeajaxeventcalendarGetcategory($query['category']);
                $segments[] = $query['category'].':'.JFilterOutput::stringURLSafe($mycatname);
            }
                
            unset($query['task']);
            unset($query['category']);
        }
        
        unset($query['view']);
        
    }
    return $segments;
}

function jeajaxeventcalendarParseRoute($segments)
{
    $vars = array();
    $count = count($segments);
    
    if(!empty($count)) {
        $vars['view'] = $segments[0];
    }
    
    if(!empty($segments[1]) && $vars['view']=='alleventlist_more') {
        if(preg_match('/^([0-9]+)\:/', $segments[1])) {
            $eventid = (int)$segments[1];
        } else {
            $eventid = jeajaxeventcalendarGeteventid($segments[1]);
        }
        $vars['event_id'] = $eventid;
    }
    
    if(!empty($segments[1]) && $vars['view']=='alleventlist_more') {
        if(preg_match('/^([0-9]+)\:/', $segments[1])) {
            $eventid = (int)$segments[1];
        } else {
            $eventid = jeajaxeventcalendarGeteventid($segments[1]);
        }
        $vars['event_id'] = $eventid;
    }
    
    if(!empty($segments[1]) && $vars['view']=='google_map') {
        if(preg_match('/^([0-9]+)\:/', $segments[1])) {
            $eventid = (int)$segments[1];
        } else {
            $eventid = jeajaxeventcalendarGeteventid($segments[1]);
        }
        $vars['eventid']= $eventid;
        $vars['tmpl']    = 'component';
    } 
    
    if(!empty($segments[1]) && $vars['view']=='mycalender') {
        if(preg_match('/^([0-9]+)\:/', $segments[1])) {
            $eventid = (int)$segments[1];
        } else {
            $eventid = jeajaxeventcalendarGeteventid($segments[1]);
        }
        $vars['event_id']     = $eventid;
        if($segments[2]=='My Google Calendar')
            $vars['task']    = 'gcalendar';
        if($segments[2]=='My ICalendar')
            $vars['task']    = 'generate_ical';
        if($segments[2]=='My Outlook')
            $vars['task']    = 'generate_outlook';        
    }
    
    if(!empty($segments[1]) && $vars['view']=='eventcrlist_detail') {
        if($segments[1]=='Add Event'){
            $vars['cid'][0]    = 0;
        } else {
            if(preg_match('/^([0-9]+)\:/', $segments[1])) {
                $eventyid = (int)$segments[1];
            } else {
                $segments[1] = str_replace('Edit ','',$segments[1]);
                $eventyid = jeajaxeventcalendarGeteventiduser($segments[1]);
            }
            $vars['cid'][0] = $eventyid;
        }
    }
    
    if(!empty($segments[1]) && $vars['view']=='eventcrlist') {
        if(preg_match('/^([0-9]+)\:/', $segments[1])) {
            $eventyid = (int)$segments[1];
        } else {
            $eventyid = jeajaxeventcalendarGeteventiduser($segments[1]);
        }
        $vars['cid'][0] = $eventyid;
        $vars['task']    = 'delete';        
        
    }
    
    if($vars['view']=='event') {
        if($segments[1]=='MyRss')
            $segments[1] = 'rss_feed';
        $vars['task']    = $segments[1];
        
        if($segments[2]=='All Category') {
            $vars['category']    = '0';
        } else {
            if(preg_match('/^([0-9]+)\:/', $segments[1])) {
                $mycatid = (int)$segments[1];
            } else {
                $mycatid = jeajaxeventcalendarGetcategoryid($segments[2]);
            }
            $vars['category'] = $mycatid;
        }
    }
    return $vars;
}

function jeajaxeventcalendarGetevent($event_id)
{
    $db = JFactory::getDBO();
    $sql = "SELECT `title` FROM #__jeajx_event WHERE `id`=".$event_id;
    $db->setQuery($sql);
    $title = $db->loadResult();
    return $title;
}

function jeajaxeventcalendarGeteventid($event_name)
{
    $db = JFactory::getDBO();
    $sql = "SELECT `id` FROM #__jeajx_event WHERE `title`='".$event_name."' ";
    $db->setQuery($sql);
    $id = $db->loadResult();
    return $id;
}

function jeajaxeventcalendarGeteventuser($event_id)
{
    $user = clone(JFactory::getuser());
    $user_id= $user->id;
    $db = JFactory::getDBO();
    $sql = "SELECT `title` FROM #__jeajx_event WHERE `id`=".$event_id.' AND insert_user='.$user_id;
    $db->setQuery($sql);
    $event_name = $db->loadResult();
    return $event_name;
}

function jeajaxeventcalendarGeteventiduser($event_name)
{
    $user = clone(JFactory::getuser());
    $user_id= $user->id;
    $db = JFactory::getDBO();
    $sql = "SELECT `id` FROM #__jeajx_event WHERE `title`='".$event_name."' AND insert_user=".$user_id;
    $db->setQuery($sql);
    $event_id = $db->loadResult();
    return $event_id;
}

function jeajaxeventcalendarGetcategory($cat_id)
{
    $user = clone(JFactory::getuser());
    $user_id= $user->id;
    $db = JFactory::getDBO();
    $sql = "SELECT `ename` FROM #__jeajx_cal_category WHERE `id`=".$cat_id;
    $db->setQuery($sql);
    $cat_name = $db->loadResult();
    return $cat_name;
}

function jeajaxeventcalendarGetcategoryid($cat_name)
{
    $user = clone(JFactory::getuser());
    $user_id= $user->id;
    $db = JFactory::getDBO();
    $sql = "SELECT `id` FROM #__jeajx_cal_category WHERE `ename`='".$cat_name."'";
    $db->setQuery($sql);
    $cat_id = $db->loadResult();
    return $cat_id;
}
