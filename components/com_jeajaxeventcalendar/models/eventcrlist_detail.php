<?php
/**
* @package   JE Ajax Event Calendar
* @copyright Copyright (C) 2009 - 2010 Open Source Matters. All rights reserved.
* @license   http://www.gnu.org/licenses/lgpl.html GNU/LGPL, see LICENSE.php
* Contact to : emailtohardik@gmail.com, joomextensions@gmail.com
* Visit : http://www.joomlaextensions.co.in/
**/

defined( '_JEXEC' ) or die( 'Restricted access' );
jimport('joomla.application.component.model');

class eventcrlist_detailModeleventcrlist_detail extends JModelLegacy
{
	var $_id = null;
	var $_data = null;
	var $_region = null;
	var $_table_prefix = null;
	var $_copydata	=	null;

	function __construct()
	{
		parent::__construct();
		$this->_table_prefix = '#__jeajx_';
		$array = JRequest::getVar('cid',  0, '', 'array');
		$this->setId((int)$array[0]);
	}

	function setId($id)
	{
		$this->_id		= $id;
		$this->_data	= null;
	}


	function &getData()
	{
		if ($this->_loadData())
		{
		} else  $this->_initData();
		
		return $this->_data;
	}

	function _loadData()
	{
		if (empty($this->_data))
		{
			$query = 'SELECT * FROM '.$this->_table_prefix.'event WHERE id = '. $this->_id;
			$this->_db->setQuery($query);
			$this->_data = $this->_db->loadObject();
			return (boolean) $this->_data;
		}
		return true;
	}

	function _initData()
	{
		if (empty($this->_data))
		{
			$detail = new stdClass();
			$detail->id			= 0;
			$detail->title		= null;
			$detail->category	= null;
			$detail->usr		= null;
			$detail->desc		= null;
			$detail->start_date	= null;
			$detail->end_date	= null;
			$detail->published	= 1;
			$detail->bgcolor	= null;
			$detail->txtcolor	= null;
			$detail->street		= null;
			$detail->country	= null;
			$detail->city		= null;
			$detail->postcode	= null;
			$detail->insert_user= 0;
			$detail->youtubelink= null;
			$detail->googlelink	= null;
			$detail->erepeat	= 0;
			$this->_data		 	= $detail;
			return (boolean) $this->_data;
		}
		return true;
	}

	function store($data)
	{ 
		// ======= This table is called from the backend side =============
		$row =&$this->getTable('event');
		// ================================================================
		if (!$row->bind($data)) {
			$this->setError($this->_db->getErrorMsg());
			return false;
		}
		if (!$row->store()) {
			$this->setError($this->_db->getErrorMsg());
			return false;
		}
		return $row->id;
	}

	function getcategory() {
		$db= & JFactory :: getDBO();
		$q = "SELECT id AS value,ename AS text FROM ".$this->_table_prefix."cal_category WHERE published=1 AND pcat_id=0";
		$db->setQuery($q); 
		$parents=$db->loadObjectList();
		if(count($parents)!=0) {	
			for($i=0;$i<count($parents);$i++){
				$this->_cat_list[]= $parents[$i];
				$this->get_child($parents[$i]->value,0);
			}
			return $this->_cat_list;
		} else {
			return $parents;
		}
		
			
	}
	
	function get_child($id="",$count){
		$count++;
		$db= & JFactory :: getDBO();
		
		$addquery	= "";
		$q = "SELECT id AS value,ename AS text FROM ".$this->_table_prefix."cal_category WHERE published=1 AND pcat_id=".$id.$addquery;
		$db->setQuery($q);
		$child=$db->loadObjectList();
			
		for($i=0;$i<count($child);$i++){
			$des ='';
			for($k=0;$k<$count;$k++) {
				$des.=' - ';	
			}
			$child[$i]->text = $des.$child[$i]->text;
			$this->_cat_list[]= $child[$i];
			$this->get_child($child[$i]->value,$count);
		}
		
	}

	// ==================== code for display the dynemic field in the default page =====================

	function getfields()
	{
		$db= & JFactory :: getDBO();
		$sql="SELECT * FROM ".$this->_table_prefix."fields WHERE published=1 AND field_section=2";
		$db->setQuery($sql);
		return $db->loadObjectlist();
	}
	// =================================================================================================
	function getuser()
	{
		$db= & JFactory :: getDBO();
		$sql="SELECT id AS value,username AS text FROM #__users";
		$db->setQuery($sql);
		return $db->loadObjectlist();
	}
	
	function getconfigration()
	{
		$db= & JFactory :: getDBO();
		$query = "SELECT * FROM ".$this->_table_prefix."event_configration WHERE id=1";
		$db->setQuery($query);
		return $db->loadObject();
	}
	
	function geteventphoto()
	{
		$db= & JFactory :: getDBO();
		$query = 'SELECT * FROM '.$this->_table_prefix.'event_photo WHERE event_id = '.$this->_id;
		$db->setQuery($query);
		return $db->LoadObjectList();
	}
// ========================================= Fun to insert DateEvent Table ==================================================== //	
	function deletedateevent($event_id)
	{
	    $db= & JFactory :: getDBO();
		$query = "DELETE FROM ".$this->_table_prefix."dateevent WHERE event_id=".$event_id;
		$db->setQuery($query);
		$db->query();
		return true;
	}
	
	function insertdateevent($event_id,$dailysdate,$dailyedate)
	{
	    $db= & JFactory :: getDBO();
		$query = "INSERT INTO ".$this->_table_prefix."dateevent (id,event_id,dailysdate,dailyedate) VALUES ('',".$event_id.",'".$dailysdate."','".$dailyedate."')";
		$db->setQuery($query);
		$db->query();
		return true;
	}
	
	function getdateevent($event_id)
	{
		$db= & JFactory :: getDBO();
		$query = 'SELECT * FROM '.$this->_table_prefix.'dateevent WHERE event_id = '.$event_id;
		$db->setQuery($query);
		return $db->loadObjectList();
	}
// ===================================== EOF Fun to insert DateEvent Table ==================================================== //	
}
?>
