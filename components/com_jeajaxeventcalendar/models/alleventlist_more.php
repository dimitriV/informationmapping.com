<?php
/**
* @package   JE Ajax Event Calendar
* @copyright Copyright (C) 2009 - 2010 Open Source Matters. All rights reserved.
* @license   http://www.gnu.org/licenses/lgpl.html GNU/LGPL, see LICENSE.php
* Contact to : emailtohardik@gmail.com, joomextensions@gmail.com
* Visit : http://www.joomlaextensions.co.in/
**/

defined( '_JEXEC' ) or die( 'Restricted access' );
jimport('joomla.application.component.model');

class alleventlist_moreModelalleventlist_more extends JModelLegacy
{
	var $_data = null;
	var $_total = null;
	var $_pagination = null;
	var $_table_prefix = null;
	
	function __construct()
	{
		parent::__construct();
		global $context;
		$mainframe = JFactory::getApplication();
		$context='id';		
		$this->_table_prefix = '#__jeajx_';
	}
	
	function getData()
	{	
		if (empty($this->_data))
		{
			$query = $this->_buildQuery();
			$this->_data = $this->_getList($query);
		}
		return $this->_data;
	}

	function getTotal()
	{
		if (empty($this->_total))
		{
			$event_id = JRequest::getVar( 'event_id','','','int');
			$query = 'SELECT * FROM '.$this->_table_prefix.'event WHERE published=1 AND id='.$event_id.' ORDER BY start_date ASC ';
			//$query = 'SELECT * FROM '.$this->_table_prefix.'event WHERE published=1 AND id='.$event_id;
			$this->_total = $this->_getListCount($query);
		}
		return $this->_total;
	}

	function _buildQuery()
	{
		$event_id = JRequest::getVar( 'event_id','','','int');
		$start = ( JRequest::getVar( 'limitstart', 0, '', 'int' ) );
		$limit = intval(5);
		$query = 'SELECT * FROM '.$this->_table_prefix.'event WHERE published=1 AND id='.$event_id.' LIMIT '.$start.', '.$limit;
		return $query;
	}

	function getPagination()
	{
		if (empty($this->_pagination))
		{
			jimport('joomla.html.pagination');
			$this->_pagination = new JPagination( $this->getTotal(), JRequest::getVar( 'limitstart', 0 ), 5 );
		}
		return $this->_pagination;
	}

	function gettempsetting()
	{
		$db= & JFactory :: getDBO();
		$sql="SELECT * FROM ".$this->_table_prefix."tempsetting WHERE id=1";
		$db->setQuery($sql);
		return $db->loadObject();
	}

	// =============== Gettnig Fields name =====================================================================//
	function getfields($field_section)																				
	{																												
		$db= & JFactory :: getDBO();																				
		$sql='SELECT * FROM '.$this->_table_prefix.'fields WHERE field_section='.$field_section;
		$db->setQuery($sql);																						
		return $db->loadObjectList();																				
	}																												
	// =============== EOF Gettnig Fields name =================================================================//

	// ============================= Getting Dynemic field Value =======================================//
	function fieldsvalue($field_id,$hid)																		
	{																									
		$db= & JFactory :: getDBO();																	
		$sql="SELECT * FROM ".$this->_table_prefix."fields_data where fieldid='".$field_id."' and hid='".$hid."' and section=2";	
		$db->setQuery($sql);																			
		return $db->loadObject();																	
	}																									
	// ============================= EOF Getting Dynemic field Value ===================================//
	function getcountry($country_id) {
		$db= & JFactory :: getDBO();
		$sql = "SELECT country_name FROM ".$this->_table_prefix."country  WHERE country_id=".$country_id;
		$db->setQuery($sql);
		return $db->loadObject();
	}

	function geteventphoto($event_id)																			
	{	
		//$event_id = JRequest::getVar( 'event_id','','','int');																											
		$db= & JFactory :: getDBO();																				
		$sql='SELECT * FROM '.$this->_table_prefix.'event_photo WHERE event_id='.$event_id;
		$db->setQuery($sql);																						
		return $db->loadObjectList();																					
	}

	function eventmainphoto($event_id)																			
	{	
		//$event_id = JRequest::getVar( 'event_id','','','int');																											
		$db= & JFactory :: getDBO();																				
		$sql='SELECT * FROM '.$this->_table_prefix.'event_photo WHERE main_image=1 AND event_id='.$event_id;
		$db->setQuery($sql);																						
		return $db->loadObject();																					
	}
	
	function relatedevents($category)																			
	{	
		$db= & JFactory :: getDBO();																				
		$event_id = JRequest::getVar( 'event_id','','','int');
		$sql='SELECT id,title FROM '.$this->_table_prefix.'event WHERE published=1 AND category='.$category.' AND id!='.$event_id.' ORDER BY id DESC LIMIT 0,16';
		$db->setQuery($sql);																						
		return $db->loadObjectlist();																					
	}
	
	// ============================= Event hits code ============================ //
	function eventhits($event_id)																			
	{	
		$db= & JFactory :: getDBO();																				
		$sql='SELECT max(ehits) AS event_hits FROM '.$this->_table_prefix.'event WHERE published=1 AND id='.$event_id;
		$db->setQuery($sql);																						
		$eventhits	= $db->loadObject();
		
		$total_eventhits	= $eventhits->event_hits+1;
		$sqlins	= 'UPDATE '.$this->_table_prefix.'event SET ehits='.$total_eventhits.' WHERE published=1 AND id='.$event_id;
		$db->setQuery($sqlins);																						
		$db->query();
		return true;
	}		
	// ============================= Event hits code ============================ //
} 
?>