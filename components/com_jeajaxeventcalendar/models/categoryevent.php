<?php
 /**
* @package   JE Ajax Event Calendar
* @copyright Copyright (C) 2009 - 2010 Open Source Matters. All rights reserved.
* @license   http://www.gnu.org/licenses/lgpl.html GNU/LGPL, see LICENSE.php
* Contact to : emailtohardik@gmail.com, joomextensions@gmail.com
* Visit : http://www.joomlaextensions.co.in/
**/ 


defined( '_JEXEC' ) or die( 'Restricted access' );

jimport('joomla.application.component.model');

class categoryeventModelcategoryevent extends JModelLegacy
{
	var $_data = null;
	var $_total = null;
	var $_pagination = null;
	var $_table_prefix = null;

	function __construct()
	{
			
		parent::__construct();
		global $context;
		$mainframe = JFactory::getApplication();
		$context='id';		
	  	$this->_table_prefix = '#__jeajx_';
	}
	
	function getData()
	{	
		if (empty($this->_data))
		{
			$query = $this->_buildQuery();
			$this->_data = $this->_getList($query);
		}
		return $this->_data;
	}
	
	function getTotal()
	{
		if (empty($this->_total))
		{
			
			global $context;
			$mainframe = JFactory::getApplication();
			
			$search = JRequest::getVar('search','','request','int');
			
			$redconfig 	= &$mainframe->getParams();
			$category_id=$redconfig->get('cat_id');
			
			if($category_id!='') {
				if($category_id!=0) {
					$child_cats	= $this->childcategory($category_id);
					if(count($child_cats)!=0) {
						$mycategories	= implode(',',$child_cats);
						$mycategories	.= ','.$category_id;
					} else
						$mycategories	= $category_id;
						
					$addquery = ' WHERE published=1 AND id IN ('.$mycategories.')';
				} else {
					$addquery = ' WHERE AND published=1';
				}
				$query = 'SELECT * FROM '.$this->_table_prefix.'cal_category '.$addquery;
			} else {
				$query = 'SELECT * FROM '.$this->_table_prefix.'cal_category WHERE published=1';
			}
				
			$this->_total = $this->_getListCount($query);
		}
		return $this->_total;
	}
	
	function _buildQuery()
	{	
		global $context;
		$mainframe = JFactory::getApplication();
		
		$search = JRequest::getVar('search','','','int');
		
		$start = ( JRequest::getVar( 'limitstart', 0, '', 'int' ) );
		$limit = intval(10);
		
		$redconfig 	= &$mainframe->getParams();
		$category_id=$redconfig->get('cat_id');
		
		if($category_id!='') {
			if($category_id!=0) {
				
				$child_cats	= $this->childcategory($category_id);
				
				if(count($child_cats)!=0) {
					$mycategories	= implode(',',$child_cats);
					$mycategories	.= ','.$category_id;
				} else
					$mycategories	= $category_id;
					
				$addquery = ' WHERE published=1 AND id IN ('.$mycategories.')';
			} else {
				$addquery = ' WHERE published=1';
			}
			$query = 'SELECT * FROM '.$this->_table_prefix.'cal_category '.$addquery.' LIMIT '.$start.', '.$limit;
		} else {
			$query = 'SELECT * FROM '.$this->_table_prefix.'cal_category WHERE published=1 LIMIT '.$start.', '.$limit;
		}
	
		return $query;
	}
	
	function getPagination()
	{
		if (empty($this->_pagination))
		{
			jimport('joomla.html.pagination');
			$this->_pagination = new JPagination( $this->getTotal(), JRequest::getVar( 'limitstart', 0 ), 10 );
		}
		return $this->_pagination;
	}
	
	function gettempsetting()
	{
		$db= & JFactory :: getDBO();
		$sql="SELECT * FROM ".$this->_table_prefix."tempsetting WHERE id=1";
		$db->setQuery($sql);
		return $db->loadObject();
	}
	
	// =============== Gettnig Fields name ========================================================================//	
	function getfields($field_section)																				
	{																												
		$db= & JFactory :: getDBO();																				
		$sql='SELECT * FROM '.$this->_table_prefix.'fields WHERE field_section='.$field_section;
		$db->setQuery($sql);																						
		return $db->loadObjectList();																					
	}																												
	// =============== EOF Gettnig Fields name ====================================================================//	
	
	// ============================= Getting Dynemic field Value =======================================//
	function fieldsvalue($field_id,$hid)																		
	{																									
		$db= & JFactory :: getDBO();																	
		$sql="SELECT * FROM ".$this->_table_prefix."fields_data where fieldid='".$field_id."' and hid='".$hid."' and section=2";	
		$db->setQuery($sql);																			
		return $db->loadObject();																	
	}																									
	// ============================= EOF Getting Dynemic field Value ===================================//
	function getcountry($country_id) {
		$db= & JFactory :: getDBO();
		$sql = "SELECT country_name FROM ".$this->_table_prefix."country  WHERE country_id=".$country_id;
		$db->setQuery($sql);
		return $db->loadObject();
	}
			
	function eventmainphoto($event_id)																			
	{	
		$db= & JFactory :: getDBO();																				
		$sql='SELECT * FROM '.$this->_table_prefix.'event_photo WHERE main_image=1 AND event_id='.$event_id;
		$db->setQuery($sql);																						
		return $db->loadObject();																					
	}
	
	function getevents($cat_id)																			
	{	
		$db= & JFactory :: getDBO();																				
		$sql='SELECT * FROM '.$this->_table_prefix.'event WHERE category='.$cat_id;
		$db->setQuery($sql);																						
		return $db->loadObjectlist();
	}
	
	// ============================================================= Get child Categories ================================================================== //
	function childcategory($cat_id)																			
	{	
		$db= & JFactory :: getDBO();																				
		$sql='SELECT * FROM '.$this->_table_prefix.'cal_category WHERE pcat_id='.$cat_id;
		$db->setQuery($sql);																						
		return $db->loadResultArray();
	}
	// ============================================================= Get child Categories ================================================================== //

}