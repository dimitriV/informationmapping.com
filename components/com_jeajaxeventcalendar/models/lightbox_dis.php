<?php
/**
* @package   JE Ajax Event Calendar
* @copyright Copyright (C) 2009 - 2010 Open Source Matters. All rights reserved.
* @license   http://www.gnu.org/licenses/lgpl.html GNU/LGPL, see LICENSE.php
* Contact to : emailtohardik@gmail.com, joomextensions@gmail.com
* Visit : http://www.joomlaextensions.co.in/
**/

defined ('_JEXEC') or die ('Restricted access');
jimport('joomla.application.component.model');

class lightbox_disModellightbox_dis extends JModelLegacy
{
	var $_data = null;
	var $_total = null;
	var $_pagination = null;
	var $_table_prefix = null;

	function __construct()
	{
		parent::__construct();
		global $mainframe, $context; 
		$context='itemid';
		$this->_table_prefix = '#__jeajx_';
	}

	function itemdata($itemid)
	{
		$db= & JFactory :: getDBO();																
		$sql='SELECT * FROM '.$this->_table_prefix.'event WHERE id='.$itemid;			
		$db->setQuery($sql);																			
		$bhavu = $db->loadObjectList();																	
		return $bhavu;
	}
}	
?>