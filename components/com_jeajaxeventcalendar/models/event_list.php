<?php
/**
* @package   JE Ajax Event Calendar
* @copyright Copyright (C) 2009 - 2010 Open Source Matters. All rights reserved.
* @license   http://www.gnu.org/licenses/lgpl.html GNU/LGPL, see LICENSE.php
* Contact to : emailtohardik@gmail.com, joomextensions@gmail.com
* Visit : http://www.joomlaextensions.co.in/
**/

defined( '_JEXEC' ) or die( 'Restricted access' );
jimport('joomla.application.component.model');

class event_listModelevent_list extends JModelLegacy
{
	var $_data = null;
	var $_total = null;
	var $_pagination = null;
	var $_table_prefix = null;
	
	function __construct()
	{
		parent::__construct();
		global $context;
		$mainframe = JFactory::getApplication();
		$context='id';		
		$this->_table_prefix = '#__jeajx_';
		$limit		= $mainframe->getUserStateFromRequest( $context.'limit', 'limit', $mainframe->getCfg('list_limit'), 0);
		$limitstart = $mainframe->getUserStateFromRequest( $context.'limitstart', 'limitstart', 0 );
		$this->setState('limit', $limit);
		$this->setState('limitstart', $limitstart);
	}
	
	function getData()
	{	
		if (empty($this->_data))
		{
			$query = $this->_buildQuery();
			$this->_data = $this->_getList($query);
		}
		return $this->_data;
	}
	
	function getTotal()
	{
		if (empty($this->_total))
		{
			global $context;
			$mainframe = JFactory::getApplication();
			
			$date_event_id1	= $mainframe->getUserStateFromRequest( $context.'date_event_id', 'date_event_id','0');
			$myevent_dates	= $mainframe->getUserStateFromRequest( $context.'myevent_dates', 'myevent_dates','0');
			$myevent_ids1	= substr($date_event_id1, 0, strlen($date_event_id1)-1);
			
			$query = 'SELECT * FROM '.$this->_table_prefix.'event WHERE published=1 AND id IN ('.$myevent_ids1.') ORDER BY id ASC';
			$this->_total = $this->_getListCount($query);
		}
		return $this->_total;
	}

	function _buildQuery()
	{
		global $context;
		$mainframe = JFactory::getApplication();
		
		$user = clone(JFactory::getUser());
		$insert_user = $user->id;
		$start = (JRequest::getVar('limitstart', 0, '', 'int' ));
		$limit = intval(10);
		
		$date_event_id1	= $mainframe->getUserStateFromRequest( $context.'date_event_id', 'date_event_id','0');
		$myevent_dates	= $mainframe->getUserStateFromRequest( $context.'myevent_dates', 'myevent_dates','0');
		$myevent_ids1	= substr($date_event_id1, 0, strlen($date_event_id1)-1);
		
		$query = 'SELECT * FROM '.$this->_table_prefix.'event WHERE published=1 AND id IN ('.$myevent_ids1.') ORDER BY id ASC LIMIT '.$start.', '.$limit;
		return $query;
	}

	function getPagination()
	{
		if (empty($this->_pagination))
		{
			jimport('joomla.html.pagination');
			$this->_pagination = new JPagination( $this->getTotal(), JRequest::getVar( 'limitstart', 0 ), 10 );
		}
		return $this->_pagination;
	}
	
	function gettempsetting()
	{
		$db= & JFactory :: getDBO();
		$sql="SELECT dateevent_tempt FROM ".$this->_table_prefix."tempsetting WHERE id=1";
		$db->setQuery($sql);
		return $db->loadObject();
	}
	
	function eventmainphoto($event_id)																			
	{	
		$db= & JFactory :: getDBO();																				
		$sql='SELECT * FROM '.$this->_table_prefix.'event_photo WHERE main_image=1 AND event_id='.$event_id;
		$db->setQuery($sql);																						
		return $db->loadObject();																					
	}
	
	function eventcategory($category)																			
	{	
		$db= & JFactory :: getDBO();																				
		$sql='SELECT * FROM '.$this->_table_prefix.'cal_category WHERE id='.$category;
		$db->setQuery($sql);																						
		return $db->loadObject();																					
	}
} 
?>