<?php
 /**
* @package   JE Ajax Event Calendar
* @copyright Copyright (C) 2009 - 2010 Open Source Matters. All rights reserved.
* @license   http://www.gnu.org/licenses/lgpl.html GNU/LGPL, see LICENSE.php
* Contact to : emailtohardik@gmail.com, joomextensions@gmail.com
* Visit : http://www.joomlaextensions.co.in/
**/ 

defined ('_JEXEC') or die ('Restricted access');
jimport('joomla.application.component.model');

class eventcrlistModeleventcrlist extends JModelLegacy
{
	var $_data = null;
	var $_total = null;
	var $_pagination = null;
	var $_table_prefix = null;
	
	function __construct()
	{
		parent::__construct();
		global $context; 
		$mainframe = JFactory::getApplication();
		$context='id';
		$this->_table_prefix = '#__jeajx_';
	}
	
	function getData()
	{		
		if (empty($this->_data))
		{
			$query = $this->_buildQuery();
			$this->_data = $this->_getList($query);
		}
		return $this->_data;
	}

	function _buildQuery()
	{
		$user = clone(JFactory::getUser());
		$userid = $user->id;
		$start = ( JRequest::getVar( 'limitstart', 0, '', 'int' ) );
		$limit = intval(10);
		
		$query = "SELECT e.*,c.ename AS catname FROM ".$this->_table_prefix."event AS e LEFT JOIN ".$this->_table_prefix."cal_category AS c ON e.category=c.id WHERE e.insert_user=".$userid." OR e.usr=".$userid." ORDER BY e.id ASC LIMIT $start, " .$limit;
		return $query;
	}

	function getTotal()
	{
		if (empty($this->_total))
		{
			$user = clone(JFactory::getUser());
			$userid = $user->id;
			
			$query = "SELECT e.*,c.ename AS catname FROM ".$this->_table_prefix."event AS e LEFT JOIN ".$this->_table_prefix."cal_category AS c ON e.category=c.id WHERE e.insert_user=".$userid." OR e.usr=".$userid." ORDER BY e.id ASC ";
			$this->_total = $this->_getListCount($query);
		}
		return $this->_total;
	}

	function getPagination()
	{
		if (empty($this->_pagination))
		{
			jimport('joomla.html.pagination');
			$this->_pagination = new JPagination( $this->getTotal(), JRequest::getVar( 'limitstart', 0 ), 10 );
		}
		return $this->_pagination;
	}
	
	function gettempsetting()
	{
		$db= & JFactory :: getDBO();
		$sql="SELECT * FROM ".$this->_table_prefix."tempsetting WHERE id=1";
		$db->setQuery($sql);
		return $db->loadObject();
	}
	
	function eventmainphoto($event_id)																			
	{	
		$db= & JFactory :: getDBO();																				
		$sql='SELECT * FROM '.$this->_table_prefix.'event_photo WHERE main_image=1 AND event_id='.$event_id;
		$db->setQuery($sql);																						
		return $db->loadObject();																					
	}

}	







