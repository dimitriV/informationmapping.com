<?php
 /**
* @package   JE Ajax Event Calendar
* @copyright Copyright (C) 2009 - 2010 Open Source Matters. All rights reserved.
* @license   http://www.gnu.org/licenses/lgpl.html GNU/LGPL, see LICENSE.php
* Contact to : emailtohardik@gmail.com, joomextensions@gmail.com
* Visit : http://www.joomlaextensions.co.in/
**/ 

defined( '_JEXEC' ) or die( 'Restricted access' );
jimport('joomla.application.component.model');

class alleventlistModelalleventlist extends JModelLegacy
{
	var $_data = null;
	var $_total = null;
	var $_pagination = null;
	var $_table_prefix = null;

	function __construct()
	{
		parent::__construct();
		global $context;
		$mainframe = JFactory::getApplication();
		$context='id';		
		$this->_table_prefix = '#__jeajx_';
		
		$limit = $mainframe->getUserStateFromRequest( $context.'limit', 'limit', $mainframe->getCfg('list_limit'), 0);
		$limit = 10;
		$limitstart = $mainframe->getUserStateFromRequest( $context.'limitstart', 'limitstart', 0 );
		$this->setState('limit', $limit);
		$this->setState('limitstart', $limitstart);
	
	}

	function getData()
	{	
		if (empty($this->_data))
		{
			$query = $this->_buildQuery();
			$this->_data = $this->_getList($query);
		}
		return $this->_data;
	}

	function getTotal()
	{
		if (empty($this->_total))
		{
			
			global $context;
			$mainframe = JFactory::getApplication();
			$search = JRequest::getVar('search','','request','int');
			
			$redconfig 		= &$mainframe->getParams();
			$category_id	= $redconfig->get('cat_id');
		// ========================== Code For Filter Search ============================ //	
			$eventsort_data	= JRequest::getInt('mysortdata','1');
			$event_orderby	= JRequest::getWord('eorder','a');
			if($eventsort_data==1) {
				if($event_orderby=='a')
					$myorderby	= 'start_date ASC';
				else
					$myorderby	= 'start_date DESC';
			}
			
			if($eventsort_data==2) {
				if($event_orderby=='a')
					$myorderby	= 'title ASC';
				else
					$myorderby	= 'title DESC';
			}	
		// ========================= EOF Code For Filter Search ========================= //
			if($search==1)  {
				$ename	= $mainframe->getUserStateFromRequest( $context.'ename', 'ename','0');
				$start_date	= $mainframe->getUserStateFromRequest( $context.'start_date', 'start_date','0');
				$end_date	= $mainframe->getUserStateFromRequest( $context.'end_date', 'end_date','0');
				$category	= $mainframe->getUserStateFromRequest( $context.'category', 'category',  '0');
				
				$search_childcats	= $this->childcategory($category);
			
				if(count($search_childcats)!=0) {
					$mycategories	= implode(',',$search_childcats);
					$mycategories	.= ','.$category;
				} else
					$mycategories	= $category;
				
				
				if($ename!='Enter event name' && $start_date!='' && $end_date!='') {
					$addquery = ' WHERE published=1 AND category IN ('.$mycategories.') AND title LIKE "%'.$ename.'%" AND start_date>="'.$start_date.'" AND end_date<="'.$end_date.'" ';
				}
				else if($ename!='Enter event name' && $start_date!='' && $end_date=='') {
					$addquery = ' WHERE published=1 AND category IN ('.$mycategories.') AND title LIKE "%'.$ename.'%" AND start_date="'.$start_date.'" ';
				}
				else if($ename!='Enter event name' && $start_date=='' && $end_date!='') {
					$addquery = ' WHERE published=1 AND category IN ('.$mycategories.') AND title LIKE "%'.$ename.'%" AND end_date="'.$end_date.'" ';
				}
				else if($ename=='Enter event name' && $start_date!='' && $end_date!='') {
					$addquery = ' WHERE published=1 AND category IN ('.$mycategories.') AND start_date>="'.$start_date.'" AND end_date<="'.$end_date.'" ';
				}
				else if($ename!='Enter event name' && $start_date=='' && $end_date=='') {
					$addquery = ' WHERE published=1 AND category IN ('.$mycategories.') AND title LIKE "%'.$ename.'%" ';
				}
				else if($ename=='Enter event name' && $start_date!='' && $end_date=='') {
					$addquery = ' WHERE published=1 AND category IN ('.$mycategories.') AND start_date="'.$start_date.'" ';
				}
				else if($ename=='Enter event name' && $start_date=='' && $end_date!='') {
					$addquery = ' WHERE published=1 AND category IN ('.$mycategories.') AND end_date="'.$end_date.'" ';
				}
				else {
					$addquery = ' WHERE published=1 AND category IN ('.$mycategories.')';
				}
			
				$query = 'SELECT * FROM '.$this->_table_prefix.'event '.$addquery.' ORDER BY '.$myorderby;
			
			} else if($category_id!='') {
				if($category_id!=0) {
					// ======================== All category of parents and also parent category ========================== //
					$child_cats	= $this->childcategory($category_id);
					if(count($child_cats)!=0) {
						$mycategories	= implode(',',$child_cats);
						$mycategories	.= ','.$category_id;
					} else {
						$mycategories	= $category_id;
					}
					// ====================== EOF All category of parents and also parent category ======================== //
					$addquery = ' WHERE published=1 AND category IN ('.$mycategories.')';
				}
				else
					$addquery = ' WHERE published=1';
				
				$query = 'SELECT * FROM '.$this->_table_prefix.'event '.$addquery.' AND start_date > NOW() ORDER BY '.$myorderby;
				
			} else {
				$query = 'SELECT * FROM '.$this->_table_prefix.'event WHERE start_date > NOW() AND published=1 ORDER BY '.$myorderby;
			}
			
			$this->_total = $this->_getListCount($query);
		}
		return $this->_total;
	}
	
	function _buildQuery()
	{	
		global $context;
		$mainframe = JFactory::getApplication();
		$search = JRequest::getVar('search','','','int');
		
		$start = ( JRequest::getVar( 'limitstart', 0, '', 'int' ) );
		$limit = intval(10);
		
		$redconfig 	= &$mainframe->getParams();
		$category_id=$redconfig->get('cat_id');
	
	
	// ========================== Code For Filter Search ============================ //		
		$eventsort_data	= JRequest::getInt('mysortdata','1');
		$event_orderby	= JRequest::getWord('eorder','a');
		if($eventsort_data==1) {
			if($event_orderby=='a')
				$myorderby	= 'start_date ASC';
			else
				$myorderby	= 'start_date DESC';
		}
		
		if($eventsort_data==2) {
			if($event_orderby=='a')
				$myorderby	= 'title ASC';
			else
				$myorderby	= 'title DESC';
		}
	// ========================= EOF Code For Filter Search ========================= //	
		if($search==1)  {
			$ename	= $mainframe->getUserStateFromRequest( $context.'ename', 'ename','0');
			$start_date	= $mainframe->getUserStateFromRequest( $context.'start_date', 'start_date','0');
			$end_date	= $mainframe->getUserStateFromRequest( $context.'end_date', 'end_date','0');
			$category	= $mainframe->getUserStateFromRequest( $context.'category', 'category',  '0');
			
			$search_childcats	= $this->childcategory($category);
			
			if(count($search_childcats)!=0) {
				$mycategories	= implode(',',$search_childcats);
				$mycategories	.= ','.$category;
			} else
				$mycategories	= $category;
			
			if($ename!='Enter event name' && $start_date!='' && $end_date!='') {
				$addquery = ' WHERE published=1 AND category IN ('.$mycategories.') AND title LIKE "%'.$ename.'%" AND start_date>="'.$start_date.'" AND end_date<="'.$end_date.'" ';
			}
			else if($ename!='Enter event name' && $start_date!='' && $end_date=='') {
				$addquery = ' WHERE published=1 AND category IN ('.$mycategories.') AND title LIKE "%'.$ename.'%" AND start_date="'.$start_date.'" ';
			}
			else if($ename!='Enter event name' && $start_date=='' && $end_date!='') {
				$addquery = ' WHERE published=1 AND category IN ('.$mycategories.') AND title LIKE "%'.$ename.'%" AND end_date="'.$end_date.'" ';
			}
			else if($ename=='Enter event name' && $start_date!='' && $end_date!='') {
				$addquery = ' WHERE published=1 AND category IN ('.$mycategories.') AND start_date>="'.$start_date.'" AND end_date<="'.$end_date.'" ';
			}
			else if($ename!='Enter event name' && $start_date=='' && $end_date=='') {
				$addquery = ' WHERE published=1 AND category IN ('.$mycategories.') AND title LIKE "%'.$ename.'%" ';
			}
			else if($ename=='Enter event name' && $start_date!='' && $end_date=='') {
				$addquery = ' WHERE published=1 AND category IN ('.$mycategories.') AND start_date="'.$start_date.'" ';
			}
			else if($ename=='Enter event name' && $start_date=='' && $end_date!='') {
				$addquery = ' WHERE published=1 AND category IN ('.$mycategories.') AND end_date="'.$end_date.'" ';
			}
			else {
				$addquery = ' WHERE published=1 AND category IN ('.$mycategories.')';
			}
			
			$query = 'SELECT * FROM '.$this->_table_prefix.'event '.$addquery.' ORDER BY '.$myorderby.' LIMIT '.$start.', '.$limit;
			
		} else if($category_id!='') {
			if($category_id!=0) {
				// ======================== All category of parents and also parent category ========================== //
				$child_cats	= $this->childcategory($category_id);
				if(count($child_cats)!=0) {
					$mycategories	= implode(',',$child_cats);
					$mycategories	.= ','.$category_id;
				} else
					$mycategories	= $category_id;
				// ====================== EOF All category of parents and also parent category ======================== //
				$addquery = ' WHERE published=1 AND start_date > NOW() AND category IN ('.$mycategories.')';
			}
			else
				$addquery = ' WHERE published=1 AND start_date > NOW()';
				
			$query = 'SELECT * FROM '.$this->_table_prefix.'event '.$addquery.' ORDER BY '.$myorderby.' LIMIT '.$start.', '.$limit;
				
		} else {
			$query = 'SELECT * FROM '.$this->_table_prefix.'event WHERE published=1 AND start_date > NOW() AND category !="10" ORDER BY country, '.$myorderby.' LIMIT '.$start.', '.$limit;
		}
	
		return $query;
	}

	function getPagination()
	{
		if (empty($this->_pagination))
		{
			jimport('joomla.html.pagination');
			$this->_pagination = new JPagination( $this->getTotal(), JRequest::getVar( 'limitstart', 0 ), 10 );
		}
		return $this->_pagination;
	}

	function gettempsetting()
	{
		$db= & JFactory :: getDBO();
		$sql="SELECT * FROM ".$this->_table_prefix."tempsetting WHERE id=1";
		$db->setQuery($sql);
		return $db->loadObject();
	}
	
	// =============== Gettnig Fields name =====================================================================//
	function getfields($field_section)																				
	{																												
		$db= & JFactory :: getDBO();																				
		$sql='SELECT * FROM '.$this->_table_prefix.'fields WHERE field_section='.$field_section;
		$db->setQuery($sql);																						
		return $db->loadObjectList();																				
	}																												
	// =============== EOF Gettnig Fields name =================================================================//

	// ============================= Getting Dynemic field Value =======================================//
	function fieldsvalue($field_id,$hid)																		
	{																									
		$db= & JFactory :: getDBO();																	
		$sql="SELECT * FROM ".$this->_table_prefix."fields_data where fieldid='".$field_id."' and hid='".$hid."' and section=2";	
		$db->setQuery($sql);																			
		return $db->loadObject();																	
	}																									
	// ============================= EOF Getting Dynemic field Value ===================================//
	function getcountry($country_id) {
		$db= & JFactory :: getDBO();
		$sql = "SELECT country_name FROM ".$this->_table_prefix."country  WHERE country_id=".$country_id;
		$db->setQuery($sql);
		return $db->loadObject();
	}
	
	function eventmainphoto($event_id)																			
	{	
		$db= & JFactory :: getDBO();																				
		$sql='SELECT * FROM '.$this->_table_prefix.'event_photo WHERE main_image=1 AND event_id='.$event_id;
		$db->setQuery($sql);																						
		return $db->loadObject();																					
	}
	
	function eventcategory($category)																	
	{	
		$db= & JFactory :: getDBO();																				
		$sql='SELECT * FROM '.$this->_table_prefix.'cal_category WHERE id='.$category;
		$db->setQuery($sql);																						
		return $db->loadObject();																					
	}
	
	// ============================================== Child Category ================================================= //
	function childcategory($cat_id)																			
	{	
		$db= & JFactory :: getDBO();																				
		$sql='SELECT * FROM '.$this->_table_prefix.'cal_category WHERE pcat_id='.$cat_id;
		$db->setQuery($sql);																						
		return $db->loadResultArray();
	}
	// ============================================== Child Category ================================================= //
} 
?>
