<?php
/**
* @package   JE Ajax Event Calendar
* @copyright Copyright (C) 2009 - 2010 Open Source Matters. All rights reserved.
* @license   http://www.gnu.org/licenses/lgpl.html GNU/LGPL, see LICENSE.php
* Contact to : emailtohardik@gmail.com, joomextensions@gmail.com
* Visit : http://www.joomlaextensions.co.in/
**/
defined( '_JEXEC' ) or die( 'Restricted access' );
jimport( 'joomla.application.component.model' );

class eventModelevent extends JModel
{
	var $_id = null;
	var $_data = null;
	var $_product = null; // product data
	var $_table_prefix = null;
	var $_template = null;
	
	function __construct()
	{
		global $context;
		$mainframe = JFactory::getApplication();
		$context = 'cal';
		$this->_table_prefix = '#__jeajx_';	
		parent::__construct();
	}

	function event_desc($id)
	{
		$db = JFactory::getDbo();
		$query = ' SELECT * FROM '.$this->_table_prefix.'event where published=1 and id='.$id;
		$db->setQuery($query);
		$res=$db->loadObject();
		return $res;
	}
	
	function getCategory()
	{
		$db = JFactory::getDbo();
		$query = ' SELECT id  as value, ename  as text FROM '.$this->_table_prefix.'cal_category where published=1';
		$db->setQuery($query);
		$res=$db->loadObjectList();
		return $res;
	}
	
	function geteventphoto($id="")
	{
		$db = JFactory::getDbo();
		$query = ' SELECT image FROM '.$this->_table_prefix.'event_photo where main_image=1 AND event_id ='.$id;
		$db->setQuery($query);
		$res=$db->loadObject();
		return $res->image;
	}
}