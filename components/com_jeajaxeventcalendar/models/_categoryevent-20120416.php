<?php
 /**
* @package   JE Ajax Event Calendar
* @copyright Copyright (C) 2009 - 2010 Open Source Matters. All rights reserved.
* @license   http://www.gnu.org/licenses/lgpl.html GNU/LGPL, see LICENSE.php
* Contact to : emailtohardik@gmail.com, joomextensions@gmail.com
* Visit : http://www.joomlaextensions.co.in/
**/ 


defined( '_JEXEC' ) or die( 'Restricted access' );

jimport('joomla.application.component.model');

class categoryeventModelcategoryevent extends JModel
{
	var $_data = null;
	var $_total = null;
	var $_pagination = null;
	var $_table_prefix = null;

	function __construct()
	{
			
		parent::__construct();
		global $context;
		$mainframe = JFactory::getApplication();
		$context='id';		
	  	$this->_table_prefix = '#__jeajx_';
	}
	
	function getData()
	{	
		if (empty($this->_data))
		{
			$query = $this->_buildQuery();
			$this->_data = $this->_getList($query);
		}
		return $this->_data;
	}
	
	function getTotal()
	{
		if (empty($this->_total))
		{
			global $context;
			$mainframe = JFactory::getApplication();
			
			$search = JRequest::getVar('search','','request','int');
			
			$redconfig 	= &$mainframe->getParams();
			$category_ids = $redconfig->get('cat_ids');
			
			if($category_ids != '' && $category_ids != 0) {
				$mycategories = $category_ids;
				$cat_ids_arr = explode(',', $category_ids);
				if(count($cat_ids_arr)){
					foreach($cat_ids_arr as $catid){
						$child_cats	= $this->childcategory($catid);
						$mycategories .= (count($child_cats)) ? ','.implode(',',$child_cats) : '';
					}					
				}
				$query = 'SELECT * FROM '.$this->_table_prefix.'cal_category WHERE published=1 AND id IN ('.$mycategories.')';
			}
			else {
				$query = 'SELECT * FROM '.$this->_table_prefix.'cal_category WHERE published=1';
			}
				
			$this->_total = $this->_getListCount($query);
		}
		return $this->_total;
	}
	
	function _buildQuery()
	{	
		global $context;
		$mainframe = JFactory::getApplication();
		
		$search = JRequest::getVar('search','','','int');
		
		$start = ( JRequest::getVar( 'limitstart', 0, '', 'int' ) );
		$limit = intval(10);
		
		$redconfig 	= &$mainframe->getParams();
		$category_ids = $redconfig->get('cat_ids');
		
		if($category_ids != '' && $category_ids != 0) {
			$mycategories = $category_ids;
			$cat_ids_arr = explode(',', $category_ids);
			if(count($cat_ids_arr)){
				foreach($cat_ids_arr as $catid){
					$child_cats	= $this->childcategory($catid);
					$mycategories .= (count($child_cats)) ? ','.implode(',',$child_cats) : '';
				}					
			}
			$query = 'SELECT * FROM '.$this->_table_prefix.'cal_category WHERE published=1 AND id IN ('.$mycategories.') ORDER BY ordering';
		}
		else {
			$query = 'SELECT * FROM '.$this->_table_prefix.'cal_category WHERE published=1 ORDER BY ordering';
		}
	
		return $query;
	}
	
	function getPagination()
	{
		if (empty($this->_pagination))
		{
			jimport('joomla.html.pagination');
			$this->_pagination = new JPagination( $this->getTotal(), JRequest::getVar( 'limitstart', 0 ), 10 );
		}
		return $this->_pagination;
	}
	
	function gettempsetting()
	{
		$db= & JFactory :: getDBO();
		$sql="SELECT * FROM ".$this->_table_prefix."tempsetting WHERE id=1";
		$db->setQuery($sql);
		return $db->loadObject();
	}
	
	// =============== Gettnig Fields name ========================================================================//	
	function getfields($field_section)																				
	{																												
		$db= & JFactory :: getDBO();																				
		$sql='SELECT * FROM '.$this->_table_prefix.'fields WHERE field_section='.$field_section;
		$db->setQuery($sql);																						
		return $db->loadObjectList();																					
	}																												
	// =============== EOF Gettnig Fields name ====================================================================//	
	
	// ============================= Getting Dynemic field Value =======================================//
	function fieldsvalue($field_id,$hid)																		
	{																									
		$db= & JFactory :: getDBO();																	
		$sql="SELECT * FROM ".$this->_table_prefix."fields_data where fieldid='".$field_id."' and hid='".$hid."' and section=2";	
		$db->setQuery($sql);																			
		return $db->loadObject();																	
	}																									
	// ============================= EOF Getting Dynemic field Value ===================================//
	function getcountry($country_id) {
		$db= & JFactory :: getDBO();
		$sql = "SELECT country_name FROM ".$this->_table_prefix."country  WHERE country_id=".$country_id;
		$db->setQuery($sql);
		return $db->loadObject();
	}
			
	function eventmainphoto($event_id)																			
	{	
		$db= & JFactory :: getDBO();																				
		$sql='SELECT * FROM '.$this->_table_prefix.'event_photo WHERE main_image=1 AND event_id='.$event_id;
		$db->setQuery($sql);																						
		return $db->loadObject();																					
	}
	
	function getevents($cat_id)																			
	{	
		$db= & JFactory :: getDBO();																				
		$mainframe = JFactory::getApplication();
		$params = &$mainframe->getParams();
		$filter = $params->get('filter', 'all');
		$order_by = $params->get('order', 'start_date');
		$order_dir = $params->get('direction', 'asc');
		
		$sql = 'SELECT * FROM '.$this->_table_prefix.'event WHERE published = 1 AND category='.$cat_id;
		$sql .= ($filter == 'upcoming') ? ' AND start_date > NOW()' : '';
		$sql .= ' ORDER BY '.$order_by.' '.$order_dir;
		
		$db->setQuery($sql);																						
		return $db->loadObjectlist();
	}
	
	// ============================================================= Get child Categories ================================================================== //
	function childcategory($cat_id)																			
	{	
		$db= & JFactory :: getDBO();																				
		$sql='SELECT * FROM '.$this->_table_prefix.'cal_category WHERE published = 1 AND pcat_id='.$cat_id;
		$db->setQuery($sql);																						
		return $db->loadResultArray();
	}
	// ============================================================= Get child Categories ================================================================== //

}