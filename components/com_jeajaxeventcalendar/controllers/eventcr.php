<?php
 /**
* @package   JE Ajax Event Calendar
* @copyright Copyright (C) 2009 - 2010 Open Source Matters. All rights reserved.
* @license   http://www.gnu.org/licenses/lgpl.html GNU/LGPL, see LICENSE.php
* Contact to : emailtohardik@gmail.com, joomextensions@gmail.com
* Visit : http://www.joomlaextensions.co.in/
**/

defined ('_JEXEC') or die ('Restricted access');
jimport( 'joomla.application.component.controller' );

class eventcrController extends JControllerLegacy
{
	function __construct( $default = array())
	{
		parent::__construct( $default );
	}	
	
	function cancel()
	{
		$this->setRedirect( 'index.php' );
	}

	function display() {
		parent::display();
	}
	
	function save()
	{
		$mainframe = JFactory::getApplication();
		JRequest::checkToken() or jexit( 'Invalid Token' );
		$db= & JFactory :: getDBO();
		$post = JRequest::get ( 'post' );
		$option = JRequest::getVar('option','','','string'); 
		$sql="SELECT autopub FROM  #__je_event_configration";
		$db->setQuery($sql);
		$row_data=$db->loadObject();
		if($row_data->autopub==1)
		{
			$pub=1;
		}
		else
		{
			$pub=0;
		}
		$db = JFactory::getDbo();
		$sql="insert into #__je_event values('','".$post['frname']."',".$post['category'].",'0','".$post['desc']."','".$post['start_date']."','".$post['end_date']."',".$pub.",'','')";
		$db->setQuery($sql);
		$db->query();
		$msg=JText::_( 'SUCCESSFULY_SUBMITED' );
		$return	= JRoute::('index.php?option=com_jeajaxeventcalendar');
		$mainframe->redirect( $return,$msg);
	}
}
?>
