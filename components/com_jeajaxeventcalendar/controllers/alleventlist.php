<?php
 /**
* @package   JE Ajax Event Calendar
* @copyright Copyright (C) 2009 - 2010 Open Source Matters. All rights reserved.
* @license   http://www.gnu.org/licenses/lgpl.html GNU/LGPL, see LICENSE.php
* Contact to : emailtohardik@gmail.com, joomextensions@gmail.com
* Visit : http://www.joomlaextensions.co.in/
**/ 

defined( '_JEXEC' ) or die( 'Restricted access' );
jimport( 'joomla.application.component.controller' );

class alleventlistController extends JControllerLegacy
{	
	function __construct( $default = array())
	{
		parent::__construct( $default );
		$this->_table_prefix = '#__jeajx_';
	}
	
	function cancel()
	{
		$option = JRequest::getVar ('option','','','string');
		$this->setRedirect( 'index.php?option='.$option.'&view=alleventlist'  );
	}

	function display() 
	{
		parent::display();
	}
	
	
}	
?>