<?php
 /**
* @package   JE Ajax Event Calendar
* @copyright Copyright (C) 2009 - 2010 Open Source Matters. All rights reserved.
* @license   http://www.gnu.org/licenses/lgpl.html GNU/LGPL, see LICENSE.php
* Contact to : emailtohardik@gmail.com, joomextensions@gmail.com
* Visit : http://www.joomlaextensions.co.in/
**/ 
defined( '_JEXEC' ) or die( 'Restricted access' );
jimport( 'joomla.application.component.controller' );
 
class mycalenderController extends JControllerLegacy
{	
	function __construct( $default = array())
	{
		parent::__construct( $default );
		$this->_table_prefix = '#__jeajx_';
	}
		
	function cancel()
	{
		$option = JRequest::getVar ('option','','','string');
		//$this->setRedirect( 'index.php?option='.$option.'&view=alleventlist'  );
	}
	
	function display() 
	{
		parent::display();
	}
	
	function generate_outlook() {
		$ical = new gencalendar();
		$ical->outlook();
	}
	
	function generate_ical() {
		$ical = new gencalendar();
		$ical->icalendar();
	}
	
	function gcalendar() {
	
		$option = JRequest::getVar('option');
		$db = JFactory::getDbo();
		$event_id 	= JRequest::getInt('event_id');
		$query1 	= "SELECT * FROM #__jeajx_event WHERE id=".$event_id;
		$db->setQuery($query1);
		$event_data	= $db->loadObject();
		
		$exp_startdate	= $event_data->start_date;
		$exp_sdate		= explode('-',$exp_startdate);
		$startdateonly 	= $exp_sdate[0].$exp_sdate[1].$exp_sdate[2];
		$starttimeonly	= '000000';
		$startdate		= $startdateonly."T".$starttimeonly;
		
		$exp_enddate	= $event_data->end_date;
		$exp_edate		= explode('-',$exp_enddate);
		$enddateonly 	= $exp_edate[0].$exp_edate[1].$exp_edate[2];
		$endtimeonly 	= '000000';
		$enddate		= $enddateonly."T".$endtimeonly;
		
		
		$query2 = "SELECT id,ename FROM #__jeajx_cal_category WHERE id=".$event_data->category;
		$db->setQuery($query2);
		$category_data	= $db->loadObject();
		
		$description	= $event_data->desc;
		
		$search = array ("'<script[^>]*?>.*?</script>'si",  // Strip out javascript
                 "'<[/!]*?[^<>]*?>'si",          // Strip out HTML tags
               /*  "'([rn])[s]+'",*/                // Strip out white space
                 "'&(quot|#34);'i",                // Replace HTML entities
                 "'&(amp|#38);'i",
                 "'&(lt|#60);'i",
                 "'&(gt|#62);'i",
                 "'&(nbsp|#160);'i",
                 "'&(iexcl|#161);'i",
                 "'&(cent|#162);'i",
                 "'&(pound|#163);'i",
                 "'&(copy|#169);'i",
                 "'&#(d+);'e");                    // evaluate as php

		$replace = array ("",
                 "",
                /* "\1",*/
                 "\"",
                 "&",
                 "<",
                 ">",
                 " ",
                 chr(161),
                 chr(162),
                 chr(163),
                 chr(169),
                 "chr(\1)");
				 
			 

		$description = preg_replace($search,$replace,$description);
		$description = mysql_escape_string($description);
		$description = str_replace('\r\n', ' ', $description);
		
		
		$location	= $event_data->street.','.$event_data->city.','.$event_data->country;
		$location = preg_replace($search,$replace,$location);
		
		
		
		$event_title	= $event_data->title;
		$event_title	= preg_replace($search,$replace,$event_title);
		
		echo "Google Calendar";
		
	?>	
		<form action="https://www.google.com/calendar/event" method="get" name="gcalform1">
			<input type="hidden" name="dates" value="<?php echo $startdate.'Z/'.$enddate.'Z'; ?>" />
			<input type="hidden" name="details" value="<?php echo $description; ?>" />
			<input type="hidden" name="location" value="<?php echo $location; ?>" />
			<input type="hidden" name="text" value="<?php echo $event_title; ?>" />
			<input type="hidden" name="trp" value="false" />
			<input type="hidden" name="sprop" value="name:" />
			<input type="hidden" name="action" value="TEMPLATE" />
		</form>
<?php	echo '<script language="javascript" type="text/javascript">
				var form = document.gcalform1;
				form.submit();
			</script>';
 		exit;
	}
}	

?>