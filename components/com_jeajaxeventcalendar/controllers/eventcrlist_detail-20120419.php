<?php
 /**
* @package   JE Ajax Event Calendar
* @copyright Copyright (C) 2009 - 2010 Open Source Matters. All rights reserved.
* @license   http://www.gnu.org/licenses/lgpl.html GNU/LGPL, see LICENSE.php
* Contact to : emailtohardik@gmail.com, joomextensions@gmail.com
* Visit : http://www.joomlaextensions.co.in/
**/ 

defined ( '_JEXEC' ) or die ( 'Restricted access' );
jimport ( 'joomla.application.component.controller' );

class eventcrlist_detailController extends JController {
	
	function __construct($default = array()) {
		parent::__construct ( $default );
		$this->_table_prefix = '#__jeajx_';
		$this->registerTask ( 'add', 'edit' );
	}
	
	function display() 
	{
		$user = clone(JFactory::getUser());
		if($user->id!='' )
			parent::display();
		else {
			$msg = JText::_ ( 'PLEASE_LOGIN_TO_VIEW_THIS_PAGE' );
			$this->setRedirect ( 'index.php', $msg );
		}
	}

	function save() {
		$mainframe	= JFactory::getApplication(); 
		JRequest::checkToken() or jexit( 'Invalid Token' );
		$post = JRequest::get ( 'post' );
		$post ['category'] = $post ['category1'];
		$Itemid = JRequest::getVar('Itemid','','','int');
		$db = JFactory::getDbo();
		$option = JRequest::getVar('option','','request','string');
		$view = JRequest::getVar('view','','request','string');
		
		// ======= NEW code for comparision of captcha ========================
		$cap		=	$_SESSION['comments-captcha-code'];
		$textval	= $post['cap'];
		// ================================================================
		$post["desc"] = JRequest::getVar( 'desc', '', 'post', 'string', JREQUEST_ALLOWRAW );
		$cid = JRequest::getVar ( 'cid', array (0 ), 'post', 'array' );
		$post ['id'] = $cid [0];
		$cnt=implode('`',$post['usr']);
		$post['usr']=$cnt;
		$model = $this->getModel ( 'eventcrlist_detail' );
		$thumb	= $model->getconfigration();
		
		if($cap==$textval)
		{
			// ================================== Video Link Code ================================================= //
			if($post['youtubelink']) {
				$post['googlelink'] = '';
			}
			if($post['googlelink']) {
				$post['youtubelink'] = '';
			}
			// ================================== EOF Video Link Code ============================================= //
			
			if($post['erepeat']==0 || $post['erepeat']==1 || $post['erepeat']==2 || $post['erepeat']==3) {
				if($post['id']!=0) {
					$model->deletedateevent($post['id']);
				}
			}
			if($post['erepeat']==4) {
				$dailysdate 	= JRequest::getVar ( 'dailysdate', '', 'post', 'array' );
				$dailyedate 	= JRequest::getVar ( 'dailyedate', '', 'post', 'array' );
				
				$post['start_date']	= $dailysdate[0];
				$post['end_date']	= $dailyedate[0];
			}
			
			$myeventid	= $model->store ( $post );
			
			if ($myeventid) {
				$msg = JText::_ ( 'EVENT_DETAIL_SAVED' );
			} else {
				$msg = JText::_ ( 'ERROR_SAVING_EVENT_DETAIL' );
			}
		// +++++++++++++++++++++++++++++++++++++++++++++++++++ Multiple image code ++++++++++++++++++++++++++++++++++++++ //		
			$file 	= JRequest::getVar ( 'extra_name', '', 'files', 'array' );
			$len 	= count ($file['name']);
			$pid 	= $post['mainphoto']-1;
			$main	=0;
			if($file["name"][$pid]=="")
				$pname = $post['value_id'][$pid];
			else
				$pname=$file["name"][$pid];	
		// ======================================= Code for daily selected Event ============================================= //
			if($post['erepeat']==4) {
				$dailysdate 	= JRequest::getVar ( 'dailysdate', '', 'post', 'array' );
				$dailyedate 	= JRequest::getVar ( 'dailyedate', '', 'post', 'array' );
				if($dailysdate[0]!='') {
					$model->deletedateevent($myeventid);
					for($l=0;$l<count($dailysdate);$l++) 
						$model->insertdateevent($myeventid,$dailysdate[$l],$dailyedate[$l]);
				}
			}
		// ===================================== EOF Code for daily selected Event =========================================== //
			$myimage_maxsize	= $thumb->max_img_size;
			
			for($i=0;$i<$len;$i++)
			{
				if($file['name'][$i]!="")
				{
					$iname='extra_name';
					$file =& JRequest::getVar($iname, '', 'files', 'array' );//Get File name, tmp_name
					$row->image= JPath::clean(time().'_'.$file['name'][$i]);//Make the filename unique
					$filetype = strtolower(JFile::getExt($file['name'][$i]));//Get extension of the file
					$ufile_size	= $file['size'][$i]/1048576; // Get the file size in Mb
					
					if($filetype =='jpeg' || $filetype=='jpg' || $filetype =='png' || $filetype=='gif' ||$filetype =='bmp')
					{					
						if($ufile_size<=$myimage_maxsize || $ufile_size==0) {	
							$src	= $file['tmp_name'][$i];
							$dest 	= JPATH_ROOT.DS.'components/'.$option.'/assets/event/images/'.$row->image ; 
							JFile::upload($src,$dest);	
							$dest1 	= JPATH_ROOT.DS.'components/'.$option.'/assets/event/images/thumb_'.$row->image;
							copy($dest,$dest1);
							$img 	= new thumbnail();
							$dest 	= JPATH_ROOT.DS.'components/'.$option.'/assets/event/images/'.$row->image;				
							$thumb	= $model->getconfigration();
							$img->CreatThumb($filetype,$dest1,$dest,$thumb->thumb_width,$thumb->thumb_height);
							if($post['value_id'][$i])
							{
								$sql = "DELETE FROM ".$this->_table_prefix."event_photo WHERE image='".$post["value_id"][$i]."'";
								$db->setQuery($sql);
								$db->query();
								$dest 	= JPATH_ROOT.DS.'components/'.$option.'/assets/event/images/'.$post["value_id"][$i];
								unlink($dest);
								$dest12 = JPATH_ROOT.DS.'components/'.$option.'/assets/event/images/thumb_'.$post["value_id"][$i];
								unlink($dest12);
							}
							if($file['name'][$i]==$pname)
							{
								$pname		= $row->image;
							}
							if($post['id']!=0)
							{
								$sqlphoto	= "INSERT INTO ".$this->_table_prefix."event_photo VALUES('',".$post['id'].",'".$row->image."',".$main.")";
							} else {
								$sql	= 'SELECT id FROM  '.$this->_table_prefix.'event WHERE title= "'.$post['title'].'" and category='.$post['category'];
								$db->setQuery($sql);
								$row_data	= $db->loadRow();
								$id			= $row_data[0];
								
								$sqlphoto	= "INSERT INTO ".$this->_table_prefix."event_photo VALUES('',".$id.",'".$row->image."',".$main.")";		 
							} 
							$db->setQuery($sqlphoto);
							$db->query();
						} else {
							$errlink = JRoute::_('index.php?option='.$option.'&view='.$view.'&cid[]='.$post ['id']);
							$msg1 = JText::_ ( 'YOU_CAN_UPLOAD' ).'&nbsp;'.$myimage_maxsize.JText::_('MB').'&nbsp;'.JText::_ ( 'IMAGE_FILE' );
							$mainframe->redirect( $errlink,$msg1 );
						}
					} else {
						$view 	= JRequest::getVar ('view');
						$task 	= 'edit';
						$sql	= "SELECT id FROM  ".$this->_table_prefix."event WHERE title= ".$post['title']." and category=".$post['category'];
						$db->setQuery($sql);
						$row_data	= $db->loadRow();
						$id			= $row_data[0];
						if($post['id'])
							$cid 	= $post['id'];
						else
							$cid 	= $id;
						
						$mylink = JRoute::_('index.php?option='.$option.'&view='.$view.'&cid[]='.$cid);
						$msg = JText::_ ( 'PLEASE_UPLOAD_VALID_IMAGE_FILE' );
						$mainframe->redirect( $mylink,$msg );
					}	
				}
			}
			$sqluser="UPDATE ".$this->_table_prefix."event_photo SET main_image=0 WHERE event_id=".$post['id'];	
			$db->setQuery($sqluser);
			$db->query();
			
			$sql="UPDATE ".$this->_table_prefix."event_photo SET main_image= 1 WHERE image= '".$pname."'";	
			$db->setQuery($sql);
			$db->query();
			// +++++++++++++++++++++++++++++++++++++++++++++++ EOF multiple image code +++++++++++++++++++++++++++++++++++++ //		
			if($post ['id'])
			{
				$count1=$post ['id'];
			} else {
				$sql="SELECT id FROM ".$this->_table_prefix."event WHERE title= '".$post['title']."' ";
				$db->setQuery($sql);
				$row_data=$db->loadObject();
				$count1=$row_data->id;
			}
			// ========== To inser the empoded user in the event table ================================	
			$sql="UPDATE ".$this->_table_prefix."event SET usr='".$cnt."' where id=".$count1;
			$db->setQuery($sql);
			$db->Query();
			// ========================================================================================	
		// =============== Code for Saving dynemic fields value =======================================
			$res11=new extra_field();
			$res11->extra_field_save($post,2,$count1,$count1);
		// ============================================================================================
			$save_link	= JRoute::_('index.php?option=' . $option . '&view=eventcrlist&Itemid='.$Itemid);
			$this->setRedirect ( $save_link, $msg );
		} else {
			$msg=JText::_( 'PLEASE_ENTER_CORRECT_CODE_GIVEN_IN_IMAGE' );
			$code_link	= JRoute::_('index.php?option=' . $option . '&view=eventcrlist_detail&cid[]='.$post ['id'].'&Itemid='.$Itemid.'&err=1');
			$this->setRedirect ( $code_link, $msg );
		}
	}
	
	
	//====================== New Captcha Code ==============================//		
	function captchacr() {
		//require_once(JPATH_COMPONENT.DS."helpers/kcaptcha/kcaptcha.php");
		@session_start();
		$captcha = new KCAPTCHA();
		$_SESSION['comments-captcha-code'] = $captcha->getKeyString();
		exit;
	}
	//====================== EOF New Captcha Code ==========================//
	//+++++++++++++++++++++++++++++++ Ajax Captcha Code +++++++++++++++++++++++++++++++++++++++++++++++ //
	function refresh_captchacr() {
		$option = JRequest::getVar('option','','request','string');
		$uri =& JURI::getInstance();
		$url= $uri->root();
			
		$dest = $url.'index.php?option='.$option.'&view=eventcrlist_detail&task=captchacr&tmpl=component&ac='.rand();
		echo '<img src="'.$dest.'" />';
		exit;
	}
	//++++++++++++++++++++++++++++++ EOF Ajax Captcha Code +++++++++++++++++++++++++++++++++++++++++++ //
	
	function unlink2()
	{
	  	$image = JRequest::getVar ('pname');
		$db = JFactory::getDbo();
		
		$dest = JPATH_ROOT.DS.'components/com_jeajaxeventcalendar/assets/event/images/'.$image; //specific path of the file															
			unlink($dest);
		$dest = JPATH_ROOT.DS.'components/com_jeajaxeventcalendar/assets/event/images/thumb_'.$image; //specific path of the file															
			unlink($dest);
		
		$sql = 'DELETE FROM #__jeajx_event_photo WHERE image = "'.$image.'"';
		$db->setQuery($sql);
		$temp = $db->query();
		
		if($temp)
			echo "true";
		else
			echo "false";
		
		exit;
	} 
	
	function del_dailyevent()
	{
	  	$dailyevent_id = JRequest::getInt('dailyevent_id');
		$db = JFactory::getDbo();
		$sql = 'DELETE FROM #__jeajx_dateevent WHERE id='.$dailyevent_id;
		$db->setQuery($sql);
		$temp = $db->query();
		
		if($temp)
			echo "true";
		else
			echo "false";
		
		exit;
	}
	
}
?>