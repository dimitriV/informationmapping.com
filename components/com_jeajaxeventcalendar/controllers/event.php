<?php
 /**
* @package   JE Ajax Event Calendar
* @copyright Copyright (C) 2009 - 2010 Open Source Matters. All rights reserved.
* @license   http://www.gnu.org/licenses/lgpl.html GNU/LGPL, see LICENSE.php
* Contact to : emailtohardik@gmail.com, joomextensions@gmail.com
* Visit : http://www.joomlaextensions.co.in/
**/ 

defined( '_JEXEC' ) or die( 'Restricted access' );
jimport( 'joomla.application.component.controller' );

class eventController extends JControllerLegacy  
{ 
	function __construct( $default = array())
	{
		parent::__construct( $default );
		global $context;
		$mainframe = JFactory::getApplication();
		$this->_table_prefix = '#__jeajx_';	
		parent::__construct();
	}	

	function cancel()
	{
		$option = JRequest::getVar('option','','','string');
		$this->setRedirect ( 'index.php?option=' . $option  );
		return true;
	}

 	function display() {
		parent::display();
	}

	function showThisMonth()
	{
		$mainframe	= JFactory::getApplication();
		$document = &JFactory::getDocument();
		$my =& JFactory::getUser();
		$option = JRequest::getVar('option','','','string');
		$Itemid = JRequest::getVar('Itemid','','request','int');
		$uri =& JURI::getInstance();
		$url= $uri->root();
		$img=$url."components/com_jeajaxeventcalendar/assets/images/";
		$par=$this->para_data();
		$output = '';
		$month = JRequest::getVar('month','','','int');
		$year = JRequest::getVar('year','','','int');
		// ========== Function To get all data from Event table =============//
		$cat = (int)JRequest::getVar('cat',0,'','int');
		$edataall=$this->alleventdata($cat);
		// =============== End of Event data function ======================//
		if($month == '' && $year == '') { 
			$time = time();
			$month = date('n',$time);
			$year = date('Y',$time);
		}
		if($month == '')
		{
			$time = time();
			$month = date('n',$time);
		}
		$date = getdate(mktime(0,0,0,$month,1,$year));
		$today = getdate();
		$hours = $today['hours'];
		$mins = $today['minutes'];
		$secs = $today['seconds'];
		if(strlen($hours)<2) $hours="0".$hours;
		if(strlen($mins)<2) $mins="0".$mins;
		if(strlen($secs)<2) $secs="0".$secs;
		$days=date("t",mktime(0,0,0,$month,1,$year));
		$start = $date['wday']+1;
		//$name = $date['month'];
		// ======================== Date Convert Calss ================== //
		$mydate_formate	= new gencalendar();
		// ==================== EOF Date Convert Calss ================== //
		
		// ============================= Convert month ===================== //		
		$name	= $mydate_formate->gen_formatmonth($month);
		// =========================== EOF Convert month =================== //
		
		$year2 = $date['year'];
		$offset = $days + $start - 1;
		if($month==12) { 
			$next=1; 
			$nexty=$year + 1; 
		} else { 
			$next=$month + 1; 
			$nexty=$year; 
		}
		if($month==1) { 
			$prev=12; 
			$prevy=$year - 1; 
		} else { 
			$prev=$month - 1; 
			$prevy=$year; 
		}
		if($offset <= 28) $weeks=28; 
		else if($offset > 35) $weeks = 42; 
		else $weeks = 35; 
		
		/********************************************************************/
		$db = JFactory::getDbo();
		$query = ' SELECT id  as value, ename  as text FROM '.$this->_table_prefix.'cal_category where published=1';
		$db->setQuery($query);
		$opt=$db->loadObjectList();
		$sqlapi = "SELECT gmap_api,gmap_width,gmap_height,gmap_display FROM ".$this->_table_prefix."event_configration WHERE id =1";	
		$db->setQuery($sqlapi);
		$googleapi = $db->LoadObject();
		
		$op="<select name='cat' id='cat' onchange='javascript:navigate($month,$year,this.value)'>";
		$op.="<option value='0'>".JText::_('SELECT_CATEGORY')."</option>";
		for($i=0;$i<count($opt);$i++)
		{
			if($cat==$opt[$i]->value)
				$op.="<option value='".$opt[$i]->value."' selected='selected'>".$opt[$i]->text."</option>";
			else
				$op.="<option value='".$opt[$i]->value."'>".$opt[$i]->text."</option>";
		}
		$op.="</select>";
		$year1="<select name='year' id='year' onchange='javascript:navigate(\"\",this.value,\"\")'>";
		$year1.="<option value='0'>".JText::_('SELECT')."</option>";
		$y=2000;
		$en=2050;
		for($i=$y;$i<$en;$i++)
		{
			$year1.="<option value='".$i."'>".$i."</option>";
		}
		$year1.="</select>";	
		/************************************************************************/
		$output .= "
				<table cellspacing='1' width= '100%'>
				<tr style='background:#$par->head1;color:#$par->head1_txtcolor;' >
					<td colspan='7'>
						<table class='calhead' border='0'>
							<tr>
								<td>
									<a href='javascript:navigate($prev,$prevy,\"\")'><img src='".$img."calLeft.gif'></a> <a href='javascript:navigate(\"\",\"\",\"\")'><img src='".$img."calCenter.gif'></a> <a href='javascript:navigate($next,$nexty,\"\")'><img src='".$img."calRight.gif'></a>
								</td>
								<td align='right'>
									<div>$name $year2</div>
								</td>
								<td align='right'>".JText::_('YEAR').":</td>
								<td width='75px'>".$year1."</td>
								<td align='right' width='130px'> 
									".JText::_('CATEGORY_SELECT').":
								</td>
								<td align='right' width='100px'>".$op."</td>
							</tr>
						</table>
					</td>
				</tr>
				<tr class='dayhead' style='background:#$par->head2;color:#$par->head2_txtcolor;'>
					<td>".JText::_('SUN')."</td>
					<td>".JText::_('MON')."</td>
					<td>".JText::_('TUE')."</td>
					<td>".JText::_('WED')."</td>
					<td>".JText::_('THU')."</td>
					<td>".JText::_('FRI')."</td>
					<td>".JText::_('SAT')."</td>
				</tr>";
		$col=1;
		$cur=1;
		$next=0;
		for($i=1;$i<=$weeks;$i++) { 
			if($next==3) $next=0;
			if($col==1) 
				$output.="<tr class='dayrow''>"; 
			
			$output.="<td valign='top' onMouseOver=\"this.className='dayover'\" onMouseOut=\"this.className='dayout'\" style='background:#$par->head3;color:#$par->head3_txtcolor'>";
			if($i <= ($days+($start-1)) && $i >= $start) {
				// =========== This code for compare the Day and Month with the database date ======				
				if($cur<10)
					$cur1 = '0'.$cur;
				else
					$cur1 = $cur;
				if($month<10)
					$month1 = '0'.$month;
				else
					$month1 = $month;
				// 	============= End of compare day and month function ===========================
				$dt = $year."-".$month1."-".$cur1;
				$output.="<div class='day' ><b";
				$output.=">$cur</b></div>";
				
				for($j=0;$j<count($edataall);$j++)  
				{
					$link=JRoute::_("index.php?tmpl=component&option=".$option."&view=alleventlist_more&Itemid=".$Itemid."&event_id=".$edataall[$j]->id);          
					// ========== Compare the date calendar date with Event date ==================	
					$today_style='font-weight:bold; font-size:11px;';
					if($googleapi->gmap_display==1)
					{
				// =============================== This code for GOOGLE MAP ============================================== //
						$gmaplink 	= JRoute::_('index.php?tmpl=component&option='.$option.'&view=google_map&eventid='.$edataall[$j]->id);
						$gmapicon=$url.'components/com_jeajaxeventcalendar/assets/images/google-map-icon.png';
						$gmap_icon = "<a  style=\"color:#0099FF;\"  href='".$gmaplink."' title=\"Google MAP\" onclick=\"window.open(this.href,'win2','status=no,toolbar=no,scrollbars=yes,titlebar=no,menubar=no,resizable=yes,width=".$googleapi->gmap_width.",height=".$googleapi->gmap_height.",directories=no,location=no'); return false;\" rel=\"nofollow\"><img src='".$gmapicon."' height=\"16px\" width=\"15px\"></a>";
					}
					else
						$gmap_icon ='';
				// =============================== EOF This code for GOOGLE MAP ========================================== //
				// +++++++++++++++++++++++++++++ No Repeat Event +++++++++++++++++++++++++++++++++++++ //
					if($edataall[$j]->erepeat==0) {
						if($dt==$edataall[$j]->start_date) {
							//$output.="<br />";
							$output.= "<a id=\"inline\"  onclick=\"getdatainlight('".$link."')\"  rel=\"floatbox\" ><span style='".$today_style."padding:2px; margin:5px;background:#".$edataall[$j]->bgcolor."'><font color='#".$edataall[$j]->txtcolor."'> ".$edataall[$j]->title."</font></a>".$gmap_icon."</span><br>";
						}
						
					}
				// +++++++++++++++++++++++++++++ EOF No Repeat Event +++++++++++++++++++++++++++++++++ //
					else if($edataall[$j]->erepeat==4) {
						$alldateevent	=	$this->getalldateevent($edataall[$j]->id);
						if(count($alldateevent)!=0) {
							for($k=0;$k<count($alldateevent);$k++) {
								if($dt==$alldateevent[$k]->dailysdate) {
									//$output.="<br />";
									$output.= "<a id=\"inline\"  onclick=\"getdatainlight('".$link."')\"  rel=\"floatbox\" ><span style='".$today_style."padding:2px; margin:5px;background:#".$edataall[$j]->bgcolor."'><font color='#".$edataall[$j]->txtcolor."'> ".$edataall[$j]->title."</font></a>".$gmap_icon."</span><br>";
								}
							}
						}
					}
				// (((((((((((((((((((((((((((((((((((((((((  Repeat Event ))))))))))))))))))))))))))))))))))))))))))))))) //	
					else {
						// =========== Event Start Date ============== //
						$start_date=$edataall[$j]->start_date;
						$sdate=explode("-",$start_date);
						// ========= EOF Event Start Date ============ //
						
						// =========== Current Date ============== //
						$dt = $year."-".$month1."-".$cur1;
						$cdate=explode("-",$dt);
						// ========== EOF Current Date =========== //
						
						// ============= Event Date Weekly Event ============= // 
						$s_t=mktime(0,0,0,$sdate[1],$sdate[2],$sdate[0]);
						$sdate_day=getdate($s_t);
						// =========== EOF Event Date Weekly Event =========== // 
						
						// ================ Current Dete Weekly ============== //
						$c_t=mktime(0,0,0,$cdate[1],$cdate[2],$cdate[0]);
						$cdate_day=getdate($c_t);
						// ============== EOF Current Dete Weekly ============ //
						
						if($c_t>=$s_t) {
						// <<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<< Year Repeat >>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>> //
							if($edataall[$j]->erepeat==1) {
								if($cdate[1]==$sdate[1] && $cdate[2]==$sdate[2]) {	// <<<<< Year Repeat >>>>>>>
									//$output.="<br />";
									$output.= "<a id=\"inline\"  onclick=\"getdatainlight('".$link."')\"  rel=\"floatbox\" ><span style='".$today_style."padding:2px; margin:5px;background:#".$edataall[$j]->bgcolor."'><font color='#".$edataall[$j]->txtcolor."'> ".$edataall[$j]->title."</font></a>".$gmap_icon."</span><br>";
								}
							} 
						// <<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<< EOF Year Repeat >>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>> //	
						// <<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<< Month Repeat >>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>> //
							if($edataall[$j]->erepeat==2) {
								if($cdate[2]==$sdate[2]) {	// <<<<< Monthly Repeat >>>>>>>
									//$output.="<br />";
									$output.= "<a id=\"inline\"  onclick=\"getdatainlight('".$link."')\"  rel=\"floatbox\" ><span style='".$today_style."padding:2px; margin:5px;background:#".$edataall[$j]->bgcolor."'><font color='#".$edataall[$j]->txtcolor."'> ".$edataall[$j]->title."</font></a>".$gmap_icon."</span><br>";
								}
							}
						// <<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<< EOF Month Repeat >>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>> //	
						// <<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<< Week Repeat >>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>> //
							if($edataall[$j]->erepeat==3) {
								if($cdate_day['weekday']==$sdate_day['weekday']) {	// <<<<< Weekly Repeat >>>>>>>
									//$output.="<br />";
									$output.= "<a id=\"inline\"  onclick=\"getdatainlight('".$link."')\"  rel=\"floatbox\" ><span style='".$today_style."padding:2px; margin:5px;background:#".$edataall[$j]->bgcolor."'><font color='#".$edataall[$j]->txtcolor."'> ".$edataall[$j]->title."</font></a>".$gmap_icon."</span><br>";
								}
							}
						// <<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<< EOF Week Repeat >>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>> //
						}	
					}
			 	// (((((((((((((((((((((((((((((((((((((((( EOF  Repeat Event )))))))))))))))))))))))))))))))))))))))))))) //				
				}	
				$output.="</td>";
				$cur++; 
				$col++; 
			}  else { 
				$output.="&nbsp;</td>"; 
				$col++; 
			}  
			if($col==8) { 
				$output.="</tr>"; 
				$col=1; 
			}
		}
		$output.="</table>";
		echo $output;
	} 

	function event_data($date,$cat)
	{	
		$user 		= clone(JFactory::getUser());
		$db = JFactory::getDbo();
		if(!$user)
			$user1	= 0;
		else
			$user1	= $user->id;
		if($cat==0){
			$query = 'SELECT id,title,published,start_date,bgcolor,txtcolor,erepeat FROM '.$this->_table_prefix.'event WHERE published=1 and start_date="'.$date.'" and usr like "%'.$user1.'%" or usr = 0 and  start_date="'.$date.'" and published=1 ';
		} else {
			$query = 'SELECT id,title,published,start_date,bgcolor,txtcolor,erepeat FROM '.$this->_table_prefix.'event WHERE published=1 and start_date="'.$date.'" AND category='.$cat.'and usr like "%'.$user1.'%"';
		}
		$db->setQuery($query);
		$res=$db->loadObjectlist();
		return $res;
	}

	function alleventdata($cat)
	{	
		$user 		= clone(JFactory::getUser());
		$db = JFactory::getDbo();
		if(!$user)
			$user1=0;
		else
			$user1= $user->id;
		
		if($cat==0){
			$query = ' SELECT id,title,start_date,bgcolor,txtcolor,erepeat FROM #__jeajx_event WHERE published=1 AND usr like "%'.$user1.'%" OR usr=0 AND published=1';
		} else {
			$child_cats	= $this->childcategory($cat);
			if(count($child_cats)!=0) {
				$mycategories	= implode(',',$child_cats);
				$mycategories	.= ','.$cat;
			} else
				$mycategories	= $cat;
				
			$query = ' SELECT id,title,start_date,bgcolor,txtcolor,erepeat FROM #__jeajx_event WHERE published=1 AND category IN ('.$mycategories.') AND usr like "%'.$user1.'%" OR usr=0 AND published=1 AND category IN ('.$mycategories.') ';
		}
		
		$db->setQuery($query);
		$res=$db->loadObjectlist();
		return $res;
	}
	
	// ========================================= Get Child Category =============================================== //
	function childcategory($cat_id)																			
	{	
		$db= & JFactory :: getDBO();																				
		$sql='SELECT * FROM '.$this->_table_prefix.'cal_category WHERE pcat_id='.$cat_id;
		$db->setQuery($sql);																						
		return $db->loadResultArray();
	}
	// ======================================= EOF Get Child Category ============================================= //

	function Category()
	{
		$db = JFactory::getDbo();
		$query = ' SELECT id  as value, ename  as text FROM '.$this->_table_prefix.'cal_category where published=1';
		$db->setQuery($query);
		$res=$db->loadObjectList();
		return $res;
	}

	function para_data()
	{
		$db = JFactory::getDbo();
		$query = ' SELECT * FROM '.$this->_table_prefix.'event_configration';
		$db->setQuery($query);
		$res=$db->loadObject();
		return $res;
	}

	function usermail()
	{
		$mainframe = JFactory::getApplication();
		$MailFrom    = $mainframe->getCfg('mailfrom');
		$db = JFactory::getDbo();
		$query = 'SELECT usr FROM '.$this->_table_prefix.'event where published=1 and start_date="2010-8-24" ';
		$db->setQuery($query);
		$res=$db->loadObjectlist();
		for($i=0;$i<count($res);$i++)
		{
			if($res[$i]->usr=='0') 
			{
				$query = 'SELECT * FROM #__users ';
			} else {
				if(is_array($res[$i]->usr) )
					$res_data = explode('`',$res[$i]->usr);
				else
					$res_data = $res[$i]->usr;
				$query = 'SELECT * FROM #__users WHERE id IN('.$res_data.')';
			}
			$db->setQuery($query);
			$resmail=$db->loadObjectlist();
			for($j=0;$j<count($resmail);$j++)
			{
				$frommail = $MailFrom;
				$fromname = 'Administrator';
				$recipient = $resmail[$j]->email;
				$subject = 'Event Reminder';
				$meassage = 'Dear Frined,
								The event will be organized tommorrow';
				JUtility::sendMail($frommail, $fromname, $recipient, $subject, $meassage, $mode=1);
			}
		}
		exit;
	}

	function setcalendar()
	{
		$document = &JFactory::getDocument();
 		$my =& JFactory::getUser();
		$option = JRequest::getVar('option','','','string');
		$Itemid = JRequest::getVar('Itemid','','','int');

		$uri =& JURI::getInstance();
		$url= $uri->root();
		//$document = & JFactory::getDocument();
		$document->addStyleSheet('modules/mod_jeajaxcalendar/css/ajaxcalendar.css');
		//$document->addScript("modules/mod_jeajaxcalendar/js/ajax.js");
		$par=$this->para_data();
		
		//echo '<input type="hidden"  name="jelive_url" id="jelive_url" value="'.$url.'" />';

 		$output = '';
 		$month = JRequest::getVar('month','','','int');
  		$year = JRequest::getVar('year','','','int');
		// ========== Function To get all data from Event table =============//
		$cat = JRequest::getVar('mycategory_id',0,'','int');
		$edataall=$this->alleventdata($cat);
		// =============== End of Event data function ======================//
		if($month == '' && $year == '') { 
			$time = time();
			$month = date('n',$time);
    		$year = date('Y',$time);
		}
		if($month == '')
		{
			$time = time();
			$month = date('n',$time);
		}
		$date = getdate(mktime(0,0,0,$month,1,$year));
		$today = getdate();
		$hours = $today['hours'];
		$mins = $today['minutes'];
		$secs = $today['seconds'];
		
		if(strlen($hours)<2) $hours="0".$hours;
		if(strlen($mins)<2) $mins="0".$mins;
		if(strlen($secs)<2) $secs="0".$secs;

		$days=date("t",mktime(0,0,0,$month,1,$year));
		$start = $date['wday']+1;
		//$name = $date['month'];
		
		$year2 = $date['year'];
		
		$head1 = JRequest::getVar('col1','','','string');
		$head2 = JRequest::getVar('col2','','','string');
		$head3 = JRequest::getVar('col3','','','string');
		
		$head1_txtcolor = JRequest::getVar('col1_txtcolot','','','string');
		$head2_txtcolor = JRequest::getVar('col2_txtcolot','','','string');
		$head3_txtcolor = JRequest::getVar('col3_txtcolot','','','string');
		
		
		// ================ Current Month Day and Year ============= // 
		$current_year = date('Y');
		$current_month = date('m');
		$current_day = date('d');
		// ============== EOF Current Month Day and Year =========== //
		
		// ======================== Date Convert Calss ================== //
		$mydate_formate	= new gencalendar();
		// ==================== EOF Date Convert Calss ================== //
		
		// ============================= Convert month ===================== //		
		$name	= $mydate_formate->gen_formatmonth($month);
		// =========================== EOF Convert month =================== //
		
		$offset = $days + $start - 1;
 
		if($month==12) { 
			$next=1; 
			$nexty=$year + 1; 
		} else { 
			$next=$month + 1; 
			$nexty=$year; 
		}
		
		if($month==1) { 
			$prev=12; 
			$prevy=$year - 1; 
		} else { 
			$prev=$month - 1; 
			$prevy=$year; 
		}

		if($offset <= 28) $weeks=28; 
		elseif($offset > 35) $weeks = 42; 
		else $weeks = 35; 
/********************************************************************/
		
		$y=2000;
		$en=2050;
		
		$myformlink	=	JRoute::_("index.php?option=com_jeajaxeventcalendar&view=event_list&Itemid=".$Itemid);	
		
		$output = '<form name="myevent_valfrm" method="post" action="'.$myformlink.'">';
		
		$output .='<table border="0" cellpadding="2" cellspacing="1" width="100%" >';
		
		$output .="<tr valign='top' style='background:#$head1;color:#$head1_txtcolor;'>";
		
		$output .= "<th ><a href=\"javascript:onclick = nextmonth('".$prev."','".$prevy."','".$head1."','".$head2."','".$head3."','".$cat."','".$head1_txtcolor."','".$head2_txtcolor."','".$head3_txtcolor."','".$Itemid."')\"><span style=\"color:#".$head1_txtcolor.";\"><b>".JText::_('<<')."</b></span></a></th>"; 
		
		$output .='<th colspan="5" align="center"><span style="color:#'.$head1_txtcolor.';">'.$name.' '.$year2.'</span></th>';
		
		$output .= "<th align='right' ><a href=\"javascript:onclick = nextmonth('".$next."','".$nexty."','".$head1."','".$head2."','".$head3."','".$cat."','".$head1_txtcolor."','".$head2_txtcolor."','".$head3_txtcolor."','".$Itemid."')\"><span style=\"color:#".$head1_txtcolor.";\"><b>".JText::_('>>')."</b></span></a></th>";
		
		$output .= '</tr>';
		
		$output .= '<tr bgcolor="#'.$head2.'" style="font-weight:bold;color:#'.$head2_txtcolor.';">
					<td>'.JText::_('SUN1').'</td>
					<td >'.JText::_('MON1').'</td>
					<td >'.JText::_('TUE1').'</td>
					<td >'.JText::_('WED1').'</td>
					<td >'.JText::_('THU1').'</td>
					<td >'.JText::_('FRI1').'</td>
					<td >'.JText::_('SAT1').'</td>
					</tr>';
		
		$col=1;
		$cur=1;
		$next=0;

		for($i=1;$i<=$weeks;$i++) { 
			if($next==3) 
				$next=0;
			if($col==1) 
				$output .= '<tr bgcolor="#'.$head3.'" style="color:#'.$head3_txtcolor.';">';
  			
			
			if($i <= ($days+($start-1)) && $i >= $start) {
			// =========== This code for compare the Day and Month with the database date ======				
				if($cur<10)
					$cur1 = '0'.$cur;
				else
					$cur1 = $cur;
					
				if($month<10)
					$month1 = '0'.$month;
				else
					$month1 = $month;
					
			// 	============= End of compare day and month function ===========================
				$dt = $year."-".$month1."-".$cur1;
				
				
				$eventdata = '';
				$eventdata_color	= '';
				
				$myevent_dates = '';
				$myevent_id	= '';
				
			 	for($j=0;$j<count($edataall);$j++)  
				{
					//$link=JRoute::_("index.php?tmpl=component&option=".$option."&view=alleventlist_more&Itemid=".$Itemid."&event_id=".$edataall[$j]->id);          
					// ========== Compare the date calendar date with Event date ==================	
					$today_style='font-weight:bold;';
					
				// +++++++++++++++++++++++++++++ No Repeat Event +++++++++++++++++++++++++++++++++++++ //
					if($edataall[$j]->erepeat==0) {
						if($dt==$edataall[$j]->start_date) {
							$start_norepeat=explode("-",$edataall[$j]->start_date);
								
							$eventdata.=$edataall[$j]->title.'<br>';
							$myevent_id	.= $edataall[$j]->id.',';
									
							$myevent_dates	= $dt;
									
							if($edataall[$j]->bgcolor!='')
								$eventdata_color	.= $edataall[$j]->bgcolor;
							else
								$eventdata_color	.= '135cae';
						}
						
						
					}
				// +++++++++++++++++++++++++++++ EOF No Repeat Event +++++++++++++++++++++++++++++++++ //
					else if($edataall[$j]->erepeat==4) {
						$alldateevent	=	$this->getalldateevent($edataall[$j]->id);
						if(count($alldateevent)!=0) {
							for($k=0;$k<count($alldateevent);$k++) {
								if($dt==$alldateevent[$k]->dailysdate) {
									$edate_repeat=explode("-",$edataall[$j]->start_date);
									
									$link	=	JRoute::_("index.php?option=com_jeajaxeventcalendar&view=event_list&Itemid=".$Itemid."&ey=".$edate_repeat[0].'&em='.$edate_repeat[1].'&ed='.$edate_repeat[2]);	
									$eventdata.=$edataall[$j]->title.'<br>';
									
									$myevent_id	.= $edataall[$j]->id.',';
									
									$myevent_dates	= $dt;
									
									if($edataall[$j]->bgcolor!='')
										$eventdata_color	.= $edataall[$j]->bgcolor;
									else
										$eventdata_color	.= '135cae';
								}
							}
						}
					}
				// (((((((((((((((((((((((((((((((((((((((((  Repeat Event ))))))))))))))))))))))))))))))))))))))))))))))) //	
					else {
						// =========== Event Start Date ============== //
						$start_date=$edataall[$j]->start_date;
						$sdate=explode("-",$start_date);
						
						// ========= EOF Event Start Date ============ //
						
						// =========== Current Date ============== //
						$dt = $year."-".$month1."-".$cur1;
						$cdate=explode("-",$dt);
						// ========== EOF Current Date =========== //
						
						
						// ============= Event Date Weekly Event ============= // 
						$s_t=mktime(0,0,0,$sdate[1],$sdate[2],$sdate[0]);
						$sdate_day=getdate($s_t);
						// =========== EOF Event Date Weekly Event =========== // 
						
						// ================ Current Dete Weekly ============== //
						$c_t=mktime(0,0,0,$cdate[1],$cdate[2],$cdate[0]);
						$cdate_day=getdate($c_t);
						// ============== EOF Current Dete Weekly ============ //
						
						if($c_t>=$s_t) {
						// <<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<< Year Repeat >>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>> //
							if($edataall[$j]->erepeat==1) {
								if($cdate[1]==$sdate[1] && $cdate[2]==$sdate[2]) {	// <<<<< Year Repeat >>>>>>>
									$eventdata.=$edataall[$j]->title.'<br>';
									$myevent_id	.= $edataall[$j]->id.',';
									
									$myevent_dates	= $dt;
									
									if($edataall[$j]->bgcolor!='')
										$eventdata_color	.= $edataall[$j]->bgcolor;
									else
										$eventdata_color	.= '135cae';
								}
							} 
						// <<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<< EOF Year Repeat >>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>> //	
						// <<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<< Month Repeat >>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>> //
							if($edataall[$j]->erepeat==2) {
								if($cdate[2]==$sdate[2]) {	// <<<<< Monthly Repeat >>>>>>>
									$eventdata.=$edataall[$j]->title.'<br>';
									
									$myevent_id	.= $edataall[$j]->id.',';
									
									//echo $myevent_id;
									
									$myevent_dates	= $dt;
									
									if($edataall[$j]->bgcolor!='')
										$eventdata_color	.= $edataall[$j]->bgcolor;
									else
										$eventdata_color	.= '135cae';
								}
							}
						// <<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<< EOF Month Repeat >>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>> //	
						// <<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<< Week Repeat >>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>> //
							if($edataall[$j]->erepeat==3) {
								if($cdate_day['weekday']==$sdate_day['weekday']) {	// <<<<< Weekly Repeat >>>>>>>
									$eventdata.=$edataall[$j]->title.'<br>';
									$myevent_id	.= $edataall[$j]->id.',';
									
									$myevent_dates	= $dt;
									
									if($edataall[$j]->bgcolor!='')
										$eventdata_color	.= $edataall[$j]->bgcolor;
									else
										$eventdata_color	.= '135cae';
								}
							}
						// <<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<< EOF Week Repeat >>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>> //
						}	
					}
			 	// (((((((((((((((((((((((((((((((((((((((( EOF  Repeat Event )))))))))))))))))))))))))))))))))))))))))))) //				
				}
				//echo $eventdata.'<br>'; 
				
				if($eventdata!='') {
					if(strlen($eventdata_color)>6)
						$eventdata_color	= 'C00000';
					else
						$eventdata_color	= $eventdata_color;
						
					//$output.="<td valign='top' align='center' bgcolor='#".$eventdata_color."' ><span class=\"ToolText\" onMouseOver=\"javascript:this.className='ToolTextHover'\" onMouseOut=\"javascript:this.className='ToolText'\"><a href='".$link."' style='background-color:#".$eventdata_color."; color:#".$menutextcolor.";'><b>$cur</b></a><span >".$eventdata."</span></span>";
					$output.="<td valign='top' align='center' bgcolor='#".$eventdata_color."' ><span class=\"ToolText\" onMouseOver=\"javascript:this.className='ToolTextHover'\" onMouseOut=\"javascript:this.className='ToolText'\"><a href='javascript:void(0)' onclick=\"mydatewise_event('".$myevent_id."','".$myevent_dates."')\" style='background-color:#".$eventdata_color."; color:#".$head3_txtcolor.";'><b>$cur</b></a><span >".$eventdata."</span></span>";
				} else if (($year == $current_year) and ($month == $current_month) and ($cur == $current_day))  {
					$output.="<td valign='top' align='center' bgcolor='#".$head3."' ><span><b>$cur</b></span>";
				} else {
					$output.=" <td valign='top' align='center' bgcolor='#".$head3."' ><span>$cur</span>";
				}
					
				$output.="</td>";
				$cur++; 
				$col++; 
			} 
			else { 
				$output.="<td valign='top' align='center' bgcolor='#".$head3."' >&nbsp;</td>"; 
				$col++; 
			}  
	    
			if($col==8) { 
	   			$output.="</tr>"; 
	    		$col=1; 
    		}
		}
		
		$output.="</table>";
		$output.="<input type=\"hidden\" id='date_event_id' name='date_event_id' value=''><input type='hidden' id='myevent_dates' name='myevent_dates' value=''>";
		$output.="</form>";
  		echo $output;
		exit;
	}
	
	// ==================================================== Rss Feed Xml =================================================== //
	function rss_feed(){
	
		$db = JFactory::getDbo();
		$uri =& JURI::getInstance();
		$url= $uri->root();
		
		$cat_id = JRequest::getVar('category','0','','int');
		if($cat_id == 0)
			$sql = "SELECT c.ename,e.* FROM jos_jeajx_event As e,jos_jeajx_cal_category As c WHERE e.category = c.id";
		else
			$sql = "SELECT c.ename,e.* FROM jos_jeajx_event As e,jos_jeajx_cal_category As c WHERE e.category = ".$cat_id." AND e.category = c.id";
		$db->setQuery($sql);
		$feed_data=$db->loadObjectList();		
		
		$model = $this->getModel ( 'event' );
	
		$rss_data ='<?xml version="1.0" encoding="UTF-8"?>
					<!-- generator="FeedCreator 1.7.3" -->
				<rss version="2.0"> 
				<channel> 
					<title>'.JText::_('Events').'</title>
					<description>'.JText::_('Event Description').'</description>  
					<link>'.$url.'</link>
					<generator>FeedCreator 1.7.3</generator>';
				$rss_item = '';
				$rss_image = '';
				for($i=0;$i<count($feed_data);$i++){
					$photo = $model->geteventphoto($feed_data[$i]->id);
					if(count($photo)!=0)
						$photo	= $photo;
					else
						$photo	= 'noimage.png';
					
					//echo $photo.'<br>'	;
					
					$description = $feed_data[$i]->desc;
					$desc_videolink = preg_match("{video_link}",$description);
					if($desc_videolink==1) 
						$description = str_replace('{video_link}','',$description);
					$desc_image = preg_match("{images}",$description);
					if($desc_image==1) 
						$description = str_replace('{images}','',$description);
					$desc_video = preg_match("{video}",$description);
					if($desc_video==1) 
						$description = str_replace('{video}','',$description);
						
					$rss_item .= '
					<item>
						<title>'.$feed_data[$i]->title.'</title>
						<link>'.$url.'index.php?option=com_jeajaxeventcalendar&amp;view=alleventlist_more&amp;event_id='.$feed_data[$i]->id.'</link>
						<description><![CDATA[<img src="'.$url.'components/com_jeajaxeventcalendar/assets/event/images/thumb_'.$photo.'" border="0" width="95" height="71" style="float: right; margin-left: 5px; margin-right: 5px;" /><br>'.$description .']]></description>
						<category>'.$feed_data[$i]->ename.'</category>
						<pubDate>'.$feed_data[$i]->start_date.'</pubDate>
					</item>';
				}
		$rss_data .= $rss_item;
		$rss_data .= '</channel>';
		$rss_data .= '</rss>';
		
		//exit;	
		echo $rss_data;
		exit;
	}
	// ================================================== EOF Rss Feed Xml ================================================= //
	
	// =========================================== Get All Date Events ================================================= //	
	function getalldateevent($event_id)
	{
		$db= & JFactory :: getDBO();
		$query = 'SELECT * FROM '.$this->_table_prefix.'dateevent WHERE event_id='.$event_id;
		$db->setQuery($query);
		return $db->loadObjectlist();
	}
	// ========================================= EOF Get All Date Events =============================================== //
	
}