<?php
/**
* @package		EasyBlog
* @copyright	Copyright (C) 2010 Stack Ideas Private Limited. All rights reserved.
* @license		GNU/GPL, see LICENSE.php
* EasyBlog is free software. This version may have been modified pursuant
* to the GNU General Public License, and as distributed it includes or
* is derivative of works licensed under the GNU General Public License or
* other free or open source software licenses.
* See COPYRIGHT.php for copyright notices and details.
*/

// Check to ensure this file is included in Joomla!
defined('_JEXEC') or die( 'Restricted access' );

jimport( 'joomla.application.component.view');
jimport( 'joomla.filesystem.file' );
jimport( 'joomla.filesystem.folder' );

require_once( JPATH_ROOT . DS . 'administrator' . DS . 'components' . DS . 'com_media' . DS . 'helpers' . DS . 'media.php' );

class EasyBlogViewImages extends EasyBlogView
{
	public function loadScript($scripts)
	{
		$document	= JFactory::getDocument();
		$version = str_ireplace('.', '', EasyBlogHelper::getLocalVersion());

		foreach($scripts as $script)
		{
			$document->addScript(JURI::root() . $script . '?' . $version); 
		}
	}

	public function display($tpl = null)
	{
		$config     = EasyBlogHelper::getConfig();
		
		$document	= JFactory::getDocument();
		$my         = JFactory::getUser();
		$user		= JFactory::getUser(JRequest::getInt('blogger_id', $my->id));

		$app 	= JFactory::getApplication();
		
		if( $my->id <= 0 )
		{
			echo JText::_( 'COM_EASYBLOG_NOT_ALLOWED' );
			exit;
		}

		if( $config->get( 'debug_javascript' ) || JRequest::getInt( 'ebjsdebug') == 1 )
		{
			$this->loadScript(array(
				'media/com_easyblog/js/dev/easyblog.foundry.js',
				'media/foundry/js/dev/jquery.json.js',
				'media/foundry/js/dev/jquery.lang.js',
				'media/foundry/js/dev/jquery.lang.rsplit.js',
				'media/foundry/js/dev/jquery.event.destroyed.js',
				'media/foundry/js/dev/jquery.class.js',
				'media/foundry/js/dev/jquery.controller.js',
				'media/foundry/js/dev/jquery.view.js',
				'media/foundry/js/dev/jquery.view.ejs.js',
				'media/foundry/js/dev/jquery.scrollTo.js',
				'media/com_easyblog/js/dev/easyblog.filemanager.js',
				'media/com_easyblog/js/dev/easyblog.filemanager.operations.js',
				'media/com_easyblog/js/dev/easyblog.filemanager.uploader.js',
				'media/com_easyblog/js/dev/easyblog.filemanager.toolbar.js',
				'media/com_easyblog/js/dev/easyblog.filemanager.toolbar.search.js',
				'media/com_easyblog/js/dev/easyblog.filemanager.toolbar.pathway.js',
				'media/com_easyblog/js/dev/easyblog.filemanager.toolbar.upload.js',
				'media/com_easyblog/js/dev/easyblog.filemanager.browser.js',
				'media/com_easyblog/js/dev/easyblog.filemanager.browser.itemhandler.file.js',
				'media/com_easyblog/js/dev/easyblog.filemanager.browser.previewhandler.file.js',
				'media/com_easyblog/js/dev/easyblog.filemanager.browser.itemhandler.folder.js',
				'media/com_easyblog/js/dev/easyblog.filemanager.browser.previewhandler.folder.js',
				'media/com_easyblog/js/dev/easyblog.filemanager.browser.itemhandler.video.js',
				'media/com_easyblog/js/dev/easyblog.filemanager.browser.previewhandler.video.js',
				'media/com_easyblog/js/dev/easyblog.filemanager.browser.itemhandler.image.js',
				'media/com_easyblog/js/dev/easyblog.filemanager.browser.previewhandler.image.js',
				'media/com_easyblog/js/dev/easyblog.filemanager.browser.itemhandler.upload.js',
				'media/com_easyblog/js/dev/easyblog.filemanager.browser.previewhandler.upload.js'
			));
		} else {
			$this->loadScript(array(
				'media/com_easyblog/js/easyblog.foundry.js',
				'media/foundry/js/jquery.mvc.js',
				'media/foundry/js/jquery.scrollTo.js',
				'media/com_easyblog/js/easyblog.filemanager.js'
			));
		}

		JHTML::_('script'    , 'plupload.js', 'components/com_easyblog/assets/vendors/plupload/');
		JHTML::_('script'    , 'plupload.html4.js', 'components/com_easyblog/assets/vendors/plupload/');
		JHTML::_('script'    , 'plupload.flash.js', 'components/com_easyblog/assets/vendors/plupload/');
		JHTML::_('stylesheet', 'styles.css', 'components/com_easyblog/themes/dashboard/system/css/');
		JHTML::_('stylesheet', 'filemanager.css', 'components/com_easyblog/assets/css/');
		
		$main_image_path	= $config->get('main_image_path');
        $main_image_path 	= rtrim($main_image_path, '/');
		$userImagePath  	= str_replace('/', DS, $main_image_path . DS . $user->id);
		$userImageBasePath  = $main_image_path . '/' . $user->id;
  		$userUploadPath     = JPATH_ROOT . DS . str_replace('/', DS, $main_image_path . DS . $user->id);
  		$userUploadPathBase = '/'. $main_image_path . '/' . $user->id;

  		if(! JFolder::exists($userUploadPath))
  		{
  		    JFolder::create( $userUploadPath );
	        $source 		= JPATH_ROOT . DS . 'components' . DS . 'com_easyblog' . DS . 'index.html';
			$destination	= $userUploadPath . DS .'index.html';
        	JFile::copy( $source , $destination );
  		}
  		
  		$folder	= JRequest::getVar( 'folder' );

		$userUploadPath	.= DS . str_ireplace( '/' , DS , $folder );
		
		if( !empty( $folder ) )
		{
			$userImageBasePath	.= '/' . $folder;
		}

		$images = $this->getFiles( $userUploadPath , JURI::root() . $userImageBasePath , $folder );

		// Set the path so that javascript can process the parts.
		$path	= '';
		if( $folder )
		{
			$path	= '/' . $folder;
		}

		$debug		= ( $config->get( 'debug_javascript') || JRequest::getVar( 'ebjsdebug' ) == 1 ) ? 'true' : 'false';

		$theme = new CodeThemes( true );
		$theme->set( 'debug'	, $debug );
		$theme->set( 'path' 	, $path );
		$theme->set( 'baseURL',	JURI::root() . $userImageBasePath );
		$theme->set( 'session', JFactory::getSession() );
		$theme->set( 'images' , $images );
		$theme->set( 'blogger_id' , $user->id );

		echo $theme->fetch( 'dashboard.images.php' );
	}
	
	public function getFiles( $folder , $baseURL , $subfolder = '' )
	{
		static $list;

		if (is_array($list))
		{
			return $list;
		}

		// @rule: Retrieve folders
		$folders	= JFolder::folders( $folder , '.' , false , true );
		$foldersData= array();
		
		if( $folders )
		{
			foreach( $folders as $curFolder )
			{
			    // fixed for path in window enviroment in joomla 1.7.
				$curFolder   = str_ireplace( '/' , DS , $curFolder );
				$curFolder	= str_ireplace( $folder . DS , '' , $curFolder );


				$foldersData[] = EasyBlogHelper::getHelper( 'ImageData' )->getFolderObject( $folder , $curFolder , $baseURL , $subfolder );
			}
		}

		// Retrieve files
		$files		= JFolder::files( $folder , '.' , false , true );
		$data		= array();

		array_multisort(
    		array_map( 'filectime', $files ),
    		SORT_NUMERIC,
    		SORT_DESC,
    		$files
		);

		if( $files )
		{
			foreach( $files as $file )
			{
			    // fixed for path in window enviroment in joomla 1.7.
				$file   = str_ireplace( '/' , DS , $file );
				$file	= str_ireplace( $folder . DS , '' , $file );

				if( is_file( $folder . DS . $file ) && substr($file, 0, 1) != '.' && strtolower($file) !== 'index.html' && stristr( $file , EBLOG_MEDIA_THUMBNAIL_PREFIX ) === false )
				{
					$data[] = EasyBlogHelper::getHelper( 'ImageData' )->getObject( $folder , $file , $baseURL , $subfolder );
				}
			}
		}
		krsort( $data );

		$data	= array_merge( $foldersData , $data );
		
		return $data;
	}


}
