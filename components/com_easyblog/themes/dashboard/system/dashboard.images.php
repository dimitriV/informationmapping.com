<?php
/**
* @package      EasyBlog
* @copyright    Copyright (C) 2010 Stack Ideas Private Limited. All rights reserved.
* @license      GNU/GPL, see LICENSE.php
* EasyBlog is free software. This version may have been modified pursuant
* to the GNU General Public License, and as distributed it includes or
* is derivative of works licensed under the GNU General Public License or
* other free or open source software licenses.
* See COPYRIGHT.php for copyright notices and details.
*/

defined('_JEXEC') or die('Restricted access');

$controllerName	= 'controller';
$isBackend		= false;

if( stristr( JRequest::getURI() , '/administrator' ) !== false )
{
	$controllerName	= 'c';
	$isBackend      = true;
}
?>
<?php if( stristr( JRequest::getURI() , '/administrator' ) !== false ){ ?>
<div id="eblog-wrapper" class="eblog-default">
<?php } ?>
<script type="text/javascript">
(function($){

$.lang(
{
    'COM_EASYBLOG_UPLOAD_LOG_COMPLETED' : '<?php echo JText::_( 'COM_EASYBLOG_UPLOAD_LOG_COMPLETED' );?>',
    'COM_EASYBLOG_UPLOAD_LOG_UPLOADING' : '<?php echo JText::_( 'COM_EASYBLOG_UPLOAD_LOG_UPLOADING' );?>',
    'COM_EASYBLOG_UPLOAD_LOG_UPLOADED_PROGRESS' : '<?php echo JText::_( 'COM_EASYBLOG_UPLOAD_LOG_UPLOADED_PROGRESS');?>',
    'COM_EASYBLOG_DELETE_FILE_CONFIRMATION': '<?php echo JText::_( 'COM_EASYBLOG_DELETE_FILE_CONFIRMATION' );?>',
    'COM_EASYBLOG_DELETE_IMAGE_CONFIRMATION': '<?php echo JText::_( 'COM_EASYBLOG_DELETE_IMAGE_CONFIRMATION' ); ?>',
    'COM_EASYBLOG_DELETE_VIDEO_CONFIRMATION': '<?php echo JText::_( 'COM_EASYBLOG_DELETE_VIDEO_CONFIRMATION' );?>',
    'COM_EASYBLOG_DELETE_FOLDER_CONFIRMATION': '<?php echo JText::_( 'COM_EASYBLOG_DELETE_FOLDER_CONFIRMATION' );?>',
    'COM_EASYBLOG_IMAGE_UPLOADER_SEARCH_NO_RESULTS': '<?php echo JText::_( 'COM_EASYBLOG_IMAGE_UPLOADER_SEARCH_NO_RESULTS');?>',
    'COM_EASYBLOG_IMAGE_UPLOADER_YOU_DO_NOT_HAVE_ANY_IMAGES_UPLOADED': '<?php echo JText::_('COM_EASYBLOG_IMAGE_UPLOADER_YOU_DO_NOT_HAVE_ANY_IMAGES_UPLOADED'); ?>',
    'COM_EASYBLOG_DELETING': '<?php echo JText::_('COM_EASYBLOG_DELETING');?>',
    'COM_EASYBLOG_DELETED': '<?php echo JText::_('COM_EASYBLOG_DELETED');?>',
    'COM_EASYBLOG_ERROR_DELETING': '<?php echo JText::_('COM_EASYBLOG_ERROR_DELETING');?>',
    'COM_EASYBLOG_CREATING_FOLDER': '<?php echo JText::_('COM_EASYBLOG_CREATING_FOLDER');?>',
    'COM_EASYBLOG_FOLDER_CREATED': '<?php echo JText::_('COM_EASYBLOG_FOLDER_CREATED');?>',
    'COM_EASYBLOG_ERROR_CREATING_FOLDER': '<?php echo JText::_('COM_EASYBLOG_ERROR_CREATING_FOLDER');?>',
    'COM_EASYBLOG_ITEM_INSERTED': '<?php echo JText::_('COM_EASYBLOG_ITEM_INSERTED');?>'
});

$(document).ready(function()
{
    $('.fileManager').implement(
        'EasyBlog.FileManager',
        {
            dataset: <?php echo $this->json_encode($images); ?>,
    		editorName: 'write_content',
            path: '<?php echo $path; ?>',
    		debug: <?php echo $debug; ?>,
            uploadSettings: {

                container: 'fileManager-plupload-container',
                browse_button : 'upload-start-button',

                url: '<?php echo JURI::base(); ?>index.php?option=com_easyblog&<?php echo $controllerName;?>=images&task=ajaxupload&tmpl=component&format=json&sessionid=<?php echo $session->getId(); ?>&<?php echo JUtility::getToken();?>=1&bloggger_id=<?php echo $blogger_id; ?>&folder=<?php echo ltrim( $path , '/' ); ?>',
                <?php if( $system->config->get( 'main_upload_client') == 'flash' && !$isBackend ){ ?>
                runtimes           : 'flash, html4',
                <?php } else { ?>
                runtimes           : 'html4,flash',
                <?php } ?>
                flash_swf_url      : '<?php echo JURI::root(); ?>components/com_easyblog/assets/vendors/plupload/plupload.flash.swf',
                max_file_size       : '<?php echo $system->config->get( 'main_upload_image_size' );?>mb',
                max_file_count: 20,
                unique_names  : true,
                filters       : [{title: "Image files", extensions: "<?php echo $system->config->get( 'main_media_extensions' );?>"}]

                <?php if( $system->config->get( 'main_resize_image_on_upload') ){ ?>
                ,resize: {
                    width  : <?php echo $system->config->get( 'main_maximum_image_width' ); ?>,
                    height : <?php echo $system->config->get( 'main_maximum_image_height' ); ?>,
                    quality: <?php echo $system->config->get( 'main_upload_quality' ); ?>
                }
                <?php } ?>

            }

        },
        function(){}
    );
});

})(Foundry);
</script>


<!--[if IE 8]>
<style type="text/css">
#ezblog-dashboard .create-folder-form #folder-name {
    line-height: 32px;
}

.item-container .item-thumbnail {
    float: left;
}
</style>
<![endif]-->


<!--[if IE 7]>
<style type="text/css">

.fileManager-toolbar-search {
    float: left;
}
.fileManager-toolbar-pathway {
    float: left;
    margin-left: 0 !important;
}
.item-container .item-thumbnail,
.item-container .item-title {
    float: left;
}
body #eblog-wrapper .item-container .ui-button.item-button {
    top: 5px;
    right: 5px;
    height: 27px !important;
}

.image-size, .image-size label, .image-size input {
    float: left;
}

body #eblog-wrapper .ui-button#upload-start-button span,
body #eblog-wrapper .ui-button#upload-stop-button span, {
    margin-top: -3px;
}

.create-folder-form,
.create-folder-toggle {
    float: left;
}
.create-folder-button {
    float: left;
    margin: 0 5px;
}

.label-cancel {
    padding: 0 3px !important;
}

#ezblog-dashboard .pathway-nav li {
    float: left;
}
#ezblog-dashboard .pathway-nav li + li {
    margin-left: 5px;
}
</style>
<![endif]-->

<div id="ezblog-dashboard" class="fileManager">

    <!-- FileManager.toolbar: Start -->
    <div id="fileManager-toolbar" class="fileManager-toolbar clearfix">

        <!-- FileManager.toolbar.search: Start -->
        <div class="fileManager-toolbar-search">
            <button class="item-search-toggle"></button>
            <form class="item-search-form">
                <table>
                    <tr>
                        <td class="search-label" nowrap><?php echo JText::_( 'COM_EASYBLOG_SEARCH_IN' ); ?> <strong class="item-search-location"></strong>:</td>
                        <td class="search-wrap"><input type="text" name="image-search" class="item-search-keyword" value="" /></td>
                    </tr>
                </table>
            </form>
        </div>
        <!-- FileManager.toolbar.search: End -->

        <!-- FileManager.toolbar.pathway: Start -->
        <div class="fileManager-toolbar-pathway">
            <ul class="pathway-nav">
            </ul>

            <div class="pathway-action">
                <div class="create-folder-form">
                    <input type="text" id="folder-name" name="folder-name" value="" />
                    <input type="hidden" name="current-path" id="current-path" value="" />
                </div>
                <button type="button" class="ui-button create-folder-button"><?php echo JText::_('COM_EASYBLOG_CREATE_FOLDER') ?></button>
                <a class="create-folder-toggle" href="javascript: void(0);">
                    <span class="label-create"><?php echo JText::_( 'COM_EASYBLOG_MEDIA_CREATE_FOLDER' ); ?></span>
                    <span class="label-cancel"><?php echo JText::_('Cancel') ?></span>
                </a>
            </div>
        </div>
        <!-- FileManager.toolbar.pathway: End -->

        <!-- FileManager.toolbar.upload: Start -->
        <?php if ($this->acl->rules->upload_image): ?>
        <div class="fileManager-toolbar-upload init">
            <div id="fileManager-plupload-container">
                <button id="upload-start-button" type="button" class="ui-button">
                    <span><?php echo JText::_( 'COM_EASYBLOG_UPLOAD_BUTTON' ); ?></span>
                    <span class="wait"><?php echo JText::_( 'COM_EASYBLOG_WAIT' ); ?></span>
                </button>
                <div class="fileManager-upload-note">
                    <i class="a-point-top"></i>
                    <div class="in">
                        <div><?php echo JText::_( 'COM_EASYBLOG_ALLOWED_UPLOAD_TYPES' );?></div>
                        <?php
                        $allowed	= explode( ',' , $system->config->get( 'main_media_extensions' ) );
                        
                        for( $i = 0; $i < count( $allowed ); $i ++ )
                        {
                        ?>
                        <?php echo $i != 0 ? ',' : '';?> <strong>.<?php echo $allowed[ $i ];?></strong> 
                        <?php
						}
						?>
                    </div>
                </div>
            </div>

            <button id="upload-stop-button" type="button" class="ui-button"><span><?php echo JText::_( 'COM_EASYBLOG_STOP_UPLOAD_BUTTON' ); ?></span></button>                    
            <div class="upload-progress">
                <div class="in">
                    <div class="upload-progress-bar"></div>
                    <div class="upload-progress-value">0%</div>
                </div>
            </div>
        </div>
        <?php endif; ?>
        <!-- FileManager.toolbar.upload: End -->

        <div class="fileManager-toolbar-message"></div>

    </div>
    <!-- FileManager.toolbar: End -->

    <!-- FileManager.browser: Start -->
    <div class="fileManager-browser">
        <div class="fileManager-browser-sidePane image-item-details"></div>
        <div class="fileManager-browser-contentPane">
            <div class="contentPane-message"></div>
            <ul class="fileManager-itemGroup">
                <li class="item-separator separator-type-upload"><?php echo JText::_( 'COM_EASYBLOG_RECENT_UPLOADS' ); ?></span></li>
                <li class="item-separator separator-type-all"><?php echo JText::_( 'COM_EASYBLOG_IN_THIS_FOLDER' ); ?></span></li>
            </ul>
        </div>
    </div>
    <!-- FileManager.browser: End -->

</div>

<?php ob_start(); ?>

<script type="text/ejs" id="EasyBlog.FileManager.Toolbar.Pathway.Item">
<li class="pathway-item"><a href="<?php echo JRoute::_('index.php?option=com_easyblog&view=images&tmpl=component'); ?>&folder=<@= folder @>"><span><@= folderName @></span></a></li>
</script>

<script type="text/ejs" id="EasyBlog.FileManager.Browser.ItemContainer">
<li class="item-container item-type-<@= item.type @>"></li>
</script>

<script type="text/ejs" id="EasyBlog.FileManager.Browser.ItemHandler.Folder.Item">
<div class="item-summary clearfix">
    <div class="item-thumbnail">
        <span></span>
        <img src="<?php echo rtrim( JURI::root() , '/' ) . '/components/com_easyblog/assets/images/media/folder.png' ;?>" alt="<@= name @>" />
    </div>
    <div class="item-title"><span class="item-filename"><@= name @></span><span class="item-filesize"><@= files @> <?php echo JText::_( 'COM_EASYBLOG_ITEMS' );?></span></div>
    <input type="hidden" name="location" value="<?php echo JRoute::_('index.php?option=com_easyblog&view=images&tmpl=component'); ?>&folder=<@= path @>" />
    <button type="button" class="ui-button item-button folder-view-button"><?php echo JText::_('COM_EASYBLOG_VIEW_FOLDER') ?></button>
    <button type="button" class="ui-button item-button folder-delete-button"><?php echo JText::_('COM_EASYBLOG_DELETE_FOLDER') ?></button>
</div>
</script>

<script type="text/ejs" id="EasyBlog.FileManager.Browser.ItemHandler.Image.Item">
<div class="item-summary clearfix">
    <div class="item-thumbnail">
        <span></span>
        <img src="<@= thumbnail @>" alt="<@= name @> - <@= size @>" />
    </div>
    <div class="item-title"><span class="item-filename"><@= name @></span><span class="item-filesize"><@= plupload.formatSize(size) @></span></div>
    <button type="button" class="ui-button item-button image-insert-button"><?php echo JText::_('COM_EASYBLOG_INSERT_BUTTON') ?></button>
    <button type="button" class="ui-button item-button image-delete-button"><?php echo JText::_('COM_EASYBLOG_DELETE_BUTTON') ?></button>
</div>
</script>

<script type="text/ejs" id="EasyBlog.FileManager.Browser.ItemHandler.Video.Item">
<div class="item-summary clearfix">
    <div class="item-thumbnail">
        <span></span>
        <img src="<?php echo rtrim( JURI::root() , '/' ) . '/components/com_easyblog/assets/images/media/media.png' ;?>" alt="<@= name @>" />
    </div>
    <div class="item-title"><span class="item-filename"><@= name @></span><span class="item-filesize"><@= plupload.formatSize(size) @></span></div>
    <button type="button" class="ui-button item-button video-insert-button"><?php echo JText::_('COM_EASYBLOG_INSERT_BUTTON') ?></button>
    <button type="button" class="ui-button item-button video-delete-button"><?php echo JText::_('COM_EASYBLOG_DELETE_BUTTON') ?></button>
</div>
</script>

<script type="text/ejs" id="EasyBlog.FileManager.Browser.ItemHandler.File.Item">
<div class="item-summary clearfix">
    <div class="item-thumbnail">
        <span></span>
        <img src="<@= thumbUrl @>" alt="<@= name @> - <@= size @>" />
    </div>
    <div class="item-title"><span class="item-filename"><@= name @></span><span class="item-filesize"><@= plupload.formatSize(size) @></span></div>
    <button type="button" class="ui-button item-button file-insert-button"><?php echo JText::_('COM_EASYBLOG_INSERT_BUTTON') ?></button>
    <button type="button" class="ui-button item-button file-delete-button"><?php echo JText::_('COM_EASYBLOG_DELETE_BUTTON') ?></button>
</div>
</script>

<script type="text/ejs" id="EasyBlog.FileManager.Browser.PreviewContainer">
<div class="preview-container preview-type-<@= item.type @>"></div>
</script>

<script type="text/ejs" id="EasyBlog.FileManager.Browser.ItemHandler.Upload.Item">
<div class="item-summary clearfix" id="<@= id @>" class="upload-item">
    <div class="item-thumbnail"></div>
    <div class="item-title"><@= name @> <span class="item-filesize">0%</span></div>

    <button type="button" class="ui-button item-button upload-remove-button"><?php echo JText::_('COM_EASYBLOG_REMOVE') ?></button>
    <button type="button" class="ui-button item-button upload-view-button"><?php echo JText::_('COM_EASYBLOG_VIEW_FILE') ?></button>
</div>
</script>

<script type="text/ejs" id="EasyBlog.FileManager.Browser.PreviewHandler.Upload.Item">
<div class="item-preview">
    <div class="upload-state"></div>
    <div class="upload-progress">
        <div class="in">
            <div class="upload-progress-bar"></div>
            <div class="upload-progress-value">0%</div>
        </div>
    </div>
    <ul class="upload-log reset-ul list-full"></ul>
</div>

<div class="item-properties">
    <div class="clearfix">
        <button type="button" class="button-head silver tight float-l upload-remove-button"><?php echo JText::_('COM_EASYBLOG_REMOVE') ?></button>
        <button type="button" class="button-head tight float-r upload-view-button"><?php echo JText::_('COM_EASYBLOG_VIEW_FILE') ?></button>
    </div>
</div>
</script>

<script type="text/ejs" id="EasyBlog.FileManager.Browser.PreviewHandler.Upload.LogItem">
<li>
    <span class="ts"><@= timestamp @></span>
    <div><@= message @></div>
</li>
</script>

<script type="text/ejs" id="EasyBlog.FileManager.Browser.PreviewHandler.Folder.Editor">
<div class="item-preview">
    <div style="margin:5px"><img src="<?php echo JURI::root();?>components/com_easyblog/assets/images/media/folder_preview.png" width="64" height="64"/></div>
    <div style="text-align:center;display:inline-block;font-size:10px;font-weight:700;background:#ddd;padding:2px 10px;border-radius:3px;-moz-border-radius:3px;-webkit-border-radius:3px"><@= name @></div>
    <input type="hidden" name="path" value="<@= path @>" />
</div>

<div class="item-properties">
    <table width="100%" cellpadding="0" cellspacing="0" class="reset-table">
    	<tr>
    		<td><?php echo JText::_( 'COM_EASYBLOG_TOTAL_PHOTOS' );?>:</td>
    		<td width="70%" style="font-weight:700"><@= gallery.length @> <?php echo JText::_( 'COM_EASYBLOG_PHOTOS' );?></td>
    	</tr>
    	<tr>
    		<td><?php echo JText::_( 'COM_EASYBLOG_CREATED_ON' );?>:</td>
    		<td width="70%" style="font-weight:700"><@= created @></td>
    	</tr>
    	<tr>
    		<td><?php echo JText::_( 'COM_EASYBLOG_SIZE' );?>:</td>
    		<td width="70%" style="font-weight:700"><@= size @></td>
    	</tr>
    </table>

	<div class="folder-gallery">
		<@ if( gallery.length >= 1 ){ @>
			<@ $.each( gallery , function(i , source ){ @>
			<img src="<@= source @>" width="48" height="48" />
			<@ });@>
		<@ } else { @>
        <div>
        	<?php echo JText::_( 'COM_EASYBLOG_NO_IMAGES_IN_FOLDER' ); ?>
        </div>
        <@ } @>
	</div>


    <div class="clearfix mtm">
    	<button type="button" class="button-head silver tight float-l delete-button"><?php echo JText::_('COM_EASYBLOG_DELETE_FOLDER') ?></button>
    	<@ if( gallery.length >= 1 ){ @>
        <button type="button" class="button-head tight float-r gallery-insert-button"><?php echo JText::_('COM_EASYBLOG_INSERT_GALLERY') ?></button>
        <@ } @>
        <div class="message-notification"></div>
    </div>
</div>
</script>

<script type="text/ejs" id="EasyBlog.FileManager.Browser.PreviewHandler.File.Editor">
<div class="item-preview">
    <img src="<@= preview @>" alt="<@= name @> - <@= size @>" width="256" height="256" />
</div>

<div class="item-properties">
    <ul class="reset-ul list-form">
    	<li>
			<label for="file-title"><?php echo JText::_('COM_EASYBLOG_IMAGE_MANAGER_TITLE') ?></label>
            <div><input type="text" id="file-title" value="<@= name @>" style="width: 300px;"/></div>
		</li>
		<li>
			<label for="file-created"><?php echo JText::_( 'COM_EASYBLOG_CREATED_ON' );?></label>
			<div><@= created @></div>
		</li>
		<li>
			<label for="file-created"><?php echo JText::_( 'COM_EASYBLOG_SIZE' );?></label>
			<div><@= size @></div>
		</li>
    </ul>
    <div class="clearfix mtm">
        <?php if ($this->acl->rules->upload_image): ?>
        	<button type="button" class="button-head silver tight float-l file-delete-button"><?php echo JText::_('COM_EASYBLOG_DELETE_BUTTON') ?></button>
        <?php endif; ?>
        <button type="button" class="button-head tight float-r file-insert-button"><?php echo JText::_('COM_EASYBLOG_INSERT_BUTTON') ?></button>
    </div>
</div>
</script>

<script type="text/ejs" id="EasyBlog.FileManager.Browser.PreviewHandler.Video.Editor">
<div class="item-preview">
    <@= preview @>
</div>

<div class="item-properties">
    <ul class="reset-ul list-form">
        <li>
            <label for="video-title"><?php echo JText::_('COM_EASYBLOG_IMAGE_MANAGER_TITLE') ?></label>
            <div><input type="text" id="video-title" value="<@= name @>" class="input text" /></div>
        </li>
        <li>
            <label for="video-autoplay"><?php echo JText::_('COM_EASYBLOG_IMAGE_MANAGER_AUTOPLAY') ?></label>
            <div>
            	<select name="autoplay" id="video-autoplay">
            		<option value="0"><?php echo JText::_( 'COM_EASYBLOG_NO' ); ?></option>
					<option value="1"><?php echo JText::_( 'COM_EASYBLOG_YES' ); ?></option>
            	</select>
			</div>
        </li>
        <li>
            <label for="video-size"><?php echo JText::_('COM_EASYBLOG_IMAGE_MANAGER_DIMENSIONS') ?></label>
            <div>
                <span class="video-size">
                    <label for="video-width"><?php echo JText::_('COM_EASYBLOG_IMAGE_MANAGER_WIDTH') ?></label>
                    <input type="text" id="video-width" value="<@= width @>" class="input text" />
                </span>
                <span class="video-size">
                    <label for="video-height"><?php echo JText::_('COM_EASYBLOG_IMAGE_MANAGER_HEIGHT') ?></label>
                    <input type="text" id="video-height" value="<@= height @>"  class="input text" />
                </span>
            </div>
        </li>
    </ul>
    <div class="clearfix mtm">
        <?php if ($this->acl->rules->upload_image): ?>
        <button type="button" class="button-head silver tight float-l video-delete-button"><?php echo JText::_('COM_EASYBLOG_DELETE_BUTTON') ?></button>
        <?php endif; ?>
        <button type="button" class="button-head tight float-r video-insert-button"><?php echo JText::_('COM_EASYBLOG_INSERT_BUTTON') ?></button>
    </div>
</div>
</script>

<script type="text/ejs" id="EasyBlog.FileManager.Browser.PreviewHandler.Image.Editor">
<div class="item-preview">
    <img src="<@= url @>" alt="<@= name @> - <@= size @>" />
</div>

<div class="item-properties">
    <ul class="reset-ul list-form">
        <li>
            <label for="image-url"><?php echo JText::_( 'COM_EASYBLOG_IMAGE_MANAGER_URL' );?></label>
            <div><input type="text" id="image-url" value="<@= url @>" readonly="readonly" class="input text readonly"/></div>
                <div>
                    <input type="checkbox" id="image-thumbnail" checked="checked" />
                    <label for="image-thumbnail"><?php echo JText::_('COM_EASYBLOG_MEDIAMANAGER_USE_THUMBNAIL') ?></label>
                </div>
        </li>
        <li>
            <label for="image-title"><?php echo JText::_('COM_EASYBLOG_IMAGE_MANAGER_TITLE') ?></label>
            <div><input type="text" id="image-title" value="<@= name @>" class="input text" /></div>
        </li>
        <?php if( $system->config->get( 'main_image_usersettings' ) ){ ?>
        <li>
            <label for="image-size"><?php echo JText::_('COM_EASYBLOG_IMAGE_MANAGER_DIMENSIONS') ?></label>
            <div>
                <span class="image-size">
                    <label for="image-width"><?php echo JText::_('COM_EASYBLOG_IMAGE_MANAGER_WIDTH') ?></label>
                        <input type="text" id="image-width" value="<@= thumbwidth @>" class="input text" />
                </span>
                <span class="image-size">
                    <label for="image-height"><?php echo JText::_('COM_EASYBLOG_IMAGE_MANAGER_HEIGHT') ?></label>
                        <input type="text" id="image-height" value="<@= thumbheight @>"  class="input text" />
                </span>
                <span class="image-size">
                    <label for="image-height">&nbsp;</label>
                    <input type="checkbox" id="image-aspect-ratio" checked="checked" /> <?php echo JText::_( 'COM_EASYBLOG_IMAGE_MANAGER_LOCK_RATIO' );?>
                </span>
            </div>
        </li>
        <li>
            <label for="image-align"><?php echo JText::_('COM_EASYBLOG_IMAGE_MANAGER_ALIGNMENT') ?></label>
            <div>
                <select size="1" id="image-align" class="input select">
                    <option value=""<?php echo $system->config->get( 'main_image_alignment' ) == 'none' ? ' selected="selected"' : '';?>><?php echo JText::_('COM_EASYBLOG_IMAGE_MANAGER_NOT_SET_OPTION') ?></option>
                    <option value="left"<?php echo $system->config->get( 'main_image_alignment' ) == 'left' ? ' selected="selected"' : '';?>><?php echo JText::_('COM_EASYBLOG_IMAGE_MANAGER_LEFT_OPTION') ?></option>
                    <option value="right"<?php echo $system->config->get( 'main_image_alignment' ) == 'right' ? ' selected="selected"' : '';?>><?php echo JText::_('COM_EASYBLOG_IMAGE_MANAGER_RIGHT_OPTION') ?></option>
                    <option value="center"<?php echo $system->config->get( 'main_image_alignment' ) == 'center' ? ' selected="selected"' : '';?>><?php echo JText::_('COM_EASYBLOG_IMAGE_MANAGER_CENTER_OPTION') ?></option>
                </select>
            </div>
        </li>
        <?php } else { ?>
        	<input type="hidden" id="image-align" value="<?php echo $system->config->get( 'main_image_alignment' ); ?>" />
        <?php } ?>
        <li>
            <div>
                <input type="checkbox" id="image-featured" />
                <label for="image-featured"><?php echo JText::_('COM_EASYBLOG_IMAGE_MANAGER_USE_AS_FEATURED') ?></label>
            </div>
        </li>
    </ul>

    <div class="clearfix mtm">
        <?php if ($this->acl->rules->upload_image): ?>
        <button type="button" class="button-head silver tight float-l image-delete-button"><?php echo JText::_('COM_EASYBLOG_DELETE_BUTTON') ?></button>
        <?php endif; ?>
        <button type="button" class="button-head tight float-r image-insert-button"><?php echo JText::_('COM_EASYBLOG_INSERT_BUTTON') ?></button>
    </div>
</div>
</script>

<script type="text/ejs" id="EasyBlog.FileManager.Browser.PreviewHandler.Folder.Gallery">
	<p>&nbsp;</p>
	<input class="easyblog-gallery" type="hidden" name="easyblog-gallery" value="<@= obj @>" />
    <div class="easyblog-placeholder-gallery" style="border: 1px dashed #666;color: #fff;width: 80px;text-align:center;background: #333333; width: 250px;height: 150px;">
        <span style="margin-top: 65px;font-weight:700;width:100%;font-size: 20px;display:block;"><?php echo JText::_( 'Gallery:' );?> <@= name @></span>
    </div>
    <p>&nbsp;</p>
</script>

<script type="text/ejs" id="EasyBlog.FileManager.Browser.PreviewHandler.Video.Placeholder">
	<p>&nbsp;</p>
	<input class="easyblog-video" type="hidden" name="easyblog-video" value="<@= obj @>" />
	<div class="easyblog-placeholder-video" style="background:#fff; border:1px solid #ddd; text-align:center; width: <@= parseInt( width ) + 2 @>px; height: <@= parseInt( height ) + 2 @>px;">
        <div style="width: <@= parseInt( width ) @>px;height: <@= parseInt( height ) @>px; border:1px solid #fff; background:#f4f4f4;">
            <span style="margin-top: <@= parseInt( height / 2 ) - 10 @>px; font-weight: 700; width: 100%; font-size: 10px; font-family: Tahoma; color: #333; display: block;">
                <@=name@> [ <@= parseInt( width ) @> x <@= parseInt( height ) @> ]
            </span>
        </div>
    </div>
    <p>&nbsp;</p>
</script>

<?php
$str = ob_get_contents();
ob_end_clean();

$revert = array('%21'=>'!', '%2A'=>'*', '%27'=>"'", '%28'=>'(', '%29'=>')');
$ejsTmpl = strtr(rawurlencode($str), $revert);
?>

<script type="text/javascript">
sQuery(decodeURIComponent('<?php echo addslashes( $ejsTmpl ); ?>')).appendTo('body');
</script>


<?php if( stristr( JRequest::getURI() , '/administrator' ) !== false ){ ?>
</div>
<?php } ?>