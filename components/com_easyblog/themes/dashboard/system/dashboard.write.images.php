<?php
/**
 * @package		EasyBlog
 * @copyright	Copyright (C) 2010 Stack Ideas Private Limited. All rights reserved.
 * @license		GNU/GPL, see LICENSE.php
 *
 * EasyBlog is free software. This version may have been modified pursuant
 * to the GNU General Public License, and as distributed it includes or
 * is derivative of works licensed under the GNU General Public License or
 * other free or open source software licenses.
 * See COPYRIGHT.php for copyright notices and details.
 */

defined('_JEXEC') or die('Restricted access');

if( !$useImageManager )
{
	return;
}
?>

<script type="text/javascript">
(function($)
{
	eblog.fileManager = {

		isResizing: false,

		isReady: false,

		stayOnTop: false,

		settings: {
			url: '<?php echo EasyBlogRouter::_('index.php?option=com_easyblog&view=images&tmpl=component&e_name=' . $editorName . '&blogger_id=' . $blogger_id, false); ?>',
			height: 550,
			appHeight: 514, /* TODO: Recalculate */
			collapsedHeight: 88
		},

		init: function()
		{
			var fileManager = eblog.fileManager;

			fileManager.element = $('#eblog-filemanager');

			if (!fileManager.element.parent().is('body'))
			{
				fileManager.element = fileManager.element.appendTo('body');
			}

			fileManager.app = $('.filemanager-app');
			fileManager.header = $('.filemanager-header');
			fileManager.stayOnTopCheckbox = $('#filemanager-stayontop');
			fileManager.dock = $('#filemanager-dock');

			fileManager.element.css('overflow', 'hidden');

			var show = true;
			if (fileManager.element.hasClass('dock'))
			{
				if (fileManager.element.hasClass('active'))
				{
					fileManager.element.css('height', 0).removeClass('active');
					fileManager.dock
							.css(
							{
								height: 0
							});
					show = false;					
				} else {
					fileManager.element
						.addClass('active')
						.css(
						{
							height: fileManager.settings.height
						});

					fileManager.dock
						.css(
						{
							height: fileManager.element.outerHeight(true)
						});
					show = false;
				}

			} else {
				fileManager.setLayout();	
			}

			$(window)
				.unbind('resize.fileManager')
				.bind('resize.fileManager', function()
				{
					if (!fileManager.element.hasClass('dock'))
					{
						fileManager.setLayout(true);	
					}
				})
				.bind('keyup.fileManager', function(event)
				{
					if (event.keyCode==27 && !fileManager.element.hasClass('dock'))
					{
						return (event.shiftKey) ? fileManager.show() : fileManager.hide();
					}	
				});
			
			fileManager.stayOnTopCheckbox
				.change((function()
				{
					fileManager.stayOnTop = fileManager.stayOnTopCheckbox.is(':checked');
					return arguments.callee;
				})());

			if (!fileManager.isReady) fileManager.load();


			if (fileManager.element.hasClass('dock') && fileManager.element.hasClass('active'))
			{
				fileManager.setDockLayout();
			}
			
			if (show) fileManager.show();
		},

		setLayout: function(fast)
		{
			var fileManager = eblog.fileManager;
			var dashboard = $('#eblog-wrapper');

			var active = fileManager.element.hasClass('active');

			if (fast)
			{
				fileManager.element
					.css(
					{
						top    : (active) ? 0 : fileManager.settings.height * -1,
						left   : dashboard.offset().left
					});
			} else {

				var dashboardWidth = dashboard.width() - 2 /* 2px border */;
				var minWidth = 600;
				var width = (dashboardWidth > minWidth) ? dashboardWidth : minWidth;

				fileManager.element
					.toggleClass('eb-loading', fileManager.isReady)
					.css(
					{
						width  : width,
						height : fileManager.settings.height,
						top    : (active) ? 0 : fileManager.settings.height * -1,
						left   : dashboard.offset().left
					});
			}
		},

		load: function()
		{
			var fileManager = eblog.fileManager;

			// Double check
			if (fileManager.element.find('.filemanager-app').length > 0) return;

			fileManager.element
				.addClass('eb-loading');

			fileManager.app = $('<iframe class="filemanager-app" frameborder="0">');

			fileManager.app
				.css(
				{
					width: '100%',
					height: fileManager.settings.appHeight,
					opacity: 0
				})
				.appendTo(fileManager.element)
				.one('load', function()
				{
					fileManager.ready();
				})
				.attr('src', fileManager.settings.url);
		},

		ready: function()
		{
			var fileManager = eblog.fileManager;

			fileManager.element
				.removeClass('eb-loading');
			
			fileManager.app
				.stop()
				.animate({opacity: 1});
			
			fileManager.element
				.bind('mouseover.fileManager', fileManager.expand)
				.bind('mouseout.fileManager', fileManager.collapse);
		},

		show: function()
		{
			var fileManager = eblog.fileManager;

			try { IeCursorFix(); } catch(e) {};

			fileManager.isResizing = true;

			fileManager.element
				.stop()
				.animate(
				{
					top: 0
				},
				function()
				{
					fileManager.element.addClass('active');

					fileManager.isResizing = false;
				});
		},

		hide: function()
		{

			var fileManager = eblog.fileManager;
					
			if (fileManager.element.hasClass('dock'))
			{
				fileManager.init();
				return;
			}

			fileManager.isResizing = true;

			fileManager.element
				.stop()
				.animate(
				{
					top: '-' + (fileManager.element.outerHeight(true) + 50) + 'px'
				},
				function()
				{
					fileManager.element.removeClass('active');

					fileManager.isResizing = false;
				});			
		},

		expandMonitor: null,
		collapseMonitor: null,

		expand: function()
		{
			var fileManager = eblog.fileManager;

			if (fileManager.isResizing || fileManager.stayOnTop || fileManager.element.hasClass('dock')) return;

			clearTimeout(fileManager.collapseMonitor);

			clearTimeout(fileManager.expandMonitor);

			fileManager.expandMonitor = setTimeout(function()
			{
				if (fileManager.isResizing) return;
				
				fileManager.element
					.stop()
					.animate({
						height: fileManager.settings.height
					}, 'fast');				
			}, 500);
		},

		collapse: function(delay)
		{
			if (delay===undefined || typeof delay=='object')
				delay = 800;
			
			var fileManager = eblog.fileManager;

			if (fileManager.isResizing || fileManager.stayOnTop || fileManager.element.hasClass('dock')) return;

			clearTimeout(fileManager.collapseMonitor);

			clearTimeout(fileManager.expandMonitor);

			fileManager.collapseMonitor = setTimeout(function()
			{
				if (fileManager.isResizing) return;

				fileManager.element
					.stop()
					.animate({
						height: fileManager.settings.collapsedHeight
					}, 'fast');					
			}, delay);
		},

		toggleDock: function()
		{

			var fileManager = eblog.fileManager;

			if (fileManager.element.hasClass('dock'))
			{
				fileManager.element.removeClass('dock');
				$('.filemanager-header .toggleDock').removeClass('docked');

				fileManager.setLayout();

				fileManager.dock
					.css(
					{
						height: 0
					});
			} else {
				fileManager.element.addClass('dock');
				
				$('.filemanager-header .toggleDock').addClass('docked');

				fileManager.element
					.css(
					{
						width: $('.write-posteditor').width(),
						height: fileManager.settings.height,
						top: fileManager.dock.offset().top,
						left: fileManager.dock.offset().left
					});

				fileManager.dock
					.css(
					{
						height: fileManager.element.outerHeight(true)
					});
			}
		},

		setDockLayout: function()
		{
			var fileManager = eblog.fileManager;

			if (fileManager.element.hasClass('dock') && fileManager.element.hasClass('active'))
			{
				fileManager.element
					.css(
					{
						width: $('.write-posteditor').width(),
						height: fileManager.settings.height,
						top: fileManager.dock.offset().top,
						left: fileManager.dock.offset().left
					});

				fileManager.dock
					.css(
					{
						height: fileManager.element.outerHeight(true)
					});				
			}
		}

	};

})(sQuery);

</script>

<div id="eblog-filemanager">
    <div class="filemanager-header"><h3><?php echo JText::_('COM_EASYBLOG_IMAGE_MANAGER_DIALOG_TITLE'); ?></h3><span class="stay-on-top"><input id="filemanager-stayontop" type="checkbox" name="filemanager-stayontop"/><label for="filemanager-stayontop"><?php echo JText::_('COM_EASYBLOG_STAY_ON_TOP'); ?></label></span><a href="javascript:void(0);" onclick="eblog.fileManager.toggleDock();" class="toggleDock">Toggle Dock</a><a href="javascript:void(0);" onclick="eblog.fileManager.hide();" class="closeme">Close</a></div>
</div>

<a href="javascript:void(0);" onclick="eblog.fileManager.init();" class="ico-dimage float-l prel mrs">
	<b class="ir"><?php echo JText::_( 'COM_EASYBLOG_DASHBOARD_WRITE_INSERT_MEDIA' );?></b>
	<span class="ui-toolnote">
		<i></i>
		<b><?php echo JText::_( 'COM_EASYBLOG_DASHBOARD_WRITE_INSERT_MEDIA' );?></b>
		<span><?php echo JText::_('COM_EASYBLOG_DASHBOARD_WRITE_INSERT_MEDIA_TIPS'); ?></span>
	</span>
</a>
<i></i>
