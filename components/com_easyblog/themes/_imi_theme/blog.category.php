<?php
/**
 * @package		EasyBlog
 * @copyright	Copyright (C) 2010 Stack Ideas Private Limited. All rights reserved.
 * @license		GNU/GPL, see LICENSE.php
 *  
 * EasyBlog is free software. This version may have been modified pursuant
 * to the GNU General Public License, and as distributed it includes or
 * is derivative of works licensed under the GNU General Public License or
 * other free or open source software licenses.
 * See COPYRIGHT.php for copyright notices and details.
 */

defined('_JEXEC') or die('Restricted access');
?>

<div id="ezblog-body">
    <div id="ezblog-detail" class="forCategory mtl">
    	<div class="profile-head clearfix">
    	    <?php if($config->get('layout_categoryavatar', true)) : ?>
    	    <div class="profile-avatar float-l prel">
                <i class="pabs"></i>
                <img src="<?php echo $category->getAvatar();?>" align="top" width="80" height="80" class="avatar" />
            </div>
    	    <?php endif; ?>

    	    <div class="profile-info">
    			<h3 class="profile-title rip mbm"><?php echo JText::_( $category->title ); ?></h3>
                <?php if ($category->get( 'description' ) ) : ?>
    			<div class="profile-info">
    				<?php echo $category->get( 'description' ); ?>
    			</div>
                <?php endif; ?>
    			<div class="profile-connect">
                    <ul class="connect-links reset-ul float-li clearfix">
    			    <?php if($config->get('main_categorysubscription')) { ?>
					<li>
                        <a href="javascript:eblog.subscription.show( '<?php echo EBLOG_SUBSCRIPTION_CATEGORY; ?>' , '<?php echo $category->id;?>');" title="<?php echo JText::_('COM_EASYBLOG_SUBSCRIPTION_SUBSCRIBE_CATEGORY'); ?>" class="link-subscribe">
                            <span><?php echo JText::_('COM_EASYBLOG_SUBSCRIPTION_SUBSCRIBE_CATEGORY'); ?></span>
                        </a>
                    </li>
    				<?php } ?>

        			<?php if( (!$category->private) || ($category->private && $system->my->id != 0 ) ) : ?>
        				<?php if( $config->get('main_rss') ){ ?>
                        <li>
        			    	<a href="<?php echo $category->getRSS();?>" title="<?php echo JText::_('COM_EASYBLOG_SUBSCRIBE_FEEDS'); ?>" class="link-rss">
                                <span><?php echo JText::_('COM_EASYBLOG_SUBSCRIBE_FEEDS'); ?></span>
                            </a>
                        </li>
        			    <?php } ?>
        		    <?php endif; ?>

        		        <li><span class="total-post"><?php echo $this->getNouns( 'COM_EASYBLOG_CATEGORIES_COUNT' , $category->cnt , true ); ?></span></li>
                    </ul>
    		    </div>
    		    <?php if(! empty($category->nestedLink)) { ?>
    		    <div class="blogger-child small ptm mtm" style="border-top:1px dotted #ccc">
    		        <span><?php echo JText::_( 'COM_EASYBLOG_CATEGORIES_SUBCATEGORIES' ); ?></span>
    		    	<?php echo $category->nestedLink; ?>
    		    </div>
    		    <?php } ?>
    	    </div><!--end: .profile-info-->
            <div class="clear"></div>
    	</div><!--end: .profile-head-->
    </div>

    <?php if ( $teamBlogCount > 0) { ?>
	<div class="eblog-message info"><?php echo $this->getNouns( 'COM_EASYBLOG_CATEGORIES_LISTINGS_TEAMBLOG_COUNT' , $teamBlogCount , true ); ?></div>
	<?php } ?>

    <div id="ezblog-posts" class="forCategory">
    <?php if($system->my->id == 0 && $category->private == 1 ) : ?>
        <div class="eblog-message warning"><?php echo JText::_('COM_EASYBLOG_CATEGORIES_FOR_REGISTERED_USERS_ONLY');?></div>
    <?php else : ?>

    	<?php if( $blogs ) { ?>
    		<?php
    		foreach ( $blogs as $row )
    		{
    			$this->set( 'data' , array( $row ) );
    			echo $this->fetch( 'blog.item.php' );
    		}
    		?>
    	<?php } else { ?>
    	<div class="eblog-message warning"><?php echo JText::_('COM_EASYBLOG_NO_BLOG_ENTRY');?></div>
    	<?php } ?>

    	<?php if ( $pagination ) : ?>
    	<div class="pagination">
    		<?php echo $pagination; ?>
    	</div>
    	<?php endif; ?>
    <?php endif; ?>
    </div>

</div><!--end: #ezblog-body-->