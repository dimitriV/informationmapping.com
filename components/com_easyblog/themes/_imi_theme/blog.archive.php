<?php
/**
 * @package		EasyBlog
 * @copyright	Copyright (C) 2010 Stack Ideas Private Limited. All rights reserved.
 * @license		GNU/GPL, see LICENSE.php
 *
 * EasyBlog is free software. This version may have been modified pursuant
 * to the GNU General Public License, and as distributed it includes or
 * is derivative of works licensed under the GNU General Public License or
 * other free or open source software licenses.
 * See COPYRIGHT.php for copyright notices and details.
 */

defined('_JEXEC') or die('Restricted access');
?>
<div id="ezblog-body">
    <div class="eblog-message info"><?php echo $archiveTitle; ?></div>
 <ul class="archive-list reset-ul">
    <?php
	if( $data )
	{
		foreach ( $data as $row )
		{
			$isMineBlog = EasyBlogHelper::isMineBlog($row->created_by, $my->id);

			$team   = '';
		    if( isset($row->team_id) )
		    {
				$team	= ( !empty( $row->team_id )) ? '&team=' . $row->team_id : '';
		    }

            $blogger    =& EasyBlogHelper::getTable( 'Profile', 'Table');
            $blogger->load( $row->created_by );
    ?>

			<li id="entry_<?php echo $row->id; ?>" class="post-wrapper <?php echo (!empty($team)) ? 'teamblog-post' : '' ;?>">
                <h3 class="rip blog-title">
    				<a href="<?php echo EasyBlogRouter::_('index.php?option=com_easyblog&view=entry'.$team.'&id='.$row->id); ?>" class="fsx fwb"><?php echo $row->title; ?></a>
                    <?php if( $row->isFeatured ) { ?><sup class="tag-featured"><?php echo Jtext::_('COM_EASYBLOG_FEATURED_FEATURED'); ?></sup><?php } ?>
                </h3>
                <div class="blog-meta mbl">
					<div class="in">
						<?php echo JText::_( 'COM_EASYBLOG_POSTED' );?> 
	                    <?php echo JText::sprintf( 'COM_EASYBLOG_IN' , EasyBlogRouter::_('index.php?option=com_easyblog&view=categories&layout=listings&id='.$row->category_id), $row->category); ?>
						<span><?php echo $this->formatDate( $system->config->get('layout_dateformat') , $row->created ); ?></span>
					</div>
				</div>
				<div><?php echo (JString::strlen($row->text) > 150) ? JString::substr(strip_tags($row->text), 0, 150) . '...' : strip_tags($row->text) ; ?></div>
				<?php if( $row->readmore ) { ?>
				<div class="blog-more-button mtl mbm">
					<a href="<?php echo EasyBlogRouter::_('index.php?option=com_easyblog&view=entry'.$team.'&id='.$row->id); ?>" class="blog-more-button">
						<span><?php echo JText::_('COM_EASYBLOG_CONTINUE_READING'); ?></span>
					</a>
				</div>
				<?php } ?>
                <div class="small">
                	<?php if( $system->config->get( 'layout_hits' ) ): ?>
						<span class="blog-hit"><?php echo JText::sprintf( 'COM_EASYBLOG_HITS_TOTAL' , $row->hits ); ?></span>
					<?php endif; ?>
                    <?php
    				if( $system->config->get('main_comment') && $row->totalComments !== false )
    				{
    				?>
                    <span class="archive-blog-comments"><a href="<?php echo EasyBlogRouter::_('index.php?option=com_easyblog&view=entry'.$team.'&id='.$row->id); ?>#comments"><?php echo $this->getNouns( 'COM_EASYBLOG_COMMENT_COUNT' , $row->totalComments , true ); ?></a></span>
    				<?php
    				}
    				?>
                </div>
			</li>
    <?php
    	}
    }
    else
    {
    ?>
     <div class="eblog-message info mtm"><?php echo $emptyPostMsg;?></div>
    <?php } ?>
    </ul>
    <?php if ( $pagination ) : ?>
    <div class="pagination clearfix">
        <?php echo $pagination; ?>
    </div>
    <?php endif; ?>
</div>
