<?php
/**
 * @package		EasyBlog
 * @copyright	Copyright (C) 2010 Stack Ideas Private Limited. All rights reserved.
 * @license		GNU/GPL, see LICENSE.php
 *
 * EasyBlog is free software. This version may have been modified pursuant
 * to the GNU General Public License, and as distributed it includes or
 * is derivative of works licensed under the GNU General Public License or
 * other free or open source software licenses.
 * See COPYRIGHT.php for copyright notices and details.
 */

defined('_JEXEC') or die('Restricted access');
?>
<li>
	<h3 class="blog-title rip">
		<a href="<?php echo EasyBlogRouter::_('index.php?option=com_easyblog&view=entry&id=' . $entry->id ); ?>"><?php echo $entry->title; ?></a>
		<?php if( $entry->isFeatured ) { ?><sup class="tag-featured"><?php echo Jtext::_('COM_EASYBLOG_FEATURED_FEATURED'); ?></sup><?php } ?>
	</h3>
	<div class="blog-meta fsm mts">
		<div class="in">
		<?php echo JText::_( 'COM_EASYBLOG_POSTED_ON' ); ?>
		<?php echo $this->formatDate( $system->config->get( 'layout_dateformat' ), $entry->created ); ?>

		<?php if( $system->config->get('main_comment') && $entry->totalComments !== false ){ ?>
		-
		<span class="post-comments">
			<a href="<?php echo EasyBlogRouter::_('index.php?option=com_easyblog&view=entry&id='.$entry->id); ?>#comments">
				<?php echo $this->getNouns( 'COM_EASYBLOG_COMMENT_COUNT' , $entry->totalComments , true );?>
			</a>
		</span>
		<?php } ?>
		</div>
	</div>
	<div class="mts">
		<?php echo JString::substr( strip_tags( $entry->text ) , 0 , 350 ); ?> ...
	</div>
</li>
