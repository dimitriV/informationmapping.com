<?php
/**
 * @package		EasyBlog
 * @copyright	Copyright (C) 2010 Stack Ideas Private Limited. All rights reserved.
 * @license		GNU/GPL, see LICENSE.php
 *
 * EasyBlog is free software. This version may have been modified pursuant
 * to the GNU General Public License, and as distributed it includes or
 * is derivative of works licensed under the GNU General Public License or
 * other free or open source software licenses.
 * See COPYRIGHT.php for copyright notices and details.
 */
defined('_JEXEC') or die('Restricted access');


$team   	= ( isset( $teamId) && ! empty($teamId)) ? '&team=' . $teamId : '';
$socialPosition = $system->config->get('main_socialbutton_position');

foreach($data as $row)
{
	$showAvatar	= isset( $showAvatar ) ? $showAvatar : true;
	$isMineBlog = EasyBlogHelper::isMineBlog($row->created_by, $system->my->id);

	$blogger	=& EasyBlogHelper::getTable( 'Profile', 'Table');
	$blogger->load( $row->created_by );

	$loadratings		= true;
	$loadsocialbutton 	= true;
	$loadtags 			= false;
	$loadfblike 		= true;
	$loadcomments		= true;

	if($system->config->get('main_password_protect', true) && !empty($row->blogpassword))
	{
		if(!EasyBlogHelper::verifyBlogPassword($row->blogpassword, $row->id))
		{
			$loadratings		= false;
			$loadsocialbutton 	= false;
			$loadtags 			= false;
			$loadfblike 		= false;
			$loadcomments		= false;
		}
	}

	if( empty( $team ) )
	{
		// if team is empty, then we cehck against the team_id from query.
		if( isset($row->team_id) )
		{
			$team	= ( !empty( $row->team_id )) ? '&team=' . $row->team_id : '';
		}
	}
?>
<div id="entry_<?php echo $row->id; ?>" class="blog-post clearfix prel <?php echo (!empty($team)) ? 'teamblog-post' : '' ;?>">
<div class="blog-post-in">

	<?php
	/**
	* ----------------------------------------------------------------------------------------------------------
	* Site Admin Control
	* ----------------------------------------------------------------------------------------------------------
	*/
	?>
	<?php if( $system->admin || $isMineBlog ) : ?>
	<div class="post-admin-menu pabs gradA">
		<span class="icon-menu"></span>
		<ul class="reset-ul float-li float-l pabs admin_menu">
			<?php if( $system->admin || ($isMineBlog && $acl->rules->delete_entry)) : ?>
			<?php if( $system->admin ) : ?>
			<li class="featured_add" <?php echo ($row->isFeatured) ? 'style="display:none;"' : '';?> >
				<a href="javascript:eblog.featured.add('post','<?php echo $row->id;?>');">
					<?php echo Jtext::_('COM_EASYBLOG_FEATURED_FEATURE_THIS'); ?>
				</a>
			</li>
			<li class="featured_remove" <?php echo ($row->isFeatured) ? '' : 'style="display:none;"';?> >
				<a href="javascript:eblog.featured.remove('post','<?php echo $row->id;?>');">
					<?php echo Jtext::_('COM_EASYBLOG_FEATURED_FEATURE_REMOVE'); ?>
				</a>
			</li>
			<?php endif; ?>
			<li class="edit"><a href="<?php echo EasyBlogRouter::_('index.php?option=com_easyblog&view=dashboard&layout=write&blogid='.$row->id);?><?php echo ($system->config->get( 'layout_dashboardanchor' ) ) ? '#write-entry' : '';?>"><?php echo Jtext::_('COM_EASYBLOG_ADMIN_EDIT_ENTRY'); ?></a></li>
			<li class="delete">
				<a href="javascript:eblog.blog.confirmDelete( '<?php echo $row->id;?>' , '<?php echo $currentURL;?>' );"><?php echo Jtext::_('COM_EASYBLOG_ADMIN_DELETE_ENTRY'); ?></a>
			</li>
			<li class="unpublish">
				<a href="<?php echo EasyBlogRouter::_('index.php?option=com_easyblog&controller=dashboard&from=eblog&task=toggleBlogStatus&status=0&blogId=' . $row->id );?>"><?php echo Jtext::_('COM_EASYBLOG_ADMIN_UNPUBLISH_ENTRY'); ?></a>
			</li>
			<?php endif; ?>
		</ul>
	</div>
	<?php endif; ?>

	<?php
	/**
	* ----------------------------------------------------------------------------------------------------------
	* Author avatar
	* ----------------------------------------------------------------------------------------------------------
	*/
	?>
	<?php if ( $system->config->get('layout_avatar') && $showAvatar ) : ?>
	<div class="blog-avatar float-l prel">
		<?php
			if( isset( $row->team_id ) )
			{
				$teamBlog   =& EasyBlogHelper::getTable( 'TeamBlog', 'Table');
				$teamBlog->load( $row->team_id );
		?>
		<a href="<?php echo EasyBlogRouter::_('index.php?option=com_easyblog&view=teamblog&layout=listings&id=' . $teamBlog->id ); ?>" class="avatar isTeamBlog float-l prel">
			<img src="<?php echo $teamBlog->getAvatar(); ?>" alt="<?php echo $teamBlog->title; ?>" class="avatar" width="60" height="60" />
		</a>
		<a href="<?php echo $blogger->getProfileLink(); ?>" class="avatar isBlogger float-l pabs">
			<img src="<?php echo $row->avatar; ?>" alt="<?php echo $row->displayName; ?>" class="avatar" width="30" height="30" />
		</a>
		<?php
			} else {
		?>
		<a href="<?php echo $blogger->getProfileLink(); ?>" class="avatar float-l">
			<img src="<?php echo $row->avatar; ?>" alt="<?php echo $row->displayName; ?>" class="avatar isBlogger" width="60" height="60" />
		</a>
		<?php } ?>
	</div>
	<?php endif; ?>


	<div class="blog-content clearfix">
		<!-- the title -->
		<h2 id="title_<?php echo $row->id; ?>" class="blog-title<?php echo ($row->isFeatured) ? ' featured' : '';?> rip mbs">
			<?php echo $row->title; ?>
			<?php if( $row->isFeatured ) { ?>
			<sup class="tag-featured"><?php echo Jtext::_('COM_EASYBLOG_FEATURED_FEATURED'); ?></sup>
			<?php } ?>
		</h2>
		<div class="blog-meta mbl">
			<div class="in">
				<span class="blog-author">
					<?php echo JText::_( 'COM_EASYBLOG_POSTED_ON' ); ?>
					<?php echo $this->formatDate( $system->config->get('layout_dateformat') , $row->created ); ?>
				</span>
			</div>
		</div><!--end: .blog-meta-->

		<!-- joomla content plugin call -->
			<?php echo $row->event->afterDisplayTitle; ?>

		<div class="blog-text clearfix prel">

			<?php
			/**
			* ----------------------------------------------------------------------------------------------------------
			* Content Text
			* ----------------------------------------------------------------------------------------------------------
			*/
			?>
			<!-- joomla content plugin call -->
			<?php echo $row->event->beforeDisplayContent; ?>

			<?php
			if ($loadsocialbutton ) :
			// show Tweetmeme and Google Buzz if enabled
			if($socialPosition == 'top') EasyBlogHelper::showSocialButton($row , true );
			if($socialPosition == 'left' || $socialPosition == 'right') EasyBlogHelper::showSocialButton($row , true );
			endif;
			?>
			<!-- the content -->
			<?php echo $row->text; ?>

			<!-- joomla content plugin call -->
			<?php echo $row->event->afterDisplayContent; ?>

			<?php /* content copyright */ ?>
			<?php if( $system->config->get( 'layout_copyrights' ) && !empty($row->copyrights) ) { ?>
			<div class="blog-copyrights mtm">
				<div class="in clearfix">
					<span class="blog-copyrights">
						<?php echo JText::_('COM_EASYBLOG_COPYRIGHTS_PREFIX') . $row->copyrights ?>
					</span>
				</div>
			</div>
			<?php } ?>

			<?php if( $system->config->get( 'main_locations_blog_frontpage' ) ){ ?>
			<?php echo EasyBlogHelper::getHelper( 'Maps' )->getHTML( true ,
																	$row->address,
																	$row->latitude ,
																	$row->longitude ,
																	$system->config->get( 'main_locations_blog_map_width') ,
																	$system->config->get( 'main_locations_blog_map_height' ),
																	JText::sprintf( 'COM_EASYBLOG_LOCATIONS_BLOG_POSTED_FROM' , $row->address ),
																	'post_map_canvas_' . $row->id );?>
			<?php } ?>
		</div><!--end: .blog-text-->

		<?php if( $row->readmore ) { ?>
			<div class="blog-more-button mtl mbm">
				<a href="<?php echo EasyBlogRouter::_('index.php?option=com_easyblog&view=entry'.$team.'&id='.$row->id); ?>" class="blog-more-button">
					<span><?php echo JText::_('COM_EASYBLOG_CONTINUE_READING'); ?></span>
				</a>
			</div>
		<?php } ?>

		<?php
		/**
		* ----------------------------------------------------------------------------------------------------------
		* Blog tags
		* ----------------------------------------------------------------------------------------------------------
		*/
		?>
		<?php if($loadtags): ?>
		<div class="blog-taglist mtm small">
			<div class="in">
				<span class="blog-tag"><?php echo JText::sprintf('COM_EASYBLOG_TAG_LIST', $row->tags); ?></span>
			</div>
		</div>
		<?php endif; ?>

		<div class="blog-meta-bottom fsm mtm">
			<div class="in clearfix">
			<?php if( $system->config->get( 'layout_hits' ) ): ?>
				<span class="blog-hit"><?php echo JText::sprintf( 'COM_EASYBLOG_HITS_TOTAL' , $row->hits ); ?></span>
			<?php endif; ?>
			<?php if( $system->config->get('main_comment') && $row->totalComments !== false ) { ?>
				<span class="blog-comments">
					<a href="<?php echo EasyBlogRouter::_('index.php?option=com_easyblog&view=entry'.$team.'&id='.$row->id); ?>#comments"><?php echo $this->getNouns( 'COM_EASYBLOG_COMMENT_COUNT' , $row->totalComments , true );?></a>
				</span>
			<?php } ?>
			</div>
		</div>

		<?php
		/**
		* ----------------------------------------------------------------------------------------------------------
		* Blog rating
		* ----------------------------------------------------------------------------------------------------------
		*/
		?>
		<?php if($loadratings && $system->config->get( 'main_ratings_frontpage' ) ) { ?>
		<div class="blog-rating mtm">
			<?php echo EasyBlogHelper::getHelper( 'ratings' )->getHTML( $row->id , EBLOG_RATINGS_TYPE_ENTRY , JText::_( 'COM_EASYBLOG_RATINGS_RATE_BLOG_ENTRY') , 'blog-' . $row->id . '-ratings' , $system->config->get( 'main_ratings_frontpage_locked' ) ); ?>
		</div>
		<?php } ?>

		<?php
		// show Tweetmeme and Google Buzz if enabled
		if($socialPosition == 'bottom' && $loadsocialbutton)
		{
			//EasyBlogHelper::showSocialButton($row , true );
		}
		?>

		<?php if( $system->config->get('main_facebook_like') && $system->config->get('main_facebook_like_layout') == 'standard' && $loadfblike && $system->config->get( 'integrations_facebook_show_in_listing') ) : ?>
		<div class="facebook-likes mtm clearfix">
			<div id="eb-fblikes" class="align<?php echo ($this->getDirection() == 'rtl') ? 'right' : 'left'; ?>">
				<?php echo $row->facebookLike; ?>
			</div>
		</div>
		<?php endif; ?>

		<?php
		/**
		* ----------------------------------------------------------------------------------------------------------
		* Comment view in post listing
		* ----------------------------------------------------------------------------------------------------------
		*/
		?>
		<?php
		if( $system->config->get('layout_showcomment', false) && $loadcomments && EasyBlogHelper::getHelper( 'Comment')->isBuiltin() )
		{
		   $this->set( 'row' , $row );
		   $this->set( 'team' , $team );

		   echo $this->fetch( 'blog.item.comment.list.php' );
		}
		?>
	</div><!--end: .blog-content-->
</div><!--end: .blog-post-in-->

</div><!--end: .blog-post-->
<?php
}//end foreach
?>
