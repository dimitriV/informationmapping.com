<?php
/**
 * @package		EasyBlog
 * @copyright	Copyright (C) 2010 Stack Ideas Private Limited. All rights reserved.
 * @license		GNU/GPL, see LICENSE.php
 *
 * EasyBlog is free software. This version may have been modified pursuant
 * to the GNU General Public License, and as distributed it includes or
 * is derivative of works licensed under the GNU General Public License or
 * other free or open source software licenses.
 * See COPYRIGHT.php for copyright notices and details.
 */
defined('_JEXEC') or die('Restricted access');

$notice		= '';
$type		= 'info';
$counter 	= 0;
?>
<div id="section-comments" class="blog-section style-<?php echo $system->config->get( 'layout_comment_theme' );?>">
	<?php if(! $allowComment) { ?>
	<div class="eblog-message info">
	    <?php echo JText::_('COM_EASYBLOG_COMMENT_DISABLED'); ?>
	</div>
	<?php } else { ?>
		<h3 class="section-title"><span><?php echo JText::_('COM_EASYBLOG_COMMENTS'); ?></span></h3>
		<div id="blog-comment">
			<a name="comments" id="comments"> </a>
			<?php
			if ( $blogComments )
			{
				$counter	= ( count($blogComments) > 0 ) ? 1 : 0;

				foreach ($blogComments as $comment)
				{
					$created			= EasyBlogDateHelper::dateWithOffSet($comment->created);
					$indentDirection    = ( $this->getDirection() == 'rtl') ? 'right' : 'left';
				?>
			<div id="comment-<?php echo $comment->id; ?>" class="comment-row comment-style-<?php echo $counter % 2; ?> <?php echo ($comment->depth > 0) ? 'comment-replied' : ''; ?>" <?php echo ($comment->depth > 0) ? 'style="margin-'.$indentDirection.':' . ($comment->depth * 30) . 'px;"' : ''; ?> >
				<a name="comment-<?php echo $comment->id; ?>"> </a>
				<div class="comment-block clearfix">
                    <?php
                    /**
                    * ----------------------------------------------------------------------------------------------------------
                    * Comment Avatar
                    * ----------------------------------------------------------------------------------------------------------
                    */
                    ?>
					<?php if ( $config->get('layout_avatar') ) : ?>
						<?php
						    $avatarLink 	= 'javascript:void(0);';
						    $avatarTooltip  = '';
							if( $comment->poster->id != 0 ){
								$avatarLink		= $comment->poster->getProfileLink();
							    $avatarTooltip  = EasyBlogTooltipHelper::getBloggerHTML( $comment->poster->id, array('my'=>'left bottom','at'=>'left top','of'=>array('traverseUsing'=>'prev')) );
							}
						?>
						<a href="<?php echo $avatarLink; ?>" title="<?php echo $comment->poster->getName();?>" class="comment-avatar avatar float-l">
							<img src="<?php echo $comment->poster->getAvatar(); ?>" alt="<?php echo $comment->poster->getName(); ?>" class="avatar" />
						</a>
						<?php echo $avatarTooltip; ?>
					<?php endif; ?>


                    <?php
                    /**
                    * ----------------------------------------------------------------------------------------------------------
                    * Comment content
                    * ----------------------------------------------------------------------------------------------------------
                    */
                    ?>
                    <div class="comment-content<?php echo (($counter == 0 || $counter == 1) ? ' first-comment' : ''); ?>">
    					<div class="comment-head prel fsm">
                            <i class="comment-arrow pabs"></i>
    						<span class="comment-author">
                                <?php
                                    $commentAuthor  = ($comment->created_by != 0) ? $comment->poster->getName() : $comment->name;
                                ?>
    							<b><?php echo $commentAuthor.' '.JText::_('COM_EASYBLOG_SAYS'); ?></b>
    						</span>
    						<span class="comment-date">
    							<?php #echo EasyBlogDateHelper::toFormat($created, $config->get('layout_dateformat', '%F, %d %B %Y')); ?>
    							<?php echo EasyBlogDateHelper::toFormat($created, 'F jS, Y \a\t H:i A'); ?>
    						</span>
                            <span class="comment-action small fsm">
    							<?php if( ($my->id == $comment->created_by && $this->acl->rules->edit_comment || $system->admin ) && $my->id != 0 ) { ?>
	    							<b>&middot;</b>
									<a href="javascript:eblog.comments.edit( '<?php echo $comment->id;?>' );"><?php echo JText::_( 'COM_EASYBLOG_COMMENT_EDIT' ); ?></a>
    							<?php } ?>
    							<?php if( $system->admin || ( $my->id == $comment->created_by && $this->acl->rules->delete_comment ) && $my->id != 0  ) { ?>
    								<b>&middot;</b>
    								<a href="javascript:eblog.comments.remove( '<?php echo $comment->id;?>' );"><?php echo JText::_( 'COM_EASYBLOG_COMMENT_DELETE' ); ?></a>
    							<?php } ?>
                            </span>
    					</div>

    					<div class="comment-body prel">
    						<i class="comment-arrow pabs"></i>
						    <div>
    						    <?php if( !empty( $comment->title ) ){ ?>
    							<h4 class="comment-title rip mbs" id="comment-title-<?php echo $comment->id;?>"><?php echo $comment->title; ?></h4>
    							<?php } ?>
    							<div class="comment-text"><?php echo $comment->comment; ?></div>
							</div>
    					</div>


                        <?php
                        /**
                        * ----------------------------------------------------------------------------------------------------------
                        * User control
                        * ----------------------------------------------------------------------------------------------------------
                        */
                        ?>
                        <div class="comment-control small">
                        	<?php if ( ($this->acl->rules->allow_comment || (empty($my->id) && $config->get('main_allowguestcomment'))) && ( ($comment->depth + 1) < $config->get('comment_maxthreadedlevel')) ) : ?>
        					<span id="toolbar-<?php echo $comment->id; ?>" class="comment-reply">
        						<span id="toolbar-reply-<?php echo $comment->id; ?>" class="comment-reply-yes show-this">
        							<a href="javascript:eblog.comment.reply('<?php echo $comment->id; ?>', '<?php echo $comment->depth + 1;?>', <?php echo $config->get('comment_autotitle', 0); ?>);" class="reply">								<?php echo JText::_('COM_EASYBLOG_REPLY'); ?>
        							</a>
        						</span>
        						<span id="toolbar-cancel-<?php echo $comment->id; ?>" class="comment-reply-no">
        							<a href="javascript:eblog.comment.cancel('<?php echo $comment->id; ?>');" class="cancel">
        								<?php echo JText::_('COM_EASYBLOG_CANCEL'); ?>
    								</a>
        						</span>
        					</span>
        					<?php endif;?>
                            <?php if($config->get('comment_likes')) : ?>
                            <span class="comment-like">
            					<span id="likes-container-<?php echo $comment->id;?>" class="likes-container" style="display:<?php echo (empty($comment->likesAuthor)) ? 'none': 'inline';?>;" >
            						<b>&middot;</b>
            						<?php echo $comment->likesAuthor;?>
            					</span>
                                <?php if($config->get('comment_likes') && $my->id != 0) : ?>
        							<span id="likes-<?php echo $comment->id;?>">
        								<?php if(empty($comment->isLike)) : ?>
        									<b>&middot;</b>
        							    	<a href="javascript:eblog.comment.likes('<?php echo $comment->id; ?>', '1', '0');" class="likes">
        							    		<?php echo JText::_('COM_EASYBLOG_LIKES');?>
    							    		</a>
        							    <?php else : ?>
        							    	<a href="javascript:eblog.comment.likes('<?php echo $comment->id; ?>', '0', '<?php echo $comment->isLike;?>');" class="likes">
        							    		<?php echo JText::_('COM_EASYBLOG_UNLIKE');?>
    							    		</a>
        							    <?php endif; ?>
        							</span>
        						<?php endif; ?>
                            </span>
        					<?php endif; ?>

                        </div>
    				</div>

    				<div id="comment-reply-form-<?php echo $comment->id; ?>" style="display:none;"></div>
                </div>
			</div>
			<?php
					$counter++;
				}
			}
			else
			{
				// if user is login
				if ( $my->id > 0 || $config->get( 'main_allowguestcomment' ) )
				{
				?>
				<div id="empty-comment-notice" class="pam"><?php echo JText::_('COM_EASYBLOG_COMMENTS_NO_COMMENT_YET'); ?></div>
				<?php
				}
			}

			if( $my->id <= 0 && !$config->get('main_allowguestcomment') )
			{
			?>
			<div id="empty-comment-notice" class="warning pam"><?php echo JText::sprintf('COM_EASYBLOG_COMMENTS_PLEASE_LOGIN', $loginURL); ?></div>
			<?php
			}
			?>
		</div>
		<?php if( $pagination->getPagesLinks() ) { ?>
		<div class="pagination clearfix"><?php echo $pagination->getPagesLinks();?></div>
		<?php } ?>
		<div id="comment-separator" class="clear"></div>

		<?php if ( $this->acl->rules->allow_comment || (empty($my->id) && $config->get('main_allowguestcomment'))  ) : ?>
		<div id="comment-form" class="blog-section clearfix">
			<a name="commentform" id="commentform"></a>
			<h3 class="section-title" id="comment-form-title"><?php echo JText::_('COM_EASYBLOG_LEAVE_YOUR_COMMENT');?></h3>
			<form id="frmComment">

				<div id="eblog-message" class="eblog-message"></div>

				<div id="blogSubmitWait" style="display: none;"><img src="<?php echo rtrim(JURI::root(),'/') . '/components/com_easyblog/assets/images/loader.gif' ?>" alt="Loading" border="0" /></div>

				<div class="comment-form clearfix">

					<?php if ( $config->get('layout_avatar') ) : ?>
					<div class="comment-avatar float-l">
						<?php if ( $my ) : ?>
						<img src="<?php echo $my->avatar; ?>" alt="<?php echo $my->displayName; ?>" class="avatar" />
						<?php else : ?>
						<img src="<?php echo rtrim(JURI::root(), '/') . '/components/com_easyblog/assets/images/default_blogger.png'; ?>" alt=""  class="avatar" />
						<?php endif; ?>
					</div>
					<?php endif; ?>

					<div class="comment-content">
    					<div class="comment-body prel">
    						<i class="comment-arrow pabs"></i>

							<?php if($canRegister && $my->id == 0) : ?>
							<div class="form-row">
								<label class="label"><?php echO JText::_('COM_EASYBLOG_FILL_IN_USERNAME_AND_FULLNAME_TO_REGISTER'); ?></label>
							</div>
							<?php endif; ?>
                           
							<input type="hidden" id="title" name="title" value="" />

                          	<div class="comment-field-left">  <?php if ( $my->id == 0 ) : ?>
								<?php if ( $canRegister ) : ?>
								<div class="form-row">
									<label class="label" for="esusername"><?php echo JText::_('COM_EASYBLOG_USERNAME'); ?> <small>(<?php echo JText::_('COM_EASYBLOG_REQUIRED_FOR_REGISTRATIONS'); ?>)</small></label>
								</div>
								<div class="form-row mrl mtm">
                                    <input class="inputbox width-full ffa fsg fwb" type="text" id="esusername" name="esusername" value="">
                                </div>
								<?php endif; ?>

								<div class="form-row mrl mtm">
                                	<span class="star">*</span>
									<label><?php echo JText::_('COM_EASYBLOG_NAME'); ?></label>
                                    <input class="inputbox width-full ffa fsg fwb" type="text" id="esname" name="esname" value="">
                                </div>

								<?php if( $config->get( 'comment_show_email' ) || $config->get('comment_require_email' ) || $canRegister ){ ?>
								<div class="form-row mtm">
                                    <div class="mrl">
                                    	<?php if($config->get('comment_require_email' )){ ?>
                                    		<span class="star">*</span>
                                    	<?php } ?>
                                    	<label><?php echo JText::_('COM_EASYBLOG_EMAIL_ADDRESS'); ?></label>
                                        <input class="inputbox width-full ffa fsg fwb" type="text" id="esemail" name="esemail" value="">
                                    </div>
                                </div>
                                <?php } ?>

                                <?php if( $config->get( 'comment_show_website' ) || $config->get('comment_require_website' ) ){ ?>
								<div class="form-row mtm">
                                    <div class="mrl">
                                    	<?php if($config->get('comment_require_website' )){ ?>
                                    		<span class="star">*</span>
                                    	<?php } ?>
                                    	<label><?php echo JText::_('COM_EASYBLOG_WEBSITE'); ?></label>
                                        <input class="inputbox width-full ffa fsg fwb" type="text" id="url" name="url" value="">
                                    </div>
                                </div>
                                <?php } ?>
                                <div class="clear"></div>
							<?php endif; ?></div>
                            <div class="comment-field-right">
								<div class="form-row">
									<span class="star">*</span>
	                            	<label><?php echo JText::_('COM_EASYBLOG_YOUR_MESSAGE'); ?></label>
									<textarea id="comment" name="comment" class="textarea"></textarea>
								</div>
								<div class="extra-fields">
									<?php echo EasyBlogHelper::getHelper( 'Captcha' )->getHTML();?>
									<?php if ( $my->id == 0 && $canRegister) : ?>
										<div class="form-row">
											<input class="inputbox" style="width:10px;" type="checkbox" id="esregister" name="esregister" value="y" />
											<label>
												<?php echo JText::_('COM_EASYBLOG_REGISTER_AS_SITE_MEMBER'); ?>
											</label>
										</div>
									<?php endif; ?>
		
									<?php if($config->get('comment_tnc')) : ?>
									<div class="form-row mts mbs fsm">
										<input type="checkbox" name="tnc" id="tnc" value="y" />
										<label><?php echo JText::sprintf('COM_EASYBLOG_AGREE_TERMS_AND_CONDITIONS', 'javascript: ejax.load(\'entry\', \'showTnc\');'); ?></label>
									</div>
									<?php endif; ?>
								</div>
								<div class="required-submit">
									<div class="required-info">
										<?php echo JText::_('COM_EASYBLOG_COMMENT_REQUIRED_INFO'); ?>
									</div>
									<div class="comment-submit-blue-btn"><button class="btn btn-blue" type="button" id="btnSubmit" onclick="eblog.comment.save();return false;">
                                    	<?php echo JText::_('COM_EASYBLOG_SUBMIT_COMMENT') ; ?>
                                    </button>
                                    </div>
								</div>
							</div>

		          			<?php if ( $my->id != 0 ) : ?>
		          			<input type="hidden" id="esname" name="esname" value="<?php echo $my->name; ?>" />
		          			<input type="hidden" id="esemail" name="esemail" value="<?php echo $my->email; ?>" />
		          			<input type="hidden" id="url" name="url" value="<?php echo $my->url; ?>" />
		          			<?php endif; ?>

							<input type="hidden" name="id" value="<?php echo $blog->id; ?>" />
							<input type="hidden" name="parent_id" id="parent_id" value="0" />
							<input type="hidden" name="comment_depth" id="comment_depth" value="0" />
							<input type="hidden" name="controller" value="blog" />
							<input type="hidden" name="task" value="commentSave" />
							<input type="hidden" id="totalComment" name="totalComment" value="<?php echo $counter; ?>" />

							<div id="subscription-box" class="subscription-box">
							<?php if ($subscriptionId) : ?>
								<div id="unsubscription-message" class="unsubscription-message"><?php echo JText::_('COM_EASYBLOG_ENTRY_AUTO_SUBSCRIBE_SUBSCRIBED_NOTE'); ?> <a href="javascript:void(0);" title="" onclick="eblog.blog.unsubscribe('<?php echo $subscriptionId; ?>', '<?php echo $blog->id; ?>');"><?php echo JText::_('COM_EASYBLOG_UNSUBSCRIBE_BLOG'); ?></a></div>
							<?php else : ?>
								<?php if($config->get('main_subscription') && $blog->subscription) : ?>
								<div id="subscription-message" class="subscription-message">
									<div class="clearfix mtm">
										<input type="checkbox" name="subscribe-to-blog" id="subscribe-to-blog" value="yes" class="float-l"<?php echo $system->config->get( 'comment_autosubscribe' ) ? ' checked="checked"' :'';?> />
										<label for="subscribe-to-blog"><?php echo JText::_('COM_EASYBLOG_SUBSCRIBE_BLOG'); ?>
										<?php if( $my->id > 0 ) : ?>
									 	(<?php echo $my->email; ?>)
									 	<?php else: ?>
									 	(<?php echo JText::_('COM_EASYBLOG_ENTRY_AUTO_SUBSCRIBE_NOTE'); ?>)
									 	<?php endif; ?>
									 	</label>
									</div>
								</div>
								<?php endif; ?>

							<?php endif; ?>
						</div>


    						<span class="bottom"><span class="inner"></span></span>
    					</div><!--end: .form-container-->
                    </div>

				</div>
			</form>
		</div>
		<?php endif; ?>
	<?php } ?>
</div>