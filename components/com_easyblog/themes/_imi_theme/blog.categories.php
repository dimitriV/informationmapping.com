<?php
/**
 * @package		EasyBlog
 * @copyright	Copyright (C) 2010 Stack Ideas Private Limited. All rights reserved.
 * @license		GNU/GPL, see LICENSE.php
 *
 * EasyBlog is free software. This version may have been modified pursuant
 * to the GNU General Public License, and as distributed it includes or
 * is derivative of works licensed under the GNU General Public License or
 * other free or open source software licenses.
 * See COPYRIGHT.php for copyright notices and details.
 */

defined('_JEXEC') or die('Restricted access');
$mainframe	=& JFactory::getApplication();
?>
<div id="ezblog-body">
	<div id="ezblog-section"><?php echo JText::_('COM_EASYBLOG_CATEGORIES_PAGE_HEADING'); ?></div>
	<div id="ezblog-category">
		<?php foreach($data as $category) { ?>
		<div class="profile-item clearfix">
            <div class="profile-head">
                <?php if($system->config->get('layout_categoryavatar', true)) : ?>
                <div class="profile-avatar float-l">
                    <i class="pabs"></i>
                    <a href="<?php echo EasyBlogRouter::_('index.php?option=com_easyblog&view=categories&layout=listings&id='.$category->id); ?>" class="avatar">
                        <img src="<?php echo $category->avatar;?>" align="top" width="80" class="avatar" />
                    </a>
                </div><!--end: .profile-avatar-->
                <?php endif; ?>

                <div class="profile-info">
                    <h3 class="profile-title rip"><a href="<?php echo EasyBlogRouter::_('index.php?option=com_easyblog&view=categories&layout=listings&id='.$category->id); ?>"><?php echo JText::_( $category->title ); ?></a></h3>
                    <?php if ( $category->description ) { ?>
                    <div class="profile-info mtm">
                        <?php echo $category->description; ?>
                    </div>
                    <?php } ?>
                    <?php if($system->config->get('main_categorysubscription')) { ?>
                    <div class="profile-connect mtm pbs">
                        <ul class="connect-links reset-ul float-li">
                            <?php if($system->config->get('main_categorysubscription')) { ?>
                            <?php if( ($category->private && $system->my->id != 0 ) || ($system->my->id == 0 && $system->config->get( 'main_allowguestsubscribe' )) || $system->my->id != 0) : ?>
                                <li>
                                    <a href="javascript:eblog.subscription.show( '<?php echo EBLOG_SUBSCRIPTION_CATEGORY; ?>' , '<?php echo $category->id;?>');" title="<?php echo JText::_('COM_EASYBLOG_SUBSCRIPTION_SUBSCRIBE_CATEGORY'); ?>" class="link-subscribe">
                                        <span><?php echo JText::_('COM_EASYBLOG_SUBSCRIPTION_SUBSCRIBE_CATEGORY'); ?></span>
                                    </a>
                                </li>
                            <?php endif; ?>
                            <?php } ?>
                            <?php if( $system->config->get('main_rss') ){ ?>
                                <li>
                                    <a href="<?php echo $category->rssLink;?>" title="<?php echo JText::_('COM_EASYBLOG_SUBSCRIBE_FEEDS'); ?>" class="link-rss">
                                        <span><?php echo JText::_('COM_EASYBLOG_SUBSCRIBE_FEEDS'); ?></span>
                                    </a>
                                </li>
                            <?php } ?>
                        </ul>
                    </div>
                    <?php } ?>
                    
                    <?php if(! empty($category->nestedLink)) { ?>
                    <div class="profile-child ptm">
                        <span><?php echo JText::_( 'COM_EASYBLOG_CATEGORIES_SUBCATEGORIES' ); ?></span>
                        <?php echo $category->nestedLink; ?>
                    </div>
                    <?php } ?>
                </div><!--end: .profile-info-->
                <div class="clear"></div>
            </div><!--end: .profile-head-->


           
            
            
            <div class="clear"></div>
	</div><!--end: .blogger-item-->

	<?php } //end foreach ?>

	<?php if(count($data) <= 0) { ?>
	<div><?php echo JText::_('COM_EASYBLOG_NO_RECORDS_FOUND'); ?></div>
	<?php } ?>

	<?php if ( $pagination ) : ?>
    <div class="pagination clearfix">
        <?php echo $pagination; ?>
    </div>
    <?php endif; ?>
</div>
</div><!--end: #ezblog-body-->