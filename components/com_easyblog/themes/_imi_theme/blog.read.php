<?php
/**
 * @package		EasyBlog
 * @copyright	Copyright (C) 2010 Stack Ideas Private Limited. All rights reserved.
 * @license		GNU/GPL, see LICENSE.php
 *
 * EasyBlog is free software. This version may have been modified pursuant
 * to the GNU General Public License, and as distributed it includes or
 * is derivative of works licensed under the GNU General Public License or
 * other free or open source software licenses.
 * See COPYRIGHT.php for copyright notices and details.
 */
defined('_JEXEC') or die('Restricted access');

$teamIdLink = '';
if( isset( $team ) )
	$teamIdLink = '&team=' . $team;

$socialPosition = $system->config->get('main_socialbutton_position');

?>
<script type="text/javascript">
sQuery(document).ready( function(){
	eblog.blog.tab.init();
});
</script>
<div id="ezblog-body">
<div id="entry_<?php echo $blog->id; ?>" class="blog-read clearfix">
	<?php if ( !empty( $notice ) ) : ?>
	<div class="eblog-message info"><?php echo $notice; ?></div>
	<?php endif; ?>
	<?php
	/**
	* ----------------------------------------------------------------------------------------------------------
	* Admin or author option on this blog
	* ----------------------------------------------------------------------------------------------------------
	*/
	?>
	<?php echo $this->fetch( 'blog.admin.tool.php' ); ?>

	<div class="blog-head mtm">
		<?php if( $blog->isFeatured() ) { ?>
		<div class="tag-featured in-block"><?php echo Jtext::_('COM_EASYBLOG_FEATURED_FEATURED'); ?></div>
		<?php } ?>
		<h1 id="title_<?php echo $blog->id; ?>" class="blog-title<?php echo ($isFeatured) ? ' featured-item' : '';?> rip">
			<?php echo $blog->title; ?>
		</h1>
	</div>

	<div class="blog-brief ptl pbl mbl">
		<div class="in">
			<?php if( $system->config->get( 'layout_avatar' ) && $system->config->get( 'layout_avatar_in_read_blog' ) ){ ?>
			<div class="blog-avatar float-l prel mrm">
				<?php
					if( isset( $team ) )
					{
						$teamBlog   =& EasyBlogHelper::getTable( 'TeamBlog', 'Table');
						$teamBlog->load( $team );
				?>
				<a href="<?php echo EasyBlogRouter::_('index.php?option=com_easyblog&view=teamblog&layout=listings&id=' . $teamBlog->id ); ?>" class="avatar isTeamBlog float-l prel">
					<img src="<?php echo $teamBlog->getAvatar(); ?>" alt="<?php echo $teamBlog->title; ?>" class="avatar" width="36" height="36" />
				</a>
				<a href="<?php echo $blogger->getProfileLink(); ?>" class="avatar isBlogger float-l pabs">
					<img src="<?php echo $blogger->getAvatar(); ?>" alt="<?php echo $blogger->getName(); ?>" class="avatar" width="20" height="20" />
				</a>
				<?php
					} else {
				?>
				<a href="<?php echo $blogger->getProfileLink(); ?>" class="avatar float-l">
					<img src="<?php echo $blogger->getAvatar(); ?>" alt="<?php echo $blogger->getName(); ?>" class="avatar isBlogger" width="36" height="36" />
				</a>
				<?php } ?>
			</div>
			<?php } ?>
			<div class="blog-meta">
				<div class="in">
					<?php echo JText::_( 'COM_EASYBLOG_POSTED' );?> 
                    <?php echo JText::sprintf( 'COM_EASYBLOG_IN' , EasyBlogRouter::_('index.php?option=com_easyblog&view=categories&layout=listings&id='.$blog->category_id), $blog->getCategoryName() ); ?>
					<span><?php echo $this->formatDate( $system->config->get('layout_dateformat') , $blog->created ); ?></span>
				</div>
			</div>
			
		</div>
	</div>


	<?php
	/**
	* ----------------------------------------------------------------------------------------------------------
	* End blog head. Content start here
	* ----------------------------------------------------------------------------------------------------------
	*/
	?>
	<?php echo $blog->event->afterDisplayTitle; ?>
	<div class="blog-text clearfix prel mtm mbm">
		<?php if($socialPosition == 'top') EasyBlogHelper::showSocialButton($blog); ?>
		<?php if($socialPosition == 'left' || $socialPosition == 'right') EasyBlogHelper::showSocialButton($blog); ?>
		<?php echo $blog->event->beforeDisplayContent; ?>
		<?php if(!empty($blog->toc)){ echo $blog->toc; } ?>
		<?php echo $blog->content; ?>

		<?php if( $system->config->get( 'main_locations_blog_entry' ) ){ ?>
		<?php echo EasyBlogHelper::getHelper( 'Maps' )->getHTML( true ,
																$blog->address,
																$blog->latitude,
																$blog->longitude ,
																$system->config->get( 'main_locations_blog_map_width') ,
																$system->config->get( 'main_locations_blog_map_height' ),
																JText::sprintf( 'COM_EASYBLOG_LOCATIONS_BLOG_POSTED_FROM' , $blog->address ),
																'post_map_canvas_' . $blog->id );?>
		<?php } ?>
		<?php echo $blog->event->afterDisplayContent; ?>

		<?php /* content copyright */ ?>
		<?php if( $system->config->get( 'layout_copyrights' ) && !empty($blog->copyrights) ) { ?>
		<div class="blog-copyrights mtm">
			<div class="in clearfix">
				<span class="blog-copyrights">
					<?php echo JTEXT::_('COM_EASYBLOG_COPYRIGHTS_PREFIX') . $blog->copyrights ?>
				</span>
			</div>
		</div>
		<?php } ?>

		<?php echo $this->fetch( 'blog.source'. EasyBlogHelper::getHelper( 'Sources' )->getTemplateFile( $blog->source ) . '.php' ); ?>

	</div>

	<?php if( $system->config->get( 'layout_hits' ) ) { ?>
	<div class="blog-rate mtm">
		<div class="in clearfix">
			<span class="blog-rating">
				<?php echo EasyBlogHelper::getHelper( 'ratings' )->getHTML( $blog->id , EBLOG_RATINGS_TYPE_ENTRY , JText::_( 'COM_EASYBLOG_RATINGS_RATE_BLOG_ENTRY') , 'blog-ratings' ); ?>
			</span>
		</div>
	</div>
	<?php } ?>

	<?php
		if($socialPosition == 'bottom'){
			?>
			<div class="socialbuttons-label">
				<?php echo JText::_('COM_EASYBLOG_SOCIALSHARE_LABEL'); ?>
			</div>
			<?php
			EasyBlogHelper::showSocialButton($blog);
		}
	?>

	<?php if( $system->config->get('main_facebook_like') && $system->config->get('main_facebook_like_layout') == 'standard' ) : ?>
	<div id="eb-fblikes" class="align<?php echo ($this->getDirection() == 'rtl') ? 'right' : 'left'; ?> mtl">
		<?php echo $facebookLike; ?>
	</div>
   
	<div class="clear"></div>
	<?php endif; ?>

 <div class="blog-option mts">
				<ul class="reset-ul float-li small fsm">
					
					<?php if( $system->config->get('main_comment') && $blog->totalComments !== false ){ ?>
					<li class="blog-comments">
						<?php echo $this->getNouns( 'COM_EASYBLOG_COMMENT_COUNT' , $blog->totalComments , true ); ?>
					</li>
					<?php } ?>
					
					<?php echo EasyBlogHelper::getHelper( 'publishtools' )->getHTML( $blog->id ); ?>
				</ul>
			</div>
	<?php
	/**
	* ----------------------------------------------------------------------------------------------------------
	* Entry navigation
	* ----------------------------------------------------------------------------------------------------------
	*/
	?>
	<?php if ( !empty( $prevLink ) OR !empty( $nextLink ) ) : ?>
	<ul class="blog-navi reset-ul float-li small clearfix pbm ptm mtl">
		<?php if ( !empty( $prevLink ) ) : ?>
		<li class="entry-prev">
			<a href="<?php echo EasyBlogRouter::_('index.php?option=com_easyblog&view=entry&id=' . $prevLink['id'] . $teamIdLink); ?>" title="<?php echo JText::sprintf('COM_EASYBLOG_ENTRY_BLOG_PREVIOUS_TITLE', $prevLink['title']); ?>"><?php echo JText::sprintf('COM_EASYBLOG_BLOG_ENTRY_PREV', $prevLink['title']); ?></a>
		</li>
		<?php endif; ?>

		<?php if ( !empty( $nextLink ) ) : ?>
		<li class="entry-next">
			<a href="<?php echo EasyBlogRouter::_('index.php?option=com_easyblog&view=entry&id=' . $nextLink['id'] . $teamIdLink); ?>" title="<?php echo JText::sprintf('COM_EASYBLOG_ENTRY_BLOG_NEXT_TITLE', $nextLink['title']); ?>"><?php echo JText::sprintf('COM_EASYBLOG_BLOG_ENTRY_NEXT', $nextLink['title']); ?></a>
		</li>
		<?php endif; ?>
	</ul>
	<?php endif; ?>
</div><!--end: .blog-read-->
<?php echo $adsenseHTML; ?>
<?php if( $system->config->get('main_showauthorinfo') ) { ?>
<ul class="tab_button reset-ul float-li clearfix">
	<?php if( $system->config->get('main_showauthorinfo') ) { ?>
	<li id="button-author" class="tab_item author"><a href="javascript:void(0);" class="ico"><span><?php echo JText::_('COM_EASYBLOG_ENTRY_ABOUT_AUTHOR_TITLE'); ?></span></a></li>
	<?php } ?>
	<?php if( !empty( $blogRelatedPost ) ) { ?>
	<li id="button-related" class="tab_item related"><a href="javascript:void(0);" class="ico"><span><?php echo JText::_('COM_EASYBLOG_ENTRY_BLOG_RELATED_POST_TAB'); ?></span></a></li>
	<?php } ?>
	<?php if( $system->config->get('main_trackbacks') ) { ?>
	<li id="button-trackbacks" class="tab_item trackbacks"><a href="javascript:void(0);" class="ico"><span><?php echo JText::_('COM_EASYBLOG_ENTRY_BLOG_TRACKBACKS_TAB'); ?></span></a></li>
	<?php } ?>
</ul>
<?php } ?>
<?php if( $system->config->get('main_showauthorinfo') ): ?>
	<?php echo $this->fetch( 'author.info.php' ); ?>
<?php endif; ?>

<?php if( $system->config->get('main_trackbacks') ) { ?>
	<?php echo $this->fetch( 'blog.trackback.php' ); ?>
<?php } ?>

<?php if(! empty($blogRelatedPost)) { ?>
	<?php echo $this->fetch( 'blog.related.php' ); ?>
<?php } ?>

<?php if( $system->config->get('main_comment') ) { ?>
	<?php echo $commentHTML;?>
<?php } ?>
</div><!--end: #ezblog-body-->
