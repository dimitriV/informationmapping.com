<?php
/**
 * @package		EasyBlog
 * @copyright	Copyright (C) 2010 Stack Ideas Private Limited. All rights reserved.
 * @license		GNU/GPL, see LICENSE.php
 *  
 * EasyBlog is free software. This version may have been modified pursuant
 * to the GNU General Public License, and as distributed it includes or
 * is derivative of works licensed under the GNU General Public License or
 * other free or open source software licenses.
 * See COPYRIGHT.php for copyright notices and details.
 */

defined('_JEXEC') or die('Restricted access');
?>

Good day,<br />
<br />
<br />
New blog '<?php echo $blogTitle; ?>' has been added by author '<?php echo $blogAuthor; ?>' and is currently pending review from administrator.
<br />
<br />
Please login to website and head to dashboard page to review the above blog post.
<br />
<br />
Have a nice day!
<br />