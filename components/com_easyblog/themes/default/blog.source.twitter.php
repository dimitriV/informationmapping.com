<?php
/**
 * @package		EasyBlog
 * @copyright	Copyright (C) 2010 Stack Ideas Private Limited. All rights reserved.
 * @license		GNU/GPL, see LICENSE.php
 *
 * EasyBlog is free software. This version may have been modified pursuant
 * to the GNU General Public License, and as distributed it includes or
 * is derivative of works licensed under the GNU General Public License or
 * other free or open source software licenses.
 * See COPYRIGHT.php for copyright notices and details.
 */
defined('_JEXEC') or die('Restricted access');

if( !isset( $blog ) && !isset( $row ) )
{
    return;
}
$item	= isset( $blog ) ? $blog : $row;

$microblog		= EasyBlogHelper::getTable( 'TwitterMicroblog' );
$microblog->loadByPostId( $item->id );
?>
<?php if( $microblog->tweet_author ){ ?>
	<span class="source-linkback"><?php echo JText::_( 'COM_EASYBLOG_TWITTER_FOLLOW' );?> @<a href="http://twitter.com/<?php echo $microblog->tweet_author;?>" target="_blank"><?php echo $microblog->tweet_author;?></a></span>
<?php } ?>
