<?php
/**
 * @package		EasyBlog
 * @copyright	Copyright (C) 2010 Stack Ideas Private Limited. All rights reserved.
 * @license		GNU/GPL, see LICENSE.php
 *  
 * EasyBlog is free software. This version may have been modified pursuant
 * to the GNU General Public License, and as distributed it includes or
 * is derivative of works licensed under the GNU General Public License or
 * other free or open source software licenses.
 * See COPYRIGHT.php for copyright notices and details.
 */
defined('_JEXEC') or die('Restricted access');
?>
<?php
	
    $team       = '';
    if( isset($entry->team_id) )
    {
		$team	= ( !empty( $entry->team_id )) ? '&team=' . $entry->team_id : '';
    }
?>
<li>
	<h3 class="blog-title via-email rip">
        <a href="<?php echo EasyBlogRouter::_('index.php?option=com_easyblog&view=entry'.$team.'&id='.$entry->id); ?>"><?php echo $entry->title; ?></a>
        <?php if( $entry->isFeatured ) { ?><sup class="tag-featured"><?php echo Jtext::_('COM_EASYBLOG_FEATURED_FEATURED'); ?></sup><?php } ?>
    </h3>
    <div class="blog-meta via-email fsm mts">
        <span class="blog-date"><?php echo JText::sprintf('COM_EASYBLOG_IN', EasyBlogRouter::_('index.php?option=com_easyblog&view=categories&layout=listings&id='.$entry->category_id), $entry->category); ?></span>
        <?php if( EasyBlogHelper::getHelper( 'Comment' )->isBuiltin() && $system->config->get('main_comment') && $entry->totalComments !== false ){ ?>
        -
        <span class="blog-comments"><a href="<?php echo EasyBlogRouter::_('index.php?option=com_easyblog&view=entry&id='.$entry->id); ?>#comments"><?php echo $this->getNouns( 'COM_EASYBLOG_COMMENT_COUNT' , $entry->totalComments , true );?></a></span>
		<?php } ?>
    </div>
    <div class="mts">
    	<?php echo JString::substr( strip_tags( $entry->intro . $entry->content ) , 0 , 350 ); ?> ...
    </div>
	<div class="mts fsm in-block width-full">
        <span class="float-r small">
        <?php echo $this->formatDate( $system->config->get('layout_shortdateformat', '%b %d'), $entry->created ); ?>
        </span>
		<span class="blog-tags"><?php echo JText::sprintf('COM_EASYBLOG_TAG_LIST', $entry->tags); ?></span>
	</div>
</li>