/*
 * jquery.stackSuggest
 * An auto-suggest plugin for textbox that
 * filters and return matching data entity
 * within a given object-based dataset.
 *
 * Copyright (c) 2011 Jason Ramos
 * Dual licensed under the MIT and GPL licenses:
 * http://www.opensource.org/licenses/mit-license.php
 * http://www.gnu.org/licenses/gpl.html
 *
 * Version : 0.1
 * Released: Tuesday, 12th April 2011.
 *
 */

(function($)
{

	var methods = {
		
		initialize: function(options) {

			var options = options;

			return this.each(function()
			{
				var $this = $(this),
					data  = $this.data('stackSuggest');

				if (!data) {
					var defaultOptions = {
						dataset: [],
						exclude: [],
						delimiter: ',',
						container: $(),
						template: $this.next('script.stackSuggest'),
						showOnFocus: true,
						showOnClick: false,
						useFirstSuggestionAsDefault: false,
						position: {
							my: 'left top',
							at: 'left bottom',
							of: $this
						},
						custom: function(){}
					}

					options = $.extend(true, {}, defaultOptions, options);				

					if (options.template.length<1)
						return;

					// Attach each data with a unique id
					options.dataset.each(function($entry, i)
					{
						options.dataset[i]['$dataId'] = $.uid('$dataId-');
					});

					methods.save.call($this, options);
					methods.bind.call($this);
				}
			});
		},

		// extendDataset: function(dataset)
		// {	
		// 	var $this = $(this),
		// 		options = $this.data('stackSuggest');

		// 	dataset.each(function($entry, i)
		// 	{
		// 		dataset[i]['$dataId'] = $.uid('$dataId-');
		// 	});

		// 	options.dataset = $.extend(options.dataset, dataset);

		// 	methods.save.call($this, options);
		// },

		dataset: function(dataset)
		{
			var $this = $(this),
				options = $this.data('stackSuggest');

			dataset.each(function($entry, i)
			{
				dataset[i]['$dataId'] = $.uid('$dataId-');
			});

			options.dataset = dataset;

			methods.save.call($this, options);			
		},

		destroy: function() {
			return this.each(function()
			{
				var $this = $(this);

				$this
					.unbind('.stackSuggest')
					.removeData('stackSuggest');
			});
		},

		save: function(options)
		{
			return $(this).data('stackSuggest', options);
		},

		bind: function()
		{
			return this.each(function()
			{
				var $this = $(this),
					options = $this.data('stackSuggest');

				$this
					.bind('blur.stackSuggest', function()
					{
						if (!options.container.data('isFocused'))
						{
							methods.hide.call($this);
						}
					})
					.bind('click.stackSuggest', function()
					{
						if (options.showOnClick)
						{
							$this.trigger('focus');
						}
					})
					.bind('focus', function(){
						
						// @TODO: Perhaps a configuration option for this?
						if( options.showOnFocus )
						{
							if (options.dataset.length < 1) return;

							// Initial focus would display the list of tags by default regardless
							if ($.trim($this.val()) =='' && (options.container.hasClass('hidden') || options.container.length < 1))
							{
								methods.populate.call( $this , '' , true );
							}
							
							// If there is value for the input already, do a filter on it.
							if( $.trim($this.val() ) != '' )
							{
								methods.populate.call($this);
							}
						}
					})
					.bind('keydown.stackSuggest', function(event)
					{
						window._isEnter = (event.keyCode==13);

						switch (event.keyCode)
						{
							case $.ui.keyCode.UP:
								methods.highlight.call($this, -1);
								break;

							case $.ui.keyCode.DOWN:
								methods.highlight.call($this, 1);
								break;
						}
					})
					.bind('keypress.stackSuggest', function(event)
					{
						window._isEnter = window._isEnter && (event.keyCode == 13);
					})
					.bind('keyup.stackSuggest', function(event)
					{
						switch(event.keyCode)
						{	
							case $.ui.keyCode.DOWN:
							case $.ui.keyCode.UP:
								if ($.trim($this.val())=='' && (options.container.hasClass('hidden') || options.container.length < 1))
								{
									methods.populate.call($this, '', true)
								}
								break;

							case $.ui.keyCode.ESCAPE:
								methods.hide.call($this);
								break;
							
							case $.ui.keyCode.ENTER:
								if (window._isEnter)
								{
									methods.use.call($this);
								} else {
									window._isEnter = false;
									methods.populate.call($this);
								}
								break;
							
							default:
								methods.populate.call($this);
						}
					});
			});
		},

		show: function() {
			return this.each(function()
			{
				var $this = $(this),
				    options = $this.data('stackSuggest');
				
				options.container
					.removeClass('hidden')
					.show(0, function()
					{
						options.container
							.css({
								width: $this.outerWidth()
							})
							.position(options.position);
					});
			});
		},

		hide: function() {
			return this.each(function()
			{
				var $this   = $(this),
				    options = $this.data('stackSuggest');

				options.container.addClass('hidden').hide();

			});
		},

		highlight: function(d)
		{
			var $this = $(this),
			    options = $this.data('stackSuggest');

			$s = options.container.find('.stackSuggestItem');

			if ($s.length < 1) return;
			
			var a = $s.length - 1, r = 0;
			$s.each(function(i, s)
			{
				var s = $(s);

				if(s.hasClass('active'))
				{
					r = i + d; if (r > a) r = 0; if (r < 0) r = a;
				}

				s.removeClass('active');
			});

			$r = $s.eq(r);

			$sg = options.container.find('.stackSuggestItemGroup');

			var topline  = $sg.scrollTop();
			var baseline = $sg.height() + $sg.scrollTop();
			var itemTop  = $r.position().top + $sg.scrollTop();
			var itemBottom = itemTop + $r.outerHeight(true);

			if (itemTop-10 <= topline)
			{
				$sg.scrollTop(itemBottom - $sg.height());
			} else if (itemBottom+10 >= baseline) {
				$sg.scrollTop(itemTop);
			}
				
			$r.addClass('active');
		},

		sanitize: function(keyword) {

			var $this    = $(this),
				options  = $this.data('stackSuggest');

			var d = options.delimiter;

			/* Holy grail of trimming whitespace & delimiter altogether
			 *
			 * Turns this : ",df        ,,,  ,,,abc, sdasd sdfsdf    ,   asdsad, ,, , "
			 * into this  : "df,abc,sdasd sdfsdf,asdsad"
		     */
			keyword = keyword
				.replace(new RegExp('^['+d+'\\s]+|['+d+',\\s]+$','g'), '') // /^[,\s]+|[,\s]+$/g
				.replace(new RegExp(d+'['+d+'\\s]*'+d,'g'), d) // /,[,\s]*,/g
				.replace(new RegExp('[\\s]+'+d,'g'), d) // /[\s]+,/g
				.replace(new RegExp(d+'[\\s]+','g'), d); // /,[\s]+/g

			if (keyword=='') return [];

			// Remove duplicates
			var keywords = keyword.split(d);
			keywords = $.grep(keywords, function(keyword, index)
			{
				return $.inArray(keyword, keywords) === index;
			});

			return keywords;
		},

		suggest: function(keywords) {

			var $this    = $(this),
				options  = $this.data('stackSuggest'),
				dataset  = [];

			function $match(value, keyword)
			{
				if (typeof value=='string' || typeof value=='number')
					return value.toUpperCase().indexOf(keyword.toUpperCase()) >= 0;

				return false;
			}

			$.each(keywords, function(i, keyword)
			{
				// Optimization: Only lookup the first 5 keywords.
				if (i > 5) return false;

				dataset = dataset.concat(
					$.grep(options.dataset, function(data)
					{
						var match = false;

						// Don't include this if it is already on the match list
						$.each(dataset, function()
						{
							match = (this.$dataId==data.$dataId);
							return !match;
						});
						if (match) return false;

						// Don't include this data if it is on exclude list
						$.each(options.exclude, function()
						{
							match = (this==data.$dataId);
							return !match;
						})
						if (match) return false;

						if (options.filterkey!==undefined)
						{
							$.each(options.filterkey, function(i, key)
							{																
								match = $match(data[key], keyword);
								return !match;
							});
						} else {
							for (key in data)
							{
								match = $match(data[key], keyword);
								if (match) break;
							}
						}

						return match;
					})
				);
			});

			return dataset;
		},

		custom: function(keywords)
		{
			var $this    = $(this),
				options  = $this.data('stackSuggest'),
				dataset  = [];

			$.each(keywords, function(i, keyword)
			{
				dataset.push(options.custom.call($this, keyword));
			});

			return dataset;
		},

		use: function()
		{
			var $this      = $(this),
			    options    = $this.data('stackSuggest'),
			    keyword    = $.trim($this.val()),
			    suggestion = options.container.find('.stackSuggestItem.active'),
			    $dataId    = suggestion.attr('dataid'),
			    tmplData   = suggestion.tmplItem().data;

			// if (keyword=='') return;

			if (suggestion.hasClass('custom'))
			{
				$.each(tmplData.custom, function(i, data)
				{
					options.add.call($this, data);
				});
			} else {
				$.each(tmplData.dataset, function(i, data)
				{
					if (data.$dataId==$dataId) options.add.call($this, data);
				})
			}

			methods.hide.call($this);

			// $this.val('').focus();
		},

		exclude: function($dataId)
		{
			var $this = $(this),
				options = $this.data('stackSuggest');

			options.exclude.push($dataId);

			return methods.save.call($this, options);
		},

		include: function($dataId)
		{
			var $this = $(this),
				options = $this.data('stackSuggest');

			options.exclude = $.grep(options.exclude, function(dataId)
			{
				return dataId!=$dataId;
			});

			return methods.save.call($this, options);
		},

		populate: function(keyword, showAll)
		{
			var $this = $(this),
			    options = $this.data('stackSuggest');

			// TODO: Re-evaluate logic here
			// Return if no keywords are given
			if (keyword==undefined || keyword=='')
			{
				keyword = (showAll) ? '' : $.trim($this.val());

				if (keyword=='' && (!showAll || options.dataset.length < 1))
				{
					methods.hide.call($this);
					return;
				}
			}

			// See if existing suggestion container exists
			if (options.container.length > 0)
			{
				// And retrieve the $dataId of the currently highlighted suggestion
				var $activeDataId = options.container.find('.stackSuggestItem.active:not(.custom)').tmplItem().data['$dataId'];
				options.container.remove();
			}

			var keywords = (showAll) ? '' : methods.sanitize.call($this, keyword);

			// Build suggestion container
			var tmplData = {
				dataset: (showAll) ? options.dataset : methods.suggest.call($this, keywords),
				custom : methods.custom.call($this, keywords)
			};

			options.container = options.template.tmpl(tmplData).appendTo('body');

			options.container
				.mouseover(function()
				{
					options.container.data('isFocused', true);
				})
				.mouseout(function()
				{
					options.container.data('isFocused', false);
				});

			options.container
				.find('.stackSuggestItem')
				.mouseover(function()
				{
					options.container.data('isFocused', true);
				})
				.mouseout(function()
				{
					options.container.data('isFocused', false);
				})
				.click(function()
				{
					options.container
						.find('.stackSuggestItem')
						.removeClass('active');

					$(this).addClass('active');

					options.container.data('isFocused', false);

					methods.use.call($this);
				});

			// Restore active selection
			var activeSelection = 
				options.container.find('.stackSuggestItem')
					.filter(function()
					{
						return ($(this).attr('dataId') == $activeDataId);
					})
					.addClass('active');

			// If active selection does not exist, revert to default selection
			if (activeSelection.length < 1)
			{
				if (options.useFirstSuggestionAsDefault)
				{
					options.container.find('.stackSuggestItem:first')
						.addClass('active');
				} else {
					options.container.find('.stackSuggestItem.custom')
						.addClass('active');					
				}
			}


			

			// Save options
			methods.save.call($this, options);

			// Display suggestion
			methods.show.call($this);
		}
	};
	
	$.fn.stackSuggest = function(method) {
		if ( methods[method] ) {
			return methods[method].apply( this, Array.prototype.slice.call( arguments, 1 ));
		} else if ( typeof method === 'object' || ! method ) {
			return methods.initialize.apply( this, arguments );
		} else {
			$.error( 'Method ' +  method + ' does not exist on $.stackSuggest' );
		}
	};

})(sQuery);