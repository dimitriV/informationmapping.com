/*
 * jquery.uid
 * Generates a unique id with optional prefix/suffix.
 *
 * Copyright (c) 2011 Jason Ramos
 * Dual licensed under the MIT and GPL licenses:
 * http://www.opensource.org/licenses/mit-license.php
 * http://www.gnu.org/licenses/gpl.html
 *
 * Version : 0.1
 * Released: Thursday, 14th April 2011.
 *
 */

(function($)
{
	$.uid = function(p,s)
	{
		return ((p) ? p : '') + Math.random().toString().replace('.','') + ((s) ? s : '');
	}
})(sQuery);