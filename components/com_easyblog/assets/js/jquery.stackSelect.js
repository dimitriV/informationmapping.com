/*!
 * jquery.stackSelect.
 * Chainable multiple selection event handler.
 *
 * Copyright (c) 2011 Jason Ramos
 * Dual licensed under the MIT and GPL licenses:
 * http://www.opensource.org/licenses/mit-license.php
 * http://www.gnu.org/licenses/gpl.html
 *
 * Version : 0.1
 * Released: Thursday, 7th April 2011.
 *
 * $(e).stackSelect({
 *    selected  : function(){},   // this refers to the input being checked
 *    deselected: function(){},   // this refers to the input being unchecked
 *    
 *    // returns selected elements & deselected elements in separate arguments
 *    complete  : function(selected, deselected){}
 * })
 */
(function($)
{
	var defaultOptions = {	
		select: '.stackSelect',
		selectAll: '.stackSelectAll',
		selectGroup: '.stackSelectGroup',
		selected: function() {},
		deselected: function() {},
		complete: function() {}
	}

	$.fn.stackSelect = function(o)
	{		
		var o = $.extend({}, defaultOptions, o);

		var target = this;
		var selectGroup = target.closest(o.selectGroup);
		var selectAll   = selectGroup.find(o.selectAll);
		var select      = selectGroup.find(o.select);
		var completed   = true;

		var complete = function() {
			if (completed) o.complete.apply(target, [select.filter(':checked'), select.not(':checked')]);
		};

		select.checked(
			function() { o.selected.apply(this); complete(); },
			function() { o.deselected.apply(this); complete(); });

		selectAll.checked(
			function() { completed=false; select.checked(true);  completed=true; complete(); },
			function() { completed=false; select.checked(false); completed=true; complete(); });

		return this;
	}
})(sQuery);