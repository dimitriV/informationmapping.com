/*
 * jquery.stretchToFit
 *
 * Copyright (c) 2011 Jason Ramos
 * Dual licensed under the MIT and GPL licenses:
 * http://www.opensource.org/licenses/mit-license.php
 * http://www.gnu.org/licenses/gpl.html
 *
 * Version : 0.1
 * Released: Wednesday, 13th April 2011.
 *
 */
(function($)
{
	$.fn.stretchToFit = function() {
		return $.each(this, function()
		{
			var $this = $(this);

			$this
				.css('width', '100%')
				.css('width', $this.width() * 2 - $this.outerWidth(true) - parseInt($this.css('borderLeftWidth')) - parseInt($this.css('borderRightWidth')));
		});
	}
})(sQuery);