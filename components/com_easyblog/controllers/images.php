<?php
/**
 * @version		$Id: file.php 14401 2010-01-26 14:10:00Z louis $
 * @package		Joomla
 * @subpackage	Content
 * @copyright	Copyright (C) 2005 - 2010 Open Source Matters. All rights reserved.
 * @license		GNU/GPL, see LICENSE.php
 * Joomla! is free software. This version may have been modified pursuant to the
 * GNU General Public License, and as distributed it includes or is derivative
 * of works licensed under the GNU General Public License or other free or open
 * source software licenses. See COPYRIGHT.php for copyright notices and
 * details.
 */

// Check to ensure this file is included in Joomla!
defined('_JEXEC') or die( 'Restricted access' );

jimport('joomla.filesystem.file');
jimport('joomla.filesystem.folder');

require_once( JPATH_ROOT . DS . 'administrator' . DS . 'components' . DS . 'com_media' . DS . 'helpers' . DS. 'media.php' );
require_once( EBLOG_HELPERS . DS . 'image.php' );
require_once( EBLOG_CLASSES . DS . 'simpleimage.php' );

/**
 * Weblinks Weblink Controller
 *
 * @package		Joomla
 * @subpackage	Weblinks
 * @since 1.5
 */
//class MediaControllerFile extends MediaController
class EasyBlogControllerImages extends JController
{
	function ajaxupload()
	{
		$mainframe	=& JFactory::getApplication();
		$my         =& JFactory::getUser();
		$config     =& EasyBlogHelper::getConfig();
		$acl		= EasyBlogACLHelper::getRuleSet();
		
		// @rule: Only allowed users are allowed to upload images.
		if( $my->id == 0 || empty( $acl->rules->upload_image ) )
		{
			$sessionid	= JRequest::getVar('sessionid');
			if ($sessionid)
			{
				$session	= EasyBlogHelper::getTable('Session', 'JTable');
				$session->load($sessionid);

				if (!$session->userid)
				{
					$this->output( $this->getMessageObj( EBLOG_MEDIA_SECURITY_ERROR , JText::_( 'COM_EASYBLOG_NOT_ALLOWED' ) ) );
				}
				$my	= JFactory::getUser($session->userid);
			}
			else
			{
				$this->output( $this->getMessageObj( EBLOG_MEDIA_SECURITY_ERROR , JText::_( 'COM_EASYBLOG_NOT_ALLOWED' ) ) );
			}
		}

		// Check if the blogger_id is given or not
		$blogger_id = JRequest::getInt( 'bloggger_id', $my->id );
		
	    if( $my->id != $blogger_id)
	    {
	        // now we knwo this two is different person.
			// lets check if the current user have the power to upload photos into other ppl folder or not.
			if( ! EasyBlogHelper::isSiteAdmin( $my->id ) )
			{
				$this->output( $this->getMessageObj( EBLOG_MEDIA_SECURITY_ERROR , JText::_( 'COM_EASYBLOG_NOT_ALLOWED' ) ) );
			}
	    }

		$file 				= JRequest::getVar( 'file', '', 'files', 'array' );
		$subfolder			= JRequest::getVar( 'folder' );
		$subfolder			= str_ireplace( '../' , '' , $subfolder );

		$imagePath			= str_ireplace( array( "/" , "\\" ) , DS , rtrim( $config->get('main_image_path') , '/') );
		$userUploadPath    	= JPATH_ROOT . DS . str_replace('/', DS, $imagePath . DS . $blogger_id);

		$baseUri			= rtrim( str_ireplace( '\\' , '/' , $config->get( 'main_image_path') ) , '/' ) . '/' . $blogger_id;
		$baseUri			= rtrim( JURI::root() , '/' ) . '/' . $baseUri;
		
		$baseUri			.= !empty( $subfolder ) ? '/' . $subfolder : '';
		$folder     		= JPath::clean( $userUploadPath );		
		$folder				.= !empty( $subfolder ) ? DS . JPath::clean( $subfolder ) : '';

		// Set FTP credentials, if given
		jimport('joomla.client.helper');
		JClientHelper::setCredentialsFromRequest('ftp');

		// Make the filename safe
		jimport('joomla.filesystem.file');
		$file['name']	= JFile::makeSafe($file['name']);

		// After making the filename safe, and the first character begins with . , we need to rename this file. Perhaps it's a unicode character
		$file['name']	= trim( $file['name'] );
		$filename	= strtolower( $file['name'] );

		if(strpos( $filename , '.' ) === false )
		{
		    $filename	= JFactory::getDate()->toFormat( "%Y-%m-%d-%H-%M-%S" ) . '.' . $filename;
		}
		else if( strpos( $filename , '.' ) == 0 )
		{
			$filename	= JFactory::getDate()->toFormat( "%Y-%m-%d-%H-%M-%S" ) . $filename;
		}
		
		$storagePath 	= JPath::clean( $folder . DS . $filename );

		// @task: try to rename the file if another image with the same name exists
		if( JFile::exists( $storagePath ) )
		{
			$i	= 1;
			while( JFile::exists( $storagePath ) )
			{
				$tmpName	= $i . '_' . $filename;
				$storagePath	= JPath::clean( $folder . DS . $tmpName );
				$i++;
			}
			$filename	= $tmpName;
		}

		$msg		= $this->getMessageObj();

		$file['name']	= $filename;
		$allowed		= EasyImageHelper::canUploadFile( $file , $msg );

		if( $allowed !== true )
		{
			return $this->output( $msg );
		}

		if( $this->isImage( $file ) )
		{
			$response	= $this->uploadImage( $folder , $filename , $file , $baseUri , $storagePath , $subfolder );
		}
		else
		{
			$response	= $this->uploadFile( $folder , $filename , $file , $baseUri ,  $storagePath , $subfolder );
		}

		return $this->output( $response );
	}

	private function getMessageObj( $code = '' , $message = '', $item = false )
	{
		$obj			= new stdClass();
		$obj->code		= $code;
		$obj->message	= $message;
		
		if( $item )
		{
			$obj->item	= $item;
		}
		
		return $obj;
	}
	
	private function output( $response )
	{
		include_once( EBLOG_CLASSES . DS . 'json.php' );
		$json	= new Services_JSON();
		echo $json->encode( $response );
		exit;
	}

	public function uploadFile( $folder , $filename , $file , $baseUri , $storagePath , $subfolder )
	{
		// Copy the file into the appropriate location.
		$source		= $file[ 'tmp_name' ];
		$dest		= $folder . DS . $filename;
		
		if( !JFile::copy( $source , $dest ) )
		{
			return $this->getMessageObj( EBLOG_MEDIA_PERMISSION_ERROR , JText::_( 'COM_EASYBLOG_IMAGE_MANAGER_UPLOAD_ERROR' ) );
		}

		return $this->getMessageObj( EBLOG_MEDIA_UPLOAD_SUCCESS , 
									JText::_( 'COM_EASYBLOG_IMAGE_MANAGER_UPLOAD_SUCCESS' ) , 
									EasyBlogHelper::getHelper( 'ImageData' )->getObject( $folder , $filename , $baseUri , $subfolder ) 
			);
	}
	
	public function uploadImage( $folder , $filename , $file , $baseUri , $storagePath , $subfolder )
	{
		$config			= EasyBlogHelper::getConfig();

		if (isset($file['name']))
		{
			$maxWidth	= $config->get('main_maximum_image_width');
			$maxHeight	= $config->get('main_maximum_image_height');
						
			if($config->get('main_resize_image_on_upload' ) || (empty($maxWidth) && empty($maxHeight)))
			{
				$image = new SimpleImage();
				$image->load($file['tmp_name']);
				$image->resizeToFit($maxWidth, $maxHeight);

				$uploadStatus = $image->save( $storagePath , $image->image_type , $config->get( 'main_upload_quality' ) );
			} else {
				$uploadStatus = JFile::upload($file['tmp_name'], $storagePath);
			}

			// @task: thumbnail's file name
			$storagePathThumb	= JPath::clean( $folder . DS . EBLOG_MEDIA_THUMBNAIL_PREFIX . $filename );

			// @rule: Generate a thumbnail for each uploaded images
			$image = new SimpleImage();
			$image->load( $storagePath );

			$fileWidth	= $image->getWidth();
			$maxWidth	= EBLOG_THUMB_WIDTH;

			if( $fileWidth > $maxWidth)
			{
				$image->resizeToWidth( $maxWidth );
				$image->save( $storagePathThumb , $image->image_type , $config->get( 'main_upload_quality' ) );
			}
			else
			{
				JFile::copy( $storagePath , $storagePathThumb );
			}
			
			if( !$uploadStatus )
			{
				return $this->getMessageObj( EBLOG_MEDIA_PERMISSION_ERROR , JText::_( 'COM_EASYBLOG_IMAGE_MANAGER_UPLOAD_ERROR' ) );
			}
			else
			{
			    // file uploaded. Now we test if the index.html was there or not.
			    // if not, copy from easyblog root into this folder
			    if(! JFile::exists( $folder . DS . 'index.html' ) )
			    {
			        $targetFile = JPATH_ROOT . DS . 'components' . DS . 'com_easyblog' . DS . 'index.html';
			        $destFile   = $folder . DS .'index.html';
			        
			        if( JFile::exists( $targetFile ) )
			        {
			        	JFile::copy( $targetFile, $destFile );
			        }
			    }

				return $this->getMessageObj( EBLOG_MEDIA_UPLOAD_SUCCESS , 
											JText::_( 'COM_EASYBLOG_IMAGE_MANAGER_UPLOAD_SUCCESS' ) , 
											EasyBlogHelper::getHelper( 'ImageData' )->getObject( $folder , $filename , $baseUri , $subfolder ) 
					);
			}
		}
		else
		{
			return $this->getMessageObj( EBLOG_MEDIA_TRANSPORT_ERROR , JText::_( 'COM_EASYBLOG_MEDIA_MANAGER_NO_UPLOAD_FILE' ) );
		}
		
		return $response;
	}
	
	/**
	 * Returns the file type
	 */	 	
	private function isImage( $file )
	{
		
		if( @getimagesize( $file['tmp_name'] ) === false )
		{
			return false;
		}
		
		return true;
	}
}
