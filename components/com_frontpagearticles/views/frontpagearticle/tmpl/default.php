
<?php 
/**
 * @version     $Id: default.php  2010-10-23 04:13:25Z $
 * @package     RRC
 * @subpackage  com_frontpagearticles
 * @author      ITOffshore Nepal
 * @copyright   Copyright (C) 2011 ITOffshore Nepal.
 * @license     ITOffshore Nepal
 **/
// no direct access
defined('_JEXEC') or die('Restricted access');

$item = $this->item;
$button1_exist	= $item->button_label1 && $item->button_link1;
$button2_exist	= $item->button_label2 && $item->button_link2;
$text_exist		= $item->text && $item->text_link;

?>
<div class="item-page-fullwidth">
	<h2><a href="#"><?php echo $item->title;?></a></h2>
	<?php if($item->filename || $button1_exist || $button2_exist || $text_exist): ?>
	    <div class="product-thumbnail">
	        <div class="thumbnail">
	            <img border="0" alt="" src="<?php echo JURI::base() . 'images/frontpagearticles/' . $item->filename; ?>"/>
	        </div>
	        <div class="button-wrapper">
	        	<?php if($button1_exist || $button2_exist): ?>
	            <div class="button-section">
	            	<?php if($button1_exist): ?>
	                	<a href="<?php echo $item->button_link1;?>" class="greybtn"><?php echo $item->button_label1;?></a>
	                <?php endif; ?>
	                <?php if($button2_exist): ?>
	                	<a href="<?php echo $item->button_link2;?>" class="bluebtn"><?php echo $item->button_label2;?></a>
	                <?php endif; ?>
	            </div>
	            <?php endif; ?>
	            <?php if($text_exist): ?>
	            	<a href="<?php echo $item->text_link;?>"><?php echo $item->text;?></a>
	            <?php endif; ?>
	        </div>
	    </div>
	<?php endif; ?>
    <?php echo $item->description;?>
</div>
