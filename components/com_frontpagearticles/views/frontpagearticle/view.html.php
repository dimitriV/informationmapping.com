<?php
/**
 * @version     $Id: frontpagearticles.php  2010-10-23 04:13:25Z $
 * @package     RRC
 * @subpackage  com_frontpagearticles
 * @author      ITOffshore Nepal
 * @copyright   Copyright (C) 2011 ITOffshore Nepal.
 * @license     ITOffshore Nepal
 **/

jimport( 'joomla.application.component.view');

/**
 * HTML View class for the Frontpagearticle Component
 *
 * @package		Information mapping
 * @subpackage	com_frontpagearticles
 */
class FrontpagearticlesViewFrontpagearticle extends JView
{
	protected $item;
	protected $params;
	protected $state;
	
	function display($tpl = null)
	{
		$this->item		= $this->get('Item');
		$this->state	= $this->get('State');
		$this->params	= $this->state->get('params');
        
		parent::display($tpl);
	}
}
?>
