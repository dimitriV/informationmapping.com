<?php
/**
 * @version     $Id: router.php  2010-12-31 04:13:25Z $
 * @package     Information mapping
 * @subpackage  com_frontpagearticles
 * @author      ITOffshore Nepal
 * @copyright   Copyright (C) 2005 - 2010 ITOffshore Nepal. All rights reserved.
 * @license     ITOffshore Nepal
 */

defined('_JEXEC') or die;

/**
 * Build the route for the  component
 *
 * @param	array	An array of URL arguments
 * @return	array	The URL arguments to use to assemble the subsequent URL.
 * @since	1.5
 */
function FrontpagearticlesBuildRoute(&$query)
{
	static $items;
    $segments = array();
    if (isset($query['id'])) {
        $id = $query['id'];
		$segments[] = $query['id'];
		unset($query['id']);
        
	}
    if(isset($query['view'])){
        $view = $query['view'];
        $segments[] = $query['view'];
        unset( $query['view'] );
    }
  
   
     
	return $segments;
}

/**
 * Parse the segments of a URL.
 *
 * @param	array	The segments of the URL to parse.
 *
 * @return	array	The URL attributes to be used by the application.
 * @since	1.5
 */
function FrontpagearticlesParseRoute($segments)
{
	$vars	= array();
    
    if(isset($segments[1]))
        $vars['view'] = $segments[1];
    
    if(isset($segments[0]))
        $vars['id'] = $segments[0];
      
	return $vars;
}

