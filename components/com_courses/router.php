<?php
/**
 * @version     $Id: router.php  2010-12-31 04:13:25Z $
 * @package     RRC
 * @subpackage  com_contactpages
 * @author      ITOffshore Nepal
 * @copyright   Copyright (C) 2005 - 2010 ITOffshore Nepal. All rights reserved.
 * @license     ITOffshore Nepal
 */

defined('_JEXEC') or die;

/**
 * Build the route for the com_contactpages component
 *
 * @param	array	An array of URL arguments
 * @return	array	The URL arguments to use to assemble the subsequent URL.
 * @since	1.5
 */
function CoursesBuildRoute(&$query)
{
	static $items;
    $segments = array();
    
    if(isset($query['view'])){
        $segments[] = $query['view'];
        unset( $query['view'] );
    }
    if (isset($query['id'])){
		$segments[] = $query['id'];
		unset($query['id']);
	}
    if (isset($query['result'])){
		$segments[] = $query['result'];
		unset($query['result']);
	}

	return $segments;
}

/**
 * Parse the segments of a URL.
 *
 * @param	array	The segments of the URL to parse.
 *
 * @return	array	The URL attributes to be used by the application.
 * @since	1.5
 */
function CoursesParseRoute($segments)
{
	$vars	= array();
   
    if(isset($segments[0])){
        $vars['view'] = $segments[0];
    }
	if(isset($segments[1])){
	    switch ($segments[1]) {
	       case 'success':
             $vars['result'] = $segments[1];
           break;
           default:
            $vars['id'] = $segments[1];
           break;
	    }
        
    }
	return $vars;
}
?>

