<?php

/**
 * @version     $Id: controller.php  2010-10-23 04:13:25Z $
 * @package    IM
 * @subpackage  com_courses
 * @author       Niroshan shrestha <niroshan.shrestha@itoffshorenepal.com>
 * @author       sanil shrestha <sanil.shrestha@itoffshorenepal.com>
 * @copyright  Copyright (C) 2011 ITOffshore Nepal.
 * @license     ITOffshore Nepal
 */
// No direct access
defined('_JEXEC') or die;

jimport('joomla.application.component.controller');

/* including pdf generation class */
require_once(JPATH_SITE . DIRECTORY_SEPARATOR . 'components' . DIRECTORY_SEPARATOR . 'com_courses' . DIRECTORY_SEPARATOR . 'tcpdf' . DIRECTORY_SEPARATOR . 'config' . DIRECTORY_SEPARATOR . 'lang' . DIRECTORY_SEPARATOR . 'eng.php');
require_once(JPATH_SITE . DIRECTORY_SEPARATOR . 'components' . DIRECTORY_SEPARATOR . 'com_courses' . DIRECTORY_SEPARATOR . 'tcpdf' . DIRECTORY_SEPARATOR . 'tcpdf.php');

/**
 * course Component Controller
 *
 * @package		Joomla.Site
 * @subpackage	Search
 * @since 1.5
 */
class CoursesController extends JControllerLegacy
{

	private function _downloadFile($fullPath)
	{

		// Must be fresh start
		if (headers_sent())
			die('Headers Sent');

		// Required for some browsers
		if (ini_get('zlib.output_compression'))
			ini_set('zlib.output_compression', 'Off');

		// File Exists?
		if (file_exists($fullPath)) {

			// Parse Info / Get Extension
			$fsize = filesize($fullPath);
			$path_parts = pathinfo($fullPath);
			$ext = strtolower($path_parts["extension"]);

			$ctype = mime_content_type($fullPath);

			header("Pragma: public"); // required
			header("Expires: 0");
			header("Cache-Control: must-revalidate, post-check=0, pre-check=0");
			header("Cache-Control: private", false); // required for certain browsers
			header("Content-Type: $ctype");
			header("Content-Disposition: attachment; filename=\"" . basename($fullPath) . "\";");
			header("Content-Transfer-Encoding: binary");
			header("Content-Length: " . $fsize);
			ob_clean();
			flush();
			readfile($fullPath);
		} else
			die('File Not Found');
	}

	/**
	 * auto populate course name on  change course language
	 * will be called by ajax request
	 */
	public function populateCoursename()
	{
		$courselangugeid = JRequest::getVar('courselangugeid');

		$model = $this->getModel('certification');

		$lists = $model->getCourseName($courselangugeid);

		// prepare option
		$option = '<option value="">' . JText::_('COM_COURSES_SELECT') . '</option>';
		foreach ($lists as $list) {
			$option .= '<option value="' . $list['id'] . '">' . $list['title'] . '</option>';
		}

		echo $option;
		exit;
	}

	/**
	 * auto populate instructor on change on partner
	 * will be called by ajax request
	 */
	public function populateInstructor()
	{
		$partnerid = JRequest::getVar('partnerid');

		$model = $this->getModel('certification');

		$lists = $model->getCourseInstructor($partnerid);

		// prepare option
		$option = '<option value="">' . JText::_('COM_COURSES_SELECT') . '</option>';
		foreach ($lists as $list) {
			$option .= '<option value="' . $list['id'] . '">' . $list['name'] . '</option>';
		}
		$option.= '<option value="other">' . JText::_('COM_COURSES_OTHER') . '</option>';

		echo $option;
		exit;
	}

	/**
	 * redirect if not loged in
	 * @return type bool
	 */
	public function authorizationCheck()
	{
		$user = & JFactory::getUser();

		if ($user->get('guest')) {
			$msg = JText::_('COM_COURSES_ONLY_AUTHORIZED_USER_CAN_VIEW_THIS_PAGE');
			$return = base64_encode(JURI::current());
			$url = JRoute::_("index.php?option=com_users&view=login&return=$return");
			$this->setRedirect($url, $msg, 'info');
		}

		return true;
	}

	/**
	 * default method to handle default task
	 */
	public function display()
	{
		$view = JRequest::getCmd('view', 'courses');
		$layout = JRequest::getCmd('layout', 'default');

		// authorization check
		if ($view == "certification") {
			$authorizationcheck = $this->authorizationCheck();
		}

		parent::display();
	}

	/**
	 * save certificaion request for valid request
	 * send email for student and instructor
	 */
	public function requestcertificate()
	{
		$authorizationcheck = $this->authorizationCheck();

		$model = $this->getModel('certification');

		if (!$model->saveCertificationRequest()) {
			$msg = $model->getError();
			$this->setRedirect(JRoute::_('index.php?option=com_courses&view=certification', false), $msg, 'error');
		} else {

			/* sending email  for instructor  & student */
			$instructorEmailTemplate = $model->getEmailTemplate('INSTRUCTOR_WAITING');
			$studentEmailTemplate = $model->getEmailTemplate('STUDENT_WAITING');

			/* get instructor information */
			$instructorInfo = $model->instructorInfo;

			/* get student info */
			$studentInfo = $model->studentInfo;

			// prepare and send email to instructor
			$sendEmailtoInstructor = $this->_prepareWaitingEmail($instructorEmailTemplate, $instructorInfo, $studentInfo);

			if ($sendEmailtoInstructor === true) {
				// after sending email to instructor send email to student
				$sendEmailtoStudent = $this->_prepareWaitingEmail($studentEmailTemplate, $instructorInfo, $studentInfo);

				if ($sendEmailtoStudent == true) {

					//$msg = JText::_('COM_COURSES_CERTIFICATION_REQUEST_SENT_SUCESSFULLY');
					$this->setRedirect(JRoute::_('index.php?option=com_courses&view=certification&result=success', false));
				} else {

					$msg = JText::_('COM_COURSES_CERTIFICATION_REQUEST_SENT_BUT_EMAIL_COULD_NOT_BE_SENT_TO_THE_STUDENT');
					$this->setRedirect(JRoute::_('index.php?option=com_courses', false), $msg, 'error');
				}
			} else {

				$msg = JText::_('COM_COURSES_CERTIFICATION_REQUEST_SENT_BUT_EMAIL_COULD_NOT_BE_SENT_TO_INSTRUCTOR');
				$this->setRedirect(JRoute::_('index.php?option=com_courses', false), $msg, 'error');
			}
		}
	}

	/**
	 * prepare email for sending the instructor and user when certificaion request is made sucessfully
	 * @param type $emailTemplate
	 * @param type $instructorInfo
	 * @param type $studentInfo
	 * @return type
	 */
	private function _prepareWaitingEmail($emailTemplate, $instructorInfo, $studentInfo)
	{
		$approveLink = '';
		$disapproveLink = '';
		$sender = array();

		/* access component paramter */
		$app = JFactory::getApplication('site');
		$params = & $app->getParams('com_courses');

		/* user or student  information */
		$userId = $studentInfo['id'];
		$studentName = $studentInfo['name'];
		$studentEmail = $studentInfo['email'];
		$studentKey = $studentInfo['studentkey'];
		$dateFrom = $studentInfo['datefrom'];
		$dateTo = $studentInfo['dateto'];
		$uniqId = $studentInfo['uniqid'];
		$coursename = $studentInfo['coursename'];

		/* instructor information */
		if (isset($instructorInfo['type']) && ($instructorInfo['type'] == 'other')) {
			// use default email of instructor specified from backend prefences
			$instructorName = $instructorInfo['name'];
			$instructorEmail = $params->get('defaultemail');
		} else {
			$instructorName = $instructorInfo['name'];
			$instructorTelephone = $instructorInfo['telephone'];
			$instructorEmail = $instructorInfo['email'];
		}


		/* prepare email body */
		$body = $emailTemplate['description'];

		/* choose the recipient and define link based on email template */
		switch ($emailTemplate['template_key']) {
			case 'INSTRUCTOR_WAITING':
				$sender = array($studentEmail, $studentName);
				$approveLink = '<a href="' . JRoute::_(JURI::base() . "index.php?option=com_courses&task=approve&studentkey=$studentKey&uniqid=$uniqId", false) . '">' . JText::_('APPROVE') . '</a>';
				$disapproveLink = '<a href="' . JRoute::_(JURI::base() . "index.php?option=com_courses&task=disapprove&studentkey=$studentKey&uniqid=$uniqId", false) . '">' . JText::_('DISAPPROVE') . '</a>';
				$recipient = array($instructorEmail);
				/* assume all instructor email must go to email defined from back end */
				//$recipient = array($params->get('defaultemail'));

				/* token replacment */
				$body = str_replace('[APPROVE_LINK]', $approveLink, $body);
				$body = str_replace('[DISAPPROVE_LINK]', $disapproveLink, $body);

				break;
			case 'STUDENT_WAITING':
				$sender = array($params->get('defaultemail'), $params->get('defaultname'));
				$recipient = array($studentEmail);
				break;
			default :
				/* for error handling */
				exit('Unknown Template key');
				break;
		}

		/* token replacment */
		$body = str_replace('[COURSE_NAME]', $coursename, $body);
		$body = str_replace('[COURSE_START_DATE]', $dateFrom, $body);
		$body = str_replace('[COURSE_END_DATE]', $dateTo, $body);

		$body = str_replace('[INSTRUCTOR_NAME]', $instructorName, $body);
		$body = str_replace('[INSTRUCTOR_EMAIL]', $instructorEmail, $body);

		$body = str_replace('[STUDENT_NAME]', $studentName, $body);
		$body = str_replace('[STUDENT_EMAIL]', $studentEmail, $body);
		$body = str_replace('[STUDENT_KEY]', $studentKey, $body);

		// current date..
		$date = & JFactory::getDate();
		$currentDate = $date->format('%A, %d %B %Y');

		//prepare email details
		$body = str_replace('[SUBMITED_DATE]', $currentDate, $body);
		$body = str_replace('[DATE]', $currentDate, $body);

		$subject = $emailTemplate['email_subject'];

		$sendEmail = $this->_sendEmail($sender, $recipient, $subject, $body);

		return $sendEmail;
	}

	/**
	 * send email
	 * @param type $recipient, email reciver
	 * @param type $subject, email subject
	 * @param type $body, email content
	 * @param type string, path to file , that should be attached
	 * @return type bool
	 */
	private function _sendEmail($sender, $recipient, $subject, $body, $attachment = '')
	{
		$mailer = & JFactory::getMailer();
		$config = & JFactory::getConfig();

		/* sender and recipient information */
		if (empty($sender)) {
			$sender = array(
				$config->getValue('config.mailfrom'),
				$config->getValue('config.fromname'));
		}

		$mailer->setSender($sender);

		$mailer->addRecipient($recipient);

		/* creating the email */
		$mailer->setSubject($subject);

		/* allow html in email */
		$mailer->isHTML(true);
		$mailer->Encoding = 'base64';
		/* set email body */
		$mailer->setBody($body);

		if ($attachment) {
			//$mailer->addAttachment($attachment);
		}

		/* send email */
		$send = & $mailer->Send();

		if ($send !== true) {
			//return $send->message;
			return false;
		} else {
			return true;
		}
	}

	/**
	 * validate the student key
	 * will call by ajax request
	 */
	public function validateStudentkey()
	{
		$model = $this->getModel('certification');

		$count = $model->isValidStudentkey();

		echo $count;

		exit;
	}

	/**
	 * prepare the disapproval or denay email template with token replacement
	 * @param $emailTemplate, email template created form back end
	 * @param $information, data array requred to prepare email template
	 * @return bool
	 */
	private function _disapproveEmail($emailTemplate, $information)
	{
		/* access component paramter */
		$app = JFactory::getApplication('site');
		$params = & $app->getParams('com_courses');

		/* for disapproval email for both case the sender is defiend form back end */
		$sender = array($params->get('defaultemail'), $params->get('defaultname'));

		/* prepare values to replace token */
		$instructorName = $information['instructorname'] ? $information['instructorname'] : $information['instructor_name'];
		$instructorEmail = $information['instructoremail'] ? $information['instructoremail'] : $params->get('defaultemail');
		$studentName = $information['first_name'] . ' ' . $information['last_name'];
		$studentEmail = $information['studentemail'];
		$submitedDate = date('F j, Y ', strtotime($information['created']));
		$studentKey = $information['student_key'];
		$coursename = $information['title'];
		$dataFrom = $information['start_date'];
		$dateTo = $information['end_date'];

		//preapre recipient
		switch ($emailTemplate['template_key']) {
			case 'STUDENT_DENIED':
				$recipient = array($studentEmail);
				break;
			case 'INSTRUCTOR_DENIED':
				$recipient = array($instructorEmail);
				/* assume all instructor email  must be sent to default email */
				//$recipient = array($params->get('defaultemail'));
				break;
			default :
				exit('Unknown Template key');
				break;
		}
		/* prepare body */

		$body = $emailTemplate['description'];

		/* token replacment */
		$body = str_replace('[COURSE_NAME]', $coursename, $body);
		$body = str_replace('[COURSE_START_DATE]', $dataFrom, $body);
		$body = str_replace('[COURSE_END_DATE]', $dateTo, $body);


		$body = str_replace('[INSTRUCTOR_NAME]', $instructorName, $body);
		$body = str_replace('[INSTRUCTOR_EMAIL]', $instructorEmail, $body);

		$body = str_replace('[STUDENT_NAME]', $studentName, $body);
		$body = str_replace('[STUDENT_EMAIL]', $studentEmail, $body);

		$body = str_replace('[SUBMITED_DATE]', $submitedDate, $body);
		$body = str_replace('[STUDENT_KEY]', $studentKey, $body);

		// current date..
		$date = & JFactory::getDate();
		$currentDate = $date->format('%A, %d %B %Y');

		$body = str_replace('[DATE]', $currentDate, $body);

		$subject = $emailTemplate['email_subject'];

		$sendEmail = $this->_sendEmail($sender, $recipient, $subject, $body);

		return $sendEmail;
	}

	/**
	 * Certificaion disapprove or denied
	 * will be called by the link clicked form the email of the instructor
	 * @param NULL
	 * @return redirect with appropriate msg
	 */
	public function disapprove()
	{
		$model = $this->getModel('certification');
		$studentKey = JRequest::getVar('studentkey', false);
		$filename = JRequest::getVar('uniqid', false);


		$result = $model->disapproveCertification($studentKey, $filename);

		if (is_array($result)) {
			// send email to student and instructor
			$instructorEmailTemplate = $model->getEmailTemplate('INSTRUCTOR_DENIED');
			$studentEmailTemplate = $model->getEmailTemplate('STUDENT_DENIED');

			$sendEmailInstructor = $this->_disapproveEmail($instructorEmailTemplate, $result);
			$sendEmailStudent = $this->_disapproveEmail($studentEmailTemplate, $result);

			if (($sendEmailInstructor === true) && ($sendEmailStudent === true)) {
				$msg = JText::_('COM_COURSES_CERTIFICATION_REQUEST_DENIED_SUCESSFULLY');
				/* get the landing page for sucessfull disapproval */
				$url = $model->getLandingPage('disapprove');
				if ($url) {
					$this->setRedirect(JRoute::_($url, false));
				} else {
					$this->setRedirect(JURI::base(), $msg);
				}
			} else {
				$msg = JText::_('COM_COURSES_CERTIFICATION_REQUEST_DENIED_SUCESSFULLY_BUT_FAILED_TO_SEND_EMAIL_TO_USER_OR_INSTRUCTOR');
				$this->setRedirect(JURI::base(), $msg, 'error');
			}
		} else {
			$msg = JText::_('COM_COURSES_CERTIFICATION_DENIED_PROCESS_FAILED');
			/* get the landing page for  disapproval process failed */
			$url = $model->getLandingPage('disapprove-failed');
			if ($url) {
				$this->setRedirect(JRoute::_($url, false));
			} else {
				$this->setRedirect(JURI::base(), $msg);
			}
		}
	}

	/**
	 * approve certificaion process by instructor
	 * will be invoke when link on instructor's email will be clicked
	 */
	public function approve()
	{
		$model = $this->getModel('certification');
		$studentKey = JRequest::getVar('studentkey', false);
		$filename = JRequest::getVar('uniqid', false);

		$result = $model->approveCertification($studentKey, $filename);

		if (is_array($result)) {
			/* generate certificaion on pdf format */
			$filename = $this->_preparePdfTemplate($result);
			// send email to student and instructor
			$instructorEmailTemplate = $model->getEmailTemplate('INSTRUCTOR_APPROVED');
			$studentEmailTemplate = $model->getEmailTemplate('STUDENT_APPROVED');

			$sendEmailInstructor = $this->_prepareApproveEmail($instructorEmailTemplate, $result);
			$sendEmailStudent = $this->_prepareApproveEmail($studentEmailTemplate, $result);

			if (($sendEmailInstructor === true) && ($sendEmailStudent === true)) {
				$msg = JText::_('COM_COURSES_CERTIFICATION_REQUEST_APPROVED_SUCESSFULLY');
				/* get the landing page for sucessfull approval */
				$url = $model->getLandingPage('approve');
				if ($url) {
					$this->setRedirect(JRoute::_($url, false));
				} else {
					$this->setRedirect(JURI::base(), $msg);
				}
			} else {
				$msg = JText::_('COM_COURSES_CERTIFICATION_REQUEST_APPROVED_BUT_EMAIL_COULD_NOT_BE_SENT_TO_USER_OR_INSTRUCTOR');
				$this->setRedirect(JURI::base(), $msg, 'error');
			}
		} else {
			$msg = JText::_('COM_COURSES_CERTIFICATION_REQUEST_APPROVAL_PROCESS_FAILED');
			/* get the landing page for approval process failed */
			$url = $model->getLandingPage('approve-failed');
			if ($url) {
				$this->setRedirect(JRoute::_($url, false));
			} else {
				$this->setRedirect(JURI::base(), $msg);
			}
		}
	}

	/**
	 * prepare email , when certificaion is approved by instructor
	 * @param $emailTemplate , template created from back end
	 * @param nformation, information required to prepare the complete email template
	 * @return redirect with appropriate msg
	 */
	private function _prepareApproveEmail($emailTemplate, $information)
	{
		/* access component paramter */
		$app = JFactory::getApplication('site');
		$params = & $app->getParams('com_courses');

		/* for disapproval email for both case the sender is defiend form back end */
		$sender = array($params->get('defaultemail'), $params->get('defaultname'));

		/* prepare the values to replace token */
		$attachment = '';
		$downloadURI = JURI::base() . 'index.php?option=com_courses&view=certification&Itemid=' . JRequest::getVar('Itemid') . '&lang=' . JRequest::getVar('lang');
		$downloadLink = '<a href="' . $downloadURI . '">' . JText::_('COM_COURSES_DOWNLOAD_CERTIFICATE') . '</a>';
		$instructorName = $information['instructorname'] ? $information['instructorname'] : $information['instructor_name'];
		$instructorEmail = $information['instructoremail'] ? $information['instructoremail'] : $params->get('defaultemail');
		$studentName = $information['first_name'] . ' ' . $information['last_name'];
		$studentEmail = $information['studentemail'];
		$submitedDate = date('F j, Y ', strtotime($information['created']));
		$studentKey = $information['student_key'];

		$coursename = $information['title'];
		$dataFrom = $information['start_date'];
		$dateTo = $information['end_date'];

		/* prepare body */
		$body = $emailTemplate['description'];

		//preapre recipient
		switch ($emailTemplate['template_key']) {
			case 'STUDENT_APPROVED':
				$attachment = JPATH_BASE.'/coursecertificate/'.$information['filename'];
				$recipient = array($studentEmail);

				/* token replacment */
				$body = str_replace('[DOWNLOAD_LINK]', $downloadLink, $body);
				break;
			case 'INSTRUCTOR_APPROVED':
				$recipient = array($instructorEmail);
				/* assume all instructor email  must be sent to default email */
				//$recipient = array($params->get('defaultemail'));
				break;
			default :
				exit('Unknown Template key');
				break;
		}


		/* token replacment */
		$body = str_replace('[COURSE_NAME]', $coursename, $body);
		$body = str_replace('[COURSE_START_DATE]', $dataFrom, $body);
		$body = str_replace('[COURSE_END_DATE]', $dateTo, $body);

		$body = str_replace('[INSTRUCTOR_NAME]', $instructorName, $body);
		$body = str_replace('[INSTRUCTOR_EMAIL]', $instructorEmail, $body);

		$body = str_replace('[STUDENT_NAME]', $studentName, $body);
		$body = str_replace('[STUDENT_EMAIL]', $studentEmail, $body);

		$body = str_replace('[SUBMITED_DATE]', $submitedDate, $body);
		$body = str_replace('[STUDENT_KEY]', $studentKey, $body);

		// current date..
		$date = & JFactory::getDate();
		$currentDate = $date->format('%A, %d %B %Y');

		$body = str_replace('[DATE]', $currentDate, $body);

		$subject = $emailTemplate['email_subject'];

		$sendEmail = $this->_sendEmail($sender, $recipient, $subject, $body, $attachment);

		return $sendEmail;
	}

	/**
	 * prepare pdf template
	 * @param type $information
	 * @return type filename or false
	 */
	private function _preparePdfTemplate($information)
	{
		/* access component paramter */
		$app = JFactory::getApplication('site');
		$params = & $app->getParams('com_courses');

		$filename = $information['filename'];
		$instructorName = $information['instructorname'] ? $information['instructorname'] : $information['instructor_name'];
		$instructorEmail = $information['instructoremail'] ? $information['instructoremail'] : $params->get('defaultemail');

		$pdfTemplate = array();

		$pdfTemplate['full_name'] = $information['first_name'] . ' ' . $information['last_name'];

		$pdfTemplate['completion_date'] = $information['date_to'];

		$pdfTemplate['instructor_name'] = $instructorName;

		$pdfTemplate['course_name'] = $information['title'];

		return $this->_generatePdf($pdfTemplate, $filename);
	}

	/**
	 * generate pdf
	 * @param type $pdfTemplate
	 * @param type $filename
	 * @return type filename or bool
	 */
	private function _generatePdf($pdfTemplate, $filename)
	{
			// create new PDF document
		$pdf = new TCPDF(PDF_PAGE_ORIENTATION, PDF_UNIT, PDF_PAGE_FORMAT, true, 'UTF-8', false);

		// set document information
		$pdf->SetCreator(PDF_CREATOR);


		// set header and footer fonts
		$pdf->setHeaderFont(Array(PDF_FONT_NAME_MAIN, '', PDF_FONT_SIZE_MAIN));

		// set default monospaced font
		$pdf->SetDefaultMonospacedFont(PDF_FONT_MONOSPACED);

		//set margins
		//$pdf->SetMargins(PDF_MARGIN_LEFT, PDF_MARGIN_TOP, PDF_MARGIN_RIGHT);
		$pdf->SetMargins(0, 0, 0);
		$pdf->SetHeaderMargin(0);
		$pdf->SetFooterMargin(0);

		$pdf->setPrintHeader(false);

		// remove default footer
		$pdf->setPrintFooter(false);

		//set auto page breaks
		$pdf->SetAutoPageBreak(TRUE, PDF_MARGIN_BOTTOM);

		//set image scale factor
		$pdf->setImageScale(PDF_IMAGE_SCALE_RATIO);

		// add a page
		$pdf->AddPage();

		$bMargin = $pdf->getBreakMargin();

		$auto_page_break = $pdf->getAutoPageBreak();

		$pdf->SetAutoPageBreak(false, 0);

		$img_file = JPATH_SITE.'/components/com_courses/tcpdf/images/certificate.png';
		$pdf->Image($img_file, 0, 0, 310, 200, '', '', 'C', true, 200, 'C', false, false, 0, 0);

		$pdf->SetTextColor(0, 0, 0);

		$pdf->SetFont('helveticaB', '', 15);

		$pdf->writeHTMLCell(140, 10, 77, 75, $pdfTemplate['full_name'], 0, 0, false, true, 'C', true);

		$pdf->SetFont('helveticaB', '', 16);
		$pdf->SetTextColor(23, 159, 199);

		$pdf->writeHTMLCell(135, 10, 78, 56, "Information Mapping International certifies that", 0, 0, false, true, 'C', true);
		$pdf->writeHTMLCell(145, 10, 64, 91, "has successfully completed the Information Mapping", 0, 0, false, true, 'R', true);

		$pdf->writeHTMLCell(35, 10, 211, 91, "course", 0, 0, false, true, 'L', true);

		$pdf->SetFont('helveticaB', '', 16);
		$pdf->writeHTMLCell(160, 10, 64, 98, $pdfTemplate['course_name'], 0, 0, false, true, 'C', true);

		$pdf->SetTextColor(0, 0, 0);
		$pdf->SetFont('helveticaB', '', 12);
		$pdf->writeHTMLCell(78, 10, 124, 122, $pdfTemplate['completion_date'], 0, 0, false, true, 'C', true);

		/*
		$pdf->SetTextColor(15, 36, 62);
		$pdf->SetFont('helveticaB', '', 12);
		$pdf->writeHTMLCell(78, 10, 50, 123, "Date of Completion:", 0, 0, false, true, 'R', true);
		 */



		//$pdf->SetTextColor(31, 51, 105);
		//$pdf->writeHTMLCell(140, 10, 122, 174, $pdfTemplate['instructor_name'], 0, 0, false, true, 'C', true);

		$filename = $filename ? $filename : uniqid() . '.pdf';

		$destination = JPATH_SITE.'/coursecertificate/'.$filename;

		$createPdf = $pdf->Output($destination, 'F');

		if ($createPdf) {
			return $filename;
		} else {
			return false;
		}
	}

}
