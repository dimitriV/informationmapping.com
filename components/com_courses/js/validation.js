function checkInstructor(instructor) {
	if(instructor=='other') {
		jQuery('#otherpartner').fadeIn('slow');
	} else {
		jQuery('#otherpartner').fadeOut('slow');
	}
}

jQuery(document).ready(function(){
	//global vars
	var firstname = jQuery("#first_name");
	var firstnameInfo = jQuery("#first_name_error");

	var lastname = jQuery("#last_name");
	var lastnameInfo = jQuery("#last_name_error");

	var studentkey = jQuery('#student_key');
	var studentkeyInfo = jQuery('#student_key_error');

	var courselanguage = jQuery('#courselanguage');
	var courselanguageInfo = jQuery('#courselanguage_error');

	var courseNames = jQuery('#courseNames');
	var courseNamesInfo = jQuery('#courseNames_error');

	var partner = jQuery('#partner_id');
	var partnerInfo = jQuery('#partner_id_error');

	var courseInstructor = jQuery('#courseInstructor');
	var courseInstructorInfo = jQuery('#courseInstructor_error');

	var instructor = jQuery('#instructor_name');
	var instructorInfo = jQuery('#instructor_name_error');

	var datefrom = jQuery('#date_from');
	var datefromInfo = jQuery('#date_from_error');
	var datefromInfoInvalid = jQuery('#date_from_invalid_error');
	
	var dateto = jQuery('#date_to');
	var datetoInfo = jQuery('#date_to_error');
	var datetoInfoInvalid = jQuery('#date_to_invalid_error');
	//var datesInfo = jQuery('#dates_error');

	var suggestion = jQuery('#submission-suggestion');
	
	// validate first name
	function validateFirstName(){
		var a = firstname.val();
		if(a.length > 0){
			firstnameInfo.addClass("certification-hide");
			return true;
		} else {
			firstnameInfo.removeClass("certification-hide");
			return false;
		}
	}

	//validation functions
	function validateLastName(){
		var a = lastname.val();
		if(a.length > 0){
			lastnameInfo.addClass("certification-hide");
			return true;
		} else {
			lastnameInfo.removeClass("certification-hide");
			return false;
		}
	}

	function validateCourseLanguage() {
		//testing regular expression
		var a = courselanguage.val();
		var filter = /^[0-9]+$/;
		if(filter.test(a)){
			courselanguageInfo.addClass("certification-hide");
			return true;
		} else {
			courselanguageInfo.removeClass("certification-hide");
			return false;
		}
	}

	function validateCourseName() {
		//testing regular expression
		var a = courseNames.val();
		var filter = /^[0-9]+$/;
		if(filter.test(a)){
			courseNamesInfo.addClass("certification-hide");
			return true;
		} else {
			courseNamesInfo.removeClass("certification-hide");
			return false;
		}
	}

	function validatePartner() {
		//testing regular expression
		var a = partner.val();
		var filter = /^[0-9]+$/;
		if(filter.test(a)){
			partnerInfo.addClass("certification-hide");
			return true;
		} else {
			partnerInfo.removeClass("certification-hide");
			return false;
		}
	}

	function validateCourseInstructor() {
		var a = courseInstructor.val();
		var filter = /^[0-9]+|other$/;
		if(filter.test(a)){
			courseInstructorInfo.addClass("certification-hide");
			return true;
		} else {
			courseInstructorInfo.removeClass("certification-hide");
			return false;
		}
	}

	function validateInstructorName() {
		if(courseInstructor.val()!='other') {
			return true;
		}
		var a = instructor.val();
		if(a.length > 0){
			instructorInfo.addClass("certification-hide");
			return true;
		} else {
			instructorInfo.removeClass("certification-hide");
			return false;
		}
	}

	function validateDateFrom() {
		//if it's NOT valid
		if(datefrom.val().length < 4){
			//datefrom.removeClass("certification-hide");
			datefromInfo.removeClass("certification-hide");
			return false;
		}
		//if it's valid
		else{
			datefromInfo.addClass("certification-hide");
			var date_from = new Date(datefrom.val());
			var today = new Date();
			if (date_from > today) {
				datefromInfoInvalid.removeClass("certification-hide");
				return false;
			}
			else{
				datefromInfoInvalid.addClass("certification-hide");
				return true;
			}
		}
	}

	function validateDateto(){
		//if it's NOT valid
		if(dateto.val().length < 4){
			datetoInfo.removeClass("certification-hide");
			return false;
		}
		//if it's valid
		else{
			datetoInfo.addClass("certification-hide");
			var date_to = new Date(dateto.val());
			var today = new Date();
			if (date_to > today) {
				datetoInfoInvalid.removeClass("certification-hide");
				return false;
			}
			else{
				datetoInfoInvalid.addClass("certification-hide");
				return true;
			}
			
			return true;
		}
	}
	
	function validateDates(){
		//if(datefrom.val().length > 0 && dateto.val().length > 0){
		if(validateDateFrom() & validateDateto()){
			var dateFrom = new Date(datefrom.val());
			var dateTo = new Date(dateto.val());
			var dateDiff = dateFrom - dateTo;
			if(dateDiff > 0){
				datetoInfoInvalid.removeClass("certification-hide");
				return false;	
			}
			else{
				datetoInfoInvalid.addClass("certification-hide");
				return true;
			}
		}
		return false;		
	}
	
	function validateData() {
		if(validateFirstName() & validateLastName() & validateCourseLanguage() & validateCourseName() & validatePartner() & validateCourseInstructor() & validateInstructorName() & validateDates()){
			return true;
		}
		return false;
	}
	
	jQuery('#btnGenerateCertificate').click(function(e){
		jQuery('#student_key').fadeTo('slow',0.5);
		
		jQuery.post('index.php?option=com_courses&task=validateStudentkey', {studentkey:studentkey.val()}, function(count){
			jQuery('#student_key').fadeTo('slow', 1);
			if(count>=1) {
				studentkeyInfo.addClass("certification-hide");
				isValidKey = true;
			} else {
				studentkeyInfo.removeClass("certification-hide");
				isValidKey = false;
			}
			var isValidData = validateData();
			if(isValidData && isValidKey){
				suggestion.addClass("certification-hide");
				jQuery('#frmCertification').submit();
			}
			else{
				suggestion.removeClass("certification-hide");
			}
		});
	});
});
