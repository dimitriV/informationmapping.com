<?php
/**
 * @version     $Id: view.html.php  2010-10-23 04:13:25Z $
 * @package     IM 
 * @subpackage  com_partner
 * @author      ITOffshore Nepal
 * @copyright   Copyright (C) 2011 ITOffshore Nepal.
 * @license     ITOffshore Nepal
 */

jimport( 'joomla.application.component.view');

/**
 * HTML View class for the courses Component
 *
 * @package		
 * @subpackage	Components
 */
class CoursesViewDetail extends JView
{
	function display($tpl = null)
	{
		
		$this->state		= $this->get('State');
		$model 				= $this->getModel('Detail');
		$data				= $this->get('Result');
		$this->assignRef( 'data', $data );
		$params	= &$this->state->params;
		$this->pageclass_sfx = htmlspecialchars($params->get('pageclass_sfx'));
		
		// Add breadcrumb item
		$app	= JFactory::getApplication();
		$pathway = $app->getPathway();
		$pathway->addItem($this->data->title);
     
		parent::display($tpl);
	}
}
?>
