<?php 
/**
 * @version     $Id: default.php  2010-10-23 04:13:25Z $
 * @package     IM 
 * @subpackage  com_partner
 * @author      ITOffshore Nepal
 * @copyright   Copyright (C) 2011 ITOffshore Nepal.
 * @license     ITOffshore Nepal
 */
// no direct access
defined('_JEXEC') or die('Restricted access');
?>

<?php 
	$currenturl =& JFactory::getURI(); 
	$baseurl = JURI::base(); 
	$mainframe = JFactory::getApplication();
    $isSef = $mainframe->getCfg('sef');  
    $urllink =JRoute::_('index.php?option=com_courses&view=detail&id='.$this->data->id);
    
	    if($isSef){
		     $link = $urllink.'?tmpl=component&amp;print=1&amp;layout=default';
		     }
	     
   		 else{
	        $link = $urllink.'&amp;tmpl=component&amp;print=1&amp;layout=default';
	        }

?>

<div class="partner-detail<?php echo $this->pageclass_sfx;?>">
<ul class="actions">

<?php if(JRequest::getVar('print')){?>
    <li>
	    <a onclick="window.print();return false;" href="#">
	    <img alt="<?php echo JText::_('COM_COURSES_PRINT');?>" src="<?php echo $baseurl; ?>/media/system/images/printButton.png">
	    </a>
    </li>
	<?php }
	else
	{
	?>
    <li class="print-icon">
	    <a rel="nofollow" onclick="window.open(this.href,'win2','status=no,toolbar=no,scrollbars=yes,titlebar=no,menubar=no,resizable=yes,width=640,height=480,directories=no,location=no'); return false;" title="<?php echo JText::_('COM_COURSES_PRINT');?>" href="<?php echo $link; ?>">
	    <img alt="<?php echo JText::_('COM_COURSES_PRINT');?>" src="<?php echo $baseurl; ?>/media/system/images/printButton.png"></a>
    </li>
<?php 
}
?>
</ul>

<?php 
if($this->data){?>
<?php if(!JRequest::getVar('print')){?>
	<div class="partner-back-link">
		<a href="<?php echo JRoute::_('index.php?option=com_courses&view=partner&id='.$this->data->countryid);?>"><span>
		<?php echo JText::_('COM_COURSES_BACK'); ?></span></a>
	</div>
	<?php }
	?>	
	<div class="partner-info">
		<h2><?php echo $this->data->title; ?></h2>
		<?php echo $this->data->description;?>
	</div>
<?php
}

?>
</div>