<?php
// no direct access
defined('_JEXEC') or die('Restricted access');
?>

<div class="download-certificate-section">
<?php if ($this->params->get('show_page_heading', 1)) : ?>
	<h2>
	<?php echo $this->escape($this->params->get('page_heading')); ?>
	</h2>
<?php endif; ?>
	<?php if (is_array($this->certificateLists) && !empty($this->certificateLists)) : ?>
		<ul class="pdfcertificate-list">
			<?php foreach ($this->certificateLists as $certificate) : ?>
				<li class="download-certificate">
					<a href="<?php echo "index.php?option=com_courses&task=downloadpdf&filename=".$certificate['filename']; ?>"><?php echo $certificate['title']; ?></a>
				</li>
			<?php endforeach; ?>
		</ul>
	<?php else: ?>
		<div class="no-certificates"><?php echo JText::_('COM_COURSES_NO_CERTIFICATES'); ?></div>
	<?php endif; ?>
</div>