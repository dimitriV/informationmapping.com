<?php
/**
 * @version     $Id: default.php  2010-10-23 04:13:25Z $
 * @package     IM
 * @subpackage  com_partner
 * @author      ITOffshore Nepal
 * @copyright   Copyright (C) 2011 ITOffshore Nepal.
 * @license     ITOffshore Nepal
 */
// no direct access
defined('_JEXEC') or die('Restricted access');
/* access component paramter */
$app = JFactory::getApplication('site');
$params = & $app->getParams('com_courses');
?>
<?php if(JRequest::getVar('result')=='success'): ?>
<div class="certification">
    <?php echo $params->get('successfullmsg');  ?>
</div>
<?php else : ?>

<script type="text/javascript" src="<?php echo JURI::base() ?>components/com_courses/js/validation.js"></script>
<script type="text/javascript">
	function populateCoursename(courselangugeid) {
		jQuery.ajax({
			url: "index.php?option=com_courses&task=populateCoursename",
			data: "courselangugeid="+courselangugeid,
			success: function(html) {
				jQuery('#courseNames').html(html);
				jQuery('#uniform-courseNames span:first-child').text('<?php echo JText::_('COM_COURSES_SELECT'); ?>');
			}
		});
    }

    function populateInstructor(partnerid) {
		jQuery.ajax({
			url: "index.php?option=com_courses&task=populateInstructor",
			data: "partnerid="+partnerid,
			success: function(html) {
				jQuery('#courseInstructor').html(html);
				jQuery('#uniform-courseInstructor span:first-child').text('<?php echo JText::_('COM_COURSES_SELECT'); ?>');
			}
		});
    }
	var dd = new Date("March 08, 2012");
	//alert(dd.toDateString());
</script>
<div class="certification">
	<div class="certification-title">
        <?php if ($this->params->get('show_page_heading', 1)) : ?>
            <h2><?php echo $this->escape($this->params->get('page_heading')); ?></h2>
        <?php endif; ?>
		<h2><?php echo $params->get('formtitle'); ?></h2>
	</div>
	<div class="certification-description"><?php echo $params->get('formdescription'); ?></div>
	<form action="<?php echo JRoute::_('index.php?option=com_courses&view=certification'); ?>" method = "post" id="frmCertification" name="frmCertification">
		<dl class="group-one">
			<dt>
			<label><?php echo JText::_('COM_COURSES_FIRST_NAME'); ?></label>
			</dt>
			<dd><span class="certification-input-field">
					<input type="text" name="first_name"  id="first_name" value="<?php echo $this->firstName;?>"/>
				</span><span class="certification-info"><?php echo JTEXT::_('COM_COURSES_WILL_APPEAR_ON_YOUR_CERTIFICATE'); ?></span><span class="certification-hide certification-error" id="first_name_error"><?php echo JTEXT::_('COM_COURSES_INVALID_FIRST_NAME'); ?></span></dd>
			<dt>
			<label><?php echo JText::_('COM_COURSES_LAST_NAME'); ?></label>
			</dt>
			<dd><span class="certification-input-field">
					<input type="text" name="last_name"  id="last_name" value="<?php echo $this->lastName;?>"/>
				</span><span class="certification-info"><?php echo JTEXT::_('COM_COURSES_WILL_APPEAR_ON_YOUR_CERTIFICATE'); ?></span><span class="certification-hide certification-error" id="last_name_error"><?php echo JTEXT::_('COM_COURSES_INVALID_LAST_NAME'); ?></span></dd>
		</dl>
		<dl class="group-two">
			<dt>
			<label><?php echo JText::_('COM_COURSES_COURSE_LANGUAGES'); ?></label>
			</dt>
			<dd><span class="certification-select">
					<select onchange="populateCoursename(this.value)" id="courselanguage" name="course_language_id" class="styled" >
						<option value=""><?php echo JText::_('COM_COURSES_SELECT'); ?></option>
						<?php
						$option = '';
						foreach ($this->courseLanguages as $language) {
							$option .= '<option value="' . $language['id'] . '">' . $language['language_name'] . '</option>';
						}
						echo $option;
						?>
					</select>
				</span><span class="certification-hide certification-error" id="courselanguage_error"><?php echo JTEXT::_('COM_COURSES_SELECT_COURSE_LANGUAGE'); ?></span></dd>
			<dt>
			<label><?php echo JText::_('COM_COURSES_COURSE_NAME'); ?></label>
			</dt>
			<dd><span class="certification-select">
					<select id="courseNames" name="course_id" class="styled">
						<option value=""><?php echo JText::_('COM_COURSES_SELECT'); ?></option>
					</select>
				</span><span class="certification-hide certification-error" id="courseNames_error"><?php echo JTEXT::_('COM_COURSES_SELECT_COURSE_NAME'); ?></span></dd>
			<dt>
			<label><?php echo JText::_('COM_COURSES_IM_PARTNER'); ?></label>
			</dt>
			<dd><span class="certification-select">
					<select onchange="populateInstructor(this.value)" name="partner_id"  id="partner_id" class="styled">
						<option value=""><?php echo JText::_('COM_COURSES_SELECT'); ?></option>
						<?php
						$option = '';
						foreach ($this->courseImPartners as $courseImPartner) {

							$option .= '<option value="' . $courseImPartner['id'] . '">' . $courseImPartner['title'] . '</option>';
						}

						echo $option;
						?>
					</select>
				</span><span class="certification-hide certification-error" id="partner_id_error"><?php echo JTEXT::_('COM_COURSES_SELECT_COURSE_PARTNER'); ?></span></dd>
			<dt>
			<label><?php echo JText::_('COM_COURSES_INSTRUCTOR'); ?></label>
			</dt>
			<dd><span class="certification-select">
					<select id="courseInstructor" onchange ="checkInstructor(this.value)" name="instructor_id" class="styled" >
						<option value=""><?php echo JText::_('COM_COURSES_SELECT'); ?></option>
					</select>
				</span><span class="certification-hide certification-error" id="courseInstructor_error"><?php echo JTEXT::_('COM_COURSES_INVALID_INSTRUCTOR_NAME'); ?></span></dd>
			</dd>
		</dl>
		<dl style="display:none;" id="otherpartner">
			<dt>
			<label><?php echo JText::_('COM_COURSES_INSTRUCTOR_NAME'); ?></label>
			</dt>
			<dd>
				<span class="certification-input-field">
					<input type="text" name = "instructor_name" id="instructor_name"/><br>
				</span><span class="certification-hide certification-error" id="instructor_name_error"><?php echo JTEXT::_('COM_COURSES_INVALID_INSTRUCTOR_NAME'); ?></span>		</dd>
		</dl>
		<dl class="group-two">
			<dt>
			<label>
				<?php echo JText::_('COM_COURSES_STUDENT_KEY'); ?></label>
				</dt>
				<dd><span class="certification-input-field">
						<input type="text" name="student_key" id="student_key" />
					</span><span class="certification-info"><?php echo JTEXT::_('COM_COURSES_SEE_THE_BACKCOVER_OF_PARTICIPATION_GUIDE'); ?></span><span class="certification-hide certification-error" id="student_key_error"><?php echo JTEXT::_('COM_COURSES_INVALID_STUDENT_KEY'); ?></span></dd>
		</dl>
		<dl class="group-three">
			<dt>
			<label>
				<?php echo JText::_('COM_COURSES_COURSE_DATE'); ?></label>
				</dt>
            <dd>
                <span class="date"><?php echo JText::_('COM_COURSES_FROM'); ?></span><span class="date-picker"><?php echo JHTML::_('calendar', '', 'date_from', 'date_from', '%B %d, %Y', 'readonly'); ?></span>
                <span class="certification-hide certification-error date-error" id="date_from_error"><?php echo JTEXT::_('COM_COURSES_SELECT_DATE_FROM'); ?></span>
                <span class="certification-hide certification-error date-error" id="date_from_invalid_error"><?php echo JTEXT::_('COM_COURSES_INVALID_DATE_FROM'); ?></span>
                <span class="date"><?php echo JText::_('COM_COURSES_TO'); ?></span>
                <span class="date-picker"><?php echo JHTML::_('calendar', '', 'date_to', 'date_to', '%B %d, %Y', 'readonly'); ?></span>
                <span class="certification-hide certification-error date-error" id="date_to_error"><?php echo JTEXT::_('COM_COURSES_SELECT_DATE_TO'); ?></span>
                <span class="certification-hide certification-error date-error" id="date_to_invalid_error"><?php echo JTEXT::_('COM_COURSES_INVALID_DATE_TO'); ?></span>
                <!--<span class="certification-hide certification-error date-error" id="dates_error"><?php echo JTEXT::_('COM_COURSES_INVALID_DATE_TO'); ?></span>-->
            </dd>
		</dl>
		<dl class="group-three">
			<dt class="generate-certificate">
				<button type="button" name="btnGenerateCertificate" id="btnGenerateCertificate" class="btn btn-blue"><?php echo JText::_('COM_COURSES_GENERATE_CERTIFICATE'); ?></button>
			</dt>
			<!--<dd class="certification-info"><span><?php echo JTEXT::_('COM_COURSES_ALL_FIELDS_MUST_BE_FILLED_OUT_FOR_SUCESSFUL_SUBMISSION'); ?></span></dd>-->
		</dl>
		<input type="hidden" name="task" value = "requestcertificate" />
		<?php echo JHtml::_('form.token'); ?>
	</form>
</div>
<?php endif; ?>