<?php
/**
 * @version     $Id: view.html.php  2010-10-23 04:13:25Z $
 * @package     IM
 * @subpackage  com_partner
 * @author      ITOffshore Nepal
 * @copyright   Copyright (C) 2011 ITOffshore Nepal.
 * @license     ITOffshore Nepal
 */
 // no direct access
 defined('_JEXEC') or die('Restricted access');
jimport('joomla.application.component.view');

/**
 * HTML View class for the course component
 *
 * @package		certificaiton view
 * @subpackage	Componentstm
 */
class CoursesViewCertification extends JViewLegacy
{
	protected $params;
	protected $state;

	function display($tpl = null)
	{
		
		/*
			Added User credentials to show in form
			20160302 by Peter Geens
			BEGIN
		*/

		$user 		= JFactory::getUser();
		$profile 	= JUserHelper::getProfile($user->id);
		$firstName 	= $profile->magebridgefirstlast['firstname'];
		$lastName  	= $profile->magebridgefirstlast['lastname'];

		$this->assignRef('firstName', $firstName);
		$this->assignRef('lastName', $lastName);

		/*
			END 
			Added User credentials to show in form
			20160302 by Peter Geens
		*/

		$this->state	= $this->get('State');

		$courseLanguages = $this->get('CourseLanguages');
		$this->assignRef('courseLanguages', $courseLanguages);

		$courseNames = $this->get('CourseName');
		$this->assignRef('courseNames', $courseNames);

		$courseImPartners = $this->get('CourseImPartner');
		$this->assignRef('courseImPartners', $courseImPartners);

		$courseInstructors = $this->get('CourseInstructor');
		$this->assignRef('courseInstructors', $courseInstructors);

		// for pdf download link
		$certificateLists = $this->get('CertificateDownloadLink');
		$this->assignRef('certificateLists', $certificateLists);
		
		$this->params	= $this->state->get('params');
		
		$this->_prepareDocument();		

		parent::display($tpl);
	}
	
	/**
	 * Prepares the document
	 */
	protected function _prepareDocument()
	{
		$app	= JFactory::getApplication();
		$menus	= $app->getMenu();
		$menu	= $menus->getActive();
		if ($menu){
			$this->params->def('page_heading', $this->params->get('page_title', $menu->title));
		}
		else{
			$this->params->def('page_heading', JText::_('COM_COURSES_DOWNLOAD_CERTIFICATE'));
		}
	}

}
?>
