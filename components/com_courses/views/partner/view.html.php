<?php
/**
 * @version     $Id: view.html.php  2010-10-23 04:13:25Z $
 * @package     IM 
 * @subpackage  com_partner
 * @author      ITOffshore Nepal
 * @copyright   Copyright (C) 2011 ITOffshore Nepal.
 * @license     ITOffshore Nepal
 */

jimport( 'joomla.application.component.view');

/**
 * 
 *
 * @package		Joomla.Tutorials
 * @subpackage	Components
 */
class CoursesViewPartner extends JViewLegacy
{
	function display($tpl = null)
	{
		
		$this->state 		= $this->get('State');
		$data 				= $this->get( 'Result' );
		$params				= &$this->state->params;
		$countries 			= $this->get( 'Countries' );
		$this->assignRef( 'data',	$data );
		$this->assignRef( 'params',	$params );
		$this->assignRef( 'countries',	$countries );
		
		$this->pageclass_sfx = htmlspecialchars($params->get('pageclass_sfx'));
    
        require_once JPATH_COMPONENT_SITE .'/models/fields/country.php';
       	parent::display($tpl);
	}
}
?>
