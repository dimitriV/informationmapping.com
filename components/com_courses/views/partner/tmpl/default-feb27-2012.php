<?php 
/**
 * @version     $Id: default.php  2010-10-23 04:13:25Z $
 * @package     IM 
 * @subpackage  com_partner
 * @author      ITOffshore Nepal
 * @copyright   Copyright (C) 2011 ITOffshore Nepal.
 * @license     ITOffshore Nepal
 */

// no direct access
defined('_JEXEC') or die('Restricted access');
JHTML::_('behavior.mootools');
JHTML::_('behavior.modal'); 
?>
	
<script type="text/javascript">
	jQuery(document).ready(function(){
		jQuery('.acc_container').hide(); 
		jQuery('.acc_trigger').click(function(){
		if( jQuery(this).next().next().is(':hidden') ) { 
			jQuery(this).hide();
			jQuery(this).next().hide();
			jQuery(this).toggleClass('active').next().next().slideDown(); 
		}
			return false; 
		});

	});

	function countryname(countryid){
		countryid_value =countryid.value;
		window.location = countryid_value;
		exit;
	}
</script>

<?php 
$partners_image_path = JURI::root().'images/partners/';
$menu = &JSite::getMenu();
$active = $menu->getActive();

if($active){
	$itemid = $active->id;
}

$country_id = JRequest::getVar('id');
#$link = $country_id ? JRoute::_('index.php?option=com_courses&view=partner&id='.$country_id.'&Itemid='.$itemid, false) : '';
$i = 1;
$partners_count = count($this->data);
?>

<div class="partners-list<?php echo $this->pageclass_sfx;?>">
	<?php if($this->params->get('show_page_heading', 1)): ?>
		<h1><?php echo $this->params->get('page_heading'); ?></h1>
	<?php endif; ?>
	<h2><?php echo JText::_('COM_COURSES_PARTNER_VIEW_DEFAULT_TITLE'); ?></h2>
	
	<?php if(!empty($this->countries->country)): ?>
		<div class="partner-heading">
			<h2><?php echo $this->countries->country;?></h2>
		</div>
	<?php endif; ?>
	
	<div class="country-select">
		<form action="" method="get" name="adminForm" id="adminForm">
			<select name="country_id" id="country_id" class="inputbox styled" onchange="countryname(this);">
				<option value=""><?php echo ($country_id == '') ? JText::_('COM_COURSES_SELECT_YOUR_COUNTRY') : JText::_('COM_COURSES_SELECT_OTHER_COUNTRY');?></option>
				<?php echo JHtml::_('select.options', JFormFieldCountry::getOption(), 'value', 'text' );?>
			</select>
		</form>
	</div>

	<?php if($this->data): ?>
		<?php foreach($this->data as $val): ?>
			<div class="fracture-list">
	            <div class="fracture-list-wrapper<?php echo ($partners_count == $i ? ' last' : ''); ?>">
			        <h2 class="acc_trigger"><a href="#"><?php echo $val->title;?></a></h2>
		        	<span class="visit-site">
		        		<?php if($val->website): ?>
							<a href="<?php echo $val->website; ?>" target="_blank"><?php echo JText::_('COM_COURSES_VISIT_SITE');?></a>
						<?php endif; ?>
					</span>
			        <div class="acc_container">
			          	<div class="partners-logo">
			          	<?php 
			          	  $file = 'images/partners/'.$val->filename;
						  if(file_exists($file)){?>
							<img alt="<?php echo $val->title;?>" src="<?php echo $partners_image_path.$val->filename;?>"/>
							<?php 
							}
							else{
								echo $val->title;
								}
							?>
						</div>
						<div class="partners-info-wrapper">
							<div class="partners-info">
								<span class="partner-name"><?php echo $val->title; ?></span>
								<?php
									$address = array();
									if($val->address) $address[] = $val->address;
									if($val->city) $address[] = $val->city;
								?>
								<?php if(count($address)): ?>
									<span class="partner-address"><?php echo implode('<br />', $address); ?></span>
								<?php endif; ?>
								<?php if($val->website): ?>
									<span class="website-url">
	                                	<a href="<?php echo $val->website; ?>" target="_blank"><?php echo $val->website; ?></a>
									</span>
								<?php endif; ?>
							</div>
							<span class="more-info">
								<a href="<?php echo JRoute::_('index.php?option=com_courses&view=detail&id='.$val->id);?>" title="More Info">I</a>
							</span>
						</div>
			        </div>
			      </div>
              </div>
              <?php $i++; ?>
		<?php endforeach;
	endif;
	?>
</div>