<?php
/**
 *  download helper
 */
class downloadCertificateHelper
{
	/**
	 * check either the file belongs to user or not..
	 * @param type $filename
	 * @return type bool
	 */
	public static function allowDownload($filename)
	{
		$user = & JFactory::getUser();
		$user_id = $user->id;
		$db = & JFactory::getDBO();

		$query = " SELECT COUNT(id)
					FROM #__course_certificates
				 WHERE user_id = " . (int) $user_id . " AND  filename = '". $filename ."' ";

		$db->setQuery($query);

		$count = $db->loadResult();

		if ($count > 0) {
			return true;
		} else {
			return false;
		}
	}

	/*
	 * download pdf
	 */
	public static function downloadPdf($fullPath)
	{
		// Must be fresh start
		if (headers_sent())
			die('Headers Sent');

		// Required for some browsers
		if (ini_get('zlib.output_compression'))
			ini_set('zlib.output_compression', 'Off');
		
		$fullPath = JPATH_ROOT.'/'.$fullPath;
		// File Exists?
		if (file_exists($fullPath)) {

			// Parse Info / Get Extension
			$fsize = filesize($fullPath);
			$path_parts = pathinfo($fullPath);
			$ext = strtolower($path_parts["extension"]);

			//$ctype =  mime_content_type ($fullPath);
			// Determine Content Type
			switch ($ext) {
				case "pdf": $ctype = "application/pdf";
					break;
				case "exe": $ctype = "application/octet-stream";
					break;
				case "zip": $ctype = "application/zip";
					break;
				case "doc": $ctype = "application/msword";
					break;
				case "xls": $ctype = "application/vnd.ms-excel";
					break;
				case "ppt": $ctype = "application/vnd.ms-powerpoint";
					break;
				case "gif": $ctype = "image/gif";
					break;
				case "png": $ctype = "image/png";
					break;
				case "jpeg":
				case "jpg": $ctype = "image/jpg";
					break;
				default: $ctype = "application/force-download";
			}

			header("Pragma: public"); // required
			header("Expires: 0");
			header("Cache-Control: must-revalidate, post-check=0, pre-check=0");
			header("Cache-Control: private", false); // required for certain browsers
			header("Content-Type: $ctype");
			header("Content-Disposition: attachment; filename=\"" . basename($fullPath) . "\";");
			header("Content-Transfer-Encoding: binary");
			header("Content-Length: " . $fsize);
			ob_clean();
			flush();
			readfile($fullPath);
		}
		else{
			die('File Not Found');
		}
	}

}
