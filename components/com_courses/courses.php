<?php
/**
 * @version     $Id: courses.php  2010-10-23 04:13:25Z $
 * @package     IM
 * @subpackage  com_partner
 * @author      ITOffshore Nepal
 * @copyright   Copyright (C) 2011 ITOffshore Nepal.
 * @license     ITOffshore Nepal
 */

// no direct access
defined('_JEXEC') or die;

//increase memory limit required for pdf operation
ini_set('memory_limit','256M');

jimport('joomla.application.component.controller');
require_once JPATH_COMPONENT.'/helpers/download.php';

// Create the controller
$controller = JControllerLegacy::getInstance('Courses');

// handle download request...
if ((JRequest::getCmd('task') == 'downloadpdf') && (JRequest::getCmd('filename'))) {
	$controller->authorizationCheck();
	$filename = JRequest::getCmd('filename');
	$check = downloadCertificateHelper::allowDownload($filename);
	if($check){
		$fullPath = 'coursecertificate/'. $filename;
		downloadCertificateHelper::downloadPdf($fullPath);
	}
	else{
		// not autorized...
		$msg = JText::_('COM_COURSES_ONLY_AUTHORIZED_USER_CAN_VIEW_THIS_PAGE');
		die($msg);
	}
	exit;
}

// Perform the Request task
$controller->execute(JRequest::getCmd('task'));

// Redirect if set by the controller
$controller->redirect();

?>