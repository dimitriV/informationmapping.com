<?php

/**
 * @version     $Id: certification.php  2010-10-23 04:13:25Z $
 * @package     com_course
 * @subpackage  certification model
 * @author      sanil shrestha<sanil.shrestha@itoffshorenepal.com>
 * @copyright   Copyright (C) 2011 ITOffshore Nepal.
 * @license     ITOffshore Nepal
 */
// Check to ensure this file is included in Joomla!
defined('_JEXEC') or die('Restricted access');

jimport('joomla.application.component.model');

/**
 * Model class for the certification
 *
 * @package		course component
 * @subpackage	certificaiton view
 */
class CoursesModelCertification extends JModelLegacy
{

    public $studentInfo;
    public $instructorInfo;
    
    /**
	 * Method to auto-populate the model state.
	 *
	 * Note. Calling getState in this method will result in recursion.
	 *
	 * @since	1.6
	 */
	protected function populateState()
	{
		$app = JFactory::getApplication('site');
		// Load the parameters.
		$params = $app->getParams();
		$this->setState('params', $params);
	}
    
    /**
     * get Landing page according for successful approval or disapproval of the certification request
     * @param result
     * @return string 
     */
     public function getLandingPage($result='')
     {        
        // for fetching configuration
        $app = JFactory::getApplication('site');
        $params = & $app->getParams('com_courses');

        // get landing page id according to parameter passed
        switch ($result) {
            case 'approve':
               $itemId = $params->get('approved');
            break;
            case 'disapprove':
                $itemId = $params->get('disapproved');
            break;
            case 'approve-failed':
                $itemId = $params->get('approvedfailed');
            break;
            case 'disapprove-failed':
                $itemId = $params->get('disapprovedfailed');
            break;
            
            default:
                return false;
            break;
        }
        
        // if landing page not defined form back end...
        if(!$itemId) {
            return false;
        }
        $query = $this->_db->getQuery(true);
        //get menu link from itemId
        $query->select('link')
                ->from('#__menu')
                ->where('published = 1')
                ->where('id ='.(int)$itemId);
                
        $this->_db->setQuery($query);
        
        $link = $this->_db->loadResult();
        
        if($link) {
            // concat Itemid on link
            $link .= $link.'&Itemid='.(int)$itemId;
            return $link;
        } 
        
        return false;
        
        
     }

    /**
     * get published coure languages
     * @return type  array
     */
    public function getCourseLanguages()
    {
        $lang = & JFactory::getLanguage();
        $langTag = $lang->getTag();

        $query = $this->_db->getQuery(true);

        $query->select('id')
                ->select('language_name')
                ->from('#__course_courselanguages')
                ->where('state = 1')
                ->where('language in (' . '"' . $langTag . '"' . ',' . '"*"' . ')')
                ->order('language_name');


        $this->_db->setQuery($query);

        return $this->_db->loadAssocList();
    }
    /**
     * get published coures
     * @return type  array
     */
    public function getCourseName($id='')
    {
        $lang = & JFactory::getLanguage();
        $langTag = $lang->getTag();
        if (!$id) {
            return array();
        }
        $query = $this->_db->getQuery(true);

        $query->select('id ,courselanguageid, title')
                ->from('#__course_courses')
                ->where('state = 1')
                ->where('language in (' . '"' . $langTag . '"' . ',' . '"*"' . ')')
                ->where('courselanguageid=' . (int) $id)
                ->order('title');

        $this->_db->setQuery($query);

        return $this->_db->loadAssocList();
    }

    /**
     * get published coure partners
     * @return type  array
     */
    public function getCourseImPartner()
    {
        $lang = & JFactory::getLanguage();
        $langTag = $lang->getTag();
        $query = $this->_db->getQuery(true);

        $query->select('id')
                ->select('countryid')
                ->select('title')
                ->from('#__course_partners')
                ->where('state = 1')
                ->where('language in (' . '"' . $langTag . '"' . ',' . '"*"' . ')')
                ->order('title');

        $this->_db->setQuery($query);

        return $this->_db->loadAssocList();
    }

    /**
     * get published coure instructors
     * @return type  array
     */
    public function getCourseInstructor($partnerId="")
    {
        $lang = & JFactory::getLanguage();
        $langTag = $lang->getTag();
        if (!$partnerId) {
            return array();
        }
        $query = $this->_db->getQuery(true);

        $query->select('id, partnerid, name')
                ->from('#__course_instructors')
                ->where('state = 1')
                ->where('language in (' . '"' . $langTag . '"' . ',' . '"*"' . ')')
                ->where('partnerid=' . (int) $partnerId)
                ->order('name');
        $this->_db->setQuery($query);

        return $this->_db->loadAssocList();
    }

    /**
     * get email template according to template key
     * @param type $template_key, template key refering particular template
     * @return type array
     */
    public function getEmailTemplate($template_key)
    {
        $lang = & JFactory::getLanguage();
        $langTag = $lang->getTag();

        $query = $this->_db->getQuery(true);

        $query->select('email_subject , description, template_key')
                ->from('#__course_emailtemplates');

        if ($template_key) {
            $query->where("template_key='" . $template_key . "'");
        }

        $query->where('language="' . $langTag . '"');

        $this->_db->setQuery($query);

        return $this->_db->loadAssoc();
    }

    /**
     * check where key typed by student match with key stored on db
     * @return type string
     */
    public function isValidStudentkey()
    {
        $studentKey = JRequest::getVar('studentkey', false);
        if (!$studentKey) {
            return false;
        }

        $query = $this->_db->getQuery(true);

        $query->select('COUNT(studentkey)')
                ->from('#__course_studentkeys')
                ->where('studentkey = "' . trim($studentKey) . '"')
                ->where('state != "-2"')
                ->where('status = 0');

        $this->_db->setQuery($query);

        return $this->_db->loadResult();
    }

    /**
     * get instructor information by instructor id
     * @param type $instructorId
     * @return type
     */
    private function _getInstructorInfo($instructorId)
    {
        $query = $this->_db->getQuery(true);
        $query->select('id, name , telephone, email')
                ->from('#__course_instructors')
                ->where('id = ' . (int) $instructorId);
        $this->_db->setQuery($query);

        return $this->_db->loadAssoc();
    }

/**
 * Get course Name by Couse Id
 * @param type $courseId
 * @return type
 */
   private function courseTitleById($courseId)
   {
       $query = $this->_db->getQuery(true);

        $query->select('title')
                ->from('#__course_courses')
                ->where('id=' . (int) $courseId);

        $this->_db->setQuery($query);

        return $this->_db->loadResult();
   }
    /**
     * Save the certificaion request
     * @return type
     */
    public function saveCertificationRequest()
    {
        $user = & JFactory::getUser();
		
        $row = $this->getTable('Certificates', 'CoursesTable');
		
        $data = JRequest::get('post');
		
        //update created data
        $date = JFactory::getDate();
        $data['created'] = $date->format('Y-%m-%d %T');
        $data['date_from'] = date('Y-m-d', strtotime($data['date_from']));
        $data['date_to'] = date('Y-m-d', strtotime($data['date_to']));
		
		$courseName = $this->courseTitleById($data['course_id']);
		
        // generate unique id for security purpose
        $uniqId = uniqid();
		
        // set student information
        $this->studentInfo = array(
            'id' => $user->id,
            'name' => $data['first_name'] . ' ' . $data['last_name'],
            'email' => $user->email,
            'studentkey' => $data['student_key'],
            'datefrom' => $data['date_from'],
            'dateto' => $data['date_to'],
            'uniqid' => $uniqId,
            'created' => $data['created'],
	   'coursename' => $courseName,
        );

        // set instructor information
        if ($data['instructor_id'] == 'other') {
            $this->instructorInfo = array(
                'type' => 'other',
                'name' => $data['instructor_name'],
            );
        } else {
            $this->instructorInfo = $this->_getInstructorInfo($data['instructor_id']);
        }

        /* complete the data array */
        //track user id
        $data['user_id'] = $user->id;

        // set status as pending
        $data['status'] = 0;

        //set uniqueId as secerte unique key
        $data['uniqid'] = $uniqId;
		
		// validate the dates not to be future dates
		if (strtotime($data['date_from']) || strtotime($data['date_to'])) {
            //$this->setError($row->getError());
            //return false;
        }
		
        if (!$row->bind($data)) {
            $this->setError($row->getError());
            return false;
        }
        if (!$row->check()) {
            $this->setError($row->getError());
            return false;
        }
        if (!$row->store()) {
            $this->setError($row->getError());
            return false;
        }
        // change status of used student key

        $this->_disableUsedStudentKeys($data['student_key']);

        return true;
    }

    /**
     * change status of used key
     * so that it cant be used further
     * @param type $key
     * @return type
     */
    private function _disableUsedStudentKeys($key)
    {
    	$user = JFactory::getUser();
        $query = 'UPDATE #__course_studentkeys SET status=1 WHERE studentkey="' . $key . '"';
        $this->_db->setQuery($query);
        if (!$this->_db->query()) {
            return false;
        }
        return true;
    }

    /**
     * approve certification
     * @param type $studentKey
     * @param type $filename
     * @return type
     */
    public function approveCertification($studentKey, $uniqid)
    {
        if (!$studentKey || !$uniqid) {
            return false;
        }

        // checking the combination of the student key and uniqid i.e filename ...for now

        $query = $this->_db->getQuery(true);

        $query->select('c.id, c.user_id, c.first_name, c.last_name, c.instructor_id, c.instructor_name, c.created, DATE_FORMAT(c.date_to, "%M %d, %Y") date_to, c.date_from as start_date, c.date_to as end_date, c.student_key, i.name AS instructorname, i.email AS instructoremail, u.email as studentemail, co.title')
                ->from('#__course_certificates as c')
                ->join('LEFT', '`#__course_instructors` AS i ON i.id = c.instructor_id')
                ->join('LEFT', '`#__course_courses` AS co ON c.course_id = co.id')
                ->join('LEFT', '`#__users` AS u ON u.id = c.user_id')
                ->where('c.student_key = "' . trim($studentKey) . '"')
                ->where('c.uniqid = "' . $uniqid . '"')
                ->where('c.status = 0');

        $this->_db->setQuery($query);

        $row = $this->_db->loadAssoc();

        if (!is_array($row)) {
            return false;
        }
        // update the status to approve on certifactes table
        $pdfFileName = uniqid() . '.pdf';
        $query = "UPDATE #__course_certificates SET status = '1', filename = '" . $pdfFileName . "'  WHERE id =" . (int) $row['id'];
        $this->_db->setQuery($query);
        if (!$this->_db->query()) {
            return false;
        }
        // update row array...
        $row['filename'] = $pdfFileName;
        return $row;
    }

    /**
     * get certication download detials for logged in uesrs
     * @return type array
     */
    public function getCertificateDownloadLink()
    {

        $user = & JFactory::getUser();
        $query = $this->_db->getQuery(true);

        $query->select('c.id, c.course_id, c.status, c.student_key, c.filename, co.title')
                ->from('#__course_certificates as c')
                ->join('LEFT', '#__course_courses AS co ON co.id = c.course_id')
                ->where('c.status = 1')
                ->where('c.user_id =' . (int) $user->id);

        $this->_db->setQuery($query);
        return $this->_db->loadAssocList();
    }

    /**
     * disapprove certification
     * @param type $studentKey
     * @param type $filename
     * @return type
     */
    public function disapproveCertification($studentKey, $uniqid)
    {
        if (!$studentKey || !$uniqid) {
            return false;
        }

        // checking the combination of the student key and uniqid i.e filename ...for now

        $query = $this->_db->getQuery(true);

        $query->select('c.id, c.user_id,  DATE_FORMAT(c.date_to, "%M %d, %Y") date_to, c.date_from as start_date, c.date_to as end_date, c.first_name, c.last_name, c.instructor_id, c.instructor_name, c.created , c.student_key,  i.name AS instructorname, i.email AS instructoremail, u.email as studentemail, co.title')
                ->from('#__course_certificates as c')
                ->join('LEFT', '`#__course_instructors` AS i ON i.id = c.instructor_id')
	       ->join('LEFT', '`#__course_courses` AS co ON c.course_id = co.id')
                ->join('LEFT', '`#__users` AS u ON u.id = c.user_id')
                ->where('c.student_key = "' . trim($studentKey) . '"')
                ->where('c.uniqid = "' . $uniqid . '"')
                ->where('c.status = 0');

        $this->_db->setQuery($query);

        $row = $this->_db->loadAssoc();

        if (!is_array($row)) {
            return false;
        }

        // update the status to disapprove on certifactes table
        $query = "UPDATE #__course_certificates SET status = '-1' WHERE id =" . (int) $row['id'];
        $this->_db->setQuery($query);

        if (!$this->_db->query()) {
            return false;
        }

        return $row;
    }

}

?>
