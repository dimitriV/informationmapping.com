<?php

/**
 * @version     $Id: partner.php  2010-10-23 04:13:25Z $
 * @package     IM 
 * @subpackage  com_partner
 * @author      ITOffshore Nepal
 * @copyright   Copyright (C) 2011 ITOffshore Nepal.
 * @license     ITOffshore Nepal
 */
// Check to ensure this file is included in Joomla!
defined('_JEXEC') or die('Restricted access');
jimport('joomla.application.component.model');

class CoursesModelPartner extends JModelLegacy
{

    protected function populateState()
    {
        $app = JFactory::getApplication();
        $params = $app->getParams();
        $this->setState('params', $params);
    }

    function getResult()
    {
        $db = & JFactory::getDBO();
        $lang = & JFactory::getLanguage();
        $langTag = $lang->getTag();
        $id = JRequest::getVar('id', '0');
        $query = 'SELECT * FROM #__course_partners WHERE countryid=' . $id . ' AND state=1 AND language in (' . '"' . $langTag . '"' . ',' . '"*"' . ') ORDER BY ordering';
        $db->setQuery($query);
        $data = $db->loadObjectList();
        return $data;
    }

    function getCountries()
    {
       
        $db = & JFactory::getDBO();
        $lang = & JFactory::getLanguage();
        $langTag = $lang->getTag();
        $id = JRequest::getVar('id', '0');
        $query = 'SELECT * FROM #__course_countries WHERE state=1 AND language in (' . '"' . $langTag . '"' . ',' . '"*"' . ') AND  id=' . $id;
        $db->setQuery($query);
        $countries_title = $db->loadObject();
        return $countries_title;
    }

}

?>