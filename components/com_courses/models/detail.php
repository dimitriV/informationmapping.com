<?php

/**
 * @version     $Id: detail.php  2010-10-23 04:13:25Z $
 * @package     IM 
 * @subpackage  com_partner
 * @author      ITOffshore Nepal
 * @copyright   Copyright (C) 2011 ITOffshore Nepal.
 * @license     ITOffshore Nepal
 */
// Check to ensure this file is included in Joomla!
defined('_JEXEC') or die('Restricted access');
jimport('joomla.application.component.model');

class CoursesModelDetail extends JModel
{

    protected function populateState()
    {
        $app = JFactory::getApplication();
        $params = $app->getParams();
        $this->setState('params', $params);
    }

    function getResult()
    {
        $db = & JFactory::getDBO();
        $lang = & JFactory::getLanguage();
        $langTag = $lang->getTag();
        $id = JRequest::getVar('id');
        $query = 'SELECT * FROM #__course_partners where id=' . $id;
        $db->setQuery($query);
        $data = $db->loadObject();
        return $data;
    }

}

?>