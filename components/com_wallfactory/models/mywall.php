<?php
/*------------------------------------------------------------------------
com_wallfactory - Wall Factory
------------------------------------------------------------------------
author    TheFactory
copyright Copyright (C) 2010 SKEPSIS Consult SRL. All Rights Reserved.
@license - http://www.gnu.org/licenses/gpl-2.0.html GNU/GPL
Websites: http://www.thefactory.ro
Technical Support:  Forum - http://www.thefactory.ro/joomla-forum/
-------------------------------------------------------------------------*/

defined('_JEXEC') or die('Restricted access');

jimport('joomla.application.component.model');

class FrontendModelMywall extends JModel
{
  var $_settings;
  var $_alias;

  function __construct()
  {
    parent::__construct();

    $this->wallSettings = new wallSettings();
    $this->wallHelper = wallHelper::getInstance();

  }

  function getMyWall()
  {
    $user =& JFactory::getUser();
    $query = ' SELECT *'
           . ' FROM #__wallfactory_walls'
           . ' WHERE user_id = ' . $user->id;
    $this->_db->setQuery($query);

    return $this->_db->loadObject($query);
  }

  function isWallAvailable()
  {
    $user =& JFactory::getUser();

    if ($user->guest)
    {
      return false;
    }

    $title = JRequest::getVar('title', '', 'REQUEST', 'string');
    $this->setAlias($title);
    if ($title == '')
    {
      return false;
    }

    $query = ' SELECT COUNT(id)'
           . ' FROM #__wallfactory_walls'
           . ' WHERE title = "' . $title . '"'
           . ' OR user_id = ' . $user->id;
           
    $this->_db->setQuery($query);
    return !$this->_db->loadResult();
  }
  
  function setAlias($alias)
  {
    $this->_alias = $alias;
    $this->_data  = null;
  }

  function register()
  {
    $user =& JFactory::getUser();

    if ($user->guest)
    {
      return false;
    }

    $title = JRequest::getVar('title', '', 'REQUEST', 'string');

    if (!$this->isTitleAvailable())
    {
      return false;
    }
    
    $wall =& $this->getTable('wall');
    $date =& JFactory::getDate();

    $wall->user_id        = $user->id;
    $wall->published      = 1;
    $wall->title          = $title;
    $wall->alias          = $this->_alias;
    $wall->posts_per_page = 10;
    $wall->date_created     = $date->toMySQL();

    $wall->store();

    // Create user folder
    $users_folder = JPATH_SITE.DS.'components'.DS.'com_wallfactory'.DS.'storage'.DS.'users';
    $user_folder  = $users_folder.DS.$user->id;

    mkdir($user_folder, 0775);
    mkdir($user_folder.DS.'avatar', 0775);
    mkdir($user_folder.DS.'folder', 0775);
    mkdir($user_folder.DS.'folder'.DS.'root', 0775);

    return true;
  }
  
  function isTitleAvailable()
  {
    $user =& JFactory::getUser();

    if ($user->guest)
    {
      return false;
    }

    $title = JRequest::getVar('title', '', 'REQUEST', 'string');

    if ($title == '')
    {
      return false;
    }

    $query = ' SELECT COUNT(id)'
           . ' FROM #__wallfactory_walls'
           . ' WHERE title = "' . $title . '"'
           . ' OR user_id = ' . $user->id;
    $this->_db->setQuery($query);
    return !$this->_db->loadResult();
  }
  
  function getAllowedBlogger()
  {
    // Allow all wall owners
    if ($this->wallSettings->allowed_wallowners_all)
    {
      return true;
    }

    // Per user check
    $user =& JFactory::getUser();
    if (in_array($user->id, $this->wallSettings->allowed_wallowners_users))
    {
      return true;
    }

    // Per group check
    $acl =& JFactory::getACL();
    foreach ($this->wallSettings->allowed_wallowners_groups as $group)
    {
      if ($acl->is_group_child_of(intval($user->gid), $group))
      {
        return true;
      }
    }

    // No match found
    return false;
  }

  function getTotalComments()
  {
    $user =& JFactory::getUser();

    $query = ' SELECT COUNT(c.id)'
           . ' FROM #__wallfactory_comments c'
           . ' LEFT JOIN #__wallfactory_posts p ON p.id = c.post_id'
           . ' LEFT JOIN #__wallfactory_walls w ON w.id = p.wall_id'
           . ' WHERE w.user_id = ' . $user->id;
    $this->_db->setQuery($query);

    return $this->_db->loadResult();
  }
  
  function getTotalFollowers()
  {
    $user =& JFactory::getUser();

    $query = ' SELECT id'
           . ' FROM #__wallfactory_walls'
           . ' WHERE user_id = ' . $user->id;
    $this->_db->setQuery($query);
    $my_wall_id = $this->_db->loadResult();

    $query = ' SELECT COUNT(f.id)'
           . ' FROM #__wallfactory_followers f'
           . ' WHERE f.wall_id = ' . $my_wall_id;
    $this->_db->setQuery($query);

    return $this->_db->loadResult();
  }

  function getTotalFollowed()
  {
    $user =& JFactory::getUser();

    $query = ' SELECT COUNT(f.id)'
           . ' FROM #__wallfactory_followers f'
           . ' LEFT JOIN #__wallfactory_walls w ON w.id = f.wall_id'
           . ' WHERE f.user_id = ' . $user->id
           . ' AND w.published = 1';
    $this->_db->setQuery($query);

    return $this->_db->loadResult();
  }

  function getTotalNotifications()
  {
    $user =& JFactory::getUser();

    $query = ' SELECT COUNT(f.id)'
           . ' FROM #__wallfactory_followers f'
           . ' LEFT JOIN #__wallfactory_walls w ON w.id = f.wall_id'
           . ' WHERE f.user_id = ' . $user->id
           . ' AND notification = 1'
           . ' AND w.published = 1';
    $this->_db->setQuery($query);

    return $this->_db->loadResult();
  }

  
}