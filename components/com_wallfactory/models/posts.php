<?php
/*------------------------------------------------------------------------
com_wallfactory - Wall Factory
------------------------------------------------------------------------
author    TheFactory
copyright Copyright (C) 2010 SKEPSIS Consult SRL. All Rights Reserved.
@license - http://www.gnu.org/licenses/gpl-2.0.html GNU/GPL
Websites: http://www.thefactory.ro
Technical Support:  Forum - http://www.thefactory.ro/joomla-forum/
-------------------------------------------------------------------------*/

defined('_JEXEC') or die('Restricted access');

jimport('joomla.application.component.model');

class FrontendModelPosts extends JModel
{
  var $_data;
  var $_total;
  var $_pagination;
  var $_limit;
  var $_limitstart;
  var $_params;
  var $_totalcomments;
  var $_commentsdata;

  function __construct()
  {
    parent::__construct();

    $this->wallSettings = new wallSettings();
    $posts_per_page = $this->getSettings();
    $limit = ($posts_per_page != null) ? ($posts_per_page->posts_per_page) :10;
    
    global $mainframe, $option;
    
    $this->_limit      = JRequest::getVar('limit',  $limit, 'REQUEST', 'integer');
    $this->_limitstart = JRequest::getVar('limitstart', 0,  'REQUEST', 'integer');
 
    $this->_limitstart = ($this->_limit != 0 ? (floor($this->_limitstart / $this->_limit) * $this->_limit) : 0);
  }

  // Pagination
  function getData($wall_id = null)
  {
    if (empty($this->_data))
    {
      $query = $this->_buildQuery($wall_id);
      $this->_data = $this->_getList($query, $this->_limitstart, $this->_limit);
      
      return $this->_data;
    }
  
  }
  
  function _buildQuery($wall_id)
  {
    $user  =& JFactory::getUser();
    $where =  $this->_buildWhereCondition($wall_id);

    $query = ' SELECT p.id, p.wall_id, p.user_id, p.content, p.notification, p.date_created, p.date_updated, p.enable_comments, p.reported, '
	    	. ' IF(p.alias <> "", p.alias,"' . JText::_('Guest') . '") AS alias, '
	    	. ' (SELECT count( DISTINCT c.id ) FROM #__wallfactory_comments c WHERE c.post_id = p.id ) AS no, '
	    	. ' IF(u.username IS NULL,"' . JText::_('Guest') . '", u.username) AS username, '
	    	. ' u.id as wall_owner_id, w.alias AS wall_alias,'
           	. ' DATE_FORMAT(p.date_created, "%Y-%m-%d") AS date_spacer, IF(f.id, 1, 0) AS followed,'
           	. ' s.avatar_extension, s.name, s.avatar_type, s.allow_profile_view, '
           	. ' IF (w.user_id = ' . $user->id . ', 1, 0) AS my_post'
           	. ' FROM #__wallfactory_posts p'
           	. ' LEFT JOIN #__wallfactory_walls w ON w.id = p.wall_id'
           	. ' LEFT JOIN #__users u ON u.id = p.user_id'
           	. ' LEFT JOIN #__wallfactory_followers f ON w.id = f.wall_id AND f.user_id = ' . $user->id
           	. ' LEFT JOIN #__wallfactory_urls l ON l.post_id = p.id '
           	. ' LEFT JOIN #__wallfactory_media m ON m.post_id = p.id '
           	. ' LEFT JOIN #__wallfactory_members s ON s.user_id = p.user_id '
           	. $where
           	. ' GROUP BY p.id'
           	. ' ORDER BY p.date_created DESC';
           	
    return $query;
  }

  function _buildWhereCondition($wall_id)
  {
    $layout =& JRequest::getVar('layout', '', 'GET', 'string');
    $archive = 'archive' == $layout ? 1 : 0;
	$where = '';
	
    if ($wall_id)
    {
    	$where = ' WHERE w.published = 1';
	    $where .= ' AND w.id = ' . $wall_id;
    } else {
    	$where .= ' WHERE 1 ';
    }

    return $where;
  }

  function getTotal($wall_id)
  {
    if (empty($this->_total))
    {
      $query = $this->_buildQuery($wall_id);
      $this->_total = $this->_getListCount($query);
    }

    return $this->_total;
  }
  
  function getTotalPosts() {
  	if (!empty($this->_data))
	{
	   	$count = array();
      	foreach ($this->_data as $i => $post) { 
           $query = ' SELECT COUNT(distinct p.id) as posts_no '
      		. ' FROM #__wallfactory_posts p'
      		. ' WHERE p.user_id = ' . $post->user_id;
      		
	      $this->_db->setQuery($query);
	      $result = $this->_db->loadResult();
	      $count[$post->user_id] = $result;
      }
 
      return $count;			
	}
  }
	
  function getPagination($wall_id = null)
  {
    if (empty($this->_pagination))
    {
      jimport('joomla.html.pagination');
      $this->_pagination = new JPagination($this->getTotal($wall_id), $this->_limitstart, $this->_limit);
    }

    return $this->_pagination;
  }

  // Getters
  function getMyWall()
  {
    $user =& JFactory::getUser();
	
    $query = ' SELECT id, alias '
           . ' FROM #__wallfactory_walls'
           . ' WHERE user_id = ' . $user->id;
    
    $this->_db->setQuery($query);
    return $this->_db->loadObject();
    
  }
  
	function getComments($wall_id = null)
	{
	  $user     =& JFactory::getUser();
	  $database =& JFactory::getDBO();
	  	  
	  $alias =& JRequest::getVar('alias', '', 'GET', 'cmd');
  
      $where =  $this->_buildWhereCondition($wall_id);

      $query_posts = ' SELECT p.*, '
		   . ' c.user_id as comment_user_id, c.content, c.date_created as comment_date, '
		   . ' u.username, u.id as wall_owner_id, w.alias AS wall_alias,'
		   . ' s.avatar_extension, '
           . '   DATE_FORMAT(p.date_created, "%Y-%m-%d") AS date_spacer, IF(f.id, 1, 0) AS followed,'
           . '  IF (p.user_id = ' . $user->id . ', 1, 0) AS my_post'
           . ' FROM #__wallfactory_posts p'
           . ' LEFT JOIN #__wallfactory_walls w ON w.id = p.wall_id'
           . ' LEFT JOIN #__users u ON u.id = p.user_id'
           . ' LEFT JOIN #__wallfactory_followers f ON w.id = f.wall_id AND f.user_id = ' . $user->id
           . ' LEFT JOIN #__wallfactory_urls l ON l.post_id = p.id '
           . ' LEFT JOIN #__wallfactory_media m ON m.post_id = p.id '
           . ' LEFT JOIN #__wallfactory_comments c ON c.post_id = p.id '
           . ' LEFT JOIN #__wallfactory_members s ON s.user_id = c.user_id '
           . $where
           . ' GROUP BY p.id'
           . ' ORDER BY p.date_created DESC';
           
   	$this->_commentsdata = $this->_getList($query_posts, $this->_limitstart, $this->_limit);
   	
    $comments = array();
	$no_of_comments = $this->wallSettings->comments_per_page;

	$limit_condition =  ' LIMIT 0, '."$no_of_comments".' ';
	
    foreach ($this->_commentsdata as $i => $post) {
      	$query = ' SELECT c.id, c.post_id, c.user_id, c.content, c.date_created, c.author_alias, c.reported, s.avatar_type, s.avatar_extension '
      	   . ' , IF(u.username = "", u.username, "' . JText::_('Guest') . ' ") AS username '
           . ' FROM #__wallfactory_comments c'
           . ' LEFT JOIN #__wallfactory_posts p ON p.id = c.post_id'
           . ' LEFT JOIN #__wallfactory_walls w ON w.id = p.wall_id'
           . ' LEFT JOIN #__users u ON u.id = c.user_id'
           . ' LEFT JOIN #__wallfactory_members s ON s.user_id = c.user_id'
           . $where
           . ' AND c.post_id = '.$post->id
           . ' ORDER BY c.post_id, c.date_created DESC'
           . $limit_condition;
	    $this->_db->setQuery($query);
  
	    $comments[$post->id] = $this->_db->loadObjectList();
     
	}
    
	return $comments;
       
  }
  
  function getUrls($wall_id = null) {
		$user =& JFactory::getUser();
		$database =& JFactory::getDBO();
	  	  
	  	$alias =& JRequest::getVar('alias', '', 'GET', 'cmd');
		$where =  $this->_buildWhereCondition($wall_id);

    	$query_posts = ' SELECT p.*, '
    		. ' c.user_id as comment_user_id, c.content, c.date_created as comment_date, '
      		. ' u.username, u.id as wall_owner_id, w.alias AS wall_alias,'
           	. '   DATE_FORMAT(p.date_created, "%Y-%m-%d") AS date_spacer, IF(f.id, 1, 0) AS followed,'
           	. '  IF (p.user_id = ' . $user->id . ', 1, 0) AS my_post'
           	. ' FROM #__wallfactory_posts p'
           	. ' LEFT JOIN #__wallfactory_walls w ON w.id = p.wall_id'
           	. ' LEFT JOIN #__users u ON u.id = p.user_id'
           	. ' LEFT JOIN #__wallfactory_followers f ON w.id = f.wall_id AND f.user_id = ' . $user->id
           	. ' LEFT JOIN #__wallfactory_urls l ON l.post_id = p.id '
           	. ' LEFT JOIN #__wallfactory_media m ON m.post_id = p.id '
           	. ' LEFT JOIN #__wallfactory_comments c ON c.post_id = p.id '
           	. $where
           	. ' GROUP BY p.id'
           	. ' ORDER BY p.date_created DESC';
	   
	    $this->_data = $this->_getList($query_posts, $this->_limitstart, $this->_limit);
	    
	  	$urls = array();

        foreach ($this->_data as $i => $row) {
      	 $query = ' SELECT l.*, w.alias '
	           . ' FROM #__wallfactory_urls l '
	           . ' LEFT JOIN #__wallfactory_walls w ON w.user_id = l.user_id '
	           . ' WHERE l.post_id = ' .$row->id;

	     $this->_db->setQuery($query);
		          
    	 $urls[$row->id] = $this->_db->loadObjectList();
   	  }
 
     return $urls;
		
	}
	
	function getMedia($wall_id = null) {
		
	  $user     =& JFactory::getUser();
	  $database =& JFactory::getDBO();
	  	  
	  $alias =& JRequest::getVar('alias', '', 'GET', 'cmd');
	  $where =  $this->_buildWhereCondition($wall_id);

      $query_posts = ' SELECT p.*, '
    	   . ' c.user_id as comment_user_id, c.content, c.date_created as comment_date, '
    	   . ' u.username, u.id as wall_owner_id, w.alias AS wall_alias,'
           . '   DATE_FORMAT(p.date_created, "%Y-%m-%d") AS date_spacer, IF(f.id, 1, 0) AS followed,'
           . '  IF (p.user_id = ' . $user->id . ', 1, 0) AS my_post'
           . ' FROM #__wallfactory_posts p'
           . ' LEFT JOIN #__wallfactory_walls w ON w.id = p.wall_id'
           . ' LEFT JOIN #__users u ON u.id = p.user_id'
           . ' LEFT JOIN #__wallfactory_followers f ON w.id = f.wall_id AND f.user_id = ' . $user->id
           . ' LEFT JOIN #__wallfactory_urls l ON l.post_id = p.id '
           . ' LEFT JOIN #__wallfactory_media m ON m.post_id = p.id '
           . ' LEFT JOIN #__wallfactory_comments c ON c.post_id = p.id '
           . $where
           . ' GROUP BY p.id'
           . ' ORDER BY p.date_created DESC';
		
	   
	    $this->_data = $this->_getList($query_posts, $this->_limitstart, $this->_limit);
	    
	  	$media = array();

      foreach ($this->_data as $i => $row) {
      	 $query = ' SELECT m.*, w.alias '
	           . ' FROM #__wallfactory_media m '
	           . ' LEFT JOIN #__wallfactory_walls w ON w.user_id = m.user_id '
	           . ' WHERE m.post_id = '.$row->id;

	     $this->_db->setQuery($query);
		
    	 $media[$row->id] = $this->_db->loadObjectList();
   
	  }

	  return $media;
	}

   function getBookmarks()
   {
    if (is_null($this->_data))
    {
      return false;
    }
    $query = $this->_buildQuery(null);
    $this->_data = $this->_getList($query, $this->_limitstart, $this->_limit);

    $text = '';
   	$bookmarks = array();
     
  	foreach ($this->_data as $k=>$post) {
  
    $path = JURI::root().'index.php?option=com_wallfactory&view=post&id=' . $post->id . '&alias=' . $post->alias . '';
    $page_full_url = urlencode($path);
       
    $title         = urlencode(strip_tags(substr($post->content, 0, 100)));
    $text          = urlencode(strip_tags(substr($post->content, 0, 100)));
    $id = $post->id;

    if ($text != '') {
	    $query = ' SELECT b.*'
	           . ' FROM #__wallfactory_bookmarks b'
	           . ' WHERE b.published = 1';
	    $this->_db->setQuery($query);
	    $bookmarks[$id] = $this->_db->loadObjectList();
	
	    foreach ($bookmarks[$id] as $i => $bookmark)
	    {
	      $link = str_replace(array('%%url%%', '%%title%%',  '%%bodytext%%'),
	                          array($page_full_url, $title, $text),
	                          $bookmark->link);
	      $bookmarks[$id][$i]->full_link = $link;
	    }
    }
  }
  
    return $bookmarks;
  }	
  	
  function getVideoBookmarks()
  {
    $urls = $this->getUrls(null);
   
    if (is_null($urls))
    {
      return false;
    }
    
   $video_bookmarks = array();
   $video_title = '';
  
   foreach ($urls as $k=>$videourl) {
    if (!empty($videourl)) {
 	  foreach ($videourl as $vurl) {
 
	    $path = JURI::root().'index.php?option=com_wallfactory&view=post&id=' . $vurl->post_id . '&alias=' . $vurl->alias . '';
	    $page_full_url = urlencode($path);
	    $video_title 	= ($vurl->video_title != '') ? urlencode($vurl->video_title) : urlencode($vurl->video_link);
	    
	    $id = $vurl->post_id;
	 
	    if ($video_title != '') {
		    $query = ' SELECT b.*'
		           . ' FROM #__wallfactory_bookmarks b'
		           . ' WHERE b.published = 1';
		    $this->_db->setQuery($query);
		    $video_bookmarks[$id] = $this->_db->loadObjectList();
		
		    foreach ($video_bookmarks[$id] as $i => $bookmark)
		    {
		      $video_link = str_replace(array('%%url%%',   '%%title%%'),
		                          array($page_full_url, $video_title),
		                          $bookmark->link);
		                          
		      $video_bookmarks[$id][$i]->full_link = $video_link;
		    }
	    }
 	  }
    }
   }

   return $video_bookmarks;
  }
  
  function getUrlBookmarks()
  {
    $urls = $this->getUrls(null);
 
    if (is_null($urls))
    {
      return false;
    }

    $url_title = '';
    $url_bookmarks = array();
        
    foreach ($urls as $k=>$url) {
     if (!empty($url)) {
 	  foreach ($url as $uurl) {
 	  	  
	    $path = JURI::root().'index.php?option=com_wallfactory&view=post&id=' . $uurl->post_id . '&alias=' . $uurl->alias . '';
    	$page_full_url = urlencode($path);
	    $url_title     = urlencode($uurl->url);

		$id = $uurl->post_id;
		
		if ($url_title != '') {
		
		    $query = ' SELECT b.*'
		           . ' FROM #__wallfactory_bookmarks b'
		           . ' WHERE b.published = 1';
		    $this->_db->setQuery($query);
		    $url_bookmarks[$id] = $this->_db->loadObjectList();
		
		    foreach ($url_bookmarks[$id] as $i => $bookmark)
		    {
		      $url_link = str_replace(array('%%url%%',   '%%title%%'),
		                          array($page_full_url, $url_title),
		                          $bookmark->link);
		      $url_bookmarks[$id][$i]->full_link = $url_link;
		    }
		}
 	  }
     }
    }
 
    return $url_bookmarks;
  }
  
  function getMp3Bookmarks()
  {
    
    $mp3array = $this->getMedia(null);
   
    if (is_null($mp3array))
    {
      return false;
    }

    $mp3_bookmarks = array();
    $mp3_title = '';
    
    foreach ($mp3array as $k=>$mediarow) {
	    
      if (!empty($mediarow)) {
 	  	foreach ($mediarow as $m) {
		    $path = JURI::root().'index.php?option=com_wallfactory&view=post&id=' . $m->post_id . '&alias=' . $m->alias . '';
	    	$page_full_url = urlencode($path);
		    if ($m->folder == 'mp3')
		    	$mp3_title     = urlencode($m->title);

			$id = $m->post_id;

			if ($mp3_title != '') {
			    $query = ' SELECT b.*'
			           . ' FROM #__wallfactory_bookmarks b'
			           . ' WHERE b.published = 1';
			    $this->_db->setQuery($query);
			    $mp3_bookmarks[$id] = $this->_db->loadObjectList();
			
			    foreach ($mp3_bookmarks[$id] as $i => $bookmark)
			    {
			      $mp3_link = str_replace(array('%%url%%',   '%%title%%'),
			                          array($page_full_url, $mp3_title),
			                          $bookmark->link);
			      $mp3_bookmarks[$id][$i]->full_link = $mp3_link;
			    }
			}
 	  	}
      }
    }
 
    return $mp3_bookmarks;
  }
  
  function getFileBookmarks()
  {
  	$filearray = $this->getMedia(null);
   
    if (is_null($filearray))
    {
      return false;
    }
    
  	$file_bookmarks = array();
    $file_title = '';
    
    foreach ($filearray as $k=>$media) {
    	if (!empty($media)) {
	 	  foreach ($media as $m) {
		    $path = JURI::root().'index.php?option=com_wallfactory&view=post&id=' . $m->post_id . '&alias=' . $m->alias . '';
	    	$page_full_url = urlencode($path);
		    if ($m->folder == 'files')
		    	$file_title     = urlencode($m->title);

			$id = $m->post_id;
	
			if ($file_title != '') {
				$query = ' SELECT b.*'
			           . ' FROM #__wallfactory_bookmarks b'
			           . ' WHERE b.published = 1';
			    $this->_db->setQuery($query);
			    $file_bookmarks[$id] = $this->_db->loadObjectList();
			
			    foreach ($file_bookmarks[$id] as $i => $bookmark)
			    {
			      $file_link = str_replace(array('%%url%%',   '%%title%%'),
			                          array($page_full_url, $file_title),
			                          $bookmark->link);
			      $file_bookmarks[$id][$i]->full_link = $file_link;
			    }
			}
	 	  }
    	}
    }

    return $file_bookmarks;
  }
  
  function getImageBookmarks()
  {
    $imagearray = $this->getMedia(null);

    if (is_null($imagearray))
    {
      return false;
    }
    
    $image_bookmarks = array();
    $image_title = '';

    foreach ($imagearray as $k=>$mediaimg) {
      if (!empty($mediaimg)) {
 	  	foreach ($mediaimg as $m) {
		    $path = JURI::root().'index.php?option=com_wallfactory&view=post&id=' . $m->post_id . '&alias=' . $m->alias . '';
		    $page_full_url = urlencode($path);
		    if ($m->folder == 'images')
		    	$image_title     = urlencode($m->title);
			
			$id = $m->post_id;
			if ($image_title != '') {
			
			    $query = ' SELECT b.*'
			           . ' FROM #__wallfactory_bookmarks b'
			           . ' WHERE b.published = 1';
			    $this->_db->setQuery($query);
			    $image_bookmarks[$id] = $this->_db->loadObjectList();
			
			    foreach ($image_bookmarks[$id] as $i => $bookmark)
			    {
			      $image_link = str_replace(array('%%url%%',   '%%title%%'),
			                          array($page_full_url, $image_title),
			                          $bookmark->link);
			      $image_bookmarks[$id][$i]->full_link = $image_link;
			    }
			}
 	  	}
 	  }
    }
    
    return $image_bookmarks;
  }
	
  function getSettings()
  {
    $user =& JFactory::getUser();

    $query = ' SELECT *'
           . ' FROM #__wallfactory_walls'
           . ' WHERE user_id = ' . $user->id;
    $this->_db->setQuery($query);
    
    return $this->_db->loadObject();
    
  }
  

}