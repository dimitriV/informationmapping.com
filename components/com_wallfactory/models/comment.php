<?php
/*------------------------------------------------------------------------
com_wallfactory - Wall Factory
------------------------------------------------------------------------
author    TheFactory
copyright Copyright (C) 2010 SKEPSIS Consult SRL. All Rights Reserved.
@license - http://www.gnu.org/licenses/gpl-2.0.html GNU/GPL
Websites: http://www.thefactory.ro
Technical Support:  Forum - http://www.thefactory.ro/joomla-forum/
-------------------------------------------------------------------------*/

defined('_JEXEC') or die('Restricted access');

jimport('joomla.application.component.model');

class FrontendModelComment extends JModel
{
  function __construct()
  {
    parent::__construct();

    $this->wallSettings = new wallSettings();
    global $mainframe, $option;
    $this->_limit      = JRequest::getVar('limit', $this->wallSettings->comments_per_page, 'REQUEST', 'integer');
    $this->_limitstart = JRequest::getVar('limitstart', 0, 'REQUEST', 'integer');

    $this->_limitstart = ($this->_limit != 0 ? (floor($this->_limitstart / $this->_limit) * $this->_limit) : 0);
  }

  
  function store($post_id,$content)
  {
    $user =& JFactory::getUser();

    if ($user->guest && !$this->wallSettings->allow_guest_comments)
    {
      die('{ "errors": 2, "message": "' . JText::_('You must login to be able to write a comment') . '"}');
    }

    if ($user->guest && $this->wallSettings->captcha_comment)
    {
      require_once(JPATH_COMPONENT_ADMINISTRATOR.DS.'lib'.DS.'recaptcha'.DS.'recaptchalib.php');

      $resp = recaptcha_check_answer(base64_decode($this->wallSettings->recaptcha_private_key),
        $_SERVER["REMOTE_ADDR"],
        $_POST["recaptcha_challenge_field"],
        $_POST["recaptcha_response_field"]);

      if (!$resp->is_valid)
      {
        die('{ "errors": 1, "message": "' . JText::_('Invalid captcha! Try again!') . '"}');
      }
    }
    
    $content =& JRequest::getVar('comment', '', 'REQUEST', 'string');
    $post_id =& JRequest::getVar('post_id', 0, 'REQUEST', 'integer');
    
    /*$query = ' SELECT  w.user_id, p.enable_comments '
           . ' FROM #__wallfactory_posts p'
           . ' LEFT JOIN #__wallfactory_walls w ON w.id = p.wall_id'
           . ' WHERE p.id = ' . $post_id;
    $this->_db->setQuery($query);
  
    $info = $this->_db->loadObject();

    if (!$info->enable_comments)
    {
      die('{ "errors": 2, "message": "' . JText::_('Comments have been disabled for this post') . '"}');
    }*/

    // Get banned words list
    $banned_words = $this->wallSettings->banned_words;
    foreach ($banned_words as $i => $word)
    {
      $banned_words[$i] = base64_decode($word);
    }
   
          
    $content = str_ireplace($banned_words, '***', $content);
    
    $query = ' SELECT  w.user_id, w.alias '
           . ' FROM #__wallfactory_walls w '
           . ' WHERE w.user_id = ' . $user->id;
    $this->_db->setQuery($query);
    $author = $this->_db->loadObject();
    
    if (!$author) {
    	$author_alias 	= 'Guest';
    	$user_id 		= 0;
    	
    } else {
       $author_alias 	= $author->alias;
       $user_id    		= $user->id;
    }
    
    $comments =& $this->getTable('comment');
    
    $comment->post_id      = $post_id;
    $comment->user_id      = $user_id;
    $comment->content      = $content;
    $comment->date_created = date('Y-m-d H:i:s');
    $comment->author_alias = $author_alias;
    $comment->approved = 1;
 
   if (!$comments->bind($comment))
	    {
	      $this->setError($this->_db->getErrorMsg());
	      return false;
	    }
	    
    if (!$comments->check())
    {
      $this->setError($this->_db->getErrorMsg());
      return false;
    }

    if (empty($comments->id))
    {
      $isNew = true;
      $post->date_created = date('Y-m-d H:i:s');
    }
   
    if (empty($comments->id)) {
	    if (!$comments->store())
	    {
	      $this->setError($this->_db->getErrorMsg());
	      return false;
	    }
    }

    // Send notification
    if ($this->wallSettings->enable_notification_new_comment)
    {
      $this->sendNewCommentNotification($comments->id);
    }

  }
  
  function delete($comment_id)
  {
    $user =& JFactory::getUser();
    $database =& JFactory::getDBO();

    $query = ' DELETE'
           . ' FROM #__wallfactory_comments'
           . ' WHERE id = ' . $comment_id;
    $database->setQuery($query);
    $database->query();

    return true;
  }

  function getNotificationFollowers()
    {
    $user     =& JFactory::getUser();

    $query_wall = ' SELECT w.id '
           . ' FROM #__wallfactory_walls w '
    	   . ' WHERE w.published = 1'
           . ' AND w.user_id = '.$user->id;
           
    $this->_db->setQuery($query_wall);
    $wall_id = $this->_db->loadResult();
    
  	$query = ' SELECT f.user_id '
           . ' FROM #__wallfactory_followers f '
    	   . ' LEFT JOIN #__wallfactory_walls w ON w.id = f.wall_id'
           . ' WHERE w.published = 1'
           . ' AND w.id = '.$wall_id;
        
    $this->_db->setQuery($query);

    return implode(', ', $this->_db->loadResultArray());
  }

  function sendNewCommentNotification($comment_id)
  {
    $ids =  $this->getNotificationFollowers();
  
  	$query = ' SELECT c.content, c.id, p.id AS post_id, w.title AS wall_title,'
           . '   w.id AS wall_id, u.username AS owner_username, u.id AS owner_id, u.email AS owner_email,'
           . '   w.comment_notification'
           . ' FROM #__wallfactory_comments c'
           . ' LEFT JOIN #__wallfactory_posts p ON p.id = c.post_id'
           . ' LEFT JOIN #__wallfactory_walls w ON w.id = p.wall_id'
           . ' LEFT JOIN #__users u ON u.id = w.user_id'
           . ' WHERE c.id = ' . $comment_id;
    $this->_db->setQuery($query);
    $comment = $this->_db->loadObject();

    $subject = base64_decode($this->wallSettings->notification_new_comment_subject);
    $message = base64_decode($this->wallSettings->notification_new_comment_message);

    $search  = array('%%username%%', '%%walltitle%%', '%%commenttext%%', '%%postlink%%', '%%walllink%%', '%%commentlink%%');

    // Send notification to the wall owner
    if ($comment->comment_notification)
    {
      $replace = array(
        $comment->owner_username,
        $comment->wall_title,
        $comment->content
      );

      if ($this->wallSettings->enable_notification_email_html)
      {
       // $replace[] = '<a href="' . JRoute::_(JURI::root() . 'index.php?option=com_wallfactory&view=post&id=' . $comment->post_id) . '">' . $comment->post_title . '</a>';
        $replace[] = '<a href="' . JRoute::_(JURI::root() . 'index.php?option=com_wallfactory&view=wall&id=' . $comment->wall_id) . '">' . $comment->wall_title . '</a>';
       // $replace[] = '<a href="' . JRoute::_(JURI::root() . 'index.php?option=com_wallfactory&view=post&id=' . $comment->post_id) . '#comments">' . $comment->post_title . '</a>';
      }
      else
      {
        $replace[] = JRoute::_(JURI::root() . 'index.php?option=com_wallfactory&view=post&id=' . $comment->post_id);
        $replace[] = JRoute::_(JURI::root() . 'index.php?option=com_wallfactory&view=wall&id=' . $comment->wall_id);
        $replace[] = JRoute::_(JURI::root() . 'index.php?option=com_wallfactory&view=post&id=' . $comment->post_id . '#comments');
      }

      $custom_message = str_replace($search, $replace, $message);
      $custom_subject = str_replace($search, $replace, $subject);

      if ($this->wallSettings->enable_notification_email_html)
      {
        $custom_message = nl2br($custom_message);
      }

      $mail =& JFactory::getMailer();
      $mail->addRecipient($comment->owner_email);
      $mail->setSubject(JText::_($custom_subject));
      $mail->setBody(JText::_($custom_message));
      $mail->IsHTML($this->wallSettings->enable_notification_email_html);

      $mail->send();
    }

    // Send notifications to the admins
    if (count($this->wallSettings->notification_new_comment_receivers))
    {
      $query = ' SELECT u.username, u.email'
             . ' FROM #__users u'
             . ' WHERE u.id IN (' . $ids . ')';
      $this->_db->setQuery($query);
      $receivers = $this->_db->loadObjectList();

      foreach ($receivers as $receiver)
      {
        $replace = array(
          $receiver->username,
          $comment->wall_title,
          $comment->content
        );

        if ($this->wallSettings->enable_notification_email_html)
        {
          //$replace[] = '<a href="' . JRoute::_(JURI::root() . 'index.php?option=com_wallfactory&view=post&id=' . $comment->post_id) . '">' . $comment->post_title . '</a>';
          $replace[] = '<a href="' . JRoute::_(JURI::root() . 'index.php?option=com_wallfactory&view=wall&id=' . $comment->wall_id) . '">' . $comment->wall_title . '</a>';
          //$replace[] = '<a href="' . JRoute::_(JURI::root() . 'index.php?option=com_wallfactory&view=post&id=' . $comment->post_id) . '#comments">' . $comment->post_title . '</a>';
        }
        else
        {
          $replace[] = JRoute::_(JURI::root() . 'index.php?option=com_wallfactory&view=post&id=' . $comment->post_id);
          $replace[] = JRoute::_(JURI::root() . 'index.php?option=com_wallfactory&view=wall&id=' . $comment->wall_id);
          $replace[] = JRoute::_(JURI::root() . 'index.php?option=com_wallfactory&view=post&id=' . $comment->post_id . '#comments');
        }

        $custom_message = str_replace($search, $replace, $message);
        $custom_subject = str_replace($search, $replace, $subject);

        if ($this->wallSettings->enable_notification_email_html)
        {
          $custom_message = nl2br($custom_message);
        }

        $mail =& JFactory::getMailer();
        $mail->addRecipient($receiver->email);
        $mail->setSubject(JText::_($custom_subject));
        $mail->setBody(JText::_($custom_message));
        $mail->IsHTML($this->wallSettings->enable_notification_email_html);

        $mail->send();
      }
    }
  }
  
 
}