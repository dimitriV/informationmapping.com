<?php
/*------------------------------------------------------------------------
com_wallfactory - Wall Factory
------------------------------------------------------------------------
author    TheFactory
copyright Copyright (C) 2010 SKEPSIS Consult SRL. All Rights Reserved.
@license - http://www.gnu.org/licenses/gpl-2.0.html GNU/GPL
Websites: http://www.thefactory.ro
Technical Support:  Forum - http://www.thefactory.ro/joomla-forum/
-------------------------------------------------------------------------*/

defined('_JEXEC') or die('Restricted access');

jimport('joomla.application.component.model');

class FrontendModelWalls extends JModel
{
  var $_data;
  var $_total;
  var $_pagination;
  var $_limit;
  var $_limitstart;
  var $_params;

  function __construct()
  {
    parent::__construct();

    global $mainframe, $option;
    $this->_params =& $mainframe->getParams();
$this->wallSettings = new wallSettings();
    $this->_limit      = JRequest::getVar('limit',      $this->wallSettings->num_posts_per_page, 'REQUEST', 'integer');
    $this->_limitstart = JRequest::getVar('limitstart', 0,                                       'REQUEST', 'integer');

    $this->_limitstart = ($this->_limit != 0 ? (floor($this->_limitstart / $this->_limit) * $this->_limit) : 0);
  }

  function getData()
  {
    if (empty($this->_data))
    {
      $query = $this->_buildQuery();
      $this->_data = $this->_getList($query, $this->_limitstart, $this->_limit);
    }

    return $this->_data;
  }

  function _buildQuery()
  {
    $where   = $this->_buildWhereCondition();
    $orderby = $this->_buildOrderByCondition();
    $user    =& JFactory::getUser();

    $query = ' SELECT w.* '
   		. ' , u.username, u.name, f.id AS followed, s.avatar_type, s.avatar_extension, u.email'
           . ' FROM #__wallfactory_walls w'
           . ' LEFT JOIN #__users u ON u.id = w.user_id'
           . ' LEFT JOIN #__wallfactory_followers f ON f.wall_id = w.id AND f.user_id = ' . $user->id
           . ' LEFT JOIN #__wallfactory_members s ON s.user_id = w.user_id'
           . $where
           . $orderby;
           
      return $query;
  }

  function _buildWhereCondition()
  {
    $session =& JFactory::getSession();
    $where = ' WHERE w.published = 1';

    switch ($session->get('filter.walls.followed', ''))
    {
      case 'followed':
        $where .= ' AND f.id IS NOT NULL';
      break;

      case 'not_followed':
        $where .= ' AND f.id IS NULL';
      break;
    }

    return $where;
  }

  function _buildOrderByCondition()
  {
    $orderby = '';
    $sort    = ($this->_params->def('order_walls_order', 0) == 0) ? 'ASC' : 'DESC';
    $orderby = ' ORDER BY w.date_created ' . $sort;

    return $orderby;
  }

  function getTotal()
  {
    if (empty($this->_total))
    {
      $query = $this->_buildQuery();
      $this->_total = $this->_getListCount($query);
    }

    return $this->_total;
  }

  function getPagination()
  {
    if (empty($this->_pagination))
    {
      jimport('joomla.html.pagination');
      $this->_pagination = new JPagination($this->getTotal(), $this->_limitstart, $this->_limit);
    }

    return $this->_pagination;
  }

  function getSettings()
  {
    $user =& JFactory::getUser();

    $query = ' SELECT *'
           . ' FROM #__wallfactory_members'
           . ' WHERE user_id = ' . $user->id;
    $this->_db->setQuery($query);
    return $this->_db->loadObject();
  }
}