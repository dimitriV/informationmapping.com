<?php
/*------------------------------------------------------------------------
com_wallfactory - Wall Factory
------------------------------------------------------------------------
author    TheFactory
copyright Copyright (C) 2010 SKEPSIS Consult SRL. All Rights Reserved.
@license - http://www.gnu.org/licenses/gpl-2.0.html GNU/GPL
Websites: http://www.thefactory.ro
Technical Support:  Forum - http://www.thefactory.ro/joomla-forum/
-------------------------------------------------------------------------*/

defined('_JEXEC') or die('Restricted access');

jimport('joomla.application.component.model');

class FrontendModelManageFollowers extends JModel
{
  var $_data;
  var $_total;
  var $_pagination;
  var $_limit;
  var $_limitstart;
  var $_wall_id;
  var $_latest_posts;

  function __construct()
  {
    parent::__construct();

    global $mainframe, $option;
    $this->_limit      = JRequest::getVar('limit',      10, 'REQUEST', 'integer');
    $this->_limitstart = JRequest::getVar('limitstart', 0,  'REQUEST', 'integer');

    $this->_limitstart = ($this->_limit != 0 ? (floor($this->_limitstart / $this->_limit) * $this->_limit) : 0);

    $user =& JFactory::getUser();
    $query = ' SELECT id'
           . ' FROM #__wallfactory_walls'
           . ' WHERE user_id = ' . $user->id;
    $this->_db->setQuery($query);
    $this->_wall_id = $this->_db->loadResult();
  }

  function getData()
  {
    if (empty($this->_data))
    {
      $query = $this->_buildQuery();
      $this->_data = $this->_getList($query, $this->_limitstart, $this->_limit);
    }
      
    return $this->_data;
  }

  function _buildQuery()
  {
    $user   =& JFactory::getUser();

    $query = ' SELECT f.*, u.username, w.alias as wall_alias'
           . ' FROM #__wallfactory_followers f'
           . ' LEFT JOIN #__users u ON u.id = f.user_id'
           . ' LEFT JOIN #__wallfactory_walls w ON w.user_id = f.user_id '   
           . ' WHERE f.wall_id = ' . $this->_wall_id
           . ' ORDER BY f.date_created DESC';
           
    return $query;
  }
  
  function getLastpost() {
  	
   if (!empty($this->_data))
   {
    $lasts = array();
		
    foreach ($this->_data as $i => $post) {
   
  	$query = ' SELECT u.username, w.alias as wall_alias'
    	   . ' ,p.id as post_id, p.content as post_content, p.date_created'
           . ' FROM #__wallfactory_posts p'
           . ' LEFT JOIN #__users u ON u.id = p.user_id'
           . ' LEFT JOIN #__wallfactory_walls w ON w.id = p.wall_id ' 
           . ' LEFT JOIN #__wallfactory_urls l ON l.post_id = p.id '   
           . ' LEFT JOIN #__wallfactory_media m ON m.post_id = p.id '
           . ' LEFT JOIN #__wallfactory_comments c ON c.post_id = p.id '  
           . ' WHERE p.user_id = ' . $post->user_id
           . ' GROUP BY p.id'
           . ' ORDER BY p.date_created DESC LIMIT 0,1';

    	$this->_db->setQuery($query);
    	$result = $this->_db->loadAssocList();
    	if ($result)
    		$lasts[$post->user_id] = $result;
	}	

    return  $lasts;

   }
  }
  
  function getTotalPosts() {
  	if (!empty($this->_data))
	{
	  $count = array();
      foreach ($this->_data as $i => $post) { 
           $query = ' SELECT COUNT(distinct p.id) as posts_no '
      		. ' FROM #__wallfactory_posts p'
      		. ' WHERE p.user_id = ' . $post->user_id;
     		
	      $this->_db->setQuery($query);
	      $result = $this->_db->loadResult();
	      $count[$post->user_id] = $result;
      }
     
      return $count;			
	}
  }
 
  function getTotal()
  {
    if (empty($this->_total))
    {
      $query = $this->_buildQuery();
      $this->_total = $this->_getListCount($query);
    }

    return $this->_total;
  }

  function getPagination()
  {
    if (empty($this->_pagination))
    {
      jimport('joomla.html.pagination');
      $this->_pagination = new JPagination($this->getTotal(), $this->_limitstart, $this->_limit);
    }

    return $this->_pagination;
  }
}