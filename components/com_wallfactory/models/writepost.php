<?php
/*------------------------------------------------------------------------
com_wallfactory - Wall Factory
------------------------------------------------------------------------
author    TheFactory
copyright Copyright (C) 2010 SKEPSIS Consult SRL. All Rights Reserved.
@license - http://www.gnu.org/licenses/gpl-2.0.html GNU/GPL
Websites: http://www.thefactory.ro
Technical Support:  Forum - http://www.thefactory.ro/joomla-forum/
-------------------------------------------------------------------------*/

defined('_JEXEC') or die('Restricted access');

jimport('joomla.application.component.model');

class FrontendModelWritepost extends JModel
{
	function __construct()
  {
    parent::__construct();

    $alias 		= JRequest::getVar('alias', '', 'REQUEST', 'cmd');
    $post_id 	= JRequest::getVar('post_id', '', 'REQUEST', 'int');

    $this->setAlias($alias);
	$this->_post_id = $post_id;
    $this->wallSettings = new wallSettings();
  }

  function setAlias($alias)
  {
    $this->_alias = $alias;
    
    $this->_data  = null;
    $this->_image  = null;
    $this->_mp3file = null;
    $this->_file = null;
    $this->_link  = null;
  }
  

  // Getters
  function &getData()
  {
    if (empty($this->_data))
    {
      $user =& JFactory::getUser();

      $query = ' SELECT p.* '
             . ' FROM #__wallfactory_posts p'
             . ' LEFT JOIN #__wallfactory_walls w ON w.user_id = p.user_id'
             . ' WHERE p.alias = "' . $this->_alias . '"'
             . ' AND w.user_id = ' . $user->id
             . ' AND p.id = ' . $this->_post_id;
             
      $this->_db->setQuery($query);

      $this->_data = $this->_db->loadObject();
    }

    if (!$this->_data)
    {
      $this->_data = $this->getTable('post');
      $this->_data->published = 1;
    }

    return $this->_data;
  }
  
  function &getImage()
  {
    if (empty($this->_image))
    {
      $user =& JFactory::getUser();

      $query = ' SELECT p.*, m.id as image_id, m.folder, m.title as image_title, m.name as image_name, m.extension, m.description, m.date_added as image_date '
      	     . ' FROM #__wallfactory_posts p'
             . ' LEFT JOIN #__wallfactory_walls w ON w.user_id = p.user_id'
             . ' LEFT JOIN #__wallfactory_media m ON m.post_id = p.id'
             . ' WHERE p.alias = "' . $this->_alias . '"'
             . ' AND m.folder = "images" '
             . ' AND w.user_id = ' . $user->id
             . ' AND p.id = ' . $this->_post_id;
             
      $this->_db->setQuery($query);

      $this->_image = $this->_db->loadObjectList();
    }

    if (!$this->_image)
    {
      $this->_image = $this->getTable('post');
      $this->_image->published = 1;
    }

    return $this->_image;
  }
  
  function &getMp3()
  {
    if (empty($this->_mp3file))
    {
      $user =& JFactory::getUser();

      $query = ' SELECT p.*, m.id as mp3_id, m.folder, m.title as mp3_title, m.name as mp3_name, m.extension, m.description, m.date_added as mp3_date '
             . ' FROM #__wallfactory_posts p'
             . ' LEFT JOIN #__wallfactory_walls w ON w.user_id = p.user_id'
             . ' LEFT JOIN #__wallfactory_media m ON m.post_id = p.id'
             . ' WHERE p.alias = "' . $this->_alias . '"'
             . ' AND m.folder = "mp3" '
             . ' AND w.user_id = ' . $user->id
             . ' AND p.id = ' . $this->_post_id;
 
      $this->_db->setQuery($query);

      $this->_mp3file = $this->_db->loadObject();
    }

    if (!$this->_mp3file)
    {
      $this->_mp3file = $this->getTable('post');
      $this->_mp3file->published = 1;
    }

    return $this->_mp3file;
  }
  
  function &getFile()
  {
    if (empty($this->_file))
    {
      $user =& JFactory::getUser();

      $query = ' SELECT p.*, m.id as file_id, m.folder, m.title as file_title, m.name as file_name, m.extension, m.description, m.date_added as file_date '
      		 . ' FROM #__wallfactory_posts p'
             . ' LEFT JOIN #__wallfactory_walls w ON w.user_id = p.user_id'
             . ' LEFT JOIN #__wallfactory_media m ON m.post_id = p.id'
             . ' WHERE p.alias = "' . $this->_alias . '"'
             . ' AND m.folder = "files" '
             . ' AND w.user_id = ' . $user->id
             . ' AND p.id = ' . $this->_post_id;
             
      $this->_db->setQuery($query);

      $this->_file = $this->_db->loadObject();
    }

    if (!$this->_file)
    {
      $this->_file = $this->getTable('post');
      $this->_file->published = 1;
    }

    return $this->_file;
  }
  
  function &getLinks()
  {
    if (empty($this->_link))
    {
      $user =& JFactory::getUser();

      $query = ' SELECT p.*, '
       		 . ' l.id as link_id, l.url, l.video_link, l.date_added as url_date '
             . ' FROM #__wallfactory_posts p'
             . ' LEFT JOIN #__wallfactory_walls w ON w.user_id = p.user_id'
             . ' LEFT JOIN #__wallfactory_urls l ON l.post_id = p.id'
             . ' WHERE p.alias = "' . $this->_alias . '"'
             . ' AND w.user_id = ' . $user->id
             . ' AND p.id = ' . $this->_post_id;
             
      $this->_db->setQuery($query);

      $this->_link = $this->_db->loadObject();
    }

    if (!$this->_link)
    {
      $this->_link = $this->getTable('post');
      $this->_link->published = 1;
    }

    return $this->_link;
  }


  function getWall()
  {
    $user =& JFactory::getUser();

    $query = ' SELECT w.*'
           . ' FROM #__wallfactory_walls w'
           . ' WHERE w.user_id = ' . $user->id;
    $this->_db->setQuery($query);

    return $this->_db->loadObject();
  }

// Tasks
  function store()
  {
    $user =& JFactory::getUser();
    $data =  JRequest::get('post');
         
    $data['content'] = JRequest::getVar('post_message', '', 'post', 'string', JREQUEST_ALLOWRAW);
    $data['image_name'] = JRequest::getVar('image_name', '', 'post', 'string', JREQUEST_ALLOWRAW);
    $data['image_description'] = JRequest::getVar('image-description', '', 'post', 'string', JREQUEST_ALLOWRAW);
    $data['video_link'] = JRequest::getVar('video-link', '', 'post', 'string', JREQUEST_ALLOWRAW);
    $data['mp3_name'] = JRequest::getVar('mp3_name', '', 'post', 'string', JREQUEST_ALLOWRAW);
    $data['file_name'] = JRequest::getVar('file_name', '', 'post', 'string', JREQUEST_ALLOWRAW);
	$data['link'] = JRequest::getVar('link', '', 'post', 'string', JREQUEST_ALLOWRAW);
	$data['wall_id'] = JRequest::getVar('wall_id', '', 'POST', 'string');
        
    $query = ' SELECT w.id, w.user_id, w.alias, '
     		. 'w.title, w.published'
           	. ' FROM #__wallfactory_walls w'
           	. ' WHERE w.user_id = ' . $user->id;
           
    $this->_db->setQuery($query);
    $wall = $this->_db->loadObject();
    
    $datenow     =& JFactory::getDate();
    
    $post =& $this->getTable('post');
        
	$data['hidFileID-image'] = JRequest::getVar('hidFileID-image', '', 'post', 'string', JREQUEST_ALLOWRAW);
    $data['hidFileID-mp3'] = JRequest::getVar('hidFileID-mp3', '', 'post', 'string', JREQUEST_ALLOWRAW);
    $data['hidFileID-file'] = JRequest::getVar('hidFileID-file', '', 'post', 'string', JREQUEST_ALLOWRAW);
    
    $posts = array();
    $posts['wall_id'] = $data['wall_id'];
    $posts['user_id'] = $wall->user_id;
    $posts['alias']   = $wall->alias;
    $posts['content'] = $data['content'];
    
    
    if (!$post->bind($posts))
    {
      $this->setError($this->_db->getErrorMsg());
      return false;
    }

    if (!$post->check())
    {
      $this->setError($this->_db->getErrorMsg());
      return false;
    }

    if (empty($post->id))
    {
      $isNew = true;
      $post->date_created = date('Y-m-d H:i:s');
    }
    $post->date_updated = date('Y-m-d H:i:s');

    if ($post) {
   	
   	// media
   	$media = array();
   	$media['name'] = array();
   	
   	$media['name']['image'] = ($data['image_name'] != '') ? $data['image_name'] : '';
   	$media['image_description'] = ($data['image_description'] != '') ? $data['image_description'] : '';
   	
   	$media['name']['mp3_name'] 	= ($data['mp3_name'] != '') ? $data['mp3_name'] : '';
   	$media['name']['file_name'] = ($data['file_name'] != '') ? $data['file_name'] : ''; 
   	  
    foreach ($media['name'] as $rec) {
    	if ($rec != '') {
	    	$media_query = ' UPDATE #__wallfactory_media  '
	    			. ' SET post_id = '.$post->id 
	    			. ' WHERE name = '.$rec;
	    
	    	$this->_db->setQuery($media_query);
    	}
    }
    $this->_db->query();

    // urls
    $url = array();
    $url['user_id']		= $user->id;
    $url['url'] 		= ($data['link'] != '') ? $data['link']: ''; 
    $url['video_link'] 	= ($data['video_link'] != '') ? $data['video_link'] : '';
    $url['post_id'] 	= $post->id;
   	    
    $urls_table =& $this->getTable('urls');
      	
    if (($url['url'] != "") || ($url['video_link'] != "") ){
	    	
    	if (!$urls_table->bind($url))
	    {
	      $this->setError($this->_db->getErrorMsg());
	      return false;
	    }
	
	    if (!$urls_table->check())
	    {
	      $this->setError($this->_db->getErrorMsg());
	      return false;
	    }
	
	    if (empty($urls_table->id))
	    {
	      $isNew = true;
	      $urls_table->date_added = date('Y-m-d H:i:s');
	    }
	    
	    if (!$urls_table->store())
	    {
	      $this->setError($this->_db->getErrorMsg());
	      return false;
	    }
    }
   }
    
    if ($wall->published)
    {
      
      // Notifications
      $old_post =& $this->getTable('post');
      $old_post->load($post->id);

      if (!$old_post->notification)
      {
        // Send notifications to followers
        if ($this->wallSettings->enable_notification_follow_wall)
        {
          $this->sendFollowersNotifications($post);
        }

        // Send notification to the admin
        if ($this->wallSettings->enable_notification_new_post && count($this->wallSettings->notification_new_post_receivers))
        {
          $this->sendNewPostNotification($post);
        }

        // Update notification status
        $post->notification = 1;
        $post->store();
      }
     
    }

    return $post;
  }
  
    
  function deleteMedia()
	{
	  $id       =& JRequest::getVar('media_id', 0, 'GET', 'integer');
	  $alias    =& JRequest::getVar('alias', 0, 'GET', 'string');
	  $user     =& JFactory::getUser();

	  $query = ' SELECT m.* '
      		 . ' FROM #__wallfactory_media m'
             . ' LEFT JOIN #__wallfactory_posts p ON p.id = m.post_id'
             . ' WHERE p.alias = "' . $alias . '"'
             . ' AND m.user_id = ' . $user->id
             . ' AND m.id = ' . $id;
 	 
	  $this->_db->setQuery($query);
	  $row = $this->_db->loadAssocList();
	  $type = $row[0]['folder']; 

	  if (!$row)
	  {
	    return false;
	  }

    // delete from media 
     $query = ' DELETE'
           . ' FROM #__wallfactory_media'
           . ' WHERE id = ' . $id;
     $this->_db->setQuery($query);
     $this->_db->query();
    
     $this->_deleteStatistics($row);
 
	 return true;
	}

    function deleteUrl()
	{
	  $id       =& JRequest::getVar('link_id', 0, 'GET', 'integer');
	  $alias    =& JRequest::getVar('alias', 0, 'GET', 'string');
	  $type    =& JRequest::getVar('type', 0, 'GET', 'int');
	  
	  $user     =& JFactory::getUser();

	  $field = ($type == 1 ) ? 'video_link' : 'url';
	  $folder = ($type == 1 ) ? '"video_link" as folder' : '"url" as folder';
	  
	  $query = ' SELECT '.$field.' '
	  		 . ' , l.id, l.user_id, l.post_id, '.$folder.' ' 
      		 . ' FROM #__wallfactory_urls l'
             . ' LEFT JOIN #__wallfactory_posts p ON p.id = l.post_id'
             
             . ' WHERE p.alias = "' . $alias . '"'
             . ' AND l.user_id = ' . $user->id
             . ' AND l.id = ' . $id;
  	 
	  $this->_db->setQuery($query);
	  $row =  $this->_db->loadAssocList();
             
	  if (!$row)
	  {
	    return false;
	  }

    // delete from media 
     $query = ' UPDATE #__wallfactory_urls'
     	   . ' SET '.$field.' = "" '	
           . ' WHERE id = ' . $id;
           
                  
     $this->_db->setQuery($query);
     $this->_db->query();
    
    $query = ' SELECT url, video_link '
      		 . ' FROM #__wallfactory_urls l'
             . ' LEFT JOIN #__wallfactory_posts p ON p.id = l.post_id'
             . ' WHERE p.alias = "' . $alias . '"'
             . ' AND l.user_id = ' . $user->id
             . ' AND l.id = ' . $id;
           
      $this->_db->setQuery($query);
     
     $result =  $this->_db->loadAssocList();

     $this->_db->query();
    
    // delete from urls
    if ($result[0]['video_link'] == "" && $result[0]['url'] == "") {
    	$query = ' DELETE'
           . ' FROM #__wallfactory_urls'
           . ' WHERE id = ' . $id;
    	 $this->_db->setQuery($query);
      	 $this->_db->query();
    }
    $this->_deleteStatistics($row);
   
	 return true;
	}
	
	function _deleteStatistics($media) {
	  $user     =& JFactory::getUser();
	
      if ($media) {
    
	    $type = $media[0]['folder'];
	    
	    switch ($type) {
	    	case 'images': 
	    		$field = 'images';
	    		break;
	    	case 'mp3': 
	    		$field = 'mp3';
	    		break;
	    	case 'files': 
	    		$field = 'files';
	    		break;
	    	case 'video_link':
	    		$field = 'video_links';
	    		break;
	    	case 'url':
	    		$field = 'urls';
	    		break;
	    }
       
	$statistics_table =& $this->getTable('statistics');
		
	$query = ' SELECT id, posts, '
			. $field
			. ' FROM #__wallfactory_statistics  '
		    . ' WHERE user_id = '.$user->id;
	     			
	   $this->_db->setQuery($query);
       $row = $this->_db->loadAssoc();
       
       if (!$row['id']) {
      	
		      return false;
	    
       } else {

      	$stat["$field"] = $row["$field"] - 1;

       	  $statistics_table->load($row['id']);
       		
       		if (!$statistics_table->bind($stat))
		    {
		      $this->setError($this->_db->getErrorMsg());
		      return false;
		    }
	    	
	    	if (!$statistics_table->store())
		    {
		      $this->setError($this->_db->getErrorMsg());
		      return false;
		    }
       	
       }
    }
	
	}

  function getNotificationFollowers()
  {
    $user     =& JFactory::getUser();

    $query_wall = ' SELECT w.id '
           . ' FROM #__wallfactory_walls w '
    	   . ' WHERE w.published = 1'
           . ' AND w.user_id = '.$user->id;
           
    $this->_db->setQuery($query_wall);
    $wall_id = $this->_db->loadResult();
    
    
  	$query = ' SELECT f.user_id '
           . ' FROM #__wallfactory_followers f '
    	   . ' LEFT JOIN #__wallfactory_walls w ON w.id = f.wall_id'
           . ' WHERE w.published = 1'
           . ' AND w.id = '.$wall_id;
              
    $this->_db->setQuery($query);

    return implode(', ', $this->_db->loadResultArray());
  }

  // Helpers
 
  function sendNewPostNotification($post)
  {
  	$ids =  $this->getNotificationFollowers();
  
    $query = ' SELECT u.email, u.username'
           . ' FROM #__users u'
           . ' WHERE u.id IN (' . $ids . ')';
    $this->_db->setQuery($query);
    $receivers = $this->_db->loadObjectList();

    $query = ' SELECT w.title'
           . ' FROM #__wallfactory_posts p'
           . ' LEFT JOIN #__wallfactory_walls w ON w.id = p.wall_id'
           . ' WHERE p.id = ' . $post->id;
    $this->_db->setQuery($query);
    $wall_title = $this->_db->loadResult();

    $subject = base64_decode($this->wallSettings->notification_new_post_subject);
    $message = base64_decode($this->wallSettings->notification_new_post_message);

    $search  = array('%%username%%', '%%walltitle%%',  '%%postlink%%', '%%walllink%%');

    foreach ($receivers as $receiver)
    {
      $replace = $this->getEmailReplacements(
        $receiver->username,
        $wall_title,
        $post->id,
        $post->wall_id
      );

      $custom_subject = str_replace($search, $replace, $subject);
      $custom_message = str_replace($search, $replace, $message);

      if ($this->wallSettings->enable_notification_email_html)
      {
        $custom_message = nl2br($custom_message);
      }

      $mail =& JFactory::getMailer();
      $mail->addRecipient($receiver->email);
      $mail->setSubject(JText::_($custom_subject));
      $mail->setBody(JText::_($custom_message));
      $mail->IsHTML($this->wallSettings->enable_notification_email_html);

      $mail->send();
    }
  }

  function getEmailReplacements($username, $wall_title, $follower_alias, $post_id, $wall_id)
  {
    $replace = array(
      $username,
      $wall_title,
      $follower_alias,
	  $post_id,
	  $wall_id
    );

    if ($this->wallSettings->enable_notification_email_html)
    {
      $replace[] = '<a href="' . JRoute::_(JURI::root() . 'index.php?option=com_wallfactory&view=post&id=' . $post_id. '&alias='.$follower_alias) . '">' . $follower_alias . '</a>';
      $replace[] = '<a href="' . JRoute::_(JURI::root() . 'index.php?option=com_wallfactory&view=wall&&alias=' . $follower_alias) . '">' . $wall_title . '</a>';
    }
    else
    {
      $replace[] = JRoute::_(JURI::root() . 'index.php?option=com_wallfactory&view=post&id=' . $post_id);
      $replace[] = JRoute::_(JURI::root() . 'index.php?option=com_wallfactory&view=wall&id=' . $wall_id);
    }

    return $replace;
  }

  function sendFollowersNotifications($post)
  {
    $query = ' SELECT f.user_id, u.username, u.email, w.title AS wall_title, w.alias '
           . ' FROM #__wallfactory_followers f'
           . ' LEFT JOIN #__users u ON u.id = f.user_id'
           . ' LEFT JOIN #__wallfactory_walls w ON w.id = f.wall_id'
           . ' WHERE f.wall_id = ' . $post->wall_id
           . ' AND f.notification = 1';
    $this->_db->setQuery($query);
    $followers = $this->_db->loadObjectList();

    $subject = base64_decode($this->wallSettings->notification_follow_wall_subject);
    $message = base64_decode($this->wallSettings->notification_follow_wall_message);

    $search  = array('%%username%%', '%%walltitle%%', '%%postalias%%', '%%postlink%%', '%%walllink%%');

    foreach ($followers as $follower)
    {
      $replace = $this->getEmailReplacements(
        $follower->username,
        $follower->wall_title,
        $follower->alias,
        $post->id,
        $post->wall_id
      );

      $custom_subject = str_replace($search, $replace, $subject);
      $custom_message = str_replace($search, $replace, $message);

      if ($this->wallSettings->enable_notification_email_html)
      {
        $custom_message = nl2br($custom_message);
      }

      $mail =& JFactory::getMailer();
      $mail->addRecipient($follower->email);
      $mail->setSubject(JText::_($custom_subject));
      $mail->setBody(JText::_($custom_message));
      $mail->IsHTML($this->wallSettings->enable_notification_email_html);

      $mail->send();
    }
  }

  function updateServices($wall)
  {
    require_once(JPATH_COMPONENT_ADMINISTRATOR.DS.'lib'.DS.'xmlrpc'.DS.'xmlrpc.inc');
    require_once(JPATH_COMPONENT_ADMINISTRATOR.DS.'lib'.DS.'xmlrpc'.DS.'xmlrpcs.inc');

    foreach ($this->wallSettings->list_update_services as $service)
    {
      $service = base64_decode($service);
      $service = parse_url($service);

      $url = JRoute::_(JURI::root() . 'index.php?option=com_wallfactory&view=wall&id=' . $wall->id);

      $m = new xmlrpcmsg('wewallUpdates.ping', array(new xmlrpcval($wall->title, 'string'), new xmlrpcval($url, 'string')));
      $c = new xmlrpc_client(isset($service['path']) ? $service['path'] : '/', $service['host'], 80);
      $c->setRequestCompression(null);
      $c->setAcceptedCompression(null);

      #$c->setDebug(2);
      $r = $c->send($m);
      #print $r->faultCode();
    }
  }
}