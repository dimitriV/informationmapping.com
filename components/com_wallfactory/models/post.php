<?php
/*------------------------------------------------------------------------
com_wallfactory - Wall Factory
------------------------------------------------------------------------
author    TheFactory
copyright Copyright (C) 2010 SKEPSIS Consult SRL. All Rights Reserved.
@license - http://www.gnu.org/licenses/gpl-2.0.html GNU/GPL
Websites: http://www.thefactory.ro
Technical Support:  Forum - http://www.thefactory.ro/joomla-forum/
-------------------------------------------------------------------------*/

defined('_JEXEC') or die('Restricted access'); ?>

<?php jimport('joomla.application.component.model');

class FrontendModelPost extends JModel
{
  var $_post;
  var $_alias;
  var $_data;
  var $_total;
  var $_pagination;
  var $_limit;
  var $_limitstart;
  var $_urls;
  
  function __construct()
  {
    parent::__construct();

    $alias    =& JRequest::getVar('alias', '', 'POST', 'string');
    $this->setAlias($alias);
    
    $this->wallSettings = new wallSettings();
    
  }

  function getPostData()
  {
    $alias =& JRequest::getVar('alias', '', 'GET', 'string');
	$post_id =& JRequest::getVar('id', 0, 'GET', 'cmd'); 
		
    $query = ' SELECT p.id, p.wall_id, p.user_id, p.alias, p.content, p.notification, p.date_created, p.date_updated, p.enable_comments, p.reported, p.reporting_user_id , '
    		. ' w.title AS wall_title, w.alias AS wall_alias,  '
    		. ' u.username, u.id as wall_owner_id, '
    		. ' (SELECT count( DISTINCT c.id ) FROM #__wallfactory_comments c WHERE c.post_id = p.id ) AS no, '
    		. ' s.avatar_extension, s.avatar_type '
           	. ' FROM #__wallfactory_posts p'
           	. ' LEFT JOIN #__wallfactory_walls w ON w.id = p.wall_id'
           	. ' LEFT JOIN #__users u ON u.id = w.user_id'
           	. ' LEFT JOIN #__wallfactory_members s ON s.user_id = p.user_id '
           	. ' WHERE p.id = '.$post_id;
  
    $this->_db->setQuery($query);
    $post = $this->_db->loadObject();

    if (!is_null($post))
    {
      $user =& JFactory::getUser();
    }

    $this->_post = $post;

    return $this->_post;
  }

  function setAlias($alias)
  {
    $this->_alias = $alias;
    $this->_data  = null;
  }
  
  function getTotalPosts() {
  	
  	if (!empty($this->_post))
	{
	   	$count = array();
      
           $query = ' SELECT COUNT(distinct p.id) as posts_no '
      		. ' FROM #__wallfactory_posts p'
      		. ' WHERE p.user_id = ' . $this->_post->user_id;
      		
	      $this->_db->setQuery($query);
	      $result = $this->_db->loadResult();
	      $count[$this->_post->user_id] = $result;
      
      return $count;			
	}
  }
	
   
   // Getters
  function &getData()
  {
    if (empty($this->_data))
    {
      $user =& JFactory::getUser();

      $query = ' SELECT p.*, w.alias AS wall_alias '
             . ' FROM #__wallfactory_posts p'
             . ' LEFT JOIN #__wallfactory_walls w ON w.id = p.wall_id'
             . ' WHERE p.alias = "' . $this->_alias . '"'
             . ' AND w.user_id = ' . $user->id;
             
      $this->_db->setQuery($query);

      $this->_data = $this->_db->loadObject();
    }

    if (!$this->_data)
    {
      $this->_data = $this->getTable('post');
      $this->_data->published = 1;
    }

    return $this->_data;
  }

  function getWall()
  {
    $user =& JFactory::getUser();

    $query = ' SELECT w.*'
           . ' FROM #__wallfactory_walls w'
           . ' WHERE w.user_id = ' . $user->id;
    $this->_db->setQuery($query);

    return $this->_db->loadObject();
  }
  
  function getComments()
  {
      $user     =& JFactory::getUser();
	  $alias =& JRequest::getVar('alias', '', 'GET', 'cmd');
  	  $post_id =& JRequest::getVar('id', 0, 'GET', 'cmd'); 
      

      $query_post = ' SELECT p.*, '
		    . ' c.user_id as comment_user_id, c.content, c.date_created as comment_date, '
		    . ' u.username, u.id as wall_owner_id, w.alias AS wall_alias,'
		    . ' s.avatar_extension, '
            . ' DATE_FORMAT(p.date_created, "%Y-%m-%d") AS date_spacer, IF(f.id, 1, 0) AS followed,'
            . ' IF (p.user_id = ' . $user->id . ', 1, 0) AS my_post'
            . ' FROM #__wallfactory_posts p'
            . ' LEFT JOIN #__wallfactory_walls w ON w.id = p.wall_id'
            . ' LEFT JOIN #__users u ON u.id = w.user_id'
            . ' LEFT JOIN #__wallfactory_followers f ON w.id = f.wall_id AND f.user_id = ' . $user->id
            . ' LEFT JOIN #__wallfactory_urls l ON l.post_id = p.id '
            . ' LEFT JOIN #__wallfactory_media m ON m.post_id = p.id '
            . ' LEFT JOIN #__wallfactory_comments c ON c.post_id = p.id '
            . ' LEFT JOIN #__wallfactory_members s ON s.user_id = c.user_id '
            . ' WHERE p.id = '.$post_id;
 
    $this->_db->setQuery($query_post);
    $post = $this->_db->loadObject();  
   	$this->post = $post;
   	
    $comments = array();
	$no_of_comments = $this->wallSettings->comments_per_page;

	$limit_condition =  ' LIMIT 0, '."$no_of_comments".' ';
	
    if ($post) {
      	$query = ' SELECT c.id, c.post_id, c.user_id, c.content, c.date_created, c.author_alias, c.reported, s.avatar_type, s.avatar_extension '
      	   . ' , IF(u.username = "", u.username, "' . JText::_('Guest') . ' ") AS username, '
      	   . '  IF (s.name = "", u.username, s.name) AS name'
           . ' FROM #__wallfactory_comments c'
           . ' LEFT JOIN #__wallfactory_posts p ON p.id = c.post_id'
           . ' LEFT JOIN #__wallfactory_walls w ON w.id = p.wall_id'
           . ' LEFT JOIN #__users u ON u.id = c.user_id'
           . ' LEFT JOIN #__wallfactory_members s ON s.user_id = c.user_id'
           . ' WHERE w.alias = "'.$post->alias.'" '
           . ' AND c.post_id = '.$post->id
           . ' ORDER BY c.post_id, c.date_created DESC'
           . ' LIMIT 0, 10 ';
          // . $limit_condition;
	    
        $this->_db->setQuery($query);
   
	    $comments[$post->id] = $this->_db->loadObjectList();
	}
 
	return $comments;
  }
  
  function getUrls($wall_id = null) 
  {
	$user =& JFactory::getUser();
	$post_id  =& JRequest::getVar('id', 0, 'GET', 'integer');
	
    $query_post = ' SELECT p.*, '
    		. ' c.user_id as comment_user_id, c.content, c.date_created as comment_date, '
      		. ' u.username, u.id as wall_owner_id, w.alias AS wall_alias,'
           	. '   DATE_FORMAT(p.date_created, "%Y-%m-%d") AS date_spacer, IF(f.id, 1, 0) AS followed,'
           	. '  IF (p.user_id = ' . $user->id . ', 1, 0) AS my_post'
           	. ' FROM #__wallfactory_posts p'
           	. ' LEFT JOIN #__wallfactory_walls w ON w.id = p.wall_id'
           	. ' LEFT JOIN #__users u ON u.id = w.user_id'
           	. ' LEFT JOIN #__wallfactory_followers f ON w.id = f.wall_id AND f.user_id = ' . $user->id
           	. ' LEFT JOIN #__wallfactory_urls l ON l.post_id = p.id '
           	. ' LEFT JOIN #__wallfactory_media m ON m.post_id = p.id '
           	. ' LEFT JOIN #__wallfactory_comments c ON c.post_id = p.id '
            . ' WHERE p.id = '.$post_id
           	. ' ORDER BY p.date_created DESC';
	   
	$this->_db->setQuery($query_post);
    $result = $this->_db->loadObject();
     
	 $urls = array();

       if ($result) {
		
      	 $query = ' SELECT l.*'
	           . ' FROM #__wallfactory_urls l '
	           . ' WHERE l.post_id = ' .$result->id;

	     $this->_db->setQuery($query);
    
    	$urls[$result->id] = $this->_db->loadObjectList();
   	  }
    
     return $urls;
		
	}
	
  function getMedia() 
  {
	$user =& JFactory::getUser();
	$post_id  =& JRequest::getVar('id', 0, 'GET', 'integer');
	
    $query_post = ' SELECT p.*, '
    		. ' c.user_id as comment_user_id, c.content, c.date_created as comment_date, '
   		 	. ' u.username, u.id as wall_owner_id, w.alias AS wall_alias,'
           	. ' DATE_FORMAT(p.date_created, "%Y-%m-%d") AS date_spacer, IF(f.id, 1, 0) AS followed,'
           	. ' IF (p.user_id = ' . $user->id . ', 1, 0) AS my_post'
           	. ' FROM #__wallfactory_posts p'
           	. ' LEFT JOIN #__wallfactory_walls w ON w.id = p.wall_id'
           	. ' LEFT JOIN #__users u ON u.id = w.user_id'
           	. ' LEFT JOIN #__wallfactory_followers f ON w.id = f.wall_id AND f.user_id = ' . $user->id
           	. ' LEFT JOIN #__wallfactory_urls l ON l.post_id = p.id '
           	. ' LEFT JOIN #__wallfactory_media m ON m.post_id = p.id '
           	. ' LEFT JOIN #__wallfactory_comments c ON c.post_id = p.id '
          	. ' WHERE p.id = '.$post_id
            . ' ORDER BY p.date_created DESC';
		
	$this->_db->setQuery($query_post);
    $result = $this->_db->loadObject();  
     
	 $media = array();

	 if ($result) {
		
	      	$query = ' SELECT m.*'
		           . ' FROM #__wallfactory_media m '
		           . ' WHERE m.post_id = '.$result->id;
		         
		 	$this->_db->setQuery($query);

	    	$media[$result->id] = $this->_db->loadObjectList();
		 }
	  
	  return $media;
  }
  
  
  // Tasks
  function store()
  {
    $user =& JFactory::getUser();
    $data =  JRequest::get('post');
 
    $data['content'] 			= JRequest::getVar('post_message', '', 'post', 'string');
    $data['image_name'] 		= JRequest::getVar('image_name', '', 'post', 'string');
    $data['image_description'] 	= JRequest::getVar('image-description', '', 'post', 'string');
    $data['video_link'] 		= JRequest::getVar('video-link', '', 'post', 'string');
    $data['mp3_name'] 			= JRequest::getVar('mp3_name', '', 'post', 'string');
    $data['file_name'] 			= JRequest::getVar('file_name', '', 'post', 'string');
	$data['link'] 				= JRequest::getVar('link', '', 'post', 'string');
	$data['post_id'] 			= JRequest::getVar('post_id', 0, 'POST', 'int');
	$data['wall_id'] 			= JRequest::getVar('wall_id', 0, 'POST', 'int');
    $data['hidFileID-image'] 	= JRequest::getVar('hidFileID-image', '', 'post', 'int');
    $data['hidFileID-mp3'] 		= JRequest::getVar('hidFileID-mp3', '', 'post', 'int');
    $data['hidFileID-file'] 	= JRequest::getVar('hidFileID-file', '', 'post', 'int');
 
    $v = 1;
    // Check if post message is empty
    if ('' == $data['content'])
    {
      $this->setError(JText::_('POST_MESSAGE_ERROR_MESSAGE_EMPTY'));
      $v = 0;
      return false;
    }
    
    if ($v == 1) {
	    // Check if post image title is empty
	    if ($data['hidFileID-image'] != '') {
    		
	    	if ('' == $data['image_name'])
		    {
		      $this->setError(JText::_('POST_IMAGE_TITLE_ERROR_MESSAGE_EMPTY'));
		      return false;
		    }
	    }
	    // Check if post mp3 title is empty
	    if ($data['hidFileID-mp3'] != '') {
		    if ('' == $data['mp3_name'])
		    {
		      $this->setError(JText::_('POST_MP3_TITLE_ERROR_MESSAGE_EMPTY'));
		      return false;
		    }
	    }
	    
	    // Check if post file title is empty
	    if ($data['hidFileID-file'] != '') {
		    if ('' == $data['file_name'])
		    {
		      $this->setError(JText::_('POST_FILE_TITLE_ERROR_MESSAGE_EMPTY'));
		      return false;
		    }
	    }
    }
    
    $query = ' SELECT w.* '
           	. ' FROM #__wallfactory_walls w'
           	. ' WHERE w.user_id = ' . $user->id;
    $this->_db->setQuery($query);
    $wall = $this->_db->loadObject();

    $datenow     =& JFactory::getDate();
    $post =& $this->getTable('post');
   	
    $posts = array();
	$posts['wall_id'] = $data['wall_id'];
    if ($wall) {
	    $posts['user_id'] = $wall->user_id;
	    $posts['alias']   = $wall->alias;
    }
    $posts['content'] = $data['content'];

    if ($data['post_id']  != 0)   
    {
    	$post->load($data['post_id']);	
    	
    	if (!$post->bind($posts))
	    {
	      $this->setError($this->_db->getErrorMsg());
	      return false;
	    }
    	
    	if (!$post->store())
	    {
	      $this->setError($this->_db->getErrorMsg());
	      return false;
	    }
    }  else {
	    if (!$post->bind($posts))
	    {
	      $this->setError($this->_db->getErrorMsg());
	      return false;
	    }
  	    
	    if (!$post->check())
	    {
	      $this->setError($this->_db->getErrorMsg());
	      return false;
	    }

	    if (empty($post->id))
	    {
	      $isNew = true;
	      $post->date_created = date('Y-m-d H:i:s');
	    }
    	$post->date_updated = date('Y-m-d H:i:s');

	    if (empty($post->id)) {
		    if (!$post->store())
		    {
		      $this->setError($this->_db->getErrorMsg());
		     # die('stopped');
		      return false;
		    }
	    }
    }

    if ($post) {
   	  jimport('joomla.filesystem.file');
   	  // media
 	  $media_table =& $this->getTable('media');
 	
   	  $media_img = array();
   	  $media_mp3 = array();
   	  $media_file = array();
   	
   	  $media_img['title'] = ($data['image_name'] != '') ? $data['image_name'] : "";
   	  $media_img['image_description'] = ($data['image_description'] != '') ? $data['image_description'] : "";
   	
   	  if ($data['hidFileID-image'] != '') {
  		
   	  	$media_img['hidID'] = ($data['hidFileID-image'] != '') ? $data['hidFileID-image'] : 0;
  		$media_img['post_id'] = $post->id;
  		
  		$media_table->load($media_img['hidID']);	
    	
    	if (!$media_table->bind($media_img))
	    {
	      $this->setError($this->_db->getErrorMsg());
	      return false;
	    }
    	
    	if (!$media_table->store())
	    {
	      $this->setError($this->_db->getErrorMsg());
	      return false;
	    }

   	  } 
	  else 
	  {
		if ( ($media_img['title'] != "") || ($media_img['image_description'] != "") ){
	   			
			$img_query = ' UPDATE #__wallfactory_media  '
		    			. ' SET title = "'. $media_img['title'] .'"'
		    			. ' , description = "'. $media_img['image_description'] .'"'
		    			. ' WHERE post_id = '.$post->id;
			
		    $this->_db->setQuery($img_query);
		    $this->_db->query();
	    }
	  }
  	
     $media_mp3['title'] = ($data['mp3_name'] != '') ? $data['mp3_name'] : '';
 
     if ($data['hidFileID-mp3'] != '') {
   		$media_mp3['hidID'] = ($data['hidFileID-mp3'] != '') ? $data['hidFileID-mp3'] : '';
   		$media_mp3['post_id'] = $post->id;
	    			
   		$media_table->load($media_mp3['hidID']);	
    	
    	if (!$media_table->bind($media_mp3))
	    {
	      $this->setError($this->_db->getErrorMsg());
	      return false;
	    }
    	
    	if (!$media_table->store())
	    {
	      $this->setError($this->_db->getErrorMsg());
	      return false;
	    }
   		
   	 } else {
   		// update 
   		if ( $media_mp3['title'] != "")  {
   			$mp3_query = ' UPDATE #__wallfactory_media  '
		    		. ' SET title = "'. $media_mp3['title'] .'"'
		    		. ' WHERE post_id = '.$post->id;
			    			
		    $this->_db->setQuery($mp3_query);
		    $this->_db->query();
   		}	    
   	 }
   	   	
   	 $media_file['title'] = ($data['file_name'] != '') ? $data['file_name'] : '';
   	 
   	 if ($data['hidFileID-file'] != '') {
   		$media_file['hidID'] = ($data['hidFileID-file'] != '') ? $data['hidFileID-file'] : '';
   		$media_file['post_id'] = $post->id;
   		
   		$media_table->load($media_file['hidID']);	
    	
    	if (!$media_table->bind($media_file))
	    {
	      $this->setError($this->_db->getErrorMsg());
	      return false;
	    }
    	
    	if (!$media_table->store())
	    {
	      $this->setError($this->_db->getErrorMsg());
	      return false;
	    }

   	 } else {
   		// update
 	    if ( $media_file['title'] != "")  {
   			$file_query = ' UPDATE #__wallfactory_media  '
	    			. ' SET title = "'. $media_file['title'] .'"'
	    			. ' WHERE post_id = '.$post->id;

	    	$this->_db->setQuery($file_query);
	     	$this->_db->query();
		  }
   	 }
    
   	 // urls
     $url = array();
     
     $url['user_id']		= $user->id;
     $url['wall_id']		= $data['wall_id'];
     $url['url'] 			= ($data['link'] != '') ? $data['link']: ''; 
     $url['video_link'] 	= ($data['video_link'] != '') ? $data['video_link'] : '';
     $url['post_id'] 		= $post->id;
        
     if ( $url['url'] != '') {
     	
     	$url['url_title'] = "";
     	
       if(function_exists("curl_init")) {
    	$link = $url['url']; /* gets the data from a URL */
 		$ch = curl_init();
		$timeout = 30;
		curl_setopt($ch,CURLOPT_URL,$link);
		curl_setopt($ch,CURLOPT_RETURNTRANSFER,1); /*Set curl to return the data instead of printing it to the browser*/
		curl_setopt($ch,CURLOPT_CONNECTTIMEOUT,$timeout);
		
		$result_url = curl_exec($ch); /*Execute the fetch */
		
		$err     = curl_errno( $ch );
    	$errmsg  = curl_error( $ch );
		//$header  = curl_getinfo( $ch );
		curl_close($ch);
 		
       } else {
   	  	if (function_exists('file_get_contents')) {
	   		$result_url = @file_get_contents($url['url']);
   	  	}
       }
   	 
	    preg_match('#<meta([^>]*)name="([^>\"]*)title"([^>]*)>#i', $result_url, $matchesUrlTitle);												
	    preg_match('#<meta([^>]*)name="([^>\"]*)description"([^>]*)>#i', $result_url, $matchesUrlDescription);
	   		
		if ( (!empty($matchesUrlTitle)) || (!empty($matchesUrlDescription) )) {
			$tags = get_meta_tags($url['url']);

			if (isset($tags['title']))
				$url['url_title'] = strip_tags(htmlspecialchars_decode($tags['title']));
			if (isset($tags['description']))	
				$url['url_description'] = strip_tags(htmlspecialchars_decode($tags['description']));
			else 
				$url['url_description'] = '';
		} 
		if ($url['url_title'] == "") {
			$matchesTitle = ltrim( $url['url'], ' "\/');
			$matchesTitle = rtrim( $url['url'], ' "/');
		
			$url['url_title'] = $this->_db->getEscaped($matchesTitle);
		
		}
  	  }
  	  
	if ( $url['video_link'] != '') {

	  if(function_exists("curl_init")) {
		
	    $vlink = $url['video_link']; /* gets the data from a URL */
 		$ch = curl_init();
		$timeout = 30;
		curl_setopt($ch,CURLOPT_URL,$vlink);
		curl_setopt($ch,CURLOPT_RETURNTRANSFER,1); /*Set curl to return the data instead of printing it to the browser*/
		curl_setopt($ch,CURLOPT_CONNECTTIMEOUT,$timeout);
		
		$result_video_link = curl_exec($ch); /*Execute the fetch */
		
		$err     = curl_errno( $ch );
    	$errmsg  = curl_error( $ch );
		curl_close($ch);

	
	} else {
	  if (function_exists('file_get_contents')) {
	 
		$result_video_link = @file_get_contents($url['video_link']);
	  }
	}	
		preg_match('#<meta([^>]*)name="([^>\"]*)title"([^>]*)>#i', 			$result_video_link, $matchesVideoTitle);												
		preg_match('#<meta([^>]*)name="([^>\"]*)description"([^>]*)>#i', 	$result_video_link, $matchesVideoDescription);
		preg_match('#<link([^>]*)rel="([^>\"]*)image_src"([^>]*)>#i', 		$result_video_link, $imgVideoSourceThumb);
		preg_match('#<link([^>]*)rel="([^>\"]*)video_src"([^>]*)>#i', 		$result_video_link, $matchesVideoSource);
		
		preg_match('#<meta([^>]*)property="([^>\"]*)og:site_name"([^>]*)>#i', 	$result_video_link, $matchesVideoSitename);
		preg_match('#<meta([^>]*)property="([^>\"]*)og:url"([^>]*)>#i', 		$result_video_link, $matchesVideoUrl);
		preg_match('#<meta([^>]*)property="([^>\"]*)og:title"([^>]*)>#i', 		$result_video_link, $matchesVideoTitle_og);
		preg_match('#<meta([^>]*)property="([^>\"]*)og:image"([^>]*)>#i', 		$result_video_link, $matchesVideoThumbnail);
		preg_match('#<meta([^>]*)property="([^>\"]*)og:description"([^>]*)>#i', $result_video_link, $matchesVideoDescription_og);

		$videotags = get_meta_tags($url['video_link']);

		$url['video_title'] = '';
		$url['video_description'] = '';
		$url['video_thumbnail'] = '';
		$url['video_sitename'] = '';
		$url['video_sourceThumb'] = '';
		
		$db = JFactory::getDBO();
		
		if (!empty($matchesVideoSitename)) {
		  foreach ($matchesVideoSitename as $i=>$val) {
		
		  	$count[$i] = substr_count($val, ' content=');
		  	
			if ($count[$i] > 0) {
				$matchesSitename = ltrim($matchesVideoSitename[$i], ' content="/');
				$matchesSitename = ltrim($matchesSitename, ' "\/');
				$matchesSitename = rtrim($matchesSitename, ' "/');
				$url['video_sitename'] = $db->getEscaped($matchesSitename);
			}

		  }
			
		  if ( ($url['video_sitename'] == 'YouTube') || ($url['video_sitename'] == 'Vimeo') ) {
			//get URL
			if (!empty($matchesVideoUrl)) {
				foreach ($matchesVideoUrl as $i=>$val) {
					
					$count[$i] = substr_count($val, ' content="');
					if ($count[$i] > 0) {
						$matchesUrl = ltrim($matchesVideoUrl[$i], ' content="/');
						$matchesUrl = ltrim($matchesUrl, ' "\/');
						$matchesUrl = rtrim($matchesUrl, ' "/');
						$url['video_link'] = $db->getEscaped($matchesUrl);
					}
				}
			}
		
			//get title
			if (!empty($matchesVideoTitle_og)) {
				foreach ($matchesVideoTitle_og as $i=>$val) {
					
					$count[$i] = substr_count($val, ' content="');
					if ($count[$i] > 0) {
						$matchesTitle = ltrim($matchesVideoTitle_og[$i], ' content="/');
						$matchesTitle = ltrim($matchesTitle, ' "\/');
						$matchesTitle = rtrim($matchesTitle, ' "/');
						$url['video_title'] = $db->getEscaped($matchesTitle);
					}
				}
			}

			// if null get meta name=title
			
			//get image 
			if (!empty($matchesVideoThumbnail)) {
				foreach ($matchesVideoThumbnail as $i=>$val) {
					
					$count[$i] = substr_count($val, ' content="');
					if ($count[$i] > 0) {
						$matchesThumbnail = ltrim($matchesVideoThumbnail[$i], ' content="/');
						$matchesThumbnail = ltrim($matchesThumbnail, ' "\/');
						$matchesThumbnail = rtrim($matchesThumbnail, ' "/');
						$matchesThumbnail = ltrim($matchesThumbnail, ' "http:\/\/');
						$url['video_thumbnail'] = $db->getEscaped($matchesThumbnail);
					}
				}
			}

			//get description
			if (!empty($matchesVideoDescription_og)) {
				foreach ($matchesVideoDescription_og as $i=>$val) {
					
					$count[$i] = substr_count($val, ' content="');
					if ($count[$i] > 0) {
						$matchesDescription = ltrim($matchesVideoDescription_og[$i], ' content="/');
						$matchesDescription = ltrim($matchesDescription, ' "\/');
						$matchesDescription = rtrim($matchesDescription, ' "/');
						$url['video_description'] = $db->getEscaped($matchesDescription);
					}
				}
			}
				
			} else {
				// if Metacafe
				if ($url['video_sitename'] == 'Metacafe') {
			
				//get URL
				if (!empty($matchesVideoSource)) {
					foreach ($matchesVideoSource as $i=>$val) {
						
						$count[$i] = substr_count($val, ' href=');
						if ($count[$i] > 0) {
							$matchesVideo = ltrim($matchesVideoSource[$i], '  href=');
							$matchesVideo = ltrim($matchesVideo, ' "\/');
							$matchesVideo = rtrim($matchesVideo, ' "/');
							$url['video_link'] = $db->getEscaped($matchesVideo);
						}
					}
				} 
				
				//get title
				if (!empty($matchesVideoTitle)) {
					foreach ($matchesVideoTitle as $i=>$val) {
							
						$count[$i] = substr_count($val, ' content="');
						if ($count[$i] > 0) {
							$matchesTitle = ltrim($matchesVideoTitle[$i], ' content="/');
							$matchesTitle = ltrim($matchesTitle, ' "\/');
							$matchesTitle = rtrim($matchesTitle, ' "/');
							$url['video_title'] = $db->getEscaped($matchesTitle);
						}
					}
				} 
				
				//get image 		
				if (!empty($imgVideoSourceThumb)) {	
					foreach ($imgVideoSourceThumb as $i=>$val) {
						
						$count[$i] = substr_count($val, 'href');
						if ($count[$i] > 0) {
							$imgSourceThumb = ltrim($imgVideoSourceThumb[$i], ' href=');
							$imgSourceThumb = ltrim($imgSourceThumb, ' "\/');
							$imgSourceThumb = rtrim($imgSourceThumb, ' "/');
							$url['video_sourceThumb'] = $db->getEscaped($imgSourceThumb);
						}
					}
				}
						
				//get description	
				if (!empty($matchesVideoDescription)) {
					foreach ($matchesVideoDescription as $i=>$val) {
						
						$count[$i] = substr_count($val, ' content="');
						if ($count[$i] > 0) {
							$matchesDescription = ltrim($matchesVideoDescription[$i], ' content="/');
							$matchesDescription = ltrim($matchesDescription, ' "\/');
							$matchesDescription = rtrim($matchesDescription, ' "/');
							$url['video_description'] = $db->getEscaped($matchesDescription);
						}
					}
				}
				}
		}
		
	}	else {
		
		//get URL
		if (!empty($matchesVideoSource)) {
			foreach ($matchesVideoSource as $i=>$val) {
		
				$count[$i] = substr_count($val, ' href=');
								
				if ($count[$i] > 0) {
					$matchesVideo = ltrim($matchesVideoSource[$i], ' href=');
					$matchesVideo = ltrim($matchesVideo, ' "\/');
					$matchesVideo = rtrim($matchesVideo, ' "/');
					$url['video_link'] = $db->getEscaped($matchesVideo);
				}
			}
		}
		
		//get title	
		if (!empty($matchesVideoTitle)) {
			foreach ($matchesVideoTitle as $i=>$val) {
		
			$count[$i] = substr_count($val, ' content="');
			if ($count[$i] > 0) {
				$matchesTitle = ltrim($matchesVideoTitle[$i], ' content="/');
				$matchesTitle = ltrim($matchesTitle, ' "\/');
				$matchesTitle = rtrim($matchesTitle, ' "/');
				$url['video_title'] = $db->getEscaped($matchesTitle);
			}
		}
		} else {
			if (!empty($matchesVideoTitle2)) {
				foreach ($matchesVideoTitle2 as $i=>$val) {
				
					$count[$i] = substr_count($val, ' content="');
					if ($count[$i] > 0) {
						$matchesTitle2 = ltrim($matchesVideoTitle2[$i], ' content="/');
						$matchesTitle2 = ltrim($matchesTitle2, ' "\/');
						$matchesTitle2 = rtrim($matchesTitle2, ' "/');
						$url['video_title'] = $db->getEscaped($matchesTitle2);
					}
				}
			}
		}	
		
		//get description	
		if (!empty($matchesVideoDescription)) {
		foreach ($matchesVideoDescription as $i=>$val) {
		
			$count[$i] = substr_count($val, ' content="');
			if ($count[$i] > 0) {
				$matchesDescription = ltrim($matchesVideoDescription[$i], ' content="/');
				$matchesDescription = ltrim($matchesDescription, ' "\/');
				$matchesDescription = rtrim($matchesDescription, ' "/');
				$url['video_description'] = $db->getEscaped($matchesDescription);
			}
		}
		}
			
		//get image 			
		if (!empty($matchesVideoThumbnail)) {
			foreach ($matchesVideoThumbnail as $i=>$val) {
		
			$count[$i] = substr_count($val, ' content="');
			if ($count[$i] > 0) {
				$matchesThumbnail = ltrim($matchesVideoThumbnail[$i], ' content="/');
				$matchesThumbnail = ltrim($matchesThumbnail, ' "\/');
				$matchesThumbnail = rtrim($matchesThumbnail, ' "/');
				$url['video_thumbnail'] = $db->getEscaped($matchesThumbnail);
			}
		}
		}	else {
			if (!empty($imgVideoSourceThumb)) {	
				foreach ($imgVideoSourceThumb as $i=>$val) {
					$count[$i] = substr_count($val, 'href');
						if ($count[$i] > 0) {
						$imgSourceThumb = ltrim($imgVideoSourceThumb[$i], ' href=');
						$imgSourceThumb = ltrim($imgSourceThumb, ' "\/');
						$imgSourceThumb = rtrim($imgSourceThumb, ' "/');
						$url['video_sourceThumb'] = $db->getEscaped($imgSourceThumb);
					}
				}
			}
		}

		}

	}     	
    $urls_table =& $this->getTable('urls');
    
    if ( ($url['url'] != "") || ($url['video_link'] != "") ){
    	
    	$url_query = ' SELECT id from #__wallfactory_urls  '
		    			. ' WHERE post_id = '.$post->id;
		 
	    $this->_db->setQuery($url_query);
    	$result = $this->_db->loadResult();
   	
    	if (!$result) {
	    		
	    	if (!$urls_table->bind($url))
		    {
		      $this->setError($this->_db->getErrorMsg());
		      return false;
		    }
		
		    if (!$urls_table->check())
		    {
		      $this->setError($this->_db->getErrorMsg());
		      return false;
		    }
		
		    if (empty($urls_table->id))
		    {
		      $isNew = true;
		      $urls_table->date_added = date('Y-m-d H:i:s');
		    }
		    
		    if (!$urls_table->store())
		    {
		      $this->setError($this->_db->getErrorMsg());
		      return false;
		    }
		    
	   } else {
	   		//update
	 		$urls_table->load($result);	
    	
	    	if (!$urls_table->bind($url))
		    {
		      $this->setError($this->_db->getErrorMsg());
		      return false;
		    }
	    	
	    	if (!$urls_table->store())
		    {
		      $this->setError($this->_db->getErrorMsg());
		      return false;
		    }
	   }
    }
    }

    if ($wall && $wall->published)
    {
      // Notifications
      $old_post =& $this->getTable('post');
      $old_post->load($post->id);
      
      if (!$old_post->notification)
      {
        // Send notification to the followers
        if ($this->wallSettings->enable_notification_new_post && $this->wallSettings->enable_notify_new_post_followers)
        {
          $this->sendNewPostNotification($post);
        }

        // Send notification to the admin
        if ($this->wallSettings->enable_notification_new_post && $this->wallSettings->enable_notify_new_post_admin)
        {
          $this->sendAdminNewPostNotification($post);
        }
        
		if ($wall->post_notification)
		{
          $this->sendOwnerNewPostNotification($post, $wall->user_id);
        }
		
        // Update notification status
        $post->notification = 1;
    
        $post->store();
      }
    }
   
    // statistics
    if ($post) {
	$statistics_table =& $this->getTable('statistics');
	
	
	$query = ' SELECT id, posts, urls, video_links, images, mp3, files FROM #__wallfactory_statistics  '
		    			. ' WHERE user_id = '.$user->id;
	     			
	   $this->_db->setQuery($query);
       $row = $this->_db->loadAssoc();
    	
       
       if (!$row['id']) {
	   		$stat['posts']			= 1;
			$stat['urls'] 			= ($data['link'] != '') ? 1 : 0;
       		$stat['video_links'] 	= ($data['video_link'] != '') ? 1 : 0;
       		$stat['images']			= ($data['hidFileID-image'] != '') ? 1 : 0;
       		$stat['mp3']			= ($data['hidFileID-mp3'] != '') ? 1 : 0;
       		$stat['files']			= ($data['hidFileID-file'] != '') ? 1 : 0;
       		$stat['user_id']		= $user->id;
       		$stat['wall_id']		= $data['wall_id'];
       	
    		if (!$statistics_table->bind($stat))
		    {
		      $this->setError($this->_db->getErrorMsg());
		      return false;
		    }
		    
	    	if (!$statistics_table->store())
		    {
		      $this->setError($this->_db->getErrorMsg());
		      return false;
		    }	
		    
       } else {
       	
       	$stat['posts'] = $row['posts'] + 1;
       	
       	if ( ($data['hidFileID-image']) != "" )
       		$stat['images'] = $row['images'] + 1;
       	
       	if ( ($data['hidFileID-mp3']) != "" )
       		$stat['mp3'] = $row['mp3'] + 1;
       	
       	if ( ($data['hidFileID-file']) != "" )
       		$stat['files'] = $row['files'] + 1;
       		
       			
       	if ( ($url['url'] != ''))
       		$stat['urls'] = $row['urls'] + 1;
       	
		if ( ($url['video_link'] != "") )
			$stat['video_links'] = $row['video_links'] + 1;
       		$statistics_table->load($row['id']);
       		
       		if (!$statistics_table->bind($stat))
		    {
		      $this->setError($this->_db->getErrorMsg());
		      return false;
		    }
	    	
	    	if (!$statistics_table->store())
		    {
		      $this->setError($this->_db->getErrorMsg());
		      return false;
		    }
       }
    }
    
    if (!$post) {
    	$img_id = array();
    	$mp3_id = array();
    	$file_id = array();
    	
    	if ($data['hidFileID-image'] != '') {
    		$img_id['hidID'] = ($data['hidFileID-image'] != '') ? $data['hidFileID-image'] : 0;
	  		$query1 = ' SELECT name, extension FROM #__wallfactory_media  '
		    			. ' WHERE id = '.$img_id['hidID']
		    			. ' AND folder = "images" ';
	
		    $this->_db->setQuery($query1);
		    $media_name = $this->_db->loadAssoc();
		    
		    //delete from folder
	    	$path  = JPATH_SITE.DS.'components'.DS.'com_wallfactory'.DS.'storage'.DS.'users'.DS.$user->id.DS.'images'.DS.$media_name['name'].'.'.$media_name['extension'];
		  	@unlink($path);
		    			
    		$query2 = ' DELETE FROM #__wallfactory_media  '
		    			. ' WHERE id = '.$img_id['hidID']
		    			. ' AND folder = "images" ' ;
		    			
		    $this->_db->setQuery($query2);
		    $this->_db->query();
     	
    	}
    	
    	// for mp3
    	if ($data['hidFileID-mp3'] != '') {
    		$mp3_id['hidID'] = ($data['hidFileID-mp3'] != '') ? $data['hidFileID-mp3'] : '';
	  		$query1 = ' SELECT name, extension FROM #__wallfactory_media  '
		    			. ' WHERE id = '.$mp3_id['hidID']
		    			. ' AND folder = "images" ';
	
		    $this->_db->setQuery($query1);
		    $media_name = $this->_db->loadAssoc();
		    
		    //delete from folder
	    	$path  = JPATH_SITE.DS.'components'.DS.'com_wallfactory'.DS.'storage'.DS.'users'.DS.$user->id.DS.'images'.DS.$media_name['name'].'.'.$media_name['extension'];
		  	@unlink($path);
		    			
    		$query2 = ' DELETE FROM #__wallfactory_media  '
		    			. ' WHERE id = '.$mp3_id['hidID']
		    			. ' AND folder = "images" ' ;
		    			
		    $this->_db->setQuery($query2);
		    $this->_db->query();
     	
    	}
    	
    	// for file 
   	  	if ($data['hidFileID-file'] != '') {
    		$file_id['hidID'] = ($data['hidFileID-file'] != '') ? $data['hidFileID-file'] : '';
	  		$query1 = ' SELECT name, extension FROM #__wallfactory_media  '
		    			. ' WHERE id = '.$file_id['hidID']
		    			. ' AND folder = "images" ';
	
		    $this->_db->setQuery($query1);
		    $media_name = $this->_db->loadAssoc();
		    
		    //delete from folder
	    	$path  = JPATH_SITE.DS.'components'.DS.'com_wallfactory'.DS.'storage'.DS.'users'.DS.$user->id.DS.'images'.DS.$media_name['name'].'.'.$media_name['extension'];
		  	@unlink($path);
		    			
    		$query2 = ' DELETE FROM #__wallfactory_media  '
		    			. ' WHERE id = '.$file_id['hidID']
		    			. ' AND folder = "images" ' ;
		    			
		    $this->_db->setQuery($query2);
		    $this->_db->query();
     	
    	}
    }
     
    return $post;
  
  }
  
  
  function checkUserFolder($user_id)
  {
    $user_folder = $filepath = JPATH_SITE.DS.'components'.DS.'com_wallfactory'.DS.'storage'.DS.'users'.DS.$user_id;
    if (!is_dir($user_folder))
    {
      mkdir($user_folder);
    }

  }
  
  function delete()
	{
	  $id       =& JRequest::getVar('post_id', 0, 'GET', 'integer');
	  $alias    =& JRequest::getVar('alias', 0, 'GET', 'string');
	  $user     =& JFactory::getUser();

	  $query = ' SELECT p.id'
	         . ' FROM #__wallfactory_posts p'
	         . ' LEFT JOIN #__wallfactory_walls w ON w.id = p.wall_id'
	         . ' WHERE p.id = ' . $id
	        // . ' AND p.alias = "'. $alias .'"'
	         . ' AND p.user_id = ' . $user->id;
	  $this->_db->setQuery($query);
	  
	  $post = $this->_db->loadResult();
	  
	  if (!$post)
	  {
	    return false;
	  }
	  	  
	  $query_url = ' SELECT video_link, url, '
	  		 . ' l.id, l.user_id, l.post_id ' 
      		 . ' FROM #__wallfactory_urls l'
             . ' LEFT JOIN #__wallfactory_posts p ON p.id = l.post_id'
             
             . ' WHERE p.alias = "' . $alias . '"'
             . ' AND l.user_id = ' . $user->id
             . ' AND l.post_id = ' . $id;
  	 
	  $this->_db->setQuery($query_url);
	  $row_url =  $this->_db->loadAssocList();
     
	  if ($row_url) {
	  	$this->_deleteStatistics($row_url, 'url');
	  }
 	     
      $query_media = ' SELECT m.* '
      		 . ' FROM #__wallfactory_media m'
             . ' LEFT JOIN #__wallfactory_posts p ON p.id = m.post_id'
             . ' WHERE p.alias = "' . $alias . '"'
             . ' AND m.user_id = ' . $user->id
             . ' AND m.post_id = ' . $id;
	 
	  $this->_db->setQuery($query_media);
	  $result = $this->_db->loadAssocList();
	  
	  if ($result) {
	  
	  	$this->_deleteStatistics($result, 'media');
  	
	  	foreach ($result as $k=>$row) {
	  	
	  		$name[$k] 		= $row['name'];
		  	$extension[$k] 	= $row['extension'];
		 	$folder[$k] 	= $row['folder'];
		 	
	  		$path  = JPATH_SITE.DS.'components'.DS.'com_wallfactory'.DS.'storage'.DS.'users'.DS.$user->id.DS.$folder[$k].DS.$name[$k].'.'.$extension[$k];
  		
	  		@unlink($path);
	  	}

	  }
 
    // Delete comments
    $query = ' DELETE'
           . ' FROM #__wallfactory_comments'
           . ' WHERE post_id = ' . $id;
    $this->_db->setQuery($query);
    $this->_db->query();

    // delete from media 
    $query = ' DELETE'
           . ' FROM #__wallfactory_media'
           . ' WHERE post_id = ' . $id;
    $this->_db->setQuery($query);
    $this->_db->query();
    
    // delete from urls
     $query = ' DELETE'
           . ' FROM #__wallfactory_urls'
           . ' WHERE post_id = ' . $id;
    $this->_db->setQuery($query);
    $this->_db->query();
    
	// Delete the post
	  $query = ' DELETE'
	         . ' FROM #__wallfactory_posts'
	         . ' WHERE id = ' . $id;
	         
	         
	  $this->_db->setQuery($query);
	  $this->_db->query();
	  
	  $this->_deleteStatistics($post, 'post');

	  return true;
	}

	function _deleteStatistics($media,$type) {
	  $user     =& JFactory::getUser();
	
      if ($media) {
	  if ($type == 'url') {
	  
		  $url 			= $media[0]['url'];
		  $video_link 	= $media[0]['video_link'];
		  $links_id		= $media[0]['id'];
	  }
	  
	  if ($type == 'media') {
	  
		  foreach ($media as $k=>$row) {
		  
		  	$folder[$k] 	= $media[$k]['folder'];
		  	$media_id[$k] 	= $media[$k]['id'];
		  }
	  }
	  
      if ($type == 'post') {
		  $post_id 		= $media;
		   
	  }
	 
	$stat = array();
    	
	$statistics_table =& $this->getTable('statistics');
		
	$query = ' SELECT id, posts, '
			. ' urls, video_links, images, mp3, files'
			. ' FROM #__wallfactory_statistics  '
		    . ' WHERE user_id = '.$user->id;
	     			
	   $this->_db->setQuery($query);
       $row = $this->_db->loadAssoc();
    	
       if (!$row['id']) {
            return false;
       } else {
   	  
        if ($type == 'url') {
	      if ($url != "") 			$stat['urls'] = $row['urls'] - 1;
	      if ($video_link != "") 	$stat['video_links'] = $row['video_links'] - 1;
        }

        if ($type == 'media') {
	      foreach ($folder as $k=>$val) {
	      	if ($val != "") 
	      		$stat["$val"] = $row["$val"] - 1;
	      }
        }

        if ($type == 'post') {
	      if ($post_id != "") $stat['posts'] = $row['posts'] - 1;
        }
	      
       	  $statistics_table->load($row['id']);
       		
       		if (!$statistics_table->bind($stat))
		    {
		      $this->setError($this->_db->getErrorMsg());
		      return false;
		    }
	    	
	    	if (!$statistics_table->store())
		    {
		      $this->setError($this->_db->getErrorMsg());
		      return false;
		    }
       }
    }

	}

	function publish()
	{
	  $id       =& JRequest::getVar('id', 0, 'POST', 'integer');
	  $user     =& JFactory::getUser();

	  $query = ' SELECT p.id'
	         . ' FROM #__wallfactory_posts p'
	         . ' LEFT JOIN #__wallfactory_walls w ON w.id = p.wall_id'
	         . ' WHERE p.id = ' . $id
	         . ' AND w.user_id = ' . $user->id;
	  $this->_db->setQuery($query);
	  $comment = $this->_db->loadResult();

	  if (!$comment)
	  {
	    die('{ "status": 1, "message": "Post not found!" }');
	  }

	  $query = ' UPDATE #__wallfactory_posts p'
	         . ' SET p.published = !p.published'
	         . ' WHERE p.id = ' . $id;
	  $this->_db->setQuery($query);

	  $this->_db->query();

	  die(' { "status" : 2 }');
	}
	
	function getNotificationFollowers()
    {
    $user     =& JFactory::getUser();

    $query_wall = ' SELECT w.id '
           . ' FROM #__wallfactory_walls w '
    	   . ' WHERE w.published = 1'
           . ' AND w.user_id = '.$user->id;
           
    $this->_db->setQuery($query_wall);
    $wall_id = $this->_db->loadResult();
    
  	$query = ' SELECT f.user_id '
           . ' FROM #__wallfactory_followers f '
    	   . ' LEFT JOIN #__wallfactory_walls w ON w.id = f.wall_id'
           . ' WHERE w.published = 1'
           . ' AND w.id = '.$wall_id;
               
    $this->_db->setQuery($query);

    return implode(', ', $this->_db->loadResultArray());
  }

  
  // Helpers
  function getAdmins()
  {
    $query = ' SELECT u.id, u.username'
           . ' FROM #__users u'
           . ' WHERE u.gid IN (24, 25)';
    $this->_db->setQuery($query);
    return $this->_db->loadObjectList();
  }
  
  function sendNewPostNotification($post)
  {
   // Send report to wall owner
	/*  if ($post->report_post_notification)
	  {
	    $array[] = $post->wall_owner_id;
	  }
  	*/
  	$ids =  $this->getNotificationFollowers();
  	
    if ($ids != '') {
    $query = ' SELECT u.email, u.username'
           . ' FROM #__users u'
           . ' WHERE u.id IN (' . $ids . ')';
    $this->_db->setQuery($query);
  
    $receivers = $this->_db->loadObjectList();
    
    $query = ' SELECT w.title'
           . ' FROM #__wallfactory_posts p'
           . ' LEFT JOIN #__wallfactory_walls w ON w.id = p.wall_id'
           . ' WHERE p.id = ' . $post->id;
    $this->_db->setQuery($query);
    $wall_title = $this->_db->loadResult();

    $subject = base64_decode($this->wallSettings->notification_new_post_subject);
    $message = base64_decode($this->wallSettings->notification_new_post_message);

    $search  = array('%%username%%', '%%walltitle%%',  '%%postlink%%', '%%walllink%%');

    foreach ($receivers as $receiver)
    {
      $replace = $this->getEmailReplacements(
        $receiver->username,
        $wall_title,
        $post->id,
        $post->wall_id,
        $post->alias
      );

      $custom_subject = str_replace($search, $replace, $subject);
      $custom_message = str_replace($search, $replace, $message);

      if ($this->wallSettings->enable_notification_email_html)
      {
        $custom_message = nl2br($custom_message);
      }

      $mail =& JFactory::getMailer();
      $mail->addRecipient($receiver->email);
      $mail->setSubject(JText::_($custom_subject));
      $mail->setBody(JText::_($custom_message));
      $mail->IsHTML($this->wallSettings->enable_notification_email_html);

      $mail->send();
    }
    }
  }
  
  function sendAdminNewPostNotification($post)
  {
     
	$admin_ids = array();
	$admins = $this->getAdmins();

	foreach ($admins as $admin) {
		$admin_ids[] = $admin->id;
	}    
	  	
	$ids = implode(', ', $admin_ids);
	 
	// Send report to wall owner
	/*  if ($post->report_post_notification)
	  {
	    $array[] = $post->wall_owner_id;
	  }
  	*/
 	
    if ($ids != '') {
    	$query = ' SELECT u.email, u.username'
           . ' FROM #__users u'
           . ' WHERE u.id IN (' . $ids . ')';
    	$this->_db->setQuery($query);
    
    
    	$receivers = $this->_db->loadObjectList();

    $query = ' SELECT w.title'
           . ' FROM #__wallfactory_posts p'
           . ' LEFT JOIN #__wallfactory_walls w ON w.id = p.wall_id'
           . ' WHERE p.id = ' . $post->id;
    $this->_db->setQuery($query);
    $wall_title = $this->_db->loadResult();

    $subject = base64_decode($this->wallSettings->notification_new_post_subject);
    $message = base64_decode($this->wallSettings->notification_new_post_message);

    $search  = array('%%username%%', '%%walltitle%%',  '%%postlink%%', '%%walllink%%');

    foreach ($receivers as $receiver)
    {
      $replace = $this->getEmailReplacements(
        $receiver->username,
        $wall_title,
        $post->id,
        $post->wall_id,
        $post->alias
      );

      $custom_subject = str_replace($search, $replace, $subject);
      $custom_message = str_replace($search, $replace, $message);

      if ($this->wallSettings->enable_notification_email_html)
      {
        $custom_message = nl2br($custom_message);
      }

      $mail =& JFactory::getMailer();
      $mail->addRecipient($receiver->email);
      $mail->setSubject(JText::_($custom_subject));
      $mail->setBody(JText::_($custom_message));
      $mail->IsHTML($this->wallSettings->enable_notification_email_html);

      $mail->send();
    }
    }
  }

  function sendOwnerNewPostNotification($post, $wall_user_id)
  {
    
    if ($wall_user_id != '') {
    	$query = ' SELECT u.email, u.username'
           . ' FROM #__users u'
           . ' WHERE u.id = ' . $wall_user_id;
    	$this->_db->setQuery($query);
     	$receivers = $this->_db->loadObjectList();
    
    $query = ' SELECT w.title'
           . ' FROM #__wallfactory_posts p'
           . ' LEFT JOIN #__wallfactory_walls w ON w.id = p.wall_id'
           . ' WHERE p.id = ' . $post->id;
    $this->_db->setQuery($query);
    $wall_title = $this->_db->loadResult();

    $subject = base64_decode($this->wallSettings->notification_new_post_subject);
    $message = base64_decode($this->wallSettings->notification_new_post_message);

    $search  = array('%%username%%', '%%walltitle%%',  '%%postlink%%', '%%walllink%%');

    foreach ($receivers as $receiver)
    {
      $replace = $this->getEmailReplacements(
        $receiver->username,
        $wall_title,
        $post->id,
        $post->wall_id,
        $post->alias
      );

      $custom_subject = str_replace($search, $replace, $subject);
      $custom_message = str_replace($search, $replace, $message);

      if ($this->wallSettings->enable_notification_email_html)
      {
        $custom_message = nl2br($custom_message);
      }

      $mail =& JFactory::getMailer();
      $mail->addRecipient($receiver->email);
      $mail->setSubject(JText::_($custom_subject));
      $mail->setBody(JText::_($custom_message));
      $mail->IsHTML($this->wallSettings->enable_notification_email_html);
      
      $mail->send();
    }
    }
  }
  
  function getEmailReplacements($username, $wall_title, $post_id, $wall_id, $post_alias)
  {
    $replace = array(
      $username,
      $wall_title      
    );
    
    if ($this->wallSettings->enable_notification_email_html)
    {
      $replace[] = '<a href="' . JRoute::_(JURI::root() . 'index.php?option=com_wallfactory&view=post&id=' . $post_id. '&alias='.$post_alias) . '">' . $wall_title . '</a>';
      $replace[] = '<a href="' . JRoute::_(JURI::root() . 'index.php?option=com_wallfactory&view=wall&alias=' . $post_alias) . '">' . $wall_title . '</a>';
      
      /*$replace[] = '<a href="' . JRoute::_(JURI::root() . 'index.php?option=com_wallfactory&view=post&id=' . $post_id. '&alias='.$post_alias) . '">' . $wall_title . '</a>';
      $replace[] = '<a href="' . JRoute::_(JURI::root() . 'index.php?option=com_wallfactory&view=wall&alias=' . $post->alias) . '">' . $wall_title . '</a>';*/
    }
    else
    {
      $replace[] = JRoute::_(JURI::root() . 'index.php?option=com_wallfactory&view=post&id=' . $post_id);
      $replace[] = JRoute::_(JURI::root() . 'index.php?option=com_wallfactory&view=wall&id=' . $wall_id);
    }

    return $replace;
  }

  function sendFollowersNotifications($post)
  {
    $query = ' SELECT f.user_id, u.username, u.email, w.title AS wall_title'
           . ' FROM #__wallfactory_followers f'
           . ' LEFT JOIN #__users u ON u.id = f.user_id'
           . ' LEFT JOIN #__wallfactory_walls w ON w.id = f.wall_id'
           . ' WHERE f.wall_id = ' . $post->wall_id
           . ' AND f.notification = 1';
    $this->_db->setQuery($query);
    $followers = $this->_db->loadObjectList();

    $subject = base64_decode($this->wallSettings->notification_follow_wall_subject);
    $message = base64_decode($this->wallSettings->notification_follow_wall_message);

    $search  = array('%%username%%', '%%walltitle%%',  '%%postlink%%', '%%walllink%%');

    foreach ($followers as $follower)
    {
      $replace = $this->getEmailReplacements(
        $follower->username,
        $follower->wall_title,
        $post->id,
        $post->wall_id
      );

      $custom_subject = str_replace($search, $replace, $subject);
      $custom_message = str_replace($search, $replace, $message);

      if ($this->wallSettings->enable_notification_email_html)
      {
        $custom_message = nl2br($custom_message);
      }

      $mail =& JFactory::getMailer();
      $mail->addRecipient($follower->email);
      $mail->setSubject(JText::_($custom_subject));
      $mail->setBody(JText::_($custom_message));
      $mail->IsHTML($this->wallSettings->enable_notification_email_html);

      $mail->send();
    }
  }

  function getBookmarks()
  {
    if (is_null($this->_post))
    {
      return false;
    }  

    $post_id       =& JRequest::getVar('id', 0, 'GET', 'integer');
	$user     =& JFactory::getUser();  
    $text = '';
   	$bookmarks = array();
 
    $path = JURI::root().'index.php?option=com_wallfactory&view=post&id=' . $this->_post->id . '&alias=' . $this->_post->alias . '';
    $page_full_url = urlencode($path);
       
    $title         = urlencode(strip_tags(substr($this->_post->content, 0, 100)));
    $text          = urlencode(strip_tags(substr($this->_post->content, 0, 100)));
    $id 		= $this->_post->id;

   if ($text != '') {
	    $query = ' SELECT b.*'
	           . ' FROM #__wallfactory_bookmarks b'
	           . ' WHERE b.published = 1';
	    $this->_db->setQuery($query);
	    $bookmarks[$id] = $this->_db->loadObjectList();
	
	    foreach ($bookmarks[$id] as $i => $bookmark)
	    {
	      $link = str_replace(array('%%url%%', '%%title%%',  '%%bodytext%%'),
	                          array($page_full_url, $title, $text),
	                          $bookmark->link);
	      $bookmarks[$id][$i]->full_link = $link;
	    }
    }
     
    return $bookmarks;
  }

  function getVideoBookmarks()
  {
    
    $post_id  =& JRequest::getVar('id', 0, 'GET', 'integer');
    $video_title = '';
   
   	$query = ' SELECT l.*'
	           . ' FROM #__wallfactory_urls l '
	           . ' WHERE l.post_id = ' .$post_id;
	$this->_db->setQuery($query);
    $url = $this->_db->loadObject();

    if ($url) {
    
    $video_bookmarks = array();
 
    $path = JURI::root().'index.php?option=com_wallfactory&view=post&id=' . $this->_post->id . '&alias=' . $this->_post->alias . '';
    $page_full_url = urlencode($path);
    $video_title 	= ($url->video_title != '') ? urlencode($url->video_title) : urlencode($url->video_link);
 
    if ($video_title != '') {
	    $query = ' SELECT b.*'
	           . ' FROM #__wallfactory_bookmarks b'
	           . ' WHERE b.published = 1';
	    $this->_db->setQuery($query);
	    $video_bookmarks[$post_id] = $this->_db->loadObjectList();
	
	    foreach ($video_bookmarks[$post_id] as $i => $bookmark)
	    {
	      $video_link = str_replace(array('%%url%%',   '%%title%%'),
	                          array($page_full_url, $video_title),
	                          $bookmark->link);
	                          
	      $video_bookmarks[$post_id][$i]->full_link = $video_link;
	    }
 }
   	return $video_bookmarks; 
    }
    else 
    	return false;
  }
  
  function getUrlBookmarks()
  {
    
    $post_id  =& JRequest::getVar('id', 0, 'GET', 'integer');
    $url_title = '';
    $url_bookmarks = array();
        
    $query = ' SELECT l.*'
	           . ' FROM #__wallfactory_urls l '
	           . ' WHERE l.post_id = ' .$post_id;

	$this->_db->setQuery($query);
    $url = $this->_db->loadObject();
  
   if ($url) {
 
	$path = JURI::root().'index.php?option=com_wallfactory&view=post&id=' . $this->_post->id . '&alias=' . $this->_post->alias . '';
    $page_full_url = urlencode($path);
	$url_title     = urlencode($url->url);
		
	if ($url_title != '') {
		
	    $query = ' SELECT b.*'
	           . ' FROM #__wallfactory_bookmarks b'
	           . ' WHERE b.published = 1';
	    $this->_db->setQuery($query);
	    $url_bookmarks[$post_id] = $this->_db->loadObjectList();
	
	    foreach ($url_bookmarks[$post_id] as $i => $bookmark)
	    {
	      $url_link = str_replace(array('%%url%%',   '%%title%%'),
	                          array($page_full_url, $url_title),
	                          $bookmark->link);
	      $url_bookmarks[$post_id][$i]->full_link = $url_link;
	    }
	}
    
    return $url_bookmarks;
    }
    else 
    	return false;
  }
  
  function getMp3Bookmarks()
  {
    $mp3_bookmarks = array();
    $post_id  =& JRequest::getVar('id', 0, 'GET', 'integer');
    $mp3_title = '';
    
   	$query = ' SELECT m.*'
	           . ' FROM #__wallfactory_media m '
	           . ' WHERE m.post_id = '.$post_id
	           . ' AND folder = "mp3" ';
	$this->_db->setQuery($query);
 		
    $media = $this->_db->loadObject();
    if ($media) {
    	$path = JURI::root().'index.php?option=com_wallfactory&view=post&id=' . $this->_post->id . '&alias=' . $this->_post->alias . '';
    	$page_full_url = urlencode($path);
	    if ($media->folder == 'mp3')
	    	$mp3_title     = urlencode($media->title);
		
		if ($mp3_title != '') {
		    $query = ' SELECT b.*'
		           . ' FROM #__wallfactory_bookmarks b'
		           . ' WHERE b.published = 1';
		    $this->_db->setQuery($query);
		    $mp3_bookmarks[$post_id] = $this->_db->loadObjectList();
		
		    foreach ($mp3_bookmarks[$post_id] as $i => $bookmark)
		    {
		      $mp3_link = str_replace(array('%%url%%',   '%%title%%'),
		                          array($page_full_url, $mp3_title),
		                          $bookmark->link);
		      $mp3_bookmarks[$post_id][$i]->full_link = $mp3_link;
		    }
		}
    
    return $mp3_bookmarks;
    }
    else 
    	return false;
  }
  
  function getFileBookmarks()
  {
    $file_bookmarks = array();
    $post_id  =& JRequest::getVar('id', 0, 'GET', 'integer');
    $file_title = '';
    
    $query = ' SELECT m.*'
	           . ' FROM #__wallfactory_media m '
	           . ' WHERE m.post_id = '.$post_id
	           . ' AND folder = "files" ';
	$this->_db->setQuery($query);
		
    $media = $this->_db->loadObject();
	if ($media) {
    	$path = JURI::root().'index.php?option=com_wallfactory&view=post&id=' . $this->_post->id . '&alias=' . $this->_post->alias . '';
    	$page_full_url = urlencode($path);
	    if ($media->folder == 'files')
	    	$file_title     = urlencode($media->title);
		
		if ($file_title != '') {
			$query = ' SELECT b.*'
		           . ' FROM #__wallfactory_bookmarks b'
		           . ' WHERE b.published = 1';
		    $this->_db->setQuery($query);
		    $file_bookmarks[$post_id] = $this->_db->loadObjectList();
		
		    foreach ($file_bookmarks[$post_id] as $i => $bookmark)
		    {
		      $file_link = str_replace(array('%%url%%',   '%%title%%'),
		                          array($page_full_url, $file_title),
		                          $bookmark->link);
		      $file_bookmarks[$post_id][$i]->full_link = $file_link;
		    }
		}
    
    return $file_bookmarks;
    }
    else 
    	return false;
  }
  
  function getImageBookmarks()
  {
    $image_bookmarks = array();
    $post_id  =& JRequest::getVar('id', 0, 'GET', 'integer');
    $image_title = '';
    
   $query = ' SELECT m.*'
	           . ' FROM #__wallfactory_media m '
	           . ' WHERE m.post_id = '.$post_id
	           . ' AND folder = "images" ';
	$this->_db->setQuery($query);
		
    $media = $this->_db->loadObject();
	if ($media) {
    	$path = JURI::root().'index.php?option=com_wallfactory&view=post&id=' . $this->_post->id . '&alias=' . $this->_post->alias . '';
    	$page_full_url = urlencode($path);
	    if ($media->folder == 'images')
	    	$image_title     = urlencode($media->title);

		if ($image_title != '') {
		
		    $query = ' SELECT b.*'
		           . ' FROM #__wallfactory_bookmarks b'
		           . ' WHERE b.published = 1';
		    $this->_db->setQuery($query);
		    $image_bookmarks[$post_id] = $this->_db->loadObjectList();
		
		    foreach ($image_bookmarks[$post_id] as $i => $bookmark)
		    {
		      $image_link = str_replace(array('%%url%%',   '%%title%%'),
		                          array($page_full_url, $image_title),
		                          $bookmark->link);
		      $image_bookmarks[$post_id][$i]->full_link = $image_link;
		    }
		}
    
    return $image_bookmarks;
    }
    else 
    	return false;
  }  
  
  
  function follow()
  {
    $user   =& JFactory::getUser();
    $action =  JRequest::getVar('action', '', 'GET', 'string');

    if (!in_array($action, array('subscribe', 'unsubscribe')) || $user->guest)
    {
      return false;
    }

    $id = JRequest::getVar('id', 0, 'GET', 'integer');

    if ($action == 'subscribe')
    {
      $query = ' SELECT *'
             . ' FROM #__wallfactory_followers'
             . ' WHERE wall_id = ' . $id
             . ' AND user_id = ' . $user->id;
      $this->_db->setQuery($query);
      $check = $this->_db->loadResult();

      if (!$check)
      {
        $query = ' INSERT'
               . ' INTO #__wallfactory_followers'
               . ' VALUES ("", ' . $id . ', ' . $user->id . ', NOW())';
        $this->_db->setQuery($query);
        $this->_db->query();

        return true;
      }
    }
    else
    {
      $query = ' DELETE'
             . ' FROM #__wallfactory_followers'
             . ' WHERE wall_id = ' . $id
             . ' AND user_id = ' . $user->id;
      $this->_db->setQuery($query);
      $this->_db->query();

      return true;
    }

    return false;
  }

}