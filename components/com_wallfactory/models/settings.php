<?php
/*------------------------------------------------------------------------
com_wallfactory - Wall Factory
------------------------------------------------------------------------
author    TheFactory
copyright Copyright (C) 2010 SKEPSIS Consult SRL. All Rights Reserved.
@license - http://www.gnu.org/licenses/gpl-2.0.html GNU/GPL
Websites: http://www.thefactory.ro
Technical Support:  Forum - http://www.thefactory.ro/joomla-forum/
-------------------------------------------------------------------------*/

defined('_JEXEC') or die('Restricted access');

jimport('joomla.application.component.model');

class FrontendModelSettings extends JModel
{
  function __construct()
  {
    parent::__construct();
  }
    
  function getSettings()
  {
    $user =& JFactory::getUser();

    $query = ' SELECT * '
           . ' FROM #__wallfactory_members '
           . ' WHERE user_id = ' . $user->id;
    $this->_db->setQuery($query);
    $settings = $this->_db->loadObject();

    if (is_null($settings))
    {
      $settings =& $this->getTable('members');
      $settings->user_id = $user->id;
      $settings->store();

      // Create user folder
      $folder = JPATH_SITE.DS.'components'.DS.'com_wallfactory'.DS.'storage'.DS.'users'.DS.$user->id;
      if (!file_exists($folder))
      {
        @mkdir($folder, 0775);
        @mkdir($folder.DS.'avatar', 0775);
        @mkdir($folder.DS.'folder', 0775);
        @mkdir($folder.DS.'folder'.DS.'root', 0775);
      }
    }

    return $settings;
  }

  function getWallSettings()
  {
    $user =& JFactory::getUser();

    $query = ' SELECT *'
           . ' FROM #__wallfactory_walls'
           . ' WHERE user_id = ' . $user->id;
    $this->_db->setQuery($query);
    return $this->_db->loadObject();
  }

  function store()
  {
    // Store settings
    $settings = $this->getSettings();

    $wall =& $this->getTable('members');
    $data =  JRequest::get('post');
    $data['id'] = $settings->id;

    if (!$wall->bind($data))
    {
      JError::raiseWarning($this->_db->getErrorMsg());
      return false;
    }

    if (!$wall->check())
    {
      JError::raiseWarning($this->_db->getErrorMsg());
      return false;
    }

    if (!$wall->store())
    {
      JError::raiseWarning($this->_db->getErrorMsg());
      return false;
    }

    // Store wall settigns
    if (wallHelper::checkWallOwner(false))
    {
      $settings = $this->getWallSettings();

      $wall_settings =& $this->getTable('Wall');
      $data['id']    =  $settings->id;

      if (!$wall_settings->bind($data))
      {
        JError::raiseWarning($this->_db->getErrorMsg());
        return false;
      }

      if (!$wall_settings->check())
      {
        JError::raiseWarning($this->_db->getErrorMsg());
        return false;
      }

      if (!$wall_settings->store())
      {
        JError::raiseWarning($this->_db->getErrorMsg());
        return false;
      }
    }

   /* 
    $this->updateSettings();

    return $this->writeSettings();*/
    
    return true;
  }
}