<?php
/*------------------------------------------------------------------------
com_wallfactory - Wall Factory
------------------------------------------------------------------------
author    TheFactory
copyright Copyright (C) 2010 SKEPSIS Consult SRL. All Rights Reserved.
@license - http://www.gnu.org/licenses/gpl-2.0.html GNU/GPL
Websites: http://www.thefactory.ro
Technical Support:  Forum - http://www.thefactory.ro/joomla-forum/
-------------------------------------------------------------------------*/

defined('_JEXEC') or die('Restricted access');

jimport('joomla.application.component.model');

class FrontendModelManageFollowed extends JModel
{
  var $_data;
  var $_total;
  var $_pagination;
  var $_limit;
  var $_limitstart;
  var $_wall_id;

  function __construct()
  {
    parent::__construct();

    global $mainframe, $option;
    $this->_limit      = JRequest::getVar('limit',      10, 'REQUEST', 'integer');
    $this->_limitstart = JRequest::getVar('limitstart', 0,  'REQUEST', 'integer');

    $this->_limitstart = ($this->_limit != 0 ? (floor($this->_limitstart / $this->_limit) * $this->_limit) : 0);
  }

  function getData()
  {
    if (empty($this->_data))
    {
      $query = $this->_buildQuery();
      $this->_data = $this->_getList($query, $this->_limitstart, $this->_limit);
    }
				    
    return $this->_data;
  }

  function _buildQuery()
  {
    $where =  $this->_buildWhereCondition();
    $query = ' SELECT f.id, f.wall_id, f.notification, f.date_created, w.user_id, w.title, w.alias, u.username, MAX(p.date_created)'
           . ' FROM #__wallfactory_followers f'
           . ' LEFT JOIN #__wallfactory_walls w ON w.id = f.wall_id'
           . ' LEFT JOIN #__users u ON u.id = w.user_id'
           . ' LEFT JOIN #__wallfactory_posts p ON p.wall_id = w.id'
           . $where
           . ' GROUP BY w.id'
           . ' ORDER BY f.date_created DESC';
           
    return $query;
  }

  function _buildWhereCondition()
  {
    $user    =& JFactory::getUser();
    $session =& JFactory::getSession();

    $where = ' WHERE f.user_id = ' . $user->id
           . ' AND w.published = 1';

    switch ($session->get('filter.followed.notification', ''))
    {
      case 'with':
        $where .= ' AND f.notification = 1';
      break;

      case 'without':
        $where .= ' AND f.notification = 0';
      break;
    }

    return $where;
  }

  function getTotal()
  {
    if (empty($this->_total))
    {
      $query = $this->_buildQuery();
      $this->_total = $this->_getListCount($query);
    }

    return $this->_total;
  }

  function getPagination()
  {
    if (empty($this->_pagination))
    {
      jimport('joomla.html.pagination');
      $this->_pagination = new JPagination($this->getTotal(), $this->_limitstart, $this->_limit);
    }

    return $this->_pagination;
  }
  
function getSettings()
  {
    $user =& JFactory::getUser();

    $query = ' SELECT *'
           . ' FROM #__wallfactory_members'
           . ' WHERE user_id = ' . $user->id;
    $this->_db->setQuery($query);
    return $this->_db->loadObject();
  }
  
  function getLatestPosts()
  {
  	$post = array();
  	foreach ($this->_data as $k=>$data) {
  	  if (!isset($data->user_id))
	  {
	      return false;
	  } else {

        $query = ' SELECT p.id, p.wall_id, p.user_id, p.alias, p.content, p.date_created, p.date_updated  '
    		. ' , w.title AS wall_title, w.user_id as wall_owner, w.alias AS wall_alias,  '
    		. '  u.username, u.id as wall_owner_id, '
    		. ' s.avatar_extension '
           	. ' FROM #__wallfactory_posts p'
           	. ' LEFT JOIN #__wallfactory_walls w ON w.id = p.wall_id'
           	. ' LEFT JOIN #__users u ON u.id = w.user_id'
           	. ' LEFT JOIN #__wallfactory_members s ON s.user_id = p.user_id '
           	. ' WHERE p.user_id = '.$data->user_id
           	. ' ORDER BY p.date_created DESC'
           	. ' LIMIT 0, 1 ';
            
    $this->_db->setQuery($query);
   	$post[$data->id] = $this->_db->loadAssoc();

    $this->_post = $post;
     
	    }
  	}       

	return $post;

  }
  
  function getTotalPosts() {
  	if (!empty($this->_data))
	{
	   	$count = array();

      foreach ($this->_data as $i => $post) { 
           $query = ' SELECT COUNT(distinct p.id) as posts_no '
      		. ' FROM #__wallfactory_posts p'
      		. ' WHERE p.user_id = ' . $post->user_id;
	   
  	      $this->_db->setQuery($query);
	      $result = $this->_db->loadResult();
	      $count[$post->user_id] = $result;
      }
      
      return $count;			
	}
  }

  
  function getMedia() 
  {
	$user =& JFactory::getUser();
	$media = array();
 
	if (!isset($this->_post))
	{
	      return false;
	} 
		
	foreach ($this->_post as $data) {
		
		$query = ' SELECT m.*'
		           . ' FROM #__wallfactory_media m '
		           . ' WHERE m.post_id = '.$data['id'];
		         
		 	$this->_db->setQuery($query);
	    
	  		$followed = $data['id'];
	    	$media["$followed"] = $this->_db->loadObjectList();

	  return $media;
  	}
  }
  
  function getUrls() 
  {
	$user =& JFactory::getUser();
	$urls = array();
 
	if (!isset($this->_post))
	{
	      return false;
	} 
		
	foreach ($this->_post as $data) {
  	
      	 $query = ' SELECT l.*'
	           . ' FROM #__wallfactory_urls l '
	           . ' WHERE l.post_id = ' .$data['id'];

	     $this->_db->setQuery($query);
    
 		$followed = $data['id'];
	    $urls["$followed"] = $this->_db->loadObjectList();
    	
   	  }

     return $urls;
		
  }
  
}