<?php
/*------------------------------------------------------------------------
com_wallfactory - Wall Factory
------------------------------------------------------------------------
author    TheFactory
copyright Copyright (C) 2010 SKEPSIS Consult SRL. All Rights Reserved.
@license - http://www.gnu.org/licenses/gpl-2.0.html GNU/GPL
Websites: http://www.thefactory.ro
Technical Support:  Forum - http://www.thefactory.ro/joomla-forum/
-------------------------------------------------------------------------*/

defined('_JEXEC') or die('Restricted access');

jimport('joomla.application.component.model');

class FrontendModelFeed extends JModel
{
  function __construct()
  {
    parent::__construct();
  }

  function getChannel()
  {
  	$alias =& JRequest::getVar('alias', '', 'GET', 'cmd');
    $where = '';
	
	if ($alias != null) 
		$where =  ' WHERE w.alias = "' . $alias . '"';
	
    $query = ' SELECT w.*'
           . ' FROM #__wallfactory_walls w'
           //. ' WHERE w.alias = "' . $alias . '"';
           . $where;  
  
    $this->_db->setQuery($query);
    $wall = $this->_db->loadObject();

    return $wall;
  }

  function getData()
  {
    $content =& JRequest::getVar('content', 'entries', 'GET', 'string');

    if (!in_array($content, array('entries', 'comments')))
    {
      $content = 'entries';
    }

    switch ($content)
    {
      case 'entries':
        return $this->getEntries();
      break;

      case 'comments':
        return $this->getComments();
      break;
    }
  }

  // Helpers
  function getEntries()
  {
    $alias =& JRequest::getVar('alias', '', 'GET', 'cmd');
	$where = '';
	
	if ($alias != null) 
		$where =  ' AND w.alias = "' . $alias . '"';
		
    $query = ' SELECT p.id, CONCAT("' . JText::_('Posted by') . ' ", name ) AS title, p.alias, p.date_created, p.content, IF ( u.username = "", "' . JText::_(' Guest') . '",  u.username) AS name'
           . ' FROM #__wallfactory_posts p'
           . ' LEFT JOIN #__wallfactory_walls w ON w.id = p.wall_id'
           . ' LEFT JOIN #__users u ON u.id = p.user_id'
           . ' WHERE w.published = 1'
           . $where
           . ' ORDER BY p.date_created DESC'
           . ' LIMIT 0, 15';
    $this->_db->setQuery($query);
    $entries = $this->_db->loadObjectList();

    foreach ($entries as $i => $entry)
    {
      $entries[$i]->link = JRoute::_('index.php?option=com_wallfactory&view=post&id=' . $entry->id . '&alias=' . $entry->alias);
    }

    return $entries;
  }

  function getComments()
  {
  	global $Itemid;  
  	$alias =& JRequest::getVar('alias', '', 'GET', 'cmd');
	$where = '';
	
	if ($alias != null) 
		$where =  ' AND c.author_alias = "' . $alias . '"';
	
    
    $query = ' SELECT c.*, w.alias, CONCAT("' . JText::_('Comment on') . ' ", w.alias , "' . JText::_(' post') . '") AS title, u.username, s.avatar_type, s.avatar_extension, IF (s.name = "", u.username, s.name) AS name, p.id as post_id'
           . ' FROM #__wallfactory_comments c'
           . ' LEFT JOIN #__users u ON u.id = c.user_id'
           . ' LEFT JOIN #__wallfactory_posts p ON p.id = c.post_id'
           . ' LEFT JOIN #__wallfactory_walls w ON w.id = p.wall_id'
           . ' LEFT JOIN #__wallfactory_members s ON s.user_id = c.user_id'
           . ' WHERE w.published = 1'
           . $where
           . ' ORDER BY c.date_created DESC '
           . ' LIMIT 0, 15';
    
    $this->_db->setQuery($query);
  
    $comments = $this->_db->loadObjectList();

    foreach ($comments as $i => $comment)
    {
    	$comments[$i]->link = JRoute::_('index.php?option=com_wallfactory&view=comments&layout=_list&post_id=' . $comment->post_id . '&alias=' . $comment->alias . '&Itemid='.$Itemid);
    }

    return $comments;
  }
}