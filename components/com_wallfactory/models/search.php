<?php
/*------------------------------------------------------------------------
com_wallfactory - Wall Factory
------------------------------------------------------------------------
author    TheFactory
copyright Copyright (C) 2010 SKEPSIS Consult SRL. All Rights Reserved.
@license - http://www.gnu.org/licenses/gpl-2.0.html GNU/GPL
Websites: http://www.thefactory.ro
Technical Support:  Forum - http://www.thefactory.ro/joomla-forum/
-------------------------------------------------------------------------*/

defined('_JEXEC') or die('Restricted access');

jimport('joomla.application.component.model');

class FrontendModelSearch extends JModel
{
  var $_data;
  var $_total;
  var $_pagination;
  var $_limit;
  var $_limitstart;

  function __construct()
  {
    parent::__construct();

    global $mainframe, $option;
    $limit      =& JRequest::getVar('limit',      20, 'REQUEST', 'integer');
    $limitstart =& JRequest::getVar('limitstart', 0,  'REQUEST', 'integer');

    $limitstart = ($limit != 0 ? (floor($limitstart / $limit) * $limit) : 0);

    $this->_limit      = $limit;
    $this->_limitstart = $limitstart;
  }

  function getData()
  {
    if (empty($this->_data))
    {
      $query = $this->_buildQuery();
      $this->_data = $this->_getList($query, $this->_limitstart, $this->_limit);
    }

    return $this->_data;
  }

  function _buildQuery()
  {
    $having = $this->_buildContentHaving();

    $query = ' SELECT p.id, p.date_created, p.content, p.alias, CONCAT("' . JText::_('Posted at') . ' ", p.date_created ) AS title, '
           . ' p.wall_id, w.alias AS wall_alias'
           . ' FROM #__wallfactory_posts p'
           . ' LEFT JOIN #__wallfactory_walls w ON w.id = p.wall_id'
           . ' GROUP BY p.id '
           . $having
           . ' ORDER BY p.date_created DESC';
           
    return $query;
  }

  function _buildContentHaving()
  {
    $type =& JRequest::getVar('type', '', 'GET', 'string');
    $q    =& JRequest::getVar('q', '', 'GET', 'string');
    $q    =  urldecode($q);

    $keywords = explode(',', $q);
    $operand = 'AND';

    $conditions = array();
    foreach ($keywords as $keyword)
    {
      $condition = array();
      $keyword = trim($this->_db->getEscaped($keyword));
      $keyword = html_entity_decode($keyword);

      if ($type != 'tags')
      {
        $condition[] = 'p.content LIKE "%' . $keyword . '%"';
        $condition[] = 'w.alias LIKE "%' . $keyword . '%"';
      }


      $condition = implode(' OR ', $condition);
      $condition = '(' . $condition . ')';

      $conditions[] = $condition;
    }

    $conditions  = ' HAVING ' . implode(' ' . $operand . ' ', $conditions);

    return $conditions;
  }

  function getTotal()
  {
    if (empty($this->_total))
    {
      $query = $this->_buildQuery();
      $this->_total = $this->_getListCount($query);
    }

    return $this->_total;
  }

  function getPagination()
  {
    if (empty($this->_pagination))
    {
      jimport('joomla.html.pagination');
      $this->_pagination = new JPagination($this->getTotal(), $this->_limitstart, $this->_limit);
    }

    return $this->_pagination;
  }

}