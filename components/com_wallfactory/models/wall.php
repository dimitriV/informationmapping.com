<?php
/*------------------------------------------------------------------------
com_wallfactory - Wall Factory
------------------------------------------------------------------------
author    TheFactory
copyright Copyright (C) 2010 SKEPSIS Consult SRL. All Rights Reserved.
@license - http://www.gnu.org/licenses/gpl-2.0.html GNU/GPL
Websites: http://www.thefactory.ro
Technical Support:  Forum - http://www.thefactory.ro/joomla-forum/
-------------------------------------------------------------------------*/

defined('_JEXEC') or die('Restricted access');

jimport('joomla.application.component.model');

class FrontendModelWall extends JModel
{
  var $_id;
  var $_user_id;
  var $_data;
  var $_total;
  var $_pagination;
  var $_limit;
  var $_limitstart;
  
  var $_alias;
  var $_post;
  var $_posts;
  var $_urls;
  var $_media;

  function __construct()
  {
    parent::__construct();

	$this->alias =& JRequest::getVar('alias', '', 'REQUEST', 'cmd');
    $this->wallSettings = new wallSettings();
  }

  
  function getWallData()
  {
    $user   =& JFactory::getUser();
    
  	if (empty($this->_data))
    {
      $alias =& JRequest::getVar('alias', '', 'REQUEST', 'cmd');
    
      if (isset($alias) && ($alias != '')) {
       	$where = '  WHERE w.alias = "' . $alias . '"';
       	
      } else {
      	$where = ' WHERE w.user_id = "' . $user->id .'"';
      }
            
      $query = ' SELECT w.*, '
      		 . ' (select COUNT(f.id) from #__wallfactory_followers f  where  f.wall_id = w.id  ) AS followers, '
      		 . ' s.avatar_extension, s.name, s.gender, s.description, s.birthday, s.avatar_type  '
             . ' FROM #__wallfactory_walls w'
             . ' LEFT JOIN #__wallfactory_followers f ON f.wall_id = w.id'
             . ' LEFT JOIN #__wallfactory_members s ON s.user_id = w.user_id '
             .  $where
             . ' AND w.published = 1';

      $this->_db->setQuery($query);
      $this->_data = $this->_db->loadObject();

      $this->_data->layout = 'archive';
    }

    return $this->_data; 
  }
  
  function &getData()
  {
  	$alias =& JRequest::getVar('alias', '', 'GET', 'string');
	
    if (empty($this->_posts))
    {
      $user =& JFactory::getUser();

      $query = ' SELECT p.*, w.alias AS wall_alias, s.name '
             . ' FROM #__wallfactory_posts p'
             . ' LEFT JOIN #__wallfactory_walls w ON w.id = p.wall_id'
             . ' LEFT JOIN #__wallfactory_members s ON s.user_id = w.user_id '
             . ' WHERE p.alias = "' . $alias . '"'
             . ' AND w.user_id = ' . $user->id;

      $this->_db->setQuery($query);

      $this->_posts = $this->_db->loadObject();
    }

    if (!$this->_posts)
    {
      $this->_posts = $this->getTable('post');
      $this->_posts->published = 1;
    }
    
    return $this->_posts;
  }

  function getPosts()
  {
    if (!isset($this->_data->id))
    {
      return false;
    }

    // Set pagination variables
    JRequest::setVar('limit', $this->_data->posts_per_page, 'POST');
    $model =& JModel::getInstance('posts', 'FrontendModel');
    $this->_post = $model->getData($this->_data->id);
    
   return $this->_post;
 
  }

  function getPostsPagination()
  {
    if (!isset($this->_data->id))
    {
      return false;
    }

    $model =& JModel::getInstance('posts', 'FrontendModel');
    return $model->getPagination($this->_data->id);
  }

  function getComments()
  {
      $user 	=& JFactory::getUser();
  	  $post_id  =& JRequest::getVar('id', 0, 'GET', 'integer');
  	  $database =& JFactory::getDBO();

  	  JRequest::setVar('limit', $this->_data->posts_per_page, 'POST');
      $model =& JModel::getInstance('posts', 'FrontendModel');
      
      return $model->getComments($this->_data->id);

	}
	
	function getMedia() {
		$user =& JFactory::getUser();
		$post_id  =& JRequest::getVar('post_id', 0, 'GET', 'integer');
		$database =& JFactory::getDBO();
  	  
  	  JRequest::setVar('limit', $this->_data->posts_per_page, 'POST');

      $model =& JModel::getInstance('posts', 'FrontendModel');
	  $this->_media = $model->getMedia($this->_data->id);   

      return $model->getMedia($this->_data->id);
	}

  function getTotalPosts() {
  	if (!empty($this->_post))
	{
	   	$count = array();

      foreach ($this->_post as $i => $post) { 
           $query = ' SELECT COUNT(distinct p.id) as posts_no '
      		. ' FROM #__wallfactory_posts p'
      		. ' WHERE p.user_id = ' . $post->user_id;
      		
	      $this->_db->setQuery($query);
	      $result = $this->_db->loadResult();
	      $count[$post->user_id] = $result;
      }
      
      return $count;			
	}
  }
	
	
  function getFollowed()
  {
    if (!isset($this->_data->id))
    {
      return false;
    }

    $user =& JFactory::getUser();

    $query = ' SELECT f.id'
           . ' FROM #__wallfactory_followers f'
           . ' WHERE f.wall_id = ' . $this->_data->id
           . ' AND f.user_id = ' . $user->id;
    $this->_db->setQuery($query);

    return $this->_db->loadResult();
  }

  function getIsMyWall()
  {
    $user =& JFactory::getUser();

    return isset($this->_data->user_id) ? ($user->id == $this->_data->user_id) : 0;
  }
  
  function getSettings()
  {
    $user =& JFactory::getUser();

    $query = ' SELECT *'
           . ' FROM #__wallfactory_members'
           . ' WHERE user_id = ' . $user->id;
    $this->_db->setQuery($query);
    return $this->_db->loadObject();
  }
  
  function getUrls()
  {
  	$user =& JFactory::getUser();
	$post_id  =& JRequest::getVar('id', 0, 'GET', 'integer');
	$database =& JFactory::getDBO();
  	  
  	JRequest::setVar('limit', $this->_data->posts_per_page, 'POST');
    $model =& JModel::getInstance('posts', 'FrontendModel');
    $this->_urls = $model->getUrls($this->_data->id);   
    
    return $model->getUrls($this->_data->id);  
  
  }
 
  function storeUrls()
  {
    // Store settings
    $urls = $this->getUrls();

    $wall =& $this->getTable('urls');
    $data =  JRequest::get('post');
    $data['id'] = $urls->id;

    if (!$wall->bind($data))
    {
      JError::raiseWarning($this->_db->getErrorMsg());
      return false;
    }

    if (!$wall->check())
    {
      JError::raiseWarning($this->_db->getErrorMsg());
      return false;
    }

    if (!$wall->store())
    {
      JError::raiseWarning($this->_db->getErrorMsg());
      return false;
    }

    // Store wall settigns
    if (wallHelper::checkWallOwner(false))
    {
      $settings = $this->getwallSettings();

      $wall_settings =& $this->getTable('Wall');
      $data['id']    =  $settings->id;

      if (!$wall_settings->bind($data))
      {
        JError::raiseWarning($this->_db->getErrorMsg());
        return false;
      }

      if (!$wall_settings->check())
      {
        JError::raiseWarning($this->_db->getErrorMsg());
        return false;
      }

      if (!$wall_settings->store())
      {
        JError::raiseWarning($this->_db->getErrorMsg());
        return false;
      }
    }

    return true;
  }
  
  
  function getBookmarks()
  {
    if (is_null($this->_post))
    {
      return false;
    }
    
    $text = '';
   	$bookmarks = array();
  	foreach ($this->_post as $k=>$post) {
 
    $path = JURI::root().'index.php?option=com_wallfactory&view=post&id=' . $post->id . '&alias=' . $post->alias . '';
    $page_full_url = urlencode($path);
       
    $title         = urlencode(strip_tags(substr($post->content, 0, 100)));
    $text          = urlencode(strip_tags(substr($post->content, 0, 100)));
    $id = $post->id;

    if ($text != '') {
	    $query = ' SELECT b.*'
	           . ' FROM #__wallfactory_bookmarks b'
	           . ' WHERE b.published = 1';
	    $this->_db->setQuery($query);
	    $bookmarks[$id] = $this->_db->loadObjectList();
	
	    foreach ($bookmarks[$id] as $i => $bookmark)
	    {
	      $link = str_replace(array('%%url%%', '%%title%%',  '%%bodytext%%'),
	                          array($page_full_url, $title, $text),
	                          $bookmark->link);
	      $bookmarks[$id][$i]->full_link = $link;
	    }
    }
  }
    return $bookmarks;
  }
   
  function getVideoBookmarks()
  {
    $video_title = '';
    if (is_null($this->_urls))
    {
      return false;
    }

   $video_bookmarks = array();
   foreach ($this->_urls as $k=>$url) {
 
 
   foreach ($this->_urls[$k] as $vurl) {
 
    $page_full_url  = urlencode((isset($_SERVER['HTTPS']) ? 'https' : 'http') . '://' . $_SERVER['HTTP_HOST'] . $_SERVER['REQUEST_URI']);
    $video_title 	= ($vurl->video_title != '') ? urlencode($vurl->video_title) : urlencode($vurl->video_link);
    
    $id = $vurl->post_id;
 
    if ($video_title != '') {
	    $query = ' SELECT b.*'
	           . ' FROM #__wallfactory_bookmarks b'
	           . ' WHERE b.published = 1';
	    $this->_db->setQuery($query);
	    $video_bookmarks[$id] = $this->_db->loadObjectList();
	
	    foreach ($video_bookmarks[$id] as $i => $bookmark)
	    {
	      $video_link = str_replace(array('%%url%%',   '%%title%%'),
	                          array($page_full_url, $video_title),
	                          $bookmark->link);
	                          
	      $video_bookmarks[$id][$i]->full_link = $video_link;
	    }
    }
 }
 }

    return $video_bookmarks;
  }
  
  
  function getUrlBookmarks()
  {
    
    $post_id  =& JRequest::getVar('id', 0, 'GET', 'integer');
    $url_title = '';
    $url_bookmarks = array();
        
    if (is_null($this->_urls))
    {
      return false;
    }

    foreach ($this->_urls as $k=>$url) {
    
 	  foreach ($this->_urls[$k] as $uurl) {
 	  	  
	    $page_full_url = urlencode((isset($_SERVER['HTTPS']) ? 'https' : 'http') . '://' . $_SERVER['HTTP_HOST'] . $_SERVER['REQUEST_URI']);
	    $url_title     = urlencode($uurl->url);
		
		$id = $uurl->post_id;
		
		if ($url_title != '') {
		
		    $query = ' SELECT b.*'
		           . ' FROM #__wallfactory_bookmarks b'
		           . ' WHERE b.published = 1';
		    $this->_db->setQuery($query);
		    $url_bookmarks[$id] = $this->_db->loadObjectList();
		
		    foreach ($url_bookmarks[$id] as $i => $bookmark)
		    {
		      $url_link = str_replace(array('%%url%%',   '%%title%%'),
		                          array($page_full_url, $url_title),
		                          $bookmark->link);
		      $url_bookmarks[$id][$i]->full_link = $url_link;
		    }
		}
 	  }
    
    }
 
    return $url_bookmarks;
  }
  
  function getMp3Bookmarks()
  {
    $mp3_bookmarks = array();
    $post_id  =& JRequest::getVar('id', 0, 'GET', 'integer');
    $mp3_title = '';
    
    if (is_null($this->_media))
    {
      return false;
    }

    foreach ($this->_media as $k=>$media) {
    
 	  foreach ($this->_media[$k] as $m) {
	    $page_full_url = urlencode((isset($_SERVER['HTTPS']) ? 'https' : 'http') . '://' . $_SERVER['HTTP_HOST'] . $_SERVER['REQUEST_URI']);
	    if ($m->folder == 'mp3')
	    	$mp3_title     = urlencode($m->title);
		
		$id = $m->post_id;
		if ($mp3_title != '') {
		    $query = ' SELECT b.*'
		           . ' FROM #__wallfactory_bookmarks b'
		           . ' WHERE b.published = 1';
		    $this->_db->setQuery($query);
		    $mp3_bookmarks[$id] = $this->_db->loadObjectList();
		
		    foreach ($mp3_bookmarks[$id] as $i => $bookmark)
		    {
		      $mp3_link = str_replace(array('%%url%%',   '%%title%%'),
		                          array($page_full_url, $mp3_title),
		                          $bookmark->link);
		      $mp3_bookmarks[$id][$i]->full_link = $mp3_link;
		    }
		}
 	  }
    
    }
 
    return $mp3_bookmarks;
  }
  
  function getFileBookmarks()
  {
    $file_bookmarks = array();
    $post_id  =& JRequest::getVar('id', 0, 'GET', 'integer');
    $file_title = '';
    if (is_null($this->_media))
    {
      return false;
    }

    foreach ($this->_media as $k=>$media) {
   
 	  foreach ($this->_media[$k] as $m) {
	    $page_full_url = urlencode((isset($_SERVER['HTTPS']) ? 'https' : 'http') . '://' . $_SERVER['HTTP_HOST'] . $_SERVER['REQUEST_URI']);
	    if ($m->folder == 'files')
	    	$file_title     = urlencode($m->title);
		
		$id = $m->post_id;

		if ($file_title != '') {
			$query = ' SELECT b.*'
		           . ' FROM #__wallfactory_bookmarks b'
		           . ' WHERE b.published = 1';
		    $this->_db->setQuery($query);
		    $file_bookmarks[$id] = $this->_db->loadObjectList();
		
		    foreach ($file_bookmarks[$id] as $i => $bookmark)
		    {
		      $file_link = str_replace(array('%%url%%',   '%%title%%'),
		                          array($page_full_url, $file_title),
		                          $bookmark->link);
		      $file_bookmarks[$id][$i]->full_link = $file_link;
		    }
		}
 	  }
    }

    return $file_bookmarks;
  }
  
  function getImageBookmarks()
  {
    $image_bookmarks = array();
    $post_id  =& JRequest::getVar('id', 0, 'GET', 'integer');
    $image_title = '';
 
    if (is_null($this->_media))
    {
      return false;
    }

    foreach ($this->_media as $k=>$media) {
   
 	  foreach ($this->_media[$k] as $m) {
	    $page_full_url = urlencode((isset($_SERVER['HTTPS']) ? 'https' : 'http') . '://' . $_SERVER['HTTP_HOST'] . $_SERVER['REQUEST_URI']);
	    if ($m->folder == 'images')
	    	$image_title     = urlencode($m->title);
		
		$id = $m->post_id;
		
		if ($image_title != '') {
		
		    $query = ' SELECT b.*'
		           . ' FROM #__wallfactory_bookmarks b'
		           . ' WHERE b.published = 1';
		    $this->_db->setQuery($query);
		    $image_bookmarks[$id] = $this->_db->loadObjectList();
		
		    foreach ($image_bookmarks[$id] as $i => $bookmark)
		    {
		      $image_link = str_replace(array('%%url%%',   '%%title%%'),
		                          array($page_full_url, $image_title),
		                          $bookmark->link);
		      $image_bookmarks[$id][$i]->full_link = $image_link;
		    }
		}
 	  }
    
    }
    
    return $image_bookmarks;
  }
  
  function getNotificationFollowers()
  {
    $user     =& JFactory::getUser();

    $query_wall = ' SELECT w.id '
           . ' FROM #__wallfactory_walls w '
    	   . ' WHERE w.published = 1'
           . ' AND w.user_id = '.$user->id;
           
    $this->_db->setQuery($query_wall);
    $wall_id = $this->_db->loadResult();
    
    
  	$query = ' SELECT f.user_id '
           . ' FROM #__wallfactory_followers f '
    	   . ' LEFT JOIN #__wallfactory_walls w ON w.id = f.wall_id'
           . ' WHERE w.published = 1'
           . ' AND w.id = '.$wall_id;
               
    $this->_db->setQuery($query);

    return implode(', ', $this->_db->loadResultArray());
  }
  
  // Helpers
 
  function sendNewPostNotification($post)
  {
  	$ids =  $this->getNotificationFollowers();
    	
    $query = ' SELECT u.email, u.username'
           . ' FROM #__users u'
           . ' WHERE u.id IN (' . $ids . ')';
    $this->_db->setQuery($query);
    $receivers = $this->_db->loadObjectList();

    $query = ' SELECT w.title'
           . ' FROM #__wallfactory_posts p'
           . ' LEFT JOIN #__wallfactory_walls w ON w.id = p.wall_id'
           . ' WHERE p.id = ' . $post->id;
    $this->_db->setQuery($query);
    $wall_title = $this->_db->loadResult();

    $subject = base64_decode($this->wallSettings->notification_new_post_subject);
    $message = base64_decode($this->wallSettings->notification_new_post_message);

    $search  = array('%%username%%', '%%walltitle%%', '%%postlink%%', '%%walllink%%');

    foreach ($receivers as $receiver)
    {
      $replace = $this->getEmailReplacements(
        $receiver->username,
        $wall_title,
        $post->id,
        $post->wall_id
      );

      $custom_subject = str_replace($search, $replace, $subject);
      $custom_message = str_replace($search, $replace, $message);

      if ($this->wallSettings->enable_notification_email_html)
      {
        $custom_message = nl2br($custom_message);
      }

      $mail =& JFactory::getMailer();
      $mail->addRecipient($receiver->email);
      $mail->setSubject(JText::_($custom_subject));
      $mail->setBody(JText::_($custom_message));
      $mail->IsHTML($this->wallSettings->enable_notification_email_html);

      $mail->send();
    }
  }

  function getEmailReplacements($username, $wall_title, $post_id, $wall_id)
  {
    $replace = array(
      $username,
      $wall_title
    );

    if ($this->wallSettings->enable_notification_email_html)
    {
      $replace[] = '<a href="' . JRoute::_(JURI::root() . 'index.php?option=com_wallfactory&view=post&id=' . $post_id. '&alias='. $post->alias) . '">' . $wall_title . '</a>';
      $replace[] = '<a href="' . JRoute::_(JURI::root() . 'index.php?option=com_wallfactory&view=wall&alias=' . $post->alias) . '">' . $wall_title . '</a>';
    }
    else
    {
      $replace[] = JRoute::_(JURI::root() . 'index.php?option=com_wallfactory&view=post&id=' . $post_id. '&alias='. $post->alias);
      $replace[] = JRoute::_(JURI::root() . 'index.php?option=com_wallfactory&view=wall&alias=' . $post->alias);
    }

    return $replace;
  }

  function sendFollowersNotifications($post)
  {
    $query = ' SELECT f.user_id, u.username, u.email, w.title AS wall_title'
           . ' FROM #__wallfactory_followers f'
           . ' LEFT JOIN #__users u ON u.id = f.user_id'
           . ' LEFT JOIN #__wallfactory_walls w ON w.id = f.wall_id'
           . ' WHERE f.wall_id = ' . $post->wall_id
           . ' AND f.notification = 1';
    $this->_db->setQuery($query);
    $followers = $this->_db->loadObjectList();

    $subject = base64_decode($this->wallSettings->notification_follow_wall_subject);
    $message = base64_decode($this->wallSettings->notification_follow_wall_message);

    $search  = array('%%username%%', '%%walltitle%%', '%%postlink%%', '%%walllink%%');

    foreach ($followers as $follower)
    {
      $replace = $this->getEmailReplacements(
        $follower->username,
        $follower->wall_title,
        $post->id,
        $post->wall_id
      );

      $custom_subject = str_replace($search, $replace, $subject);
      $custom_message = str_replace($search, $replace, $message);

      if ($this->wallSettings->enable_notification_email_html)
      {
        $custom_message = nl2br($custom_message);
      }

      $mail =& JFactory::getMailer();
      $mail->addRecipient($follower->email);
      $mail->setSubject(JText::_($custom_subject));
      $mail->setBody(JText::_($custom_message));
      $mail->IsHTML($this->wallSettings->enable_notification_email_html);

      $mail->send();
    }
  }
  
}