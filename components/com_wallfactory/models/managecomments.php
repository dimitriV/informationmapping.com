<?php
/*------------------------------------------------------------------------
com_wallfactory - Wall Factory
------------------------------------------------------------------------
author    TheFactory
copyright Copyright (C) 2010 SKEPSIS Consult SRL. All Rights Reserved.
@license - http://www.gnu.org/licenses/gpl-2.0.html GNU/GPL
Websites: http://www.thefactory.ro
Technical Support:  Forum - http://www.thefactory.ro/joomla-forum/
-------------------------------------------------------------------------*/

defined('_JEXEC') or die('Restricted access');

jimport('joomla.application.component.model');

class FrontendModelManageComments extends JModel
{
  var $_data;
  var $_total;
  var $_pagination;
  var $_limit;
  var $_limitstart;

  function __construct()
  {
    parent::__construct();

    global $mainframe, $option;
    $this->_limit      = JRequest::getVar('limit',      10, 'REQUEST', 'integer');
    $this->_limitstart = JRequest::getVar('limitstart', 0,  'REQUEST', 'integer');

    $this->_limitstart = ($this->_limit != 0 ? (floor($this->_limitstart / $this->_limit) * $this->_limit) : 0);
  }

  function getData()
  {
    if (empty($this->_data))
    {
      $query = $this->_buildQuery();
      $this->_data = $this->_getList($query, $this->_limitstart, $this->_limit);
    }

    #var_dump($this->_db->_sql);

    return $this->_data;
  }

  function _buildQuery()
  {
    $filter =  $this->_getFilterCondition();
    $user   =& JFactory::getUser();

    $query = ' SELECT c.*, u.username, p.alias AS alias'
           . ' FROM #__wallfactory_comments c'
           . ' LEFT JOIN #__wallfactory_posts p ON p.id = c.post_id'
           . ' LEFT JOIN #__wallfactory_walls w ON w.id = p.wall_id'
           . ' LEFT JOIN #__users u ON u.id = c.user_id'
           . ' WHERE w.user_id = ' . $user->id
           . $filter
           . ' ORDER BY c.date_created DESC';
    return $query;
  }

  function _getFilterCondition()
  {
    $session =& JFactory::getSession();
    $where = '';

    // Published state filter
    switch ($session->get('filter.comments', ''))
    {
      case 'approved':
        $where .= ' AND c.approved = 1';
      break;

      case 'unapproved':
        $where .= ' AND c.approved = 0';
      break;

      case 'reported':
        $where .= ' AND c.reported = 1';
      break;
    }

    // Comment type filter
    switch ($session->get('filter.comments.type', -1))
    {
      case 0:
        $where .= ' AND c.type = 0';
      break;

      case 1:
        $where .= ' AND c.type = 1';
      break;
    }

    // Post filter
    if ($session->get('filter.post_id', 0))
    {
      $where .= ' AND c.post_id = ' . $session->get('filter.post_id', 0);
    }

    return $where;
  }

  function getTotal()
  {
    if (empty($this->_total))
    {
      $query = $this->_buildQuery();
      $this->_total = $this->_getListCount($query);
    }

    return $this->_total;
  }

  function getPagination()
  {
    if (empty($this->_pagination))
    {
      jimport('joomla.html.pagination');
      $this->_pagination = new JPagination($this->getTotal(), $this->_limitstart, $this->_limit);
    }

    return $this->_pagination;
  }
}