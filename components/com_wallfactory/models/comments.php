<?php
/*------------------------------------------------------------------------
com_wallfactory - Wall Factory
------------------------------------------------------------------------
author    TheFactory
copyright Copyright (C) 2010 SKEPSIS Consult SRL. All Rights Reserved.
@license - http://www.gnu.org/licenses/gpl-2.0.html GNU/GPL
Websites: http://www.thefactory.ro
Technical Support:  Forum - http://www.thefactory.ro/joomla-forum/
-------------------------------------------------------------------------*/

defined('_JEXEC') or die('Restricted access');

jimport('joomla.application.component.model');

class FrontendModelComments extends JModel
{
  var $_data;
  var $_total;
  var $_pagination;
  var $_limit;
  var $_limitstart;
  var $_comments; 
  var $_post;
  
  function __construct()
  {
    parent::__construct();

    $this->wallSettings = new wallSettings();

    global $mainframe, $option;
    //$this->_limit      = JRequest::getVar('limit', $this->wallSettings->comments_per_page, 'REQUEST', 'integer');
    $this->_limit      = JRequest::getVar('limit', 		10, 'REQUEST', 'integer');
    $this->_limitstart = JRequest::getVar('limitstart', 0, 'REQUEST', 'integer');

    $this->_limitstart = ($this->_limit != 0 ? (floor($this->_limitstart / $this->_limit) * $this->_limit) : 0);
  }

  function getData()
  {
    if (empty($this->_data))
    {
      $query = $this->_buildQuery();
      $this->_data = $this->_getList($query, $this->_limitstart, $this->_limit);
    }
    return $this->_data;
  }
  
  function _buildQuery()
  {
    $post_id 	=& JRequest::getVar('post_id', 0, 'GET', 'integer');
    //$post_id    =& JRequest::getVar('id', 0, 'REQUEST', 'integer');
    $layout 	=& JRequest::getVar('layout', '', 'GET', 'string');
    $condition = ($layout == 'latest') ? ' LIMIT 0,5' : '';
 
    //$content = $database->getEscaped(JRequest::getVar('comment', '', 'REQUEST', 'string'));
    $user    =& JFactory::getUser();

    $query = ' SELECT c.*, ' 
    	   . ' IF(u.username = "", u.username, "' . JText::_('Guest') . ' ") AS username, ' 
    	   . ' s.avatar_type, s.avatar_extension, IF (s.name = "", u.username, s.name) AS name'
           . ' FROM #__wallfactory_comments c'
           . ' LEFT JOIN #__users u ON u.id = c.user_id'
           . ' LEFT JOIN #__wallfactory_members s ON s.user_id = c.user_id'
           . ' WHERE c.post_id = ' . $post_id
           . ' ORDER BY c.date_created DESC '
           . $condition;
           
   return $query;
  }
  

  function getTotal()
  {
    if (empty($this->_total))
    {
      $query = $this->_buildQuery();
      $this->_total = $this->_getListCount($query);
    }

    return $this->_total;
  }

  function getPagination()
  {
    if (empty($this->_pagination))
    {
      jimport('joomla.html.pagination');
      $this->_pagination = new JPagination($this->getTotal(), $this->_limitstart, $this->_limit);
    }
 
    return $this->_pagination;
  }
  
  function &getPost()
  {
	global $Itemid;
  	$post_id 	=& JRequest::getVar('post_id', 0, 'GET', 'integer');
    $alias 	=& JRequest::getVar('alias', '', 'GET', 'string');
  	    
  	if (empty($this->_post))
    {
      $user =& JFactory::getUser();

      $query = ' SELECT p.id, p.wall_id, p.user_id, p.content, p.notification, p.date_created, p.date_updated, p.enable_comments, p.reported, '
	    	 . ' IF(p.alias <> "", p.alias,"' . JText::_('Guest') . '") AS alias, '
	    	 . ' IF(u.username IS NULL,"' . JText::_('Guest') . '", u.username) AS username, '
	    	 . ' u.id as wall_owner_id, w.alias AS wall_alias,'
	    	 . ' DATE_FORMAT(p.date_created, "%Y-%m-%d") AS date_spacer, '
	    	 . ' s.avatar_extension, s.avatar_type '
             . ' FROM #__wallfactory_posts p'
             . ' LEFT JOIN #__wallfactory_walls w ON w.id = p.wall_id'
             . ' LEFT JOIN #__users u ON u.id = w.user_id'
             . ' LEFT JOIN #__wallfactory_members s ON s.user_id = p.user_id '
			 . ' WHERE p.id = ' . $post_id;
      
      $this->_db->setQuery($query);

      $this->_post = $this->_db->loadObject();
    }

    return $this->_post;
  }
  

}