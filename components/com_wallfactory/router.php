<?php
/*------------------------------------------------------------------------
com_wallfactory - Wall Factory
------------------------------------------------------------------------
author    TheFactory
copyright Copyright (C) 2010 SKEPSIS Consult SRL. All Rights Reserved.
@license - http://www.gnu.org/licenses/gpl-2.0.html GNU/GPL
Websites: http://www.thefactory.ro
Technical Support:  Forum - http://www.thefactory.ro/joomla-forum/
-------------------------------------------------------------------------*/

require_once(JPATH_SITE.DS.'components'.DS.'com_wallfactory'.DS.'WallRoute.php');

function WallFactoryBuildRoute(&$query)
{
  $segments = array();

  // If no view or controller is set, then we don't need to parse the routes
  if (!isset($query['view']) && !isset($query['controller']))
  {
    return $segments;
  }

  // Get the routes
  $routes = WallRoute::getRoutes();

  // Parse each route
  foreach ($routes as $alias => $route)
  {
    // Match route
    if (!isset($query[$route['type']]) || $query[$route['type']] != $route['value'])
    {
      continue;
    }

    $valid = true;
    $temp  = array();

    // Parse params
    if (isset($route['params']))
    {
      foreach ($route['params'] as $type => $param)
      {
        if (!isset($query[$param]) && 'optional' !== $type)
        {
          $valid = false;
          break;
        }
      }
    }

    if (!$valid)
    {
      continue;
    }

    unset($query[$route['type']]);

    // Set the segments
    $segments[] = $alias;

    if (isset($route['params']))
    {
      foreach ($route['params'] as $param)
      {
        if (isset($query[$param]))
        {
          $segments[] = $query[$param];
          unset($query[$param]);
        }
      }
    }

    break;
  }

  return $segments;
}

function WallFactoryParseRoute($segments)
{
  // Parse the segments
  foreach ($segments as $i => $segment)
  {
    $segments[$i] = str_replace(':', '-', $segment);
  }

  // Get the routes
  $routes = WallRoute::getRoutes();

  if (array_key_exists($segments[0], $routes))
  {
    $route = $routes[$segments[0]];

    $vars[$route['type']] = $route['value'];

    // Set the task
    if ('controller' == $route['type'])
    {
      $vars['task'] = $route['task'];
    }

    // Set other vars
    if (isset($route['vars']))
    {
      foreach ($route['vars'] as $type => $var)
      {
        $vars[$type] = $var;
      }
    }

    if (isset($route['params']))
    {
      $i = 1;
      foreach ($route['params'] as $type => $param)
      {
        if (isset($segments[$i]))
        {
          $vars[$param] = $segments[$i];
        }

        $i++;
      }
    }
  }

  return $vars;
}
