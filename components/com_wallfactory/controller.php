<?php
/*------------------------------------------------------------------------
com_wallfactory - Wall Factory
------------------------------------------------------------------------
author    TheFactory
copyright Copyright (C) 2010 SKEPSIS Consult SRL. All Rights Reserved.
@license - http://www.gnu.org/licenses/gpl-2.0.html GNU/GPL
Websites: http://www.thefactory.ro
Technical Support:  Forum - http://www.thefactory.ro/joomla-forum/
-------------------------------------------------------------------------*/

defined('_JEXEC') or die('Restricted access');

jimport('joomla.application.component.controller');

class FrontendController extends JController
{
  function __construct()
  {
    parent::__construct();

    require_once(JPATH_COMPONENT_ADMINISTRATOR.DS.'wallSettings.class.php');
    require_once(JPATH_COMPONENT_ADMINISTRATOR.DS.'lib'.DS.'factory'.DS.'wallHelper.class.php');
    require_once(JPATH_COMPONENT_ADMINISTRATOR.DS.'lib'.DS.'factory'.DS.'html.php');
    require_once(JPATH_COMPONENT_SITE.DS.'WallRoute.php');

    $this->wallSettings = new wallSettings();
    $this->wallHelper   = wallHelper::getInstance();

    $this->trackIp();
    $this->checkPermissions();
  }

  function display()
  {
    parent::display();
  }
  
  function trackIp()
  {
    $user =& JFactory::getUser();

    if (!$user->guest)
    {
      JTable::addIncludePath(JPATH_COMPONENT_ADMINISTRATOR.DS.'tables');
      $ip =& JTable::getInstance('ip', 'Table');
      $ip->update();
    }
  }
  
  function checkPermissions()
  {
    global $mainframe, $Itemid;
	JTable::addIncludePath(JPATH_COMPONENT_ADMINISTRATOR.DS.'tables');
	
    $view       =& JRequest::getVar('view',       '', 'REQUEST', 'string');
    $controller =& JRequest::getVar('controller', '', 'REQUEST', 'string');
    $type = '' == $controller ? $view : $controller;

    // Banned users are not allowed
    $user    =& JFactory::getUser();
    $wallmembers =& JTable::getInstance('members', 'Table');
	$wallmembers->findOneByUserId($user->id);
	
    if ($wallmembers->banned && 'banned' != $type)
    {
      $this->redirectTo(
        'index.php?option=com_wallfactory&view=banned&Itemid=' . $Itemid,
        'PERMISSIONS_BANNED'
      );
    }

    // Permissions
    $settings = new wallSettings();

    $permissions = array(
      'banned'      => array(),
      'comment'     => array('loggedin', 'registered', 'enable_comments'),
      'wall'       	=> array('loggedin', 'registered'),
    );

    if (!isset($permissions[$type]))
    {
      return false;
    }
    
    $permissions = $permissions[$type];

    $id       =& JRequest::getVar('id', 0, 'REQUEST', 'integer');

    $my_wall = ($id == 0 || $user->id == $id);

      $permissions = array(
        (!$my_wall)                         ? '' : 'loggedin',
        (!$my_wall)                         ? '' : 'registered',
      );
     
    foreach ($permissions as $permission)
    {
      switch ($permission)
      {
        case 'loggedin':
          if ($user->guest)
            if (!$settings->enable_guest_view) 
	          {
	            $uri     =& JFactory::getURI();
	            $referer =  base64_encode($uri->_uri);
	
	            $this->redirectTo(
	              'index.php?option=com_user&view=login&Itemid=' . $Itemid . '&return=' . $referer,
	              'PERMISSIONS_LOGIN'
	            );
	          }
      
          break;
        case 'registered':
         if ($user->guest)
           if (!$settings->enable_guest_view) 
          {
            $this->redirectTo(
            	'index.php?option=com_wallfactory&view=newwall&Itemid=' . $Itemid. '&return=' . $referer, 
                'Please register first !'
            );
          }
           
          break;
      }
    }
  }

  function redirectTo($link, $warning = null, $message = null)
  {
    if ($this->isAjaxRequest())
    {
      die('{"status":"0","message":"' . JText::_('PERMISSIONS_NOT_AVAILABLE') . '","error":"' . JText::_($warning) . '"}');
    }

    if (null != $warning)
    {
      JError::raiseWarning('ERROR', JText::_($warning));
    }

    global $mainframe;
    $link = JRoute::_($link, false);

    if (null != $message)
    {
      $mainframe->redirect($link, JText::_($message));
    }

    $mainframe->redirect($link);
  }

  function isAjaxRequest()
  {
    return (isset($_SERVER['HTTP_X_REQUESTED_WITH']) && ($_SERVER['HTTP_X_REQUESTED_WITH'] == 'XMLHttpRequest'));
  }
  
  
}