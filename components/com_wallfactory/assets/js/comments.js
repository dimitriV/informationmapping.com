jQueryFactory(document).ready(function ($) {

	// Load comments

	$('div[id^="wallfactory-comments"]').each(function () {
		var id = $(this).attr('class');
	 	$('#wallfactory-comments' + id + ' .update').css('visibility', 'visible').css('visibility', 'visible');
 	});
	 	
 	$('div[id^="wallfactory-comments"]').each(function () {
 		var id = $(this).attr('class');
 		 		
 		$('#wallfactory-comments' + id + ' .loading').hide();
 	});

	 $(".wallfactory-comment_box").show();

	 /*&format=raw*/
  $('#wallfactory-comments .wallfactory-report').click( function ()
  {
    var test = $(this).parent();
    $.post(root + "index.php?option=com_wallfactory&controller=comment&task=report&format=raw", {
      id:   $(this).attr('rel')},
      function(data)
      {
        test.html(data);
      }
    );

    $(this).parent().html($('#wallfactory-loading').html());

    return false;
  });

  // Delete
  $('#wallfactory-comments .wallfactory-delete').click( function () {
    if (confirm('Are you sure you want to delete this comment?'))
    {
      $(this).parent().load($(this).attr('href'));
      $(this).parent().html($('#wallfactory-loading').html());
    }
    return false;
  });


    
  // Submit comment
  $('.submit-comment').live( 'click', function(event)  {
	event.preventDefault();
	
    var error = false;
	
    var id = $(this).attr("rel");
        
    if ($('#comment' + id).val() == '')
    {
      $('#comment' + id).addClass('error');
      error = true;
    }
    else
    {
      $('#comment' + id).removeClass('error');
    }

    if (error)
    {
      return false;
    }

    // submit loader
    $(".wallfactory-comment-loader" + id ).show();    
	$(this).parents('.round_post:first').find('.comments_list:first').hide();
	
	 if (is_guest) {
	
    	$.post(root + 'index.php?option=com_wallfactory&format=raw&controller=comment&task=addGuestComment', {
	 	   comment: $('#comment' + id).val(),
	  	   post_id: id },
    		function (response) {
    			    			
    			var user_alias = response.alias;
   			
    			window.location.href = root + 'index.php?option=com_wallfactory&view=comments&layout=_guest_preview&post_id=' + id + '&alias=' + user_alias;
    		});
	 }
	 else {
	 
		$.post(root + 'index.php?option=com_wallfactory&format=raw&controller=comment&task=addComment', {
      	comment: $('#comment' + id).val(),
	  	post_id: id },
    
	  	function(response) {
	     $('#wallfactory-comments' + id + ' .update').load(root + 'index.php?option=com_wallfactory&format=raw&view=comments&layout=_latest&post_id=' + $('#wallfactory-comments' + id).attr('class'));
    	});
     
       // hide wall comments
	   //	$(".comments_list").hide();
		$("#post_comments" + id + " .round_comments").hide();	
	   	$("#wallfactory-comment-loader" + id).hide();
	   	$("#comment-form" + id).hide();
	}
    
	return false;

  });
  
  
  $('#name').focus( function () {
    $('#name').removeClass('error')
  });

  
});