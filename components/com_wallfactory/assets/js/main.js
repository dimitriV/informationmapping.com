jQueryFactory(document).ready( function ($) {
 
  $('.show-comment-form').live( 'click', function(event)  {
	event.preventDefault();
	var id = $(this).attr("rel");
	$('#comment-form' + id).toggle("normal");
  }); 

  $('#show-attachments-form').live( 'click', function(event)  {
  	event.preventDefault();
	  $('#attachments').toggle("normal");
  });
    
  $('.cancel-comment').live( 'click', function(event)  {
	 event.preventDefault();
	 var id = $(this).attr("rel");
	 $('#comment-form' + id).toggle("normal");
  });
  
  $('.show-share-video').live( 'click', function(event)  {
  	event.preventDefault();
	var id = $(this).attr("rel");
	$('#share-video' + id).toggle("normal");
  });
	  
  $('.show-share-url').live( 'click', function(event)  {
	var id = $(this).attr("rel");
  	event.preventDefault();
	$('#share-url' + id).toggle("normal");
  });
  
  $('.show-share-mp3').live( 'click', function(event)  {
	var id = $(this).attr("rel");
  	event.preventDefault();
	$('#share-mp3' + id).toggle("normal");
  });
  
  $('.show-share-image').live( 'click', function(event)  {
	var id = $(this).attr("rel");
  	event.preventDefault();
	$('#share-image' + id).toggle("normal");
  });
  
  $('.show-share-file').live( 'click', function(event)  {
	var id = $(this).attr("rel");
  	event.preventDefault();
	$('#share-file' + id).toggle("normal");
  });
  
  $('.show-share-post').live( 'click', function(event)  {
	var id = $(this).attr("rel");
  	event.preventDefault();
	$('#share-post' + id).toggle("normal");
  });

});

