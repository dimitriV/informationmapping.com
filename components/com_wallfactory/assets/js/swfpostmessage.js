jQueryFactory(document).ready(function ($) {
  	if($().progressbar)
    {
      // Progress bar
      $("#image-progressbar").progressbar({
        value: 0
      });
    }
    else
    {
      $("#image_container").html("<h1>jQuery UI library is missing or not loaded! This page cannot be displayed properly!</h1>");
    }
    
    if($().progressbar)
    {
      // Progress bar
      $("#mp3-progressbar").progressbar({
        value: 0
      });
    }
    else
    {
      $("#mp3_container").html("<h1>jQuery UI library is missing or not loaded! This page cannot be displayed properly!</h1>");
    }

    if($().progressbar)
    {
      // Progress bar
      $("#file-progressbar").progressbar({
        value: 0
      });
    }
    else
    {
      $("#file_container").html("<h1>jQuery UI library is missing or not loaded! This page cannot be displayed properly!</h1>");
    }
  	// Ajax Flash Upload
	
		// image upload  
	$('#swfupload-image-control').swfupload({
		upload_url: root + 'index.php?option=com_wallfactory&controller=upload&task=uploadImage',
		file_size_limit : "1024000",
		file_types : "*.gif;*.jpg;*.jpeg;*.png",
		file_types_description : "Image Files",
		file_upload_limit : "0",
		file_queue_limit: "1",
		file_post_name : "image",
		flash_url : root + 'components/com_wallfactory/assets/swfs/swfupload.swf',
		button_placeholder_id: 'image-button',
		button_image_url: root + 'components/com_wallfactory/assets/images/button-upload.png',
        button_width: 61,
        button_height: 22,
        debug: false,
		custom_settings : {something : "here", cancelButtonId : "btnCancelImage"},
		post_params: { "PHPSESSID" : PHPsessionID, sessionName : sessionId,
	  		'title': $("#image_name").val() , 'postID' : $('#hidFileID-image').val()
		}
		  	
		})
		.bind('fileQueued', function(event, file){
		  // start the upload since it's queued
		  $('#btnCancelImage').attr('disabled', '');
		  $(this).swfupload('startUpload');
		})
		.bind('uploadProgress', function(event, file, bytesLoaded){
		  $('#image-progressbar').progressbar('option', 'value', Math.floor(bytesLoaded * 100 / file.size));
		})
		.bind('uploadSuccess',function(file, serverData, receivedResponse) {
			
			
			try {
				var response = eval("(" + receivedResponse + ")");
	    		if (response === " ") {
	    		} else {
	    			$('#hidFileID-image').val(response.lastid);
	    		}
			}
        	catch (ex) {
				//
			}
    	})
		.bind('uploadComplete', function(event, file){
		  $("#image-progressbar").hide();
			 $("#current_image").fadeOut();
		  	 $(this).fadeIn(function () {
		        // upload completed, show the new image
		        $("#image_container").css("height", 30);
		       	$("#image_container").html(file.name + ' uploaded succesfully.');
			 });
		
		})
		.bind('uploadStart', function(event, file){
			$("#image-progressbar").show();
		});

  		$('#btnCancelImage').click(function() {
		    $('#swfupload-image-control').swfupload('cancelUpload');
            alert('Canceled');
             $('#btnCancelImage').attr('disabled', 'disabled');
         });
        
		// mp3 upload
		$('#swfupload-mp3-control').swfupload({
			upload_url: root + 'index.php?option=com_wallfactory&controller=upload&task=uploadMp3',
			file_size_limit : "102400000",
			  file_types :  "*.mp3",
			  file_types_description : "Mp3 Files",
			  file_upload_limit : "0",
			  file_queue_limit: "1",
			  file_post_name : "mp3",
			  flash_url : root + 'components/com_wallfactory/assets/swfs/swfupload.swf',
			  button_placeholder_id: 'mp3-button',
			  button_image_url: root + 'components/com_wallfactory/assets/images/button-upload.png',
	          button_width: 61,
	          button_height: 22,
			  debug: false,
			  post_params: { "PHPSESSID" : PHPsessionID, sessionName : sessionId,
		  			'title': $("#mp3_name").val() , 'postID' : $('#hidFileID-mp3').val()
				    },
			  custom_settings : {something : "here", cancelButtonId : "btnCancelMp3"}
		})
		
		.bind('fileQueued', function(event, file){
		  // start the upload since it's queued
		  $('#btnCancelMp3').attr('disabled', '');
		  $(this).swfupload('startUpload');
		})
		.bind('uploadProgress', function(event, file, bytesLoaded){
		  $('#mp3-progressbar').progressbar('option', 'value', Math.floor(bytesLoaded * 100 / file.size));
		})
		.bind('uploadSuccess',function(file, serverData, receivedResponse) {
			try {
				var response = eval("(" + receivedResponse + ")");
			
	    		if (response === " ") {
				//
	    		} else {
	    			$('#hidFileID-mp3').val(response.lastid);
	    		}
			}
        	catch (ex) {
        		//
			}
    	})
		.bind('uploadComplete', function(event, file){
		  $("#mp3-progressbar").hide();
		  $("#current_mp3").fadeOut();
		  	 $(this).fadeIn(function () {
		        // upload completed, show the new image
		        $("#mp3_container").css("height", 30);
		       	$("#mp3_container").html(file.name + ' uploaded succesfully.');
			 });
		})
		.bind('uploadStart', function(event, file){
			$("#mp3-progressbar").show();
		});

		$('#btnCancelMp3').click(function() {
	        $('#swfupload-mp3-control').swfupload('cancelUpload');
            $('#btnCancelMp3').attr('disabled', 'disabled');
	    });
		
		// files upload
		$('#swfupload-file-control').swfupload({
		  upload_url: root + 'index.php?option=com_wallfactory&controller=upload&task=uploadFile',
		  file_size_limit : "102400000",
		  file_types : "*.doc;*.txt;*.pdf",
		  file_types_description : "Files",
		  file_upload_limit : "0",
		  file_queue_limit: "1",
		  file_post_name : "file",
		  flash_url : root + 'components/com_wallfactory/assets/swfs/swfupload.swf',
		  button_placeholder_id: 'file-button',
		  button_image_url: root + 'components/com_wallfactory/assets/images/button-upload.png',
          button_width: 61,
          button_height: 22,
		  debug: false,
		  post_params: { "PHPSESSID" : PHPsessionID, sessionName : sessionId,
	  			'title': $("#file_name").val() , 'postID' : $('#hidFileID-file').val()
			    },
		  custom_settings : {something : "here", cancelButtonId : "btnCancelFile"}
		})
		.bind('fileQueued', function(event, file){
		  // start the upload since it's queued
		  $('#btnCancelFile').attr('disabled', '');
		  $(this).swfupload('startUpload');
		})
		.bind('uploadProgress', function(event, file, bytesLoaded){
		  $('#file-progressbar').progressbar('option', 'value', Math.floor(bytesLoaded * 100 / file.size));
		})
		.bind('uploadSuccess',function(file, serverData, receivedResponse) {
			try {
				var response = eval("(" + receivedResponse + ")");
				
	    		if (response === " ") {
				//
	    		} else {
	    			$('#hidFileID-file').val(response.lastid);
	    		}
			}
        	catch (ex) {
				//
			}
    	})
		.bind('uploadComplete', function(event, file){
		  $("#file-progressbar").hide();
		  $("#current_file").fadeOut();
		  	 $(this).fadeIn(function () {
		        // upload completed, show the new image
		        $("#file_container").css("height", 30);
		       	$("#file_container").html(file.name + ' uploaded succesfully.');
			 });
	
		})
		.bind('uploadStart', function(event, file){
			$("#file-progressbar").show();
		});
		
		$('#btnCancelFile').click(function() {
	        $('#swfupload-file-control').swfupload('cancelUpload');
            $('#btnCancelFile').attr('disabled', 'disabled');
	    });
		
  });
