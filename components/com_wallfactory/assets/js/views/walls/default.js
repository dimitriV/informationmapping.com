jQueryFactory(document).ready(function ($) {
  var last_pagination_link = document.location.href;

  // Ajax Loading Bar
  $("#loading-bar").ajaxStart(function(){
    $(this).show();
    $("#action-message").fadeOut('fast');
  });
  $("#loading-bar").ajaxStop(function(){
    $(this).hide();
  });

  // Submit filter
  $("#submit-filter").click(function () {
    var filter = $("#filter").val();

    $.post(route_walls_filter, {
      filter: filter,
      format: "raw"
    }, function (response) {
      $.post(route_walls, { format: "raw" }, function (response) {
        $("#walls").html(response);
        paginationLinks();
        $('html').animate({scrollTop:0}, 'fast');
      });
    });

    return false;
  });

  // Pagination links
  function paginationLinks()
  {
    $(".pagination a").click(function () {
      var href = $(this).attr("href");

      $.post(href, { format: "raw" }, function (response) {
        $("#walls").html(response);
        last_pagination_link = href;
        paginationLinks();
        $('html').animate({ scrollTop: 0 }, 'fast');
      });

      return false;
    });
  }

  // Init
  paginationLinks();
});