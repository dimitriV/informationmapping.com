jQueryFactory(document).ready(function ($) {
  // Action bar show
  $(".portlet-content").hover(function () {
    $(this).find(".action-bar").fadeIn("fast");
  }, function () {
    $(this).find(".action-bar").fadeOut("fast");
  });

  // Follow/Unfollow wall
  $(".follow").click(function () {
    var link    = $(this);
    var id      = $(this).parent().parent().parent().parent().parent().attr("id");
    var content = $(this).parent().parent().parent().parent();
    id = id.split("_");
    id = id[1];

    $.post(route_follow, {
      id:     id,
      format: "raw"
    }, function (response) {
      switch (response.status)
      {
        case 1:
          var new_text = content.hasClass("followed") ? txt_follow : txt_stop_following;
          content.toggleClass("followed");
          link.html(new_text);

          $("#action-message").removeClass("action-error").fadeIn("fast").html(response.message);
        break;

        case 0:
          $('html').animate({scrollTop:0}, 'fast');
          $("#action-message").addClass("action-error").fadeIn("fast").html(response.message);
        break;
      }
    }, "json");

    return false;
  });
});