jQueryFactory(document).ready(function ($) {
  // Show action bar
  $(".wall").hover(function () {
    var id = $(this).attr("rel");
    $(".row_" + id).find(".action-bar").css("visibility", "visible");
  }, function () {
    var id = $(this).attr("rel");
    $(".row_" + id).find(".action-bar").css("visibility", "hidden");
  });

  // Follow/Unfollow wall
  $(".follow").click(function () {
    var link    = $(this);
    var id      = $(this).parent().parent().parent().attr("rel");
    var content = $(this).parent().parent().parent();

    $.post(route_follow, {
      id:     id,
      format: "raw"
    }, function (response) {
      switch (response.status)
      {
        case 1:
          var new_text = content.hasClass("followed") ? txt_follow : txt_stop_following;
          $("tr[rel=" + id + "]").toggleClass("followed");
          link.html(new_text);

          $("#action-message").removeClass("action-error").fadeIn("fast").html(response.message);
        break;

        case 0:
          $('html').animate({scrollTop:0}, 'fast');
          $("#action-message").addClass("action-error").fadeIn("fast").html(response.message);
        break;
      }
    }, "json");

    return false;
  });
});