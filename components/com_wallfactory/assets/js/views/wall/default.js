jQueryFactory(document).ready(function ($) {
  // Ajax Loading Bar
  $("#loading-bar").ajaxStart(function(){
    $(this).show();
  });
  $("#loading-bar").ajaxStop(function(){
    $(this).hide();
  });

  // Follow / Unsubscribe
  $("#follow-wall").click(function () {
    var parent    = $(this).parent();
    var status    = $(this).attr("rel");
    var followers = parseInt($("#followers").html());

    $.post(route_follow_wall, {
      id:     wall_id,
      format: "raw"
    }, function (response) {
      if (response.status == 1)
      {
        if (status == 1)
        {
          $("#follow-info").html(txt_you_are_following_this_wall);
          $("#follow-wall").attr("rel", 0).html(txt_unsubscribe);
          followers++;
        }
        else
        {
          $("#follow-info").html("");
          $("#follow-wall").attr("rel", 1).html(txt_follow_this_wall);
          followers--;
        }

        $("#followers").html(followers);
      }
    }, "json");

    return false;
  });
 
    // Write post
  $("#wall_form").submit(function () {
    var valid = true;
	var post = 1;
	$("textarea#post_message").removeClass("highlight");
	$("#image_name").removeClass("highlight");
	$("#mp3_name").removeClass("highlight");
	$("#file_name").removeClass("highlight");
	
	var mp3uploaded = $('input#hidFileID-mp3').val();
	var fileuploaded = $('input#hidFileID-file').val();
	var imageuploaded = $('input#hidFileID-image').val();

    // main post
    if ($("textarea#post_message").val() == "")
    {
	  valid = false;
	  $("textarea#post_message").addClass("highlight");
	  post = 0;
    }
    
	if (post == 1 )
    {
     // check media uploads
		if (imageuploaded != "")
		{
			// Check image title
			if ($("#image_name").val() == "")
			{
				valid = false;
				$("#image_name").addClass("highlight");
			}				
		}
		
		if (mp3uploaded != "")
		{
			// Check mp3 title
			if ($("#mp3_name").length)
			{
			  if ($("#mp3_name").val() == "")
			  {
				valid = false;
				$("#mp3_name").addClass("highlight");
			  }
			}
		}
		
		if (fileuploaded != "")
		{
			// Check file title
			if ($("#file_name").length)
			{
			  if ($("#file_name").val() == "")
			  {
				valid = false;
				$("#file_name").addClass("highlight");
			  }
			}
		}
	}

    return valid;
  });

 

  // Pagination links
  $("#pagination a").live("click", function () {
    var href = $(this).attr("href");
       
    if (-1 == href.indexOf("limitstart"))
    {
      var split = href.split("start=");
      
      split = split[1] ? split[1] : 0;
      href = document.location.href + "?start=" + split;
    }

    $.post(href, { format: "raw" }, function (response) {
      $(".posts").html(response);
      
      $('div[id^="wallfactory-comments"]').each(function () {
       
 		var id = $(this).attr('class');
 		$('#wallfactory-comments' + id + ' .loading').hide();
 	});    
       
 
  });
    
    return false;
  });
});