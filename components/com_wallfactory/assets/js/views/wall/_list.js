jQueryFactory(document).ready(function ($) {
   
 // Pagination links
  $("#pagination a").click(function () {
    var href = $(this).attr("href");
    
    $.post(href, { format: "raw" }, function (response) {
      $(".posts").html(response);
   });

    return false;
  });
});