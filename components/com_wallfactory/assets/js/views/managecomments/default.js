jQueryFactory(document).ready(function ($) {
  var last_pagination_link = document.location.href;
  // Register Events on page load
  registerEvents();

  // Ajax Loading Bar
  $("#loading-bar").ajaxStart(function(){
    $(this).show();
    $("#action-message").fadeOut("fast");
  });
  $("#loading-bar").ajaxStop(function(){
    $(this).hide();
  });

  // Submit filter
  $("#submit-filter").click(function () {
    var filter = $("#filter").val();
    var type   = $("#type").val();

    $.post(route_filter, {
      filter: filter,
      type:   type,
      format: "raw"
    }, function (response) {
      $.post(route_load_comments, { format: "raw" }, function (response) {
        $("#comments").html(response);
        registerEvents();
        $('html').animate({scrollTop:0}, 'fast');
      });
    });

    return false;
  });

  // Submit form
  $("#form").submit(function  () {
    var params = {};
    var checked = false;

    $(this).find("input.checkbox").each(function () {
      if ($(this).attr("checked"))
      {
        params[$(this).attr("name")] = $(this).attr("checked");
        checked = true;
      }
    });

    // Check if at lease one item is selected
    if (!checked)
    {
      $("#action-message").fadeOut("fast", function () {
        $(this).addClass("action-error").show().html(txt_select_one_item)
      });
      $('html').animate({scrollTop:0}, 'fast');

      return false;
    }

    switch ($("#task").val())
    {
      case "":
        $("#action-message").fadeOut("fast", function () {
          $(this).addClass("action-error").show().html(txt_select_action)
        });
        $('html').animate({scrollTop:0}, 'fast');

        return false;
      break;

      case "batchDelete":
      if (!confirm(txt_confirm))
      {
        return false;
      }
      break;
    }

    params['task']   = $("#task").val();
    params['format'] = "raw";

    $.post(route_execute_batch, params, function (response) {
      if (response.status == 1)
      {
        switch (response.action)
        {
          case "approve":
            response.ids.each(function (i) {
              $("#comment_" + i).removeClass("unapproved").find(".approve").html(txt_unapprove);
            });
          break;

          case "unapprove":
            response.ids.each(function (i) {
              $("#comment_" + i).addClass("unapproved").find(".approve").html(txt_approve);
            });
          break;

          case "delete":
            $.post(last_pagination_link, { format: "raw" }, function (data) {
              $("#comments").html(data);
              registerEvents();
              $('html').animate({scrollTop:0}, 'fast');
              $("#action-message").removeClass("action-error").show().html(response.message);
            });
          break;
        }
        $("#action-message").removeClass("action-error").show().html(response.message);
      }
      else
      {
        $("#action-message").addClass("action-error").show().html(response.message);
      }

      $('html').animate({scrollTop:0}, 'fast');
    }, "json");

    return false;
  });

  // Pagination links
  function paginationLinks()
  {
    $(".pagination a").click(function () {
      var href = $(this).attr("href");

      $.post(href, { format: "raw" }, function (response) {
        $("#comments").html(response);
        $("#action-message").hide();
        last_pagination_link = href;
        registerEvents();
        $('html').animate({scrollTop:0}, 'fast');
      });

      return false;
    });
  }

  // Register Events
  function registerEvents()
  {
    // Pagination links
    paginationLinks();

    // Remove report
    $(".report a").click(function () {
      var parent = $(this).parent().parent().parent().parent();
      var id = parent.attr("id");
      id = id.split("_");
      id = id[1];

      $.post(route_remove_report, {
        id:     id,
        format: "raw"
      }, function (response) {
        switch (response.status)
        {
          case 1:
          $("#action-message").addClass("action-error").show().html(response.message);
          break;

          case 2:
          $("#action-message").removeClass("action-error").show().html(response.message);

          $("#comment_" + id + " .report").remove();
          $("#comment_" + id).removeClass("reported");
          break;
        }

        $('html').animate({scrollTop:0}, 'fast');
      }, "json");

      return false;
    });

    // Post Comments
    $("a[id*=post-comments-]").click(function () {
      var id = $(this).attr("id").split("-");
      id = id[2];

      $.post(route_filter_post, {
        id:     id,
        format: "raw"
      }, function () {
        $.post(route_load_comments, { format: "raw" }, function (response) {
          $("#comments").html(response);
          $('html').animate({scrollTop:0}, 'fast');
          registerEvents();
        });
      });

      return false;
    });

    // Show all posts
    $("#show-all-posts").click(function () {
      $.post(route_filter_post, {
        id:     0,
        format: "raw"
      }, function () {
        $.post(route_load_comments, { format: "raw" }, function (response) {
          $("#comments").html(response);
          $('html').animate({scrollTop:0}, 'fast');
          registerEvents();
        });
      });

      return false;
    });

    // Quick edit
    $(".quickedit").click(function () {
      var link        = $(this);
      var parent      = link.parent().parent().parent();
      var id          = parent.attr("id").split('_');
      var cell        = link.parent().parent();
      var content     = cell.find(".comment-content");
      var comment     = content.html();

      id = id[1];
      content.html("<textarea id='new-comment-" + id + "' style='width: 100%; height: 150px;'>" + comment + "</textarea><br /><input type='button' class='quickedit-save-" + id + "' value='" + txt_save + "' /><a href='#' class='quickedit-cancel-" + id + "' style='margin-left: 10px;'>Cancel</a>");

      // Cancel
      $(".quickedit-cancel-" + id).click(function () {
        content.html(comment);
        parent.find(".comment-actions").css("visibility", "hidden");

        return false;
      });

      // Save
      $(".quickedit-save-" + id).click(function () {
        $.post(route_quick_edit, {
          id:      id,
          content: $("#new-comment-" + id).val(),
          format:  "raw"
        }, function (response) {
          switch (response.status)
          {
            case 0:
              $("#action-message").addClass("action-error").show().html(response.message);
              content.html(comment);
            break;

            case 1:
              $("#action-message").removeClass("action-error").show().html(response.message);
              content.html($("#new-comment-" + id).val());
            break;
          }

          $('html').animate({scrollTop:0}, 'fast');
        }, "json");

        return false;
      });

      return false;
    });

    // Approve button
    $(".approve").click(function () {
      var link        = $(this);
      var parent      = link.parent().parent().parent();
      var id          = parent.attr("id").split('_');
      var text        = parent.hasClass("unapproved") ? txt_unapprove : txt_approve;
      var old_text    = link.html();
      var status_text = parent.hasClass("unapproved") ? txt_approved : txt_unapproved;

      id = id[1];

      $.post(route_approve, {
        id: id
      }, function (response) {
        switch (response.status)
        {
          case 1:
            $("#action-message").addClass("action-error").show().html(response.message);

            $('html').animate({scrollTop:0}, 'fast');
          break;

          case 2:
            link.html(text);
            parent.toggleClass('unapproved');

            $("#action-message").removeClass("action-error").show().html("1 " + txt_comment_has_been + status_text);
          break;
        }
      }, "json");

      return false;
    });

    // Delete button
    $(".delete").click(function () {
      var link        = $(this);
      var parent      = link.parent().parent().parent();
      var id          = parent.attr("id").split('_');
      var old_text    = link.html();

      id = id[1];

      $.post(route_delete, {
        id: id
      }, function (response) {
        switch (response.status)
        {
          case 1:
            $("#action-message").addClass("action-error").show().html(response.message);
            link.html(old_text);
          break;

          case 2:
            $("#action-message").removeClass("action-error").show().html("1 " + txt_comment_has_been_deleted);

            $.post(last_pagination_link, { format: "raw"}, function (response) {
              $("#comments").html(response);
              registerEvents();
            });
          break;
        }

        $('html').animate({scrollTop:0}, 'fast');
      }, "json");

      return false;
    });

    // Show comment actions
    $("tr[id*='comment_']").hover(function () {
      $(this).find(".comment-actions").css("visibility", "visible");
    }, function () {
      $(this).find(".comment-actions").css("visibility", "hidden");
    });

    // Check all
    $("#checkAll").change(function () {
      var status = $(this).attr("checked");

      $("input.checkbox").each(function () {
        $(this).attr("checked", status);
      });
    });
  }
});