jQueryFactory(document).ready(function ($) {
  var last_pagination_link = document.location.href;

  // Register Events on page load
  registerEvents();

  // Ajax Loading Bar
  $("#loading-bar").ajaxStart(function(){
    $(this).show();
    $("#action-message").fadeOut('fast');
  });
  $("#loading-bar").ajaxStop(function(){
    $(this).hide();
  });


  // Submit filter
  $("#submit-filter").click(function () {
    var filter          = $("#filter").val();
    var category_filter = $("#category_filter").val();

    $.post(route_filter, {
      filter:          filter,
      category_filter: category_filter,
      format:          "raw"
    }, function (response) {
      $.post(route_load_posts, { format: "raw"}, function (response) {
        $("#posts").html(response);
        registerEvents();
        $('html').animate({scrollTop:0}, 'fast');
      });

    });

    return false;
  });

  // Submit form
  $("#form").submit(function  () {
    var params  = {};
    var checked = false;

    // Check if at least one object has been selected
    $(this).find("input.checkbox").each(function () {
      if ($(this).attr("checked"))
      {
        params[$(this).attr("name")] = $(this).attr("checked");
        checked = true;
      }
    });

    // Check if at lease one item is selected
    if (!checked)
    {
      $("#action-message").fadeOut("fast", function () {
        $(this).addClass("action-error").show().html(txt_select_one_item)
      });
      $('html').animate({scrollTop:0}, 'fast');

      return false;
    }

    switch ($("#task").val())
    {
      case "":
        $("#action-message").fadeOut("fast", function () {
          $(this).addClass("action-error").show().html(txt_select_action)
        });
        $('html').animate({scrollTop:0}, 'fast');

        return false;
      break;

      case "batchDelete":
        if (!confirm(txt_confirm))
        {
          return false;
        }
      break;

    }

    params['task']   = $("#task").val();
    params['format'] = "raw";

    $.post(route_batch, params, function (response) {
      switch (response.action)
      {
        case "publish":
        response.ids.each(function (i) {
          $("#post_" + i).removeClass("unapproved").find(".publish").html(txt_unpublish);
        });
        break;

        case "unpublish":
        response.ids.each(function (i) {
          $("#post_" + i).addClass("unapproved").find(".publish").html(txt_publish);
        });
        break;

        case "delete":
          $.post(last_pagination_link, { format: "raw" }, function (response) {
            $("#posts").html(response);
            registerEvents();
            $('html').animate({scrollTop:0}, 'fast');
          });
        break;

       
      }

      $("#action-message").removeClass("action-error").show().html(response.message);

      $('html').animate({scrollTop:0}, 'fast');
    }, "json");

    return false;
  });

  // Pagination links
  function paginationLinks()
  {
    $(".pagination a").click(function () {
      var href = $(this).attr("href");

      $.post(href, { format: "raw" }, function (response) {
        $("#posts").html(response);
        last_pagination_link = href;
        registerEvents();
        $('html').animate({scrollTop:0}, 'fast');
      });

      return false;
    });
  }

  // Register Events
  function registerEvents()
  {
    // Pagination links
    paginationLinks();

    // Publish button
    $(".publish").click(function () {
      var link        = $(this);
      var parent      = link.parent().parent().parent();
      var id          = parent.attr("id").split('_');
      var text        = parent.hasClass("unapproved") ? txt_unpublish : txt_publish;
      var old_text    = link.html();
      var status_text = parent.hasClass("unapproved") ? txt_published : txt_unpublished;

      id = id[1];

      $.post(route_publish, {
        id:     id,
        format: "raw"
      }, function (response) {
        switch (response.status)
        {
          case 1:
          $("#action-message").addClass("action-error").show().html(response.message);
          link.html(old_text);

          $('html').animate({scrollTop:0}, 'fast');
          break;

          case 2:
          link.html(text);
          parent.toggleClass('unapproved');

          $("#action-message").removeClass("action-error").show().html("1 " + txt_post_has_been + " " + status_text);
          break;
        }
      }, "json");

      return false;
    });

    // Delete button
    $(".delete").click(function () {
      var link        = $(this);
      var parent      = link.parent().parent().parent();
      var id          = parent.attr("id").split('_');
      var old_text    = link.html();

      id = id[1];

      $.post(route_delete, {
        id:     id,
        format: "raw"
      }, function (response) {
        switch (response.status)
        {
          case 1:
            $("#action-message").addClass("action-error").show().html(response.message);
            link.html(old_text);
          break;

          case 2:
            $("#action-message").removeClass("action-error").show().html("1 " + txt_post_has_been_deleted);

            $.post(last_pagination_link, { format: "raw" }, function (response) {
              $("#posts").html(response);
              registerEvents();
            });
          break;
        }

        $('html').animate({scrollTop:0}, 'fast');
      }, "json");

      return false;
    })

    // Show comment actions
    $("tr[id*='post_']").hover(function () {
      $(this).find(".post-actions").css("visibility", "visible");
    }, function () {
      $(this).find(".post-actions").css("visibility", "hidden");
    });

    // Check all
    $("#checkAll").change(function () {
      var status = $(this).attr("checked");

      $("input.checkbox").each(function () {
        $(this).attr("checked", status);
      });
    });
  }
});