jQueryFactory(document).ready(function ($) {
  // Ajax Loading Bar
  $("#loading-bar").ajaxStart(function(){
    $(this).show();
  });
  $("#loading-bar").ajaxStop(function(){
    $(this).hide();
  });

  // Load comments
  $('div[id^="wallfactory-comments"]').each(function () {
		var id = $(this).attr('class');
		$.post(root + 'index.php?option=com_wallfactory&view=comments&layout=_list&post_id=' + id
				, { format: "raw" }, function (response) {
    $("#wallfactory-comments").html(response);
  });
  });
  
  // Write comment
  $('.submit-comment').live( 'click', function(event)  {
	event.preventDefault();
    var valid = true;

    var id = $(this).attr("rel");
    
    // Comment
    if ($("#comment").val() == "")
    {
      valid = false;
      $("#comment_error").show();
    }
    else
    {
      $("#comment_error").hide();
    }

    // Captcha enabled 
    if (captcha_comment)
    {
      // Comment
      if ($("#recaptcha_response_field").val() == "")
      {
        valid = false;
        $("#recaptcha_response_field_error span").html(txt_field_required);
        $("#recaptcha_response_field_error").show();
      }
      else
      {
        $("#recaptcha_response_field_error").hide();
      }
    }
   
   if (valid)
   {

   	 if (is_guest) {
   	  
    var link = $(this);
      link.hide();
      $("#comment-loader").show();

      $.post(route_post_comment, {
        recaptcha_response_field:  $("#recaptcha_response_field").val(),
        recaptcha_challenge_field: $("#recaptcha_challenge_field").val(),
        comment: $('#comment' + id).val(),
        post_id: id

      }, function (response) {
     	
        switch (response.errors)
        {
          case 1:
         
          $("#recaptcha_response_field_error span").html(response.message);
          $("#recaptcha_response_field_error").show();

          Recaptcha.reload();

          link.show();
          $("#wallfactory-comment-loader" + id).hide();
          $("#comment-form" + id).hide();
          break;

          case 2:
          // need login
	        window.location.href = root + 'index.php?option=com_user&view=login';
          break;
        }

        window.location.href = root + 'index.php?option=com_wallfactory&view=comments&layout=_list&post_id=' + $('.round_post').attr('rel') ;
      });
    

    $('div[id^="wallfactory-comments"]').each(function () {
 		var id = $(this).attr('class');
	 		
 		$('#wallfactory-comments' + id + ' .loading').hide();
 	});
   }
   
   else {
	 
	$.post(root + 'index.php?option=com_wallfactory&format=raw&controller=comment&task=addComment', {
      comment: $('#comment' + id).val(),
	  post_id: id },
    
	  function(response) {
	     $('#wallfactory-comments' + id + ' .update').load(root + 'index.php?option=com_wallfactory&format=raw&view=comments&layout=_latest&post_id=' + $('#wallfactory-comments' + id).attr('class'));
    });
     
    // hide wall comments
   	$(".comments_list").hide();

   	$("#wallfactory-comment-loader" + id).hide();
   	$("#comment-form" + id).hide();
   }
   
   }
   
    return false;
  });
  

});