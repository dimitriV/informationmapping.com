jQueryFactory(document).ready(function ($) {
  var last_pagination_link = document.location.href;
  // Register Events on page load
  registerEvents();

  // Ajax Loading Bar
  $("#loading-bar").ajaxStart(function(){
    $(this).show();
    $("#action-message").fadeOut("fast");
  });
  $("#loading-bar").ajaxStop(function(){
    $(this).hide();
  });

  // Pagination links
  function paginationLinks()
  {
    $(".pagination a").click(function () {
      var href = $(this).attr("href");

      $.post(href, { format: "raw" }, function (response) {
        $("#followed").html(response);
        last_pagination_link = href;
        registerEvents();
        $('html').animate({scrollTop:0}, 'fast');
      });

      return false;
    });
  }

  // Submit filter
  $("#submit-filter").click(function () {
    var notification = $("#notification").val();

    $.post(route_filter, {
      notification: notification,
      format:       "raw"
    }, function (response) {
      $.post(route_load_walls, { format: "raw" }, function (response) {
        $("#followed").html(response);
        registerEvents();
        $('html').animate({scrollTop:0}, 'fast');
      });
    });

    return false;
  });

  // Submit form
  $("#form").submit(function  () {
    var params  = {};
    var checked = false;

    $(this).find("input.checkbox").each(function () {
      if ($(this).attr("checked"))
      {
        params[$(this).attr("name")] = $(this).attr("checked");
        checked = true;
      }
    });

    // Check if at lease one item is selected
    if (!checked)
    {
      $("#action-message").fadeOut("fast", function () {
        $(this).addClass("action-error").show().html(txt_select_at_least_one_item)
      });
      $('html').animate({scrollTop:0}, 'fast');

      return false;
    }

    // Check the action
    switch ($("#task").val())
    {
      case "":
      $("#action-message").fadeOut("fast", function () {
        $(this).addClass("action-error").show().html(txt_select_the_action)
      });
      $('html').animate({scrollTop:0}, 'fast');

      return false;
      break;

      case "batchDelete":
      if (!confirm(txt_confirm))
      {
        return false;
      }
      break;
    }

    params['task']   = $("#task").val();
    params['format'] = "raw";

    // Submit the form
    $.post(route_submit, params, function (response) {
      if (response.errors != 1)
      {
        switch (response.action)
        {
          case "add_notification":
            response.ids.each(function (i) {
              $("#followed_" + i).removeClass("unapproved").find(".notification").html(txt_remove_notification);
            });
          break;

          case "remove_notification":
            response.ids.each(function (i) {
              $("#followed_" + i).addClass("unapproved").find(".notification").html(txt_add_notification);
            });
          break;

          case "delete":
            $.post(last_pagination_link, { format: "raw" }, function (response) {
              $("#followed").html(response);
              registerEvents();
              $('html').animate({scrollTop:0}, 'fast');
            });
          break;
        }

        $("#action-message").removeClass("action-error").show().html(response.message);
      }
      else
      {
        $("#action-message").addClass("action-error").show().html(response.message);
      }

      $('html').animate({scrollTop:0}, 'fast');
    }, "json");

    return false;
  });

  // Register Events
  function registerEvents()
  {
    // Pagination links
    paginationLinks();

    // Notification button
    $(".notification").click(function () {
      var link        = $(this);
      var parent      = link.parent().parent().parent();
      var id          = parent.attr("id").split('_');
      var text        = parent.hasClass("unapproved") ? txt_remove_notification : txt_add_notification;
	  var status_text = parent.hasClass("unapproved") ? txt_added : txt_removed;
      id = id[1];

      $.post(route_notification, { id: id, format: "raw" }, function (response) {

        if (response.errors != 1)
        {
          switch (response.status)
          {
            case 1:
            $("#action-message").addClass("action-error").show().html(response.message);
            $('html').animate({scrollTop:0}, 'fast');
            break;

            case 2:
			if (text == 'add notification') {
				var path = root + 'components/com_wallfactory/assets/images/cancel.png';
				link.html('<img src="'+path+'" alt="Test Image" title="Add notification" />');
				}
			else { 
				if (text == 'remove notification') 
					var path = root + 'components/com_wallfactory/assets/images/accept.png';
					link.html('<img src="'+path+'" alt="Test Image" title="Remove notification" />');
			}
			

            parent.toggleClass('unapproved');

            $("#action-message").removeClass("action-error").show().html("1 " + txt_notification_has_been + " " + status_text);
            break;
          }
        }
        else
        {
          $("#action-message").addClass("action-error").show().html(response.message);
          $('html').animate({scrollTop:0}, 'fast');
        }
      }, "json");

      return false;
    });

    // Delete button
    $(".delete").click(function () {
      var link        = $(this);
      var parent      = link.parent().parent();
	  var id          = parent.attr("id").split('_');
	    
      id = id[1];

      $.post(route_delete, {
        id:     id,
        format: "raw"
      }, function (response) {
	    switch (response.status)
        {
          case 1:
            $("#action-message").addClass("action-error").show().html(response.message);
          break;

          case 2:
            $("#action-message").removeClass("action-error").show().html("1 " + txt_followed_wall_has_been_removed);

            $.post(last_pagination_link, { format: "raw" }, function (response) {
              $("#followed").html(response);
              registerEvents();
            });
          break;
        }

        $('html').animate({scrollTop:0}, 'fast');
      }, "json");

      return false;
    });

   
    // Check all
    $("#checkAll").click(function () {
      var status = $(this).attr("checked");

      $("input.checkbox").each(function () {
        $(this).attr("checked", status);
      });
    });
  }
});