jQueryFactory(document).ready(function ($) {
  // Register Events on page load
  registerEvents();

  // Ajax Loading Bar
  $("#loading-bar").ajaxStart(function(){
    $(this).show();
  });
  $("#loading-bar").ajaxStop(function(){
    $(this).hide();
  });

  // Pagination links
  function paginationLinks()
  {
    $(".pagination a").click(function () {
      var href = $(this).attr("href");

      $.post(href, { format: "raw" }, function (response) {
        $("#followers").html(response);
        registerEvents();
        $('html').animate({ scrollTop: 0 }, 'fast');
      });

      return false;
    });
  }

  // Register Events
  function registerEvents()
  {
    // Pagination links
    paginationLinks();
  }
});