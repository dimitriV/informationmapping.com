jQueryFactory(document).ready(function ($) {
  // Ajax Loading Bar
  $("#loading-bar").ajaxStart(function(){
    $(this).show();
  });
  $("#loading-bar").ajaxStop(function(){
    $(this).hide();
  });

    // Write post
  $("#wall_form").submit(function () {
    var valid = true;
	var post = 1;
	$("textarea#post_message").removeClass("highlight");
	$("#image_name").removeClass("highlight");
	$("#mp3_name").removeClass("highlight");
	$("#file_name").removeClass("highlight");
	
	var mp3uploaded = $('input#hidFileID-mp3').val();
	var fileuploaded = $('input#hidFileID-file').val();
	var imageuploaded = $('input#hidFileID-image').val();

    // main post
    if ($("textarea#post_message").val() == "")
    {
	  valid = false;
	  $("textarea#post_message").addClass("highlight");
	  post = 0;
    }
    
	if (post == 1 )
    {
      // check media uploads
		if (imageuploaded != "") 
		{
		  if ($("#image_name").val() == "")
			  {
				valid = false;
				$("#image_name").addClass("highlight");
			  }				
		}
		
		if (mp3uploaded != "")
		{
			// Check mp3 title
			if ($("#mp3_name").length)
			{
			  if ($("#mp3_name").val() == "")
			  {
				valid = false;
				$("#mp3_name").addClass("highlight");
			  }
			}
		}
		
		if (fileuploaded != "")
		{
			// Check file title
			if ($("#file_name").length)
			{
			  if ($("#file_name").val() == "")
			  {
				valid = false;
				$("#file_name").addClass("highlight");
			  }
			}
		}
	}

    return valid;
  });

  
  // Pagination links
  function paginationLinks()
  {
    $("#wallfactory-pagination a").click(function () {
      var href = $(this).attr("href");

      $.post(href, { format: "raw" }, function (response) {
        $("#wallfactory-posts").html(response);
        
        $('div[id^="wallfactory-comments"]').each(function () {
 		var id = $(this).attr('class');
 		 		
 		$('#wallfactory-comments' + id + ' .loading').hide();
 	});
        
  });

      return false;
    });
  }

});