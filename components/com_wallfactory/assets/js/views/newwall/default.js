jQueryFactory(document).ready(function ($) {

  var t;

  // Validate the title
  $("#title").keyup(function (e) {

   if (e.keyCode == 13)
    {
	   	return true;
  	}

    var title = $("#title").val();
    $("#availability").html("<span class='wallfactory-button wallfactory-loader'>" + loading_text + "</span>");
    $("#register-submit").attr("disabled", "disabled");

    clearTimeout(t);
    t = setTimeout(function () {

      // Send the request
      $.post(route_check, { format: "raw", title: title }, function (response) {
        var class_name = response.errors ? 'cross' : 'accept';
        $("#availability").html("<span class='wallfactory-button wallfactory-" + class_name + "'>" + response.message + "</span>");

        if (!response.errors)
        {
          $("#register-submit").attr("disabled", "");
        }
      }, "json");

    }, 500);
  })
});