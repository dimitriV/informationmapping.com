<?php
/*------------------------------------------------------------------------
com_wallfactory - Wall Factory
------------------------------------------------------------------------
author    TheFactory
copyright Copyright (C) 2010 SKEPSIS Consult SRL. All Rights Reserved.
@license - http://www.gnu.org/licenses/gpl-2.0.html GNU/GPL
Websites: http://www.thefactory.ro
Technical Support:  Forum - http://www.thefactory.ro/joomla-forum/
-------------------------------------------------------------------------*/

defined('_JEXEC') or die('Restricted access');

require_once(JPATH_COMPONENT.DS.'controller.php');

if ($controller = JRequest::getWord('controller'))
{
	require_once(JPATH_COMPONENT.DS.'controllers'.DS.$controller.'.php');
}

$classname = 'FrontendController'.$controller;
$controller = new $classname();

$controller->execute(JRequest::getVar('task'));

$controller->redirect();
