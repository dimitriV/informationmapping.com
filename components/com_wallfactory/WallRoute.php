<?php
/*------------------------------------------------------------------------
com_wallfactory - Wall Factory
------------------------------------------------------------------------
author    TheFactory
copyright Copyright (C) 2010 SKEPSIS Consult SRL. All Rights Reserved.
@license - http://www.gnu.org/licenses/gpl-2.0.html GNU/GPL
Websites: http://www.thefactory.ro
Technical Support:  Forum - http://www.thefactory.ro/joomla-forum/
-------------------------------------------------------------------------*/

defined('_JEXEC') or die('Restricted access');

class WallRoute
{
  function _($route, &$object = null)
  {
   	global $Itemid, $mainframe;
   
    $router =& $mainframe->getRouter();
    
    
    $routes =  WallRoute::getRoutes();
    $route  =  $routes[$route];

    $output = 'index.php?option=com_wallfactory&'.$route['type'].'='.$route['value'];

    if ('controller' == $route['type'] && isset($route['task']))
    {
      $output .= '&task='.$route['task'];
    }

    foreach ($route['params'] as $param)
    {
      if (isset($object->$param))
      {
        $output .= '&'.$param.'='.$object->$param;
      }
    }

    $output .= '&Itemid='.$Itemid;

    return JRoute::_($output);
  }

  function getRoutes()
  {
    static $routes = null;

    if (is_null($routes))
    {
      $routes = array(
        'post' => array(
          'type'   => 'view',
          'value'  => 'post',
          'params' => array('alias','id')),
        'wall' => array(
          'type'   => 'view',
          'value'  => 'wall',
          'params' => array('alias')),
        'edit-post' => array(
          'type'   => 'view',
          'value'  => 'writePost',
          'params' => array('alias','post_id')),
        'write-post' => array(
          'type'   => 'view',
          'value'  => 'writePost',
          'params' => array()),
        'settings' => array(
          'type'   => 'view',
          'value'  => 'settings',
          'params' => array()),
		'you-have-been-banned' => array(
      		'type'   => 'view',
			'value'  => 'banned',
      		'params' => array('alias','post_id')),
      
      );
    }
   
    
    return $routes;
  }
}