<?php 
/*------------------------------------------------------------------------
com_wallfactory - Wall Factory
------------------------------------------------------------------------
author    TheFactory
copyright Copyright (C) 2010 SKEPSIS Consult SRL. All Rights Reserved.
@license - http://www.gnu.org/licenses/gpl-2.0.html GNU/GPL
Websites: http://www.thefactory.ro
Technical Support:  Forum - http://www.thefactory.ro/joomla-forum/
-------------------------------------------------------------------------*/

defined('_JEXEC') or die('Restricted access'); ?>

<h1><?php echo $this->wall->alias.'\'s Wall'; ?></h1>

<div class="align-top">

  <div id="main_post">
	<form action="<?php echo $this->root; ?>index.php" method="post" id="wall_form">
	  <?php if (!$this->user->guest || ($this->user->guest && $this->wallSettings->enable_guest_write) ): ?>
		<textarea class="round" id="post_message" cols="80" rows="5" name="post_message"></textarea> 
		
		<div class="align-top">
			<?php if (!$this->user->guest): ?>
			<a href="#" class="save-button" style="margin: 8px; float:left;" id="show-attachments-form"><b><?php echo JText::_('More options'); ?></b></a>
			<?php endif; ?>
			
			<div class="post-button"><!--style="border: 1px solid #dddddd; background-color: #ffffff; padding: 5px 10px; color: #135CAE;"-->
				<input type="submit" class="save-button" value="<?php echo JText::_('POST on the wall'); ?>"  />
			</div>
		</div>
	  <?php endif; ?>
	  <div class="wallfactory-clear"></div>

		<div style="vertical-align: top; display: none; " id="attachments">
	      <div class="pposts">
	        <?php require_once('_upload.php'); ?>
	      </div>
		</div>
	
	  <input type="hidden" name="option" value="com_wallfactory" />			  
	  <input type="hidden" name="controller" value="post" />
	  <input type="hidden" name="task" value="save" />
	  <input type="hidden" name="wall_id" id="wall_id" value="<?php echo $this->wall->id; ?>" />
	  <input type="hidden" name="current_wall_alias" id="current_wall_alias" value="<?php echo $this->wall->alias; ?>" />
	  <input type="hidden" name="PHPSESSID" id="PHPSESSID" value="<?php echo session_id(); ?>" />	  
	  <input type="hidden" name="Itemid" value="<?php echo $this->Itemid; ?>" />  
	</form>
  </div>
  
 <div class="right-menu">
     <!-- About me -->
	 <?php 
	  $source = JPATH_SITE.DS.'components'.DS.'com_wallfactory'.DS.'storage'.DS.'users'.DS.$this->wall->user_id.'/avatar/avatar.'. $this->wall->avatar_extension;
	 
	  if (file_exists($source)) {
	  $size = getimagesize($source);
							
		$max_size = 80;
		if ($size[0] > $max_size || $size[1] > $max_size)  {
		    if ($size[0] > $size[1]) {
		        $new_width  = round($max_size,0);
		        $new_height = round($max_size * $size[1] / $size[0],0);
		    }
		    else
		    {
		        $new_width  = round($max_size * $size[0] / $size[1],0);
		        $new_height = round($max_size,0);
		    }
		}
		else
		{
		    $new_width  = round($size[0],0);
		    $new_height = round($size[1],0);
		}
	  }
	 ?>
	<div style="padding: 3px;">
	 
	 <table cellspacing="0" cellpadding="0">
		<tr><td style="width: 90px;">
		   <div style="padding: 2px;">
			   <?php if ($this->wallSettings->enable_avatars && $this->wall->avatar_type): ?>
			   	<?php	
			 //  	$avatar_source = JPATH_SITE.DS.'components'.DS.'com_wallfactory'.DS.'storage'.DS.'users'.DS.$this->wall->user_id .'/avatar/avatar.'.$this->wall->avatar_extension; 
			 //  		if (file_exists($avatar_source)): ?>
			   	 <a href="<?php echo JRoute::_('index.php?option=com_wallfactory&Itemid=' . $this->Itemid . '&view=wall&alias=' . $this->wall->alias); ?>" style="margin-right: 1px;"> 
				  	 <?php echo $this->wallHelper->getAvatar($this->wall->user_id, $this->wall->avatar_type, $this->wall->avatar_extension);  ?>
			   	   	<!-- <img src="<?php //echo JURI::root(); ?>components/com_wallfactory/storage/users/<?php //echo $this->wall->user_id; ?>/avatar/avatar.<?php //echo $this->wall->avatar_extension; ?>?<?php //echo time(); ?>" class="user-avatar" 
					style="width:<?php //echo $new_width;?>px; height:<?php //echo $new_height;?>px" alt="avatar" />-->
				 </a>
			<?php //endif; ?>
			   <?php else: ?>
				<?php ($this->wallSettings->wall_title_display == 0) ? ($name = $this->user->name) : ($name = $this->wall->alias);
				         echo '<b>'.$name.'</b>'; ?>
			   <?php endif; ?>	 
	   	  </div>
     </td>
     <td class="align-top">
      <div class="content">
        <?php 
        if ($this->wall->name != '') echo '<b>'.$this->wall->name.'</b><br />';
        if ($this->wall->gender != '') {
        	$gender = ($this->wall->gender == 'm') ? 'Male' : 'Female';
        	echo $gender.'<br />';
        }
        if ($this->wall->birthday != '') echo date('d F Y', JText::_(strtotime($this->wall->birthday))).'<br />';
        if ($this->wall->description != '') echo $this->wall->description;
		?>
      </div>
   	</td> 
   </tr>
   <tr>
   	  <td colspan="2">
 		<div class="wallfactory-info">
	 		<span style="float:right; word-wrap: break-word;"><?php echo JText::_('Wall followed by '); ?><span id="followers"><?php echo $this->wall->followers; ?></span> <?php echo ($this->wall->followers == 1) ? JText::_('user') : JText::_('users'); ?>.</span>
	 		<span class="wallfactory-right">
		 		<?php if ( ( !$this->user->guest) && ($this->user->id != $this->wall->user_id) ): ?>
		              <a href="#" id="follow-wall" rel="<?php echo $this->followed ? 0 : 1; ?>"><?php echo $this->followed ? JText::_('Unsubscribe') : JText::_('Follow this wall'); ?></a>
		              <span id="follow-info"><?php echo $this->followed ? JText::_(' from this wall') : ''; ?></span>.
		        <?php endif; ?>
          
		        <?php if ( $this->user->guest): ?>
		          <?php echo JText::_('You must login to follow this wall'); ?>
        		<?php endif; ?>
        	</span>
			<p class="wallfactory-right"><?php echo JText::_('Member since: '); ?><span id="followers"><?php echo date('d F Y', JText::_(strtotime($this->wall->date_created))); ?></span></p>
		</div>	
	  </td>		
 	</tr>
  </table>	
  </div>
</div>

</div>
<div class="wallfactory-clear wallfactory-maxwidth"></div>

<?php if ($this->wall->id): ?>

	<div id="loading-bar" class="wallfactory-none"><?php echo JText::_('Loading...'); ?></div>

	<table width="100%">
	  <tr>
	    <td><div class="align-top">
	      <div class="posts">
	        <?php require_once('_list.php'); ?>
	      </div></div>
	    </td>
	  </tr>
	</table>

	<div class="wallfactory-clear"></div>

<?php else: ?>
  <h1><?php echo JText::_('An error has occured!'); ?></h1>
  <p><?php echo JText::_('The wall you are looking for wasn\'t found!'); ?></p>
  <br />
  <p><?php echo JText::_('Return to the'); ?> <a href="javascript: history.back();"><?php echo JText::_('previous page'); ?></a>, <?php echo JText::_('or find more walls'); ?> <a href="<?php echo JRoute::_('index.php?option=com_wallfactory&view=walls&layout=latest&Itemid=' . $this->Itemid); ?>"><?php echo JText::_('here'); ?></a>.</p>
<?php endif; ?>

