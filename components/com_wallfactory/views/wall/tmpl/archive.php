<?php
/*------------------------------------------------------------------------
com_wallfactory - Wall Factory
------------------------------------------------------------------------
author    TheFactory
copyright Copyright (C) 2010 SKEPSIS Consult SRL. All Rights Reserved.
@license - http://www.gnu.org/licenses/gpl-2.0.html GNU/GPL
Websites: http://www.thefactory.ro
Technical Support:  Forum - http://www.thefactory.ro/joomla-forum/
-------------------------------------------------------------------------*/

defined('_JEXEC') or die('Restricted access'); ?>

<?php if ($this->wall->id): ?>

<div id="loading-bar" class="wallfactory-none"><?php echo JText::_('Loading...'); ?></div>

<h1><?php echo JText::_('Archive'); ?></h1>
<div class="posts1">
  <?php require_once('_list.php'); ?>
</div>

<div class="wallfactory-clear"></div>

<?php else: ?>
  <h1><?php echo JText::_('An error has occured!'); ?></h1>
  <p><?php echo JText::_('The wall you are looking for wasn\'t found!'); ?></p>
  <br />
  <p><?php echo JText::_('Return to the'); ?> <a href="javascript: history.back();"><?php echo JText::_('previous page'); ?></a>, <?php echo JText::_('or find more walls'); ?> <a href="<?php echo JRoute::_('index.php?option=com_wallfactory&view=walls&layout=latest&Itemid=' . $this->Itemid); ?>"><?php echo JText::_('here'); ?></a>.</p>
<?php endif; ?>