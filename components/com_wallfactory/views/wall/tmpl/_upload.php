<?php
/*------------------------------------------------------------------------
com_wallfactory - Wall Factory
------------------------------------------------------------------------
author    TheFactory
copyright Copyright (C) 2010 SKEPSIS Consult SRL. All Rights Reserved.
@license - http://www.gnu.org/licenses/gpl-2.0.html GNU/GPL
Websites: http://www.thefactory.ro
Technical Support:  Forum - http://www.thefactory.ro/joomla-forum/
-------------------------------------------------------------------------*/

defined('_JEXEC') or die('Restricted access'); ?>

<div class="wallfactory_fieldset" id="wallfactory-wrapper">
	
 	<span class="legend">Attach to your wall message</span>
	<table cellpadding="3" cellspacing="0">
  
	<?php if ($this->wallSettings->allow_post_links): ?>
    <!-- URL -->
      <tr>
       <td width="80">
          <label for="link"><?php echo JText::_('URL'); ?>:</label>
        </td>
        <td><input type="text" maxlength="255" size="50" class="input_border" id="link" name="link"></td>
        
    </tr>
    <?php endif; ?>  
    
    <?php if ($this->wallSettings->allow_post_videos): ?>
    <tr><td colspan="2" class="upload_separator"></td></tr>
    
    <!-- video -->
    <tr>
        <td width="80">
          <label for="video"><?php echo JText::_('Embedded Video'); ?>:</label>
        </td>
        <td>
        <div id="video-sources" name="video-sources">
		  <ul style="margin: 0px; padding: 0px 0px 0px 20px; list-style-type: none;">
			<li style="margin: 0px; padding: 0px;">	
			<a target="_blank" alt="YouTube" title="YouTube" href="http://www.youtube.com/">YouTube</a></li>
	        <li><img src="<?php echo JURI::root(); ?>components/com_wallfactory/assets/images/bullet_grey.png" /></li>
			<li><a target="_blank" alt="MySpace Video" title="MySpace Video" href="http://vids.myspace.com/">MySpace Video</a></li>
			<li><img src="<?php echo JURI::root(); ?>components/com_wallfactory/assets/images/bullet_grey.png" /></li>
	        <li><a target="_blank" alt="Vimeo" title="Vimeo" href="http://www.vimeo.com/">Vimeo</a></li>
	        <li><img src="<?php echo JURI::root(); ?>components/com_wallfactory/assets/images/bullet_grey.png" /></li>
	        <li><a target="_blank" alt="Metacafe" title="Metacafe" href="http://www.metacafe.com/">Metacafe</a></li>
	        <li><img src="<?php echo JURI::root(); ?>components/com_wallfactory/assets/images/bullet_grey.png" /></li>
	        <li><a target="_blank" alt="Howcast" title="Howcast" href="http://www.howcast.com/">Howcast</a></li>
		  </ul>
		</div> 
	    </td>
    </tr>
    <tr>
        <td colspan="2"><input id="video-link" maxlength="255" type="text" size="50" class="input_border" name="video-link"></td>
    </tr>
    
    <?php endif; ?>  
   
	<?php if ($this->wallSettings->allow_post_images): ?>
	<tr><td colspan="2" class="upload_separator"></td></tr>
	<!-- image -->
    <tr>
        <td>
          <label for="image"><?php echo JText::_('Photo title'); ?>:</label>
        </td>
        <td><input type="text" maxlength="100" size="38" class="input_border" id="image_name" name="image_name"></td>
    </tr>
    <tr>
        <td>
          <label for="image"><?php echo JText::_('Photo description'); ?>:</label>
        </td>
  	 	<td><textarea id="image-description" cols="23" rows="3" name="image-description"></textarea> </td>
    </tr>
     
	<tr>
        <td colspan="2">
        
        <div id="image_container" class="image_container">
			<div id="current_image"></div>           
        </div>
        </td>
    </tr>
    <tr>
    	<td><label for="txtImageName">Image:</label></td>    
    	<td><div>
    		  <div>
	       	   	<input type="text" id="txtImageName" class="image-name" disabled="true">
        		<span id="swfupload-image-control" class="wallfactory-margin">
        			<input type="button" id="image-button" />
        		    <input type="button" id="btnCancelImage" value="Cancel Upload" disabled="disabled" style="font-size: 8pt; vertical-align: top;" />
        		</span>
        	  </div>
        	  <div id="image-progressbar" class="wallfactory-none"></div>
        	  <input name="hidFileID-image" id="hidFileID-image" value="" type="hidden">
        	</div>
        </td>	
    </tr>
  <?php endif; ?>  
  
   <?php if ($this->wallSettings->allow_post_mp3files): ?> 
	    <tr><td colspan="2" class="upload_separator"></td></tr>
	    <!-- mp3 -->
	     <tr>
	        <td>
	          <label for="mp3"><?php echo JText::_('MP3 title'); ?>:</label>
	        </td>
	        <td><input type="text" maxlength="255" size="38" class="input_border" id="mp3_name" name="mp3_name"></td>
	      </tr>
      
	      <tr>
	        <td colspan="2">
		        
		        <div id="mp3_container" class="image_container">
					<div id="current_mp3"></div>           
		        </div>
	        </td>
	      </tr>
	      <tr>
	        <td><label for="txtMp3Name">Mp3 File:</label></td>
	        <td><div>
	    		  <div>
	    		  	<input type="text" id="txtMp3Name" class="image-name" disabled="true">
	        	   	<span id="swfupload-mp3-control" class="wallfactory-margin">
	        	   		<input type="button" id="mp3-button" /></span>
	        	   		<input type="button" id="btnCancelMp3" value="Cancel Upload" disabled="disabled" style="font-size: 8pt; vertical-align: top;" />
	        	   	</span>	
	        	  </div>
	        	  <div id="mp3-progressbar" class="wallfactory-none"></div>
	        	  <input type="hidden" name="hidFileID-mp3" id="hidFileID-mp3" value="" />
	        	</div>    
	        </td>	
	      </tr>
      	
       <?php endif; ?>  
       
      <?php if ($this->wallSettings->allow_post_files): ?>
      <tr><td colspan="2" class="upload_separator"></td></tr>
      <!-- file -->
      <tr>
        <td width="80">
          <label for="image"><?php echo JText::_('File title'); ?>:</label>
        </td>
         <td><input type="text" maxlength="255" size="38" class="input_border" id="file_name" name="file_name"></td>
      </tr>
      <tr>
        <td colspan="2">
            <div id="file_container" class="image_container">
				<div id="current_file"></div>           
	        </div>
        </td>
      </tr>
      <tr>
        <td><label for="txtFileName">File name:</label></td>
        <td><div>
    		  <div>
    		  	<input type="text" id="txtFileName" class="image-name" disabled="true">
        		<span id="swfupload-file-control" class="wallfactory-margin">
        			<input type="button" id="file-button" /></span>
        			<input type="button" id="btnCancelFile" value="Cancel Upload" disabled="disabled" style="font-size: 8pt; vertical-align: top;" />
        			<span style="vertical-align: top;"><?php echo JText::_(' (doc,txt,pdf only)'); ?></span>
        		</span>	
        	  </div>
        	  <div id="file-progressbar" class="wallfactory-none"></div>
        	  <input type="hidden" name="hidFileID-file" id="hidFileID-file" value="" />
			</div>
		</td>	
      </tr>
      
    <?php endif; ?>  
    
    </table>
    
</div>
<!--</form>-->
<div class="wallfactory-spacer"></div>
