<?php
/*------------------------------------------------------------------------
com_wallfactory - Wall Factory
------------------------------------------------------------------------
author    TheFactory
copyright Copyright (C) 2010 SKEPSIS Consult SRL. All Rights Reserved.
@license - http://www.gnu.org/licenses/gpl-2.0.html GNU/GPL
Websites: http://www.thefactory.ro
Technical Support:  Forum - http://www.thefactory.ro/joomla-forum/
-------------------------------------------------------------------------*/

defined('_JEXEC') or die('Restricted access');

jimport('joomla.application.component.view');
//jimport( 'joomla.document.feed.feed' );

class FrontendViewWall extends JView
{
  function __construct()
  {
    parent::__construct();

    $this->wallHelper   = wallHelper::getInstance();
    $this->_wallSettings = new wallSettings();
    
    $this->alias_req = JRequest::getVar('alias','','GET', 'string');
  }

  function display($tpl = null)
  {
    require_once(JPATH_COMPONENT_ADMINISTRATOR.DS.'lib'.DS.'recaptcha'.DS.'recaptchalib.php');
  	global $Itemid;

    wallHelper::checkGuestView();
    
    $allow_guest = wallHelper::checkProfileView($this->alias_req);
    
    if (!$allow_guest) 
    	wallHelper::checkWallOwner();
    	
    $session =& JFactory::getSession();
    $user    =& JFactory::getUser();
    
	$wall   =& $this->get('WallData');
		
    $post   =& $this->get('Data');
    
    if (!is_null($post))
    {
      JRequest::setVar('id', $post->id, 'GET');
    }
	
    $comments 	=& $this->get('Comments'); 
    $media 		=& $this->get('Media'); 
    $urls 		=& $this->get('Urls'); 
    $posts      =& $this->get('Posts');
 	$settings   =& $this->get('Settings');
    $pagination =& $this->get('PostsPagination');
    $followed   =& $this->get('Followed');
    $my_wall    =& $this->get('IsMyWall');
    $count  	=& $this->get('TotalPosts');
    
    $this->assignRef('comments',   			$comments);
    $this->assignRef('media',   			$media);
    $this->assignRef('urls',   				$urls);
    $this->assignRef('wall',              	$wall);
    $this->assignRef('posts',             	$posts);
    $this->assignRef('settings',           	$settings);
    $this->assignRef('wallSettings', 		$this->_wallSettings);
    $this->assignRef('session',       		$session);
    $this->assignRef('root',          		JUri::root());
    $this->assignRef('count',      	  		$count);
    $this->assignRef('pagination',        	$pagination);
    $this->assignRef('followed',          	$followed);
    $this->assignRef('Itemid',           	$Itemid);
    $this->assignRef('user',              	$user);
    $this->assignRef('my_wall',           	$my_wall);
    $this->assignRef('allow_guest_comments', $this->_wallSettings->allow_guest_comments);
     
    if ($this->_wallSettings->enable_bookmarks)
    {
      $bookmarks =& $this->get('Bookmarks');
      $this->assignRef('bookmarks', $bookmarks);
      
      $video_bookmarks =& $this->get('VideoBookmarks');
      $this->assignRef('video_bookmarks', $video_bookmarks);
      
      $url_bookmarks =& $this->get('UrlBookmarks');
      $this->assignRef('url_bookmarks', $url_bookmarks);
      
      $mp3_bookmarks =& $this->get('Mp3Bookmarks');
      $this->assignRef('mp3_bookmarks', $mp3_bookmarks);
      
      $file_bookmarks =& $this->get('FileBookmarks');
      $this->assignRef('file_bookmarks', $file_bookmarks);
      
      $image_bookmarks =& $this->get('ImageBookmarks');
      $this->assignRef('image_bookmarks', $image_bookmarks);
      
    }
     	
 	JHtmlFactory::jQueryScript();
    JHtmlFactory::jQueryScript('cookie', 'ui', 'ui-widget', 'ui-mouse', 'ui-sortable');
    JHtmlFactory::jQueryScript('swfupload', 'ui', 'ui-widget', 'ui-mouse', 'ui-position', 'ui-draggable', 'ui-dialog', 'ui-tabs', 'ui-progressbar');
	JHTML::script('swfupload.js', 'components/com_wallfactory/assets/js/');
    JHTML::script('default.js', 'components/com_wallfactory/assets/js/views/wall/');
    JHTML::_('script', 'main.js', 'components/com_wallfactory/assets/js/');
    JHTML::_('script', 'comments.js', 'components/com_wallfactory/assets/js/');
    JHTML::_('script', 'jquery.prettyPhoto.js', 'components/com_wallfactory/assets/js/');  
    	
    if (!is_null($post))
    {
    	JHTML::script('default.js', 'components/com_wallfactory/assets/js/views/post/');
    }
         
    $declarations1 = array(
      'wall_id'                         => isset($wall->id) ? $wall->id : 0,
      'txt_you_are_following_this_wall' => JText::_('You are following this wall.'),
      'txt_unsubscribe'                 => JText::_('Unsubscribe'),
      'txt_follow_this_wall'            => JText::_('Follow this wall'),
      'route_follow_wall'               => JRoute::_('index.php?option=com_wallfactory&controller=wall&task=follow'),
      'wall_alias'                      => $wall->alias,
      'root'                            => JURI::root(),
      'route_upload_image' 				=> JRoute::_( 'index.php?option=com_wallfactory&controller=upload&task=uploadImage' ), 
      'route_upload_mp3' 				=> JRoute::_( 'index.php?option=com_wallfactory&controller=upload&task=uploadMp3' ), 
	  'route_upload_file' 				=> JRoute::_('index.php?option=com_wallfactory&controller=upload&task=uploadFile' ), 
	  'PHPsessionID' 					=> session_id (), 
	  'sessionName' 					=> $session->getName (), 
	  'sessionId' 						=> $session->getId () ,
    );
   
    wallHelper::addScriptDeclarations($declarations1);
    
    
    if (!is_null($post))
    {
      $declarations2 = array(
     	'post_id'               		=> !is_null($post) ? $post->id : 0,
     	'route_post_comment'    		=> JRoute::_('index.php?option=com_wallfactory&controller=comment&task=addComment'),
      	'route_report_comment'  		=> JRoute::_('index.php?option=com_wallfactory&controller=comment&task=report'),
      	'txt_loading_comments'  		=> JText::_('Loading comments...'),
      	'is_guest'						=> $user->guest,
      	'captcha_comment'       		=> $this->_wallSettings->captcha_comment,
      	
      ); 
      wallHelper::addScriptDeclarations($declarations2);
    }
        
    JHTML::stylesheet('wall.css',    'components/com_wallfactory/assets/css/views/');
    JHTML::stylesheet('jquery-ui-1.7.2.custom.css', 'components/com_wallfactory/assets/css/');
    JHTML::stylesheet('main.css',    'components/com_wallfactory/assets/css/');
    JHTML::stylesheet('posts.css',   'components/com_wallfactory/assets/css/views/');
    JHTML::stylesheet('buttons.css', 'components/com_wallfactory/assets/css/');
	JHTML::stylesheet('writepost.css', 'components/com_wallfactory/assets/css/');
	JHTML::stylesheet('prettyPhoto.css', 'components/com_wallfactory/assets/css/'); //media="screen"
   
	$document =& JFactory::getDocument();
	
	$href1 = JRoute::_('index.php?option=com_wallfactory&view=feed&type=rss&format=feed&content=entries&alias=' . $wall->alias); 
	$href2 = JRoute::_('index.php?option=com_wallfactory&view=feed&type=rss&format=feed&content=comments&alias=' . $wall->alias); 
	$href3 = JRoute::_('index.php?option=com_wallfactory&view=posts&type=rss'); 
	
	$attribs1 = array('type' => 'application/rss+xml', 'title' => 'RSS Feed Entries');
	$attribs2 = array('type' => 'application/rss+xml', 'title' => 'RSS Feed Comments'); 
	$attribs3 = array('type' => 'application/rss+xml', 'title' => 'RSS Feed News'); 
	$document->addHeadLink( $href1, 'alternate', 'rel', $attribs1 );
	$document->addHeadLink( $href2, 'alternate', 'rel', $attribs2 );
	$document->addHeadLink( $href3, 'alternate', 'rel', $attribs3 );
	
    $this->_display($tpl);
  }


  function _display($tpl)
  {
      parent::display($tpl);
   
  }
}