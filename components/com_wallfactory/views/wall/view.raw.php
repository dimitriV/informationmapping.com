<?php
/*------------------------------------------------------------------------
com_wallfactory - Wall Factory
------------------------------------------------------------------------
author    TheFactory
copyright Copyright (C) 2010 SKEPSIS Consult SRL. All Rights Reserved.
@license - http://www.gnu.org/licenses/gpl-2.0.html GNU/GPL
Websites: http://www.thefactory.ro
Technical Support:  Forum - http://www.thefactory.ro/joomla-forum/
-------------------------------------------------------------------------*/

defined('_JEXEC') or die('Restricted access');

jimport('joomla.application.component.view');

class FrontendViewWall extends JView
{
  function __construct()
  {
    parent::__construct();

    $this->wallHelper   = wallHelper::getInstance();
    $this->_wallSettings = new wallSettings();
  }

  function display($tpl = null)
  {
    global $Itemid;

    wallHelper::checkGuestView();
    wallHelper::checkWallOwner();

    $session =& JFactory::getSession();
    $user    =& JFactory::getUser();

    $wall   =& $this->get('WallData');
    $post   =& $this->get('Data');
    if (!is_null($post))
    {
      JRequest::setVar('id', $post->id, 'GET');
    }
	
    $comments 	=& $this->get('Comments'); 
    $media 		=& $this->get('Media'); 
    $urls 		=& $this->get('Urls'); 
    $posts      =& $this->get('Posts');
    $settings   =& $this->get('Settings');
    $pagination =& $this->get('PostsPagination');
    $followed   =& $this->get('Followed');
    $my_wall    =& $this->get('IsMyWall');
    $count  	=& $this->get('TotalPosts');
	
    $this->assignRef('comments',   			$comments);
    $this->assignRef('media',   			$media);
    $this->assignRef('urls',   				$urls);
    $this->assignRef('posts',           	$posts);
    $this->assignRef('wall',            	$wall);
    $this->assignRef('settings',        	$settings);
    $this->assignRef('wallSettings', 		$this->_wallSettings);
    $this->assignRef('pagination',      	$pagination);
    $this->assignRef('followed',        	$followed);
    $this->assignRef('session',       		$session);
    $this->assignRef('root',          		JUri::root());
    $this->assignRef('Itemid',          	$Itemid);
    $this->assignRef('user', 				$user);   
    $this->assignRef('my_wall',          	$my_wall);
    $this->assignRef('count',      	  		$count);
    $this->assignRef('allow_guest_comments', $this->_wallSettings->allow_guest_comments);
     
    if ($this->_wallSettings->enable_bookmarks)
    {
     $bookmarks =& $this->get('Bookmarks');
      $this->assignRef('bookmarks', $bookmarks);
      
      $video_bookmarks =& $this->get('VideoBookmarks');
      $this->assignRef('video_bookmarks', $video_bookmarks);
      
      $url_bookmarks =& $this->get('UrlBookmarks');
      $this->assignRef('url_bookmarks', $url_bookmarks);
      
      $mp3_bookmarks =& $this->get('Mp3Bookmarks');
      $this->assignRef('mp3_bookmarks', $mp3_bookmarks);
      
      $file_bookmarks =& $this->get('FileBookmarks');
      $this->assignRef('file_bookmarks', $file_bookmarks);
      
      $image_bookmarks =& $this->get('ImageBookmarks');
      $this->assignRef('image_bookmarks', $image_bookmarks);
      
    }
     	
    JHtmlFactory::jQueryScript();
    JHTML::script('default.js', 'components/com_wallfactory/assets/js/views/wall/');
    
    JHtmlFactory::jQueryScript('cookie', 'ui', 'ui-widget', 'ui-mouse', 'ui-sortable');
    JHtmlFactory::jQueryScript('swfupload', 'ui', 'ui-widget', 'ui-mouse', 'ui-position', 'ui-draggable', 'ui-dialog', 'ui-tabs', 'ui-progressbar');
	JHTML::script('swfupload.js', 'components/com_wallfactory/assets/js/');

	JHTML::_ ( 'script', 'main.js', 'components/com_wallfactory/assets/js/' );
	JHTML::_ ( 'script', 'comments.js', 'components/com_wallfactory/assets/js/' );
	JHTML::_ ( 'script', 'jquery.prettyPhoto.js', 'components/com_wallfactory/assets/js/' );  
    
    $this->_layout = '_list';

    $this->_display($tpl);
  }


  function _display($tpl)
  {
      parent::display($tpl);
  }
}