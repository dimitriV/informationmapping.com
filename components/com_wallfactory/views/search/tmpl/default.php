<?php
/*------------------------------------------------------------------------
com_wallfactory - Wall Factory
------------------------------------------------------------------------
author    TheFactory
copyright Copyright (C) 2010 SKEPSIS Consult SRL. All Rights Reserved.
@license - http://www.gnu.org/licenses/gpl-2.0.html GNU/GPL
Websites: http://www.thefactory.ro
Technical Support:  Forum - http://www.thefactory.ro/joomla-forum/
-------------------------------------------------------------------------*/

defined('_JEXEC') or die('Restricted access'); ?>

<h1><?php echo JText::_('Search results'); ?></h1>

<?php if (!count($this->results)): ?>
  <?php echo JText::_('No results found.'); ?>
<?php else: ?>
  <div id="results">
    <?php foreach ($this->results as $result): ?>
      <div class="title">
	      <a href="<?php echo WallRoute::_('post', $result); ?>"><?php echo $result->title; ?></a> 
      </div>
      <div class="content"><?php echo $result->content; ?></div>
      <div class="info"><?php echo JText::_('by '); ?>
        <a href="<?php echo JRoute::_('index.php?option=com_wallfactory&view=wall&Itemid=' . $this->Itemid . '&alias=' . $result->alias); ?>">
          <?php echo $result->alias; ?>
        </a>
      </div>

      <div class="spacer"></div>
    <?php endforeach; ?>
  </div>
<?php endif; ?>

<div id="pagination">
  <?php echo $this->pagination->getPagesLinks(); ?>
</div>