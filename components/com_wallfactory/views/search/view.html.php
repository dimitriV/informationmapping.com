<?php
/*------------------------------------------------------------------------
com_wallfactory - Wall Factory
------------------------------------------------------------------------
author    TheFactory
copyright Copyright (C) 2010 SKEPSIS Consult SRL. All Rights Reserved.
@license - http://www.gnu.org/licenses/gpl-2.0.html GNU/GPL
Websites: http://www.thefactory.ro
Technical Support:  Forum - http://www.thefactory.ro/joomla-forum/
-------------------------------------------------------------------------*/

defined('_JEXEC') or die('Restricted access');

jimport('joomla.application.component.view');

class FrontendViewSearch extends JView
{
  function __construct()
  {
    parent::__construct();

    $this->wallHelper   = wallHelper::getInstance();
    $this->wallSettings = new wallSettings();
  }

  function display($tpl = null)
  {
    global $Itemid;

    $results    =& $this->get('Data');
   
    $pagination =& $this->get('Pagination');

    require_once(JPATH_BASE.DS.'administrator'.DS.'components'.DS.'com_search'.DS.'helpers'.DS.'search.php' );

    $searchword =& JRequest::getVar('q', '', 'GET', 'string');

    $searchword = trim($searchword);
    $searchword = trim($searchword, ',');

    $searchwords = explode(',', $searchword);
    foreach ($searchwords as $i => $searchword)
    {
      $searchwords[$i] = trim($searchword);
    }
		$needle = $searchwords[0];

		$searchwords = array_unique($searchwords);
		$searchRegex = '#(';
		$x = 0;
		foreach ($searchwords as $k => $hlword)
		{
		  $searchRegex .= ($x == 0 ? '' : '|');
		  $searchRegex .= preg_quote($hlword, '#');
		  $x++;
		}
		$searchRegex .= ')#iu';

    foreach ($results as $i => $result)
    {
      $content = $result->content;
      $wall_alias   = $result->wall_alias;
     
      // Content
      $content = SearchHelper::prepareSearchContent($content, 200, $needle);
      $wall_alias   = SearchHelper::prepareSearchContent($wall_alias, 100, $needle);
      
      if ($needle != '')
      {
        $content 		= preg_replace($searchRegex, '<span class="wallfactory-highlight">\0</span>', $content);
        $wall_alias   	= preg_replace($searchRegex, '<span class="wallfactory-highlight">\0</span>', $wall_alias);
      }

      $results[$i]->content = $content;
      $results[$i]->wall_alias   = $wall_alias;
      
    }

    $this->assignRef('results',    $results);
    $this->assignRef('pagination', $pagination);
    $this->assignRef('Itemid',     $Itemid);

    JHTML::stylesheet('search.css', 'components/com_wallfactory/assets/css/views/');

    $document =& JFactory::getDocument();
    $document->setTitle(ucwords($searchword) . ' - ' . $document->getTitle());

    $this->_display($tpl);
  }

  
  function _display($tpl)
  {
      parent::display($tpl);
  }
}