<?php
/*------------------------------------------------------------------------
com_wallfactory - Wall Factory
------------------------------------------------------------------------
author    TheFactory
copyright Copyright (C) 2010 SKEPSIS Consult SRL. All Rights Reserved.
@license - http://www.gnu.org/licenses/gpl-2.0.html GNU/GPL
Websites: http://www.thefactory.ro
Technical Support:  Forum - http://www.thefactory.ro/joomla-forum/
-------------------------------------------------------------------------*/

defined('_JEXEC') or die('Restricted access');

jimport('joomla.application.component.view');

class FrontendViewPosts extends JView
{
  function __construct()
  {
    parent::__construct();

    $this->wallHelper   = wallHelper::getInstance();
    $this->wallSettings = new wallSettings();
  }

  function display($tpl = null)
  {
    global $Itemid;

    wallHelper::checkGuestView();

    $session 	= & JFactory::getSession ();
	$user 		= & JFactory::getUser ();
		
    $posts      =& $this->get ( 'Data' );
    $comments 	=& $this->get ( 'Comments' );
	$media 		=& $this->get ( 'Media' ); 
    $urls 		=& $this->get ( 'Urls' ); 
    $pagination =& $this->get ( 'Pagination' );
    $settings 	=& $this->get ( 'Settings' );
    $my_wall 	=& $this->get ( 'MyWall' );
    $count  	=& $this->get ('TotalPosts');

    $this->assignRef ( 'posts',      	$posts );
    $this->assignRef ( 'comments', 		$comments );
	$this->assignRef ( 'media',   		$media );
    $this->assignRef ( 'urls',   		$urls );
    $this->assignRef ( 'pagination', 	$pagination );
    $this->assignRef ( 'settings', 		$settings );
    $this->assignRef ( 'wallSettings', 	$this->wallSettings );
    $this->assignRef ( 'session', 		$session );
	$this->assignRef ( 'root', 			JUri::root () );
	$this->assignRef ( 'count',      	$count);
    $this->assignRef ( 'user',       	$user );
    $this->assignRef ( 'my_wall', 		$my_wall );
    $this->assignRef ( 'Itemid',     	$Itemid );
    $this->assignRef ( 'allow_guest_comments', $this->wallSettings->allow_guest_comments );
	
  
    $this->_layout = '_list';
    
    JHtmlFactory::jQueryScript ();
	JHTML::script ( 'latest.js', 'components/com_wallfactory/assets/js/views/posts/' );
		
	JHtmlFactory::jQueryScript ( 'cookie', 'ui', 'ui-widget', 'ui-mouse', 'ui-sortable' );
	JHtmlFactory::jQueryScript ( 'swfupload', 'ui', 'ui-widget', 'ui-mouse', 'ui-position', 'ui-draggable', 'ui-dialog', 'ui-tabs', 'ui-progressbar' );
	JHTML::script ( 'swfupload.js', 'components/com_wallfactory/assets/js/' );
		
	JHTML::_ ( 'script', 'main.js', 'components/com_wallfactory/assets/js/' );
	JHTML::_ ( 'script', 'comments.js', 'components/com_wallfactory/assets/js/' );
	JHTML::_ ( 'script', 'jquery.prettyPhoto.js', 'components/com_wallfactory/assets/js/' );  

    parent::display($tpl);
  }
}