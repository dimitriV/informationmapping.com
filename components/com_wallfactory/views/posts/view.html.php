<?php
/*------------------------------------------------------------------------
com_wallfactory - Wall Factory
------------------------------------------------------------------------
author    TheFactory
copyright Copyright (C) 2010 SKEPSIS Consult SRL. All Rights Reserved.
@license - http://www.gnu.org/licenses/gpl-2.0.html GNU/GPL
Websites: http://www.thefactory.ro
Technical Support:  Forum - http://www.thefactory.ro/joomla-forum/
-------------------------------------------------------------------------*/

defined ( '_JEXEC' ) or die ( 'Restricted access' );

jimport ( 'joomla.application.component.view' );

class FrontendViewPosts extends JView {
	
	function __construct() {
		parent::__construct ();
		
		$this->wallHelper = wallHelper::getInstance();
		$this->wallSettings = new wallSettings();
		
		require_once (JPATH_SITE.DS.'components'.DS.'com_wallfactory'.DS.'WallRoute.php');

	}
	
	function display($tpl = null) 
	{
	  require_once(JPATH_COMPONENT_ADMINISTRATOR.DS.'lib'.DS.'recaptcha'.DS.'recaptchalib.php');
	  global $Itemid;

	  wallHelper::checkGuestView();
		
	  $session 		=& JFactory::getSession ();
	  $user 		=& JFactory::getUser ();

		$posts 		=& $this->get ( 'Data' );
		
		$comments 	=& $this->get ( 'Comments' );
		$media 		=& $this->get ( 'Media' ); 
    	$urls 		=& $this->get ( 'Urls' ); 
	
		$pagination =& $this->get ( 'Pagination' );
		$settings 	=& $this->get ( 'Settings' );
		$my_wall 	=& $this->get ( 'MyWall' );
		$count  	=& $this->get ( 'TotalPosts' );
		
		$this->assignRef ( 'posts', 			$posts );
		$this->assignRef ( 'comments', 			$comments );
		$this->assignRef ( 'media',   			$media);
    	$this->assignRef ( 'urls',   			$urls);
		$this->assignRef ( 'pagination', 		$pagination );
		$this->assignRef ( 'user', 				$user );
		$this->assignRef ( 'my_wall', 			$my_wall );
		$this->assignRef ( 'Itemid', 			$Itemid );
		$this->assignRef ( 'settings', 			$settings );
		$this->assignRef ( 'wallSettings', 		$this->wallSettings );
		$this->assignRef ( 'session', 			$session );
		$this->assignRef ( 'root', 				JUri::root () );
		$this->assignRef ( 'count',      	  	$count);
		$this->assignRef ( 'allow_guest_comments', $this->wallSettings->allow_guest_comments );
	//	$this->assignRef ( 'captcha_comment', 	$this->wallSettings->captcha_comment );
		

    
    if ($this->wallSettings->enable_bookmarks)
    {
      $bookmarks =& $this->get('Bookmarks');
      $this->assignRef('bookmarks', $bookmarks);
      
      $video_bookmarks =& $this->get('VideoBookmarks');
      $this->assignRef('video_bookmarks', $video_bookmarks);
      
      $url_bookmarks =& $this->get('UrlBookmarks');
      $this->assignRef('url_bookmarks', $url_bookmarks);
      
      $mp3_bookmarks =& $this->get('Mp3Bookmarks');
      $this->assignRef('mp3_bookmarks', $mp3_bookmarks);
      
      $file_bookmarks =& $this->get('FileBookmarks');
      $this->assignRef('file_bookmarks', $file_bookmarks);
      
      $image_bookmarks =& $this->get('ImageBookmarks');
      $this->assignRef('image_bookmarks', $image_bookmarks);
      
    }
		
		JHTML::stylesheet ( 'posts.css', 'components/com_wallfactory/assets/css/views/' );
		JHTML::stylesheet ( 'buttons.css', 'components/com_wallfactory/assets/css/' );
		JHTML::stylesheet ( 'main.css', 'components/com_wallfactory/assets/css/' );
		JHTML::stylesheet ( 'jquery.notice.css', 'components/com_wallfactory/assets/css/' );
		JHTML::stylesheet ( 'jquery-ui-1.7.2.custom.css', 'components/com_wallfactory/assets/css/' );
		JHTML::stylesheet ( 'writepost.css', 'components/com_wallfactory/assets/css/' );
		JHTML::stylesheet ( 'prettyPhoto.css', 'components/com_wallfactory/assets/css/' ); //media="screen"
		
		JHtmlFactory::jQueryScript ();
		JHTML::script ( 'latest.js', 'components/com_wallfactory/assets/js/views/posts/' );
		
		JHtmlFactory::jQueryScript ( 'cookie', 'ui', 'ui-widget', 'ui-mouse', 'ui-sortable' );
		JHtmlFactory::jQueryScript ( 'swfupload', 'ui', 'ui-widget', 'ui-mouse', 'ui-position', 'ui-draggable', 'ui-dialog', 'ui-tabs', 'ui-progressbar' );
		JHTML::script ( 'swfupload.js', 'components/com_wallfactory/assets/js/' );
		
		JHTML::_ ( 'script', 'main.js', 'components/com_wallfactory/assets/js/' );
		JHTML::_ ( 'script', 'comments.js', 'components/com_wallfactory/assets/js/' );
		JHTML::_ ( 'script', 'jquery.prettyPhoto.js', 'components/com_wallfactory/assets/js/' );  
		
		$declarations1 = array (
			'root' 					=> JUri::root() ,
			'route_upload_image' 	=> JRoute::_ ( 'index.php?option=com_wallfactory&controller=upload&task=uploadImage' ), 
			'route_upload_mp3' 		=> JRoute::_ ( 'index.php?option=com_wallfactory&controller=upload&task=uploadMp3' ), 
			'route_upload_file' 	=> JRoute::_ ( 'index.php?option=com_wallfactory&controller=upload&task=uploadFile' ), 
			'PHPsessionID' 			=> session_id (), 
	  		'sessionName' 			=> $session->getName (), 
	  		'sessionId' 			=> $session->getId () ,
		);
		
		wallHelper::addScriptDeclarations ( $declarations1 );
		
		if (! is_null ( $posts )) {
			$declarations2 = array (
			'txt_field_required'    => JText::_('This field is required'),
			'route_post_comment' 	=> JRoute::_ ( 'index.php?option=com_wallfactory&controller=comment&task=addComment' ), 
			'route_report_comment' => JRoute::_ ( 'index.php?option=com_wallfactory&controller=comment&task=report' ), 
			'txt_loading_comments' => JText::_ ( 'Loading comments...' ),
			'is_guest'			=> $user->guest,
			'captcha_comment' => $this->wallSettings->captcha_comment 
			);
			
			wallHelper::addScriptDeclarations ( $declarations2 );
		}
		
      if (!$user->guest) {
		$document =& JFactory::getDocument();
	
		$href1 = JRoute::_('index.php?option=com_wallfactory&view=feed&type=rss&format=feed&content=entries'); 
		$href2 = JRoute::_('index.php?option=com_wallfactory&view=feed&type=rss&format=feed&content=comments'); 
		
		$attribs1 = array('type' => 'application/rss+xml', 'title' => 'RSS Feed Entries');
		$attribs2 = array('type' => 'application/rss+xml', 'title' => 'RSS Feed Comments'); 
		$document->addHeadLink( $href1, 'alternate', 'rel', $attribs1 );
		$document->addHeadLink( $href2, 'alternate', 'rel', $attribs2 );

      }
	  
      $this->_display ( $tpl );
	}
		
	function _display($tpl) {
		
		$this->_layout = 'latest';
		parent::display ( $tpl );
	
	}
}