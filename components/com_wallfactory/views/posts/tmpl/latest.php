<?php
/*------------------------------------------------------------------------
com_wallfactory - Wall Factory
------------------------------------------------------------------------
author    TheFactory
copyright Copyright (C) 2010 SKEPSIS Consult SRL. All Rights Reserved.
@license - http://www.gnu.org/licenses/gpl-2.0.html GNU/GPL
Websites: http://www.thefactory.ro
Technical Support:  Forum - http://www.thefactory.ro/joomla-forum/
-------------------------------------------------------------------------*/

defined('_JEXEC') or die('Restricted access'); ?>

<div id="loading-bar" class="wallfactory-none"><?php echo JText::_('Loading...'); ?></div>

<h1><?php echo JText::_('News Feed'); ?></h1>

<div class="wallfactory-table" id="wallfactory-posts">
  <?php require_once('_list.php'); ?>
</div>