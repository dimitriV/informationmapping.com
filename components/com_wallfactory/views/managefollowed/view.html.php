<?php
/*------------------------------------------------------------------------
com_wallfactory - Wall Factory
------------------------------------------------------------------------
author    TheFactory
copyright Copyright (C) 2010 SKEPSIS Consult SRL. All Rights Reserved.
@license - http://www.gnu.org/licenses/gpl-2.0.html GNU/GPL
Websites: http://www.thefactory.ro
Technical Support:  Forum - http://www.thefactory.ro/joomla-forum/
-------------------------------------------------------------------------*/

defined('_JEXEC') or die('Restricted access');

jimport('joomla.application.component.view');

class FrontendViewManageFollowed extends JView
{
  function __construct()
  {
    parent::__construct();

    $this->wallHelper   = wallHelper::getInstance();
    $this->wallSettings = new wallSettings();
  }

  function display($tpl = null)
  {
    global $Itemid;

    wallHelper::checkGuestView(true);

    $user       	=& JFactory::getUser();
    $walls      	=& $this->get('Data');
    
    $pagination 	=& $this->get('Pagination');
    $session    	=& JFactory::getSession();
    $settings   	=& $this->get('Settings');
    
    $latest_post   	=& $this->get('LatestPosts');
    $latest_media  	=& $this->get('Media');
    $latest_urls   	=& $this->get('Urls');
    $count  		=& $this->get('TotalPosts');

    $this->assignRef('walls',         $walls);
    $this->assignRef('pagination',    $pagination);
    $this->assignRef('Itemid',        $Itemid);
    $this->assignRef('settings',      $settings);
    $this->assignRef('latest_post',   $latest_post);
    $this->assignRef('latest_media',  $latest_media);
    $this->assignRef('latest_urls',   $latest_urls);
    $this->assignRef('count',      	  $count);
    $this->assignRef('notification',  $session->get('filter.followed.notification', ''));
    $this->assignRef('notifications', $this->wallSettings->enable_notification_follow_wall);
    $this->assignRef('root',          JUri::root());

    JHTML::stylesheet('jquery-ui-1.7.2.custom.css', 'components/com_wallfactory/assets/css/');
    JHTML::stylesheet('buttons.css',                'components/com_wallfactory/assets/css/');
    JHTML::stylesheet('main.css',                   'components/com_wallfactory/assets/css/');
    JHTML::stylesheet('writepost.css',              'components/com_wallfactory/assets/css/');
    JHTML::stylesheet('prettyPhoto.css', 			'components/com_wallfactory/assets/css/' ); //media="screen"

    JHtmlFactory::jQueryScript();
    JHTML::script('default.js', 'components/com_wallfactory/assets/js/views/managefollowed/');
    JHTML::_ ( 'script', 'jquery.prettyPhoto.js', 'components/com_wallfactory/assets/js/' );  

    $declarations = array(
      'Itemid'                             => $Itemid,
      'txt_select_at_least_one_item'       => JText::_('select at least one item'),
      'txt_select_the_action'              => JText::_('select the action'),
      'txt_confirm'                        => JText::_('Are you sure you want to delete these followed walls?'),
      'txt_remove_notification'            => JText::_('remove notification'),
      'txt_add_notification'               => JText::_('add notification'),
      'txt_added'                          => JText::_('added'),
      'txt_removed'                        => JText::_('removed'),
      'route_notification'                 => JRoute::_('index.php?option=com_wallfactory&controller=managefollowed&task=notification'),
      'txt_notification_has_been'          => JText::_('notification has been'),
      'route_delete'                       => JRoute::_('index.php?option=com_wallfactory&controller=managefollowed&task=delete'),
      'txt_followed_wall_has_been_removed' => JText::_('followed wall has been removed'),
      'route_filter'                       => JRoute::_('index.php?option=com_wallfactory&controller=managefollowed&task=filter'),
      'route_load_walls'                   => JRoute::_('index.php?option=com_wallfactory&view=managefollowed'),
      'route_submit'                       => JRoute::_('index.php?option=com_wallfactory&controller=managefollowed&task=executeBatch'),
    );
    wallHelper::addScriptDeclarations($declarations);

    $this->_display($tpl);
  }


  function _display($tpl)
  {
      parent::display($tpl);
  }
}