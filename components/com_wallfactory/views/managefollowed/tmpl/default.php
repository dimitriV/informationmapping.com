<?php
/*------------------------------------------------------------------------
com_wallfactory - Wall Factory
------------------------------------------------------------------------
author    TheFactory
copyright Copyright (C) 2010 SKEPSIS Consult SRL. All Rights Reserved.
@license - http://www.gnu.org/licenses/gpl-2.0.html GNU/GPL
Websites: http://www.thefactory.ro
Technical Support:  Forum - http://www.thefactory.ro/joomla-forum/
-------------------------------------------------------------------------*/

defined('_JEXEC') or die('Restricted access'); ?>

<h1><?php echo JText::_('Manage Followed Walls'); ?></h1>

<div id="loading-bar" class="wallfactory-none"><?php echo JText::_('Loading...'); ?></div>

<div style="height: 30px; text-align: center; ">
  <span id="action-message" class="wallfactory-none"></span>
</div>

<form action="<?php echo $this->root; ?>index.php" method="POST" id="form">

  <div id="followed">
    <?php require_once('_list.php'); ?>
  </div>

  <div class="actions">
    <div class="wallfactory-left">
      <select id="task" name="task">
        <option value=""></option>
        <?php if ($this->notifications): ?>
          <option value="batchAddNotification"><?php echo JText::_('Add notification'); ?></option>
          <option value="batchRemoveNotification"><?php echo JText::_('Remove notification'); ?></option>
        <?php endif; ?>
        <option value="batchDelete"><?php echo JText::_('Delete'); ?></option>
      </select>
      <input type="submit" value="<?php echo JText::_('go'); ?>" />
    </div>

    <?php if ($this->notifications): ?>
    <div class="wallfactory-right">
      <select id="notification" name="notification">
        <option value="" <?php echo ($this->notification == '') ? 'selected="selected"' : ''; ?>></option>
        <option value="with" <?php echo ($this->notification == 'with') ? 'selected="selected"' : ''; ?>><?php echo JText::_('With notification'); ?></option>
        <option value="without" <?php echo ($this->notification == 'without') ? 'selected="selected"' : ''; ?>><?php echo JText::_('Without notification'); ?></option>
      </select>
      <input type="button" value="<?php echo JText::_('filter'); ?>" id="submit-filter" />
    </div>
    <?php endif; ?>
  </div>

  <input type="hidden" name="ids" id="ids" value="" />
  <input type="hidden" name="option" value="com_wallfactory" />
  <input type="hidden" name="controller" value="managecomment" />
  <input type="hidden" name="Itemid" value="<?php echo $this->Itemid; ?>" />
</form>

<div id="loading-bar" class="wallfactory-none"><?php echo JText::_('Loading...'); ?></div>

<div class="wallfactory-clear"></div>