<?php
/*------------------------------------------------------------------------
com_wallfactory - Wall Factory
------------------------------------------------------------------------
author    TheFactory
copyright Copyright (C) 2010 SKEPSIS Consult SRL. All Rights Reserved.
@license - http://www.gnu.org/licenses/gpl-2.0.html GNU/GPL
Websites: http://www.thefactory.ro
Technical Support:  Forum - http://www.thefactory.ro/joomla-forum/
-------------------------------------------------------------------------*/

defined('_JEXEC') or die('Restricted access'); ?>

<form action="<?php echo $this->root; ?>index.php" method="POST" id="form">
<table class="table">
  <thead class="ui-widget-header">
    <tr>
      <th class="checkbox"><input type="checkbox" id="checkAll" /></th>
      <th style="width: 60px;"><?php echo JText::_('Avatar'); ?></th>
      <th><?php echo Jtext::_('User'); ?></th>
      <th style="width: 100px; text-align: center;"><?php echo Jtext::_('Friend since'); ?></th>
      <th style="width: 10%; text-align: center;"><?php echo Jtext::_('Posts'); ?></th>
      <th style="width: 10%; text-align: center;"><?php echo Jtext::_('Notification'); ?></th>
      <th style="width: 10%; text-align: center;"><?php echo Jtext::_('Remove'); ?></th>
    </tr>
  </thead>

  <tfoot class="ui-widget-header">
    <tr>
      <th colspan="8" id="pagination">
        <?php echo $this->pagination->getPagesLinks(); ?>
      </th>
    </tr>
  </tfoot>

  <tbody>
    <?php foreach ($this->walls as $i => $wall): ?>
      <tr id="followed_<?php echo $wall->id; ?>" class="<?php echo ($i % 2) ? 'even' : ''; ?> <?php echo ($this->notifications && $wall->notification) ? '' : 'unapproved'; ?>">
        <td rowspan="2" class="checkbox"><input type="checkbox" class="checkbox" name="followed[<?php echo $wall->id; ?>]" /></td>
        <td rowspan="2" style="width: 60px;">
        	<a href="<?php echo JRoute::_('index.php?option=com_wallfactory&Itemid=' . $this->Itemid . '&view=wall&alias=' . $wall->alias); ?>" class="alias-margin">
        		<img src="<?php echo JURI::root(); ?>components/com_wallfactory/storage/users/<?php echo $wall->user_id; ?>/avatar/avatar.<?php echo $this->settings->avatar_extension; ?>?<?php echo time(); ?>" 
        	class="guest-avatar" alt="avatar" /></img></a>
        </td>
        <td><a href="<?php echo WallRoute::_('wall', $wall); ?>"><b>
        <?php ($this->wallSettings->wall_title_display == 0) ? ($name =  $wall->name) : ($name =  $wall->alias);
	          			echo $name; ?></b> </a>
        </td>
       
        <td style="width: 100px; text-align: center;"><?php echo date('d.m.Y', JText::_(strtotime( $wall->date_created))); ?></td>
        <td style="text-align: center;"><?php echo $this->count[$wall->user_id]; ?></td>
        <td style="text-align: center;">
        	<span style="padding: 3px 3px 2px;">
  			<?php  if ($this->notifications): ?>
			  	<?php $path = JURI::root().'components/com_wallfactory/assets/images/'; ?>
				<a href="#" class="notification"><?php echo $wall->notification ? '<img src="'.$path.'cancel.png' .'" border="0" alt="remove notification" />' : '<img src="'.$path.'cancel.png' .'" border="0" alt="add notification" />'; ?></a>
			<?php endif; ?>
  			</span>
  		</td>
  		<td style="text-align: center;"><a href="#" class="delete"><img src="<?php echo $path.'delete.png'; ?>" border="0" alt="delete friend" /><?php //echo JText::_('Delete'); ?></a></td>
      </tr>
      <tr>
      	
      	<td colspan="5">
      	 <div style="margin-top: 1px;" >
          	<b><?php echo Jtext::_('Latest post on date '); ?></b>
          		 <span class="wallfactory-year wallfactory-action"><?php echo date('H:m:s a l, d F Y', JText::_(strtotime($this->latest_post["$wall->id"]['date_created']))); ?></span>
          </div>
      	</td>
      </tr>
      <tr>
      	<td>&nbsp;</td>
      	<td colspan="6">
      
      	<div class="managefollowed">
      	<?php $for_post = $this->latest_post["$wall->id"]['id']; ?>
     		
           	<div style="color: #fff padding-top: 2px;">
           		<div style="padding: 5px 0 5px 5px; line-height: 110%; font-size: 12px; background-color:#fff;"><?php echo nl2br(html_entity_decode($this->latest_post["$wall->id"]['content'])); ?></div>
       		   	<div class="wallfactory-media-container">
           
				<!--  display attached media for the post  -->
		        <div id="wallfactory-your-media<?php echo $wall->id; ?>">
				    <?php if (!empty($this->latest_urls[$for_post][0]) ){ ?>
	          			<div>
          		  		<?php  
				     	foreach ( $this->latest_urls[$for_post] as $link) { 
				     
				     		if (  $link->url != '' ):  
							   	if (isset($link->url_title) && $link->url_title != "") 
								 	$url_title = $link->url_title;
								else 
									$url_title = $link->url;
							     	
							   	if (isset($link->url_description)) 
							  		$url_description = nl2br(html_entity_decode($link->url_description));
							   	else 
							 		$url_description = '';
							     ?>
							     <div class="round_media">	
								   	<img alt="url" title="url" src="<?php echo JURI::root(); ?>components/com_wallfactory/assets/images/world_link.png">
								   	<span class="media_title"><a target="_blank" href="<?php echo $link->url; ?>"><?php echo $url_title; ?></a>
								   	
								    <p style="padding: 5px 5px;"><?php echo $url_description; ?></p>
								 </div>	
						  	<?php endif; ?>
					  		<!-- video -->
						 	<?php  if (  $link->video_link != '' ):  
								$thumb_source = ($link->video_thumbnail != "") ? $link->video_thumbnail : $link->video_sourceThumb;
								if (isset($link->video_title) && $link->video_title != "") 
								 	$video_title = stripslashes($link->video_title);
								else 
									$video_title = $link->video_link;
					     	
					     		if (isset($link->video_description)) 
					     		 	$video_description = nl2br(html_entity_decode(stripslashes($link->video_description)));
					     		else 
					     			$video_description = '';
					     		
						 		?>
							  	<div class="round_media">
									<?php if (strpos($link->video_link,'youtube')): 	?>	
									<table width="100%" cellspacing="0" cellpadding="0" border="0">
										<tr>
										  <td>
											<div class="media_thumbnail"> 
											  <a title="<?php echo $link->video_title; ?>" rel="prettyPhoto" onclick="jQueryFactory.prettyPhoto.open('<?php echo $link->video_link; ?>', '', '')" href="javascript:void(0);">
												<img src="<?php echo $thumb_source; ?>" alt="YouTube" class="media_width" /><br /> 
											  </a>
											</div>
										  </td>
										
										  <td>
											<div class="video_content">
												<span class="media_title"><?php echo strip_tags($video_title, '<a>'); ?></span>
							    				<span class="media_description"><?php echo strip_tags($video_description, '<a>');  ?></p>	
											</div>	
										  </td>
										</tr>	
									</table>	
									<?php endif; ?>	
									
									<?php if (strpos($link->video_link,'vimeo')): 	?>	
									<table width="100%" cellspacing="0" cellpadding="0">
										<tr>
										  <td>
											<div class="media_thumbnail">
											  <a title="<?php echo $link->video_title; ?>" onclick="jQueryFactory.prettyPhoto.open('<?php echo $link->video_link; ?>', '', '')" href="javascript:void(0);">
												<img src="<?php echo $thumb_source; ?>" alt="<?php echo $link->video_sitename; ?>" class="media_width" /><br />
	 										  </a>
											</div>
										  </td>
										  <td>
											<div class="video_content">
												<span class="media_title"><?php echo strip_tags($video_title, '<a>'); ?></span>
						    					<p style="padding: 5px 5px 4px 4px;"><?php echo strip_tags($video_description, '<a>'); ?></p>	
											</div>		
										  </td>
										</tr>	
									</table>	
									<?php endif; ?>
								
									<?php if (strpos($link->video_link,'metacafe')): ?>	
									<table width="100%" cellspacing="0" cellpadding="0">
										<tr>
										  <td>
											<div class="media_thumbnail">
												<a href="<?php echo $link->video_link; ?>?width=70%&amp;height=70%" rel="prettyPhoto" title="<?php echo $link->video_title; ?>">
												<img  src="<?php echo $thumb_source; ?>" class="media_width" alt="<?php echo $link->video_title; ?>"></a>
											</div>
										  </td>
																	
										  <td>			
											<div class="video_content">
												<span class="media_title"><?php echo strip_tags($video_title, '<a>'); ?></span>
								    			<div class="video-description"><?php echo strip_tags($video_description, '<a>'); ?></div>	
											</div>	
										  </td>
										</tr>	
									</table>		
									<?php endif; ?>

									<?php if (strpos($link->video_link,'myspace')): ?>	
									<table width="100%" cellspacing="0" cellpadding="0">
										<tr>
										   <td>
											<div class="media_thumbnail">
											  <a title="<?php echo $link->video_title; ?>"  onclick="jQueryFactory.prettyPhoto.open('<?php echo $link->video_link; ?>', '', '')" href="javascript:void(0);">
												<img src="<?php echo $thumb_source; ?>" alt="<?php echo $link->video_sitename; ?>" class="media_width" /><br />
											  </a>
											</div>
										  </td>
							
										  <td>		
											<div class="video_content">
												<span class="media_title"><?php echo strip_tags($video_title, '<a>'); ?></spanp>
								    			<div class="video-description"><?php echo strip_tags($video_description, '<a>'); ?></div>	
											</div>	
										  </td>
										</tr>	
									</table>			
									<?php endif; ?>		
									<?php if (strpos($link->video_link,'howcast')): ?>	
									<table width="100%" cellspacing="0" cellpadding="0">
										<tr>
										  <td>
											<div class="media_thumbnail">
												<a href="<?php echo $link->video_link; ?>?width=500&height=350" rel="prettyPhoto" title="<?php echo $link->video_title; ?>">
												<img  src="<?php echo $link->video_sourceThumb; ?>" class="media_width" alt="<?php echo $link->video_title; ?>"></a>
											</div>	
										  </td>
										  <td>
											<div class="video_content">
												<span class="media_title"><?php echo strip_tags($video_title, '<a>'); ?></span>
								    			<div class="video-description"><?php echo strip_tags($video_description, '<a>'); ?></div>	
											</div>	
										  </td>
										</tr>	
									</table>		
									<?php endif; ?>		
						      	</div>  <!-- end round videobox -->    
								<br /><br />
							<?php endif; ?> <!-- end video link -->  
			          	<?php 
				     }  // end foreach link
				     ?>
			      	
		      	</div> <!-- class="wallfactory-text" -->
				<?php } // end exista links  ?>	
						
			   <?php if (!empty($this->latest_media[$for_post][0]) ) {
 	          			foreach ( $this->latest_media[$for_post] as $m) {    
							switch ($m->folder) {
									
							case 'images': 
					    ?>
						<div class="round_media">
				           	<?php  $filename = JPATH_SITE.DS.'components'.DS.'com_wallfactory'.DS.'storage'.DS.'users'.DS.$m->user_id.DS.$m->folder.DS.$m->name.'.'.$m->extension;
								
				           		if (file_exists($filename)) {
				           			$size = getimagesize($filename);
									$max_size = 80;
									if ($size[0] > $max_size || $size[1] > $max_size)  {
									    if ($size[0] > $size[1]) {
									        $new_width  = $max_size;
									        $new_height = round($max_size * $size[1] / $size[0],0);
									    }
									    else
									    {
									        $new_width  = round($max_size * $size[0] / $size[1],0);
									        $new_height = $max_size;
									    }
									}
									else
									{
									    $new_width  = $size[0];
									    $new_height = $size[1];
									}
								} 
								?>
								<table width="100%" cellspacing="0" cellpadding="0">
									<tr>
									   <td class="media_width">
					            		<div class="media_width wallfactory-left">
											<a title="name" onclick="jQueryFactory.prettyPhoto.open('<?php echo JURI::root(); ?>components/com_wallfactory/storage/users/<?php echo $m->user_id;?>/<?php echo $m->folder;?>/<?php echo $m->name; ?>.<?php echo $m->extension; ?>','<?php echo $m->name; ?>');" href="javascript:void(0);">
					            				<img src="<?php echo JURI::root(); ?>components/com_wallfactory/storage/users/<?php echo $m->user_id;?>/<?php echo $m->folder;?>/<?php echo $m->name; ?>.<?php echo $m->extension; ?>" 
											rel="prettyPhoto" style="width: <?php echo $new_width;?>px; height: <?php echo $new_height;?>px; cursor: pointer; padding: 3px;"><br />
										 	</a>
										</div>
									  </td>
									  
									  <td>
										<div class="wallfactory-left">
											<?php if (isset($m->title)) 
									     		 	$m_title = nl2br(html_entity_decode($m->title));
									     	  else 
									     			$m_title = '';
					
									     	  if (isset($m->description)) 
									     		 	$m_description = nl2br(html_entity_decode($m->description));
									     	  else 
									     			$m_description = '';	
										   	?>
											<span class="media_title"><?php echo $m_title; ?></span> <br />
											<span class="media_description"><?php echo $m_description; ?></span><br />
										</div>
									 </td>
									</tr>	
								</table>	
								
						  </div>
						<br /><br />
						<?php  break;
							case 'mp3':
						?>
						    <div class="round_media">
					          <?php $link = JURI::root().'components/com_wallfactory/storage/users/'.$m->user_id.'/'.$m->folder.'/'.$m->name.'.'.$m->extension; ?>
					       
							    <img alt="Mp3" title="Mp3" src="<?php echo JURI::root(); ?>components/com_wallfactory/assets/images/sound.png" style="padding: 1px; background-repeat: no-repeat;">   
						   		<span class="media_title"><?php echo $m->title; ?></span>
						   		    
							    <div class="media_file">
							        <object classid="clsid:D27CDB6E-AE6D-11cf-96B8-444553540000" width="220" height="25" id="flashContent"> 
							        <param name="movie" value="<?php echo JURI::root(); ?>components/com_wallfactory/assets/swfs/player.swf" /> 
							        <param name="FlashVars" value="playerID=1&amp;noinfo=yes&amp;bg=#ffffff&amp;soundFile=<?php echo $link; ?>" /> 
							        <param name="wmode" value="transparent" /> 
							        <param name="quality" value="high">
							        <param name="menu" value="true">
							        <!--[if !IE]>--> 
							        <object type="application/x-shockwave-flash" data="<?php echo JURI::root(); ?>components/com_wallfactory/assets/swfs/player.swf" width="177" height="20"> 
							        <param name="FlashVars" value="playerID=1&amp;noinfo=yes&amp;bg=#ffffff&amp;soundFile=<?php echo $link; ?>" /> 
							        <param name="wmode" value="transparent" />
							        <param name="quality" value="high">
									<param name="menu" value="true"> 
							        <!--<![endif]--> 
							        <a href="<?php echo $link; ?>" class="wallfactory-text">Play Audio</a> 
							        <!--[if !IE]>--> 
							        </object> 
							        <!--<![endif]--> 
							        </object>
						        </div>
						  </div>  
				         <br />  	
						<?php  break;
							case 'files': ?>
							<?php  $link = JURI::root().'components/com_wallfactory/storage/users/'.$m->user_id.'/'.$m->folder.'/'.$m->name.'.'.$m->extension; ?>
					    <?php if (file_exists ($link)): ?>
		            		<div class="round_media">
		            			<span class="wallfactory-button wallfactory-file" >
		            				<span class="media_title"><?php echo $m->name; ?>
					     	  	</span>	
                   				
			            	  </span>    
							  <div class="media_file">
							  	<img alt="file" title="file" src="<?php echo JURI::root(); ?>components/com_wallfactory/assets/images/arrow_down.png">
								<?php require_once(JPATH_SITE.DS.'components'.DS.'com_wallfactory'.DS.'download.php'); ?>
							  </div>
							  
					    	</div>
						    <?php endif; ?>
						    
                   			 <?php 
								break; ?>
							<?php }
								}	
							}
							?>
						</div>
			
				</div>
				 <div class="wallfactory-clear"></div>
				
       		</div>
     </div>
      	</td>
      </tr>
      
    <?php endforeach; ?>
  </tbody>
</table>
  <input type="hidden" name="ids" id="ids" value="" />
  <input type="hidden" name="option" value="com_wallfactory" />
  <input type="hidden" name="controller" value="managecomment" />
  <input type="hidden" name="Itemid" value="<?php echo $this->Itemid; ?>" />
</form>

<script>
 var root = "<?php echo JUri::root(); ?>";
</script>

<script>
jQueryFactory(document).ready(function ($) {
	
	$("a[rel^='prettyPhoto']").prettyPhoto( {
		theme: 'facebook',
		markup: '<div class="pp_pic_holder"> \
						<div class="ppt">&nbsp;</div> \
						<div class="pp_top"> \
							<div class="pp_left"></div> \
							<div class="pp_middle"></div> \
							<div class="pp_right"></div> \
						</div> \
						<div class="pp_content_container"> \
							<div class="pp_left"> \
							<div class="pp_right"> \
								<div class="pp_content"> \
									<div class="pp_loaderIcon"></div> \
									<div class="pp_fade"> \
										<a href="#" class="pp_expand" title="Expand the image">Expand</a> \
										<div class="pp_hoverContainer"> \
											<a class="pp_next" href="#">next</a> \
											<a class="pp_previous" href="#">previous</a> \
										</div> \
										<div id="pp_full_res"></div> \
										<div class="pp_details clearfix"> \
											<p class="pp_description"></p> \
											<a class="pp_close" href="#">Close</a> \
											<div class="pp_nav"> \
												<a href="#" class="pp_arrow_previous">Previous</a> \
												<p class="currentTextHolder">0/0</p> \
												<a href="#" class="pp_arrow_next">Next</a> \
											</div> \
										</div> \
									</div> \
								</div> \
							</div> \
							</div> \
						</div> \
						<div class="pp_bottom"> \
							<div class="pp_left"></div> \
							<div class="pp_middle"></div> \
							<div class="pp_right"></div> \
						</div> \
					</div> \
					<div class="pp_overlay"></div>'
	});
});
</script>