<?php
/*------------------------------------------------------------------------
com_wallfactory - Wall Factory
------------------------------------------------------------------------
author    TheFactory
copyright Copyright (C) 2010 SKEPSIS Consult SRL. All Rights Reserved.
@license - http://www.gnu.org/licenses/gpl-2.0.html GNU/GPL
Websites: http://www.thefactory.ro
Technical Support:  Forum - http://www.thefactory.ro/joomla-forum/
-------------------------------------------------------------------------*/

defined('_JEXEC') or die('Restricted access');

jimport('joomla.application.component.view');

class FrontendViewManageFollowed extends JView
{
  function __construct()
  {
    parent::__construct();

    $this->wallHelper   = wallHelper::getInstance();
    $this->wallSettings = new wallSettings();
  }

  function display($tpl = null)
  {
    global $Itemid;

    $walls      =& $this->get('Data');
    $pagination =& $this->get('Pagination');

    $settings   =& $this->get('Settings');
    
    $latest_post   =& $this->get('LatestPosts');
    $latest_media  =& $this->get('Media');
    $latest_urls   =& $this->get('Urls');
    $count  		=& $this->get('TotalPosts');
    
    
    $this->assignRef('walls',         $walls);
    $this->assignRef('pagination',    $pagination);
    $this->assignRef('settings',      $settings);
    $this->assignRef('latest_post',   $latest_post);
    $this->assignRef('latest_media',  $latest_media);
    $this->assignRef('latest_urls',   $latest_urls);
    $this->assignRef('count',      	  $count);
    $this->assignRef('notifications', $this->wallSettings->enable_notification_follow_wall);
    $this->assignRef('Itemid',        $Itemid);

    $this->_layout = '_list';

    $this->_display($tpl);
  }


  function _display($tpl)
  {
     parent::display($tpl);
  }
}