<?php
/*------------------------------------------------------------------------
com_wallfactory - Wall Factory
------------------------------------------------------------------------
author    TheFactory
copyright Copyright (C) 2010 SKEPSIS Consult SRL. All Rights Reserved.
@license - http://www.gnu.org/licenses/gpl-2.0.html GNU/GPL
Websites: http://www.thefactory.ro
Technical Support:  Forum - http://www.thefactory.ro/joomla-forum/
-------------------------------------------------------------------------*/

defined('_JEXEC') or die('Restricted access'); ?>

<h1><?php echo JText::_('My wall Followers'); ?></h1>

<div id="followers">
  <?php require_once('_list.php'); ?>
</div>

<div id="loading-bar" class="wallfactory-none"><?php echo JText::_('Loading...'); ?></div>

<div class="wallfactory-clear"></div>