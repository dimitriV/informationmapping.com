<?php
/*------------------------------------------------------------------------
com_wallfactory - Wall Factory
------------------------------------------------------------------------
author    TheFactory
copyright Copyright (C) 2010 SKEPSIS Consult SRL. All Rights Reserved.
@license - http://www.gnu.org/licenses/gpl-2.0.html GNU/GPL
Websites: http://www.thefactory.ro
Technical Support:  Forum - http://www.thefactory.ro/joomla-forum/
-------------------------------------------------------------------------*/

defined('_JEXEC') or die('Restricted access'); ?>

<table class="table">
  <thead class="ui-widget-header">
    <tr>
      <th><?php echo Jtext::_('Username'); ?></th>
      <th><?php echo Jtext::_('Latest post'); ?></th>
      <th style="width: 120px;"><?php echo Jtext::_('Following since'); ?></th>
      <th><?php echo Jtext::_('Posts'); ?></th>
    </tr>
  </thead>

  <tfoot class="ui-widget-header">
    <tr>
      <th colspan="4" id="pagination">
        <?php echo $this->pagination->getPagesLinks(); ?>
      </th>
    </tr>
  </tfoot>

  <tbody>
    <?php foreach ($this->followers as $i => $follower): ?>
      <tr id="comment_<?php echo $follower->id; ?>" class="<?php echo ($i % 2) ? 'even' : ''; ?>">
        <td width="25%"><b><a href="<?php echo JRoute::_('index.php?option=com_wallfactory&Itemid=' . $this->Itemid . '&view=wall&alias=' . $follower->wall_alias); ?>" style="display: block; margin-right: 8px;">
        <?php echo $follower->username; ?></a></b></td>
        <td><?php if (isset($this->lastpost[$follower->user_id])): ?>
         	<a href="<?php echo JRoute::_('index.php?option=com_wallfactory&Itemid=' . $this->Itemid . '&view=post&id='.$this->lastpost[$follower->user_id][0]['post_id'].'&alias=' . $follower->wall_alias); ?>" class="alias-font">	
          				<?php echo JText::_('View latest post'); ?>	
          		</a> 
          		
          		<span><?php echo html_entity_decode($this->lastpost[$follower->user_id][0]["post_content"]); ?></span>
	           <div class="wallfactory-right">
				 <span class="wallfactory-year wallfactory-action"><?php echo date('H:m a, d.m.Y', JText::_(strtotime($this->lastpost[$follower->user_id][0]['date_created']))); ?></span>
			   </div>
        	<?php else: ?>
       		<?php echo Jtext::_('No posts fom this user.'); ?>
       	
       	<?php endif; ?>	
        </td>
        <td width="10%"><span class="wallfactory-year wallfactory-action"><?php echo date('d.m.Y', JText::_(strtotime($follower->date_created))); ?></span></td>
        <td width="10%" style="text-align: right;"><span class="wallfactory-action"><?php echo $this->count[$follower->user_id]; ?></span></td>
      </tr>
     
    <?php endforeach; ?>
  </tbody>
</table>