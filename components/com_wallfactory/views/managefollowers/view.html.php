<?php
/*------------------------------------------------------------------------
com_wallfactory - Wall Factory
------------------------------------------------------------------------
author    TheFactory
copyright Copyright (C) 2010 SKEPSIS Consult SRL. All Rights Reserved.
@license - http://www.gnu.org/licenses/gpl-2.0.html GNU/GPL
Websites: http://www.thefactory.ro
Technical Support:  Forum - http://www.thefactory.ro/joomla-forum/
-------------------------------------------------------------------------*/

defined('_JEXEC') or die('Restricted access');

jimport('joomla.application.component.view');

class FrontendViewManageFollowers extends JView
{
  function __construct()
  {
    parent::__construct();

    $this->wallHelper   = wallHelper::getInstance();
    $this->wallSettings = new wallSettings();
  }

  function display($tpl = null)
  {
    global $Itemid;

    wallHelper::checkWallOwner();

    $user       =& JFactory::getUser();
    $followers  =& $this->get('Data');
    
    $lastpost 	=& $this->get('Lastpost');
	$count  	=& $this->get('TotalPosts');
    
    $pagination =& $this->get('Pagination');

    $this->assignRef('followers',  $followers);
    $this->assignRef('lastpost',   $lastpost);
    $this->assignRef('count',      $count);
    $this->assignRef('pagination', $pagination);
    $this->assignRef('Itemid',     $Itemid);
  

    JHTML::stylesheet('jquery-ui-1.7.2.custom.css', 'components/com_wallfactory/assets/css/');
    JHTML::stylesheet('buttons.css',                'components/com_wallfactory/assets/css/');
    JHTML::stylesheet('main.css',                   'components/com_wallfactory/assets/css/');

    JHtmlFactory::jQueryScript();
    JHTML::script('default.js', 'components/com_wallfactory/assets/js/views/managefollowers/');

    $this->_display($tpl);
  }
  
  function _display($tpl)
  {

      parent::display($tpl);
  }
}