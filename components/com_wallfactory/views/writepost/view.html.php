<?php
/*------------------------------------------------------------------------
com_wallfactory - Wall Factory
------------------------------------------------------------------------
author    TheFactory
copyright Copyright (C) 2010 SKEPSIS Consult SRL. All Rights Reserved.
@license - http://www.gnu.org/licenses/gpl-2.0.html GNU/GPL
Websites: http://www.thefactory.ro
Technical Support:  Forum - http://www.thefactory.ro/joomla-forum/
-------------------------------------------------------------------------*/

defined('_JEXEC') or die('Restricted access');

jimport('joomla.application.component.view');

class FrontendViewWritepost extends JView
{
  function __construct()
  {
    parent::__construct();

    $this->wallHelper   = wallHelper::getInstance();
    $this->wallSettings = new wallSettings();
  }

  function display($tpl = null)
  {
    global $Itemid;

    wallHelper::checkGuestView();
    wallHelper::checkWallOwner();
     
   	$post       =& $this->get('Data');
  	$image      =& $this->get('Image');
   	$mp3       	=& $this->get('Mp3');
   	$file       =& $this->get('File');
   	$links      =& $this->get('Links');
    $wall       =& $this->get('Wall');

   if (!is_null($post))
    {
      JRequest::setVar('id', $post->id, 'GET');
    }

    $session =& JFactory::getSession();
    $user    =& JFactory::getUser();
    
    $this->assignRef('post',       		$post);
    $this->assignRef('image',       	$image);
    $this->assignRef('mp3',       		$mp3);
    $this->assignRef('file',       	    $file);
    $this->assignRef('links',       	$links);
	$this->assignRef('wall',       		$wall);
 	$this->assignRef('settings',        $settings);
    $this->assignRef('wallSettings', 	$this->wallSettings);
    $this->assignRef('session',       	$session);
    $this->assignRef('root',          	JUri::root());
    $this->assignRef('Itemid',     		$Itemid);
	$this->assignRef('user',            $user);
	
    JHtmlFactory::jQueryScript();
    JHtmlFactory::jQueryScript('cookie', 'ui', 'ui-widget', 'ui-mouse', 'ui-sortable');
    JHtmlFactory::jQueryScript('swfupload', 'ui', 'ui-widget', 'ui-mouse', 'ui-position', 'ui-draggable', 'ui-dialog', 'ui-tabs', 'ui-progressbar');
	JHTML::script('swfupload.js', 'components/com_wallfactory/assets/js/');
     
    JHTML::_('script', 'main.js', 'components/com_wallfactory/assets/js/');
    JHTML::_('script', 'jquery.noconflict.js', 'components/com_wallfactory/assets/js/');
 	JHTML::_('script', 'comments.js', 'components/com_wallfactory/assets/js/');
      
    JHTML::script('default.js', 'components/com_wallfactory/assets/js/views/wall/');
	JHTML::script('_list.js', 'components/com_wallfactory/assets/js/views/wall/');
    
    $declarations = array(
      'wall_id'                         => isset($wall->id) ? $wall->id : 0,
      'txt_loading_comments'  			=> JText::_('Loading comments...'),
      'post_id'               			=> !is_null($post) ? $post->id : 0,
      'captcha_comment'       			=> $this->wallSettings->captcha_comment,
      'txt_you_are_following_this_wall' => JText::_('You are following this wall.'),
      'txt_unsubscribe'                 => JText::_('Unsubscribe'),
      'txt_follow_this_wall'            => JText::_('Follow this wall'),
      'route_follow_wall'               => JRoute::_('index.php?option=com_wallfactory&controller=wall&task=follow'),
      'route_post_comment'    			=> JRoute::_('index.php?option=com_wallfactory&controller=comment&task=addComment'),
      'route_load_comments'   			=> JRoute::_('index.php?option=com_wallfactory&view=comments&layout=_list&post_id=' . $post->id),
      'route_report_comment'  			=> JRoute::_('index.php?option=com_wallfactory&controller=comment&task=report'),
      'wall_alias'                      => $wall->alias,
      'root'                            => JURI::root(),
      'PHPsessionID' 					=> session_id (), 
	  'sessionName' 					=> $session->getName (), 
	  'sessionId' 						=> $session->getId () ,
    );
      
    wallHelper::addScriptDeclarations($declarations);
    
    JHTML::stylesheet('writepost.css',              'components/com_wallfactory/assets/css/');
    JHTML::stylesheet('wall.css',    'components/com_wallfactory/assets/css/views/');
    JHTML::stylesheet('jquery-ui-1.7.2.custom.css', 'components/com_wallfactory/assets/css/');
    JHTML::stylesheet('main.css',    'components/com_wallfactory/assets/css/');
    JHTML::stylesheet('posts.css',   'components/com_wallfactory/assets/css/views/');
    JHTML::stylesheet('buttons.css', 'components/com_wallfactory/assets/css/');
    
    parent::display($tpl);
  }

}