<?php
/*------------------------------------------------------------------------
com_wallfactory - Wall Factory
------------------------------------------------------------------------
author    TheFactory
copyright Copyright (C) 2010 SKEPSIS Consult SRL. All Rights Reserved.
@license - http://www.gnu.org/licenses/gpl-2.0.html GNU/GPL
Websites: http://www.thefactory.ro
Technical Support:  Forum - http://www.thefactory.ro/joomla-forum/
-------------------------------------------------------------------------*/

defined('_JEXEC') or die('Restricted access'); ?>

<h1>
	<?php if (!empty($this->post->id)): ?>
    	<?php echo JText::_('Editing post'); ?>: "<small><?php echo $this->post->id; ?></small>"
  	<?php else: ?>
   		<?php echo JText::_('Write new post'); ?>
   <?php endif; ?>
</h1>

<div class="wallfactory-left" id="main_post">
	<form action="<?php echo $this->root; ?>index.php" method="post" id="wall_form">
		<textarea class="round" id="post_message" cols="80" rows="10" name="post_message"><?php echo $this->post->content; ?></textarea> 
		
		<div class="align-top">
			<div class="post-button">
		    	<input type="submit" class="save-button" value="<?php echo JText::_('POST on the wall'); ?>" />
		  	</div>
	    </div>
	    <div class="wallfactory-clear wallfactory-maxwidth"></div>
	<?php if (!empty($this->post->id)): ?>
		<div class="align-top">
	      <div class="posts">
	        <?php require_once('_edit_media.php'); ?>
	      </div>
		</div>
	<?php else: ?>
		<div class="align-top">
	      <div class="posts">
	        <?php require_once('_upload.php'); ?>
	      </div>
		</div>
	<?php endif; ?>	
		
		<span id="status" class="wallfactory-none"></span>
	<?php if (!empty($this->post->id)): ?>
		<?php $wall_id = $this->post->wall_id;
			  $wall_alias = $this->post->alias;
			  $post_id = $this->post->id;
		?>
	<?php else: ?>
		<?php $wall_id = $this->wall->id;
			  $wall_alias = $this->wall->alias;
			  $post_id = 0;
		?>		  
	<?php endif; ?>	
		
	  <input type="hidden" name="option" value="com_wallfactory" />			  
	  <input type="hidden" name="controller" value="post" />
	  <input type="hidden" name="task" value="save" />
	  <input type="hidden" name="wall_id" id="wall_id" value="<?php echo $wall_id; ?>" />
	  <input type="hidden" name="current_wall_alias" id="current_wall_alias" value="<?php echo $wall_alias; ?>" />
	  <input type="hidden" name="post_id" id="post_id" value="<?php echo $post_id; ?>" />
	  <input type="hidden" name="Itemid" value="<?php echo $this->Itemid; ?>" />  
  	
	</form>
  </div>
<div class="wallfactory-clear"></div> 
<script>
 var root = "<?php echo JUri::root(); ?>";
 var PHPsessionID = "<?php echo session_id(); ?>";
 var sessionName = "<?php echo $this->session->getName(); ?>";
 var sessionId = "<?php echo $this->session->getId(); ?>";
</script>

<script src="components/com_wallfactory/assets/js/swfpostmessage.js"></script>




