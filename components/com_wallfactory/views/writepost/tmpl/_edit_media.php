<?php
/*------------------------------------------------------------------------
com_wallfactory - Wall Factory
------------------------------------------------------------------------
author    TheFactory
copyright Copyright (C) 2010 SKEPSIS Consult SRL. All Rights Reserved.
@license - http://www.gnu.org/licenses/gpl-2.0.html GNU/GPL
Websites: http://www.thefactory.ro
Technical Support:  Forum - http://www.thefactory.ro/joomla-forum/
-------------------------------------------------------------------------*/

defined('_JEXEC') or die('Restricted access'); ?>

<div class="wallfactory_fieldset" id="wallfactory-wrapper">
	
 	<span class="legend"><?php echo JText::_('Edit attachments'); ?>: </span>
	<table cellpadding="3" cellspacing="0">
   
	<!-- image -->
	
   <?php if (isset($this->image)) : ?> 
     <?php foreach ($this->image as $row): ?>
   	   <?php if  (isset ($row->id) && ($row->id != null) ) : ?>
 
    <tr>
        <td>
          <label for="image"><?php echo JText::_('Photo title'); ?>:</label>
        </td>
        <td><input type="text" maxlength="100" size="38" class="input_border" id="image_name" name="image_name" value="<?php echo $row->image_title; ?>" ></td>
    </tr>
    <tr>
        <td>
          <label for="image"><?php echo JText::_('Photo description'); ?>:</label>
        </td>
  	 	<td><textarea id="image-description" cols="20" rows="3" name="image-description"><?php echo $row->description; ?></textarea> </td>
    </tr>

    <tr>
    	<td><label for="txtImageName">Image:</label></td>    
    	<td><div>
        		<span><?php echo $row->image_name.'.'.$row->extension; ?></span>
        </div>
        </td>
      </tr>
      
      <tr>
       
    	<td colspan="2"><div>
    		  <a class="wallfactory-button wallfactory-page_delete" href="<?php echo JRoute::_('index.php?option=com_wallfactory&Itemid=' . $this->Itemid . '&controller=writepost&task=deleteMedia&alias=' . $this->post->alias).'&media_id='.$row->image_id ; ?>"><?php echo JText::_('Delete image'); ?></a>
        	</div>
        </td>	
    </tr>
   
       <?php endif; ?>
	 <?php endforeach; ?>
	
   <?php endif; ?>
   <!-- video -->
   
<?php if (isset($this->links) && ($this->links->video_link != null) ): ?>
   <?php //foreach ($this->links as $link): ?>
   	
    <?php //if (!empty($this->links)): ?>
    <?php if (isset($this->image) ): ?>
    <tr><td colspan="2" class="upload_separator"></td></tr>
    <?php endif; ?>
    <tr>
        <td width="80">
          <label for="video"><?php echo JText::_('Video URL'); ?>:</label>
        </td>
        <td><input id="video-link" type="text" size="80" class="input_border" name="video-link" value="<?php echo $this->links->video_link; ?>" disabled="disabled"></td>
        </tr>
       
      <tr>
        <td colspan="2">
    		 <a class="wallfactory-button wallfactory-page_delete"
    		  href="<?php echo JRoute::_('index.php?option=com_wallfactory&Itemid=' . $this->Itemid . '&controller=writepost&task=deleteUrl&alias=' . $this->post->alias).'&link_id='.$this->links->link_id.'&type=1' ; ?>">
    		  <?php echo JText::_('Delete video link'); ?></a>
			
		</td>	
    </tr>
   
    
    <?php //endforeach; ?>
     <?php endif; ?>
    <!-- mp3 -->
    <?php if (isset($this->mp3) && ($this->mp3->id != null) ): ?>
   <?php //foreach ($this->mp3 as $row): ?>
     <tr><td colspan="2" class="upload_separator"></td></tr>
     <tr>
        <td>
          <label for="mp3"><?php echo JText::_('MP3 title'); ?>:</label>
        </td>
        <td><input type="text" maxlength="100" size="38" class="input_border" id="mp3_name" name="mp3_name" value="<?php echo $this->mp3->mp3_title; ?>"></td>
      </tr>
      
      <tr>
        <td><label for="txtMp3Name">Mp3 File:</label></td>
        <td><div>
        		<span><?php echo $this->mp3->mp3_name.'.'.$this->mp3->extension; ?></span>
        		 
        </div>
        </td>
      </tr>
      <tr>
        <td colspan="2"><div>
    		  <div> <a class="wallfactory-button wallfactory-page_delete" href="<?php echo JRoute::_('index.php?option=com_wallfactory&Itemid=' . $this->Itemid . '&controller=writepost&task=deleteMedia&alias=' . $this->post->alias).'&media_id='.$this->mp3->mp3_id ; ?>"><?php echo JText::_('Delete file'); ?></a>
        	</div>    
        </td>	
      </tr>
      
      
       <?php //endforeach; ?>
       <?php endif; ?>
      <!-- file -->
    <?php if (isset($this->file) && ($this->file->id != null) ): ?>
   		<?php //foreach ($this->file as $row): ?>
      
      <tr><td colspan="2" class="upload_separator"></td></tr>
      <tr>
        <td width="80">
          <label for="image"><?php echo JText::_('File title'); ?>:</label>
        </td>
         <td><input type="text" maxlength="100" size="38" class="input_border" id="file_name" name="file_name" value="<?php echo $this->file->file_title; ?>"></td>
      </tr>
      <tr>
        <td><label for="txtFileName"><?php echo JText::_('File name'); ?>:</label></td>
        <td><div>
        		<span><?php echo $this->file->file_name.'.'.$this->file->extension; ?></span>
        </div>
        </td>
      </tr>
      <tr>
        <td colspan="2">
    		 <a class="wallfactory-button wallfactory-page_delete" href="<?php echo JRoute::_('index.php?option=com_wallfactory&Itemid=' . $this->Itemid . '&controller=writepost&task=deleteMedia&alias=' . $this->post->alias).'&media_id='.$this->file->file_id ; ?>"><?php echo JText::_('Delete file'); ?></a>
			
		</td>	
      </tr>
      <?php if (isset($this->links) && ($this->links->url != null) ): ?>
      <tr><td colspan="2" class="upload_separator"></td></tr>
       <?php endif; ?>
       <?php //endforeach; ?>
      <?php endif; ?>
    <!-- URL -->
   <?php if (isset($this->links) && ($this->links->url != null) ): ?>
   <?php //foreach ($this->links as $link): ?>
      <tr>
       <td width="80">
          <label for="url"><?php echo JText::_('URL'); ?>:</label>
        </td>
        <td><input type="text" maxlength="100" size="60" class="input_border" id="url" name="url" value="<?php echo $this->links->url; ?>"></td>
      </tr>
      
      <tr>
        <td colspan="2">
    		 <a class="wallfactory-button wallfactory-page_delete" href="<?php echo JRoute::_('index.php?option=com_wallfactory&Itemid=' . $this->Itemid . '&controller=writepost&task=deleteUrl&alias=' . $this->post->alias).'&link_id='.$this->links->link_id.'&type=2' ; ?>"><?php echo JText::_('Delete link'); ?></a>
			
		</td>	
    </tr>
    <?php //endforeach; ?>
    <?php endif; ?>
    </table>
    
</div>
<!--</form>-->
<div class="wallfactory-spacer"></div>
