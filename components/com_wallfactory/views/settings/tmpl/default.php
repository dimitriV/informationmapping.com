<?php
/*------------------------------------------------------------------------
com_wallfactory - Wall Factory
------------------------------------------------------------------------
author    TheFactory
copyright Copyright (C) 2010 SKEPSIS Consult SRL. All Rights Reserved.
@license - http://www.gnu.org/licenses/gpl-2.0.html GNU/GPL
Websites: http://www.thefactory.ro
Technical Support:  Forum - http://www.thefactory.ro/joomla-forum/
-------------------------------------------------------------------------*/

defined('_JEXEC') or die('Restricted access'); ?>

<h1><?php echo JText::_('My account settings'); ?></h1>
<p><?php echo JText::_('Edit your settings'); ?></p>

<form action="<?php echo $this->root; ?>index.php" method="post">

  <div id="tabs">
	  <ul>
	    <!-- General settings -->
	    <li><a href="#tabs-1"><?php echo JText::_('General'); ?></a></li> 

	    <!-- wall settings -->
	    <?php if ($this->wall_owner): ?>
  		  <li><a href="#tabs-2"><?php echo JText::_('Wall'); ?></a></li>
  		<?php endif; ?>

  		<!-- Avatar settings -->
  		<?php if ($this->wallSettings->enable_avatars): ?>
		    <li><a href="#tabs-3"><?php echo JText::_('Avatar'); ?></a></li>
		  <?php endif; ?>
	  </ul>

	  <!-- TAB: General settings -->
	  <div id="tabs-1">
  	  <?php require_once('_general.php'); ?>
   	  </div>

	  <!-- TAB: wall settings -->
	  <?php if ($this->wall_owner): ?>
	    <div id="tabs-2">
  		  <?php require_once('_wall.php'); ?>
   	  </div>
   	<?php endif; ?>

   	<!-- TAB: Avatar settings -->
   	<?php if ($this->wallSettings->enable_avatars): ?>
	    <div id="tabs-3">
  		  <?php require_once('_avatar.php'); ?>
  	  </div>
  	<?php endif; ?>
  </div>

  <div style="margin: 10px; ">
    <input type="submit" value="<?php echo JText::_('Save settings'); ?>" class="save-button" />
  </div>

  <input type="hidden" name="option" value="com_wallfactory" />
  <input type="hidden" name="controller" value="settings" />
  <input type="hidden" name="task" value="update" />
  <input type="hidden" name="Itemid" value="<?php echo $this->Itemid; ?>" />
</form>

<script>
  jQueryFactory(document).ready(function ($) {
    if($().tabs)
    {
      $("#tabs").tabs({ selected: 0});
    }
    else
    {
      $("#tabs").html("<h1>jQuery UI library is missing or not loaded! This page cannot be displayed properly!</h1>");
    }

    if($().progressbar)
    {
      // Progress bar
      $("#progressbar").progressbar({
        value: 0
      });
    }
    else
    {
      $("#tabs").html("<h1>jQuery UI library is missing or not loaded! This page cannot be displayed properly!</h1>");
    }
 
		// Ajax Flash Upload
		$('#swfupload-control').swfupload({
		  upload_url: "<?php echo JRoute::_('index.php?option=com_wallfactory&controller=upload&task=uploadAvatar'); ?>",
		  file_size_limit : "10240",
		  file_types : "*.*",
		  file_types_description : "All Files",
		  file_upload_limit : "0",
		  file_queue_limit: "1",
		  file_post_name : "avatar",
		  flash_url : "<?php echo JURI::root(); ?>components/com_wallfactory/assets/swfs/swfupload.swf",
		  button_width : 100,
		  button_height : 20,
		  button_text: '<span class="theFont"><?php echo JText::_('Select image'); ?></span>',
		  button_text_style: ".theFont { font-size: 12px; font-family: Helvetica,Arial,sans-serif; font-weight: normal; }",
		  button_placeholder : $('#button')[0],
		  debug: false,
		  post_params: { "PHPSESSID" : "<?php echo session_id(); ?>", '<?php echo $this->session->getName(); ?>' : '<?php echo $this->session->getId(); ?>' },
		  custom_settings : {something : "here"}
		})
		.bind('fileQueued', function(event, file){
		  // start the upload since it's queued
		  $(this).swfupload('startUpload');
		})
		.bind('uploadProgress', function(event, file, bytesLoaded){
		  $('#progressbar').progressbar('option', 'value', Math.floor(bytesLoaded * 100 / file.size));
		})
		.bind('uploadComplete', function(event, file){
		  $("#progressbar").hide();

		  $("#current_avatar").fadeOut().load('<?php echo JRoute::_('index.php?option=com_wallfactory&controller=upload&task=getAvatar'); ?>', function () {
		    $(this).fadeIn(function () {
		      // upload completed, show the new avatar
		      $("#avatar_container").css("height", $("#current_avatar").css("height"));
		    });
		  });
		})
		.bind('uploadStart', function(event, file){
			$("#progressbar").show();
		});

  });
</script>