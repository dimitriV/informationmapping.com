<?php
/*------------------------------------------------------------------------
com_wallfactory - Wall Factory
------------------------------------------------------------------------
author    TheFactory
copyright Copyright (C) 2010 SKEPSIS Consult SRL. All Rights Reserved.
@license - http://www.gnu.org/licenses/gpl-2.0.html GNU/GPL
Websites: http://www.thefactory.ro
Technical Support:  Forum - http://www.thefactory.ro/joomla-forum/
-------------------------------------------------------------------------*/

defined('_JEXEC') or die('Restricted access'); ?>

<table class="wallfactory-maxwidth">
  <tbody>
   
    <!-- Posts per page -->
    <tr>
      <td class="align-top"><label for="posts_per_page"><b><?php echo JText::_('Posts per page'); ?>:</b></label></td>
      <td><input id="posts_per_page" name="posts_per_page" value="<?php echo $this->wall_settings->posts_per_page; ?>" /></td>
    </tr>


    <!--New Comment Notification-->
    <?php if ($this->wallSettings->enable_notification_new_comment): ?>
      <tr>
        <td class="align-top"><label for="comment_notification"><b><?php echo JText::_('New comment notification'); ?>:</b></label></td>
        <td>
          <select id="comment_notification" name="comment_notification">
            <option value="0" <?php echo ($this->wall_settings->comment_notification == 0) ? 'selected="selected"' : ''; ?>><?php echo JText::_('No'); ?></option>
            <option value="1" <?php echo ($this->wall_settings->comment_notification == 1) ? 'selected="selected"' : ''; ?>><?php echo JText::_('Yes'); ?></option>
          </select>
        </td>
      </tr>
    <?php endif; ?>

    <!--New Report Comment Notification-->
    <?php if ($this->wallSettings->enable_notification_new_report_comment): ?>
      <tr>
        <td class="align-top"><label for="report_comment_notification"><b><?php echo JText::_('New report comment notification'); ?>:</b></label></td>
        <td>
          <select id="report_comment_notification" name="report_comment_notification">
            <option value="0" <?php echo ($this->wall_settings->report_comment_notification == 0) ? 'selected="selected"' : ''; ?>><?php echo JText::_('No'); ?></option>
            <option value="1" <?php echo ($this->wall_settings->report_comment_notification == 1) ? 'selected="selected"' : ''; ?>><?php echo JText::_('Yes'); ?></option>
          </select>
        </td>
      </tr>
    <?php endif; ?>
    
    <!--New Post Notification-->
    <?php if ($this->wallSettings->enable_notification_new_post): ?>
      <tr>
        <td class="align-top"><label for="post_notification"><b><?php echo JText::_('New post notification'); ?>:</b></label></td>
        <td>
          <select id="post_notification" name="post_notification">
            <option value="0" <?php echo ($this->wall_settings->post_notification == 0) ? 'selected="selected"' : ''; ?>><?php echo JText::_('No'); ?></option>
            <option value="1" <?php echo ($this->wall_settings->post_notification == 1) ? 'selected="selected"' : ''; ?>><?php echo JText::_('Yes'); ?></option>
          </select>
        </td>
      </tr>
    <?php endif; ?>

    <!--New Report post Notification-->
    <?php if ($this->wallSettings->enable_notification_new_report_post): ?>
      <tr>
        <td class="align-top"><label for="report_post_notification"><b><?php echo JText::_('New report post notification'); ?>:</b></label></td>
        <td>
          <select id="report_post_notification" name="report_post_notification">
            <option value="0" <?php echo ($this->wall_settings->report_post_notification == 0) ? 'selected="selected"' : ''; ?>><?php echo JText::_('No'); ?></option>
            <option value="1" <?php echo ($this->wall_settings->report_post_notification == 1) ? 'selected="selected"' : ''; ?>><?php echo JText::_('Yes'); ?></option>
          </select>
        </td>
      </tr>
    <?php endif; ?>

  </tbody>
</table>
