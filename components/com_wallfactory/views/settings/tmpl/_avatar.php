<?php
/*------------------------------------------------------------------------
com_wallfactory - Wall Factory
------------------------------------------------------------------------
author    TheFactory
copyright Copyright (C) 2010 SKEPSIS Consult SRL. All Rights Reserved.
@license - http://www.gnu.org/licenses/gpl-2.0.html GNU/GPL
Websites: http://www.thefactory.ro
Technical Support:  Forum - http://www.thefactory.ro/joomla-forum/
-------------------------------------------------------------------------*/

defined('_JEXEC') or die('Restricted access'); ?>

<table class="wallfactory-maxwidth">
  <tbody>

    <tr>
      <td style="width: 10px;"><input type="radio" id="no_avatar" name="avatar_type" value="0" <?php echo ($this->settings->avatar_type == 0) ? 'checked="checked"' : ''; ?> /></td>
      <td style="width: 250px;"><label for="no_avatar"><?php echo Jtext::_('Don\'t use an avatar'); ?></label></td>
      <td></td>
    </tr>

    <tr>
      <td colspan="3" style="padding: 5px 0px;"><div style="border-bottom: 1px dotted #999999;"></div></td>
    </tr>
    
    <tr>
      <td><input <?php echo (!wallHelper::getIntegration('cb') || !$this->wallSettings->allow_cb_avatars) ? 'disabled="disabled"' : ''; ?> type="radio" id="cb_avatar" name="avatar_type" value="1" <?php echo ($this->settings->avatar_type == 1) ? 'checked="checked"' : ''; ?> /></td>
      <td><label for="cb_avatar"><?php echo Jtext::_('Use Community Builder avatar'); ?></label></td>
      <td></td>
    </tr>

    <tr>
      <td colspan="3" style="padding: 5px 0px;"><div style="border-bottom: 1px dotted #999999;"></div></td>
    </tr>

    <tr>
      <td><input <?php echo (!$this->wallSettings->enable_gravatar) ? 'disabled="disabled"' : ''; ?> type="radio" id="cb_gravatar" name="avatar_type" value="3" <?php echo ($this->settings->avatar_type == 3) ? 'checked="checked"' : ''; ?> /></td>
      <td><label for="cb_gravatar"><?php echo Jtext::_('Use Gravatar'); ?></label></td>
      <td></td>
    </tr>

    <tr>
      <td colspan="3" style="padding: 5px 0px;"><div style="border-bottom: 1px dotted #999999;"></div></td>
    </tr>

    <tr>
      <td style="vertical-align: top;"><input type="radio" id="my_avatar" name="avatar_type" value="2" <?php echo ($this->settings->avatar_type == 2) ? 'checked="checked"' : ''; ?> /></td>
      <td style="vertical-align: top;"><label for="my_avatar"><?php echo Jtext::_('Use my avatar'); ?></label></td>
      <td>
        <div id="avatar_container">
          <div id="current_avatar">
          <?php // if avatar, else img 'no avatar' ?>
            <img src="<?php echo JURI::root(); ?>components/com_wallfactory/storage/users/<?php echo $this->user->id; ?>/avatar/avatar.<?php echo $this->settings->avatar_extension; ?>?<?php echo time(); ?>" />
          </div>
        </div>

        <div id="swfupload-control" class="wallfactory-margin"><input type="button" id="button" /></div>
        <div id="progressbar" class="wallfactory-none"></div>
      </td>
    </tr>

  </tbody>
</table>

<style>
  #current_avatar img { border: 1px solid #999999; }
</style>