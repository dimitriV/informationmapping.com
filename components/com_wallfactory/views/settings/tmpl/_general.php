<?php 
/*------------------------------------------------------------------------
com_wallfactory - Wall Factory
------------------------------------------------------------------------
author    TheFactory
copyright Copyright (C) 2010 SKEPSIS Consult SRL. All Rights Reserved.
@license - http://www.gnu.org/licenses/gpl-2.0.html GNU/GPL
Websites: http://www.thefactory.ro
Technical Support:  Forum - http://www.thefactory.ro/joomla-forum/
-------------------------------------------------------------------------*/

defined('_JEXEC') or die('Restricted access'); ?>
<?php JHTML::_('behavior.calendar'); ?>

<table class="wallfactory-maxwidth">
  <tbody>
  <!-- Description -->
    <tr>
      <td><span class="align-top media_width"><label for="description"><b><?php echo JText::_('About me'); ?>:</b></label></span></td>
      <td><textarea rows="10" cols="60" id="description" name="description"><?php echo $this->settings->description; ?></textarea></td>
    </tr>

  <!-- Gender -->
    <tr>
      <td class="align-top"><label for="gender"><b><?php echo JText::_('Gender'); ?>:</b></label></td>
      <td>
      	<select id="gender" name="gender">
      		<option value="x" <?php echo ($this->settings->gender == 'x') ? 'selected="selected"' : ''; ?>><?php echo JText::_('--Select gender--'); ?></option>
        	<option value="m" <?php echo ($this->settings->gender == 'm') ? 'selected="selected"' : ''; ?>><?php echo JText::_('Male'); ?></option>
        	<option value="f" <?php echo ($this->settings->gender == 'f') ? 'selected="selected"' : ''; ?>><?php echo JText::_('Female'); ?></option>
      	</select>
      </td>
    </tr>
  
  <!-- Birthday -->
    <tr>
      <td> <span class="align-top"><label for="birthday"><b><?php echo JText::_('Birthday'); ?>:</b></label></span></td>
      <td><?php $birthday = $this->settings->birthday;
      	 	echo JHTML::calendar("$birthday",'birthday','birthday','%Y-%m-%d'); 
      		// Simply use the above function and you are done, if you dont want to have any default date, simply leave the first param blank i.e '' 
		?>
      </td>
    </tr>  
    
  <!-- Name -->
    <tr>
      <td><span class="align-top"><label for="name"><b><?php echo JText::_('Name'); ?>:</b></label></span></td>
      <td><input id="name" name="name" value="<?php echo $this->settings->name; ?>" /></td>
    </tr>
    
     <!-- Personal info view -->
    <tr>
      <td class="align-top"><label for="gender"><b><?php echo JText::_('Profile info available for'); ?>:</b></label></td>
      <td>
      	<select id="allow_profile_view" name="allow_profile_view">
      		<option value="0" <?php echo ($this->settings->allow_profile_view == 0) ? 'selected="selected"' : ''; ?>><?php echo JText::_('WALL OWNERS'); ?></option>
        	<option value="1" <?php echo ($this->settings->allow_profile_view == 1) ? 'selected="selected"' : ''; ?>><?php echo JText::_('ALL USERS'); ?></option>
      	</select>
      </td>
    </tr>

  </tbody>
</table>
