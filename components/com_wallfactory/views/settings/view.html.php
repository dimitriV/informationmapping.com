<?php
/*------------------------------------------------------------------------
com_wallfactory - Wall Factory
------------------------------------------------------------------------
author    TheFactory
copyright Copyright (C) 2010 SKEPSIS Consult SRL. All Rights Reserved.
@license - http://www.gnu.org/licenses/gpl-2.0.html GNU/GPL
Websites: http://www.thefactory.ro
Technical Support:  Forum - http://www.thefactory.ro/joomla-forum/
-------------------------------------------------------------------------*/

defined('_JEXEC') or die('Restricted access');

jimport('joomla.application.component.view');

class FrontendViewSettings extends JView
{
  function __construct()
  {
    parent::__construct();

    $this->wallHelper   = wallHelper::getInstance();
    $this->wallSettings = new wallSettings();
  }

  function display($tpl = null)
  {
    global $Itemid;

    $this->wallHelper->checkGuestView(true);
    wallHelper::checkIsLoggedIn();
    wallHelper::checkWallOwner();

    $user          =& JFactory::getUser();
    $settings      =& $this->get('Settings');
    $wall_settings =& $this->get('WallSettings');
    $session       =& JFactory::getSession();
    $wall_owner    =  wallHelper::checkWallOwner(false);
    $wallSettings  = new wallSettings();
 
    $this->assignRef('settings',      $settings);
    $this->assignRef('wall_settings', $wall_settings);
    $this->assignRef('Itemid',        $Itemid);
    $this->assignRef('user',          $user);
    $this->assignRef('session',       $session);
    $this->assignRef('wall_owner',    $wall_owner);
    $this->assignRef('root',          JUri::root());
    $this->assignRef('wallSettings',  $wallSettings);

    JHtmlFactory::jQueryScript('swfupload', 'ui-widget', 'ui-tabs', 'ui-progressbar');
    JHTML::script('swfupload.js', 'components/com_wallfactory/assets/js/');

    JHTML::stylesheet('jquery-ui-1.7.2.custom.css', 'components/com_wallfactory/assets/css/');
    JHTML::stylesheet('buttons.css',                'components/com_wallfactory/assets/css/');
    JHTML::stylesheet('writepost.css',                'components/com_wallfactory/assets/css/');

    parent::display($tpl);
  }
}