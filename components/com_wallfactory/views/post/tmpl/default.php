<?php 
/*------------------------------------------------------------------------
com_wallfactory - Wall Factory
------------------------------------------------------------------------
author    TheFactory
copyright Copyright (C) 2010 SKEPSIS Consult SRL. All Rights Reserved.
@license - http://www.gnu.org/licenses/gpl-2.0.html GNU/GPL
Websites: http://www.thefactory.ro
Technical Support:  Forum - http://www.thefactory.ro/joomla-forum/
-------------------------------------------------------------------------*/

defined('_JEXEC') or die('Restricted access'); ?>

<?php if (!$this->post): ?>

  <h1><?php echo JText::_('An error has occured!'); ?></h1>
  <p><?php echo JText::_('The post you are looking for wasn\'t found!'); ?></p>
  <br />
  <p><?php echo JText::_('Return to the'); ?> <a href="javascript: history.back();"><?php echo JText::_('previous page'); ?></a>, <?php echo JText::_('or find more posts'); ?> <a href="<?php echo JRoute::_('index.php?option=com_wallfactory&view=posts&layout=latest&Itemid=' . $this->Itemid); ?>"><?php echo JText::_('here'); ?></a>.</p>

<?php else: ?>

  <div id="loading-bar" class="wallfactory-none"><?php echo JText::_('Loading...'); ?></div>
  <div class="wallfactory-left wallfactory-maxwidth">
 	<div class="main_message" style="background-color: #ddd;">
      <!-- avatar --> 
		<div class="avatar">
		  	<?php if ($this->post->user_id == 0): ?>
  				<img src="<?php echo JURI::root(); ?>components/com_wallfactory/assets/images/guest.png" class="guest-avatar" alt="avatar" />
			<?php else: ?>	
  				<a href="<?php echo JRoute::_('index.php?option=com_wallfactory&Itemid=' . $this->Itemid . '&view=wall&alias=' . $this->post->alias); ?>" class="alias-margin"> 
					<?php echo $this->wallHelper->getAvatar($this->post->user_id, $this->post->avatar_type, $this->post->avatar_extension);  ?>	
  				
  				<!--<img src="<?php //echo JURI::root(); ?>components/com_wallfactory/storage/users/<?php //echo $this->post->user_id; ?>/avatar/avatar.<?php //echo $this->post->avatar_extension; ?>?<?php echo time(); ?>" class="user-avatar" alt="avatar" />-->
				</a>
			<?php endif; ?>
			<?php if ($this->post->user_id != 0): ?>	
				<a href="<?php echo JRoute::_('index.php?option=com_wallfactory&Itemid=' . $this->Itemid . '&view=wall&alias=' . $this->post->alias); ?>" class="alias-font">	
          			<?php ($this->wallSettings->wall_title_display == 0) ? ($name = $this->post->name) : ($name = $this->post->alias);
          			echo $name; ?>
          		</a> <br />
          		<span><?php echo JText::_('Posts: '); echo $this->count[$this->post->user_id]; ?></span>
          	
         	<?php else: ?>
         		<span class="wallfactory-comment-content"><?php echo JText::_('Guest'); ?></span>	
            <?php endif; ?> 	
	   	</div>
            
	    <div class="round_post">
	    	 <div class="wallfactory-post-content">	
		  	     <div class="post_message">
           			<div style="padding: 2px 3px; font-size: 12px;"><?php echo nl2br(html_entity_decode($this->post->content)); ?></div>
	       		 </div>
			</div>
	    	<div class="action-bar">
          	  	<div class="wallfactory-action-header">
		          		<span class="wallfactory-year wallfactory-action"><?php echo date('H:m:s a l, d F Y', JText::_(strtotime($this->post->date_created))); ?></span>
		      	    	<span  class="wallfactory-year right-icon">
			        	  	<?php if ($this->user->id == $this->post->user_id): ?>
			      				<a class="wallfactory-icon wallfactory-page_edit" href="<?php echo JRoute::_('index.php?option=com_wallfactory&Itemid=' . $this->Itemid . '&view=writepost&alias=' . $this->post->alias).'&post_id='.$this->post->id ; ?>"><?php echo JText::_('edit'); ?></a> |
			            	    <a class="wallfactory-icon wallfactory-page_delete" href="<?php echo JRoute::_('index.php?option=com_wallfactory&Itemid=' . $this->Itemid . '&controller=post&task=delete&alias=' . $this->post->alias).'&post_id='.$this->post->id ; ?>"><?php echo JText::_('delete'); ?></a> |
			            	<?php endif; ?>
							<?php if ($this->wallSettings->enable_bookmarks && $this->post->user_id != 0): ?>
				       		<!-- Bookmarks -->    
		       		 		<span class="wallfactory-icon wallfactory-share"> 
				              <a href="#" class="show-share-post" id="show-share-post<?php echo $this->post->id; ?>" rel="<?php echo $this->post->id; ?>"><?php echo JText::_(' share'); ?></a> |
					        </span>
							<?php endif; ?>
			        		<?php if ( ($this->wallSettings->allow_comments && !$this->user->guest) || ($this->wallSettings->allow_comments && $this->wallSettings->allow_guest_comments) ): ?>
				       		   	<span class="wallfactory-icon wallfactory-comment_add">
						             <a href="#" class="show-comment-form" id="show-comment-form<?php echo $this->post->id; ?>" rel="<?php echo $this->post->id; ?>"><?php echo JText::_('comment'); ?></a> 
						        </span>     
						             <div class="wallfactory-comment-added" id="comment-message">
							      		<?php echo JText::_('Your comment was added.'); ?>
							     	 </div>
						   <?php else: ?>
			            		<?php echo JText::_('Comments are not allowed for your group!'); ?>
			               <?php endif; ?>
		               		<span>
		               		  <?php if ($this->user->id != $this->post->user_id): ?>
			            		  <?php if (!$this->user->guest && $this->post->reported == 0): ?>					
									   | <a class="wallfactory-icon wallfactory-report" href="<?php echo JUri::root(); ?>index.php?option=com_wallfactory&controller=post&task=report&format=raw&type=2&alias=<?php echo $this->post->alias;?>&id=<?php echo $this->this->post->id; ?>" rel="<?php echo $this->post->id; ?>">
						            <?php echo JText::_('report'); ?></a>
								  <?php endif; ?>  
						     <?php endif; ?>	

				        	<?php if ($this->post->reported == 1): ?>	
				               	| <span class="wallfactory-icon wallfactory-reported"> <?php echo  JText::_('reported'); ?></span>
				       		<?php endif; ?>
						       
	            	  		</span>
	            	 	</span> 
		             <!--</div>-->
		       	<!--</div>-->
	        	</div>
	        	<span class="wallfactory-media-bookmark" id="share-post<?php echo $this->post->id; ?>">
	              <?php if ($this->wallSettings->enable_bookmarks && $this->post->user_id != 0): ?>
			          <fieldset>
			            <?php foreach ($this->bookmarks[$this->post->id] as $k=>$bookmark): ?>
			              <a target="_blank" href="<?php echo $bookmark->full_link; ?>"><img src="<?php echo JURI::root(); ?>administrator/components/com_wallfactory/storage/bookmarks/<?php echo $bookmark->id; ?>.<?php echo $bookmark->extension; ?>" /></a>
			            <?php endforeach; ?>
			          </fieldset>
		          <?php endif; ?>
            	</span>
     	
            	
            	<span class="wallfactory-comment-form" id="comment-form<?php echo $this->post->id; ?>">
            		<div class="cancel-comment-wrapper">
		              <a href="#" class="cancel-comment" rel="<?php echo $this->post->id; ?>" >&nbsp;</a>
		            </div>  
	            		<div>
	            			<table cellspacing="0" cellpadding="0" width="100%">
	            			  <tr>
	            				<td class="comment-column">
	            					<textarea name="comment<?php echo $this->post->id; ?>" id="comment<?php echo $this->post->id; ?>" rows="2" cols="2" class="comment-textarea"></textarea>
	            				</td>
	            				<td><a href="#" class="submit-comment" rel="<?php echo $this->post->id; ?>"><?php echo JText::_('Submit'); ?></a>
		               			</td>
		           			  </tr>
		           			</table>
		           		</div>
		             <?php if ($this->user->guest && $this->wallSettings->allow_guest_comments): ?>
		              <?php if ( ($this->wallSettings->allow_comments) && ($this->wallSettings->captcha_comment) ): ?>
	               		<div id="recaptcha_response_field_error" colspan="2" style="color: #ff0000; display: none;">&darr;&nbsp;<span></span>&nbsp;&darr;</div>
	                 	<div style="vertical-align: top;"><label for="recaptcha_response_field"><?php echo JText::_('Are you human'); ?>?</label>
	                   		<div><?php echo $this->captcha_html; ?></div>
	                 	</div>
	              	  <?php endif; ?>
		              <?php endif; ?> 
		               
		              <div id="wallfactory-comment-loader<?php echo $this->post->id; ?>" class="wallfactory-none">
		                <img alt="ajax-loader" src="components/com_wallfactory/assets/images/ajax-loader-blue.gif" class="wallfactory-icon" />
		                <?php echo JText::_('Processing form...'); ?>
		              </div>
               </span>
            
	  	     </div><!-- action-bar -->
          	
        	 <div class="wallfactory-clear"></div>

			<div class="wallfactory-media-container">	
	      		<!--  display attached media for the post  -->
		    	<div id="wallfactory-your-media<?php echo $this->post->id; ?>">
	          		<?php if (!empty($this->urls[$this->post->id]) ) { ?>
	          			<div class="wallfactory-text">
          		  		<?php  
				     	foreach ( $this->urls[$this->post->id] as $link) { 
				     
				     		if (  $link->url != '' ):  
							   	if (isset($link->url_title) && $link->url_title != "") 
								 	$url_title = $link->url_title;
								else 
									$url_title = $link->url;
							     	
							   	if (isset($link->url_description)) 
							  		$url_description = nl2br(html_entity_decode($link->url_description));
							   	else 
							 		$url_description = '';
							     ?>
							     <div class="round_media">	
								   	<img alt="url" title="url" src="<?php echo JURI::root(); ?>components/com_wallfactory/assets/images/world_link.png">
								   	<span class="media_title"><a target="_blank" href="<?php echo $link->url; ?>"><?php echo $url_title; ?></a>
									<?php if ($this->wallSettings->enable_bookmarks && $this->post->user_id != 0): ?>
								    	<span class="wallfactory-icon wallfactory-share">
									   		<a href="#" class="show-share-url" id="show-share-url<?php echo $this->post->id; ?>" rel="<?php echo $this->post->id; ?>">
									   		 <?php echo JText::_('share'); ?>
									   		</a>
									   	</span>
									<?php endif; ?>
								    	<span class="wallfactory-media-bookmark" id="share-url<?php echo $this->post->id; ?>">
								        <!-- url Bookmarks -->    
				       					    <?php if ($this->wallSettings->enable_bookmarks && $this->post->user_id != 0): ?>
										         <fieldset>
										           <?php foreach ($this->url_bookmarks[$this->post->id] as $k=>$url_bookmark): ?>
										             <a target="_blank" href="<?php echo $url_bookmark->full_link; ?>"><img src="<?php echo JURI::root(); ?>administrator/components/com_wallfactory/storage/bookmarks/<?php echo $url_bookmark->id; ?>.<?php echo $url_bookmark->extension; ?>" /></a>
										           <?php endforeach; ?>
										         </fieldset>
										       <?php endif; ?>
			            				</span>
			            			</span>	
								    <p style="padding: 5px 5px;"><?php echo $url_description; ?></p>
								 </div>	
						  	<?php endif; ?>
					  		<!-- video -->
						 	<?php  if (  $link->video_link != '' ):  
								$thumb_source = ($link->video_thumbnail != "") ? $link->video_thumbnail : $link->video_sourceThumb;
								
								if (isset($link->video_title) && $link->video_title != "") 
								 	$video_title = stripslashes($link->video_title);
								else 
									$video_title = $link->video_link;
					     	
					     		if (isset($link->video_description)) 
					     		 	$video_description = nl2br(html_entity_decode(stripslashes($link->video_description)));
					     		else 
					     			$video_description = '';
						 		?>
							  	<div class="round_media">
									<?php if (strpos($link->video_link,'youtube')): 	?>	
									<table width="100%" cellspacing="0" cellpadding="0" border="0">
										<tr>
										  <td rowspan="2" class="media_width">
											<div class="media_thumbnail">
											   <a title="<?php echo $link->video_title; ?>" onclick="jQueryFactory.prettyPhoto.open('<?php echo $link->video_link; ?>', '', '')" href="javascript:void(0);">
												<img src="<?php echo 'http://'.$thumb_source; ?>" alt="YouTube" class="media_width" /><br /> 
											  </a>
											</div>
										  </td>
										  <td class="media_share">
										  <?php if ($this->wallSettings->enable_bookmarks && $this->post->user_id != 0): ?>
											<div class="wallfactory-icon wallfactory-share">	
								           		<a href="#" class="show-share-video" id="show-share-video<?php echo $this->post->id; ?>" rel="<?php echo $this->post->id; ?>"><span class="wallfactory-year"><?php echo JText::_('share'); ?></span></a>
										    </div>
											<?php else: ?>
												&nbsp;
											<?php endif; ?>
									      </td>
									    </tr>
									    <tr>  
										  <td>
											<div class="video_content">
												<span class="media_title" style="padding: 2px; white-space:normal; "><?php echo strip_tags($video_title, '<a>'); ?></span>
							    				<div class="video-description"><?php echo strip_tags($video_description, '<a>');  ?></div>	
											</div>	
										  </td>
										</tr>	
									</table>	
									<?php endif; ?>	
									
									<?php if (strpos($link->video_link,'vimeo')): 	?>	
									<table width="100%" cellspacing="0" cellpadding="0">
										<tr>
										  <td rowspan="2" class="media_width">
											<div class="media_thumbnail">
												<a title="<?php echo $link->video_title; ?>" onclick="jQueryFactory.prettyPhoto.open('<?php echo $link->video_link; ?>', '', '')" href="javascript:void(0);">
												<img src="<?php echo 'http://'.$thumb_source; ?>" rel="prettyPhoto" alt="<?php echo $link->video_sitename; ?>" class="media_width" /><br />
											  </a>
											</div>
										  </td>
										  <td class="media_share">
										  <?php if ($this->wallSettings->enable_bookmarks && $this->post->user_id != 0): ?>
											<div class="wallfactory-icon wallfactory-share">	
								              	<a href="#" class="show-share-video" id="show-share-video<?php echo $this->post->id; ?>" rel="<?php echo $this->post->id; ?>"><span class="wallfactory-year"><?php echo JText::_('share'); ?></span></a>
										    </div>
											<?php else: ?>
												&nbsp;
											<?php endif; ?>
									      </td>
										</tr>
										<tr>
										  <td class="align-top">
											<div class="video_content">
												<span class="media_title"><?php echo strip_tags($video_title, '<a>'); ?></span>
						    					<div class="video-description"><?php echo strip_tags($video_description, '<a>'); ?></div>	
											</div>		
										  </td>
										</tr>	
									</table>	
									<?php endif; ?>
								
									<?php if (strpos($link->video_link,'metacafe')): ?>	
									<table width="100%" cellspacing="0" cellpadding="0">
										<tr>
										  <td rowspan="2" class="media_width">
											<div class="media_thumbnail">
												<a href="<?php echo $link->video_link; ?>?width=70%&amp;height=70%" rel="prettyPhoto" title="<?php echo $link->video_title; ?>">
												<img  src="<?php echo $thumb_source; ?>" class="media_width" alt="<?php echo $link->video_title; ?>"></a>
												<br />
											</div>
										  </td>
										  <td class="media_share">
										    <?php if ($this->wallSettings->enable_bookmarks && $this->post->user_id != 0): ?>
											<div class="wallfactory-icon wallfactory-share">	
								              	<a href="#" class="show-share-video" id="show-share-video<?php echo $this->post->id; ?>" rel="<?php echo $this->post->id; ?>"><span class="wallfactory-year"><?php echo JText::_('share'); ?></span></a>
										    </div>
											<?php else: ?>
												&nbsp;
											<?php endif; ?>
									      </td>
										</tr>
										<tr>
										  
										  <td class="align-top">			
											<div class="video_content">
												<span class="media_title"><?php echo strip_tags($video_title, '<a>'); ?></span>
								    			<div class="video-description"><?php echo strip_tags($video_description, '<a>'); ?></div>	
											</div>	
										  </td>
										</tr>	
									</table>		
									<?php endif; ?>

									<?php if (strpos($link->video_link,'myspace')): ?>	
									<table width="100%" cellspacing="0" cellpadding="0">
										<tr>
										   <td rowspan="2" class="media_width">
											<div class="media_thumbnail">
											  <a title="<?php echo $link->video_title; ?>"  onclick="jQueryFactory.prettyPhoto.open('<?php echo $link->video_link; ?>', '', '')" href="javascript:void(0);">
												<img src="<?php echo $thumb_source; ?>" alt="<?php echo $link->video_sitename; ?>" class="media_width" /><br />
											  </a>
											</div>
										  </td>
										  <td class="media_share">
										    <?php if ($this->wallSettings->enable_bookmarks && $this->post->user_id != 0): ?>
											<div class="wallfactory-icon wallfactory-share">	
								              	<a href="#" class="show-share-video" id="show-share-video<?php echo $this->post->id; ?>" rel="<?php echo $this->post->id; ?>"><span class="wallfactory-year"><?php echo JText::_('share'); ?></span></a>
										    </div>
											<?php else: ?>
												&nbsp;
											<?php endif; ?>
									      </td>
										</tr>
										<tr>
										  <td class="align-top">		
											<div class="video_content">
												<span class="media_title"><?php echo strip_tags($video_title, '<a>'); ?></span>
								    			<div class="video-description"><?php echo strip_tags($video_description, '<a>'); ?></div>	
											</div>	
										  </td>
										</tr>	
									</table>			
									<?php endif; ?>		
									<?php if (strpos($link->video_link,'howcast')): ?>	
									<table width="100%" cellspacing="0" cellpadding="0">
										<tr>
										  <td rowspan="2" class="media_width">
											<div class="media_thumbnail">
												<a href="<?php echo $link->video_link; ?>?width=500&height=350" rel="prettyPhoto" title="<?php echo $link->video_title; ?>">
												<img  src="<?php echo $link->video_sourceThumb; ?>" class="media_width" alt="<?php echo $link->video_title; ?>"></a>
												<br />
											</div>	
										  </td>
										  <td class="media_share">
										    <?php if ($this->wallSettings->enable_bookmarks && $this->post->user_id != 0): ?>
											<div class="wallfactory-icon wallfactory-share">	
								              	<a href="#" class="show-share-video" id="show-share-video<?php echo $this->post->id; ?>" rel="<?php echo $this->post->id; ?>"><span class="wallfactory-year"><?php echo JText::_('share'); ?></span></a>
										    </div>
											<?php else: ?>
												&nbsp;
											<?php endif; ?>
									      </td>
										</tr>
										<tr>
										  <td class="align-top">
											<div class="video_content">
												<span class="media_title"><?php echo strip_tags($video_title, '<a>'); ?></span>
								    			<div class="video-description"><?php echo strip_tags($video_description, '<a>'); ?></div>	
											</div>	
										  </td>
										</tr>	
									</table>		
									<?php endif; ?>		
								   	<div class="wallfactory-media-bookmark" id="share-video<?php echo $this->post->id; ?>">
					            	<!-- video Bookmarks -->    
	       						      	<?php if ($this->wallSettings->enable_bookmarks && $this->post->user_id != 0): ?>
								          <fieldset>
								            <?php foreach ($this->video_bookmarks[$this->post->id] as $k=>$video_bookmark): ?>
								              <a target="_blank" href="<?php echo $video_bookmark->full_link; ?>"><img src="<?php echo JURI::root(); ?>administrator/components/com_wallfactory/storage/bookmarks/<?php echo $video_bookmark->id; ?>.<?php echo $video_bookmark->extension; ?>" /></a>
								            <?php endforeach; ?>
								          </fieldset>
							          	<?php endif; ?>
		           					</div>
									
						      	</div>  <!-- end round videobox -->    
								<br /><br />
							<?php endif; ?> <!-- end video link -->  
			          	<?php 
				     }  // end foreach link
				     ?>
			      	
		      	</div> <!-- class="wallfactory-text" -->
				<?php } // end exista links ?>	
         		
         		
         		<?php if (!empty($this->media[$this->post->id]) ) { ?>
					<div class="wallfactory-text">	
		           <?php  foreach ( $this->media[$this->post->id] as $m) {    
						switch ($m->folder) {
						
						case 'images': 
		                ?>
		           	 	<div class="round_media">
				           	<?php  $filename = JPATH_SITE.DS.'components'.DS.'com_wallfactory'.DS.'storage'.DS.'users'.DS.$m->user_id.DS.$m->folder.DS.$m->name.'.'.$m->extension;
								
				           		if (file_exists($filename)) {
				           			$size = getimagesize($filename);
									$max_size = 80;
									if ($size[0] > $max_size || $size[1] > $max_size)  {
									    if ($size[0] > $size[1]) {
									        $new_width  = $max_size;
									        $new_height = round($max_size * $size[1] / $size[0],0);
									    }
									    else
									    {
									        $new_width  = round($max_size * $size[0] / $size[1],0);
									        $new_height = $max_size;
									    }
									}
									else
									{
									    $new_width  = $size[0];
									    $new_height = $size[1];
									}
								} 
								?>
								<table width="100%" cellspacing="0" cellpadding="0">
									<tr>
									   <td rowspan="2" class="media_width">
					            		<div class="media_thumbnail">
											<a title="name" onclick="jQueryFactory.prettyPhoto.open('<?php echo JURI::root(); ?>components/com_wallfactory/storage/users/<?php echo $m->user_id;?>/<?php echo $m->folder;?>/<?php echo $m->name; ?>.<?php echo $m->extension; ?>','<?php echo $m->name; ?>');" href="javascript:void(0);">	
					            				<img src="<?php echo JURI::root(); ?>components/com_wallfactory/storage/users/<?php echo $m->user_id;?>/<?php echo $m->folder;?>/<?php echo $m->name; ?>.<?php echo $m->extension; ?>" 
											rel="prettyPhoto" style="width: <?php echo $new_width;?>px; height: <?php echo $new_height;?>px; cursor: pointer; padding: 3px;"><br />
										 	</a>
										</div>
									  </td>
									  <td class="media_share">
									   	  <?php if ($this->wallSettings->enable_bookmarks && $this->post->user_id != 0): ?>
											<div class="wallfactory-icon wallfactory-share">	
								              	<a href="#" class="show-share-image" id="show-share-image<?php echo $this->post->id; ?>" rel="<?php echo $this->post->id; ?>"><span class="wallfactory-year"><?php echo JText::_('share'); ?></span></a>
										    </div>
										  <?php else: ?>
												&nbsp;
										  <?php endif; ?>    
									   </td>
									</tr>
									<tr>    
									  <td class="align-top">
										<div class="wallfactory-left">
											<?php if (isset($m->title)) 
									     		 	$m_title = nl2br(html_entity_decode($m->title));
									     	  else 
									     			$m_title = '';
					
									     	  if (isset($m->description)) 
									     		 	$m_description = nl2br(html_entity_decode($m->description));
									     	  else 
									     			$m_description = '';	
										   	?>
											<span class="media_title" style="padding: 2px 0;"><?php echo $m_title; ?></span> <br />
											<span class="media_description"><?php echo $m_description; ?></span><br />
										</div>
									 </td>
									</tr>	
								</table>	
								<div class="wallfactory-right">	
									 <?php if ($this->wallSettings->enable_bookmarks && $this->post->user_id != 0): ?>				          				          	    
						                <span class="wallfactory-media-bookmark" id="share-image<?php echo $this->post->id; ?>">
							               <!-- video Bookmarks -->    
	       							      <?php if ($this->wallSettings->enable_bookmarks && $this->post->user_id != 0): ?>
									          <fieldset>
									            <?php foreach ($this->image_bookmarks[$this->post->id] as $k=>$image_bookmark): ?>
									              <a target="_blank" href="<?php echo $image_bookmark->full_link; ?>"><img src="<?php echo JURI::root(); ?>administrator/components/com_wallfactory/storage/bookmarks/<?php echo $image_bookmark->id; ?>.<?php echo $image_bookmark->extension; ?>" /></a>
									            <?php endforeach; ?>
									          </fieldset>
								          <?php endif; ?>
							  			</span>
									<?php endif; ?>	
		            			</div>
					
						  </div>
						  <br /><br />
							<?php  break;
							case 'mp3':
							?>
				         	<div class="round_media">
					          <?php $link = JURI::root().'components/com_wallfactory/storage/users/'.$m->user_id.'/'.$m->folder.'/'.$m->name.'.'.$m->extension; ?>
					       
							    <img alt="Mp3" title="Mp3" src="<?php echo JURI::root(); ?>components/com_wallfactory/assets/images/sound.png" style="padding: 1px; background-repeat: no-repeat;"> 
								<span class="media_title"><?php echo $m->title; ?>
								<?php if ($this->wallSettings->enable_bookmarks && $this->post->user_id != 0): ?>
						   			<span class="wallfactory-icon wallfactory-share">
						   				<a href="#" class="show-share-mp3" id="show-share-mp3<?php echo $this->post->id; ?>" rel="<?php echo $this->post->id; ?>"><span class="wallfactory-year"><?php echo JText::_('share'); ?></span></a>
						   			</span>
								<?php endif; ?>  	
						   		</span>
						   		  
							    <div class="media_file">
							        <object classid="clsid:D27CDB6E-AE6D-11cf-96B8-444553540000" width="220" height="25" id="flashContent"> 
							        <param name="movie" value="<?php echo JURI::root(); ?>components/com_wallfactory/assets/swfs/player.swf" /> 
							        <param name="FlashVars" value="playerID=1&amp;noinfo=yes&amp;bg=#ffffff&amp;soundFile=<?php echo $link; ?>" /> 
							        <param name="wmode" value="transparent" /> 
							        <param name="quality" value="high">
							        <param name="menu" value="true">
							        <!--[if !IE]>--> 
							        <object type="application/x-shockwave-flash" data="<?php echo JURI::root(); ?>components/com_wallfactory/assets/swfs/player.swf" width="177" height="20"> 
							        <param name="FlashVars" value="playerID=1&amp;noinfo=yes&amp;bg=#ffffff&amp;soundFile=<?php echo $link; ?>" /> 
							        <param name="wmode" value="transparent" />
							        <param name="quality" value="high">
									<param name="menu" value="true"> 
							        <!--<![endif]--> 
							        <a href="<?php echo $link; ?>" class="wallfactory-text">Play Audio</a> 
							        <!--[if !IE]>--> 
							        </object> 
							        <!--<![endif]--> 
							        </object>
						        </div>
						        
						        <span class="wallfactory-media-bookmark" id="share-mp3<?php echo $this->post->id; ?>">
					               <!-- mp3 Bookmarks -->    
       						      <?php if ($this->wallSettings->enable_bookmarks && $this->post->user_id != 0): ?>
						          <fieldset>
						            <?php foreach ($this->mp3_bookmarks[$this->post->id] as $k=>$mp3_bookmark): ?>
						              <a target="_blank" href="<?php echo $mp3_bookmark->full_link; ?>"><img src="<?php echo JURI::root(); ?>administrator/components/com_wallfactory/storage/bookmarks/<?php echo $mp3_bookmark->id; ?>.<?php echo $mp3_bookmark->extension; ?>" /></a>
						            <?php endforeach; ?>
						          </fieldset>
						          <?php endif; ?>
            					</span>
						  </div>  
				         <br />  	
	            		<?php  break;
		            		case 'files': ?>
	            			<?php  $link = JURI::root().'components/com_wallfactory/storage/users/'.$m->user_id.'/'.$m->folder.'/'.$m->name.'.'.$m->extension; ?>
					     <?php if (file_exists($link)): ?>
		            		<div class="round_media">
		            			<span class="wallfactory-button wallfactory-file" >
		            				<span class="media_title"><?php echo $m->name; ?>
					     	  	</span>	
								<?php if ($this->wallSettings->enable_bookmarks && $this->post->user_id != 0): ?>
                   				<span class="wallfactory-icon wallfactory-share">
						   			<a href="#" class="show-share-file" id="show-share-file<?php echo $this->post->id; ?>" rel="<?php echo $this->post->id; ?>"><span class="wallfactory-year"><?php echo JText::_('share'); ?></span></a>
						   		</span>
								<?php endif; ?>
			            	  </span>    
							  <div class="media_file">
							  	<img alt="file" title="file" src="<?php echo JURI::root(); ?>components/com_wallfactory/assets/images/arrow_down.png">
								<?php require_once(JPATH_SITE.DS.'components'.DS.'com_wallfactory'.DS.'download.php'); ?>
							  </div>
							  <span class="wallfactory-media-bookmark" id="share-file<?php echo $this->post->id; ?>">
					               <!-- file Bookmarks -->    
			       			      	<?php if ($this->wallSettings->enable_bookmarks && $this->post->user_id != 0): ?>
							          	<fieldset>
								            <?php foreach ($this->file_bookmarks[$this->post->id] as $k=>$file_bookmark): ?>
								              <a target="_blank" href="<?php echo $file_bookmark->full_link; ?>"><img src="<?php echo JURI::root(); ?>administrator/components/com_wallfactory/storage/bookmarks/<?php echo $file_bookmark->id; ?>.<?php echo $file_bookmark->extension; ?>" /></a>
								            <?php endforeach; ?>
								        </fieldset>
							        <?php endif; ?>
			            	    </span>
					    	</div>
						    <?php endif; ?>
						  <?php 
						break; ?>
				    <?php }
					}	?>
				</div> <!-- class="wallfactory-text" -->
				<?php 
			   }
			?>
      </div> 
	<div class="wallfactory-clear"></div> 	
      <?php endif; ?> 
   
	<div class="wallfactory-spacer"></div>    
  </div>

 <!--  </div>  -->

<div class="wallfactory-clear"></div> 

<!-- <div class="main_message" style="background-color: #000;"> -->
<?php if ($this->wallSettings->allow_comments ): ?>
  <?php if (!count($this->comments[$this->post->id]) ): ?>
    <?php echo JText::_('No comments found! Be the first one to write a comment!'); ?>
<?php else: ?>  
<div style="width: 99%;">
  <div class="update"></div>
    <div class="comments_list">
	  <div class="wallfactory-text">
        <div class="round_comments">
			<?php //if (!empty($this->comments[$this->post->id]) ) {  ?>
			<table class="wallfactory-table">
  				<?php foreach ($this->comments[$this->post->id] as $k=>$comment): ?>
  			   <tr class="wallfactory-comment-header">
      			<td style="width:70%;">
        			<img src="<?php echo JURI::root(); ?>components/com_wallfactory/assets/images/comment.png" class="wallfactory-icon" />
        			<span class="wallfactory-comment-author">
        			<?php if ($comment->user_id != 0): ?>
        			  <a href="<?php echo JRoute::_('index.php?option=com_wallfactory&Itemid=' . $this->Itemid . '&view=wall&alias=' . $comment->author_alias); ?>" class="comment-alias">	
	          			<?php echo $comment->author_alias; ?>
	          		 </a>
	          		<?php else: ?>
	          			<span style="font-weight: bold; font-size: 13px; float: left;"><?php echo JText::_('Guest'); ?></span>
	          		<?php endif; ?> 
	          	    </span>
        			<span class="wallfactory-comment-date">(<?php echo date('H:i d.m.Y', strtotime($comment->date_created)); ?>)</span>
      			</td>
      			<td style="text-align: right; ">
    		      	<?php if (!$this->user->guest && ($this->user->id == $comment->user_id)): ?>
			          <span class="right-icon">
			            <a class="wallfactory-button wallfactory-comment_delete" href="<?php echo JUri::root(); ?>index.php?option=com_wallfactory&controller=comment&task=delete&format=raw&type=2&alias=<?php echo $this->post->alias;?>&id=<?php echo $comment->id; ?>" >
			            <?php echo JText::_('delete'); ?></a>
			          </span>
			     	<?php endif; ?>
			     	<?php if ($this->user->id != $comment->user_id): ?>
      		        	<?php if (!$this->user->guest && $comment->reported == 0): ?>
			     		  <span class="wallfactory-comment-report">
			 				<a class="wallfactory-icon wallfactory-report" href="<?php echo JUri::root(); ?>index.php?option=com_wallfactory&controller=comment&task=report&format=raw&type=2&alias=<?php echo $post->alias;?>&id=<?php echo $comment->id; ?>" rel="<?php echo $comment->id; ?>">
				          <?php echo JText::_('report'); ?></a>
				          </span>
						<?php endif; ?> 	
					<?php endif; ?>  
		     		<?php if ($comment->reported == 1): ?>	
		        		<span class="wallfactory-icon wallfactory-reported"> <?php echo  JText::_('reported'); ?></span>
		     	 	<?php endif; ?>
		       </td>
		    </tr>
		    <tr class="wallfactory-comment-text">
		    	<td colspan="2"><div class="wallfactory-comment-content"><?php //echo $comment->content; ?>
		      <?php echo stripslashes(nl2br(str_replace("\\n", '<br />', $comment->content))); ?></div>
		       </td>
		    </tr>
  		    <?php endforeach; ?>
  			<tr>
			  	<td colspan="2">
			  	<?php if ($this->post->no > $this->wallSettings->comments_per_page): ?>
					<div style="padding: 5px 0px;">
						<a class="wallfactory-button wallfactory-comments" href="<?php echo JUri::root(); ?>index.php?option=com_wallfactory&&view=comments&layout=_list&post_id=<?php echo $this->post->id; ?>&alias=<?php echo $this->post->alias;?>&Itemid=<?php echo $this->Itemid; ?>" rel="<?php echo $this->post->id; ?>">
				         <?php echo JText::_('View all comments'); ?></a>
			        </div>
				<?php endif; ?>		          
				</td>
		    </tr>
		</table>
	</div>
	</div>
  </div>
  </div>
<?php endif; ?>
<!-- </div> -->


    <?php endif; ?>
    </div>
   <div class="wallfactory-clear"></div> 
  </div>
</div>

<div class="wallfactory-clear"></div> 
<script>
  var root = "<?php echo JUri::root(); ?>";
</script>



<script>
  jQueryFactory(document).ready(function ($) {
    // Report comment
    $(".report a").click(function () {
      var id     = $(this).parent().attr("id");
      var parent = $(this).parent();

      id = id.split("_");
      id = id[1];

      $.post("<?php echo JRoute::_('index.php?option=com_wallfactory&controller=comment&task=report'); ?>", {
        id:     id,
        format: "raw"
      }, function (response) {
        switch (response.status)
        {
          case 1:
            alert(response.message);
          break;

          case 2:
            parent.html(response.message);
          break;
        }
      }, "json");

      return false;
    });

    // hide wall comments
     $("#comments_list").hide();
      
        	
	$("a[rel^='prettyPhoto']").prettyPhoto( {
		theme: 'facebook',
		markup: '<div class="pp_pic_holder"> \
						<div class="ppt">&nbsp;</div> \
						<div class="pp_top"> \
							<div class="pp_left"></div> \
							<div class="pp_middle"></div> \
							<div class="pp_right"></div> \
						</div> \
						<div class="pp_content_container"> \
							<div class="pp_left"> \
							<div class="pp_right"> \
								<div class="pp_content"> \
									<div class="pp_loaderIcon"></div> \
									<div class="pp_fade"> \
										<a href="#" class="pp_expand" title="Expand the image">Expand</a> \
										<div class="pp_hoverContainer"> \
											<a class="pp_next" href="#">next</a> \
											<a class="pp_previous" href="#">previous</a> \
										</div> \
										<div id="pp_full_res"></div> \
										<div class="pp_details clearfix"> \
											<p class="pp_description"></p> \
											<a class="pp_close" href="#">Close</a> \
											<div class="pp_nav"> \
												<a href="#" class="pp_arrow_previous">Previous</a> \
												<p class="currentTextHolder">0/0</p> \
												<a href="#" class="pp_arrow_next">Next</a> \
											</div> \
										</div> \
									</div> \
								</div> \
							</div> \
							</div> \
						</div> \
						<div class="pp_bottom"> \
							<div class="pp_left"></div> \
							<div class="pp_middle"></div> \
							<div class="pp_right"></div> \
						</div> \
					</div> \
					<div class="pp_overlay"></div>'
	});
     

  });
</script>