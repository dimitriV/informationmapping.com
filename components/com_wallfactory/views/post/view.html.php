<?php
/*------------------------------------------------------------------------
com_wallfactory - Wall Factory
------------------------------------------------------------------------
author    TheFactory
copyright Copyright (C) 2010 SKEPSIS Consult SRL. All Rights Reserved.
@license - http://www.gnu.org/licenses/gpl-2.0.html GNU/GPL
Websites: http://www.thefactory.ro
Technical Support:  Forum - http://www.thefactory.ro/joomla-forum/
-------------------------------------------------------------------------*/

defined('_JEXEC') or die('Restricted access');

jimport('joomla.application.component.view');

class FrontendViewPost extends JView
{
  function __construct()
  {
    parent::__construct();

    $this->wallHelper   = wallHelper::getInstance();
    $this->wallSettings = new wallSettings();
    
    $this->alias_req = JRequest::getVar('alias','','GET', 'string');
    
  }

  function display($tpl = null)
  {
    require_once(JPATH_COMPONENT_ADMINISTRATOR.DS.'lib'.DS.'recaptcha'.DS.'recaptchalib.php');
    global $Itemid;

    wallHelper::checkGuestView();
    
    $allow_guest = wallHelper::checkProfileView($this->alias_req);
    
    if (!$allow_guest) 
    	wallHelper::checkWallOwner();

    $post   	=& $this->get('PostData');
    $media 		=& $this->get('Media'); 
    $urls 		=& $this->get('Urls'); 
   
   	$comments 	=& $this->get('Comments'); 
   	$count  	=& $this->get('TotalPosts');
   	   	
    if (!is_null($post))
    {
      JRequest::setVar('id', $post->id, 'GET');
    }
    
    $user   =& JFactory::getUser();
    $app    =& JFactory::getApplication();
    
    $settings =& JTable::getInstance('members', 'Table');
    $settings->findOneByUserId($user->id);
    $settings->email = $user->email;

    $this->assignRef('post',                 $post);
    $this->assignRef('media',   			 $media);
    $this->assignRef('urls',   				 $urls);
    $this->assignRef('comments',   			 $comments);
  	$this->assignRef('count',      	  		 $count);
    $this->assignRef('user',                 $user);
    $this->assignRef('Itemid',               $Itemid);
    $this->assignRef('enable_bookmarks',     $this->wallSettings->enable_bookmarks);
    $this->assignRef('allow_guest_comments', $this->wallSettings->allow_guest_comments);
    $this->assignRef('captcha_comment',      $this->wallSettings->captcha_comment);
    $this->assignRef('settings', 			 $settings);

    if ($this->wallSettings->captcha_comment)
    {
      $this->assignRef('captcha_html', recaptcha_get_html(base64_decode($this->wallSettings->recaptcha_public_key)));
    }

    if ($this->wallSettings->enable_bookmarks)
    {
	  $bookmarks =& $this->get('Bookmarks');
      $this->assignRef('bookmarks', $bookmarks);
      
      $video_bookmarks =& $this->get('VideoBookmarks');
      $this->assignRef('video_bookmarks', $video_bookmarks);
      
      $url_bookmarks =& $this->get('UrlBookmarks');
      $this->assignRef('url_bookmarks', $url_bookmarks);
      
      $mp3_bookmarks =& $this->get('Mp3Bookmarks');
      $this->assignRef('mp3_bookmarks', $mp3_bookmarks);
      
      $file_bookmarks =& $this->get('FileBookmarks');
      $this->assignRef('file_bookmarks', $file_bookmarks);
      
      $image_bookmarks =& $this->get('ImageBookmarks');
      $this->assignRef('image_bookmarks', $image_bookmarks);
      
    }
    
    JHtmlFactory::jQueryScript();
    JHtmlFactory::jQueryScript('cookie', 'ui', 'ui-widget', 'ui-mouse', 'ui-sortable');
    JHtmlFactory::jQueryScript('swfupload', 'ui', 'ui-widget', 'ui-mouse', 'ui-position', 'ui-draggable', 'ui-dialog', 'ui-tabs', 'ui-progressbar');
    JHTML::script('default.js', 'components/com_wallfactory/assets/js/views/wall/');
    JHTML::_('script', 'main.js', 'components/com_wallfactory/assets/js/');
    JHTML::_('script', 'comments.js', 'components/com_wallfactory/assets/js/');
    JHTML::_('script', 'jquery.prettyPhoto.js', 'components/com_wallfactory/assets/js/');  
    JHTML::script('default.js', 'components/com_wallfactory/assets/js/views/post/');	
    
    JHTML::stylesheet('jquery-ui-1.7.2.custom.css', 'components/com_wallfactory/assets/css/');
    JHTML::stylesheet('buttons.css',                'components/com_wallfactory/assets/css/');
    JHTML::stylesheet('posts.css',                  'components/com_wallfactory/assets/css/views/');
    JHTML::stylesheet('writepost.css', 				'components/com_wallfactory/assets/css/');
    JHTML::stylesheet('post.css',                   'components/com_wallfactory/assets/css/views/');
    JHTML::stylesheet('comments.css',               'components/com_wallfactory/assets/css/views/');
    JHTML::stylesheet('main.css',                   'components/com_wallfactory/assets/css/');
    JHTML::stylesheet('prettyPhoto.css', 			'components/com_wallfactory/assets/css/'); //media="screen"

    if ($post)
    {
      $declarations = array(
        'txt_loading_comments'  => JText::_('Loading comments...'),
        'root'                  => JURI::base(),
        'post_id'               => !is_null($post) ? $post->id : 0,
        'captcha_comment'       => $this->wallSettings->captcha_comment,
        'is_guest'				=> $user->guest,
        'txt_field_required'    => JText::_('This field is required'),
        'txt_comment_added'     => JText::_('Comment added!'),
        'route_post_comment'    => JRoute::_('index.php?option=com_wallfactory&controller=comment&task=addComment'),
        'route_report_comment'  => JRoute::_('index.php?option=com_wallfactory&controller=comment&task=report'),
        'route_vote'            => JRoute::_('index.php?option=com_wallfactory&controller=comment&task=registerVote'),
        'route_load_comments'   => JRoute::_('index.php?option=com_wallfactory&view=comments&layout=_list&post_id=' . $post->id),
      );
      wallHelper::addScriptDeclarations($declarations);

      $document =& JFactory::getDocument();

    }
   
    $this->_display($tpl);
  }

  function _display($tpl)
  {
  	parent::display($tpl);
  }
}