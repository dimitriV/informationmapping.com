<?php
/*------------------------------------------------------------------------
com_wallfactory - Wall Factory
------------------------------------------------------------------------
author    TheFactory
copyright Copyright (C) 2010 SKEPSIS Consult SRL. All Rights Reserved.
@license - http://www.gnu.org/licenses/gpl-2.0.html GNU/GPL
Websites: http://www.thefactory.ro
Technical Support:  Forum - http://www.thefactory.ro/joomla-forum/
-------------------------------------------------------------------------*/

defined('_JEXEC') or die('Restricted access');

jimport('joomla.application.component.view');

class FrontendViewBanned extends JView
{

  function display($tpl = null)
  {
    JHTML::stylesheet('main.css',    'components/com_wallfactory/assets/css/');
    JHTML::stylesheet('buttons.css', 'components/com_wallfactory/assets/css/');

    parent::display($tpl);
  }

}