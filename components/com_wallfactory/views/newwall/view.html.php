<?php
/*------------------------------------------------------------------------
com_wallfactory - Wall Factory
------------------------------------------------------------------------
author    TheFactory
copyright Copyright (C) 2010 SKEPSIS Consult SRL. All Rights Reserved.
@license - http://www.gnu.org/licenses/gpl-2.0.html GNU/GPL
Websites: http://www.thefactory.ro
Technical Support:  Forum - http://www.thefactory.ro/joomla-forum/
-------------------------------------------------------------------------*/

defined('_JEXEC') or die('Restricted access');

jimport('joomla.application.component.view');

class FrontendViewNewwall extends JView
{
  function __construct()
  {
    parent::__construct();

    $this->wallHelper   = wallHelper::getInstance();
    $this->wallSettings = new wallSettings();
  }

  function display($tpl = null)
  {
    global $Itemid;

    $this->wallHelper->checkGuestView(true);

    $model   =& JModel::getInstance('mywall', 'FrontendModel');
    $my_wall = $model->getMyWall();
    $user    =& JFactory::getUser();
       
    if (!is_null($my_wall))
    {
      $this->_layout = 'error';
    }
    else
    {
      JHTML::stylesheet('buttons.css', 'components/com_wallfactory/assets/css/');

      JHtmlFactory::jQueryScript();
      JHTML::script('default.js', 'components/com_wallfactory/assets/js/views/newwall/');

      $allowed_wallowners = $model->getAllowedBlogger();
      
      $this->assignRef('allowed_wallowners', $allowed_wallowners);
      $this->assignRef('user_name', $user->name);
      
    }

    $this->assignRef('Itemid', $Itemid);

    $declarations = array(
      'loading_text' => JText::_('Checking for availability...'),
      'route_check'  => JRoute::_('index.php?option=com_wallfactory&controller=mywall&task=availability')
    );
    $this->wallHelper->addScriptDeclarations($declarations);

    $this->_display($tpl);
  }

  function _display($tpl)
  {
  	parent::display($tpl);
  }
}