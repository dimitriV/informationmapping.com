<?php
/*------------------------------------------------------------------------
com_wallfactory - Wall Factory
------------------------------------------------------------------------
author    TheFactory
copyright Copyright (C) 2010 SKEPSIS Consult SRL. All Rights Reserved.
@license - http://www.gnu.org/licenses/gpl-2.0.html GNU/GPL
Websites: http://www.thefactory.ro
Technical Support:  Forum - http://www.thefactory.ro/joomla-forum/
-------------------------------------------------------------------------*/

defined('_JEXEC') or die('Restricted access'); ?>

<h1><?php echo JText::_('Activate new wall'); ?></h1>

<?php if ($this->allowed_wallowners): ?>
  <p><?php echo JText::_('Activate your new wall!'); ?></p>
  <form>
    <table>
      <tr>
        <td><label for="title"><?php echo JText::_('Wall Title'); ?>:</label></td>
        <td>
          <input type="text" id="title" name="title" value="<?php echo $this->user_name ;?>" /><span id="availability"></span>
        </td>
      </tr>
      <tr>
        <td colspan="2"><input id="register-submit"  type="submit" value="<?php echo JText::_('Register') ;?>" /></td> <!--disabled="disabled"-->
      </tr>
    </table>
    <input type="hidden" name="option" value="com_wallfactory" />
    <input type="hidden" name="controller" value="mywall" />
    <input type="hidden" name="task" value="register" />
    <input type="hidden" name="Itemid" value="<?php echo $this->Itemid; ?>" />
  </form>
<?php else: ?>
  <p><?php echo JText::_('You are not allowed to create a wall!'); ?></p>
<?php endif; ?>