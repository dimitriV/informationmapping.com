<?php 
/*------------------------------------------------------------------------
com_wallfactory - Wall Factory
------------------------------------------------------------------------
author    TheFactory
copyright Copyright (C) 2010 SKEPSIS Consult SRL. All Rights Reserved.
@license - http://www.gnu.org/licenses/gpl-2.0.html GNU/GPL
Websites: http://www.thefactory.ro
Technical Support:  Forum - http://www.thefactory.ro/joomla-forum/
-------------------------------------------------------------------------*/

defined('_JEXEC') or die('Restricted access'); ?>

<h1><?php echo JText::_('You already have a wall.'); ?></h1>

<p>
  <a href="<?php echo JRoute::_('index.php?option=com_wallfactory&view=wall&Itemid=' . $this->Itemid); ?>">
    <?php echo JText::_('Click here to view your wall'); ?>
  </a>
</p>