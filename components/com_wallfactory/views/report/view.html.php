<?php
/*------------------------------------------------------------------------
com_wallfactory - Wall Factory
------------------------------------------------------------------------
author    TheFactory
copyright Copyright (C) 2010 SKEPSIS Consult SRL. All Rights Reserved.
@license - http://www.gnu.org/licenses/gpl-2.0.html GNU/GPL
Websites: http://www.thefactory.ro
Technical Support:  Forum - http://www.thefactory.ro/joomla-forum/
-------------------------------------------------------------------------*/

defined('_JEXEC') or die('Restricted access');

jimport('joomla.application.component.view');

class FrontendViewReport extends JView
{
  function display($tpl = null)
  {
   	$id     = JRequest::getVar('id', 0, 'GET', 'integer');
    $source = JRequest::getVar('source', '', 'GET', 'string');

    $this->assignRef('id', $id);
    $this->assignRef('source', $source);

    $document =& JFactory::getDocument();
    $document->addScriptDeclaration('var root = "' . JUri::root() . '";');
    
    parent::display($tpl);
  }

  function report($tpl = null)
  {
    $report =& $this->get('Report');

    $tpl = ($report) ? 'reported' : 'error';

    $document =& JFactory::getDocument();
    $document->addScriptDeclaration('var root = "' . JUri::root() . '";');
    
    parent::display('reported');
  }
}