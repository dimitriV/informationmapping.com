<?php
/*------------------------------------------------------------------------
com_wallfactory - Wall Factory
------------------------------------------------------------------------
author    TheFactory
copyright Copyright (C) 2010 SKEPSIS Consult SRL. All Rights Reserved.
@license - http://www.gnu.org/licenses/gpl-2.0.html GNU/GPL
Websites: http://www.thefactory.ro
Technical Support:  Forum - http://www.thefactory.ro/joomla-forum/
-------------------------------------------------------------------------*/

defined('_JEXEC') or die('Restricted access'); ?>

<?php JHTML::_('stylesheet', 'main.css', 'components/com_wallfactory/assets/css/'); ?>

<script>
  jQuery(document).ready( function ($)
  {
    $('#wallfactory-submit-report').click( function (event)
    {
      event.preventDefault();

    	$.post("<?php echo JUri::root(); ?>index.php?option=com_wallfactory&task=report&format=raw", {
          name:    $('#name_').val(),
          email:   $('#email').val(),
          message: $('#message').val(),
          type:    1,
          id:      <?php echo $this->id; ?>,
          source:  "<?php echo $this->source; ?>"},
        function(data){
          $('#wallfactory-report-loading').hide();
          $('#report-update').html(data);
      });

      $('#wallfactory-report-loading').show();
    });
  });
</script>

<div id="report-update">
  <table>
    <tr>
      <td><label for="name_"><?php echo JText::_('Name'); ?>:<label></td>
      <td><input name="name_" id="name_" /></td>
    </tr>

    <tr>
      <td><label for="email"><?php echo JText::_('Email'); ?>:<label></td>
      <td><input name="email" id="email" /></td>
    </tr>

    <tr>
      <td style="vertical-align: top;"><label for="message"><?php echo JText::_('Message'); ?>:<label></td>
      <td><textarea name="message" id="message" style="width: 300px; height: 100px;"></textarea></td>
    </tr>

    <tr>
      <td colspan="2">
        <a href="#" id="wallfactory-submit-report"><?php echo JText::_('Submit report'); ?></a>
      </td>
    </tr>
  </table>
</div>

<div id="wallfactory-report-loading" class="wallfactory-comment-form"><img src="<?php echo JURI::base(); ?>components/com_wallfactory/assets/images/ajax-loader.gif" class="wallfactory-icon" /><?php echo JText::_('Processing form, please wait...'); ?></div>