<?php
/*------------------------------------------------------------------------
com_wallfactory - Wall Factory
------------------------------------------------------------------------
author    TheFactory
copyright Copyright (C) 2010 SKEPSIS Consult SRL. All Rights Reserved.
@license - http://www.gnu.org/licenses/gpl-2.0.html GNU/GPL
Websites: http://www.thefactory.ro
Technical Support:  Forum - http://www.thefactory.ro/joomla-forum/
-------------------------------------------------------------------------*/

defined('_JEXEC') or die('Restricted access');

jimport('joomla.application.component.view');

class FrontendViewLogin extends JView
{
  function __construct()
  {
    parent::__construct();

    $this->wallHelper   = wallHelper::getInstance();
    $this->wallSettings = new wallSettings();
  }

  function display($tpl = null)
  {
    global $Itemid;

    $session =& JFactory::getSession();

    $this->assignRef('Itemid',  $Itemid);
    $this->assignRef('referer', base64_encode($session->get('referer', JRoute::_('index.php'))));

    JHTML::stylesheet('buttons.css', 'components/com_wallfactory/assets/css/');

    $this->_display($tpl);
  }

  function _display($tpl)
  {
    
      parent::display($tpl);
    
  }
}