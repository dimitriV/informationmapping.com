<?php
/*------------------------------------------------------------------------
com_wallfactory - Wall Factory
------------------------------------------------------------------------
author    TheFactory
copyright Copyright (C) 2010 SKEPSIS Consult SRL. All Rights Reserved.
@license - http://www.gnu.org/licenses/gpl-2.0.html GNU/GPL
Websites: http://www.thefactory.ro
Technical Support:  Forum - http://www.thefactory.ro/joomla-forum/
-------------------------------------------------------------------------*/

defined('_JEXEC') or die('Restricted access'); ?>

<h1><?php echo JText::_('Rss Feed Entries!'); ?></h1>

<<?php echo '?'; ?>xml version="1.0">
<rss version="2.0">
   <channel>
      <title><?php echo $this->channel->title; ?></title>
      <link><?php echo $this->root . JRoute::_('index.php?option=com_wallfactory&view=wall&id=' . $this->channel->id); ?></link>
      <description><?php echo htmlentities($this->channel->description); ?></description>
      <pubDate><?php echo gmdate(DATE_RSS); ?></pubDate>
      <?php foreach ($this->data as $post): ?>
        <item>
          <title><?php echo $post->title; ?></title>
          <link><?php echo $this->root . JRoute::_('index.php?option=com_wallfactory&view=post&id=' . $post->id . '&slug=' . $post->slug); ?></link>
          <description><?php echo wallHelper::trimText(strip_tags($post->content), 200); ?></description>
          <pubDate><?php echo gmdate(DATE_RSS, strtotime($post->date_created)); ?></pubDate>
          <guid><?php echo $this->root . JRoute::_('index.php?option=com_wallfactory&view=post&id=' . $post->id . '&slug=' . $post->slug); ?></guid>
        </item>
      <?php endforeach; ?>
   </channel>
</rss>