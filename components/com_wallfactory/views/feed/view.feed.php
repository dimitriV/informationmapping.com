<?php
/*------------------------------------------------------------------------
com_wallfactory - Wall Factory
------------------------------------------------------------------------
author    TheFactory
copyright Copyright (C) 2010 SKEPSIS Consult SRL. All Rights Reserved.
@license - http://www.gnu.org/licenses/gpl-2.0.html GNU/GPL
Websites: http://www.thefactory.ro
Technical Support:  Forum - http://www.thefactory.ro/joomla-forum/
-------------------------------------------------------------------------*/

defined('_JEXEC') or die('Restricted access');

jimport('joomla.application.component.view');

class FrontendViewFeed extends JView
{
  function display($tpl = null)
  {
    global $mainframe;
	$this->wallHelper   = wallHelper::getInstance();
    $content =& JRequest::getVar('content', 'entries', 'GET', 'string');
	
    if (!in_array($content, array('entries', 'comments')))
    {
      $content = 'entries';
    }
    
    $data     =& $this->get('Data');
    $channel  =& $this->get('Channel');
    $document	=& JFactory::getDocument();
    
    $document->title       = $channel->title;
    $document->link        = JRoute::_('index.php?option=com_wallfactory&view=wall&id=' . $channel->id . '&slug=' . wallHelper::getSlug($channel->title));

    foreach ($data as $entry)
    {
      	$title = $this->escape($entry->title);
		$title = html_entity_decode($title);
	
		$item = new JFeedItem();

			$item->title 		   = $title;
			$item->link 		   = $entry->link;
			$item->description 	= wallHelper::trimText($entry->content, 400);
			$item->date			   = $entry->date_created;
			
			$document->addItem($item);
    }

  }
}