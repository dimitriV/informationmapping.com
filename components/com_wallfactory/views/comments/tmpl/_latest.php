<?php 
/*------------------------------------------------------------------------
com_wallfactory - Wall Factory
------------------------------------------------------------------------
author    TheFactory
copyright Copyright (C) 2010 SKEPSIS Consult SRL. All Rights Reserved.
@license - http://www.gnu.org/licenses/gpl-2.0.html GNU/GPL
Websites: http://www.thefactory.ro
Technical Support:  Forum - http://www.thefactory.ro/joomla-forum/
-------------------------------------------------------------------------*/

defined('_JEXEC') or die('Restricted access'); ?>

<?php if (!count($this->comments)): ?>
  <?php echo JText::_('No comments found! Be the first one to write a comment!'); ?>
<?php endif; ?>

<table class="wallfactory-table">
  <?php foreach ($this->comments as $comment): ?>
    <tr>
    	<td>
    	<div class="wallfactory-comment">
   	       <div class="wallfactory-comments-latest">	
		   	<div>
		   		<span style="width: 20%">
			   		<?php if (!$this->user->guest):  ?>	
		          		 <a href="<?php echo JRoute::_('index.php?option=com_wallfactory&Itemid=' . $this->Itemid . '&view=wall&alias=' . $comment->author_alias); ?>" class="comment-alias">	
		          			<?php echo $comment->name; ?>
		          		 </a> 
		          	<?php else: ?>
		          		<?php echo JText::_('Guest'); ?>
		          	<?php endif; ?>		 
	          	</span>
	          	<span class="wallfactory-right"> <span class="wallfactory-year wallfactory-action"><?php echo date('H:m:s a l, d F Y', JText::_(strtotime($comment->date_created))); ?></span>
				<span class="wallfactory-comment-report">
	          	<?php if (!$this->user->guest):  ?>	  
	          		<?php if ($this->user->id == $comment->user_id): ?>
				          <span class="wallfactory-comment-report">
				            <img src="<?php echo JURI::root(); ?>components/com_wallfactory/assets/images/comment_delete.png" class="wallfactory-icon" />
				            <a href="<?php echo JUri::root(); ?>index.php?option=com_wallfactory&controller=comment&task=delete&format=raw&type=2&alias=<?php echo $comment->author_alias;?>&id=<?php echo $comment->id; ?>" >
				            <?php echo JText::_('delete'); ?></a>
				          </span>
				     <?php endif; ?>
				     <?php if ($this->user->id != $comment->user_id): ?>
			            <?php if (!$this->user->guest && $comment->reported == 0): ?>					
							| <span class="wallfactory-icon wallfactory-report"> 
					 	 		<a href="<?php echo JUri::root(); ?>index.php?option=com_wallfactory&controller=comment&task=report&format=raw&type=2&alias=<?php echo $comment->author_alias;?>&id=<?php echo $comment->id; ?>" rel="<?php echo $comment->id; ?>">
						  				<?php echo JText::_('report'); ?>
								</a>
							  </span>
								<?php endif; ?>
				     <?php endif; ?>	  
				     <?php if ($comment->reported == 1): ?>
				           	| <span class="wallfactory-icon wallfactory-reported"><?php echo  JText::_('reported'); ?></span>
				     <?php endif; ?>
				<?php endif; ?> 
				</span>    
     			 </span>
      			 <div><span class="wallfactory-comment-content wallfactory-maxwidth"><?php echo $comment->content; ?></span></div>
		   </div>
		  </div>
 		  <div class="wallfactory-clear"></div>  
	  </div>
      </td>
    </tr>
  <?php endforeach; ?>
</table>

<script>
  var root = "<?php echo JUri::root(); ?>";
</script>



<script>
  jQueryFactory(document).ready(function ($) {
  
    // Report comment
    $(".report a").click(function () {
      var id     = $(this).parent().attr("id");
      var parent = $(this).parent();

      id = id.split("_");
      id = id[1];

      $.post("<?php echo JRoute::_('index.php?option=com_wallfactory&controller=comment&task=report'); ?>", {
        id:     id,
        format: "raw"
      }, function (response) {
        switch (response.status)
        {
          case 1:
            alert(response.message);
          break;

          case 2:
            parent.html(response.message);
          break;
        }
      }, "json");

      return false;
    });

    // hide wall comments
    	  $("#comments_list").hide();
      
      

  });
</script>