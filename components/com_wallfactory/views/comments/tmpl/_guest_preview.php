<?php
/*------------------------------------------------------------------------
com_wallfactory - Wall Factory
------------------------------------------------------------------------
author    TheFactory
copyright Copyright (C) 2010 SKEPSIS Consult SRL. All Rights Reserved.
@license - http://www.gnu.org/licenses/gpl-2.0.html GNU/GPL
Websites: http://www.thefactory.ro
Technical Support:  Forum - http://www.thefactory.ro/joomla-forum/
-------------------------------------------------------------------------*/

defined('_JEXEC') or die('Restricted access'); ?>

<?php if (!count($this->post)): ?>
	<?php echo JText::_('No post found!'); ?>
<?php else: ?>	
	<div class="wallfactory-left">
 		<div class="main_message">
      	<!-- avatar --> 
		<div class="avatar">
			<?php if ($this->wallSettings->enable_avatars): ?>
			   	<?php if ($this->post->user_id == 0): ?>
	      				<img src="<?php echo JURI::root(); ?>components/com_wallfactory/assets/images/guest.png" class="guest-avatar" alt="avatar" />
				<?php else: ?>	
					<?php if ($this->post->avatar_type): ?>
						<a href="<?php echo JRoute::_('index.php?option=com_wallfactory&Itemid=' . $this->Itemid . '&view=wall&alias=' . $this->post->alias); ?>" class="alias-margin"> 
						  <?php echo $this->wallHelper->getAvatar($this->post->user_id, $this->post->avatar_type, $this->post->avatar_extension); ?>
	      	 			  <!--<img src="<?php //echo JURI::root(); ?>components/com_wallfactory/storage/users/<?php //echo $this->post->user_id; ?>/avatar/avatar.<?php //echo $this->post->avatar_extension; ?>?<?php //echo time(); ?>" class="user-avatar" alt="avatar" />-->
						</a>
					<?php endif; ?>
				<?php endif; ?>
			<?php endif; ?>
		  
          <div>
			 <?php if ($this->post->user_id != 0): ?>	
			<a href="<?php echo JRoute::_('index.php?option=com_wallfactory&Itemid=' . $this->Itemid . '&view=wall&alias=' . $this->post->alias); ?>" class="alias-font">	
          		<?php ($this->wallSettings->wall_title_display == 0) ? ($name = $this->post->name) : ($name = $this->post->alias);
          		echo $name; ?>
          	</a> <br />
          	<span><?php //echo JText::_('Posts: '); echo $this->count[$this->post->user_id]; ?></span>
          	<?php else: ?>
         		<span class="wallfactory-comment-content"><?php echo JText::_('Guest'); ?></span>	
            <?php endif; ?> 	
		  </div>	
		</div>
   		<div class="round_post" rel=<?php echo $this->post->id; ?>>
	        <!-- main message -->
	        <div class="action-bar">
	        	<div class="wallfactory-action-header">
		          	<span class="wallfactory-year wallfactory-action"><?php echo date('H:m:s a l, d F Y', JText::_(strtotime($this->post->date_created))); ?></span>
		        </div>
		 		<div style="color: #ccc; padding-top: 2px;">
        			<p style="padding: 5px 0 5px 5px; line-height: 120%; font-size: 12px;"><?php echo nl2br(html_entity_decode($this->post->content)); ?></p>
       			</div>	
		 		<span style="padding: 5px;" id="comment-form<?php echo $this->post->id; ?>">
            		<div>
            			<table cellspacing="0" cellpadding="0" width="80%">
            			  <tr>
            				<td class="comment-column">
            					<textarea name="comment<?php echo $this->post->id; ?>" id="comment<?php echo $this->post->id; ?>" rows="2" cols="2" 
            					style="background: none repeat scroll 0 0 #FFFFFF; width: 330px; height:30px; overflow:hidden;" ><?php echo $this->text_message; ?></textarea>
            				</td>
            				<td><a href="#" class="submit-comment" rel="<?php echo $this->post->id; ?>"><?php echo JText::_('Submit'); ?></a>
                  			</td>
            			  </tr>
            			</table>
		            </div>
		            <?php if ($this->user->guest && $this->wallSettings->allow_guest_comments): ?> 
		              <?php if ( ($this->wallSettings->allow_comments) && ($this->wallSettings->captcha_comment) ): ?>
	               		<div id="recaptcha_response_field_error" colspan="2" style="color: #ff0000; display: none;">&darr;&nbsp;<span></span>&nbsp;&darr;</div>
	                 	<div style="vertical-align: top;"><label for="recaptcha_response_field"><?php echo JText::_('Are you human'); ?>?</label>
	                   		<div><?php echo $this->captcha_html; ?></div>
	                 	</div>
	              	  <?php endif; ?>
		             <?php endif; ?>   
		              <div id="wallfactory-comment-loader<?php echo $this->post->id; ?>" class="wallfactory-none">
		                <img alt="ajax-loader" src="components/com_wallfactory/assets/images/ajax-loader-blue.gif" class="wallfactory-icon" />
		                <?php echo JText::_('Processing form...'); ?>
		              </div>
               </span>
		</div><!-- action-bar -->
     
      <div id="wallfactory-comment-loader<?php echo $this->post->id; ?>" class="wallfactory-none">
        <img alt="ajax-loader" src="components/com_wallfactory/assets/images/ajax-loader-blue.gif" class="wallfactory-icon" />
        <?php echo JText::_('Processing form...'); ?>
      </div>

	</div>
	<div class="wallfactory-clear"></div>	 
  	
</div>
<?php endif; ?>

<div class="wallfactory-clear"></div> 

<script>
  var root = "<?php echo JUri::root(); ?>";
</script>
