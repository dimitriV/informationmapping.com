<?php
/*------------------------------------------------------------------------
com_wallfactory - Wall Factory
------------------------------------------------------------------------
author    TheFactory
copyright Copyright (C) 2010 SKEPSIS Consult SRL. All Rights Reserved.
@license - http://www.gnu.org/licenses/gpl-2.0.html GNU/GPL
Websites: http://www.thefactory.ro
Technical Support:  Forum - http://www.thefactory.ro/joomla-forum/
-------------------------------------------------------------------------*/

defined('_JEXEC') or die('Restricted access'); ?>

<?php if (!count($this->post)): ?>
	<?php echo JText::_('No post found!'); ?>
<?php else: ?>	
	<div class="wallfactory-left">
 		<div class="main_message">
      	<!-- avatar --> 
		<div class="avatar">
		  <?php if ($this->wallSettings->enable_avatars): ?>
			   	<?php if ($this->post->user_id == 0): ?>
	      				<img src="<?php echo JURI::root(); ?>components/com_wallfactory/assets/images/guest.png" class="guest-avatar" alt="avatar" />
				<?php else: ?>	
					<?php if ($this->post->avatar_type): ?>
						<a href="<?php echo JRoute::_('index.php?option=com_wallfactory&Itemid=' . $this->Itemid . '&view=wall&alias=' . $this->post->alias); ?>" class="alias-margin"> 
						  <?php echo $this->wallHelper->getAvatar($this->post->user_id, $this->post->avatar_type, $this->post->avatar_extension); ?>
	      	 			  <!--<img src="<?php //echo JURI::root(); ?>components/com_wallfactory/storage/users/<?php //echo $this->post->user_id; ?>/avatar/avatar.<?php //echo $this->post->avatar_extension; ?>?<?php //echo time(); ?>" class="user-avatar" alt="avatar" />-->
						</a>
					<?php endif; ?>
				<?php endif; ?>
			<?php endif; ?>
          <div>
			 <?php if ($this->post->user_id != 0): ?>	
			<a href="<?php echo JRoute::_('index.php?option=com_wallfactory&Itemid=' . $this->Itemid . '&view=wall&alias=' . $this->post->alias); ?>" class="alias-font">	
          		<?php ($this->wallSettings->wall_title_display == 0) ? ($name = $this->post->name) : ($name = $this->post->alias);
          		echo $name; ?>
          	</a> <br />
          	<?php else: ?>
         		<span class="wallfactory-comment-content"><?php echo JText::_('Guest'); ?></span>	
            <?php endif; ?> 	
		</div>	
		</div>
   		<div class="round_post" rel="<?php echo $this->post->id; ?>">
	        <!-- main message -->
	        <div class="action-bar">
	        	<div class="wallfactory-action-header">
		          	<span class="wallfactory-year wallfactory-action"><?php echo date('H:m:s a l, d F Y', JText::_(strtotime($this->post->date_created))); ?></span>
		          		<span class="wallfactory-year right-icon">
			        	  	<?php if ($this->user->id == $this->post->user_id): ?>
			      				<a class="wallfactory-icon wallfactory-page_edit" href="<?php echo JRoute::_('index.php?option=com_wallfactory&Itemid=' . $this->Itemid . '&view=writepost&alias=' . $this->post->alias).'&post_id='.$this->post->id ; ?>"><?php echo JText::_('edit'); ?></a> |
			            	    <a class="wallfactory-icon wallfactory-page_delete" href="<?php echo JRoute::_('index.php?option=com_wallfactory&Itemid=' . $this->Itemid . '&controller=post&task=delete&alias=' . $this->post->alias).'&post_id='.$this->post->id ; ?>"><?php echo JText::_('delete'); ?></a> |
			            	<?php endif; ?>
							<?php if ($this->wallSettings->enable_bookmarks): ?>
				       		<!-- Bookmarks -->    
		       		 		<span class="wallfactory-icon wallfactory-share"> 
				              <a href="#" class="show-share-post" id="show-share-post<?php echo $this->post->id; ?>" rel="<?php echo $this->post->id; ?>"><?php echo JText::_(' share'); ?></a> |
					        </span>
							<?php endif; ?>
			        		<?php if ( ($this->wallSettings->allow_comments && !$this->user->guest) || ($this->wallSettings->allow_comments && $this->wallSettings->allow_guest_comments) ): ?>
				       		   	<span class="wallfactory-icon wallfactory-comment_add" />
						             <a href="#" class="show-comment-form" id="show-comment-form<?php echo $this->post->id; ?>" rel="<?php echo $this->post->id; ?>"><?php echo JText::_('comment'); ?></a> 
						        </span>     
					             <div class="wallfactory-comment-added" id="comment-message">
						      		<?php echo JText::_('Your comment was added.'); ?>
						     	 </div>
						   <?php else: ?>
			            		<?php echo JText::_('Comments are not allowed for your group!'); ?>
			               <?php endif; ?>
		               		<span class="wallfactory-year" style="margin-right: 3px; float: right;">
			            	<?php if ($this->user->id != $this->post->user_id): ?>
			            		<?php if (!$this->user->guest && $this->post->reported == 0): ?>
									<span class="wallfactory-icon wallfactory-report">
					 				   | <a href="<?php echo JUri::root(); ?>index.php?option=com_wallfactory&controller=post&task=report&format=raw&type=2&alias=<?php echo $this->post->alias;?>&id=<?php echo $this->post->id; ?>" rel="<?php echo $this->post->id; ?>">
						          <?php echo JText::_('report'); ?></a>
						        	</span>
						        <?php endif; ?>	
						    <?php endif; ?>
				        	<?php if ($this->post->reported == 1): ?>	
				        	 	| <span class="wallfactory-icon wallfactory-reported"><?php echo  JText::_('reported'); ?></span>
						    <?php endif; ?>	   
						      
	            	  		</span>
	            	 	</span> 
	            	 	
			  </div>
		 		<span class="wallfactory-comment-form" id="comment-form<?php echo $this->post->id; ?>">
            	   <div class="cancel-comment-wrapper">
		              <a href="#" class="cancel-comment" rel="<?php echo $this->post->id; ?>" >&nbsp;</a>
		            </div>  
	            	<div>
            			<table cellspacing="0" cellpadding="0" width="80%">
            			  <tr>
            				<td class="comment-column">
            					<textarea name="comment<?php echo $this->post->id; ?>" id="comment<?php echo $this->post->id; ?>" rows="2" cols="2" 
            					style="background: none repeat scroll 0 0 #FFFFFF; width: 330px; height:30px; overflow:hidden;" ></textarea>
            				</td>
            				<td><a href="#" class="submit-comment" rel="<?php echo $this->post->id; ?>"><?php echo JText::_('Submit'); ?></a>
                  			</td>
            			  </tr>
            			</table>
            		</div>
		           <?php if ($this->user->guest && $this->wallSettings->allow_guest_comments): ?>  
		              <?php if ( ($this->wallSettings->allow_comments) && ($this->wallSettings->captcha_comment) ): ?>
	               		<div id="recaptcha_response_field_error" colspan="2" style="color: #ff0000; display: none;">&darr;&nbsp;<span></span>&nbsp;&darr;</div>
	                 	<div style="vertical-align: top;"><label for="recaptcha_response_field"><?php echo JText::_('Are you human'); ?>?</label>
	                   		<div><?php echo $this->captcha_html; ?></div>
	                 	</div>
	              	  <?php endif; ?>
		           <?php endif; ?>     
		              <div id="wallfactory-comment-loader<?php echo $this->post->id; ?>" class="wallfactory-none">
		                <img alt="ajax-loader" src="components/com_wallfactory/assets/images/ajax-loader-blue.gif" class="wallfactory-icon" />
		                <?php echo JText::_('Processing form...'); ?>
		              </div>
               </span>
            
		</div><!-- action-bar -->
		
		<div class="wallfactory-post-content">	
	  	     <div class="post_message">
       			<div style="padding: 2px 3px; font-size: 12px;"><?php echo nl2br(html_entity_decode($this->post->content)); ?></div>
   		 	</div>
   		 </div>
	      
      <div id="wallfactory-comment-loader<?php echo $this->post->id; ?>" class="wallfactory-none">
        <img alt="ajax-loader" src="components/com_wallfactory/assets/images/ajax-loader-blue.gif" class="wallfactory-icon" />
        <?php echo JText::_('Processing form...'); ?>
      </div>
   


	</div>
	<div class="wallfactory-clear"></div>	 
					          	
  	
</div>
<?php endif; ?>

<div class="wallfactory-clear"></div> 

<div class="main_message" style="background-color: #fff;">
<?php if (!count($this->comments)): ?>
  <?php echo JText::_('No comments found! Be the first one to write a comment!'); ?>
<?php else: ?>  
<div id="wallfactory-comments<?php echo $this->post->id; ?>" class="<?php echo $this->post->id; ?>">
<div class="update"></div>   
<div class="comments_list">
	<table class="table">
	  <?php foreach ($this->comments as $comment): ?>
	     
	    <tr class="wallfactory-comment-text">
	    	<td>
	    		<div class="wallfactory-comment">
	   	       		<div class="wallfactory-comments-latest">	
			   			<div class="wallfactory-comment-text" class="wallfactory-left wallfactory-maxwidth">
				   			<span style="width: 20%">
						   		<?php if ($comment->user_id != 0):  ?>	
					          		 <a href="<?php echo JRoute::_('index.php?option=com_wallfactory&Itemid=' . $this->Itemid . '&view=wall&alias=' . $comment->author_alias); ?>" class="comment-alias">	
					          			<?php echo $comment->author_alias; ?>
					          		 </a> 
					          	<?php else: ?>
					          		<?php echo JText::_('Guest'); ?>
					          	<?php endif; ?>	
					         </span> 		 
		          			 <!--<span style="float: right;">-->
		          				<span class="wallfactory-year wallfactory-action"><?php echo date('H:m:s a l, d F Y', JText::_(strtotime($comment->date_created))); ?></span>
		          			 	<?php if ($this->user->id == $comment->user_id): ?>
						          <span class="wallfactory-comment-report">
						            <img src="<?php echo JURI::root(); ?>components/com_wallfactory/assets/images/comment_delete.png" class="wallfactory-icon" />
						            <a href="<?php echo JUri::root(); ?>index.php?option=com_wallfactory&controller=comment&task=delete&format=raw&type=2&alias=<?php echo $this->post->alias;?>&id=<?php echo $comment->id; ?>" >
						            <?php echo JText::_('delete'); ?></a>
						          </span>
						     	<?php endif; ?>
						     	<span class="wallfactory-comment-report">
							     	<?php if ($this->user->id != $comment->user_id): ?>
			          			 		<?php if (!$this->user->guest && $comment->reported == 0): ?>					
										   <span class="wallfactory-icon wallfactory-report">
										 	 <a  href="<?php echo JUri::root(); ?>index.php?option=com_wallfactory&controller=comment&task=report&format=raw&type=2&alias=<?php echo $this->post->alias;?>&id=<?php echo $comment->id; ?>" rel="<?php echo $comment->id; ?>">
								          	<?php echo JText::_('report'); ?></a>
							          	  </span>
							     			<?php endif; ?>
							     	<?php endif; ?>
							     	<?php if ($comment->reported == 1): ?>
							        	   <span class="wallfactory-icon wallfactory-reported"><?php echo  JText::_('reported'); ?></span>
							       	<?php endif; ?>
	      			 	   		</span>
	      			 	</div>
	      			 	<div class="wallfactory-clear"></div>
	      			 	<div><span class="wallfactory-comment-content"><?php echo $comment->content; ?></span></div>
			   			
			  		</div>
	 		  		<div class="wallfactory-clear"></div>  
		  	  </div>
	    
	     	</td>
	    </tr>
	  <?php endforeach; ?>
	</table>
	</div>
</div>	
<?php endif; ?>
</div>

<div id="pagination">
	<?php echo $this->pagination->getPagesLinks(); ?>
</div>
<script>
  var root = "<?php echo JUri::root(); ?>";
</script>



<script>
  jQueryFactory(document).ready(function ($) {
   
    // Report comment
    $(".report a").click(function () {
      var id     = $(this).parent().attr("id");
      var parent = $(this).parent();

      id = id.split("_");
      id = id[1];

      $.post("<?php echo JRoute::_('index.php?option=com_wallfactory&controller=comment&task=report'); ?>", {
        id:     id,
        format: "raw"
      }, function (response) {
        switch (response.status)
        {
          case 1:
            alert(response.message);
          break;

          case 2:
            parent.html(response.message);
          break;
        }
      }, "json");

      return false;
    });

    // hide wall comments
     $("#comments_list").hide();
      
      

  });
</script>