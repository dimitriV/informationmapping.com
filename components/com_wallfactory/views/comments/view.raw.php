<?php
/*------------------------------------------------------------------------
com_wallfactory - Wall Factory
------------------------------------------------------------------------
author    TheFactory
copyright Copyright (C) 2010 SKEPSIS Consult SRL. All Rights Reserved.
@license - http://www.gnu.org/licenses/gpl-2.0.html GNU/GPL
Websites: http://www.thefactory.ro
Technical Support:  Forum - http://www.thefactory.ro/joomla-forum/
-------------------------------------------------------------------------*/

defined('_JEXEC') or die('Restricted access');

jimport('joomla.application.component.view');

class FrontendViewComments extends JView
{
  function __construct()
  {
    parent::__construct();

    $this->wallHelper   = wallHelper::getInstance();
    $this->wallSettings = new wallSettings();
  }

  function display($tpl = null)
  {
    global $Itemid;  
    
  	$user       =& JFactory::getUser();
    
    $comments   =& $this->get('Data');
    $pagination =& $this->get('Pagination');
    $post   	=& $this->get('Post');

    $this->assignRef('comments',   $comments);
    $this->assignRef('pagination', $pagination);
    $this->assignRef('user',       $user);
    $this->assignRef('post',       		$post);
    $this->assignRef('Itemid',     		$Itemid);
    $this->assignRef('wallSettings',	$this->wallSettings);

    parent::display($tpl);
  }
}