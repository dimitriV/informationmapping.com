<?php
/*------------------------------------------------------------------------
com_wallfactory - Wall Factory
------------------------------------------------------------------------
author    TheFactory
copyright Copyright (C) 2010 SKEPSIS Consult SRL. All Rights Reserved.
@license - http://www.gnu.org/licenses/gpl-2.0.html GNU/GPL
Websites: http://www.thefactory.ro
Technical Support:  Forum - http://www.thefactory.ro/joomla-forum/
-------------------------------------------------------------------------*/

defined('_JEXEC') or die('Restricted access');

jimport('joomla.application.component.view');

class FrontendViewComments extends JView
{
  function __construct()
  {
    parent::__construct();

    $this->wallHelper   = wallHelper::getInstance();
    $this->wallSettings = new wallSettings();
  }

  function display($tpl = null)
  {
  	require_once(JPATH_COMPONENT_ADMINISTRATOR.DS.'lib'.DS.'recaptcha'.DS.'recaptchalib.php');
  	global $Itemid;  
  	
  	wallHelper::checkGuestView();
  	
    $user       =& JFactory::getUser();
    
    $comments   =& $this->get('Data');
    $pagination =& $this->get('Pagination');
    $post   	=& $this->get('Post');

    $session  =& JFactory::getSession();
    $text_message = $session->get('message');    
       
    $this->assignRef('comments',   		$comments);
    $this->assignRef('pagination', 		$pagination);
    $this->assignRef('user',       		$user);
    $this->assignRef('post',       		$post);
    $this->assignRef('Itemid',     		$Itemid);
    $this->assignRef('wallSettings',	$this->wallSettings);
    $this->assignRef ( 'captcha_comment', 	$this->wallSettings->captcha_comment );
    $this->assignRef('text_message',     	$text_message);

    if ($this->wallSettings->captcha_comment)
    {
    	$this->assignRef('captcha_html', recaptcha_get_html(base64_decode($this->wallSettings->recaptcha_public_key)));
    }
    
    JHtmlFactory::jQueryScript();
    JHTML::script('default.js', 'components/com_wallfactory/assets/js/views/post/');
    JHTML::_('script', 'main.js', 'components/com_wallfactory/assets/js/');
    JHTML::_('script', 'comments.js', 'components/com_wallfactory/assets/js/');
    JHTML::_('script', 'jquery.noconflict.js', 'components/com_wallfactory/assets/js/');

    
    if (! is_null ( $post )) {
		$declarations = array (
		'txt_field_required'    => JText::_('This field is required'),
		'route_post_comment' 	=> JRoute::_ ( 'index.php?option=com_wallfactory&controller=comment&task=addComment' ), 
		'route_report_comment' => JRoute::_ ( 'index.php?option=com_wallfactory&controller=comment&task=report' ), 
		'txt_loading_comments' => JText::_ ( 'Loading comments...' ), 
		'is_guest'			=> $user->guest,
		'captcha_comment' => $this->wallSettings->captcha_comment 
		);
		
		wallHelper::addScriptDeclarations ( $declarations );
	}
        
    JHTML::stylesheet('jquery-ui-1.7.2.custom.css', 'components/com_wallfactory/assets/css/');
    JHTML::stylesheet('buttons.css',                'components/com_wallfactory/assets/css/');
    JHTML::stylesheet('post.css',                   'components/com_wallfactory/assets/css/views/');
    JHTML::stylesheet('main.css',                   'components/com_wallfactory/assets/css/');
    JHTML::stylesheet('comments.css',               'components/com_wallfactory/assets/css/views/');
    
    parent::display($tpl);
  }
}