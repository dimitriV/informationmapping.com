<?php
/*------------------------------------------------------------------------
com_wallfactory - Wall Factory
------------------------------------------------------------------------
author    TheFactory
copyright Copyright (C) 2010 SKEPSIS Consult SRL. All Rights Reserved.
@license - http://www.gnu.org/licenses/gpl-2.0.html GNU/GPL
Websites: http://www.thefactory.ro
Technical Support:  Forum - http://www.thefactory.ro/joomla-forum/
-------------------------------------------------------------------------*/

defined('_JEXEC') or die('Restricted access');

class FrontendControllerComment extends FrontendController
{
	function __construct()
	{
		parent::__construct();

	}

	function addComment()
	{
	  $database =& JFactory::getDBO();
	  
  	  $post_id  =& JRequest::getVar('post_id', 0, 'REQUEST', 'integer');
	  $content = $database->getEscaped(JRequest::getVar('comment', '', 'REQUEST', 'string'));
	  
	  $model =& $this->getModel('comment');
	  $model->store($post_id,$content);
	}

	function addGuestComment()
	{
	  global $mainframe, $Itemid;
	  
	  $database =& JFactory::getDBO();
	  $user     =& JFactory::getUser();
	  
  	  $post_id  =& JRequest::getVar('post_id', 0, 'REQUEST', 'integer');
	  $content = $database->getEscaped(JRequest::getVar('comment', '', 'REQUEST', 'string'));
	   
	  $query = ' SELECT IF(p.alias <> "", p.alias,"' . JText::_('Guest') . '") AS alias'
	           . ' FROM #__wallfactory_posts p'
	           . ' WHERE p.id ='.$post_id;
	    $database->setQuery($query);
	    $alias = $database->loadResult();
	    
	    // Register session for message
        $session =& JFactory::getSession();
        if ($session->get('message'))
				$session->clear('message');		
    	$session->set('message', $content);
	 	  
	    header('Cache-Control: no-cache, must-revalidate');
	    header('Expires: Mon, 26 Jul 1997 05:00:00 GMT');
	    header('Content-type: application/json');
	    
	    die('{ "alias": "'.$alias.'", "message":"'.$content.'" }');
	}
	
	function report()
	{
	  global $mainframe, $Itemid;
		
	  $id       =& JRequest::getVar('id', 0, 'GET', 'integer');
	  $alias 	=& JRequest::getVar('alias', '', 'GET', 'string');
	  
	  $user     =& JFactory::getUser();
	  $database =& JFactory::getDBO();

	  // Find the comment
	  $query = ' SELECT c.*, w.report_comment_notification,'
	         . ' p.id AS post_id, w.title AS wall_title, w.alias, w.id AS wall_id,'
	         . ' w.user_id AS wall_owner_id'
	         . ' FROM #__wallfactory_comments c'
	         . ' LEFT JOIN #__wallfactory_posts p ON p.id = c.post_id'
	         . ' LEFT JOIN #__wallfactory_walls w ON w.id = p.wall_id'
	         . ' WHERE c.id = ' . $id
	         . ' AND c.approved = 1'
	         . ' AND c.reported = 0';
	  $database->setQuery($query);
	  $comment = $database->loadObject();
	  
	  // Comment not found
	  if (!$comment) {
	    	$msg = 'Comment not found or already reported!';
	    	$mainframe->redirect(JRoute::_('index.php?option=com_wallfactory&view=wall&alias='.$alias.'&Itemid=' . $Itemid), $msg); 
	
	  }
	  else  {
     	$msg = 'Comment reported';
	  	// Report the comment
	  	
	  	$query = ' UPDATE #__wallfactory_comments'
	         . ' SET reported = 1, reporting_user_id = ' . $user->id
	         . ' WHERE id = ' . $id;
	  	$database->setQuery($query);
	  	$database->query();

		  // Send notification
		  if ($this->wallSettings->enable_notification_new_report_comment)
		  {
		    $this->sendReportNotification($comment);
		  }

		$mainframe->redirect(JRoute::_('index.php?option=com_wallfactory&view=wall&alias='.$alias.'&Itemid=' . $Itemid), $msg); 
	  	//die('{"status": 2, "message": "' . JText::_('(reported)') . '"}');
		}
	}

	function delete() {
		
	  global $mainframe, $Itemid;
	  
	  $database =& JFactory::getDBO();
	  
      $comment_id  =& JRequest::getVar('id', 0, 'REQUEST', 'integer');
      $alias = JRequest::getVar('alias', '', 'GET', 'string');
	  
	  $model =& $this->getModel('comment');
	  $result = $model->delete($comment_id);
	  
	  if ($result)
     	$msg = 'Comment deleted';
      else 
     	$msg = 'Comment not found';
    
    $mainframe->redirect(JRoute::_('index.php?option=com_wallfactory&view=wall&alias='.$alias.'&Itemid=' . $Itemid), $msg);
	  
	}
	
	function sendReportNotification($comment)
	{
	  $database =& JFactory::getDBO();
	  $array    =  array();

	  // Send report to admins
	  if (count($this->wallSettings->notification_new_report_comment_receivers))
	  {
	    $array = $this->wallSettings->notification_new_report_comment_receivers;
	  }

	  // Send report to wall owner
	  if ($comment->report_comment_notification)
	  {
	    $array[] = $comment->wall_owner_id;
	  }

	  if (count($array))
	  {
	    $query = ' SELECT u.username, u.email'
	           . ' FROM #__users u'
	           . ' WHERE u.id IN (' . implode(', ', $array) . ')';
	    $database->setQuery($query);
	    $receivers = $database->loadObjectList();

	    $subject = base64_decode($this->wallSettings->notification_new_report_comment_subject);
	    $message = base64_decode($this->wallSettings->notification_new_report_comment_message);

	    $search  = array('%%username%%', '%%walltitle%%', '%%commenttext%%', '%%postlink%%', '%%walllink%%', '%%commentlink%%');

	    foreach ($receivers as $receiver)
	    {
	      $replace = array(
	        $receiver->username,
	        $comment->wall_title,
	        $comment->content
	      );

	      if ($this->wallSettings->enable_notification_email_html)
	      {
	        $replace[] = '<a href="' . JRoute::_(JURI::root() . 'index.php?option=com_wallfactory&view=post&id=' . $comment->post_id. '&alias='.$comment->alias) . '">' . $comment->alias . '</a>';
	        $replace[] = '<a href="' . JRoute::_(JURI::root() . 'index.php?option=com_wallfactory&view=wall&alias=' . $comment->alias) . '">' . $comment->wall_title . '</a>';
	        //$replace[] = '<a href="' . JRoute::_(JURI::root() . 'index.php?option=com_wallfactory&view=post&id=' . $comment->post_id) . '#comments">' . $comment->post_title . '</a>';
	      }
	      else
	      {
	        $replace[] = JRoute::_(JURI::root() . 'index.php?option=com_wallfactory&view=post&id=' . $comment->post_id. '&alias='.$comment->alias);
	        $replace[] = JRoute::_(JURI::root() . 'index.php?option=com_wallfactory&view=wall&alias=' . $comment->alias);
	        //$replace[] = JRoute::_(JURI::root() . 'index.php?option=com_wallfactory&view=post&id=' . $comment->post_id . '#comments');
	      }

	      $custom_message = str_replace($search, $replace, $message);
	      $custom_subject = str_replace($search, $replace, $subject);

	      if ($this->wallSettings->enable_notification_email_html)
	      {
	        $custom_message = nl2br($custom_message);
	      }

	      $mail =& JFactory::getMailer();
	      $mail->addRecipient($receiver->email);
	      $mail->setSubject(JText::_($custom_subject));
	      $mail->setBody(JText::_($custom_message));
	      $mail->IsHTML($this->wallSettings->enable_notification_email_html);

	      $mail->send();
	    }
	  }
	}
	
	
  function getTotalComments($id)
  {
    $id       =& JRequest::getVar('id', 0, 'POST', 'integer');
	$user     =& JFactory::getUser();
	$database =& JFactory::getDBO();

    $query = ' SELECT COUNT(id)'
           . ' FROM #__wallfactory_comments'
           . ' WHERE post_id = ' . $id
          ;
    $database->setQuery($query);

    return $database->loadResult();
  }

  function getPagination()
  {
    jimport('joomla.html.pagination');

    $pagination = new JPagination($this->total, $this->limitstart, $this->comments_per_page);
    return $pagination;
  }
    
  function getComments()
  {
    
  	  $user 	=& JFactory::getUser();
  	  $post_id  =& JRequest::getVar('id', 0, 'GET', 'integer');
  	  $database =& JFactory::getDBO();
  	  
	  if ($user->guest)
	  {
	    die('{ "errors": 1, "message": "' . JText::_('You need to login') . '"}');
	  }

	  $view  =& $this->getView('comments', 'raw');
      $model =& $this->getModel('comments');
				
      $view->setModel($model, true);
      $view->display();
 
  }
	
	
}