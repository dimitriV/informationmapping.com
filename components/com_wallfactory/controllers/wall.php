<?php
/*------------------------------------------------------------------------
com_wallfactory - Wall Factory
------------------------------------------------------------------------
author    TheFactory
copyright Copyright (C) 2010 SKEPSIS Consult SRL. All Rights Reserved.
@license - http://www.gnu.org/licenses/gpl-2.0.html GNU/GPL
Websites: http://www.thefactory.ro
Technical Support:  Forum - http://www.thefactory.ro/joomla-forum/
-------------------------------------------------------------------------*/

defined('_JEXEC') or die('Restricted access');

class FrontendControllerWall extends FrontendController
{
	function __construct()
	{
		parent::__construct();
	}

	function follow()
	{
	  $id       =& JRequest::getVar('id', 0, 'POST', 'integer');
	  $user     =& JFactory::getUser();
	  $database =& JFactory::getDBO();

	  // Check if user is logged in
	  if ($user->guest)
	  {
	    die('{ "status": 0, "message": "' . JText::_('You must login') . '"}');
	  }

	  // Find the wall
	  $query = ' SELECT w.id, f.id AS followed_id'
	         . ' FROM #__wallfactory_walls w'
	         . ' LEFT JOIN #__wallfactory_followers f ON f.wall_id = ' . $id . ' AND f.user_id = ' . $user->id
	         . ' WHERE w.id = ' . $id;
    $database->setQuery($query);
    $result = $database->loadObject();

    header('Cache-Control: no-cache, must-revalidate');
    header('Expires: Mon, 26 Jul 1997 05:00:00 GMT');
    header('Content-type: application/json');

    if (!$result)
    {
      die('{ "status": 0, "message": "' . JText::_('Wall not found') . '"}');
    }

    JTable::addIncludePath(JPATH_COMPONENT_ADMINISTRATOR.DS.'tables');
    $follower =& JTable::getInstance('followers', 'Table');

    if ($result->followed_id)
    {
      $follower->delete($result->followed_id);

      die('{ "status": 1, "message": "' . JText::_('unsubscribed from 1 wall') . '"}');
    }
    else
    {
      $date =& JFactory::getDate();

      $follower->wall_id      = $id;
      $follower->user_id      = $user->id;
      $follower->notification = 1;
      $follower->date_created   = $date->toMySql();

      $follower->store();

      die('{ "status": 1, "message": "' . JText::_('subscribed to 1 wall') . '"}');
    }
	}
	
	function update_media()
	{
	  global $mainframe, $Itemid;
	  
	  $user     =& JFactory::getUser();
	  
	  $model =& $this->getModel('wall');

	  $wall = $model->getWallSettings();
	  
	  
	  if ($model->storeMedia())
	  {
	    $msg = JText::_('Media Saved!');
	  }
	  else
	  {
	    $msg = JText::_('Error Saving Media!');
	  }

	  $mainframe->redirect(JRoute::_('index.php?option=com_wallfactory&view=wall&Itemid=' . $Itemid), $msg);
	  
	}
	
	function update_urls()
	{
	  global $mainframe, $Itemid;
	  
	  $user     =& JFactory::getUser();
	  
	  $model =& $this->getModel('wall');

	  $wall = $model->getWallSettings();
	  
	  
	  if ($model->storeUrls())
	  {
	    $msg = JText::_('URLs Saved!');
	  }
	  else
	  {
	    $msg = JText::_('Error Saving URLs!');
	  }

	  $mainframe->redirect(JRoute::_('index.php?option=com_wallfactory&view=wall&Itemid=' . $Itemid), $msg);
	  
	}
	
}