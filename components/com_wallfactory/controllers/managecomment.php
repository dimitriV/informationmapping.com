<?php
/*------------------------------------------------------------------------
com_wallfactory - Wall Factory
------------------------------------------------------------------------
author    TheFactory
copyright Copyright (C) 2010 SKEPSIS Consult SRL. All Rights Reserved.
@license - http://www.gnu.org/licenses/gpl-2.0.html GNU/GPL
Websites: http://www.thefactory.ro
Technical Support:  Forum - http://www.thefactory.ro/joomla-forum/
-------------------------------------------------------------------------*/

defined('_JEXEC') or die('Restricted access');

class FrontendControllerManageComment extends FrontendController
{
	function __construct()
	{
		parent::__construct();
	}

	function approve()
	{
	  if ($this->wallSettings->approve_status == 1)
	  {
	    die('{ "status": 1, "message": "' . JText::_('Only admins can approve comments!') . '" }');
	  }

	  $id       =& JRequest::getVar('id', 0, 'POST', 'integer');
	  $user     =& JFactory::getUser();
	  $database =& JFactory::getDBO();

	  $query = ' SELECT c.id'
	         . ' FROM #__wallfactory_comments c'
	         . ' LEFT JOIN #__wallfactory_posts p ON p.id = c.post_id'
	         . ' LEFT JOIN #__wallfactory_walls w ON w.id = p.wall_id'
	         . ' WHERE c.id = ' . $id
	         . ' AND w.user_id = ' . $user->id;
	  $database->setQuery($query);
	  $comment = $database->loadResult();

	  if (!$comment)
	  {
	    die('{ "status": 1, "message": "Comment not found!" }');
	  }

	  $query = ' UPDATE #__wallfactory_comments c'
	         . ' SET c.approved = !c.approved'
	         . ' WHERE c.id = ' . $id;
	  $database->setQuery($query);

	  $database->query();

	  die(' { "status" : 2 }');
	}

	function delete()
	{
	  $id       =& JRequest::getVar('id', 0, 'POST', 'integer');
	  $user     =& JFactory::getUser();
	  $database =& JFactory::getDBO();

	  $query = ' SELECT c.id'
	         . ' FROM #__wallfactory_comments c'
	         . ' LEFT JOIN #__wallfactory_posts p ON p.id = c.post_id'
	         . ' LEFT JOIN #__wallfactory_walls w ON w.id = p.wall_id'
	         . ' WHERE c.id = ' . $id
	         . ' AND w.user_id = ' . $user->id;
	  $database->setQuery($query);
	  $comment = $database->loadResult();

	  if (!$comment)
	  {
	    die('{ "status": 1, "message": "Comment not found!" }');
	  }


	  // Delete comment
	  $query = ' DELETE'
	         . ' FROM #__wallfactory_comments'
	         . ' WHERE id = ' . $id;
	  $database->setQuery($query);
	  $database->query();

	  die(' { "status" : 2, "message": "comment deleted" }');
	}

	function executeBatchAction($action, $message)
	{
	  $database =& JFactory::getDBO();
	  $ids      =& JRequest::getVar('comment', array(), 'POST', 'array');
	  $user     =& JFactory::getUser();

	  $ids = array_keys($ids);

	  // Validate the ids received
	  $query = ' SELECT c.id'
	         . ' FROM #__wallfactory_comments c'
	         . ' LEFT JOIN #__wallfactory_posts p ON p.id = c.post_id'
	         . ' LEFT JOIN #__wallfactory_walls w ON w.id = p.wall_id'
	         . ' WHERE c.id IN (' . implode(', ', $ids) . ')'
	         . ' AND w.user_id = ' . $user->id;
    $database->setQuery($query);
    $ids = $database->loadResultArray();

    switch (count($ids))
    {
      case 0:
        $message = JText::_('No comment has');
        break;

      case 1:
        $message = JText::_('1 comment has');
        break;

      default:
        $message = count($ids) . ' ' . JText::_('comments have');
        break;
    }

    // We now have only the valid ids
    switch ($action)
    {
      case 'approve':
        $query = ' UPDATE #__wallfactory_comments'
    	         . ' SET approved = 1';
    	  $message .= ' ' . JText::_('been approved!');
    	break;

    	case 'unapprove':
        $query = ' UPDATE #__wallfactory_comments'
    	         . ' SET approved = 0';
    	  $message .= ' ' . JText::_('been unapproved!');
    	break;

    	case 'delete':
    	  $query = ' DELETE'
    	         . ' FROM #__wallfactory_comments';
    	  $message .= ' ' . JText::_('been deleted!');
      break;
    }

    $query .= ' WHERE id IN (' . @implode(', ', $ids) . ')';

    $database->setQuery($query);
    $database->query();

    header('Cache-Control: no-cache, must-revalidate');
    header('Expires: Mon, 26 Jul 1997 05:00:00 GMT');
    header('Content-type: application/json');

    die('{"action": "' . $action . '", "status": 1, "message": "' . $message . '", "ids": [' . @implode(',', $ids) . ']}');
	}

	function batchUnapprove()
	{
	  if ($this->wallSettings->approve_status == 1)
	  {
	    die('{ "status": 0, "message": "' . JText::_('Only admins can approve comments!') . '" }');
	  }

	  $this->executeBatchAction('unapprove', JText::_('Comment(s) Unapproved!'));
	}

	function batchApprove()
	{
	  if ($this->wallSettings->approve_status == 1)
	  {
	    die('{ "status": 0, "message": "' . JText::_('Only admins can approve comments!') . '" }');
	  }

	  $this->executeBatchAction('approve', JText::_('Comment(s) Approved!'));
	}

	function batchDelete()
	{
	  $this->executeBatchAction('delete', JText::_('Comment(s) Deleted!'));
	}

	function filter()
	{
	  $filter = JRequest::getVar('filter', '', 'POST', 'string');
	  $type   = JRequest::getVar('type',   -1, 'POST', 'integer');

    $session =& JFactory::getSession();
    $session->set('filter.comments', $filter);
    $session->set('filter.comments.type', $type);
	}

  function filterPost()
	{
	  $id       =& JRequest::getVar('id', 0, 'POST', 'integer');
	  $database =& JFactory::getDBO();
	  $user     =& JFactory::getUser();

	  if ($id == 0)
	  {
	    $session =& JFactory::getSession();
      $session->set('filter.post_id', 0);
      $session->set('filter.post_title', '');

      return;
	  }

	  $query = ' SELECT p.title'
	         . ' FROM #__wallfactory_posts p'
	         . ' LEFT JOIN #__wallfactory_walls w ON w.id = p.wall_id'
	         . ' WHERE p.id = ' . $id
	         . ' AND w.user_id = ' . $user->id;
	  $database->setQuery($query);
	  $title = $database->loadResult();

	  if ($title)
	  {
      $session =& JFactory::getSession();
      $session->set('filter.post_id', $id);
      $session->set('filter.post_title', $title);
	  }
	}

	function quickSave()
	{
	  $database =& JFactory::getDBO();
	  $id       =& JRequest::getVar('id', 0, 'POST', 'integer');
	  $content   =& JRequest::getVar('content', '', 'POST', 'string');
	  $user     =& JFactory::getUser();

	  $query = ' SELECT c.id'
	         . ' FROM #__wallfactory_comments c'
	         . ' LEFT JOIN #__wallfactory_posts p ON p.id = c.post_id'
	         . ' LEFT JOIN #__wallfactory_walls w ON w.id = p.wall_id'
	         . ' WHERE w.user_id = ' . $user->id
	         . ' AND c.id = ' . $id;
	  $database->setQuery($query);

	  if (!$database->loadResult())
	  {
	    die('{"status": 0, "message": "' . JText::_('Comment not available!') . '"}');
	  }

	  $query = ' UPDATE #__wallfactory_comments'
	         . ' SET content = "' . $content . '"'
	         . ' WHERE id = ' . $id;
	  $database->setQuery($query);
	  $database->query();

	  die('{"status": 1, "message": "' . JText::_('Comment saved!') . '"}');
	}

	function save()
	{
	  header('Cache-Control: no-cache, must-revalidate');
    header('Expires: Mon, 26 Jul 1997 05:00:00 GMT');
    header('Content-type: application/json');

	  $user     =& JFactory::getUser();
	  $id       =& JRequest::getVar('id', 0, 'POST', 'integer');
	  $database =& JFactory::getDBO();

	  // Check if the comment exists and if the current user has access to it
	  $query = ' SELECT b.id'
	         . ' FROM #__wallfactory_comments c'
	         . ' LEFT JOIN #__wallfactory_posts p ON p.id = c.post_id'
	         . ' LEFT JOIN #__wallfactory_walls w ON w.id = p.wall_id'
	         . ' WHERE c.id = ' . $id
	         . ' AND w.user_id = ' . $user->id;
	  $database->setQuery($query);

	  if (!$database->loadResult())
	  {
	    // No access
	    die('{"status" : 0, "message": "' . JText::_('Comment not found') . '" }');
	  }

	  // Update comment
	  JTable::addIncludePath(JPATH_COMPONENT_ADMINISTRATOR.DS.'tables');
	  $comment =& JTable::getInstance('comment', 'Table');
	  $data    =& JRequest::get('post');

	  if (!$comment->bind($data))
    {
      die('{"status" : 0, "message": "' . JText::_('error saving comment') . '" }');
    }

    if (!$comment->check())
    {
      die('{"status" : 0, "message": "' . JText::_('error saving comment') . '" }');
    }

    if (!$comment->store())
    {
      die('{"status" : 0, "message": "' . JText::_('error saving comment') . '" }');
    }

    die('{"status" : 1, "message": "' . JText::_('comment saved') . '" }');
	}

	function removeReport()
	{
	  $id       =& JRequest::getVar('id', 0, 'POST', 'integer');
	  $user     =& JFactory::getUser();
	  $database =& JFactory::getDBO();

	  $query = ' SELECT c.id'
	         . ' FROM #__wallfactory_comments c'
	         . ' LEFT JOIN #__wallfactory_posts p ON p.id = c.post_id'
	         . ' LEFT JOIN #__wallfactory_walls w ON w.id = p.wall_id'
	         . ' WHERE w.user_id = ' . $user->id
	         . ' AND c.id = ' . $id;
	  $database->setQuery($query);
	  $comment = $database->loadResult();

	  if (!$comment)
	  {
	    die('{ "status": 1, "message": "' . JText::_('Comment not found') . '" }');
	  }

	  $query = ' UPDATE #__wallfactory_comments'
	         . ' SET reported = 0, reporting_user_id = 0'
	         . ' WHERE id = ' . $id;
	  $database->setQuery($query);
	  $database->query();

	  die('{ "status": 2, "message": "' . JText::_('Report removed') . '" }');
	}
}