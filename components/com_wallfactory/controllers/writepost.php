<?php
/*------------------------------------------------------------------------
com_wallfactory - Wall Factory
------------------------------------------------------------------------
author    TheFactory
copyright Copyright (C) 2010 SKEPSIS Consult SRL. All Rights Reserved.
@license - http://www.gnu.org/licenses/gpl-2.0.html GNU/GPL
Websites: http://www.thefactory.ro
Technical Support:  Forum - http://www.thefactory.ro/joomla-forum/
-------------------------------------------------------------------------*/

defined('_JEXEC') or die('Restricted access');

class FrontendControllerWritepost extends FrontendController
{
  function __construct()
	{
		parent::__construct();
		
	}

  function save()
  {
    global $Itemid;

    $model = $this->getModel('writepost');

    $post = $model->store();

    if ($post === false)
    {
      echo '{ "status": 0 }';
    }
    else
    {
      echo '{ "status": 1, "id": ' . $post->id . ', "last_saved": "' . $post->updated_at . '", "permalink": "' . $post->alias . '", "pinged": "' . str_replace("\n", "<br />", trim($post->pinged, "\n")) . '"}';
    }
  }
  
  function deleteMedia()
  {
    global $mainframe, $Itemid;

    $alias = JRequest::getVar('alias', '', 'GET', 'string');
    $media_id = JRequest::getVar('media_id', '', 'GET', 'int');
    
    $model = $this->getModel('writepost');

    $media_record = $model->deleteMedia();

    if ($media_record)
     $msg = 'Media record deleted';
    else 
     $msg = 'Media record not found';
    
    $mainframe->redirect(JRoute::_('index.php?option=com_wallfactory&view=wall&alias='.$alias.'&Itemid=' . $Itemid), $msg);
  }
	
  function deleteUrl()
  {
    global $mainframe, $Itemid;

    $alias = JRequest::getVar('alias', '', 'GET', 'string');
    $link_id = JRequest::getVar('link_id', '', 'GET', 'int');
    $type    =& JRequest::getVar('type', 0, 'GET', 'int');
    
    $model = $this->getModel('writepost');

    $media_record = $model->deleteUrl();

    if ($media_record)
     $msg = 'Link deleted';
    else 
     $msg = 'Link not found';
    
    $mainframe->redirect(JRoute::_('index.php?option=com_wallfactory&view=wall&alias='.$alias.'&Itemid=' . $Itemid), $msg);
  }
  
}