<?php
/*------------------------------------------------------------------------
com_wallfactory - Wall Factory
------------------------------------------------------------------------
author    TheFactory
copyright Copyright (C) 2010 SKEPSIS Consult SRL. All Rights Reserved.
@license - http://www.gnu.org/licenses/gpl-2.0.html GNU/GPL
Websites: http://www.thefactory.ro
Technical Support:  Forum - http://www.thefactory.ro/joomla-forum/
-------------------------------------------------------------------------*/

defined('_JEXEC') or die('Restricted access');

class FrontendControllerManageFollowed extends FrontendController
{
	function __construct()
	{
		parent::__construct();
	}

	function notification()
	{
	  if (!$this->wallSettings->enable_notification_follow_wall)
	  {
	    die('{ "errors": "1", "message": "' . JText::_('not allowed') . '" }');
	  }

	  $id       =& JRequest::getVar('id', 0, 'POST', 'integer');
	  $user     =& JFactory::getUser();
	  $database =& JFactory::getDBO();

	  $query = ' SELECT f.id'
	         . ' FROM #__wallfactory_followers f'
	         . ' WHERE f.user_id = ' . $user->id
	         . ' AND f.id = ' . $id;
	  $database->setQuery($query);
	  $wall = $database->loadResult();

	  if (!$wall)
	  {
	    die('{ "status": 1, "message": "' . JText::_('Followed wall not found!') . '" }');
	  }

	  $query = ' UPDATE #__wallfactory_followers f'
	         . ' SET f.notification = !f.notification'
	         . ' WHERE f.id = ' . $id;
	  $database->setQuery($query);

	  $database->query();

	  die(' { "status" : 2 }');
	}

	function delete()
	{
	  $id       =& JRequest::getVar('id', 0, 'POST', 'integer');
	  $user     =& JFactory::getUser();
	  $database =& JFactory::getDBO();

	  $query = ' SELECT f.id'
	         . ' FROM #__wallfactory_followers f'
	         . ' WHERE f.user_id = ' . $user->id
	         . ' AND f.id = ' . $id;
	         
	  $database->setQuery($query);
	  $wall = $database->loadResult();

	  if (!$wall)
	  {
	    die('{ "status": 1, "message": "' . JText::_('Followed wall not found!') . '" }');
	  }

	  $query = ' DELETE'
	         . ' FROM #__wallfactory_followers'
	         . ' WHERE id = ' . $id;
	  $database->setQuery($query);

	  $database->query();

	  die(' { "status" : 2 }');
	}

	function executeBatchAction($action)
	{
	  $database =& JFactory::getDBO();
	  $ids      =& JRequest::getVar('followed', array(), 'POST', 'array');
	  $user     =& JFactory::getUser();

	  $ids = array_keys($ids);

	  // Validate the ids received
	  $query = ' SELECT f.id'
	         . ' FROM #__wallfactory_followers f'
	         . ' WHERE f.id IN (' . implode(', ', $ids) . ')'
	         . ' AND f.user_id = ' . $user->id;
    $database->setQuery($query);
    $ids = $database->loadResultArray();

    switch (count($ids))
    {
      case 0:
        $message = JText::_('No notification has');
        break;

      case 1:
        $message = JText::_('1 notification has');
        break;

      default:
        $message = count($ids) . ' ' . JText::_('notifications have');
        break;
    }

    // We now have only the valid ids
    switch ($action)
    {
      case 'add_notification':
        $query = ' UPDATE #__wallfactory_followers'
    	         . ' SET notification = 1';
    	  $message .= ' ' . JText::_('been added!');
    	break;

    	case 'remove_notification':
        $query = ' UPDATE #__wallfactory_followers'
    	         . ' SET notification = 0';
    	  $message .= ' ' . JText::_('been removed!');
    	break;

    	case 'delete':
    	  $query = ' DELETE'
    	         . ' FROM #__wallfactory_followers';
    	  $message .= ' ' . JText::_('been deleted!');
      break;
    }

    $query .= ' WHERE id IN (' . @implode(', ', $ids) . ')';

    $database->setQuery($query);
    $database->query();

    header('Cache-Control: no-cache, must-revalidate');
    header('Expires: Mon, 26 Jul 1997 05:00:00 GMT');
    header('Content-type: application/json');

    die('{"action": "' . $action . '", "message": "' . $message . '", "ids": [' . @implode(',', $ids) . ']}');
	}

	function batchRemoveNotification()
	{
	  if (!$this->wallSettings->enable_notification_follow_wall)
	  {
	    die('{ "errors": "1", "message": "' . JText::_('not allowed') . '" }');
	  }

	  $this->executeBatchAction('remove_notification');
	}

	function batchAddNotification()
	{
	  if (!$this->wallSettings->enable_notification_follow_wall)
	  {
	    die('{ "errors": "1", "message": "' . JText::_('not allowed') . '" }');
	  }

	  $this->executeBatchAction('add_notification');
	}

	function batchDelete()
	{
	  $this->executeBatchAction('delete');
	}

	function filter()
	{
	  $notification = JRequest::getVar('notification', '', 'POST', 'string');

    $session =& JFactory::getSession();
    $session->set('filter.followed.notification', $notification);
	}
}