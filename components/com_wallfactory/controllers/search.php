<?php
/*------------------------------------------------------------------------
com_wallfactory - Wall Factory
------------------------------------------------------------------------
author    TheFactory
copyright Copyright (C) 2010 SKEPSIS Consult SRL. All Rights Reserved.
@license - http://www.gnu.org/licenses/gpl-2.0.html GNU/GPL
Websites: http://www.thefactory.ro
Technical Support:  Forum - http://www.thefactory.ro/joomla-forum/
-------------------------------------------------------------------------*/

defined('_JEXEC') or die('Restricted access');

class FrontendControllerSearch extends FrontendController
{
	function __construct()
	{
		parent::__construct();
	}

	function search()
	{
	
	  $q =& JRequest::getVar('q', '', 'GET', 'string');

	  $this->setRedirect(JRoute::_('index.php?option=com_wallfactory&view=search&q=' . urlencode($q), false));
	}
}