<?php
/*------------------------------------------------------------------------
com_wallfactory - Wall Factory
------------------------------------------------------------------------
author    TheFactory
copyright Copyright (C) 2010 SKEPSIS Consult SRL. All Rights Reserved.
@license - http://www.gnu.org/licenses/gpl-2.0.html GNU/GPL
Websites: http://www.thefactory.ro
Technical Support:  Forum - http://www.thefactory.ro/joomla-forum/
-------------------------------------------------------------------------*/

defined('_JEXEC') or die('Restricted access');

class FrontendControllerMywall extends FrontendController
{
	function __construct()
	{
		parent::__construct();
	}

	function availability()
	{
	  $model =& $this->getModel('mywall');

	//  if ($model->isTitleAvailable())
	  if ($model->isWallAvailable())
	  
	  {
	    $errors = 0;
	    $msg = JText::_('Available');
	  }
	  else
	  {
	    $errors = 1;
	    $msg = JText::_('Not Available');
	  }

	  header('Cache-Control: no-cache, must-revalidate');
      header('Content-type: application/json');

	  die('{"errors": ' . $errors . ', "message": "' . $msg . '"}');
	}

	function register()
	{
	  global $mainframe, $Itemid;

	  $model =& $this->getModel('mywall');

	  if ($model->register())
	  {
	    $msg = JText::_('Wall registered succesfuly!');
	    $redirect = JRoute::_('index.php?option=com_wallfactory&view=settings&Itemid=' . $Itemid);
	  }
	  else
	  {
	    $msg = JText::_('An error occurred! Please try again!');
	    $redirect = JRoute::_('index.php?option=com_wallfactory&view=newwall&Itemid=' . $Itemid);
	  }

	  $mainframe->redirect($redirect, $msg);
	}
}