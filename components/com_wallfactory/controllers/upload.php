<?php
/*------------------------------------------------------------------------
com_wallfactory - Wall Factory
------------------------------------------------------------------------
author    TheFactory
copyright Copyright (C) 2010 SKEPSIS Consult SRL. All Rights Reserved.
@license - http://www.gnu.org/licenses/gpl-2.0.html GNU/GPL
Websites: http://www.thefactory.ro
Technical Support:  Forum - http://www.thefactory.ro/joomla-forum/
-------------------------------------------------------------------------*/

defined('_JEXEC') or die('Restricted access');

class FrontendControllerUpload extends FrontendController
{
	function __construct()
	{
		parent::__construct();
	}

	function uploadAvatar()
	{
	  if (!$this->wallSettings->enable_avatars)
	  {
	    return false;
	  }

	  $database =& JFactory::getDBO();
	  $query = ' SELECT userid'
	         . ' FROM #__session'
	         . ' WHERE session_id = "' . $database->getEscaped($_POST['PHPSESSID']) . '"';
	  $database->setQuery($query);
	  $id = $database->loadResult();

	  $user =& JFactory::getUser($id);

	  if (!$user->guest)
	  {
	    require_once(JPATH_COMPONENT_ADMINISTRATOR.DS.'lib'.DS.'resize'.DS.'resize.class.php');
	    jimport('joomla.filesystem.file');
	    $photo = JRequest::getVar('avatar', array(), 'FILES', 'array');

	    $this->checkUserFolder($id);

	    $extension = strtolower(JFile::getExt($photo['name']));
	    $filepath = JPATH_SITE.DS.'components'.DS.'com_wallfactory'.DS.'storage'.DS.'users'.DS.$user->id.DS.'avatar'.DS.'avatar.'.$extension;

	    $resize = new RESIZEIMAGE($photo['tmp_name']);
	    $resize->resize_scale($this->wallSettings->avatar_max_width, $this->wallSettings->avatar_max_height, $filepath);
	    $resize->close();
	
	    $query = ' UPDATE #__wallfactory_members'
	           . ' SET avatar_extension = "' . $extension . '"'
	           . ' WHERE user_id = ' . $user->id;
	    $database->setQuery($query);
	    $database->query();
	  }
	}

	function getAvatar()
	{
	  $user     =& JFactory::getUser();
	  $database =& JFactory::getDBO();
	
	  $query = ' SELECT avatar_extension'
	           . ' FROM #__wallfactory_members'
	           . ' WHERE user_id = ' . $user->id;
	  $database->setQuery($query);
	  $extension = $database->loadResult();
    
	  die('<img src="' . JURI::root() . 'components/com_wallfactory/storage/users/' . $user->id . '/avatar/avatar.' . $extension . '?time=' . time() . '" />');
	}

	function getImage()
	{
	  $user     =& JFactory::getUser();
      $database =& JFactory::getDBO();

      $query = ' SELECT name, extension'
           . ' FROM #__wallfactory_media'
           . ' WHERE user_id = ' . $user->id;
      $database->setQuery($query);
      $image_info = $database->loadAssoc();
     
      $name = $image_info['name'];
      $extension = $image_info['extension'];
      
	  die('<img src="' . JURI::root() . 'components/com_wallfactory/storage/users/' . $user->id . '/images/'.$name.'.'. $extension . '" />');
	}
	
	function uploadFiles()
  	{
      $database =& JFactory::getDBO();
      $query = ' SELECT userid'
	         . ' FROM #__session'
	         . ' WHERE session_id = "' . $database->getEscaped($_POST['PHPSESSID']) . '"';
	  $database->setQuery($query);
	  $id = $database->loadResult();

	  $user =& JFactory::getUser($id);

    if (!$user->guest)
    {
      $this->checkUserFolder($id);

      $folder =  base64_decode(JRequest::getVar('folder', '', 'POST', 'string'));
	  $folder =  JPATH_SITE.DS.'components'.DS.'com_wallfactory'.DS.'storage'.DS.'users'.DS.$id.DS.'folder'.$folder.DS;

      $model      =& JModel::getInstance('media', 'FrontendModel');
      $free_space =  $model->getFreeSpace($id, 1);

      $photo    = JRequest::getVar('photo', array(), 'FILES', 'array');
      $location = $folder . $photo['name'];
      $thumb    = $folder . '_' . $photo['name'];

      // Check if there is enough space left to upload the photo
      //die('{ "free_space": "' . $free_space . '"}');
      if ($free_space > $photo['size'] || $free_space == -1)
      {
        require_once(JPATH_COMPONENT_ADMINISTRATOR.DS.'lib'.DS.'resize'.DS.'resize.class.php');

        $resize = new RESIZEIMAGE($photo['tmp_name']);
        $resize->resize_scale(100, 100, $thumb);
        $resize->close();

        move_uploaded_file($photo['tmp_name'], $location);

        $free_space = $model->getFreeSpace($id);
        die('{ "status": 1, "free_space": "' . $free_space . '"}');
      }

      $free_space = $model->getFreeSpace($id);
      die('{ "status": 0, "free_space": "' . $free_space . '", "message": "' . JText::_('Not enough free space') . '"}');
    }
  }

  function checkUserFolder($user_id)
  {
    $user_folder = $filepath = JPATH_SITE.DS.'components'.DS.'com_wallfactory'.DS.'storage'.DS.'users'.DS.$user_id;
    if (!is_dir($user_folder))
    {
      mkdir($user_folder);
    }

    $avatar_folder = $user_folder . DS . 'avatar';
    if (!is_dir($avatar_folder))
    {
      mkdir($avatar_folder);
    }

    $storage_folder = $user_folder . DS. 'folder';
    if (!is_dir($storage_folder))
    {
      mkdir($storage_folder);
    }
    
    $image_folder = $user_folder . DS. 'images';
    if (!is_dir($image_folder))
    {
      mkdir($image_folder);
    }
        
    $mp3_folder = $user_folder . DS. 'mp3';
    if (!is_dir($mp3_folder))
    {
      mkdir($mp3_folder);
    }
    
    $root_folder = $storage_folder . DS. 'root';
    if (!is_dir($root_folder))
    {
      mkdir($root_folder);
    }
  }
  
  
  function uploadImage()
  {
	 if (!$this->wallSettings->allow_post_images)
	  {
	    return false;
	  }
	  $database =& JFactory::getDBO();
	  $query = ' SELECT userid'
	         . ' FROM #__session'
	         . ' WHERE session_id = "' . $database->getEscaped($_POST['PHPSESSID']) . '"';
	  $database->setQuery($query);
	  $id = $database->loadResult();

	  $user =& JFactory::getUser($id);
	  if (!$user->guest)
	  {
	    require_once(JPATH_COMPONENT_ADMINISTRATOR.DS.'lib'.DS.'resize'.DS.'resize.class.php');
	    jimport('joomla.filesystem.file');
	    $photo = JRequest::getVar('image', array(), 'FILES', 'array');
	    $this->checkUserFolder($id);

	    $name 		= strtolower(JFile::stripExt($photo['name']));
	    $extension 	= strtolower(JFile::getExt($photo['name']));
	    $datenow     =& JFactory::getDate();
	    
	    $filepath = JPATH_SITE.DS.'components'.DS.'com_wallfactory'.DS.'storage'.DS.'users'.DS.$user->id.DS.'images'.DS.$name.'.'.$extension;

	    $resize = new RESIZEIMAGE($photo['tmp_name']);
	    $resize->resize_scale($this->wallSettings->image_max_width, $this->wallSettings->image_max_height, $filepath);
	    $resize->close();
	    
	    $query = ' SELECT w.id '
	         . ' FROM #__wallfactory_walls w'
	         . ' WHERE user_id = "' . $user->id . '"'; 
	    $database->setQuery($query);
	    $wall_id = $database->loadResult();

	    
	    JTable::addIncludePath(JPATH_COMPONENT_ADMINISTRATOR.DS.'tables');
	    $media_table =& JTable::getInstance('media', 'Table');
		
		  $media_table->user_id         = $user->id;
		  $media_table->wall_id			= $wall_id;
		  $media_table->folder		    = "images";
		  $media_table->name            = $name;
		  $media_table->extension 		= $extension;
		  $media_table->date_added		= date('Y-m-d H:i:s');
		  $media_table->post_id			= 0;
  	     
	      $media_table->store();
	      $lastid = $media_table->id;

	   
	   	header('Cache-Control: no-cache, must-revalidate');
	    header('Expires: Mon, 26 Jul 1997 05:00:00 GMT');
	    header('Content-type: application/json');
	   
	    if (!$lastid)
	    	 die('{ "lastid": "0"}');
	    else 
	    	 die('{ "lastid": "'.$lastid.'" }');	 
	   
	  }
	}
	
	function uploadMp3()
	{
	 
	 if (!$this->wallSettings->allow_post_mp3files)
	  {
	    return false;
	  }

	  $database =& JFactory::getDBO();
	  $query = ' SELECT userid'
	         . ' FROM #__session'
	         . ' WHERE session_id = "' . $database->getEscaped($_POST['PHPSESSID']) . '"';
	  $database->setQuery($query);
	  $id = $database->loadResult();

	  $user =& JFactory::getUser($id);
	  if (!$user->guest)
	  {
	    jimport('joomla.filesystem.file');
	    	    
	    $mp3 = JRequest::getVar('mp3', array(), 'FILES', 'array');
	    
	    $this->checkUserFolder($id);
    
    	$name 		= strtolower(JFile::stripExt($mp3['name']));
	    $extension = strtolower(JFile::getExt($mp3['name']));
	    $datenow     =& JFactory::getDate();
	    
	    $filepath = JPATH_SITE.DS.'components'.DS.'com_wallfactory'.DS.'storage'.DS.'users'.DS.$user->id.DS.'mp3'.DS.$name.'.'.$extension;
	    $src = $mp3['tmp_name'];
	    JFile::upload($src, $filepath);
	     	
    	$query = ' SELECT w.id '
	         . ' FROM #__wallfactory_walls w'
	         . ' WHERE user_id = "' . $user->id . '"'; 
	    $database->setQuery($query);
	    $wall_id = $database->loadResult();
	    
	    JTable::addIncludePath(JPATH_COMPONENT_ADMINISTRATOR.DS.'tables');
	    $media_table =& JTable::getInstance('media', 'Table');
		
		  $media_table->user_id         = $user->id;
		  $media_table->wall_id			= $wall_id;
		  $media_table->folder		    = "mp3";
		  $media_table->name            = $name;
		  $media_table->extension 		= $extension;
		  $media_table->date_added		= date('Y-m-d H:i:s');
		  $media_table->post_id			= 0;
  	     
	      $media_table->store();
	      $lastid = $media_table->id;

	   header('Cache-Control: no-cache, must-revalidate');
	    header('Expires: Mon, 26 Jul 1997 05:00:00 GMT');
	    header('Content-type: application/json');
	   
	    if (!$lastid)
	    	 die('{ "lastid": "0"}');
	    else 
	    	 die('{ "lastid": "'.$lastid.'" }');	 
    
	  }
	}
  
	function uploadFile()
	{
	  if (!$this->wallSettings->allow_post_files)
	  {
	    return false;
	  }
	 
	  $database =& JFactory::getDBO();
	  $query = ' SELECT userid'
	         . ' FROM #__session'
	         . ' WHERE session_id = "' . $database->getEscaped($_POST['PHPSESSID']) . '"';
	  $database->setQuery($query);
	  $id = $database->loadResult();

	  $user =& JFactory::getUser($id);

	  if (!$user->guest)
	  {
	    jimport('joomla.filesystem.file');
	   	   
	    $file = JRequest::getVar('file', array(), 'FILES', 'array');
	    
	    $this->checkUserFolder($id);
    
    	$name 		= strtolower(JFile::stripExt($file['name']));
	    $extension 	= strtolower(JFile::getExt($file['name']));
	    $datenow    =& JFactory::getDate();
	    $filepath 	= JPATH_SITE.DS.'components'.DS.'com_wallfactory'.DS.'storage'.DS.'users'.DS.$user->id.DS.'files'.DS.$name.'.'.$extension;
	    $src 		= $file['tmp_name'];
	    JFile::upload($src, $filepath);
	    	  
	    // files
    	$files = array();
    	$files['user_id']		= $user->id;
    	$files['folder'] 		= 'files'; 
    	$files['name'] 			= $name; 
	   	$files['extension'] 	= $extension;
  	
	   	$query = ' SELECT w.id '
	         . ' FROM #__wallfactory_walls w'
	         . ' WHERE user_id = "' . $user->id . '"'; 
	    $database->setQuery($query);
	    $wall_id = $database->loadResult();
	    
	    JTable::addIncludePath(JPATH_COMPONENT_ADMINISTRATOR.DS.'tables');
	    $media_table =& JTable::getInstance('media', 'Table');
		
		  $media_table->user_id         = $user->id;
		  $media_table->wall_id			= $wall_id;
		  $media_table->folder		    = "files";
		  $media_table->name            = $name;
		  $media_table->extension 		= $extension;
		  $media_table->date_added		= date('Y-m-d H:i:s');
		  $media_table->post_id			= 0;
  	     
	      $media_table->store();
	      $lastid = $media_table->id;
	
	   	header('Cache-Control: no-cache, must-revalidate');
	    header('Expires: Mon, 26 Jul 1997 05:00:00 GMT');
	    header('Content-type: application/json');
	   
	    if (!$lastid)
	    	 die('{ "lastid": "0"}');
	    else 
	    	 die('{ "lastid": "'.$lastid.'" }');	 
    
	  }
	}

}