<?php
/*------------------------------------------------------------------------
com_wallfactory - Wall Factory
------------------------------------------------------------------------
author    TheFactory
copyright Copyright (C) 2010 SKEPSIS Consult SRL. All Rights Reserved.
@license - http://www.gnu.org/licenses/gpl-2.0.html GNU/GPL
Websites: http://www.thefactory.ro
Technical Support:  Forum - http://www.thefactory.ro/joomla-forum/
-------------------------------------------------------------------------*/

defined('_JEXEC') or die('Restricted access');

class FrontendControllerSettings extends FrontendController
{
	function __construct()
	{
		parent::__construct();
	}

	function update()
	{
	  global $mainframe, $Itemid;
	  
	  $user     =& JFactory::getUser();
	  
	  $model =& $this->getModel('settings');

	  $wall = $model->getWallSettings();
	  
	  
	  if ($model->store())
	  {
	    $msg = JText::_('Settings Saved!');
	  }
	  else
	  {
	    $msg = JText::_('Error Saving Settings!');
	  }

	  $mainframe->redirect(JRoute::_('index.php?option=com_wallfactory&view=wall&Itemid=' . $Itemid), $msg);
	  
	}
}