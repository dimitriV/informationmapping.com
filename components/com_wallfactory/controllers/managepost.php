<?php
/*------------------------------------------------------------------------
com_wallfactory - Wall Factory
------------------------------------------------------------------------
author    TheFactory
copyright Copyright (C) 2010 SKEPSIS Consult SRL. All Rights Reserved.
@license - http://www.gnu.org/licenses/gpl-2.0.html GNU/GPL
Websites: http://www.thefactory.ro
Technical Support:  Forum - http://www.thefactory.ro/joomla-forum/
-------------------------------------------------------------------------*/

defined('_JEXEC') or die('Restricted access');

class FrontendControllerManagePost extends FrontendController
{
	function __construct()
	{
		parent::__construct();
	}

	function delete()
	{
	  $id       =& JRequest::getVar('post_id', 0, 'POST', 'integer');
	  $alias    =& JRequest::getVar('alias', 0, 'POST', 'integer');
	  $user     =& JFactory::getUser();
	  $database =& JFactory::getDBO();

	  $query = ' SELECT p.id'
	         . ' FROM #__wallfactory_posts p'
	         . ' LEFT JOIN #__wallfactory_walls w ON w.id = p.wall_id'
	         . ' WHERE p.id = ' . $id
	         . ' AND p.alias = ' . $alias
	         . ' AND w.user_id = ' . $user->id;
	  $database->setQuery($query);
	  $post = $database->loadResult();

	  if (!$post)
	  {
	    die('{ "status": 1, "message": "Post not found!" }');
	  }

	  
    // Delete comments
    $query = ' DELETE'
           . ' FROM #__wallfactory_comments'
           . ' WHERE post_id = ' . $id;
    $database->setQuery($query);
    $database->query();

    // delete from media 
     $query = ' DELETE'
           . ' FROM #__wallfactory_media'
           . ' WHERE post_id = ' . $id;
    $database->setQuery($query);
    $database->query();
    
    // delete from urls
     $query = ' DELETE'
           . ' FROM #__wallfactory_urls'
           . ' WHERE post_id = ' . $id;
    $database->setQuery($query);
    $database->query();
    
    
	// Delete the post
	  $query = ' DELETE'
	         . ' FROM #__wallfactory_posts'
	         . ' WHERE id = ' . $id;
	  $database->setQuery($query);
	  $database->query();

	  die(' { "status" : 2 }');
	}

	function publish()
	{
	  $id       =& JRequest::getVar('id', 0, 'POST', 'integer');
	  $user     =& JFactory::getUser();
	  $database =& JFactory::getDBO();

	  $query = ' SELECT p.id'
	         . ' FROM #__wallfactory_posts p'
	         . ' LEFT JOIN #__wallfactory_walls w ON w.id = p.wall_id'
	         . ' WHERE p.id = ' . $id
	         . ' AND w.user_id = ' . $user->id;
	  $database->setQuery($query);
	  $comment = $database->loadResult();

	  if (!$comment)
	  {
	    die('{ "status": 1, "message": "Post not found!" }');
	  }

	  $query = ' UPDATE #__wallfactory_posts p'
	         . ' SET p.published = !p.published'
	         . ' WHERE p.id = ' . $id;
	  $database->setQuery($query);

	  $database->query();

	  die(' { "status" : 2 }');
	}

	function filter()
	{
	  $filter          = JRequest::getVar('filter',          '', 'POST', 'string');
	  $filter_archive  = JRequest::getVar('filter_archive',  '', 'POST', 'string');
	  $category_filter = JRequest::getVar('category_filter', '', 'POST', 'string');

    $session =& JFactory::getSession();
    $session->set('filter.posts',          $filter);
    $session->set('filter.posts.category', $category_filter);
    $session->set('filter.posts.archived', $filter_archive);
	}

	function showComments()
	{
	  $id       =& JRequest::getVar('id', 0, 'GET', 'integer');
	  $database =& JFactory::getDBO();
	  $user     =& JFactory::getUser();

	  if ($id == 0)
	  {
	    $session =& JFactory::getSession();
        $session->set('filter.post_id', 0);

      die('+');
	  }

	  $query = ' SELECT p.title'
	         . ' FROM #__wallfactory_posts p'
	         . ' LEFT JOIN #__wallfactory_walls w ON w.id = p.wall_id'
	         . ' WHERE p.id = ' . $id
	         . ' AND w.user_id = ' . $user->id;
	  $database->setQuery($query);
	  $title = $database->loadResult();

	  if ($title)
	  {
      $session =& JFactory::getSession();
      $session->set('filter.post_id', $id);
	  }

	  $session->set('filter.comments', '');

	  global $mainframe, $Itemid;
	  $mainframe->redirect(JRoute::_('index.php?option=com_wallfactory&view=managecomments&Itemid=' . $Itemid));
	}

	function executeBatchAction($action)
	{
	  $database =& JFactory::getDBO();
	  $ids      =& JRequest::getVar('post', array(), 'POST', 'array');
	  $user     =& JFactory::getUser();

	  $ids = array_keys($ids);

	  // Validate the ids received
	  $query = ' SELECT p.id'
	         . ' FROM #__wallfactory_posts p'
	         . ' LEFT JOIN #__wallfactory_walls w ON w.id = p.wall_id'
	         . ' WHERE p.id IN (' . implode(', ', $ids) . ')'
	         . ' AND w.user_id = ' . $user->id;
    $database->setQuery($query);
    $ids = $database->loadResultArray();

    switch (count($ids))
    {
      case 0:
        $message = JText::_('No post has');
        break;

      case 1:
        $message = JText::_('1 post has');
        break;

      default:
        $message = count($ids) . ' ' . JText::_('posts have');
        break;
    }

    // We now have only the valid ids
    switch ($action)
    {
      case 'publish':
        $query = ' UPDATE #__wallfactory_posts'
    	         . ' SET published = 1';
    	  $message .= ' ' . JText::_('been published!');
    	break;

    	case 'unpublish':
        $query = ' UPDATE #__wallfactory_posts'
    	         . ' SET published = 0';
    	  $message .= ' ' . JText::_('been unpublished!');
    	break;

    	case 'archive':
        $query = ' UPDATE #__wallfactory_posts'
    	         . ' SET archived = 1';
    	  $message .= ' ' . JText::_('been archived!');
    	break;

    	case 'unarchive':
        $query = ' UPDATE #__wallfactory_posts'
    	         . ' SET archived = 0';
    	  $message .= ' ' . JText::_('been unarchived!');
    	break;

    	case 'delete':
    	  $query = ' DELETE'
    	         . ' FROM #__wallfactory_posts';
    	  $message .= ' ' . JText::_('been deleted!');
      break;
    }

    $query .= ' WHERE id IN (' . @implode(', ', $ids) . ')';

    $database->setQuery($query);
    $database->query();

    header('Cache-Control: no-cache, must-revalidate');
    header('Expires: Mon, 26 Jul 1997 05:00:00 GMT');
    header('Content-type: application/json');

    die('{"action": "' . $action . '", "message": "' . $message . '", "ids": [' . @implode(',', $ids) . ']}');
	}

	function batchUnpublish()
	{
	  $this->executeBatchAction('unpublish');
	}

	function batchPublish()
	{
	  $this->executeBatchAction('publish');
	}

	function batchDelete()
	{
	  $this->executeBatchAction('delete');
	}

}