<?php
/*------------------------------------------------------------------------
com_wallfactory - Wall Factory
------------------------------------------------------------------------
author    TheFactory
copyright Copyright (C) 2010 SKEPSIS Consult SRL. All Rights Reserved.
@license - http://www.gnu.org/licenses/gpl-2.0.html GNU/GPL
Websites: http://www.thefactory.ro
Technical Support:  Forum - http://www.thefactory.ro/joomla-forum/
-------------------------------------------------------------------------*/

defined('_JEXEC') or die('Restricted access');

class FrontendControllerPost extends FrontendController
 {
	function __construct()
	{
		parent::__construct();
	}

	function follow()
	{
	  $action = JRequest::getVar('action', '', 'GET', 'string');
	  $model  =& $this->getModel('post');

	  if ($model->follow())
	  {
	    if ($action == 'subscribe')
	    {
	      $msg = 'Subscribed!';
	      $new_html = JText::_('Stop following');
	    }
	    else
	    {
	      $msg = 'Unsubscribed!';
	      $new_html = JText::_('Follow wall');
	    }

	    $errors = 0;
	  }
	  else
	  {
	    $msg    = 'Error occured!';
	    $errors = 1;

	    $new_html = ($action == 'unsubscribe') ? JText::_('Stop following') : JText::_('Follow wall');
	  }

	  die('{"errors": ' . $errors . ', "message": "' . $msg . '", "new_html": "' . $new_html . '"}');
	}
	
	
    function save()
	{
	    global $mainframe, $Itemid;
	
	    $alias = JRequest::getVar('current_wall_alias', '', 'POST', 'string');
	    
	    $model = $this->getModel('post');
	
	    $post = $model->store();
	
	    if ($post)
	     $msg = 'Post saved';
	    else 
	     $msg = 'Post not saved';
	    
	    $mainframe->redirect(JRoute::_('index.php?option=com_wallfactory&view=posts&Itemid=' . $Itemid), $msg);
	}
	
	function report()
	{
	  global $mainframe, $Itemid;
		
	  $id       =& JRequest::getVar('id', 0, 'GET', 'integer');
	  $alias 	=& JRequest::getVar('alias', '', 'GET', 'string');
	  
	  $user     =& JFactory::getUser();
	  $database =& JFactory::getDBO();

	  // Find the post
	  $query = ' SELECT p.*, w.report_post_notification,'
	         . ' w.title AS wall_title, w.id AS wall_id, w.alias, '
	         . ' w.user_id AS wall_owner_id'
	         . ' FROM #__wallfactory_posts p'
	         . ' LEFT JOIN #__wallfactory_walls w ON w.id = p.wall_id'
	         . ' WHERE p.id = ' . $id
	         . ' AND p.reported = 0';
	  $database->setQuery($query);
	  $post = $database->loadObject();
	  
	  // Comment not found
	  if (!$post) {
	    	$msg = 'Post not found or already reported!';
	    	$mainframe->redirect(JRoute::_('index.php?option=com_wallfactory&view=wall&alias='.$alias.'&Itemid=' . $Itemid), $msg); 
	
	  }
	  else  {
     	$msg = 'Post reported';
	  	// Report the post
	  	
	  	$query = ' UPDATE #__wallfactory_posts'
	         . ' SET reported = 1, reporting_user_id = ' . $user->id
	         . ' WHERE id = ' . $id;
	  	$database->setQuery($query);
	  	$database->query();

		  // Send notification
		  if ($this->wallSettings->enable_notification_new_report_post)
		  {
		    $this->sendReportNotification($post);
		  }

		$mainframe->redirect(JRoute::_('index.php?option=com_wallfactory&view=wall&alias='.$alias.'&Itemid=' . $Itemid), $msg); 
	  	//die('{"status": 2, "message": "' . JText::_('(reported)') . '"}');
		}
	}
	
    function delete()
    {
      global $mainframe, $Itemid;

      $alias = JRequest::getVar('alias', '', 'GET', 'string');
      $post_id = JRequest::getVar('post_id', '', 'GET', 'int');
    
      $model = $this->getModel('post');
      $post = $model->delete();

      if ($post)
     	$msg = 'Post deleted';
      else 
     	$msg = 'Post not found';
    
      $mainframe->redirect(JRoute::_('index.php?option=com_wallfactory&view=posts&Itemid=' . $Itemid), $msg);
    }
  
    function sendReportNotification($post)
	{
	  $database =& JFactory::getDBO();
	  $array    =  array();

	  // Send report to admins
	  if (count($this->wallSettings->notification_new_report_post_receivers))
	  {
	    $array = $this->wallSettings->notification_new_report_post_receivers;
	  }

	  // Send report to wall owner
	  if ($post->report_post_notification)
	  {
	    $array[] = $post->wall_owner_id;
	  }

	  if (count($array))
	  {
	    $query = ' SELECT u.username, u.email'
	           . ' FROM #__users u'
	           . ' WHERE u.id IN (' . implode(', ', $array) . ')';
	    $database->setQuery($query);
	    $receivers = $database->loadObjectList();

	    $subject = base64_decode($this->wallSettings->notification_new_report_post_subject);
	    $message = base64_decode($this->wallSettings->notification_new_report_post_message);

	    $search  = array('%%username%%', '%%walltitle%%', '%%posttext%%', '%%postlink%%', '%%walllink%%', '%%postlink%%');

	    foreach ($receivers as $receiver)
	    {
	      $replace = array(
	        $receiver->username,
	        $post->alias,
	        $post->wall_title,
	        $post->content
	      );

	      if ($this->wallSettings->enable_notification_email_html)
	      {
	        $replace[] = '<a href="' . JRoute::_(JURI::root() . 'index.php?option=com_wallfactory&view=post&id=' . $post->post_id. '&alias='.$post->alias) . '">' . $post->alias . '</a>';
	        $replace[] = '<a href="' . JRoute::_(JURI::root() . 'index.php?option=com_wallfactory&view=wall&alias=' . $post->alias) . '">' . $post->wall_title . '</a>';
	        //$replace[] = '<a href="' . JRoute::_(JURI::root() . 'index.php?option=com_wallfactory&view=post&id=' . $post->post_id) . '#posts">' . $post->post_title . '</a>';
	      }
	      else
	      {
	        $replace[] = JRoute::_(JURI::root() . 'index.php?option=com_wallfactory&view=post&id=' . $post->post_id. '&alias='.$post->alias);
	        $replace[] = JRoute::_(JURI::root() . 'index.php?option=com_wallfactory&view=wall&&alias=' . $post->alias);
	        //$replace[] = JRoute::_(JURI::root() . 'index.php?option=com_wallfactory&view=post&id=' . $post->post_id . '#posts');
	      }

	      $custom_message = str_replace($search, $replace, $message);
	      $custom_subject = str_replace($search, $replace, $subject);

	      if ($this->wallSettings->enable_notification_email_html)
	      {
	        $custom_message = nl2br($custom_message);
	      }

	      $mail =& JFactory::getMailer();
	      $mail->addRecipient($receiver->email);
	      $mail->setSubject(JText::_($custom_subject));
	      $mail->setBody(JText::_($custom_message));
	      $mail->IsHTML($this->wallSettings->enable_notification_email_html);

	      $mail->send();
	    }
	  }
	}

}