<?php
/**
 * @version     $Id: default.php  2010-10-23 04:13:25Z $
 * @author      ITOffshore Nepal
 * @copyright   Copyright (C) 2011 ITOffshore Nepal.
 * @license     ITOffshore Nepal
 */
// no direct access
defined('_JEXEC') or die('Restricted access');

JHtml::_('behavior.calendar');
?>

<script type="text/javascript">
    jQuery(document).ready(function(){
        jQuery('#submitImpForm').click(function(){
            var valid = true;
            jQuery('.required').each(function(){
                if(jQuery(this).val() == ''){
                    jQuery(this).addClass('invalid');
                    valid = false;
                }	
            });
            if(valid){
                jQuery.ajax({
                    type: 'POST',
                    url: 'index.php?option=com_ssomoodle',
                    data: {task: 'isvalidstudentkey', student_key: jQuery('#jform_student_key').val()},
                    success: function(result){
                        if(result==1){
                            jQuery('#impForm').submit();	
                        }
                        else{
                            jQuery('#jform_student_key').addClass('invalid');
                            jQuery('#student_key_error').removeClass('certification-hide');
                        }
                    }
                });	
            }
            return false;
        });
	
        jQuery('.required').change(function(){
            jQuery(this).removeClass('invalid');
        });
	
        jQuery('#jform_student_key').change(function(){
            jQuery('#student_key_error').addClass('certification-hide');
        });
    });
    function Toggle(id)
{
    if (document.getElementById)
    {
        var thisdiv = document.getElementById(id);
        if(thisdiv.style.display == 'none')
            thisdiv.style.display = 'block';
        else
            thisdiv.style.display = 'none';
        
        var divs = document.body.getElementsByTagName("fieldset");
        for(i = 0; i < divs.length; i++)
        {
            if ((divs[i].id != id) && (divs[i].id.substr(0,10) == 'toggle_'))
                divs[i].style.display = 'none';
        }
    }
}
</script>

<div class="certification">
    <div class="certification-title">
        <h1>Information Mapping Professional Certification: Your Resume</h1>
    </div>
</div>

<div id="content" class="impform">
    <p>Please complete your resume in order to get access to the Information Mapping Professional Certification Exam.</p>
    <!--<div class="certification-description"><?php //echo $this->params->get('formdescription');  ?></div>-->
    <form action="<?php echo JRoute::_('index.php?option=com_ssomoodle&view=impform'); ?>" method="post" id="impForm" name="impForm">

        <fieldset>
            <legend>Information Mapping&reg; Training</legend>
            <ol>
                <li>
                    <label for="coursetitle_imap"><?php echo JText::_('COM_SSOMOODLE_IMPFORM_COURSE'); ?> *</label>
                    <select id="jform_course_id" name="jform[course_id]" class="styled required" >
                        <option value=""><?php echo JText::_('COM_SSOMOODLE_IMPFORM_SELECT_COURSE'); ?></option>
                        <?php
                        foreach ($this->courses as $course) {
                            echo '<option value="' . $course->id . '">' . $course->title . '</option>';
                        }
                        ?>
                    </select>                    
                </li>
                <li>
                    <label for="company_imap"><?php echo JText::_('COM_SSOMOODLE_IMPFORM_TRAINING_COMPANY'); ?></label>
                    <select name="jform[training_company]" id="jform_training_company" class="styled required">                            
                        <option> </option>
                        <option value="6">Australia - TP3</option>                                                                        
                        <option value="8">Belgium - Synaps</option>                                                                        
                        <option value="9">Belgium - U&I Learning</option>                                                                        
                        <option value="10">Canada - Information Mapping Canada</option>
                        <option value="11">Denmark - Key2Know</option>                                                                        
                        <option value="13">France - Takoma</option>                                                                        
                        <option value="15">India - TecDoc</option>                                                                        
                        <option value="32">India - Triumph</option> 
                        <option value="36">India - Information Mapping India</option> 
                        <option value="33">Indonesia - Indocita</option>
                        <option value="30">Israel - Writepoint</option>                                                                        
                        <option value="28">Italy - Writec</option>                                                                        
                        <option value="29">Japan - Ricoh</option>  
                        <option value="35">Japan - CDI/Kozoka</option> 
                        <option value="34">Malaysia - Global TNA</option>                                                                       
                        <option value="19">Mexico - Information Mapping Mexico (Contexto Didactico)</option>
                        <option value="20">New Zealand - TACTICS Limited</option>                                                                        
                        <option value="5">None</option>                                                                        
                        <option value="22">Sweden - Semantix</option>                                                                        
                        <option value="23">Switzerland - Aracane</option>                                                                        
                        <option value="24">The Netherlands - U&I Learning</option> 
                        <option value="37">Turkey - Ozge</option> 
                        <option value="38">Turkey - Urban</option>                                                                        
                        <option value="31">UK - Firebrand</option>                                                                        
                        <option value="27">USA - Information Mapping Inc.</option>                                                            
                    </select>
                </li>
                <li>
                    <label for="month_imap"><?php echo JText::_('COM_SSOMOODLE_IMPFORM_COURSE_DATE'); ?> *</label>
                    <?php echo JHTML::_('calendar', '', 'jform[course_date]', 'jform_course_date', '%B %d, %Y', 'readonly="readonly" class="required input-text"'); ?>
                </li>
                <li>
                    <label><?php echo JText::_('COM_SSOMOODLE_IMPFORM_STUDENT_KEY'); ?> *</label>
                    <span class="certification-input-field"><input type="text" class="input-text required" id="jform_student_key" name="jform[student_key]" /></span>
                    <br />
                    <span class="certification-info"><?php echo JTEXT::_('COM_SSOMOODLE_IMPFORM_STUDENT_KEY_INFO'); ?></span>			
                    <br />
                    <span class="certification-hide certification-error input-text" id="student_key_error"><?php echo JTEXT::_('COM_SSOMOODLE_MSG_INVALID_STUDENT_KEY'); ?></span>
                </li>
            </ol>

        </fieldset> 

        <fieldset>
            <legend>Your Professional Experience</legend>
            <ol>
                <li>
                    <label for="profsummary"><?php echo JText::_('COM_SSOMOODLE_IMPFORM_EXPERIENCE'); ?> *</label>
                    <textarea id="jform_experience" name="jform[experience]" class="required"></textarea>
                </li>
                <li>
                    <label for="specialties"><?php echo JText::_('COM_SSOMOODLE_IMPFORM_SPECIALITIES'); ?> *</label>
                    <textarea id="jform_specialities" name="jform[specialities]" class="required"></textarea>
                </li>
            </ol>
        </fieldset> 

        <fieldset>
            <legend>Your Current Job</legend>
            <ol>
                <li>
                    <label for="company_job0"><?php echo JText::_('COM_SSOMOODLE_IMPFORM_CURRENT_COMPANY'); ?> *</label>
                    <input type="text" class="input-text required" id="jform_current_company" name="jform[current_company]"/> 
                </li>
                <li>
                    <label for="title_job0"><?php echo JText::_('COM_SSOMOODLE_IMPFORM_CURRENT_FUNCTION'); ?> *</label>
                    <input type="text" class="input-text required" id="jform_current_function" name="jform[current_function]" />
                </li>
                <li>
                    <label for="description_job0"><?php echo JText::_('COM_SSOMOODLE_IMPFORM_CURRENT_FUNCTION_DESC'); ?> *</label>
                    <textarea id="jform_current_function_desc" name="jform[current_function_desc]" class="required"></textarea>
                </li>
                <li>
                    <label for="telephone"><?php echo JText::_('COM_SSOMOODLE_IMPFORM_TELEPHONE'); ?> *</label>
                    <input type="text" class="input-text required" id="jform_telephone" name="jform[telephone]" /> 
                </li>
                <li>
                    <label for="website"><?php echo JText::_('COM_SSOMOODLE_IMPFORM_COMPANY_WEBSITE'); ?> *</label>
                    <input type="text" class="input-text required" id="jform_company_website" name="jform[company_website]" />
                </li>
            </ol>
        </fieldset> 		

        <fieldset class="submit">
            <button type="button" class="btn bnt-blue" id="submitImpForm"><?php echo JText::_('COM_SSOMOODLE_IMPFORM_SUBMIT'); ?></button>
            <input type="hidden" name="task" value="saveimpform" />
            <?php echo JHtml::_('form.token'); ?>
        </fieldset>
    </form>
</div>
</div>



