<?php
/**
 * @version     $Id: default.php  2010-10-23 04:13:25Z $
 * @author      ITOffshore Nepal
 * @copyright   Copyright (C) 2011 ITOffshore Nepal.
 * @license     ITOffshore Nepal
 */
// no direct access
defined('_JEXEC') or die('Restricted access');

JHtml::_('behavior.calendar');
?>

<script type="text/javascript">
    jQuery(document).ready(function(){
        jQuery('#submitImpForm').click(function(){

            var valid = true;

            jQuery('.required').each(function(){
                if(jQuery(this).val() == ''){
                    jQuery(this).addClass('invalid');
                    valid = false;
                }
            });

            if(valid){
                var sUrl = "/index.php?option=com_ssomoodle&task=isvalidstudentkey&student_key=" + jQuery('#jform_student_key').val();
                jQuery.get(sUrl, function( data ) {
                    if(data==1){
                        jQuery('#impForm').submit();
                    }
                    else{
                        jQuery('#jform_student_key').addClass('invalid');
                        jQuery('#student_key_error').removeClass('certification-hide');
                    }
                });
            }
            return false;
        });

        jQuery('.required').change(function(){
            jQuery(this).removeClass('invalid');
        });

        jQuery('#jform_student_key').change(function(){
            jQuery('#student_key_error').addClass('certification-hide');
        });
    });
    function Toggle(id)
    {
        if (document.getElementById)
        {
            var thisdiv = document.getElementById(id);
            if(thisdiv.style.display == 'none')
                thisdiv.style.display = 'block';
            else
                thisdiv.style.display = 'none';

            var divs = document.body.getElementsByTagName("fieldset");
            for(i = 0; i < divs.length; i++)
            {
                if ((divs[i].id != id) && (divs[i].id.substr(0,10) == 'toggle_'))
                    divs[i].style.display = 'none';
            }
        }
    }
</script>

<div class="certification">
    <div class="certification-title">
        <h1>Information Mapping Professional Certification: </h1>
    </div>
</div>

<div id="content" class="impform">
    <!--<div class="certification-description"><?php //echo $this->params->get('formdescription');  ?></div>-->
    <form action="<?php echo JRoute::_('index.php?option=com_ssomoodle&view=impform'); ?>" method="post" id="impForm" name="impForm">

        <fieldset>
            <legend>Information Mapping&reg; Training</legend>
            <ol>
                <li>
                    <label for="coursetitle_imap"><?php echo JText::_('COM_SSOMOODLE_IMPFORM_COURSE'); ?> *</label>
                    <select id="jform_course_id" name="jform[course_id]" class="styled required" >
                        <option value=""><?php echo JText::_('COM_SSOMOODLE_IMPFORM_SELECT_COURSE'); ?></option>
                        <?php
                        foreach ($this->courses as $course) {
                            echo '<option value="' . $course->id . '">' . $course->title . '</option>';
                        }
                        ?>
                    </select>
                </li>
                <li>
                    <label for="company_imap"><?php echo JText::_('COM_SSOMOODLE_IMPFORM_TRAINING_COMPANY'); ?></label>
                    <select name="jform[training_company]" id="jform_training_company" class="styled required">
                        <option> </option>
                        <option value="17">Australia - Information Mapping Australia</option>
                        <option value="13">France - Takoma</option>
                        <option value="36">India - Information Mapping India</option>
                        <option value="33">Indonesia - Indocita</option>
                        <option value="28">Italy - Writec</option>
                        <option value="34">Malaysia - Global TNA</option>
                        <option value="19">Mexico - Information Mapping Mexico (Contexto Didactico)</option>
                        <option value="5">None</option>
                        <option value="40">Poland - GETiT</option>
                        <option value="41">Russia - ITORUM</option>
                        <option value="42">South Africa - INBOX</option>
                        <option value="39">UK - Skillset</option>
                        <option value="27">USA - Information Mapping Inc.</option>
                    </select>
                </li>
                <li>
                    <label for="month_imap"><?php echo JText::_('COM_SSOMOODLE_IMPFORM_COURSE_DATE'); ?> *</label>
                    <?php echo JHTML::_('calendar', '', 'jform[course_date]', 'jform_course_date', '%B %d, %Y', 'readonly="readonly" class="required input-text"'); ?>
                </li>
                <li>
                    <label><?php echo JText::_('COM_SSOMOODLE_IMPFORM_STUDENT_KEY'); ?> *</label>
                    <span class="certification-input-field"><input type="text" class="input-text required" id="jform_student_key" name="jform[student_key]" /></span>
                    <br />
                    <span class="certification-info"><?php echo JTEXT::_('COM_SSOMOODLE_IMPFORM_STUDENT_KEY_INFO'); ?></span>
                    <br />
                    <span class="certification-hide certification-error input-text" id="student_key_error"><?php echo JTEXT::_('COM_SSOMOODLE_MSG_INVALID_STUDENT_KEY'); ?></span>
                </li>
            </ol>

        </fieldset>

        <fieldset class="submit">
            <input type="hidden" name="task" value="saveimpform" />
            <?php echo JHtml::_('form.token'); ?>
            <button type="button" class="btn btn-blue" id="submitImpForm"><?php echo JText::_('COM_SSOMOODLE_IMPFORM_SUBMIT'); ?></button>
        </fieldset>
    </form>
</div>
</div>