<?php
/**
 * @author      ITOffshore Nepal
 * @copyright   Copyright (C) 2011 ITOffshore Nepal.
 * @license     ITOffshore Nepal
 */
// no direct access
defined('_JEXEC') or die('Restricted access');

jimport('joomla.application.component.view');

class SsoMoodleViewImpForm extends JViewLegacy
{	
    protected $state;
    protected $params;
    protected $courses;

	function display($tpl = null)
	{
		$user = JFactory::getUser();
        $model = $this->getModel('impform');

        if ($user->get('guest')) {
			$uri = JFactory::getURI();
			$app = JFactory::getApplication();
			$app->redirect(JRoute::_('index.php?option=com_users&view=login&return='.base64_encode($uri)), JText::_('COM_SSOMOODLE_ERROR_LOGIN_TO_ACCESS_MOODLE'));
			return;
		}
		if ($model->isValidToRedirect($user->id, $user->email)) {
			$app = JFactory::getApplication();
			$app->redirect(JRoute::_('index.php?option=com_ssomoodle&view=moodle'));
			return;
		}

		
		$this->state = $this->get('State');	
		$this->params = $this->state->get('params');
		$this->courses = $this->get('Courses');
		
		$this->_prepareDocument();		

		parent::display($tpl);
	}
	
	/**
	 * Prepares the document
	 */
	protected function _prepareDocument()
	{
		$app	= JFactory::getApplication();
		$menus	= $app->getMenu();
		$menu	= $menus->getActive();
		if ($menu){
			$this->params->def('page_heading', $this->params->get('page_title', $menu->title));
		}
	}

}
?>
