<?php

/**
 * @version     1.0.0
 * @package     com_ssomoodle
 * @copyright   Copyright (C) 2012. All rights reserved.
 * @license     GNU General Public License version 2 or later; see LICENSE.txt
 * @author      
 */
// No direct access
defined('_JEXEC') or die;

jimport('joomla.application.component.view');

require_once(__DIR__.'/../../mageconfig.php');

class SsoMoodleViewMoodle extends JViewLegacy {

    protected $state;
    protected $params;
    protected $customer_id;
    protected $user_email;
    protected $user_firstname;
    protected $user_lastname;
    protected $user_country;
    protected $user_city;
    protected $token_answer;    

    public function display($tpl = null) 
    {
        $user = JFactory::getUser();
        
        $moodle_link = 'index.php';
        
        if ($user->get('guest')) {
            $uri = JFactory::getURI();
            $app = JFactory::getApplication();
            $app->redirect(JRoute::_('index.php?option=com_users&view=login&return='.base64_encode($uri)), JText::_('COM_SSOMOODLE_ERROR_LOGIN_TO_ACCESS_MOODLE'));
            return;
        }

        $user_id = $user->id;
        $user_email = $user->email;

        $register = MageBridge::getRegister();
	$register->add('headers');
        $bridge = MageBridge::getBridge();
	$bridge->build();
        $magento_config = $bridge->getMageConfig();
        $magento_user_id = (isset($magento_config['customer/magento_id'])) ? $magento_config['customer/magento_id'] : null;

        if($magento_user_id < 1) {
            Die('Your Magento id could not be found. Please contact us.');
        }
        
        if(strpos($_SERVER['HTTP_REFERER'], 'imp-form')) {
            $moodle_link = 'exam.php';
        }
        
        $user_id = $magento_user_id;
        $secret = '7d888c7a57163d02aa057f5a';
        $token = mt_rand();
        $signature = sha1('sso'.$token.$user_id.$secret);
        $url = '/academy/local/sso/' . $moodle_link . '?idnumber='.$user_id.'&token='.$token.'&signature='.$signature;
        header('HTTP/1.1 302 Found'); 
        header('Location: '.$url);
    }

    /**
     * Prepares the document
     */
    protected function _prepareDocument() {
        $app = JFactory::getApplication();
        $menus = $app->getMenu();

        $menu = $menus->getActive();
        if ($menu) {
            $this->params->def('page_heading', $this->params->get('page_title', $menu->title));
        }
    }

}
