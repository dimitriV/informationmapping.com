<?php
/**
 * @version     1.0.0
 * @package     com_ssomoodle
 * @copyright   Copyright (C) 2012. All rights reserved.
 * @license     GNU General Public License version 2 or later; see LICENSE.txt
 * @author      
 */
// no direct access
defined('_JEXEC') or die;

$params = $this->params;

?>
<script type="text/javascript">
	window.onload = function(){
	    document.getElementById('ssoForm').submit();
	}
</script>

<div class="main-content<?php echo $params->get('pageclass_sfx'); ?>">
    <?php if ($params->get('show_page_heading')): ?>
        <h1><?php echo $params->get('page_heading'); ?></h1>
    <?php endif; ?>
    <?php echo JText::_('COM_SSOMOODLE_MOODLE_REDIRECT_MSG'); ?>
    
    <form action="<?php echo $params->get('sso_url'); ?>" method="post" name="ssoForm" id="ssoForm">
        <input type="hidden" name="token" value="<?php echo $this->token_answer; ?>" />
        <input type="hidden" name="idnumber" value="<?php echo $this->customer_id; ?>" />
		<input type="hidden" name="email" value="<?php echo $this->user_email; ?>" />
		<input type="hidden" name="firstname" value="<?php echo $this->user_firstname; ?>" />
		<input type="hidden" name="lastname" value="<?php echo $this->user_lastname; ?>" />
		<input type="hidden" name="country" value="<?php echo $this->user_country; ?>" />
		<input type="hidden" name="city" value="<?php echo $this->user_city; ?>" />
    </form>
</div>
