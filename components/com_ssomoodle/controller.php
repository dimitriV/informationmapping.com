<?php

/**
 * @version     1.0.0
 * @package     com_ssomoodle
 * @copyright   Copyright (C) 2012. All rights reserved.
 * @license     GNU General Public License version 2 or later; see LICENSE.txt
 * @author      
 */
// No direct access
defined('_JEXEC') or die;

jimport('joomla.application.component.controller');

class SsoMoodleController extends JControllerLegacy {    
	
	public function saveimpform()
	{
		$user = JFactory::getUser();
		$model = $this->getModel('impform');
		
		if (!$model->saveImpForm()) {
			$msg = $model->getError();
			$this->setRedirect(JRoute::_('index.php?option=com_ssomoodle&view=impform', false), $msg, 'error');
			return;
		}
		
		$this->setRedirect(JRoute::_('index.php?option=com_ssomoodle&view=moodle'));
		return;
	}
	
	public function isvalidstudentkey(){
		$model = $this->getModel('impform');
		echo $model->isValidStudentkey();
		die();
	}
}
