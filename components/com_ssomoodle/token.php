<?php
//***********************************************************
// Joomla-Moodle SSO - Tokens Class
//***********************************************************

/**
 * Workflow:
 * 
 * 0. Include this file and create a 'Tokens' instance:
 *		@require_once('Tokens.php');
 *		$tokens = new Tokens();
 *
 * 1. Query the sso.php script via GET with a 'init' parameter, like so:
 *		$question = @file_get_contents('http://path/to/sso.php?init');
 * 
 * 2. Calculate an answer token for the question token like this:
 *		$answer = $tokens->calc($question);
 *
 * 3. Send the answer token (named 'token') along with your POST request.
 *		<input type="hidden" name="token" value="<?php echo $answer; ?>" />
 */


/**
 * Class for creating and checking tokens
 *
 * @author		toon daelman @ vision
 * @copyright 	2012 Vision
 */
class Token {
	// Variables ----------------------------
	/**
	 * Salt for the answer.
	 *
	 * @var string
	 */
	private $salt;



	// Methods ------------------------------
	/**
	 * Constructor
	 *
	 * @param	string[optional] $salt		The salt to add to the question before sha1 hashing
	 */
	function __construct() {

		$salt = 'f0$/-/!Zz!€M@n!zZ!€';

		// Set salt
		$this->salt = substr(md5((string) $salt), 0, 8);
	}


	/**
	 * Method to get a 'Question'-token
	 *
	 * @return	string	$question			Returns a question token (changes every 60 seconds)
	 */
	public function get() {
		return 'sso-'.substr(md5((int) floor(time()/(60)) . $this->salt), 0, 8);
	}


	/**
	 * Method to calculate an 'Answer'-token
	 *
	 * @return	string	$answer				Returns an answer token for given question token
	 * @param	string	$question			The question token you want to answer
	 */
	public function calc($question) {
		return 'sso-'.substr(sha1($this->salt.$question), 0, 8);
	}


	/**
	 * Method to check a 'Answer'-token
	 *
	 * @return	bool	$success			Returns true or false for success or failure of the question/answer check
	 * @param	string	$answer				The answer you want to check for validity
	 */
	public function check($answer) {
		return (bool) ($answer == $this->calc($this->get()));
	}
}

?>
