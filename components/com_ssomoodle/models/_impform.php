<?php

/**
 * @package     com_ssomoodle
 * @copyright   Copyright (C) 2011 ITOffshore Nepal.
 * @license     ITOffshore Nepal
 */
// Check to ensure this file is included in Joomla!
defined('_JEXEC') or die('Restricted access');

jimport('joomla.application.component.model');

/**
 * Model class for the certification
 *
 * @package		course component
 * @subpackage	certificaiton view
 */
class SsoMoodleModelImpForm extends JModelLegacy
{    
    /**
	 * Method to auto-populate the model state.
	 *
	 * Note. Calling getState in this method will result in recursion.
	 *
	 * @since	1.6
	 */
	protected function populateState()
	{
		$app = JFactory::getApplication('site');
		// Load the parameters.
		$params = $app->getParams();
		$this->setState('params', $params);
	}
	
	/**
     * Returns a Table object, always creating it
     *
     * @param	type	The table type to instantiate
     * @param	string	A prefix for the table class name. Optional.
     * @param	array	Configuration array for model. Optional.
     * @return	JTable	A database object
     */
    public function getTable($type = 'ImpForm', $prefix = 'SsoMoodleTable', $config = array()) {
        return JTable::getInstance($type, $prefix, $config);
    }
    
    /**
     * get published courses
     * @return type  array
     */
    public function getCourses()
    {
        $lang = JFactory::getLanguage();
        $langTag = $lang->getTag();

        $query = $this->_db->getQuery(true);

        $query->select('id')
                ->select('title')
                ->from('#__moodle_courses')
                ->where('state = 1')
                ->where('language in (' . '"' . $langTag . '"' . ',' . '"*"' . ')')
                ->order('title ASC');


        $this->_db->setQuery($query);

        return $this->_db->loadObjectList();
    }

    /**
     * check where key typed by student match with key stored on db
     * @return type string
     */
    public function isValidStudentkey()
    {
        $studentKey = JRequest::getVar('student_key');
        if (!$studentKey) {
            return false;
        }

        $query = $this->_db->getQuery(true);

        $query->select('COUNT(studentkey)')
                ->from('#__course_studentkeys')
                ->where('studentkey = "' . trim($studentKey) . '"')
                ->where('state != "-2"')
                ->where('status_imp = \'0\'');
		//->where('status = 0');

        $this->_db->setQuery($query);
        
        return $this->_db->loadResult();
    }
    
    /**
     * Save the certificaion request
     * @return type
     */
    public function saveImpForm()
    {
        $user = & JFactory::getUser();
        $data = JRequest::getVar('jform', array(), 'post', 'array');
        //echo '<pre>';print_r($data); echo '</pre>'; die();
        $data['course_date'] = date('Y-m-d', strtotime($data['course_date']));
        
        $table = $this->getTable();
		
        if (!$table->bind($data)) {
            $this->setError($table->getError());
            return false;
        }
        if (!$table->check()) {
            $this->setError($table->getError());
            return false;
        }
        if (!$table->store()) {
            $this->setError($table->getError());
            return false;
        }

	$this->_disableUsedStudentKeys($user->id, $data['student_key']);
	$this->assignUserGroup($user->id);

        return true;
    }

    private function _disableUsedStudentKeys($user_id, $key)
    {
        $query = 'UPDATE #__course_studentkeys SET status_imp = \'1\' WHERE studentkey="' . $key . '"';

        $this->_db->setQuery($query);
        if (!$this->_db->query()) {
            return false;
        }
        return true;
    }
    
    private function assignUserGroup($userid)
    {
    	$query = 'SELECT user_id FROM #__user_usergroup_map WHERE user_id='.$userid.' AND group_id=18';
        $this->_db->setQuery($query);
        if(!$this->_db->loadResult()){
			$query = 'INSERT INTO #__user_usergroup_map SET user_id='.$userid.', group_id=18';
	        $this->_db->setQuery($query);
	        if (!$this->_db->query()) {
	            return false;
	        }
		}
        return true;
    }

}

?>
