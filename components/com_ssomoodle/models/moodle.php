<?php
/**
 * @package     com_ssomoodle
 * @author      ITOffshore Nepal
 * @copyright   Copyright (C) 2011 IT Offshore Nepal.
 * @license     IT Offshore Nepal
 **/
// no direct access
defined('_JEXEC') or die('Restricted access'); 

jimport( 'joomla.application.component.model' );

class SsoMoodleModelMoodle extends JModelLegacy
{
	protected function populateState()
	{
		$app = JFactory::getApplication('site');
		
		$params = $app->getParams();
		$this->setState('params', $params);
	}
	
	public function getMageDbo(){
		require_once(JPATH_COMPONENT.'/mageconfig.php');
		
        $option = array();
        $option['driver']	= 'mysql';
        $option['host']		= MAGE_HOST;
        $option['user']		= MAGE_USER;
        $option['password']	= MAGE_PASSWORD;
        $option['database']	= MAGE_DATABASE;
        $option['prefix']	= '';

        $mageDbo = & JDatabase::getInstance($option);

        return $mageDbo;		
	}
	
	public function getCustomerData(){
		$mageDbo = $this->getMageDbo();
		$user = JFactory::getUser();
		$customer = array();
		
		$query = 'SELECT customer_id FROM magebridge_customer_joomla WHERE joomla_id='.$user->id;
		$mageDbo->setQuery($query);
		$customer_id = $mageDbo->loadResult();
		
		if($customer_id){
			$customer['customer_id'] = $customer_id;
			
			$bridge = MageBridgeModelBridge::getInstance();
			$register = MageBridgeModelRegister::getInstance();
			$id = $register->add('api', 'customer_customer.info', $customer_id);
			$bridge->build();
			$customer_data = $register->getById($id);
			
			if(isset($customer_data['data']['default_billing']) && $customer_data['data']['default_billing'] > 0){
				$id = $register->add('api', 'customer_address.info', $customer_data['data']['default_billing']);
				$bridge->build();
				$address_data = $register->getById($id);
			}
		}
		
		$customer['customer_id'] = $customer_id;
		$customer['country_id'] = isset($address_data['data']['country_id']) ? $address_data['data']['country_id'] : '';
		$customer['city'] = isset($address_data['data']['city']) ? $address_data['data']['city'] : '';
		
		return $customer;
	}
}
?>
