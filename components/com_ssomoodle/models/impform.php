<?php

/**
 * @package     com_ssomoodle
 * @copyright   Copyright (C) 2011 ITOffshore Nepal.
 * @license     ITOffshore Nepal
 */
// Check to ensure this file is included in Joomla!
defined('_JEXEC') or die('Restricted access');

jimport('joomla.application.component.model');

// Joomla 3
jimport('joomla.log.logger.formattedtext');


/**
 * Model class for the certification
 *
 * @package		course component
 * @subpackage	certificaiton view
 */
class SsoMoodleModelImpForm extends JModelLegacy
{    
    /**
	 * Method to auto-populate the model state.
	 *
	 * Note. Calling getState in this method will result in recursion.
	 *
	 * @since	1.6
	 */
	protected function populateState()
	{
		$app = JFactory::getApplication('site');
		// Load the parameters.
		$params = $app->getParams();
		$this->setState('params', $params);
	}
	
	/**
     * Returns a Table object, always creating it
     *
     * @param	type	The table type to instantiate
     * @param	string	A prefix for the table class name. Optional.
     * @param	array	Configuration array for model. Optional.
     * @return	JTable	A database object
     */
    public function getTable($type = 'ImpForm', $prefix = 'SsoMoodleTable', $config = array()) {
        return JTable::getInstance($type, $prefix, $config);
    }
    
    /**
     * get published courses
     * @return type  array
     */
    public function getCourses()
    {
        $lang = JFactory::getLanguage();
        $langTag = $lang->getTag();

        $query = $this->_db->getQuery(true);

        $query->select('id')
                ->select('title')
                ->from('#__moodle_courses')
                ->where('state = 1')
                ->where('language in (' . '"' . $langTag . '"' . ',' . '"*"' . ')')
                ->order('title ASC');


        $this->_db->setQuery($query);

        return $this->_db->loadObjectList();
    }

    /**
     * check where key typed by student match with key stored on db
     * @return type string
     */
    public function isValidStudentkey()
    {
        $studentKey = JRequest::getVar('student_key');
        if (!$studentKey) {
            return false;
        }

        $query = $this->_db->getQuery(true);

        $query->select('COUNT(studentkey)')
                ->from('#__course_studentkeys')
                ->where('studentkey = "' . trim($studentKey) . '"')
                ->where('state != "-2"')
                ->where('status_imp = \'0\'');
		//->where('status = 0');

        $this->_db->setQuery($query);
        
        return $this->_db->loadResult();
    }

    private  function _isValidStudentkey($studkey)
    {
        $studentKey = $studkey;
        if (!$studentKey) {
            return false;
        }

        $query = $this->_db->getQuery(true);

        $query->select('COUNT(studentkey)')
            ->from('#__course_studentkeys')
            ->where('studentkey = "' . trim($studentKey) . '"')
            ->where('state != "-2"')
            ->where('status_imp = \'0\'');
        //->where('status = 0');

        $this->_db->setQuery($query);

        return $this->_db->loadResult();
    }
    
    /**
     * Is this studentkey already certificated ?
     * @return type
     */
    public function hasCertificate($studkey) {
        $studentKey = $studkey;
        if (!$studentKey) {
            return false;
        }

        $query = $this->_db->getQuery(true);
        $query->select('COUNT(studentkey)')
            ->from('#__course_certificates')
            ->where('studentkey = "' . trim($studentKey) . '"')
            ->where('status = \'1\'');
        $this->_db->setQuery($query);
        return $this->_db->loadResult();   
    }

    public function isValidToRedirect($userid,$useremail) {
//        $config = array(
//            'text_file' => 'impform.log'
//        );
//        $logger = new JLogLoggerFormattedtext($config);
//        $comment = 'test'.$userid.$useremail;
//        $status = JLog::ERROR;
//
//        $entry = new JLogEntry($comment, $status);
//        $logger->addEntry($entry);

        if (!$userid) {
            return false;
        }

        if (!$useremail) {
            return false;
        }
        $option = array(); //prevent problems
         
        $option['driver']   = 'mysql';            // Database driver name
        $option['host']     = 'localhost';    // Database host name
        $option['user']     = 'prod_academy';       // User for database authentication
        $option['password'] = 'KD8VDqKZYCrqYtcJ';   // Password for database authentication
        $option['database'] = 'imi_prod_academy';      // Database name
         
        $db = JDatabaseDriver::getInstance( $option );

        $query = $db->getQuery(true)
            ->select('timeend')
            ->from('mdl_user_enrolments as enrolment')
            ->join('INNER','mdl_user as user ON user.id = enrolment.userid')
            ->where('user.email = \''. $useremail . '\' and enrolment.enrolid = 13');

        $date = JFactory::getDate()->toUnix();

        $db->setQuery($query);
        $result = $db->loadObject();


        if ($date > $result->timeend) {
            return false;
        }else return true;
    }


    /**
     * Save the certificaion request
     * @return type
     */
    public function saveImpForm()
    {
        $user = & JFactory::getUser();
        $data = JRequest::getVar('jform', array(), 'post', 'array');
        //echo '<pre>';print_r($data); echo '</pre>'; die();
        $data['course_date'] = date('Y-m-d', strtotime($data['course_date']));
        
        $table = $this->getTable();
		
        if (!$table->bind($data)) {
            $this->setError($table->getError());
            return false;
        }
        if (!$table->check()) {
            $this->setError($table->getError());
            return false;
        }
        if (!$table->store()) {
            $this->setError($table->getError());
            return false;
        }
    $test = $this->_isValidStudentkey($data['student_key']);
  if($test == '1') {
        $this->_disableUsedStudentKeys($user->id, $data['student_key']);
        $this->assignUserGroup($user->id);
        return true;
   }
        return false;
    }

    private function _disableUsedStudentKeys($user_id, $key)
    {
        $query = 'UPDATE #__course_studentkeys SET status_imp = \'1\', user_id="'. $user_id .'" WHERE studentkey="' . $key . '"';

        $this->_db->setQuery($query);
        if (!$this->_db->query()) {
            return false;
        }
        return true;
    }
    
    private function assignUserGroup($userid)
    {
    	$query = 'SELECT user_id FROM #__user_usergroup_map WHERE user_id='.$userid.' AND group_id=18';
        $this->_db->setQuery($query);
        if(!$this->_db->loadResult()){
			$query = 'INSERT INTO #__user_usergroup_map SET user_id='.$userid.', group_id=18';
	        $this->_db->setQuery($query);
	        if (!$this->_db->query()) {
	            return false;
	        }
		}
        return true;
    }



}

?>
