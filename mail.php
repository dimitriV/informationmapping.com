
<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml">
<head>
    <meta content="text/html; charset=utf-8" http-equiv="Content-Type" />
    <!-- Facebook sharing information tags -->
    <meta property="og:title" content="%%subject%%" />
    <title>%%subject%%</title>
</head>
<body bgcolor="#FAFAFA" style="-webkit-text-size-adjust: 100%; -ms-text-size-adjust: 100%; height: 100% !important; width: 100% !important; background-color: #FAFAFA; margin: 0; padding: 0;">
<style type="text/css">
    #outlook a {
        padding: 0;
    }
    .body{
        width: 100% !important;
        -webkit-text-size-adjust: 100%;
        -ms-text-size-adjust: 100%;
        margin: 0;
        padding: 0;
    }
    .ExternalClass {
        width:100%;
    }
    .ExternalClass,
    .ExternalClass p,
    .ExternalClass span,
    .ExternalClass font,
    .ExternalClass td,
    .ExternalClass div {
        line-height: 100%;
    }
    img {
        outline: none;
        text-decoration: none;
        -ms-interpolation-mode: bicubic;
    }
    a img {
        border: none;
    }
    p {
        margin: 1em 0;
    }
    table td {
        border-collapse: collapse;
    }
    /* hide unsubscribe from forwards*/
    blockquote .original-only, .WordSection1 .original-only {
        display: none !important;
    }
    @media only screen and (max-width: 480px){
        body, table, td, p, a, li, blockquote{-webkit-text-size-adjust:none !important;} /* Prevent Webkit platforms from changing default text sizes */
        body{width:100% !important; min-width:100% !important;} /* Prevent iOS Mail from adding padding to the body */
        #bodyCell{padding:10px !important;}
        #templateContainer{
            max-width:550px !important;
            width:100% !important;
        }
        h1{
            font-size:28px !important;
            line-height:100% !important;
        }
        h2{
            font-size:24px !important;
            line-height:100% !important;
        }
        h3{
            font-size:18px !important;
            line-height:100% !important;
        }
        h4{
            font-size:16px !important;
            line-height:100% !important;
        }
        #templatePreheader{display:none !important;} /* Hide the template preheader to save space */
        #headerImage{
            height:auto !important;
            max-width:550px !important;
            width:100% !important;
        }
        .headerContent{
            font-size:20px !important;
            line-height:125% !important;
        }
        #bodyImage{
            height:auto !important;
            max-width:510px !important;
            width:100% !important;
        }
        .bodyContent{
            font-size:16px !important;
            line-height:125% !important;
        }
        .templateColumnContainer{display:block !important; width:100% !important;}
        .columnImage{
            height:auto !important;
            max-width:260px !important;
            width:100% !important;
        }
        .leftColumnContent{
            font-size:16px !important;
            line-height:125% !important;
        }
        .rightColumnContent{
            font-size:16px !important;
            line-height:125% !important;
        }
        .footerContent{
            font-size:14px !important;
            line-height:115% !important;
        }
        .footerContent a{display:block !important;} /* Place footer social and utility links on their own lines, for easier access */
    }
</style>
<p align="center" style="font-size: 6px; margin: 0px; color: #fafafa;">Download our free paper</p><p align="center" style="font-size: 10px; margin: 10px 0px; color: #444444; font-family: arial;"><a href="%%view_online%%" target="_blank" style="color: #009edf;">View message in your browser</a>&nbsp;&nbsp;|&nbsp;&nbsp;<a href="%%addthis_url_email%%" target="_blank" style="color: #009edf;">Send this message to a colleague</a></p>
<table align="center" border="0" cellpadding="0" cellspacing="0" id="bodyTable" style="-ms-text-size-adjust: 100%; -webkit-text-size-adjust: 100%; background-color: #FAFAFA; border-collapse: collapse !important; height: 100% !important; margin: 0; mso-table-lspace: 0pt; mso-table-rspace: 0pt; padding: 0; width: 100% !important">
    <tbody><tr>
        <td align="center" id="bodyCell" style="-webkit-text-size-adjust: 100%; -ms-text-size-adjust: 100%; mso-table-lspace: 0pt; mso-table-rspace: 0pt; height: 100% !important; width: 100% !important; margin: 0;" valign="top">
            <!-- BEGIN TEMPLATE // -->
            <table border="0" cellpadding="0" cellspacing="0" id="templateContainer" style="-webkit-text-size-adjust: 100%; -ms-text-size-adjust: 100%; mso-table-lspace: 0pt; mso-table-rspace: 0pt; border-collapse: collapse !important; width: 550px; border: 1px solid #dddddd;">
                <tbody>
                <tr>
                    <td align="center" style="-webkit-text-size-adjust: 100%; -ms-text-size-adjust: 100%; mso-table-lspace: 0pt; mso-table-rspace: 0pt;" valign="top">
                        <!-- BEGIN BODY // -->
                        <table border="0" cellpadding="0" cellspacing="0" id="templateBody" style="-ms-text-size-adjust: 100%; -webkit-text-size-adjust: 100%; background-color: #FFFFFF;  border-collapse: collapse !important; border-top-color: #FFFFFF; border-top-style: solid; border-top-width: 1px; mso-table-lspace: 0pt; mso-table-rspace: 0pt" width="100%">
                            <tbody><tr>
                                <td align="left" valign="top" bgcolor="#009edf" class="bodyContent" style="-webkit-text-size-adjust: 100%; -ms-text-size-adjust: 100%; mso-table-lspace: 0pt; mso-table-rspace: 0pt; color: #ffffff; font-family: Arial; font-size: 35px; line-height: 40px; text-align: left;" pardot-region="body_content00"> <p style="margin: 20px 20px;"><a href="http://www.informationmapping.com/us" target="_blank"><img src="http://www2.informationmapping.com/l/8622/2015-04-16/ln1pt/8622/127186/im_logo_white_on_blue.png" alt="Information Mapping" width="137" height="50" border="0"></a></p></td>
                                <td align="left" valign="top" bgcolor="#009edf" class="bodyContent" style="-webkit-text-size-adjust: 100%; -ms-text-size-adjust: 100%; mso-table-lspace: 0pt; mso-table-rspace: 0pt; color: #ffffff; font-family: Arial; font-size: 35px; line-height: 40px; text-align: left;" pardot-region="body_content00"><table align="right" border="0" cellpadding="2" style="margin-right: 40px;">
                                        <tbody>
                                        <tr>
                                            <td>
                                                <p style="color: #ffffff; line-height: 18px; font-family: Arial, Helvetica, sans-serif; font-size: 12px; margin: 30px 20px;" align="right" >Share:</p>
                                            </td>
                                            <td width="32"><a href="https://www.linkedin.com/groups?gid=983927" target="_blank"><img alt="LinkedIn" src="http://www2.informationmapping.com/l/8622/2015-04-28/m3chw/8622/127644/32_linkedin_circle.png" style="width: 32px; height: 32px; border-width: 0px; border-style: solid;" border="0" height="32" width="32"></a></td>
                                            <td width="32"><a href="https://twitter.com/infomap" target="_blank"><img alt="Twitter" src="http://www2.informationmapping.com/l/8622/2015-03-09/jbr4f/8622/125234/32_twitter_circle.png" style="width: 32px; height: 32px; border-width: 0px; border-style: solid;" border="0" height="32" width="32"></a></td>
                                        </tr>
                                        </tbody>
                                    </table></td>
                            </tr>



                            </tbody></table>
                        <!-- // END BODY -->
                    </td>
                </tr>


                <tr>
                    <td pardot-region="right_column_content" align="left" class="bodyContent" style="-webkit-text-size-adjust: 100%; -ms-text-size-adjust: 100%; mso-table-lspace: 0pt; mso-table-rspace: 0pt; color: #505050; font-family: Arial; font-size: 16px; line-height: 24px; text-align: left;" bgcolor="#ffffff"><h1 style="margin: 40px 20px 10px 20px; font-size: 28px; line-height: 32px">Does your organization care enough about good writing?</h1>
                        <p style="margin: 5px 20px 0px 20px;">I’m sure you can think of times when poorly written business communications have caused problems or even increased risk for your organization. Most managers can—so why do so few of them care about good writing?</p></td>
                </tr>
                <tr>
                    <td align="center" style="-webkit-text-size-adjust: 100%; -ms-text-size-adjust: 100%; mso-table-lspace: 0pt; mso-table-rspace: 0pt;" valign="top">
                        <!-- BEGIN COLUMNS // -->
                        <table border="0" cellpadding="20" cellspacing="0" id="templateColumns" style="-ms-text-size-adjust: 100%; -webkit-text-size-adjust: 100%; background-color: #FFFFFF; border-bottom-color: #ffffff; border-bottom-style: solid; border-bottom-width: 0px; border-collapse: collapse !important; border-top-color: #FFFFFF; border-top-style: solid; border-top-width: 1px; mso-table-lspace: 0pt; mso-table-rspace: 0pt" width="100%">
                            <tbody><tr pardot-repeatable="" class="">
                                <td align="left" style="padding-bottom: 0; -webkit-text-size-adjust: 100%; -ms-text-size-adjust: 100%; mso-table-lspace: 0pt; mso-table-rspace: 0pt;" valign="top" bgc="">
                                    <table align="center" border="0" cellpadding="0" cellspacing="0" class="templateColumnContainer" style="-webkit-text-size-adjust: 100%; -ms-text-size-adjust: 100%; mso-table-lspace: 0pt; mso-table-rspace: 0pt; border-collapse: collapse !important; display: inline; " bgcolor="#fafafa">
                                        <tbody>
                                        <tr>
                                            <td pardot-region="left_column_content" align="center" class="leftColumnContent" style="color: rgb(80, 80, 80); font-family: Arial; font-size: 14px; line-height: 22px; text-align: left; padding: 0px; background: rgb(225, 242, 250);" valign="top" bgcolor="#f5f5f5" pardot-data=""><h2 style="color: #009edf; display: block; font-family: Helvetica; font-size: 20px; font-style: normal; font-weight: bold; letter-spacing: normal; line-height: 26px; margin: 20px; text-align: center; "><a href="http://www.informationmapping.com/en/resources/whitepapers/545-why-managers-should-care-about-writing" style="color: #009edf; text-decoration: none;">DOWNLOAD FREE PAPER</a></h2>

                                                <p align="center"><a href="http://www.informationmapping.com/en/resources/whitepapers/545-why-managers-should-care-about-writing" target="_blank"><img alt="paper thumbnail" height="155" src="http://www2.informationmapping.com/l/8622/2015-12-14/26pch4/8622/137389/managers_care_paper_thumb_email.png" style="width: 260px; height: 150px;" width="275"></a></p>

                                                <p style="margin: 20px; font-size: 16px; line-height: 24px;">Download <em><a href="http://www.informationmapping.com/en/resources/whitepapers/545-why-managers-should-care-about-writing" style="color: #009edf"><strong>Why Managers Should Care About Writing</strong></a></em>, to find out why it’s time for managers and other leaders to acknowledge the importance of good writing skills. You’ll also learn how you can help your organization gain efficiency and competitive advantage through better written communications.</p>

                                                <table align="center" border="0" cellpadding="0" cellspacing="0">
                                                    <tbody>
                                                    <tr>
                                                        <td style="text-align:center; ">
                                                            <table border="0" cellpadding="0" cellspacing="0" style="margin:0 auto;">
                                                                <tbody>
                                                                <tr>
                                                                    <td align="center" bgcolor="#d4145a" style="-webkit-border-radius: 3px; -moz-border-radius: 3px; border-radius: 3px;">
                                                                        <p style="margin: 10px 15px;"><a href="http://www.informationmapping.com/en/resources/whitepapers/545-why-managers-should-care-about-writing" style="font-size: 14px; font-family: Helvetica, Arial, sans-serif; color: #ffffff; text-decoration: none; color: #ffffff; text-decoration: none; -webkit-border-radius: 3px; -moz-border-radius: 3px; border-radius: 3px; padding: 5px 10px; display: inline-block;" target="_blank"><strong>DOWNLOAD NOW!</strong></a></p>
                                                                    </td>
                                                                </tr>
                                                                </tbody>
                                                            </table>
                                                        </td>
                                                    </tr>
                                                    </tbody>
                                                </table><br />

                                            </td>
                                        </tr>
                                        </tbody></table></td>
                            </tr>


                            <tr>
                                <td pardot-region="right_column_content" align="left" class="bodyContent" style="-webkit-text-size-adjust: 100%; -ms-text-size-adjust: 100%; mso-table-lspace: 0pt; mso-table-rspace: 0pt; color: #505050; font-family: Arial; font-size: 16px; line-height: 24px; text-align: left;" bgcolor="#ffffff" valign="top">


                                    <br>
                                    <p style="border-top: 1px solid #cccccc; padding-top: 20px; margin-bottom: 5px;" align="center"><strong>JOIN US FOR A FREE WEBINAR</strong></p>
                                    <p style="font-family: Arial, Helvetica, sans-serif; color: #505050; line-height: 20px; font-size: 16px; margin: 0px 0px 5px 0px;" align="center"><strong><a href="http://www.informationmapping.com/en/5-essentials" style="color: #009edf; text-decoration: none;">5 Essentials to Help Non-writers Create <br />
                                                Effective, Usable Documents</a> </strong></p>
                                    <p style="font-family: Arial, Helvetica, sans-serif; color: #505050; line-height: 20px; font-size: 14px; margin: 5px 0px 20px 0px; border-bottom: 1px solid #cccccc; padding-bottom: 10px;" align="center">January 7, 2016&nbsp;&nbsp;|&nbsp;&nbsp;9 am and 2 pm EST | <a href="http://www.informationmapping.com/en/5-essentials" style="color: #009edf;"><strong>REGISTER</strong></a><strong></strong></p>
                                </td>
                            </tr>
                            <tr>
                                <td bgcolor="#f5f5f5">
                                    <table id="templateFooter" style="-ms-text-size-adjust: 100%; -webkit-text-size-adjust: 100%; background-color: #f5f5f5; mso-table-lspace: 0pt; mso-table-rspace: 0pt" align="center" border="0" cellpadding="0" cellspacing="0">
                                        <tbody>
                                        <tr>
                                            <td class="footerContent" pardot-region="footer_content01" style="-webkit-text-size-adjust: 100%; -ms-text-size-adjust: 100%; mso-table-lspace: 0pt; mso-table-rspace: 0pt; color: #808080; font-family: Arial; font-size: 12px; line-height: 15px; text-align: left; padding: 10px;" align="left" valign="top"><p style="margin: 10px 20px; color: #666; line-height: 18px; font-family: Arial, Helvetica, sans-serif; font-size: 12px;" align="center"><strong>Information Mapping, Inc.</strong><br>
                                                    135 Beaver Street &nbsp;|&nbsp;&nbsp;Waltham, MA 02452<br>
                                                    <a href="http://www.informationmapping.com" style="color: #666;" target="_self">Visit our website</a><br>
                                                    <a href="%%unsubscribe%%" style="color: #666;" target="_self">Unsubscribe</a></p>
                                                <table align="center" border="0" cellpadding="2">
                                                    <tbody>
                                                    <tr>
                                                        <td>
                                                            <p style="color: #666; line-height: 18px; font-family: Arial, Helvetica, sans-serif; font-size: 12px;" align="right">Follow Information Mapping on:</p>
                                                        </td>
                                                        <td width="32"><a href="https://www.linkedin.com/groups?gid=983927" target="_blank"><img alt="LinkedIn" src="http://www2.informationmapping.com/l/8622/2015-04-02/kghjw/8622/126538/32_linkedin_circle_666.png" style="width: 32px; height: 32px; border-width: 0px; border-style: solid;" border="0" height="32" width="32"></a></td>
                                                        <td width="32"><a href="https://twitter.com/infomap" target="_blank"><img alt="Twitter" src="http://www2.informationmapping.com/l/8622/2015-04-02/kghjy/8622/126540/32_twitter_circle_666.png" style="width: 32px; height: 32px; border-width: 0px; border-style: solid;" border="0" height="32" width="32"></a></td>
                                                    </tr>
                                                    </tbody>
                                                </table>
                                                <p style="margin: 10px 20px; color: #666; line-height: 18px; font-family: Arial, Helvetica, sans-serif; font-size: 12px;" align="center"><a href="http://www.informationmapping.com/demo/" style="color: #666;">Uncover Information Mapping in 60 seconds</a></p>
                                            </td>
                                        </tr>
                                        </tbody>
                                    </table>
                                </td>
                            </tr>
                            </tbody></table>
                        <!-- // END COLUMNS -->
                    </td>
                </tr>

                </tbody></table>
            <!-- // END TEMPLATE -->


            <br>
            <!--
                  This email was originally designed by the wonderful folks at MailChimp and remixed by Pardot.
                  It is licensed under CC BY-SA 3.0
                --></td></tr></tbody></table>
</body>
</html>
 
