<?php
include_once("connect.php");
include("header.php"); ?>
<script>
    $(document).ready(function() {
        $( "#exp" ).click(function() {
            $("#sub").submit();
        });
    });
</script>
<body>
<?php
if($_POST['validate']){
    $pass = isset($_POST['pass']) ? trim($_POST['pass']) : '';
    $_SESSION['wachtwoord'] = $pass;
}
if(empty($_SESSION['wachtwoord']))
{  include('login.php'); }
if(!empty($_SESSION['wachtwoord']) && $_SESSION['wachtwoord'] != $password){
    include('loginIncorrect.php'); }
if($_SESSION['wachtwoord'] == $password)
{
    include('nav.php'); ?>

<form id="sub" method="post" enctype="multipart/form-data" action="csv.php">
    <p class="rechts" ><a  href="create.php" ><i class="fa fa-plus-square fa-2x"></i></a></p>
    <table class="table table-striped" id="dataTables-users">
        <thead>
        <tr><th>ID</th><th>First name</th><th>Last name</th><th>Email</th><th>Lucky Number</th><th>Date</th><th>Winner</th><th>Validated</th><th>Comments</th><th>Edit/Add</th><th>Delete</th></tr>
        </thead>
        <tbody>
        <?php
        $result = mysql_query("SELECT * FROM $TableName");
        $aantal = 1;
        while($row = mysql_fetch_array($result))
        {

            echo "<tr>";
            echo "<td class='tdextra'>" . $aantal++ . "</td>";
            echo "<td>" . $row['firstname'] . "</td>";
            echo "<td>" . $row['lastname'] . "</td>";
            echo "<td class='tdextra'>" . $row['email'] . "</td>";
            echo "<td>" . $row['luckynumber'] . "</td>";
            echo "<td>" . $row['timestamp'] . "</td>";
            echo "<td>";
            if($row['won'] == "0") { echo "no"; } else { echo "yes"; }
            echo "</td>";
            echo "<td>";
            if($row['validated'] == "0") { echo "no"; } else { echo "yes"; }
            echo "</td>";
            echo "<td>";
            if($row['extra'] == "") { echo "&nbsp;"; } else { echo $row['extra']; }
            echo "</td>";
            echo "<td><a href=\"edit.php?i=" . $row['id'] . "\"><i class='fa fa-edit fa-2x'></a></td>";
            echo "<td><a href=\"Delete.php?i=" . $row['id'] . "\"><i class='fa fa-trash-o fa-2x'></a></td>";
            echo "</tr>\n";
        }
        ?>
        </tbody>
    </table>
    </form>
    <script>
        $(document).ready(function() {
            $('#dataTables-users').dataTable();
        });
    </script>
<?php
}
include("footer.php");
?>
</body>
</html>