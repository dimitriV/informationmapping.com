<nav class="navbar navbar-default navbar-fixed-top" role="navigation">
        <div class="container-fluid">
            <!-- Brand and toggle get grouped for better mobile display -->
            <div class="navbar-header">
                <button type="button" class="navbar-toggle collapsed " data-toggle="collapse" data-target="#bs-example-navbar-collapse-1">
                    <span class="sr-only">Toggle navigation</span>
                    <span class="icon-bar"></span>
                    <span class="icon-bar"></span>
                    <span class="icon-bar"></span>
                </button>
                <a class="navbar-brand extra" href="http://www.informationmapping.com/<?= $TableName ?>"><img
                        src="images/im.png" class="img-responsive" alt="Information Mapping"></a>
            </div>

            <!-- Collect the nav links, forms, and other content for toggling -->
            <div class="collapse navbar-collapse" id="bs-example-navbar-collapse-1">
                <ul class="nav navbar-nav">
                    <li><a href="../<?= $TableName ?>/index.php"><span class="glyphicon glyphicon-share" aria-hidden="true"></span> Frontend</a></li>
                    <li class="active"><a href="../<?= $TableName ?>/overview.php"><i class="fa fa-refresh fa-spin"></i> Overview <span class="sr-only"></span></a></li>
                    <li><a href="../<?= $TableName ?>/winner.php"><i class="fa fa-play-circle-o fa-lg"></i> Assign Winner</a></li>
                    <li><a href="../<?= $TableName ?>/validate.php"><span class="glyphicon glyphicon-check" aria-hidden="true"></span> Validation Form</a></li>

                </ul>
                    <ul class="nav navbar-nav navbar-right">

                        <?php if( $_SERVER['REQUEST_URI'] == '/'.$TableName.'/overview.php'){?>
                        <li><button type="button" id="exp" class="btn btn-default navbar-btn"><i class="fa fa-download fa-lg"></i> Export CSV</button></li>
                        <?php } ?>
                        <li><a href="../<?= $TableName ?>/logout.php"><i class="fa fa-user fa-fw"></i> Logout</a></li>
                    </ul>
            </div><!-- /.navbar-collapse -->
        </div><!-- /.container-fluid -->
    </nav>
    <div class="container">
        <div class="row">
            <div class="col-sm-9" id="header"><a href="http://www.informationmapping.com/"><img
                        src="images/logo-informationmapping.jpg" width="169" height="71" alt="Information Mapping"></a></div>
            <div class="col-sm-3" id="header2" style="padding-top: 20px; text-align: right; padding-left: 0px;"><a href="http://www.informationmapping.com/" target="_blank"><i class="fa fa-globe fa-lg"></i><span class="col-sm-4"></span> Discover our website</a></div>
        </div>
        <div class="row">
            <div class="col-sm-12">
                <h1><?= $title ?> Back-End Application</h1>
            </div>
        </div>