﻿<?php include_once("connect.php"); ?>

<!DOCTYPE html>
<html>
<head>
    <title><?php echo $title ?></title>
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <!-- Bootstrap -->
    <link href="css/bootstrap.min.css" rel="stylesheet" media="screen">
    <link href="css/bootstrapValidator.min.css" rel="stylesheet" media="screen">
    <link href="css/font-awesome.min.css" rel="stylesheet">
    <!-- HTML5 shim and Respond.js IE8 support of HTML5 elements and media queries -->
    <!--[if lt IE 9]>
    <script src="js/html5shiv.js"></script>
    <script src="js/respond.min.js"></script>
    <![endif]-->
    <link href="//fonts.googleapis.com/css?family=Tauri" rel="stylesheet" type="text/css" />
    <link href="css/imi.css" rel="stylesheet" media="screen">
    <script src="js/jquery.min.js"></script>
</head>
<script>
    $(document).ready(function() {
    $( "#klick" ).click(function() {
        $( "#div_name" ).toggle();
    });
    });
</script>
<body>
<div class="row">
    <div class="col-sm-12" id="header">
        <div class="container">
            <p>&nbsp;</p><a id="klick" href="#"><img src="images/logo.png" width="152" height="63" alt="Information Mapping"></a>
        </div>

    </div>
</div>

<div class="row visible-xs">
    <div class="col-sm-12">
        <div class="container">
            <h1>SEE IF YOU HAVE WON THIS KINDLE</h1>
            <p class="text-center"><img src="images/kindle-klein.png" alt="Kindle e-reader" class="img-responsive"></p>
        </div>
    </div>
</div>

<div class="row" id="content">
<div class="container">
<div class="col-sm-6">
<?php
// implement bootstrap 3
// emailadres mag maar 1 keer per dag deelnemen





/*************************************************
VERZENDINGSGEGEVENS
 *************************************************/

$from = "info@informationmapping.com";
/*$reply = "webmaster@wvdwebdesign.be";
$cc ="";*/
$bcc = "smourabit@informationmapping.com";

// onderwerp mail
$onderwerp = "Information Mapping Kindle";   //See if you have won this e-reader
$onderwerp = html_entity_decode (  $onderwerp, ENT_QUOTES, 'UTF-8' ); // will convert special characters in readable format e.g. &hearts; will be converted to a heart

// Prioriteit: 1 = dringend, 3 = normaal, 5 low
$prioriteit = "1";

/*************************************************
FORMULIERVARIABELEN BEVEILIGEN
 *************************************************/

// zet gevaarlijke tekens in invoervelden om
function cleanup($string="")
{
    $string = trim($string); // verwijdert spaties aan begin en einde
//$string = htmlentities($string); // converteert < > & " en andere speciale tekens naar veilige tekens
    $string = htmlspecialchars($string);
    $string = nl2br($string); // converteert linebreaks in textareas naar <br />
// plaatst escapetekens bij quotes indien nodig
    if(!get_magic_quotes_gpc())
    {
        $string = addslashes($string);
    }

    return $string;
}

// MX-record e-mailadres beveiligen - checkdnsrr() werkt niet op Windowsservers
function check_email($mail_address) {
    $pattern = "/^[\w-]+(\.[\w-]+)*@";
    $pattern .= "([0-9a-z][0-9a-z-]*[0-9a-z]\.)+([a-z]{2,4})$/i";
    if (preg_match($pattern, $mail_address)) {
        $parts = explode("@", $mail_address);
        if (checkdnsrr($parts[1], "MX")){
            // echo "The e-mail address is valid."; 
            return true;
        } else {
            // echo "The e-mail host is not valid."; 
            return false;
        }
    } else {
        // echo "The e-mail address contains invalid characters."; 
        return false;
    }
}

// uniek transactienummer creëren
function randomize()
{
    // transactienummer genereren
    $transactienummer=mt_rand(1000,4000);

    // checken of transactienummer wel uniek is
    $result = mysql_query("SELECT * FROM $TableName WHERE luckynumber = $transactienummer");
    while ($row = mysql_fetch_assoc($result)) {
        $nr = $row['luckynumber'];
    }

    if($nr != $transactienummer)
    {
        return $transactienummer;
    }
    else
    {
        return randomize();
    }
}

/*************************************************
FORMULIERGEGEVENS
 *************************************************/
$firstname = isset($_POST['firstname']) ? cleanup($_POST['firstname']) : '';
$lastname = isset($_POST['lastname']) ? cleanup($_POST['lastname']) : '';
$email = isset($_POST['email']) ? cleanup($_POST['email']) : '';
$extra = isset($_POST['extra']) ? cleanup($_POST['extra']) : '';
$verzenden = isset($_POST['verzenden']) ? cleanup($_POST['verzenden']) : '';

if(isset($verzenden)) {




    if(empty($firstname) || strlen($firstname) > 100)
    {
        $error_firstname = "<div class=\"alert alert-danger\">First name not filled in.</div>";
    }

    if(empty($lastname) || strlen($lastname) > 100)
    {
        $error_lastname = "<div class=\"alert alert-danger\">Last name not filled in.</div>";
    }

    if(empty($email) || strlen($email) > 150)
    {
        $error_email = "<div class=\"alert alert-danger\">E-mail address not filled in.</div>";
    }

    $check_email = mysql_query("SELECT email, timestamp FROM $TableName WHERE email = '$email'");
    $aantal_email = mysql_num_rows($check_email);
    if($aantal_email > 0)
    {
        $error_algespeeld = "<div class=\"alert alert-danger\">You have already submitted this form.</div>";
    }

    if(empty($error_firstname) && empty($error_lastname) && empty($error_email) && empty($error_algespeeld))
    {

        $form_sent = "1";
        echo "<p class=\"feedback\">THANK YOU!</p><p style=\"color: #fff;\">Please check your e-mail to see how you can get the Kindle.</p>
        <p><a href='http://www.informationmapping.com/$TableName' class='btn btn-default btn-lg'>Back</a></p>";

        $transactienummer = randomize();
        $result = mysql_query("INSERT INTO $TableName (firstname, lastname, email, luckynumber, timestamp, extra) VALUES ('$firstname','$lastname','$email', '$transactienummer',NOW(), '$extra')");


        $message = "<!DOCTYPE html PUBLIC \"-//W3C//DTD XHTML 1.0 Transitional//EN\" \"http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd\">
<html xmlns=\"http://www.w3.org/1999/xhtml\">
<head>
    <meta content=\"text/html; charset=utf-8\" http-equiv=\"Content-Type\" />

    <meta property=\"og:title\" content=\"Information Mapping Kindle\" />

    <title>Information Mapping Kindle</title>
</head>

<body bgcolor=\"#FAFAFA\" style=\"-webkit-text-size-adjust: 100%; -ms-text-size-adjust: 100%; height: 100% !important; width: 100% !important; background-color: #FAFAFA; margin: 0; padding: 0;\">
<style type=\"text/css\">
        #outlook a {
        padding: 0;
    }
    .body{
        width: 100% !important;
        -webkit-text-size-adjust: 100%;
        -ms-text-size-adjust: 100%;
        margin: 0;
        padding: 0;
    }
    .ExternalClass {
        width:100%;
    }
    .ExternalClass,
    .ExternalClass p,
    .ExternalClass span,
    .ExternalClass font,
    .ExternalClass td,
    .ExternalClass div {
        line-height: 100%;
    }
    img {
        outline: none;
        text-decoration: none;
        -ms-interpolation-mode: bicubic;
    }
    a img {
        border: none;
    }
    p {
        margin: 1em 0;
    }
    table td {
        border-collapse: collapse;
    }
    /* hide unsubscribe from forwards*/
    blockquote .original-only, .WordSection1 .original-only {
        display: none !important;
    }

    @media only screen and (max-width: 480px){
        body, table, td, p, a, li, blockquote{-webkit-text-size-adjust:none !important;} /* Prevent Webkit platforms from changing default text sizes */
        body{width:100% !important; min-width:100% !important;} /* Prevent iOS Mail from adding padding to the body */

        #bodyCell{padding:10px !important;}

        #templateContainer{
            max-width:600px !important;
            width:100% !important;
        }

        h1{
        font-size:24px !important;
            line-height:100% !important;
        }

        h2{
        font-size:20px !important;
            line-height:100% !important;
        }

        h3{
        font-size:18px !important;
            line-height:100% !important;
        }

        h4{
        font-size:16px !important;
            line-height:100% !important;
        }

        #templatePreheader{display:none !important;} /* Hide the template preheader to save space */

        #headerImage{
            height:auto !important;
            max-width:600px !important;
            width:100% !important;
        }

.headerContent{
    font-size:20px !important;
            line-height:125% !important;
        }

        #bodyImage{
            height:auto !important;
            max-width:560px !important;
            width:100% !important;
        }

        .bodyContent{
    font-size:18px !important;
            line-height:125% !important;
        }

        .templateColumnContainer{display:block !important; width:100% !important;}

        .columnImage{
    height:auto !important;
            max-width:260px !important;
            width:100% !important;
        }

        .leftColumnContent{
    font-size:16px !important;
            line-height:125% !important;
        }

        .rightColumnContent{
    font-size:16px !important;
            line-height:125% !important;
        }

        .footerContent{
    font-size:14px !important;
            line-height:115% !important;
            background-color:#eee !important;
        }

        .footerContent a{display:block !important;} /* Place footer social and utility links on their own lines, for easier access */
    }
</style>

<table align=\"center\" border=\"0\" cellpadding=\"0\" cellspacing=\"0\" id=\"bodyTable\" style=\"-ms-text-size-adjust: 100%; -webkit-text-size-adjust: 100%; background-color: #FAFAFA; border-collapse: collapse !important; height: 100% !important; margin: 0; mso-table-lspace: 0pt; mso-table-rspace: 0pt; padding: 0; width: 100% !important\" width=\"100%\">
    <tbody><tr>
        <td align=\"center\" id=\"bodyCell\" style=\"-webkit-text-size-adjust: 100%; -ms-text-size-adjust: 100%; mso-table-lspace: 0pt; mso-table-rspace: 0pt; height: 100% !important; width: 100% !important; border-top-width: 0px; border-top-color: #ffffff; border-top-style: solid; margin: 0; padding: 20px;\" valign=\"top\">
            <!-- BEGIN TEMPLATE // -->
<table border=\"0\" cellpadding=\"0\" cellspacing=\"0\" id=\"templateContainer\" style=\"-webkit-text-size-adjust: 100%; -ms-text-size-adjust: 100%; mso-table-lspace: 0pt; mso-table-rspace: 0pt; border-collapse: collapse !important; width: 600px; border: 1px solid #ddd;\">
                <tbody><tr>
                    <td align=\"center\" style=\"-webkit-text-size-adjust: 100%; -ms-text-size-adjust: 100%; mso-table-lspace: 0pt; mso-table-rspace: 0pt;\" valign=\"top\">

                    </td>
                </tr>

                <tr>
                    <td align=\"center\" style=\"-webkit-text-size-adjust: 100%; -ms-text-size-adjust: 100%; mso-table-lspace: 0pt; mso-table-rspace: 0pt;\" valign=\"top\">
                        <!-- BEGIN HEADER // -->
<table border=\"0\" cellpadding=\"0\" cellspacing=\"0\" id=\"templateHeader\" style=\"-ms-text-size-adjust: 100%; -webkit-text-size-adjust: 100%; background-color: #FFFFFF; border-bottom-color: #ffffff; border-bottom-style: solid; border-bottom-width: 0px; border-collapse: collapse !important; mso-table-lspace: 0pt; mso-table-rspace: 0pt\" width=\"100%\">
                            <tbody><tr>
                                <td pardot-region=\"header_image\" align=\"left\" class=\"headerContent\" style=\"-webkit-text-size-adjust: 100%; -ms-text-size-adjust: 100%; mso-table-lspace: 0pt; mso-table-rspace: 0pt; color: #505050; font-family: Helvetica; font-size: 20px; font-weight: bold; line-height: 20px; text-align: left; vertical-align: middle; padding: 0;\" valign=\"top\"><img alt=\"Information Mapping\" border=\"0\" height=\"129\" id=\"headerImage\" src=\"http://www.informationmapping.com/ExpoTraining/images/mailheader.png\" style=\"outline: none; text-decoration: none; max-width: 600px; width: 600px; height: 129px; border-width: 0px; border-style: solid;\" width=\"600\"></td>
                            </tr>
                            </tbody></table>
                        <!-- // END HEADER -->
                    </td>
                </tr>

                <tr>
                    <td align=\"center\" style=\"-webkit-text-size-adjust: 100%; -ms-text-size-adjust: 100%; mso-table-lspace: 0pt; mso-table-rspace: 0pt;\" valign=\"top\">
                        <!-- BEGIN BODY // -->
<table border=\"0\" cellpadding=\"0\" cellspacing=\"0\" id=\"templateBody\" style=\"-ms-text-size-adjust: 100%; -webkit-text-size-adjust: 100%; background-color: #FFFFFF; border-bottom-color: #ffffff; border-bottom-style: solid; border-bottom-width: 0px; border-collapse: collapse !important; border-top-color: #FFFFFF; border-top-style: solid; border-top-width: 0px; mso-table-lspace: 0pt; mso-table-rspace: 0pt\" width=\"100%\">
                            <tbody><tr>
                                <td pardot-region=\"body_content00\" align=\"left\" class=\"bodyContent\" style=\"-webkit-text-size-adjust: 100%; -ms-text-size-adjust: 100%; mso-table-lspace: 0pt; mso-table-rspace: 0pt; color: #505050; font-family: Helvetica; font-size: 16px; line-height: 24px; text-align: left; padding: 20px;\" valign=\"top\"><h1>See if you have won the Kindle!</h1><p>Dear $firstname,</p>

<p><strong>CONGRATULATIONS!</strong></p>
<p>You are one of the ExpoTraining Conference attendees who is ready to win the Kindle!
On Friday, June 5, as of 5 PM you can enter your number: <span style=\"color:#009EDF; font-size:26px; font-weight:bold;\">$transactienummer</span> at the Writec booth (Stand A32 - MiCo Ala Nord 0) to see if you’ve won!</p>
<p>Good luck!  </p>
<p>The Writec Team</p>
</td>
                            </tr>

                            </tbody></table>
                        <!-- // END BODY -->
                    </td>
                </tr>



                <tr>
                    <td align=\"center\" style=\"-webkit-text-size-adjust: 100%; -ms-text-size-adjust: 100%; mso-table-lspace: 0pt; mso-table-rspace: 0pt;\" valign=\"top\">
                        <!-- BEGIN FOOTER // -->
                        <table align=\"center\" border=\"0\" cellpadding=\"0\" cellspacing=\"0\" id=\"templateFooter\" style=\"-ms-text-size-adjust: 100%; -webkit-text-size-adjust: 100%; background-color: #eee; border-collapse: collapse !important; border-top-color: #FFFFFF; border-top-style: solid; border-top-width: 0px; mso-table-lspace: 0pt; mso-table-rspace: 0pt\" width=\"100%\">
                            <tbody>

                            <tr>
                                <td pardot-region=\"footer_content01\" align=\"left\" class=\"footerContent\" style=\"-webkit-text-size-adjust: 100%; -ms-text-size-adjust: 100%; mso-table-lspace: 0pt; mso-table-rspace: 0pt; color: #808080; font-family: Helvetica; font-size: 12px; line-height: 15px; text-align: left; padding: 10px;\" valign=\"top\"><p align=\"center\" style=\"margin: 10px 20px; color: #666; line-height: 18px; font-family: Arial, Helvetica, sans-serif; font-size: 12px;\"><strong>WRITEC </strong><br>
via G. di Vittorio, 85 - 25015 Desenzano del Garda (BS) - ITALY<br>
<a href=\"http://www.writec.com\" style=\"color: #666;\" target=\"_self\">Visit our website</a></p>



<p align=\"center\" style=\"margin: 10px 20px; color: #666; line-height: 18px; font-family: Arial, Helvetica, sans-serif; font-size: 12px;\"><a href=\"http://www.informationmapping.com/demo/\" style=\"color: #666;\">Uncover Information Mapping in 60 seconds</a></p>
</td>
                            </tr>


                            </tbody></table>
                        <!-- // END FOOTER -->
                    </td>
                </tr>
                </tbody></table>
            <!-- // END TEMPLATE -->
        </td>
    </tr>
    </tbody></table><br>
</body>
</html>
";


        $bcc = $emailCopyTo;
        // headers seperated by \n on Unix, \r\n on Windows servers
        $headers  = "MIME-Version: 1.0\n";
        $headers .= "Content-type: text/html; charset=utf-8\n";
        $headers .= "From: Information Mapping International <$from>\n";
        $headers .= "BCC: <$bcc>\n";
        $headers .= "Reply-To: IMI <info@informationmapping.com>\n";
        $headers .= "Return-Path: IMI <info@informationmapping.com>";
        $headers .= "X-Priority: $prioriteit\n"; // 3 voor normale prioriteit
        $headers .= "Date: " . date("r") . "\n";

        $to = $email; // e-mailadres van de ontvanger

        mail($to, $onderwerp, $message, $headers, "-f info@informationmapping.com"); // fifth parameter is obliged for some webhosts
        header( "refresh:4;url=index.php" );
    }

}// einde isset $verzenden
?>
<?php
$form_sent = isset($form_sent);
if($form_sent != 1)
{ ?>
    <?php if($error_algespeeld && isset($_POST['verzenden'])) { echo $error_algespeeld; } ?>

    <form id="regexpEmailForm" role="form" action="#" method="post">
        <div class="form-group">
            <div class="col-sm-10">
                <label for="firstname">FIRST NAME</label>
            </div>
            <div class="col-sm-10">
                <input type="text" class="form-control input-lg" id="firstname" name="firstname" value="<?php if (isset($_POST['verzenden'])) { echo $firstname; } ?>">
                <?php if($error_firstname && isset($_POST['verzenden'])) { echo $error_firstname; } ?>
            </div>
        </div>
        <div class="form-group">
            <div class="col-sm-10">
                <label for="lastname">LAST NAME</label>
            </div>
            <div class="col-sm-10">
                <input type="text" class="form-control input-lg" id="lastname" name="lastname" value="<?php if (isset($_POST['verzenden'])) { echo $lastname; } ?>">
                <?php if($error_lastname && isset($_POST['verzenden'])) { echo $error_lastname; } ?>
            </div>
        </div>
        <div class="form-group">
            <div class="col-sm-10">
                <label for="email">E-MAIL ADDRESS</label>
            </div>
            <div class="col-sm-10">
                <div class="input-group margin-bottom-sm">
                    <span class="input-group-addon"><i class="fa fa-envelope-o fa-fw"></i></span>
                    <input type="text" class="form-control input-lg" id="email" name="email" value="<?php if (isset($_POST['verzenden'])) { echo $email; } ?>">
                    <?php if($error_email && isset($_POST['verzenden'])) { echo $error_email; } ?>
                </div>

           </div>
        </div>
        <div id="div_name" style="display:none;">
            <div class="form-group">
                <div class="col-sm-10">
                    <label for="extra">COMMENTS</label>
                </div>
                <div class="col-sm-10">
                    <input type="text" name="extra" id="extra" class="form-control input-lg" value="<?php if (isset($_POST['verzenden'])) { echo $extra; } ?>" />
                </div>
            </div>
        </div>
        <div class="col-sm-10">
            <button id="verzenden" type="submit" class="btn btn-default btn-lg" name="verzenden">I am ready to win!</button>
        </div>
    </form>

    <div class="col-sm-10" id="db">
        <p>Your e-mail address will be added to our newsletter database. You can unsubscribe at any time. We never share or sell your e-mail address with other parties.</p>
    </div>
<?php } // einde $form_sent ?>
<script>
    $(document).ready(function() {
        $('').bootstrapValidator({
                feedbackIcons: {
                    valid: 'glyphicon glyphicon-ok',
                    invalid: 'glyphicon glyphicon-remove',
                    validating: 'glyphicon glyphicon-refresh'
                },
                live: 'enabled',
                fields: {
                    email: {
                        validators: {
                            emailAddress: {
                                message: 'The value is not a valid email address'
                            },
                            regexp: {
                                regexp: '^[^@\\s]+@([^@\\s]+\\.)+[^@\\s]+$',
                                message: 'The value is not a valid email address'
                            }
                        }
                    }
                }
            })
            .on('error.validator.bv', function(e, data) {
                // data.bv        --> The BootstrapValidator instance
                // data.field     --> The field name
                // data.element   --> The field element
                // data.validator --> The current validator name

                if (data.field === 'email') {
                    // The email field is not valid
                    data.element
                        .data('bv.messages')
                        // Hide all the messages
                        .find('.help-block[data-bv-for="' + data.field + '"]').hide()
                        // Show only message associated with current validator
                        .filter('[data-bv-validator="' + data.validator + '"]').show();
                }
            });
    });
</script>
</div>

<div class="col-sm-6 hidden-xs" id="image">
    <img src="images/kindle.png" width="296" height="341" alt="Kindle e-reader">
    <div id="callout"><img src="images/pijl.png" width="358" height="384" alt="See if you have won this e-reader"></div>
</div>
</div>
</div>
<!-- Include all compiled plugins (below), or include individual files as needed -->
<script src="js/jquery.min.js"></script>
<script src="js/bootstrapValidator.min.js"></script>
<script src="js/bootstrap.min.js"></script>


</body>
</html>