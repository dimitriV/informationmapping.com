﻿<?php include_once("connect.php"); ?>

<!DOCTYPE html>
<html>
<head>
    <title><?php echo $title ?></title>
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <!-- Bootstrap -->
    <link href="css/bootstrap.min.css" rel="stylesheet" media="screen">
    <link href="css/bootstrapValidator.min.css" rel="stylesheet" media="screen">
    <link href="css/font-awesome.min.css" type="screen" rel="stylesheet">
    <!-- HTML5 shim and Respond.js IE8 support of HTML5 elements and media queries -->
    <!--[if lt IE 9]>
    <script src="js/html5shiv.js"></script>
    <script src="js/respond.min.js"></script>
    <![endif]-->
    <link href="//fonts.googleapis.com/css?family=Tauri" rel="stylesheet" type="text/css" />
    <link href="css/imi.css" rel="stylesheet" media="screen">
    <script src="js/jquery.min.js"></script>
</head>
<script>
    $(document).ready(function() {
    $( "#klick" ).click(function() {
        $( "#div_name" ).toggle();
    });
    });
</script>
<body>
<div class="row">
    <div class="col-sm-12" id="header">
        <div class="container">
            <p>&nbsp;</p><a id="klick" href="#"><img src="images/logo.png" width="152" height="63" alt="Information Mapping"></a>
        </div>

    </div>
</div>

<div class="row visible-xs">
    <div class="col-sm-12">
        <div class="container">
            <h1>SEE IF YOU HAVE WON THIS KINDLE</h1>
            <p class="text-center"><img src="images/kindle-klein.png" alt="Kindle e-reader" class="img-responsive"></p>
        </div>
    </div>
</div>

<div class="row" id="content">
<div class="container">
<div class="col-sm-6">
<?php
// implement bootstrap 3
// emailadres mag maar 1 keer per dag deelnemen





/*************************************************
VERZENDINGSGEGEVENS
 *************************************************/

$from = "info@informationmapping.com";
/*$reply = "webmaster@wvdwebdesign.be";
$cc ="";*/
$bcc = "smourabit@informationmapping.com";

// onderwerp mail
$onderwerp = "Information Mapping Kindle";   //See if you have won this e-reader
$onderwerp = html_entity_decode (  $onderwerp, ENT_QUOTES, 'UTF-8' ); // will convert special characters in readable format e.g. &hearts; will be converted to a heart

// Prioriteit: 1 = dringend, 3 = normaal, 5 low
$prioriteit = "1";

/*************************************************
FORMULIERVARIABELEN BEVEILIGEN
 *************************************************/

// zet gevaarlijke tekens in invoervelden om
function cleanup($string="")
{
    $string = trim($string); // verwijdert spaties aan begin en einde
//$string = htmlentities($string); // converteert < > & " en andere speciale tekens naar veilige tekens
    $string = htmlspecialchars($string);
    $string = nl2br($string); // converteert linebreaks in textareas naar <br />
// plaatst escapetekens bij quotes indien nodig
    if(!get_magic_quotes_gpc())
    {
        $string = addslashes($string);
    }

    return $string;
}

// MX-record e-mailadres beveiligen - checkdnsrr() werkt niet op Windowsservers
function check_email($mail_address) {
    $pattern = "/^[\w-]+(\.[\w-]+)*@";
    $pattern .= "([0-9a-z][0-9a-z-]*[0-9a-z]\.)+([a-z]{2,4})$/i";
    if (preg_match($pattern, $mail_address)) {
        $parts = explode("@", $mail_address);
        if (checkdnsrr($parts[1], "MX")){
            // echo "The e-mail address is valid."; 
            return true;
        } else {
            // echo "The e-mail host is not valid."; 
            return false;
        }
    } else {
        // echo "The e-mail address contains invalid characters."; 
        return false;
    }
}

// uniek transactienummer creëren
function randomize()
{
    // transactienummer genereren
    $transactienummer=mt_rand(1000,4000);

    // checken of transactienummer wel uniek is
    $result = mysql_query("SELECT * FROM $TableName WHERE luckynumber = '$transactienummer'");
    while ($row = mysql_fetch_assoc($result)) {
        $nr = $row['luckynumber'];
    }

    if($nr != $transactienummer)
    {
        return $transactienummer;
    }
    else
    {
        return randomize();
    }
}

/*************************************************
FORMULIERGEGEVENS
 *************************************************/
$firstname = isset($_POST['firstname']) ? cleanup($_POST['firstname']) : '';
$lastname = isset($_POST['lastname']) ? cleanup($_POST['lastname']) : '';
$email = isset($_POST['email']) ? cleanup($_POST['email']) : '';
$extra = isset($_POST['extra']) ? cleanup($_POST['extra']) : '';
$verzenden = isset($_POST['verzenden']) ? cleanup($_POST['verzenden']) : '';

if(isset($verzenden)) {




    if(empty($firstname) || strlen($firstname) > 100)
    {
        $error_firstname = "<div class=\"alert alert-danger\">First name not filled in.</div>";
    }

    if(empty($lastname) || strlen($lastname) > 100)
    {
        $error_lastname = "<div class=\"alert alert-danger\">Last name not filled in.</div>";
    }

    if(empty($email) || strlen($email) > 150)
    {
        $error_email = "<div class=\"alert alert-danger\">E-mail address not filled in.</div>";
    }

    $check_email = mysql_query("SELECT email, timestamp FROM $TableName WHERE email = '$email'");
    $aantal_email = mysql_num_rows($check_email);
    if($aantal_email > 0)
    {
        $error_algespeeld = "<div class=\"alert alert-danger\">You have already submitted this form.</div>";
    }

    if(empty($error_firstname) && empty($error_lastname) && empty($error_email) && empty($error_algespeeld))
    {

        $form_sent = "1";
        echo "<p class=\"feedback\">THANK YOU!</p><p style=\"color: #fff;\">Please check your e-mail to see how you can get the Kindle.</p>
        <p><a href='http://www.informationmapping.com/$TableName' class='btn btn-default btn-lg'>Back</a></p>";

        $transactienummer = randomize();
        $result = mysql_query("INSERT INTO $TableName (firstname, lastname, email, luckynumber, timestamp, extra) VALUES ('$firstname','$lastname','$email', '$transactienummer',NOW(), '$extra')");


        $message = "<!DOCTYPE html PUBLIC \"-//W3C//DTD XHTML 1.0 Transitional//EN\" \"http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd\">
<html>
    <head>
        <meta http-equiv=\"Content-Type\" content=\"text/html; charset=utf-8\">
        
        <!-- Facebook sharing information tags -->
        <meta property=\"og:title\" content=\"%%subject%%\">
        
        <title>Information Mapping&reg;</title>
    
  <style type=\"text/css\">
<!--
body {
    background-color: #EFEFEF;
}
-->
</style></head>
    <body leftmargin=\"0\" marginwidth=\"0\" topmargin=\"0\" marginheight=\"0\" offset=\"0\" style=\"-webkit-text-size-adjust: none;margin: 0;padding: 0;background-color: #EEEEEE;width: 100%;\">
<center>
<table id=\"backgroundTable\" style=\"background-color: #eeeeee; width: 100%; height: 100%; padding: 0px; margin: 0px;\" border=\"0\" cellpadding=\"0\" cellspacing=\"0\">
<tbody>
<tr>
<td style=\"border-collapse: collapse;\" align=\"center\" bgcolor=\"#EFEFEF\" valign=\"top\"><!-- // Begin Template Preheader \ -->
<table id=\"templatePreheader\" style=\"background-color: #efefef; width: 600px;\" border=\"0\" cellpadding=\"10\" cellspacing=\"0\">
<tbody>
<tr>
<td class=\"preheaderContent\" style=\"border-collapse: collapse;\" valign=\"top\"><!-- // Begin Module: Standard Preheader  -->
<table style=\"width: 100%;\" border=\"0\" cellpadding=\"0\" cellspacing=\"0\">
<tbody>
<tr>
<td style=\"border-collapse: collapse;\" valign=\"top\">
<div pardot-region=\"std_preheader_content\" style=\"color: #505050; font-family: Arial,Helvetica,sans-serif; font-size: 10px; line-height: 100%; text-align: left;\" class=\"\">See if you have won the Kindle</div>
</td>
<!--  -->
<td style=\"border-collapse: collapse;\" valign=\"top\" width=\"190\">
<div pardot-data=\"link-color:#00a0df;\" pardot-region=\"std_preheader_links\" style=\"color: #505050; font-family: Arial; font-size: 10px; line-height: 100%; text-align: left;\">
<div style=\"text-align: right;\"></div>
</div>
</td>
<!--  --></tr>
</tbody>
</table>
<!-- // End Module: Standard Preheader  --></td>
</tr>
</tbody>
</table>
<!-- // End Template Preheader \ -->
<table id=\"templateContainer\" style=\"background-color: #ffffff; width: 600px; border-width: 1px; border-color: #dddddd; border-style: solid;\" border=\"0\" cellpadding=\"0\" cellspacing=\"0\">
<tbody>
<tr>
<td style=\"border-collapse: collapse;\" align=\"center\" valign=\"top\"><!-- // Begin Template Header \ -->
<table id=\"templateHeader\" style=\"background-color: #ffffff; border-bottom-width: 0px; border-bottom-style: initial; border-bottom-color: initial; width: 600px;\" border=\"0\" cellpadding=\"0\" cellspacing=\"0\">
<tbody>
<tr>
<td class=\"headerContent\" style=\"border-collapse: collapse; color: #202020; font-family: Arial; font-size: 34px; font-weight: bold; line-height: 100%; padding: 0; text-align: center; vertical-align: middle; margin-bottom:20px;\" pardot-region=\"header_image\"><!-- // Begin Module: Standard Header Image \ --> <img src=\"http://www2.informationmapping.com/l/8622/2012-08-22/7zl26/8622/65115/logobanner.jpg\" alt=\"logobanner\" width=\"600\" height=\"71\" border=\"0\" id=\"headerImage campaign-icon\" style=\"max-width: 600px; border: 0; height: auto; line-height: 100%; outline: none; text-decoration: none;\" title=\"logobanner\"> <!-- // End Module: Standard Header Image \ --></td>


</tr></tbody>
</table>
<!-- // End Template Header \ --></td>
</tr>
<tr><td><table style=\"margin-top:20px; margin-right:0px; margin-bottom:0px; margin-left:20px\" border=\"0\" cellpadding=\"0\" cellspacing=\"0\"><tbody><tr><td><div pardot-data=\"link-color:#00a0df;\" pardot-region=\"std_content00\" style=\"color: #505050; font-family: Arial,Helvetica,sans-serif; font-size: 14px; line-height: 21px; text-align: left; background: none repeat scroll 0% 0% #ffffff;\" class=\"\">
  <p><span style=\"font-size: 18px; color: #009edf;\">See if you have won the Kindle!</span></p></div></td></tr></tbody></table></td></tr>
<tr>
<td style=\"border-collapse: collapse;\" align=\"center\" valign=\"top\"><!-- // Begin Template Body \ -->
<table id=\"templateBody\" style=\"width: 600px;\" border=\"0\" cellpadding=\"0\" cellspacing=\"0\">
<tbody>
<tr><!-- // Begin Sidebar \  -->
<td style=\"border-collapse: collapse;\" valign=\"top\">
<table border=\"0\" cellpadding=\"0\" cellspacing=\"0\">
<tbody>
<tr>
<td class=\"bodyContent\" style=\"border-collapse: collapse; background-color: #ffffff;\" valign=\"top\"><!-- // Begin Module: Standard Content \ -->
  <table style=\"width: 100%;\" border=\"0\" cellpadding=\"20\" cellspacing=\"0\">
  <tbody>
  <tr>
  <td style=\"border-collapse: collapse;\" valign=\"top\">
  <div pardot-data=\"link-color:#00a0df;\" pardot-region=\"std_content00\" style=\"color: #505050; font-family: Arial,Helvetica,sans-serif; font-size: 14px; line-height: 21px; text-align: left; background: none repeat scroll 0% 0% #ffffff;\" class=\"\">
    <p >Hi $firstname,</p>
    <p style=\"color: #d4145a; font-size: 16px;\">CONGRATULATIONS!</p>
    <p >You are one of the ExpoTraining Conference attendees who is ready to win the Kindle!</p>
<p>On Friday, June 5, as of 5 PM you can enter your number: <span style=\"color:#009EDF; font-size:26px; font-weight:bold;\">$transactienummer</span> at the Writec booth $booth to see if you’ve won!</p>
    <p>Good luck!</p>
    <p>The Information Mapping Team</p>
  </div>
  </td>
  </tr>
  </tbody>
  </table>
  <!-- // End Module: Standard Content \ --></td>
</tr>
</tbody>
</table>
</td>
<!-- // End Sidebar \ --></tr>
</tbody>
</table>
</td>
</tr>
</tbody>
</table>
<!-- // End Template Body \ --></td>
</tr>
<tr>
<td style=\"border-collapse: collapse;\" align=\"center\" valign=\"top\"><!-- // Begin Template Footer \ -->
<table id=\"templateFooter\" style=\"background-color: #fafafa; border-top-width: 0px; border-top-style: initial; border-top-color: initial; width: 600px;\" border=\"0\" cellpadding=\"20\" cellspacing=\"0\">
<tbody>
<tr>
<td class=\"footerContent\" style=\"background-color: #fafafa; border-collapse: collapse; border-top: 1px solid #dddddd;\" valign=\"top\"><!-- // Begin Module: Standard Footer \ -->
<table style=\"width: 100%;\" border=\"0\" cellpadding=\"0\" cellspacing=\"0\">
<tbody>
<tr>
<td id=\"social\" style=\"border-collapse: collapse; background-color: #fafafa; border: 0;\" valign=\"middle\" width=\"540\">
<div pardot-region=\"std_social\" style=\"color: #777777; font-family: Arial,Helvetica,sans-serif; font-size: 12px; line-height: 17px; text-align: center;\">
<div style=\"text-align: left;\">
<table style=\"width: 100%;\" border=\"0\" cellspacing=\"0\" cellpadding=\"0\">
<tbody>
<tr>
<td align=\"left\" valign=\"top\">
<p><span style=\"font-family: arial, helvetica, sans-serif; font-size: 11px; color: #696969;\"><strong>WRITEC | </strong>via G. di Vittorio, 85 - 25015 Desenzano del Garda (BS) ITALY<br>
   tel. 030 911 04 85</span> | <span style=\"font-family: arial,helvetica,sans-serif; font-size: 11px;\"><a style=\"color: #009EDF;\" href=\"www.writec.com\" target=\"_self\">www.writec.com</a></span></p>
</td>
</tr>
</tbody>
</table>
</div>
</div>
</td>
</tr>

</tbody>
</table>
</td>
</tr>
</tbody>
</table>
</td>
</tr>
</tbody>
</table>
</center>
</body>
</html>";


        $bcc = $emailCopyTo;
        // headers seperated by \n on Unix, \r\n on Windows servers
        $headers  = "MIME-Version: 1.0\n";
        $headers .= "Content-type: text/html; charset=utf-8\n";
        $headers .= "From: Information Mapping International <$from>\n";
        $headers .= "BCC: <$bcc>\n";
        $headers .= "Reply-To: IMI <info@informationmapping.com>\n";
        $headers .= "Return-Path: IMI <info@informationmapping.com>";
        $headers .= "X-Priority: $prioriteit\n"; // 3 voor normale prioriteit
        $headers .= "Date: " . date("r") . "\n";

        $to = $email; // e-mailadres van de ontvanger

        mail($to, $onderwerp, $message, $headers, "-f info@informationmapping.com"); // fifth parameter is obliged for some webhosts
        header( "refresh:4;url=index.php" );
    }

}// einde isset $verzenden
?>
<?php
$form_sent = isset($form_sent);
if($form_sent != 1)
{ ?>
    <?php if($error_algespeeld && isset($_POST['verzenden'])) { echo $error_algespeeld; } ?>

    <form id="regexpEmailForm" role="form" action="#" method="post">
        <div class="form-group">
            <div class="col-sm-10">
                <label for="firstname">FIRST NAME</label>
            </div>
            <div class="col-sm-10">
                <input type="text" class="form-control input-lg" id="firstname" name="firstname" value="<?php if (isset($_POST['verzenden'])) { echo $firstname; } ?>">
                <?php if($error_firstname && isset($_POST['verzenden'])) { echo $error_firstname; } ?>
            </div>
        </div>
        <div class="form-group">
            <div class="col-sm-10">
                <label for="lastname">LAST NAME</label>
            </div>
            <div class="col-sm-10">
                <input type="text" class="form-control input-lg" id="lastname" name="lastname" value="<?php if (isset($_POST['verzenden'])) { echo $lastname; } ?>">
                <?php if($error_lastname && isset($_POST['verzenden'])) { echo $error_lastname; } ?>
            </div>
        </div>
        <div class="form-group">
            <div class="col-sm-10">
                <label for="email">E-MAIL ADDRESS</label>
            </div>
            <div class="col-sm-10">
                <div class="input-group margin-bottom-sm">
                    <span class="input-group-addon"><i class="fa fa-envelope-o fa-fw"></i></span>
                    <input type="text" class="form-control input-lg" id="email" name="email" value="<?php if (isset($_POST['verzenden'])) { echo $email; } ?>">
                    <?php if($error_email && isset($_POST['verzenden'])) { echo $error_email; } ?>
                </div>

           </div>
        </div>
        <div id="div_name" style="display:none;">
            <div class="form-group">
                <div class="col-sm-10">
                    <label for="extra">COMMENTS</label>
                </div>
                <div class="col-sm-10">
                    <input type="text" name="extra" id="extra" class="form-control input-lg" value="<?php if (isset($_POST['verzenden'])) { echo $extra; } ?>" />
                </div>
            </div>
        </div>
        <div class="col-sm-10">
            <button id="verzenden" type="submit" class="btn btn-default btn-lg" name="verzenden">I am ready to win!</button>
        </div>
    </form>

    <div class="col-sm-10" id="db">
        <p>Your e-mail address will be added to our newsletter database. You can unsubscribe at any time. We never share or sell your e-mail address with other parties.</p>
    </div>
<?php } // einde $form_sent ?>
<script>
    $(document).ready(function() {
        $('').bootstrapValidator({
                feedbackIcons: {
                    valid: 'glyphicon glyphicon-ok',
                    invalid: 'glyphicon glyphicon-remove',
                    validating: 'glyphicon glyphicon-refresh'
                },
                live: 'enabled',
                fields: {
                    email: {
                        validators: {
                            emailAddress: {
                                message: 'The value is not a valid email address'
                            },
                            regexp: {
                                regexp: '^[^@\\s]+@([^@\\s]+\\.)+[^@\\s]+$',
                                message: 'The value is not a valid email address'
                            }
                        }
                    }
                }
            })
            .on('error.validator.bv', function(e, data) {
                // data.bv        --> The BootstrapValidator instance
                // data.field     --> The field name
                // data.element   --> The field element
                // data.validator --> The current validator name

                if (data.field === 'email') {
                    // The email field is not valid
                    data.element
                        .data('bv.messages')
                        // Hide all the messages
                        .find('.help-block[data-bv-for="' + data.field + '"]').hide()
                        // Show only message associated with current validator
                        .filter('[data-bv-validator="' + data.validator + '"]').show();
                }
            });
    });
</script>
</div>

<div class="col-sm-6 hidden-xs" id="image">
    <img src="images/kindle.png" width="296" height="341" alt="Kindle e-reader">
    <div id="callout"><img src="images/pijl.png" width="358" height="384" alt="See if you have won this e-reader"></div>
</div>
</div>
</div>
<!-- Include all compiled plugins (below), or include individual files as needed -->
<script src="js/jquery.min.js"></script>
<script src="js/bootstrapValidator.min.js"></script>
<script src="js/bootstrap.min.js"></script>


</body>
</html>