<?php 
session_start();
session_unset(); // alle variabelen vrijgeven
session_destroy(); // sessie afsluiten
include("header.php");
include_once('ENV.php');
?>
<body>
<div class="container">
    <div class="row">
        <div class="col-sm-9" id="header"><a href="http://www.informationmapping.com/"><img
                    src="images/logo-informationmapping.jpg" width="169" height="71" alt="Information Mapping"></a></div>
        <div class="col-sm-3" id="header2" style="padding-top: 20px; text-align: right; padding-left: 0px;"><a href="http://www.informationmapping.com/" target="_blank"><i class="fa fa-globe fa-lg"></i><span class="col-sm-4"></span> Discover our website</a></div>
    </div>
    <div class="row">
        <div class="col-sm-12">
            <h1><?= $title ?> Back-End Application</h1>
        </div>
    </div>
<form action="#" method="post">
    <div class="container ">
        <div class="form-group col-sm-6">
            <div class="panel panel-default">
                <div class="panel-heading">
                    <h3 class="panel-title">Logout</h3>
                </div>
                <div class="panel-body">
                    You are logged out successfully.
                </div>
            </div>
        </div>
        <div class="form-group col-sm-6">
        <button type="button" class="btn btn-primary" onclick="location.href='http://www.informationmapping.com/<?= $TableName ?>/overview.php'" >Log in again</button>
        <button type="button" class="btn btn-default" onclick="location.href='http://www.informationmapping.com/<?= $TableName ?>'">Go to Frontend application</button>
    </div>
        </div>
</form>
<?php include("footer.php"); ?>
</body>
</html>