<div class="row" id="footer" style="margin-top: 50px;">
    <div class="col-sm-12" id="footertext">
        <p>&copy; <?php date_default_timezone_set('Europe/Amsterdam'); echo  date("Y"); ?> Information Mapping International | <a
    href="http://www.informationmapping.com/">www.informationmapping.com</a> | <em
    class="fa fa-phone"> </em> +32 9 253 14 25 | Contact our office in <a
    href="http://www.informationmapping.com/us/contact">US</a>, <a
    href="http://www.informationmapping.com/en/contact-3">Europe</a> or <a
    href="http://www.informationmapping.com/in/contact-2">Asia</a></p>

<p class="icons"><a href="https://twitter.com/infomap"><i class="fa fa-twitter-square fa-2x"
                                                          style="color: #009edf;"></i></a> <a
        href="https://www.facebook.com/iminv"><i class="fa fa-facebook-square fa-2x"
                                                 style="color: #009edf;"></i></a> <a
        href="http://www.linkedin.com/groups?gid=983927"><i class="fa fa-linkedin-square fa-2x"
                                                            style="color: #009edf;"></i></a> <a
        href="http://www.youtube.com/user/InformationMapping"><i class="fa fa-youtube-square fa-2x"
                                                                 style="color: #009edf;"></i></a></p>
</div>
</div>
</div>
<!-- Include all compiled plugins (below), or include individual files as needed -->
<script src="js/datatables/jquery.dataTables.js"></script>
<script src="js/datatables/3/dataTables.bootstrap.js"></script>
<script src="js/bootstrapValidator.min.js"></script>
<script src="js/bootstrap.min.js"></script>

