<?php
session_start();
include_once("connect.php");
?>
<!DOCTYPE html>
<html lang="en">
<head>
    <meta charset="utf-8">
    <title><?= $title ?></title>
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <meta http-equiv="X-UA-Compatible" content="IE=10" />
    <!-- Bootstrap -->
    <link href="css/bootstrap.min.css" rel="stylesheet" media="screen">
    <link href="css/font-awesome.min.css" rel="stylesheet">
    <!-- HTML5 shim and Respond.js IE8 support of HTML5 elements and media queries -->
    <!--[if lt IE 9]>
    <script src="js/html5shiv.js"></script>
    <script src="js/respond.min.js"></script>
    <![endif]-->
    <link href="//fonts.googleapis.com/css?family=Tauri" rel="stylesheet" type="text/css" />
    <link href="css/imi.css" rel="stylesheet" media="screen">
</head>
<body>
<div class="row">
    <div class="col-sm-12" id="header">
        <div class="container"> <p>&nbsp;</p><img src="images/logo.png" width="152" height="63" alt="Information Mapping"></div>
    </div>
</div>
<div class="row visible-xs">
    <div class="col-sm-12">
        <div class="container">
            <?php
            if($_SESSION['won'] == "ja")
            {
                echo "<h1>YOU HAVE WON THIS KINDLE</h1>";
            }
            else {
                echo "<h1>SEE IF YOU HAVE WON THIS KINDLE</h1>";
            }
            ?>
            <p class="text-center"><img src="images/kindle-klein.png" alt="Kindle e-reader" class="img-responsive"></p>
        </div>
    </div>
</div>
<div class="row" id="content">
    <div class="container">
        <div class="col-sm-6">
            <?php

            /*************************************************
            VERZENDINGSGEGEVENS
             *************************************************/

            $from = "info@informationmapping.com";
            $bcc = "smourabit@informationmapping.com";


            // Prioriteit: 1 = dringend, 3 = normaal, 5 low
            $prioriteit = "1";

            /*************************************************
            FORMULIERVARIABELEN BEVEILIGEN
             *************************************************/

            // zet gevaarlijke tekens in invoervelden om
            function cleanup($string="")
            {
                $string = trim($string); // verwijdert spaties aan begin en einde
//$string = htmlentities($string); // converteert < > & " en andere speciale tekens naar veilige tekens
                $string = htmlspecialchars($string);
                $string = nl2br($string); // converteert linebreaks in textareas naar <br />
// plaatst escapetekens bij quotes indien nodig
                if(!get_magic_quotes_gpc())
                {
                    $string = addslashes($string);
                }

                return $string;
            }


            /*************************************************
            FORMULIERGEGEVENS
             *************************************************/
            $luckynumber = isset($_POST['luckynumber']) ? cleanup($_POST['luckynumber']) : '';
            $verzenden = isset($_POST['verzenden']) ? cleanup($_POST['verzenden']) : '';

            if(isset($verzenden)) {




                if(empty($luckynumber) || strlen($firstname) > 5)
                {
                    $error_luckynumber = "<div class=\"alert alert-danger\">Number not filled in.</div>";
                }

                if(empty($error_luckynumber))
                {

                    $form_sent = "1";


                    $result = mysql_query("SELECT * FROM $TableName WHERE luckynumber = '$luckynumber' && won = '1'");
                    $aantal_email = mysql_num_rows($result);
                    if($aantal_email > 0)
                    {
                        $row = mysql_fetch_assoc($result);
                        $firstname = $row['firstname'];
                        $lastname = $row['lastname'];
                        $to = $row['email'];
                        $luckynumber = $row['luckynumber'];
// update record to validated
                        $update_validated = mysql_query("UPDATE $TableName SET validated = '1' WHERE luckynumber = '$luckynumber'");

                        echo "<p class=\"feedback\">CONGRATULATIONS!</p><p style=\"color: #fff;\"> $firstname $lastname, you are the winner of our Kindle. </p>";
                        echo "<button class=\"btn btn-default btn-lg\" name=\"tryagain\" type=\"submit\" onClick=\"location.href='validate.php'\">Back</button>";
                        $gewonnen = 'ja';
                        $_SESSION['won'] = "ja";

                    }
                    else {

                        echo "<p class=\"feedback\" style=\"width: 400px;\">UNFORTUNATELY, YOU DID NOT WIN THE KINDLE<!-- TODAY-->.</p>
                        <button class=\"btn btn-default btn-lg\" id=\"tryagain\" type=\"submit\" onClick=\"location.href='validate.php'\">Back</button>";

                        // update record to validated
                        $update_validated = mysql_query("UPDATE $TableName SET validated = '1' WHERE luckynumber = '$luckynumber'");

                        $result = mysql_query("SELECT * FROM $TableName WHERE luckynumber = '$luckynumber'");
                        $row = mysql_fetch_assoc($result);
                        $firstname = $row['firstname'];
                        $lastname = $row['lastname'];
                        $to = $row['email'];
                    }
                }
            }// einde isset $verzenden

            ?>
            <?php
            $form_sent = isset($form_sent);
            if($form_sent != 1)
            {
                ?>
                <form id="myform" role="form" action="#" method="post" ">
                    <div class="form-group">
                        <div class="col-sm-10">
                            <label for="luckynumber">ENTER YOUR NUMBER:</label>
                        </div>
                        <div class="col-sm-10">
                            <input type="number" class="form-control input-lg" id="luckynumber" name="luckynumber" value="<?php if (isset($_POST['verzenden'])) { echo $luckynumber; } ?>">
                            <?php if($error_luckynumber && isset($_POST['verzenden'])) { echo $error_luckynumber; } ?>
                        </div>
                    </div>
                    <div class="col-sm-10">
                        <button type="submit" class="btn btn-default btn-lg" name="verzenden">I am ready to win!</button>
                    </div>
                </form>
            <?php
            }// einde $form_sent
            ?>
        </div>
        <div class="col-sm-6 hidden-xs" id="image"> <img src="images/kindle.png" width="296" height="341" alt="Kindle e-reader">
            <div id="callout">
                <?php
                if($gewonnen == "ja")
                {
                    echo "<img src=\"images/pijl2.png\" width=\"358\" height=\"384\" alt=\"You have won this e-reader\">";
                }
                else {
                    echo "<img src=\"images/pijl.png\" width=\"358\" height=\"384\" alt=\"See if you have won this e-reader\">";
                }
                ?>
            </div>
        </div>
    </div>
</div>


<!-- jQuery (necessary for Bootstrap's JavaScript plugins) -->
<script src="//code.jquery.com/jquery.js"></script>
<!-- Include all compiled plugins (below), or include individual files as needed -->
<script src="js/bootstrap.min.js"></script>
</body>
</html>