<?php
include_once("connect.php");
include("header.php"); ?>
<body>
<?php
if(empty($_SESSION['wachtwoord']))
{  include('login.php'); }
if(!empty($_SESSION['wachtwoord']) && $_SESSION['wachtwoord'] != $password){
    include('loginIncorrect.php'); }
if($_SESSION['wachtwoord'] == $password)
{
    include('nav.php');



    $verzenden = isset($_POST['verzenden']) ? $_POST['verzenden'] : '';
    $extra = isset($_POST['extra']) ? $_POST['extra'] : '';
    $firstname = isset($_POST['firstname']) ? $_POST['firstname'] : '';
    $lastname = isset($_POST['lastname']) ? $_POST['lastname'] : '';
    $email = isset($_POST['email']) ? $_POST['email'] : '';


    if($verzenden)
    {
        function randomize()
        {
            // transactienummer genereren
            $transactienummer=mt_rand(1000,4000);

            // checken of transactienummer wel uniek is
            $result = mysql_query("SELECT * FROM $TableName WHERE luckynumber = '$transactienummer'");
            while ($row = mysql_fetch_assoc($result)) {
                $nr = $row['luckynumber'];
            }

            if($nr != $transactienummer)
            {
                return $transactienummer;
            }
            else
            {
                return randomize();
            }
        }
        $transactienummer = randomize();
        mysql_query("INSERT INTO $TableName(firstname,lastname,email,luckynumber,timestamp,won,validated,extra)  VALUES('$firstname','$lastname','$email','$transactienummer',NOW(),'0','1','$extra')");
        ?>
        <form action="#" method="post">
            <div class="container">
                <div class="form-group">
                    <div class="panel panel-default">
                        <div class="panel-heading">
                            <h3 class="panel-title">Manually add lead.</h3>
                        </div>
                        <div class="panel-body">
                            Lead saved successfully.
                        </div>
                    </div>
                </div>
                <button type="button" class="btn btn-success" onclick="location.href='overview.php'">Back</button>
            </div>
        </form>
    <?php }
    else
    {
        ?>

        <form  id="createForm"  action="#" method="post">
            <div class="container">
                <div class="form-group">
                    <label for="InputFirstName">First Name</label>
                    <input type="text" name="lastname" class="form-control" id="InputFirstName" placeholder="Enter first name">
                </div>
                <div class="form-group">
                    <label for="InputLastName">Last Name</label>
                    <input type="text" name="firstname" class="form-control" id="InputLastName" placeholder="Enter last name">
                </div>
                <div class="form-group">
                    <label for="InputEmail">Email address</label>
                    <input type="email" name="email" class="form-control" id="email" placeholder="Enter email">
                </div>
                <div class="form-group">
                    <label for="LabelForInputExtra">Enter your comment (max. 300 characters):</label>
                    <textarea name="extra" class="form-control" rows="3"></textarea>
                </div>
                <button type="submit" class="btn btn-primary" name="verzenden" value="Save">Save</button>
                <button type="button" class="btn btn-default" onclick="location.href='overview.php'">Back</button>
            </div>
        </form>
        <script>
            $(document).ready(function() {
                $('#createForm').bootstrapValidator({
                    feedbackIcons: {
                        valid: 'glyphicon glyphicon-ok',
                        invalid: 'glyphicon glyphicon-remove',
                        validating: 'glyphicon glyphicon-refresh'
                    },
                    live: 'enabled',
                    fields: {
                        email: {
                            validators: {
                                emailAddress: {
                                    message: 'The value is not a valid email address'
                                },
                                regexp: {
                                    regexp: '^[^@\\s]+@([^@\\s]+\\.)+[^@\\s]+$',
                                    message: 'The value is not a valid email address'
                                }
                            }
                        }
                    }
                })
                    .on('error.validator.bv', function(e, data) {
                        // data.bv        --> The BootstrapValidator instance
                        // data.field     --> The field name
                        // data.element   --> The field element
                        // data.validator --> The current validator name

                        if (data.field === 'email') {
                            // The email field is not valid
                            data.element
                                .data('bv.messages')
                                // Hide all the messages
                                .find('.help-block[data-bv-for="' + data.field + '"]').hide()
                                // Show only message associated with current validator
                                .filter('[data-bv-validator="' + data.validator + '"]').show();
                        }
                    });
            });
        </script>
    <?php
    }
}
include("footer.php");
?>
</body>
</html>