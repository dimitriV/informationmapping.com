<!DOCTYPE html>
<html lang="en">
  <head>
    <meta charset="utf-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <title>Consistent documents within your organization</title>
    <link href="css/bootstrap.min.css" rel="stylesheet" media="screen">
    <link href="//fonts.googleapis.com/css?family=Tauri" rel="stylesheet" type="text/css" />
    <link href="//netdna.bootstrapcdn.com/font-awesome/4.1.0/css/font-awesome.min.css" rel="stylesheet">
	<link href="css/imi-general.css" rel="stylesheet" media="screen">
    <script src="//code.jquery.com/jquery.min.js"></script>
	<script type="text/javascript" src="js/html5lightbox.js"></script>

  </head>
  <body>
  <div class="container">
  	
  	<header class="row">
    	<section class="col-xs-9"> 
        	<a href="http://www.informationmapping.com/"><img src="images/logo-informationmapping.jpg" width="169" height="71" alt="Information Mapping"></a>
        </section>
    	<section class="col-xs-3"> 
        	<a href="http://www.informationmapping.com/" target="_blank"><em class="fa fa-globe"> </em> Discover our website</a>
        </section>
  	</header>
      
    <div class="jumbotron">  
        <div class="row">
            <section class="col-md-3">
        		<p class="hidden-xs hidden-sm"><img src="images/readerblue.png" alt="" class="img-responsive"/></p>
                <p class="visible-xs visible-sm"><img src="images/readerblue_sm.png" alt="" class="img-responsive img-centered"/></p>
            </section>
            <section class="col-md-9">
                <h1>People don’t want to read documentation. They just need to use it.</h1>
                <p>We all prefer documents that are clear and straightforward.</p>
                <p>With <strong>FS Pro 2013 for Microsoft Word</strong> you will write exactly what you want to write and your reader will  understand exactly what you’re saying.</p>
                <p><a class="btn btn-primary btn-lg" href="http://www2.informationmapping.com/fspro2013-trial" role="button"><i class="fa fa-download"></i> &nbsp;Try it and see how it works, free of charge!</a></p>
            </section>
        </div>
    </div>
    
    <div class="row blocks">
  		<section class="col-md-3">
        	<div class="border">
                <p class="text-center gray"><i class="fa fa-thumbs-o-up fa-4x"></i></p>
                <h3>Benefits</h3>
                <p>Using FS Pro 2013, you and your team members will</p>
                <ul>
                    <li>Improve document structure, quality and consistency</li>
                    <li>Speed up writing by more than 50%</li>
                    <li>Focus on content instead of format</li>
                    <li>Adopt a proven, flexible authoring standard</li>
                </ul>
            </div>
        </section>
        
        <section class="col-md-3">
        	<div class="border">
                <p class="text-center gray"><i class="fa fa-paperclip fa-4x"></i></p>
                <h3>What do you get?</h3>
                <p>Make your trusted Microsoft Word more powerful with:</p>
                <ul>
                    <li>A great add-in for Microsoft Word</li>
                    <li>Integrated video tutorials to get started quickly and easily</li>
                    <li>Ready-to-use business document templates</li>
                    <li>Native support for the proven Information Mapping&reg; Method</li>
                </ul>
            </div>
        </section>
		
        <section class="col-md-6 text-center">  
        	<div class="border">
                <p><img src="images/FSPro-ribbon.jpg" alt="" class="img-responsive img-centered"></p>
                <p>Create <strong>professional</strong> and <strong>consistent</strong> documents. Start today!</p>
                <div class="btn-group dropup" >
                    <button type="button" class="btn btn-primary dropdown-toggle" data-toggle="dropdown" aria-expanded="false">
                        Buy FS Pro 2013 for your team and save up to 33% <span class="caret"></span>
                    </button>
                    <ul class="dropdown-menu" role="menu">
                        <li><a href="http://www.informationmapping.com/en/shop/checkout/cart/add?product=392&amp;qty=1&amp;bundle_option[545]=775" target="_blank"><i class="fa fa-user"></i> Buy a single user license for $125</a></li>
                        <li><a href="http://www.informationmapping.com/en/shop/checkout/cart/add?product=486&amp;qty=1&amp;bundle_option[654]=1009" target="_blank"><i class="fa fa-users"></i> Buy a 5-Pack for $559 (save 10%)</a></li>
                        <li><a href="http://www.informationmapping.com/en/shop/checkout/cart/add?product=491&amp;qty=1&amp;bundle_option[727]=1181" target="_blank"><i class="fa fa-users"></i> Buy a 10-Pack for $999 (save 20%)</a></li>
                        <li><a href="http://www.informationmapping.com/en/shop/checkout/cart/add?product=556&amp;qty=1&amp;bundle_option[728]=1189" target="_blank"><i class="fa fa-users"></i> Buy a 50-Pack for $4,675 (save 33%)</a></li>
                    </ul>
                </div>
            </div>
		</section>
    </div>

    <footer class="row">
        <section class="col-sm-12">
        	<div class="border">
            	<section class="col-sm-9">
            		<p>&copy; <?php echo date("Y"); ?> Information Mapping International | Contact our office in <a href="http://www.informationmapping.com/us/contact">US</a>, <a href="http://www.informationmapping.com/en/contact-3">Europe</a> or <a href="http://www.informationmapping.com/in/contact-2">Asia</a></p>
                </section>
                <section class="col-sm-3 text-right">
                    <p> 
                        <a href="https://twitter.com/infomap"><i class="fa fa-twitter-square fa-2x"></i></a> 
                        <a href="https://www.facebook.com/iminv"><i class="fa fa-facebook-square fa-2x"></i></a> 
                        <a href="http://www.linkedin.com/groups?gid=983927"><i class="fa fa-linkedin-square fa-2x"></i></a> 
                        <a href="http://www.youtube.com/user/InformationMapping"><i class="fa fa-youtube-square fa-2x"></i></a> 
                    </p>
            	</section>
            </div>
        </section>
    </footer>
</div>

   <script src="js/bootstrap.min.js"></script> 
   <script src="js/ga.js"></script> 
   <script src="js/pardot.js"></script> 
   <script src="js/remarketing.js"></script>
   <script type="text/javascript" src="//www.googleadservices.com/pagead/conversion.js"></script>
   <script src="js/olark.js"></script>
    <noscript>
        <div style="display:inline;"> 
            <img height="1" width="1" style="border-style:none;" alt="" src="//googleads.g.doubleclick.net/pagead/viewthroughconversion/963549049/?value=0&amp;guid=ON&amp;script=0"/> 
        </div>
    </noscript>
	</body>
</html>