
(function(i,s,o,g,r,a,m){i['GoogleAnalyticsObject']=r;i[r]=i[r]||function(){
    (i[r].q=i[r].q||[]).push(arguments)},i[r].l=1*new Date();a=s.createElement(o),
    m=s.getElementsByTagName(o)[0];a.async=1;a.src=g;m.parentNode.insertBefore(a,m)
})(window,document,'script','//www.google-analytics.com/analytics.js','ga');

ga('create', 'UA-24028581-1', 'auto', {'allowLinker': true});
// On every domain.
ga('require', 'linker');
// List of every domain to share linker parameters.
ga('linker:autoLink', ['informationmapping.com', 'informationmapping-webinars.com']);
ga('send', 'pageview'); // Send hits after initializing the auto-linker plug-in.
ga('require', 'ecommerce');