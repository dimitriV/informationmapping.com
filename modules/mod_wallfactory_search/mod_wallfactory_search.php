<?php
/*------------------------------------------------------------------------
mod_wallfactory_search - Wall Factory
------------------------------------------------------------------------
author    TheFactory
copyright Copyright (C) 2010 SKEPSIS Consult SRL. All Rights Reserved.
@license - http://www.gnu.org/licenses/gpl-2.0.html GNU/GPL
Websites: http://www.thefactory.ro
Technical Support:  Forum - http://www.thefactory.ro/joomla-forum/
-------------------------------------------------------------------------*/

defined('_JEXEC') or die('Restricted access');

$database =& JFactory::getDBO();

// Integration check
$query = ' SELECT COUNT(c.id)'
       . ' FROM #__components c'
       . ' WHERE c.option="com_wallfactory"';
$database->setQuery($query);
$db_check = $database->loadResult();

if (!$db_check)
{
  require(dirname( __FILE__ ).DS.'tmpl'.DS.'error.php');
}
else
{
  global $Itemid;

  $q =& JRequest::getVar('q', '', 'REQUEST', 'string');

  require(dirname(__FILE__).DS.'tmpl'.DS.'default.php');
}