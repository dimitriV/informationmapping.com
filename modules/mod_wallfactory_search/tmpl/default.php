<?php
/*------------------------------------------------------------------------
mod_wallfactory_search - Wall Factory
------------------------------------------------------------------------
author    TheFactory
copyright Copyright (C) 2010 SKEPSIS Consult SRL. All Rights Reserved.
@license - http://www.gnu.org/licenses/gpl-2.0.html GNU/GPL
Websites: http://www.thefactory.ro
Technical Support:  Forum - http://www.thefactory.ro/joomla-forum/
-------------------------------------------------------------------------*/

defined('_JEXEC') or die('Restricted access'); ?>

<form method="GET" action="<?php echo JRoute::_('index.php'); ?>">
  <input type="text" name="q" id="q" value="<?php echo htmlentities($q); ?>" />
  <input type="hidden" name="option" value="com_wallfactory" />
  <input type="hidden" name="controller" value="search" />
  <input type="hidden" name="task" value="search" />
  <input type="hidden" name="Itemid" value="<?php echo $Itemid; ?>" />

  <input type="submit" value="<?php echo JText::_('Search'); ?>" />
</form>