<?php
/*------------------------------------------------------------------------
mod_wallfactory_search - Wall Factory
------------------------------------------------------------------------
author    TheFactory
copyright Copyright (C) 2010 SKEPSIS Consult SRL. All Rights Reserved.
@license - http://www.gnu.org/licenses/gpl-2.0.html GNU/GPL
Websites: http://www.thefactory.ro
Technical Support:  Forum - http://www.thefactory.ro/joomla-forum/
-------------------------------------------------------------------------*/

defined('_JEXEC') or die('Restricted access'); ?>

<?php echo JText::_('You need to install Wall Factory Component'); ?>.

<br /><br />

<a href="http://www.thefactory.ro"><?php echo JText::_('Click to get it'); ?></a>.