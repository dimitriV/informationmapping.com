<?php
/**
 *
 *
 * @package   multitrans
 * copyright Bob Galway
 * @license GPL3
 */

// no direct access
defined('_JEXEC') or die;

//Collect Parameters for content

//Collect Parameters
$server1 = $params->get('server1');
$frame = $params->get('frame');
$site = $params->get('langg1');


$padl = $params->get('paddingleft');
$padr = $params->get('paddingright');
$padt = $params->get('paddingtop');
$padb = $params->get('paddingbottom');
$mart = $params->get('margin-top');
$marb = $params->get('margin-bottom');
$marl = $params->get('margin-leftmodule');
$bgcol = $params->get('colour2');
$width = $params->get('width');
$widthu = $params->get('widthunit');
$tags = $params->get('customtags');


$asia = $params->get('asia');
$other = $params->get('other');
$west = $params->get('west');
$east = $params->get('east');

$selection="";
$indiv=$params->get('indiv');


$height='';
if (($server1==4)){$height='height:27px !important;overflow:hidden !important;';}

$cssmt2="";
$cssmt="";
if (!empty($padr)){$cssmt .= 'padding-right:'.$padr.'px;';}
if (!empty($padl)){$cssmt .= 'padding-left:'.$padl.'px;';}
if (!empty($padt)){$cssmt .= 'padding-top:'.$padt.'px;';}
if (!empty($padb)){$cssmt .= 'padding-bottom:'.$padb.'px;';}
if (!empty($marr)){$cssmt .= 'margin-right:'.$marr.'px;';}
if (!empty($marl)){$cssmt .= 'margin-left:'.$marl.'px;';}
if (!empty($mart)){$cssmt .= 'margin-top:'.$mart.'px;';}
if (!empty($marb)){$cssmt .= 'margin-bottom:'.$marb.'px;';}
if (!empty($bgcol)){$cssmt .= 'background:'.$bgcol.';';}
if (!empty($width)){$cssmt .= 'width:'.$width.$widthu.';';}

//add widget styling

if(!empty($height)){$cssmt2 = 'style="'.$height.'"';}
 if((!empty($cssmt))&& (($server1==4)||($server1==1)||($server1==2)||($server1==3))){$cssmt ='<style type="text/css">#trans{'.$cssmt.'}</style>';}
if (($server1==5)||($server1==6)||($server1==7)||($server1==8)||($server1==0)){$cssmt ='';}

$doc =& JFactory::getDocument();
if(!empty($cssmt)){
$doc->addCustomTag( $cssmt );
}
if(!empty($tags)){
$doc->addCustomTag( $tags );
}

//select language ranges to include

$langs='';
if($other==1){$langs.='af,mt,sw,ht,'; }
if($east==1){$langs.='sq,be,bg,hr,cs,et,el,hu,lv,lt,mk,pl,ro,ru,sr,sk,sl,uk,'; }
if($west==1){$langs.='ca,da,nl,en,fi,fr,gl,de,is,ga,it,no,pt,es,sv,cy,yi,eu,'; }
if($asia==1){$langs.='ar,zh-CN,zh-TW,tl,iw,hi,id,ja,ko,ms,fa,th,tr,vi,ur,hy,az,ka,'; }
if (!empty($indiv)){$langs .= $indiv;}

// common code for widgets

$gdiv='<div id="google_translate_element"></div>';
$gscript1 ='<script type="text/javascript">
function googleTranslateElementInit() {
 	 new google.translate.TranslateElement({';
$gscript2 ='</script>
<script type="text/javascript" src="//translate.google.com/translate_a/element.js?cb=googleTranslateElementInit"></script>';

echo '<div id="trans" ><div '.$cssmt2.'>';

// select and publish widget.

//auto

if ($server1 == 0){
echo $gscript1.'
   	 pageLanguage: \''.$site.'\',
 	 multilanguagePage: true,
})
;}';
echo $gscript2;
}
//Vertical & Basic

if (($server1 == 1)||($server1 == 4)){
	echo $gdiv.$gscript1.'
    pageLanguage: \''.$site.'\',
	includedLanguages:\''.$langs.'\',
	multilanguagePage: true
 },  \'google_translate_element\');
}';
echo $gscript2;
}

//Horizontal

if ($server1 == 2){echo 
	$gdiv.$gscript1.'
	pageLanguage: \''.$site.'\',
	includedLanguages:\''.$langs.'\',
    multilanguagePage: true,
	layout: google.translate.TranslateElement.InlineLayout.HORIZONTAL
  }, \'google_translate_element\');
}';
echo $gscript2;
}

//Simple

if ($server1 == 3){echo  
	$gdiv.$gscript1.'
    pageLanguage: \''.$site.'\',
  	includedLanguages:\''.$langs.'\',
	multilanguagePage: true,
	layout: google.translate.TranslateElement.InlineLayout.SIMPLE
  }, \'google_translate_element\');
}';
echo $gscript2;
}

//Bottom Right

if ($server1 == 5){echo 
	$gscript1.'
    pageLanguage: \''.$site.'\',
    multilanguagePage: true,
	floatPosition: google.translate.TranslateElement.FloatPosition.BOTTOM_RIGHT
  });
}';
echo $gscript2;
}

//Bottom Left

if ($server1 == 6){echo
	$gscript1.'
    pageLanguage: \''.$site.'\',
    multilanguagePage: true,
	floatPosition: google.translate.TranslateElement.FloatPosition.BOTTOM_LEFT
  });
}';
echo $gscript2;
}

//Top Right

if ($server1 == 7){echo
	$gscript1.'
    pageLanguage: \''.$site.'\',
    multilanguagePage: true,
	floatPosition: google.translate.TranslateElement.FloatPosition.TOP_RIGHT
  });
}';
echo $gscript2;
}

//Top Left

if ($server1 == 8){echo'
'.$gscript1.'
    pageLanguage: \''.$site.'\',
    multilanguagePage: true,
	floatPosition: google.translate.TranslateElement.FloatPosition.TOP_LEFT
  });
}';
echo $gscript2;

}




echo '</div></div>';

?>

