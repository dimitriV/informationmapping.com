<?php // no direct access
defined('_JEXEC') or die('Restricted access');
$image_type = $params->get('image_type', 'small');
$showLink = $params->get('showLink', '1');
?>

<?php if(count($items) > 0): ?>
<div class="articleImageBlock<?php echo $params->get('moduleclass_sfx'); ?>">
<?php 	
	foreach($items as $item){
        $image = ($image_type == 'large') ? $item->large_image : $item->small_image;
		if($image != ''):
	?>
			<div class="articleImage">
            <img  src="<?php echo JURI::root().'images/articleimages/small/'.$image;?>"  alt="<?php echo $item->image_title; ?>" />
            <?php if($showLink & $item->link!='' && $item->image_title!=''):?>
            <a href="<?php echo $item->link;?>"><?php echo $item->image_title;?></a>
            <?php endif;?>
            </div>
	<?php
		endif;
	}
?>
</div>
<?php endif; ?>