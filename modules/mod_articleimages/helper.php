<?php
/**
* @version		$Id: helper.php 10857 2008-08-30 06:41:16Z willebil $
* @package		Joomla
* @copyright	Copyright (C) 2005 - 2008 Open Source Matters. All rights reserved.
* @license		GNU/GPL, see LICENSE.php
* Joomla! is free software. This version may have been modified pursuant
* to the GNU General Public License, and as distributed it includes or
* is derivative of works licensed under the GNU General Public License or
* other free or open source software licenses.
* See COPYRIGHT.php for copyright notices and details.
*/

// no direct access
defined('_JEXEC') or die('Restricted access');


class modArticleImageHelper
{
    function getArticleImage(&$params)
    {
       	$db	=& JFactory::getDBO();
        if($params->get('articleid')){
            $aid = $params->get('articleid');
        }
        else{
            $aid = JRequest::getInt('id');
        }
        $sql = "SELECT
                    a.*
                FROM
                    #__article_images AS a
                WHERE
                    a.article_id= '".$aid."'
				ORDER BY
					ordering";
        $db->setQuery($sql);

		return $db->loadObjectList();
    }
}
