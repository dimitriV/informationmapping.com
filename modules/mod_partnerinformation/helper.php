<?php

/**
 * @version     $Id: helper.php  2010-10-23 04:13:25Z $
 * @package     IM 
 * @subpackage  mod_partnerinformation
 * @author      ITOffshore Nepal
 * @copyright   Copyright (C) 2011 ITOffshore Nepal.
 * @license     ITOffshore Nepal
 */
// no direct access
defined('_JEXEC') or die;

class modPartnerInformationHelper
{

    function getList($id)
    {
        $db = JFactory::getDbo();
        $app = JFactory::getApplication();
        $lang = & JFactory::getLanguage();
        $langTag = $lang->getTag();
        $id = JRequest::getVar('id', '0');

        $query = $db->getQuery(true);
        $query->select('*');
        $query->from('#__course_partners WHERE state=1 AND id=' . $id. ' AND language in (' . '"' . $langTag . '"' . ',' . '"*"' . ')');
        
        $db->setQuery($query);
        $list = $db->loadObject();

        return $list;
    }

}

?>