<?php
/**
 * @version     $Id: mod_partnerinformation.php  2010-10-23 04:13:25Z $
 * @package     IM 
 * @subpackage  mod_partnerinformation
 * @author      ITOffshore Nepal
 * @copyright   Copyright (C) 2011 ITOffshore Nepal.
 * @license     ITOffshore Nepal
 */
// no direct access
defined('_JEXEC') or die;

// Include the weblinks functions only once
require_once dirname(__FILE__).'/helper.php';

$list = modPartnerInformationHelper::getList($params);
$view= JRequest::getVar('view');

if (!count($list)) {
	return false;
}

$pretext = $params->get('pretext');
$moduleclass_sfx = htmlspecialchars($params->get('moduleclass_sfx'));

if($view=='detail'){
	require JModuleHelper::getLayoutPath('mod_partnerinformation',$params->get('layout', 'default'));
}
?>