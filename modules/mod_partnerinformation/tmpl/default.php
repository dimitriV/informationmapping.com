<?php
/**
 * @version     $Id: default.php  2010-10-23 04:13:25Z $
 * @package     IM 
 * @subpackage  mod_partnerinformation
 * @author      ITOffshore Nepal
 * @copyright   Copyright (C) 2011 ITOffshore Nepal.
 * @license     ITOffshore Nepal
 */

defined('_JEXEC') or die;

?>

<?php if($pretext): ?>
<div class="pre-text">
	<?php echo $pretext; ?>
</div>
<?php endif; ?>

<div class="partner-contact">
	<span class="title"><?php echo $list->title; ?></span>
	<?php if($list->address || $list->city): ?>
	    <div class="partner-contact-address">
	    	<?php if($list->address): ?>
	    		<span class="address"><?php echo $list->address; ?></span>
	    	<?php endif; ?>
	    	<?php if($list->city): ?>
	        	<span class="city"><?php echo $list->city; ?></span>
	        <?php endif; ?>
		</div>
	<?php endif; ?>
	<?php if($list->phone || $list->fax): ?>
		<div class="partner-contact-number">
			<?php if($list->phone): ?>
	        	<span class="phone"><?php echo $list->phone; ?></span>
	        <?php endif; ?>
	        <?php if($list->fax): ?>
	        	<span class="fax"><?php echo $list->fax; ?></span>
	        <?php endif; ?>
	    </div>
    <?php endif; ?>
    <?php if($list->email || $list->website): ?>
		<div class="partner-contact-url">
			<?php if($list->email): ?>
	        	<span class="email"><a href="mailto:<?php echo $list->email; ?>"><?php echo $list->email; ?></a></span>
	        <?php endif; ?>
	        <?php if($list->website): ?>
	        	<span class="website"><a href="<?php echo $list->website; ?>" target="_blank"><?php echo $list->website; ?></a></span>
	        <?php endif; ?>
		</div>
	<?php endif; ?>
</div>
