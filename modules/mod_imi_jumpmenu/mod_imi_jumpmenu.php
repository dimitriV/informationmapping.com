<?php
/**
 * @version		$Id: mod_weblinks.php 21097 2011-04-07 15:38:03Z dextercowley $
 * @package		Joomla.Site
 * @subpackage	mod_weblinks
 * @copyright	Copyright (C) 2005 - 2009 Open Source Matters, Inc. All rights reserved.
 * @license		GNU General Public License version 2 or later; see LICENSE.txt
 */

// no direct access
defined('_JEXEC') or die;

// Include the weblinks functions only once
require_once dirname(__FILE__).'/helper.php';

$menutype = $params->get('menutype','mainmenu');

$list = modJMenuHelper::getSubMenu($menutype);

$moduleclass_sfx = htmlspecialchars($params->get('moduleclass_sfx'));

$title = htmlspecialchars($params->get('label', 'Currency Switcher'));

/* track active menu */
$menu = &JSite::getMenu();
$active = $menu->getActive();
$Itemid = $active->id;

require JModuleHelper::getLayoutPath('mod_imi_jumpmenu', 'default');
