<?php
/**
 * @version		$Id: helper.php 22152 2011-09-25 18:52:19Z dextercowley $
 * @package		Joomla.Site
 * @subpackage	mod_imi_jumpmenu
 * @copyright	Copyright (C) 2005 - 2009 Open Source Matters, Inc. All rights reserved.
 * @license		GNU General Public License version 2 or later; see LICENSE.txt
 */

// no direct access
defined('_JEXEC') or die;

class modJMenuHelper
{
	public function getSubMenu($menutype)
    {
        /* detect current language */
        $lang =& JFactory::getLanguage();
        $language = $lang->getTag();

        $db =& JFactory::getDBO();
        $query = "SELECT `id`, `title`, `link`, `type`, `params`
                        FROM #__menu 
                    WHERE
						`menutype` = '".$menutype."' 
                    	AND `level` = 1
                        AND `published` = 1 
                        AND `language` IN('*','".$language."') 
                    ORDER BY `lft`";
        $db->setQuery($query);
        
        return $db->loadAssocList();
    }
}
