<?php
// no direct access
defined('_JEXEC') or die('Restricted access');

$facebook_link = htmlspecialchars($params->get('facebook_link'));
$twitter_link = htmlspecialchars($params->get('twitter_link'));
$linkedin_link = htmlspecialchars($params->get('linkedin_link'));

?>
<div class="social-images">
    <ul>
        <li><a href="<?php echo $facebook_link; ?>" title="follow us on facebook" target="_blank">
                <img src="modules/mod_AddThis/images/facebook.gif" alt="Follow us on Facebook" /></a></li>
        <li><a href="<?php echo $twitter_link; ?>" title="follow us on twitter" target="_blank">
                <img src="modules/mod_AddThis/images/twitter.gif" alt="Follow us on Twitter" /></a></li>
        <li><a href="<?php echo $linkedin_link; ?>" title="follow us on linkedin" target="_blank">
                <img src="modules/mod_AddThis/images/linked.gif" alt="Follow us on Linkedin" /></a></li>
        <li><a href="https://www.youtube.com/user/InformationMapping" title="follow us on youtube" target="_blank">
                <img src="modules/mod_AddThis/images/youtube.gif" alt="Follow us on Youtube" /></a></li>
        <li><a href="http://www.informationmapping.com/en/resources-en/blog/latest?format=feed&type=rss" title="follow us on RSS" target="_blank">
                <img src="modules/mod_AddThis/images/rss.gif" alt="Follow us on RSS" /></a></li>
    </ul>
</div>
    
   