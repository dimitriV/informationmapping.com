<?php
/*------------------------------------------------------------------------
# default.php - POPPER
# ------------------------------------------------------------------------
# author    Kent Elchuk
# copyright Copyright (C) 2012 bohemiawebsites.com. All Rights Reserved.
# @license - http://www.gnu.org/licenses/gpl-2.0.html GNU/GPL
# Websites: http://www.bohemiawebsites.com
# Technical Support:  Forum - http://www.bohemiawebsites.com/Forum
-------------------------------------------------------------------------*/
// no direct access

defined('_JEXEC') or die('Direct Access to this location is not allowed.');
?>

<style type="text/css">

#popper_content {
padding:<?= $params->get('modPadding'); ?>;
background:<?= $params->get('modBackground'); ?>;
width:<?= $params->get('modWidth'); ?>;
height:<?= $params->get('modHeight'); ?>;
z-index:<?= $params->get('mod-ZIndex'); ?>;
margin:0 auto;
margin-top:<?= ($params->get('topDistance')/2); ?>%;
text-align:<?= $params->get('modTextAlign'); ?>;
border:<?= $params->get('modBorderSize'); ?> solid <?= $params->get('moduleBorderColor'); ?>;
color:<?= $params->get('modtextcolor'); ?>;
filter:alpha(opacity=<?= $params->get('modopacityexplorer'); ?>);
}

#popper_close{
position:relative;
margin-top:-20px;
margin-left:<?= $params->get('modWidth');?>;
background:url(../tmpl/images/close-popper.gif);
}

#popper_close_img{
position: relative;
cursor:pointer;
cursor:hand;
background:url(../tmpl/images/close-popper.gif);
z-index:20000;
}

#popper {
position: absolute;
z-index:<?= $params->get('overlay-ZIndex'); ?>;
left: 0px;
top: 0px;
width:100%;
height: 1500px;
background:url(http://<?= $params->get('modulewebsiteaddress')?>/modules/mod_popper/tmpl/images/overlay-popper.gif);  
opacity:<?= $params->get('modopacitymodern'); ?>;
-moz-opacity:<?= $params->get('modopacityancientfirefox'); ?>;
-khtml-opacity:<?= $params->get('modopacityancientsafari'); ?>;
filter:alpha(opacity=<?= $params->get('modopacityexplorer'); ?>);
margin:0 auto;
}

/*div > #popper { position: fixed; }*/

</style>

<div id="popper">
<script>
function killOverlay() {
	//window.location = "http://www.google.com/"
	//function hideItem(divID) {
  refID = document.getElementById('popper');
	refID.style.display = "none";
	//}
}
function hidestuff(boxid){
   document.getElementById(boxid).style.visibility="hidden";
}
</script>

<div id="popper_content">

<div id="popper_close"><img src="http://<? echo $params->get('modulewebsiteaddress') ?>/modules/mod_popper/tmpl/images/close-popper.gif" onclick="hidestuff('popper')" /></div>

<?

$modules =& JModuleHelper::getModules($params->get('modPosition'));

for($i=0;$i<1;$i++){

   echo JModuleHelper::renderModule($modules[$i]);   
}

?>

</div>

</div>

<? if($params->get('modLifetime') != 'none'){ ?>

<script type="text/javascript">

setTimeout("killOverlay()", <?= $params->get('modLifetime'); ?>);

</script>

<? } ?>