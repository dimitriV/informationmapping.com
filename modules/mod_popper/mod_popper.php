<?php
/*------------------------------------------------------------------------
# mod_popper.php - POPPER
# ------------------------------------------------------------------------
# author    Kent Elchuk
# copyright Copyright (C) 2012 bohemiawebsites.com. All Rights Reserved.
# @license - http://www.gnu.org/licenses/gpl-2.0.html GNU/GPL
# Websites: http://www.bohemiawebsites.com
# Technical Support:  Forum - http://www.bohemiawebsites.com/Forum
-------------------------------------------------------------------------*/
// no direct access

defined('_JEXEC') or die('Direct Access to this location is not allowed.');


$useCookie 	= (int) $params->get( 'useCookie', 0 );

$cookieTime 	= (int) $params->get( 'cookieTime', 3600 );

if($useCookie != 1) {

	if(empty($_COOKIE['tp_show_loaded'])) {

		unset($_COOKIE['tp_show_loaded']);

	}

} else {

	if(empty($_COOKIE['tp_show_loaded'])) {

		setcookie("tp_show_loaded", "loaded", time()+3600);

	} else {

		return;

	}

}

// Include the syndicate functions only once

JHTML::_('behavior.mootools');

require(JModuleHelper::getLayoutPath('mod_popper'));


$document = & JFactory::getDocument();


$document->addScript(JURI::root().'modules/mod_popper/tmpl/js/popper.js');


?>