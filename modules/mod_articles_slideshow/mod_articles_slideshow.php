<?php
/**
 * @version		1.0
 * @package		Aricles slideshow
 * @subpackage	mod_articles_slideshow
 * @copyright	Information Mapping, Inc. All rights reserved.
 * @license		
 */

// no direct access
defined('_JEXEC') or die;

// Include the syndicate functions only once
require_once dirname(__FILE__).'/helper.php';

$list = &modArticlesSlideshowHelper::getList($params);
$moduleclass_sfx = htmlspecialchars($params->get('moduleclass_sfx'));

require JModuleHelper::getLayoutPath('mod_articles_slideshow', $params->get('layout', 'default'));
