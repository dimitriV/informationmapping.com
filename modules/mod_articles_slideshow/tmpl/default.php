<?php
/**
 * @version		1.0
 * @package		Joomla.Site
 * @subpackage	mod_articles_slideshow
 * @copyright	Information mapping. All rights reserved.
 * @license		
 */
// no direct access
defined('_JEXEC') or die;

// Base url
$baseurl = JURI::base();

$document = &JFactory::getDocument();
$app = JFactory::getApplication();

// Recent used template
$templateDir = JURI::base() . 'templates/' . $app->getTemplate();

// Adding javascript
$document->addScript($baseurl . '/modules/mod_articles_slideshow/js/jquery.flexslider.js');

// Adding css
$document->addStyleSheet($baseurl . 'modules/mod_articles_slideshow/css/flexslider.css');
?>
<!-- Hook up the FlexSlider -->
<script type="text/javascript">
    (function($){
        $(window).load(function() {
            $('.flexslider').flexslider({
                animation: "slide",             //Select your animation type (fade/slide)
                directionNav: false,            //Create navigation for previous/next navigation? (true/false)
                controlNav: true,               //Create navigation for paging control of each clide? (true/false)
                keyboardNav: false,             //Allow for keyboard navigation using left/right keys (true/false)
                touchSwipe: false,              //Touch swipe gestures for left/right slide navigation (true/false)
                prevText: "",                   //Set the text for the "previous" directionNav item
                nextText: "",                   //Set the text for the "next" directionNav item
                pausePlay: false,               //Create pause/play dynamic element (true/false)
                pauseOnAction: false,           //Pause the slideshow when interacting with control elements, highly recommended. (true/false)
                controlsContainer: "",          //Advanced property: Can declare which container the navigation elements should be appended too. Default container is the flexSlider element. Example use would be ".flexslider-container", "#container", etc. If the given element is not found, the default action will be taken.
                manualControls: ""              //Advanced property: Can declare custom control navigation. Example would be ".flex-control-nav" or "#tabs-nav", etc. The number of elements in your controlNav should match the number of slides/tabs (obviously).
            });
        });
        
    })(jQuery);
   
</script>

<div class="articleslideshow<?php echo $moduleclass_sfx ?>">
    <div class="flexslider">
        <ul class="slides">
            <?php foreach ($list as $article): ?>          
                <li>
                    <div class="article-desc">
                        <h3><?php echo strip_tags($article->title); ?></h3>
                        <?php echo $article->introtext; ?>
                    </div>
                    <div class="article-img">
                        <img src="<?php echo $baseurl . 'images/articleimages/large/' . $article->small_image; ?>" alt="<?php echo $article->image_title; ?>" />    
                    </div>
                </li>
            <?php endforeach; ?>
        </ul>
    </div>
</div>