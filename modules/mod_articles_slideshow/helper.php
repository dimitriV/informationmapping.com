<?php
/**
 * @version		$Id: helper.php 20541 2011-02-03 21:12:06Z dextercowley $
 * @package		Joomla.Site
 * @subpackage	mod_banners
 * @copyright	Copyright (C) 2005 - 2011 Open Source Matters, Inc. All rights reserved.
 * @license		GNU General Public License version 2 or later; see LICENSE.txt
 */

// no direct access
defined('_JEXEC') or die;


class modArticlesSlideshowHelper
{
	static function getList(&$params)
	{        
        // Number of article to slideshow
        $article_slideshow_count	= trim($params->get('articles_slideshow_count'));
        
        // Category of article to slideshow        
         $articles_slideshow_category	= strtolower(trim($params->get('articles_slideshow_category')));        
         
		$db		= JFactory::getDbo();

        $query = " SELECT c.id, c.title, c.introtext, ai.image_title, ai.small_image, ai.large_image"              
                ." FROM #__content c"
                ." JOIN #__article_images ai"
                ." ON c.id = ai.article_id" 
                ." WHERE c.catid=(SELECT id FROM #__categories WHERE alias='".$articles_slideshow_category."' )"
                ." ORDER BY id DESC ";
         if(!empty ($article_slideshow_count))       
                $query .= " LIMIT " . $article_slideshow_count;            

         $db->setQuery( $query );     
         $result = $db->loadObjectList();
         return (array) $result;
        /**
        $query	= $db->getQuery(true);
		$query->select('a.id, a.name, a.username, a.registerDate');
		$query->order('a.registerDate DESC');
		$query->from('#__users AS a');
        
        if(!empty($article_slideshow_count))
            $db->setQuery($query,0,$params->get('shownumber'));
        else
            $db->setQuery($query);
         */	
	}
}
