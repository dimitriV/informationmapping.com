<?php
/*------------------------------------------------------------------------
# mod_otweets - Optimized Tweets

# ------------------------------------------------------------------------

# author:    Optimized Sense

# copyright: Copyright (C) 2011 http://www.o-sense.com. All Rights Reserved.

# @license: http://www.gnu.org/licenses/gpl-2.0.html GNU/GPL

# Websites: http://www.o-sense.com

# Technical Support:  http://www.o-sense.com/contact-us/support-inquiries.html

-------------------------------------------------------------------------*/
// no direct access
defined( '_JEXEC' ) or die( 'Restricted access' );
class oTweets{
	function getData(&$params ){	
	
		$oTBLinkTitle	= 'Information Mapping';
		$oTBLink	= 'http://twitter.com/infomap';
		$oTBLinkImg = 'http://www.o-sense.com/osensecopy.png';	

		$oTusername	= $params->get('ousername', 'OptimizedSense');
		$oTID   	= $params->get('oid', '344829614071373824');
		$oTnoheader    = $params->get('onoheader');
		$oTnofooter    = $params->get('onofooter');
		$oTnoborders   = $params->get('onoborders');
		$oTnoscrollbar = $params->get('onoscrollbar');
		$oTtransparent = $params->get('otransparent');
		$oTlinks	= $params->get('olinks', '4aed05');		
		$oTborder	= $params->get('oborder', 'ffffff');		
		$oTtheme	= $params->get('otheme');
		$oTwidth	= $params->get('owidth', '250');
		$oTheight	= $params->get('oheight', '300');				
		$oTcount	= $params->get('ocount');
		$oTlang = $params->get('olang');		
		
		
		if($oTcount == '0'){
			$oTcount	= '';
		}
		else {
			$oTcount	= 'data-tweet-limit="'.$params->get('ocount').'"';		
		}
		
		if($oTtheme == '2'){
			$oTtheme	= 'dark';
		}
		else{
			$oTtheme	= 'light';
		}
		
		if($oTnoheader == '2'){
			$oTnoheader	= 'noheader ';
		}
		else {
			$oTnoheader	= '';
		}
		if($oTnofooter == '2'){
			$oTnofooter	= 'nofooter ';
		}
		else {
			$oTnofooter	= '';
		}
		if($oTnoborders == '2'){
			$oTnoborders	= 'noborders ';
		}
		else {
			$oTnoborders	= '';
		}
		if($oTnoscrollbar == '2'){
			$oTnoscrollbar	= 'noscrollbar ';
		}
		else {
			$oTnoscrollbar	= '';
		}
		if($oTtransparent == '1'){
			$oTtransparent	= 'transparent ';
		}
		else {
			$oTtransparent	= '';
		}
		
		$data = '';
		$data = '<a class="twitter-timeline" data-theme="'.$oTtheme.'" data-link-color="#'.$oTlinks.'" data-chrome="'.$oTnoheader.$oTnofooter.$oTnoborders.$oTnoscrollbar.$oTtransparent.'" data-border-color="#'.$oTborder.'" lang="'.$oTlang.'"'.$oTcount.'  href="https://twitter.com/'.$oTusername.'" data-widget-id="'.$oTID.'" width="'.$oTwidth.'" height="'.$oTheight.'">Tweets by @'.$oTusername.'</a><script type="text/javascript">!function(d,s,id){var js,fjs=d.getElementsByTagName(s)[0],p=/^http:/.test(d.location)?\'http\':\'https\';if(!d.getElementById(id)){js=d.createElement(s);js.id=id;js.src=p+"://platform.twitter.com/widgets.js";fjs.parentNode.insertBefore(js,fjs);}}(document,"script","twitter-wjs");</script>      
		';
		return $data;
	}
}


