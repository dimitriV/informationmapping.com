<?php
/**
* @package Testimonial Fader
* @copyright Copyright (C) 2011 Infyways Solutuons. All rights reserved.
* @license http://www.gnu.org/copyleft/gpl.html GNU/GPL, see LICENSE.php
*/
// no direct access
defined( '_JEXEC' ) or die( 'Restricted access' );
$load1 = $params->get('load1');
$testimonials = $params->get('testimonials');
$author = $params->get('author');
$tfont = $params->get('tfont');
$afont = $params->get('afont');
$tsize = $params->get('tsize');
$asize = $params->get('asize');
$title = $params->get('title');
$tcolor = $params->get('tcolor');
$acolor = $params->get('acolor');
$speed = $params->get('speed');
$timeout = $params->get('timeout');
$width = $params->get('width');
$height= $params->get('height');
$animation = $params->get('animation');
$quote = $params->get('quote');
$tweight = $params->get('tweight');
$aweight = $params->get('aweight');
$background = $params->get('background');
$user_defined = $params->get('user_defined');
$jsfiles = $params->get('jsfiles');
require(JModuleHelper::getLayoutPath('mod_testimonial_fader','default'));
?>