<?php // no direct access
/**
* @package Testimonial Fader
* @copyright Copyright (C) 2011 Infyways Solutuons. All rights reserved.
* @license http://www.gnu.org/copyleft/gpl.html GNU/GPL, see LICENSE.php
*/
defined('_JEXEC') or die('Restricted access');
$document = &JFactory::getDocument();
$base_url=JURI::root();
$document->addStyleSheet('modules/mod_testimonial_fader/tmpl/css/jq_fade.css');
if($jsfiles==1)
{
	if($load1==1)
	{
	$document->addScript("modules/mod_testimonial_fader/tmpl/js/jquery.js");
	} 
	$document->addScript("modules/mod_testimonial_fader/tmpl/js/jquery.innerfade.js");
	$document->addCustomTag("<script type='text/javascript'> var jax = jQuery.noConflict();
	jax(document).ready( function()	{jax('#testimonial').innerfade({ animationtype: '$animation', speed: $speed, timeout: $timeout, type: 'random',containerheight:'$height' });} ); 
</script>");
}
else
{
	if($load1==1)
	{
	echo '<script src="'.$base_url.'modules/mod_testimonial_fader/tmpl/js/jquery.js" type="text/javascript"></script>';
	}
	echo '<script src="'.$base_url.'modules/mod_testimonial_fader/tmpl/js/jquery.innerfade.js" type="text/javascript"></script><script type="text/javascript"> var jax = jQuery.noConflict();
	jax(document).ready( function()	{jax(\'#testimonial\').innerfade({ animationtype: \''.$animation.'\', speed: '.$speed.', timeout: '.$timeout.', type: \'random\',containerheight:\''.$height.'\' });} ); 
</script>';
}
?>
<?php if($tfont!='user'){
?>
<link href='http://fonts.googleapis.com/css?family=<?php echo $tfont;?>' rel='stylesheet' type='text/css'>
<?php }?>


<style>
#fader{height:<?php echo $height;?>!important; width:<?php echo $width;?>!important; margin-top: 0;padding: 0 65px 10px 0!important;}
blockquote {
background: url("<?php echo $base_url;?>modules/mod_testimonial_fader/tmpl/images/left-<?php echo $quote;?>.png") no-repeat scroll left top transparent!important;line-height: 150%!important; margin: 15px 0!important; padding: 7px 0 0 40px !important;}
#testimonial p{
font-family:<?php if($tfont!='user'){
echo "'".$tfont."', sans-serif " ;} else {echo $user_defined;}?>;
font-size:<?php echo $tsize;?>px;
font-weight:<?php echo $tweight;?>; 
color:<?php echo $tcolor;?>; 
margin-left:5px !important; 
margin:0 !important; 
padding:10 !important;
}
#testimonial span{background: url("<?php echo $base_url;?>modules/mod_testimonial_fader/tmpl/images/right-<?php echo $quote;?>.png") no-repeat scroll right bottom transparent; font-size:<?php echo $asize;?>px;color:<?php echo $acolor;?>;font-weight:<?php echo $aweight;?>;float:right; padding:10px 50px 10px !important;}
</style>


<div id="fader">
<blockquote>
	<ul id="testimonial">
	<?php 
	$testimonial1=explode('/',$testimonials);
	$author1=explode('/',$author);
	for($i=0;$i<count($testimonial1);$i++)
	{
	?>		<li><p><?php echo $testimonial1[$i];?><br><span><?php echo $author1[$i];?></span></p></li>				
	<?php }?>
	</ul>
</blockquote>
</div>