sQuery(document).ready( function() {
	// add flag to first item
	sQuery('#eblog-module-archive li:first a.archive-title').addClass('active-year');
	
	// show 1st item container
	sQuery('#eblog-module-archive li:first .year-container').show();

	sQuery('#eblog-module-archive a.archive-title').bind('click', function() {

		var str = sQuery(this).attr('id');
		var str = str.split('_');
		var year = str[3];
		
		if ( !sQuery(this).hasClass('active-year') )
		{
			// Remove any active class
			sQuery( '#eblog-module-archive a.active-year' ).removeClass( 'active-year' );
			
			// Add class to the existing item
			sQuery( this ).addClass( 'active-year' );
			
			// Slide up active year element.
			sQuery( '#eblog-module-archive .year-container' ).slideUp( 'fast' );
			sQuery( this ).siblings( 'div.year-container').slideDown( 'fast' );
		}		
		return false;
	});
	
});