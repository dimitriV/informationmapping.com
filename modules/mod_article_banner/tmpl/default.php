<?php
/**
 * @version		$Id: mod_article_banner.php 2011-12-09 $
 * @package		Joomla.Site
 * @subpackage	mod_article_banner
 * @copyright	Copyright (C) 2011 IT Offshore Nepal. All rights reserved.

 */

defined('_JEXEC') or die;

$moduleclass_sfx = $params->get('moduleclass_sfx');
$banner_image = $params->get('banner_image');
$btn1_text = $params->get('btn1_text');
$btn1_link = $params->get('btn1_link');
$btn2_text = $params->get('btn2_text');
$btn2_link = $params->get('btn2_link');

$banner_style = ($banner_image && $banner_image != -1) ? ' style="background: url(./images/articlebanners/'.$banner_image.') 50% 50% no-repeat;"' : '';

?>

<div class="article-banner"<?php echo $banner_style; ?>>
	<h3><?php echo $article->title; ?></h3>
	<div class="content"><?php echo $article->introtext; ?>   
    </div>
	<?php if(($btn1_text && $btn1_link) || ($btn2_text && $btn2_link)): ?>
		<div class="banner-buttons">
			<?php if($btn1_text && $btn1_link): ?>
				<div class="banner-grey-button">
				<span class="left"></span><span class="btnlink"><a href="<?php echo $btn1_link; ?>"><?php echo $btn1_text; ?></a></span><span class="right"></span>
				</div>
			<?php endif; ?>
			<?php if($btn2_text && $btn2_link): ?>
				<div class="banner-blue-button">
					<span class="left"></span><span class="btnlink"><a href="<?php echo $btn2_link; ?>"><?php echo $btn2_text; ?></a></span><span class="right"></span>
				</div>
			<?php endif; ?>
		</div>
	<?php endif; ?>
</div>
