<?php
/**
 * @version		$Id: mod_article_banner.php 2011-12-09 $
 * @package		Joomla.Site
 * @subpackage	mod_article_banner
 * @copyright	Copyright (C) 2011 IT Offshore Nepal. All rights reserved.
 */

// no direct access
defined('_JEXEC') or die;

// Include the weblinks functions only once
require_once dirname(__FILE__).'/helper.php';

if(JRequest::getVar('option') == 'com_magebridge' && JRequest::getVar('request')){
	return;
}

$article = modArticleBannerHelper::getArticle($params);

if ($article == '') {
	return;
}

$moduleclass_sfx = htmlspecialchars($params->get('moduleclass_sfx'));

require JModuleHelper::getLayoutPath('mod_article_banner',$params->get('layout', 'default'));
