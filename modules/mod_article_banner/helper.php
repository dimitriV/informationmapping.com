<?php
/**
 * @version		$Id: mod_article_banner.php 2011-12-09 $
 * @package		Joomla.Site
 * @subpackage	mod_article_banner
 * @copyright	Copyright (C) 2011 IT Offshore Nepal. All rights reserved.

 */

// no direct access
defined('_JEXEC') or die;

class modArticleBannerHelper
{
	static function getArticle($params)
	{
		$article_id = $params->get('article_id');
		if($article_id){
			$db = &JFactory::getDbo();
			$sql = "SELECT * FROM #__content WHERE id = ".$article_id." AND state = 1";
			$db->setQuery($sql);
			return $db->loadObject();
		}
		
		return '';

	}
}
