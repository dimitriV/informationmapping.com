<?php
/**
/**
*Author: Infyways Solutions
*Mail: info@infyways.com
*URL:www.infyways.com
* @package Client Logo Fader
* @copyright Copyright (C) 2011 Infyways Solutuons. All rights reserved.
* @license http://www.gnu.org/copyleft/gpl.html GNU/GPL, see LICENSE.php
*/
// no direct access
defined( '_JEXEC' ) or die( 'Restricted access' );
$load1 = $params->get('load1');
$images = $params->get('images');
$link = $params->get('link');
$speed = $params->get('speed');
$timeout = $params->get('timeout');
$animation = $params->get('animation');
$target = $params->get('target');
$type = $params->get('type');
$height = $params->get('height');
$image_lg_height = $params->get('image_lg_height');
$image_lg_width = $params->get('image_lg_width');
$show_by = $params->get('show_by');
$crop = $params->get('crop');

require(JModuleHelper::getLayoutPath('mod_logo_fader','default'));
?>