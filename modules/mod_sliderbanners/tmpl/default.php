<?php
/**
 * @version		1.0
 * @package		Joomla.Site
 * @subpackage	mod_sliderbanners
 * @copyright	Information mapping. All rights reserved.
 * @license		
 */
// no direct access
defined('_JEXEC') or die;

// Base url
$baseurl = JURI::base();

$document = &JFactory::getDocument();
$app = JFactory::getApplication();

// Recent used template
$templateDir = JURI::base() . 'templates/' . $app->getTemplate();
// Adding javascript
$document->addScript($baseurl . '/modules/mod_sliderbanners/js/jquery.flexslider.js');
// Adding css
$document->addStyleSheet($baseurl . 'modules/mod_sliderbanners/css/flexslider.css');

$interval = (int)$params->get('sliderbanners_interval', 5000);
?>
<!-- Hook up the FlexSlider -->
<script type="text/javascript">
    (function($){
        $(window).load(function() {
            $('.flexslider').flexslider({
                animation: "fade",             //Select your animation type (fade/slide)
                //slideshowSpeed: <?php echo $interval; ?>,
                directionNav: false,            //Create navigation for previous/next navigation? (true/false)
                controlNav: true,               //Create navigation for paging control of each clide? (true/false)
                keyboardNav: false,             //Allow for keyboard navigation using left/right keys (true/false)
                touchSwipe: false,              //Touch swipe gestures for left/right slide navigation (true/false)
                prevText: "",                   //Set the text for the "previous" directionNav item
                nextText: "",                   //Set the text for the "next" directionNav item
                pausePlay: false,               //Create pause/play dynamic element (true/false)
                pauseOnAction: false,           //Pause the slideshow when interacting with control elements, highly recommended. (true/false)
                controlsContainer: "",          //Advanced property: Can declare which container the navigation elements should be appended too. Default container is the flexSlider element. Example use would be ".flexslider-container", "#container", etc. If the given element is not found, the default action will be taken.
                manualControls: ""              //Advanced property: Can declare custom control navigation. Example would be ".flex-control-nav" or "#tabs-nav", etc. The number of elements in your controlNav should match the number of slides/tabs (obviously).
            });
        });
        
    })(jQuery);
   
</script>

<div class="articleslideshow<?php echo $moduleclass_sfx ?>">
    <div class="flexslider">
        <ul class="slides">
            <?php foreach ($list as $data): ?>          
            <li>
                <div class="slider-info-wrapper">

                    <div class="slider-info">
                        <h3><?php echo strip_tags($data->title); ?></h3>
                            <?php echo $data->description; ?>
                    </div>
                    <?php if(($data->button_label1 && $data->button_link1) || ($data->button_label2 && $data->button_link2)): ?>
	                    <div class="slider-button-wrapper">
	                    	<?php if($data->button_label1 && $data->button_link1): ?>
                                <?php if($data->target_link1 == 0): ?>
                                <?php $target_link1 = '_blank' ?>
                                <?php else: ?>
                                 <?php $target_link1 = '_parent' ?>
                                <?php endif; ?>
                                
	                        	<div class="grey-button"><a href="<?php echo $data->button_link1;?>" target="<?php echo $target_link1;?>"><span><?php echo $data->button_label1;?></span></a></div>
	                        <?php endif; ?>
                                    
	                        <?php if($data->button_label2 && $data->button_link2): ?>
                                        <?php if($data->target_link2 == 0): ?>
                                <?php $target_link2 = '_blank' ?>
                                <?php else: ?>
                                 <?php $target_link2 = '_parent' ?>
                                <?php endif; ?>
	                        	<div class="blue-button"><a href="<?php echo $data->button_link2;?>" target="<?php echo $target_link2;?>"><span><?php echo $data->button_label2;?></span></a></div>
	                        <?php endif; ?>
	                    </div>
                    <?php endif; ?>
                </div>
                <?php if($data->filename): ?>
	                <div class="article-img">
	                    <div class="slider-image">
	                    	<table width="487" border="0" cellspacing="0" cellpadding="0">
							  <tr>
							    <td align="center" valign="middle">
							    	<?php if($data->file_link): ?>
										<a href="<?php echo $data->file_link;?>" target="_blank"><img src="<?php echo $baseurl . 'images/sliderbanners/' . $data->filename; ?>" alt="<?php echo $data->title; ?>" /></a>
									<?php else: ?>
										<img src="<?php echo $baseurl . 'images/sliderbanners/' . $data->filename; ?>" alt="<?php echo $data->title; ?>" />
									<?php endif; ?>
								</td>
							  </tr>
							</table>
	                    </div>    
	                </div>
                <?php endif; ?>
            </li>
            <?php endforeach; ?>
        </ul>
    </div>
</div>