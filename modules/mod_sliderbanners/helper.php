<?php
/**
 * @version		$Id: helper.php 20541 2011-02-03 21:12:06Z dextercowley $
 * @package		Joomla.Site
 * @subpackage	mod_sliderbbanners
 * @copyright	Copyright (C) 2005 - 2011 Open Source Matters, Inc. All rights reserved.
 * @license		GNU General Public License version 2 or later; see LICENSE.txt
 */

// no direct access
defined('_JEXEC') or die;


class modSliderbannersHelper
{
	static function getList(&$params)
	{        
        // Number of article to slideshow
        $sliderbanners_count	= trim($params->get('sliderbanners_count'));
        
        // Category of article to slideshow        
         $sliderbanners_category	= strtolower(trim($params->get('sliderbanners_category')));        
         
		$db		= JFactory::getDbo();

        $query = " SELECT id, title, description,filename,button_label1,button_label2,target_link1,button_link1,button_link2,target_link2,file_link,catid,state,ordering"              
                ." FROM #__sliderbanners"
                ." WHERE state=1 ";
        if(!empty($sliderbanners_category)) 
        {       
              $query .=" AND catid=".$sliderbanners_category;
        }        
              $query .= " ORDER BY ordering DESC ";
         if(!empty ($sliderbanners_count))       
                $query .= " LIMIT " . $sliderbanners_count;            
           // echo $query;
         $db->setQuery( $query );     
         $result = $db->loadObjectList();
         return (array) $result;
        /**
        $query	= $db->getQuery(true);
		$query->select('a.id, a.name, a.username, a.registerDate');
		$query->order('a.registerDate DESC');
		$query->from('#__users AS a');
        
        if(!empty($article_slideshow_count))
            $db->setQuery($query,0,$params->get('shownumber'));
        else
            $db->setQuery($query);
         */	
	}
}
