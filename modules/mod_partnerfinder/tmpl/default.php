<?php
/**
 * @version     $Id: default.php  2010-10-23 04:13:25Z $
 * @package     IM 
 * @subpackage  mod_partnerfinder
 * @author      ITOffshore Nepal
 * @copyright   Copyright (C) 2011 ITOffshore Nepal.
 * @license     ITOffshore Nepal
 */

defined('_JEXEC') or die;
?>


<script type="text/javascript">
	function countryname(countryid){
		countryidvalue =countryid.value;
		window.location = countryidvalue;
		exit;
	}
</script>

<div class="partnerfinder<?php echo $moduleclass_sfx; ?>">
	<div class="partner-wrapper">
		<div class="pre-text"><?php echo $pretext; ?></div>
		<div class="select-country">
			<form action="" method="get" name="adminForm" id="adminForm">
				<select name="countryid" id="countryid" class="inputbox styled" onchange="countryname(this)">
					<option value=""><?php echo JText::_('MOD_PARTNERFINDER_SELECT_YOUR_COUNTRY');?></option>
					<?php echo JHtml::_('select.options', JFormFieldCountryName::getOption(), 'value', 'text' );?>
				</select>
			</form>
		</div>
	</div>
</div>
