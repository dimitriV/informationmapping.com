<?php
/**
 * @version     $Id: helper.php  2010-10-23 04:13:25Z $
 * @package     IM 
 * @subpackage  mod_partnerfinder
 * @author      ITOffshore Nepal
 * @copyright   Copyright (C) 2011 ITOffshore Nepal.
 * @license     ITOffshore Nepal
 */

// no direct access
defined('_JEXEC') or die;

class modPartnerFinderHelper
{
	 function getMenuId(){
		$lang = & JFactory::getLanguage();
        $langTag = $lang->getTag();
        $db = & JFactory::getDBO();
        $query = "SELECT id FROM #__menu WHERE link='index.php?option=com_courses&view=partner' AND published=1 AND language IN('".$langTag."', '*')";
        $db->setQuery($query);
        $menuid = $db->loadObject();
        return $menuid;
	}
}
?>