<?php
/**
 * @version     $Id: mod_partnerfinder.php  2010-10-23 04:13:25Z $
 * @package     IM 
 * @subpackage  mod_partnerfinder
 * @author      ITOffshore Nepal
 * @copyright   Copyright (C) 2011 ITOffshore Nepal.
 * @license     ITOffshore Nepal
 */

// no direct access
defined('_JEXEC') or die;

// Include the weblinks functions only once
require_once dirname(__FILE__).'/helper.php';
require_once dirname(__FILE__).'/countryname.php';

$menuid = modPartnerFinderHelper::getMenuid();

if (!count($menuid)) {
	return;
}

$moduleclass_sfx = htmlspecialchars($params->get('moduleclass_sfx'));
$pretext = $params->get('pretext');

require JModuleHelper::getLayoutPath('mod_partnerfinder',$params->get('layout', 'default'));

?>