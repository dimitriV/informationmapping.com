<?php

/**
 * @version     $Id: countryname.php  2010-10-23 04:13:25Z $
 * @package     IM 
 * @subpackage  mod_partnerfinder
 * @author      ITOffshore Nepal
 * @copyright   Copyright (C) 2011 ITOffshore Nepal.
 * @license     ITOffshore Nepal
 */
defined('JPATH_BASE') or die;

jimport('joomla.html.html');
jimport('joomla.form.formfield');
jimport('joomla.form.helper');
JFormHelper::loadFieldClass('list');

/**
 * Form Field class for the Joomla Framework.
 *
 * @package		Joomla.Administrator
 * @subpackage	com_categories
 * @since		1.6
 */
class JFormFieldCountryName extends JFormFieldList
{

    /**
     * The form field type.
     *
     * @var		string
     * @since	1.6
     */
    protected $type = 'CountryName';

    /**
     * Method to get the field options.
     *
     * @return	array	The field option objects.
     * @since	1.6
     */
    public function getOption()
    {
        // Initialize variables.
        $option = array();

        $db = JFactory::getDbo();
        $lang = & JFactory::getLanguage();
        $langTag = $lang->getTag();
        $itemid = JFormFieldCountryName::getMenuId();
        $query = $db->getQuery(true);

        $query->select('a.id AS value, a.country AS text');
        $query->from('#__course_countries AS a');
        $query->where('a.state = 1 ');
        $query->where('language in (' . '"' . $langTag . '"' . ',' . '"*"' . ')');
        $query->order('a.country');

        // Get the options.
        $db->setQuery($query);
        $option = $db->loadObjectList();
        foreach ($option as $opt) {

            $opt->value = JRoute::_('index.php?option=com_courses&view=partner&id=' . $opt->value . '&Itemid=' . $itemid->id, false);
        }
        // Check for a database error.
        if ($db->getErrorNum()) {
            JError::raiseWarning(500, $db->getErrorMsg());
        }

        return $option;
    }

    function getMenuId()
    {
        $lang = & JFactory::getLanguage();
        $langTag = $lang->getTag();
        $db = & JFactory::getDBO();
        $query = "SELECT id FROM #__menu WHERE link='index.php?option=com_courses&view=partner' AND published=1 AND language IN('".$langTag."', '*')";
        $db->setQuery($query);
        $menuid = $db->loadObject();
        return $menuid;
    }

}

?>