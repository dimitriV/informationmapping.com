<?php
// no direct access
defined('_JEXEC') or die('Restricted access');

$login_to = $params->get('login_to');
$login_text = $params->get('login_text');
$login_link = base64_encode($params->get('login_url'));

$logout_text = $params->get('logout_text');
$logout_link = base64_encode($params->get('logout_url'));
$logout_pretext = $params->get('logout_pretext');

$user = JFactory::getUser();

require(JModuleHelper::getLayoutPath('mod_logoutlink'));