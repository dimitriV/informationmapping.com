<?php
/**
* @version   $Id$
* @package   Jumi
* @copyright (C) 2011 - 2011 MadeByChad.com, 2011 Chad Wagner
* @license   GNU/GPL v3 http://www.gnu.org/licenses/gpl.html
*/

defined('_JEXEC') or die('Restricted access');

if($user->guest){
	$return = ($login_link != '') ? '&return='.$login_link : '';
	$url = ($login_to == 'magento') ? MageBridgeUrlHelper::route('customer/account/login') : JRoute::_('index.php?option=com_users&view=login'.$return);
	echo '<div class="moduletable-j-loginout">
			<div id="login-form-toplink"><a href="'.$url.'" class="login-button">'.$login_text.'</a>
			</div>
		</div>';
}
else{
	//$pre_text = ($logout_pretext != '') ? sprintf($logout_pretext, $user->name) : '';
	//echo '<span>'.$pre_text.'</span>';
    $host = $_SERVER['REQUEST_URI'];
    //echo $host;
    if(strpos($host, 'customer/account') || strpos($host, 'subscriptions') || strpos($host, 'savedcards') )
    {
        ?>
        <form action="<?php echo JRoute::_('index.php', true); ?>" method="post" id="login-form-toplink" style="display:inline;" class="logout-button">
            <input type="submit" name="Submit" class="button" value="<?php echo $logout_text ?>" />
            <input type="hidden" name="option" value="com_users" />
            <input type="hidden" name="task" value="user.logout" />
            <input type="hidden" name="return" value="<?php echo $logout_link; ?>" />
            <?php echo JHtml::_('form.token'); ?>
        </form>
        <?php
    }
    else
    {
        echo '<div class="my-account"><a href="/en/shop/customer/account/" title="My Account">My Account</a></div>';
    }


}
?>