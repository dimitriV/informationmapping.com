<?php
/**
* @package   JE Ajax Upcoming Events
* @copyright Copyright (C) 2011 IT Offshore Nepal. All rights reserved.
**/

defined('_JEXEC') or die('Restricted access');

class mod_jeajax_upcoming_eventsHelper{
	
	function getevents($params){
		$db			= & JFactory::getDBO();
		$result		= null;
		$cat_ids	= trim($params->get('cat_ids', 0));
		$limit		= trim($params->get('limit', '5'));
		$order_by 	= $params->get('order', 'ordering');
		$order_dir 	= $params->get('direction', 'asc');
		
		$where = ($cat_ids > 0) ? ' AND category IN ('.$cat_ids.')' : '';
		
		if($order_by == 'ordering'){
			$order = ' ORDER BY e.ordering, c.ordering '.$order_dir;
		}
		else{
			$order = ' ORDER BY e.start_date, c.ordering ASC';
		}
		
		$query = "SELECT e.id, e.title, e.ordering, e.start_date, e.end_date, e.city, e.country,
					DATE_FORMAT(e.start_date, '%M') event_month, c.ename, ec.country_name
					FROM #__jeajx_event e
					LEFT JOIN #__jeajx_cal_category c ON e.category = c.id
					LEFT JOIN #__jeajx_country ec ON e.country = ec.country_id
					WHERE e.start_date > NOW() AND e.published = 1 AND c.published = 1".$where.$order;

		$db->setQuery($query, 0, $limit);
		$result = $db->loadObjectlist();
		
		if ($db->getErrorNum()) {
			JError::raiseWarning( 500, $db->stderr() );
		}
		
		return $result;
	}
}

