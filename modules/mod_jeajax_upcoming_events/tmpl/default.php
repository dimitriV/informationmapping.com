<?php 
/**
* @package   JE Ajax Upcoming Events
* @copyright Copyright (C) 2011 IT Offshore Nepal. All rights reserved.
**/
defined('_JEXEC') or die('Restricted access'); 

$moduleclass_sfx = $params->get('moduleclass_sfx','');
$details_text = $params->get('details_text', 'See details');
$more_events_text = $params->get('more_events_text', 'More events');
$menuitem	= $params->get('menuitem', 0);
$order_by 	= $params->get('order', 'ordering');
$order_dir 	= $params->get('direction', 'asc');

if($order_by == 'start_date' && $order_dir == 'desc'){
	$events = array_reverse($events);
}
?>

<div class="events-wrapper<?php echo $moduleclass_sfx; ?>">
<?php if(count($events)){ ?>
	<div class="month-block">

<?php	
	$event_month = '';
	$i = 0;
	foreach($events as $event){
		$start_date = explode('-', $event->start_date);
		$end_date = explode('-', $event->end_date);
		$location = array();
		if($event->city != ''){
			$location[] = $event->city;
		}
		if($event->country_name != ''){
			$location[] = $event->country_name;
		}
		$location = implode(', ', $location);
		$location = ($location != '') ? '('.$location.')' : '';
	
		if($event_month != $event->event_month){
			$event_month = $event->event_month;
			if($i > 0){
			?>
				</div><div class="month-block">
			<?php
			}
			?>
			<h2><?php echo JText::_(strtoupper($event_month)); ?></h2>




<?php
// show evergreen webinar
if($start_date[2] == $end_date[2]) {
echo "<div class=\"event-block\">";
echo "<a href=\"http://www.informationmapping-webinars.com/webinar1/webinar-register.php?trackingID1=home&trackingID2=all_events&landingpage=default&expiration=default\" target=\"_blank\">";
echo "<strong>Every Tuesday and Thursday</strong></a><br />";
echo "<span>Information Mapping: What is it? Why use it?</span>";
echo "</div>";
}
?>


			<?php
		}
		$i++;
	?>


		<div class="event-block">
			
			<a href="<?php echo JRoute::_( 'index.php?option=com_jeajaxeventcalendar&view=alleventlist_more&event_id='.$event->id.'&Itemid='.$menuitem ); ?>">
			<strong><?php 
			if($start_date[2] == $end_date[2]) {
			echo $event_month . " " . $start_date[2].' '.$location;	
			} else
			{
			echo $event_month . " " . $start_date[2].'-'.$end_date[2].' '.$location; 
			}
			?></strong></a><br />
			<span><?php echo $event->title; ?></span><br />
            
			<!--<a href="<?php echo JRoute::_( 'index.php?option=com_jeajaxeventcalendar&view=alleventlist_more&event_id='.$event->id.'&Itemid='.$menuitem ); ?>"><?php echo $details_text; ?></a>-->
		</div>
	<?php } ?>

	</div>



	<a class="read-more" href="<?php echo JRoute::_( 'index.php?Itemid='.$menuitem ); ?>"><?php echo $more_events_text; ?></a>


	<?php } ?>
</div>
