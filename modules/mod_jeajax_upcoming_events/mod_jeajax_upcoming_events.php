<?php 
/**
* @package   JE Ajax Upcoming Events
* @copyright Copyright (C) 2011 IT Offshore Nepal. All rights reserved.
**/

defined('_JEXEC') or die('Restricted access');

require_once (dirname(__FILE__).'/helper.php');

$layout = $params->get('layout','default');
$layout = JFilterInput::clean($layout, 'word');
$events	= mod_jeajax_upcoming_eventsHelper::getevents($params);
$path = JModuleHelper::getLayoutPath('mod_jeajax_upcoming_events', $layout);
if (file_exists($path)) {
	require($path);
}
