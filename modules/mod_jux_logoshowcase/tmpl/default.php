<?php
/**
 * @package 	JUX Logo Showcase for Joomla!
 * @subpackage 	JUX Logo Showcase Module for Joomla!
 * @version 	$Id$
 * @author 		JoomlaUX Admin
 * @copyright	Copyright(C) 2014 - JoomlaUX Solutions. All rights reserved.
 * @license 	http://www.gnu.org/licenses/gpl-3.0.html GNU/GPL version 3
**/
// no direct access
defined('_JEXEC') or die('Restricted access');
$slidePath = $params->get('folder.folder','');
$slides = array();
$slide = array();
if (($images = $params->get('folder.images','')))
{
	$slides = json_decode($images);
}
// Ordering Slider
foreach ($slides as $position)
{
	$slide[$position->position] = $position;
}
ksort($slide);
// Process width, height
$img_width.="px";
$img_height.="px";
?>

<!-- Grid function - isotope -->
<script type="text/javascript">
	jQuery(window).load(function(){
		jQuery('.isotope<?php echo $module->id ?>').isotope({
			itemSelector: '.jux_logo_item'
		});
	});
</script>

<script type="text/javascript">
	jQuery(window).load(function() {
		var owl = jQuery(".carousel<?php echo $module->id ?>");
		owl.owlCarousel({
			items :  <?php echo $params->get('juxslider_numitem');?>,
			lazyLoad : true,
			autoPlay : <?php if ($params->get('juxslider_autoplay') == 1) {echo $params->get('juxslider_auto_value');} else {echo 'false';}?>,
		    stopOnHover : <?php if ($params->get('juxslider_stoponhover') == 1) {echo 'true';} else {echo 'false';}?>,
		    paginationSpeed : <?php echo $params->get('juxslider_paginationSpeed'); ?>,
		    goToFirstSpeed : 3000,
		    transitionStyle:"fade",
		    pagination : <?php if ($params->get('juxslider_pagination') == 1) {echo 'true';} else {echo 'false';}?>
		});
	});
</script>

<div id="jux_logo<?php echo $module->id ?>" class="jux_logo_showcase">
	<!-- Gird display -->
	<?php if($display == 'grid') : ?>
		<div class="isotope<?php echo $module->id ?> logo_showcase_grid">
			<?php foreach($slide as $i => $item):
			$imageSrc = JURI::base(true) . '/' . $slidePath . '/' . $item->image;
			$title = $item->title;
			$url = $item->url;
			$target = $item->urlTarget;

			?>
			<div class="jux_logo_item">
				<a class="logo_item_link"href="<?php echo $url; ?>" target="<?php echo $target; ?>" >
					<img class="logo_item_image" src="<?php echo $imageSrc; ?>" style="width: <?php echo $img_width; ?>; height: <?php echo $img_height;?>" />
					<?php if($params->get('jux_itemhover_tooltip_style') != 'none') :?>
						<span><?php echo $title; ?></span>
					<?php endif; ?>
				</a>
			</div>
		<?php endforeach;?>
		</div>	
	<?php endif; ?>
	<!---Slider Display -->
	<?php if($display =='slider') : ?>
		<div class="carousel<?php echo $module->id ?> logo_showcase_slider" >
			<?php foreach($slide as $i => $s):
			$imageSrc = JURI::base(true) . '/' . $slidePath . '/' . $s->image;
			$title = $s->title;
			$url = $s->url;
			$target = $s->urlTarget;
			?>
			<div class="jux_logo_item">
				<a class="logo_item_link" href="<?php echo $url; ?>" target="<?php echo $target; ?>" >
					<img class="logo_item_image lazyOwl" src="<?php echo $imageSrc; ?>" style="width: <?php echo $img_width; ?>; height: <?php echo $img_height;?>" />
					<?php if($params->get('jux_itemhover_tooltip_style') != 'none') :?>
						<span><?php echo $title; ?></span>
					<?php endif; ?>
				</a>
			</div>
		<?php endforeach;?>
		</div>
	<?php endif; ?>
</div>