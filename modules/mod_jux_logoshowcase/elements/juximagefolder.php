<?php

/**
 * @version		$Id$
 * @author		JUXTheme
 * @package		Joomla.Site
 *
 * @copyright	Copyright (C) 2013 JUXTheme. All rights reserved.
 * @license		License GNU General Public License version 2 or later; see LICENSE.txt, see LICENSE.php
 */

if (isset($_REQUEST['juxaction'])){
	if (!defined('_JEXEC')) {
    define('_JEXEC', 1);}
	
	$path = dirname(dirname(dirname(dirname(__FILE__))));
    if (!defined('JPATH_BASE'))
    	define('JPATH_BASE', $path);
   
    require_once JPATH_BASE . '/includes/defines.php';
    require_once JPATH_BASE . '/includes/framework.php';
    
    // Mark afterLoad in the profiler.
	JDEBUG ? $_PROFILER->mark('afterLoad') : null;
	
	// Instantiate the application.
	$app = JFactory::getApplication('site');

	// Initialise the application.
	$app->initialise();

	$task = isset($_REQUEST['task']) ? $_REQUEST['task'] : false;
	
	jimport('joomla.filesystem.folder');
	jimport('joomla.filesystem.file');
	jimport('joomla.application.module.helper');
	
	class JUXImageFolderAction {
		var $moduleName = '';
		
		function __construct(){
			$this->moduleName = basename(dirname(__DIR__));
		}
		
		private function basePath(){
			if (strpos(php_sapi_name(), 'cgi') !== false && !ini_get('cgi.fix_pathinfo') && !empty($_SERVER['REQUEST_URI']))
			{
				// PHP-CGI on Apache with "cgi.fix_pathinfo = 0"
				// We shouldn't have user-supplied PATH_INFO in PHP_SELF in this case
				// because PHP will not work with PATH_INFO at all.
				$script_name = $_SERVER['PHP_SELF'];
			}
			else
			{
				// Others
				$script_name = $_SERVER['SCRIPT_NAME'];
			}

			return rtrim(dirname(dirname(dirname(dirname($script_name)))), '/\\');
		}
		
		private function getModule($mid){
			$db = JFactory::getDbo();
			$query = $db->getQuery(true);
			$query->select('m.id, m.title, m.module, m.position, m.content, m.showtitle, m.params');
			$query->from('#__modules AS m');
			$query->where('m.id = '.$mid);
			$db->setQuery($query);
			$module = $db->loadObject ();
			return $module;
		}
		
		function getListImage(){
			$input = JFactory::getApplication()->input;
			$folder = $input->getString('folder');
			$mid = $input->getInt('mid');
			$fieldname = $input->getString('fieldname');
			$imageList = array();
			$success = false;
			$path = JPath::clean(JPATH_ROOT . '/' . $folder);
			
			$module = $this->getModule($mid);
			$params = new JRegistry();
			$paramString = isset($module->params) ? $module->params : '';
			$params->loadString($paramString);
			$imagesCurr = json_decode($params->get($fieldname.'.images'),true);
			$folderCurr = $params->get($fieldname.'.folder');
			if (JFolder::exists($path)) {
				$files = JFolder::files($path);
				$i = 0;
				foreach ($files as $file) {
					if (is_file($path.'/'.$file) && substr($file, 0, 1) != '.' && strtolower($file) !== 'index.html'){
						$ext = JFile::getExt($file);
						switch ($ext) {
							// Image
							case 'jpg':
							case 'png':
							case 'gif':
							case 'xcf':
							case 'odg':
							case 'bmp':
							case 'jpeg':
							case 'ico':
								$image = $folder . '/' . $file;
								$tmp = array();
								$nameArr = explode('.',$file);
								$name = $nameArr[0];
								$tmp['image'] = $file;
								$tmp['title'] = '';								
								$tmp['url'] = '';
								$tmp['urlTarget'] = '';
								$tmp['imagesrc'] = $image;
								$imageList[$name] = $tmp;
								break;
						}
					}
				$i++;
				}
			}
			$html = '';
			if (count($imageList)){
				$success = true;
				$imgArr = array();
				$flag = false;
				if (($folderCurr == $folder) && is_array($imagesCurr)){
					$flag = true;
					foreach ($imagesCurr as $k=>$v){
						$v['key'] = $k;
						if (isset($v['position'])){
							$imgArr[$v['position']] = $v;
						}
					}
				}
				ksort($imgArr);
				$i=0;
				foreach ($imgArr as $k=>$img){
					if (JFile::exists(JPATH_ROOT.'/'.$folder.'/'.$img['image'])){
						
						$nameArr = explode('.',$img['image']);
						$key = 
						$html .= '<div class="jux-img brick small">
							<div>
								<img	data-image="'.$img['image'].'" 
										data-name="'.$nameArr[0].'" 
										data-title="'.(isset($img['title']) ? $img['title'] :'' ).'" 
										data-url="'.(isset($img['url']) ? $img['url'] :'' ).'"
										data-url-target="'.(isset($img['urlTarget']) ? $img['urlTarget'] : 1 ).'"
										data-imagesrc="'.$img['imagesrc'].'" style="max-width: 100px; max-height: 100px;" src="'.$this->basePath().'/'.str_replace($this->basePath(), '',$img['imagesrc']).'
								">
							</div>
							<br>
							<div class="jux-img-btn">
								<a href="#juxModal" onclick="juxModal('.$i.')" title="Edit"><i class="icon-edit"></i></a>&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;<a href="javascript:void(0)" title="Delete" onclick="juxDelete(\''.$img['image'].'\',this)"><i class="icon-delete" ></i></a>
							</div>
							</div>';
						if (isset($imageList[$img['key']]))
							unset($imageList[$img['key']]);
					}
					$i++;
				}
				foreach ($imageList as $k=>$img){
					if (JFile::exists(JPATH_ROOT.'/'.$folder.'/'.$img['image'])){
						$nameArr = explode('.',$img['image']);
						$html .= '<div class="jux-img brick small">
							<div>
								<img	data-image="'.$img['image'].'" 
										data-name="'.$nameArr[0].'" 
										data-title="'.(isset($img['title']) ? $img['title'] :'' ).'" 
										data-url="'.(isset($img['url']) ? $img['url'] :'' ).'"
										data-url-target="'.(isset($img['urlTarget']) ? $img['urlTarget'] : 1 ).'"
										data-imagesrc="'.$img['imagesrc'].'" style="max-width: 100px; max-height: 100px;" src="'.$this->basePath().'/'.str_replace($this->basePath(), '',$img['imagesrc']).'
								">
							</div>
							<br>
							<div class="jux-img-btn">
								<a href="#juxModal" onclick="juxModal('.$i.')" title="Edit"><i class="icon-edit"></i></a>&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;<a href="javascript:void(0)" title="Delete" onclick="juxDelete(\''.$img['image'].'\',this)"><i class="icon-delete" ></i></a>
							</div>
							</div>';
					}
					$i++;
				}
			}
			$return = array('imageHtml'=>$html,'success'=>$success);
			echo json_encode($return);
		}

		/*
		 * Method to upload images
		 */
		public function getUploadImage()
		{
			$forderArr = JFactory::getApplication()->input->post->get('jform', null, 'array');
			$file = JFactory::getApplication()->input->files->get('files');
			$forder = $forderArr['params']['folder']['folder'];
			$this->_uploadFile($forder,$file);
		}
		/*
		 * 
		 */
		protected function _uploadFile($forder,$file)
		{
			$fileName = $file['name'];
			$fileType = $file['type'];
			// Check file type
			$fileType = explode('/', $fileType);
			if ($fileType[0] != 'image')
			{
				return false;
			}
			// Uploading
			if($file)
			{
				$basePath = JPATH_SITE . '/' . $forder;
				$fileDest = JPATH_SITE . '/' . $forder . '/' . $file['name'];
				$fileSrc  = $file['tmp_name'];
				// Upload file
				jimport('joomla.filesystem.file');
				$mediaUpload = JFile::upload($fileSrc, $fileDest);
				if ($mediaUpload)
				{
					return true;
				}
			}
		}
		/*
		 * Delete file
		 */
		function deleteImage(){
			$input = JFactory::getApplication()->input;
			$success = false;
			$folder = $input->getString('folder');
			$image = $input->getString('image');
			$fullPath = JPATH_ROOT.'/'.$folder.'/'.$image;
			if (JFile::exists($fullPath)){
				if (JFile::delete($fullPath))$success = true;
			}
			$return = array('success'=>$success);
			echo json_encode($return);
		}
	}
	if ($task){
		
		$juxAction = new JUXImageFolderAction();	
		$juxAction->$task();
	}
	exit();
}

defined( '_JEXEC' ) or die( 'Restricted access' );
jimport('joomla.filesystem.folder');

class JFormFieldJUXImageFolder extends JFormField {
	/**
	 * The form field type.
	 *
	 * @var    string
	 */
	public $type = 'JUXImageFolder';
	
	/**
	 * The image config
	 */
	private $config = '';
	/**
	 * Method to instantiate the form field object.
	 *
	 * @param   JForm  $form  The form to attach to the form field object.
	 *
	 * @since   11.1
	 */
	public function __construct($form = null)
	{
		parent::__construct($form);
	}
	/**
	 * Method add script to document.
	 */
	private function init(){
		
		
		$params = new JRegistry();
		$params->loadObject($this->form->getValue('params'));
		$this->config = $params->get($this->fieldname.'.images');
		$uri = str_replace("\\","/", str_replace(JPATH_SITE, JURI::root(true), dirname(__FILE__) ));
		
		$document = JFactory::getDocument();
		$document->addStyleSheet(JUri::root() . 'modules/mod_jux_logoshowcase/elements/jux-media-upload/css/jquery.fileupload-ui.css');
		if (JVERSION < '3.0.0'){
			$document->addScript(JURI::root() . 'modules/mod_jux_logoshowcase/assets/js/jquery.min.js');
			$document->addScript(JURI::root() . 'modules/mod_jux_logoshowcase/elements/assets/js/bootstrap.min.js');
			$document->addScript(JURI::root() . 'modules/mod_jux_logoshowcase/elements/assets/js/chosen.jquery.min.js');			
			$document->addCustomTag('<script type="text/javascript">jQuery.noConflict();</script>');
			
			$document->addStyleSheet(JURI::root() . 'modules/mod_jux_logoshowcase/elements/assets/css/chosen.css');			
			$document->addStyleDeclaration('
				#juxListImage,juxSort,.gridly{
					width:450px !important;
				}
				.icon-apply:before, .icon-edit:before, .icon-pencil:before {
					content: "Edit";				
				}	
				.icon-delete:before, .icon-remove:before, .icon-cancel-2:before {
					content: "Delete";
				}
				.fileinput-button input {
					float: right;
					top: 0px;
					transform: none;
				}
				.control-label {
					margin-top: 12px;
				}
				.chzn-container{
					width: 220px !important;
				}
				.chzn-container-single .chzn-drop{
					width: 218px !important;
				}
				juxUrlTarget_chzn{
					float: left;
				}
				.tip-wrap{
					z-index:9999;
				}
				
			');
			$document->addScriptDeclaration('			
				jQuery(function(jQuery){
					jQuery(".fileupload-buttonbar").find("#jux25-button").css("display","block");
					jQuery(".fileupload-buttonbar").find("span .jux-add-file").css("display","none");
					jQuery("#juxColsGridly").val("6");
					jQuery("#juxModal").find("input").attr("size","50");				
				});	
			');
			// Colums of gallery
			$juxCols = 6;
		}else
		{
			$juxCols = 12;
		}

		$document->addScript($uri.'/jux-media-upload/js/vendor/jquery.ui.widget.js');
		$document->addScript($uri.'/jux-media-upload/js/jquery.fileupload.js');
		$document->addScript($uri.'/jux-media-upload/js/jquery.fileupload-process.js');
		$document->addScript(JUri::root() . 'modules/mod_jux_logoshowcase/elements/jux-media-upload/js/jquery.fileupload-validate.js');

		$document->addScript($uri.'/assets/js/jquery.gridly.js');		
		$document->addStyleSheet($uri.'/assets/css/jquery.gridly.css');
		$document->addStyleDeclaration('
		#juxSort{
			display: inline-table;
		    list-style: none outside none;
		    margin: 0;
		    padding: 0;
		    position: relative;
		}
		.jux-img{
		 	border: 1px solid #CCCCCC;
		    cursor: move;	
		    margin: 5px;
		    padding: 1px;
		    text-align: center;
		    width: 100px;
		    z-index: 10;
		}
		.jux-img img{
			
		}
		  .gridly
		  {
		    position: relative;
		    width: 100%;
		  }
		  .brick.small
		  {
		    width: 110px;
		    height: 140px;
		  }
		  .juxMessage{
			color: red;
			font-size: 13px;
			}
		');
		
		$document->addScriptDeclaration('
		var JUX_IMAGE_FOLDER_ACTION = "'.$uri.'/juximagefolder.php?juxaction=folders";
		var JUX_IMAGE_ID = "'.JFactory::getApplication()->input->getInt('id').'";
		var JUX_IMAGE_FIELDNAME = "'.$this->fieldname.'";
		');
		
		$document->addScriptDeclaration('
		jQuery(document).ready(function(){
			juxListImages();
						
			var form = document.adminForm;
			if(!form){
				return false;
			}
			
			var onsubmit = form.onsubmit;
			form.onsubmit = function(e){
				juxUpdateImages();
				if(jQuery.isFunction(onsubmit)){
					onsubmit();
				}
			};
			// Description for form
			jQuery(".title-desc").attr("title","'.JText::_('MOD_JUX_LOGOSHOWCASE_TITLE_DESC').'");
			jQuery(".url-desc").attr("title","'.JText::_('MOD_JUX_LOGOSHOWCASE_URL_DESC').'");
			jQuery(".urlTarget-desc").attr("title","'.JText::_('MOD_JUX_LOGOSHOWCASE_URL_TARGET_DESC').'");
		});
		function juxListImages(){
			var folder = jQuery("#'. $this->id.'").val();
			if(folder == ""){
				alert("Folder path required");
				return;
			}
			jQuery("#juxListImage #juxSort").html("<img src=\"'.str_replace("\\","/", str_replace(JPATH_SITE, JURI::root(true),dirname(__FILE__))).'/assets/images/loading.gif\" width=\"30\" height=\"30\" />");
			jQuery.post(JUX_IMAGE_FOLDER_ACTION,
				{
					task:"getListImage"
					,folder:folder
					,mid:JUX_IMAGE_ID
					,fieldname:JUX_IMAGE_FIELDNAME
					
				},function(res){
					if(res.success){
						jQuery("#juxSort").html(res.imageHtml);
						return jQuery( "#juxSort" ).gridly({selector:".jux-img",columns:'.$juxCols.'});
					}else{
						jQuery("#juxListImage #juxSort").html("<strong style=\'color: red\'>Image not found</strong>");
						return ;	
					}
				},"json");
		};
		function juxUpdateImages(){
			var images = jQuery("#juxListImage").find("img");
			var config = {};
			images.each (function(index,element){
				var $this = jQuery(this),
					name = $this.data("name"),
					position = $this.closest(".jux-img").data("position"),
					item = {};
				$this.data("position",position);
				for (var d in $this.data()) {
					item[d] = $this.data(d);
				};
				if (Object.keys(item).length) config[name] = item;
			});
			jQuery("#'.$this->fieldname.'_images'.'").val(JSON.stringify(config));
		}
		function juxDelete(image,element){
			if(confirm("If you want, We\'ll delete the file on the folder. However, if you want to see that file again, you\'ll have to upload that file")){
				var folder = jQuery("#'. $this->id.'").val();
				jQuery.post(JUX_IMAGE_FOLDER_ACTION,
				{
					task:"deleteImage"
					,folder:folder
					,image:image
				},function(res){
					if(res.success){
						jQuery(element).closest(".brick").remove();
						return jQuery( "#juxSort" ).gridly({selector:".jux-img"});
					}
				},"json");
			}
		}
		
		// Method to add selected chosen to select option.
		function juxSelectedChosen(selectID,valSelected,selectID_Chzn){
			// Remove all selected
			jQuery(selectID).find("option").removeAttr("selected");
			// Add selected
			var flag  = false;
			jQuery(selectID).find("option").each(function(){
				if(jQuery(this).val() == valSelected)
				{
					jQuery(this).attr("selected","selected");
					flag = true;
					return false;
				}				
			});
			if(!flag){
				jQuery("selectID option:first-child").attr("selected","selected");
			}
			
			jQuery(selectID).removeClass("chzn-done").next(selectID_Chzn).remove();
			jQuery(selectID).chosen({
				disable_search_threshold : 10,
				allow_single_deselect : true
			});
		}
		

		function juxModal(id){
			var images = jQuery("#juxListImage").find("img");
			var image = images.get(id);
			jQuery("#juxTitle").val(jQuery(image).data("title"));
			jQuery("#juxUrl").val(jQuery(image).data("url"));
			jQuery("#juxUrlTarget").val(jQuery(image).data("url-target"))
			jQuery("#juxModal").data("imageId",id);
			jQuery("#juxModal").modal("show");
						
			// Select UrlTarget
			var selectUrlTarget = jQuery("#juxUrlTarget");
			var valSelectedUrlTarget = jQuery(image).data("url-target");
			var selectID_ChznUrlTarget = jQuery("#juxUrlTarget_chzn");
			juxSelectedChosen(selectUrlTarget,valSelectedUrlTarget,selectID_ChznUrlTarget);
			
		}
		function juxUpdateImgData(){
			var imageId = jQuery("#juxModal").data("imageId");
			var images = jQuery("#juxListImage").find("img");
			var title = jQuery("#juxTitle").val(),
				url = jQuery("#juxUrl").val(),
				url_target = jQuery("#juxUrlTarget").val();
			var image = images.get(imageId);
			jQuery(image).data("title",title).data("url",url).data("url-target",url_target);
			
			jQuery("#juxModal").find("input,textarea").each(function(){
				jQuery(this).val("");
			});
			jQuery("#juxModal").modal("hide");
		}
		');
	}
	/**
	 * Method to get the field input markup for a generic list.
	 * Use the multiple attribute to enable multiselect.
	 *
	 * @return  string  The field input markup.
	 *
	 */
	protected function getInput()
	{
		$this->init();
		
		$html = array();
		$attr = '';

		// Initialize some field attributes.
		$attr .= $this->element['class'] ? ' class="' . (string) $this->element['class'] . '"' : '';

		// To avoid user's confusion, readonly="true" should imply disabled="true".
		if ((string) $this->element['readonly'] == 'true' || (string) $this->element['disabled'] == 'true')
		{
			$attr .= ' disabled="disabled"';
		}

		$attr .= $this->element['size'] ? ' size="' . (int) $this->element['size'] . '"' : '';
		$attr .= $this->multiple ? ' multiple="multiple"' : '';
		$attr .= $this->required ? ' required="required" aria-required="true"' : '';
		
		// Initialize JavaScript field attributes.
		$attr .= ' onchange="juxListImages();"' ;

		// Get the field options.
		$options = (array) $this->getOptions();

		// Create a regular list.
		$html[] = JHtml::_('select.genericlist', $options, $this->name.'[folder]', trim($attr), 'value', 'text', $this->value, $this->id);
		$html[]	= '<div id="juxListImage" style="margin-top: 18px;"><div id="juxSort" class="gridly"></div></div>';
		$html[]	= '<input id="'.$this->fieldname.'_images'.'" name="'.$this->name.'[images]" type="hidden" value="" />';		

		// Upload
		$html[] = '<!-- The fileupload-buttonbar contains buttons to add/delete files and start/cancel the upload -->
						<div class="fileupload-buttonbar">
							<div class="col-lg-7">
								<!-- The fileinput-button span is used to style the file input field as button -->
								<span class="btn btn-success fileinput-button">
									<i class="glyphicon glyphicon-plus"></i>
									<button id="jux25-button" type="button" style="display: none;">
										<span>Add files...</span>
									</button>
									<span class="jux-add-file">Add files...</span>
									<input id="fileupload" type="file" name="files" multiple="multiple" style="width:auto;height:auto">
								</span>						
								<!-- The loading indicator is shown during file processing -->
								<span class="fileupload-loading"></span>
							</div>
							<br>
							<!-- The global progress bar -->
							<div id="progress" class="progress progress-success progress-striped" style="width: 100%;">
								<div class="bar"></div>
							</div>
						</div>
						<div id="juxMessage" class="juxMessage" style="display:none"></div>';
		$html[]= "<script>
				jQuery(function () {
					jQuery('#fileupload').fileupload({
						url: JUX_IMAGE_FOLDER_ACTION,
						dataType: 'json',
						autoUpload: true,
						acceptFileTypes: /(\.|\/)(ico|png|gif|xcf|odg|bmp|jpeg)$/i,
						done: function (e, data) {
							juxListImages();
							jQuery('#progress .bar').css(
								'width',
								0 + '%'
							);							
						},
						progressall: function (e, data) {
							var progress = parseInt(data.loaded / data.total * 100, 10);
							jQuery('#progress .bar').css(
								'width',
								progress + '%'
							);
						}
					}).on('fileuploadprocessalways', function (e, data) {
							var index = data.index,
								file = data.files[index];
							
							if (file.error) {
								jQuery('#juxMessage').fadeIn().html(file.error);
									setTimeout(function(){
									jQuery('#juxMessage').hide();
								},5000);
							}
						
					}).on('fileuploadadd', function (e, data) {
						jQuery('input[name=task]').prop('value','getUploadImage');
					});
				});
				</script>";
			$html[]	= $this->createModal();
		return implode($html);
	}
	
	private function createModal(){
		$html = '<br/><br/><br/>
			<div id="juxModal" class="modal hide fade">
				<div class="modal-body">
					<div class="control-group">
						<label class="control-label title-desc" title="" for="juxTitle" >Title</label>
						<div class="controls">
							<input type="text" id="juxTitle">
						</div>
					</div>
					<div class="control-group">
						<label class="control-label url-desc" for="juxUrl">Url</label>
						<div class="controls">
							<input type="text" id="juxUrl">
						</div>
					</div>
					<div class="control-group">					
						<label class="control-label urlTarget-desc" for="juxUrlTarget">Url Target</label>
						<div class="controls">
							<select id="juxUrlTarget" style="display:block!important">
								<option value="_parent">Parent</option>
								<option value="_blank">Blank</option>
							</select>
						</div>
					</div>
				</div>
				<div class="modal-footer">
					<button class="btn" data-dismiss="modal" type="button">Close</button>
					<button class="btn btn-primary" onclick="juxUpdateImgData()" type="button" >Update</button>
				</div>
			</div>
		';
		return $html;
	}
	/**
	 * Method to get the field options. 
	 *
	 * @return  array  The field option objects.
	 *
	 */
	protected function getOptions()
	{
		$options = array();

		// Initialize some field attributes.
		$filter = (string) $this->element['filter'];
		$exclude = (string) $this->element['exclude'];

		// Get the path in which to search for file options.
		$path = (string) $this->element['directory'];
		if (!is_dir($path))
		{
			$path = JPATH_ROOT . '/' . $path;
		}

		// Get a list of folders in the search path with the given filter.
		//$folders = JFolder::folders($path, $filter);
		$listFolers = self::listFolderTree($path,$filter,100);

		// Build the options list from the list of folders.
		if (is_array($listFolers)){
			$children = array();
			foreach ($listFolers as $k => $folder) {
					if ($exclude)
					{
						if (preg_match(chr(1) . $exclude . chr(1), $folder))
						{
							continue;
						}
					}
					
					$folder = (object) $folder;
					$folder->title = $folder->name;
					$folder->parent_id = $folder->parent;
					$pt = $folder->parent;
					$list = @$children[$pt] ? $children[$pt] : array();
					array_push($list, $folder);
					$children[$pt] = $list;
				}
		
			$list = JHTML::_('menu.treerecurse', 0, '', array(), $children, 9999, 0, 0);
			$options = array();
			foreach ($list as $item) {
				$item->treename = JString::str_ireplace('&#160;', '- ', JString::str_ireplace('&#160;&#160;', '&#160;', $item->treename));
				$options[] = JHTML::_('select.option',str_replace(DIRECTORY_SEPARATOR,'/',trim($item->relname,DIRECTORY_SEPARATOR)), ' ' . $item->treename);
				
			}
		}
		return $options;
	}
	
	/**
	 * Lists folder in format suitable for tree display.
	 *
	 * @param   string   $path      The path of the folder to read.
	 * @param   string   $filter    A filter for folder names.
	 * @param   integer  $maxLevel  The maximum number of levels to recursively read, defaults to three.
	 * @param   integer  $level     The current level, optional.
	 * @param   integer  $parent    Unique identifier of the parent folder, if any.
	 *
	 * @return  array  Folders in the given folder.
	 *
	 */
	public static function listFolderTree($path, $filter, $maxLevel = 100, $level = 1, $parent = 1)
	{
		$dirs = array();
		
		if ($level == 1)
		{
			$fullName = JPath::clean($path);
			$dirs[] = array('id' => 1, 'parent' => 0, 'name' => basename($path), 'fullname' => $fullName,
					'relname' => str_replace(JPATH_ROOT, '', $fullName));
			$GLOBALS['_jux_folder_tree_index'] = 1;
		}
		if ($level < $maxLevel)
		{
			
			$folders =JFolder::folders($path, $filter);
			
			// First path, index foldernames
			foreach ($folders as $name)
			{
				$id = ++$GLOBALS['_jux_folder_tree_index'];
				$fullName = JPath::clean($path . '/' . $name);
				$dirs[] = array('id' => $id, 'parent' => $parent, 'name' => $name, 'fullname' => $fullName,
					'relname' => str_replace(JPATH_ROOT, '', $fullName));
				$dirs2 = self::listFolderTree($fullName, $filter, $maxLevel, $level + 1, $id);
				$dirs = array_merge($dirs, $dirs2);
			}
		}
		return $dirs;
	}
}
