<?php
/**
 * @version		$id: $
 * @author		JoomlaUX!
 * @package		Joomla!
 * @subpackage	mod_jux_logoshowcase
 * @copyright	Copyright (C) 2013 by JoomlaUX. All rights reserved.
 * @license		http://www.gnu.org/licenses/gpl-3.0.html GNU/GPL version 3, See LICENSE.txt
**/

// no direct access
defined('_JEXEC') or die( 'Restricted access' );

if (!class_exists('modJUXMegaMenuCss')){
	require_once dirname(__FILE__).'/includes/css.php';
}

class mod_JUX_LogoshowcaseHelper {
	
	public static function getCssProcessor(&$params,$filename,$prefix){
		return modJUXLogoshowcaseCss::process($params,$filename,$prefix);		
	}

	public static function javascript() {
		$document = JFactory::getDocument();
		$document->addScript(JURI::base() . 'modules/mod_jux_logoshowcase/assets/js/isotope.pkgd.min.js');
		$document->addScript(JURI::base() . 'modules/mod_jux_logoshowcase/assets/js/owl.carousel.js');
	}

	public static function css() {
		$document = JFactory::getDocument();
		$document->addStyleSheet(JURI::base(). 'modules/mod_jux_logoshowcase/assets/css/style.css');
		$document->addStyleSheet(JURI::base() . 'modules/mod_jux_logoshowcase/assets/css/owl.carousel.css');
		$document->addStyleSheet(JURI::base() . 'modules/mod_jux_logoshowcase/assets/css/owl.theme.css');
		$document->addStyleSheet(JURI::base() . 'modules/mod_jux_logoshowcase/assets/css/owl.transitions.css');
	}
}