<?php
/**
 * @package 	JUX Logo Showcase for Joomla!
 * @subpackage 	JUX Logo Showcase Module for Joomla!
 * @version 	$Id$
 * @author 		JoomlaUX Admin
 * @copyright	Copyright(C) 2014 - JoomlaUX Solutions. All rights reserved.
 * @license 	http://www.gnu.org/licenses/gpl-3.0.html GNU/GPL version 3
**/
// no direct access
defined('_JEXEC') or die( 'Restricted access' );

/**	Loading helper class **/
require_once (dirname(__FILE__).'/'.'helper.php');

// Add style css and js to header
mod_JUX_LogoshowcaseHelper::css();
mod_JUX_LogoshowcaseHelper::javascript();

if(!defined('DEMO_MODE')) {
    // Change DEMO_MODE value to 1 to enable the demo mode.
    define('DEMO_MODE', 0);
}
$document = JFactory::getDocument();

// Get Params
$display = $params->get('jux_display','slider');
$img_width = $params->get('jux_img_width',100);
$img_height = $params->get('jux_img_height',100);

/* load javascript. */
$input = JFactory::getApplication()->input;

// Get module sfx class
$moduleclass_sfx = htmlspecialchars($params->get('moduleclass_sfx'));

$customcss = 'modules/mod_jux_logoshowcase/assets/css/style/custom-'.$module->id.'.css';
if (mod_JUX_LogoshowcaseHelper::getCssProcessor($params,$customcss,'#jux_logo'.$module->id)){
	$document->addStyleSheet($customcss);
}

require( JModuleHelper::getLayoutPath( 'mod_jux_logoshowcase', $params->get('layout', 'default') ) );