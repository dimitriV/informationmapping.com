<?php header('X-UA-Compatible: IE=edge,chrome=1'); ?>
<!DOCTYPE html>
<html>
<head>
<meta charset="utf-8">
<title>A corporated standard for your documents</title>
<meta name="viewport" content="width=device-width, initial-scale=1.0">
<meta name="description" content="Start with FS Pro 2013 and increase your productivity today">
<meta name="keywords" content="writing technical documentation, proposal letter template, technical writing software, template for proposal, business document template, proposal software, writing software, documentation software" />
<link href="css/bootstrap.min.css" rel="stylesheet" media="screen">
<!-- HTML5 shim and Respond.js IE8 support of HTML5 elements and media queries -->
<!--[if lt IE 9]>
      <script src="js/html5shiv.js"></script>
      <script src="js/respond.min.js"></script>
    <![endif]-->
<link href="//fonts.googleapis.com/css?family=Tauri" rel="stylesheet" type="text/css" />
<link href="css/imi.css" rel="stylesheet" media="screen">
<link href="//netdna.bootstrapcdn.com/font-awesome/4.1.0/css/font-awesome.min.css" rel="stylesheet">
<!--<link href="http://tympanus.net/Development/HoverEffectIdeas/css/component.css" rel="stylesheet">-->

<script src="//code.jquery.com/jquery.min.js"></script>
<script type="text/javascript" src="js/html5lightbox.js"></script>
<script>



$(document).ready(function(){
  /*$("#panel1").hide();
  $("#panel2").hide();
  $("#panel3").hide();
  $("#panel4").hide();*/
  $("#zoe1").mouseenter(function(){
  $("#panel1").fadeIn();
  $("#img14").fadeTo("fast",0.15);
});

  $("#zoe1").mouseleave(function(){
  $("#panel1").fadeOut();
  $("#img14").fadeTo("fast",1);
});

  $("#zoe2").mouseenter(function(){
  $("#panel2").fadeIn();
  $("#img15").fadeTo("fast",0.15);
});

  $("#zoe2").mouseleave(function(){
  $("#panel2").fadeOut();
  $("#img15").fadeTo("fast",1);
});

  $("#zoe3").mouseenter(function(){
  $("#panel3").fadeIn();
  $("#img16").fadeTo("fast",0.15);
});

  $("#zoe3").mouseleave(function(){
  $("#panel3").fadeOut();
  $("#img16").fadeTo("fast",1);
});

  $("#zoe4").mouseenter(function(){
  $("#panel4").fadeIn();
  $("#img17").fadeTo("fast",0.15);
});

  $("#zoe4").mouseleave(function(){
  $("#panel4").fadeOut();
  $("#img17").fadeTo("fast",1);
});


});
</script>
<style>
#panel1, #panel2, #panel3, #panel4 {
	display: none;
	position: absolute;
	top: 40px;
	width: 261px;
	padding: 20px;
	font-size: 16px;
	font-family: Tauri;
	line-height: 20px;
}
#panel1 a, #panel2 a, #panel3 a, #panel4 a {
	color: #333;
}
#zoe1, #zoe2, #zoe3, #zoe4 {
	text-align: center;
}
</style>
</head>
<body>
<div class="container">
  <div class="row">
    <div class="col-sm-9" id="header"> <a href="http://www.informationmapping.com/"><img src="images/logo-informationmapping.jpg" width="169" height="71" alt="Information Mapping"></a></div>
    <div class="col-sm-3" id="header2" style="padding-top: 30px; text-align: right; padding-left: 0px;"> <a href="http://www.informationmapping.com/" target="_blank"><em class="fa fa-globe"> </em> Discover our website</a></div>
  </div>
  <div class="row">
    <div class="col-sm-12" id="toptitle">
      <!--<h1>Create clean &amp; consistent documents within your team</h1>-->
    </div>
  </div>
  <div class="row" style="padding-bottom: 30px;">
    <div class="col-sm-9">
      <div class="row">
        <div class="col-sm-12" style="padding-right: 30px; padding-top: 30px;"> <img src="images/gettothepoint.png" alt="Team Pack" class="img-responsive"/>
          <p style="font-size: 16px;  margin-top: 33px;font-weight: bold;">Looking for the right word? We don't read business documents word for word. We all prefer quick, consistent and professional documents that go straight to the point, telling us what we need to know.</p>
          <p style="margin-top: 25px;margin-bottom: 25px; font-size:16px; line-height: 24px;">Welcome to FS Pro 2013 for Microsoft Word. It’s software that helps you write exactly what you have to write, to help others read exactly they have to know. You’ll write clearly, consistently, professionally and go straight to the point. </p>
          <!--<a href="https://informationmapping.box.com/s/vh6fmejvtots8xhpymc6" target="_blank"><img src="images/seeba.png" alt="See Before &amp; After examples" class="img-responsive" /></a> -->
          
          
          
          <div class="row">
          <div class="col-sm-4" style="text-align: center;">
          <p style="font-size: 16px;"><i class="fa fa-user fa-2x"></i> &nbsp;1 user</p>
          <a href="http://www.informationmapping.com/en/shop/checkout/cart/add?product=392&amp;qty=1&amp;bundle_option[545]=773&amp;bundle_option[545]=774&amp;bundle_option[545]=986&amp;bundle_option[545]=775" target="_blank" class="btn btn-default btn-sm" style="box-shadow: inset 0px 1px 1px 0px #FAA82B;background-color: #F90;border-color: #f90; padding: 15px;font-weight: bold;font-size: 16px;-webkit-box-shadow: 2px 2px 5px 0px rgba(51,49,51,1);
-moz-box-shadow: 2px 2px 5px 0px rgba(51,49,51,1);
box-shadow: 2px 2px 5px 0px rgba(51,49,51,1);background: linear-gradient(to bottom, rgba(244,110,20,1) 0%,rgba(237,70,0,1) 100%);"><i class="fa fa-shopping-cart"></i> &nbsp;BUY NOW for $ 125</a>
          </div>
          <div class="col-sm-4" style="text-align: center; padding-right: 0;">
          <p style="font-size: 16px;"><i class="fa fa-group fa-2x"></i> &nbsp;5 users (save 10%)</p>
          <a href="http://www.informationmapping.com/en/shop/checkout/cart/add?product=486&amp;qty=1&amp;bundle_option[654]=1010&amp;bundle_option[654]=1011&amp;bundle_option[654]=1012&amp;bundle_option[654]=1009" target="_blank" class="btn btn-default btn-sm" style="box-shadow: inset 0px 1px 1px 0px #FAA82B;background-color: #F90;border-color: #f90; padding: 15px;font-weight: bold;font-size: 16px;-webkit-box-shadow: 2px 2px 5px 0px rgba(51,49,51,1);
-moz-box-shadow: 2px 2px 5px 0px rgba(51,49,51,1);
box-shadow: 2px 2px 5px 0px rgba(51,49,51,1);background: linear-gradient(to bottom, rgba(244,110,20,1) 0%,rgba(237,70,0,1) 100%);"><i class="fa fa-shopping-cart"></i> &nbsp;BUY NOW for $ 559</a>
          </div>
          <div class="col-sm-4" style="text-align: center; padding-right: 0;">
          <p style="font-size: 16px;"><i class="fa fa-group fa-2x"></i> &nbsp;10 users (save 20%)</p>
          <a href="http://www.informationmapping.com/en/shop/checkout/cart/add?product=491&amp;qty=1&amp;bundle_option[655]=1018&amp;bundle_option[655]=1019&amp;bundle_option[655]=1020&amp;bundle_option[655]=1017" target="_blank" class="btn btn-default btn-sm" style="box-shadow: inset 0px 1px 1px 0px #FAA82B;background-color: #F90;border-color: #f90; padding: 15px;font-weight: bold;font-size: 16px;-webkit-box-shadow: 2px 2px 5px 0px rgba(51,49,51,1);
-moz-box-shadow: 2px 2px 5px 0px rgba(51,49,51,1);
box-shadow: 2px 2px 5px 0px rgba(51,49,51,1);background: linear-gradient(to bottom, rgba(244,110,20,1) 0%,rgba(237,70,0,1) 100%);"><i class="fa fa-shopping-cart"></i> &nbsp;BUY NOW for $ 999</a>
          </div>
          </div>
          
          
          
          
          
          
          
          </div>
      </div>
    </div>
    <div class="col-sm-3" id="promo"> 
      
      <div class="row">
        
        <!--<p style=""><strong>FS Pro 2013 includes:</strong></p>-->
      </div>
      <div class="row">

      <div class="col-sm-12">
        
     <!-- <p style="line-height: 25px; padding-top: 7px;"> <i class="fa fa-check-square-o" style="color: #009edf"></i> Add-in  software for Microsoft Word<br />
        <i class="fa fa-check-square-o" style="color: #009edf"></i> One license key for easy administration<br />
        <i class="fa fa-check-square-o" style="color: #009edf"></i> Video tutorial<br />
        <i class="fa fa-check-square-o" style="color: #009edf"></i> 20 business templates<br />

      </p>-->

      </div>
      </div>
      <div class="row">
            <!--<div class="col-sm-12" style="padding-right: 0px;">
      <img src="images/5pack-10packisolated.png" alt="" class="img-responsive" style="padding-top: 27px;" />
      </div>-->
      
      </div>
      
      
<div class="row">
      
      <p>&nbsp;</p><p>&nbsp;</p><p>&nbsp;</p><p>&nbsp;</p><p>&nbsp;</p><p>&nbsp;</p><p>&nbsp;</p><p>&nbsp;</p>
      
      <h2 style="margin-bottom: 25px; color: #333;font-family:'Tauri', sans-serif;font-size: 22px;">Make your point quickly and clearly!</h2>
      <p>Write important information. No filler.</p>
      <p style="line-height: 25px; padding-top: 7px;"> 
      <i class="fa fa-check-square-o" style="color: #009edf"></i> Accommodate reading habits.<br />
      <i class="fa fa-check-square-o" style="color: #009edf"></i> Add-in  software for Microsoft Word<br />
        <!--A software tool that makes it easy to write clear, easily understood documents in Microsoft Word<br>--> 
        <i class="fa fa-check-square-o" style="color: #009edf"></i> One license key for easy administration<br />
        <!--You will get one license key for 5 activations instead of 5 separate license keys. This makes license management quick and easy!<br />--> 
        <i class="fa fa-check-square-o" style="color: #009edf"></i> Video tutorials<br />
        <!--Get started quickly with the built-in video tutorials that help you learning the software tool.--> 
        <i class="fa fa-check-square-o" style="color: #009edf"></i> 20 business templates<br />
        <!--With the FS Pro 2013 software you'll receive a whole range of typical business document templates to jumpstart your writing!--> 
      </p><p style="padding-top: 15px; padding-bottom: 15px;font-weight: bold;">Would you like to see it for yourself? <a href="http://www2.informationmapping.com/fspro2013-trial" target="_blank">Try FS Pro 2013</a> for free for 30 days.</p> 
      </div>

      
      
      
    </div>
  </div>
  <div class="row" style="padding-top: 10px;padding-bottom: 30px; border-top: 1px solid #ddd;">
    
    <div class="col-sm-3" id="zoe2"> <a href="integrated.html" class="html5lightbox" data-width="1000" data-height="300" title="It's integrated in Word"><img src="images/img2.png" id="img15" class="img-responsive" alt="" /></a>

        <h3 style="width: 261px; text-align: center; position: absolute; bottom: -12px;">INTEGRATES INTO MS WORD</h3>

      <div id="panel2"><a href="integrated.html" class="html5lightbox" data-width="1000" data-height="300" title="It's integrated in Word">No complicated new programs. FS Pro integrates into MS 	Word. You already know how to use it.</a></div>
    </div>
    
    <div class="col-sm-3" id="zoe4"> <a href="speed.html" class="html5lightbox" data-width="1300" data-height="650" title="Speed up your writing"><img src="images/img4.png" id="img17" class="img-responsive" alt="" /></a>

        <h3 style="width: 261px; text-align: center; position: absolute; bottom: -12px;">SPEEDS UP YOUR WRITING</h3>

      <div id="panel4"><a href="speed.html" class="html5lightbox" data-width="1300" data-height="650" title="Speed up your writing">Forget about format. Focus on content. And double your 	writing speed.</a></div>
    </div>
    
    
    
    
    
    <div class="col-sm-3" id="zoe3"> <a href="templates.html" class="html5lightbox" data-width="1300" data-height="650" title="Includes 20 templates"><img src="images/img3.png" id="img16" class="img-responsive" alt="" /></a>

        <h3 style="width: 261px; text-align: center; position: absolute; bottom: -12px;">INCLUDES 20 TEMPLATES</h3>

      <div id="panel3"><a href="templates.html" class="html5lightbox" data-width="1300" data-height="650" title="Includes 20 templates">With templates for everything from manuals and minutes to 	proposals and policies. Half the work is already done.</a></div>
    </div>
    

<div class="col-sm-3" id="zoe1" style="position:relative;"> 
    <a href="before_after.html" class="html5lightbox" data-width="1300" data-height="550" title="See a before &amp; after"><img src="images/img1.png" id="img14" class="img-responsive" alt="" /></a>

        <h3 style="width: 261px; text-align: center; position: absolute; bottom: -12px;">VISIBLE RESULTS</h3>

      <div id="panel1"><a href="before_after.html" class="html5lightbox" data-width="1300" data-height="550" title="See a before &amp; after">Seeing is believing. Click here and you’ll believe the 	difference FS Pro 2013 makes.</a></div>
    </div>    
    
    
  </div>
  <div class="row" style="padding-top: 30px;padding-bottom: 20px; border-top: 1px solid #ddd;">
    <div class="row">
      <div class="col-sm-9">
        <h2 style="margin-bottom: 40px; color: #333;font-size: 22px; font-family:'Tauri', sans-serif; margin-left: 15px;">The benefits for you and your organization</h2>
        <div class="row" style="padding-left: 15px;">
          <div class="col-sm-4">
            <div style="text-align: center; color: #666"><i class="fa fa-pencil fa-4x"></i></div>
            <h3 style="text-align:center; line-height:25px; color: #666;">For writers </h3>
            <p style="line-height: 22px;">Work more quickly, efficiently and effectively. Especially in 	team-authoring situations.</p>
          </div>
          <div class="col-sm-4">
            <div style="text-align: center; color: #666"><i class="fa fa-group fa-4x"></i></div>
            <h3 style="text-align:center; line-height:25px; color: #666;">For readers </h3>
            <p style="line-height: 22px;">Information that’s easy to find. And even easier to 	understand.  </p>
          </div>
          <div class="col-sm-4">
            <div style="text-align: center; color: #666"><i class="fa fa-building fa-4x"></i></div>
            <h3 style="text-align:center;  line-height:25px; color: #666;">For your organization</h3>
            <p style="line-height: 22px;">Fewer errors. Higher productivity. Improved operating efficiency. Isn’t this what your organization wants?</p>
          </div>
        </div>
      </div>
      <div class="col-sm-3" id="form"  style="border-left: 1px solid #ddd">
        <div>
          <h2 style="margin-bottom: 25px; color: #333;font-family:'Tauri', sans-serif;font-size: 22px;">What FS Pro 2013 does for you</h2>
          <p><i class="fa fa-check-square-o" style="color: #009edf"></i> You'll work up to 2 times faster.</p>
          <p><i class="fa fa-check-square-o" style="color: #009edf"></i> You already use Microsoft Word. We just make it more powerful.</p>
          <p><i class="fa fa-check-square-o" style="color: #009edf"></i> We decrease errors by 54%.</p>
          <p><i class="fa fa-check-square-o" style="color: #009edf"></i> Go straight to the point - save your readers time, increase their interest.</p>
          <img src="images/laptop_FSPro2013.png" alt="" class="img-responsive" /> </div>
      </div>
    </div>
  </div>
  <div class="row" style="padding-top: 30px; padding-bottom: 30px; border-top: 1px solid #ddd;">
    <div class="col-sm-6 text-center" style="color: #666">
      <p><i class="fa fa-clock-o fa-4x"></i></p>
      <h3>See how much time &amp; money you'll save.</h3>
      <form>
        <a href="http://www.informationmapping.com/demo/" target="_blank" class="btn btn-default">Take the time test.</a>
      </form>
    </div>
    <div class="col-sm-6 text-center" style="color: #666" id="questions">
      <p><i class="fa fa-question-circle fa-4x"></i></p>
      <h3>Questions?</h3>
      <form>
        <a href="http://www.informationmapping.com/livedemo" target="_blank" class="btn btn-default">A live demo will give you all the answers.</a>
      </form>
    </div>
  </div>
  <div class="row" id="footer" style="margin-top: 0px;">
    <div class="col-sm-12" id="footertext">
      <p>&copy; <?php echo date("Y"); ?> Information Mapping International | <a href="http://www.informationmapping.com/">www.informationmapping.com</a> | <a href="tel://003292531425"><em class="fa fa-phone"> </em> +32 9 253 14 25</a> | Contact our office in <a href="http://www.informationmapping.com/us/contact">US</a>, <a href="http://www.informationmapping.com/en/contact-3">Europe</a> or <a href="http://www.informationmapping.com/in/contact-2">Asia</a></p>
      <p class="icons"> <a href="https://twitter.com/infomap"><i class="fa fa-twitter-square fa-2x" style="color: #009edf;"></i></a> <a href="https://www.facebook.com/iminv"><i class="fa fa-facebook-square fa-2x" style="color: #009edf;"></i></a> <a href="http://www.linkedin.com/groups?gid=983927"><i class="fa fa-linkedin-square fa-2x" style="color: #009edf;"></i></a> <a href="http://www.youtube.com/user/InformationMapping"><i class="fa fa-youtube-square fa-2x" style="color: #009edf;"></i></a> </p>
    </div>
  </div>
</div>
<script src="js/bootstrap.min.js"></script> 
<script type="text/javascript">

  var _gaq = _gaq || [];
  _gaq.push(['_setAccount', 'UA-24028581-1']);
  _gaq.push(['_trackPageview']);

  (function() {
    var ga = document.createElement('script'); ga.type = 'text/javascript'; ga.async = true;
    ga.src = ('https:' == document.location.protocol ? 'https://ssl' : 'http://www') + '.google-analytics.com/ga.js';
    var s = document.getElementsByTagName('script')[0]; s.parentNode.insertBefore(ga, s);
  })();

</script> 
<script type="text/javascript">
piAId = '9622';
piCId = '36422';

(function() {
	function async_load(){
		var s = document.createElement('script'); s.type = 'text/javascript';
		s.src = ('https:' == document.location.protocol ? 'https://pi' : 'http://cdn') + '.pardot.com/pd.js';
		var c = document.getElementsByTagName('script')[0]; c.parentNode.insertBefore(s, c);
	}
	if(window.attachEvent) { window.attachEvent('onload', async_load); }
	else { window.addEventListener('load', async_load, false); }
})();
</script> 
<script type="text/javascript">
/* <![CDATA[ */
var google_conversion_id = 963549049;
var google_custom_params = window.google_tag_params;
var google_remarketing_only = true;
/* ]]> */
</script> 
<script type="text/javascript" src="//www.googleadservices.com/pagead/conversion.js">
</script>
<noscript>
<div style="display:inline;"> <img height="1" width="1" style="border-style:none;" alt="" src="//googleads.g.doubleclick.net/pagead/viewthroughconversion/963549049/?value=0&amp;guid=ON&amp;script=0"/> </div>
</noscript>
</body>
</html>