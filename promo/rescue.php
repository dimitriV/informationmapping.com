<?php header('X-UA-Compatible: IE=edge,chrome=1'); ?>
<!DOCTYPE html>
<html>
<head>
<meta charset="utf-8">
<title>Information Mapping to the rescue for your documentation needs</title>
<meta name="viewport" content="width=device-width, initial-scale=1.0">
<meta name="description" content="Start with FS Pro 2013 and increase your productivity today">
<meta name="keywords" content="writing technical documentation, proposal letter template, technical writing software, template for proposal, business document template, proposal software, writing software, documentation software" />
<link href="css/bootstrap.min.css" rel="stylesheet" media="screen">
<!-- HTML5 shim and Respond.js IE8 support of HTML5 elements and media queries -->
<!--[if lt IE 9]>
      <script src="js/html5shiv.js"></script>
      <script src="js/respond.min.js"></script>
    <![endif]-->
<link href="//fonts.googleapis.com/css?family=Tauri" rel="stylesheet" type="text/css" />
<link href="css/imi.css" rel="stylesheet" media="screen">
<link href="//netdna.bootstrapcdn.com/font-awesome/4.1.0/css/font-awesome.min.css" rel="stylesheet">
<!--<link href="http://tympanus.net/Development/HoverEffectIdeas/css/component.css" rel="stylesheet">-->

<script src="//code.jquery.com/jquery.min.js"></script>
<script type="text/javascript" src="js/html5lightbox.js"></script>
<script>



$(document).ready(function(){
  /*$("#panel1").hide();
  $("#panel2").hide();
  $("#panel3").hide();
  $("#panel4").hide();*/
  $("#zoe1").mouseenter(function(){
  $("#panel1").fadeIn();
  $("#img14").fadeTo("fast",0.15);
});

  $("#zoe1").mouseleave(function(){
  $("#panel1").fadeOut();
  $("#img14").fadeTo("fast",1);
});

  $("#zoe2").mouseenter(function(){
  $("#panel2").fadeIn();
  $("#img15").fadeTo("fast",0.15);
});

  $("#zoe2").mouseleave(function(){
  $("#panel2").fadeOut();
  $("#img15").fadeTo("fast",1);
});

  $("#zoe3").mouseenter(function(){
  $("#panel3").fadeIn();
  $("#img16").fadeTo("fast",0.15);
});

  $("#zoe3").mouseleave(function(){
  $("#panel3").fadeOut();
  $("#img16").fadeTo("fast",1);
});

  $("#zoe4").mouseenter(function(){
  $("#panel4").fadeIn();
  $("#img17").fadeTo("fast",0.15);
});

  $("#zoe4").mouseleave(function(){
  $("#panel4").fadeOut();
  $("#img17").fadeTo("fast",1);
});


});
</script>
<style>
#panel1, #panel2, #panel3, #panel4 {
	display: none;
	position: absolute;
	top: 40px;
	width: 261px;
	padding: 20px;
	font-size: 16px;
	font-family: Tauri;
	line-height: 20px;
}
#panel1 a, #panel2 a, #panel3 a, #panel4 a {
	color: #333;
}
#zoe1, #zoe2, #zoe3, #zoe4 {
	text-align: center;
}
</style>
</head>
<body>
<div class="container">
  <div class="row">
    <div class="col-sm-9" id="header"> <a href="http://www.informationmapping.com/"><img src="images/logo-informationmapping.jpg" width="169" height="71" alt="Information Mapping"></a></div>
    <div class="col-sm-3" id="header2" style="padding-top: 30px; text-align: right; padding-left: 0px;"> <a href="http://www.informationmapping.com/" target="_blank"><em class="fa fa-globe"> </em> Discover our website</a></div>
  </div>
  <div class="row">
    <div class="col-sm-12" id="toptitle">
      <!--<h1>Information Mapping&reg; to the rescue for your documentation needs</h1>-->
    </div>
  </div>
  <div class="row" style="padding-bottom: 30px;">
    <div class="col-sm-2">
      <div class="row">
        <div class="col-sm-12" style="padding-right: 30px; padding-top: 30px;"> </div>
      <img src="images/download.jpg" alt="Team Pack" class="img-responsive"/>
      </div>
    </div>
    <div class="col-sm-6">
    
          <p style="font-size: 22px;  margin-top: 30px; font-family: 'Tauri', sans-serif;">You're not in business to create great documentation? <strong>We are!</strong></p>
          <p style="margin-top: 25px;margin-bottom: 25px; font-size:16px; line-height: 24px;">Our professional services team applies Information Mapping&reg;, a proven structured authoring methodology, to transform your content into clear, user-focused documents.</p>
          <a href="https://informationmapping.box.com/s/vh6fmejvtots8xhpymc6" target="_blank">See <strong>Before</strong> and <strong>After</strong> Example</a>
    </div>
    <div class="col-sm-4" id="promo"> 
      <!--<h2 style="padding-top: 13px; text-align:center">Save <strong>up to 20%</strong></h2>-->
      
      <div class="row">
        <h2 style="padding-top: 10px;padding-bottom: 13px;">Let's talk!</h2>
        
              <form class="form-horizontal" role="form" action="#" method="post">
        <div class="form-group">
          <div class="col-sm-10">
            <label for="firstname">First name</label>
          </div>
          <div class="col-sm-10">
            <input type="text" class="form-control input-sm" id="firstname" name="firstname" value="">
                      </div>
        </div>
        <div class="form-group">
          <div class="col-sm-10">
            <label for="lastname">Last name</label>
          </div>
          <div class="col-sm-10">
            <input type="text" class="form-control input-sm" id="lastname" name="lastname" value="">
                      </div>
        </div>
        <div class="form-group">
          <div class="col-sm-10">
            <label for="email">Phone number</label>
          </div>
          <div class="col-sm-10">
            <input type="email" class="form-control input-sm" id="email" name="email" value="">
                      </div>
        </div>
        <!--<div class="form-group">
          <div class="col-sm-10">
            <label for="job">Job title</label>
          </div>
          <div class="col-sm-10">
            <input type="text" class="form-control" id="job" name="job" value="">
                      </div>
        </div>-->
        <!--<div class="form-group">
          <div class="col-sm-10">
            <label for="phone">Phone</label>
          </div>
          <div class="col-sm-10">
            <input type="tel" class="form-control" id="phone" name="phone" value="">
                      </div>
        </div>-->
        <div class="form-group" style="display: none;">
          <div class="col-sm-10">
            <label for="country">Country</label>
          </div>
          <div class="col-sm-10">
            <select name="country" id="country" class="form-control">
              <option value="Afghanistan">Afghanistan</option>
              <option value="Albania">Albania</option>
              <option value="Algeria">Algeria</option>
              <option value="American Samoa">American Samoa</option>
              <option value="Andorra">Andorra</option>
              <option value="Angola">Angola</option>
              <option value="Anguilla">Anguilla</option>
              <option value="Antarctica">Antarctica</option>
              <option value="Antigua and Barbuda">Antigua and Barbuda</option>
              <option value="Argentina">Argentina</option>
              <option value="Armenia">Armenia</option>
              <option value="Aruba">Aruba</option>
              <option value="Australia">Australia</option>
              <option value="Austria">Austria</option>
              <option value="Azerbaijan">Azerbaijan</option>
              <option value="Bahamas">Bahamas</option>
              <option value="Bahrain">Bahrain</option>
              <option value="Bangladesh">Bangladesh</option>
              <option value="Barbados">Barbados</option>
              <option value="Belarus">Belarus</option>
              <option value="Belgium" selected>Belgium</option>
              <option value="Belize">Belize</option>
              <option value="Benin">Benin</option>
              <option value="Bermuda">Bermuda</option>
              <option value="Bhutan">Bhutan</option>
              <option value="Bolivia">Bolivia</option>
              <option value="Bosnia and Herzegovina">Bosnia and Herzegovina</option>
              <option value="Botswana">Botswana</option>
              <option value="Bouvet Island">Bouvet Island</option>
              <option value="Brazil">Brazil</option>
              <option value="British Indian Ocean Territory">British Indian Ocean Territory</option>
              <option value="Brunei Darussalam">Brunei Darussalam</option>
              <option value="Bulgaria">Bulgaria</option>
              <option value="Burkina Faso">Burkina Faso</option>
              <option value="Burundi">Burundi</option>
              <option value="Cambodia">Cambodia</option>
              <option value="Cameroon">Cameroon</option>
              <option value="Canada">Canada</option>
              <option value="Cape Verde">Cape Verde</option>
              <option value="Cayman Islands">Cayman Islands</option>
              <option value="Central African Republic">Central African Republic</option>
              <option value="Chad">Chad</option>
              <option value="Chile">Chile</option>
              <option value="China">China</option>
              <option value="Christmas Island">Christmas Island</option>
              <option value="Cocos (Keeling) Islands">Cocos (Keeling) Islands</option>
              <option value="Colombia">Colombia</option>
              <option value="Comoros">Comoros</option>
              <option value="Congo">Congo</option>
              <option value="Congo, The Democratic Republic of The">Congo, The Democratic Republic of The</option>
              <option value="Cook Islands">Cook Islands</option>
              <option value="Costa Rica">Costa Rica</option>
              <option value="Cote D'ivoire">Cote D'ivoire</option>
              <option value="Croatia">Croatia</option>
              <option value="Cuba">Cuba</option>
              <option value="Cyprus">Cyprus</option>
              <option value="Czech Republic">Czech Republic</option>
              <option value="Denmark">Denmark</option>
              <option value="Djibouti">Djibouti</option>
              <option value="Dominica">Dominica</option>
              <option value="Dominican Republic">Dominican Republic</option>
              <option value="Ecuador">Ecuador</option>
              <option value="Egypt">Egypt</option>
              <option value="El Salvador">El Salvador</option>
              <option value="Equatorial Guinea">Equatorial Guinea</option>
              <option value="Eritrea">Eritrea</option>
              <option value="Estonia">Estonia</option>
              <option value="Ethiopia">Ethiopia</option>
              <option value="Falkland Islands (Malvinas)">Falkland Islands (Malvinas)</option>
              <option value="Faroe Islands">Faroe Islands</option>
              <option value="Fiji">Fiji</option>
              <option value="Finland">Finland</option>
              <option value="France">France</option>
              <option value="French Guiana">French Guiana</option>
              <option value="French Polynesia">French Polynesia</option>
              <option value="French Southern Territories">French Southern Territories</option>
              <option value="Gabon">Gabon</option>
              <option value="Gambia">Gambia</option>
              <option value="Georgia">Georgia</option>
              <option value="Germany">Germany</option>
              <option value="Ghana">Ghana</option>
              <option value="Gibraltar">Gibraltar</option>
              <option value="Greece">Greece</option>
              <option value="Greenland">Greenland</option>
              <option value="Grenada">Grenada</option>
              <option value="Guadeloupe">Guadeloupe</option>
              <option value="Guam">Guam</option>
              <option value="Guatemala">Guatemala</option>
              <option value="Guinea">Guinea</option>
              <option value="Guinea-bissau">Guinea-bissau</option>
              <option value="Guyana">Guyana</option>
              <option value="Haiti">Haiti</option>
              <option value="Heard Island and Mcdonald Islands">Heard Island and Mcdonald Islands</option>
              <option value="Holy See (Vatican City State)">Holy See (Vatican City State)</option>
              <option value="Honduras">Honduras</option>
              <option value="Hong Kong">Hong Kong</option>
              <option value="Hungary">Hungary</option>
              <option value="Iceland">Iceland</option>
              <option value="India">India</option>
              <option value="Indonesia">Indonesia</option>
              <option value="Iran, Islamic Republic of">Iran, Islamic Republic of</option>
              <option value="Iraq">Iraq</option>
              <option value="Ireland">Ireland</option>
              <option value="Israel">Israel</option>
              <option value="Italy">Italy</option>
              <option value="Jamaica">Jamaica</option>
              <option value="Japan">Japan</option>
              <option value="Jordan">Jordan</option>
              <option value="Kazakhstan">Kazakhstan</option>
              <option value="Kenya">Kenya</option>
              <option value="Kiribati">Kiribati</option>
              <option value="Korea, Democratic People's Republic of">Korea, Democratic People's Republic of</option>
              <option value="Korea, Republic of">Korea, Republic of</option>
              <option value="Kuwait">Kuwait</option>
              <option value="Kyrgyzstan">Kyrgyzstan</option>
              <option value="Lao People's Democratic Republic">Lao People's Democratic Republic</option>
              <option value="Latvia">Latvia</option>
              <option value="Lebanon">Lebanon</option>
              <option value="Lesotho">Lesotho</option>
              <option value="Liberia">Liberia</option>
              <option value="Libyan Arab Jamahiriya">Libyan Arab Jamahiriya</option>
              <option value="Liechtenstein">Liechtenstein</option>
              <option value="Lithuania">Lithuania</option>
              <option value="Luxembourg">Luxembourg</option>
              <option value="Macao">Macao</option>
              <option value="Macedonia, The Former Yugoslav Republic of">Macedonia, The Former Yugoslav Republic of</option>
              <option value="Madagascar">Madagascar</option>
              <option value="Malawi">Malawi</option>
              <option value="Malaysia">Malaysia</option>
              <option value="Maldives">Maldives</option>
              <option value="Mali">Mali</option>
              <option value="Malta">Malta</option>
              <option value="Marshall Islands">Marshall Islands</option>
              <option value="Martinique">Martinique</option>
              <option value="Mauritania">Mauritania</option>
              <option value="Mauritius">Mauritius</option>
              <option value="Mayotte">Mayotte</option>
              <option value="Mexico">Mexico</option>
              <option value="Micronesia, Federated States of">Micronesia, Federated States of</option>
              <option value="Moldova, Republic of">Moldova, Republic of</option>
              <option value="Monaco">Monaco</option>
              <option value="Mongolia">Mongolia</option>
              <option value="Montserrat">Montserrat</option>
              <option value="Morocco">Morocco</option>
              <option value="Mozambique">Mozambique</option>
              <option value="Myanmar">Myanmar</option>
              <option value="Namibia">Namibia</option>
              <option value="Nauru">Nauru</option>
              <option value="Nepal">Nepal</option>
              <option value="Netherlands">Netherlands</option>
              <option value="Netherlands Antilles">Netherlands Antilles</option>
              <option value="New Caledonia">New Caledonia</option>
              <option value="New Zealand">New Zealand</option>
              <option value="Nicaragua">Nicaragua</option>
              <option value="Niger">Niger</option>
              <option value="Nigeria">Nigeria</option>
              <option value="Niue">Niue</option>
              <option value="Norfolk Island">Norfolk Island</option>
              <option value="Northern Mariana Islands">Northern Mariana Islands</option>
              <option value="Norway">Norway</option>
              <option value="Oman">Oman</option>
              <option value="Pakistan">Pakistan</option>
              <option value="Palau">Palau</option>
              <option value="Palestinian Territory, Occupied">Palestinian Territory, Occupied</option>
              <option value="Panama">Panama</option>
              <option value="Papua New Guinea">Papua New Guinea</option>
              <option value="Paraguay">Paraguay</option>
              <option value="Peru">Peru</option>
              <option value="Philippines">Philippines</option>
              <option value="Pitcairn">Pitcairn</option>
              <option value="Poland">Poland</option>
              <option value="Portugal">Portugal</option>
              <option value="Puerto Rico">Puerto Rico</option>
              <option value="Qatar">Qatar</option>
              <option value="Reunion">Reunion</option>
              <option value="Romania">Romania</option>
              <option value="Russian Federation">Russian Federation</option>
              <option value="Rwanda">Rwanda</option>
              <option value="Saint Helena">Saint Helena</option>
              <option value="Saint Kitts and Nevis">Saint Kitts and Nevis</option>
              <option value="Saint Lucia">Saint Lucia</option>
              <option value="Saint Pierre and Miquelon">Saint Pierre and Miquelon</option>
              <option value="Saint Vincent and The Grenadines">Saint Vincent and The Grenadines</option>
              <option value="Samoa">Samoa</option>
              <option value="San Marino">San Marino</option>
              <option value="Sao Tome and Principe">Sao Tome and Principe</option>
              <option value="Saudi Arabia">Saudi Arabia</option>
              <option value="Senegal">Senegal</option>
              <option value="Serbia and Montenegro">Serbia and Montenegro</option>
              <option value="Seychelles">Seychelles</option>
              <option value="Sierra Leone">Sierra Leone</option>
              <option value="Singapore">Singapore</option>
              <option value="Slovakia">Slovakia</option>
              <option value="Slovenia">Slovenia</option>
              <option value="Solomon Islands">Solomon Islands</option>
              <option value="Somalia">Somalia</option>
              <option value="South Africa">South Africa</option>
              <option value="South Georgia and The South Sandwich Islands">South Georgia and The South Sandwich Islands</option>
              <option value="Spain">Spain</option>
              <option value="Sri Lanka">Sri Lanka</option>
              <option value="Sudan">Sudan</option>
              <option value="Suriname">Suriname</option>
              <option value="Svalbard and Jan Mayen">Svalbard and Jan Mayen</option>
              <option value="Swaziland">Swaziland</option>
              <option value="Sweden">Sweden</option>
              <option value="Switzerland">Switzerland</option>
              <option value="Syrian Arab Republic">Syrian Arab Republic</option>
              <option value="Taiwan, Province of China">Taiwan, Province of China</option>
              <option value="Tajikistan">Tajikistan</option>
              <option value="Tanzania, United Republic of">Tanzania, United Republic of</option>
              <option value="Thailand">Thailand</option>
              <option value="Timor-leste">Timor-leste</option>
              <option value="Togo">Togo</option>
              <option value="Tokelau">Tokelau</option>
              <option value="Tonga">Tonga</option>
              <option value="Trinidad and Tobago">Trinidad and Tobago</option>
              <option value="Tunisia">Tunisia</option>
              <option value="Turkey">Turkey</option>
              <option value="Turkmenistan">Turkmenistan</option>
              <option value="Turks and Caicos Islands">Turks and Caicos Islands</option>
              <option value="Tuvalu">Tuvalu</option>
              <option value="Uganda">Uganda</option>
              <option value="Ukraine">Ukraine</option>
              <option value="United Arab Emirates">United Arab Emirates</option>
              <option value="United Kingdom">United Kingdom</option>
              <option value="United States">United States</option>
              <option value="United States Minor Outlying Islands">United States Minor Outlying Islands</option>
              <option value="Uruguay">Uruguay</option>
              <option value="Uzbekistan">Uzbekistan</option>
              <option value="Vanuatu">Vanuatu</option>
              <option value="Venezuela">Venezuela</option>
              <option value="Viet Nam">Viet Nam</option>
              <option value="Virgin Islands, British">Virgin Islands, British</option>
              <option value="Virgin Islands, U.S.">Virgin Islands, U.S.</option>
              <option value="Wallis and Futuna">Wallis and Futuna</option>
              <option value="Western Sahara">Western Sahara</option>
              <option value="Yemen">Yemen</option>
              <option value="Zambia">Zambia</option>
              <option value="Zimbabwe">Zimbabwe</option>
            </select>
                      </div>
        </div>
        <div class="form-group">
          <div class="col-sm-10">
            <button type="submit" class="btn btn-default" name="verzenden" style="background-color:#009edf; color: #fff; margin-left: 0px;">Call me!</button>
          </div>
        </div>
      </form>

      </div>
      
    </div>
  </div>
  
  <div class="row" style="padding-top: 30px;padding-bottom: 20px; border-top: 1px solid #ddd;">
    <div class="row">
      <div class="col-sm-9">
        <h2 style="margin-bottom: 40px; color: #333;font-size: 22px; font-family:'Tauri', sans-serif; margin-left: 15px;">What can we do for you?</h2>
        <div class="row" style="padding-left: 15px;">
          <div class="col-sm-4">
            <div style="text-align: center; color: #666"><i class="fa fa-search fa-4x"></i></div>
            <h3 style="text-align:center; line-height:25px; color: #666;">Content Audit</h3>
            <p style="line-height: 22px;">We review your existing content, audience, and supporting infrastructure to determine how you can better use your documentation.</p>
          </div>
          <div class="col-sm-4">
            <div style="text-align: center; color: #666"><i class="fa fa-sitemap fa-4x"></i></div>
            <h3 style="text-align:center; line-height:25px; color: #666;">Content Architecture</h3>
            <p style="line-height: 22px;">We restructure your existing content to make it more readily available to end-users.</p>
          </div>
          <div class="col-sm-4">
            <div style="text-align: center; color: #666"><i class="fa fa-copy fa-4x"></i></div>
            <h3 style="text-align:center;  line-height:25px; color: #666;">Document Creation</h3>
            <p style="line-height: 22px;">We work with you to create new, more efficient documents.</p>
          </div>
        </div>
      
<div class="row" style="padding-left: 15px; padding-top: 50px;">
          <div class="col-sm-4">
            <div style="text-align: center; color: #666;"><i class="fa fa-pencil fa-4x"></i></div>
            <h3 style="text-align:center; line-height:25px; color: #666;">Document Redesign</h3>
            <p style="line-height: 22px;">We apply the Information Mapping&reg; Method to existing documents, improving clarity and usability while providing standardization.</p>
          </div>
          <div class="col-sm-4">
            <div style="text-align: center; color: #666"><i class="fa fa-group fa-4x"></i></div>
            <h3 style="text-align:center; line-height:25px; color: #666;">Mentoring</h3>
            <p style="line-height: 22px;">We work with you to monitor your documentation staff, providing guidance and corrections.</p>
          </div>
          <div class="col-sm-4">
            <div style="text-align: center; color: #666"><i class="fa fa-building fa-4x"></i></div>
            <h3 style="text-align:center;  line-height:25px; color: #666;">For the organization</h3>
            <p style="line-height: 22px;">The entire organization benefits from lower error rates, higher productivity and improved operating efficiency.</p>
          </div>
        </div>
      </div>
      <div class="col-sm-3" id="form"  style="border-left: 1px solid #ddd">
        <div>
          <h2 style="margin-bottom: 25px; color: #333;font-family:'Tauri', sans-serif;font-size: 22px;">Our customers say<br /><br />
          <i class="fa fa-star" style="color: #009edf"></i>
          <i class="fa fa-star" style="color: #009edf"></i>
          <i class="fa fa-star" style="color: #009edf"></i>
          <i class="fa fa-star" style="color: #009edf"></i>
          <i class="fa fa-star" style="color: #009edf"></i>
          </h2>
          <p><i class="fa fa-quote-left"></i> I made it from typist to CEO of a writing company thanks of the use of FS Pro. I recommend it to everyone. <i class="fa fa-quote-right"></i><br />
          <em>Neon - Light Company</em></p>
          <img src="images/laptop_FSPro2013.png" alt="" class="img-responsive" /> </div>
      </div>
    </div>
  </div>
  <div class="row" style="padding-top: 30px; padding-bottom: 30px; border-top: 1px solid #ddd;">
    <div class="col-sm-6 text-center" style="color: #666">
      <p><i class="fa fa-clock-o fa-4x"></i></p>
      <h3>How much time can you save?</h3>
      <form>
        <a href="http://www.informationmapping.com/demo/" target="_blank" class="btn btn-default">Take the time test</a>
      </form>
    </div>
    <div class="col-sm-6 text-center" style="color: #666" id="questions">
      <p><i class="fa fa-question-circle fa-4x"></i></p>
      <h3>Questions?</h3>
      <form>
        <a href="mailto:info@informationmapping.com" class="btn btn-default">Contact one of our experts</a>
      </form>
    </div>
  </div>
  <div class="row" id="footer" style="margin-top: 0px;">
    <div class="col-sm-12" id="footertext">
      <p>&copy; <?php echo date("Y"); ?> Information Mapping International | <a href="http://www.informationmapping.com/">www.informationmapping.com</a> | <a href="tel://003292531425"><em class="fa fa-phone"> </em> +32 9 253 14 25</a> | Contact our office in <a href="http://www.informationmapping.com/us/contact">US</a>, <a href="http://www.informationmapping.com/en/contact-3">Europe</a> or <a href="http://www.informationmapping.com/in/contact-2">Asia</a></p>
      <p class="icons"> <a href="https://twitter.com/infomap"><i class="fa fa-twitter-square fa-2x" style="color: #009edf;"></i></a> <a href="https://www.facebook.com/iminv"><i class="fa fa-facebook-square fa-2x" style="color: #009edf;"></i></a> <a href="http://www.linkedin.com/groups?gid=983927"><i class="fa fa-linkedin-square fa-2x" style="color: #009edf;"></i></a> <a href="http://www.youtube.com/user/InformationMapping"><i class="fa fa-youtube-square fa-2x" style="color: #009edf;"></i></a> </p>
    </div>
  </div>
</div>
<script src="js/bootstrap.min.js"></script> 
<script type="text/javascript">

  var _gaq = _gaq || [];
  _gaq.push(['_setAccount', 'UA-24028581-1']);
  _gaq.push(['_trackPageview']);

  (function() {
    var ga = document.createElement('script'); ga.type = 'text/javascript'; ga.async = true;
    ga.src = ('https:' == document.location.protocol ? 'https://ssl' : 'http://www') + '.google-analytics.com/ga.js';
    var s = document.getElementsByTagName('script')[0]; s.parentNode.insertBefore(ga, s);
  })();

</script> 
<script type="text/javascript">
piAId = '9622';
piCId = '36422';

(function() {
	function async_load(){
		var s = document.createElement('script'); s.type = 'text/javascript';
		s.src = ('https:' == document.location.protocol ? 'https://pi' : 'http://cdn') + '.pardot.com/pd.js';
		var c = document.getElementsByTagName('script')[0]; c.parentNode.insertBefore(s, c);
	}
	if(window.attachEvent) { window.attachEvent('onload', async_load); }
	else { window.addEventListener('load', async_load, false); }
})();
</script> 
<script type="text/javascript">
/* <![CDATA[ */
var google_conversion_id = 963549049;
var google_custom_params = window.google_tag_params;
var google_remarketing_only = true;
/* ]]> */
</script> 
<script type="text/javascript" src="//www.googleadservices.com/pagead/conversion.js">
</script>
<noscript>
<div style="display:inline;"> <img height="1" width="1" style="border-style:none;" alt="" src="//googleads.g.doubleclick.net/pagead/viewthroughconversion/963549049/?value=0&amp;guid=ON&amp;script=0"/> </div>
</noscript>
</body>
</html>