<?php header('X-UA-Compatible: IE=edge,chrome=1'); ?>
<!DOCTYPE html>
<html>
<head>
<meta charset="utf-8">
<title>Create structured and easy-to-read content in MS Word</title>
<meta name="viewport" content="width=device-width, initial-scale=1.0">
<meta name="description" content="Start with FS Pro 2013 and increase your productivity today">
<meta name="keywords" content="writing technical documentation, proposal letter template, technical writing software, template for proposal, business document template, proposal software, writing software, documentation software" />
<link href="css/bootstrap.min.css" rel="stylesheet" media="screen">
<!-- HTML5 shim and Respond.js IE8 support of HTML5 elements and media queries -->
<!--[if lt IE 9]>
      <script src="js/html5shiv.js"></script>
      <script src="js/respond.min.js"></script>
    <![endif]-->
<link href="//fonts.googleapis.com/css?family=Tauri" rel="stylesheet" type="text/css" />
<link href="css/imi.css" rel="stylesheet" media="screen">
<link href="//netdna.bootstrapcdn.com/font-awesome/4.1.0/css/font-awesome.min.css" rel="stylesheet">
</head>
<body>
<div class="container">
  <div class="row">
    <div class="col-sm-9" id="header"> <a href="http://www.informationmapping.com/"><img src="images/logo-informationmapping.jpg" width="169" height="71" alt="Information Mapping"></a></div>
    <div class="col-sm-3" id="header2" style="padding-top: 30px; text-align: right; padding-left: 0px;"> <a href="http://www.informationmapping.com/" target="_blank"><em class="fa fa-globe"> </em> Discover our website</a></div>
  </div>
  <div class="row">
    <div class="col-sm-12" id="toptitle">
      <h1>Create structured and easy-to-read content in MS Word</h1>
    </div>
  </div>
  <div class="row" style="padding-bottom: 30px;">
    <div class="col-sm-9" style="border-right: 1px solid #ddd">
      <div class="row">
        <div class="col-sm-12" style="padding-right: 30px; padding-top: 30px;"> <img src="images/structured-content.jpg" alt="Say goodbye to inconsistent documents" class="img-responsive"/>
          <p style="font-size: 22px; font-family:'Tauri', sans-serif; margin-top: 20px;">FS Pro 2013 is a software tool that adds power to Microsoft Word. It lets you create clear and user-focused meeting minutes, policies and procedures, SOPs, proposals, project plans, manuals and more.</p>
          <p style="margin-top: 25px;margin-bottom: 25px; font-size:16px; line-height: 24px">Our free 30-day trial will show you how you can save time and increase your productivity. <strong>Thousands of people worldwide already love it. Are you the next one?</strong></p>
          <a href="https://informationmapping.box.com/s/vh6fmejvtots8xhpymc6" target="_blank"><img src="images/seeba.png" alt="See Before &amp; After examples" class="img-responsive" /></a> </div>
      </div>
    </div>
    <div class="col-sm-3" id="promo">
      <h2 style="padding-top: 13px;">Discover FS Pro 2013<br />
        </h2>
      <div class="row">
        <div class="col-sm-12">
          <form style="margin-bottom: 20px; margin-top: 20px;" method="post" action="http://www.informationmapping.com/en/shop/checkout/cart/add?product=297&amp;qty=1&amp;bundle_option[325]=476&amp;bundle_option[329]=480&amp;bundle_option[330]=481&amp;bundle_option[326]=851&amp;bundle_option[327]=1096">
            <button type="submit" class="btn btn-default" id="buy"><i class="fa fa-shopping-cart"></i> &nbsp;LET'S TRY IT! </button>
          </form>
          <p style="font-size: 12px; margin-top: -10px; margin-bottom: 20px;">Your discount code: <span style="color: red;">IMSK13</span></p>
        </div>
      </div>
      
      <p>This Kit includes:</p>
      <p style="line-height: 25px;"> <i class="fa fa-check-square-o" style="color: #009edf"></i> FS Pro 2013 software<br />
        <i class="fa fa-check-square-o" style="color: #009edf"></i> Online training<br />
        <i class="fa fa-check-square-o" style="color: #009edf"></i> Video tutorial<br />
        <i class="fa fa-check-square-o" style="color: #009edf"></i> e-Handbook with templates <br />
        <i class="fa fa-check-square-o" style="color: #009edf"></i> Quick Reference Card </p>
    </div>
  </div>
  <div class="row" style="padding-top: 30px;padding-bottom: 20px; border-top: 1px solid #ddd;">

      <div class="row">
      <div class="col-sm-12">
      <h2 style="margin-bottom: 40px; color: #333;font-size: 22px; font-family:'Tauri', sans-serif; margin-left: 15px;">This is how the Starter Kit&#8482; will make you a more consistent writer!</h2>
      </div></div>
      <div class="col-sm-3">
        <div style="text-align: center; color: #666"><i class="fa fa-edit fa-4x"></i></div>
        <h3 style="text-align:center; line-height:25px; color: #666;">Improve your<br />
          writing techniques </h3>
        <p style="line-height: 22px;">Use the interactive online training program and the video tutorials  to join the hundreds of thousands of people worldwide who already create easily understood business documents in less time.</p>
      </div>
      <div class="col-sm-3">
        <div style="text-align: center; color: #666"><i class="fa fa-desktop fa-4x"></i></div>
        <h3 style="text-align:center; line-height:25px; color: #666;">Let the FS&nbsp;Pro&nbsp;2013 software assist you </h3>
        <p style="line-height: 22px;">FS Pro 2013 lets you create clear and user-focused meeting minutes, policies and procedures, SOPs, proposals, project plans, manuals and more in Microsoft Word.</p>
      </div>
      <div class="col-sm-3">
        <div style="text-align: center; color: #666"><i class="fa fa-file-word-o fa-4x"></i></div>
        <h3 style="text-align:center;  line-height:25px; color: #666;">Great templates to jumpstart your writing</h3>
        <p style="line-height: 22px;">With the Starter Kit you'll receive a whole range of typical business document templates to get you started immediately.</p>
      </div>

    <div class="col-sm-3" id="form"  style="border-left: 1px solid #ddd">
      <div>
        <h2 style="margin-bottom: 25px;">Why use it?</h2>
        <p><i class="fa fa-check-square-o" style="color: #009edf"></i> Work up to 2 times faster</p>
        <p><i class="fa fa-check-square-o" style="color: #009edf"></i> Add power to Microsoft Word</p>
        <p><i class="fa fa-check-square-o" style="color: #009edf"></i> Decrease error rates by 54%</p>
        <p><i class="fa fa-check-square-o" style="color: #009edf"></i> Reduce reading time</p>

        <a href="http://www.informationmapping.com/en/shop/checkout/cart/add?product=297&amp;qty=1&amp;bundle_option[325]=476&amp;bundle_option[329]=480&amp;bundle_option[330]=481&amp;bundle_option[326]=851&amp;bundle_option[327]=1096" target="_blank">
        <img src="images/laptop_IMSK2.png" alt="Information Mapping Starter Kit" class="img-responsive" /></a> </div>
    </div>
  </div>
  <div class="row" style="padding-top: 30px; padding-bottom: 30px; border-top: 1px solid #ddd;">
    <div class="col-sm-6 text-center" style="color: #666">
      <p><i class="fa fa-clock-o fa-4x"></i></p>
      <h3>How much time can you save?</h3>
      <form>
        <a href="http://www.informationmapping.com/demo/" target="_blank" class="btn btn-default">Take the time test</a>
      </form>
    </div>
    <div class="col-sm-6 text-center" style="color: #666" id="questions">
      <p><i class="fa fa-question-circle fa-4x"></i></p>
      <h3>Questions?</h3>
      <form>
        <a href="mailto:info@informationmapping.com" class="btn btn-default">Contact one of our experts</a>
      </form>
    </div>
  </div>
  <div class="row" id="footer" style="margin-top: 0px;">
    <div class="col-sm-12" id="footertext">
      <p>&copy; <?php echo date("Y"); ?> Information Mapping International | <a href="http://www.informationmapping.com/">www.informationmapping.com</a> | <a href="tel://003292531425"><em class="fa fa-phone"> </em> +32 9 253 14 25</a> | Contact our office in <a href="http://www.informationmapping.com/us/contact">US</a>, <a href="http://www.informationmapping.com/en/contact-3">Europe</a> or <a href="http://www.informationmapping.com/in/contact-2">Asia</a></p>
      <p class="icons"> <a href="https://twitter.com/infomap"><i class="fa fa-twitter-square fa-2x" style="color: #009edf;"></i></a> <a href="https://www.facebook.com/iminv"><i class="fa fa-facebook-square fa-2x" style="color: #009edf;"></i></a> <a href="http://www.linkedin.com/groups?gid=983927"><i class="fa fa-linkedin-square fa-2x" style="color: #009edf;"></i></a> <a href="http://www.youtube.com/user/InformationMapping"><i class="fa fa-youtube-square fa-2x" style="color: #009edf;"></i></a> </p>
    </div>
  </div>
</div>
<script src="//code.jquery.com/jquery.min.js"></script> 
<script src="js/bootstrap.min.js"></script> 
<script type="text/javascript">

  var _gaq = _gaq || [];
  _gaq.push(['_setAccount', 'UA-24028581-1']);
  _gaq.push(['_trackPageview']);

  (function() {
    var ga = document.createElement('script'); ga.type = 'text/javascript'; ga.async = true;
    ga.src = ('https:' == document.location.protocol ? 'https://ssl' : 'http://www') + '.google-analytics.com/ga.js';
    var s = document.getElementsByTagName('script')[0]; s.parentNode.insertBefore(ga, s);
  })();

</script> 
<script type="text/javascript">
piAId = '9622';
piCId = '36422';

(function() {
	function async_load(){
		var s = document.createElement('script'); s.type = 'text/javascript';
		s.src = ('https:' == document.location.protocol ? 'https://pi' : 'http://cdn') + '.pardot.com/pd.js';
		var c = document.getElementsByTagName('script')[0]; c.parentNode.insertBefore(s, c);
	}
	if(window.attachEvent) { window.attachEvent('onload', async_load); }
	else { window.addEventListener('load', async_load, false); }
})();
</script> 
<script type="text/javascript">
/* <![CDATA[ */
var google_conversion_id = 963549049;
var google_custom_params = window.google_tag_params;
var google_remarketing_only = true;
/* ]]> */
</script> 
<script type="text/javascript" src="//www.googleadservices.com/pagead/conversion.js">
</script>
<noscript>
<div style="display:inline;"> <img height="1" width="1" style="border-style:none;" alt="" src="//googleads.g.doubleclick.net/pagead/viewthroughconversion/963549049/?value=0&amp;guid=ON&amp;script=0"/> </div>
</noscript>
</body>
</html>