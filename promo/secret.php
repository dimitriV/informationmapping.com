<?php header('X-UA-Compatible: IE=edge,chrome=1'); ?>
<!DOCTYPE html>
<html>
<head>
<meta charset="utf-8">
<title>What's the secret to perfect documents?</title>
<meta name="viewport" content="width=device-width, initial-scale=1.0">
<meta name="description" content="Start with FS Pro 2013 and increase your productivity today">
<meta name="keywords" content="writing technical documentation, proposal letter template, technical writing software, template for proposal, business document template, proposal software, writing software, documentation software" />
<link href="css/bootstrap.min.css" rel="stylesheet" media="screen">
<!-- HTML5 shim and Respond.js IE8 support of HTML5 elements and media queries -->
<!--[if lt IE 9]>
      <script src="js/html5shiv.js"></script>
      <script src="js/respond.min.js"></script>
    <![endif]-->
<link href="//fonts.googleapis.com/css?family=Tauri" rel="stylesheet" type="text/css" />
<link href="css/imi.css" rel="stylesheet" media="screen">
<link href="//netdna.bootstrapcdn.com/font-awesome/4.1.0/css/font-awesome.min.css" rel="stylesheet">
<link href="css/simpletextrotator.css" rel="stylesheet">
<script type="text/javascript" src="js/html5lightbox.js"></script>
</head>
<body>
<div class="container">
  <div class="row">
    <div class="col-sm-9" id="header"> <a href="http://www.informationmapping.com/"><img src="images/logo-informationmapping.jpg" width="169" height="71" alt="Information Mapping"></a></div>
    <div class="col-sm-3" id="header2" style="padding-top: 30px; text-align: right; padding-left: 0px;"> <a href="http://www.informationmapping.com/" target="_blank"><em class="fa fa-globe"> </em> Discover our website</a></div>
  </div>
 
  <div class="row" style="padding-bottom: 30px;">
    <div class="col-sm-12">
      <div class="row">
        <div class="col-sm-6" style="padding-right: 30px; padding-top: 30px;"> <img src="images/FSPro-ribbon-reflection-banner-hand.jpg" alt="Say goodbye to inconsistent documents" align="left" class="img-responsive"/ style="margin-right:50px;"></div>
  
  <div class="col-sm-6" style="padding-right: 30px; padding-top: 30px;"> 
          <p style="font-size: 22px; font-family:'Tauri', sans-serif; margin-top: 70px;">The secret to first-class <span class="rotate" style="color:#d4145a;"><strong>meeting minutes?, policies and procedures?, proposals?, manuals?, training materials?</strong></span></p>
          <p style="margin-top: 25px;margin-bottom: 25px; font-family:'Tauri', sans-serif; font-size:16px; line-height: 24px"><strong>We'll tell you. </strong>It starts with  easy-to-use software that adds power to Microsoft Word. In just a few minutes, you'll create   documents that look like <a href="before_after2.html" class="html5lightbox" data-width="900" data-height="550" title="See an example">this</a>.<br>
          </p>
<p>
<div style="display:inline"> <a class="btn btn-primary btn-lg nomargin" href="http://www2.informationmapping.com/fspro2013-trial" role="button" style="background-color:#d4145a; border-color:#d4145a; margin-left:0px; font-family:'Tauri', sans-serif;">Unlock your 30-day trial</a></div>&nbsp;&nbsp;<span style="margin-top: 25px;margin-bottom: 25px; font-family:'Tauri', sans-serif; font-size:16px; line-height: 24px">or</span>&nbsp;&nbsp;
            


<div class="btn-group" >
  <button type="button" class="btn btn-default dropdown-toggle btn-lg" data-toggle="dropdown" style="color:#009edf; font-family:'Tauri', sans-serif;">
    Buy now <span class="caret"></span>
  </button>
  <ul class="dropdown-menu" role="menu">
    <li style="font-family:'Tauri', sans-serif;"><a href="http://www.informationmapping.com/en/shop/checkout/cart/add?product=392&amp;qty=1&amp;bundle_option[545]=773&amp;bundle_option[545]=774&amp;bundle_option[545]=986&amp;bundle_option[545]=775" target="_blank"><i class="fa fa-user" style="color:#009edf;"></i>&nbsp;&nbsp;&nbsp;Buy a license for $125</a></li>
    <li style="font-family:'Tauri', sans-serif;"><a href="http://www.informationmapping.com/en/shop/checkout/cart/add?product=486&amp;qty=1&amp;bundle_option[654]=1010&amp;bundle_option[654]=1011&amp;bundle_option[654]=1012&amp;bundle_option[654]=1009" target="_blank"><i class="fa fa-users" style="color:#009edf;"></i>&nbsp;&nbsp;Buy a 5-Pack and save 10%</a></li>
    <li style="font-family:'Tauri', sans-serif;"><a href="http://www.informationmapping.com/en/shop/checkout/cart/add?product=491&amp;qty=1&amp;bundle_option[655]=1018&amp;bundle_option[655]=1019&amp;bundle_option[655]=1020&amp;bundle_option[655]=1017" target="_blank"><i class="fa fa-users" style="color:#009edf;"></i>&nbsp;&nbsp;Buy a 10-Pack and save 20%</a></li>
  </ul>
</div>
</p>
       
   
   
  </div>
  
  </div></div></div>
  <div class="row" style="padding-top: 30px;padding-bottom: 20px; border-top: 1px solid #ddd;">
  <div class="col-sm-4">
        <div style="text-align: center; color: #666"><i class="fa fa-quote-left fa-4x"></i></div>
      <h3 style="text-align:center; line-height:25px; color: #666;">What our customers say</h3>
        
        <p style="line-height: 22px; text-align:center"><img src="images/susan-ullmann.jpg" align="left" class="img-circle"><i class="fa fa-quote-left" style="color:#009edf;"></i>&nbsp;&nbsp;Thank you for this wonderful application that provides more than structures for communication. People need to know about programs that actually promote clarity.&nbsp;&nbsp;<i class="fa fa-quote-right" style="color:#009edf;"></i><em>&nbsp;&nbsp;</em></p>
        <p style="line-height: 22px; text-align:center"><em>Susan Ullmann - Corporate Instructional Designer, Learning and Development at Immucor</em></p>
<p style="line-height: 22px; text-align:center"><a href="http://www.informationmapping.com/en/resources/cases"><i class="fa fa-external-link" style="color:#009edf;"></i>&nbsp;&nbsp;See how our customers benefit</a></p>
    </div>
    <div class="col-sm-4">
<div style="text-align: center; color: #666"><i class="fa fa-file-video-o fa-4x"></i></div>
        <h3 style="text-align:center; line-height:25px; color: #666;">Discover our video course </h3>
      <p style="line-height: 22px; text-align:center">In 15 modules that average 20 minutes each, a highly experienced Information Mapping® Master Instructor will teach you all the secrets of this great Method.</p>
            <p style="line-height: 22px; text-align:center"><a href="https://www.informationmapping.com/imessentials/#section3" target="_blank"><i class="fa fa-search" style="color:#009edf;"></i>&nbsp;&nbsp;See a free module</a></p>
      </div>
    <div class="col-sm-4">
        <div style="text-align: center; color: #666"><i class="fa fa-gears fa-4x"></i></div>
      <h3 style="text-align:center;  line-height:25px; color: #666;">We're here to assist you</h3>
      <p style="line-height: 22px; text-align:center">From reviewing your content architecture to restructuring your documents or providing support to your writers, we can make your communications truly first class.</p>
    <p style="line-height: 22px; text-align:center">Our team of Certified Consultants is ready to&nbsp;work&nbsp;with&nbsp;you! </p>
        <p style="line-height: 22px; text-align:center"><a href="mailto:info@informationmapping.com"><i class="fa fa-envelope" style="color:#009edf;"></i>&nbsp;&nbsp;Let's talk!</a></p>
      </div>

    
  </div>
  <div class="row" id="footer" style="margin-top: 0px;">
    <div class="col-sm-12" id="footertext">
      <p>&copy; <?php echo date("Y"); ?> Information Mapping International | <a href="http://www.informationmapping.com/">www.informationmapping.com</a> | <em class="fa fa-phone"> </em> +32 9 253 14 25 | Contact our office in <a href="http://www.informationmapping.com/us/contact">US</a>, <a href="http://www.informationmapping.com/en/contact-3">Europe</a> or <a href="http://www.informationmapping.com/in/contact-2">Asia</a></p>
      <p class="icons"> <a href="https://twitter.com/infomap"><i class="fa fa-twitter-square fa-2x" style="color: #009edf;"></i></a> <a href="https://www.facebook.com/iminv"><i class="fa fa-facebook-square fa-2x" style="color: #009edf;"></i></a> <a href="http://www.linkedin.com/groups?gid=983927"><i class="fa fa-linkedin-square fa-2x" style="color: #009edf;"></i></a> <a href="http://www.youtube.com/user/InformationMapping"><i class="fa fa-youtube-square fa-2x" style="color: #009edf;"></i></a> </p>
    </div>
  </div>
</div>
<script src="//code.jquery.com/jquery.min.js"></script> 
<script src="js/bootstrap.min.js"></script> 
<script type="text/javascript">

  var _gaq = _gaq || [];
  _gaq.push(['_setAccount', 'UA-24028581-1']);
  _gaq.push(['_trackPageview']);

  (function() {
    var ga = document.createElement('script'); ga.type = 'text/javascript'; ga.async = true;
    ga.src = ('https:' == document.location.protocol ? 'https://ssl' : 'http://www') + '.google-analytics.com/ga.js';
    var s = document.getElementsByTagName('script')[0]; s.parentNode.insertBefore(ga, s);
  })();

</script> 
<script type="text/javascript">
piAId = '9622';
piCId = '36422';

(function() {
	function async_load(){
		var s = document.createElement('script'); s.type = 'text/javascript';
		s.src = ('https:' == document.location.protocol ? 'https://pi' : 'http://cdn') + '.pardot.com/pd.js';
		var c = document.getElementsByTagName('script')[0]; c.parentNode.insertBefore(s, c);
	}
	if(window.attachEvent) { window.attachEvent('onload', async_load); }
	else { window.addEventListener('load', async_load, false); }
})();
</script> 
<script type="text/javascript">
/* <![CDATA[ */
var google_conversion_id = 963549049;
var google_custom_params = window.google_tag_params;
var google_remarketing_only = true;
/* ]]> */
</script> 
<script type="text/javascript" src="//www.googleadservices.com/pagead/conversion.js">
</script>
 <script src="js/jquery.simple-text-rotator.js"></script>
    
    <script>
    $('#nav').affix({
		  offset: {
			top: $('header').height()
		  }
	});	
	$(".rotate").textrotator({
        animation: "fade",
        separator: ",",
    speed: 1500
    });
	</script>
    <script type="text/javascript" src="js/html5lightbox.js"></script>
<noscript>
<div style="display:inline;"> <img height="1" width="1" style="border-style:none;" alt="" src="//googleads.g.doubleclick.net/pagead/viewthroughconversion/963549049/?value=0&amp;guid=ON&amp;script=0"/> </div>
</noscript>
<!-- begin olark code -->
<script data-cfasync="false" type='text/javascript'>/*<![CDATA[*/window.olark||(function(c){var f=window,d=document,l=f.location.protocol=="https:"?"https:":"http:",z=c.name,r="load";var nt=function(){
f[z]=function(){
(a.s=a.s||[]).push(arguments)};var a=f[z]._={
},q=c.methods.length;while(q--){(function(n){f[z][n]=function(){
f[z]("call",n,arguments)}})(c.methods[q])}a.l=c.loader;a.i=nt;a.p={
0:+new Date};a.P=function(u){
a.p[u]=new Date-a.p[0]};function s(){
a.P(r);f[z](r)}f.addEventListener?f.addEventListener(r,s,false):f.attachEvent("on"+r,s);var ld=function(){function p(hd){
hd="head";return["<",hd,"></",hd,"><",i,' onl' + 'oad="var d=',g,";d.getElementsByTagName('head')[0].",j,"(d.",h,"('script')).",k,"='",l,"//",a.l,"'",'"',"></",i,">"].join("")}var i="body",m=d[i];if(!m){
return setTimeout(ld,100)}a.P(1);var j="appendChild",h="createElement",k="src",n=d[h]("div"),v=n[j](d[h](z)),b=d[h]("iframe"),g="document",e="domain",o;n.style.display="none";m.insertBefore(n,m.firstChild).id=z;b.frameBorder="0";b.id=z+"-loader";if(/MSIE[ ]+6/.test(navigator.userAgent)){
b.src="javascript:false"}b.allowTransparency="true";v[j](b);try{
b.contentWindow[g].open()}catch(w){
c[e]=d[e];o="javascript:var d="+g+".open();d.domain='"+d.domain+"';";b[k]=o+"void(0);"}try{
var t=b.contentWindow[g];t.write(p());t.close()}catch(x){
b[k]=o+'d.write("'+p().replace(/"/g,String.fromCharCode(92)+'"')+'");d.close();'}a.P(2)};ld()};nt()})({
loader: "static.olark.com/jsclient/loader0.js",name:"olark",methods:["configure","extend","declare","identify"]});
/* custom configuration goes here (www.olark.com/documentation) */
olark.identify('3504-626-10-9459');/*]]>*/</script><noscript><a href="https://www.olark.com/site/3504-626-10-9459/contact" title="Contact us" target="_blank">Questions? Feedback?</a> powered by <a href="http://www.olark.com?welcome" title="Olark live chat software">Olark live chat software</a></noscript>
<!-- end olark code -->
</body>
</html>