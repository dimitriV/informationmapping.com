﻿<?php
$db = mysql_connect("localhost","prod_magento","3c8EuuB6RhBHE6rU") or die ("The connection to the database could not be established. Please try again later or inform info@informationmapping.com");
mysql_set_charset('utf8',$db); // added to avoid problems in the SQL DB with e.g. Nestlé
mysql_select_db("imi_prod_magento",$db);

// bij elke aanroep van dit script eerst de tijdelijke tabel legen, daarna opvullen
$legen = mysql_query("TRUNCATE TABLE temp_notactivated");
// de eerste keer runnen voor iedereen
// daarna op de dag van expiry query aanpassen en cron job dagelijks instellen
// first I compared email address of order with email address of activation
// however sometimes it was another email address used for the activation
// therefore I needed to compare license key, if license key in as_customer is present an activation has been done
// at first this wasn't working, the sql query was too heavy due to > 20.000 rows in the tbl license_serialnumbers
// this could be limited by adding l.product_id from this table in the query

$result = mysql_query("
select
i.order_id, i.product_id, i.qty_ordered, 
o.entity_id, o.store_id, o.status, o.customer_email, o.customer_firstname, o.created_at, 
l.serial_number, l.order_id, l.product_id
FROM sales_flat_order_item AS i, sales_flat_order AS o, license_serialnumbers AS l 
WHERE 
o.entity_id = l.order_id && i.order_id = o.entity_id && o.status = 'complete' && (o.store_id = '1' OR o.store_id = '5') && 
i.product_id = l.product_id &&
(i.product_id = '406' 
OR i.product_id = '389'
OR i.product_id = '421'
OR i.product_id = '385'
OR i.product_id = '396'
OR i.product_id = '414'
OR i.product_id = '418'
OR i.product_id = '420'
OR i.product_id = '405'
OR i.product_id = '415'
OR i.product_id = '401'
OR i.product_id = '391'
OR i.product_id = '403'
OR i.product_id = '390'
OR i.product_id = '419'
OR i.product_id = '397'
OR i.product_id = '410'
OR i.product_id = '423'
OR i.product_id = '487'
OR i.product_id = '492'
OR i.product_id = '399'
OR i.product_id = '490'
OR i.product_id = '489'
OR i.product_id = '488'
OR i.product_id = '495'
)

order by i.order_id"); // group by o.customer_email for unique email addresses only deleted here
while($row1 = mysql_fetch_array($result))
{
$t_serial_number = $row1['serial_number'];
$t_firstname = $row1['customer_firstname'];
$t_email = $row1['customer_email'];
$t_created_at = $row1['created_at'];
$t_qty_ordered = $row1['qty_ordered'];
mysql_query("INSERT INTO temp_notactivated (serial_number, email, firstname, created_at, qty) VALUES ('$t_serial_number','$t_email','$t_firstname', '$t_created_at', '$t_qty_ordered')");	
}

sleep(1);

// mailen enkel op vervaldag, ik in BCC, voor iedereen die 1 QTY gekocht heeft
$check = mysql_query("SELECT * FROM temp_notactivated WHERE qty = '1' && created_at = date_add(CURDATE(), INTERVAL -31 DAY) && serial_number NOT IN (SELECT COL2 FROM as_customerinfo) GROUP BY email"); // add group by email to only mail a user once
while($row2 = mysql_fetch_array($check))
{
	$c_serial_number1 = $row2['serial_number'];
	$c_firstname1 = $row2['firstname'];
	$c_email1 = $row2['email'];
	//echo $c_email1;
	//echo $c_serial_number1;

// E-mailadres van de ontvanger
$emailadres = $c_email1; // meerdere e-mailadressen gescheiden tussen komma's
// afzender
$email = "info@informationmapping.com"; 
$bcc = "wvandessel@informationmapping.com";

$headers .= "From: Information Mapping International <$email>\n"; // From must be the same as the domain you are sending from, otherwise will not work
$headers .= "Reply-To: Information Mapping International <$email>\n";
$headers .= "BCC:Information Mapping Administrator<$bcc>\n";
$headers .= "Date: " . date("r") . "\n";

	// onderwerp mail
	$onderwerp = "Activate your FS Pro 2013 software"; 
	
	$message1 = "Hello $c_firstname1\n\n"; 
	$message1 .= "We noticed you purchased FS Pro 2013 a while ago, but have not activated it yet.";
	$message1 .= "You can find the download link at http://www.informationmapping.com/downloads/FSPro2013.zip\n";
	$message1 .= "Your license key is $c_serial_number1 \n\n";  
	$message1 .= "You can learn to use FS Pro 2013 by visiting our online Getting Started Tutorial at http://www.informationmapping.com/fspro2013-tutorial/\n";   
	$message1 .= "If you have any questions, don't hesitate to send us an email. \n\n";
	$message1 .= "With kind regards,\nThe Information Mapping team";
	
	if(mail($emailadres, $onderwerp, $message1, $headers, "-f info@informationmapping.com"))
	{
	echo "Mail sent";	
	}
}

sleep(1);

// mailen enkel op vervaldag, ik in BCC, voor iedereen die > 1 QTY gekocht heeft
// mail after 61 days because it takes more time to roll out volume keys into an organization
/*$check2 = mysql_query("SELECT * FROM temp_notactivated WHERE qty > '1' && created_at = date_add(CURDATE(), INTERVAL -61 DAY) && serial_number NOT IN (SELECT COL2 FROM as_customerinfo) GROUP BY email");
while($row3 = mysql_fetch_array($check2))
{
	$c_serial_number = $row3['serial_number'];
	$c_firstname = $row3['firstname'];
	$c_email = $row3['email'];
	$c_created_at = $row3['created_at'];

// E-mailadres van de ontvanger
$emailadres = $c_email; // meerdere e-mailadressen gescheiden tussen komma's
// afzender
$email = "info@informationmapping.com"; 
$bcc = "wvandessel@informationmapping.com";

$headers .= "From: Information Mapping International <$email>\n"; // From must be the same as the domain you are sending from, otherwise will not work
$headers .= "Reply-To: Information Mapping International <$email>\n";
$headers .= "BCC:Information Mapping Administrator <$bcc>\n";
$headers .= "Date: " . date("r") . "\n";

	// onderwerp mail
	$onderwerp = "Activate your FS Pro 2013 software"; 
	
	$message = "Hello $c_firstname\n\n"; 
	$message .= "We noticed that you purchased multiple FS Pro 2013 licenses on $c_created_at, but not all your licenses have been activated yet.";
	$message .= "You can find the download link of the software at http://www.informationmapping.com/downloads/FSPro2013.zip\n"; 
	$message .= "You can learn to use FS Pro 2013 by visiting our online Getting Started Tutorial at http://www.informationmapping.com/fspro2013-tutorial/\n";   
	$message .= "If you have any questions, don't hesitate to send us an email. \n\n";
	$message .= "With kind regards,\nThe Information Mapping team";
	
	if(mail($emailadres, $onderwerp, $message, $headers, "-f info@informationmapping.com"))
	{
	echo "Mail sent";	
	}
}*/







$me = "wvandessel@informationmapping.com";
$onderwerpme = "CRON reminder_activation";
$headersme = "From: Information Mapping Administrator <info@informationmapping.com>\r\n"; // From must be the same as the domain you are sending from, otherwise will not work
mail($eme, $onderwerpme, "yes", $headersme); 
?>