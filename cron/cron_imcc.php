<?php
$db = mysql_connect("localhost","prod_magento","3c8EuuB6RhBHE6rU") or die ("The connection to the database could not be established. Please try again later or inform info@informationmapping.com");
mysql_set_charset('utf8',$db); // added to avoid problems in the SQL DB with e.g. Nestlé
mysql_select_db("imi_prod_magento",$db) or die("db error");

// afzender
$email = "info@informationmapping.com"; 
$cc = "fvanlerberghe@informationmapping.com";
$bcc = "wvandessel@informationmapping.com";

// Prioriteit: 1 = dringend, 3 = normaal, 5 low
$prioriteit = "1";

// !!!!!! had to remove the \r or it will not sent to Gmail

//$headers  = "MIME-Version: 1.0\r\n";
//$headers .= "Content-type: text/html; charset=iso-8859-1\r\n";	
//$headers .= "Content-type: text/plain; charset=iso-8859-1\r\n";	
$headers .= "From: Information Mapping International <$email>\n"; // From must be the same as the domain you are sending from, otherwise will not work
$headers .= "Reply-To: Information Mapping International <$email>\n";
$headers .= "CC: <$cc>\n";
$headers .= "BCC: <$bcc>\n";
//$headers .= "Organization: Information Mapping International\r\n";
//$headers .= "X-Priority: $prioriteit\r\n"; // 3 voor normale prioriteit  	
$headers .= "Date: " . date("r") . "\n"; 

$shoplink_exam = "https://www.informationmapping.com/en/shop/imcc-certification";


// check if IMCC requestor has bought the IMCC exam after the approval of resume + 14 days
// if yes, do nothing
// if no, send notification mail

$result = mysql_query("SELECT * FROM resume AS r, resumefeedback AS f 
WHERE r.program = 'Information Mapping Certified Consultant Program' && r.resume_id = f.resume_id && f.status = '2' && f.feedbackdate = DATE_ADD(CURDATE(), INTERVAL -14 DAY)");
while($row=mysql_fetch_array($result))
{
	//$feedbackdate = $row['feedbackdate'];
	$emailadres = $row['email'];
	$firstname = $row['firstname'];
	// check for every approved IMCC resume if we could find an order
	$result_order = mysql_query("SELECT * FROM sales_flat_order_item WHERE product_id = '188' && product_options LIKE '%$emailadres%'");// nog uitbreiden naar enkel de completed orders
	$aantal = mysql_num_rows($result_order);
	if($aantal == 0)
	{
	// no order placed, so send reminder notification

		$message = "Hello $firstname,\n\n"; 
		$message .= "Your resume to become an Information Mapping Certified Consultant was approved two weeks ago.\n";  
		$message .= "This is a friendly reminder to proceed with the next step and purchase the IMCC Exam on the Information Mapping Web Store:\n\n";   
		$message .= "$shoplink_exam \n\n"; 
		$message .= "If you have any questions about the certification process, please send me an e-mail. \n\n";
		$message .= "With kind regards,\nFilip Vanlerberghe";
	
		// onderwerp mail
		$onderwerp = "Buy the IMCC Exam to become an Information Mapping Certified Consultant"; 
	
		mail($emailadres, $onderwerp, $message, $headers, "-f info@informationmapping.com");
		
	}
}

sleep(1);

// run cron job that checks if an IMCC candidate which passed the IMCC exam has bought the IMCC license

$shoplink_license = "https://www.informationmapping.com/en/shop/certification/im-certified-consultant/imcc-license";


// check if IMCC candidate has bought the IMCC license after the passing of the exam + 14 days
// if yes, do nothing
// if no, send notification mail

$result = mysql_query("SELECT * FROM resume AS r, resumefeedback AS f 
WHERE r.program = 'Information Mapping Certified Consultant Program' && r.resume_id = f.resume_id && f.exam_passed = '1' && f.exam_passed_date = DATE_ADD(CURDATE(), INTERVAL -14 DAY)");
while($row=mysql_fetch_array($result))
{
	
	$emailadres = $row['email'];
	$firstname = $row['firstname'];
	// check for every approved IMCC exam if we could find an order
	$result_order = mysql_query("SELECT * FROM sales_flat_order_item WHERE product_id = '189' && product_options LIKE '%$emailadres%'");// nog uitbreiden naar enkel de completed orders
	$aantal = mysql_num_rows($result_order);
	if($aantal == 0)
	{
	// no order placed, so send reminder notification

		$message = "Hello $firstname,\n\n"; 
		$message .= "You passed the IMCC Exam to become an Information Mapping Certified Consultant two weeks ago.\n";  
		$message .= "This is a friendly reminder to proceed with the next step and purchase the IMCC License on the Information Mapping Web Store:\n\n";   
		$message .= "$shoplink_license \n\n"; 
		$message .= "If you have any questions about the certification process, please send me an e-mail. \n\n";
		$message .= "With kind regards,\nFilip Vanlerberghe";
	
		// onderwerp mail
		$onderwerp = "Buy the IMCC License to become an Information Mapping Certified Consultant"; 
	
		mail($emailadres, $onderwerp, $message, $headers, "-f info@informationmapping.com");
		
	}
}

sleep(1);

// check if IMCC license has expired after 1 year (is based on exam_passed_date) - send out renewal notification one month earlier
// ook uitsturen bij persoon die status 3 heeft (geen imcc examen nodig)
// mag enkel uitgestuurd worden als de persoon ook de license gekocht heeft

$result = mysql_query("SELECT * FROM resume AS r, resumefeedback AS f 
WHERE r.program = 'Information Mapping Certified Consultant Program' && r.resume_id = f.resume_id && (f.exam_passed = '1' OR f.exam_passed = '3') && f.exam_passed_date = DATE_ADD(CURDATE(), INTERVAL -335 DAY)");
while($row=mysql_fetch_array($result))
{
	
	$emailadres = $row['email'];
	$firstname = $row['firstname'];
	$renew = $row['exam_passed_date'];
	$date_part = explode("-", $renew);
	$year = $date_part[0];
	$month = $date_part[1];
	$day = $date_part[2];
	$year = $date_part[0]+1;
	// send email to expired license
	$result_order = mysql_query("SELECT * FROM sales_flat_order_item WHERE product_id = '189' && product_options LIKE '%$emailadres%'");// nog uitbreiden naar enkel de completed orders
	$aantal = mysql_num_rows($result_order);
	if($aantal > 0)
	{

		$message = "Hello $firstname,\n\n"; 
		$message .= "Your IMCC License will expire in one month. Please renew your license by $year-$month-$day to continue to provide Information Mapping services to your customers.\n";  
		$message .= "You can renew the IMCC License on the Information Mapping Web Store:\n\n";   
		$message .= "$shoplink_license \n\n"; 
		$message .= "If you have any questions about the certification process, please send me an e-mail. \n\n";
		$message .= "With kind regards,\nFilip Vanlerberghe";
	
		// onderwerp mail
		$onderwerp = "Renew your IMCC License"; 
	
		mail($emailadres, $onderwerp, $message, $headers, "-f info@informationmapping.com");
	}
}
sleep(1);

// RUN CRON that checks if an order was placed today for an IMCC license, then Tom needs to be alerted to create an IMCC Kit
// SELECT * FROM `sales_flat_order` WHERE DATE(created_at)= CURDATE()

$result = mysql_query("SELECT * FROM resume AS r, resumefeedback AS f 
WHERE r.program = 'Information Mapping Certified Consultant Program' && r.resume_id = f.resume_id && (f.exam_passed = '1' OR f.exam_passed = '3')");
while($row=mysql_fetch_array($result))
{
	
	$firstname = $row['firstname'];
	$lastname = $row['lastname'];
	$email = $row['email'];
	$address1 = $row['address1'];
	$city = $row['city'];
	$postalcode = $row['postalcode'];
	$state = $row['state'];
	$country = $row['country'];
	$start_certification = $row['exam_passed_date'];

$result_b = mysql_query("SELECT * FROM sales_flat_order_item AS it, sales_flat_order AS ord WHERE it.order_id = ord.entity_id && ord.status = 'complete' && it.product_id = '189' 
&& product_options LIKE '%$email%'
&& DATE(it.created_at) = CURDATE()"); // select imcc license orders placed today, if registered email for order and IMCC app is not the same, nothing will be send out
$aantal_b = mysql_num_rows($result_b);
	if($aantal_b > 0)
	{
		$message = "Hello Tom,\n\n"; 
		$message .= "An IMCC License was bought today.\n";  
		$message .= "Please prepare an IMCC Kit.\n";  
		$message .= "First name: $firstname\n"; 
		$message .= "Last name: $lastname\n"; 
		$message .= "Email: $email\n"; 
		$message .= "Address: $address1\n"; 
		$message .= "City: $city\n"; 
		$message .= "Postal code: $postalcode\n"; 
		$message .= "State: $state\n"; 
		$message .= "Country: $country\n"; 
		$message .= "Start certification (yyyy-mm-dd): $start_certification\n"; 
		$message .= "With kind regards,\nInformation Mapping Administrator";
	
		// onderwerp mail
		$onderwerp = "IMCC Kit Needed"; 
	
	$emailadres = "tvandervennet@informationmapping.com";
		mail($emailadres, $onderwerp, $message, $headers, "-f info@informationmapping.com");
	}
}
?>