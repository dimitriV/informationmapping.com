<?php
$rootdir="";
$page="Dash";
include_once("includes/connect.php");
include("includes/header.php");
?>
<body>
<script>
    $(document).ready(function() {
        $( "#exp" ).click(function() {
            $("#sub").submit();
        });
    });
</script>
<script type="text/javascript">
    function checkSubmit(e)
    {
        if(e && e.keyCode == 13)
        {
            document.forms[0].submit();
        }
    }
</script>
<div class="bg-light" id="wrap">
    <div id="top">
        <!-- .navbar -->
        <?php include("includes/navtop.php"); ?>
    </div><!-- /#top -->
    <?php include("includes/navleft.php"); ?>
    <div id="content">
        <div class="outer">
            <div class="inner">
                <div class="inner-head">
                    <h1>Dashboard <br>
                        <div class="smalls">Welcome to IMI CRM!</div></h1>
                </div>
                <?php
                if (!isset($_SESSION["username"])){
                    echo $niet_ingelogd;
                }
                elseif($permission != "1"){
                    echo $geen_toegang;
                }else{
                    ?>
                <div class="row">
                    <div class="container">
                        <h1>Welcome to the IMI Webapp</h1>
                    <p class="well">You can use this Application do replace license keys or add License Manager records or even extend eLearning access.</p>
                        </div>
                    </div>
                <?php }?>
            </div><!-- /.inner -->
        </div><!-- /.outer -->
    </div><!-- /#content -->
    <?php include("includes/footer.php"); ?>
</body>
</html>