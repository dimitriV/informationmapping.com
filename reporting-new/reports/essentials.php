<?php
$rootdir="../";
$page="REssential";
include_once($rootdir."includes/connect.php");
include($rootdir."includes/header.php");
?>
<?php
// Requested file
// Could also be e.g. 'currencies.json' or 'historical/2011-01-01.json'
$file = 'latest.json';
$appId = 'aa5359fe3548463391803f83e0bfe119';

// Open CURL session:
$ch = curl_init("https://openexchangerates.org/api/{$file}?app_id={$appId}");
curl_setopt($ch, CURLOPT_RETURNTRANSFER, 1);

// Get the data:
$json = curl_exec($ch);
curl_close($ch);

// Decode JSON response:
$exchangeRates = json_decode($json);

// You can now access the rates inside the parsed object, like so:
/*printf(
    "1 %s in EUR: %s (as of %s)",
    $exchangeRates->base,
    $exchangeRates->rates->EUR,
    date('H:i jS F, Y', $exchangeRates->timestamp)
);
*/
$convertrate= $exchangeRates->rates->EUR;
?>
<body>
<div class="bg-light" id="wrap">
    <div id="top">
        <!-- .navbar -->
        <?php include($rootdir."includes/navtop.php"); ?>
    </div><!-- /#top -->
    <?php include($rootdir."includes/navleft.php"); ?>
    <div id="content">
        <div class="outer">
            <div class="inner">
                <div class="inner-head">
                    <h2>Reports</h2>
                </div>
                <?php
                if (!isset($_SESSION["username"])){
                    echo $niet_ingelogd;
                }
                elseif($permission != "1"){
                    echo $geen_toegang;
                }else{
                ?>
                <div class="row">
                    <div class="col-md-12">
                        <div class="panel panel-content panel-default">
                            <div class="panel-heading">
                                <h3 class="panel-title">Video Course Sales<span class="label label-info"></span></h3>
                            </div>
                            <div class="panel-body">
                                <?php
                                function replace_store($code)
                                {
                                    if($code == "17") {$storeview = str_replace("17", "Partner USD", $code); }
                                    if($code == "5") { $storeview = str_replace("5", "US Store view", $code); }
                                    if($code == "1") { $storeview = str_replace("1", "EUR Store view", $code); }
                                    if($code == "16") { $storeview = str_replace("16", "Partner EUR", $code); }

                                    return $storeview;
                                }


                                function replace_amounts($nummerke)
                                {

                                    $nummer = number_format($nummerke, 2, ',', ''); // afgerond op 2 cijfers na de komma
                                    return $nummer;
                                }
                                ?>

                                </p>
                                <table class="table table-bordered table-condensed table-hover table-striped" id="dataTables-video">
                                    <thead>
                                    <tr>
                                        <th>Order #</th>
                                        <th>Store</th>
                                        <th>Customer Email</th>
                                        <th>Order Date</th>
                                        <th>Product</th>
                                        <th>Qty</th>
                                        <th>Discount</th>
                                        <th>Coupon</th>
                                        <th>Invoiced</th>
                                    </tr>
                                    </thead>
                                    <tfoot>
                                    <tr>
                                        <td></td>
                                        <td></td>
                                        <td></td>
                                        <td></td>
                                        <td></td>
                                        <td></td>
                                        <td></td>
                                        <td></td>
                                        <td></td>
                                    </tr>
                                    </tfoot>
                                    <tbody>
                                    <?php
                                    $grandtotal = 0;
                                    $query2_result = $db->query("SELECT * FROM imi_prod_magento.elearning_slot as s
                                                                            left join sales_flat_order as o on o.entity_id=s.order_id
                                                                            left join sales_flat_order_item as i on i.order_id=o.entity_id
                                                                            where sku like 'roy-informationmapping-essentials' and total_paid > 3

                                                                            order by s.id desc;");

                                    while($report = $query2_result->fetch_array()){
//                                        $ordernummer = $report['order_id'];
//
//                                        $res = $db->query("SELECT increment_id, coupon_code, imi_customer_email FROM sales_flat_order WHERE entity_id = '$ordernummer'");
//                                        $row = $res->fetch_assoc();
                                        echo "<tr>";
                                        echo "<td>" . $report['increment_id'] . "</td>";
                                        echo "<td>" . replace_store($report['store_id']) . "</td>";
                                        echo "<td>" . $report['imi_customer_email'] . "</td>";
                                        echo "<td>" . $report['created_at'] . "</td>";
                                        echo "<td>" . $report['name'] . "</td>";
                                        echo "<td>" . round($report['qty_invoiced'],0) . "</td>";
                                        echo "<td>" . round($report['discount_amount'],2) . "</td>";
                                        echo "<td>" . $report['coupon_code'] . "</td>";
                                        if($report['store_id'] == "17" || $report['store_id'] == "5")
                                        {
                                            echo "<td> €";
                                            $amount = replace_amounts(($report['base_row_total_incl_tax']- $report['discount_amount'])*$convertrate) ;
                                            echo $amount;
                                            echo "</td>";
                                        }
                                        else
                                        {
                                            echo "<td> €";
                                            $amount = replace_amounts(($report['base_row_total_incl_tax']- $report['discount_amount'])) ;
                                            echo $amount;
                                            echo "</td>";
                                        }
                                        echo "</tr>";
                                    }
                                    ?>
                                    </tbody>
                                </table>
                            </div>
                        </div>
                    </div>
                    <script>
                        $(document).ready(function() {

                            $('#dataTables-video').dataTable({
                                "pageLength": 10,
                                "order": [ 3    , 'desc' ],
                                "columnDefs": [
                                    { "width": "20%", "targets": 2 }
                                ],

                                "footerCallback": function ( row, data, start, end, display ) {
                                    var api = this.api(), data;

                                    // Remove the formatting to get integer data for summation
                                    var intVal = function ( i ) {
                                        return typeof i === 'string' ?
                                        i.replace(/[\€,]/g, '')/100 :
                                            typeof i === 'number' ?
                                                i : 0;
                                    };

                                    // Total over all pages
                                    total = api
                                        .column( 8 )
                                        .data()
                                        .reduce( function (a, b) {
                                            return intVal(a) + intVal(b);
                                        } );

                                    // Total over this page
                                    pageTotal = api
                                        .column( 8, { page: 'current'} )
                                        .data()
                                        .reduce( function (a, b) {
                                            return intVal(a) + intVal(b);
                                        }, 0 );


                                    // Update footer
                                    $( api.column( 8 ).footer() ).html(
                                        '<b>Page Total: €'+ pageTotal.toFixed(2) +'</b>'
                                    );
                                    $( api.column( 0 ).footer() ).html(
                                        '<b>Grand Total: €'+ total.toFixed(2) +'</b>'
                                    );
                                }

                            });
                        });
                    </script>
                    <?php }?>
                </div><!-- /.inner -->
            </div><!-- /.outer -->
        </div><!-- /#content -->
        <?php include($rootdir."includes/footer.php"); ?>
</body>
</html>