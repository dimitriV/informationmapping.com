<?php
$rootdir="../";
$page="LReplace";
include_once($rootdir."includes/connect.php");
include($rootdir."includes/header.php");
?>
<body>
<div class="bg-light" id="wrap">
    <div id="top">
        <!-- .navbar -->
        <?php include($rootdir."includes/navtop.php"); ?>
    </div><!-- /#top -->
    <?php include($rootdir."includes/navleft.php"); ?>
    <div id="content">
        <div class="outer">
            <div class="inner">
                <div class="inner-head">
                    <h2>License Overview</h2>
                </div>
                <?php
                if (!isset($_SESSION["username"])){
                    echo $niet_ingelogd;
                }
                elseif($permission != "1"){
                    echo $geen_toegang;
                }else{
                ?>
                <div class="row">
                    <div class="col-md-10">
                        <div class="panel panel-content panel-default">
                            <div class="panel-heading">
                                <h3 class="panel-title">Replace License Key<span class="label label-info"></span></h3>
                            </div>
                            <div class="panel-body">
                                <form class="form-horizontal" method="post" role="form">
                                    <div class="form-group">
                                        <label for="inputLicense" class="col-sm-2 control-label">Old license key:</label>
                                        <div class="col-sm-10">
                                            <input type="text" class="form-control" name="oldlickey" id="inputLicenseold" placeholder="All FS Pro keys">
                                        </div>
                                    </div>
                                    <div class="form-group">
                                        <label for="inputLicense" class="col-sm-2 control-label">New license key:</label>
                                        <div class="col-sm-10">
                                            <input type="text" class="form-control" name="newlickey" id="inputLicensenew" placeholder="All FS Pro keys">
                                        </div>
                                    </div>
                                    <div class="form-group">
                                        <div class="col-sm-offset-2 col-sm-10">
                                            <button type="submit" name="verzenden" value="Save" class="btn btn-primary">Replace</button>
                                        </div>
                                    </div>
                                </form>

                                <?php

                                // HIERONDER JUIST ZOEKSCRIPT VOOR MULTIPLE ZOEKWAARDEN

                                $oldlickey = isset($_POST['oldlickey']) ? trim($_POST['oldlickey']) : '';
                                $newlickey = isset($_POST['newlickey']) ? trim($_POST['newlickey']) : '';
                                $verzenden = isset($_POST['verzenden']) ? trim($_POST['verzenden']) : '';


                                if($verzenden) {
                                    if (empty($oldlickey))
                                        $error .= "<li>You haven't entered an old license key.</li>\n";

                                    if (empty($newlickey))
                                        $error .= "<li>You haven't entered a new license key.</li>\n";

                                    // read if key exists in shop
                                    $resultres_vol = $db->query("SELECT serial FROM license_pool WHERE serial like '%$oldlickey%'");
                                    $res_vol =  $resultres_vol->num_rows;
                                    if($res_vol < 1)
                                        $error .= "<li>This key does not exist in the shop database.</li>\n";

                                    if(isset($error))
                                    {
                                        // Afdrukken van de errors
                                        echo "<div class='well'> <p><strong>Your action was not successful:</strong></p>";
                                        echo "<ul>";
                                        echo $error;
                                        echo "</ul></div>";

                                    }
                                    else
                                    {

                                        $result = $db->query("UPDATE license_pool SET serial = '$newlickey' WHERE serial like '%$oldlickey%'");
                                        if($result) { echo "<p>License Pool updated sucessfully</p>"; } else { echo "<p>Error: Query 1 not saved successfully (tbl license_pool)</p>";  }

                                        $result2 = $db->query("UPDATE subscriptions_subscription SET serial = '$newlickey' WHERE serial LIKE '%$oldlickey%'");
                                        if($result2) { echo "<p>Key is a subscription key, subscription updated succesfully</p>"; } else { echo "<p>Key is not a subscription key, see renewal key message</p>";  }

                                        $result3 = $db->query("UPDATE subscriptions_renewal SET serial = '$newlickey' WHERE serial LIKE '%$oldlickey%'");
                                        if($result3) { echo "<p>Key is a renewal key, renewal updated succesfully</p>"; } else { echo "<p>Key is not a renewal key, see subscription key message</p>";  }
                                    }
                                }
                                ?>
                            </div>
                        </div>
                    </div>
                    <?php }?>
                </div><!-- /.inner -->
            </div><!-- /.outer -->
        </div><!-- /#content -->
        <?php include($rootdir."includes/footer.php"); ?>
</body>
</html>