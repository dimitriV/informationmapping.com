<?php
$rootdir="../";
$page="LAdd";
include_once($rootdir."includes/connect.php");
include($rootdir."includes/header.php");
?>
<body>
<div class="bg-light" id="wrap">
    <div id="top">
        <!-- .navbar -->
        <?php include($rootdir."includes/navtop.php"); ?>
    </div><!-- /#top -->
    <?php include($rootdir."includes/navleft.php"); ?>
    <div id="content">
        <div class="outer">
            <div class="inner">
                <div class="inner-head">
                    <h2>License Overview</h2>
                </div>
                <?php
                if (!isset($_SESSION["username"])){
                    echo $niet_ingelogd;
                }
                elseif($permission != "1"){
                    echo $geen_toegang;
                }else{
                ?>
                <div class="row">
                    <div class="col-md-10">
                        <div class="panel panel-content panel-default">
                            <div class="panel-heading">
                                <h3 class="panel-title">Add Volume key to web store order<span class="label label-info"></span></h3>
                            </div>
                            <div class="panel-body">
                                <form class="form-horizontal" method="post" role="form">
                                    <div class="form-group">
                                        <label for="inputLicense" class="col-sm-2 control-label">License key:</label>
                                        <div class="col-sm-6">
                                            <input type="text" class="form-control" name="lickey" placeholder="All FS Pro keys">
                                        </div>
                                    </div>
                                    <div class="form-group">
                                        <label class="col-sm-2 control-label">Order #:</label>
                                        <div class="col-sm-6">
                                            <input type="text" class="form-control" name="orderid">
                                        </div>
                                    </div>
                                    <div class="form-group">
                                        <label class="col-sm-2 control-label">Product:</label>
                                        <div class="col-sm-6">
                                            <select class="form-control" name="productid">
                                                <option value="401">FS Pro 2013 1 Year volume license</option>
                                                <option value="402">FS Pro 2013 2 Year volume license</option>
                                                <option value="403">FS Pro 2013 3 Year volume license</option>
                                                <option value="405">FS Pro 2013 Perpetual volume license</option>
                                                <option value="421">FS Pro 2013 Migrate to 1 year volume license</option>
                                                <option value="422">FS Pro 2013 Migrate to 2 Year volume license</option>
                                                <option value="423">FS Pro 2013 Migrate to 3 Year volume license</option>
                                            </select>
                                        </div>
                                    </div>
                                    <div class="form-group">
                                        <div class="col-sm-offset-2 col-sm-6">
                                            <button type="submit" name="verzenden" value="Save" class="btn btn-primary">Replace</button>
                                        </div>
                                    </div>
                                </form>
                                <?php
                                // HIERONDER JUIST ZOEKSCRIPT VOOR MULTIPLE ZOEKWAARDEN

                                $lickey = isset($_POST['lickey']) ? $_POST['lickey'] : '';
                                $orderid = isset($_POST['orderid']) ? $_POST['orderid'] : '';
                                $productid = isset($_POST['productid']) ? $_POST['productid'] : '';
                                $verzenden = isset($_POST['verzenden']) ? $_POST['verzenden'] : '';

                                if($verzenden)
                                {
                                    if(empty($lickey))
                                        $error .= "<li>You haven't entered a license key.</li>\n";

                                    if(empty($orderid))
                                        $error .= "<li>You haven't entered an order id.</li>\n";

                                    if(strlen($orderid) < 1)
                                        $error .= "<li>You haven't entered a valid increment_id. Example of valid input format: 1700002267</li>\n";

                                    if(empty($productid))
                                        $error .= "<li>You haven't entered a product id.</li>\n";


                                    // read license_id
                                    $res_voll = $db->query("SELECT item_id FROM sales_flat_order_item WHERE product_id=".$productid." AND order_id =".$orderid);
                                    $row_l = $res_voll->fetch_assoc();
                                    $order_item_id = $row_l['item_id'];


                                    if(isset($error))
                                    {
                                        // Afdrukken van de errors
                                        echo "<div class='well'><strong>Your action was not successful:</strong>";
                                        echo "<ul>";
                                        echo $error;
                                        echo "</ul></div>";

                                    }
                                    else
                                    {

                                        $result = $db->query("INSERT INTO license_pool SET serial = '$lickey', type='license', product_id='$productid' ");
                                        if($result) { echo "<p>Saved query 1 successfully (tbl license_pool)</p>"; } else { echo "<p>Error: Query 1 not saved successfully (tbl license_pool)</p>";  }

                                        // read pool_id
                                        $res = $db->query("SELECT id FROM license_pool WHERE serial =".$lickey);
                                        $row_l = $res_voll->fetch_assoc();
                                        $pool_id = $row_l['id'];

                                        $result = $db->query("UPDATE subscriptions_subscription SET serial = '$lickey', poold_id='$pool_id' WHERE order_item_id = '$order_item_id'");
                                        if($result) { echo "<p>Saved query 1 successfully (tbl subscriptions_subscription)</p>"; } else { echo "<p>Error: Query 1 not saved successfully (tbl subscriptions_subscription)</p>";  }


                                    }
                                }
                                ?>
                                <?php include($rootdir."includes/search_key.php"); ?>
                            </div>
                        </div>
                    </div>
                    <?php }?>
                </div><!-- /.inner -->
            </div><!-- /.outer -->
        </div><!-- /#content -->
        <?php include($rootdir."includes/footer.php"); ?>
</body>
</html>