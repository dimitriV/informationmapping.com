<?php
$rootdir="../";
$page="LServerEdit";
include_once($rootdir."includes/connect.php");
include($rootdir."includes/header.php");
?>
<body>
<div class="bg-light" id="wrap">
    <div id="top">
        <!-- .navbar -->
        <?php include($rootdir."includes/navtop.php"); ?>
    </div><!-- /#top -->
    <?php include($rootdir."includes/navleft.php"); ?>
    <div id="content">
        <div class="outer">
            <div class="inner">
                <div class="inner-head">
                    <h2>Edit License Server:</h2>
                </div>
                <?php
                if (!isset($_SESSION["username"])){
                    echo $niet_ingelogd;
                }
                elseif($permission != "1"){
                    echo $geen_toegang;
                }else{
                ?>
                <div class="row">
                    <div class="col-md-6">
                        <div class="panel panel-default">
                            <div class="panel-body">
                                <?php
                                $id = is_numeric($_GET['id']) ? $_GET['id'] : '';
                                $verzenden = isset($_POST['verzenden']) ? $_POST['verzenden'] : '';


                                $company = isset($_POST['company']) ? $_POST['company'] : '';
                                $contactEmail = isset($_POST['contactEmail']) ? $_POST['contactEmail'] : '';
                                $licenses = isset($_POST['licenses']) ? $_POST['licenses'] : '';
                                $serverKey = isset($_POST['serverKey']) ? $_POST['serverKey'] : '';
                                $expiryDate = isset($_POST['expiryDate']) ? $_POST['expiryDate'] : '';
                                $comment = isset($_POST['comment']) ? $_POST['comment'] : '';
                                $date = date_create();
                                $timestamp=date_format($date, "Y-m-d");



                                if($verzenden)
                                {
                                    $query= $db->query("UPDATE reporting_license_server SET company='$company', contact_email='$contactEmail', number_licenses='$licenses', server_key='$serverKey', expiry_date='$expiryDate', comment='$comment', modified_at=NOW() WHERE id=$id;");
                                    ?>
                                    <form action="#" method="post">
                                        <div class="form-group">
                                            <div class="panel panel-default">
                                                <div class="panel-heading">
                                                    <h3 class="panel-title">Edit License Server Details.</h3>
                                                </div>
                                                <div class="panel-body">
                                                    License server saved successfully.
                                                </div>
                                            </div>
                                        </div>
                                        <button type="button" class="btn btn-success" onclick="location.href='license_server.php'">Back</button>
                                    </form>
                                <?php }
                                else
                                {
                                    ?>

                                    <form action="#" method="post">

                                        <div class="form-group">
                                            <label for="LabelForInputCompany">Company name:</label>
                                            <?php
                                            $result = $db->query("SELECT * FROM reporting_license_server where id = '$id'");
                                            $row = $result->fetch_assoc(); ?>
                                            <input name="company" class="form-control" value="<?php echo $row['company'];?>">
                                        </div>
                                        <div class="form-group">
                                            <label for="LabelForInputIndustry">Contact Email address:</label>
                                            <input name="contactEmail" class="form-control" value="<?php echo $row['contact_email'];?>">
                                        </div>
                                        <div class="form-group">
                                            <label for="LabelForInputIndustry">Number of licenses:</label>
                                            <input name="licenses" class="form-control" value="<?php echo $row['number_licenses'];?>">
                                        </div>
                                        <div class="form-group">
                                            <label for="LabelForInputIndustry">Server Key:</label>
                                            <input name="serverKey" class="form-control" value="<?php echo $row['server_key'];?>">
                                        </div>
                                        <div class="form-group">
                                            <label for="LabelForInputIndustry">Expiry Date:</label>
                                            <input name="expiryDate" class="form-control" value="<?php echo $row['expiry_date'];?>">
                                        </div>
                                        <div class="form-group">
                                            <label for="LabelForInputExtra">Enter your comment (max. 300 characters):</label>
                                            <textarea name="comment" class="form-control" rows="5"><?php
                                                $result = $db->query("SELECT comment FROM reporting_license_server
                                                                         WHERE id = '$id'");
                                                $row = $result->fetch_assoc();
                                                echo $row['comment'];?></textarea>
                                        </div>
                                        <button type="submit" class="btn btn-primary" name="verzenden" value="Save">Save</button>
                                        <button type="button" class="btn btn-default" onclick="location.href='license_server.php'">Back</button>

                                    </form>
                                <?php
                                }
                                }?>
                            </div>
                        </div>
                    </div>
                </div><!-- /.inner -->
            </div><!-- /.outer -->
        </div><!-- /#content -->
        <?php include($rootdir."includes/footer.php"); ?>
</body>
</html>