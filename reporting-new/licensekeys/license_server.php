<?php
$rootdir="../";
$page="LServer";
include_once($rootdir."includes/connect.php");
include($rootdir."includes/header.php");
?>
<body>
<div class="bg-light" id="wrap">
    <div id="top">
        <!-- .navbar -->
        <?php include($rootdir."includes/navtop.php"); ?>
    </div><!-- /#top -->
    <?php include($rootdir."includes/navleft.php"); ?>
    <div id="content">
        <div class="outer">
            <div class="inner">
                <div class="inner-head">
                    <h2>License Manager:</h2>
                </div>
                <?php
                if (!isset($_SESSION["username"])){
                    echo $niet_ingelogd;
                }
                elseif($permission != "1"){
                    echo $geen_toegang;
                }else{
                ?>
                <div class="row">
                    <div class="col-lg-12">
                        <div class="box">
                            <header>
                                <div class="icons">
                                    <i class="fa fa-users"></i>
                                </div>
                                <h5>License Manager Overview:</h5>
                                <div><a href="license_server_create.php" class="add pull-right">Add License Manager <i class="fa fa-plus-square fa-lg"></i></a></div>
                            </header>
                            <div id="collapse4" class="body">
                                <table class="table table-bordered table-condensed table-hover table-striped" id="dataTables-license-server">
                                    <thead>
                                    <tr>
                                        <th>Lic.ID</th>
                                        <th>Company</th>
                                        <th>Contact Email</th>
                                        <th>Number of Licenses</th>
                                        <th>Server Key</th>
                                        <th>Expiry Date</th>
                                        <th>Comment</th>
                                    </tr>
                                    </thead>
                                    <tbody> <?php
                                    $query = $db->query("SELECT * FROM reporting_license_server where active=1");

                                    while($row = $query->fetch_array())
                                    {
                                        echo "<tr>";
                                        echo "<td>" . $row['id'] . "</a></td>";
                                        echo "<td><a href=\"license_server_edit.php?id=" . $row['id'] . "\" >" . $row['company'] . "</a></td>";
                                        //  echo "<td><a class=\"pull-right\" target=\"_blank\" href=\"https://connect.data.com/search#p%3Dsearchresult%3B%3Bss%3Dsingleboxsearch%3B%3Bq%3D%7B%22freeText%22%3A%22".$row['name']."%22%7D\"><img src=\"../assets/img/datacom.png\" alt=\"Datacom\" height=\"16\" width=\"16\" ></a></td>";
                                        echo "<td>" . $row['contact_email'] . "</td>";
                                        //   echo "<td>" . $row['code'] . "</td>";
                                        echo "<td>" . $row['number_licenses'] . "</td>";
                                        echo "<td>" . $row['server_key'] . "</td>";
                                        echo "<td>" . $row['expiry_date'] . "</td>";
                                        echo "<td>" . $row['comment'] . "</td>";
                                        echo "</tr>";
                                    }
                                    ?>
                                    </tbody>
                                </table>
                            </div>
                        </div>
                    </div>
                    <?php }?>
                </div><!-- /.inner -->
            </div><!-- /.outer -->
        </div><!-- /#content -->
        <script>
            $(document).ready(function() {
                $('#dataTables-license-server').dataTable({
                    "pageLength": 15,
                    "order": [[ 1, "asc" ],[ 0, "desc" ]],
                    "columnDefs": [
                        {
                            "targets": [ 0 ],
                            "width": "5%",
                            "visible": true
                        }
                    ]
                });
            });
        </script>
        <?php include($rootdir."includes/footer.php"); ?>
</body>
</html>