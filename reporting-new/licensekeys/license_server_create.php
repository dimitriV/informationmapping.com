<?php
$rootdir="../";
$page="LServerCreate";
include_once($rootdir."includes/connect.php");
include($rootdir."includes/header.php");
?>
<body>
<div class="bg-light" id="wrap">
    <div id="top">
        <!-- .navbar -->
        <?php include($rootdir."includes/navtop.php"); ?>
    </div><!-- /#top -->
    <?php include($rootdir."includes/navleft.php"); ?>
    <div id="content">
        <div class="outer">
            <div class="inner">
                <div class="inner-head">
                    <h2>License server:</h2>
                </div>
                <?php
                if (!isset($_SESSION["username"])){
                    echo $niet_ingelogd;
                }
                elseif($permission != "1"){
                    echo $geen_toegang;
                }else{
                ?>
                <div class="row">
                    <div class="col-md-6">
                        <div class="panel panel-default">
                            <div class="panel-body">
                                <?php

                                $verzenden = isset($_POST['verzenden']) ? $_POST['verzenden'] : '';


                                $company = isset($_POST['company']) ? $_POST['company'] : '';
                                $contactEmail = isset($_POST['contactEmail']) ? $_POST['contactEmail'] : '';
                                $licenses = isset($_POST['licenses']) ? $_POST['licenses'] : '';
                                $serverKey = isset($_POST['serverKey']) ? $_POST['serverKey'] : '';
                                $expiryDate = isset($_POST['expiryDate']) ? $_POST['expiryDate'] : '';
                                $comment = isset($_POST['comment']) ? $_POST['comment'] : '';
                                $date = date_create();
                                $timestamp=date_format($date, "Y-m-d");



                                if($verzenden)
                                {
                                    $query0 = $db->query("UPDATE reporting_license_server SET active=0 WHERE server_key like '%$serverKey%';");

                                    $query= $db->query("INSERT INTO reporting_license_server (company,contact_email,number_licenses, server_key, expiry_date, comment, created_at, modified_at, active)
                                                          VALUES ('$company','$contactEmail','$licenses','$serverKey','$expiryDate','$comment',NOW(),NOW(),1)");

                                    ?>

                                    <form action="#" method="post">
                                        <div class="form-group">
                                            <div class="panel panel-default">
                                                <div class="panel-heading">
                                                    <h3 class="panel-title">Save License Server Details.</h3>
                                                </div>
                                                <div class="panel-body">
                                                    Server license saved successfully.
                                                </div>
                                            </div>
                                        </div>
                                        <button type="button" class="btn btn-success" onclick="location.href='license_server.php'">Back</button>
                                    </form>
                                <?php }
                                else
                                {
                                    ?>

                                    <form action="#" method="post">

                                        <div class="form-group">
                                            <label for="LabelForInputCompany">Company name:</label>
                                            <input type="text" name="company" class="form-control">
                                        </div>
                                        <div class="form-group">
                                            <label for="LabelForInputIndustry">Contact email:</label>
                                            <input type="email" name="contactEmail" class="form-control">
                                        </div>
                                        <div class="form-group">
                                            <label for="LabelForInputIndustry">Number of licenses:</label>
                                            <input type="text" name="licenses" class="form-control">
                                        </div>
                                        <div class="form-group">
                                            <label for="LabelForInputIndustry">Server Key:</label>
                                            <input type="text" name="serverKey" class="form-control">
                                        </div>
                                        <div class="form-group">
                                            <label for="LabelForInputIndustry">Expiry Date:</label>
                                            <input type="date" name="expiryDate" class="form-control">
                                        </div>
                                        <div class="form-group">
                                            <label for="LabelForInputExtra">Enter your comment (max. 300 characters):</label>
                                            <textarea name="comment" class="form-control" rows="5"></textarea>
                                        </div>
                                        <button type="submit" class="btn btn-primary" name="verzenden" value="Save">Save</button>
                                        <button type="button" class="btn btn-default" onclick="location.href='license_server.php'">Back</button>

                                    </form>
                                <?php
                                } }
                                ?>
                            </div>
                        </div>
                    </div>
                </div>
            </div><!-- /.outer -->
        </div><!-- /#content -->
        <?php include($rootdir."includes/footer.php"); ?>
</body>
</html>