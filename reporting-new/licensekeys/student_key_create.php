<?php
$rootdir="../";
$page="SCreate";
include_once($rootdir."includes/connect.php");
include($rootdir."includes/header.php");
?>
<body>
<div class="bg-light" id="wrap">
    <div id="top">
        <!-- .navbar -->
        <?php include($rootdir."includes/navtop.php"); ?>
    </div><!-- /#top -->
    <?php include($rootdir."includes/navleft.php"); ?>
    <div id="content">
        <div class="outer">
            <div class="inner">
                <div class="inner-head">
                    <h2>License Overview</h2>
                </div>
                <?php
                if (!isset($_SESSION["username"])){
                    echo $niet_ingelogd;
                }
                elseif($permission != "1"){
                    echo $geen_toegang;
                }else{
                ?>
                <div class="row">
                    <div class="col-md-10">
                        <div class="panel panel-content panel-default">
                            <div class="panel-heading">
                                <h3 class="panel-title">Replace License Key<span class="label label-info"></span></h3>
                            </div>
                            <div class="panel-body">
                                <?php
                                // zet gevaarlijke tekens in invoervelden om
                                function cleanup($string="")
                                {
                                    $string = trim($string); // verwijdert spaties aan begin en einde
//$string = htmlentities($string); // converteert < > & " en andere speciale tekens naar veilige tekens
                                    $string = htmlspecialchars($string);
                                    $string = nl2br($string); // converteert linebreaks in textareas naar <br />
// plaatst escapetekens bij quotes indien nodig
                                    if(!get_magic_quotes_gpc())
                                    {
                                        $string = addslashes($string);
                                    }

                                    return $string;
                                }

                                $verzenden = isset($_POST['verzenden']) ? cleanup($_POST['verzenden']) : '';
                                if($verzenden)
                                {

                                    $number_keys = isset($_POST['number_keys']) ? cleanup($_POST['number_keys']) : '';
                                    if($number_keys > 1000)
                                    { echo "Error: You entered an amount bigger than 1000. You can only generate 1000 student keys at one time. Please try again."; exit();}
                                    $choice_partner = isset($_POST['choice_partner']) ? cleanup($_POST['choice_partner']) : '';

                                    $choice_course = isset($_POST['choice_course']) ? cleanup($_POST['choice_course']) : '';

                                    // read out partner prefix
                                    $prefix_result = $db->query("SELECT partner_id, partner_trainingkey FROM reporting_partners WHERE partner_id = '$choice_partner'");
                                    $row_prefix = $prefix_result->fetch_assoc();
                                    $prefix = $row_prefix['partner_trainingkey'];

                                    // read out course id
                                    $course_result = $db->query("SELECT course_id, course_trainingkey FROM reporting_courses WHERE course_trainingkey = '$choice_course'");
                                    $row_course = $course_result->fetch_assoc();
                                    $course_id = $row_course['course_id'];
// uniek transactienummer creëren
                                    function randomize()
                                    {

                                        global $choice_partner; // bring this variable in for use in the function
                                        $nr = ""; // toegevoegd om notices te vermijden
                                        // transactienummer genereren
                                        $transactienummer=mt_rand(10000,99999);
                                        global $db;
                                        // checken of transactienummer wel uniek is
                                        $result = $db->query("SELECT * FROM reporting_trainingkey WHERE tk = '$transactienummer' && partner_id = '$choice_partner'");
                                        while ($row = $result->fetch_assoc()) {
                                            $nr = $row['tk'];
                                        }

                                        if($nr != $transactienummer)
                                        {
                                            return $transactienummer;
                                        }
                                        else
                                        {
                                            return randomize();
                                        }
                                    }


                                    echo "<table>";
                                    for( $i=0; $i<$number_keys; $i++ ) // 1000
                                    {
                                        $transactienummer = randomize();
                                        //$sql_insert = $db->query("INSERT INTO reporting_trainingkey (tk, course_id, partner_id) VALUES ('$transactienummer', '37','28')");
                                        $sql_insert = $db->query("INSERT INTO reporting_trainingkey (tk, course_id, partner_id, date) VALUES ('$transactienummer', '$course_id','$choice_partner', NOW())");
                                        echo "<tr><td>" . $prefix . $choice_course . $transactienummer . "</td></tr>"; // 26 voor MSPP, 15 voor Foundation, 38 voor DTBD 2days
                                        // also save new student keys directly into imijoomla_int -> imi_course_studentkeys table
                                        $complete_key = $prefix . $choice_course . $transactienummer;

                                        $db3->query("INSERT INTO imi_course_studentkeys (studentkey, language, created, created_by) VALUES ('$complete_key', '*',NOW(), '1600')");
                                    }
                                    echo "</table>";

                                }
                                ?>
                                <?php if(!$verzenden) {?>
                                    <div class="col-sm-6">
                                        <form class="form-horizontal" method="post" role="form">
                                            <select class="form-control" name="choice_partner">
                                                <option value="">- Please select a partner-</option>
                                                <?php
                                                $choice_partner =""; // added to avoid notices

                                                $partner_result=$db->query("SELECT * FROM reporting_partners WHERE active='yes' ORDER BY partner_company ASC");
                                                while($partner=$partner_result->fetch_array()){


                                                    ?>
                                                    <option value="<?php echo $partner['partner_id'];  ?>" <?php if($choice_partner==$partner['partner_id']) { echo "selected='selected'";} ?>><?php echo $partner['partner_company']; ?></option>
                                                <?php
                                                }
                                                ?>
                                            </select><br />

                                            <select class="form-control" name="choice_course">
                                                <option value="">- Select course -</option>
                                                <?php
                                                $course = ""; // added to avoid empty variable notices

                                                $course_result=$db->query("SELECT * FROM reporting_courses ORDER BY course_name ASC");
                                                while($course_row=$course_result->fetch_array()){
                                                    // select training_key field, this field is used in the student keys

                                                    ?>
                                                    <option value="<?php echo $course_row['course_trainingkey'] ?>" <?php if(($course)==$course_row['course_trainingkey']) { echo "selected='selected'";} ?>> <?php echo $course_row['course_name'] ?> </option>
                                                <?php
                                                }
                                                ?>
                                            </select><br />
                                            <input type="text" class="form-control" name="number_keys" value="1000" /><br />
                                            <input type="submit" name="verzenden" value="Generate student keys" class="btn btn-primary" />
                                        </form>
                                    </div>
                                <?php } // einde if not verzenden ?>
                            </div>
                        </div>
                    </div>
                    <?php }?>
                </div><!-- /.inner -->
            </div><!-- /.outer -->
        </div><!-- /#content -->
        <?php include($rootdir."includes/footer.php"); ?>
</body>
</html>