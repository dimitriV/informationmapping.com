<?php
$rootdir="../";
$page="APermissions";
include_once($rootdir."includes/connect.php");
include($rootdir."includes/header.php");
?>
<body>

<div class="bg-light" id="wrap">
    <div id="top">
        <!-- .navbar -->
        <?php include($rootdir."includes/navtop.php"); ?>
    </div><!-- /#top -->
    <?php include($rootdir."includes/navleft.php"); ?>
    <div id="content">
        <div class="outer">
            <div class="inner">
                <div class="inner-head">
                    <h1>User Permissions</h1>
                </div>
                <?php
                if (!isset($_SESSION["username"])){
                    echo $niet_ingelogd;
                }
                elseif($permission != "1"){
                    echo $geen_toegang;
                }else{
                    ?>
                    <div class="row">

                        <?php
                        $headerID=1;
                        $query = $db->query("SELECT * FROM reporting_pages group by categorie order by id;");
                        while($row = $query->fetch_array()){

                            ?>
                            <div class="col-lg-6">
                                <div class="panel-group" id="accordion" role="tablist" aria-multiselectable="true">
                                    <div class="panel panel-default">
                                        <div class="panel-heading" role="tab" id="headingOne" data-toggle="collapse" data-parent="#accordion" href="#collapse<?= $headerID; ?>" aria-expanded="true" aria-controls="collapse<?= $headerID; ?>">
                                            <h4 class="panel-title">
                                                <?= $row['categorie']; ?><i id="arrow" class="fa fa-chevron-down pull-right"></i>
                                            </h4>
                                        </div>
                                        <div id="collapse<?= $headerID++; ?>" class="panel-collapse collapse" role="tabpanel" aria-labelledby="headingOne">
                                            <div class="input-group">
                                                <li class="list-group-item"><strong>Select All Pages</strong></li>
                                                      <span class="input-group-addon">
                                                        <input id="selectall" type="checkbox" aria-label="...">
                                                      </span>
                                            </div>
                                            <?php  $pages = $db->query("SELECT * FROM reporting_pages WHERE categorie like '".$row['categorie']."';");
                                                while($row_pages = $pages->fetch_array()){
                                                ?>
                                                <div class="input-group">
                                                    <li class="list-group-item"><?php if($row_pages['depth']==2){ echo str_repeat('&nbsp;', 5).' - '; } ?><?=  $row_pages['name']; ?></li>
                                                      <span class="input-group-addon">
                                                        <input class="checkbox" type="checkbox" aria-label="...">
                                                      </span>
                                                </div>
                                                <?php } ?>
                                            </ul>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        <?php } ?>
                    </div>
                <?php }?>
            </div><!-- /.inner -->
        </div><!-- /.outer -->
    </div><!-- /#content -->
    <script>
            $('#selectall').click(function(event) {  //on click
                if(this.checked) { // check select status
                    $('.checkbox').each(function() { //loop through each checkbox
                        this.checked = true;  //select all checkboxes with class "checkbox1"
                    });
                }else{
                    $('.checkbox').each(function() { //loop through each checkbox
                        this.checked = false; //deselect all checkboxes with class "checkbox1"
                    });
                }
            });

        $("div[id='headingOne']").on('click', function(){
            $('i',this).toggleClass("fa fa-chevron-right fa fa-chevron-down");
        });
    </script>
    <?php include($rootdir."includes/footer.php"); ?>
</body>
</html>