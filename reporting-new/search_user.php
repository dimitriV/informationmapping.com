<?php
$page="search_user";
include("includes/connect.php");
include("includes/header.php");
?>
<body>
<div class="bg-light" id="wrap">
<div id="top">
    <!-- .navbar -->
    <?php include("includes/navtop.php"); ?>
</div><!-- /#top -->
<?php include("includes/navleft.php"); ?>
<div id="content">
<div class="outer">
<div class="inner">
<div class="inner-head">
    <h1>Customer Details</h1>
</div>
<?php
if (!isset($_SESSION["username"])){
    echo $niet_ingelogd;
}
elseif($permission != "1"){
    echo $geen_toegang;
}else{
?>
<!--Begin Datatables-->
<div class="row">
<div class="col-lg-12">
<div class="box">
<header>
    <div class="icons">
        <i class="fa fa-user"></i>
    </div>
    <?php
    $entity_name = $_GET['entity_name'];
    $entity_email = $_GET['entity_email'];
    $entity_id = $_GET['entity_id'];
    $r_imcc = $db->query("SELECT * FROM resume AS r, resumefeedback AS f WHERE r.email = '$entity_email' && r.program = 'Information Mapping Certified Consultant Program' && r.resume_id = f.resume_id && (f.exam_passed = '1' OR f.exam_passed = '3')");
    $row_imcc = $r_imcc->fetch_assoc();

    $userid = $db->query("SELECT * FROM customer_entity WHERE email = '$entity_email'");
    $userid_row = $userid->fetch_array();
    $magento_userid = $userid_row['entity_id']; // Magento ID

    $userid_academy = $db2->query("SELECT * FROM mdl_user WHERE email = '$entity_email'");
    $userid_row_academy = $userid_academy->fetch_array();
    $academy_userid = $userid_row_academy['id'];

    $userid_joomla = $db3->query("SELECT * FROM imi_users WHERE email = '$entity_email'");
    $userid_row_joomla = $userid_joomla->fetch_array();
    $joomla_userid = $userid_row_joomla['id'];


    ?>
    <h5>User: <?php echo $entity_name ?> <br> Magento ID: <?php if(empty($magento_userid)) { $magento_userid = "guest"; echo $magento_userid; } else { echo $magento_userid; }?>
        | Academy ID:  <?php if(empty($academy_userid)) { $academy_userid = "none"; echo $academy_userid; } else { echo $academy_userid; } ?>
        | Joomla ID: <?php  if(empty($joomla_userid)) { $joomla_userid = "none"; echo $joomla_userid; } else { echo $joomla_userid; } ?>
        <?php if(!empty($row_imcc)) { echo "<strong> | Information Mapping Certified Consultant</strong>"; } ?>

    </h5>
</header>
<div id="collapse4" class="body">
    <table class="table table-bordered table-condensed table-hover table-striped" id="search_user">
        <thead>
        <tr>
            <th>Order Details</th>
            <th>E-learning enrollments</th>
            <th>Activated FS Pro</th>
            <th>Certificate of Attendance</th>
            <th>IMP Exam</th>
        </tr>
        </thead>
        <tbody>

        <tr><td>
                <?php
                if($magento_userid != "guest") {
                    $orders = $db->query("SELECT entity_id, increment_id, DATE_FORMAT(created_at, '%d/%m/%Y') AS newDate, customer_id, subtotal, status FROM sales_flat_order WHERE customer_id = '$magento_userid'");
                }else{
                    $orders = $db->query("SELECT entity_id, increment_id, DATE_FORMAT(created_at, '%d/%m/%Y') AS newDate, subtotal, status FROM sales_flat_order WHERE entity_id = '$entity_id'");

                }
                $orders_aantal =$orders->num_rows;
                if($orders_aantal > 0)
                {
                    while($row= $orders->fetch_array()){

                        echo "#" . $row['increment_id'] . " | " . $row['newDate'] . " (" . $row['status'] . ")<br />";
                        // detail van order uitlezen
                        $ord_id = $row['entity_id'];
                        //echo $ord_id;
                        $res_detail = $db->query("SELECT * FROM sales_flat_order_item WHERE order_id = '$ord_id' && product_type = 'bundle'");
                        echo "<ul>";
                        while($row_detail = $res_detail->fetch_array())
                        {

                            echo "<li>" . $row_detail['name'] . "</li>";

                        }
                        echo "</ul>";
                    }
                }
                else { echo "No orders placed"; }
                ?>
            </td>
            <td>
                <?php



                // enkel uitvoeren als er een Moodle id gevonden is!
                if($academy_userid != "none") {
                    $enrollments = $db2->query("SELECT * FROM mdl_user_enrolments WHERE userid = '$academy_userid'");
                }
                $enrollments_aantal = $enrollments->num_rows;

                if($enrollments_aantal > 0)
                {

                    while($row2= $enrollments->fetch_array()){

                        $enrolid = $row2['enrolid'];

                        // enrolid must be read in mdl_enrol to get the courseid
                        $sqlsql = $db2->query("SELECT id,courseid FROM mdl_enrol WHERE id ='$enrolid'");
                        $rowsql = $sqlsql->fetch_array();
                        // end mdl_enrol, nu nog course name uitlezen (geldt ook voor quiz)
                        $tooncursus = $rowsql['courseid'];

                        $sql_course = $db2->query("SELECT * FROM mdl_course WHERE id = '$tooncursus'");
                        $report_course = $sql_course->fetch_assoc();

                        $out = $report_course['fullname'];

                        echo $out;
                        echo " | ";
                        if($row2['timestart'] == "0") { echo "no start date"; } else { echo date('d F Y', $row2['timestart']); }
                        echo " until ";
                        if($row2['timeend'] == "0") { echo "no end date"; } else { echo date('d F Y', $row2['timeend']); }
                        echo ")<br />";
                    }

                    echo "<p><strong>Results: </strong></p>";

                    $result_grades = $db2->query("SELECT gg.itemid, gg.userid, gg.rawgrade, gg.rawgrademax, gg.timemodified, gi.id, gi.itemname FROM mdl_grade_grades AS gg, mdl_grade_items AS gi WHERE gg.itemid = gi.id && gg.userid = '$academy_userid' && gg.rawgrade IS NOT NULL");
                    while($row_grades= $result_grades->fetch_array())
                    {
                        echo $row_grades['itemname'] . ": ";
                        echo round($row_grades['rawgrade'],2) . " / " . round($row_grades['rawgrademax'],0);
                        echo " (" . date('d F Y', $row_grades['timemodified']) . ")<br />";
                    }

                }
                else { echo "No enrollments found"; }
                ?>
                <p><strong>Last login: </strong>
                    <?php

                    $acc = $db2->query("SELECT from_unixtime(lastlogin, '%Y %D %M %h:%i:%s') AS conv_lastaccess FROM mdl_user WHERE id = '$academy_userid'");
                    $acc_academy = $acc->fetch_assoc();
                    $academy_timeaccess = $acc_academy['conv_lastaccess'];
                    echo $academy_timeaccess;
                    ?>
                </p>
            </td>
            <td>
                <?php
                // read FS Pro 2013 keys
                //$res_lic = $db2->query("SELECT * FROM as_unique_key2013 WHERE enduser_email = '$entity_email'");
                $res_lic = $db->query("SELECT
                                                *
                                            FROM
                                                license_orders AS o
                                                    INNER JOIN
                                                license_serialnumbers AS s ON o.license_id = s.id
                                                    INNER JOIN
                                                license_info AS i ON i.license_id = o.license_id
                                            WHERE
                                                i.email LIKE '$entity_email'");
                $aantal_lic = $res_lic->num_rows;
                echo "<strong>FS Pro keys: </strong><br />" . $aantal_lic . " license keys<br />";
                if($aantal_lic > 0)
                {
                    while($row = $res_lic->fetch_array())
                    {
                        echo $row['last_license_key'] . "<br />";
                    }
                }
                else
                {
                    echo "No";
                }


                ?>
            </td>
            <td>
                <?php
                // enkel uitvoeren als er een Joomla id gevonden is!
                if($joomla_userid != "none") {
                    $certificate = $db3->query("SELECT * FROM imi_course_certificates WHERE user_id = '$joomla_userid'"); // user_id kan uit imi_users gehaald worden op basis van email
                }
                $certificate_aantal = $certificate->num_rows;

                if($certificate_aantal > 0)
                {
                    while($row=$certificate->fetch_array()){
                        echo $row['first_name'] . " " . $row['last_name'] . " | " . $row['date_from'] . " | <a href=\"http://www.informationmapping.com/coursecertificate/" . $row['filename'] . "\" target=\"_blank\">Download</a><br />";
                    }
                }
                else { echo "No Certificate of Attendance generated"; }
                ?>
            </td>
            <td>
                <?php
                // enkel uitvoeren als er een Moodle id gevonden is!
                if($academy_userid != "none") {
                    $imp = $db2->query("SELECT * FROM mdl_quiz_grades WHERE userid = '$academy_userid' && quiz = '1'
                                        OR (userid = '$academy_userid' && quiz = '16') ");
                    $imp_aantal = $imp->num_rows;
                    if($imp_aantal > 0)
                    {
                        while($row3= $imp->fetch_array()){

                            echo round($row3['grade'],2) . " / 20 (" . date('d F Y', $row3['timemodified']) . ")<br />";
                        }
                    }
                    else { echo "No grades found for IMP Exam"; }


                }
                ?>
            </td>
        </tr>


        </tbody>
    </table>
</div>
</div>
</div>
</div><!-- /.row -->
<?php }?>
</div><!-- /.inner -->
</div><!-- /.outer -->
</div><!-- /#content -->

<?php include("includes/footer.php"); ?>
<script>
    $(document).ready(function() {
        $('#search_user').dataTable();
    });
</script>

</body>
</html>