<?php
$page="Dash";
include_once("includes/connect.php");
include("includes/header.php"); ?>

<body>
    <div class="container">
        <div class="row">
            <div class="col-md-4 col-md-offset-4">
                <div class="login-panel panel panel-default text-center">
                    <div class="panel-heading">
                        <a href="dashboard.php" class="login-img">
                            <img src="assets/img/logo.png" alt="">
                        </a>
                    </div>
                    <div class="panel-body">
                        <p class="text-center pv">SIGN IN TO CONTINUE.</p>
                        <form role="form" action="includes/login_check.php" method="post" name="form1" id="form1" class="form">
                            <fieldset>
                                <div class="input-group form-group ">
                                    <input class="form-control" placeholder="E-mail" name="email" type="text" autofocus><span class="input-group-addon"><i class="fa fa-envelope"></i></span>
                                </div>
                                <div class="input-group form-group">
                                    <input class="form-control" placeholder="Password" name="password" type="password" value=""><span class="input-group-addon"><i class="fa fa-lock fa-lg"></i></span>
                                </div>
                                <div class="checkbox">
                                    <label>
                                        <input name="remember" type="checkbox" value="Remember Me">Remember Me
                                    </label>
                                </div>
                                <!-- Change this to a button or input when using this as a form -->
                                <button type="submit"  name="Submit" class="btn btn-primary btn-block">Login</button>
                            </fieldset>
                        </form>
                    </div>
                </div>
            </div>
        </div>
    </div>
<?php include("includes/footer.php"); ?>
</body>

</html>
