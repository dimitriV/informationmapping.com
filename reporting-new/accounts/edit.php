<?php
$rootdir="../";
$page="ACEdit";
include_once($rootdir."includes/connect.php");
include($rootdir."includes/header.php");
?>
<script>
    $(document).ready(function() {



        $('#chkOwner').click(function () {
            if ($("#chkOwner").is(':checked')) {
                $("#ownerDefault").addClass(" hidden");
                $("#owner").removeClass(" hidden");
            } else{
                $("#ownerDefault").removeClass(" hidden");
                $("#owner").addClass(" hidden");
            }
        });
    });
</script>
<body>
<div class="bg-light" id="wrap">
    <div id="top">
        <!-- .navbar -->
        <?php include($rootdir."includes/navtop.php"); ?>
    </div><!-- /#top -->
    <?php include($rootdir."includes/navleft.php"); ?>
    <div id="content">
        <div class="outer">
            <div class="inner">
                <div class="inner-head">
                    <h2>Accounts Overview:</h2>
                </div>
                <?php
                if (!isset($_SESSION["username"])){
                    echo $niet_ingelogd;
                }
                elseif($permission != "1"){
                    echo $geen_toegang;
                }else{
                ?>
                <div class="row">
                    <div class="col-md-6">
                        <div class="panel panel-default">
                            <div class="panel-body">
                                <?php
                                $id = is_numeric($_GET['id']) ? $_GET['id'] : '';
                                $verzenden = isset($_POST['verzenden']) ? $_POST['verzenden'] : '';


                                $company = isset($_POST['company']) ? $_POST['company'] : '';
                                $indusrtry = isset($_POST['industry']) ? $_POST['industry'] : '';
                                if (isset($_POST['chkOwner'])){
                                    $owner = isset($_POST['owner']) ? $_POST['owner'] : '';
                                }else {
                                    $owner = isset($_POST['ownerDefault']) ? $_POST['ownerDefault'] : '';
                                }
                                $projected = isset($_POST['projected']) ? $_POST['projected'] : '';
                                $comment = isset($_POST['comment']) ? $_POST['comment'] : '';
                                $date = date_create();
                                $timestamp=date_format($date, 'U');


                                if($verzenden)
                                {
                                    $db->query("UPDATE reporting_customers SET name = '$company', industry = '$indusrtry',ownerid = '$owner',comment ='$comment', modified_at ='$timestamp' WHERE id = '$id'");
                                    $db->query("UPDATE reporting_customer_turnover SET projected = '$projected' WHERE customerid = '$id'"); ?>

                                    <form action="#" method="post">
                                        <div class="form-group">
                                            <div class="panel panel-default">
                                                <div class="panel-heading">
                                                    <h3 class="panel-title">Edit Customer.</h3>
                                                </div>
                                                <div class="panel-body">
                                                    Customer saved successfully.
                                                </div>
                                            </div>
                                        </div>
                                        <button type="button" class="btn btn-success" onclick="location.href='overview.php'">Back</button>
                                    </form>
                                <?php }
                                else
                                {
                                    ?>

                                    <form action="#" method="post">

                                        <div class="form-group">
                                            <label for="LabelForInputCompany">Company name:</label>
                                            <?php
                                            $result = $db->query("SELECT c.name as company_name, c.industry, so.name as owner, so.id as ownerid FROM reporting_customers as c
                                                                                LEFT JOIN
                                                                        reporting_sales_organization as so on c.ownerid=so.id
                                                                         WHERE c.id = '$id'");
                                            $row = $result->fetch_assoc(); ?>
                                            <input name="company" class="form-control" value="<?php echo $row['company_name'];?>">
                                        </div>
                                        <div class="form-group">
                                            <label for="LabelForInputIndustry">Indusrtry:</label>
                                            <input name="industry" class="form-control" value="<?php echo $row['industry'];?>">
                                        </div>
                                        <div class="form-group">
                                            <label for="LabelForInputOwner">Owner:</label>
                                            <input id="ownerDefault" class="form-control" value="<?php echo $row['owner'];?>" disabled>
                                            <input type="hidden" name="ownerDefault" value="<?php echo $row['ownerid'];?>">

                                            <select id="owner" name="owner" class="form-control hidden">
                                                <?php $sales = $db->query("SELECT * FROM reporting_sales_organization where name not like ''");
                                                while($row_sales = $sales->fetch_array())
                                                {
                                                    ?>
                                                    <option value="<?= $row_sales['id']?>"><?= $row_sales['name']?></option>
                                                <?php } ?>
                                            </select>
                                            <input type="checkbox" id="chkOwner" name="chkOwner" >Overwrite
                                        </div>
                                        <table class="table table-bordered">
                                            <thead>
                                            <tr>
                                                <?php
                                                $q = $db->query("SELECT COLUMN_NAME
                                        FROM INFORMATION_SCHEMA.COLUMNS
                                        WHERE TABLE_SCHEMA='imi_staging_magento'
                                        AND TABLE_NAME='reporting_customer_turnover' AND ORDINAL_POSITION >2;");
                                                while($row_a = $q->fetch_array()) {
                                                    echo "<th>" . $row_a['COLUMN_NAME'] . "</th>";
                                                } ?>
                                                <th>Total</th>
                                            </tr>
                                            </thead>
                                            <tbody>
                                                <?php
                                                $query = $db->query("SELECT
                                                                    ct.projected,
                                                                    ct.2010,
                                                                    ct.2011,
                                                                    ct.2012,
                                                                    ct.2013,
                                                                    ct.2014,
                                                                   (ct.2010 + ct.2011 + ct.2012 + ct.2013 + ct.2014) as 'Total'
                                                                FROM  reporting_customer_turnover as ct
                                                                WHERE ct.customerid='$id'" );

                                                while($row = $query->fetch_array())
                                                {
                                                    $projection= $row['projected'];
                                                    echo "<tr>";
                                                    echo "<td>$" . $row['2010'] . "</td>";
                                                    echo "<td>$" . $row['2011'] . "</td>";
                                                    echo "<td>$" . $row['2012'] . "</td>";
                                                    echo "<td>$" . $row['2013'] . "</td>";
                                                    echo "<td>$" . $row['2014'] . "</td>";
                                                    echo "<td><strong>$" . $row['Total'] . "</strong></td>";
                                                    echo "</tr>";
                                                }
                                                ?>
                                            </tbody>
                                        </table>
                                        <div class="form-group">
                                            <label for="LabelForInputProjected">Target <?= date("Y"); ?>:</label>
                                            <input name="projected" class="form-control" value="<?php echo $projection;?>">
                                        </div>
                                        <div class="form-group">
                                            <label for="LabelForInputExtra">Enter your comment (max. 300 characters):</label>
                                            <textarea name="comment" class="form-control" rows="5"><?php
                                                $result = $db->query("SELECT comment FROM reporting_customers
                                                                         WHERE id = '$id'");
                                                $row = $result->fetch_assoc();
                                                echo $row['comment'];?></textarea>
                                        </div>
                                        <button type="submit" class="btn btn-primary" name="verzenden" value="Save">Save</button>
                                        <button type="button" class="btn btn-default" onclick="location.href='overview.php'">Back</button>

                                    </form>
                                <?php
                                }
                                }?>
                            </div>
                        </div>
                    </div>
                </div><!-- /.inner -->
            </div><!-- /.outer -->
        </div><!-- /#content -->
        <?php include($rootdir."includes/footer.php"); ?>
</body>
</html>