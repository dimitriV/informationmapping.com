<?php
$rootdir="../";
$page="ACIndustry";
include_once($rootdir."includes/connect.php");
include($rootdir."includes/header.php");
?>
<body>
<div class="bg-light" id="wrap">
    <div id="top">
        <!-- .navbar -->
        <?php include($rootdir."includes/navtop.php"); ?>
    </div><!-- /#top -->
    <?php include($rootdir."includes/navleft.php"); ?>
    <div id="content">
        <div class="outer">
            <div class="inner">
                <div class="inner-head">
                    <h1>Industry List</h1>
                </div>
                <?php
                if (!isset($_SESSION["username"])){
                    echo $niet_ingelogd;
                }
                elseif($permission != "1"){
                    echo $geen_toegang;
                }else{
                    ?>
                    <div class="row">
                        <div class="col-md-8">
                        <div class="panel panel-default">
                            <div class="panel-body">
                                <?php
                                $headerID=1;
                                $query = $db->query("SELECT i.name as industry, so.name as owner FROM reporting_account_industry as i LEFT JOIN reporting_sales_organization as so on so.id=i.ownerid group by i.name order by so.id;");
                                while($row = $query->fetch_array()){

                                    ?>
                                    <div class="col-lg-12">
                                        <div class="panel-group" id="accordion" role="tablist" aria-multiselectable="true">
                                            <div class="col-lg-10">
                                                <div class="panel panel-info">
                                                    <div class="panel-heading" role="tab" id="headingOne" data-toggle="collapse" data-parent="#accordion" href="#collapse<?= $headerID; ?>" aria-expanded="true" aria-controls="collapse<?= $headerID; ?>">
                                                        <h4 class="panel-title">
                                                            <?= $row['industry']; ?><i id="arrow" class="fa fa-chevron-down pull-right" data-toggle="tooltip" data-placement="left" title="sub-industry"></i>
                                                        </h4>
                                                    </div>
                                                    <div id="collapse<?= $headerID; ?>" class="panel-collapse collapse" role="tabpanel" aria-labelledby="headingOne">
                                                        <?php  $pages = $db->query("SELECT * FROM reporting_account_industry WHERE name like '".$row['industry']."' order by sub_industry asc;");
                                                        while($row_pages = $pages->fetch_array()){
                                                            ?>
                                                            <div class="list-group">
                                                                <li class="list-group-item"><?=  $row_pages['sub_industry']; ?></li>
                                                            </div>
                                                        <?php } ?>
                                                        </ul>
                                                    </div>
                                                </div>
                                            </div>
                                            <div class="col-lg-2">
                                                <button id="but" type="button" class="btn btn-block" data-toggle="collapse" href="#collapse<?= $headerID; ?>" aria-expanded="true" aria-controls="collapse<?= $headerID++; ?>" data-toggle="tooltip" data-placement="left" title="sub-industry" value="<?php echo $row['owner'] ?>"><?php echo $row['owner'] ?></button>
                                            </div>
                                        </div>
                                    </div>
                                <?php } ?>
                            </div>
                        </div>
                    </div>
                        </div>
                <?php }?>
            </div><!-- /.inner -->
        </div><!-- /.outer -->
    </div><!-- /#content -->
    <script>
        $("div[id='headingOne']").on('click', function(){
            $('i',this).toggleClass("fa fa-chevron-right fa fa-chevron-down");
        });
        $("button[value='Susan Napier']").addClass("btn-info");
        $("button[value='Dave Roberts']").addClass("btn-success");
        $("button[value='Joe Pacsay']").addClass("btn-warning");
        $("button[value='Debbie Bruno']").addClass("btn-primary");
    </script>
    <?php include($rootdir."includes/footer.php"); ?>
</body>
</html>