<?php
$rootdir="../";
$page="ACIs3";
$_SESSION["file"] = $page;
include_once($rootdir."includes/connect.php");
include($rootdir."includes/header.php");
$query = "SELECT c.name as Customer, c.industry as Industry, so.name as Accountmanager, ct.projected as Target FROM
                                                                    reporting_customers as c
                                                                        LEFT JOIN
                                                                    reporting_sales_organization as so on c.ownerid=so.id
																		LEFT JOIN
                                                                    reporting_customer_turnover as ct on ct.customerid=c.id
                                                                    WHERE ownerid=8
                                                                order by Customer asc;";
$_SESSION["query"] = $query;
$_SESSION["database"] = 'db';
?>
<script>
    $(document).ready(function() {
        $( "#exp" ).click(function() {
            $("#sub").submit();
        });
    });
</script>
<body>
<div class="bg-light" id="wrap">
    <div id="top">
        <!-- .navbar -->
        <?php include($rootdir."includes/navtop.php"); ?>
    </div><!-- /#top -->
    <?php include($rootdir."includes/navleft.php"); ?>
    <div id="content">
        <div class="outer">
            <div class="inner">
                <div class="inner-head">
                    <h1><?php  $q = $db->query("SELECT name FROM reporting_sales_organization WHERE id=8"); $rowq = $q->fetch_assoc();  echo $rowq['name'];?></h1>
                </div>
                <?php
                if (!isset($_SESSION["username"])){
                    echo $niet_ingelogd;
                }
                elseif($permission != "1"){
                    echo $geen_toegang;
                }else{
                ?>
                <div class="row">
                    <div class="col-lg-12">
                        <div class="panel panel-default">
                            <div class="panel-heading">
                                <button type="button" id="exp" class="btn btn-info pull-right"><i class="fa fa-download fa-lg"></i> Export CSV</button>
                                <h2>Accounts overview: </h2>
                                <form id="sub" method="post" enctype="multipart/form-data" action="<?php echo $rootdir; ?>includes/csv.php">
                                <input type="hidden" name="filename" value="<?php echo $page ?>.csv">
                            </div>
                            <div class="panel-body">
                                <table class="table table-bordered table-condensed table-hover table-striped" id="dataTables-accounts">
                                    <thead>
                                    <tr>
                                        <th>Customer</th>
                                        <th>Industry</th>
                                        <th>Owner</th>
                                        <?php
                                        $q = $db->query("SELECT COLUMN_NAME
                                        FROM INFORMATION_SCHEMA.COLUMNS
                                        WHERE TABLE_SCHEMA='imi_staging_magento'
                                        AND TABLE_NAME='reporting_customer_turnover' AND ORDINAL_POSITION >2;");
                                        while($row_a = $q->fetch_array()) {
                                            //  echo "<th>" . $row_a['COLUMN_NAME'] . "</th>";

                                        } ?>

                                        <th>Target <?= date("Y"); ?> </th>
                                    </tr>
                                    </thead>
                                    <tbody> <?php
                                    $query = $db->query("SELECT c.name as Customer, c.industry as Industry, so.name as Accountmanager, ct.projected as Target FROM
                                                                    reporting_customers as c
                                                                        LEFT JOIN
                                                                    reporting_sales_organization as so on c.ownerid=so.id
																		LEFT JOIN
                                                                    reporting_customer_turnover as ct on ct.customerid=c.id
                                                                    WHERE ownerid=8
                                                                order by Customer asc;" );

                                    while($row = $query->fetch_array($query))
                                    {
                                        echo "<tr>";
                                        echo "<td>" . $row['Customer'] . "<a class=\"pull-right\" target=\"_blank\" href=\"https://connect.data.com/search#p%3Dsearchresult%3B%3Bss%3Dsingleboxsearch%3B%3Bq%3D%7B%22freeText%22%3A%22".$row['name']."%22%7D\"><img src=\"../assets/img/datacom.png\" alt=\"Datacom\" height=\"16\" width=\"16\" ></a></td>";
                                        //  echo "<td><a class=\"pull-right\" target=\"_blank\" href=\"https://connect.data.com/search#p%3Dsearchresult%3B%3Bss%3Dsingleboxsearch%3B%3Bq%3D%7B%22freeText%22%3A%22".$row['name']."%22%7D\"><img src=\"../assets/img/datacom.png\" alt=\"Datacom\" height=\"16\" width=\"16\" ></a></td>";
                                        echo "<td>" . $row['Industry'] . "</td>";
                                        //   echo "<td>" . $row['code'] . "</td>";
                                        echo "<td>" . $row['Accountmanager'] . "</td>";
                                        echo "<td>$" . $row['Target'] . "</td>";
                                        echo "</tr>";
                                    }
                                    ?>
                                    </tbody>
                                </table>
                                </form>
                            </div>
                        </div>
                    </div>
                    <?php }?>
                </div><!-- /.inner -->
            </div><!-- /.outer -->
        </div><!-- /#content -->
        <script>
            $(document).ready(function() {
                $('#dataTables-accounts').dataTable({
                    "pageLength": 25
                });
            });
        </script>
        <?php include($rootdir."includes/footer.php"); ?>
</body>
</html>