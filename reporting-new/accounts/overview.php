<?php
$rootdir="../";
$page="ACOverview";
include_once($rootdir."includes/connect.php");
include($rootdir."includes/header.php");
?>
<body>
<div class="bg-light" id="wrap">
    <div id="top">
        <!-- .navbar -->
        <?php include($rootdir."includes/navtop.php"); ?>
    </div><!-- /#top -->
    <?php include($rootdir."includes/navleft.php"); ?>
    <div id="content">
        <div class="outer">
            <div class="inner">
                <div class="inner-head">
                    <h2>Accounts Overview:</h2>
                </div>
                <?php
                if (!isset($_SESSION["username"])){
                    echo $niet_ingelogd;
                }
                elseif($permission != "1"){
                    echo $geen_toegang;
                }else{
                ?>
                <div class="row">
                    <div class="col-lg-12">
                        <div class="box">
                            <header>
                                <div class="icons">
                                    <i class="fa fa-users"></i>
                                </div>
                                <h5>Accounts overview:</h5>
                                <div><a href="create.php" class="add pull-right">Add Customer <i class="fa fa-plus-square fa-lg"></i></a></div>
                            </header>
                            <div id="collapse4" class="body">
                                <table class="table table-bordered table-condensed table-hover table-striped" id="dataTables-accounts">
                                    <thead>
                                    <tr>
                                        <th>Customer</th>
                                        <th>Industry</th>
                                        <th>Owner</th>
                                    <?php
                                    $q = $db->query("SELECT COLUMN_NAME
                                        FROM INFORMATION_SCHEMA.COLUMNS
                                        WHERE TABLE_SCHEMA='imi_prod_magento'
                                        AND TABLE_NAME='reporting_customer_turnover' AND ORDINAL_POSITION >2;");
                                    while($row_a = $q->fetch_array($q)) {
                                      //  echo "<th>" . $row_a['COLUMN_NAME'] . "</th>";

                                    } ?>

                                        <th>Target <?= date("Y"); ?> </th>
                                    </tr>
                                    </thead>
                                    <tbody> <?php
                                    $query = $db->query("SELECT
                                                                    c.id,
                                                                    c.name,
                                                                    c.industry,
                                                                    so.code,
                                                                    so.name as accountmanager,
                                                                    ct.projected
                                                                FROM
                                                                    reporting_customers as c
                                                                        LEFT JOIN
                                                                    reporting_sales_organization as so on c.ownerid=so.id
																		LEFT JOIN
                                                                    reporting_customer_turnover as ct on ct.customerid=c.id
                                                                order by c.id asc;" , $db);

                                    while($row = $query->fetch_array())
                                    {
                                        echo "<tr>";
                                        echo "<td><a href=\"edit.php?id=" . $row['id'] . "\" >" . $row['name'] . "</a><a class=\"pull-right\" target=\"_blank\" href=\"https://connect.data.com/search#p%3Dsearchresult%3B%3Bss%3Dsingleboxsearch%3B%3Bq%3D%7B%22freeText%22%3A%22".$row['name']."%22%7D\"><img src=\"../assets/img/datacom.png\" alt=\"Datacom\" height=\"16\" width=\"16\" ></a></td>";
                                      //  echo "<td><a class=\"pull-right\" target=\"_blank\" href=\"https://connect.data.com/search#p%3Dsearchresult%3B%3Bss%3Dsingleboxsearch%3B%3Bq%3D%7B%22freeText%22%3A%22".$row['name']."%22%7D\"><img src=\"../assets/img/datacom.png\" alt=\"Datacom\" height=\"16\" width=\"16\" ></a></td>";
                                        echo "<td>" . $row['industry'] . "</td>";
                                     //   echo "<td>" . $row['code'] . "</td>";
                                        echo "<td>" . $row['accountmanager'] . "</td>";
                                        echo "<td>$" . $row['projected'] . "</td>";
                                        echo "</tr>";
                                    }
                                    ?>
                                    </tbody>
                                </table>
                            </div>
                        </div>
                    </div>
                    <?php }?>
                </div><!-- /.inner -->
            </div><!-- /.outer -->
        </div><!-- /#content -->
        <script>
            $(document).ready(function() {
                $('#dataTables-accounts').dataTable({
                    "pageLength": 25
                });
            });
        </script>
        <?php include($rootdir."includes/footer.php"); ?>
</body>
</html>