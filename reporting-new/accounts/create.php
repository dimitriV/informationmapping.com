<?php
$rootdir="../";
$page="ACCreate";
include_once($rootdir."includes/connect.php");
include($rootdir."includes/header.php");
?>
<body>
<div class="bg-light" id="wrap">
    <div id="top">
        <!-- .navbar -->
        <?php include($rootdir."includes/navtop.php"); ?>
    </div><!-- /#top -->
    <?php include($rootdir."includes/navleft.php"); ?>
    <div id="content">
        <div class="outer">
            <div class="inner">
                <div class="inner-head">
                    <h2>Accounts Overview:</h2>
                </div>
                <?php
                if (!isset($_SESSION["username"])){
                    echo $niet_ingelogd;
                }
                elseif($permission != "1"){
                    echo $geen_toegang;
                }else{
                ?>
                <div class="row">
                    <div class="col-md-6">
                        <div class="panel panel-default">
                            <div class="panel-body">
                                <?php
                                $verzenden = isset($_POST['verzenden']) ? $_POST['verzenden'] : '';


                                $company = isset($_POST['company']) ? $_POST['company'] : '';
                                $indusrtry = isset($_POST['industry']) ? $_POST['industry'] : '';
                                $owner = isset($_POST['owner']) ? $_POST['owner'] : '';
                                $projected = isset($_POST['projected']) ? $_POST['projected'] : '';
                                $comment = isset($_POST['comment']) ? $_POST['comment'] : '';
                                $date = date_create();
                                $timestamp=date_format($date, 'U');


                                if($verzenden)
                                {
                                    $db->query("INSERT INTO reporting_customers VALUES (name = '$company', industry = '$indusrtry',ownerid = '$owner',comment ='$comment', created_at ='$timestamp')");
                                    $result = $db->query("SELECT id FROM reporting_customers WHERE name='$company')");
                                    $row = $result->fetch_assoc();
                                    $db->query("INSERT INTO reporting_customer_turnover SET projected = '$projected' WHERE customerid = ".$row['id']); ?>

                                    <form action="#" method="post">
                                        <div class="form-group">
                                            <div class="panel panel-default">
                                                <div class="panel-heading">
                                                    <h3 class="panel-title">Edit Customer.</h3>
                                                </div>
                                                <div class="panel-body">
                                                    Customer saved successfully.
                                                </div>
                                            </div>
                                        </div>
                                        <button type="button" class="btn btn-success" onclick="location.href='overview.php'">Back</button>
                                    </form>
                                <?php }
                                else
                                {
                                    ?>

                                    <form action="#" method="post">

                                        <div class="form-group">
                                            <label for="LabelForInputCompany">Company name:</label>
                                            <input name="company" class="form-control">
                                        </div>
                                        <div class="form-group">
                                            <label for="LabelForInputIndustry">Industry:</label>
                                            <select id="industry" name="industry" class="form-control">
                                                <?php $sales = $db->query("SELECT * FROM reporting_account_industry group by name order by id;");
                                                while($row_sales = $sales->fetch_array())
                                                {
                                                    ?>
                                                    <option value="<?= $row_sales['id']?>"><?= $row_sales['name']?></option>
                                                <?php } ?>
                                            </select>
                                        </div>
                                        <div class="form-group">
                                            <label for="LabelForInputCompany">Owner:</label>
                                            <select id="owner" name="owner" class="form-control">
                                                <?php $sales = $db->query("SELECT * FROM reporting_sales_organization where name not like ''");
                                                while($row_sales = $sales->fetch_array())
                                                {
                                                    ?>
                                                    <option value="<?= $row_sales['id']?>"><?= $row_sales['name']?></option>
                                                <?php } ?>
                                            </select>
                                        </div>
                                        <div class="form-group">
                                            <label for="LabelForInputProjected">Target <?= date("Y"); ?>:</label>
                                            <input name="projected" class="form-control" value="<?php echo $projection;?>">
                                        </div>
                                        <div class="form-group">
                                            <label for="LabelForInputExtra">Enter your comment (max. 300 characters):</label>
                                            <textarea name="comment" class="form-control" rows="5"></textarea>
                                        </div>
                                        <button type="submit" class="btn btn-primary" name="verzenden" value="Save">Save</button>
                                        <button type="button" class="btn btn-default" onclick="location.href='overview.php'">Back</button>

                                    </form>
                                <?php
                                }
                                ?>
                            </div>
                        </div>
                    </div>
                    <div class="col-md-6">
                        <div class="panel panel-default">
                            <div class="panel-body">
                                <label for="LabelForInputCompany">Industry Defaults:</label>
                                <table class="table table-bordered table-condensed table-striped">
                                    <thead>
                                    <tr>
                                        <th>Industry</th>
                                        <th>Code</th>
                                        <th>Owner</th>
                                    </tr>
                                    </thead>
                                    <tbody>

                                    <?php $ownerquery = $db->query("SELECT i.name as industry, code, so.name as owner FROM reporting_account_industry AS i  LEFT JOIN reporting_sales_organization as so on so.id=i.ownerid group by i.name;");
                                    while($row_owner = $ownerquery->fetch_array())
                                    {
                                        ?>
                                        <tr>
                                            <td><?= $row_owner['industry']?></td>
                                            <td><?= $row_owner['code']?></td>
                                            <td><?= $row_owner['owner']?></td>
                                        </tr>
                                    <?php } ?>
                                    </tbody>
                                </table>
                                <?php

                                }?>
                            </div>
                        </div>
                    </div>

                </div>
            </div><!-- /.outer -->
        </div><!-- /#content -->
        <?php include($rootdir."includes/footer.php"); ?>
</body>
</html>