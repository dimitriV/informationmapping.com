<?php
$rootdir="../";
$page="PCourseNew";
include_once($rootdir."includes/connect.php");
include($rootdir."includes/header.php");

?>
<body>
<div class="bg-light" id="wrap">
    <div id="top">
        <!-- .navbar -->
        <?php include($rootdir."includes/navtop.php"); ?>
    </div><!-- /#top -->
    <?php include($rootdir."includes/navleft.php"); ?>
    <div id="content">
        <div class="outer">
            <div class="inner">
                <div class="inner-head">
                    <h1>Courses Overview</h1>
                </div>
                <?php
                if (!isset($_SESSION["username"])){
                    echo $niet_ingelogd;
                }
                elseif($permission != "1"){
                    echo $geen_toegang;
                }else{ ?>
                    <div class="row">
                        <div class="col-lg-12">

                        </div>
                    </div>
                <?php }?>
            </div><!-- /.inner -->
        </div><!-- /.outer -->
    </div><!-- /#content -->
    <script>
        $(document).ready(function() {
            $('#dataTables-courses').dataTable();
        });
    </script>
    <?php include($rootdir."includes/footer.php"); ?>
</body>
</html>