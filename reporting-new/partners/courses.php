
<?php
$rootdir="../";
$page="PCourses";
include_once($rootdir."includes/connect.php");
include($rootdir."includes/header.php");


$action = isset($_POST['action']) ? trim($_POST['action']) : '';
$action = isset($_GET['action']) ? trim($_GET['action']) : '';


if($action=="delete"){
    $course_id=$_GET['course_id'];

    $course_SQL_del="DELETE FROM reporting_courses WHERE course_id=$course_id";
    $bool= $db->query($course_SQL_del);
    if($bool==1) echo "<div id='adminok'>The course has been deleted.</div>";
    if($bool<>1) echo "<div id='adminnok'>An error occurred when trying to delete the course.</div>";
}

if($action=="insert"){

    $course_name=$db->real_escape_string($_POST['course_name']);
    $active_default = "yes";
    $course_SQL_insert="INSERT INTO reporting_courses (course_name, active) VALUES ('$course_name','$active_default')";
    $bool= $db->query($course_SQL_insert);
    if($bool==1) echo "<div id='adminok'>The course has been added.</div>";
    if($bool<>1) echo "<div id='adminnok'>An error occurred when trying to add the course.</div>";
}
if($action=="save"){

    $course_name=$_POST['course_name'];
    $active_default = "yes";
    $course_SQL_insert="INSERT INTO reporting_courses (course_name, active) VALUES ('$course_name','$active_default')";
    $bool= $db->query($course_SQL_insert);
    if($bool==1) echo "<div id='adminok'>The course has been added.</div>";
    if($bool<>1) echo "<div id='adminnok'>An error occurred when trying to add the course.</div>";
}


if($action=="update"){

    $course_name=$_POST['course_name'];
    $course_id=$_POST['course_id'];
    $active=$_POST['active'];

    $course_SQL_update="UPDATE reporting_courses SET course_name='$course_name', active='$active' WHERE course_id='$course_id'";
    $bool= $db->query($course_SQL_update);
    if($bool==1) echo "<div id='adminok'>The course has been saved.</div>";
    if($bool<>1) echo "<div id='adminnok'>An error occurred when trying to save the course.</div>";
}

$course_SQL="SELECT * FROM reporting_courses ORDER BY active DESC, course_name ASC";
$course_result = $db->query($course_SQL);

?>
<body>
<div class="bg-light" id="wrap">
    <div id="top">
        <!-- .navbar -->
        <?php include($rootdir."includes/navtop.php"); ?>
    </div><!-- /#top -->
    <?php include($rootdir."includes/navleft.php"); ?>
    <div id="content">
        <div class="outer">
            <div class="inner">
                <div class="inner-head">
                    <h1>Courses Overview</h1>
                </div>
                <?php
                if (!isset($_SESSION["username"])){
                    echo $niet_ingelogd;
                }
                elseif($permission != "1"){
                    echo $geen_toegang;
                }else{ ?>
                    <div class="row">
                        <div class="col-lg-12">
                            <div class="box">
                                <header>
                                    <div class="icons">
                                        <i class="fa fa-book"></i>
                                    </div>
                                    <h5>Available courses:</h5>
                                    <div><a  href="course_new.php" class="add pull-right">Add Course <i class="fa fa-plus-square fa-lg"></i></a></div>

                                </header>
                                <div id="collapse4" class="body">
                                    <form method="post">
                                    <table class="table table-bordered table-condensed table-hover table-striped" id="dataTables-courses">
                                        <thead>
                                        <tr>
                                            <th>Active</th>
                                            <th>Coursename</th>
                                            <th>Edit</th>
                                            <th>Delete</th>
                                        </tr>
                                        </thead>
                                        <tbody>
                                        <?php
                                        while($course = $course_result->fetch_array()){ ?>
                                            <tr <?php if($course['active']=="no") { echo "class='danger'"; } ?>>
                                                <td><?php echo $course['active'] ?></td>
                                                <td><?php echo $course['course_name'] ?></td>
                                                <td><a href="course_edit.php?course_id=<?php echo $course['course_id'] ?>"><i class="fa fa-edit fa-2x"></i></a></td>
                                                <td><a href="courses.php?course_id=<?php echo $course['course_id'] ?>&action=delete" onClick="return confirm('Are you sure you want to delete this course?')"><i class="fa fa-times text-danger fa-2x"></i></a></td></tr>
                                        <?php  } ?>
                                        </tbody>
                                    </table>
                                    </form>
                                </div>
                            </div>
                        </div>
                    </div>
                <?php }?>
            </div><!-- /.inner -->
        </div><!-- /.outer -->
    </div><!-- /#content -->
    <script type="text/javascript" class="init">
        $(document).ready(function() {
            var t = $('#dataTables-courses').DataTable({
                "pageLength": 16,
                "order": [ 0    , 'desc' ]
            });
                    } );
        } );
    </script>

    <?php include($rootdir."includes/footer.php"); ?>
</body>
</html>