<?php
$rootdir="../";
$page="PEdit";
include_once($rootdir."includes/connect.php");
include($rootdir."includes/header.php");

$SQL_partner="SELECT * FROM reporting_partners WHERE partner_id=" . $_GET['partner_id'];
$partner_result=$db->query($SQL_partner);
$partner = $partner_result->fetch_array();
$db->close();
?>
<body>
<div class="bg-light" id="wrap">
    <div id="top">
        <!-- .navbar -->
        <?php include($rootdir."includes/navtop.php"); ?>
    </div><!-- /#top -->
    <?php include($rootdir."includes/navleft.php"); ?>
    <div id="content">
        <div class="outer">
            <div class="inner">
                <div class="inner-head">
                    <h2>Partner List <br>
                        <div class="smalls"><?php echo $aantal_part; ?> active /
                            <?php echo $aantal_inpart; ?> inactive partners</div></h2>

                </div>
                <?php
                if (!isset($_SESSION["username"])){
                    echo $niet_ingelogd;
                }
                elseif($permission != "1"){ // make it accessible with function admin and it
                    echo $geen_toegang;
                }else{
                ?><div class="row">
                    <div class="col-lg-6">
                        <form role="form" action="list.php?action=update" method="post">
                            <div class="panel panel-default">
                                <div class="panel-heading">
                                    <h3 class="panel-title">Edit Partner details</h3>
                                </div>
                                <div class="panel-body">
                                    <div class="form-group">
                                        <label>First name</label>
                                        <input type="text" name="partner_first" class="form-control" value="<?php echo $partner['partner_first'] ?>" >
                                    </div>
                                    <div class="form-group">
                                        <label>Last name</label>
                                        <input type="text" name="partner_last" class="form-control" value="<?php echo $partner['partner_last'] ?>" >
                                    </div>
                                    <div class="form-group">
                                        <label>Email</label>
                                        <input type="email" name="partner_email" class="form-control" value="<?php echo $partner['partner_email'] ?>" >
                                    </div>
                                    <div class="form-group">
                                        <label>Company</label>
                                        <input type="text" name="partner_company" class="form-control" value="<?php echo $partner['partner_company'] ?>" >
                                    </div>
                                    <div class="form-group">
                                        <label>User Name</label>
                                        <input type="text" name="partner_user" class="form-control" value="<?php echo $partner['partner_user'] ?>" >
                                    </div>
                                    <div class="form-group">
                                        <label>Password</label>
                                        <input type="password" name="partner_pw" class="form-control" value="<?php echo $partner['partner_pw'] ?>" >
                                    </div>
                                    <div class="form-group">
                                        <label>Commitment Fee Training</label>
                                        <input type="text" name="cftraining" class="form-control" value="<?php echo $partner['cftraining'] ?>" >
                                    </div>
                                    <div class="form-group">
                                        <label>Commitment Fee Services</label>
                                        <input type="text" name="cfservices"  class="form-control" value="<?php echo $partner['cfservices'] ?>" >
                                    </div>
                                    <div class="form-group col-lg-6">
                                        <label>Sees Revenue field</label>
                                        <select name="rev_vis" class="form-control">
                                            <?
                                            if ($partner['rev_vis']=="yes") {
                                                echo "<option value='yes' selected='selected'>yes</option>
                                                <option value='no'>no</option>";
                                            } else {
                                                echo "<option value='no' selected='selected'>no</option>
			                                    <option value='yes'>yes</option>";
                                            }
                                            ?>
                                        </select>
                                    </div>
                                    <div class="form-group col-lg-6">
                                        <label>Sees Commitment Fee block</label>
                                        <select name="cf_vis" class="form-control">
                                            <?
                                            if ($partner['cf_vis']=="yes") {
                                                echo "<option value='yes' selected='selected'>yes</option>
                                                <option value='no'>no</option>";
                                            } else {
                                                echo "<option value='no' selected='selected'>no</option>
			                                    <option value='yes'>yes</option>";
                                            }
                                            ?>
                                        </select>
                                    </div>
                                    <div class="form-group col-lg-6 ">
                                        <label>Sees Services block</label>
                                        <select name="serv_vis" class="form-control">
                                            <?
                                            if ($partner['serv_vis']=="yes") {
                                                echo "<option value='yes' selected='selected'>yes</option>
                                                <option value='no'>no</option>";
                                            } else {
                                                echo "<option value='no' selected='selected'>no</option>
			                                    <option value='yes'>yes</option>";
                                            }
                                            ?>
                                        </select>
                                    </div>
                                    <div class="form-group col-lg-6">
                                        <label>Sees Publimap block</label>
                                        <select name="publimap_vis" class="form-control">
                                            <?
                                            if ($partner['publimap_vis']=="yes") {
                                                echo "<option value='yes' selected='selected'>yes</option>
                                                <option value='no'>no</option>";
                                            } else {
                                                echo "<option value='no' selected='selected'>no</option>
			                                    <option value='yes'>yes</option>";
                                            }
                                            ?>
                                        </select>
                                    </div>
                                    <div class="form-group col-lg-6">
                                        <label>Sees iMapper block</label>
                                        <select name="imapper_vis" class="form-control">
                                            <?
                                            if ($partner['imapper_vis']=="yes") {
                                                echo "<option value='yes' selected='selected'>yes</option>
                                                <option value='no'>no</option>";
                                            } else {
                                                echo "<option value='no' selected='selected'>no</option>
			                                    <option value='yes'>yes</option>";
                                            }
                                            ?>
                                        </select>
                                    </div>
                                    <div class="form-group col-lg-6">
                                        <label>Is active</label>
                                        <select name="active" class="form-control">
                                            <?
                                            if ($partner['active']=="yes") {
                                                echo "<option value='yes' selected='selected'>yes</option>
			                                        <option value='no'>no</option>";
                                            } else {
                                                echo "<option value='no' selected='selected'>no</option>
			                                         <option value='yes'>yes</option>";
                                            }

                                            ?>
                                        </select>
                                    </div>
                                    <div class="form-group col-lg-12 ">
                                        <button type="submit" class="btn btn-primary btn-lg" >Save</button>
                                        <input type="hidden" name="partner_id" value="<?php echo $partner['partner_id'] ?>">
                                        <input type="hidden" name="action" value="update">
                                    </div>
                        </form>
                    </div>
                </div>

            </div>
            <?php }?>
        </div><!-- /.inner -->
    </div><!-- /.outer -->
</div><!-- /#content -->
<script>
    $(document).ready(function() {
        $('#dataTables-users').dataTable();
    });
</script>
<?php include($rootdir."includes/footer.php"); ?>
</body>
</html>