<?php
$rootdir="../";
$page="PReportDetails";
include_once($rootdir."includes/connect.php");
include($rootdir."includes/header.php");

$report_id=$_GET['report_id'];
$SQL_report="SELECT * FROM reporting_report WHERE report_id=" . $_GET['report_id'];
$report_result=$db->query($SQL_report);
$report=$report_result->fetch_array();
$report = $db->query("SELECT currency FROM reporting_report as r  INNER JOIN reporting_partners as p on r.partner_id=p.partner_id WHERE r.report_id='$report_id'");
$result=$report->fetch_array();
$currency= $result['currency'];
if($currency=='USD'){
    $currency='$ ';
}else $currency='€ '
?>
<body>
<div class="bg-light" id="wrap">
<div id="top">
    <!-- .navbar -->
    <?php include($rootdir."includes/navtop.php"); ?>
</div><!-- /#top -->
<?php include($rootdir."includes/navleft.php"); ?>
<div id="content">
<div class="outer">
<div class="inner">
<div class="inner-head">
    <h1>Partner Report</h1>
</div>
<?php
if (!isset($_SESSION["username"])){
    echo $niet_ingelogd;
}
elseif($permission != "1"){
    echo $geen_toegang;
}else{ ?>
    <div class="col-lg-12">
    <div class="panel panel-default">
    <div class="panel-heading">
        <div class="btn-group pull-right" role="group">
            <button class="btn btn-primary " type="button" onclick="location.href='report.php'"><i class="fa fa-angle-left"></i> Back</button>
            <button class="btn btn-primary " type="button" onclick="javascript:window.print()"><i class="fa fa-print"></i> Print</button>
        </div>
        <h3 class="panel-title"><?php echo $report['company'] ?> Report Details for <?php echo $report['month'] ?> <?php echo $report['year'] ?> </h3>
    </div>
    <?

    $SQL_person="SELECT r.report_id, r.cfservices, r.cftraining, r.comments, r.consult, r.writing, r.publimap_rev, r.imapper_rev, r.company, r.first, r.last, r.email, r.month, r.year, r.submit_time FROM reporting_report as r, reporting_partners as p WHERE r.partner_id=p.partner_id AND report_id=" . $_GET['report_id'];
    $person_result=$db->query($SQL_person);
    $person=$person_result->fetch_array();

    if(isset($_POST['action'])) $action=$_POST['action'];
    ?>
    <div class="row">
        <div class="panel-body">
            <div class="col-lg-8">
                <div class="panel panel-default">
                    <div class="panel-heading">
                        <a onclick="EditFunction()" class="pull-right"><i id="edit" class="fa fa-pencil-square-o fa-lg"></i></a>
                        <script>
                            function EditFunction() {
                                $( ":input" ).attr("type", "text");
                                $( "select" ).removeClass( "hidden" );
                                $( "#0" ).addClass( "hidden" );
                                $( "#1" ).addClass( "hidden" );
                                $( "#2" ).addClass( "hidden" );
                                $( "#3" ).addClass( "hidden" );
                                $( "#4" ).addClass( "hidden" );
                                if($('#edit').attr('class') == 'fa fa-pencil-square-o fa-lg'){
                                    $( "#edit" ).removeClass( "fa fa-pencil-square-o fa-lg" );
                                    $( "#edit" ).addClass( "fa fa-floppy-o fa-lg" );
                                }else
                                {
                                    $( "#target" ).submit();
                                }

                            }
                        </script>
                        <h2 class="panel-title">Report submitted by:
                            <?php
                            if($action=="update"){
                                $company=$_POST['company'];
                                $first=$_POST['first'];
                                $last=$_POST['last'];
                                $email=$_POST['email'];
                                $report_id=$_POST['report_id'];
                                $month=$_POST['month'];
                                $year=$_POST['year'];

                                $person_SQL_update="UPDATE reporting_report SET company='$company', first='$first',last='$last',email='$email', month='$month', year='$year' WHERE report_id='$report_id'";
                                $bool=$db->query($person_SQL_update);
                                if($bool==1) echo "<span class='label label-info'>The changes have been saved.</span></div>";
                            } ?></h2>
                    </div>
                    <table class="table table-bordered table-condensed table-hover table-striped">
                        <thead>
                        <tr>
                            <th>Company</th>
                            <th>First Name</th>
                            <th>Last Name</th>
                            <th>E-mail</th>
                            <th>Period</th>
                            <th>Submitted on</th>
                        </tr>
                        </thead>
                        <tbody>
                        <tr>
                            <form id="target" action="<?php echo $_SERVER['PHP_SELF']; ?>?report_id=<?php echo $person['report_id'] ?>" method="post" >
                                <td><select name="company" class="form-control hidden">
                                        <?php
                                        $partner_SQL="SELECT * FROM reporting_partners WHERE active='yes' ORDER BY partner_company";
                                        $partner_result=$db->query($partner_SQL);
                                        while($partner=$partner_result->fetch_array()){
                                            ?>
                                            <option value="<?php echo $partner['partner_company'] ?>" <?php if(($partner['partner_company'])==$person['company']) { echo "selected='selected'";} ?>><?php echo $partner['partner_company'] ?></option>
                                        <?php
                                        }
                                        ?>
                                    </select><span id="0" class="visible"><?php if(!empty($company)) { echo $company; } else echo $person['company'] ?></span></td>
                                <td><input type="hidden" class="form-control" name="first" value="<?php echo $person['first'] ?>"/><span id="1" class="visible"><?php if(!empty($first)) { echo $first; } else echo $person['first'] ?></span></td>
                                <td><input type="hidden" class="form-control" name="last" value="<?php echo $person['last'] ?>"/><span  id="2" class="visible"><?php if(!empty($last)) { echo $last; } else echo $person['last'] ?></span></td>
                                <td><input type="hidden" class="form-control" name="email" value="<?php echo $person['email'] ?>"/><span  id="3"  class="visible"><?php if(!empty($email)) { echo $email; } else echo $person['email'] ?></span></td>
                                <td><select name="month" class="form-control hidden">
                                        <?php
                                        $partner_SQL="SELECT * FROM reporting_months";
                                        $partner_result=$db->query($partner_SQL);
                                        while($partner=$partner_result->fetch_array()){
                                            ?>
                                            <option value="<?php echo $partner['month'] ?>" <?php if(($partner['month'])==$person['month']) { echo "selected='selected'";} ?>><?php echo $partner['month'] ?></option>
                                        <?php
                                        }
                                        ?>
                                    </select>
                                    <select name="year" class="form-control hidden">
                                        <?php
                                        $partner_SQL="SELECT * FROM reporting_year";
                                        $partner_result=$db->query($partner_SQL);
                                        while($partner=$partner_result->fetch_array()){
                                            ?>
                                            <option value="<?php echo $partner['year'] ?>" <?php if(($partner['year'])==$person['year']) { echo "selected='selected'";} ?>><?php echo $partner['year'] ?></option>
                                        <?php
                                        }
                                        ?>
                                    </select>
                                    <span  id="4"  class="visible"><?php if(!empty($period)) { echo $period; } else echo $person['month'],' ', $person['year']?></span>
                                </td>
                                <td><span class="visible"><?php echo $person['submit_time'] ?></span></td>
                                <input class="hidden" type="hidden" name="action" value="update">
                                <input class="hidden" type="hidden" name="report_id" value="<?php echo $person['report_id'] ?>">
                            </form>
                        </tr>
                        </tbody>
                    </table>
                </div>
            </div>
        </div>
    </div>
    <div class="row">
        <div class="panel-body">
            <div class="col-lg-8">
                <div class="panel panel-default">
                    <div class="panel-heading">
                        <h2 class="panel-title">Classroom training:</h2>
                    </div>
                    <table class="table table-bordered table-condensed table-hover table-striped">
                        <thead>
                        <tr>
                            <th>Title</th>
                            <th>Starting date</th>
                            <th>Duration</th>
                            <th>Participants</th>
                            <th>Type</th>
                            <th>CLP ID</th>
                            <th>Revenue</th>
                        </tr>
                        </thead>
                        <tbody>

                        <?php

                        $courses_SQL="SELECT * FROM reporting_report_courses WHERE report_id='$report_id'";
                        $courses_result=$db->query($courses_SQL);
                        while($courses=$courses_result->fetch_array()){

                            ?>
                            <tr>
                                <td><?php echo $courses['name'] ?></td>
                                <td><?php echo $courses['start_date'] ?></td>
                                <td><?php echo $courses['duration'] ?> days</td>
                                <td><?php echo $courses['participants'] ?></td>
                                <td><?php echo $courses['type'] ?></td>
                                <td><?php echo $courses['clp_id'] ?></td>
                                <td><?php echo $courses['revenue'] ?></td>
                            </tr>
                        <?php }?>

                        </tbody>
                    </table>
                </div>
            </div>
        </div>
    </div>
    <div class="row">
        <div class="col-sm-4">
            <div class="panel-body">
                <div class="panel panel-default">
                    <div class="panel-heading">
                        <h2 class="panel-title">Consultancy and writing services:</h2>
                    </div>
                    <table class="table table-bordered table-condensed table-hover table-striped">
                        <thead>
                        <tr>
                            <th>Consultancy</th>
                            <th>Writing services</th>
                        </tr>
                        </thead>
                        <tbody>
                        <tr>
                            <td><?php echo $currency, $person['consult'] ?></td>
                            <td><?php echo $currency,$person['writing'] ?></td>
                        </tr>
                        </tbody>
                    </table>
                </div>
            </div>
        </div>
        <div class="col-sm-4">
            <div class="panel-body">
                <div class="panel panel-default">
                    <div class="panel-heading">
                        <h2 class="panel-title">Monthly commitment fee:</h2>
                    </div>
                    <table class="table table-bordered table-condensed table-hover table-striped">
                        <thead>
                        <tr>
                            <th>Training(Q)</th>
                            <th>Services(M)</th>
                        </tr>
                        </thead>
                        <tbody>
                        <tr>
                            <td><?php echo $currency,$person['cftraining'] ?></td>
                            <td><?php echo $currency,$person['cfservices'] ?></td>
                        </tr>
                        </tbody>
                    </table>
                </div>
            </div>
        </div>
    </div>
    <div class="row">

            <div class="col-sm-8">
                <div class="panel-body">
                    <div class="panel panel-default">
                        <div class="panel-heading">
                            <h2 class="panel-title">Software revenue / Comments</h2>
                        </div>

                        <table class="table table-bordered table-condensed table-hover table-striped">
                            <thead>
                            <tr>
                                <th>Publimap revenue</th>
                                <th>iMapper revenue</th>
                                <th>Additional comments</th>
                            </tr>
                            </thead>
                            <tbody>
                            <tr>
                                <td><?php echo $currency, $person['publimap_rev'] ?></td>
                                <td><?php echo $currency, $person['imapper_rev'] ?></td>
                                <td><?php echo $person['comments'] ?></td>
                            </tr>
                            </tbody>
                        </table>
                </div>
            </div
        </div>
    </div>
    </div>
    </div>




<?php }?>

</div><!-- /.inner -->
</div><!-- /.outer -->
</div><!-- /#content -->
<script>
    $(document).ready(function() {
        $('#dataTables-users').dataTable();
    });
</script>
<?php include($rootdir."includes/footer.php"); ?>
</body>
</html>