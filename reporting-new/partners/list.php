<?php
$rootdir="../";
$page="PList";
include_once($rootdir."includes/connect.php");
include($rootdir."includes/header.php");

$action = isset($_POST['action']) ? trim($_POST['action']) : '';
$action = isset($_GET['action']) ? trim($_GET['action']) : '';

if($action=="delete"){
    $partner_id=$_GET['partner_id'];

    $partner_SQL_del="DELETE FROM reporting_partners WHERE partner_id=$partner_id";
    $bool=$db->query($partner_SQL_del);
    if($bool==1) echo "<div id='adminok'>The Partner has been deleted.</div>";
    if($bool<>1) echo "<div id='adminnok'>An error occurred when trying to delete the Partner.</div>";
}

if($action=="insert"){
    $partner_first=$_POST['partner_first'];
    $partner_last=$_POST['partner_last'];
    $partner_email=$_POST['partner_email'];
    $partner_company=$_POST['partner_company'];
    $partner_user=$_POST['partner_user'];
    $partner_pw=$_POST['partner_pw'];
    $rev_vis=$_POST['rev_vis'];
    $cf_vis=$_POST['cf_vis'];
    $serv_vis=$_POST['serv_vis'];
    $cfservices=$_POST['cfservices'];
    $cftraining=$_POST['cftraining'];
    $imapper_vis=$_POST['imapper_vis'];
    $publimap_vis=$_POST['publimap_vis'];
    $active=$_POST['active'];
    $partner_SQL_insert="INSERT INTO reporting_partners (partner_first,partner_last,partner_email,partner_company,partner_user,partner_pw,rev_vis,cf_vis,serv_vis,cfservices,cftraining,imapper_vis,publimap_vis,active) VALUES ('$partner_first','$partner_last','$partner_email','$partner_company','$partner_user','$partner_pw','$rev_vis','$cf_vis','$serv_vis','$cfservices','$cftraining','$imapper_vis','$publimap_vis','$active')";
    $bool=$db->query($partner_SQL_insert);
    if($bool==1) echo "<div id='adminok'>The Partner has been added.</div>";
    if($bool<>1) echo "<div id='adminnok'>An error occurred when trying to add the Partner.</div>";
}

if($action=="update"){

    $partner_first=$_POST['partner_first'];
    $partner_last=$_POST['partner_last'];
    $partner_email=$_POST['partner_email'];
    $partner_company=$_POST['partner_company'];
    $partner_id=$_POST['partner_id'];
    $partner_user=$_POST['partner_user'];
    $partner_pw=$_POST['partner_pw'];
    $rev_vis=$_POST['rev_vis'];
    $cf_vis=$_POST['cf_vis'];
    $serv_vis=$_POST['serv_vis'];
    $cfservices=$_POST['cfservices'];
    $cftraining=$_POST['cftraining'];
    $imapper_vis=$_POST['imapper_vis'];
    $publimap_vis=$_POST['publimap_vis'];
    $active=$_POST['active'];

    $partner_SQL_update="UPDATE reporting_partners SET partner_first='$partner_first',partner_last='$partner_last',partner_email='$partner_email',partner_company='$partner_company',partner_user='$partner_user',partner_pw='$partner_pw',rev_vis='$rev_vis',cf_vis='$cf_vis',serv_vis='$serv_vis',cftraining='$cftraining',cfservices='$cfservices',imapper_vis='$imapper_vis',publimap_vis='$publimap_vis',active='$active' WHERE partner_id='$partner_id'";
    $bool=$db->query($partner_SQL_update);
    if($bool==1) echo "<div id='adminok'>The Partner has been saved.</div>";
    if($bool<>1) echo "<div id='adminnok'>An error occurred when trying to save the Partner.</div>";
}
if(empty($_GET['actief']))
{
    $partner_SQL="SELECT * FROM reporting_partners WHERE active='yes' ORDER by partner_company ASC";
}else{
$partner_SQL="SELECT * FROM reporting_partners WHERE active='".$_GET['actief']."' ORDER by partner_company ASC";
}
$partner_result=$db->query($partner_SQL);

$activepart = $db->query("SELECT * FROM reporting_partners WHERE active='yes'");
$inactivepart = $db->query("SELECT * FROM reporting_partners WHERE active='no'");
$aantal_part = $activepart->num_rows;
$aantal_inpart = $inactivepart->num_rows;
?>
<body>
<div class="bg-light" id="wrap">
    <div id="top">
        <!-- .navbar -->
        <?php include($rootdir."includes/navtop.php"); ?>
    </div><!-- /#top -->
    <?php include($rootdir."includes/navleft.php"); ?>
    <div id="content">
        <div class="outer">
            <div class="inner">
                <div class="inner-head">
                    <h2>Partner List <br>
                        <div class="smalls"><?php echo $aantal_part; ?> active /
                            <?php echo $aantal_inpart; ?> inactive partners</div></h2>

                </div>
                <?php
                if (!isset($_SESSION["username"])){
                    echo $niet_ingelogd;
                }
                elseif($permission != "1"){ // make it accessible with function admin and it
                    echo $geen_toegang;
                }else{
                ?>
                <div class="row">
                    <div class="col-lg-12">
                        <div class="box">
                            <header>
                                <h5><a href="list.php?actief=yes"<i class="fa fa-angle-right"></i> Active Partners</a> / <a href="list.php?actief=no">Inactive Partners</a></h5>
                                <div><a href="new.php" class="add pull-right">Add Partner <i class="fa fa-plus-square fa-lg"></i></a></div>
                            </header>
                            <div id="collapse4" class="body">
                                <table class="table table-bordered table-condensed table-hover table-striped" id="dataTables-users">
                                    <thead>
                                    <tr>
                                        <th>Company name</th>
                                        <th>User name</th>
                                        <th>Password</th>
                                        <th>CF Tr.</th>
                                        <th>CF Serv</th>
                                        <th>View rights</th>
                                        <th>Edit</th>
                                        <th>Delete</th>
                                    </tr>
                                    </thead>
                                    <?php  if(isset($_GET['year'])){ $jaar_voor_query = $_GET['year']; }else { $jaar_voor_query = $current_year; } ?>
                                    <tbody>
                                    <?php
                                    while($partner = $partner_result->fetch_array()){
                                        ?>

                                        <tr <?php if($partner['active']=="no") { echo "class='text-danger'"; } ?>>
                                            <td><?php echo $partner['partner_company'] ?></td>
                                            <td><?php echo $partner['partner_user'] ?></td>
                                            <td><?php echo $partner['partner_pw'] ?></td>
                                            <td><?php if(!empty($partner['cftraining'])){ echo "&euro;" . $partner['cftraining']; } ?></td>
                                            <td><?php if(!empty($partner['cfservices'])){ echo "&euro;" . $partner['cfservices']; } ?></td>
                                            <td>
                                                <?php if($partner['partner_id']!='4') { ?>
                                                    <div class="dropdown">
                                                        <button class="btn btn-info dropdown-toggle" type="button" id="dropdownMenu1" data-toggle="dropdown" aria-expanded="true">
                                                            Details
                                                            <span class="caret"></span>
                                                        </button>
                                                        <ul class="dropdown-menu" role="menu" aria-labelledby="dropdownMenu1">
                                                            <li role="presentation"><a role="menuitem" tabindex="-1" >Revenue field: <strong><?php echo $partner['rev_vis'] ?></strong></a></li>
                                                            <li role="presentation"><a role="menuitem" tabindex="-1" >Commitment Fee block: <strong><?php echo $partner['cf_vis'] ?></strong></a></li>
                                                            <li role="presentation"><a role="menuitem" tabindex="-1" >Services Block: <strong><?php echo $partner['serv_vis'] ?></strong></a></li>
                                                            <li role="presentation"><a role="menuitem" tabindex="-1" >iMapper Revenue block: <strong><?php echo $partner['imapper_vis'] ?></strong></a></li>
                                                            <li role="presentation"><a role="menuitem" tabindex="-1" >Publimap Revenue block: <strong><?php echo $partner['publimap_vis'] ?></strong></a></li>
                                                        </ul>
                                                    </div>
                                                          <?php } ?>
                                        </td>
                                            <td><?php if($partner['partner_id']!="4") { ?><a href=edit.php?partner_id=<?php echo $partner['partner_id'] ?>><i class="fa fa-edit fa-2x"></i></a><?php } ?></td>
                                            <td><?php if($partner['partner_id']!="4") { ?><a href=list.php?partner_id=<?php echo $partner['partner_id'] ?>&action=delete onClick="return confirm('Are you sure you want to delete this Partner?')"><i class="fa fa-times text-danger fa-2x"></i></a><?php } ?></td>
                                        </tr>

                                    <?php
                                    }
                                    $db->close();
                                    ?>
                                    </tbody>
                                </table>
                            </div>
                        </div>
                    </div>
                    <?php }?>
                </div><!-- /.inner -->
            </div><!-- /.outer -->
        </div><!-- /#content -->
        <script>
            $(document).ready(function() {
                $('#dataTables-users').dataTable();
            });
        </script>
        <?php include($rootdir."includes/footer.php"); ?>
</body>
</html>