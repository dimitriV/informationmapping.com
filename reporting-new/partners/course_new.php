<?php
$rootdir="../";
$page="PCourseNew";
include_once($rootdir."includes/connect.php");
include($rootdir."includes/header.php");

?>
<body>
<div class="bg-light" id="wrap">
    <div id="top">
        <!-- .navbar -->
        <?php include($rootdir."includes/navtop.php"); ?>
    </div><!-- /#top -->
    <?php include($rootdir."includes/navleft.php"); ?>
    <div id="content">
        <div class="outer">
            <div class="inner">
                <div class="inner-head">
                    <h1>Courses Overview</h1>
                </div>
                <?php
                if (!isset($_SESSION["username"])){
                    echo $niet_ingelogd;
                }
                elseif($permission != "1"){
                    echo $geen_toegang;
                }else{ ?>
                <div class="row">
                    <div class="col-md-6">
                        <div class="panel panel-default">
                            <div class="panel-body">
                                <?php

                                $verzenden = isset($_POST['verzenden']) ? $_POST['verzenden'] : '';

                                $courseName = isset($_POST['courseName']) ? $_POST['courseName'] : '';
                                $course_trainingkey = isset($_POST['course_trainingkey']) ? $_POST['course_trainingkey'] : '';
                                $active = isset($_POST['active']) ? $_POST['active'] : '';
                               



                                if($verzenden)
                                {
                                    $query= $db->query("INSERT INTO reporting_courses (course_name,course_trainingkey,active)
                              VALUES ('$courseName','$course_trainingkey','$active'");

                                    ?>

                                    <form action="#" method="post">
                                        <div class="form-group">
                                            <div class="panel panel-default">
                                                <div class="panel-heading">
                                                    <h3 class="panel-title">Save License Server Details.</h3>
                                                </div>
                                                <div class="panel-body">
                                                    Server license saved successfully.
                                                </div>
                                            </div>
                                        </div>
                                        <button type="button" class="btn btn-success" onclick="location.href='license_server.php'">Back</button>
                                    </form>
                                <?php }
                                else
                                {
                                    ?>

                                    <form action="#" method="post">

                                        <div class="form-group">
                                            <label for="LabelForInputCompany">Course name:</label>
                                            <input type="text" name="courseName" class="form-control">
                                        </div>
                                        <div class="form-group">
                                            <label for="LabelForInputIndustry">Course identifier:</label>
                                            <select name="course_trainingkey" class="form-control">
                                                <?php

                                                $result = $db->query("SELECT course_trainingkey FROM reporting_courses order by course_id asc");
                                                while($row = $result->fetch_row()){
                                                    $usedtrainingkeys[] = $row;
                                                }
                                                for ($counter = 10; $counter <= 99; $counter++) {
                                                    if (in_array($counter,$usedtrainingkeys))
                                                    {
                                                        echo "<option value='$counter'>$counter</option>";
                                                    }

                                                }
                                                ?>
                                            </select>
                                        </div>
                                        <div class="form-group">
                                            <label for="LabelForInputIndustry">Active: <?php echo var_dump($usedtrainingkeys);?></label>
                                            <select id="active" name="active" class="form-control">
                                                    <option value="yes">yes</option>
                                                    <option value="no">no</option>
                                            </select>
                                        </div>
                                        <button type="submit" class="btn btn-primary" name="verzenden" value="Save">Save</button>
                                        <button type="button" class="btn btn-default" onclick="location.href='courses.php'">Back</button>

                                    </form>
                                <?php
                                 }
                                ?>
                            </div>
                        </div>
                    </div>
                </div>
                <?php }?>
            </div><!-- /.inner -->
        </div><!-- /.outer -->
    </div><!-- /#content -->
    <script>
        $(document).ready(function() {
            $('#dataTables-courses').dataTable();
        });
    </script>
    <?php include($rootdir."includes/footer.php"); ?>
</body>
</html>