<?php
$rootdir="../";
$page="POrder";
$_SESSION["file"] = $page;

include_once($rootdir."includes/connect.php");
include($rootdir."includes/header.php");

$activepart = $db->query("SELECT * FROM reporting_partners WHERE active='yes'");
$inactivepart = $db->query("SELECT * FROM reporting_partners WHERE active='no'");
$aantal_part = $activepart->num_rows;
$aantal_inpart = $inactivepart->num_rows;
if(isset($_GET['action'])) $action=$_GET['action'];
if(isset($_POST['action'])) $action=$_POST['action'];

?>
<script>
    $(document).ready(function() {
        $( "#exp" ).click(function() {
            $("#sub").submit();
        });
    });
</script>
<body>

<div class="bg-light" id="wrap">
    <div id="top">
        <!-- .navbar -->
        <?php include($rootdir."includes/navtop.php"); ?>
    </div><!-- /#top -->
    <?php include($rootdir."includes/navleft.php"); ?>
    <div id="content">
        <div class="outer">
            <div class="inner">
                <div class="inner-head">
                    <h2>Partner Orders report <br>
                        <div class="smalls"><?php echo $aantal_part; ?> active /
                            <?php echo $aantal_inpart; ?> inactive partners</div></h2>

                </div>
                <?php
                if (!isset($_SESSION["username"])){
                    echo $niet_ingelogd;
                }
                elseif($permission != "1"){
                    echo $geen_toegang;
                }else{
                    $current_year = date("Y");
                    $companyid = isset($_POST['choice_partner']) ? $_POST['choice_partner'] : '';
                    $month = isset($_POST['choice_month']) ? trim($_POST['choice_month']) : '';
                    $year = isset($_POST['choice_year']) ? $_POST['choice_year'] : $current_year;
                    $verzenden = isset($_POST['verzenden']) ? trim($_POST['verzenden']) : '';
                    ?>
                    <div class="row">
                        <div class="col-lg-8">
                            <div class="panel panel-default">
                                <div class="panel-heading">
                                    <h3 class="panel-title">Please choose a partner and specify a year</h3>
                                </div>
                                <div class="panel-body">
                                    <form method="post" action="">
                                        <div class="form-group col-lg-4">
                                            <select role="form" name="choice_partner" class="form-control">
                                                <option value="">- Please select a partner-</option>
                                                <?php
                                                $partner_SQL="SELECT * FROM reporting_partners WHERE active='yes' ORDER BY partner_company ASC";
                                                $partner_result=$db->query($partner_SQL);
                                                while($partner= $partner_result->fetch_array()){ ?>
                                                    <option value="<?php echo $partner['partner_id'];  ?>" <?php if($companyid==$partner['partner_id']) { echo "selected='selected'";} ?>"><?php echo $partner['partner_company']; ?></option>
                                <?php } ?>
                                            </select>
                                        </div>
                                        <div class="form-group col-lg-4">
                                            <select name="choice_year" class="form-control">
                                                <?php
                                                // lees alle jaren uit die in de databasetabel zitten
                                                $select_SQL = "SELECT * FROM reporting_year ORDER BY year_id ASC";
                                                $select_result = $db->query($select_SQL);
                                                while($partner = $select_result->fetch_array()){?>
                                                    <option value="<?php echo $partner['year'] ?>" <?php if(($year)==$partner['year']) { echo "selected='selected'";}  ?>> <?php echo $partner['year'] ?> </option>
                                                <?php } ?>
                                            </select>
                                        </div>
                                        <div class="form-group col-lg-12 ">
                                            <input type="submit" class="btn btn-primary btn-lg" value="Show report" name="verzenden" />
                                        </div>
                                    </form>
                                </div>
                            </div>
                        </div>
                    </div>
                    <?php
                    $partner_email=$db->query("SELECT partner_email FROM reporting_partners WHERE active='yes' and partner_id='$companyid'  ORDER BY partner_company ASC");
                    $email= $partner_email->fetch_assoc();
                    $e= substr($email['partner_email'], strpos($email['partner_email'], "@"));
                    if($e=='@infomap.com'){
                        $where = "or o.customer_email like '%informationmapping.com' and o.created_at like '$year%' AND o.status = 'complete' AND o.customer_email not like 'smourabit@informationmapping.com' AND o.customer_email not like 'fvanlerberghe@informationmapping.com' AND o.customer_email not like 'mprabhakara@informationmapping.com' AND o.customer_email not like 'info@informationmapping.com'";
                    }elseif($e=='@informationmapping.com'){
                        $where = "AND o.customer_email not like 'smourabit@informationmapping.com' AND o.customer_email not like 'fvanlerberghe@informationmapping.com' AND o.customer_email not like 'klentini@informationmapping.com' AND o.customer_email not like 'kalbanese@informationmapping.com' AND o.customer_email not like 'cciriello@informationmapping.com' AND o.customer_email not like 'info@informationmapping.com'";
                    }
                    /*
                    $where = array();
                    if (!empty($companyid)) $where[] = " customer_email like '%" . $e . "'";
                    if (!empty($year)) $where[] = " created_at like'" . $year . "%'";
                    $where = implode(' AND ', $where);
                    */
                    $year =  $_POST['choice_year'];
                    $query = "SELECT
                                    o.increment_id,
                                    o.created_at,
                                    base_currency_code,
                                    REPLACE(REPLACE(FORMAT(o.base_grand_total, 2), ',', ''),'.',',') as Total,
                                    p.method,
                                    p.po_number,
                                    group_concat(oi.name) as productName,
                                    o.customer_email
                                FROM
                                    sales_flat_order as o
                                    left join
                                    sales_flat_order_payment as p on p.parent_id=o.entity_id
                                    left join
                                    sales_flat_order_item as oi  on oi.order_id=o.entity_id
                                 WHERE o.customer_email like '%$e' and o.created_at like '$year%' AND o.status = 'complete' $where group by o.entity_id";
                                                    $_SESSION["query"] = $query;
                    $_SESSION["database"] = 'db';

                    if($verzenden) {
                        $result = $db->query($query);
                        $aantal = $result->num_rows;
                        if ($aantal == 0) {
                            echo "<p class='info'>No results found. Please choose a partner and correct year in the menu.<p>";
                        } else {
                            ?>
                            <div class="row">
                            <div class="col-lg-12">
                            <div class="panel panel-default">
                            <div class="panel-heading">
                                <button type="button" id="exp" class="btn btn-info pull-right"><i class="fa fa-download fa-lg"></i> Export CSV
                                </button>
                                <h2 class="panel-title">Partner orders:</h2>

                            </div>
                            <div class="panel-body">
                            <form id="sub" method="post" enctype="multipart/form-data" action="<?php echo $rootdir; ?>includes/csv.php">
                            <input type="hidden" name="filename" value="<?php echo $page ?>.csv">
                            <table class="table table-bordered table-condensed table-hover table-striped" id="dataTables-order-report">
                            <thead>
                            <tr>
                                <th>Order #</th>
                                <th>Order Date</th>
                                <th>Order Total</th>
                                <th>Payment method</th>
                                <th>PO number</th>
                                <th>Product name</th>
                                <th>Customer email</th>
                            </tr>
                            </thead>
                            <tbody>
                            <?php   while ($report = $result->fetch_array()) {
                                echo "<tr><td>";
                                echo $report["increment_id"];
                                echo "</td>";
                                echo "<td>";
                                echo $report["created_at"];
                                echo "</td>";
                                if($report['base_currency_code']=='USD'){
                                    $currency='$';
                                }else $currency='€';
                                echo "<td>" . $currency . $report["Total"] . "</td>";
                                if($report["method"]=='ops_cc')
                                {
                                    echo "<td> Credit Card</td>";
                                }else echo "<td>".$report["method"]."</td>";
                                echo "<td class='col-md-1'>";
                                echo $report["po_number"];
                                echo "</td>";
                                echo "<td class='col-md-5'>";
                                echo $report["productName"];
                                echo "</td>";
                                echo "<td class='col-md-2'>";
                                echo $report["customer_email"];
                                echo "</td>";
                                ?>
                                </tr>

                            <?php
                            }
                        }
                    }
                    ?>
                    </tbody>
                    </table>
                    </form>
                    </div>
                    </div>
                    </div>
                    </div>


                <?php }?>

            </div><!-- /.inner -->
        </div><!-- /.outer -->
    </div><!-- /#content -->
    <script>
        $(document).ready(function() {
            $('#dataTables-order-report').dataTable({
                "pageLength": 10
            });
        });
    </script>
    <?php include($rootdir."includes/footer.php"); ?>
</body>
</html>