<?php
$rootdir="../";
$page="POverview";
include_once($rootdir."includes/connect.php");
include($rootdir."includes/header.php");
$activepart = $db->query("SELECT * FROM reporting_partners WHERE active='yes'");
$aantal_part = $activepart->num_rows;
$inactivepart = $db->query("SELECT * FROM reporting_partners WHERE active='no'");
$aantal_inpart = $inactivepart->num_rows;
if(isset($_POST['verzenden'])) {
    header("Location: overview.php?year=" . $_POST['choice_year']);
}
?>
<body>
<div class="bg-light" id="wrap">
    <div id="top">
        <!-- .navbar -->
        <?php include($rootdir."includes/navtop.php"); ?>
    </div><!-- /#top -->
<?php include($rootdir."includes/navleft.php"); ?>
    <div id="content">
        <div class="outer">
            <div class="inner">
                <div class="inner-head">
                    <div class="dropdown  pull-right">
                        <button class="btn btn-primary dropdown-toggle" type="button" id="dropdownMenu1" data-toggle="dropdown" aria-expanded="true">
                            Year
                            <span class="caret"></span>
                        </button>
                        <ul class="dropdown-menu dropdown-menu-right" role="menu" aria-labelledby="dropdownMenu1">
                            <?php
                            // lees alle jaren uit die in de databasetabel zitten
                            $select_SQL="SELECT * FROM reporting_year ORDER BY year_id ASC";
                            $select_result= $db->query($select_SQL);
                            while($partner= $select_result->fetch_array()){

                                $current_year = date("Y");
                                // wanneer de pagina geen GET waarde heeft, veronderstellen we het huidige jaar, deze code is ook nodig om de selected='selected' hieronder goed te laten werken
                                if(empty($_GET['year']))
                                {
                                    $_GET['year'] = $current_year;
                                }
                                ?>
                                <li role="presentation"><a role="menuitem" tabindex="-1" href="overview.php?year=<?php echo $partner['year'] ?>" ><?php echo $partner['year'] ?></a></li>

                            <?php
                            }
                            ?>
                        </ul>
                    </div>
                    <h2>Partner Overview <br>
                        <div class="smalls"><?php echo $aantal_part; ?> active /
                            <?php echo $aantal_inpart; ?> inactive partners</div></h2>

                </div>
            <?php
            if (!isset($_SESSION["username"])){
                echo $niet_ingelogd;
            }
            elseif($permission != "1"){ // make it accessible with function admin and it
                echo $geen_toegang;
            }else{
                ?>
                  <div class="row">
                    <div class="col-lg-12">
                        <div class="box">
                            <header>
                                <?php  if(isset($_GET['year'])){  $jaar_voor_query = $_GET['year']; }else { $jaar_voor_query = $current_year; } ?>
                                <h5>Partner Reports for <?= $jaar_voor_query ?></h5>
                            </header>
                            <div id="collapse4" class="body">
                                <table class="table table-bordered table-condensed table-hover table-striped" id="dataTables-users">
                                    <thead>
                                    <tr>
                                        <th>Partner</th>
                                        <th>January</th>
                                       <th>February</th>
                                        <th>March</th>
                                        <th>April</th>
                                        <th>May</th>
                                        <th>June</th>
                                        <th>July</th>
                                        <th>August</th>
                                        <th>September</th>
                                        <th>October</th>
                                        <th>November</th>
                                        <th>December</th>
                                    </tr>
                                    </thead>
                                    <tbody>
                                    <?php
                                    $query = "SELECT * FROM reporting_partners WHERE active='yes' ORDER BY partner_company ASC"; // alfabetisch gerangschikt, enkel actieve partners
                                    $result = $db->query($query);
                                    while($row = $result->fetch_assoc())
                                    {
                                        $id=1;
                                        echo"<tr><td><a href=edit.php?partner_id=".$row['partner_id'].">".$row['partner_company']."</a></td>";
                                    $query_month = "SELECT * FROM reporting_months ORDER BY month_id ASC"; // alfabetisch gerangschikt, enkel actieve partners
                                    $result2 = $db->query($query_month);
                                    while($row_month = $result2->fetch_assoc()) {
                                        $query_check = $db->query("SELECT * FROM reporting_report WHERE month ='".$row_month['month']."' AND partner_id ='" . $row['partner_id'] . "' AND year = '$jaar_voor_query'");
                                        $aantal = $query_check->num_rows;
                                        if($aantal > 0) // als er een record is voor de gekozen maand, partner en jaar dan is deze ingevuld
                                        {
                                            echo "<td class='center'>Y<i class=\"fa fa-check-square-o text-success\"></i></td>";
                                        }
                                        else {
                                            echo "<td class='center'>N<i class=\"fa fa-times text-danger\"></i></td>";
                                        }

                                    }
                                        echo"</tr>";
                                    }?>
                                    </tbody>
                                </table>
                            </div>
                        </div>
                    </div>
            <?php }?>
            </div><!-- /.inner -->
        </div><!-- /.outer -->
    </div><!-- /#content -->
        <script>
            $(document).ready(function() {
                $('#dataTables-users').dataTable({
                    "pageLength": 25
                });
            });
        </script>
    <?php include($rootdir."includes/footer.php"); ?>
</body>
</html>