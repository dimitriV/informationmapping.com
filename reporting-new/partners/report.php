<?php
$rootdir="../";
$page="PReport";
$_SESSION["file"] = $page;

include_once($rootdir."includes/connect.php");
include($rootdir."includes/header.php");

$activepart =$db->query("SELECT * FROM reporting_partners WHERE active='yes'");
$inactivepart =$db->query("SELECT * FROM reporting_partners WHERE active='no'");
$aantal_part = $activepart->num_rows;
$aantal_inpart = $inactivepart->num_rows;
if(isset($_GET['action'])) $action=$_GET['action'];
if(isset($_POST['action'])) $action=$_POST['action'];

?>
<script>
    $(document).ready(function() {
        $( "#exp" ).click(function() {
            $("#sub").submit();
        });
    });
</script>
<body>

<div class="bg-light" id="wrap">
    <div id="top">
        <!-- .navbar -->
        <?php include($rootdir."includes/navtop.php"); ?>
    </div><!-- /#top -->
    <?php include($rootdir."includes/navleft.php"); ?>
    <div id="content">
        <div class="outer">
            <div class="inner">
                <div class="inner-head">
                    <h2>Partner report list <br>
                        <div class="smalls"><?php echo $aantal_part; ?> active /
                            <?php echo $aantal_inpart; ?> inactive partners</div></h2>

                </div>
                <?php
                if (!isset($_SESSION["username"])){
                    echo $niet_ingelogd;
                }
                elseif($permission != "1"){
                    echo $geen_toegang;
                }else{
                    $current_year = date("Y");
                    $companyid = isset($_POST['choice_partner']) ? $_POST['choice_partner'] : '';
                    $month = isset($_POST['choice_month']) ? trim($_POST['choice_month']) : '';
                    $year = isset($_POST['choice_year']) ? $_POST['choice_year'] : $current_year;
                    $verzenden = isset($_POST['verzenden']) ? trim($_POST['verzenden']) : '';
                    ?>
                    <div class="row">
                        <div class="col-lg-12">
                            <div class="panel panel-default">
                                <div class="panel-heading">
                                    <h3 class="panel-title">Please choose a partner and specify a year</h3>
                                </div>
                                <div class="panel-body">
                                    <form method="post" action="">
                                        <div class="form-group col-lg-4">
                                            <select role="form" name="choice_partner" class="form-control">
                                                <option value="">- Please select a partner-</option>
                                                <?php
                                                $partner_SQL="SELECT * FROM reporting_partners WHERE active='yes' ORDER BY partner_company ASC";
                                                $partner_result=$db->query($partner_SQL);
                                                while($partner=$partner_result->fetch_array()){ ?>
                                                    <option value="<?php echo $partner['partner_id'];  ?>" <?php if($companyid==$partner['partner_id']) { echo "selected='selected'";} ?>"><?php echo $partner['partner_company']; ?></option>
                                <?php } ?>
                                            </select>
                                        </div>
                                        <div class="form-group col-lg-4">
                                            <select name="choice_month" class="form-control">
                                                <option value="">- Please select a month-</option>
                                                <?php
                                                $partner_SQL="SELECT * FROM reporting_months ORDER BY month_id ASC";
                                                $partner_result=$db->query($partner_SQL);
                                                while($partner=$partner_result->fetch_array()){ ?>
                                                    <option value="<?php echo $partner['month'];  ?>" <?php if($month==$partner['month']) { echo "selected='selected'";} ?>"><?php echo $partner['month']; ?></option>
                                 <?php	}?>
                                            </select>
                                        </div>
                                        <div class="form-group col-lg-4">
                                            <select name="choice_year" class="form-control">
                                                <?php
                                                // lees alle jaren uit die in de databasetabel zitten
                                                $select_SQL="SELECT * FROM reporting_year ORDER BY year_id ASC";
                                                $select_result=$db->query($select_SQL);
                                                while($partner=$select_result->fetch_array()){?>
                                                    <option value="<?php echo $partner['year'] ?>" <?php if(($year)==$partner['year']) { echo "selected='selected'";}  ?>> <?php echo $partner['year'] ?> </option>
                                                <?php } ?>
                                            </select>
                                        </div>
                                        <div class="form-group col-lg-12 ">
                                            <input type="submit" class="btn btn-primary btn-lg" value="Show report" name="verzenden" />
                                        </div>
                                    </form>
                                </div>
                            </div>
                        </div>
                    </div>
                    <?php
                    $where = array();
                    if (!empty($companyid)) $where[] = " p.partner_id = '" . $companyid . "'";
                    if (!empty($month)) $where[] = " month = '" . $month . "'";
                    if (!empty($year)) $where[] = " year = '" . $year . "'";
                    $where = implode(' AND ', $where);
                    $totaal_part = 0;
                    $query = "SELECT p.partner_id, par.report_id, partner_company,participant_amount, c.course_id, c.course_name,  month, year  FROM reporting_participants as par INNER JOIN reporting_partners as p on p.partner_id=par.partner_id INNER JOIN reporting_courses as c on c.course_id=par.course_id WHERE $where";
                    $_SESSION["query"] = $query;
                    $_SESSION["database"] = 'db';

                    if($verzenden) {
                        $result = $db->query($query);
                        $aantal = $result->num_rows;
                        if ($aantal == 0) {
                            echo "<p class='info'>No results found. Please choose a partner and correct year in the menu.<p>";
                        } else {
                                ?>
                                <div class="row">
                                <div class="col-lg-6">
                                <div class="panel panel-default">
                                <div class="panel-heading">
                                    <button type="button" id="exp" class="btn btn-info pull-right"><i class="fa fa-download fa-lg"></i> Export CSV
                                    </button>
                                        <h2 class="panel-title">Your selected partner: <?php echo $row["partner_company"] ?></h2>

                                </div>
                                <div class="panel-body">
                                <form id="sub" method="post" enctype="multipart/form-data" action="<?php echo $rootdir; ?>includes/csv.php">
                                <input type="hidden" name="filename" value="<?php echo $page ?>.csv">
                                <table class="table table-bordered table-condensed table-hover table-striped" id="dataTables-users">
                                <thead>
                                <tr>
                                    <th>Company</th>
                                    <th>Course</th>
                                    <th>Month</th>
                                    <th>Year</th>
                                    <th># Attendees</th>
                                    <th>Details</th>
                                </tr>
                                </thead>
                                <tbody>
                                <?php   while ($report =$result->fetch_array()) {
                                echo "<tr><td>";
                                echo $report["partner_company"];
                                echo "</td>";
                                echo "<td>";
                                $result_course = $db->query("SELECT * FROM reporting_courses WHERE course_id='" . $report['course_id'] . "'");
                                while ($course_qry = $result_course->fetch_array()) {
                                    $course_name = $course_qry['course_name'];
                                }
                                echo $course_name;
                                echo "</td>";
                                echo "<td>" . $report['month'] . "</td>";
                                echo "<td>" . $year . "</td>";
                                echo "<td>";
                                echo $report['participant_amount'] . "<br />";
                                echo "</td>";
                               $totaal_part += $report['participant_amount'];
                                ?>
                                <td><a href="report_details.php?report_id=<?php echo $report['report_id'] ?>"><i class="fa fa-external-link"></i></a></td>
                                </tr>

                            <?php
                            }
                        }
                    }
                    ?>
                    </tbody>
                    </table>
                    </form>
                    </div>
                    </div>
                    </div>
                    </div>


                <?php }?>

            </div><!-- /.inner -->
        </div><!-- /.outer -->
    </div><!-- /#content -->
    <script>
        $(document).ready(function() {
            $('#dataTables-users').dataTable();
        });
    </script>
    <?php include($rootdir."includes/footer.php"); ?>
</body>
</html>