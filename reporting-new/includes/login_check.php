<?php
session_start();
include_once("connect.php");
if (!empty($_POST)){
    $query = "SELECT * FROM reporting_login
              WHERE email='" . htmlspecialchars($_POST["email"]) ."'
              AND password='". htmlspecialchars(md5(trim($_POST["password"]))). "'";
    $result = $db->query($query);
    if ($result->num_rows > 0){
        // e-mailadres gevonden, registreer gegevens in session
        $username = htmlspecialchars($_POST["email"]);
        $_SESSION['username']=$username;
        // functie van de gebruiker uitlezen: admin, moderator, it, marketing
        while ($row = $result->fetch_assoc()) {
            $id = $row['partner_id'];
            $_SESSION["login_id"] = $id;
            $name = $row['name'];
            $_SESSION["name"] = $name;
        }
        // Doorsturen naar beveiligde pagina
        header("Location: ../dashboard.php");
        exit();
    }else{

session_start();
session_unset(); // alle variabelen vrijgeven
session_destroy(); // sessie afsluiten
?>
<!doctype html>
<html class="no-js">
<head>
    <meta charset="UTF-8">
    <title>Dashboard</title>

    <!--IE Compatibility modes-->
    <meta http-equiv="X-UA-Compatible" content="IE=edge">

    <!--Mobile first-->
    <meta name="viewport" content="width=device-width, initial-scale=1">

    <!-- Bootstrap -->
    <link rel="stylesheet" href="../assets/lib/bootstrap/css/bootstrap.min.css">

    <!-- Font Awesome -->
    <link href="//maxcdn.bootstrapcdn.com/font-awesome/4.2.0/css/font-awesome.min.css" rel="stylesheet">

    <!-- Metis core stylesheet -->
    <link rel="stylesheet" href="../assets/css/main.css">
    <link rel="stylesheet" href="../assets/css/imi.css">

    <!-- metisMenu stylesheet -->
    <link rel="stylesheet" href="../assets/lib/metismenu/metisMenu.min.css">

    <!-- HTML5 Shim and Respond.js IE8 support of HTML5 elements and media queries -->

    <!-- WARNING: Respond.js doesn't work if you view the page via file:// -->

    <!--[if lt IE 9]>
    <script src="../assets/lib/html5shiv/html5shiv.js"></script>
    <script src="../assets/lib/respond/respond.min.js"></script>
    <![endif]-->
    <!--For Development Only. Not required -->
    <link rel="stylesheet" type="text/css" href="http://fonts.googleapis.com/css?family=Source%20Sans%20Pro">
    <script>
        less = {
            env: "development",
            relativeUrls: false,
            rootpath: "../assets/"
        };
    </script>
    <!--Modernizr 2.8.2-->
    <script src="../assets/lib/modernizr/modernizr.min.js"></script>
</head>
<body>
    <div class="container">
        <div class="row">
            <div class="col-md-4 col-md-offset-4">
                <div class="login-panel panel panel-default text-center">
                    <div class="panel-heading">
                        <a href="dashboard.php" class="login-img">
                            <img src="../assets/img/logo.png" alt="">
                        </a>
                    </div>
                    <div class="panel-body">
                        <p class="text-center pv">SIGN IN TO CONTINUE.</p>
                        <form role="form" action="login_check.php" method="post" name="form1" id="form1" class="form">
                            <fieldset>
                                <div class="form-group">
                                    <div class="">
                                        <div class="alert alert-danger" role="alert">
                                            <span class="glyphicon glyphicon-exclamation-sign" aria-hidden="true"></span>
                                            <span class="sr-only">Error:</span>
                                            Login credentials are incorrect, <br> please try again
                                        </div>
                                    </div>
                                </div>
                                <div class="input-group form-group ">
                                    <input class="form-control" placeholder="E-mail" name="email" type="text" autofocus><span class="input-group-addon"><i class="fa fa-envelope"></i></span>
                                </div>
                                <div class="input-group form-group">
                                    <input class="form-control" placeholder="Password" name="password" type="password" value=""><span class="input-group-addon"><i class="fa fa-lock fa-lg"></i></span>
                                </div>
                                <div class="checkbox">
                                    <label>
                                        <input name="remember" type="checkbox" value="Remember Me">Remember Me
</label>
                                </div>
                                <!-- Change this to a button or input when using this as a form -->
                                <button type="submit"  name="Submit" class="btn btn-primary btn-block">Login</button>
                            </fieldset>
                        </form>
                    </div>
                </div>
            </div>
        </div>
    </div>
    <div class="bg-light" id="footer">
        <div class="bg-light col-sm-12" id="footertext">
            <p>&copy; <?php date_default_timezone_set('Europe/Amsterdam'); echo  date("Y"); ?> Information Mapping International | <a
                    href="http://www.informationmapping.com/">www.informationmapping.com</a> | <em
                    class="fa fa-phone"> </em> +32 9 253 14 25 | Contact our office in <a
                    href="http://www.informationmapping.com/us/contact">US</a>, <a
                    href="http://www.informationmapping.com/en/contact-3">Europe</a> or <a
                    href="http://www.informationmapping.com/in/contact-2">Asia</a></p>

            <p class="icons"><a href="https://twitter.com/infomap"><i class="fa fa-twitter-square fa-2x"
                                                                      style="color: #009edf;"></i></a> <a
                    href="https://www.facebook.com/iminv"><i class="fa fa-facebook-square fa-2x"
                                                             style="color: #009edf;"></i></a> <a
                    href="http://www.linkedin.com/groups?gid=983927"><i class="fa fa-linkedin-square fa-2x"
                                                                        style="color: #009edf;"></i></a> <a
                    href="http://www.youtube.com/user/InformationMapping"><i class="fa fa-youtube-square fa-2x"
                                                                             style="color: #009edf;"></i></a></p>
        </div>
    </div>
    </div>
    <!--jQuery 2.1.1 -->
    <script src="../assets/lib/jquery/jquery.min.js"></script>
    <script src="//ajax.googleapis.com/ajax/libs/jqueryui/1.10.3/jquery-ui.min.js"></script>

    <!--Bootstrap -->
    <script src="../assets/lib/bootstrap/js/bootstrap.min.js"></script>

    <!-- MetisMenu -->
    <script src="../assets/lib/metismenu/metisMenu.min.js"></script>

    <!-- Screenfull -->
    <script src="../assets/lib/screenfull/screenfull.js"></script>

    <!-- Metis core scripts -->
    <script src="../assets/js/core.min.js"></script>
</body>
</html>
<?php }
} ?>