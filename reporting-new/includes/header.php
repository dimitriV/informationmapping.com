<?php
session_start();
?>
<!doctype html>
<html class="no-js">
<head>
    <meta charset="UTF-8">
    <title>Dashboard</title>

    <!--IE Compatibility modes-->
    <meta http-equiv="X-UA-Compatible" content="IE=edge">

    <!--Mobile first-->
    <meta name="viewport" content="width=device-width, initial-scale=1">

    <!-- Bootstrap -->
    <link rel="stylesheet" href="<?php echo $rootdir; ?>assets/lib/bootstrap/css/bootstrap.min.css">
    <!-- Metis core stylesheet -->
    <link rel="stylesheet" href="<?php echo $rootdir; ?>assets/css/main.css">
    <link rel="stylesheet" href="<?php echo $rootdir; ?>assets/css/imi.css">
    <link rel="stylesheet" type="text/css" href="<?php echo $rootdir; ?>assets/lib/qtip/css/jquery.qtip.css">
    <!-- Font Awesome -->
    <link href="//maxcdn.bootstrapcdn.com/font-awesome/4.2.0/css/font-awesome.min.css" rel="stylesheet">
    <link rel="stylesheet" href="//code.jquery.com/ui/1.11.2/themes/smoothness/jquery-ui.css">

    <!-- metisMenu stylesheet -->
    <link rel="stylesheet" href="<?php echo $rootdir; ?>assets/lib/metismenu/metisMenu.min.css">
    <link rel="stylesheet" href="<?php echo $rootdir; ?>assets/lib/datatables/3/dataTables.bootstrap.css">
    <!-- HTML5 Shim and Respond.js IE8 support of HTML5 elements and media queries -->

    <!-- WARNING: Respond.js doesn't work if you view the page via file:// -->

    <!--[if lt IE 9]>
    <script src="assets/lib/html5shiv/html5shiv.js"></script>
    <script src="assets/lib/respond/respond.min.js"></script>
    <![endif]-->
    <!--For Development Only. Not required -->
    <link rel="stylesheet" type="text/css" href="http://fonts.googleapis.com/css?family=Source%20Sans%20Pro">
    <script>
        less = {
            env: "development",
            relativeUrls: false,
            rootpath: "../assets/"
        };
    </script>
    <!--Modernizr 2.8.2-->
    <script src="<?php echo $rootdir; ?>assets/lib/modernizr/modernizr.min.js"></script>
    <script src="<?php echo $rootdir; ?>assets/lib/jquery/jquery.min.js"></script>
    <script src="<?php echo $rootdir; ?>assets/lib/datatables/jquery.dataTables.js"></script>
    <script src="<?php echo $rootdir; ?>assets/lib/datatables/3/dataTables.bootstrap.js"></script>
    <script src="<?php echo $rootdir; ?>assets/lib/bootstrap/js/bootstrap.min.js"></script>
    <script src="http://cdn.datatables.net/plug-ins/1.10.7/api/sum().js"></script>
</head>

<?php
$result=$db->query("SELECT ". $page ." FROM reporting_permissions where reporting_userid=".$_SESSION["login_id"]." and ". $page ." !=0");

$permission = $result->num_rows;
?>