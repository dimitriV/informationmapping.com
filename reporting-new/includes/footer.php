<div class="footer">
    <div class="bg-light" id="footer">
        <div class="bg-light col-sm-12" id="footertext">
            <p>&copy; <?php date_default_timezone_set('Europe/Amsterdam'); echo  date("Y"); ?> Information Mapping International | <a
                    href="http://www.informationmapping.com/">www.informationmapping.com</a> | <em
                    class="fa fa-phone"> </em> +32 9 253 14 25 | Contact our office in <a
                    href="http://www.informationmapping.com/us/contact">US</a>, <a
                    href="http://www.informationmapping.com/en/contact-3">Europe</a> or <a
                    href="http://www.informationmapping.com/in/contact-2">Asia</a></p>

            <p class="icons"><a href="https://twitter.com/infomap"><i class="fa fa-twitter-square fa-2x"
                                                                      style="color: #009edf;"></i></a> <a
                    href="https://www.facebook.com/iminv"><i class="fa fa-facebook-square fa-2x"
                                                             style="color: #009edf;"></i></a> <a
                    href="http://www.linkedin.com/groups?gid=983927"><i class="fa fa-linkedin-square fa-2x"
                                                                        style="color: #009edf;"></i></a> <a
                    href="http://www.youtube.com/user/InformationMapping"><i class="fa fa-youtube-square fa-2x"
                                                                             style="color: #009edf;"></i></a></p>
        </div>
    </div>
</div>
</div>
<!--jQuery 2.1.1 -->


<script src="//ajax.googleapis.com/ajax/libs/jqueryui/1.10.3/jquery-ui.min.js"></script>
<!--Bootstrap
<script src="<?php echo $rootdir; ?>assets/lib/bootstrap/js/bootstrap.min.js"></script>
 -->

<!-- MetisMenu -->
<script src="<?php echo $rootdir; ?>assets/lib/metismenu/metisMenu.min.js"></script>

<!-- Screenfull -->
<script src="<?php echo $rootdir; ?>assets/lib/screenfull/screenfull.js"></script>
<script src="<?php echo $rootdir; ?>assets/lib/jquery.tablesorter/jquery.tablesorter.min.js"></script>
<script src="<?php echo $rootdir; ?>assets/lib/jquery-ui-touch-punch/jquery.ui.touch-punch.min.js"></script>
<script src="<?php echo $rootdir; ?>assets/lib/kartograph/kartograph.min.js"></script>
<script type="text/javascript" src="<?php echo $rootdir; ?>assets/lib/qtip/js/jquery.qtip.min.js"></script>


<!-- Metis core scripts -->
<script src="<?php echo $rootdir; ?>assets/js/core.min.js"></script>



