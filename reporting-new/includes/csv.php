<?php
include_once('connect.php');
session_start();

// Fetch Record from Database

$output			= "";
$connection     = $_SESSION['database']; // Working with multiple database connections
$query 			= $_SESSION["query"];
$result 		= $$connection->query($query);
$field_cnt      = $result->field_count;

/* Get field information for all columns */
while ($finfo = $result->fetch_field()) {
    $heading  =	 $finfo->name;
    $output	 .= '"'.$heading.'",';
}
$output      .="\n";

// Get Records from the table
while ($row = $result->fetch_array()) {
    for ($i = 0; $i < $field_cnt; $i++) {
        $output .='"'.$row["$i"].'",';
    }
    $output .="\n";
}

// Download the file

$filename =  $_POST['filename']; // File names get added on Form submit action
header('Content-type: application/csv');
header('Content-Disposition: attachment; filename='.$filename);

echo $output;
exit;
?>
