<div id="left">
<!-- #menu -->
<ul id="menu" class="bg-blue">
<li class="nav-header">Main Navigation</li>
<li class="active">
    <a href="<?php echo $rootdir; ?>dashboard.php">
        <i class="fa fa-home fa-2x"></i><span class="link-title">&nbsp;Home</span>
    </a>
</li>
<?php
$result=$db->query("SELECT * FROM reporting_permissions where reporting_userid=".$_SESSION["login_id"]);
$view = $result->fetch_assoc(); ?>
<?php if($view['ROverview'] == "1" || $view['REssential'] == "1"  ){ ?>
    <li class="">
        <a href="javascript:;">
            <i class="fa fa-flag"></i></i>
            <span class="link-title">Reports</span>
            <span class="fa arrow"></span>
        </a>
        <ul>
            <li>
                <a href="<?php echo $rootdir; ?>reports/essentials.php">
                    <i class="fa fa-angle-right"></i>Video Course</a>
            </li>
        </ul>
    </li>
<?php } if($view['LOverview'] == "1" || $view['LSearch'] == "1"|| $view['LReplace'] == "1" || $view['LAdd'] == "1"){ ?>
    <li class="">
        <a href="javascript:;">
            <i class="fa fa-key"></i>
            <span class="link-title">License Keys</span>
            <span class="fa arrow"></span>
        </a>
        <ul>
            <?php if($view['LReplace'] == "1" ){ ?>
                <li>
                    <a href="<?php echo $rootdir; ?>licensekeys/replace.php">
                        <i class="fa fa-angle-right"></i>Replace License Key</a>
                </li>
            <?php } if($view['LAdd'] == "1" ){ ?>
                <li>
                    <a href="<?php echo $rootdir; ?>licensekeys/add.php">
                        <i class="fa fa-angle-right"></i>Add Volume Key</a>
                </li>
            <?php } if($view['LServer'] == "1" ){ ?>
                <li>
                    <a href="<?php echo $rootdir; ?>licensekeys/license_server.php">
                        <i class="fa fa-angle-right"></i>Add License Manager</a>
                </li>
            <?php } if($view['SCreate'] == "1" ){ ?>
                <li>
                    <a href="<?php echo $rootdir; ?>licensekeys/student_key_create.php">
                        <i class="fa fa-angle-right"></i>Create a Student Key</a>
                </li>
            <?php } ?>
        </ul>
    </li>
<?php } if($view['EExtend'] == "1" ){ ?>
    <li class="">
        <a href="javascript:;">
            <i class="fa fa-laptop"></i>
            <span class="link-title">E-learning</span>
            <span class="fa arrow"></span>
        </a>
        <ul>
            <?php if($view['EExtend'] == "1" ){ ?>
                <li>
                    <a href="<?php echo $rootdir; ?>elearning/extend_enrollments.php">
                        <i class="fa fa-angle-right"></i>Extend Enrollments</a>
                </li>
            <?php } ?>
        </ul>
    </li>
<?php } if($view['OOverview'] == "1" ){ ?>
    <li class="">
        <a href="javascript:;">
            <i class="fa fa-money"></i>
            <span class="link-title">Order Reports</span>
            <span class="fa arrow"></span>
        </a>
        <ul>
            <li>
                <a href="">
                    <i class="fa fa-angle-right"></i>Overview</a>
            </li>
            <li>
                <a href="">
                    <i class="fa fa-angle-right"></i>Search User</a>
            </li>
            <li>
                <a href="">
                    <i class="fa fa-angle-right"></i>Extend Enrollments</a>
            </li>
            <li>
                <a href="">
                    <i class="fa fa-angle-right"></i>IMP Exam Result</a>
            </li>
        </ul>
    </li>
<?php } ?>
</ul><!-- /#menu -->
</div><!-- /#left -->