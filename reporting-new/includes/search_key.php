<?php
$key = isset($_POST['key']) ? trim($_POST['key']) : '';

if (isset($_POST['key']))
{
    if(empty($key))
    {
        echo "Please provide a search string.";

    }
    else
    {
        $sql = "SELECT
                    *
                FROM
                     license_serialnumbers
                WHERE
                    serial_number LIKE '%$key%'";
        $result = $db->query($sql);
        $row_result = $result->fetch_array();
        $aantal = $result->num_rows;

        ?>
        <div class="row">
            <div class="col-lg-12">
                <div class="box">
                    <header>
                        <div class="icons">
                            <i class="fa fa-users"></i>
                        </div>
                        <h5>License key: &nbsp; <a data-toggle="modal" data-target="#myModal" ><?= $row_result['serial_number']; ?></a></h5>
                    </header>
                    <div id="collapse4" class="body">
                        <table class="table table-bordered table-condensed table-hover table-striped" id="dataTables-licenses">
                            <thead>
                            <tr>
                                <th>Order</th>
                                <th>Order Date</th>
                                <th>Product Name</th>
                                <th>Enduser Details</th>
                                <th>Buyer E-mail</th>
                                <th>Buyer Fullname</th>
                            </tr>
                            </thead>
                            <tbody> <?php
                            if($aantal > 0)
                            {

                                $query2= $db->query("SELECT
                                                                    s.product_id, GROUP_CONCAT(name SEPARATOR ', ') as name
                                                                FROM
                                                                    sales_flat_order_item AS i
                                                                        LEFT JOIN
                                                                    license_serialnumbers AS s on i.order_id=s.order_id
                                                                WHERE
                                                                    s.serial_number like  '%$key%' and s.product_id=i.product_id",$db);
                                while($row_item = $query2->fetch_array()) {
                                    $name = $row_item['name'] ;
                                    $product = $row_item['product_id'] ;
                                    if (strpos($name,'1 year') !== false) {
                                        $periodicity='1';
                                    }elseif (strpos($name,'2 year') !== false)
                                    {
                                        $periodicity='2';
                                    }elseif (strpos($name,'3 year') !== false)
                                    {
                                        $periodicity='3';
                                    }elseif (strpos($name,'Perpetual') !== false)
                                    {
                                        $periodicity='perpetual';
                                    }

                                }


                                $query = $db->query("SELECT
                                                            so.increment_id,
                                                            s.order_id,
                                                            so.created_at,
                                                            s.serial_number,
                                                            so.customer_email,
                                                            CONCAT(a.firstname, ' ', a.lastname) AS fullname
                                                        FROM
                                                            license_serialnumbers AS s
                                                                LEFT JOIN
                                                            license_orders AS o ON o.license_id = s.id
                                                                LEFT JOIN
                                                            license_info AS i ON i.license_id = o.license_id
                                                                LEFT JOIN
                                                            sales_flat_order AS so ON so.entity_id = s.order_id
                                                                LEFT JOIN
                                                            sales_flat_order_address AS a ON so.entity_id = a.parent_id
                                                        WHERE
                                                            s.serial_number LIKE '%$key%'" );

                                while($row = $query->fetch_array())
                                {
                                    echo "<tr>";
                                    echo "<td><a data-toggle=\"modal\" data-target=\"#myModal\" >" . $row['increment_id'] . "</a></td>";
                                    echo "<td><a data-toggle=\"modal\" data-target=\"#myModal\" >" . $row['created_at'] . "</a></td>";
                                    echo "<td><a data-toggle=\"modal\" data-target=\"#myModal\" >" . $name . "</a></td>";
                                    echo "<td>";?>
                                    <div class="dropdown">
                                        <button class="btn btn-info" type="button" data-toggle="modal" data-target="#myModal2">
                                            Details
                                        </button>
                                    </div>
                                    <?php
                                    echo "</td>";
                                    echo "<td><a class='hideoverflow' data-toggle=\"modal\" data-target=\"#myModal\" >" . $row['customer_email'] . "</a></td>";
                                    echo "<td><a data-toggle=\"modal\" data-target=\"#myModal\" >" . $row['fullname'] . "</a></td>";

                                    echo "</tr>";
                                }
                            }
                            //This part is for New license keys from FS PRO 2013
                            else {
                                echo 'test';
                            } ?>

                            </tbody>
                        </table>
                        <div class="modal fade" id="myModal2" tabindex="-1" role="dialog" aria-labelledby="myModalLabel" aria-hidden="true">
                            <div class="modal-dialog">
                                <div class="modal-content">
                                    <div class="modal-header">
                                        <button type="button" class="close" data-dismiss="modal" ><span aria-hidden="true">&times;</span><span class="sr-only">Close</span></button>
                                        <h4 class="modal-title" id="myModalLabel">End user information:</h4>
                                    </div>
                                    <div class="modal-body">
                                        <table class="table table-condensed">
                                            <thead>
                                            <tr>
                                                <th>Company Name</th>
                                                <th>Enduser email</th>
                                            </tr>
                                            </thead>
                                            <?php $query_email = $db->query("SELECT DISTINCT COL1, COL3 FROM as_customerinfo where col2 like '%$key%'" );
                                            while($row_email = $query_email->fetch_array())
                                            {
                                                echo "<tr>";
                                                echo "<td>" . $row_email['COL3'] . "</td>";
                                                echo "<td>" . $row_email['COL1'] . "</td>";
                                                echo "</tr>";
                                            }?>
                                        </table>
                                    </div>
                                    <div class="modal-footer">
                                        <button type="button" class="btn btn-info" data-dismiss="modal">Close</button>
                                    </div>
                                </div>
                            </div>
                        </div>
                        <!-- Modal -->
                        <div class="modal fade" id="myModal" tabindex="-1" role="dialog" aria-labelledby="myModalLabel" aria-hidden="true">
                            <div class="modal-dialog-lg">
                                <div class="modal-content">
                                    <div class="modal-header">
                                        <button type="button" class="close" data-dismiss="modal" ><span aria-hidden="true">&times;</span><span class="sr-only">Close</span></button>
                                        <h4 class="modal-title" id="myModalLabel">License Key information:</h4>
                                    </div>
                                    <div class="modal-body">
                                        <table class="table table-condensed">
                                            <thead>
                                            <tr>
                                                <th># Order</th>
                                                <th>Product ID</th>
                                                <th>Product name</th>
                                                <th>Enduser hostname</th>
                                                <th>Activation Date</th>
                                                <th>Expiry Date</th>
                                                <th>Activations</th>
                                            </tr>
                                            </thead>
                                            <?php
                                            $query3 =  "SELECT DISTINCT(expiry) FROM imi_prod_magento.as_lic_files where lickey like '%$key%' order by expiry desc;";
                                            $result = $db->query($query3);
                                            $row_result = $result->fetch_array();
                                            $exp = $row_result['expiry'];


                                            $query = $db->query("SELECT
                                                                                          DISTINCT(lic.created) ,lic.host_name,
                                                                    tries,
                                                                    maxtries,
                                                                    so.increment_id,
                                                                    s.order_id,
                                                                    s.serial_number
                                                                FROM
                                                                    license_serialnumbers AS s
                                                                       LEFT JOIN
                                                                    license_orders AS o ON o.license_id = s.id
                                                                       LEFT JOIN
                                                                    license_info AS i ON i.license_id = o.license_id
                                                                        LEFT JOIN
                                                                    sales_flat_order AS so ON so.entity_id = s.order_id
                                                                        LEFT JOIN
                                                                    sales_flat_order_address AS a ON so.entity_id = a.parent_id
                                                                       LEFT JOIN
                                                                    as_tries AS t ON s.serial_number = t.entrykey

                                                                        LEFT JOIN
                                                                    as_license_usage AS lic ON lic.lickey = s.serial_number
                                                                WHERE
                                                                    s.serial_number LIKE '%$key%'" );
                                            while($row = $query->fetch_array()) {
                                                if($periodicity=='perpetual'){
                                                    $expirydate =date_create('01-01-2050');
                                                }else{

                                                    $expirydate = date_create($row['created']);
                                                    date_add($expirydate, date_interval_create_from_date_string($periodicity.' year'));
                                                }
                                                $activationdate = date_create($row['created']);
                                                echo "<tr>";
                                                echo "<td>#" . $row['increment_id'] . "</td>";
                                                echo "<td>" . $product. "</td>";
                                                echo "<td>" . $name . "</td>";
                                                echo "<td>" . $row['host_name'] . "</td>";
                                                echo "<td>" . date_format($activationdate, 'd/m/Y H:i:s') . "</td>";
                                                echo "<td>" . $exp . "</td>";
                                                echo "<td>" . $row['tries'] ." / ". $row['maxtries']."</td>";
                                                echo "</tr>";
                                            }?>
                                        </table>
                                    </div>
                                    <div class="modal-footer">
                                        <button type="button" class="btn btn-info" data-dismiss="modal">Close</button>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div><!-- /.row -->
        <script>
            $(document).ready(function() {
                $('#dataTables-licenses').dataTable();
            });
        </script>
    <?php

    }
}
?>