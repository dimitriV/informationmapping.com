<nav class="navbar navbar-default navbar-fixed-top">
        <!-- Brand and toggle get grouped for better mobile display -->
        <header class="navbar-header">
            <div class="navbar-toggle">
                <a data-placement="bottom" data-original-title="Show / Hide Left" data-toggle="tooltip" class="btn toggle-left" id="menu-toggle">
                    <i class="fa fa-indent fa-2x"></i>
                </a>
            </div>
            <div class="brand-logo">
            <a href="dashboard.php" class="navbar-brand">
                <img src="<?php echo $rootdir; ?>assets/img/logo.png" alt="">
            </a>
            </div>
        </header>
    <div class="nav-wrapper">
        <div class="btn-group topnavleft">
            <div class="btn-group">
                <a href="<?php echo $rootdir; ?>dashboard.php" class="btn toggle-left"><i class="fa fa-home fa-2x"></i> - <?php // echo $_SESSION["name"]; ?> </a>
            </div>
        </div>
        <div class="topnav">
            <div class="btn-group">
                <a data-placement="bottom" data-original-title="Fullscreen" data-toggle="tooltip" class="btn" id="toggleFullScreen">
                    <i class="fa fa-expand fa-lg"></i>
                </a>
            </div>
            <div class="btn-group">
                <a href="<?php echo $rootdir; ?>includes/logout.php" data-toggle="tooltip" data-original-title="Logout" data-placement="bottom" class="btn">
                    <i class="fa fa-sign-out fa-lg"></i>
                </a>
            </div>
            <div class="btn-group item-hidden-right hidden-xs">
                <a data-placement="bottom" data-original-title="Show / Hide Left" data-toggle="tooltip" class="btn toggle-left" id="menu-toggle">
                    <i class="fa fa-indent fa-lg"></i>
                </a>
            </div>
        </div>
    </div>
</nav><!-- /.navbar -->