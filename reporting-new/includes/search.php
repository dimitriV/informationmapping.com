<?php
$searchstring = isset($_POST['searchstring']) ? $_POST['searchstring'] : '';
if (isset($_POST['searchstring']))
{
    if(empty($searchstring))
    {
        echo "Please provide a search string.";

    }
    else
    {
        $search = trim(strtolower($searchstring));
        echo $search;
        // zoeken op email adres want is uniek in elke DB
        $user_magento = $db->query("SELECT
                                            s.customer_id AS entity_id, s.entity_id as orderid,
                                            s.customer_email AS email,
                                            CONCAT(s.customer_firstname,' ',s.customer_lastname) AS fullname,
                                            CONCAT(a.firstname, ' ', a.lastname) AS fullname2
                                        FROM
                                            sales_flat_order AS s
                                                LEFT JOIN
                                            sales_flat_order_address AS a ON s.entity_id = a.parent_id
                                                LEFT JOIN
                                            customer_entity AS c ON c.entity_id = s.customer_id
                                        WHERE
                                            lower(s.customer_email) LIKE '%$search%'
                                                OR lower(s.customer_firstname) LIKE '%$search%'
                                                OR lower(s.customer_lastname) LIKE '%$search%'
                                        GROUP BY s.customer_id;");
        $aantal_magento = $user_magento->num_rows;
        ?>
        <div class="row">
            <div class="col-lg-12">
                <div class="box">
                    <header>
                        <div class="icons">
                            <i class="fa fa-users"></i>
                        </div>
                        <h5>Click the user of your choice:</h5>
                    </header>
                    <div id="collapse4" class="body">
                        <table class="table table-bordered table-condensed table-hover table-striped" id="dataTables-users">
                            <thead>
                            <tr>
                                <th>Fullname</th>
                                <th>Email</th>
                            </tr>
                            </thead>
                            <tbody> <?php
                            if($aantal_magento == 0)
                            {
                                // $user_moodle
                                $user_moodle = $db2->query("SELECT id,email, CONCAT(firstname,' ',lastname)  AS fullname FROM mdl_user WHERE lower(email) LIKE '%$search%' or lower(firstname) like '%$search%' or lower(lastname) like '%$search%'");
                                $aantal_moodle = $user_moodle->num_rows;
                                while($row_user_moodle = $user_moodle->fetch_array())
                                {
                                    echo "<tr><td><a href=\"search_user.php?entity_email=" . $row_user_moodle['email'] . "&entity_name=" . $row_user_moodle['fullname'] . "\" target=\"_blank\" title=\"" . $row_user_moodle['id'] . "\"><i class=\"fa fa-user\"></i>  " . $row_user_moodle['fullname'] . "</a></td>";
                                    echo "<td><a href=\"search_user.php?entity_email=" . $row_user_moodle['email'] . "\" target=\"_blank\" title=\"" . $row_user_moodle['id'] . "\">" . $row_user_moodle['email'] . "</a></td></tr>";
                                }
                            }
                            else {
                                while ($row_user_magento = $user_magento->fetch_array()) {
                                    if($row_user_magento['fullname'] != null){
                                        $fullname = $row_user_magento['fullname'];
                                    }else $fullname = $row_user_magento['fullname2'];
                                    if($row_user_magento['entity_id'] != null){
                                        $id = $row_user_magento['entity_id'];
                                    }else $id = $row_user_magento['orderid'];
                                    echo "<tr><td><a href=\"search_user.php?entity_email=" . $row_user_magento['email'] . "&entity_name=" . $fullname .  "&entity_id=" . $id . "\" target=\"_blank\" title=\"" . $id . "\"><i class=\"fa fa-user\"></i>  " . $fullname . "</a></td>";
                                    echo "<td><a href=\"search_user.php?entity_email=" . $row_user_magento['email'] . "&entity_name=" . $fullname .  "&entity_id=" . $id ."\" target=\"_blank\" title=\"" . $id . "\">" . $row_user_magento['email'] . "</a></td></tr>";
                                }
                            }?>
                            </tbody>
                        </table>
                    </div>
                </div>
            </div>
        </div><!-- /.row -->
        <script>
            $(document).ready(function() {
                $('#dataTables-users').dataTable();
            });
        </script>
    <?php

    }
}
?>