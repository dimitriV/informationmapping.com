<?php
$increment_id = isset($_POST['increment_id']) ? trim($_POST['increment_id']) : '';

if (isset($_POST['increment_id']))
{
if(empty($increment_id))
{
    echo "Please provide a search string.";

}
else
{

?>
                <div class="row">
                    <div class="col-lg-12">
                        <div class="box">
                            <header>
                                <div class="icons">
                                    <i class="fa fa-users"></i>
                                </div>
                                <h5>License orders overview:</h5>
                            </header>
                            <div id="collapse4" class="body">
                                <table class="table table-bordered table-condensed table-hover table-striped" id="dataTables-orders">
                                    <thead>
                                    <tr>
                                        <th>Order</th>
                                        <th>Order Date</th>
                                        <th>License Key</th>
                                        <th>Product ID</th>
                                        <th>Product Name</th>
                                        <th>Buyer ID</th>
                                        <th>Buyer Fullname</th>
                                        <th>Buyer E-mail</th>
                                    </tr>
                                    </thead>
                                    <tbody> <?php
                                    $query = $db->query("SELECT
                                                                    so.increment_id,
                                                                    s.order_id,
                                                                    s.customer_id,
                                                                    so.created_at,
                                                                    s.serial_number,
                                                                    so.customer_email,
                                                                    CONCAT(a.firstname, ' ', a.lastname) AS fullname
                                                                FROM
                                                                    license_serialnumbers AS s
                                                                        LEFT JOIN
                                                                    license_orders AS o ON o.license_id = s.id
                                                                        LEFT JOIN
                                                                    license_info AS i ON i.license_id = o.license_id
                                                                        LEFT JOIN
                                                                    sales_flat_order AS so ON so.entity_id = s.order_id
                                                                        LEFT JOIN
                                                                    sales_flat_order_address AS a ON so.entity_id = a.parent_id
                                                                WHERE
                                                                    increment_id like '%$increment_id%'" );

                                    while($row = $query->fetch_array())
                                    {
                                        $license= $row['serial_number'];

                                        $query2= $db->query("SELECT
                                                                    s.product_id, GROUP_CONCAT(name SEPARATOR ', ') as name
                                                                FROM
                                                                    sales_flat_order_item AS i
                                                                        LEFT JOIN
                                                                    license_serialnumbers AS s on i.order_id=s.order_id
                                                                WHERE
                                                                    s.serial_number like  '%$license%' and s.product_id=i.product_id");
                                        while($row_item = $query2->fetch_array()) {
                                            $name = $row_item['name'] ;
                                            $product = $row_item['product_id'] ;

                                        }
                                        echo "<tr>";
                                        echo "<td>#" . $row['increment_id'] . "</td>";
                                        echo "<td>" . $row['created_at'] . "</td>";
                                        echo "<td>" . $row['serial_number'] . "</td>";
                                        echo "<td>" . $product . "</td>";
                                        echo "<td>" . $name . "</td>";
                                        echo "<td>" . $row['customer_id'] . "</td>";
                                        echo "<td>" . $row['fullname'] . "</td>";
                                        echo "<td>" . $row['customer_email'] . "</td>";
                                        echo "</tr>";
                                    }
                                    ?>

                                    </tbody>
                                </table>
                            </div>
                        </div>
                    </div>
                    <?php } }?>
                </div><!-- /.inner -->
            </div><!-- /.outer -->
        </div><!-- /#content -->
        <script>
            $(document).ready(function() {
                $('#dataTables-orders').dataTable({
                    "pageLength": 25,
                    "order": [ 0    , 'desc' ]
                });

            });
        </script>
