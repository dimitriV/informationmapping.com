<?php
$rootdir="../";
$page="EExtend";
include_once($rootdir."includes/connect.php");
include($rootdir."includes/header.php");
?>
<script>
    $(function() {
        $( "#extend" ).datepicker({
            changeMonth: true,
            changeYear: true,
            dateFormat: "dd-mm-yy",
            onClose: function( selectedDate ) {
                $( "#to" ).datepicker( "option", "minDate", selectedDate );
            }
        });
    });
</script>
<body>
<div class="bg-light" id="wrap">
    <div id="top">
        <!-- .navbar -->
        <?php include($rootdir."includes/navtop.php"); ?>
    </div><!-- /#top -->
    <?php include($rootdir."includes/navleft.php"); ?>
    <div id="content">
        <div class="outer">
            <div class="inner">
                <div class="inner-head">
                    <h2>License Overview</h2>
                </div>
                <?php
                if (!isset($_SESSION["username"])){
                    echo $niet_ingelogd;
                }
                elseif($permission != "1"){
                    echo $geen_toegang;
                }else{
                ?>
                <div class="row">
                    <div class="col-md-10">
                        <div class="panel panel-content panel-default">
                            <div class="panel-heading">
                                <h3 class="panel-title">Extend Enrollments<span class="label label-info"></span></h3>
                            </div>
                            <div class="panel-body">
                                <form role="form" name="selectCourses"  action="<?php echo $_SERVER['PHP_SELF']; ?>" method="post">
                                <div class="form-group">
                                        <label for="inputLicense" class="control-label">Enter Firstname or Lastname or Email:</label>
                                        <input type="text" class="form-control" name="searchstring" id="inputLicenseold">
                                    </div>
                                    <div class="form-group">
                                        <button type="submit" name="zoeken" value="Search"  class="btn btn-primary">Search</button>
                                    </div>

                                    <?php
                                    if ($_POST['update']=='Apply') {
                                        foreach($_POST['courseCheck'] as $checkbox=>$id){
                                            $newDate = $_POST['extend'];
                                            $timestamp = strtotime($newDate);
                                            $nDate= explode(" ", $id);
                                            $result = $db2->query("UPDATE mdl_user_enrolments SET timeend='$timestamp' WHERE userid=$nDate[0] and enrolid=$nDate[1];");
                                            if($result) { echo "<div class=\"alert alert-success\" role=\"alert\">Enrollment extended successfully </div>"; } else { echo "<p>Error: Query 1 not saved successfully</p>";  }


                                            $roles = $db2->query("SELECT * FROM mdl_role_assignments where userid=$nDate[0] and contextid=$nDate[2];");
                                            $aantal_roles = $roles->num_rows;
                                            if($aantal_roles==0){
                                                $role = $db2->query("INSERT INTO mdl_role_assignments (roleid,contextid,userid,timemodified,modifierid)
                                                                      VALUES (5,$nDate[2],$nDate[0],UNIX_TIMESTAMP(now()),2);");
                                                if($role) { echo "<div class=\"alert alert-success\" role=\"alert\">Student Role Successfully added</div>"; } else { echo "<p>Error: Student Role not saved successfully</p>";  }

                                            }

                                        }
                                    }
                                    ?>
                                <div class="row-fluid">
                                    <?php
                                    $searchstring = isset($_POST['searchstring']) ? trim(strtolower($_POST['searchstring'])) : '';
                                    if(!empty($searchstring)){ ?>
                                        <table class="table table-bordered table-condensed table-hover table-striped" id="dataTables-licenses">
                                            <thead>
                                            <tr>
                                                <th>Name</th>
                                                <th>E-mail</th>
                                                <th>Course</th>
                                                <th>Result</th>
                                                <th>Expiry date</th>
                                                <th>Select</th>
                                            </tr>
                                            </thead>
                                            <tbody>
                                            <?php

                                            $user_moodle = $db2->query("SELECT DISTINCT u.id,u.email, concat(u.firstname,' ',u.lastname) as Fullname,
                                                                                c.fullname as CourseName,c.id as CourseID, ue.enrolid as EnrollmentID,con.id as contextid,con.depth,
                                                                                /* ro.name as role, */ FROM_UNIXTIME(ue.timecreated, '%d %M %Y') as Start,FROM_UNIXTIME(ue.timeend,'%d %M %Y') as ExperationDate
                                                                                FROM mdl_user u
                                                                                INNER JOIN mdl_user_enrolments ue on u.id=ue.userid
                                                                                INNER JOIN mdl_enrol en on en.id=ue.enrolid
                                                                                INNER JOIN mdl_course c on en.courseid=c.id
                                                                                INNER JOIN mdl_context con on con.instanceid=c.id
                                                                                /*
                                                                                INNER JOIN mdl_role_assignments r on u.id=r.userid
                                                                                INNER JOIN mdl_role ro on ro.id=r.roleid*/
                                                                                WHERE lower(u.email) like '%$searchstring%' and con.depth='3' or lower(u.firstname) like '%$searchstring%' and con.depth='3'  or lower(u.lastname) like '%$searchstring%'  and con.depth='3'");
                                            $aantal_moodle = $user_moodle->num_rows;
                                            while($row_user_moodle = $user_moodle->fetch_array())
                                            {
                                                echo "<tr>";
                                                echo "<td>" . $row_user_moodle['Fullname'] . "</td>";
                                                echo "<td>" . $row_user_moodle['email'] . "</td>";
                                                echo "<td>" . $row_user_moodle['CourseName'] . "</td>";
                                                $userID = $row_user_moodle['id'];
                                                $courseID = $row_user_moodle['CourseID'];
                                                $result_grades = $db2->query("SELECT gg.itemid, gg.userid, gg.rawgrade, gg.rawgrademax, gg.timemodified, gi.id, gi.itemname FROM mdl_grade_grades AS gg, mdl_grade_items AS gi WHERE gg.itemid = gi.id && gg.userid = '$userID' && gg.rawgrade IS NOT NULL && gi.courseid='$courseID'");
                                                if($row_grades = $result_grades->fetch_array())
                                                {

                                                    echo "<td>" . round($row_grades['rawgrade'],2) . " / " . round($row_grades['rawgrademax'],0) . "</td>";
                                                }else{
                                                    echo "<td>No grades found</td>";
                                                }


                                                echo "<td>" . $row_user_moodle['ExperationDate'] . "</td>";
                                                ?>
                                                <td><input type="checkbox" name="courseCheck[]" value="<?php echo $row_user_moodle['id'].' '.$row_user_moodle['EnrollmentID'].' '.$row_user_moodle['contextid'] ?>" /></td>
                                                <?php
                                                echo "</tr>";



                                                $roles= $db2->query("SELECT * FROM mdl_role_assignments where userid=".$row_user_moodle['id']." and contextid=".$row_user_moodle['contextid']);
                                                $aantal_roles = $roles->num_rows;
                                                if($aantal_roles == 0){
                                                    echo "<div class='well'>The user ( ".$row_user_moodle['Fullname'] .") is missing Student Role status for course: ".$row_user_moodle['CourseName']." </div>";
                                                }
                                            }
                                            ?>
                                            </tbody>
                                        </table>
                                        <div class="row">
                                            <div class="form-group col-sm-offset-4 col-sm-3">
                                                <label for="inputExtend" class="control-label"><h2>Extend selected courses:</h2></label>
                                                <input type="text" class="form-control" id="extend" name="extend" value="<?= date("d-m-Y"); ?>" placeholder="<?= date("d-m-Y"); ?>">
                                            <div class="form-group pull-right ">
                                                <input type="submit" id="update" name="update" value="Apply" class="btn btn-primary btn-lg" />
                                            </div>
                                            </div>
                                        </div>
                                </form>
                                    <?php  }  ?>
                                </div>
                            </div>
                        </div>
                    </div>

            </div><!-- /.inner -->
                    <div class="row">
                        <div class="col-md-10">
                            <div class="panel panel-content panel-default">
                                <div class="panel-heading">
                                    <h3 class="panel-title">Search Quiz attempts<span class="label label-info"></h3>
                                </div>
                                <div class="panel-body">
                                    <form class="form-horizontal" method="post" role="form">
                                        <div class="form-group">
                                            <label for="inputLicense" class="col-sm-2 control-label">Email address:</label>
                                            <div class="col-sm-10">
                                                <input type="text" class="form-control" name="email" id="inputEmail" >
                                            </div>
                                        </div>
                                        <div class="form-group">
                                            <div class="col-sm-offset-2 col-sm-10">
                                                <button type="submit" class="btn btn-primary">Search</button>
                                            </div>
                                        </div>
                                    </form>

                                </div>
                                <?php
                                $email = isset($_POST['email']) ? trim($_POST['email']) : '';

                                if (isset($_POST['email']))
                                {
                                    if(empty($email))
                                    {
                                        echo "Please provide a search string.";

                                    }
                                    else
                                    {
                                        $sql = "SELECT
                                                       u.firstname, u.lastname, u.email, q.name, a.attempt, q.attempts, TRUNCATE(a.sumgrades,2) as grade, TRUNCATE(q.sumgrades,2) as sumgrades, FROM_UNIXTIME(a.timestart,'%Y-%m-%d') as quizDate
                                                FROM
                                                    imi_prod_academy.mdl_quiz_attempts as a
                                                        LEFT JOIN
                                                    imi_prod_academy.mdl_user AS u ON u.id = a.userid
                                                    LEFT JOIN
                                                    imi_prod_academy.mdl_quiz as q on q.id=a.quiz
                                                WHERE
                                                    u.email LIKE '%$email%'";
                                        $attempts = $db2->query($sql);

                                        ?>
                                        <div class="panel-footer">
                                            <table class="table table-bordered table-condensed table-hover table-striped" id="dataTables-attempts">
                                                <thead>
                                                <tr>
                                                    <th>Name</th>
                                                    <th>Quiz</th>
                                                    <th>Attempts</th>
                                                    <th>Grade</th>
                                                    <th>Date</th>
                                                    <th>Email</th>
                                                </tr>
                                                </thead>
                                                <tbody>
                                                <?php
                                                while($row_item = $attempts->fetch_array()) {
                                                    $totalAttempts = $row_item['attempts'];
                                                    if($totalAttempts == 0){
                                                        $total="unlimited";}
                                                    else $total=$totalAttempts
                                                    ?>
                                                    <tr>
                                                        <td><?php echo $row_item['firstname']." ".$row_item['lastname'] ; ?></td>
                                                        <td><?php echo $row_item['name'] ; ?></td>
                                                        <td><?php echo $row_item['attempt']."/".$total ; ?></td>
                                                        <td><?php echo $row_item['grade']. "/" . $row_item['sumgrades'] ; ?></td>
                                                        <td><?php echo $row_item['quizDate'] ; ?></td>
                                                        <td class="email"><?php echo $row_item['email'] ; ?></td>
                                                    </tr>
                                                <?php } ?>
                                                </tbody>
                                            </table>
                                        </div>
                                    <?php } } ?>
                            </div>
                        </div>
                    </div><!-- /.inner -->
                    <?php }?>
            </div><!-- /.outer -->
        </div><!-- /#content -->
        <script>
            $(document).ready(function() {
                $('#dataTables-licenses').dataTable({
                    "pageLength": 25,
                    "order": [ 1    , 'desc' ]
                });

            });
        </script>
    <script>
        $(document).ready(function() {
            $('#dataTables-attempts').dataTable();
        });
    </script>
        <?php include($rootdir."includes/footer.php"); ?>
</body>
</html>