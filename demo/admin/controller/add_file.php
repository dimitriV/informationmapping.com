<?php
require_once(COMMON_CORE_MODEL."cms.class.php");
require_once(COMMON_CORE_COMPONENT."cms.component.php");
include_once(FCK_EDITOR_PATH."fckeditor.php");
$FileToDelete=array();
$MapFileToDelete=array();
///******* Assigning all posted data in local object *********//
$objCms = new cms();
$objCms->cms_id =(isset($_REQUEST['id']) && $_REQUEST['id'] != '')?$_REQUEST['id']:$objCms->cms_id;
$objCmsCmpnt = new cms_component();
///******* Edit Case *********//
if($objCms->cms_id!='' && $action=='edit')
{
	$objCms->selectById();
	$cms = (array) $objCms;



}
///******* Edit Case *********//
foreach($objCms as $k=>$d)
{
	if($k!='db')
	{
		$objCms->$k =(isset($_REQUEST[$k]) && $_REQUEST[$k] != '')?$_REQUEST[$k]:$objCms->$k;
	}
}


$action = isset($_REQUEST['action'])?$_REQUEST['action']:'';
///******* Assigning all posted data in local object *********//

///******* Insert / Update Case *********//
if($_POST['CmsFormSubmit'])
{  $insert_array=array();
    $insertmap_array=array();
/*echo'<pre>';
  print_r($objCms); exit;*/

	$validateData['required'] = array('page_name'=>index_lang::$cms_pagename_req,
									   'status'=>index_lang::$cms_status_req);



	$errorMsg = $commonFunction->validate_form($_POST, $validateData);
	$cms = (array) $objCms;
    foreach ($sitelanguages as $lng){
      if ($_FILES['file'.$lng['id']]['name'] != "")
    {

        $ext = $objCms->commonFunction->GetFileExtension($_FILES['file'.$lng['id']]['name']);
        $name = rtrim($_FILES['file'.$lng['id']]['name'], "." . $ext) . "_" . strtotime("now").$lng['language_name'].".".$ext;

        $res = $objCms->commonFunction->UploadFile($_FILES['file'.$lng['id']], FILE_IMAGE_PATH, $name, FILE_EXTENSIONS);
        if ($res == 1)
        {      // echo $lng['id'];
           $insert_array[$lng['id']]= $name;
            if (file_exists(FILE_IMAGE_PATH . $objCms->files[$lng['id']])&& $action == 'edit')
            {
              
              $FileToDelete[$lng['id']]= $objCms->files[$lng['id']];

            }
            //$obj_resize->resize($name, ADMIN_IMAGE_WIDTH, ADMIN_IMAGE_HEIGHT, "age", true);
         }
        elseif ($res == 0)
        {
            $errorMsg[] = "File not Valid";   //index_lang::$file_type_not_valid;
        }
        else
        {
            $errorMsg[] = "Error in upload";   // index_lang::$file_not_upload;
        }
    }
    else
    {
      $insert_array[$lng['id']]= $objCms->files[$lng['id']];
    }

    }

    foreach ($sitelanguages as $lng){
      if ($_FILES['filemap'.$lng['id']]['name'] != "")
    {

        $ext = $objCms->commonFunction->GetFileExtension($_FILES['filemap'.$lng['id']]['name']);
        $name =  rtrim($_FILES['filemap'.$lng['id']]['name'], "." . $ext) . "_" . strtotime("now") .$lng['language_name']. "." . $ext;

        $res = $objCms->commonFunction->UploadFile($_FILES['filemap'.$lng['id']], FILE_IMAGEMAP_PATH, $name, FILE_EXTENSIONS);
        if ($res == 1)
        {
           $insertmap_array[$lng['id']]= $name;
             if (file_exists(FILE_IMAGEMAP_PATH . $objCms->mapfiles[$lng['id']])&& $action == 'edit')
            {
              //$deletemap=1;
              $MapFileToDelete[$lng['id']]= $objCms->mapfiles[$lng['id']];

            }
            //$obj_resize->resize($name, ADMIN_IMAGE_WIDTH, ADMIN_IMAGE_HEIGHT, "age", true);
         }
        elseif ($res == 0) {
            $errorMsg[] = "File not Valid";   //index_lang::$file_type_not_valid;
        } else {
            $errorMsg[] = "Error in upload";   // index_lang::$file_not_upload;
        }
    }
    else
    {
      $insertmap_array[$lng['id']]= $objCms->mapfiles[$lng['id']];
    }
    }
    $objCms->files= $insert_array;
    $objCms->mapfiles= $insertmap_array;

	if(count($errorMsg) == 0)
    {
		if($objCms->insertUpdate($objCms->cms_id)==false)
		{
           // print_r($_FILES); exit;
			$errorMsg[] = $objCms->error_message;
            foreach($insert_array as $files)
            {
              @unlink(FILE_IMAGE_PATH . $files);
            }
            foreach($insertmap_array as $files)
            {
              @unlink(FILE_IMAGEMAP_PATH . $files);
            }
		}
		else
		{

			foreach($FileToDelete as $files)
            {
              @unlink(FILE_IMAGE_PATH . $files);
            }
            foreach($MapFileToDelete as $files)
            {
              @unlink(FILE_IMAGEMAP_PATH . $files);
            }
			if(!isset($objCms->cms_id))
			{
				$commonFunction->Redirect('./?page=file_list&action=view&msg=1');
				exit;
			}
			else
			{
				$commonFunction->Redirect('./?page=file_list&action=view&msg=2');
				exit;
			}

		}
	}
    else
    {
       foreach($insert_array as $files)
       {
           @unlink(FILE_IMAGE_PATH . $files);
       }
       foreach($insertmap_array as $files)
       {
            @unlink(FILE_IMAGEMAP_PATH . $files);
       }

    }
}
///******* Insert / Update Case *********//
?>