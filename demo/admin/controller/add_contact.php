<?php
$heading="Add/Edit Contact";

require_once(COMMON_CORE_MODEL."contact.class.php");

include_once(FCK_EDITOR_PATH."fckeditor.php");
include(INCLUDE_PATH . 'resize_image.php');
///******* Assigning all posted data in local object *********//
$objContact = new contact();

$Country_array=$objContact->display_countries();
$countries= (array)$Country_array;

$objContact->contact_id =(isset($_REQUEST['id']) && $_REQUEST['id'] != '')?$_REQUEST['id']:$objContact->contact_id;
$obj_resize = new resize_image();
$action = isset($_REQUEST['action'])?$_REQUEST['action']:'';
///******* Edit Case *********//
if($objContact->contact_id!='' && $action=='edit')
{
	$objContact->selectById();
	$cms = (array) $objContact;

}
///******* Edit Case *********//


foreach($objContact as $k=>$d)
{
	if($k!='db')
	{
		$objContact->$k =(isset($_REQUEST[$k]))?$_REQUEST[$k]:$objContact->$k;
	}
}
 $objContact->details= (isset($_REQUEST['details']) && $_REQUEST['details'] != '')?$_REQUEST['details']:$objContact->details;
 $objContact->street= (isset($_REQUEST['street']) && $_REQUEST['street'] != '')?$_REQUEST['street']:$objContact->street;
 $objContact->city = (isset($_REQUEST['city']) && $_REQUEST['city'] != '')?$_REQUEST['city']:$objContact->city;
 $objContact->postalcode= (isset($_REQUEST['postalcode']) && $_REQUEST['postalcode'] != '')?$_REQUEST['postalcode']:$objContact->postalcode;

//$action = isset($_REQUEST['action'])?$_REQUEST['action']:'';
///******* Assigning all posted data in local object *********//


///******* Insert / Update Case *********//
if($_POST['CmsFormSubmit'])
{



    $validateData['required'] = array('name'=>index_lang::$cms_pagename_req,
									   'status'=>index_lang::$cms_status_req);

	$errorMsg = $commonFunction->validate_form($_POST, $validateData);
   /* if($_REQUEST['details'][1]=="")
    {
       $errorMsg[] = "*Address in english is required";
    }
    if($_REQUEST['details'][2]=="")
    {
       $errorMsg[] = "*Address in german is required";
    }
    if($_REQUEST['details'][3]=="")
    {
       $errorMsg[] = "*Address in spain is required";
    }
    if($_REQUEST['details'][4]=="")
    {
       $errorMsg[] = "*Address in dutch is required";
    }
    if($_REQUEST['details'][5]=="")
    {
       $errorMsg[] = "*Address in french is required";
    }
    if($_REQUEST['details'][6]=="")
    {
       $errorMsg[] = "*Address in japan is required";
    }*/

   /*  if($_REQUEST['over_view'][1]=="")
    {
       $errorMsg[] = "*Description in english is required";
    }
    if($_REQUEST['over_view'][2]=="")
    {
       $errorMsg[] = "*Description in german is required";
    }
    if($_REQUEST['over_view'][3]=="")
    {
       $errorMsg[] = "*Description in spain is required";
    }
    if($_REQUEST['over_view'][4]=="")
    {
       $errorMsg[] = "*Description in dutch is required";
    }
    if($_REQUEST['over_view'][5]=="")
    {
       $errorMsg[] = "*Description in french is required";
    }
    if($_REQUEST['over_view'][6]=="")
    {
       $errorMsg[] = "*Description in japan is required";
    }*/

    if ($_FILES['logo']['name'] != "")
    {

        $ext = $objContact->commonFunction->GetFileExtension($_FILES['logo']['name']);
        $name = rtrim($_FILES['logo']['name'], "." . $ext) . "_" . strtotime("now") . "." . $ext;

        $res = $objContact->commonFunction->UploadFile($_FILES['logo'], LOGO_PATH, $name, IMAGE_EXTENSIONS);
        if ($res == 1)
        {
            if (file_exists(LOGO_PATH . $objContact->logo)&& $_REQUEST['id']!= '')
            {
              $delete=1;
              $FileToDelete= $objContact->logo;

            }
             $obj_resize->resize($name, 100, 100, "logo", false);

            $objContact->logo = $name;
            $UploadedFile=  $objContact->logo;
        }
        elseif ($res == 0)
        {
            $errorMsg[] = index_lang::$file_type_not_valid;
        }
        else
        {
            $errorMsg[] = index_lang::$file_not_upload;
        }
    }

	$cms = (array) $objContact;

	if(count($errorMsg) == 0)
    {
  //  echo'<pre>';
  //  print_r($objContact);exit;
		if($objContact->insertUpdate($objContact->contact_id)==false)
		{
			$errorMsg[] = $objContact->error_message;
             @unlink(LOGO_PATH . $UploadedFile);
             @unlink(LOGO_PATH ."logo_resize/100-100-".$UploadedFile);
		}
		else
		{
		  if($delete==1)
          {
              @unlink(LOGO_PATH .$FileToDelete);
              @unlink(LOGO_PATH ."logo_resize/100-100-".$FileToDelete);
              //@unlink(AGE_IMAGE_RESIZE_PATH . ADMIN_IMAGE_WIDTH . "-" . ADMIN_IMAGE_HEIGHT . "-" . $FileToDelete);
          }

			if(!isset($objContact->contact_id))
			{
				$commonFunction->Redirect('./?page=contact_list&action=view&msg=1');
				exit;
			}
			else
			{
				$commonFunction->Redirect('./?page=contact_list&action=view&msg=2');
				exit;
			}
		}
	}
}
///******* Insert / Update Case *********//


?>