<?php

require_once(COMMON_CORE_MODEL."cms.class.php");
$objCms = new cms();
$objPagging= new pagging(RECORDS_PER_PAGE);

$sort_by =(isset($_REQUEST['sort_by']) && $_REQUEST['sort_by'] != '')?$_REQUEST['sort_by']:"cms_id ";
$sort_order =(isset($_REQUEST['sort_order']) && $_REQUEST['sort_order'] != '')?$_REQUEST['sort_order']:"DESC";

if($_REQUEST['search_cms']!="" && isset($_REQUEST['cmsFormSubmit']))
{
	$redirect=str_replace("&searchby=".$_REQUEST['searchby'],"",$commonFunction->current_url());
	$redirect=str_replace("pageno=".$_REQUEST['pageno'],"pageno=1",$redirect);
   
	if(isset($_REQUEST['search_cms']))
	{
	   $qstring="&searchby=".$_REQUEST['search_cms'];	
	}
	$commonFunction->Redirect($redirect.$qstring);
}
if($_REQUEST['msg'])
{
    if($_REQUEST['msg']==1)
    {
        $successMsg[] = index_lang::$cms_success_add;
    }
	if($_REQUEST['msg']==2)
    {
        $successMsg[] = index_lang::$cms_success_update;
    }

    if($_REQUEST['msg']==3)
    {
    	$successMsg[] = index_lang::$cms_success_delete;
    }
}

//DELETE 
if($_REQUEST['id'] !='' && $_REQUEST['action']=='delete')
{	
	$objCms->cms_id = $_REQUEST['id'];
	if($objCms->delete())
	{
		$commonFunction->Redirect('./?page=cms_list&action=view&&msg=3');
	}
}
if(isset($_REQUEST['searchby']) && $_REQUEST['searchby']!="")
{
	$search_cms = $_REQUEST['searchby'];
	$cms = $objCms->select($objPagging,"where c.page_name like '%".mysql_real_escape_string($_REQUEST['searchby'])."%' and c.deleted='0' ",$sort_by, $sort_order);
	$paggingRecord=$objPagging->InfoArray();
}
else
{
	$cms = $objCms->select($objPagging,"where  c.page_name like '%".mysql_real_escape_string($_REQUEST['searchby'])."%' and c.deleted='0' ",$sort_by, $sort_order);
	$paggingRecord=$objPagging->InfoArray();	
}

$extraVars = (isset($_GET)?$_GET:array());
$pagging = $objPagging->print_pagging($extraVars,ADMIN_PAGGING_STYLE);
$starting_record = $paggingRecord['START_OFFSET'];
$ending_record = $paggingRecord['END_OFFSET'];
$total_records = $paggingRecord['TOTAL_RESULTS'];

?>