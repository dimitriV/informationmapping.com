<?php

require_once(COMMON_CORE_MODEL."cms.class.php");
require_once(COMMON_CORE_COMPONENT."cms.component.php");
include_once(FCK_EDITOR_PATH."fckeditor.php");

///******* Assigning all posted data in local object *********//
$objCms = new cms();
$objCmsCmpnt = new cms_component();

foreach($objCms as $k=>$d)
{
	if($k!='db')
	{
		$objCms->$k =(isset($_REQUEST[$k]) && $_REQUEST[$k] != '')?$_REQUEST[$k]:$objCms->$k;
	}
}

$objCms->cms_id =(isset($_REQUEST['id']) && $_REQUEST['id'] != '')?$_REQUEST['id']:$objCms->cms_id;
$action = isset($_REQUEST['action'])?$_REQUEST['action']:'';
///******* Assigning all posted data in local object *********//

///******* Insert / Update Case *********//
if($_POST['CmsFormSubmit'])
{
 /* echo'<pre>';
  print_r($_FILES); exit;*/

	$validateData['required'] = array('page_name'=>index_lang::$cms_pagename_req,
									   'status'=>index_lang::$cms_status_req);

	$validateData['email'] = array('meta_description'=>index_lang::$cms_metadesc_email);
	$validateData['numeric'] = array('meta_keywords'=>index_lang::$cms_metakey_num);

	$errorMsg = $commonFunction->validate_form($_POST, $validateData);
	$cms = (array) $objCms;
    foreach ($sitelanguages as $lng){
      if ($_FILES['file'.$lng['id']]['name'] != "")
    {

        $ext = $objCms->commonFunction->GetFileExtension($_FILES['file'.$lng['id']]['name']);
        $name = rtrim($_FILES['file'.$lng['id']]['name'], "." . $ext) . "_" . strtotime("now") . "." . $ext;

        $res = $objCms->commonFunction->UploadFile($_FILES['file'.$lng['id']], FILE_IMAGE_PATH, $name, FILE_EXTENSIONS);
        if ($res == 1)
        {
           $insert_array[$lng['id']]= $name;

            //$obj_resize->resize($name, ADMIN_IMAGE_WIDTH, ADMIN_IMAGE_HEIGHT, "age", true);
         }
        elseif ($res == 0) {
            $errorMsg[] = "File not Valid";   //index_lang::$file_type_not_valid;
        } else {
            $errorMsg[] = "Error in upload";   // index_lang::$file_not_upload;
        }
    }

    }

    foreach ($sitelanguages as $lng){
      if ($_FILES['filemap'.$lng['id']]['name'] != "")
    {

        $ext = $objCms->commonFunction->GetFileExtension($_FILES['filemap'.$lng['id']]['name']);
        $name = rtrim($_FILES['file'.$lng['id']]['name'], "." . $ext) . "_" . strtotime("now") . "." . $ext;

        $res = $objCms->commonFunction->UploadFile($_FILES['filemap'.$lng['id']], FILE_IMAGEMAP_PATH, $name, FILE_EXTENSIONS);
        if ($res == 1)
        {
           $insertmap_array[$lng['id']]= $name;

            //$obj_resize->resize($name, ADMIN_IMAGE_WIDTH, ADMIN_IMAGE_HEIGHT, "age", true);
         }
        elseif ($res == 0) {
            $errorMsg[] = "File not Valid";   //index_lang::$file_type_not_valid;
        } else {
            $errorMsg[] = "Error in upload";   // index_lang::$file_not_upload;
        }
    }

    }
    $objCms->files= $insert_array;
    $objCms->mapfiles= $insertmap_array;

	if(count($errorMsg) == 0)
    {
		if($objCms->insertUpdate($objCms->cms_id)==false)
		{
			$errorMsg[] = $objCms->error_message;
		}
		else
		{
			//$mail_vars['name'] = 'Admin';
		   //	$objCmsCmpnt->send_mail_cms('Admin','rakesh.advani@sparsh.com',$mail_vars);
			if(!isset($objCms->cms_id))
			{
				$commonFunction->Redirect('./?page=cms_list&action=view&msg=1');
				exit;
			}
			else
			{
				$commonFunction->Redirect('./?page=cms_list&action=view&msg=2');
				exit;
			}
		}
	}
}
///******* Insert / Update Case *********//

///******* Edit Case *********//
elseif($objCms->cms_id!='' && $action=='edit')
{
	$objCms->selectById();
	$cms = (array) $objCms;




}
///******* Edit Case *********//

?>