<?php
$title="Language Management";
$heading=" Add/Edit Language";
require_once(COMMON_CORE_MODEL."language.class.php");
$obj_language = new language();
$flaglogodest =  UPLOAD_PATH."languages/";
foreach($obj_language as $k=>$d)
{
	if($k!='db')
	{
		$obj_language->$k =(isset($_REQUEST[$k]) && $_REQUEST[$k] != '')?$_REQUEST[$k]:$obj_language->$k;
	}
}

$obj_language->id =(isset($_REQUEST['id']) && $_REQUEST['id'] != '')?$_REQUEST['id']:$obj_language->id;
$action = isset($_REQUEST['action'])?$_REQUEST['action']:'';
///******* Assigning all posted data in local object *********//

///******* Insert / Update Case *********//
if($_POST['LanguageFormSubmit'])
{   
	$validateData['required'] = array('language_name'=>'Language Name is Required');

	$errorMsg = $commonFunction->validate_form($_POST, $validateData);
	if($obj_language->checkexist() != false){
		$errorMsg[] = "Language Name already exist.";
	}
	$objlanguage = (array) $obj_language;
	if(isset($_REQUEST['set_default']) && $_REQUEST['set_default'] == 1){
		$obj_language->SetDefault();
	}
	if(isset($_REQUEST['defaultlanguage']) && $_REQUEST['defaultlanguage'] == 1){
		$default=$_REQUEST['defaultlanguage'];
	}
	else{$default=0;}
	if(count($errorMsg) == 0)
    {
    	if(isset($_FILES[flag_image][name]) && !empty($_FILES[flag_image][name])){
    		
			if($commonFunction->UploadFile($_FILES[flag_image],$flaglogodest) != 1){
				$errorMsg[] = index_lang::translate("Flag Image not uploaded");
	        }
			
			$obj_language->flag_image =	$_FILES[flag_image][name];
		}
		if($obj_language->insertUpdate($obj_language->id,$target_url,$default)==false)
		{
			$errorMsg[] = $obj_language->error_message;
		}
		else
		{
			if(!isset($obj_language->id))
			{
				$commonFunction->Redirect('./?page=language_list&action=view&msg=1');
				exit;
			}
			else
			{
				$commonFunction->Redirect('./?page=language_list&action=view&msg=2');
				exit;
			}
		}
	}
}
///******* Insert / Update Case *********//

///******* $objcontact_method Case *********//
elseif($obj_language->id!='' && $action=='edit')
{
	$obj_language->selectById();
	$objlanguage = (array) $obj_language;
}
///******* Edit Case *********//
//print "<pre>";  print_r($objlanguage);die;
?>