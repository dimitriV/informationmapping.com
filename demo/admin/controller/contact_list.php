<?php
$heading="Contact  Management";
require_once(COMMON_CORE_MODEL."contact.class.php");
$objContact = new contact();
$objPagging= new pagging(RECORDS_PER_PAGE);

$sort_by =(isset($_REQUEST['sort_by']) && $_REQUEST['sort_by'] != '')?$_REQUEST['sort_by']:"contact_id ";
$sort_order =(isset($_REQUEST['sort_order']) && $_REQUEST['sort_order'] != '')?$_REQUEST['sort_order']:"DESC";

if($_REQUEST['search_cms']!="" && isset($_REQUEST['cmsFormSubmit']))
{
	$redirect=str_replace("&searchby=".$_REQUEST['searchby'],"",$commonFunction->current_url());
	$redirect=str_replace("pageno=".$_REQUEST['pageno'],"pageno=1",$redirect);

	if(isset($_REQUEST['search_cms']))
	{
	   $qstring="&searchby=".$_REQUEST['search_cms'];
	}
	$commonFunction->Redirect($redirect.$qstring);
}
if($_REQUEST['msg'])
{
    if($_REQUEST['msg']==1)
    {
        $successMsg[] = "Contact Added Successfully";
    }
	if($_REQUEST['msg']==2)
    {
        $successMsg[] = "Contact Updated Successfully";
    }

    if($_REQUEST['msg']==3)
    {
    	$successMsg[] = "Contact Deleted Successfully";
    }
}

//DELETE
if($_REQUEST['id'] !='' && $_REQUEST['action']=='delete')
{
	$objContact->contact_id = $_REQUEST['id'];
	if($objContact->delete())
	{
		$commonFunction->Redirect('./?page=contact_list&action=view&&msg=3');
	}
}
if(isset($_REQUEST['searchby']) && $_REQUEST['searchby']!="")
{
	$search_cms = $_REQUEST['searchby'];
	$cms = $objContact->select($objPagging,"where c.name like '%".mysql_real_escape_string($_REQUEST['searchby'])."%' and c.deleted='0' ",$sort_by, $sort_order);
	$paggingRecord=$objPagging->InfoArray();
}
else
{
	$cms = $objContact->select($objPagging,"where  c.name like '%".mysql_real_escape_string($_REQUEST['searchby'])."%' and c.deleted='0' ",$sort_by, $sort_order);
	$paggingRecord=$objPagging->InfoArray();
}

$extraVars = (isset($_GET)?$_GET:array());
$pagging = $objPagging->print_pagging($extraVars,ADMIN_PAGGING_STYLE);
$starting_record = $paggingRecord['START_OFFSET'];
$ending_record = $paggingRecord['END_OFFSET'];
$total_records = $paggingRecord['TOTAL_RESULTS'];

?>