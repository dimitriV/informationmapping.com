<?php

	/****** Including Language File ********/
	$lang = (isset($_GET['lang']) && $_GET['lang']!='')?$_GET['lang']:'eng';
	if(!file_exists(ADMIN_LANGUAGE_PATH.$lang.'/index.php'))
	{
		$lang = 'eng';
	}
	include_once(ADMIN_LANGUAGE_PATH.$lang.'/index.php');
	/****** Including Language File ********/

	/****** Including Controller File ********/
	$page = (isset($_REQUEST['page']) && $_REQUEST['page']!='')?$_REQUEST['page']:'login';
	if(!file_exists(ADMIN_CONTROLLER_PATH . $page . '.php'))
	{
  		$page = 'contact_list';
	}

    $login_not_required_pages = array('login');
    if (isset($_GET) && !empty($_GET)) {
        foreach ($_GET as $k => $d) {
            $get[] = $k;
            $get[] = $d;
        }
        $get_ = implode(',', $get);
    }

    if (logged_in_user::id() == 0 && !in_array($page, $login_not_required_pages)) {
        $commonFunction->Redirect('./?page=login&action=view&back_url=' . $get_);
    }
	include(ADMIN_CONTROLLER_PATH . $page . '.php');
	/****** Including Controller File ********/
	
	/****** Including Master Page File ********/
	include_once(ADMIN_VIEW_PATH."master_page.php");
	
?>