<?php
/**
 * Groups configuration for default Minify implementation
 * @package Minify
 */

/** 
 * You may wish to use the Minify URI Builder app to suggest
 * changes. http://yourdomain/min/builder/
 *
 * See http://code.google.com/p/minify/wiki/CustomSource for other ideas
 **/

return array(
     'js' => array('//demo/view/js/jquery-1.7.1.js','//demo/view/js/jquery.ui.core.js','//demo/view/js/jquery.ui.slider.js','//demo/view/js/ui/jquery.ui.dialog.js','//demo/view/js/jquery-ui.js'),
     'css' => array('//demo/view/css/stylesheet.css','//demo/view/css/validationEngine.css','//demo/view/base/jquery.ui.theme.css','//demo/view/base/jquery.ui.core.css','//demo/view/base/jquery.ui.resizable.css','//demo/view/base/jquery.ui.selectable.css','//demo/view/base/jquery.ui.accordion.css','//demo/view/base/jquery.ui.autocomplete.css','//demo/view/base/jquery.ui.button.css','//demo/view/base/jquery.ui.dialog.css','//demo/view/base/jquery.ui.slider.css','//demo/view/base/jquery.ui.tabs.css','//demo/view/base/jquery.ui.datepicker.css','//demo/view/base/jquery.ui.progressbar.css')
   );