<?php
    #============================================================================================================
    	#	Created By  			: -
    	#	Created Date			: 22-03-2011
    	#	Purpose					: For Handling  cms
    	#	includes / Obj(Req)		: Files:
    	#	Last update date		: 22-03-2011
    	#	Update Purpose			: For generation
    #============================================================================================================
    class cms
    {
      	public function __construct()
      	{
      		global $commonFunction, $dbAccess;
			$this->db = $dbAccess;
            $this->commonFunction = $commonFunction;
			$fields = $this->db->SelectFields(TBL_CMS);
			foreach($fields as $k=>$d)
			{
				$this->$d = null;
			}

			$this->fields = $fields;

            // Multi language fields
            $this->description=null;
      	}

        public function select($pagging="", $condition='', $sort_by='cms_id', $sort_order = 'ASC')
      	{
      		$query = "SELECT c.*,d.* 
						FROM ".TBL_CMS." c 
						LEFT JOIN ".TBL_DROP_DOWN_OPTIONS." d 
						ON c.status=d.drop_down_option_id ".$condition." 
						ORDER BY ".$sort_by." ".$sort_order;
						
      		$cmsList = $this->db->SimpleQuery($query, "", $pagging, false);
			
			//$cmsList = $this->db->SelectQuery(TBL_CMS, "*", "", "", "", "ORDER BY $sort_by", $sort_order, $pagging, false);
			return $cmsList;
        }
		
		// insert,update query
		public function insertUpdate($id)
		{
               // echo "here";exit;
			//$fields = array('page_name', 'description', 'meta_description', 'meta_keywords', 'status', 'slug_title');
			$fields = $this->fields;
			unset($fields['created_by'], $fields['created_on'], $fields['modified_by'], $fields['modified_on'], $fields['deleted_by'], $fields['deleted_on'], $fields['deleted']);
			foreach($fields as $k=>$d)
			{
				$values[$d] = $this->$d;
			}
			if($id!='')
			{
				$values['modified_by'] = logged_in_user::id();
				$values['modify_on'] = $this->commonFunction->GetDateTime();
			}
			else
			{
				$values['created_by'] = logged_in_user::id();
				$values['created_on'] = $this->commonFunction->GetDateTime();
			}

			if($id!='')
			{
				$where = "cms_id = ".$id;
				$this->db->InsertUpdateQuery(TBL_CMS,$values,$where,false);
                $this->addMultiLangFields($id);
				return true;
			}
			else
			{
				$this->db->InsertUpdateQuery(TBL_CMS,$values,false);
                $this->addMultiLangFields($this->db->lastInsertedId);
				return true;
			}
		}

        public function addMultiLangFields($cmsid){
        //  echo "<pre>";
        // print_r($this->files);
       ///  print_r($this->mapfiles);
       //  exit;
            global $sitelanguages;
            foreach ($sitelanguages as $lng){
                $where = "cms_id=".$cmsid." AND lng_id=".$lng['id'];
                $this->db->DeleteQuery(TBL_FILES,$where);

               $values['cms_id'] = $cmsid;
               $values['lng_id'] = $lng['id'];
               $values['name'] = $this->files[$lng['id']];

               $this->db->InsertUpdateQuery(TBL_FILES,$values,false);

            }
            foreach ($sitelanguages as $lng){
                $where = "cms_id=".$cmsid." AND lng_id=".$lng['id'];
                $this->db->DeleteQuery(TBL_MAPFILES,$where);

               $values['cms_id'] = $cmsid;
               $values['lng_id'] = $lng['id'];
               $values['name'] =  $this->mapfiles[$lng['id']];
               $this->db->InsertUpdateQuery(TBL_MAPFILES,$values,false);

            }

        }
		
		// delete functionality
		public function delete()
		{
			$fields['deleted'] =1;
			$fields['deleted_by'] = logged_in_user::id();
			$fields['deleted_on'] = $this->commonFunction->GetDateTime();
			$this->db->InsertUpdateQuery(TBL_CMS,$fields,"cms_id = ".mysql_real_escape_string($this->cms_id) ,false);
			return true;
		}

		// select by id
		public function selectById()
      	{
			$getCmsDetail = $this->db->SelectQuery(TBL_CMS,"*","cms_id = ".mysql_real_escape_string($this->cms_id),"",false,"","");
			foreach($this as $k=>$d)
			{
				if($k!='db')
				{
					$this->$k =(isset($getCmsDetail[0][$k]))?$getCmsDetail[0][$k]:$this->$k;
				}
			}

            //Get Description
            $getCmsDescDetail = $this->db->SelectQuery(TBL_FILES,"*","cms_id = ".mysql_real_escape_string($this->cms_id),"",false,"","");
			foreach($getCmsDescDetail as $k=>$d)
			{
			  $files[$d['lng_id']] =$d['name'];
            }

            $this->files =$files;

            $getCmsDescDetail1 = $this->db->SelectQuery(TBL_MAPFILES,"*","cms_id = ".mysql_real_escape_string($this->cms_id),"",false,"","");
			foreach($getCmsDescDetail1 as $k=>$d)
			{
			  $mapfiles[$d['lng_id']] =$d['name'];
            }

            $this->mapfiles =$mapfiles;

      	}
        	public function selectByFrontId()
      	{
			$getCmsDetail = $this->db->SelectQuery(TBL_CMS,"*","cms_id = ".mysql_real_escape_string($this->cms_id),"",false,"","");
			foreach($this as $k=>$d)
			{
				if($k!='db')
				{
					$this->$k =(isset($getCmsDetail[0][$k]))?$getCmsDetail[0][$k]:$this->$k;
				}
			}

            //Get Description
            $getCmsDescDetail = $this->db->SelectQuery(TBL_FILES,"*","cms_id = ".mysql_real_escape_string($this->cms_id)." and lng_id= ".$_SESSION['domain_id'],"",false,"","");
            /*echo'<pre>';
            print_r($getCmsDescDetail); exit;*/
            foreach($getCmsDescDetail as $k=>$d)
			{
			  $files[$d['lng_id']] =$d['name'];
			  $que['que'] =$d['question'];
			  $ans['ans'] =$d['answer'];
            }

            $this->files =$files;
            $this->que =$que;
            $this->ans =$ans;

            $getCmsDescDetail1 = $this->db->SelectQuery(TBL_MAPFILES,"*","cms_id = ".mysql_real_escape_string($this->cms_id)." and lng_id= ".$_SESSION['domain_id'],"",false,"","");
			foreach($getCmsDescDetail1 as $k=>$d)
			{
			  $mapfiles[$d['lng_id']] =$d['name'];
			  $mapque['que'] =$d['question'];
			  $mapans['ans'] =$d['answer'];
            }

            $this->mapfiles =$mapfiles;
            $this->mapque =$mapque;
            $this->mapans =$mapans;

      	}
    }
?>