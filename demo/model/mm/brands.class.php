<?php
    #============================================================================================================
    	#	Created By  			: -
    	#	Created Date			: 23-05-2011
    	#	Purpose					: For Handling  Brands
    	#	includes / Obj(Req)		: Files:
    	#	Last update date		: 23-05-2011
    	#	Update Purpose			: For generation
    #============================================================================================================
    class brands
    {
      	public function __construct()
      	{
      		global $commonFunction, $dbAccess;
			$this->db = $dbAccess;
            $this->commonFunction = $commonFunction;
			$fields = $this->db->SelectFields(TBL_BRANDS);
			foreach($fields as $k=>$d)
			{
				$this->$d = null;
			}

			$this->fields = $fields;

            // Multi language fields
            $this->description=null;

      	}

        //Select brands by passed parama
        public function select($pagging="", $condition='', $sort_by='id', $sort_order = 'ASC')
      	{
      		$query = "SELECT b.*,c.name as client_name, if(b.status='1','Active','Inactive') as statustxt,DATE_FORMAT(b.created_date,\"".MYSQL_DATE_FORMAT_WHOLE."\") as created_date
                        FROM ".TBL_BRANDS." b LEFT JOIN ".TBL_CLIENTS." c ON c.id=b.client_id
                        ". $condition."
						ORDER BY ".$sort_by." ".$sort_order;

      		$brandsList = $this->db->SimpleQuery($query, "", $pagging, false);

			return $brandsList;
        }

        //Select all brands
        public function selectAllBrands()
      	{
      		$query = "SELECT * FROM ".TBL_BRANDS." ORDER BY name";
      		$allBrandsList = $this->db->SimpleQuery($query, "", "", false);

			return $allBrandsList;
        }

		// insert,update query
		public function insertUpdate($id)
		{

			$fields = $this->fields;
			unset($fields['created_date'],$fields['modified_date'],$fields['brand_type'],$fields['comm_plans'],$fields['player_restrict']);
			foreach($fields as $k=>$d)
			{
				$values[$d] = $this->$d;
			}

            $values['brand_type'] = (isset($this->brand_type) && $this->brand_type !='')?implode(",",$this->brand_type):'';
            $values['comm_plans'] = (isset($this->comm_plans) && $this->comm_plans !='')?implode(",",$this->comm_plans):'';
            $values['player_restrict'] = (isset($this->player_restrict) && $this->player_restrict !='')?implode(",",$this->player_restrict):'';

			if($id =='')
			{
                $values['created_date'] = $this->commonFunction->GetDateTime();
			}
            $values['modified_date'] = $this->commonFunction->GetDateTime();

			if($id!='')
			{
				$where = "id = ".$id;
				$this->db->InsertUpdateQuery(TBL_BRANDS,$values,$where,false);
                $this->addMultiLangFields($id);
				return true;
			}
			else
			{
				$this->db->InsertUpdateQuery(TBL_BRANDS,$values,false);
                $this->addMultiLangFields($this->db->lastInsertedId);
				return true;
			}
		}

        // Add multilanguage fields
         public function addMultiLangFields($brandid){
            global $sitelanguages;
            foreach ($sitelanguages as $lng){
                $where = "brand_id=".$brandid." AND lng_id=".$lng['id'];
                $this->db->DeleteQuery(TBL_BRAND_DESCRIPTION,$where);

               $values['brand_id'] = $brandid;
               $values['lng_id'] = $lng['id'];
               $values['description'] = $this->description[$lng['id']];

               $this->db->InsertUpdateQuery(TBL_BRAND_DESCRIPTION,$values,false);

            }

        }

        // Update brand's status
        public function updateBrandStatus($brandid,$post){

            //Insert contact informations
            $values['modified_date'] = $this->commonFunction->GetDateTime();
            $values['status'] =$post['status'];

    		$where = "id=".$brandid;
    		$this->db->InsertUpdateQuery(TBL_BRANDS,$values,$where,false);
    		return true;

        }

		// delete functionality
		public function delete()
		{
		     //Delete Description
			$this->db->DeleteQuery(TBL_BRAND_DESCRIPTION,"brand_id = ".mysql_real_escape_string($this->id) ,false);

            //Delete brand info
			$this->db->DeleteQuery(TBL_BRANDS,"id = ".mysql_real_escape_string($this->id) ,false);
			return true;
		}

		// select by id
		public function selectById()
      	{
			$getBrandsDetail = $this->db->SelectQuery(TBL_BRANDS,"*","id = ".mysql_real_escape_string($this->id),"",false,"","");
			foreach($this as $k=>$d)
			{
				if($k!='db')
				{
					$this->$k =(isset($getBrandsDetail[0][$k]))?$getBrandsDetail[0][$k]:$this->$k;
				}
			}

            //Get Description
            $getbrandDescDetail = $this->db->SelectQuery(TBL_BRAND_DESCRIPTION,"*","brand_id = ".mysql_real_escape_string($this->id),"",false,"","");
			foreach($getbrandDescDetail as $k=>$d)
			{
			  $description[$d['lng_id']] =$d['description'];
            }

            $this->description =$description;
      	}
		
		public function selectBylanguageId($brand_id,$language_id)
      	{
		   	$query = "SELECT b.*,bd.description
						FROM ".TBL_BRANDS." b join ".TBL_BRAND_DESCRIPTION." bd on bd.brand_id = b.id WHERE b.id ='".$brand_id."' AND bd.lng_id= '".$language_id."'";
			$brandsList = $this->db->SimpleQuery($query, "", "", false);
			return $brandsList;
      	}
		
		public function selectBybrandtype($brandtype,$language_id)
      	{
			$query = "SELECT b.*,bd.description
						FROM ".TBL_BRANDS." b join ".TBL_BRAND_DESCRIPTION." bd on bd.brand_id = b.id AND bd.lng_id= '".$language_id."' WHERE FIND_IN_SET(".$brandtype.", b.brand_type)";
			$brandsList = $this->db->SimpleQuery($query, "", "", false);
			return $brandsList;
      	}

    }
?>