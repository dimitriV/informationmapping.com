<?php
    #============================================================================================================
    	#	Created By  			: -
    	#	Created Date			: 23-05-2011
    	#	Purpose					: For Handling  Brand Type
    	#	includes / Obj(Req)		: Files:
    	#	Last update date		: 23-05-2011
    	#	Update Purpose			: For generation
    #============================================================================================================
    class clients
    {
      	public function __construct()
      	{
      		global $commonFunction, $dbAccess;
			$this->db = $dbAccess;
            $this->commonFunction = $commonFunction;
			$fields = $this->db->SelectFields(TBL_CLIENTS);
			foreach($fields as $k=>$d)
			{
				$this->$d = null;
			}

			$this->fields = $fields;

      	}

        //Select clients by passed parama
        public function select($pagging="", $condition='', $sort_by='id', $sort_order = 'ASC')
      	{
      		$query = "SELECT *, if(status='1','Active','Inactive') as statustxt,DATE_FORMAT(created_date,\"".MYSQL_DATE_FORMAT_WHOLE."\") as created_date FROM ".TBL_CLIENTS." ". $condition."
						ORDER BY ".$sort_by." ".$sort_order;

      		$clientsList = $this->db->SimpleQuery($query, "", $pagging, false);

			return $clientsList;
        }

        //Select all clients
        public function selectAllClients()
      	{
      		$query = "SELECT * FROM ".TBL_CLIENTS." ORDER BY name";
      		$allClientsList = $this->db->SimpleQuery($query, "", "", false);

			return $allClientsList;
        }

		// insert,update query
		public function insertUpdate($id)
		{

			$fields = $this->fields;
			unset($fields['created_date'],$fields['modified_date'],$fields['brand_type'],$fields['comm_plans'],$fields['player_restrict']);
			foreach($fields as $k=>$d)
			{
				$values[$d] = $this->$d;
			}

            $values['brand_type'] = (isset($this->brand_type) && $this->brand_type !='')?implode(",",$this->brand_type):'';
            $values['comm_plans'] = (isset($this->comm_plans) && $this->comm_plans !='')?implode(",",$this->comm_plans):'';
            $values['player_restrict'] = (isset($this->player_restrict) && $this->player_restrict !='')?implode(",",$this->player_restrict):'';

			if($id =='')
			{
                $values['created_date'] = $this->commonFunction->GetDateTime();
			}
            $values['modified_date'] = $this->commonFunction->GetDateTime();

            $values['payment_date'] = $this->payment_date;

			if($id!='')
			{
				$where = "id = ".$id;
				$this->db->InsertUpdateQuery(TBL_CLIENTS,$values,$where,false);
				return $id;
			}
			else
			{
				$this->db->InsertUpdateQuery(TBL_CLIENTS,$values,false);
				return $this->db->lastInsertedId;
			}
		}

        // Update client's status
        public function updateClientStatus($clientid,$post){

            //Insert contact informations
            $values['modified_date'] = $this->commonFunction->GetDateTime();
            $values['status'] =$post['status'];

    		$where = "id=".$clientid;
    		$this->db->InsertUpdateQuery(TBL_CLIENTS,$values,$where,false);
    		return true;

        }

        // Insert update client's contact
        public function insertUpdateClientContacts($clientid,$post){

             //Delete client contact info
             $this->deleteContactInfo($clientid);

             //Insert contact informations
             foreach($post['contact_name'] as $key=>$postcontacts){
                 $values['no'] =$key;
                 $values['client_id '] =$clientid;
                 $values['name'] = $post['contact_name'][$key];
                 $values['position'] = $post['contact_position'][$key];
                 $values['lng_id'] = $post['contactper_language'][$key];
                 $values['address'] = $post['contact_address'][$key];
                 $values['city'] = $post['contact_city'][$key];
                 $values['country'] = $post['contact_country'][$key];
                 $values['email'] = $post['contact_email'][$key];
                 $values['im_messanger'] = $post['contact_imtype'][$key];
                 $values['im_username'] = $post['contact_username'][$key];
                 $values['primary_contact_num'] = $post['contact_primary_num'][$key];
                 $values['contact_method'] = $post['contact_pre_methods'][$key];
                 $values['newsletter'] = (isset($post['newsletter'][$key]) && $post['newsletter'][$key] !='')?implode(",",$post['newsletter'][$key]):'';

  				 $this->db->InsertUpdateQuery(TBL_CLIENTS_CONTACT_INFO,$values,false);



             }
             return true;

        }


        // Insert update client's note
        public function insertUpdateClientNotes($clientid,$post){

            $noteid = isset($post['noteid'])?$post['noteid']:'';

            //Insert contact informations
            if($noteid =='')
  			{
  			    $values['created_date'] = $this->commonFunction->GetDateTime();
  			}

           $values['notes'] =$post['client_note'];
           $values['client_id'] =$clientid;
           $values['modified_date'] = $this->commonFunction->GetDateTime();


            if($noteid!='')
			{
				$where = "client_id=".$clientid." AND id = ".$noteid;
				$this->db->InsertUpdateQuery(TBL_CLIENT_NOTES,$values,$where,false);
				return $noteid;
			}
			else
			{
				$this->db->InsertUpdateQuery(TBL_CLIENT_NOTES,$values,false);
				return $this->db->lastInsertedId;
			}
        }

        // delete functionality
		public function deleteContactInfo($clientid)
		{
			$this->db->DeleteQuery(TBL_CLIENTS_CONTACT_INFO,"client_id = ".mysql_real_escape_string($clientid) ,false);
			return true;
		}


		// delete functionality
		public function delete()
		{
		    //Delete client contact info
            $this->deleteContactInfo($this->id);

            //Delete client info
			$this->db->DeleteQuery(TBL_CLIENTS,"id = ".mysql_real_escape_string($this->id) ,false);
			return true;
		}

        // delete functionality
		public function deleteClientNote($noteid)
		{
			$this->db->DeleteQuery(TBL_CLIENT_NOTES,"id = ".mysql_real_escape_string($noteid) ,false);
			return true;
		}

		// select by id
		public function selectById()
      	{
			$getClientsDetail = $this->db->SelectQuery(TBL_CLIENTS,"*","id = ".mysql_real_escape_string($this->id),"",false,"","");
			foreach($this as $k=>$d)
			{
				if($k!='db')
				{
					$this->$k =(isset($getClientsDetail[0][$k]))?$getClientsDetail[0][$k]:$this->$k;
				}
			}
      	}

        // select Client note by id
		public function selectClientNoteById($noteid)
      	{
			$getClientsDetail = $this->db->SelectQuery(TBL_CLIENT_NOTES,"*","id = ".mysql_real_escape_string($noteid),"",false,"","");
			return $getClientsDetail[0];
      	}

    	// select contact persons
        public function selectContactPersons(){
            $contact_persons_arr = array();

            $getClientContactPersons = $this->db->SelectQuery(TBL_CLIENTS_CONTACT_INFO,"*","client_id = ".mysql_real_escape_string($this->id),"",false,"","");
            foreach($getClientContactPersons as $contac_persons){
               if(isset($contac_persons['newsletter']) && $contac_persons['newsletter'] !=''){
                 $contac_persons['newsletter'] = explode(",",$contac_persons['newsletter']);
               } else {
                 $contac_persons['newsletter'] = array();
               }
               $contac_persons['language_txt']=$this->selectLanguageById($contac_persons['lng_id']);
               $contac_persons['imtype_txt']=$this->selectContactMethodById($contac_persons['im_messanger']);
               $contac_persons['contactmethod_txt']=$this->selectContactMethodById($contac_persons['contact_method']);
               $contac_persons['country_txt']=$this->selectCountryById($contac_persons['country']);
               $contac_persons['newsletter_txt']=$this->selectNewsletterById($contac_persons['newsletter']);
               $contact_persons_arr[$contac_persons['no']]= $contac_persons;

            }
		    return $contact_persons_arr;
        }

        // get country name
		public function selectCountryById($countryids)
      	{
            $country_list = '';
            $condition = "WHERE country_id IN (".mysql_real_escape_string($countryids).")";
            $query = "SELECT * FROM ".TBL_COUNTRY." ". $condition."
						ORDER BY country_name";

      		$countryList = $this->db->SimpleQuery($query, "", $pagging, false);
            if($countryList != ''){
                foreach($countryList as $clist){
                  $country_list .=   $clist['country_name']."<br>";
                }

            }

			return $country_list;

      	}

        // get Brand Type name
		public function selectBrandTypesById($brandtypeids)
      	{
            $brandtype_list = '';
            if($brandtypeids !=''){
              $condition = "WHERE id IN (".mysql_real_escape_string($brandtypeids).")";
              $query = "SELECT * FROM ".TBL_BRAND_TYPE." ". $condition."
  						ORDER BY name";

        		$brandtypeList = $this->db->SimpleQuery($query, "", $pagging, false);
              if($brandtypeList != ''){
                  foreach($brandtypeList as $blist){
                    $brandtype_list .=   $blist['name']."<br>";
                  }

              }
            }

			return $brandtype_list;
      	}

        // get Language name
		public function selectLanguageById($languageids)
      	{
            $language_list = '';
            if($languageids !='' && $languageids !=0){
              $condition = "WHERE id IN (".mysql_real_escape_string($languageids).")";
              $query = "SELECT * FROM ".TBL_LANGUAGES." ". $condition."
  						ORDER BY language_name";

        		$languageList = $this->db->SimpleQuery($query, "", $pagging, false);
              if($languageList != ''){
                  foreach($languageList as $llist){
                    $language_list .=   $llist['language_name']."<br>";
                  }
              }
            }

			return $language_list;
      	}

        // get Contact Method name
		public function selectContactMethodById($imtypes)
      	{
            $im_list = '';
            if($imtypes !='' && $imtypes !=0){
              $condition = "WHERE id IN (".mysql_real_escape_string($imtypes).")";
              $query = "SELECT * FROM ".TBL_CONTACT_METHOD." ". $condition."
  						ORDER BY name";

        		$imList = $this->db->SimpleQuery($query, "", $pagging, false);
              if($imList != ''){
                  foreach($imList as $llist){
                    $im_list .=   $llist['name']."<br>";
                  }
              }
            }

			return $im_list;
      	}

        // get Newsletter name
		public function selectNewsletterById($newsletertypes)
      	{     $newsletter_type = array('newsletter'=>'Newsletter','news_affiliate_bonus'=>'Affiliates Bonus Offers','news_promotions'=>'Promotions and Updates');
              $newsletter_list='';
              if(isset($newsletertypes) && $newsletertypes !=''){
                foreach($newsletertypes as $nltype){
                  $newsletter_list .=   $newsletter_type[$nltype]."<br>";
                }
              }
			  return $newsletter_list;
      	}

        // Get Client notes
        public function getClientNotes(){
            $getClientNotes = $this->db->SelectQuery(TBL_CLIENT_NOTES,"*","client_id = ".mysql_real_escape_string($this->id),"",false,"","ORDER BY created_date");
			return $getClientNotes;
        }

    }
?>