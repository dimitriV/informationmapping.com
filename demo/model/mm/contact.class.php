<?php
    #============================================================================================================
    	#	Created By  			: -
    	#	Created Date			: 22-03-2011
    	#	Purpose					: For Handling  cms
    	#	includes / Obj(Req)		: Files:
    	#	Last update date		: 22-03-2011
    	#	Update Purpose			: For generation
    #============================================================================================================
    class contact
    {
      	public function __construct()
      	{
      		global $commonFunction, $dbAccess;
			$this->db = $dbAccess;
            $this->commonFunction = $commonFunction;
			$fields = $this->db->SelectFields(TBL_CONTACT);
			foreach($fields as $k=>$d)
			{
				$this->$d = null;
			}

			$this->fields = $fields;

            // Multi language fields
            $this->description=null;
      	}

        public function select($pagging="", $condition='', $sort_by='contact_id', $sort_order = 'ASC')
      	{
      	  //echo"here"; exit;
      		$query = "SELECT c.*,d.*,e.country_name
						FROM ".TBL_CONTACT." c
					   	
						
						LEFT JOIN ".TBL_DROP_DOWN_OPTIONS." d
                        ON c.status=d.drop_down_option_id
						LEFT JOIN ".TBL_COUNTRY." e
						ON c.country_id = e.country_id  ".$condition."
						ORDER BY ".$sort_by." ".$sort_order;

            //echo $query; //exit;,tc.details,tc.street,tc.city,tc.postalcode LEFT JOIN ".TBL_CONTACT_INFO." tc ON c.contact_id = tc.contact_id
      		$cmsList = $this->db->SimpleQuery($query, "", $pagging, false);
            //print_r($cmsList); exit;
			//$cmsList = $this->db->SelectQuery(TBL_CONTACT, "*", "", "", "", "ORDER BY $sort_by", $sort_order, $pagging, false);
			return $cmsList;
        }

		public function display_countries()
      	{
      	  //echo"here"; exit;
      		$query = "SELECT c.country_id,c.country_name
						FROM ".TBL_COUNTRY." c
						ORDER BY country_name ASC";
				  //echo $query; exit;
      		$cmsList = $this->db->SimpleQuery($query, "", $pagging, false);

			//$cmsList = $this->db->SelectQuery(TBL_CONTACT, "*", "", "", "", "ORDER BY $sort_by", $sort_order, $pagging, false);
			return $cmsList;
        }

        	public function display_countries_front()
      	{
      	  //echo"here"; exit;
      		$query = "SELECT DISTINCT (c.country_id),c.country_name
						FROM ".TBL_CONTACT." d LEFT JOIN ".TBL_COUNTRY." c on d.country_id=c.country_id where d.deleted=0
						ORDER BY country_name ASC";
				  //echo $query; exit;
      		$cmsList = $this->db->SimpleQuery($query, "", $pagging, false);

			//$cmsList = $this->db->SelectQuery(TBL_CONTACT, "*", "", "", "", "ORDER BY $sort_by", $sort_order, $pagging, false);
			return $cmsList;
        }
		// insert,update query
		public function insertUpdate($id)
		{
               // echo "here";exit;
			//$fields = array('page_name', 'description', 'meta_description', 'meta_keywords', 'status', 'slug_title');
            //$Check = $this->checkExists();

        /*if (count($Check) > 0)
        {
            $this->error_message = "Same name already exists";
            return false;
        }*/
            $fields = $this->fields;
			unset($fields['created_by'], $fields['created_on'], $fields['modified_by'], $fields['modified_on'], $fields['deleted_by'], $fields['deleted_on'], $fields['deleted']);
			foreach($fields as $k=>$d)
			{
				$values[$d] = $this->$d;
			}
			if($id!='')
			{
				$values['modified_by'] = logged_in_user::id();
				$values['modify_on'] = $this->commonFunction->GetDateTime();
			}
			else
			{
				$values['created_by'] = logged_in_user::id();
				$values['created_on'] = $this->commonFunction->GetDateTime();
			}

			if($id!='')
			{
				$where = "contact_id = ".$id;
				$this->db->InsertUpdateQuery(TBL_CONTACT,$values,$where,false);
                $this->addMultiLangFields($id);
				return true;
			}
			else
			{
				$this->db->InsertUpdateQuery(TBL_CONTACT,$values,false);
                $this->addMultiLangFields($this->db->lastInsertedId);
				return true;
			}
		}
        public function checkExists() {
        $where = "";
        if (logged_in_user::id() != '') {
            $where = " AND country_id='" . $this->country_id . "' AND contact_id!='" . $this->contact_id . "'  AND deleted=0";
        }
        $query = "SELECT contact_id FROM " . TBL_CONTACT . " WHERE name='" . mysql_real_escape_string($this->name) . "' " . $where;
       // echo $query; exit;
        $getUserId = $this->db->SimpleQuery($query, false);
        return $getUserId;
    }

        public function addMultiLangFields($cmsid)
        {

            global $sitelanguages;
            foreach ($sitelanguages as $lng){
                $where = "contact_id=".$cmsid." AND lng_id=".$lng['id'];
                $this->db->DeleteQuery(TBL_CONTACT_INFO,$where);

               $values['contact_id'] = $cmsid;
               $values['lng_id'] = $lng['id'];
               $values['details'] = nl2br($this->details[$lng['id']]);
               $values['street'] = $this->street[$lng['id']];
               $values['city'] = $this->city[$lng['id']];
               $values['postalcode'] = $this->postalcode[$lng['id']];
               $values['over_view'] =  $this->over_view[$lng['id']];

               $this->db->InsertUpdateQuery(TBL_CONTACT_INFO,$values,false);

            }

        }
		
		// delete functionality
		public function delete()
		{
			$fields['deleted'] =1;
			$fields['deleted_by'] = logged_in_user::id();
			$fields['deleted_on'] = $this->commonFunction->GetDateTime();
			$this->db->InsertUpdateQuery(TBL_CONTACT,$fields,"contact_id = ".mysql_real_escape_string($this->contact_id) ,false);
			return true;
		}

		// select by id
		public function selectById()
      	{
			$getCmsDetail = $this->db->SelectQuery(TBL_CONTACT,"*","contact_id = ".mysql_real_escape_string($this->contact_id),"",false,"","");
			foreach($this as $k=>$d)
			{
				if($k!='db')
				{
					$this->$k =(isset($getCmsDetail[0][$k]))?$getCmsDetail[0][$k]:$this->$k;
				}
			}

            //Get Description
            $getDescDetail = $this->db->SelectQuery(TBL_CONTACT_INFO,"*","contact_id = ".mysql_real_escape_string($this->contact_id),"",false,"","");
			foreach($getDescDetail as $k=>$d)
			{
			  $details[$d['lng_id']] =$d['details'];
			  $street[$d['lng_id']] =$d['street'];
			  $city[$d['lng_id']] =$d['city'];
			  $postalcode[$d['lng_id']] =$d['postalcode'];
			  $over_view[$d['lng_id']] =$d['over_view'];
            }

            $this->details =$details;
            $this->street =$street;
            $this->city =$city;
            $this->postalcode =$postalcode;
            $this->over_view =$over_view;


      	}
       	public function selectByIdFront()
      	{
      	  //echo"igiofgifh"; exit;
			$getCmsDetail = $this->db->SelectQuery(TBL_CONTACT,"*","contact_id = ".mysql_real_escape_string($this->contact_id),"",false,"","");
			foreach($this as $k=>$d)
			{
				if($k!='db')
				{
					$this->$k =(isset($getCmsDetail[0][$k]))?$getCmsDetail[0][$k]:$this->$k;
				}
			}

            //Get Description
            $getDescDetail = $this->db->SelectQuery(TBL_CONTACT_INFO,"*","contact_id = ".mysql_real_escape_string($this->contact_id)." and lng_id='".mysql_real_escape_string($_SESSION['domain_id'])."'","",false,"","");
			foreach($getDescDetail as $k=>$d)
			{
			  $details[$d['lng_id']] =$d['details'];
               $street[$d['lng_id']] =$d['street'];
			  $city[$d['lng_id']] =$d['city'];
			  $postalcode[$d['lng_id']] =$d['postalcode'];
               $over_view[$d['lng_id']] =$d['over_view'];
            }

            $this->details =$details;
            $this->street =$street;
            $this->city =$city;
            $this->postalcode =$postalcode;
             $this->over_view =$over_view;


      	}

    }
?>