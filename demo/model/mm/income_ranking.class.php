<?php
    #============================================================================================================
    	#	Created By  			: -
    	#	Created Date			: 24-05-2011
    	#	Purpose					: For Handling  users
    	#	includes / Obj(Req)		: Files:
    	#	Last update date		: 24-05-2011
    	#	Update Purpose			: For generation
    #============================================================================================================
    class income_ranking
    {
      	public function __construct()
      	{
      		global $commonFunction, $dbAccess;
			$this->db = $dbAccess;
            $this->commonFunction = $commonFunction;
			$fields = $this->db->SelectFields(TBL_INCOME_RANKING);
			//print_r($fields);die;
			foreach($fields as $k=>$d)
			{
				$this->$d = null;
			}

			$this->fields = $fields;
            
      	}

        public function select($pagging="", $condition='', $sort_by='id', $sort_order = 'ASC')
      	{
      		$query = "SELECT p.* 
						FROM ".TBL_INCOME_RANKING." p "
						.$condition." 
						ORDER BY ".$sort_by." ".$sort_order;
			//echo $query;die;			
      		$income_ranking_List = $this->db->SimpleQuery($query, "", $pagging, false);
			
			return $income_ranking_List;
        }
		// Check User Exists
		public function checkexist()
      	{
      		$query = "SELECT id 
						FROM ".TBL_INCOME_RANKING." 
						WHERE grade = '".$this->grade."' AND id != '".$this->id."'";
						
      		return $this->db->SimpleQuery($query);
        }		
		// insert,update query
		public function insertUpdate($id="")
		{
			$fields = $this->fields;
			
			foreach($fields as $k=>$d)
			{
				$values[$d] = $this->$d;// $_POST[$d];
			}
			if($id!='')
			{
				$where = "id = ".$id;
				$this->db->InsertUpdateQuery(TBL_INCOME_RANKING,$values,$where,false);
        		return true;
			}
			else
			{
				$this->db->InsertUpdateQuery(TBL_INCOME_RANKING,$values,false);
        		return true;
			}
		}

       	// delete functionality
		public function delete()
		{
			$this->db->DeleteQuery(TBL_INCOME_RANKING,"id = ".mysql_real_escape_string($this->id) ,false);    
			return true;
		}

		// select by id
		public function selectById()
      	{
			$getIncomeRankingDetail = $this->db->SelectQuery(TBL_INCOME_RANKING,"*","id = ".mysql_real_escape_string($this->id),"",false,"","");
			foreach($this as $k=>$d)
			{
				if($k!='db')
				{
					$this->$k =(isset($getIncomeRankingDetail[0][$k]))?$getIncomeRankingDetail[0][$k]:$this->$k;
				}
			}
        
		}
	}
?>