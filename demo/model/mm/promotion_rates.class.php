<?php
    #============================================================================================================
    	#	Created By  			: -
    	#	Created Date			: 23-05-2011
    	#	Purpose					: For Handling  Brands
    	#	includes / Obj(Req)		: Files:
    	#	Last update date		: 23-05-2011
    	#	Update Purpose			: For generation
    #============================================================================================================
    class promotion_rates
    {
      	public function __construct()
      	{
      		global $commonFunction, $dbAccess;
			$this->db = $dbAccess;
            $this->commonFunction = $commonFunction;
			$fields = $this->db->SelectFields(TBL_PROMOTION_RATES);
			foreach($fields as $k=>$d)
			{
				$this->$d = null;
			}

			$this->fields = $fields;

      	}

        //Select brands by passed parama
        public function select($pagging="", $condition='', $sort_by='id', $sort_order = 'ASC')
      	{
      		$query = "SELECT *
                        FROM ".TBL_PROMOTION_RATES." ". $condition."
						ORDER BY ".$sort_by." ".$sort_order;

      		$brandsList = $this->db->SimpleQuery($query, "", $pagging, false);

			return $brandsList;
        }

        //Select users
        public function selectusers($pagging="", $condition='', $sort_by='id', $sort_order = 'ASC')
      	{
      		$query = "SELECT u.*,if(u.user_type='".MANAGER_ID."','Manager',if(u.user_type='".PUBLISHER_ID."','Publisher','')) as usertypetxt
                        FROM ".TBL_USERS." u ". $condition."
						ORDER BY ".$sort_by." ".$sort_order;

      		$usersList = $this->db->SimpleQuery($query, "", $pagging, false);

			return $usersList;
        }

        //Select brands
        public function selectbrands($pagging="", $condition='', $sort_by='name', $sort_order = 'ASC')
      	{
      		$query = "SELECT b.*,c.name as client_name, if(b.status='1','Active','Inactive') as statustxt,DATE_FORMAT(b.created_date,\"".MYSQL_DATE_FORMAT_WHOLE."\") as created_date
                        FROM ".TBL_BRANDS." b LEFT JOIN ".TBL_CLIENTS." c ON c.id=b.client_id
                        ". $condition."
						ORDER BY ".$sort_by." ".$sort_order;

      		$brandsList = $this->db->SimpleQuery($query, "", $pagging, false);

			return $brandsList;
        }

        //Select all brands
        public function selectAllCommissionRates()
      	{
      		$query = "SELECT * FROM ".TBL_PROMOTION_RATES." ORDER BY name";
      		$allBrandsList = $this->db->SimpleQuery($query, "", "", false);

			return $allBrandsList;
        }

		// insert,update query
		public function insertUpdate($id)
		{

			$fields = $this->fields;
			unset($fields['created_date'],$fields['modified_date'],$fields['status']);
			foreach($fields as $k=>$d)
			{
				$values[$d] = $this->$d;
			}

			if($id =='')
			{
                $values['created_date'] = $this->commonFunction->GetDateTime();
			}
            $values['modified_date'] = $this->commonFunction->GetDateTime();

			if($id!='')
			{
				$where = "id = ".$id;
				$this->db->InsertUpdateQuery(TBL_PROMOTION_RATES,$values,$where,false);
				return true;
			}
			else
			{
				$this->db->InsertUpdateQuery(TBL_PROMOTION_RATES,$values,false);
				return true;
			}
		}

        // insert,update query
		public function insertSpecialCommRateUpdate($id)
		{

			$fields = $this->fields;
			unset($fields['created_date'],$fields['modified_date'],$fields['brand_comm_1'],$fields['brand_comm_2']);
			foreach($fields as $k=>$d)
			{
				$values[$d] = $this->$d;
			}
            $values['request_by'] = $this->request_by;
            $values['comm_deduction_from'] = $this->comm_deduction_from;
			if($id =='')
			{
                $values['created_date'] = $this->commonFunction->GetDateTime();
			}
            $values['modified_date'] = $this->commonFunction->GetDateTime();

			if($id!='')
			{
				$where = "id = ".$id;
				$this->db->InsertUpdateQuery(TBL_COMMISSION_SPECIAL_RATES,$values,$where,false);
				return true;
			}
			else
			{
				$this->db->InsertUpdateQuery(TBL_COMMISSION_SPECIAL_RATES,$values,false);
				return true;
			}
		}


        // Update brand's status
        public function updateCommRateStatus($commrateid,$post,$deletetablefrm){
			//Insert contact informations
            $values['modified_date'] = $this->commonFunction->GetDateTime();
            $values['status'] =$post['status'];

    		$where = "id=".$commrateid;
    		$this->db->InsertUpdateQuery($deletetablefrm,$values,$where,false);
    		return true;

        }

		// delete functionality
		public function delete()
		{
		     //Delete Description
			$this->db->DeleteQuery(TBL_BRAND_DESCRIPTION,"brand_id = ".mysql_real_escape_string($this->id) ,false);

            //Delete brand info
			$this->db->DeleteQuery(TBL_PROMOTION_RATES,"id = ".mysql_real_escape_string($this->id) ,false);
			return true;
		}

		// select by id
		public function selectById()
      	{
			$getBrandsDetail = $this->db->SelectQuery(TBL_PROMOTION_RATES,"*","id = ".mysql_real_escape_string($this->id),"",false,"","");
			foreach($this as $k=>$d)
			{
				if($k!='db')
				{
					$this->$k =(isset($getBrandsDetail[0][$k]))?$getBrandsDetail[0][$k]:$this->$k;
				}
			}

            //Get Description
            $getbrandDescDetail = $this->db->SelectQuery(TBL_BRAND_DESCRIPTION,"*","brand_id = ".mysql_real_escape_string($this->id),"",false,"","");
			foreach($getbrandDescDetail as $k=>$d)
			{
			  $description[$d['lng_id']] =$d['description'];
            }

            $this->description =$description;
      	}



        //Select brands
        public function selectbrands_commissionrates($pagging="", $condition='', $sort_by='brand_name', $sort_order = 'ASC')
      	{
      	    $brands_commissions = array();
            // Get Brands
      		$query = "SELECT * FROM ".TBL_BRANDS." ". $condition['brandquery']."	ORDER BY ".$sort_by." ".$sort_order;
      		$brandsList = $this->db->SimpleQuery($query, "", $pagging, false);
            if(isset($brandsList) && $brandsList !=''){
               foreach($brandsList as $brand){
                  $brands_commissions[$brand['id']]= array(
                                                            "id"=>$brand['id'],
                                                            "brand_name"=> $brand['brand_name'],
                                                            );


                  if($brand['comm_plans'] != ''){   //Get commission plans
                      $query = "SELECT * FROM ".TBL_COMMISSION_TYPE." WHERE FIND_IN_SET(id,'".$brand['comm_plans']."')";
        		      $commissionList = $this->db->SimpleQuery($query, "", $pagging, false);

                      foreach($commissionList as $commlist){

                        //Get commission rates
                        $query = "SELECT * FROM ".$condition['commrate_table']." WHERE brand_id=".$brand['id']." AND commission_id=".$commlist['id'].$condition['commratequery'];
          		        $commissionRatesList = $this->db->SimpleQuery($query, "", $pagging, false);

                        //arrange commission rate list
                        $newcommissionRatesList = array();
                        if($commissionRatesList !=''){
                             foreach($commissionRatesList as $commratelist){
                               if($commratelist['rev_share_type'] == 'P'){
                                 $revsharetype="Prog. Rev Share";

                                 $range1_sign="";
                                 $range2_sign="";
                                 $amt1_sign="%";
                                 $amt2_sign="%";
                               } elseif($commratelist['rev_share_type'] == 'G'){
                                 $revsharetype="Gross Rev Share";

                                 $range1_sign="$";
                                 $range2_sign="$";
                                 $amt1_sign="%";
                                 $amt2_sign="%";
                               } else {
                                 $revsharetype="C";

                                 $range1_sign="";
                                 $range2_sign="";
                                 $amt1_sign="$";
                                 $amt2_sign="$";
                               }
                               if($commratelist['hybrid_commtypes'] != ''){
                                  $hybridcomtypeinfo = $this->db->SimpleOneQuery("SELECT * FROM ".TBL_COMMISSION_TYPE." WHERE id=".$commratelist['hybrid_commtypes']);
                                  $revsharetype = $hybridcomtypeinfo->name.",".$revsharetype;

                                  $range1_sign="";
                                  $amt1_sign="$";
                               }

                               $commratelist['range1_sign']=$range1_sign;
                               $commratelist['range2_sign']=$range2_sign;
                               $commratelist['amt1_sign']=$amt1_sign;
                               $commratelist['amt2_sign']=$amt2_sign;

                               $newcommissionRatesList[$revsharetype][]=$commratelist;
                             }
                        }
                        $brands_commissions[$brand['id']]['comm_plan'][]=array("id"=>$commlist['id'],
                                                                                "name"=>$commlist['name'],
                                                                                "totalrow"=>count($commissionRatesList),
                                                                                "comm_rates"=>$newcommissionRatesList
                                                                          );
                      }
                  }
               }
            }
            return $brands_commissions;
        }
    }
?>