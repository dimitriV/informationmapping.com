<?php
    #============================================================================================================
    	#	Created By  			: -
    	#	Created Date			: 24-05-2011
    	#	Purpose					: For Handling  users
    	#	includes / Obj(Req)		: Files:
    	#	Last update date		: 24-05-2011
    	#	Update Purpose			: For generation
    #============================================================================================================
    class country
    {
      	public function __construct()
      	{
      		global $commonFunction, $dbAccess;
			$this->db = $dbAccess;
            $this->commonFunction = $commonFunction;
			$fields = $this->db->SelectFields(TBL_COUNTRY);
			//print_r($fields);die;
			foreach($fields as $k=>$d)
			{
				$this->$d = null;
			}

			$this->fields = $fields;

            // Multi language fields
            $this->im_messanger=null;
			$this->im_username=null;
      	}

        public function select($pagging="", $condition='', $sort_by='country_id', $sort_order = 'ASC')
      	{
      		$query = "SELECT c.* 
						FROM ".TBL_COUNTRY." c "
						.$condition." 
						ORDER BY ".$sort_by." ".$sort_order;
			//echo $query;die;			
      		$userList = $this->db->SimpleQuery($query, "", $pagging, false);
			
			//$cmsList = $this->db->SelectQuery(TBL_CMS, "*", "", "", "", "ORDER BY $sort_by", $sort_order, $pagging, false);
			return $userList;
        }
		// Check User Exists
		public function checkexist()
      	{
      		$query = "SELECT country_id 
						FROM ".TBL_COUNTRY." 
						WHERE country_code = '".$this->country_code."' AND country_id != '".$this->country_id."'";
						
      		return $this->db->SimpleQuery($query);
        }		
		// insert,update query
		public function insertUpdate($id="")
		{
			//$fields = array('page_name', 'description', 'meta_description', 'meta_keywords', 'status', 'slug_title');
			$fields = $this->fields;
			unset($fields['created_on'],$fields['created_by'], $fields['modified_on'],$fields['modified_by']);

			foreach($fields as $k=>$d)
			{
				$values[$d] = $this->$d;// $_POST[$d];
			}
			
			if($id!='')
			{
				$values['modified_on'] = $this->commonFunction->GetDateTime();
				$values['modified_by'] = logged_in_user::admin_id();
			}
			else
			{
				$values['created_on'] = $this->commonFunction->GetDateTime();
				$values['created_by'] = logged_in_user::admin_id();
			}
			if($id!='')
			{
				$where = "country_id = ".$id;
				$this->db->InsertUpdateQuery(TBL_COUNTRY,$values,$where,false);
        		return true;
			}
			else
			{
				$this->db->InsertUpdateQuery(TBL_COUNTRY,$values,false);
        		return true;
			}
		}

       	// delete functionality
		public function delete()
		{
			$this->db->DeleteQuery(TBL_COUNTRY,"country_id = ".mysql_real_escape_string($this->country_id) ,false);    
			return true;
		}

		// select by country_id
		public function selectById()
      	{
			$getCountryDetail = $this->db->SelectQuery(TBL_COUNTRY,"*","country_id = ".mysql_real_escape_string($this->country_id),"",false,"","");
			foreach($this as $k=>$d)
			{
				if($k!='db')
				{
					$this->$k =(isset($getCountryDetail[0][$k]))?$getCountryDetail[0][$k]:$this->$k;
				}
			}
        
		}
	}
?>