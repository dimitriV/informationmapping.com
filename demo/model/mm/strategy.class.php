<?php
    #============================================================================================================
    	#	Created By  			: -
    	#	Created Date			: 24-05-2011
    	#	Purpose					: For Handling  users
    	#	includes / Obj(Req)		: Files:
    	#	Last update date		: 24-05-2011
    	#	Update Purpose			: For generation
    #============================================================================================================
    class strategy
    {
      	public function __construct()
      	{
      		global $commonFunction, $dbAccess;
			$this->db = $dbAccess;
            $this->commonFunction = $commonFunction;
			$fields = $this->db->SelectFields(TBL_STRATEGY);
			//print_r($fields);die;
			foreach($fields as $k=>$d)
			{
				$this->$d = null;
			}

			$this->fields = $fields;
            
      	}

        public function select($pagging="", $condition='', $sort_by='id', $sort_order = 'ASC')
      	{
      		$query = "SELECT s.* 
						FROM ".TBL_STRATEGY." s "
						.$condition." 
						ORDER BY ".$sort_by." ".$sort_order;
			//echo $query;die;			
      		$userList = $this->db->SimpleQuery($query, "", $pagging, false);
			
			//$cmsList = $this->db->SelectQuery(TBL_CMS, "*", "", "", "", "ORDER BY $sort_by", $sort_order, $pagging, false);
			return $userList;
        }
		// Check User Exists
		public function checkexist()
      	{
      		$query = "SELECT id 
						FROM ".TBL_STRATEGY." 
						WHERE name = '".$this->name."' AND id != '".$this->id."'";
						
      		return $this->db->SimpleQuery($query);
        }		
		// insert,update query
		public function insertUpdate($id="")
		{
			//$fields = array('page_name', 'description', 'meta_description', 'meta_keywords', 'status', 'slug_title');
			$fields = $this->fields;
			unset($fields['created_on'], $fields['modified_on']);

			foreach($fields as $k=>$d)
			{
				$values[$d] = $this->$d;// $_POST[$d];
			}
			
			if($id!='')
			{
				$values['modified_on'] = $this->commonFunction->GetDateTime();
			}
			else
			{
				$values['created_on'] = $this->commonFunction->GetDateTime();
			}
			if($id!='')
			{
				$where = "id = ".$id;
				$this->db->InsertUpdateQuery(TBL_STRATEGY,$values,$where,false);
        		return true;
			}
			else
			{
				$this->db->InsertUpdateQuery(TBL_STRATEGY,$values,false);
        		return true;
			}
		}

       	// delete functionality
		public function delete()
		{
			$this->db->DeleteQuery(TBL_STRATEGY,"id = ".mysql_real_escape_string($this->id) ,false);    
			return true;
		}

		// select by id
		public function selectById()
      	{
			$getStrategyDetail = $this->db->SelectQuery(TBL_STRATEGY,"*","id = ".mysql_real_escape_string($this->id),"",false,"","");
			foreach($this as $k=>$d)
			{
				if($k!='db')
				{
					$this->$k =(isset($getStrategyDetail[0][$k]))?$getStrategyDetail[0][$k]:$this->$k;
				}
			}
        
		}
	}
?>