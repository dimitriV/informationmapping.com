<?php
    #============================================================================================================
    	#	Created By  			: -
    	#	Created Date			: 22-03-2011
    	#	Purpose					: For Handling  newsletter
    	#	includes / Obj(Req)		: Files:
    	#	Last update date		: 22-03-2011
    	#	Update Purpose			: For generation
    #============================================================================================================
    class newsletter
    {
      	public function __construct()
      	{
      		global $commonFunction, $dbAccess;
			$this->db = $dbAccess;
            $this->commonFunction = $commonFunction;
			$fields = $this->db->SelectFields(TBL_NEWSLETTER_CONTACTS);
			foreach($fields as $k=>$d)
			{
				$this->$d = null;
			}

			$this->fields = $fields;
			
			$fields_newsletter = $this->db->SelectFields(TBL_NEWSLETTER);
			foreach($fields_newsletter as $k=>$d)
			{
				$this->$d = null;
			}

			$this->fields_newsletter = $fields_newsletter;

      	}
		public function select($pagging="", $condition='', $sort_by='id', $sort_order = 'ASC')
      	{
      		$query = "SELECT n.*,u.username 
						FROM ".TBL_NEWSLETTER_CONTACTS." n "
						.$condition." 
						ORDER BY ".$sort_by." ".$sort_order;
			//echo $query;die;			
      		$userList = $this->db->SimpleQuery($query, "", $pagging, false);
			
			//$cmsList = $this->db->SelectQuery(TBL_CMS, "*", "", "", "", "ORDER BY $sort_by", $sort_order, $pagging, false);
			return $userList;
        }
		// insert,update query
		public function insertUpdate($id)
		{

			$fields = $this->fields;
			unset($fields['status'], $fields['created_date']);
			foreach($fields as $k=>$d)
			{
				$values[$d] = $this->$d;
			}
			if($id!='')
			{
				$values['created_date'] = $this->commonFunction->GetDateTime();
				$values['status'] = 1;
			}
			else
			{
				$values['created_date'] = $this->commonFunction->GetDateTime();
				$values['status'] = 1;
			}

			if($id!='')
			{
				$where = "id = ".$id;
				$this->db->InsertUpdateQuery(TBL_NEWSLETTER_CONTACTS,$values,$where,false);
				return true;
			}
			else
			{
				$this->db->InsertUpdateQuery(TBL_NEWSLETTER_CONTACTS,$values,false);
				return true;
			}
		}

        // select by email
		public function selectByEmail($email)
      	{
			$getNewscontactDetail = $this->db->SimpleOneQuery("SELECT id FROM ".TBL_NEWSLETTER_CONTACTS." WHERE email = '".mysql_real_escape_string($email)."'");

            return $getNewscontactDetail->id;

      	}
		
		public function select_newsletters($pagging="", $condition='', $sort_by='id', $sort_order = 'ASC')
      	{
      		$query = "SELECT n.*
						FROM ".TBL_NEWSLETTER." n "
						.$condition." 
						ORDER BY ".$sort_by." ".$sort_order;
						
      		$newsList = $this->db->SimpleQuery($query, "", $pagging, false);
			
			return $newsList;
        }
		// Check email Exists or not
		public function checkexist_newsletters()
      	{
      		$query = "SELECT id 
						FROM ".TBL_NEWSLETTER." 
						WHERE id != '".$this->id."' AND (title = '".$this->title."' or newsletter_slug = '".$this->newsletter_slug."' )";
			return $this->db->SimpleQuery($query);
        }
		
		// insert,update query
		public function insertUpdate_newsletters($id="")
		{
			//$fields_newsletter = array('page_name', 'description', 'meta_description', 'meta_keywords', 'status', 'slug_title');
			$fields_newsletter = $this->fields_newsletter;//print "<pre>";print_r($fields_newsletter);die;
			unset($fields_newsletter['created_by'], $fields_newsletter['created_on'], $fields_newsletter['modified_by'], $fields_newsletter['modified_on'], $fields_newsletter['deleted_by'], $fields_newsletter['deleted_on'], $fields_newsletter['deleted']);
			foreach($fields_newsletter as $k=>$d)
			{
				$values_newsletter[$d] = $this->$d;// $_POST[$d];
			}
			if($id!='')
			{
				$values_newsletter['modified_by'] = logged_in_user::admin_id();
				$values_newsletter['modified_on'] = $this->commonFunction->GetDateTime();
			}
			else
			{
				$values_newsletter['created_by'] = logged_in_user::admin_id();
				$values_newsletter['created_on'] = $this->commonFunction->GetDateTime();
				$values_newsletter['modified_by'] = logged_in_user::admin_id();
				$values_newsletter['modified_on'] = $this->commonFunction->GetDateTime();
			}
			if(empty($values_newsletter['newsletter_images'])){
				unset($values_newsletter['newsletter_images']);
			}
			if($id!='')
			{
				$where = "id = ".$id;
				$this->db->InsertUpdateQuery(TBL_NEWSLETTER,$values_newsletter,$where,false);
				return true;
			}
			else
			{
				$this->db->InsertUpdateQuery(TBL_NEWSLETTER,$values_newsletter,false);
				return true;
			}
		}
			
		// delete functionality
		public function delete_newsletters()
		{
			$this->db->DeleteQuery(TBL_NEWSLETTER,"id = ".mysql_real_escape_string($this->id) ,false);
			return true;
		}

		// select by id
		public function selectById_newsletters()
      	{
			$getNewsletterDetail = $this->db->SelectQuery(TBL_NEWSLETTER,"*","id = ".mysql_real_escape_string($this->id),"",false,"","");
			foreach($this as $k=>$d)
			{
				if($k!='db')
				{
					$this->$k =(isset($getNewsletterDetail[0][$k]))?$getNewsletterDetail[0][$k]:$this->$k;
				}
			}
			
      	}
	
    }
?>