<?php
    #============================================================================================================
    	#	Created By  			: -
    	#	Created Date			: 23-05-2011
    	#	Purpose					: For Handling  Brand Type
    	#	includes / Obj(Req)		: Files:
    	#	Last update date		: 23-05-2011
    	#	Update Purpose			: For generation
    #============================================================================================================
    class brandtype
    {
      	public function __construct()
      	{
      		global $commonFunction, $dbAccess;
			$this->db = $dbAccess;
            $this->commonFunction = $commonFunction;
			$fields = $this->db->SelectFields(TBL_BRAND_TYPE);
			foreach($fields as $k=>$d)
			{
				$this->$d = null;
			}

			$this->fields = $fields;

      	}

        public function select($pagging="", $condition='', $sort_by='id', $sort_order = 'ASC')
      	{
      		$query = "SELECT *, if(status=1,'Active','Inactive') as statustxt FROM ".TBL_BRAND_TYPE." ". $condition."
						ORDER BY ".$sort_by." ".$sort_order;
						
      		$cmsList = $this->db->SimpleQuery($query, "", $pagging, false);

			//$cmsList = $this->db->SelectQuery(TBL_CMS, "*", "", "", "", "ORDER BY $sort_by", $sort_order, $pagging, false);
			return $cmsList;
        }
		
		// insert,update query
		public function insertUpdate($id)
		{
			$fields = $this->fields;
			unset($fields['created_date'],$fields['modified_date'],$fields['status'],$fields['user_id_a'],$fields['user_id_m']);
			foreach($fields as $k=>$d)
			{
				$values[$d] = $this->$d;
			}
			if($id!='')
			{
				$values['modified_date'] = $this->commonFunction->GetDateTime();
			}
			else
			{
				$values['created_date'] = $this->commonFunction->GetDateTime();
			}

			if($id!='')
			{
				$where = "id = ".$id;
				$this->db->InsertUpdateQuery(TBL_BRAND_TYPE,$values,$where,false);
				return true;
			}
			else
			{
				$this->db->InsertUpdateQuery(TBL_BRAND_TYPE,$values,false);
				return true;
			}
		}

		// delete functionality
		public function delete()
		{
			$this->db->DeleteQuery(TBL_BRAND_TYPE,"id = ".mysql_real_escape_string($this->id) ,false);
			return true;
		}

		// select by id
		public function selectById()
      	{
			$getBrandTypeDetail = $this->db->SelectQuery(TBL_BRAND_TYPE,"*","id = ".mysql_real_escape_string($this->id),"",false,"","");

			foreach($this as $k=>$d)
			{
				if($k!='db')
				{
					$this->$k =(isset($getBrandTypeDetail[0][$k]))?$getBrandTypeDetail[0][$k]:$this->$k;
				}
			}

      	}
    }
?>