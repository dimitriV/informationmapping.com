<?php
    #============================================================================================================
    	#	Created By  			: -
    	#	Created Date			: 23-05-2011
    	#	Purpose					: For Handling  Brands
    	#	includes / Obj(Req)		: Files:
    	#	Last update date		: 23-05-2011
    	#	Update Purpose			: For generation
    #============================================================================================================
    class campaign_access
    {
      	public function __construct()
      	{
      		global $commonFunction, $dbAccess;
			$this->db = $dbAccess;
            $this->commonFunction = $commonFunction;
			$fields = $this->db->SelectFields(TBL_PUBLISHER_COMPAIGN_RIGHTS);
			foreach($fields as $k=>$d)
			{
				$this->$d = null;
			}

			$this->fields = $fields;

      	}

        //Select brands by passed parama
        public function select($pagging="", $condition='', $sort_by='id', $sort_order = 'ASC')
      	{
      		$query = "SELECT *
                        FROM ".TBL_PUBLISHER_COMPAIGN_RIGHTS." ". $condition."
						ORDER BY ".$sort_by." ".$sort_order;

      		$brandsList = $this->db->SimpleQuery($query, "", $pagging, false);

			return $brandsList;
        }

        //Select users
        public function selectusers($pagging="", $condition='', $sort_by='id', $sort_order = 'ASC')
      	{
      		$query = "SELECT u.*,if(u.user_type='".MANAGER_ID."','Manager',if(u.user_type='".PUBLISHER_ID."','Publisher','')) as usertypetxt
                        FROM ".TBL_USERS." u ". $condition."
						ORDER BY ".$sort_by." ".$sort_order;

      		$usersList = $this->db->SimpleQuery($query, "", $pagging, false);

			return $usersList;
        }

        //Select brands
        public function selectbrands($pagging="", $condition='', $sort_by='name', $sort_order = 'ASC')
      	{
      		$query = "SELECT b.*,c.name as client_name, if(b.status='1','Active','Inactive') as statustxt,DATE_FORMAT(b.created_date,\"".MYSQL_DATE_FORMAT_WHOLE."\") as created_date
                        FROM ".TBL_BRANDS." b LEFT JOIN ".TBL_CLIENTS." c ON c.id=b.client_id
                        ". $condition."
						ORDER BY ".$sort_by." ".$sort_order;

      		$brandsList = $this->db->SimpleQuery($query, "", $pagging, false);

			return $brandsList;
        }

        //Select all brands
        public function selectAllCampaignAccess()
      	{
      		$query = "SELECT * FROM ".TBL_PUBLISHER_COMPAIGN_RIGHTS." ORDER BY name";
      		$allBrandsList = $this->db->SimpleQuery($query, "", "", false);

			return $allBrandsList;
        }

		// insert,update query
		public function insertUpdate($id)
		{

			$fields = $this->fields;
			unset($fields['created_date'],$fields['modified_date'],$fields['request_status']);
			foreach($fields as $k=>$d)
			{
				$values[$d] = $this->$d;
			}

			if($id =='')
			{
                $values['created_date'] = $this->commonFunction->GetDateTime();
			}
            $values['modified_date'] = $this->commonFunction->GetDateTime();

			if($id!='')
			{
				$where = "id = ".$id;
				$this->db->InsertUpdateQuery(TBL_PUBLISHER_COMPAIGN_RIGHTS,$values,$where,false);
				return $id;
			}
			else
			{
				$this->db->InsertUpdateQuery(TBL_PUBLISHER_COMPAIGN_RIGHTS,$values,false);
				return $this->db->lastInsertedId;
			}
		}

        // insert,update query
		public function insertUpdateCampaignTicket($ticket_campaign_rights)
		{
            //Check for existing ticket
            //$campaignticket_existinfo = $this->db->SimpleOneQuery("SELECT * FROM ".TBL_TICKET_CAMPAIGN_RIGHTS." WHERE user_id=".$this->user_id." AND brand_id=".$this->brand_id." AND commission_id=".$this->commission_id." AND client_id=".$this->client_id);


			if($id =='')
			{
                $ticket_campaign_rights['created_date'] = $this->commonFunction->GetDateTime();
			}
            $ticket_campaign_rights['modified_date'] = $this->commonFunction->GetDateTime();

			if($id!='')
			{
				$where = "id = ".$id;
				$this->db->InsertUpdateQuery(TBL_TICKET_CAMPAIGN_RIGHTS,$ticket_campaign_rights,$where,false);
				return $id;
			}
			else
			{
				$this->db->InsertUpdateQuery(TBL_TICKET_CAMPAIGN_RIGHTS,$ticket_campaign_rights,false);
				return $this->db->lastInsertedId;
			}
		}


        // Update brand's status
        public function updateCampaignAccessStatus($campaignid,$post){

            //Insert contact informations
            $values['modified_date'] = $this->commonFunction->GetDateTime();
            $values['status'] =$post['status'];

    		$where = "id=".$campaignid;
    		$this->db->InsertUpdateQuery(TBL_PUBLISHER_COMPAIGN_RIGHTS,$values,$where,false);
    		return true;

        }

		// delete functionality
		public function delete()
		{
            //Delete brand info
			$this->db->DeleteQuery(TBL_PUBLISHER_COMPAIGN_RIGHTS,"id = ".mysql_real_escape_string($this->id) ,false);
			return true;
		}

        // select by id
		public function selectById()
      	{
			$getBrandsDetail = $this->db->SelectQuery(TBL_PUBLISHER_COMPAIGN_RIGHTS,"*","id = ".mysql_real_escape_string($this->id),"",false,"","");
			foreach($this as $k=>$d)
			{
				if($k!='db')
				{
					$this->$k =(isset($getBrandsDetail[0][$k]))?$getBrandsDetail[0][$k]:$this->$k;
				}
			}

      	}

		// select by userid, clientid,brandid,commtype
		public function selectByuserId()
      	{
		    $campaign_existinfo = $this->db->SimpleOneQuery("SELECT * FROM ".TBL_PUBLISHER_COMPAIGN_RIGHTS." WHERE user_id=".$this->user_id." AND brand_id=".$this->brand_id." AND commission_id=".$this->commission_id." AND client_id=".$this->client_id);
			return $campaign_existinfo;

      	}

        // Delete campag
		public function DeleteCampaigns($campaign_ids)
      	{
		    //Delete brand info
			$this->db->DeleteQuery(TBL_PUBLISHER_COMPAIGN_RIGHTS,"id NOT IN (".$campaign_ids.") AND user_id=".$this->user_id ,false);
			return true;

      	}

        //Select brands
        public function selectbrands_campaignaccess($pagging="", $condition='', $sort_by='brand_name', $sort_order = 'ASC')
      	{
      	    $brands_commissions = array();
            $totalrows=0;
            //get all clients
            $query = "SELECT * FROM ".TBL_CLIENTS." WHERE status='1' ORDER BY name";
      		$clientsList = $this->db->SimpleQuery($query, "", $pagging, false);
            if(isset($clientsList) && $clientsList !=''){
               foreach($clientsList as $client){
                $totalcommplans = 0;
                $condition['brandquery']="WHERE 1 AND client_id='".$client['id']."'";                 // Get Brands
          		$query = "SELECT * FROM ".TBL_BRANDS." ". $condition['brandquery']." ORDER BY ".$sort_by." ".$sort_order;
          		$brandsList = $this->db->SimpleQuery($query, "", $pagging, false);

                if(isset($brandsList) && !empty($brandsList)){

                   $brands_commissions[$client['id']]['client_name']= $client['name'];
                   $brands_commissions[$client['id']]['client_id']= $client['id'];
                   foreach($brandsList as $brand){
                      if($brand['comm_plans'] != ''){   //Get commission plans
                           $brands_commissions[$client['id']]['brands'][$brand['id']]= array(
                                                                "id"=>$brand['id'],
                                                                "brand_name"=> $brand['brand_name'],
                                                                );


                          $query = "SELECT * FROM ".TBL_COMMISSION_TYPE." WHERE FIND_IN_SET(id,'".$brand['comm_plans']."')";
            		      $commissionList = $this->db->SimpleQuery($query, "", $pagging, false);

                          $totalcommplans = $totalcommplans + count($commissionList);
                          foreach($commissionList as $commlist){

                            //Get commission rates
                            $query = "SELECT * FROM ".TBL_PUBLISHER_COMPAIGN_RIGHTS." WHERE brand_id=".$brand['id']." AND commission_id=".$commlist['id'].$condition['commratequery'];
              		        $campaignAccessList = $this->db->SimpleQuery($query, "", $pagging, false);
                             $brands_commissions[$client['id']]['brands'][$brand['id']]['campaign_access'][]=array("id"=>$commlist['id'],
                                                                                    "name"=>$commlist['name'],
                                                                                    "camp_access"=>$campaignAccessList
                                                                              );
                          }
                      } else {
                        unset($brands_commissions[$client['id']]);
                      }
                   }
                   if($totalcommplans > 0)
                   $brands_commissions[$client['id']]['totalcommplans']= $totalcommplans;

                   $totalrows = $totalrows+$totalcommplans;
                }
               }
            }
            $brands_commissions['totalrows']= $totalrows;
            return $brands_commissions;
        }


        //Select tickets
        public function selectTickets($pagging="", $condition='', $sort_by='id', $sort_order = 'ASC')
      	{

      		$query = "SELECT u.name as requester_name, u.username,tcr.id, tcr.ticket_final_status, tcr.changed_status, tcr.request_by, tcr.created_date, br.brand_name,cm.name as commissiontype
                            FROM ".TBL_TICKET_CAMPAIGN_RIGHTS." tcr
                            JOIN ".TBL_PUBLISHER_COMPAIGN_RIGHTS." pc  ON pc.id=tcr.campaign_id
                            LEFT JOIN ".TBL_USERS." u ON u.id=pc.user_id
                            LEFT JOIN ".TBL_BRANDS." br ON br.id=pc.brand_id
                            LEFT JOIN ".TBL_COMMISSION_TYPE." cm ON cm.id=pc.commission_id
                            ORDER BY tcr.created_date ".$sort_order;

      		$ticketsList = $this->db->SimpleQuery($query, "", $pagging, false);

			return $ticketsList;

        }
    }
?>