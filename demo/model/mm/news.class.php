<?php
    #============================================================================================================
    	#	Created By  			: -
    	#	Created Date			: 20-05-2011
    	#	Purpose					: For Handling  users
    	#	includes / Obj(Req)		: Files:
    	#	Last update date		: 20-05-2011
    	#	Update Purpose			: For generation
    #============================================================================================================
    class news
    {
      	public function __construct()
      	{
      		global $commonFunction, $dbAccess;
			$this->db = $dbAccess;
            $this->commonFunction = $commonFunction;
			$fields = $this->db->SelectFields(TBL_NEWS);
			//print_r($fields);die;
			foreach($fields as $k=>$d)
			{
				$this->$d = null;
			}
			$values['news_image'] = null;
			$this->fields = $fields;
			$this->description=null;
      	}

        public function select($pagging="", $condition='', $sort_by='id', $sort_order = 'ASC')
      	{
      		$query = "SELECT n.*,nd.description,nd.lng_id 
						FROM ".TBL_NEWS." n 
						LEFT JOIN ".TBL_NEWS_DESC." nd 
						ON nd.news_id=n.id "
						.$condition." 
						ORDER BY ".$sort_by." ".$sort_order;
						
      		$newsList = $this->db->SimpleQuery($query, "", $pagging, false);
			
			return $newsList;
        }
		// Check email Exists or not
		public function checkexist()
      	{
      		$query = "SELECT id 
						FROM ".TBL_NEWS." 
						WHERE id != '".$this->id."' AND (name = '".$this->name."' or news_slug = '".$this->news_slug."' )";
			return $this->db->SimpleQuery($query);
        }
		
		// insert,update query
		public function insertUpdate($id="")
		{
			//$fields = array('page_name', 'description', 'meta_description', 'meta_keywords', 'status', 'slug_title');
			$fields = $this->fields;
			unset($fields['created_by'], $fields['created_on'], $fields['modified_by'], $fields['modified_on'], $fields['deleted_by'], $fields['deleted_on'], $fields['deleted']);
			foreach($fields as $k=>$d)
			{
				$values[$d] = $this->$d;// $_POST[$d];
			}
			if($id!='')
			{
				$values['modified_by'] = logged_in_user::admin_id();
				$values['modified_on'] = $this->commonFunction->GetDateTime();
			}
			else
			{
				$values['created_by'] = logged_in_user::admin_id();
				$values['created_on'] = $this->commonFunction->GetDateTime();
				$values['modified_by'] = logged_in_user::admin_id();
				$values['modified_on'] = $this->commonFunction->GetDateTime();
			}
			if(empty($values['news_image'])){
				unset($values['news_image']);
			}
			if($id!='')
			{
				$where = "id = ".$id;
				$this->db->InsertUpdateQuery(TBL_NEWS,$values,$where,false);
				$this->addMultiLangFields($id);
				return true;
			}
			else
			{
				$this->db->InsertUpdateQuery(TBL_NEWS,$values,false);
				$this->addMultiLangFields($this->db->lastInsertedId);
				return true;
			}
		}
			
		public function addMultiLangFields($news_id){
            global $sitelanguages;
            foreach ($sitelanguages as $lng){
                $where = "news_id=".$news_id." AND lng_id=".$lng['id'];
                $this->db->DeleteQuery(TBL_NEWS_DESC,$where);

               $values['news_id'] = $news_id;
               $values['lng_id'] = $lng['id'];
               $values['description'] = $this->description[$lng['id']];

               $this->db->InsertUpdateQuery(TBL_NEWS_DESC,$values,false);

            }

        }
			
		// delete functionality
		public function delete()
		{
			$this->db->DeleteQuery(TBL_NEWS,"id = ".mysql_real_escape_string($this->id) ,false);
			return true;
		}

		// select by id
		public function selectById()
      	{
			$getUserDetail = $this->db->SelectQuery(TBL_NEWS,"*","id = ".mysql_real_escape_string($this->id),"",false,"","");
			foreach($this as $k=>$d)
			{
				if($k!='db')
				{
					$this->$k =(isset($getUserDetail[0][$k]))?$getUserDetail[0][$k]:$this->$k;
				}
			}
			
			//Get Description
            $getNewsDescDetail = $this->db->SelectQuery(TBL_NEWS_DESC,"*","news_id = ".mysql_real_escape_string($this->id),"",false,"","");
			foreach($getNewsDescDetail as $k=>$d)
			{
			  $description[$d['lng_id']] =$d['description'];
            }

            $this->description =$description;
      	}
		public function select_latest_news_by_language_id($langugage_id, $sort_by='modified_on', $sort_order = 'DESC')
      	{
      		$query = "SELECT n.*,nd.description 
						FROM ".TBL_NEWS." n 
						JOIN ".TBL_NEWS_DESC." nd 
						ON nd.news_id=n.id ".
						"WHERE nd.lng_id = '".$langugage_id."' AND n.status='1'  
						ORDER BY ".$sort_by." ".$sort_order;
			//echo $query;die;			
      		$newsList = $this->db->SimpleQuery($query, "", $pagging, false);
			
			return $newsList;
        }
	}
?>