<?php
    #============================================================================================================
    	#	Created By  			: -
    	#	Created Date			: 22-03-2011
    	#	Purpose					: For Handling  cms
    	#	includes / Obj(Req)		: Files:
    	#	Last update date		: 22-03-2011
    	#	Update Purpose			: For generation
    #============================================================================================================
    class category
    {
      	public function __construct()
      	{
      		global $commonFunction, $dbAccess;
			$this->db = $dbAccess;
            $this->commonFunction = $commonFunction;
			$fields = $this->db->SelectFields(TBL_CATEGORY);
			foreach($fields as $k=>$d)
			{
				$this->$d = null;
			}

			$this->fields = $fields;

            // Multi language fields
            $this->description=null;
      	}

        public function select($pagging="", $condition='', $sort_by='id', $sort_order = 'ASC')
      	{
      		$query = "SELECT c.*
						FROM ".TBL_CATEGORY." c 
						".$condition." 
						ORDER BY ".$sort_by." ".$sort_order;
			//die;			
      		$faqList = $this->db->SimpleQuery($query, "", $pagging, false);
			
			return $faqList;
        }
	}
?>