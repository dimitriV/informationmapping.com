<?php
    #============================================================================================================
    	#	Created By  			: -
    	#	Created Date			: 20-05-2011
    	#	Purpose					: For Handling  users
    	#	includes / Obj(Req)		: Files:
    	#	Last update date		: 20-05-2011
    	#	Update Purpose			: For generation
    #============================================================================================================
    class message
    {
      	public function __construct()
      	{
      		global $commonFunction, $dbAccess;
			$this->db = $dbAccess;
            $this->commonFunction = $commonFunction;
			$fields = $this->db->SelectFields(TBL_MESSAGES);
			//print_r($fields);die;
			foreach($fields as $k=>$d)
			{
				$this->$d = null;
			}
			$values['flag_image'] = null;
			$this->fields = $fields;

      	}

        public function select($pagging="", $condition='', $sort_by='id', $sort_order = 'ASC')
      	{
      		$query = "SELECT m.* 
						FROM ".TBL_MESSAGES." m "
						.$condition." 
						ORDER BY ".$sort_by." ".$sort_order;
			//echo $query;die;			
      		$messageList = $this->db->SimpleQuery($query, "", $pagging, false);
			
			//$cmsList = $this->db->SelectQuery(TBL_CMS, "*", "", "", "", "ORDER BY $sort_by", $sort_order, $pagging, false);
			return $messageList;
        }
		
		// insert,update query
		public function insertUpdate()
		{
			//$fields = array('page_name', 'description', 'meta_description', 'meta_keywords', 'status', 'slug_title');
			$fields = $this->fields;
			foreach($fields as $k=>$d)
			{
				$values[$d] = $this->$d;// $_POST[$d];
			}
			//print "<pre>";print_r($values);die;
			foreach($values[to_id] as $to_id){
				$values[to_id] = $to_id;
				$this->db->InsertUpdateQuery(TBL_MESSAGES,$values,false);
			}
			return true;
		}
				
		// delete functionality
		public function delete()
		{
			$this->db->DeleteQuery(TBL_MESSAGES,"id = ".mysql_real_escape_string($this->id) ,false);
			return true;
		}
		
		public function SetStatus($status="",$where="")
      	{
      		$query = "UPDATE ".TBL_MESSAGES." 
						SET status = '".$status."' $where";
			//echo $query;die;
			return $this->db->SimpleUpdateQuery($query);
        }
		
		// select by id
		public function selectById()
      	{
			$getMessageDetail = $this->db->SelectQuery(TBL_MESSAGES,"*","id = ".mysql_real_escape_string($this->id),"",false,"","");
			foreach($this as $k=>$d)
			{
				if($k!='db')
				{
					$this->$k =(isset($getMessageDetail[0][$k]))?$getMessageDetail[0][$k]:$this->$k;
				}
			}
      	}
		public function select_users($id)
      	{
      		$query = "SELECT * 
						FROM ".TBL_USERS . " WHERE id != $id";
						
      		$userList = $this->db->SimpleQuery($query, "", $pagging, false);
			
			//$cmsList = $this->db->SelectQuery(TBL_CMS, "*", "", "", "", "ORDER BY $sort_by", $sort_order, $pagging, false);
			return $userList;
        }
		
	}
?>