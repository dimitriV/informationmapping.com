<?php
    #============================================================================================================
    	#	Created By  			: -
    	#	Created Date			: 24-05-2011
    	#	Purpose					: For Handling  users
    	#	includes / Obj(Req)		: Files:
    	#	Last update date		: 24-05-2011
    	#	Update Purpose			: For generation
    #============================================================================================================
    class access_setting
    {
      	public function __construct()
      	{
      		global $commonFunction, $dbAccess;
			$this->db = $dbAccess;
            $this->commonFunction = $commonFunction;
			$fields = $this->db->SelectFields(TBL_USER_MANAGER_ACCESS);
			//print_r($fields);die;
			foreach($fields as $k=>$d)
			{
				$this->$d = null;
			}

			$this->fields = $fields;
            
      	}

        public function select($pagging="", $condition='', $sort_by='id', $sort_order = 'ASC')
      	{
      		$query = "SELECT s.* 
						FROM ".TBL_USER_MANAGER_ACCESS." s "
						.$condition." 
						ORDER BY ".$sort_by." ".$sort_order;
			//echo $query;die;			
      		$userList = $this->db->SimpleQuery($query, "", $pagging, false);
			
			//$cmsList = $this->db->SelectQuery(TBL_CMS, "*", "", "", "", "ORDER BY $sort_by", $sort_order, $pagging, false);
			return $userList;
        }
		
		public function select_exist($id)
      	{
      		$query = "SELECT id 
						FROM ".TBL_USER_MANAGER_ACCESS." s 
						WHERE user_id ='" .$id. "' ";
      		$userList = $this->db->SimpleQuery($query, "", "", false);
			
			return $userList[0][id];
        }
		// insert,update query
		public function insertUpdateManagerAccess($id="")
		{
			$fields = $this->fields;
			unset($fields[access_withdrawreq],$fields[approve_commrates],$fields[approve_compaignaccess],$fields[approve_withdrawls],$fields[approve_transfers],$fields[approve_userstatus]);
			foreach($fields as $k=>$d)
			{
				$values[$d] = $this->$d;// $_POST[$d];
			}
			$existvalue = $this->select_exist($id);
			if(!empty($existvalue))
			{
				$where = "user_id = ".$id;
				$this->db->InsertUpdateQuery(TBL_USER_MANAGER_ACCESS,$values,$where,false);
        		return true;
			}
			else
			{
				unset($values[id]);
				$this->db->InsertUpdateQuery(TBL_USER_MANAGER_ACCESS,$values,false);
        		return true;
			}
		}
		
		// insert,update query
		public function insertUpdateSubjectApproval($id="")
		{
			$fields = $this->fields;
			unset($fields[access_commtool],$fields[access_fraudcontrol],$fields[access_contacts],$fields[access_withdrawreq],$fields[access_publisherprofile]);
			foreach($fields as $k=>$d)
			{
				$values[$d] = $this->$d;// $_POST[$d];
			}
			$existvalue = $this->select_exist($id);
			if(!empty($existvalue))
			{
				$where = "user_id = ".$id;
				$this->db->InsertUpdateQuery(TBL_USER_MANAGER_ACCESS,$values,$where,false);
        		return true;
			}
			else
			{
				unset($values[id]);
				$this->db->InsertUpdateQuery(TBL_USER_MANAGER_ACCESS,$values,false);
        		return true;
			}
		}

       	// delete functionality
		public function delete()
		{
			$this->db->DeleteQuery(TBL_USER_MANAGER_ACCESS,"id = ".mysql_real_escape_string($this->id) ,false);    
			return true;
		}

		// select by id
		public function selectById()
      	{
			$getStrategyDetail = $this->db->SelectQuery(TBL_USER_MANAGER_ACCESS,"*","id = ".mysql_real_escape_string($this->id),"",false,"","");
			foreach($this as $k=>$d)
			{
				if($k!='db')
				{
					$this->$k =(isset($getStrategyDetail[0][$k]))?$getStrategyDetail[0][$k]:$this->$k;
				}
			}
        
		}
	}
?>