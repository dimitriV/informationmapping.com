<?php
    #============================================================================================================
    	#	Created By  			: -
    	#	Created Date			: 20-05-2011
    	#	Purpose					: For Handling  users
    	#	includes / Obj(Req)		: Files:
    	#	Last update date		: 20-05-2011
    	#	Update Purpose			: For generation
    #============================================================================================================
    class email_templete
    {
      	public function __construct()
      	{
      		global $commonFunction, $dbAccess;
			$this->db = $dbAccess;
            $this->commonFunction = $commonFunction;
			$fields = $this->db->SelectFields(TBL_EMAIL_TEMPLETE);
			//print_r($fields);die;
			foreach($fields as $k=>$d)
			{
				$this->$d = null;
			}

			$this->fields = $fields;

            // Multi language fields
            $this->subject=null;
			$this->body=null;
      	}

        public function select($pagging="", $condition='', $sort_by='letter_template_id', $sort_order = 'ASC')
      	{
      		$query = "SELECT e.* 
						FROM ".TBL_EMAIL_TEMPLETE." e "
						.$condition." 
						ORDER BY ".$sort_by." ".$sort_order;
						
      		$userList = $this->db->SimpleQuery($query, "", $pagging, false);
			
			//$cmsList = $this->db->SelectQuery(TBL_CMS, "*", "", "", "", "ORDER BY $sort_by", $sort_order, $pagging, false);
			return $userList;
        }
		// Check email Exists or not
		public function checkexist()
      	{
      		$query = "SELECT letter_template_id 
						FROM ".TBL_EMAIL_TEMPLETE." 
						WHERE letter_template_id != '".$this->letter_template_id."' AND letter_template_name = '".$this->letter_template_name."'";
			return $this->db->SimpleQuery($query);
        }
		
		// insert,update query
		public function insertUpdate($id="")
		{
			//$fields = array('page_name', 'description', 'meta_description', 'meta_keywords', 'status', 'slug_title');
			$fields = $this->fields;

			foreach($fields as $k=>$d)
			{
				$values[$d] = $this->$d;// $_POST[$d];
			}
			if($id!='')
			{
				$values['modified_by'] = logged_in_user::admin_id();
				$values['modify_on'] = $this->commonFunction->GetDateTime();
			}
			else
			{
				$values['created_by'] = logged_in_user::admin_id();
				$values['created_on'] = $this->commonFunction->GetDateTime();
			}
			if($id!='')
			{
				$where = "letter_template_id = ".$id;
				$this->db->InsertUpdateQuery(TBL_EMAIL_TEMPLETE,$values,$where,false);
				$this->addMultiLangFields($id);
				return true;
			}
			else
			{
				$this->db->InsertUpdateQuery(TBL_EMAIL_TEMPLETE,$values,false);
				$this->addMultiLangFields($this->db->lastInsertedId);
				return true;
			}
		}
		public function addMultiLangFields($letterid){
            global $sitelanguages;
            foreach ($sitelanguages as $lng){
                $where = "letter_template_id=".$letterid." AND lng_id=".$lng['id'];
                $this->db->DeleteQuery(TBL_EMAIL_TEMPLETE_DESC,$where);

               $values['letter_template_id'] = $letterid;
               $values['lng_id'] = $lng['id'];
               $values['subject'] = $this->letter_template_name;
			   $values['body'] = $this->body[$lng['id']];
               $this->db->InsertUpdateQuery(TBL_EMAIL_TEMPLETE_DESC,$values,false);

            }

        }
				
		// delete functionality
		public function delete()
		{
			$this->db->DeleteQuery(TBL_EMAIL_TEMPLETE,"letter_template_id = ".mysql_real_escape_string($this->letter_template_id) ,false);
			$this->db->DeleteQuery(TBL_EMAIL_TEMPLETE_DESC,"letter_template_id = ".mysql_real_escape_string($this->letter_template_id) ,false);
			return true;
		}

		// select by id
		public function selectById()
      	{
			$getEmailTempleteDetail = $this->db->SelectQuery(TBL_EMAIL_TEMPLETE,"*","letter_template_id = ".mysql_real_escape_string($this->letter_template_id),"",false,"","");
			foreach($this as $k=>$d)
			{
				if($k!='db')
				{
					$this->$k =(isset($getEmailTempleteDetail[0][$k]))?$getEmailTempleteDetail[0][$k]:$this->$k;
				}
			}
			
			//Get Description
            $getEmailTempleteDescDetail = $this->db->SelectQuery(TBL_EMAIL_TEMPLETE_DESC,"*","letter_template_id = ".mysql_real_escape_string($this->letter_template_id),"",false,"","");
			foreach($getEmailTempleteDescDetail as $k=>$d)
			{
			  $subject[$d['lng_id']] =$d['subject'];
			  $body[$d['lng_id']] =$d['body'];
            }

            $this->subject =$subject;
			$this->body =$body;
      	}
	}
?>