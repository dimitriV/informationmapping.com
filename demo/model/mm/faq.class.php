<?php
    #============================================================================================================
    	#	Created By  			: -
    	#	Created Date			: 22-03-2011
    	#	Purpose					: For Handling  cms
    	#	includes / Obj(Req)		: Files:
    	#	Last update date		: 22-03-2011
    	#	Update Purpose			: For generation
    #============================================================================================================
    class faq
    {
      	public function __construct()
      	{
      		global $commonFunction, $dbAccess;
			$this->db = $dbAccess;
            $this->commonFunction = $commonFunction;
			$fields = $this->db->SelectFields(TBL_FAQ);
			foreach($fields as $k=>$d)
			{
				$this->$d = null;
			}

			$this->fields = $fields;

            // Multi language fields
            $this->question=null;
			$this->answer=null;
      	}

        public function select($pagging="", $condition='', $sort_by='id', $sort_order = 'ASC')
      	{
      	   	$query = "SELECT f.*,ft.*,c.name cname
						FROM ".TBL_FAQ." f 
						LEFT JOIN ".TBL_FAQ_TEXTS." ft 
						ON f.id=ft.faq_id 
						LEFT JOIN ".TBL_CATEGORY." c 
						ON f.category_id=c.id            
						".$condition." 
						ORDER BY ".$sort_by." ".$sort_order;
			//echo $query;die;			
      		$faqList = $this->db->SimpleQuery($query, "", $pagging, false);
			//$faqList = $this->db->SelectQuery(TBL_CMS, "*", "", "", "", "ORDER BY $sort_by", $sort_order, $pagging, false);
			return $faqList;
        }
		
		// insert,update query
		public function insertUpdate($id)
		{
            $fields = $this->fields;
			foreach($fields as $k=>$d)
			{
				$values[$d] = $this->$d;
			}
			
			if($id!='')
			{
				$where = "id = ".$id;
				$this->db->InsertUpdateQuery(TBL_FAQ,$values,$where,false);
                $this->addMultiLangFields($id);
				return true;
			}
			else
			{
				$this->db->InsertUpdateQuery(TBL_FAQ,$values,false);
                $this->addMultiLangFields($this->db->lastInsertedId);
				return true;
			}
		}

        public function addMultiLangFields($faqid){
            global $sitelanguages;
            foreach ($sitelanguages as $lng){
                $where = "faq_id=".$faqid." AND lng_id=".$lng['id'];
                $this->db->DeleteQuery(TBL_FAQ_TEXTS,$where);

               $values['faq_id'] = $faqid;
               $values['lng_id'] = $lng['id'];
               $values['question'] = $this->question[$lng['id']];
               $values['answer'] = $this->answer[$lng['id']];
               $this->db->InsertUpdateQuery(TBL_FAQ_TEXTS,$values,false);

            }

        }
		
		// delete functionality
		public function delete()
		{
			$this->db->DeleteQuery(TBL_FAQ,"id = ".mysql_real_escape_string($this->id) ,false);
			$this->db->DeleteQuery(TBL_FAQ_TEXTS,"faq_id = ".mysql_real_escape_string($this->id) ,false); 
			return true;
		}

		// select by id
		public function selectById()
      	{      
			$getFaqDetail = $this->db->SelectQuery(TBL_FAQ,"*","id = ".mysql_real_escape_string($this->id),"",false,"","");
			foreach($this as $k=>$d)
			{
				if($k!='db')
				{
					$this->$k =(isset($getFaqDetail[0][$k]))?$getFaqDetail[0][$k]:$this->$k;
				}
			}

            //Get Description
            $getFaqDescDetail = $this->db->SelectQuery(TBL_FAQ_TEXTS,"*","faq_id = ".mysql_real_escape_string($this->id),"",false,"","");
			foreach($getFaqDescDetail as $k=>$d)
			{
			  $question[$d['lng_id']] =$d['question'];
			  $answer[$d['lng_id']] =$d['answer'];
            }

            $this->question =$question;
			$this->answer =$answer;

      	}
    }
?>