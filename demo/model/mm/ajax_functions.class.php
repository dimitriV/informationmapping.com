<?php
    #============================================================================================================
    	#	Created By  			: -
    	#	Created Date			: 24-05-2011
    	#	Purpose					: For Handling  Ajax
    	#	includes / Obj(Req)		: Files:
    	#	Last update date		: 24-05-2011
    	#	Update Purpose			: For generation
    #============================================================================================================
    class ajax_functions
    {
        public function __construct()
      	{
      		global $commonFunction, $dbAccess;
			$this->db = $dbAccess;
            $this->commonFunction = $commonFunction;

      	}

        // Get Brand Types selected by particular client
        function GetBrandTypeOfClient($clientid){

               $query = "SELECT bt.*  FROM ".TBL_BRAND_TYPE." bt,".TBL_CLIENTS." c
                                WHERE FIND_IN_SET(bt.id,c.brand_type) AND c.id=".$clientid." AND bt.status='1' ORDER BY bt.name";

          		$brandTypeofClient = $this->db->SimpleQuery($query, "", false, false);

    			return $brandTypeofClient;
		}

        // Get Commission plans selected by particular client
        function GetCommPlansOfClient($clientid){

               $query = "SELECT cp.*  FROM ".TBL_COMMISSION_TYPE." cp,".TBL_CLIENTS." c
                                WHERE FIND_IN_SET(cp.id,c.comm_plans) AND c.id=".$clientid." AND cp.status='1' ORDER BY cp.name";

          		$commPlansofClient = $this->db->SimpleQuery($query, "", false, false);

    			return $commPlansofClient;
		}

	}
?>