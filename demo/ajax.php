<?php

	$no_load = true;
	
	#INCLUIMOS EL BOOT DEL SISTEMA
	include_once("inc/conf/inc.boot.php");
	
	$ajax = "";
	
	$accion = ((isset($_GET['accion'])) && ($_GET['accion'] != "")) ? $_GET['accion'] : "";

	include_once(PATH_CLASS . "class.ajax.php");
	
	$oAjax = new Ajax(DB_DSN);
	
	$oAjax->ConexionMysqlDsn(DB_DSN);

	if($ajax == ""){
		
		switch($accion){
		
			case 'captcha';#Listar Datos Combo

				$codigo = ((isset($_GET['codigo'])) && ($_GET['codigo'] != "")) ? $_GET['codigo'] : "";
				
				if(($codigo != "") && ($codigo === $_SESSION['captcha_sistema_interno'])){
					
					$ajax = "OK";
					
				}
				
			break;
			
			case 'update_access';#Listar Datos Combo
				
				if($_SESSION["nivel"] > 2){
				
					$id = ((isset($_GET['id'])) && ($_GET['id'] != "")) ? $_GET['id'] : "";
					$estado = ((isset($_GET['estado'])) && ($_GET['estado'] != "")) ? $_GET['estado'] : "";
					
					if(($id > 0) && ($estado != "")){
						
						$ejecucion = $oAjax->UpdateAccess($id, $estado);
						
						$ajax = ($ejecucion == 1) ? "OK" : "NOK";
						
					}
				
				}
				
			break;
			
			case 'get_load_data_tracker';#Listar Datos Combo
				
				if($_SESSION["nivel"] > 1){
				
					$id = ((isset($_GET['id'])) && ($_GET['id'] != "")) ? $_GET['id'] : "";
					
					if(($id > 0)){
						
						$registro = $oAjax->GetLoadDataTracker($id);
						
						if((is_array($registro)) && (sizeof($registro) > 0)){
							
							$ajax = "idtm=" . $registro["id_tipo_de_marca"] . "=" . $registro["tipo_de_marca_name"];
							$ajax .= "=#=";
							$ajax .= "idm=" . $registro["id_marcas"] . "=" . $registro["marca_name"];
							$ajax .= "=#=";
							$ajax .= "idgc=" . $registro["id_grupo_de_campania"] . "=" . $registro["grupo_de_campania_name"];
							
						}
						else{
						
							$ajax = 'NOK';	
							
						}
						
											
					}
					
				}
				
			break;
			
			case 'get_load_brand_type_client';#Listar Datos Combo
				
				if($_SESSION["nivel"] > 1){
				
					$id = ((isset($_GET['id'])) && ($_GET['id'] != "")) ? $_GET['id'] : "";
	
					
					if(($id > 0)){
									
						$listado = $oAjax->GetLoadBrandTypeClient($id);
						
						if((is_array($listado)) && (sizeof($listado) > 0)){
							
							$ajax = "";
							
							foreach($listado as $indice => $datos){
								
								$ajax .= $datos["id"] . "[:?]" . $datos["name"] . ((sizeof($listado) > 1) ? "|*|" : "");
								
								unset($listado[$indice]);
	
							}
							
						}
						else{
						
							$ajax = 'NOK';	
							
						}
						
											
					}

				}
				
			break;

			case 'get_load_brand_type_brand';#Listar Datos Combo
			
				if($_SESSION["nivel"] > 1){
				
					$id = ((isset($_GET['id'])) && ($_GET['id'] != "")) ? $_GET['id'] : "";
					
					if(($id > 0)){
									
						$listado = $oAjax->ListBrandTypeBrand($id);
						$permisos = $oAjax->ListPermissions($id, '1');
						
						if((is_array($listado)) && (sizeof($listado) > 0)){
							
							$ajax = "";
							
							foreach($listado as $indice => $datos){
								
								$ajax .= $datos["id"] . "[:?]" . $datos["name"] . ((isset($permisos[$datos["id"]]) || ($_SESSION["nivel"] == '6')) ? "[:?]1" : "[:?]0") . ((sizeof($listado) > 1) ? "|*|" : "");
								
								unset($listado[$indice]);
	
							}
							
						}
						else{
						
							$ajax = 'NOK';	
							
						}
						
											
					}

				}
				
			break;
			case 'get_load_brand_type_comission';#Listar Datos Combo
			
				if($_SESSION["nivel"] > 1){
				
					$id = ((isset($_GET['id'])) && ($_GET['id'] != "")) ? $_GET['id'] : "";
					
					if(($id > 0)){
									
						$listado = $oAjax->ListBrandTypeComision($id);
						$permisos = $oAjax->ListPermissions($id, '2');
						
						if((is_array($listado)) && (sizeof($listado) > 0)){
							
							$ajax = "";
							
							foreach($listado as $indice => $datos){
								
								$ajax .= $datos["id"] . "[:?]" . $datos["name"] . ((isset($permisos[$datos["id"]]) || ($_SESSION["nivel"] == '6')) ? "[:?]1" : "[:?]0") . ((sizeof($listado) > 1) ? "|*|" : "");
								
								unset($listado[$indice]);
	
							}
							
						}
						else{
						
							$ajax = 'NOK';	
							
						}
							
					}

				}
				
			break;
			case 'get_load_size_of_brand';#Listar Datos Combo
			
				if($_SESSION["nivel"] > 1){
				
					$id = ((isset($_GET['id'])) && ($_GET['id'] != "")) ? $_GET['id'] : "";
					
					if(($id > 0)){
									
						$listado = $oAjax->ListSizeOfBrand($id);
						
						if((is_array($listado)) && (sizeof($listado) > 0)){
							
							$ajax = "";
							
							foreach($listado as $indice => $datos){
								
								$ajax .= $datos["id"] . "[:?]" . $datos["name"] . ((sizeof($listado) > 1) ? "|*|" : "");
								
								unset($listado[$indice]);
	
							}
							
						}
						else{
						
							$ajax = 'NOK';	
							
						}
						
											
					}

				}
				
			break;
			case 'get_load_type_of_brand';#Listar Datos Combo
				
				if($_SESSION["nivel"] > 1){
				
					$id = ((isset($_GET['id'])) && ($_GET['id'] != "")) ? $_GET['id'] : "";
					$id_marcas = ((isset($_GET['idm'])) && ($_GET['idm'] != "")) ? $_GET['idm'] : "";
					
					if(($id > 0)){
									
						$listado = $oAjax->ListTypeOfBrand($id, $id_marcas);
	
						if((is_array($listado)) && (sizeof($listado) > 0)){
	
							$ajax = "";
	
							foreach($listado as $indice => $datos){
	
								$ajax .= $datos["id"] . "[:?]" . $datos["name"] . ((sizeof($listado) > 1) ? "|*|" : "");
	
								unset($listado[$indice]);
	
							}
	
						}
						else{
	
							$ajax = 'NOK';	
	
						}
	
					}
				
				}
				
			break;
			case 'get_load_idioma_of_brand';#Listar Datos Combo

				if($_SESSION["nivel"] > 1){

					$id = ((isset($_GET['id'])) && ($_GET['id'] != "")) ? $_GET['id'] : "";
					$id_marcas = ((isset($_GET['id_marcas'])) && ($_GET['id_marcas'] != "")) ? $_GET['id_marcas'] : "";
					$id_medida = ((isset($_GET['id_medida'])) && ($_GET['id_medida'] != "")) ? $_GET['id_medida'] : "";
	
					if(($id > 0)){
	
						$listado = $oAjax->ListIdiomaOfBrand($id, $id_marcas, $id_medida);
	
						if((is_array($listado)) && (sizeof($listado) > 0)){
	
							$ajax = "";
	
							foreach($listado as $indice => $datos){
	
								$ajax .= $datos["id"] . "[:?]" . $datos["name"] . ((sizeof($listado) > 1) ? "|*|" : "");
	
								unset($listado[$indice]);
	
							}
	
						}
						else{
	
							$ajax = 'NOK';	
	
						}
	
					}
				
				}

			break;

			case 'change_property';#Listar Datos Combo
			
				if($_SESSION["nivel"] > 2){
				
					$value = ((isset($_GET['value'])) && ($_GET['value'] != "")) ? $_GET['value'] : "";
					$table = ((isset($_GET['table'])) && ($_GET['table'] != "")) ? $_GET['table'] : "";
					$field = ((isset($_GET['field'])) && ($_GET['field'] != "")) ? $_GET['field'] : "";
					$field_id = ((isset($_GET['field_id'])) && ($_GET['field_id'] != "")) ? $_GET['field_id'] : "";
					$id_field = ((isset($_GET['id_field'])) && ($_GET['id_field'] != "")) ? $_GET['id_field'] : "";
					
					if(($value != "") && ($table != "") && ($field != "") && ($field_id != "") && ($id_field != "")){
						
						$ejecucion = $oAjax->ChangeProperty($value, $table, $field, $field_id, $id_field);
						
						$ajax = ($ejecucion == 1) ? "OK" : "NOK";
						
					}
				
				}
				
			break;
			case 'permissions_webmaster';#Listar Datos Combo
				
				if($_SESSION["nivel"] > 2){
				
					$id_usuarios = ((isset($_GET['id_usuarios'])) && ($_GET['id_usuarios'] != "")) ? $_GET['id_usuarios'] : "";
					$id_marcas = ((isset($_GET['id_marcas'])) && ($_GET['id_marcas'] != "")) ? $_GET['id_marcas'] : "";
					$id_tipo_de_comision = ((isset($_GET['id_tipo_de_comision'])) && ($_GET['id_tipo_de_comision'] != "")) ? $_GET['id_tipo_de_comision'] : "";
					$estado = ((isset($_GET['estado'])) && ($_GET['estado'] != "")) ? $_GET['estado'] : "";
					
					if(($id_usuarios != "") && ($id_marcas != "") && ($id_tipo_de_comision != "") && ($estado != "")){
						
						$ejecucion = $oAjax->PermissionsWebmaster($id_usuarios, $id_marcas, $id_tipo_de_comision, $estado);
						$oAjax->CalculateRanking("", $id_usuarios);
						
						$ajax = ($ejecucion == 1) ? "OK" : "NOK";
						
					}
				
				}
				
			break;
			case 'permissions_webmaster_pending';#Listar Datos Combo
				
				if($_SESSION["nivel"] == 2){

					$id_marcas = ((isset($_GET['id_marcas'])) && ($_GET['id_marcas'] != "")) ? $_GET['id_marcas'] : "";
					$id_tipo_de_comision = ((isset($_GET['id_tipo_de_comision'])) && ($_GET['id_tipo_de_comision'] != "")) ? $_GET['id_tipo_de_comision'] : "";
					
					if(($id_marcas != "") && ($id_tipo_de_comision != "")){
						
						$ejecucion = $oAjax->PermissionsWebmasterPending($id_marcas, $id_tipo_de_comision);
						$oAjax->CalculateRanking("", $id_usuarios);
						$ajax = ($ejecucion == 1) ? "OK" : "NOK";
						
					}
				
				}
				
			break;
			case 'reset_password';#Listar Datos Combo
				
				if($_SESSION["nivel"] > 2){
				
					$id = ((isset($_GET['id'])) && ($_GET['id'] != "")) ? $_GET['id'] : "";
					
					if(($id != "")){
						
						$data_user = $oAjax->GetDataUser($id);
						
						if((is_array($data_user)) && (sizeof($data_user) > 0)){
							
							$password = ValorAleatorio(10);
							
							$tpl = $oAjax->GetTemplateEmail("1300130683.2862");
							$tpl = html_entity_decode($tpl["html"]);
							if($tpl != ""){
		
								$ejecucion = $oAjax->ChangeProperty($password, "usuarios", "password", "id_usuarios", $id);	
								
								$tpl = str_replace("[USERNAME]", $data_user["username"], $tpl);
								$tpl = str_replace("[PASSWORD]", $password, $tpl);
								$tpl = str_replace("[SITE_NAME]", SITE_NAME, $tpl);
								$tpl = str_replace("[NAME]", $data_user["first_name"], $tpl);
								$tpl = str_replace("[LAST_NAME]", $data_user["last_name"], $tpl);
								$tpl = str_replace("[EMAIL]", $data_user["email"], $tpl);
	
								$ejecucion = Email($data_user["email"], (SITE_NAME . " " . Traducir("Nueva clave")), $tpl, SITE_NAME, EMAIL_SOPORTE);
								
							}
	
						}
						
						$ajax = ($ejecucion == 1) ? "OK" : "NOK";
						
					}
				
				}
				
			break;
			case 'active_type_comision';#Listar Datos Combo

				if($_SESSION["nivel"] > 2){

					$id = ((isset($_GET['id'])) && ($_GET['id'] != "")) ? $_GET['id'] : "";

					if(($id != "")){

						$data_permissions = $oAjax->GetDataPermissions($id);

						$data_user = $oAjax->GetDataUser($data_permissions["id_usuarios"]);

						if((is_array($data_user)) && (sizeof($data_user) > 0)){
		
							if((!isset($_SESSION[PERMISOS_MANAGER]["aprobar_comision"])) || ($_SESSION["nivel"] > 3)){
							
								$ejecucion = $oAjax->ChangeProperty(1, "usuarios_webmasters_permisos", "estado", "id_usuarios_webmasters_permisos", $id);	
								
							}
							else{

								$ejecucion = $oAjax->ChangeProperty(3, "usuarios_webmasters_permisos", "estado", "id_usuarios_webmasters_permisos", $id);	
								
							}
							
							$oAjax->ChangeProperty($_SESSION[DATOS_USUARIOS]["id_usuarios"], "usuarios_webmasters_permisos", "id_usuarios_a", "id_usuarios_webmasters_permisos", $id);	
							$oAjax->CalculateRanking($id);

							#Creat un ticket aca

	
						}
						
						$ajax = ($ejecucion == 1) ? "OK" : "NOK";
						
					}
				
				}
				
			break;
			case 'remove_type_comision';#Listar Datos Combo

				if($_SESSION["nivel"] > 2){

					$id = ((isset($_GET['id'])) && ($_GET['id'] != "")) ? $_GET['id'] : "";

					if(($id != "")){

						$data_permissions = $oAjax->GetDataPermissions($id);

						$data_user = $oAjax->GetDataUser($data_permissions["id_usuarios"]);

						if((is_array($data_user)) && (sizeof($data_user) > 0)){

							if((!isset($_SESSION[PERMISOS_MANAGER]["aprobar_comision"])) || ($_SESSION["nivel"] > 3)){
							
								$ejecucion = $oAjax->ChangeProperty(1, "usuarios_webmasters_permisos", "estado", "id_usuarios_webmasters_permisos", $id);	
								
							}
							else{

								$ejecucion = $oAjax->ChangeProperty(3, "usuarios_webmasters_permisos", "estado", "id_usuarios_webmasters_permisos", $id);	
								
							}
							
							$oAjax->ChangeProperty($_SESSION[DATOS_USUARIOS]["id_usuarios"], "usuarios_webmasters_permisos", "id_usuarios_a", "id_usuarios_webmasters_permisos", $id);	
							$oAjax->CalculateRanking($id);
	
						}
						
						$ajax = ($ejecucion == 1) ? "OK" : "NOK";
						
					}
				
				}
				
			break;
			
			
			case 'sent_message_pm';#Listar Datos Combo
				
				if($_SESSION["nivel"] > 1){

					$suject = ((isset($_GET['suject'])) && ($_GET['suject'] != "")) ? $_GET['suject'] : "";
					$contenido = ((isset($_GET['contenido'])) && ($_GET['contenido'] != "")) ? $_GET['contenido'] : "";
					
					if(($suject != "") && ($contenido != "")){
						
						$ejecucion = $oAjax->SendMessage($suject, $contenido);
						
						$ajax = ($ejecucion == 1) ? "OK" : "NOK";
						
					}
				
				}
				
			break;
			
			case 'change_status_tickect';#Listar Datos Combo
				
				if($_SESSION["nivel"] > 1){

					$id = ((isset($_GET['id'])) && ($_GET['id'] != "")) ? $_GET['id'] : "";
					
					if($id != ""){
						
						$sql = "SELECT * FROM tickets WHERE id_usuarios_hasta = '" . $_SESSION[DATOS_USUARIOS]["id_usuarios"] . "' AND id_tickets = '" . $id . "'";
						
						$registro = $oAjax->Sqlregistro($sql);
						
						if((is_array($registro)) && (sizeof($registro) > 0)){
						
							$ejecucion = $oAjax->ChangeProperty(1, "tickets", "estado", "id_tickets", $id);	
							
							$ajax = ($ejecucion == 1) ? "OK" : "NOK";
						
						}
						
					}
				
				}
				
			break;
			case 'change_status_support';#Listar Datos Combo
				
				if($_SESSION["nivel"] > 1){

					$id = ((isset($_GET['id'])) && ($_GET['id'] != "")) ? $_GET['id'] : "";
					
					if($id != ""){
						
						$sql = "SELECT * FROM soporte WHERE id_usuarios_hasta = '" . $_SESSION[DATOS_USUARIOS]["id_usuarios"] . "' AND id_soporte = '" . $id . "' ";
						
						$registro = $oAjax->SqlRegistro($sql);
						
						if((is_array($registro)) && (sizeof($registro) > 0)){
						
							$ejecucion = $oAjax->ChangeProperty(1, "soporte", "estado", "id_soporte", $id);	
							
							$ajax = ($ejecucion == 1) ? "OK" : "NOK";

						}
						
					}
				
				}
				
			break;
		}
	
	}
	
	if($ajax == ''){
		
		echo "NOK";
		
	}
	else{
	
		$ajax = utf8_decode($ajax);
		$ajax = html_entity_decode($ajax);
		$ajax = utf8_encode($ajax);
		
		echo $ajax;

		
	}
	
	die;
	
?>
