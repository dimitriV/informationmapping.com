<?php

	class index_lang
	{   //Login
         	public static $welcome='Welcome to Admin Panel of IMF';
		// General
		public static $add='Add';
		public static $edit='Edit';
		public static $delete='Delete';
		public static $username='Username';
		public static $password='Password';
		public static $login='Login';
	    public static $ForgotPassword='Forget Password';
		public static $showing='Showing';
		public static $no='No.';
		public static $of='of';
		public static $to='to';
		public static $action='Action';
		public static $no_items='No Items';
		public static $suredelete='Are you sure you want to delete?';
        public static $fieldisrequired = 'This field is required';
        //login
        public static $username_required = 'Username is required';
        public static $pwd_required = 'Password is required';

        //file
         public static $File_management = 'File Management';
         public static $File = 'File';


        // Add CMS Page
		public static $cms_title='Add/Update CMS';
		public static $cms_header='Add/Update CMS';
		public static $cms_pagename='Page Name';
		public static $cms_description='Description';
		public static $cms_metadesc='Meta Description';
		public static $cms_metakey='Meta Keywords';
		public static $cms_status='Status';
		public static $submit='Submit';
		public static $cancel='Cancel';
		public static $cms_description_req='Description is required';
		public static $cms_pagename_req='Page Name is required';
		public static $cms_status_req='Status is required';
		public static $cms_metadesc_email='Invalid Email Address in Meta Description';
		public static $cms_metakey_num='Meta Keywords must be numeric value';
		
		// CMS List Page
		public static $cms_add='Add CMS';
		public static $cms_management='CMS Management';
		public static $cms_success_add='CMS Added Successfully';
		public static $cms_success_update='CMS Updated Successfully';
		public static $cms_success_delete='CMS Deleted Successfully';
		public static $cms_lblSearch='Search CMS';
		public static $cms_btnSearch='Search';
		public static $cmslist_header='CMS';
        public static function translate($text){

            global $dbAccess,$cur_lang,$cur_lang_id;
            $lang_id = $cur_lang_id;

			$db = $dbAccess;
    		/*$data = $db->SimpleOneQuery( "SELECT * FROM ".TBL_LANGUAGES." WHERE lng_code='".$cur_lang."'");

            if(isset($data->id) && $data->id !='')
            {
               $lang_id = $data->id;
            }   */

            $text = utf8_decode($text);
    		$text = html_entity_decode($text);
    		$text = utf8_encode($text);

            if($text != ""){
                //echo "SELECT id FROM ".TBL_LANGUAGES_VAR." WHERE name = '" . ucfirst(strtolower(htmlspecialchars(htmlspecialchars_decode($text), ENT_QUOTES))) . "'";
                //$query_textvar_data =  $db->SimpleOneQuery("SELECT id FROM ".TBL_LANGUAGES_VAR." WHERE name = '" . ucfirst(strtolower(htmlspecialchars(htmlspecialchars_decode($text), ENT_QUOTES))) . "'");
                $query_textvar_data =  $db->SimpleOneQuery("SELECT id FROM ".TBL_LANGUAGES_VAR." WHERE name = '" . addslashes($text) . "'");
                if(isset($query_textvar_data->id) && $query_textvar_data->id != ''){
                       $lang_var_id = $query_textvar_data->id;
                } else {
                    //$values_lang_var['name'] = ucfirst(strtolower(htmlspecialchars(htmlspecialchars_decode($text), ENT_QUOTES)));
                    $values_lang_var['name'] = $text;
				    $db->InsertUpdateQuery(TBL_LANGUAGES_VAR,$values_lang_var,false);

                    $lang_var_id = $db->lastInsertedId;

                    $values_lang_text['lng_var_id'] = $lang_var_id;
                    $values_lang_text['lng_id'] = $lang_id;
                    //$values_lang_text['txt'] = ucfirst(strtolower(htmlspecialchars(htmlspecialchars_decode($text), ENT_QUOTES)));
                    $values_lang_text['txt'] = $text;
				    $db->InsertUpdateQuery(TBL_LANGUAGES_TEXTS,$values_lang_text,false);
                }


                if($lang_var_id > 0){
                    $data_txt = $db->SimpleOneQuery( "SELECT * FROM ".TBL_LANGUAGES_TEXTS." WHERE lng_var_id='".$lang_var_id."' AND lng_id='".$lang_id."'");
                    if(isset($data_txt->txt) && $data_txt->txt !='')
                    {
                      $text = stripslashes($data_txt->txt);
                    }

                }



            }
            return $text;

        }


	}

	

?>