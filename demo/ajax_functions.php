<?php

	error_reporting(E_ALL);
	require_once("include/includes.php");

    $ajax = "";
    $action = ((isset($_GET['action'])) && ($_GET['action'] != "")) ? $_GET['action'] : "";
	include_once(COMMON_CORE_MODEL . "ajax_functions.class.php");

	$oAjax = new ajax_functions();

	if($ajax == ""){
		switch($action){

			case 'load_brand_type_ofclient'; // List brand type selected by client

				if($loggedin_usertype !=''){     //if user is logged in
					$clientid = ((isset($_GET['id'])) && ($_GET['id'] != "")) ? $_GET['id'] : "";
					if(($clientid > 0)){
						$brand_types_ofclient = $oAjax->GetBrandTypeOfClient($clientid);
						if((is_array($brand_types_ofclient)) && (sizeof($brand_types_ofclient) > 0)){

							foreach($brand_types_ofclient as $key => $value){
								$ajax .= $value["id"] . "[:?]" . $value["name"] . ((sizeof($brand_types_ofclient) > 1) ? "|*|" : "");
								unset($brand_types_ofclient[$key]);
							}
						}
						else{
							$ajax = 'NOK';
						}

					}
				}
			break;

            case 'load_commissionplan_ofclient': // List commission plans selected by client
                if($loggedin_usertype !=''){     //if user is logged in
					$clientid = ((isset($_GET['id'])) && ($_GET['id'] != "")) ? $_GET['id'] : "";
					if(($clientid > 0)){
						$commPlansofClient = $oAjax->GetCommPlansOfClient($clientid);
						if((is_array($commPlansofClient)) && (sizeof($commPlansofClient) > 0)){

							foreach($commPlansofClient as $key => $value){
								$ajax .= $value["id"] . "[:?]" . $value["name"] . ((sizeof($commPlansofClient) > 1) ? "|*|" : "");
								unset($commPlansofClient[$key]);
							}
						}
						else{
							$ajax = 'NOK';
						}

					}
				}
			break;

		}

	}

	if($ajax == ''){
		echo "NOK";
	}
	else{
		$ajax = utf8_decode($ajax);
        $ajax = html_entity_decode($ajax);
		$ajax = utf8_encode($ajax);
		echo $ajax;
	}
	die;

?>