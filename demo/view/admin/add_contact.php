
<script>
 function add_validation(element,val)
 {

    if(document.getElementById(element).value !="")
    {
      document.getElementById(element).className=val;
    }
    else
    {
       document.getElementById(element).className="input";
    }
 }
</script>
<script type="text/javascript">
$(document).ready(function() {
	$("#add_cms").validationEngine();
});
</script>

<title>Contact Management</title>
<form name="add_cms" id="add_cms" method="post" enctype="multipart/form-data">


	<table width="100%" border="0" cellpadding="0" cellspacing="0" >
        <tr>
        <td>
        <table width="80%" cellpadding="0" cellspacing="0" border="0">
          <tr>
       <td colspan="2" style="">
       <h2 >Contact Management</h2>
       </td>
       </tr>
        </table>
        </td>
        </tr>
  		<tr>
    		<td valign="top">
				<table width="80%" class="tablebgcolor"  border="0" cellspacing="4" cellpadding="1" >

					<?php if(!empty($errorMsg)) {
						foreach($errorMsg as $k=>$d) { ?>
							<tr>
			 					<td width="100%" align="left" colspan="2" class="redlabel"  style="color:red;"><?php echo $d ?></td>
							</tr>
					<?php }} ?>
					<?php if(!empty($successMsg)) {
						foreach($successMsg as $k=>$d) { ?>
							<tr>
			 					<td width="100%" align="left" colspan="2" class="greenlabel"><?php echo $d ?></td>
							</tr>
					<?php }} ?>
					<tr>
					    <td ><span class="star">*&nbsp;</span>Name:</td>
					    <td>
							<input type="text" class="input validate[required]" value="<?php echo htmlspecialchars($cms[name]) ?>" id="name" name="name"></input>
				        </td>
				    </tr>
                    <tr>
                        <td>&nbsp;</td>
                        <td>
                         <ul id="countrytabs" class="shadetabs">
                                <?php foreach ($sitelanguages as $lng){?>
                                   <li><a href="#" rel="<?php echo 'country'.$lng['id'];?>" class="selected"><?php echo $lng['language_name'];?></a></li>
                                <?php } ?>
                                </ul>
                         </td>
                     </tr>
                	<tr>
                    <td colspan="2">
                        	<?php foreach ($sitelanguages as $lng){?>
					<div id="<?php echo 'country'.$lng['id'];?>" class="tabcontent">
                        <table width="100%">
                            <tr>
                               <td ><!--<span class="star">*&nbsp;</span>-->Details:</td>
                                <td> <textarea id="details[<?echo $lng[id];?>]" name="details[<?echo $lng[id];?>]" rows="5" cols="45" ><?php$data=str_replace('<br />',' ', $cms['details'][$lng['id']]); echo $data;?></textarea>

                                </td>
                            </tr>
                            <tr>
                               <td ><!--<span class="star">*&nbsp;</span>-->Street:</td>
                                <td><input type="text" class="input" value="<?php echo htmlspecialchars($cms['street'][$lng['id']]) ?>" id="street[<?echo $lng[id];?>]" name="street[<?echo $lng[id];?>]"></input>

                                </td>
                            </tr>
                            <tr>
                               <td ><!--<span class="star">*&nbsp;</span>-->City:</td>
                                <td><input type="text" class="input" value="<?php echo htmlspecialchars($cms['city'][$lng['id']]) ?>" id="city[<?echo $lng[id];?>]" name="city[<?echo $lng[id];?>]"></input>

                                </td>
                            </tr>
                            <tr>
                               <td ><!--<span class="star">*&nbsp;</span>-->Postal code::</td>
                                <td><input type="text" class="input" value="<?php echo htmlspecialchars($cms['postalcode'][$lng['id']]) ?>" id="postalcode[<?echo $lng[id];?>]" name="postalcode[<?echo $lng[id];?>]"></input>

                                </td>
                            </tr>

                        </table>
					</div>
					<?php } ?>
                    </td>

				    </tr>
                     <script type="text/javascript">
                                        var description=new ddtabcontent("countrytabs")
                            description.setpersist(true)
                            description.setselectedClassTarget("link") //"link" or "linkparent"
                            description.init()

                            </script>

                        <?phpif($_REQUEST[id]!="" && $cms['logo']!="" ) {
    ?>
                                    <tr>
                                        <td >
                                            Uploaded Image:
                                        </td>
                                        <td>
                                            <img src="<?php echo SITE_URL."uploads/logo/logo_resize/100-100-".$cms['logo']?>" >
                                        </td>

                                    </tr>
    <?php }?>
                                    <tr><td>Image:</td><td><span class="fieldbg">
                                                <input name="logo"   value="" type="file" id="logo" size="35"   /><br /><span><?php echo "Pls upload jpg,png or gif files only";?></span>
                                            </span></td></tr>
                    	<tr>
					    <td ><span class="star">*&nbsp;</span>Country:</td>
					    <td>
						   <select class="input" style="width: 137px;" id="country_id" name="country_id">
                           <option value="">Select Option</option>
                           <?php foreach($countries as $country){?>
                              <option value="<?= $country['country_id']; ?>" <?php if($cms[country_id]==$country['country_id']) { ?> selected="selected" <?php } ?>><?= $country['country_name']; ?></option>
                           <?php }?>
						   </select>
				        </td>
				    </tr>
                    <tr>
					    <td ><span class="star">*&nbsp;</span>Latitude:</td>
					    <td>
						  <input type="text" class="input validate[required,custom[number]]" value="<?php echo htmlspecialchars($cms[latitude]) ?>" id="latitude" name="latitude"></input>
				        </td>
				    </tr>
                    <tr>
					    <td ><span class="star">*&nbsp;</span>Longitude:</td>
					    <td>
						   <input type="text" class="input validate[required,custom[number]]" value="<?php echo htmlspecialchars($cms[longitude]) ?>" id="longitude" name="longitude"></input>
				        </td>
				    </tr>
                    <tr>
					    <td >Telephone:</td>
					    <td>
						  <input type="text" class="input" onblur="add_validation('phone','validate[custom[telephone]]');" value="<?php echo htmlspecialchars($cms[phone]) ?>" id="phone" name="phone"></input>
				        </td>
				    </tr>
                    <tr>
					    <td >Fax:</td>
					    <td>
						  <input type="text" class="input" onblur="add_validation('fax','validate[custom[fax]]');"  value="<?php echo htmlspecialchars($cms[fax]) ?>" id="fax" name="fax"></input>
				        </td>
				    </tr>
                    <tr>
					    <td>Email:</td>
					    <td>
						  <input type="text" class="input" onblur="add_validation('email','validate[custom[email]]');"  value="<?php echo htmlspecialchars($cms[email]) ?>" id="email" name="email"></input>
				        </td>
				    </tr>
                    <tr>
					    <td>Url:</td>
					    <td>
						  <input type="text" class="input" onblur="add_validation('url','validate[custom[url]]');"  value="<?php echo htmlspecialchars($cms[url]) ?>" id="url" name="url"></input>
				        </td>
				    </tr>
                    <tr>
					    <td ><span class="star">*&nbsp;</span>Description:</td>
					    <td>
                            <ul id="countrytabs1" class="shadetabs">
                            <?php foreach ($sitelanguages as $lng){?>
                               <li><a href="#" rel="<?php echo 'country1'.$lng['id'];?>" class="selected"><?php echo $lng['language_name'];?></a></li>
                            <?php } ?>
                            </ul>

                            <?php foreach ($sitelanguages as $lng){?>
                                  <div id="<?php echo 'country1'.$lng['id'];?>" class="tabcontent">
                                    <?php
    							  $oFCKeditor = new FCKeditor('over_view['.$lng[id]."]") ;
    								$oFCKeditor->BasePath = FCK_EDITOR_URL;
    								$oFCKeditor->Value = $cms['over_view'][$lng['id']];
    								$oFCKeditor->Create() ;
							        ?>
                                  <!-- <textarea id="over_view[<?echo $lng[id];?>]" name="over_view[<?echo $lng[id];?>]" rows="5" cols="45" ><?echo $cms['over_view'][$lng['id']];?></textarea>-->
                                  </div>
                            <?php } ?>

                            <script type="text/javascript">
                            var description1=new ddtabcontent("countrytabs1")
                            description1.setpersist(true)
                            description1.setselectedClassTarget("link") //"link" or "linkparent"
                            description1.init()
                            </script>

						</td>
				    </tr>
					<tr>
				   		<td class="boldlabel" valign="top"><span class="star">*&nbsp;</span>Status:</td>
						<td>
							<select class="input validate[required]" style="width: 137px;" id="status" name="status">
								<option value="">Select Option</option>
								<option value="155" <?php if(htmlspecialchars($cms[status])==155) { ?> selected="selected" <?php } ?>>Active</option>
								<option value="156" <?php if(htmlspecialchars($cms[status])==156) { ?> selected="selected" <?php } ?>>Inactive</option>
							</select>
						</td>
					</tr>
                    	<tr>
    		<td class="sbut" valign="top"><input name="CmsFormSubmit" id="CmsFormSubmit" type="submit"  class="subbut" value="<?= index_lang::$submit ?>"  />&nbsp;&nbsp;
    			<input type="button" value="<?= index_lang::$cancel ?>" class="subbut" onclick="javascript:cancelForm('?page=contact_list&amp;action=view')" name="btnCancel">
    		</td>
  		</tr>
  				</table>
			</td>
  		</tr>

	</table>
</form>