<title>Contact  Management</title>
<script type="text/javascript">
	$(document).ready(function() 
	{
 		$("#frmCMS").validationEngine();
	});
</script>
	<table width="100%" border="0" cellspacing="0" cellpadding="0" bgcolor="#FFFFFF" style="padding: 10px">
    <tr>
			<td class="admtitlebg">
				<table cellspacing="0" cellpadding="0" border="0" width="100%" >
		     		<tbody>
						<tr>
							<td>
				       			<b style="font-size:16px">Search</b>
							</td>
                            <td  align="right">
                            	&nbsp;
                            </td>
			     		</tr>
		       		</tbody>
				</table>
			</td>
	 	</tr>
        <tr>
         <td>
         &nbsp;&nbsp;&nbsp;
         </td>

        </tr>
        <tr>
	        <td>
				<form name="cms_search_form" method="post" action="?page=contact_list&action=view" id="cms_search_form">
	            	<table width="24%" class="" border="0" cellspacing="0" cellpadding="2px"  >
	              		<tr>
	                		<td>Search:</td>
	                		<td><input  style="width:150px;" name="search_cms" id="search_cms" type="text" class="input" value="<?php echo $search_cms ?>" /></td>
	                		<td class="sbut" valign="top">
	                 			<input name="cmsFormSubmit" type="hidden"  value="1" />
	                			<input name="submit" type="submit" class="subbut" value="Search"  />
							</td>
	              		</tr>
	              	</table>
				 </form>
	        </td>
	    </tr>
         <tr>
         <td>
         &nbsp;&nbsp;&nbsp;
         </td>

        </tr>
		<tr>
			<td class="admtitlebg">
				<table cellspacing="0" cellpadding="0" border="0" width="100%">
		     		<tbody>
						<tr>
							<td>
				       			<b style="font-size:16px">Contact Management</b>
							</td>
                            <td  align="right">
                            	<a class="link_title" href="./?page=add_contact&action=add">Add Contact</a>
                            </td>
			     		</tr>
		       		</tbody>
				</table>
			</td>
	 	</tr>

		<form name="frmCMS" id="frmCMS" action="" method="post">
			<input type="hidden" name="sort_order" id="sort_order" value="<?php echo $sort_order ?>"></input>
			<input type="hidden" name="sort_by" id="sort_by" value="<?php echo $sort_by ?>"></input>
		    <tr height="10px"><td></td></tr>
				<?php if(!empty($successMsg)) {
					foreach($successMsg as $k=>$d) { ?>
						<tr>
			 			<td width="100%" align="left"  colspan="2" class="greenlabel"><?php echo $d ?></td>
					</tr>
				<?php }} ?>
		        <tr>
		        	<td>
		            	<table width="100%" border="0" cellspacing="0" cellpadding="0">
		            	<tr>
		            		<td>
		                  		<table width="100%" border="0" cellspacing="0" cellpadding="0" class="gridarea">
		                  			<tr class="title">
		                    			<th colspan="2"></th>
		                    			<th colspan="3" style="text-align:right;padding-right:10px;">
											<?php if(!empty($cms)) { ?>
												<?=index_lang::$showing?> <?php echo $starting_record ?> <?=index_lang::$to?> <?php echo $ending_record ?> <?=index_lang::$of ?> <?php echo $total_records ?>
											<?php } ?>
										</th>
		                  			</tr>
		                  			<tr class="gridheaderbg">
						  				<th width="50px" align="left"><?=index_lang::$no?>.</th>
		                    			<th width="200px" align="left">
											<?echo"Name";?>
											<a href="javascript:sortSubmit('frmCMS','name');">
												<img border="0" src="<?php echo ADMIN_IMAGE_URL ?>tablehead_sort.gif"/>
											</a>
										</th>
                                        <th width="100px" align="left">
											<?echo"Country";?>
											<a href="javascript:sortSubmit('frmCMS','country_name');">
												<img border="0" src="<?php echo ADMIN_IMAGE_URL ?>tablehead_sort.gif"/>
											</a>
										</th>
										<th width="100px" align="left">
											<?=index_lang::$cms_status?>
											<a href="javascript:sortSubmit('frmCMS','status');">
												<img border="0" src="<?php echo ADMIN_IMAGE_URL ?>tablehead_sort.gif"/>
											</a>
										</th>
		                    			<th width="220px;" align="center"><?=index_lang::$action?></th>
		                  			</tr>
					  				<?php if(empty($cms)) { ?>
					   				<tr>
			              				<td valign="top" align="center" colspan="5">
			              					<span class="redlabel"><?=index_lang::$no_items?></span>
			              				</td>
		        	   				</tr>
					  				<?php } else {
                                        
					  					$cnt=1;
					  					foreach($cms as $k=>$d)
										{
											if($cnt%2==0)
											{
												$cls = "nrlbg";
											}
											else
											{
												$cls="altbg";
											}
						  					?>
							      			<tr class="<?php echo $cls ?>" >
								  				<td style="padding-left: 5px;"><?php echo $cnt++ ?></td>
							        			<td><?php echo $d['name']?></td>
							        			<td><?php echo $d['country_name']?></td>
												<td><?php echo $d['drop_down_option_value']?></td>
							         			<td align="center">
													<a href="./?page=add_contact&action=edit&id=<?php echo $d['contact_id'] ?>" title="<?=index_lang::$edit?>"><img src="<?php echo ADMIN_IMAGE_URL ?>edit.png"  alt="" border="0" /></a>&nbsp;&nbsp;
													<a onclick="return confirm('<?=index_lang::$suredelete?>')" href="./?page=contact_list&action=delete&id=<?php echo $d['contact_id'] ?>" title="<?=index_lang::$delete?>"><img src="<?php echo ADMIN_IMAGE_URL ?>delete.png" alt="" border="0" /></a>
												</td>
							      			</tr>
										<?php }
									} ?>
		                	</table>
					 	</td>
		            </tr>
					<?php if(!empty($cms)) { ?>
					   	<tr style="border-left: none;">
							<td align="right" class="manu" style="border-left: none;" ><?php echo $pagging ?></td>
					   	</tr>
				   <?php } ?>
		        </table>
		    </td>
		</tr>
	</form>
	</table>