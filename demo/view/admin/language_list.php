<title>Language  Management</title>
<script type="text/javascript">
	$(document).ready(function()
	{
 		$("#frmLanguage").validationEngine();
	});
</script>
	<table width="100%" border="0" cellspacing="0" cellpadding="0" bgcolor="#FFFFFF" style="padding: 10px">
    <tr>
			<td class="admtitlebg">
				<table cellspacing="0" cellpadding="0" border="0" width="100%">

						<tr>
							<td>
				       			<b style="font-size:16px">Search:</b>
							</td>

			     		</tr>

				</table>
			</td>
	 	</tr>
        <tr>
         <td >
         &nbsp;&nbsp;&nbsp;
         </td>

        </tr>
    <tr>
	        <td>
				<form id="search_language" name="search_language" method="post" action="">
         	<table width="30%" cellspacing="0" cellpadding="0" border="0" class="">
            <tr>
        	    <td style="padding-left: 10px;"><b>Language Name:</b></td>
        	    <td class="campo">
                    <input type="text" value="<?=isset($search_faq)?$search_faq:''?>" name="question_txt" id="question_txt" class="input validate[required]">
                </td>
                <td valign="top" class="campos2">
                    <input name="search_button" class="subbut" type="submit" value="Search">
        		</td>
            </tr>

         </table>
		</form>
	        </td>
	    </tr>
        <tr>
         <td >
         &nbsp;&nbsp;&nbsp;
         </td>

        </tr>
    <tr>
			<td class="admtitlebg">
				<table cellspacing="0" cellpadding="0" border="0" width="100%">
		     		<tbody>
						<tr>
							<td>
				       			<b style="font-size:16px"><?php echo index_lang::translate("Language List");?></b>
							</td>
                            <td  align="right">
                            	<a class="link_title" href="./?page=add_language&action=new"><?php echo index_lang::translate("Add Language"); ?></a>
                            </td>
			     		</tr>
		       		</tbody>
				</table>
			</td>
	 	</tr>
          </table>
		<form name="frmLanguage" id="frmLanguage" action="" method="post">
			<input type="hidden" name="sort_order" id="sort_order" value="<?php echo $sort_order ?>"></input>
			<input type="hidden" name="sort_by" id="sort_by" value="<?php echo $sort_by ?>"></input>
            </form>
		    <table width="100%" cellspacing="0" cellpadding="0" class="table"  bgcolor="#FFFFFF" style="padding: 10px">
       <form method="post" action="" id="frmLanguage" name="frmLanguage">
			<input type="hidden" value="<?=$sort_order?>" id="sort_order" name="sort_order">
			<input type="hidden" value="<?=$sort_by?>" id="sort_by" name="sort_by">
		</form>
		 <tr height="10px"><td></td></tr>
			<?php if(!empty($successMsg)) {
					foreach($successMsg as $k=>$d) { ?>
			<tr>
			<td width="100%" align="left"  colspan="2" class="greenlabel"><?php echo $d ?></td>
			</tr>
			<?php }} ?>
			<tr>
		        	<td>
		            	<table width="100%" cellspacing="0" cellpadding="0" border="0">
		            	<tr>
		            		<td>
								<!-- ADMIN PART ( FAQ lists) START -->
		                  		<table width="100%" cellspacing="0" cellpadding="0" border="0" class="gridarea">
                                <tr class="title">
		                    			<th colspan="2"></th>
		                    			<th style="text-align:right;padding-right:10px;" colspan="3">
											<?php if(!empty($objlanguage)) { ?>
												<?=index_lang::translate(index_lang::$showing);?> <?php echo $starting_record ?> <?=index_lang::translate(index_lang::$to)?> <?php echo $ending_record ?> <?=index_lang::$of ?> <?php echo $total_records ?>
											<?php } ?>										</th>
		                  			</tr>
                                    <tr class="gridheaderbg">
						  				<th width="100px" align="left"><?php echo index_lang::translate(index_lang::$no);?></th>
		                    			<th width="300px;" align="left" >
											<?php echo index_lang::translate("Language Name");?>
											<a href="javascript:sortSubmit('frmLanguage','language_name');">
												<img border="0" src="<?php echo ADMIN_IMAGE_URL ?>tablehead_sort.gif"/>
											</a>
										</th>
										<th width="300px;" align="left" >
											<?php echo index_lang::translate("Language Code");?>
											<a href="javascript:sortSubmit('frmLanguage','lng_code');">
												<img border="0" src="<?php echo ADMIN_IMAGE_URL ?>tablehead_sort.gif"/>
											</a>
										</th>
										<th width="200px;" align="left" >
											<?php echo index_lang::translate("Status");?>
											<a href="javascript:sortSubmit('frmLanguage','status');">
												<img border="0" src="<?php echo ADMIN_IMAGE_URL ?>tablehead_sort.gif"/>
											</a>
										</th>
										<th align="center"><?php echo index_lang::translate(index_lang::$action);?></th>
		                  			</tr>
									<?php if(empty($objlanguage)) { ?>
					   				<tr>
			              				<td valign="top" align="center" colspan="5">
			              					<span class="redlabel"><?=index_lang::$no_items?></span>
			              				</td>
		        	   				</tr>
					  				<?php } else {

					  					$cnt=1;
					  					foreach($objlanguage as $k=>$d)
										{
											if($cnt%2==0)
											{
												$cls = "nrlbg";
											}
											else
											{
												$cls="altbg";
											}
						  					?>
							      			<tr class="<?php echo $cls ?>" >
								  				<td style="padding-left: 5px;"><?php echo $cnt++ ?></td>
							        			<td style="padding-left: 5px;"><?php echo $d['language_name'] ?></td>
												<td style="padding-left: 5px;"><?php echo $d['lng_code'] ?></td>
												<td style="padding-left: 5px;"><?php if($d['status']==1) echo "Active"; else echo"Inactive"; ?></td>
												<td align="center">
													<a href="./?page=add_language&action=edit&id=<?php echo $d['id'] ?>" title="edit"><img src="<?php echo ADMIN_IMAGE_URL ?>edit.png"  alt="" border="0" /></a>&nbsp;&nbsp;
													<a onclick="return confirm('<?php echo index_lang::translate("Are you sure to delete this record");?>')" href="./?page=language_list&action=delete&id=<?php echo $d['id'] ?>" title="delete"><img src="<?php echo ADMIN_IMAGE_URL ?>delete.png" alt="" border="0" /></a>
												</td>
							      			</tr>
										<?php }
									} ?>

								</table>
								<!-- ADMIN PART ( STRATEGY lists) END -->
					 	</td>
		            </tr>
					<?php if(!empty($objlanguage)) { ?>
						<!-- PAGGING ( STRATEGY lists) START -->
					   	<tr style="border-left: none;">
							<td align="right" class="manu" style="border-left: none;" ><?php echo $pagging ?></td>
					   	</tr>
						<!-- PAGGING ( STRATEGY lists) END -->
				   <?php } ?>
				</table>
		    </td>
		</tr>
</table>