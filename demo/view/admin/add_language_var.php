<script type="text/javascript">
$(document).ready(function() {
	$("#add_language_var").validationEngine();
});
</script>
<title><?php echo index_lang::translate("Language Var Management");?></title>


<form name="add_language_var" id="add_language_var" method="post">
	 <table width="80%" cellspacing="0" cellpadding="0" border="0" >
      <tr>
     <td colspan="2">
    <h2 style="background-color: #DFDFDF;padding: 5px;"><?php echo index_lang::translate("Language Var Management");?></h2>
     </td>
     </tr>
     </table>
	 <table width="80%" cellspacing="4" cellpadding="2" border="0" class="tablebgcolor">

		<?php if(!empty($errorMsg)) {
		foreach($errorMsg as $k=>$d) { ?>
		<tr>
			<td width="100%" align="left" colspan="2" class="redlabel"><?php echo $d ?></td>
		</tr>
		<?php }} ?>
		<?php if(!empty($successMsg)) {
			foreach($successMsg as $k=>$d) { ?>
		<tr>
			<td width="100%" align="left" colspan="2" class="greenlabel"><?php echo $d ?></td>
		</tr>
		<?php }} ?>
  		<tr>
		    <td ><?php echo index_lang::translate("Language var Name");?>:</td>
		    <td class="campo">
                   		<input class="input validate[required]" id="name" name="name" value="<?=$obj_language_var['name']?>"/>
		  </td>
	    </tr>
		<tr><td height="5px;" colspan="2"></td></tr>
		<tr>
					    <td><?= "Language Text ".$lng['language_name'] ?>:</td>
					    <td>
                            <ul id="languagetabs" class="shadetabs">
                            <?php foreach ($sitelanguages as $lng){?>
                               <li><a href="#" rel="<?php echo 'name'.$lng['id'];?>" class="selected"><?php echo $lng['language_name'];?></a></li>
                            <?php } ?>
                            </ul>

                            <?php foreach ($sitelanguages as $lng){?>
                                  <div id="<?php echo 'name'.$lng['id'];?>" class="tabcontent">
                                    <?php
    								$oFCKeditor = new FCKeditor('txt['.$lng[id]."]") ;
    								$oFCKeditor->BasePath = FCK_EDITOR_URL;
    								$oFCKeditor->Value = $obj_language_var['txt'][$lng['id']];
    								$oFCKeditor->Create() ;
							        ?>
                                  </div>
                            <?php } ?>

                            <script type="text/javascript">

                            var description=new ddtabcontent("languagetabs")
                            description.setpersist(true)
                            description.setselectedClassTarget("link") //"link" or "linkparent"
                            description.init()

                            </script>

						</td>
			    </tr>
		<tr><td height="5px;" colspan="2"></td></tr>
		<tr>
			<td></td>
    		<td class="campos2" valign="top"><input name="LanguageVarFormSubmit" id="LanguageVarFormSubmit" type="submit"  class="subbut" value="<?php echo index_lang::translate("Submit");?>"  />&nbsp;&nbsp;
    		<input type="button" value="<?php echo index_lang::translate("Cancel");?>" class="subbut" onclick="javascript:cancelForm('?page=language_var_list&amp;action=view')" name="btnCancel">
    		</td>
  		</tr>
	</table>
</form>