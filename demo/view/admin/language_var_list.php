<title><?php echo index_lang::translate("Language Text Management");?></title>
<script type="text/javascript">
	$(document).ready(function()
	{
 		$("#search_language_var").validationEngine();
	});
</script>
	<table width="100%" border="0" cellspacing="0" cellpadding="0" bgcolor="#FFFFFF" style="padding: 10px" >
		<tr>
			<td class="admtitlebg">
				<table cellspacing="0" cellpadding="0" border="0" width="100%" >
		     		<tbody>
						<tr>
							<td>
				       			<b style="font-size:16px">Search:</b>
							</td>

			     		</tr>
		       		</tbody>
				</table>
			</td>
	 	</tr>
        <tr>
			<td>
              &nbsp;&nbsp;
			</td>
	 	</tr>
        <tr>
	        <td>
			   <form id="search_language_var" name="search_language_var" method="post" action="">
         	<table width="32%" cellspacing="4" cellpadding="2" border="0" >
            <tr>
        	    <td  align=""><?php echo index_lang::translate("Language Text Name");?>:</td>
        	    <td class="campo">
                    <input type="text" value="<?=isset($search_faq)?$search_faq:''?>" name="question_txt" id="question_txt" class="input validate[required]">
                </td>
                <td valign="top" class="campos2">
                    <input name="search_button" class="subbut" type="submit" value="Search">
        		</td>
            </tr>


         </table>
		</form>
	        </td>
	    </tr>
        <tr>
			<td>
              &nbsp;&nbsp;
			</td>
	 	</tr>
        <tr>
			<td class="admtitlebg">
				<table cellspacing="0" cellpadding="0" border="0" width="100%">
		     		<tbody>
						<tr>
							<td>
				       			<b style="font-size:16px"><?php echo index_lang::translate("Language Var List");?></b>
							</td>
                            <td  align="right">
                            	<a class="link_title" href="./?page=add_language_var&amp;action=new"><?php echo index_lang::translate("Add Language Txt"); ?></a>
                            </td>
			     		</tr>
		       		</tbody>
				</table>
			</td>
	 	</tr>

           <tr>
		    <table width="100%" cellspacing="0" cellpadding="0" class="table"  bgcolor="#FFFFFF">
       	<form method="post" action="" id="frmLanguageVar" name="frmLanguageVar">
			<input type="hidden" value="<?=$sort_order?>" id="sort_order" name="sort_order">
			<input type="hidden" value="<?=$sort_by?>" id="sort_by" name="sort_by">
		</form>
		 <tr height="10px"><td></td></tr>
			<?php if(!empty($successMsg)) {
					foreach($successMsg as $k=>$d) { ?>
			<tr>
			<td width="100%" align="left"  colspan="2" class="greenlabel"><?php echo $d ?></td>
			</tr>
			<?php }} ?>
			<tr>
		        	<td>
		            	<table width="100%" cellspacing="0" cellpadding="0" border="0" style="padding: 10px">
		            	<tr>
		            		<td>
								<!-- ADMIN PART ( FAQ lists) START -->
		                  		<table width="100%" cellspacing="0" cellpadding="0" border="0" class="gridarea" >
                                <tr class="title"  >
		                    			<th colspan="2"><?php //echo index_lang::translate("Language Vars");?></th>
		                    			<th style="text-align:right;padding-right:10px;" colspan="3">
											<?php if(!empty($obj_language_var)) { ?>
												<?=index_lang::translate(index_lang::$showing);?> <?php echo $starting_record ?> <?=index_lang::translate(index_lang::$to)?> <?php echo $ending_record ?> <?=index_lang::$of ?> <?php echo $total_records ?>
											<?php } ?>										</th>
		                  			</tr>
                                    <tr class="gridheaderbg">
						  				<th align="left"><?php echo index_lang::translate(index_lang::$no);?></th>
		                    			<th  align="left" width="50%" >
											<?php echo index_lang::translate("Language Var Name");?>
											<a href="javascript:sortSubmit('frmLanguageVar','name');">
												<img border="0" src="<?php echo ADMIN_IMAGE_URL ?>tablehead_sort.gif"/>
											</a>
										</th>
										<th align="center"><?php echo index_lang::translate(index_lang::$action);?></th>
		                  			</tr>
									<?php if(empty($obj_language_var)) { ?>
					   				<tr>
			              				<td valign="top" align="center" colspan="5">
			              					<span class="redlabel"><?=index_lang::$no_items?></span>
			              				</td>
		        	   				</tr>
					  				<?php } else {

					  					$cnt=1;
					  					foreach($obj_language_var as $k=>$d)
										{
											if($cnt%2==0)
											{
												$cls = "nrlbg";
											}
											else
											{
												$cls="altbg";
											}
						  					?>
							      			<tr class="<?php echo $cls ?>" >
								  				<td style="padding-left: 5px;"><?php echo $cnt++ ?></td>
							        			<td><?php echo $d['name'] ?></td>
												<td align="center">
													<a href="./?page=add_language_var&action=edit&id=<?php echo $d['id'] ?>" title="edit"><img src="<?php echo ADMIN_IMAGE_URL ?>edit.png"  alt="" border="0" /></a>&nbsp;&nbsp;
													<a onclick="return confirm('<?php echo index_lang::translate("Are you sure to delete this record");?>')" href="./?page=language_var_list&action=delete&id=<?php echo $d['id'] ?>" title="delete"><img src="<?php echo ADMIN_IMAGE_URL ?>delete.png" alt="" border="0" /></a>
												</td>
							      			</tr>
										<?php }
									} ?>

								</table>
								<!-- ADMIN PART ( STRATEGY lists) END -->
					 	</td>
		            </tr>
					<?php if(!empty($obj_language_var)) { ?>
						<!-- PAGGING ( STRATEGY lists) START -->
					   	<tr style="border-left: none;">
							<td align="right" class="manu" style="border-left: none;" ><?php echo $pagging ?></td>
					   	</tr>
						<!-- PAGGING ( STRATEGY lists) END -->
				   <?php } ?>
				</table>
		    </td>
		</tr>
</table> </tr></table>