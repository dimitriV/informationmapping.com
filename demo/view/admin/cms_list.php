<title>CMS Management</title>
<script type="text/javascript">
	$(document).ready(function() 
	{
 		$("#frmCMS").validationEngine();
	});
</script>
	<table width="100%" border="0" cellspacing="0" cellpadding="0">
		<tr>
			<td class="admtitlebg">
				<table cellspacing="0" cellpadding="0" border="0" width="100%">
		     		<tbody>
						<tr>
							<td>
				       			<h1><?=index_lang::$cms_management?></h1>
					   			<ul>
				            		<li><a class="link_title" href="./?page=add_cms&action=add"><?=index_lang::$cms_add?></a></li>
			        			</ul>
							</td>
			     		</tr>
		       		</tbody>
				</table>
			</td>
	 	</tr>
		<tr>
	        <td>	
				<form name="cms_search_form" method="post" action="?page=cms_list&action=view" id="cms_search_form">
	            	<table width="35%" class="tablebgcolor" border="0" cellspacing="0" cellpadding="2px">
	              		<tr>
	                		<td><?=index_lang::$cms_lblSearch?>:</td>
	                		<td><input  style="width:200px;" name="search_cms" id="search_cms" type="text" class="input" value="<?php echo $search_cms ?>" /></td>
	                		<td class="sbut" valign="top">
	                 			<input name="cmsFormSubmit" type="hidden"  value="1" />
	                			<input name="submit" type="submit" class="subbut" value="<?=index_lang::$cms_btnSearch?>"  />
							</td>
	              		</tr>
	              	</table>
				 </form> 
	        </td>
	    </tr>
		<form name="frmCMS" id="frmCMS" action="" method="post">
			<input type="hidden" name="sort_order" id="sort_order" value="<?php echo $sort_order ?>"></input>
			<input type="hidden" name="sort_by" id="sort_by" value="<?php echo $sort_by ?>"></input>
		    <tr height="10px"><td></td></tr>
				<?php if(!empty($successMsg)) {
					foreach($successMsg as $k=>$d) { ?>
						<tr>
			 			<td width="100%" align="left"  colspan="2" class="greenlabel"><?php echo $d ?></td>
					</tr>
				<?php }} ?>
		        <tr>
		        	<td>
		            	<table width="100%" border="0" cellspacing="0" cellpadding="0">
		            	<tr>
		            		<td>
		                  		<table width="100%" border="0" cellspacing="0" cellpadding="0" class="gridarea">
		                  			<tr class="title">
		                    			<th colspan="2"><?=index_lang::$cmslist_header?></th>
		                    			<th colspan="2" style="text-align:right;padding-right:10px;">
											<?php if(!empty($cms)) { ?>
												<?=index_lang::$showing?> <?php echo $starting_record ?> <?=index_lang::$to?> <?php echo $ending_record ?> <?=index_lang::$of ?> <?php echo $total_records ?>
											<?php } ?>
										</th>
		                  			</tr>
		                  			<tr>
						  				<th><?=index_lang::$no?>.</th>
		                    			<th>
											<?=index_lang::$cms_pagename?>
											<a href="javascript:sortSubmit('frmCMS','page_name');">
												<img border="0" src="<?php echo ADMIN_IMAGE_URL ?>tablehead_sort.gif"/>
											</a>
										</th>
										<th>
											<?=index_lang::$cms_status?>
											<a href="javascript:sortSubmit('frmCMS','status');">
												<img border="0" src="<?php echo ADMIN_IMAGE_URL ?>tablehead_sort.gif"/>
											</a>
										</th>
		                    			<th width="220px;" align="center"><?=index_lang::$action?></th>
		                  			</tr>
					  				<?php if(empty($cms)) { ?>
					   				<tr>
			              				<td valign="top" align="center" colspan="5">
			              					<span class="redlabel"><?=index_lang::$no_items?></span>
			              				</td>
		        	   				</tr>
					  				<?php } else {
                                        
					  					$cnt=1;
					  					foreach($cms as $k=>$d)
										{
											if($cnt%2==0)
											{
												$cls = "nrlbg";
											}
											else
											{
												$cls="altbg";
											}
						  					?>
							      			<tr class="<?php echo $cls ?>" >
								  				<td><?php echo $cnt++ ?></td>
							        			<td><?php echo $d['page_name'] ?> <span class="bluelabel"><?php echo $d['slug_title'] ?></span></td>
												<td><?php echo $d['drop_down_option_value'] ?></td>
							         			<td align="center">
													<a href="./?page=add_cms&action=edit&id=<?php echo $d['cms_id'] ?>" title="<?=index_lang::$edit?>"><img src="<?php echo ADMIN_IMAGE_URL ?>edit.png"  alt="" border="0" /></a>&nbsp;&nbsp;
													<a onclick="return confirm('<?=index_lang::$suredelete?>')" href="./?page=cms_list&action=delete&id=<?php echo $d['cms_id'] ?>" title="<?=index_lang::$delete?>"><img src="<?php echo ADMIN_IMAGE_URL ?>delete.png" alt="" border="0" /></a>
												</td>
							      			</tr>
										<?php }
									} ?>
		                	</table>
					 	</td>
		            </tr>
					<?php if(!empty($cms)) { ?>
					   	<tr style="border-left: none;">
							<td align="right" class="manu" style="border-left: none;" ><?php echo $pagging ?></td>
					   	</tr>
				   <?php } ?>
		        </table>
		    </td>
		</tr>
	</form>
	</table>