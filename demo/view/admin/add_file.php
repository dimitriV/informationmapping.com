<script type="text/javascript">
$(document).ready(function() {
	$("#add_cms").validationEngine();
});
</script>
<title><?=index_lang::$cms_title?></title>
<h2 style="background-color: #DFDFDF;padding: 5px;">Update File</h2>

<form name="add_cms" id="add_cms" method="post" enctype="multipart/form-data">
	<table width="100%" border="0" cellpadding="0" cellspacing="0">
  		<tr>
    		<td valign="top">
				<table width="80%" class="tablebgcolor" border="0" cellspacing="4" cellpadding="1">
					<?php if(!empty($errorMsg)) {
						foreach($errorMsg as $k=>$d) { ?>
							<tr>
			 					<td width="100%" align="left" colspan="2" class="redlabel"><?php echo $d ?></td>
							</tr>
					<?php }} ?>
					<?php if(!empty($successMsg)) {
						foreach($successMsg as $k=>$d) { ?>
							<tr>
			 					<td width="100%" align="left" colspan="2" class="greenlabel"><?php echo $d ?></td>
							</tr>
					<?php }} ?>
					<tr>
					    <td ><?= index_lang::$cms_pagename ?>:</td>
					    <td>
							<input type="text" class="input validate[required]" value="<?php echo htmlspecialchars($cms[page_name]) ?>" id="page_name" name="page_name"></input>
				        </td>
				    </tr>

					<tr>
					    <td >File:</td>
					    <td>
                            <ul id="countrytabs" class="shadetabs">
                            <?php foreach ($sitelanguages as $lng){?>
                               <li><a href="#" rel="<?php echo 'country'.$lng['id'];?>" class="selected"><?php echo $lng['language_name'];?></a></li>
                            <?php } ?>
                            </ul>

                            <?php foreach ($sitelanguages as $lng){?>
                                  <div id="<?php echo 'country'.$lng['id'];?>" class="tabcontent">
                                  <input type="file" name="file<?=$lng[id];?>" id="file<?=$lng[id];?>" />
                                   <?php if($_REQUEST['id']!=""){?>
                                   <a href="<?echo SITE_URL."uploads/file/".$cms[files][$lng[id]]?>" target="_blank"><?echo $cms[files][$lng[id]];?></a>
                                   <?php }?>
                                  </div>
                            <?php } ?>

                            <script type="text/javascript">

                            var description=new ddtabcontent("countrytabs")
                            description.setpersist(true)
                            description.setselectedClassTarget("link") //"link" or "linkparent"
                            description.init()

                            </script>

						</td>
				    </tr>

                     	<tr>
					    <td >MappedFile:</td>
					    <td>
                            <ul id="countrytabs1" class="shadetabs">
                            <?php foreach ($sitelanguages as $lng){?>
                               <li><a href="#" rel="<?php echo 'country2'.$lng['id'];?>" class="selected"><?php echo $lng['language_name'];?></a></li>
                            <?php } ?>
                            </ul>

                            <?php foreach ($sitelanguages as $lng){?>
                                  <div id="<?php echo 'country2'.$lng['id'];?>" class="tabcontent">
                                  <input type="file" name="filemap<?=$lng[id];?>" id="filemap<?=$lng[id];?>" />
                                   <a href="<?echo SITE_URL."uploads/mapfile/".$cms[mapfiles][$lng[id]]?>" target="_blank"><?echo $cms[mapfiles][$lng[id]];?></a>
                                  </div>
                            <?php } ?>

                            <script type="text/javascript">

                            var description=new ddtabcontent("countrytabs1")
                            description.setpersist(true)
                            description.setselectedClassTarget("link") //"link" or "linkparent"
                            description.init()

                            </script>

						</td>
				    </tr>


					<tr>
					    <td class="boldlabel">Type:</td>
					    <td>
                         	<select class="input" disabled="disabled" style="width: 310px;" id="type" name="type">
								<option value="">Select Option</option>
								<option value="1" <?php if(htmlspecialchars($cms[type])==1) { ?> selected="selected" <?php } ?>>Bussiness </option>
								<option value="2" <?php if(htmlspecialchars($cms[type])==2) { ?> selected="selected" <?php } ?>>Web </option>
								<option value="3" <?php if(htmlspecialchars($cms[type])==3) { ?> selected="selected" <?php } ?>>Email </option>
							</select>
                        </td>
				    </tr>
					<tr>
				   		<td class="boldlabel" valign="top"><?= index_lang::$cms_status ?>:</td>
						<td>
							<select class="input" style="width: 310px;" id="status" name="status">
								<option value="">Select Option</option>
								<option value="155" <?php if(htmlspecialchars($cms[status])==155) { ?> selected="selected" <?php } ?>>Active</option>
								<option value="156" <?php if(htmlspecialchars($cms[status])==156) { ?> selected="selected" <?php } ?>>Inactive</option>
							</select>
						</td>
					</tr>
  				</table>
			</td>
  		</tr>
  		<tr>
    		<td class="sbut" valign="top"><input name="CmsFormSubmit" id="CmsFormSubmit" type="submit"  class="inputbutton" value="<?= index_lang::$submit ?>"  />&nbsp;&nbsp;
    			<input type="button" value="<?= index_lang::$cancel ?>" class="inputbutton" onclick="javascript:cancelForm('?page=file_list&amp;action=view')" name="btnCancel">
    		</td>
  		</tr>
	</table>
</form>