<script type="text/javascript">
$(document).ready(function() {
	$("#add_cms").validationEngine();
});
</script>
<title><?=index_lang::$cms_title?></title>
<h1><?=index_lang::$cms_header?></h1>

<form name="add_cms" id="add_cms" method="post" enctype="multipart/form-data">
	<table width="100%" border="0" cellpadding="0" cellspacing="0">
  		<tr>
    		<td valign="top">
				<table width="80%" class="tablebgcolor" border="0" cellspacing="4" cellpadding="1">
					<?php if(!empty($errorMsg)) {
						foreach($errorMsg as $k=>$d) { ?>
							<tr>
			 					<td width="100%" align="left" colspan="2" class="redlabel"><?php echo $d ?></td>
							</tr>
					<?php }} ?>
					<?php if(!empty($successMsg)) {
						foreach($successMsg as $k=>$d) { ?>
							<tr>
			 					<td width="100%" align="left" colspan="2" class="greenlabel"><?php echo $d ?></td>
							</tr>
					<?php }} ?>
					<tr>
					    <td class="redlabel"><?= index_lang::$cms_pagename ?>:</td>
					    <td>
							<input type="text" class="input validate[required]" value="<?php echo htmlspecialchars($cms[page_name]) ?>" id="page_name" name="page_name"></input>
				        </td>
				    </tr>

					<tr>
					    <td class="redlabel">File:</td>
					    <td>
                            <ul id="countrytabs" class="shadetabs">
                            <?php foreach ($sitelanguages as $lng){?>
                               <li><a href="#" rel="<?php echo 'country'.$lng['id'];?>" class="selected"><?php echo $lng['language_name'];?></a></li>
                            <?php } ?>
                            </ul>

                            <?php foreach ($sitelanguages as $lng){?>
                                  <div id="<?php echo 'country'.$lng['id'];?>" class="tabcontent">
                                  <input type="file" name="file<?=$lng[id];?>" id="file<?=$lng[id];?>" />


                                  </div>
                            <?php } ?>

                            <script type="text/javascript">

                            var description=new ddtabcontent("countrytabs")
                            description.setpersist(true)
                            description.setselectedClassTarget("link") //"link" or "linkparent"
                            description.init()

                            </script>

						</td>
				    </tr>

                     	<tr>
					    <td class="redlabel">MappedFile:</td>
					    <td>
                            <ul id="countrytabs1" class="shadetabs">
                            <?php foreach ($sitelanguages as $lng){?>
                               <li><a href="#" rel="<?php echo 'country2'.$lng['id'];?>" class="selected"><?php echo $lng['language_name'];?></a></li>
                            <?php } ?>
                            </ul>

                            <?php foreach ($sitelanguages as $lng){?>
                                  <div id="<?php echo 'country2'.$lng['id'];?>" class="tabcontent">
                                  <input type="file" name="filemap<?=$lng[id];?>" id="filemap<?=$lng[id];?>" />

                                  </div>
                            <?php } ?>

                            <script type="text/javascript">

                            var description=new ddtabcontent("countrytabs1")
                            description.setpersist(true)
                            description.setselectedClassTarget("link") //"link" or "linkparent"
                            description.init()

                            </script>

						</td>
				    </tr>

					<tr>
					    <td class="boldlabel"><?= index_lang::$cms_metadesc ?>:</td>
					    <td>
							<input id="meta_description" name="meta_description" class="validate[custom[email]]" value="<?php echo htmlspecialchars($cms[meta_description]) ?>"></input>
							<!--<textarea id="meta_description" name="meta_description" rows="6" cols="50"><?php echo $cms[meta_description] ?></textarea>-->
						</td>
				    </tr>
					<tr>
					    <td class="boldlabel"><?= index_lang::$cms_metakey ?>:</td>
					    <td>
							<input id="meta_keywords" name="meta_keywords" class="validate[custom[number]]" value="<?php echo htmlspecialchars($cms[meta_keywords]) ?>"></input>
							<!--<textarea id="meta_keywords" name="meta_keywords" rows="6" class="validate[custom[number]]" cols="50"><?php echo $cms[meta_keywords] ?></textarea>-->
						</td>
				    </tr>
					<tr>
				   		<td class="boldlabel" valign="top"><?= index_lang::$cms_status ?>:</td>
						<td>
							<select class="input" style="width: 310px;" id="status" name="status">
								<option value="">Select Option</option>
								<option value="155" <?php if(htmlspecialchars($cms[status])==155) { ?> selected="selected" <?php } ?>>Active</option>
								<option value="156" <?php if(htmlspecialchars($cms[status])==156) { ?> selected="selected" <?php } ?>>Inactive</option>
							</select>
						</td>
					</tr>
  				</table>
			</td>
  		</tr>
  		<tr>
    		<td class="sbut" valign="top"><input name="CmsFormSubmit" id="CmsFormSubmit" type="submit"  class="subbut" value="<?= index_lang::$submit ?>"  />&nbsp;&nbsp;
    			<input type="button" value="<?= index_lang::$cancel ?>" class="subbut" onclick="javascript:cancelForm('?page=cms_list&amp;action=view')" name="btnCancel">
    		</td>
  		</tr>
	</table>
</form>