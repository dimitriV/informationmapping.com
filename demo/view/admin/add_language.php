<script type="text/javascript">
$(document).ready(function() {
	$("#add_language").validationEngine();
});
function setDefault(v){
	if(v ==true)
		document.getElementById('defaultlanguage').value = '1';
	else
		document.getElementById('defaultlanguage').value = '0';
}
</script>
<title>Contact Management</title>
<h2 style="background-color: #DFDFDF;padding: 5px;"><?php echo index_lang::translate("Language Management");?></h2>

<form name="add_language" id="add_language" method="post" enctype="multipart/form-data">
   	<input id="defaultlanguage" type="hidden" name="defaultlanguage" value="<?php if($objlanguage['set_default'] == 1) echo "1"; else echo "0";?>"/>
	 <table width="100%" cellspacing="4" cellpadding="2" border="0" class="tablebgcolor">
		<?php if(!empty($errorMsg)) {
		foreach($errorMsg as $k=>$d) { ?>
		<tr>
			<td width="100%" align="left" colspan="2" class="redlabel"><?php echo $d ?></td>
		</tr>
		<?php }} ?>
		<?php if(!empty($successMsg)) {
			foreach($successMsg as $k=>$d) { ?>
		<tr>
			<td width="100%" align="left" colspan="2" class="greenlabel"><?php echo $d ?></td>
		</tr>
		<?php }} ?>
  		<tr>
		    <td width="30%"><?php echo index_lang::translate("Language Name");?>:</td>
		    <td class="campo">
                   		<input class="input validate[required]" id="language_name" name="language_name" value="<?=$objlanguage['language_name']?>"/>
		  </td>
	    </tr>
		<tr><td height="5px;" colspan="2"></td></tr>
		<tr>
		    <td width="30%"><?php echo index_lang::translate("Language Code");?>:</td>
		    <td class="campo">
                   		<input class="input validate[required]" id="lng_code" name="lng_code" value="<?=$objlanguage['lng_code']?>"/>
		  </td>
	    </tr>
		<tr><td height="5px;" colspan="2"></td></tr>
		<tr>
		    <td width="30%"><?php echo index_lang::translate("Flag Image");?>:</td>
		    <td>
                   		<input type="file" class="input validate[required]" id="flag_image" name="flag_image" value=""/>&nbsp;&nbsp;<?php if($objlanguage['flag_image'] !='' && file_exists($flaglogodest.$objlanguage['flag_image'])){ ?>
                                                <img src="./image_resize.php?width=18&height=25&image=<?php echo UPLOAD_URL."languages/".$objlanguage['flag_image'];?>&nocache=1" />
                                            <?php } ?>
		  </td>
	    </tr>
		<tr><td height="5px;" colspan="2"></td></tr>
		<tr>
		    <td><?php echo index_lang::translate("Status");?>:</td>
		    <td>
                     	<table>
				<tr>
					<td class="campo">
						<select id="status" name="status" class="input validate[required] styled">
							<option value=""><?php echo index_lang::translate("Select");?></option>
							<option value="1" <?php if(htmlspecialchars($objlanguage[status])==1) { ?> selected="selected" <?php } ?>><?php echo index_lang::translate("Active");?></option>
							<option value="2" <?php if(htmlspecialchars($objlanguage[status])==2) { ?> selected="selected" <?php } ?>><?php echo index_lang::translate("Inactive");?></option>
						</select>
					</td>
				</tr>
				</table>
			</td>
	    </tr>
        <tr><td height="5px;" colspan="2"></td></tr>
		<tr>
		    <td width="30%"><?php echo index_lang::translate("Set Default");?>:</td>
		    <td>
            	<input type="checkbox" onclick="setDefault(this.checked)" id="set_default" <?php if($objlanguage['set_default'] == 1){?>checked="checked" disabled="disabled"<?php }?> name="set_default" value="1"/>
		  	</td>
	    </tr>
		<tr><td height="5px;" colspan="2"></td></tr>
		<tr>
			<td></td>
    		<td class="campos2" valign="top"><input name="LanguageFormSubmit" id="LanguageFormSubmit" type="submit"  class="subbut" value="<?php echo index_lang::translate("Submit");?>"  />&nbsp;&nbsp;
    		<input type="button" value="<?php echo index_lang::translate("Cancel");?>" class="subbut" onclick="javascript:cancelForm('?page=language_list&amp;action=view')" name="btnCancel">
    		</td>
  		</tr>
	</table>
</form>