
function generateProjectCombo(companyID,destinationID,comboID,selectedID)
{	
    $.ajax({
        type: "POST",
        url: "index.php?page=generate_project_combo&action=view",
        data: "companyID="+companyID+"&comboID="+comboID+"&selectedID="+selectedID,
        success: function(response){			
            $("#"+destinationID).html(response);            
        }
    });
}
function getStates(cid)
{
    $.ajax({
        type: "POST",
        url: "index.php?page=get_states&action=view",
        data: "country="+cid,
        success: function(msg){
            $("#div_states").html(msg);
            $("#div_city").html('<select name="city_id" id="city_id" style="width:200px;" class="input"><option value="">Select City</option>         </select>');
        }
    });
}

function getCities(sid)
{
    $.ajax({
        type: "POST",
        url: "index.php?page=get_cities&action=view",
        data: "state="+sid,
        success: function(msg){
            $("#div_city").html(msg);
        }
    });
}

function newWindow(mypage, myname, w, h, scroll) {
    var winl = (screen.width - w) / 2;
    var wint = (screen.height - h) / 2;

    winprops = 'height='+h+',width='+w+',top='+wint+',left='+winl+',scrollbars='+scroll+',resizable'
    win = window.open(mypage, myname, winprops)
    if (parseInt(navigator.appVersion) >= 4) { win.window.focus(); }
}

function submitPage(pageNo)
{
	theForms = document.getElementsByTagName("form");
	formName = theForms[0].name;

    if(formName == "frm_post"){ formName = theForms[1].name;}

	queryString = window.location.search.substring(1);
	qArray = queryString.split("&");
	total = qArray.length;
	var qString = "";
	var pageNoCame = false;
	i = 0;
	
	while(i < total)
	{		
		key = qArray[i].split("=");	
		
		if (key[0] == 'pageno')
		{
			if (i == (total-1))
				qString = qString + key[0] + "=" + pageNo;
			else
				qString = qString + key[0] + "=" + pageNo + "&";
				
			pageNoCame = true;
			i++;
		}
		else
		{
			if (i == (total-1))
				qString = qString + qArray[i];
			else
				qString = qString + qArray[i] + "&";
				
			i++;
		}			
	}
		if (pageNoCame == false)
			qString = qString + "&pageno=" + pageNo;
	
	
	//qString = qString + extraVARs;	
	eval("document."+formName+".action='?"+qString+"'");	
	eval("document."+formName+".submit();");
	
}

function check_uncheck_All(controlName1,controlName2)
{		
	chks = eval("document.getElementsByName('"+controlName2+"')");		
	i = 0;
	total_chks = chks.length;	
	while (i < total_chks)
	{		
		if (eval("document.getElementById('"+controlName1+"')").checked == true)
			chks[i].checked = true;
		else
			chks[i].checked = false;
		i++;
	}
}

function sortSubmit(formName,sortBy)
{
	if (document.getElementById("sort_order").value == 'DESC')
		document.getElementById("sort_order").value = 'ASC';
	else if (document.getElementById("sort_order").value == 'ASC')
		document.getElementById("sort_order").value = 'DESC';

	queryString = window.location.search.substring(1)
	document.getElementById("sort_by").value = sortBy;
	eval("document."+formName+".action = '?"+ queryString+"'");
	eval("document."+formName+".submit()");
}

function cancelForm(url)
{
    window.location = url;
}
