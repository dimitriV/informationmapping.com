<script>
function getAddress(val)
{
    var country=$("#country").val();
   // alert(country);
    if(country=='other')
    {
      //alert('here');
       window.location="./?page=partner&country_id=other";
    }
    else
    {
       $.ajax({
            type: "POST",
            url: "./?page=get_address&country="+val,
            data: "country="+val,
            success: function(msg){
                   if(msg=='redirect')
                   {
                       window.location="./?page=partner"
                   }
                   else
                   {
                      $("#change_content").html(msg);
                   }
            }
        });
    }
}
</script>

<script src="http://maps.google.com/maps?file=api&amp;v=2&amp;key=AIzaSyBMi07bDJDKdLr-ilpJ7co-ZJmdcNjD1rY&sensor=false"
            type="text/javascript"></script>

    <script type="text/javascript">
    //<![CDATA[

   /* var iconBlue = new GIcon();
    iconBlue.image = 'http://labs.google.com/ridefinder/images/mm_20_blue.png';
    iconBlue.shadow = 'http://labs.google.com/ridefinder/images/mm_20_shadow.png';
    iconBlue.iconSize = new GSize(12, 20);
    iconBlue.shadowSize = new GSize(22, 20);
    iconBlue.iconAnchor = new GPoint(6, 20);
    iconBlue.infoWindowAnchor = new GPoint(5, 1);

    var iconRed = new GIcon();
    iconRed.image = 'http://labs.google.com/ridefinder/images/mm_20_red.png';
    iconRed.shadow = 'http://labs.google.com/ridefinder/images/mm_20_shadow.png';
    iconRed.iconSize = new GSize(12, 20);
    iconRed.shadowSize = new GSize(22, 20);
    iconRed.iconAnchor = new GPoint(6, 20);
    iconRed.infoWindowAnchor = new GPoint(5, 1);

    var customIcons = [];
    customIcons["restaurant"] = iconBlue;
    customIcons["bar"] = iconRed;*/

    function load() {
      if (GBrowserIsCompatible()) {
        var map = new GMap2(document.getElementById("map"));
        map.addControl(new GSmallMapControl());
        map.addControl(new GMapTypeControl());
        map.setCenter(new GLatLng(47.614495, -122.341861), 2);
        //map.setCenter(new GLatLng(37.4419, -122.1419), 2);

        GDownloadUrl("map.php", function(data) {
          var xml = GXml.parse(data);
          var markers = xml.documentElement.getElementsByTagName("marker");
          for (var i = 0; i < markers.length; i++) {
            var name = markers[i].getAttribute("name");
            var address = markers[i].getAttribute("address");
            var phone = markers[i].getAttribute("phone");
            var street = markers[i].getAttribute("street");
            var city = markers[i].getAttribute("city");
            var url = markers[i].getAttribute("url");
            var image = markers[i].getAttribute("image");
            //var type = markers[i].getAttribute("type");
            var point = new GLatLng(parseFloat(markers[i].getAttribute("lat")),
                                    parseFloat(markers[i].getAttribute("lng")));
            var marker = createMarker(point, name, address, phone, street, city, url, image);
            map.addOverlay(marker);
          }
        });
      }
    }

    function createMarker(point, name, address,phone,street,city,url,image) {
      //var greenIcon = new GIcon(G_DEFAULT_ICON);
      //greenIcon.image = "http://www.google.com/intl/en_us/mapfiles/ms/micons/red-pushpin.png";
      //var markerOptions = { icon:greenIcon };

      //var marker = new GMarker(point, markerOptions);
      var marker = new GMarker(point, G_DEFAULT_ICON);
      if(image != null)
      {
      var html = "<b>" + name + "</b> <br/><div><div style='float:left;width:125px;'>"+ address+ "<br/>"+ street+ "<br/>"+ city+ "<br/>"+ phone + "<br/><br/><a target='_blank' style='right:12px;bottom:5px;position:relative' href='"+ url+"'>"+url+"</a></div><div style='float:left'><img width='70' height='40' src='"+image+"'/></div></div>";
      }
      else
      {
        var html = "<b>" + name + "</b> <br/><div><div style='float:left;width:125px;'>"+ address+ "<br/>"+ street+ "<br/>"+ city+ "<br/>"+ phone + "<br/><br/><a target='_blank' style='bottom:10px;position:relative' href='"+ url+"'>"+url+"</a></div><div style='float:left'></div></div>";
      }
      GEvent.addListener(marker, 'click', function() {
        marker.openInfoWindowHtml(html);
      });
      return marker;
    }

    function removeHTMLTags(strInputCode){

 		//var strInputCode = document.getElementById("input-code").innerHTML;
 		/*
  			This line is optional, it replaces escaped brackets with real ones,
  			i.e. < is replaced with < and > is replaced with >
 		*/
 	 	strInputCode = strInputCode.replace(/&(lt|gt);/g, function (strMatch, p1){
 		 	return (p1 == "lt")? "<" : ">";
 		});
 		var strTagStrippedText = strInputCode.replace(/<\/?[^>]+(>|$)/g, "");
 		return strTagStrippedText;
   // Use the alert below if you want to show the input and the output text
   //		alert("Input code:\n" + strInputCode + "\n\nOutput text:\n" + strTagStrippedText);

}
    //]]>
  </script>
    <div class="innerpage-containt">
    	<div class="mapping-containt">
         <h2 style="padding-bottom: 25px;"><?php echo index_lang::translate("Questions, concerns, suggestions? Feel free to contact us!");?></h2>
            <div id="map" class="map-colum" style="width: 1000px; height: 519px"></div>
         <div class="clear"></div>
        </div>
    </div>
    <div class="clear"></div>
