<script>
function getAddress(val)
{
    var country=$("#country_id").val();
        $.ajax({
            type: "POST",
            url: "./?page=get_address&country="+val,
            data: "country="+val,
            success: function(msg){
                   if(msg=='redirect')
                   {
                       window.location="./?page=contact"
                   }
                   else
                   {
                      $("#change_content").html(msg);
                   }

            }
        });
}
</script>
<script src="http://maps.google.com/maps?file=api&v=2&key=ABQIAAAAjRzpmGLVtLX7jMbLiiQSNxRZt45Vxgyi8OXrslQcf5YJ37BnKRSQRLLJC4Om2WtHFYtTHGRkNEVEYg"
            type="text/javascript"></script>

    <script type="text/javascript">
    //<![CDATA[

   /* var iconBlue = new GIcon();
    iconBlue.image = 'http://labs.google.com/ridefinder/images/mm_20_blue.png';
    iconBlue.shadow = 'http://labs.google.com/ridefinder/images/mm_20_shadow.png';
    iconBlue.iconSize = new GSize(12, 20);
    iconBlue.shadowSize = new GSize(22, 20);
    iconBlue.iconAnchor = new GPoint(6, 20);
    iconBlue.infoWindowAnchor = new GPoint(5, 1);

    var iconRed = new GIcon();
    iconRed.image = 'http://labs.google.com/ridefinder/images/mm_20_red.png';
    iconRed.shadow = 'http://labs.google.com/ridefinder/images/mm_20_shadow.png';
    iconRed.iconSize = new GSize(12, 20);
    iconRed.shadowSize = new GSize(22, 20);
    iconRed.iconAnchor = new GPoint(6, 20);
    iconRed.infoWindowAnchor = new GPoint(5, 1);

    var customIcons = [];
    customIcons["restaurant"] = iconBlue;
    customIcons["bar"] = iconRed;*/

    function load() {
      if (GBrowserIsCompatible()) {
        var map = new GMap2(document.getElementById("map"));
        map.addControl(new GSmallMapControl());
        map.addControl(new GMapTypeControl());
        map.setCenter(new GLatLng(47.614495, -122.341861), 1);

        GDownloadUrl("map.php", function(data) {
          var xml = GXml.parse(data);
          var markers = xml.documentElement.getElementsByTagName("marker");
          for (var i = 0; i < markers.length; i++) {
            var name = markers[i].getAttribute("name");
            var address = markers[i].getAttribute("address");
            //var type = markers[i].getAttribute("type");
            var point = new GLatLng(parseFloat(markers[i].getAttribute("lat")),
                                    parseFloat(markers[i].getAttribute("lng")));
            var marker = createMarker(point, name, address);
            map.addOverlay(marker);
          }
        });
      }
    }

    function createMarker(point, name, address) {
      var marker = new GMarker(point, G_DEFAULT_ICON);
      var html = "<b>" + name + "</b> <br/>"+ address;

      GEvent.addListener(marker, 'click', function() {
        marker.openInfoWindowHtml(html);
      });
      return marker;
    }

    function removeHTMLTags(strInputCode){

 		//var strInputCode = document.getElementById("input-code").innerHTML;
 		/*
  			This line is optional, it replaces escaped brackets with real ones,
  			i.e. < is replaced with < and > is replaced with >
 		*/
 	 	strInputCode = strInputCode.replace(/&(lt|gt);/g, function (strMatch, p1){
 		 	return (p1 == "lt")? "<" : ">";
 		});
 		var strTagStrippedText = strInputCode.replace(/<\/?[^>]+(>|$)/g, "");
 		return strTagStrippedText;
   // Use the alert below if you want to show the input and the output text
   //		alert("Input code:\n" + strInputCode + "\n\nOutput text:\n" + strTagStrippedText);

}
    //]]>
  </script>
<div id="leftside"></div>
    	<div id="rightside">
          <table width="100%" border="0" cellspacing="0" cellpadding="0">
          <tr>
            <td width="6" valign="top"><img src="images/main_leftbg.png" alt="" /></td>
            <td valign="top" class="main_blockbg">
               <div class="result_block">
                 	<div class="read_doc">
                <h2 class="sub_title">Contact Information</h2>
                  <p class="txt_normal">Questions, concerns, suggestions? <br />
                  Feel free to contact us!</p>
                  <br /><br />
                  <table width="100%" border="0" cellspacing="0" cellpadding="0">
                      <tr>
                        <td width="48%"  valign="top">
                        <div id="change_content" >
                           <h3><?php echo index_lang::translate("HEAD OFFICES");?></h3>
                        <p class="address"><?= utf8_encode('Information Mapping� International'); ?><br />
                          Beekstraat 46<br />
                          9031 Ghent (Belgium)<br />
                          Tel.: +32 (0)9 253 14 25<br />
                          <a href="mailto:info@informationmapping.com">info@informationmapping.com</a></p>
                          <br />
                          <br />
                          <h3>US OFFICE</h3>
                        <p class="address"><?= utf8_encode("Information Mapping� Inc USA"); ?><br />
                          135 Beaver Street<br />
                          Waltham, MA 02452 (U.S.A.) <br />
                          Tel.: 781-547-3100 <br />
                          or <br />
                          0800-INFOMAP (463-6627)</p>
                        </div>

                        </td>
                        <td valign="top">
                        <h3><?php echo utf8_encode(index_lang::translate("PARTNER NETWORK"));?></h3>
                        <div id="map" style="width: 500px; height: 300px"></div>
                        <br /><br />
                        <div style="position:relative;">
                        	<div class="parter_info">
                            	<form action="/en/local-partners" method="get">
                                	<select name="country" id="country" class="field" onchange="getAddress(this.value);">
                            			<option value="0"><?php echo index_lang::translate("Choose a country");?></option>
                                        <option value="227"> <?php echo index_lang::translate("Other countries");?></option>
                                        <?foreach($Countries as $country){?>
                                        <option value="<?php echo $country['country_id'];?>"><?php echo $country['country_name'];?></option>
                                    <?php }?>
                                    </select>
                    			</form>
                            </div>
                        </div>
                        </td>
                      </tr>
                    </table>
              </div> </div>
            </td>
            <td width="6" valign="top"><img src="images/main_rightbg.png" alt="" /></td>
          </tr>
        </table>
		</div>
    	<div class="clear"></div>