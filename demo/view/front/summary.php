<script>
function roundNumber(num, dec) {
	var result = Math.round(num*Math.pow(10,dec))/Math.pow(10,dec);
	return parseFloat(result);
}
function calculate()
{
  var looks= document.getElementById('looks').value;
  var employees= document.getElementById('employees').value;
  var salary= document.getElementById('salary').value;
  var session= document.getElementById('session').value;
  var secperday= looks*session/100;
  var hrsperyear= roundNumber(secperday,2)*250/3600;
  var totalhrsperyear= roundNumber(hrsperyear,2)*employees ;
  var savings= roundNumber(totalhrsperyear,2)*salary/2000;
  document.getElementById('secperday').innerHTML= roundNumber(secperday,2)+"sec/day";
  document.getElementById('hrsperyear').innerHTML= roundNumber(hrsperyear,2)+" hr/year";
  document.getElementById('totalhrsperyear').innerHTML= roundNumber(totalhrsperyear,2)+" hr/year";
  document.getElementById('savings').innerHTML= roundNumber(savings,2)+ " $/year";

}

</script>

	<div id="leftside"></div>
    	<div id="rightside">
          		<div class="main_heading"><h1 class="title"><?php echo index_lang::translate("The Bottom Line");?></h1></div>
                <input type="hidden" name="session" id="session" value="<?php echo $_SESSION['resulttime'];?>" />
          <div class="main_bottom_line">
          <?php
          $hours=number_format((250*20*$_SESSION['resulttime']/360000),2);
          ?>
          	<table width="96%" align="center" border="0" cellspacing="0" cellpadding="0" class="calc_box">
                  <tr>
                    <td valign="middle"><?php echo str_replace("%%SESSION%%",$_SESSION['result_display_time'],index_lang::translate("Summary1")) ;?></td>
                    <td width="190" align="right" id="secperday" valign="middle" class="time_block"><?echo number_format ((20*$_SESSION['resulttime']/100),2);?> sec/day</td>
              </tr>
                  <tr>
                    <td valign="middle"><?php echo utf8_encode( index_lang::translate("Summary2"));?></td>
                    <td align="right" valign="middle" id="hrsperyear" class="time_block"><?echo $hours ?> hr/year</td>
              </tr>
                  <tr>
                    <td valign="middle"><?php echo utf8_encode(index_lang::translate("Summary3"));?></td>
                    <td align="right" valign="middle" id="totalhrsperyear" class="time_block"><?echo ($hours*100);?> hr/year</td>
              </tr>
                  <tr>
                    <td><?php echo utf8_encode(index_lang::translate("Summary4"));?></td>
                    <td align="right" id="savings" class="time_block"><?echo number_format(20*100*$hours,2);?> $/year</td>
                  </tr>
            </table>

          </div>
          <form name="tryagain" id="tryagain" method="post">
              <table width="100%" border="0" cellspacing="0" cellpadding="0">
                  <tr>
                    <td valign="middle" class="wh_text1"><?php echo index_lang::translate("Variations in result");?></td>
                    <td width="192" valign="top"><input type="button" onclick="calculate();" name="button" class="recalc" value="<?php echo index_lang::translate("Recalculate");?>" /></td>
                </tr>
                  <tr>
                    <td align="center" valign="top">
               	      <div class="btn_demo"><button class="button" title="Try Demo Again" name="retry" type="submit"><span class="button-right"><span class="button-left"><?php echo index_lang::translate("Try Demo Again");?></span></span></button></div>
                        <div class="learn_now"><a href="#"><?php echo index_lang::translate("Learn Now");?>!</a></div>
                    </td>
                    <td  width="192" valign="top" align="center" class="wh_text2"><?php echo index_lang::translate("Calculate Instructions");?></td>
                </tr>
          </table></form>

</div>
    	<div class="clear"></div>
        <div class="wh_text1 bottom_text"><?php echo utf8_encode(index_lang::translate("Summary footer"));?></div>
    <!--content end-->
