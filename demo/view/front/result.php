<style type="text/css">


    /* HOVER STYLES */
    div#pop-up {
        display: none;
        position: absolute;
        z-index:100;
        /* width: 280px;
         padding: 10px;
         background: #eeeeee;
         color: #000000;
         border: 1px solid #1a1a1a;
         font-size: 90%;*/
    }
    div#popup1 {
        display: none;
        position: absolute;
        z-index:100;
    }

</style>

<script>
    function roundNumber(num, dec) {
        var result = Math.round(num*Math.pow(10,dec))/Math.pow(10,dec);
        return parseFloat(result);
    }


    function addCommas(nStr)
    {
        nStr += '';
        x = nStr.split('.');
        x1 = x[0];
        x2 = x.length > 1 ? '.' + x[1] : '';
        var rgx = /(\d+)(\d{3})/;
        while (rgx.test(x1)) {
            x1 = x1.replace(rgx, '$1' + ',' + '$2');
        }
        return x1 + x2;
    }
    function calculate()
    {


        var looks= $('#lookup').val();
        var employees= $('#employee').val();
        var salary= $('#salary').val();
        var session= $('#session').val();
        var secperday= looks*session/100;
        var hrsperyear= secperday*365/3600;
        var totalhrsperyear= hrsperyear*employees ;
        var savings= (totalhrsperyear)*salary/2000;
        //var hrsperyear= roundNumber(secperday,2)*365/3600;
        //var totalhrsperyear= roundNumber(hrsperyear,2)*employees ;
        //var savings= roundNumber(totalhrsperyear,2)*salary/2000;

        /*document.getElementById('secperday').innerHTML= roundNumber(secperday,2)+"sec/day";
         document.getElementById('hrsperyear').innerHTML= roundNumber(hrsperyear,2)+" hr/year";
         document.getElementById('totalhrsperyear').innerHTML= roundNumber(totalhrsperyear,2)+" hr/year";*/

        $('#result1').val(addCommas(roundNumber(savings.toFixed(2),2))+" <?php echo index_lang::translate('per year');?>");

    }



    $(function() {
        $( "#lookup" ).change(function() {

            var slider_val = this.value;
            $( "#slider-lookup" ).slider({
                range: "min",
                value: slider_val,
                min: 1,
                max: 100,
                slide: function( event, ui ) {
                    $( "#lookup" ).val( ui.value );
                    calculate();

                }
            });
            calculate();
        });

        $( "#slider-lookup" ).slider({
            range: "min",
            value: 20,
            min: 1,
            max: 100,
            slide: function( event, ui ) {
                $( "#lookup" ).val( ui.value );
                calculate();
            }
        });


        $( "#employee" ).change(function() {

            var slider_val1 = this.value;
            $( "#slider-employee" ).slider({
                range: "min",
                value: slider_val1,
                min: 1,
                max: 5000,
                slide: function( event, ui ) {
                    $( "#employee" ).val( ui.value );
                    calculate();
                }
            });
            calculate();
        });

        $( "#slider-employee" ).slider({
            range: "min",
            value: 100,
            min: 1,
            max: 5000,
            slide: function( event, ui ) {
                $( "#employee" ).val( ui.value );
                calculate();
            }
        });


        $( "#salary" ).change(function() {

            var slider_val2 = this.value;
            $( "#slider-salary" ).slider({
                range: "min",
                value: slider_val2,
                min: 1,
                max: 100000,
                slide: function( event, ui ) {
                    $( "#salary" ).val( ui.value );
                    calculate();
                }
            });

            calculate();
        });


        $( "#slider-salary" ).slider({
            range: "min",
            value: 40000,
            min: 1,
            max: 100000,
            slide: function( event, ui ) {
                $( "#salary" ).val( ui.value );
                calculate();
            }
        });

        //$( "#amount" ).val(  $( "#slider-range-min" ).slider( "value" ) );

    });
</script>
<script type="text/javascript">
    $(function() {
        var moveLeft = 20;
        var moveDown = 10;

        $('img#trigger').hover(function(e) {
            $('div#pop-up').show();
            $("div#pop-up").css('top', '5px').css('left', '601px');
            //.css('top', e.pageY + moveDown)
            //.css('left', e.pageX + moveLeft)
            //.appendTo('body');
        }, function() {
            $('div#pop-up').show();
        });


        $("div#pop-up").mouseleave(function(){
            $('div#pop-up').hide();
        });

    });


    $(function() {
        var moveLeft = 20;
        var moveDown = 10;

        $('img#trigger1').hover(function(e) {
            $('div#popup1').show();
            $("div#popup1").css('top', '5px').css('left', '601px');
            //.css('top', e.pageY + moveDown)
            //.css('left', e.pageX + moveLeft)
            //.appendTo('body');
        }, function() {
            $('div#popup1').show();
        });


        $("div#popup1").mouseleave(function(){
            $('div#popup1').hide();
        });

    });
</script>
<?php $doc_type=(isset($_SESSION['doc_id']))?$_SESSION['doc_id']:'1'; ?>
<?php if($doc_type=="1")
{

    if($_SESSION['domain_id']=='1')
    {
        $bf = "before-timeyellow";
        $af = "after-timeyellow";
    }
    if($_SESSION['domain_id']=='2')
    {
        $bf = "before-timeyellowger";
        $af = "after-timeyellowger";
    }
    if($_SESSION['domain_id']=='3')
    {
        $bf = "before-timeyellowsp";
        $af = "after-timeyellowsp";
    }
    if($_SESSION['domain_id']=='4')
    {
        $bf = "before-timeyellowdut";
        $af = "after-timeyellowdut";
    }
    if($_SESSION['domain_id']=='5')
    {
        $bf = "before-timeyellowfre";
        $af = "after-timeyellowfre";
    }
    if($_SESSION['domain_id']=='6')
    {
        $bf = "before-timeyellowjap";
        $af = "after-timeyellowjap";
    }

    $tm = "timeing-beforeylow";
    $tm1 = "timeing-afteryelow";
    $clr = "color:#ffb504!important; ";
    $ft = "first-tab cost_yellow";
    $ft2 = "firsttab-containt-yellow";
    $img = "images/yellow_noteicon.png";
    $imgcontb = "images/yel_bef.png";
    $imgconta = "images/yel_aft.png";
    ?>
    <style>
        .ui-widget-header
        {

            background: none repeat-x scroll 50% 50% #ffb504;
            border: 1px solid #AAAAAA;
            color: #222222;
            font-weight: bold;
        }


    </style>
<?php }
elseif($doc_type=="3")
{
    if($_SESSION['domain_id']=='1')
    {
        $bf = "before-timegren";
        $af = "after-timegren";
    }
    if($_SESSION['domain_id']=='2')
    {
        $bf = "before-timegrenger";
        $af = "after-timegrenger";
    }
    if($_SESSION['domain_id']=='3')
    {
        $bf = "before-timegrensp";
        $af = "after-timegrensp";
    }
    if($_SESSION['domain_id']=='4')
    {
        $bf = "before-timegrendut";
        $af = "after-timegrendut";
    }
    if($_SESSION['domain_id']=='5')
    {
        $bf = "before-timegrenfre";
        $af = "after-timegrenfre";
    }
    if($_SESSION['domain_id']=='6')
    {
        $bf = "before-timegrenjap";
        $af = "after-timegrenjap";
    }





    $tm = "timeing-beforegren";
    $tm1 = "timeing-beforegren";
    $clr = "color:#447645!important; ";
    $ft = "first-tab cost_green";
    $ft2 = "firsttab-containt-grn";
    $img = "images/green_noteicon.png";
    $imgcontb = "images/grn_bef.png";
    $imgconta = "images/grn_aft.png";
    ?>
    <style>
        .ui-widget-header
        {

            background: none repeat-x scroll 50% 50% #447645;
            border: 1px solid #AAAAAA;
            color: #222222;
            font-weight: bold;
        }


    </style>
<?php
}
elseif($doc_type=="2")
{

    if($_SESSION['domain_id']=='1')
    {
        $bf = "before-time";
        $af = "after-timepink";
    }
    if($_SESSION['domain_id']=='2')
    {
        $bf = "before-timeger";
        $af = "after-timepinkger";
    }
    if($_SESSION['domain_id']=='3')
    {
        $bf = "before-timesp";
        $af = "after-timepinksp";
    }
    if($_SESSION['domain_id']=='4')
    {
        $bf = "before-timedut";
        $af = "after-timepinkdut";
    }
    if($_SESSION['domain_id']=='5')
    {
        $bf = "before-timefre";
        $af = "after-timepinkfre";
    }
    if($_SESSION['domain_id']=='6')
    {
        $bf = "before-timejap";
        $af = "after-timepinkjap";
    }


    $tm = "timeing-before";
    $tm1 = "timeing-afterpink";
    $clr = "color:#ED008C!important; ";
    $ft = "first-tab";
    $ft2 = "firsttab-containt";
    $img = "images/note-icon.png";
    $imgcontb = "images/pink_bef.png";
    $imgconta = "images/pink_aft.png";
    ?>
    <style>
        .ui-widget-header
        {

            background: none repeat-x scroll 50% 50% #ED008C;
            border: 1px solid #AAAAAA;
            color: #222222;
            font-weight: bold;
        }


    </style>
<?php
}
?>

<input type="hidden"  name="result_time" id="result_time" value="<?= $resulttime; ?>"  />
<input type="hidden" name="result_display_time" id="result_display_time" value="<?= $result_display; ?>"/>
<input type="hidden" name="session" id="session" value="<?php echo $_SESSION['resulttime'];?>" />
<?php
//$hours=number_format((365*20*$_SESSION['resulttime']/360000),2);
$hours=(365*20*$_SESSION['resulttime']/360000);
?>
<div class="innerpage-containt">
    <div class="result-containt">
        <h2><?php echo index_lang::translate("Using a Mapped document instead of a traditional bussiness document, you save"); ?>
            <!--<?if($doc_type=="1")
                echo index_lang::translate("Typical Busines Document");
            elseif($doc_type=="3")
                echo index_lang::translate("traditional email");
            elseif($doc_type=="2")
                echo index_lang::translate("Typical Web Page");?>
      <?php  echo ",";?>
      <?php echo index_lang::translate("you save"); ?>-->
        </h2>
        <h3 style="<?php echo $clr; ?> padding-bottom:0px;">  <?= $result_display; ?>  <?php echo index_lang::translate("seconds per document");?></h3>
        <h4 style="<?php echo $clr; ?> margin-top:0px;">(Min:Sec.ms)</h4>
        <div class="result-colum">
            <div class="result-row">
                <div class="result-memofirst">
                    <div class="memofirst-colum">
                        <img src="<?php  echo $imgcontb; ?>" alt="" />
                    </div>
                    <div class="<?php  echo $bf; ?>">
                        <div class="<?php  echo $tm; ?>"><?php echo $_SESSION['doc_time']; ?></div>
                    </div>
                </div>
                <div class="result-memosecond">
                    <div class="memosecond-colum">
                        <img src="<?php  echo $imgconta; ?>" alt="" />
                    </div>
                    <div class="<?php  echo $af; ?>">
                        <div class="<?php  echo $tm1; ?>"><?php echo $_SESSION['mapdoc_time']; ?></div>
                    </div>
                </div>
            </div>
        </div>
        <?php
        $email = isset($_POST['email']) ? htmlentities($_POST['email'], ENT_QUOTES) : '';
        $name = isset($_POST['name']) ? htmlentities($_POST['name'], ENT_QUOTES) : '';
        $verzenden = isset($_GET['verzonden']);
        if($verzenden)
        {
            $result_wim = mysql_query("INSERT INTO webapp_results_named (name, email, result, date) VALUES ('$name', '$email', '$result_display', NOW())");
            echo "<div class='col-md-12'><p class='thanks'>Thank you! Your result has been saved.</p></div>";
            ?>
            <iframe src="http://www2.informationmapping.com/l/8622/2013-09-12/djnp7?name=<?php echo $name; ?>&email=<?php echo $email; ?>" width="1" height="1" style="display:none; border: none;"></iframe>
        <?php
        }
        else
        {
            ?>
            <div class="col-md-12">
                <p>Let us know your time saving result (<?php echo $result_display; ?>) and help our international research!</p>

                <form class="form-horizontal" method="post" action="http://www2.informationmapping.com/l/8622/2015-07-16/t3337">
                    <div class="form-group">
                        <label for="email" class="col-sm-2 control-label">Email</label>
                        <div class="col-sm-6">
                            <input type="email" class="form-control" id="email" name="email" required>
                        </div>
                    </div>
                    <div class="form-group">
                        <label for="firstname" class="col-sm-2 control-label">First Name</label>
                        <div class="col-sm-6">
                            <input type="text" class="form-control" id="firstname" name="firstname" required>
                        </div>
                    </div>
                    <div class="form-group">
                        <label for="lastname" class="col-sm-2 control-label">Last Name</label>
                        <div class="col-sm-6">
                            <input type="text" class="form-control" id="lastname" name="lastname" required>
                        </div>
                    </div>
                    <div class="form-group">
                        <div class="col-sm-offset-2 col-sm-10">
                            <button type="submit" name="verzonden" value="verzonden" class="btn btn-blue btn-lg">Send</button>
                        </div>
                    </div>
                </form>

            </div>
        <?php
        }
        ?>

        <div class="saving-colum">
            <div class="<?php  echo $ft; ?>"><?php echo index_lang::translate("Recalculate yourself");?>
                <div class="note-icon" ><img id="trigger1" src="<?php  echo $img; ?>" alt="" /></div>
                <div id="popup1" class="note-colum">
                    <div class="note-topcur" style="<?php echo $clr; ?>"><?php echo index_lang::translate("Note");?> <img src="<?php  echo $img; ?>" alt="" /></div>
                    <div class="note-midlecur">
                        <p><?php echo index_lang::translate("Since individual and company time saving and performance may vary,please contact an Information Mapping Partner to discuss your specific needs.");?></p>
                    </div>
                    <div class="note-bottomcur"></div>
                </div>
            </div>
            <div class="recalculate-colum">
                <div class="calculate-row">
                    <div class="calculate-left">
                        <p><?php echo index_lang::translate("Number of lookups per day");?></p>
                        <div style="width: 500px;float: left;clear: right;cursor: pointer;height: 9px;margin-top: 10px;position: relative;"><div style="color: #447645 !important" id="slider-lookup" ></div></div> </div>
                    <div class="calculate-right">
                        <input id="lookup" type="text" value="20" />
                    </div>
                </div>
                <div class="calculate-row">
                    <div class="calculate-left">
                        <p><?php echo index_lang::translate("Number of employees");?></p>
                        <div style="width: 500px;float: left;clear: right;cursor: pointer;height: 9px;margin-top: 10px;position: relative;"><div id="slider-employee" ></div></div>                </div>

                    <div class="calculate-right">
                        <input id="employee" type="text" value="100" />
                    </div>
                </div>
                <div class="calculate-row">
                    <div class="calculate-left">
                        <p><?php echo index_lang::translate("Average yearly salary (USD)");?></p>
                        <div style="width: 500px;float: left;clear: right;cursor: pointer;height: 9px;margin-top: 10px;position: relative;"><div id="slider-salary" ></div></div> </div>
                    <div class="calculate-right">
                        <input id="salary" type="text" value="40000" />
                    </div>
                </div>
                <div class="clear"></div>
            </div>
        </div>
        <div class="saving-colum">
            <div class="<?php  echo $ft; ?>"><?php echo index_lang::translate("Cost savings");?>
                <div class="note-icon"><img id="trigger" src="<?php  echo $img; ?>" alt="" /></div>
                <div id="pop-up" class="note-colum">
                    <div class="note-topcur" style="<?php echo $clr; ?>"><?php echo index_lang::translate("Note");?> <img src="<?php  echo $img; ?>" alt="" /></div>
                    <div class="note-midlecur">
                        <p><?php echo index_lang::translate("Each employee works 50 weeks per year, 40 hours per week. Time savings per lookup are based on users time test results; therefore, individual savings per lookup may vary. Other figures are based on default data or data entered by user for recalculation.");?></p>
                    </div>
                    <div class="note-bottomcur"></div>
                </div>
            </div>
            <div class="<?php  echo $ft2; ?>">
                <p><?php echo index_lang::translate("Using Information Mapping &copy can save your organization");?></p>

                <span>$<input style="border: none;font-size: 24px;font-family: Arial;width: 250px;<?php echo $clr; ?>" type="text" id="result1" value="<?php echo number_format(20*100*$hours,2);?> <?php echo index_lang::translate("per year");?>"  /></span>
            </div>
            <div class="form-group" >
                <form class="form-inline" name="tryagain" id="tryagain" method="post">
                    <div class="col-sm-offset-1 col-sm-3">
                        <button type="submit" class="btn btn-blue btn-lg btn-block" title="Try Demo Again" name="retry" value="<?php echo index_lang::translate("Try Demo Again");?>" ><?php echo index_lang::translate("Try Demo Again");?></button>
                    </div>
                    <div class="col-sm-3">
                        <button type="button" class="btn btn-blue btn-lg btn-block"  value="<?php echo index_lang::translate("Find a training");?>" onClick="window.open('http://www.informationmapping.com/demo/redirect.php?k=1');" ><?php echo index_lang::translate("Find a training");?></button>
                    </div>
                    <div class="col-sm-3">
                        <button type="button" class="btn btn-pink btn-lg btn-block" value="<?php echo index_lang::translate("Buy now");?>" onClick="window.open('http://www.informationmapping.com/demo/redirect.php?k=2')" ><?php echo index_lang::translate("Buy now");?></button>
                    </div>
                </form>
                <!--<input type="button" value="Find a training" onClick="window.open('http://training.informationmapping.com','windowname1','width=900, height=700 ,scrollbars=1'); return false;" />-->

            </div>

            <!--
    <div id="btmcontent">
  <form name="summary" id="summary" method="post">

   <input type="hidden" name="result_time" id="result_time" value="<?= $resulttime; ?>"/>
   <input type="hidden" name="result_display_time" id="result_display_time" value="<?= $result_display; ?>"/>
    <table width="100%" border="0" cellspacing="0" cellpadding="0">
      <tr>
        <td colspan="2" align="right" class="cntpad"><button class="button" name="submit" type="submit" style="width:230px;"><span class="button-right"><span class="button-left"><?php echo utf8_encode(index_lang::translate("Continue To Summary"));?></span></span></button></td>
      </tr>
    </table>
  </form>
  </div>-->
            <div class="clear"></div>
        </div>
    </div>
