<script type="text/javascript">
$(document).ready(function() {
$('#day_select input[type="checkbox"]').change(function(e){ //all checkboxes within main wrapper
    var selects = $('div[day_id="'+$(this).val()+'"] select'); // select all DIV elements which have attribute "day_id" of a value of changed checkbox (this)
    var checked = $(this).attr('checked'); // is it checked?
    if ( checked ){
        selects.removeAttr('disabled',''); // enable selects
    }else{
        selects.attr('disabled','disabled'); // disable selects
    }
    })
    .trigger('change'); // fire initial change event; this will initially enable/disable select dropdowns according to state of checkbox (probably loaded from db)
});
    </script>
<script>
 function add_validation(element,val)
 {

    if(document.getElementById(element).value !="")
    {
      document.getElementById(element).className=val;
    }
    else
    {
       document.getElementById(element).className="input";
    }
 }
</script>
<script type="text/javascript">
$(document).ready(function() {
	$("#add_contact").validationEngine();
});
</script>
<?php
if($_SESSION['domain_id']=='1')
{ $submit = 'input-btn';}
if($_SESSION['domain_id']=='2')
{ $submit = 'input-btnger';}
if($_SESSION['domain_id']=='3')
{ $submit = 'input-btnsp';}
if($_SESSION['domain_id']=='4')
{ $submit = 'input-btndut';}
if($_SESSION['domain_id']=='5')
{ $submit = 'input-btnfre';}
if($_SESSION['domain_id']=='6')
{ $submit = 'input-btnjp';}
?>

<div class="innerpage-containt">
    	<div class="contact-containt" style="padding-top: 15px;">
         <!--<div class="title-contact"><?php echo index_lang::translate("Contact us");?></div>-->
         <h2><?php echo index_lang::translate("The fastest way for a direct response to your question!");?></h2>
<div><?php  if(isset($_SESSION['success'])){ echo $_SESSION['success']; session_unset($_SESSION['success']);  }   ?></div>
<div><?php if(isset($_SESSION['error'])){ echo $_SESSION['error']; session_unset($_SESSION['error']);  }   ?></div>
         <div class="contact-colum">
         <form name="add_contact" id="add_contact" method="post" enctype="multipart/form-data">
         	<div class="contact-lef">
            	<div class="contact-row">
            	<div class="contact-txt"><?php echo index_lang::translate("Country");?></div>
                <div class="contact-input">
                                       <select name="country" id="country" class="field">
                            			<option value="0"><?php echo index_lang::translate("Choose a country");?></option>
                                        <?foreach($Countries as $country){?>
                                        <option value="<?php echo $country['country_id'];?>"><?php echo $country['country_name'];?></option>
                                    <?php }?>
                                    </select>
                </div>
            </div>

            	<div class="contact-row">
            	<div class="contact-txt"><?php echo index_lang::translate("Title");?></div>
                <div class="contact-input">  <select id="title" name="title" >
                                            <option value="mr"><?php echo index_lang::translate("Mr.");?></option>
                                            <option value="mrs"><?php echo index_lang::translate("Mrs.");?></option>
                                            <option value="miss"><?php echo index_lang::translate("Miss.");?></option>

                                        </select></div>
            </div>

            	<div class="contact-row">
            	<div class="contact-txt"><?php echo index_lang::translate("First name");?><span class="star">*</span></div>
                <div class="contact-input"><input class="input validate[required]" type="text" value="" id="fname" name="fname" /></div>
            </div>

            <div class="contact-row">
            	<div class="contact-txt"><?php echo index_lang::translate("Last name");?><span class="star">*</span></div>
                <div class="contact-input"><input class="input validate[required]" type="text" value="" id="lname" name="lname" /></div>
            </div>

            <div class="contact-row">
            	<div class="contact-txt"><?php echo index_lang::translate("Email");?><span class="star">*</span></div>
                <div class="contact-input"><input class="input validate[required]" type="text" value="" id="email" name="email" /></div>
            </div>

            <div class="contact-row">
            	<div class="contact-txt"><?php echo index_lang::translate("Phone");?><span class="star">*</span></div>
                <div class="contact-input"><input class="input validate[required]" type="text" value="" id="phone" name="phone" /></div>
            </div>
            <div class="contact-row">
            	<div class="chkbox" id="day_select"><input class="input" id="terms" name="terms" type="checkbox" value="1" style="border:0 none; outline:none;"/></div>
                <div class="contact-input">
                	<p><?php echo index_lang::translate("I agree that Information Mapping International and its official Partners can contact me about our products and services");?></p>
                </div>
            </div>

            <div class="contact-row">
            	<div class="chkbox">&nbsp;</div>
                <div class="contact-input">
                	  <div class="contact-input" day_id="1">
                                        <select style="width:120px;" id="contact" name="contact" >
                                            <option value="p"><?php echo index_lang::translate("By Phone");?></option>
                                            <option value="e"><?php echo index_lang::translate("BY Email");?></option>
                                            <!--<option value="c"><?php echo index_lang::translate("BY Contact");?></option>-->
                                        </select>
                </div>
                </div>
            </div>

            <div class="contact-row">
            	<div class="chkbox">&nbsp;</div>
                <div class="contact-input">
			   	  	<input type="hidden" id="contactForm" name="contactForm" value = "1" />
                	  <div class="<?php echo $submit; ?>"><input name="CmsFormSubmit" id="CmsFormSubmit" type="submit"  class="subbut" value="" /></div>
                </div>
            </div>

            </div>

            </form>
            <div class="contact-right">
            	<h3>Global</h3>
                <p>Information Mapping International nv <br >
                Ottergemsesteenweg Zuid 808 - bus 340 <br />
                9031 Ghent (Belgium) <br /><br />
                Tel. + 32 9 253 14 25
	            <a href="mailto:info@informationmapping.com">info@informationmapping.com</a>
    	        <a href="http://www.informationmapping.com" target="_blank">www.informationmapping.com</a>
                </p>
                <h3>US</h3>
                <p>
                Information Mapping US.<br />
                135 Beaver Street<br />
                Waltham, MA 02452, U.S.A. <br /><br />

                1-800-INFOMAP (1-800-463-6627)
                <a href="mailto:contactus@informationmapping.com">contactus@informationmapping.com </a>
               <a href="http://www.informationmapping.com" target="_blank">www.informationmapping.com</a>
			</p>
            <h3>India</h3>
                <p>Information Mapping India <br />
                Unit No. 2201 A, 22nd Floor <br />
                World Trade Center, Brigade Gateway <br />
                Malleswaram West <br />
                Bangalore 560055 <br /><br />
                Tel. +91 (80) 4936 5328/29
	            <a href="mailto:info@informationmapping.com">info@informationmapping.com</a>
    	        <a href="http://www.informationmapping.com/in" target="_blank">www.informationmapping.com/in</a>
                </p>
            </div>

         <div class="clear"></div>
         </div>

         <div class="clear"></div>
        </div>
</div>