<footer class="row">
    <div class="col-sm-12" id="footertext">
        <p class="imi-footer">&copy; <?php echo date("Y"); ?> Information Mapping International</p>
        <p class="imi-footer icons"> <a href="https://twitter.com/infomap"><i class="fa fa-twitter-square fa-lg"></i></a> <a href="https://www.facebook.com/iminv"><i class="fa fa-facebook-official fa-lg"></i></a> <a href="http://www.linkedin.com/groups?gid=983927"><i class="fa fa-linkedin-square fa-lg"></i></a> <a href="http://www.youtube.com/user/InformationMapping"><i class="fa fa-youtube-square fa-lg"></i></a> </p>
    </div>
</footer>