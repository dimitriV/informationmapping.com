<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml">
<head>
    <title>Information Mapping Demo</title>
    <meta http-equiv="Content-Type" content="text/html;charset=UTF-8">
    <meta http-equiv="X-UA-Compatible" content="IE=9">
    <!-- Latest compiled and minified CSS -->
    <link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.5/css/bootstrap.min.css">
    <link rel="stylesheet" href="//maxcdn.bootstrapcdn.com/font-awesome/4.3.0/css/font-awesome.min.css">
    <link type="text/css" rel="stylesheet" href="/demo/min/general.css" media="all" />
    <link type="text/css" rel="stylesheet" href="/demo/min/custom.css" media="all" />
    <!--<script type="text/javascript" src="/demo/min/g=js" defer></script>-->
    <!--<script type="text/javascript" src="<?php echo JS_PATH ?>note.js"></script>-->
    <script src="<?php echo JS_PATH ?>jquery-1.7.1.js"></script>
    <!--<script src="//code.jquery.com/jquery-1.11.3.min.js"></script>
    <script src="//code.jquery.com/jquery-migrate-1.2.1.min.js"></script>-->
    <!--<script src="<?php echo JS_PATH ?>jquery.ui.core.js"></script> -->
    <script src="<?php echo JS_PATH ?>jquery.ui.slider.js"></script>
    <script src="<?php echo JS_PATH ?>ui/jquery.ui.dialog.js"></script>
    <!-- Latest compiled and minified JavaScript -->
    <!--<script src="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.5/js/bootstrap.min.js"></script>-->
    <script language="javascript" src="<?php echo JS_PATH ?>jquery-ui.js"></script>

    <?php

    if($_REQUEST['page']=="contact")
    {
        include("../..".JS_PATH."jsvalidationmsg.php");
    }
    ?>
    <?php
    if(isset($_REQUEST['domain_id']) &&$_REQUEST['domain_id']>0)
    {

        if(isset($_SESSION['domain_id']))
        {

            unset($_SESSION['domain_id']);
        }

        $_SESSION['domain_id'] = $_REQUEST['domain_id'];

        if(isset($_SESSION['domain_name']))
        {

            unset($_SESSION['domain_name']);
        }

        $_SESSION['domain_name'] = 'eng';

        // echo $_SESSION['domain_id'];  exit;
    }

    if(isset($_SESSION['domain_id']))
    {
        $checked_radio= $_SESSION['domain_id'];
    }
    else
    {
        $checked_radio=1;
    }
    $language = array();
    $language[1]['text'] = 'English';
    $language[3]['text'] = 'Español';
    $language[5]['text'] = 'Français';
    $language[6]['text'] = '日本';
    $language[2]['text'] = 'Deutsch';
    $language[4]['text'] = 'Nederlands';
    ?>

    <script>
        $(document).ready(function() {
            $('.newsleader-colum').hover(function(){

                    $('.background-none').addClass('background-none1');
                    $('.background-none1').removeClass('background-none');

                },
                function()
                {
                    $('.background-none1').addClass('background-none');

                }
            );

        });

        function savedomain(val)
        {
            var id= val;
            if(id != 0 || id != null)
            {
                document.getElementById("domain_id").value= id;

            }



            document.getElementById("domain_sel").submit();
        }
    </script>
    <!-- Clocks scripts -->
    <script type="text/javascript" language="text/javascript">
        function stop_clock()
        {
            // alert(t[6].value);
            document.clock_counting.submit();

        }
    </script>
    <script type='text/javascript'>
        // 0/1 = start/end
        // 2 = state
        // 3 = length, ms
        // 4 = timer
        // 5 = epoch
        // 6 = disp el
        // 7 = lap count

        var t=[0, 0, 0, 0, 0, 0, 0, 0];

        function ss() {


            t[t[2]]=(new Date()).valueOf();
            t[2]=1-t[2];

            if (0==t[2]) {
                clearInterval(t[4]) ;


            } else {

                t[4]=setInterval(disp, 43);
                //alert(t[6].value);
            }

        }
        function r() {
            if (t[2]) ss();
            t[4]=t[3]=t[2]=t[1]=t[0]=0;
            disp();
            document.getElementById('lap').innerHTML='';
            t[7]=1;
        }

        function disp() {
            if (t[2])t[1]=(new Date()).valueOf();
            t[6].value=format(t[3]+t[1]-t[0]);
            document.getElementById('clock_counter').value = t[6].value;


        }
        function format(ms) {
            // used to do a substr, but whoops, different browsers, different formats
            // so now, this ugly regex finds the time-of-day bit alone
            var d=new Date(ms+t[5]).toString()
                .replace(/.*([0-9][0-9]:[0-9][0-9]).*/, '$1');
            var x=String(ms%100);
            while (x.length < 2) x='0'+x;
            d+='.'+x;

            return d;

        }

        function load() {
            t[5]=new Date(1970, 1, 1, 0, 0, 0, 0).valueOf();
            t[6]=document.getElementById('disp');


            disp(); ss();

        }
        load();

    </script>






    <!-- for select box -->


</head>
<?php $doc_type=(isset($_SESSION['doc_id']))?$_SESSION['doc_id']:'1'; ?>
<?php
if($doc_type=="1")
{
    $head = "yellow-header";
    $exercise = "A typical business document";
    $exercise1 = "Exercise 2: A mapped document";
}
elseif($doc_type=="3")
{
    $head = "green-header";
    $exercise = "A typical e-mail";
    $exercise1 = "A Mapped e-mail";
}
elseif($doc_type=="2")
{
    $head = "saving-header";
    $exercise = "A conventionally written web page";
    $exercise1 = "A Mapped web page";
}?>

<?php if($_REQUEST['page']=="contact" || $_REQUEST['page']=="partner"){?>
<body onload="load()" onunload="GUnload()">
<?php }
else{?>
<body>
<?php }?>
<div id="wrapper">
    <!--main area start-->
    <?php if($_REQUEST['page']=="contact"){?>
    <div class="contact-mainheader">
        <div class="header_2">
            <div class="otherhedr-colum">
                <?php }
                elseif($_REQUEST['page']=="partner"){ ?>
                <div class="find-header">
                    <div class="header_2">

                        <div class="otherhedr-colum">
                            <?php }
                            elseif($_REQUEST['page']=="did_u_know"){ ?>
                            <div class="did-header">
                                <div class="header_2">

                                    <div class="otherhedr-colum">
                                        <?php }
                                        elseif($_REQUEST['page']=="information_what"){ ?>
                                        <div class="iw-header">
                                            <div class="header_2">

                                                <div class="otherhedr-colum">
                                                    <?php }
                                                    elseif($_REQUEST['page']=="instruction" || $_REQUEST['page']=="doc_instruction" || $_REQUEST['page']=="read_doc" || $_REQUEST['page']=="read_time" || $_REQUEST['page']=="map_doc" || $_REQUEST['page']=="map_doc_popup1" || $_REQUEST['page']=="map_doc_popup2" ||$_REQUEST['page']=="result" ||$_REQUEST['page']=="congrats"){ ?>
                                                    <div class="<?php echo $head; ?>">
                                                        <div class="header_2">
                                                            <div class="otherhedr-colum">
                                                                <?php }

                                                                else{?>
                                                                <div class="main-header">
                                                                    <div class="header">
                                                                        <?php }?>

                                                                        <div class="logo"><a href="./?page=select_language" title="Information Mapping"><img title="Information Mapping" src="images/logo.png" alt="" /></a></div>
                                                                        <div class="header-right">
                                                                            <div class="social-colum" style="margin-left: 435px;float: left"><a target="_blank" href="http://www.facebook.com/iminv"><img src="images/f-icon.png" alt="" /></a> <a target="_blank" href="https://twitter.com/infomap"><img src="images/t-icon.png" alt="" /></a> <a href="mailto:info@informationmapping.com"><img src="images/m-icon.png" alt="" /></a>
                                                                            </div>


                                                                            <!--kartik language code-->
                                                                            <div class="newsleader-colum"  >
                                                                                <div class="lang_container">
                                                                                    <form name="domain_sel" id="domain_sel" method="post">
                                                                                        <div style="POSITION: absolute;">
                                                                                            <div class="meta">
                                                                                                <ul>
                                                                                                    <li class="language">

                          <span class="sub">
                          <a title="English" onclick="savedomain(<?=$checked_radio?>);" href="javascript:void(0);">
                              &nbsp;&nbsp;<span style="background: none repeat scroll 0 0 transparent !important;" <?php if($checked_radio==1){?> class="background-none" <?php }?>><?=$language[$checked_radio]['text']?></span></a></span>
           <span <?php if($checked_radio==1){?> <?php }else{?>  class="sub" <?php }?> >
                          <a title="English" onclick="savedomain(1);" href="javascript:void(0);">
                              &nbsp;&nbsp;<span <?php if($checked_radio==1){?> class="background-none" <?php }?>>English</span></a></span>
		   <span <?php if($checked_radio==3){?> <?php }else{?>  class="sub" <?php }?> >
                          <a title="Spanish" onclick="savedomain(3);" href="javascript:void(0);">
                              &nbsp;&nbsp;<span <?php if($checked_radio==3){?> class="background-none" <?php }?>>Español</span></a></span>

                          <span <?php if($checked_radio==5){?> <?php }else{?>  class="sub" <?php }?> >
                          <a title="French" onclick="savedomain(5);" href="javascript:void(0);">
                              &nbsp;&nbsp;<span <?php if($checked_radio==5){?> class="background-none" <?php }?>>Français</span></a></span>

                          <span <?php if($checked_radio==6){?> <?php }else{?>  class="sub" <?php }?> >
                          <a title="Japanese" onclick="savedomain(6);" href="javascript:void(0);">
                              &nbsp;&nbsp;<span <?php if($checked_radio==6){?> class="background-none" <?php }?>>日本</span></a></span>

                          <span <?php if($checked_radio==2){?> <?php }else{?>  class="sub" <?php }?> >
                          <a title="German" onclick="savedomain(2);" href="javascript:void(0);">
                              &nbsp;&nbsp;<span <?php if($checked_radio==2){?> class="background-none" <?php }?>>Deutsch</span></a></span>

                          <span <?php if($checked_radio==4){?> <?php }else{?>  class="sub" <?php }?> >
                          <a title="Dutch" onclick="savedomain(4);" href="javascript:void(0);">
                              &nbsp;&nbsp;<span <?php if($checked_radio==4){?> class="background-none" <?php }?>>Nederlands</span></a></span>



                                                                                                    </li>

                                                                                                </ul>
                                                                                            </div>
                                                                                        </div>
                                                                                        <input type="hidden" name="domain_id" id="domain_id" value="" />
                                                                                        <input type="hidden" name="domain_name" id="domain_name" value="" />
                                                                                    </form>
                                                                                </div>
                                                                            </div>

                                                                            <?php if($_SESSION['domain_id']=='3'){ ?>
                                                                            <div class="nav" style="float:left;width: 790px;">
                                                                                <?php }else{?>
                                                                                <div class="nav">
                                                                                    <?php } ?>

                                                                                    <ul>
                                                                                        <li class="sat-demo"><a <?php if($_REQUEST['page']=='read_doc' ||$_REQUEST['page']=='map_doc'||$_REQUEST['page']=='summary'||$_REQUEST['page']=='doc_instruction' ||$_REQUEST['page']=='select_language'){?>class="active"<?php }?> href="./?page=select_language" ><?php echo index_lang::translate("Start demo");?></a></li>
                                                                                        <li class="info-wht"><a <?php if($page=='information_what'){?>class="active"<?php }?> href="./?page=information_what"><?php echo index_lang::translate("Information what?");?></a></li>
                                                                                        <li class="did-you"><a <?php if($page=='did_u_know'){?>class="active"<?php }?> href="./?page=did_u_know"><?php echo index_lang::translate("Did you know?");?></a></li>
                                                                                        <li class="find-us"><a <?php if($page=='partner'){?>class="active"<?php }?> href="http://www.informationmapping.com/en/information-mapping/partner-network"><?php echo index_lang::translate("Find us");?></a></li>
                                                                                        <li class="contact"><a <?php if($page=='contact'){?>class="active"<?php }?> href="http://www.informationmapping.com/en/contact-3"><?php echo index_lang::translate("Contact Us");?></a></li>
                                                                                    </ul>
                                                                                </div>
                                                                            </div>
                                                                            <?php
                                                                            if($_REQUEST['page']=="partner")
                                                                            {  ?>
                                                                        </div>

                                                                        <div class="title-contact" <?php if($_SESSION['domain_id']=='5') {?>style="width: 992px;" <?php } ?> ><?php echo index_lang::translate("Your local Information Mapping Partner");?></div>
                                                                        <?php }
                                                                        elseif($_REQUEST['page']=="contact")
                                                                        {
                                                                        ?>
                                                                    </div>


                                                                    <div class="title-contact"><?php echo index_lang::translate("Contact us");?></div>
                                                                    <?php }
                                                                    elseif($_REQUEST['page']=="did_u_know")
                                                                    {
                                                                    ?>
                                                                </div>


                                                                <div class="title-contact"><?php echo index_lang::translate("Did you know?");?></div>
                                                            <?php }
                                                            elseif($_REQUEST['page']=="information_what")
                                                            {
                                                            ?>
                                                            </div>


                                                            <div class="title-contact"><?php echo index_lang::translate("Information what?");?></div>
                                                            <?php }
                                                            elseif($_REQUEST['page']=="congrats")
                                                            {
                                                            ?>
                                                        </div>


                                                        <div class="title-contact"><?php echo index_lang::translate("Congratulations!");?></div>
                                                        <?php }
                                                        elseif( $_REQUEST['page']=="instruction" || $_REQUEST['page']=="doc_instruction")
                                                        {
                                                        ?>
                                                    </div>


                                                    <div class="title-contact"> <span style="float:left;"><?php echo index_lang::translate($exercise);?></span> </div>
                                                <?php }
                                                elseif($_REQUEST['page']=="read_doc" || $_REQUEST['page']=="read_time")
                                                {
                                                ?>
                                                </div>


                                                <div class="title-contact"> <span style="float:left;"><?php echo index_lang::translate($exercise);?></span>
                                                    <?php if($_REQUEST['page']=="read_doc") { ?>
                                                        <form id="clock_counting" name="clock_counting" method="post">
                                                            <div class="rating-colum">
                                                                <div class="rate-diaply">
                                                                    <input class="time"  value="" id="disp"/>
                                                                    <input type="hidden" name="clock_counter" id="clock_counter" value="" />
                                                                    <input type="hidden" name="stop" id="stop" value="" />
                                                                </div>

                                                                <?php if($_SESSION['domain_id']=='1'){ ?>
                                                                <div class="stop-btn">
                                                                    <?php } ?>

                                                                    <?php if($_SESSION['domain_id']=='2'){ ?>
                                                                    <div class="stop-btnger">
                                                                        <?php } ?>


                                                                        <?php if($_SESSION['domain_id']=='3'){ ?>
                                                                        <div class="stop-btnsp">
                                                                            <?php } ?>


                                                                            <?php if($_SESSION['domain_id']=='4'){ ?>
                                                                            <div class="stop-btndut">
                                                                                <?php } ?>


                                                                                <?php if($_SESSION['domain_id']=='5'){ ?>
                                                                                <div class="stop-btnfre">
                                                                                    <?php } ?>

                                                                                    <?php if($_SESSION['domain_id']=='6'){ ?>
                                                                                    <div class="stop-btnjap">
                                                                                        <?php } ?>
                                                                                        <input type="submit" value="" class="button" onclick="stop_clock(); ss();" name="clock_stop" />
                                                                                    </div>
                                                                                </div>
                                                        </form>
                                                    <?php } ?>
                                                </div>
                                                <?php }

                                                elseif($_REQUEST['page']=="map_doc" || $_REQUEST['page']=="map_doc_popup1" || $_REQUEST['page']=="map_doc_popup2")
                                                {
                                                ?>
                                            </div>


                                            <div class="title-contact"> <span style="float:left;"><?php echo index_lang::translate($exercise1);?></span>
                                                <?php if($_REQUEST['page']=="map_doc") { ?>
                                                    <form id="clock_counting" name="clock_counting" method="post">
                                                        <div class="rating-colum">
                                                            <div class="rate-diaply">
                                                                <input class="time"  value="" id="disp"/>
                                                                <input type="hidden" name="clock_counter" id="clock_counter" value="" />
                                                                <input type="hidden" name="stop" id="stop" value="" />
                                                            </div>

                                                            <?php if($_SESSION['domain_id']=='1'){ ?>
                                                            <div class="stop-btn">
                                                                <?php } ?>

                                                                <?php if($_SESSION['domain_id']=='2'){ ?>
                                                                <div class="stop-btnger">
                                                                    <?php } ?>


                                                                    <?php if($_SESSION['domain_id']=='3'){ ?>
                                                                    <div class="stop-btnsp">
                                                                        <?php } ?>


                                                                        <?php if($_SESSION['domain_id']=='4'){ ?>
                                                                        <div class="stop-btndut">
                                                                            <?php } ?>


                                                                            <?php if($_SESSION['domain_id']=='5'){ ?>
                                                                            <div class="stop-btnfre">
                                                                                <?php } ?>

                                                                                <?php if($_SESSION['domain_id']=='6'){ ?>
                                                                                <div class="stop-btnjap">
                                                                                    <?php } ?>







                                                                                    <input type="submit" value="" class="button" onclick="stop_clock(); ss();" name="clock_stop" />
                                                                                </div>
                                                                            </div>
                                                    </form>
                                                <?php }?>
                                            </div>
                                            <?php }

                                            elseif($_REQUEST['page']=="result")
                                            {
                                            ?>
                                        </div>


                                        <div class="title-contact"><?php echo index_lang::translate("Summary"); ?>:<?php echo index_lang::translate("Time savings"); ?></div>
                                    <?php }
                                    ?>
                                    </div>
                                    <div class="clear"></div>
                                </div>
                                <?php if($page=='result'){
                                    include(FRONT_VIEW_PATH . $page . '.php');
                                }else{?>
                                    <div id="content">
                                    <?php	include(FRONT_VIEW_PATH . $page . '.php');  ?>
                                    </div><?php }?>
                            </div>

<?php include_once(FRONT_VIEW_PATH."common_footer.php"); ?>
</body>
</html>