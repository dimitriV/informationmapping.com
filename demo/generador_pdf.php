<?php
	
	define("PATH_LECTURA", $_SERVER['DOCUMENT_ROOT'] . "/");
	define("PATH_DESTINO", $_SERVER['DOCUMENT_ROOT'] . "/pdfs/");
	$ruta = PATH_LECTURA;

	echo $ruta;
	echo "<br>";

	$listado = array();
	ListarArchivos($ruta, $listado);
	
	if((is_array($listado)) && (sizeof($listado) > 0)){
	
		foreach($listado as $indice => $datos){
		
			$nombre_a = explode("/", $datos);
	
			$nombre_script =  $nombre_a[(sizeof($nombre_a) - 1)];

			$dn_script = explode(".", $nombre_script);

			$ext = $dn_script[(sizeof($dn_script) - 1)];

			if($ext != "jpg" && $ext != "png" && $ext != "gif" && $ext != "bmp" && $ext != "DS_Store" && $ext != "db") {

				$nombre = str_replace(".php","", $nombre_a[(sizeof($nombre_a) - 1)]);

				$fecha_modificacion = date("Y_m_d_H_i_s", filemtime($datos));

				//$comando = "groff $datos > " . PATH_DESTINO .  $nombre . ".ps | ps2pdf -dProcessColorModel=/DeviceCMYK " . PATH_DESTINO . $nombre . ".ps " . PATH_DESTINO . $nombre . ".pdf";
				$comando = "groff -Tps $datos > " . PATH_DESTINO .  $nombre . ".ps | convert " . PATH_DESTINO . $nombre . ".ps " . PATH_DESTINO . $fecha_modificacion . "_" . $nombre . ".jpg";
				//$comando = "groff $datos > " . PATH_DESTINO .  $nombre . ".ps | ps2pdf -dProcessColorModel=/DeviceCMYK " . PATH_DESTINO . $nombre . ".ps " . PATH_DESTINO . $nombre . ".pdf | convert  "  . PATH_DESTINO . $nombre . ".pdf "  . PATH_DESTINO . $nombre . ".jpg" ;

			

				exec($comando);
				unlink(PATH_DESTINO .  $nombre . ".ps");
				debug($comando);
		
			}

		}
	
	}
	
	debug($listado, "green");
	
	function debug($datos , $color = "black"){
	
		echo "<font color='$color'><pre>";
		print_r($datos);
		echo "</pre></font>";
	
	}



	function ListarArchivos($ruta, &$listado){

		$d = dir($ruta);
		
		while (false !== ($entry = $d->read())) {
		   //echo $entry."\n";
		   if($entry != ".." && $entry != "."){
		   
			   if(is_dir($ruta . $entry)){

					ListarArchivos($ruta . $entry . "/", $listado);
			   
			   }
			   else{
			   
					$listado[] = $ruta . $entry;
			   
			   }
		   
		   }
	
		   
		}
		
		$d->close();
		
		return $listado;

	}
?> 
