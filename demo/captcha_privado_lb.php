<?php

	function DibujarLinea($im, $x1 , $y1 , $x2 , $y2 , $color, $tipo = 1){
		
		if($tipo == 1){

			imageline($im, $x1, $y1, $x2, $y2, $color);
			imageline($im, $x1, $y1+1, $x2, $y2+1, $color);
			
		}
		else{
			
			imageline($im, $x1, $y1, $x2, $y2, $color);
			imageline($im, $x1+1, $y1, $x2+1, $y2, $color);
			
		}
		
	}
	function Dibujar($im, $izquierda , $arriba , $ancho , $alto , $color){

		DibujarLinea($im, $izquierda, $arriba, ($izquierda + $ancho), $arriba, $color, "1");
		DibujarLinea($im, $izquierda, $arriba, $izquierda, ($arriba + $alto), $color, "2");
		DibujarLinea($im, $izquierda, ($arriba + $alto), ($izquierda + $ancho), ($arriba + $alto), $color, "1");
		DibujarLinea($im, ($izquierda + $ancho), $arriba, ($izquierda + $ancho), ($arriba + $alto), $color, "2");
		
	}
	
	function GenerarPuntosAzar($im, $color, $ancho, $alto){

		#dibujamos puntos al azar 
		for($i = 10; $i <= 2500; $i+=10){
			$x = rand(1,$alto);
			$y = rand(1,$ancho);
	
			imageline($im, $y, $x, $y, $x, $color[rand(0,(sizeof($color) - 1))]);
	
		}		
		
	}
	
	function GenerarLineaEnCuadro($im, $x, $y, $ancho, $alto, $color){
		
		for($i = 1; $i <= 3; $i++){
			
			if(rand(1,2) == 2){
				
				imageline($im, $x, rand($y, ($y+$alto)), ($x+$ancho), rand($y, $y+$alto), $color[rand(0,(sizeof($color) - 1))]);
				
			}
			else{

				imageline($im, rand($x, ($x+$ancho)), $y, rand($x, ($x+$ancho)), ($y+$alto), $color[rand(0,(sizeof($color) - 1))]);
				
			}
			
		}
	
	}
	
	function GenerarValor($im, $p1, $p2, $p3, $p4, $p5, $p6, $p7, $color, $ancho, $alto, $posicion){

		// Reemplace la ruta por la de su propia fuente
		$fuente = $_SERVER['DOCUMENT_ROOT'] . '/inc/fonts/AustralianFlyingCorpsStencilS.ttf';
		
		switch(rand(1,4)){
			case '1';
				Dibujar($im, $p1, $p2, $p3, $p4, imagecolorallocate($im, 0, 0, 0));
				imagettftext($im, $p5, $p6, $p7, $p4, $color[rand(0,(sizeof($color) - 1))], $fuente, $_SESSION['captcha_sistema_interno'][$posicion]);
				GenerarPuntosAzar($im, $color, $ancho, $alto);
				GenerarLineaEnCuadro($im, $p1, $p2, $p3, $p4, $color);
			break;
			case '2';
				imagettftext($im, $p5, $p6, $p7, $p4, $color[rand(0,(sizeof($color) - 1))], $fuente, $_SESSION['captcha_sistema_interno'][$posicion]);
				Dibujar($im, $p1, $p2, $p3, $p4, imagecolorallocate($im, 0, 0, 0));
				GenerarLineaEnCuadro($im, $p1, $p2, $p3, $p4, $color);
				GenerarPuntosAzar($im, $color, $ancho, $alto);
			break;
			case '3';
				GenerarLineaEnCuadro($im, $p1, $p2, $p3, $p4, $color);
				GenerarPuntosAzar($im, $color, $ancho, $alto);
				Dibujar($im, $p1, $p2, $p3, $p4, imagecolorallocate($im, 0, 0, 0));
				imagettftext($im, $p5, $p6, $p7, $p4, $color[rand(0,(sizeof($color) - 1))], $fuente, $_SESSION['captcha_sistema_interno'][$posicion]);
			break;
			case '4';
				GenerarPuntosAzar($im, $color, $ancho, $alto);
				GenerarLineaEnCuadro($im, $p1, $p2, $p3, $p4, $color);
				imagettftext($im, $p5, $p6, $p7, $p4, $color[rand(0,(sizeof($color) - 1))], $fuente, $_SESSION['captcha_sistema_interno'][$posicion]);
				Dibujar($im, $p1, $p2, $p3, $p4, imagecolorallocate($im, 0, 0, 0));
			break;
			
		}
		
	}

	$no_load = true;
	include_once("inc/conf/inc.boot.php");

	header("Expires: Mon, 26 Jul 1997 05:00:00 GMT"); 
	header("Last-Modified: " . gmdate("D, d M Y H:i:s") . " GMT"); 
	header("Cache-Control: no-store, no-cache, must-revalidate"); 
	header("Cache-Control: post-check=0, pre-check=0", false);
	header("Pragma: no-cache"); 

	$ruta_imagen = $_SERVER['DOCUMENT_ROOT'] . "inc/tpls/default/imagenes/captcha.jpg";

	$ancho = "215";
	$alto = "45";

	//$im = imagecreatefromjpeg($ruta_imagen); 
	$im = imagecreatetruecolor($ancho, $alto);

    $color[] = imagecolorallocate($im, 255, 0, 0);#redpen
    $color[] = imagecolorallocate($im, 0, 153, 0);#greenpen
    $color[] = imagecolorallocate($im, 0, 0, 255);#bluepen
    $color[] = imagecolorallocate($im, 0, 0, 0);#blackpen
	//$color[] = imagecolorallocate($im, 255, 255, 255);#whitekpen
    //$color[] = imagecolorallocate($im, 0, 255, 255);#aquapen
    $color[] = imagecolorallocate($im, 255, 0, 255);#fuschiapen
    #$color[] = imagecolorallocate($im, 153, 153, 153);#greypen
    $color[] = imagecolorallocate($im, 0, 153, 153);#tealpen
    //$color[] = imagecolorallocate($im, 0, 255, 0);#limepen
    $color[] = imagecolorallocate($im, 0, 0, 153);#navypen
    $color[] = imagecolorallocate($im, 153, 0, 153);#purplepen
    $color[] = imagecolorallocate($im, 153, 0, 0);#maroonpen
    #$color[] = imagecolorallocate($im, 153, 153, 0);#olivepen

	imagefilledrectangle($im, 0, 0, ($ancho-1), ($alto-1), imagecolorallocate($im, 255, 255, 255));

	$_SESSION['captcha_sistema_interno'] = strtoupper(substr(str_shuffle("123456789ABCDEFGHJKLNMPQRSUTVXZ"),0, 6));

	// Añadir algo de sombra al texto
	GenerarValor($im, 22, 6, 26, 31, 20, 0, 25, $color, $ancho, $alto, "0");
	GenerarValor($im, 51, 6, 26, 31, 20, 0, 55, $color, $ancho, $alto, "1");
	GenerarValor($im, 80, 6, 26, 31, 20, 0, 84, $color, $ancho, $alto, "2");
	GenerarValor($im, 109, 6, 26, 31, 20, 0, 113, $color, $ancho, $alto, "3");
	GenerarValor($im, 138, 6, 26, 31, 20, 0, 142, $color, $ancho, $alto, "4");
	GenerarValor($im, 167, 6, 26, 31, 20, 0, 171, $color, $ancho, $alto, "5");
	
	header('Content-type: image/jpeg');
	
	imagejpeg($im);
	
	imagedestroy($im);

	
?>