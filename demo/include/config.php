<?php
     error_reporting(1);
	error_reporting(E_ALL & ~E_NOTICE);// & ~E_NOTICE & ~E_WARNING
	session_start();
	
	// Email
	define("FROM_EMAIL", "info@informationmapping.com");
	define("FROM_NAME", "Information Mapping");
	
	//paths of demo appliation
	define('SITE_URL',  '/demo/');
	define('SITE_PATH', $_SERVER["DOCUMENT_ROOT"] . SITE_URL);
	define('ADMIN_URL', SITE_URL . 'admin/');
	define('ADMIN_PATH', $_SERVER["DOCUMENT_ROOT"] . ADMIN_URL);
	define('VIEW_URL', SITE_URL . 'view/');
	define('VIEW_PATH', SITE_PATH . 'view/');
	define('CSS_PATH', VIEW_URL . 'css/');
	define('JS_PATH', VIEW_URL . 'js/');
	define('LANGUAGE_PATH', SITE_PATH.'languages/');
	define('ADMIN_LANGUAGE_PATH', LANGUAGE_PATH.'admin/');
	define('INCLUDE_PATH', SITE_PATH.'include/');
	define('COMMON_CORE_PATH', SITE_PATH. 'core_application/');
	define('COMMON_CORE_MODEL', SITE_PATH . 'model/');
	define('COMMON_CORE_COMPONENT', SITE_PATH . 'component/');
	define('COMMON_CORE_PHPMAILER', SITE_PATH . 'phpmailer/');
    define('ADMIN_CONTROLLER_PATH', ADMIN_PATH . 'controller/');
	define('ADMIN_VIEW_PATH', VIEW_PATH.'admin/');
	define('COMMON_JS_PATH', 'common/');
	define('UPLOADED_STUFF_PATH', SITE_PATH . 'uploads/');
	define('TMP_PATH', SITE_PATH . '/temp/');
	define("ADMIN_IMAGE_URL",SITE_URL."images/");

	define('LIBRARIES_URL',SITE_URL.'libraries/');
	define('LIBRARIES_PATH',SITE_PATH.'libraries/');
	define("FCK_EDITOR_PATH",LIBRARIES_PATH."fckeditor/");
	define("FCK_EDITOR_URL",LIBRARIES_URL."fckeditor/");
	define("MPDF_PATH",$_SERVER["DOCUMENT_ROOT"].LIBRARIES."/mpdf/mpdf.php");

	//defines the database username and password.
	define("DB_HOST","localhost");
    define("DB_USER_NAME","prod_demo");
    define("DB_PASSWORD","AGWTTJx3hqX22JPG");
	define("DB_NAME","imi_prod_demo");
	define("ADMIN_PAGGING_STYLE","3");
	define("REQUIRED_FIELD","<label class=\"required\">*</label>");

   /*  for FRONT */
   	define('FRONT_URL', SITE_URL . 'front/');
	define('FRONT_PATH', $_SERVER["DOCUMENT_ROOT"] . FRONT_URL);
	define('FRONT_VIEW_PATH', VIEW_PATH.'front/');
	define('FRONT_CONTROLLER_PATH', FRONT_PATH.'controller/');
	define("FRONT_IMAGE_URL",SITE_URL."images/");
    define("FRONT_PAGGING_STYLE","3");
	define('FRONT_LANGUAGE_PATH', LANGUAGE_PATH.'front/');
    define("IMAGE_EXTENSIONS", "jpg,gif,png");
    define('IMAGE_RESIZE_PATH', '/images/');
	define('UPLOAD_PATH', SITE_PATH . 'uploads/');
	define('UPLOAD_URL',  '/uploads/');
	define("DATE_FORMAT","Y-m-d");
	define("DATE_FORMAT_WHOLE","Y-m-d H:i:s");
    define("FILE_IMAGE_PATH", UPLOAD_PATH . "file/");
    define("FILE_IMAGEMAP_PATH", UPLOAD_PATH . "mapfile/");
    define("LOGO_PATH", UPLOAD_PATH . "logo/");
    define("FILE_EXTENSIONS", "html,htm");
	define("MYSQL_DATE_FORMAT","%Y-%m-%d");
	define("MYSQL_DATE_FORMAT_WHOLE","%Y-%m-%d %H:%i:%s");

    define("ALLOWED_UPLOAD_FILE_TYPES","");

    // User types
    define("ADMIN_ID","1");
    define("MANAGER_ID","2");
    define("PUBLISHER_ID","3");

    //Commission types
    define("CPA_ID","1");
    define("REVSHARE_ID","2");
    define("HYBRID_ID","3");
    define("CPL_ID","4");

?>