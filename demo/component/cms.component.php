<?php

	#============================================================================================================
    	#	Created By  			: 
    	#	Created Date			: 22-03-2011
    	#	Purpose					: For Handling  cms
    	#	includes / Obj(Req)		: Files:
    	#	Last update date		: 22-03-2011
    	#	Update Purpose			: For generation
    #============================================================================================================
    class cms_component
    {
      	public function __construct()
      	{
      		global $commonFunction, $dbAccess;
			$this->db = $dbAccess;
            $this->commonFunction = $commonFunction;
      	}
		public function send_mail_cms($receipient_name, $receipient_email, $mail_vars)
		{
			$vars_mail['recipient_name'] = $receipient_name;
			$vars_mail['recipient_email'] = $receipient_email;
			$vars_mail['mail_vars'] = $mail_vars;
			$this->commonFunction->mailing('cms_created', $vars_mail);
		}
	}

?>