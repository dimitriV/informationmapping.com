<?php
$title="Language Variable";
$heading="Add/Edit Language Variable";
include_once(FCK_EDITOR_PATH."fckeditor.php");
require_once(COMMON_CORE_MODEL."language_var.class.php");
$language_var = new language_var();

foreach($language_var as $k=>$d)
{
	if($k!='db')
	{
		$language_var->$k =(isset($_REQUEST[$k]) && $_REQUEST[$k] != '')?$_REQUEST[$k]:$language_var->$k;
	}
}

$language_var->id =(isset($_REQUEST['id']) && $_REQUEST['id'] != '')?$_REQUEST['id']:$language_var->id;
$action = isset($_REQUEST['action'])?$_REQUEST['action']:'';
///******* Assigning all posted data in local object *********//

///******* Insert / Update Case *********//
if($_POST['LanguageVarFormSubmit'])
{   
	$validateData['required'] = array('name'=>'Language Var Name is Required');

	$errorMsg = $commonFunction->validate_form($_POST, $validateData);
	if($language_var->checkexist() != false){
		$errorMsg[] = "Language Var Name already exist.";
	}
	$obj_language_var = (array) $language_var;
	if(count($errorMsg) == 0)
    {
		if($language_var->insertUpdate($language_var->id)==false)
		{
			$errorMsg[] = $language_var->error_message;
		}
		else
		{
			if(!isset($language_var->id))
			{
				$commonFunction->Redirect('./?page=language_var_list&action=view&msg=1');
				exit;
			}
			else
			{
				$commonFunction->Redirect('./?page=language_var_list&action=view&msg=2');
				exit;
			}
		}
	}
}
///******* Insert / Update Case *********//

///******* $objcontact_method Case *********//
elseif($language_var->id!='' && $action=='edit')
{
	$language_var->selectById();
	$obj_language_var = (array) $language_var;
}
///******* Edit Case *********//
//print "<pre>";  print_r($obj_language_var);die;
?>