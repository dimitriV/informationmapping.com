<?php

/****** Including Language File ********/

    $lang = (isset($_SESSION['domain_name']))?$_SESSION['domain_name']:'eng';
    if(!file_exists(FRONT_LANGUAGE_PATH.$lang.'/index.php'))
	{
	   	$lang = 'eng';
	}
      if(!isset($_SESSION['domain_id']))
      {
         $_SESSION['domain_id']=1;
      }

   // echo $lang; exit;
    //$lang_folder = 'eng';
	include_once(FRONT_LANGUAGE_PATH.$lang.'/index.php');
    /****** Including Language File ********/

	/****** Including Controller File ********/
	$page = (isset($_REQUEST['page']) && $_REQUEST['page']!='')?$_REQUEST['page']:'select_language';
	if(!file_exists(FRONT_CONTROLLER_PATH . $page . '.php'))
	{
  		$page = 'select_language';
	}
	include(FRONT_CONTROLLER_PATH . $page . '.php');
	/****** Including Controller File ********/

	/****** Including Master Page File ********/
     $page_array = array('get_address');
    if (!in_array($page, $page_array))
	include_once(FRONT_VIEW_PATH."master_page.php");

?>