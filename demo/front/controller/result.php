<?php
    if(isset($_REQUEST['retry']))
    {
        unset($_SESSION);
        $commonFunction->Redirect('./?page=select_language');
        exit;
    }
    require_once(COMMON_CORE_MODEL."cms.class.php");
    $objCms = new cms();
    $objCms->cms_id=(isset($_SESSION['doc_id']))?$_SESSION['doc_id']:'1';
    $objCms->selectByFrontId();
    $filename = FILE_IMAGE_PATH. $objCms->files[$_SESSION['domain_id']];
    //echo $filename; exit;
    $handle = fopen($filename, "r");
    $contents = fread($handle, filesize($filename));

    fclose($handle);
    //print_r($contents);
    $content1= explode("<style>",$contents);
    $content2= explode("</style>",$content1[1]);
     $contents= str_replace("<style>".$content2[0]."</style>" ," ",$contents);


    $filenamemap = FILE_IMAGEMAP_PATH. $objCms->mapfiles[$_SESSION['domain_id']];
    $handlemap = fopen($filenamemap, "r");
    $contentsmap = fread($handlemap, filesize($filenamemap));
    fclose($handlemap);
    $content1= explode("<style>",$contentsmap);
    $content2= explode("</style>",$content1[1]);
     $contentsmap= str_replace("<style>".$content2[0]."</style>" ," ",$contentsmap);


    $oldtime= explode(":",$_SESSION['doc_time']);
    $newtime= explode(":",$_SESSION['mapdoc_time']);
    $oldtime_value= $oldtime[0]*6000+ $oldtime[1]*100+$oldtime[2];
    $newtime_value= $newtime[0]*6000+ $newtime[1]*100+$newtime[2];
    $resulttime= $oldtime_value-$newtime_value;
   // echo abs($resulttime)."<br>";
    $min= abs($resulttime/6000);
    $min= floor($min);

    $sec= abs((abs($resulttime)-($min*6000))/100);
      $sec = floor($sec);

    //$milli= abs($resulttime-$min*6000-$sec*100) ;

    /*$sec = abs(($resulttime / 100) % 60);
    $sec = floor($sec);
     echo $sec;
    $min = abs(($resulttime / 100) / 60);
     if($min<1)
    {
      $min=0;
    }*/
     $milli= abs($resulttime)-$min*6000-$sec*100;
    if($min<10)
    {
      $min="0".$min;
    }
    if($sec<10)
    {
      $sec="0".$sec;
    }
    if($milli<10)
    {
      $milli="0".$milli;
    }
    if($resulttime<0)
     $result_display= "-".$min.":".$sec.".".$milli;
     else
     $result_display= $min.":".$sec.".".$milli;
	 
	 // added by Wim Van Dessel
	 // opslaan van land van herkomst
$host = gethostbyaddr($_SERVER['REMOTE_ADDR']);
$land_expl = explode(".",$host);

// neem het laatste deel van de providerstring, dit deel bevat de naam van het land
$land= $land_expl[count($land_expl)-1];

// omvormen van providers naar originele schrijfwijze

$landcodes = array( 
// Niet-gesponsorde TLD's 
'com'=>'Commercieel (.com)', 
'net'=>'Netwerk (.net)', 
'org'=>'Non-profit organisatie (.org)', 
'edu'=>'Amerikaanse instellingen (.edu)', 
'gov'=>'Amerikaanse instellingen (.gov)', 
'mil'=>'Amerikaanse krijgsmacht (.mil)', 
'int'=>'Internationale organisaties (.int)', 
'biz'=>'Zaken (.biz)', 
'info'=>'Informatief (.info)', 
'name'=>'Individueel persoon (.name)', 
'pro'=>'Beroepen (.pro)', 

// Gesponsorde TLD's 
'aero'=>'Luchttransport-gerelateerd (.aero)', 
'coop'=>'Co&ouml;peratief (.coop)', 
'museum'=>'Musea (.museum)', 
'jobs'=>'Personeelszaken (.jobs)', 
'travel'=>'Reisindustrie (.travel)', 

// testplatform
'localhost'=>'Localhost',

// Landelijke TLD's 
'ac'=>'Ascension', 
'ad'=>'Andorra', 
'ae'=>'Verenigde Arabische Emiraten', 
'af'=>'Afghanistan', 
'ag'=>'Antigua en Barbuda', 
'ai'=>'Anguilla', 
'al'=>'Albani&euml;', 
'am'=>'Armeni&euml;', 
'an'=>'Nederlandse Antillen', 
'ao'=>'Angola', 
'aq'=>'Antarctica', 
'ar'=>'Argentini&euml;', 
'as'=>'Amerikaans Samoa', 
'at'=>'Oostenrijk', 
'au'=>'Australi&euml;', 
'aw'=>'Aruba', 
'az'=>'Azerbeidzjan', 
'ba'=>'Bosni&euml;-Herzegovina', 
'bb'=>'Barbados', 
'bd'=>'Bangladesh', 
'be'=>'Belgi&euml;', 
'bf'=>'Burkina Faso', 
'bg'=>'Bulgarije', 
'bh'=>'Bahrein', 
'bi'=>'Burundi', 
'bj'=>'Benin', 
'bm'=>'Bermuda', 
'bn'=>'Brunei', 
'bo'=>'Bolivia', 
'br'=>'Brazili&euml;', 
'bs'=>'Bahama\'s', 
'bt'=>'Bhutan', 
'bv'=>'Bouvet', 
'bw'=>'Botswana', 
'by'=>'Wit-Rusland', 
'bz'=>'Belize', 
'ca'=>'Canada', 
'cc'=>'Cocos (Keeling) Eilanden', 
'cd'=>'Democratische Republiek Congo', 
'cf'=>'Centraal-Afrikaanse Republiek', 
'cg'=>'Republiek Congo', 
'ch'=>'Zwitserland', 
'ci'=>'Ivoorkust', 
'ck'=>'Cookeilanden', 
'cl'=>'Chili', 
'cm'=>'Kameroen', 
'cn'=>'China', 
'co'=>'Colombia', 
'cr'=>'Costa Rica', 
'cu'=>'Cuba', 
'cv'=>'Kaapverdi&euml;', 
'cx'=>'Christmaseiland', 
'cy'=>'Cyprus', 
'cz'=>'Tsjechi&euml;', 
'dd'=>'DDR', 
'de'=>'Duitsland', 
'dj'=>'Djibouti', 
'dk'=>'Denemarken', 
'dm'=>'Dominica', 
'do'=>'Dominicaanse Republiek', 
'dz'=>'Algerije', 
'ec'=>'Ecuador', 
'ee'=>'Estland', 
'eg'=>'Egypte', 
'eh'=>'Westelijke Sahara', 
'er'=>'Eritrea', 
'es'=>'Spanje', 
'et'=>'Ethiopi&euml;', 
'eu'=>'Europese Unie', 
'fi'=>'Finland', 
'fj'=>'Fiji', 
'fk'=>'Falklandeilanden', 
'fm'=>'Micronesi&euml;', 
'fo'=>'Faer&ouml;er-eilanden', 
'fr'=>'Frankrijk', 
'ga'=>'Gabon', 
'gd'=>'Grenada', 
'ge'=>'Georgi&euml;', 
'gf'=>'Frans-Guyana', 
'gg'=>'Guernsey', 
'gh'=>'Ghana', 
'gi'=>'Gibraltar', 
'gl'=>'Groenland', 
'gm'=>'Gambia', 
'gn'=>'Guinea', 
'gp'=>'Guadeloupe', 
'gq'=>'Equatoriaal Guinea', 
'gr'=>'Griekenland', 
'gs'=>'Zuid-Georgi&euml; en de Zuidelijke Sandwicheilanden', 
'gt'=>'Guatemala', 
'gu'=>'Guam', 
'gw'=>'Guinee-Bissau', 
'gy'=>'Guyana', 
'hk'=>'Hongkong', 
'hm'=>'Heard- en McDonaldeilanden', 
'hn'=>'Honduras', 
'hr'=>'Kroati&euml;', 
'ht'=>'Ha&iuml;ti', 
'hu'=>'Hongarije', 
'id'=>'Indonesi&euml;', 
'ie'=>'Ierland', 
'il'=>'Isra&euml;l', 
'im'=>'Isle of Man', 
'in'=>'India', 
'io'=>'Brits Indische Oceaan Territorium', 
'iq'=>'Irak', 
'ir'=>'Iran', 
'is'=>'IJsland', 
'it'=>'Itali&euml;', 
'je'=>'Jersey', 
'jm'=>'Jamaica', 
'jo'=>'Jordani&euml;', 
'jp'=>'Japan', 
'ke'=>'Kenya', 
'kg'=>'Kirgizi&euml;', 
'kh'=>'Cambodja', 
'ki'=>'Kiribati', 
'km'=>'Comoren', 
'kn'=>'Saint Kitts en Nevis', 
'kp'=>'Noord-Korea', 
'kr'=>'Zuid-Korea', 
'kw'=>'Koeweit', 
'ky'=>'Cayman Islands', 
'kz'=>'Kazachstan', 
'la'=>'Laos', 
'lb'=>'Libanon', 
'lc'=>'Saint Lucia', 
'li'=>'Liechtenstein', 
'lk'=>'Sri Lanka', 
'lr'=>'Liberia', 
'ls'=>'Lesotho', 
'lt'=>'Litouwen', 
'lu'=>'Luxemburg', 
'lv'=>'Letland', 
'ly'=>'Libi&euml;', 
'ma'=>'Marokko', 
'mc'=>'Monaco', 
'md'=>'Moldavi&euml;', 
'mg'=>'Madagaskar', 
'mh'=>'Marshalleilanden', 
'mk'=>'Macedoni&euml;', 
'ml'=>'Mali', 
'mm'=>'Myanmar', 
'mn'=>'Mongoli&euml;', 
'mo'=>'Macau', 
'mp'=>'Noordelijke Marianen', 
'mq'=>'Martinique', 
'mr'=>'Mauritani&euml;', 
'ms'=>'Montserrat', 
'mt'=>'Malta', 
'mu'=>'Mauritius', 
'mv'=>'Maldiven', 
'mw'=>'Malawi', 
'mx'=>'Mexico', 
'my'=>'Maleisi&euml;', 
'mz'=>'Mozambique', 
'na'=>'Namibi&euml;', 
'nc'=>'Nieuw-Caledoni&euml;', 
'ne'=>'Republiek Niger', 
'nf'=>'Norfolk Island', 
'ng'=>'Nigeria', 
'ni'=>'Nicaragua', 
'nl'=>'Nederland', 
'no'=>'Noorwegen', 
'np'=>'Nepal', 
'nr'=>'Nauru', 
'nu'=>'Niue', 
'nz'=>'Nieuw Zeeland', 
'om'=>'Oman', 
'pa'=>'Panama', 
'pe'=>'Peru', 
'pf'=>'Frans Polynesi&euml;', 
'pg'=>'Papoea-Nieuw-Guinea', 
'ph'=>'Filippijnen', 
'pk'=>'Pakistan', 
'pl'=>'Polen', 
'pm'=>'Saint-Pierre en Miquelon', 
'pn'=>'Pitcairn', 
'pr'=>'Puerto Rico', 
'ps'=>'Palestijnse Gebieden', 
'pt'=>'Portugal', 
'pw'=>'Palau', 
'py'=>'Paraguay', 
'qa'=>'Qatar', 
're'=>'R&eacute;union', 
'ro'=>'Roemeni&euml;', 
'ru'=>'Rusland', 
'rw'=>'Rwanda', 
'sa'=>'Saudi-Arabi&euml;', 
'sb'=>'Solomoneilanden', 
'sc'=>'Seychellen', 
'sd'=>'Sudan', 
'se'=>'Zweden', 
'sg'=>'Singapore', 
'sh'=>'St. Helena', 
'si'=>'Sloveni&euml;', 
'sj'=>'Svalbard en Jan Mayen', 
'sk'=>'Slowakije', 
'sl'=>'Sierra Leone', 
'sm'=>'San Marino', 
'sn'=>'Senegal', 
'so'=>'Somali&euml;', 
'sr'=>'Suriname', 
'st'=>'S&atilde;o Tom&eacute; en Principe', 
'su'=>'Sovjet-Unie', 
'sv'=>'El Salvador', 
'sy'=>'Syri&euml;', 
'sz'=>'Swaziland', 
'tc'=>'Turks- en Caicoseilanden', 
'td'=>'Tsjaad', 
'tf'=>'Franse Zuidelijke Gebieden', 
'tg'=>'Togo', 
'th'=>'Thailand', 
'tj'=>'Tadzjikistan', 
'tk'=>'Tokelau', 
'tm'=>'Turkmenistan', 
'tn'=>'Tunesi&euml;', 
'to'=>'Tonga', 
'tp'=>'Oost-Timor', 
'tr'=>'Turkije', 
'tt'=>'Trinidad en Tobago', 
'tv'=>'Tuvalu', 
'tw'=>'Taiwan', 
'tz'=>'Tanzania', 
'ua'=>'Oekra&iuml;ne', 
'ug'=>'Uganda', 
'uk'=>'Verenigd Koninkrijk', 
'um'=>'Kleine Pacifische eilanden van de Verenigde Staten', 
'us'=>'Verenigde Staten', 
'uy'=>'Uruguay', 
'uz'=>'Oezbekistan', 
'va'=>'Vaticaanstad', 
'vc'=>'Saint Vincent en de Grenadines', 
've'=>'Venezuela', 
'vg'=>'Britse Maagdeneilanden', 
'vi'=>'Amerikaanse Maagdeneilanden', 
'vn'=>'Vietnam', 
'vu'=>'Vanuatu', 
'wf'=>'Wallis en Futuna', 
'ws'=>'West-Samoa', 
'ye'=>'Jemen', 
'yt'=>'Mayotte', 
'yu'=>'Joegoslavi&euml;', 
'za'=>'Zuid-Afrika', 
'zm'=>'Zambia', 
'zr'=>'Za&iuml;re', 
'zw'=>'Zimbabwe'
);

if ( !empty($landcodes[$land]) ) { 
   $land = $landcodes[$land]; 
} 
else { 
   $land = 'Onbekend'; 
}
	 
	 $result_wim = mysql_query("INSERT INTO webapp_results (result,date,country) VALUES ('$result_display',NOW(),'$land')");
	 
	 // end added by Wim Van Dessel


      if($resulttime<=0)
        {
             $commonFunction->Redirect('./?page=congrats');
             exit;
        }


      if(isset($_SESSION['resulttime']))
      {
        unset($_SESSION['resulttime']);
      }
      $_SESSION['resulttime']= $resulttime;
      $_SESSION['result_display_time']= $result_display;


?>