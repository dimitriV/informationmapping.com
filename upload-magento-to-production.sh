#!/bin/bash

rsync -rltz --progress \
--exclude='- /includes/src/*' \
--exclude='- /install.php' \
--exclude='- /app/etc/local.xml' \
--exclude='- /php.ini.sample' \
--exclude='- /*.sh' \
--exclude='- */test/*' \
--exclude='- /nbproject/' \
--exclude='- *.log' \
--exclude='- .*' \
--exclude='- /downloader/' \
--exclude='- _notes' \
--exclude='- /media/' \
--exclude='- /maintenance.flag' \
--exclude='- /_maintenance.flag' \
--exclude='- /app/design/install/' \
--exclude='- /var/' \
--exclude='- /skin/install/' \
--exclude='- /pkginfo/' \
--rsh "ssh" \
--rsync-path "rsync" /Users/wouter/Business/Workspace/imi/magento/* root@87.238.165.25:/var/www/html/www.informationmapping.com/magento
