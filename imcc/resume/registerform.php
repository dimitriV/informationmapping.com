<h1>Register for the Information Mapping Certified Consultant Program</h1>
  <p>Please complete the form below.</p>
<?php if ($registerError=="1"): ?>
<p class="error">The two passwords are not identical.</p>
<?php endif; ?>
<?php if ($registerError=="2"): ?>
<p class="error">Invalid e-mail address</p>
<?php endif; ?>
<?php if ($registerError=="3"): ?>
<p class="error">The username already exists. Please use another one.</p>
<?php endif; ?>
<?php if ($registerError=="4"): ?>
<p class="error">Please fill out all fields.</p>
<?php endif; ?>
  <form method="post" action="register.php">
  <fieldset>
  <ol>
  <li><label for="firstname">First name</label> <input id="firstname" type="text" name="firstname" maxlength="50" size="30" value="<?php if(!empty($_POST['firstname'])) { echo $_POST['firstname']; } ?>" /></li>
  <li><label for="lastname">Last name</label> <input id="lastname" type="text" name="lastname" maxlength="50" size="30" value="<?php if(!empty($_POST['lastname'])) { echo $_POST['lastname']; } ?>" /></li>
  <li><label for="user">User name</label> <input id="user" type="text" name="user" maxlength="50" size="30" value="<?php if(!empty($_POST['user'])) { echo $_POST['user']; } ?>" /></li>
  <li><label for="pass1">Password</label><input id="pass1" type="password" name="pass1" size="30" value="<?php if(!empty($_POST['pass1'])) { echo $_POST['pass1']; } ?>" /></li>
  <li><label for="pass2">Repeat password</label><input id="pass2" type="password" name="pass2" size="30" value="<?php if(!empty($_POST['pass2'])) { echo $_POST['pass2']; } ?>" /></li>
  <li><label for="mail">E-mail address</label> <input id="mail" type="text" name="email" maxlength="100" size="30" value="<?php if(!empty($_POST['email'])) { echo $_POST['email']; } ?>" /></li>
  <li><label for="key">Access key</label> <input id="key" type="text" name="key" maxlength="100" size="30" value="<?php if(!empty($_POST['key'])) { echo $_POST['key']; } ?>" /></li>
  </ol>
  </fieldset>
  <fieldset class="submit">
  <ol>
  <li><input type="submit" name="submit" value="Register" /></li>
  </ol>
  </fieldset>
  </form>