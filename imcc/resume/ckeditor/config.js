﻿/*
Copyright (c) 2003-2010, CKSource - Frederico Knabben. All rights reserved.
For licensing, see LICENSE.html or http://ckeditor.com/license
*/

CKEDITOR.editorConfig = function( config )
{
	config.language = 'en';
	config.uiColor = '#eeeeee';
	config.width = 400;
    config.toolbar = 'MyToolbar';

    config.toolbar_MyToolbar =
    [
        ['Cut','Copy','PasteText'],
        ['Bold','Italic'],
        ['NumberedList','BulletedList','-','Outdent','Indent'],
        ['Link','Unlink'],

    ];
};

