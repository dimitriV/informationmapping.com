<?php
include_once("includes/config.php");
include_once("includes/connect.php");
include("includes/header.php");

// eerst variabelen, dan errorchack
$program = "Information Mapping Certified Consultant Program";
$firstname = htmlspecialchars($_POST['firstname'], ENT_QUOTES);
$lastname = htmlspecialchars($_POST['lastname'], ENT_QUOTES);
$email = htmlspecialchars($_POST['email'], ENT_QUOTES);
$address1 = htmlspecialchars($_POST['address1'], ENT_QUOTES);
$address2 = htmlspecialchars($_POST['address2'], ENT_QUOTES);
$city = htmlspecialchars($_POST['city'], ENT_QUOTES);
$postalcode = htmlspecialchars($_POST['postalcode'], ENT_QUOTES);
$state = htmlspecialchars($_POST['state'], ENT_QUOTES);
$country = htmlspecialchars($_POST['country'], ENT_QUOTES);
$telephone = htmlspecialchars($_POST['telephone'], ENT_QUOTES);
$website = htmlspecialchars($_POST['website'], ENT_QUOTES);

$profsummary = htmlspecialchars($_POST['profsummary'], ENT_QUOTES);
$profsummary=nl2br($profsummary);
$specialties = htmlspecialchars($_POST['specialties'], ENT_QUOTES);
$title_job0 = htmlspecialchars($_POST['title_job0'], ENT_QUOTES);
$company_job0 = htmlspecialchars($_POST['company_job0'], ENT_QUOTES);
$startmonth_job0 = htmlspecialchars($_POST['startmonth_job0'], ENT_QUOTES);
$startyear_job0 = htmlspecialchars($_POST['startyear_job0'], ENT_QUOTES);
$description_job0 = htmlspecialchars($_POST['description_job0'], ENT_QUOTES);
$description_job0=nl2br($description_job0);
$title_job1 = htmlspecialchars($_POST['title_job1'], ENT_QUOTES);
$company_job1 = htmlspecialchars($_POST['company_job1'], ENT_QUOTES);
$startmonth_job1 = htmlspecialchars($_POST['startmonth_job1'], ENT_QUOTES);
$startyear_job1 = htmlspecialchars($_POST['startyear_job1'], ENT_QUOTES);
$endmonth_job1 = htmlspecialchars($_POST['endmonth_job1'], ENT_QUOTES);
$endyear_job1 = htmlspecialchars($_POST['endyear_job1'], ENT_QUOTES);
$description_job1 = htmlspecialchars($_POST['description_job1'], ENT_QUOTES);
$description_job1=nl2br($description_job1);

$followedtraining = htmlspecialchars($_POST['followedtraining']);
$coursetitle_imap = htmlspecialchars($_POST['coursetitle_imap'], ENT_QUOTES);
$company_imap = htmlspecialchars($_POST['company_imap'], ENT_QUOTES);
$month_imap = htmlspecialchars($_POST['month_imap'], ENT_QUOTES);
$year_imap = htmlspecialchars($_POST['year_imap'], ENT_QUOTES);
$trainer_imap = htmlspecialchars($_POST['trainer_imap'], ENT_QUOTES);
$comments_imap = htmlspecialchars($_POST['comments_imap'], ENT_QUOTES);
$comments_imap=nl2br($comments_imap);
$coursetitle0 = htmlspecialchars($_POST['coursetitle0'], ENT_QUOTES);
$company0 = htmlspecialchars($_POST['company0'], ENT_QUOTES);
$month0 = htmlspecialchars($_POST['month0'], ENT_QUOTES);
$year0 = htmlspecialchars($_POST['year0'], ENT_QUOTES);
$comments0 = htmlspecialchars($_POST['comments0'], ENT_QUOTES);
$comments0=nl2br($comments0);
$coursetitle1 = htmlspecialchars($_POST['coursetitle1'], ENT_QUOTES);
$company1 = htmlspecialchars($_POST['company1'], ENT_QUOTES);
$month1 = htmlspecialchars($_POST['month1'], ENT_QUOTES);
$year1 = htmlspecialchars($_POST['year1'], ENT_QUOTES);
$comments1 = htmlspecialchars($_POST['comments1'], ENT_QUOTES);
$comments1=nl2br($comments1);

if($_FILES['certificate_imap']['size']>0){

    $source=$_FILES['certificate_imap']['tmp_name'];
    $certificate_name=time() . $_FILES['certificate_imap']['name'];
    $dest="certificates/" . $certificate_name;
    $check1=copy($source,$dest);
}

// errorcheck

function check_email($email) {
    $pattern = "/^[\w-]+(\.[\w-]+)*@";
    $pattern .= "([0-9a-z][0-9a-z-]*[0-9a-z]\.)+([a-z]{2,4})$/i";
    if (preg_match($pattern, $email)) {
        $parts = explode("@", $email);
        if (checkdnsrr($parts[1], "MX")){
            // echo "The e-mail address is valid."; 
            return true;
        } else {
            return false;
        }
    } else {
        return false;
    }
}

if(empty($firstname)) {
    $error[1] = "<br><span class=\"error\">Please fill out your first name.</span>";
}

if(empty($lastname)) {
    $error[2] = "<br><span class=\"error\">Please fill out your last name.</span>";
}

if(!check_email($email)) {
    $error[3] = "<br><span class=\"error\">Please fill out a valid e-mail address.</span>";
}

if(empty($address1)) {
    $error[4] = "<br><span class=\"error\">Please fill out your address.</span>";
}

if(empty($city)) {
    $error[5] = "<br><span class=\"error\">Please fill out your city.</span>";
}

if(empty($postalcode)) {
    $error[6] = "<br><span class=\"error\">Please fill out your postal code / ZIP.</span>";
}

if($country=="") {
    $error[7] = "<br><span class=\"error\">Please select your country.</span>";
}

if(($country=="US") && empty($postalcode)) {
    $error[8] = "<br><span class=\"error\">Please fill out your state.</span>";
}

if(empty($profsummary)) {
    $error[9] = "<br><span class=\"error\">Please summarize your professional experience.</span>";
}

if(empty($specialties)) {
    $error[10] = "<br><span class=\"error\">Please fill out your specialties.</span>";
}

if(empty($company_job0)) {
    $error[11] = "<br><span class=\"error\">Please fill out your company.</span>";
}

if(empty($title_job0)) {
    $error[12] = "<br><span class=\"error\">Please fill out your position.</span>";
}

if($startmonth_job0=="- Month -") {
    $error[13] = "<br><span class=\"error\">Please select a month.</span>";
}


if($startyear_job0=="- Year -") {
    $error[14] = "<br><span class=\"error\">Please select a year.</span>";
}

if(empty($description_job0)) {
    $error[15] = "<br><span class=\"error\">Please provide a short description of your current job.</span>";
}

$resume_SQL="SELECT email FROM resume";
$resume_result=mysql_query($resume_SQL) or die(mysql_error());
while($resume=mysql_fetch_array($resume_result)){

    if($email==$resume[email]) {
        $error[16] = "<br><span class=\"error\">This e-mail address already exists in our database.</span>";
    }
}

// maximale bestandsgrootte in bytes
$allowed_size = 2000*1024; // Grootte in bytes > 2000*1024 = 2000 kB


$size = $_FILES['certificate_imap']['size'];


if(!empty($certificate_name))
{
    // Bestandsgrootte controleren
    if($size > $allowed_size)
        $error[17] = "<br><span class=\"error\">The certificate can be max. 2 MB</span>";

    // Extensie en mime controleren
    if ($_FILES['certificate_imap']['type'] != "application/pdf")
        $error[18] = "<br><span class=\"error\">Please upload your certificate in PDF format</span>";
}

if(empty($telephone)) {
    $error[19] = "<br><span class=\"error\">Please fill out your telephone number.</span>";
}

if(empty($website)) {
    $error[20] = "<br><span class=\"error\">Please fill out the URL of your company's website.</span>";
}

if($program=="- Select your program -") {
    $error[21] = "<br><span class=\"error\">Please select a program.</span>";
}


/*
if(!empty($company_job1) || !empty($title_job1)) {
	
	if(empty($company_job1)) {
	  $error[16] = "<br><span class=\"error\">Please fill out your company.</span>";
	}
	
	if(empty($title_job1)) {
	  $error[17] = "<br><span class=\"error\">Please fill out your position.</span>";
	}
	
	if($startmonth_job1=="- Month -") {
	  $error[18] = "<br><span class=\"error\">Please select a month.</span>";
	}
	
	if($startyear_job1=="- Year -") {
	  $error[19] = "<br><span class=\"error\">Please select a year.</span>";
	}
	
	if($endmonth_job1=="- Month -") {
	  $error[20] = "<br><span class=\"error\">Please select a month.</span>";
	}
	
	if($endyear_job1=="- Year -") {
	  $error[21] = "<br><span class=\"error\">Please select a year.</span>";
	}
	
	if(empty($description_job1)) {
	  $error[22] = "<br><span class=\"error\">Please provide a short description of your current job.</span>";
	}

}


	if(empty($coursetitle_imap)) {
	  $error[23] = "<br><span class=\"error\">Please fill out the name of the course you followed.</span>";
	}
	
	if(empty($company_imap)) {
	  $error[24] = "<br><span class=\"error\">Please fill out the company that provided the training.</span>";
	}
	
	if($month_imap=="- Month -") {
	  $error[25] = "<br><span class=\"error\">Please select a month.</span>";
	}
	
	
	if($year_imap=="- Year -") {
	  $error[26] = "<br><span class=\"error\">Please select a year.</span>";
	}
	
	if(empty($trainer_imap)) {
	  $error[27] = "<br><span class=\"error\">Please fill out the name of the instructor.</span>";
	}
	
	if(empty($certificate_name)) {
		$error[28] = "<br><span class=\"error\">Please upload your certificate.</span>";
	}
	
*/

require_once('recaptchalib.php');
$privatekey = "6Lf83uYSAAAAAB54WFqK9ZlVJtgiEeY0PLtgYGKl";
$resp = recaptcha_check_answer ($privatekey,
    $_SERVER["REMOTE_ADDR"],
    $_POST["recaptcha_challenge_field"],
    $_POST["recaptcha_response_field"]);

if (!$resp->is_valid) {
    // What happens when the CAPTCHA was entered incorrectly
    $error[99] = "<span class=\"error\">The reCAPTCHA wasn't entered correctly. Go back and try it again.</span>";
}

// indien alles ok, dan toevoegen aan DB
if(isset($_POST['submit']) && !isset($error)) {

    $resume_SQL_insert="INSERT INTO resume (program, telephone, website,resumedate,firstname,lastname,email,address1,address2,city,postalcode,state,country,profsummary,specialties,title_job0,company_job0,startmonth_job0,startyear_job0,description_job0,title_job1,company_job1,startmonth_job1,startyear_job1,endmonth_job1,endyear_job1,description_job1,followedtraining,coursetitle_imap,company_imap,month_imap,year_imap,trainer_imap,certificate_imap,comments_imap,coursetitle0,company0,month0,year0,comments0,coursetitle1,company1,month1,year1,comments1) VALUES ('$program', '$telephone','$website',NOW(),'$firstname','$lastname','$email','$address1','$address2','$city','$postalcode','$state','$country','$profsummary','$specialties','$title_job0','$company_job0','$startmonth_job0','$startyear_job0','$description_job0','$title_job1','$company_job1','$startmonth_job1','$startyear_job1','$endmonth_job1','$endyear_job1','$description_job1','$followedtraining','$coursetitle_imap','$company_imap','$month_imap','$year_imap','$trainer_imap','$certificate_name','$comments_imap','$coursetitle0','$company0','$month0','$year0','$comments0','$coursetitle1','$company1','$month1','$year1','$comments1')";
    $bool=mysql_query($resume_SQL_insert);
    if($bool<>1) echo "There was an error adding the resume to the database.";
    if($bool==1) {

        ?>

        <h1><?php echo $program; ?></h1>
        <div id="content">
            <h2>Thank You!</h2>
            <p>Dear <?php echo $firstname; ?>,</p>
            <p>Thank you for completing your resume. You will receive a confirmation mail upon receipt of your resume.</p>
            <h2>What's Next?</h2>
            <p>We will evaluate your resume and provide feedback within 5 business days.</p>
            <p>Should you have any questions in the mean time, please contact us at <a href="mailto:info@informationmapping.com">info@informationmapping.com</a>.</p>
            <p>With kind regards,<br />
                Information Mapping International</p>
        </div>
        <?

        $mime_boundary="==Multipart_Boundary_x".md5(mt_rand())."x";

        $message  = "<table cellpadding='0' cellspacing='0' border='0' width='100%'><tr><td align='center'>";
        $message .= "<table cellpadding='0' cellspacing='0' border='0' width='500'><tr><td align='left'>";
        $message .= "<p><img src='http://www.informationmapping.com/imcc/resume/images/logo_email.gif' alt='Information Mapping logo' /></p>";
        $message .= "<p><font size='4' face='Arial, Helvetica, sans-serif' color=#009edf'>$program</font></p>";
        $message .= "<p><font size='2' face='Arial, Helvetica, sans-serif'>Dear $firstname,</font></p>";
        $message .= "<p><font size='2' face='Arial, Helvetica, sans-serif'>Thank you for completing your resume.<br />";
        $message .= "<font size='2' face='Arial, Helvetica, sans-serif'>We will evaluate your resume and provide feedback within 5 business days.</font></p>";
        $message .= "<p><font size='2' face='Arial, Helvetica, sans-serif'>With kind regards,</font><br />";
        $message .= "<font size='2' face='Arial, Helvetica, sans-serif'>Information Mapping International</font></p>";
        $message .= "</td></tr></table></td></tr></table>";

        $headers = "From: Information Mapping International <info@informationmapping.com>\n";
        $headers .= "BCC: Filip Vanlerberghe <fvanlerberghe@informationmapping.com>\n";
        $headers .= "BCC: Veronique Wittebolle <vwittebolle@informationmapping.com>\n";
        $headers .= "BCC: Francis Declercq <fdeclercq@informationmapping.com>\n";
        $headers .= "BCC: Wim Van Dessel <wvandessel@informationmapping.com>\n";


        $headers .= "X-Priority: 1\n";
        $headers .= "Content-Type: multipart/mixed;\n" . " boundary=\"{$mime_boundary}\"\n" . "MIME-Version: 1.0\n";

        if ($program=="Information Mapping Certified Consultant Program") {
            $subject="IMCC Program - Thank you for completing your resume";
        }

        if ($program=="Information Mapping Certified Professional Program") {
            $subject="IMCP Program - Thank you for completing your resume";
        }

        $message = "This is a multi-part message in MIME format.\n\n" .
            "--{$mime_boundary}\n" .
            "Content-Type: text/html; charset=\"iso-8859-1\"\n" .
            "Content-Transfer-Encoding: 7bit\n\n" . $message . "\n\n";

        $message .="--{$mime_boundary}--\n";
        mail($email,$subject, $message, $headers);
    }
}else{

    ?>
    <h1>Information Mapping Certified Consultant Program</h1>
    <div id="content">
    <h2>Complete Your Resume</h2>
    <p>Please fill out your resume below.</p>
    <form action="<?php echo $_SERVER['PHP_SELF']; ?>" method="post" enctype="multipart/form-data">
    <div style="display:none;">
        <fieldset>
            <legend>Choose Your Program</legend>
            <ol>
                <li>
                    <label for="program">I am applying for the</label>
                    <select name="program">
                        <option>- Select your program -</option>
                        <option <?php if(($_POST['program'])=="Information Mapping Certified Consultant Program") { echo "selected='selected'";} ?>>Information Mapping Certified Consultant Program</option>
                        <option <?php if(($_POST['program'])=="Information Mapping Certified Professional Program") { echo "selected='selected'";} ?>>Information Mapping Certified Professional Program</option>
                    </select>
                    <?php if(isset($_POST['submit']) && isset($error[21])) { echo $error[21]; } ?>
                </li>
                <li>
                    <label>&nbsp;</label>
                    <a href="http://www.informationmapping.com/en/imcc-overview/certification-programs-overview/" target="_blank">Not sure which one to choose?</a>

                </li>
            </ol>
        </fieldset>
    </div>
    <fieldset>

    <legend>About You</legend>
    <ol>
    <li>
        <label for="firstname">First name</label>
        <input id="firstname" type="text" name="firstname" size="46" value="<?php if(!empty($_POST['firstname'])) { echo $_POST['firstname']; } ?>">
        <?php if(isset($_POST['submit']) && isset($error[1])) { echo $error[1]; } ?>
    </li>
    <li>
        <label for="lastname">Last name</label>
        <input id="lastname" type="text" name="lastname" size="46" value="<?php if(!empty($_POST['lastname'])) { echo $_POST['lastname']; } ?>">
        <?php if(isset($_POST['submit']) && isset($error[2])) { echo $error[2]; } ?>
    </li>
    <li>
        <label for="email">E-mail address</label>
        <input id="email" type="text" name="email" size="46" value="<?php if(!empty($_POST['email'])) { echo $_POST['email']; } ?>">
        <?php if(isset($_POST['submit']) && isset($error[3])) { echo $error[3]; } ?>
        <?php if(isset($_POST['submit']) && isset($error[16])) { echo $error[16]; } ?>
    </li>
    <li>
        <label for="telephone">Telephone number</label>
        <input id="telephone" type="text" name="telephone" size="46" value="<?php if(!empty($_POST['telephone'])) { echo $_POST['telephone']; } ?>">
        <?php if(isset($_POST['submit']) && isset($error[19])) { echo $error[19]; } ?>
    </li>
    <li>
        <label for="address1">Address</label>
        <input id="address1" type="text" name="address1" size="46" value="<?php if(!empty($_POST['address1'])) { echo $_POST['address1']; } ?>">
        <?php if(isset($_POST['submit']) && isset($error[4])) { echo $error[4]; } ?>
    </li>
    <li>
        <label for="address2">&nbsp;</label>
        <input id="address2" type="text" name="address2" size="46" value="<?php if(!empty($_POST['address2'])) { echo $_POST['address2']; } ?>">
    </li>
    <li>
        <label for="city">City</label>
        <input id="city" type="text" name="city" size="46" value="<?php if(!empty($_POST['city'])) { echo $_POST['city']; } ?>">
        <?php if(isset($_POST['submit']) && isset($error[5])) { echo $error[5]; } ?>
    </li>
    <li>
        <label for="postalcode">Postal code / ZIP</label>
        <input id="postalcode" type="text" name="postalcode" size="46" value="<?php if(!empty($_POST['postalcode'])) { echo $_POST['postalcode']; } ?>">
        <?php if(isset($_POST['submit']) && isset($error[6])) { echo $error[6]; } ?>
    </li>
    <li>
        <label for="state">State</label>
        <input id="state" type="text" name="state" size="46" value="<?php if(!empty($_POST['state'])) { echo $_POST['state']; } ?>">
        <?php if(isset($_POST['submit']) && isset($error[8])) { echo $error[8]; } ?>
    </li>
    <li>
    <label for="country">Country</label>
    <select name="country" id="country">
    <option value=""> </option>
    <option value="Afghanistan"<?php if($_POST['country'] =="Afghanistan") { echo " selected"; } ?>>Afghanistan</option>
    <option value="Albania"<?php if($_POST['country'] =="Albania") { echo " selected"; } ?>>Albania</option>
    <option value="Algeria"<?php if($_POST['country'] =="Algeria") { echo " selected"; } ?>>Algeria</option>
    <option value="American Samoa"<?php if($_POST['country'] =="American Samoa") { echo " selected"; } ?>>American Samoa</option>
    <option value="Andorra"<?php if($_POST['country'] =="Andorra") { echo " selected"; } ?>>Andorra</option>
    <option value="Angola"<?php if($_POST['country'] =="Angola") { echo " selected"; } ?>>Angola</option>
    <option value="Anguilla"<?php if($_POST['country'] =="Anguilla") { echo " selected"; } ?>>Anguilla</option>
    <option value="Antarctica"<?php if($_POST['country'] =="Antarctica") { echo " selected"; } ?>>Antarctica</option>
    <option value="Antigua and Barbuda"<?php if($_POST['country'] =="Antigua and Barbuda") { echo " selected"; } ?>>Antigua and Barbuda</option>
    <option value="Argentina"<?php if($_POST['country'] =="Argentina") { echo " selected"; } ?>>Argentina</option>
    <option value="Armenia"<?php if($_POST['country'] =="Armenia") { echo " selected"; } ?>>Armenia</option>
    <option value="Aruba"<?php if($_POST['country'] =="Aruba") { echo " selected"; } ?>>Aruba</option>
    <option value="Australia"<?php if($_POST['country'] =="Australia") { echo " selected"; } ?>>Australia</option>
    <option value="Austria"<?php if($_POST['country'] =="Austria") { echo " selected"; } ?>>Austria</option>
    <option value="Azerbaijan"<?php if($_POST['country'] =="Azerbaijan") { echo " selected"; } ?>>Azerbaijan</option>
    <option value="Bahamas"<?php if($_POST['country'] =="Bahamas") { echo " selected"; } ?>>Bahamas</option>
    <option value="Bahrain"<?php if($_POST['country'] =="Bahrain") { echo " selected"; } ?>>Bahrain</option>
    <option value="Bangladesh"<?php if($_POST['country'] =="Bangladesh") { echo " selected"; } ?>>Bangladesh</option>
    <option value="Barbados"<?php if($_POST['country'] =="Barbados") { echo " selected"; } ?>>Barbados</option>
    <option value="Belarus"<?php if($_POST['country'] =="Belarus") { echo " selected"; } ?>>Belarus</option>
    <option value="Belgium"<?php if($_POST['country'] =="Belgium") { echo " selected"; } ?>>Belgium</option>
    <option value="Belize"<?php if($_POST['country'] =="Belize") { echo " selected"; } ?>>Belize</option>
    <option value="Benin"<?php if($_POST['country'] =="Benin") { echo " selected"; } ?>>Benin</option>
    <option value="Bermuda"<?php if($_POST['country'] =="Bermuda") { echo " selected"; } ?>>Bermuda</option>
    <option value="Bhutan"<?php if($_POST['country'] =="Bhutan") { echo " selected"; } ?>>Bhutan</option>
    <option value="Bolivia"<?php if($_POST['country'] =="Bolivia") { echo " selected"; } ?>>Bolivia</option>
    <option value="Bosnia and Herzegovina"<?php if($_POST['country'] =="Bosnia and Herzegovina") { echo " selected"; } ?>>Bosnia and Herzegovina</option>
    <option value="Botswana"<?php if($_POST['country'] =="Botswana") { echo " selected"; } ?>>Botswana</option>
    <option value="Bouvet Island"<?php if($_POST['country'] =="Bouvet Island") { echo " selected"; } ?>>Bouvet Island</option>
    <option value="Brazil"<?php if($_POST['country'] =="Brazil") { echo " selected"; } ?>>Brazil</option>
    <option value="British Indian Ocean Territory"<?php if($_POST['country'] =="British Indian Ocean Territory") { echo " selected"; } ?>>British Indian Ocean Territory</option>
    <option value="Brunei Darussalam"<?php if($_POST['country'] =="Brunei Darussalam") { echo " selected"; } ?>>Brunei Darussalam</option>
    <option value="Bulgaria"<?php if($_POST['country'] =="Bulgaria") { echo " selected"; } ?>>Bulgaria</option>
    <option value="Burkina Faso"<?php if($_POST['country'] =="Burkina Faso") { echo " selected"; } ?>>Burkina Faso</option>
    <option value="Burundi"<?php if($_POST['country'] =="Burundi") { echo " selected"; } ?>>Burundi</option>
    <option value="Cambodia"<?php if($_POST['country'] =="Cambodia") { echo " selected"; } ?>>Cambodia</option>
    <option value="Cameroon"<?php if($_POST['country'] =="Cameroon") { echo " selected"; } ?>>Cameroon</option>
    <option value="Canada"<?php if($_POST['country'] =="Canada") { echo " selected"; } ?>>Canada</option>
    <option value="Cape Verde"<?php if($_POST['country'] =="Cape Verde") { echo " selected"; } ?>>Cape Verde</option>
    <option value="Cayman Islands"<?php if($_POST['country'] =="Cayman Islands") { echo " selected"; } ?>>Cayman Islands</option>
    <option value="Central African Republic"<?php if($_POST['country'] =="Central African Republic") { echo " selected"; } ?>>Central African Republic</option>
    <option value="Chad"<?php if($_POST['country'] =="Chad") { echo " selected"; } ?>>Chad</option>
    <option value="Chile"<?php if($_POST['country'] =="Chile") { echo " selected"; } ?>>Chile</option>
    <option value="China"<?php if($_POST['country'] =="China") { echo " selected"; } ?>>China</option>
    <option value="Christmas Island"<?php if($_POST['country'] =="Christmas Island") { echo " selected"; } ?>>Christmas Island</option>
    <option value="Cocos (Keeling) Islands"<?php if($_POST['country'] =="Cocos (Keeling) Islands") { echo " selected"; } ?>>Cocos (Keeling) Islands</option>
    <option value="Colombia"<?php if($_POST['country'] =="Colombia") { echo " selected"; } ?>>Colombia</option>
    <option value="Comoros"<?php if($_POST['country'] =="Comoros") { echo " selected"; } ?>>Comoros</option>
    <option value="Congo"<?php if($_POST['country'] =="Congo") { echo " selected"; } ?>>Congo</option>
    <option value="Congo, The Democratic Republic of The"<?php if($_POST['country'] =="Congo, The Democratic Republic of The") { echo " selected"; } ?>>Congo, The Democratic Republic of The</option>
    <option value="Cook Islands"<?php if($_POST['country'] =="Cook Islands") { echo " selected"; } ?>>Cook Islands</option>
    <option value="Costa Rica"<?php if($_POST['country'] =="Costa Rica") { echo " selected"; } ?>>Costa Rica</option>
    <option value="Cote D'ivoire"<?php if($_POST['country'] =="Cote D'ivoire") { echo " selected"; } ?>>Cote D'ivoire</option>
    <option value="Croatia"<?php if($_POST['country'] =="Croatia") { echo " selected"; } ?>>Croatia</option>
    <option value="Cuba"<?php if($_POST['country'] =="Cuba") { echo " selected"; } ?>>Cuba</option>
    <option value="Cyprus"<?php if($_POST['country'] =="Cyprus") { echo " selected"; } ?>>Cyprus</option>
    <option value="Czech Republic"<?php if($_POST['country'] =="Czech Republic") { echo " selected"; } ?>>Czech Republic</option>
    <option value="Denmark"<?php if($_POST['country'] =="Denmark") { echo " selected"; } ?>>Denmark</option>
    <option value="Djibouti"<?php if($_POST['country'] =="Djibouti") { echo " selected"; } ?>>Djibouti</option>
    <option value="Dominica"<?php if($_POST['country'] =="Dominica") { echo " selected"; } ?>>Dominica</option>
    <option value="Dominican Republic"<?php if($_POST['country'] =="Dominican Republic") { echo " selected"; } ?>>Dominican Republic</option>
    <option value="Ecuador"<?php if($_POST['country'] =="Ecuador") { echo " selected"; } ?>>Ecuador</option>
    <option value="Egypt"<?php if($_POST['country'] =="Egypt") { echo " selected"; } ?>>Egypt</option>
    <option value="El Salvador"<?php if($_POST['country'] =="El Salvador") { echo " selected"; } ?>>El Salvador</option>
    <option value="Equatorial Guinea"<?php if($_POST['country'] =="Equatorial Guinea") { echo " selected"; } ?>>Equatorial Guinea</option>
    <option value="Eritrea"<?php if($_POST['country'] =="Eritrea") { echo " selected"; } ?>>Eritrea</option>
    <option value="Estonia"<?php if($_POST['country'] =="Estonia") { echo " selected"; } ?>>Estonia</option>
    <option value="Ethiopia"<?php if($_POST['country'] =="Ethiopia") { echo " selected"; } ?>>Ethiopia</option>
    <option value="Falkland Islands (Malvinas)"<?php if($_POST['country'] =="Falkland Islands (Malvinas)") { echo " selected"; } ?>>Falkland Islands (Malvinas)</option>
    <option value="Faroe Islands"<?php if($_POST['country'] =="Faroe Islands") { echo " selected"; } ?>>Faroe Islands</option>
    <option value="Fiji"<?php if($_POST['country'] =="Fiji") { echo " selected"; } ?>>Fiji</option>
    <option value="Finland"<?php if($_POST['country'] =="Finland") { echo " selected"; } ?>>Finland</option>
    <option value="France"<?php if($_POST['country'] =="France") { echo " selected"; } ?>>France</option>
    <option value="French Guiana"<?php if($_POST['country'] =="French Guiana") { echo " selected"; } ?>>French Guiana</option>
    <option value="French Polynesia"<?php if($_POST['country'] =="French Polynesia") { echo " selected"; } ?>>French Polynesia</option>
    <option value="French Southern Territories"<?php if($_POST['country'] =="French Southern Territories") { echo " selected"; } ?>>French Southern Territories</option>
    <option value="Gabon"<?php if($_POST['country'] =="Gabon") { echo " selected"; } ?>>Gabon</option>
    <option value="Gambia"<?php if($_POST['country'] =="Gambia") { echo " selected"; } ?>>Gambia</option>
    <option value="Georgia"<?php if($_POST['country'] =="Georgia") { echo " selected"; } ?>>Georgia</option>
    <option value="Germany"<?php if($_POST['country'] =="Germany") { echo " selected"; } ?>>Germany</option>
    <option value="Ghana"<?php if($_POST['country'] =="Ghana") { echo " selected"; } ?>>Ghana</option>
    <option value="Gibraltar"<?php if($_POST['country'] =="Gibraltar") { echo " selected"; } ?>>Gibraltar</option>
    <option value="Greece"<?php if($_POST['country'] =="Greece") { echo " selected"; } ?>>Greece</option>
    <option value="Greenland"<?php if($_POST['country'] =="Greenland") { echo " selected"; } ?>>Greenland</option>
    <option value="Grenada"<?php if($_POST['country'] =="Grenada") { echo " selected"; } ?>>Grenada</option>
    <option value="Guadeloupe"<?php if($_POST['country'] =="Guadeloupe") { echo " selected"; } ?>>Guadeloupe</option>
    <option value="Guam"<?php if($_POST['country'] =="Guam") { echo " selected"; } ?>>Guam</option>
    <option value="Guatemala"<?php if($_POST['country'] =="Guatemala") { echo " selected"; } ?>>Guatemala</option>
    <option value="Guinea"<?php if($_POST['country'] =="Guinea") { echo " selected"; } ?>>Guinea</option>
    <option value="Guinea-bissau"<?php if($_POST['country'] =="Guinea-bissau") { echo " selected"; } ?>>Guinea-bissau</option>
    <option value="Guyana"<?php if($_POST['country'] =="Guyana") { echo " selected"; } ?>>Guyana</option>
    <option value="Haiti"<?php if($_POST['country'] =="Haiti") { echo " selected"; } ?>>Haiti</option>
    <option value="Heard Island and Mcdonald Islands"<?php if($_POST['country'] =="Heard Island and Mcdonald Islands") { echo " selected"; } ?>>Heard Island and Mcdonald Islands</option>
    <option value="Holy See (Vatican City State)"<?php if($_POST['country'] =="Holy See (Vatican City State)") { echo " selected"; } ?>>Holy See (Vatican City State)</option>
    <option value="Honduras"<?php if($_POST['country'] =="Honduras") { echo " selected"; } ?>>Honduras</option>
    <option value="Hong Kong"<?php if($_POST['country'] =="Hong Kong") { echo " selected"; } ?>>Hong Kong</option>
    <option value="Hungary"<?php if($_POST['country'] =="Hungary") { echo " selected"; } ?>>Hungary</option>
    <option value="Iceland"<?php if($_POST['country'] =="Iceland") { echo " selected"; } ?>>Iceland</option>
    <option value="India"<?php if($_POST['country'] =="India") { echo " selected"; } ?>>India</option>
    <option value="Indonesia"<?php if($_POST['country'] =="Indonesia") { echo " selected"; } ?>>Indonesia</option>
    <option value="Iran, Islamic Republic of"<?php if($_POST['country'] =="Iran, Islamic Republic of") { echo " selected"; } ?>>Iran, Islamic Republic of</option>
    <option value="Iraq"<?php if($_POST['country'] =="Iraq") { echo " selected"; } ?>>Iraq</option>
    <option value="Ireland"<?php if($_POST['country'] =="Ireland") { echo " selected"; } ?>>Ireland</option>
    <option value="Israel"<?php if($_POST['country'] =="Israel") { echo " selected"; } ?>>Israel</option>
    <option value="Italy"<?php if($_POST['country'] =="Italy") { echo " selected"; } ?>>Italy</option>
    <option value="Jamaica"<?php if($_POST['country'] =="Jamaica") { echo " selected"; } ?>>Jamaica</option>
    <option value="Japan"<?php if($_POST['country'] =="Japan") { echo " selected"; } ?>>Japan</option>
    <option value="Jordan"<?php if($_POST['country'] =="Jordan") { echo " selected"; } ?>>Jordan</option>
    <option value="Kazakhstan"<?php if($_POST['country'] =="Kazakhstan") { echo " selected"; } ?>>Kazakhstan</option>
    <option value="Kenya"<?php if($_POST['country'] =="Kenya") { echo " selected"; } ?>>Kenya</option>
    <option value="Kiribati"<?php if($_POST['country'] =="Kiribati") { echo " selected"; } ?>>Kiribati</option>
    <option value="Korea, Democratic People's Republic of"<?php if($_POST['country'] =="Korea, Democratic People's Republic of") { echo " selected"; } ?>>Korea, Democratic People's Republic of</option>
    <option value="Korea, Republic of"<?php if($_POST['country'] =="Korea, Republic of") { echo " selected"; } ?>>Korea, Republic of</option>
    <option value="Kuwait"<?php if($_POST['country'] =="Kuwait") { echo " selected"; } ?>>Kuwait</option>
    <option value="Kyrgyzstan"<?php if($_POST['country'] =="Kyrgyzstan") { echo " selected"; } ?>>Kyrgyzstan</option>
    <option value="Lao People's Democratic Republic"<?php if($_POST['country'] =="Lao People's Democratic Republic") { echo " selected"; } ?>>Lao People's Democratic Republic</option>
    <option value="Latvia"<?php if($_POST['country'] =="Latvia") { echo " selected"; } ?>>Latvia</option>
    <option value="Lebanon"<?php if($_POST['country'] =="Lebanon") { echo " selected"; } ?>>Lebanon</option>
    <option value="Lesotho"<?php if($_POST['country'] =="Lesotho") { echo " selected"; } ?>>Lesotho</option>
    <option value="Liberia"<?php if($_POST['country'] =="Liberia") { echo " selected"; } ?>>Liberia</option>
    <option value="Libyan Arab Jamahiriya"<?php if($_POST['country'] =="Libyan Arab Jamahiriya") { echo " selected"; } ?>>Libyan Arab Jamahiriya</option>
    <option value="Liechtenstein"<?php if($_POST['country'] =="Liechtenstein") { echo " selected"; } ?>>Liechtenstein</option>
    <option value="Lithuania"<?php if($_POST['country'] =="Lithuania") { echo " selected"; } ?>>Lithuania</option>
    <option value="Luxembourg"<?php if($_POST['country'] =="Luxembourg") { echo " selected"; } ?>>Luxembourg</option>
    <option value="Macao"<?php if($_POST['country'] =="Macao") { echo " selected"; } ?>>Macao</option>
    <option value="Macedonia, The Former Yugoslav Republic of"<?php if($_POST['country'] =="Macedonia, The Former Yugoslav Republic of") { echo " selected"; } ?>>Macedonia, The Former Yugoslav Republic of</option>
    <option value="Madagascar"<?php if($_POST['country'] =="Madagascar") { echo " selected"; } ?>>Madagascar</option>
    <option value="Malawi"<?php if($_POST['country'] =="Malawi") { echo " selected"; } ?>>Malawi</option>
    <option value="Malaysia"<?php if($_POST['country'] =="Malaysia") { echo " selected"; } ?>>Malaysia</option>
    <option value="Maldives"<?php if($_POST['country'] =="Maldives") { echo " selected"; } ?>>Maldives</option>
    <option value="Mali"<?php if($_POST['country'] =="Mali") { echo " selected"; } ?>>Mali</option>
    <option value="Malta"<?php if($_POST['country'] =="Malta") { echo " selected"; } ?>>Malta</option>
    <option value="Marshall Islands"<?php if($_POST['country'] =="Marshall Islands") { echo " selected"; } ?>>Marshall Islands</option>
    <option value="Martinique"<?php if($_POST['country'] =="Martinique") { echo " selected"; } ?>>Martinique</option>
    <option value="Mauritania"<?php if($_POST['country'] =="Mauritania") { echo " selected"; } ?>>Mauritania</option>
    <option value="Mauritius"<?php if($_POST['country'] =="Mauritius") { echo " selected"; } ?>>Mauritius</option>
    <option value="Mayotte"<?php if($_POST['country'] =="Mayotte") { echo " selected"; } ?>>Mayotte</option>
    <option value="Mexico"<?php if($_POST['country'] =="Mexico") { echo " selected"; } ?>>Mexico</option>
    <option value="Micronesia, Federated States of"<?php if($_POST['country'] =="Micronesia, Federated States of") { echo " selected"; } ?>>Micronesia, Federated States of</option>
    <option value="Moldova, Republic of"<?php if($_POST['country'] =="Moldova, Republic of") { echo " selected"; } ?>>Moldova, Republic of</option>
    <option value="Monaco"<?php if($_POST['country'] =="Monaco") { echo " selected"; } ?>>Monaco</option>
    <option value="Mongolia"<?php if($_POST['country'] =="Mongolia") { echo " selected"; } ?>>Mongolia</option>
    <option value="Montserrat"<?php if($_POST['country'] =="Montserrat") { echo " selected"; } ?>>Montserrat</option>
    <option value="Morocco"<?php if($_POST['country'] =="Morocco") { echo " selected"; } ?>>Morocco</option>
    <option value="Mozambique"<?php if($_POST['country'] =="Mozambique") { echo " selected"; } ?>>Mozambique</option>
    <option value="Myanmar"<?php if($_POST['country'] =="Myanmar") { echo " selected"; } ?>>Myanmar</option>
    <option value="Namibia"<?php if($_POST['country'] =="Namibia") { echo " selected"; } ?>>Namibia</option>
    <option value="Nauru"<?php if($_POST['country'] =="Nauru") { echo " selected"; } ?>>Nauru</option>
    <option value="Nepal"<?php if($_POST['country'] =="Nepal") { echo " selected"; } ?>>Nepal</option>
    <option value="Netherlands"<?php if($_POST['country'] =="Netherlands") { echo " selected"; } ?>>Netherlands</option>
    <option value="Netherlands Antilles"<?php if($_POST['country'] =="Netherlands Antilles") { echo " selected"; } ?>>Netherlands Antilles</option>
    <option value="New Caledonia"<?php if($_POST['country'] =="New Caledonia") { echo " selected"; } ?>>New Caledonia</option>
    <option value="New Zealand"<?php if($_POST['country'] =="New Zealand") { echo " selected"; } ?>>New Zealand</option>
    <option value="Nicaragua"<?php if($_POST['country'] =="Nicaragua") { echo " selected"; } ?>>Nicaragua</option>
    <option value="Niger"<?php if($_POST['country'] =="Niger") { echo " selected"; } ?>>Niger</option>
    <option value="Nigeria"<?php if($_POST['country'] =="Nigeria") { echo " selected"; } ?>>Nigeria</option>
    <option value="Niue"<?php if($_POST['country'] =="Niue") { echo " selected"; } ?>>Niue</option>
    <option value="Norfolk Island"<?php if($_POST['country'] =="Norfolk Island") { echo " selected"; } ?>>Norfolk Island</option>
    <option value="Northern Mariana Islands"<?php if($_POST['country'] =="Northern Mariana Islands") { echo " selected"; } ?>>Northern Mariana Islands</option>
    <option value="Norway"<?php if($_POST['country'] =="Norway") { echo " selected"; } ?>>Norway</option>
    <option value="Oman"<?php if($_POST['country'] =="Oman") { echo " selected"; } ?>>Oman</option>
    <option value="Pakistan"<?php if($_POST['country'] =="Pakistan") { echo " selected"; } ?>>Pakistan</option>
    <option value="Palau"<?php if($_POST['country'] =="Palau") { echo " selected"; } ?>>Palau</option>
    <option value="Palestinian Territory, Occupied"<?php if($_POST['country'] =="Palestinian Territory, Occupied") { echo " selected"; } ?>>Palestinian Territory, Occupied</option>
    <option value="Panama"<?php if($_POST['country'] =="Panama") { echo " selected"; } ?>>Panama</option>
    <option value="Papua New Guinea"<?php if($_POST['country'] =="Papua New Guinea") { echo " selected"; } ?>>Papua New Guinea</option>
    <option value="Paraguay"<?php if($_POST['country'] =="Paraguay") { echo " selected"; } ?>>Paraguay</option>
    <option value="Peru"<?php if($_POST['country'] =="Peru") { echo " selected"; } ?>>Peru</option>
    <option value="Philippines"<?php if($_POST['country'] =="Philippines") { echo " selected"; } ?>>Philippines</option>
    <option value="Pitcairn"<?php if($_POST['country'] =="Pitcairn") { echo " selected"; } ?>>Pitcairn</option>
    <option value="Poland"<?php if($_POST['country'] =="Poland") { echo " selected"; } ?>>Poland</option>
    <option value="Portugal"<?php if($_POST['country'] =="Portugal") { echo " selected"; } ?>>Portugal</option>
    <option value="Puerto Rico"<?php if($_POST['country'] =="Puerto Rico") { echo " selected"; } ?>>Puerto Rico</option>
    <option value="Qatar"<?php if($_POST['country'] =="Qatar") { echo " selected"; } ?>>Qatar</option>
    <option value="Reunion"<?php if($_POST['country'] =="Reunion") { echo " selected"; } ?>>Reunion</option>
    <option value="Romania"<?php if($_POST['country'] =="Romania") { echo " selected"; } ?>>Romania</option>
    <option value="Russian Federation"<?php if($_POST['country'] =="Russian Federation") { echo " selected"; } ?>>Russian Federation</option>
    <option value="Rwanda"<?php if($_POST['country'] =="Rwanda") { echo " selected"; } ?>>Rwanda</option>
    <option value="Saint Helena"<?php if($_POST['country'] =="Saint Helena") { echo " selected"; } ?>>Saint Helena</option>
    <option value="Saint Kitts and Nevis"<?php if($_POST['country'] =="Saint Kitts and Nevis") { echo " selected"; } ?>>Saint Kitts and Nevis</option>
    <option value="Saint Lucia"<?php if($_POST['country'] =="Saint Lucia") { echo " selected"; } ?>>Saint Lucia</option>
    <option value="Saint Pierre and Miquelon"<?php if($_POST['country'] =="Saint Pierre and Miquelon") { echo " selected"; } ?>>Saint Pierre and Miquelon</option>
    <option value="Saint Vincent and The Grenadines"<?php if($_POST['country'] =="Saint Vincent and The Grenadines") { echo " selected"; } ?>>Saint Vincent and The Grenadines</option>
    <option value="Samoa"<?php if($_POST['country'] =="Samoa") { echo " selected"; } ?>>Samoa</option>
    <option value="San Marino"<?php if($_POST['country'] =="San Marino") { echo " selected"; } ?>>San Marino</option>
    <option value="Sao Tome and Principe"<?php if($_POST['country'] =="Sao Tome and Principe") { echo " selected"; } ?>>Sao Tome and Principe</option>
    <option value="Saudi Arabia"<?php if($_POST['country'] =="Saudi Arabia") { echo " selected"; } ?>>Saudi Arabia</option>
    <option value="Senegal"<?php if($_POST['country'] =="Senegal") { echo " selected"; } ?>>Senegal</option>
    <option value="Serbia and Montenegro"<?php if($_POST['country'] =="Serbia and Montenegro") { echo " selected"; } ?>>Serbia and Montenegro</option>
    <option value="Seychelles"<?php if($_POST['country'] =="Seychelles") { echo " selected"; } ?>>Seychelles</option>
    <option value="Sierra Leone"<?php if($_POST['country'] =="Sierra Leone") { echo " selected"; } ?>>Sierra Leone</option>
    <option value="Singapore"<?php if($_POST['country'] =="Singapore") { echo " selected"; } ?>>Singapore</option>
    <option value="Slovakia"<?php if($_POST['country'] =="Slovakia") { echo " selected"; } ?>>Slovakia</option>
    <option value="Slovenia"<?php if($_POST['country'] =="Slovenia") { echo " selected"; } ?>>Slovenia</option>
    <option value="Solomon Islands"<?php if($_POST['country'] =="Solomon Islands") { echo " selected"; } ?>>Solomon Islands</option>
    <option value="Somalia"<?php if($_POST['country'] =="Somalia") { echo " selected"; } ?>>Somalia</option>
    <option value="South Africa"<?php if($_POST['country'] =="South Africa") { echo " selected"; } ?>>South Africa</option>
    <option value="South Georgia and The South Sandwich Islands"<?php if($_POST['country'] =="South Georgia and The South Sandwich Islands") { echo " selected"; } ?>>South Georgia and The South Sandwich Islands</option>
    <option value="Spain"<?php if($_POST['country'] =="Spain") { echo " selected"; } ?>>Spain</option>
    <option value="Sri Lanka"<?php if($_POST['country'] =="Sri Lanka") { echo " selected"; } ?>>Sri Lanka</option>
    <option value="Sudan"<?php if($_POST['country'] =="Sudan") { echo " selected"; } ?>>Sudan</option>
    <option value="Suriname"<?php if($_POST['country'] =="Suriname") { echo " selected"; } ?>>Suriname</option>
    <option value="Svalbard and Jan Mayen"<?php if($_POST['country'] =="Svalbard and Jan Mayen") { echo " selected"; } ?>>Svalbard and Jan Mayen</option>
    <option value="Swaziland"<?php if($_POST['country'] =="Swaziland") { echo " selected"; } ?>>Swaziland</option>
    <option value="Sweden"<?php if($_POST['country'] =="Sweden") { echo " selected"; } ?>>Sweden</option>
    <option value="Switzerland"<?php if($_POST['country'] =="Switzerland") { echo " selected"; } ?>>Switzerland</option>
    <option value="Syrian Arab Republic"<?php if($_POST['country'] =="Syrian Arab Republic") { echo " selected"; } ?>>Syrian Arab Republic</option>
    <option value="Taiwan, Province of China"<?php if($_POST['country'] =="Taiwan, Province of China") { echo " selected"; } ?>>Taiwan, Province of China</option>
    <option value="Tajikistan"<?php if($_POST['country'] =="Tajikistan") { echo " selected"; } ?>>Tajikistan</option>
    <option value="Tanzania, United Republic of"<?php if($_POST['country'] =="Tanzania, United Republic of") { echo " selected"; } ?>>Tanzania, United Republic of</option>
    <option value="Thailand"<?php if($_POST['country'] =="Thailand") { echo " selected"; } ?>>Thailand</option>
    <option value="Timor-leste"<?php if($_POST['country'] =="Timor-leste") { echo " selected"; } ?>>Timor-leste</option>
    <option value="Togo"<?php if($_POST['country'] =="Togo") { echo " selected"; } ?>>Togo</option>
    <option value="Tokelau"<?php if($_POST['country'] =="Tokelau") { echo " selected"; } ?>>Tokelau</option>
    <option value="Tonga"<?php if($_POST['country'] =="Tonga") { echo " selected"; } ?>>Tonga</option>
    <option value="Trinidad and Tobago"<?php if($_POST['country'] =="Trinidad and Tobago") { echo " selected"; } ?>>Trinidad and Tobago</option>
    <option value="Tunisia"<?php if($_POST['country'] =="Tunisia") { echo " selected"; } ?>>Tunisia</option>
    <option value="Turkey"<?php if($_POST['country'] =="Turkey") { echo " selected"; } ?>>Turkey</option>
    <option value="Turkmenistan"<?php if($_POST['country'] =="Turkmenistan") { echo " selected"; } ?>>Turkmenistan</option>
    <option value="Turks and Caicos Islands"<?php if($_POST['country'] =="Turks and Caicos Islands") { echo " selected"; } ?>>Turks and Caicos Islands</option>
    <option value="Tuvalu"<?php if($_POST['country'] =="Tuvalu") { echo " selected"; } ?>>Tuvalu</option>
    <option value="Uganda"<?php if($_POST['country'] =="Uganda") { echo " selected"; } ?>>Uganda</option>
    <option value="Ukraine"<?php if($_POST['country'] =="Ukraine") { echo " selected"; } ?>>Ukraine</option>
    <option value="United Arab Emirates"<?php if($_POST['country'] =="United Arab Emirates") { echo " selected"; } ?>>United Arab Emirates</option>
    <option value="United Kingdom"<?php if($_POST['country'] =="United Kingdom") { echo " selected"; } ?>>United Kingdom</option>
    <option value="United States"<?php if($_POST['country'] =="United States") { echo " selected"; } ?>>United States</option>
    <option value="United States Minor Outlying Islands"<?php if($_POST['country'] =="United States Minor Outlying Islands") { echo " selected"; } ?>>United States Minor Outlying Islands</option>
    <option value="Uruguay"<?php if($_POST['country'] =="Uruguay") { echo " selected"; } ?>>Uruguay</option>
    <option value="Uzbekistan"<?php if($_POST['country'] =="Uzbekistan") { echo " selected"; } ?>>Uzbekistan</option>
    <option value="Vanuatu"<?php if($_POST['country'] =="Vanuatu") { echo " selected"; } ?>>Vanuatu</option>
    <option value="Venezuela"<?php if($_POST['country'] =="Venezuela") { echo " selected"; } ?>>Venezuela</option>
    <option value="Viet Nam"<?php if($_POST['country'] =="Viet Nam") { echo " selected"; } ?>>Viet Nam</option>
    <option value="Virgin Islands, British"<?php if($_POST['country'] =="Virgin Islands, British") { echo " selected"; } ?>>Virgin Islands, British</option>
    <option value="Virgin Islands, U.S."<?php if($_POST['country'] =="Virgin Islands, U.S.") { echo " selected"; } ?>>Virgin Islands, U.S.</option>
    <option value="Wallis and Futuna"<?php if($_POST['country'] =="Wallis and Futuna") { echo " selected"; } ?>>Wallis and Futuna</option>
    <option value="Western Sahara"<?php if($_POST['country'] =="Western Sahara") { echo " selected"; } ?>>Western Sahara</option>
    <option value="Yemen"<?php if($_POST['country'] =="Yemen") { echo " selected"; } ?>>Yemen</option>
    <option value="Zambia"<?php if($_POST['country'] =="Zambia") { echo " selected"; } ?>>Zambia</option>
    <option value="Zimbabwe"<?php if($_POST['country'] =="Zimbabwe") { echo " selected"; } ?>>Zimbabwe</option>
    </select> <?php if(isset($_POST['submit']) && isset($error[7])) { echo $error[7]; } ?>
    </li>
    </ol>
    </fieldset>

    <fieldset>
        <legend>Your Professional Experience</legend>
        <ol>
            <li>
                <label for="profsummary">Summary</label>
                <textarea id="profsummary" name="profsummary" cols="35" rows="5"><?php if(!empty($_POST['profsummary'])) { echo $_POST['profsummary']; } ?></textarea>
                <?php if(isset($_POST['submit']) && isset($error[9])) { echo $error[9]; } ?>
            </li>
            <li>
                <label for="specialties">Specialties</label>
                <input id="specialties" type="text" name="specialties" size="46" value="<?php if(!empty($_POST['specialties'])) { echo $_POST['specialties']; } ?>">
                <?php if(isset($_POST['submit']) && isset($error[10])) { echo $error[10]; } ?>
            </li>
        </ol>
    </fieldset>

    <fieldset>
        <legend>Your Current Job</legend>
        <ol>
            <li>
                <label for="company_job0">Company</label>
                <input id="company_job0" type="text" name="company_job0" size="46" value="<?php if(!empty($_POST['company_job0'])) { echo $_POST['company_job0']; } ?>">
                <?php if(isset($_POST['submit']) && isset($error[11])) { echo $error[11]; } ?>
            </li>
            <li>
                <label for="title_job0">Position</label>
                <input id="title_job0" type="text" name="title_job0" size="46" value="<?php if(!empty($_POST['title_job0'])) { echo $_POST['title_job0']; } ?>">
                <?php if(isset($_POST['submit']) && isset($error[12])) { echo $error[12]; } ?>
            </li>
            <li>

                <label for="startmonth_job0">Since</label>
                <select name="startmonth_job0">
                    <option>- Month -</option>
                    <option <?php if(($_POST['startmonth_job0'])=="January") { echo "selected='selected'";} ?>>January</option>
                    <option <?php if(($_POST['startmonth_job0'])=="February") { echo "selected='selected'";} ?>>February</option>
                    <option <?php if(($_POST['startmonth_job0'])=="March") { echo "selected='selected'";} ?>>March</option>
                    <option <?php if(($_POST['startmonth_job0'])=="April") { echo "selected='selected'";} ?>>April</option>
                    <option <?php if(($_POST['startmonth_job0'])=="May") { echo "selected='selected'";} ?>>May</option>
                    <option <?php if(($_POST['startmonth_job0'])=="June") { echo "selected='selected'";} ?>>June</option>
                    <option <?php if(($_POST['startmonth_job0'])=="July") { echo "selected='selected'";} ?>>July</option>
                    <option <?php if(($_POST['startmonth_job0'])=="August") { echo "selected='selected'";} ?>>August</option>
                    <option <?php if(($_POST['startmonth_job0'])=="September") { echo "selected='selected'";} ?>>September</option>
                    <option <?php if(($_POST['startmonth_job0'])=="October") { echo "selected='selected'";} ?>>October</option>
                    <option <?php if(($_POST['startmonth_job0'])=="November") { echo "selected='selected'";} ?>>November</option>
                    <option <?php if(($_POST['startmonth_job0'])=="December") { echo "selected='selected'";} ?>>December</option>
                </select>
                <select name="startyear_job0">
                    <option>- Year -</option>
                    <option <?php if(($_POST['startyear_job0'])=="2000") { echo "selected='selected'";} ?>>2000</option>
                    <option <?php if(($_POST['startyear_job0'])=="2001") { echo "selected='selected'";} ?>>2001</option>
                    <option <?php if(($_POST['startyear_job0'])=="2002") { echo "selected='selected'";} ?>>2002</option>
                    <option <?php if(($_POST['startyear_job0'])=="2003") { echo "selected='selected'";} ?>>2003</option>
                    <option <?php if(($_POST['startyear_job0'])=="2004") { echo "selected='selected'";} ?>>2004</option>
                    <option <?php if(($_POST['startyear_job0'])=="2005") { echo "selected='selected'";} ?>>2005</option>
                    <option <?php if(($_POST['startyear_job0'])=="2006") { echo "selected='selected'";} ?>>2006</option>
                    <option <?php if(($_POST['startyear_job0'])=="2007") { echo "selected='selected'";} ?>>2007</option>
                    <option <?php if(($_POST['startyear_job0'])=="2008") { echo "selected='selected'";} ?>>2008</option>
                    <option <?php if(($_POST['startyear_job0'])=="2009") { echo "selected='selected'";} ?>>2009</option>
                    <option <?php if(($_POST['startyear_job0'])=="2010") { echo "selected='selected'";} ?>>2010</option>
                    <option <?php if(($_POST['startyear_job0'])=="2011") { echo "selected='selected'";} ?>>2011</option>
                    <option <?php if(($_POST['startyear_job0'])=="2012") { echo "selected='selected'";} ?>>2012</option>
                    <option <?php if(($_POST['startyear_job0'])=="2013") { echo "selected='selected'";} ?>>2013</option>
                    <option <?php if(($_POST['startyear_job0'])=="2014") { echo "selected='selected'";} ?>>2014</option>
                </select>
                <?php if(isset($_POST['submit']) && isset($error[13])) { echo $error[13]; } ?>
                <?php if(isset($_POST['submit']) && isset($error[14])) { echo $error[14]; } ?>
            </li>
            <li>
                <label for="description_job0">Short description</label>
                <textarea id="description_job0" name="description_job0" cols="35" rows="5"><?php if(!empty($_POST['description_job0'])) { echo $_POST['description_job0']; } ?></textarea>
                <?php if(isset($_POST['submit']) && isset($error[15])) { echo $error[15]; } ?>
            </li>
            <li>
                <label for="website">Company website</label>
                <input id="website" type="text" name="website" size="46" value="<?php if(!empty($_POST['website'])) { echo $_POST['website']; } ?>">
                <?php if(isset($_POST['submit']) && isset($error[20])) { echo $error[20]; } ?>
            </li>
        </ol>
        <p class="add"><a href="#" onclick="Toggle('toggle_job1'); return false;">Add another job</a></p>
    </fieldset>

    <fieldset id="toggle_job1" style="display:none;">
        <legend>Add Another Job</legend>
        <ol>
            <li>
                <label for="company_job1">Company</label>
                <input id="company_job1" type="text" name="company_job1" size="46" value="<?php if(!empty($_POST['company_job1'])) { echo $_POST['company_job1']; } ?>">
                <?php if(isset($_POST['submit']) && isset($error[16])) { echo $error[16]; } ?>
            </li>
            <li>
                <label for="title_job1">Position</label>
                <input id="title_job1" type="text" name="title_job1" size="46" value="<?php if(!empty($_POST['title_job1'])) { echo $_POST['title_job1']; } ?>">
                <?php if(isset($_POST['submit']) && isset($error[17])) { echo $error[17]; } ?>
            </li>
            <li>
                <label for="startmonth_job1">From</label>
                <select name="startmonth_job1">
                    <option>- Month -</option>
                    <option <?php if(($_POST['startmonth_job1'])=="January") { echo "selected='selected'";} ?>>January</option>
                    <option <?php if(($_POST['startmonth_job1'])=="February") { echo "selected='selected'";} ?>>February</option>
                    <option <?php if(($_POST['startmonth_job1'])=="March") { echo "selected='selected'";} ?>>March</option>
                    <option <?php if(($_POST['startmonth_job1'])=="April") { echo "selected='selected'";} ?>>April</option>
                    <option <?php if(($_POST['startmonth_job1'])=="May") { echo "selected='selected'";} ?>>May</option>
                    <option <?php if(($_POST['startmonth_job1'])=="June") { echo "selected='selected'";} ?>>June</option>
                    <option <?php if(($_POST['startmonth_job1'])=="July") { echo "selected='selected'";} ?>>July</option>
                    <option <?php if(($_POST['startmonth_job1'])=="August") { echo "selected='selected'";} ?>>August</option>
                    <option <?php if(($_POST['startmonth_job1'])=="September") { echo "selected='selected'";} ?>>September</option>
                    <option <?php if(($_POST['startmonth_job1'])=="October") { echo "selected='selected'";} ?>>October</option>
                    <option <?php if(($_POST['startmonth_job1'])=="November") { echo "selected='selected'";} ?>>November</option>
                    <option <?php if(($_POST['startmonth_job1'])=="December") { echo "selected='selected'";} ?>>December</option>
                </select>
                <select name="startyear_job1">
                    <option>- Year -</option>
                    <option <?php if(($_POST['startyear_job1'])=="2000") { echo "selected='selected'";} ?>>2000</option>
                    <option <?php if(($_POST['startyear_job1'])=="2001") { echo "selected='selected'";} ?>>2001</option>
                    <option <?php if(($_POST['startyear_job1'])=="2002") { echo "selected='selected'";} ?>>2002</option>
                    <option <?php if(($_POST['startyear_job1'])=="2003") { echo "selected='selected'";} ?>>2003</option>
                    <option <?php if(($_POST['startyear_job1'])=="2004") { echo "selected='selected'";} ?>>2004</option>
                    <option <?php if(($_POST['startyear_job1'])=="2005") { echo "selected='selected'";} ?>>2005</option>
                    <option <?php if(($_POST['startyear_job1'])=="2006") { echo "selected='selected'";} ?>>2006</option>
                    <option <?php if(($_POST['startyear_job1'])=="2007") { echo "selected='selected'";} ?>>2007</option>
                    <option <?php if(($_POST['startyear_job1'])=="2008") { echo "selected='selected'";} ?>>2008</option>
                    <option <?php if(($_POST['startyear_job1'])=="2009") { echo "selected='selected'";} ?>>2009</option>
                    <option <?php if(($_POST['startyear_job1'])=="2010") { echo "selected='selected'";} ?>>2010</option>
                    <option <?php if(($_POST['startyear_job1'])=="2011") { echo "selected='selected'";} ?>>2011</option>
                    <option <?php if(($_POST['startyear_job1'])=="2012") { echo "selected='selected'";} ?>>2012</option>
                    <option <?php if(($_POST['startyear_job1'])=="2013") { echo "selected='selected'";} ?>>2013</option>
                    <option <?php if(($_POST['startyear_job1'])=="2014") { echo "selected='selected'";} ?>>2014</option>
                </select>
                <?php if(isset($_POST['submit']) && isset($error[18])) { echo $error[18]; } ?>
                <?php if(isset($_POST['submit']) && isset($error[19])) { echo $error[19]; } ?>
            </li>
            <li>
                <label for="endmonth_job1">To</label>
                <select name="endmonth_job1">
                    <option>- Month -</option>
                    <option <?php if(($_POST['endmonth_job1'])=="January") { echo "selected='selected'";} ?>>January</option>
                    <option <?php if(($_POST['endmonth_job1'])=="February") { echo "selected='selected'";} ?>>February</option>
                    <option <?php if(($_POST['endmonth_job1'])=="March") { echo "selected='selected'";} ?>>March</option>
                    <option <?php if(($_POST['endmonth_job1'])=="April") { echo "selected='selected'";} ?>>April</option>
                    <option <?php if(($_POST['endmonth_job1'])=="May") { echo "selected='selected'";} ?>>May</option>
                    <option <?php if(($_POST['endmonth_job1'])=="June") { echo "selected='selected'";} ?>>June</option>
                    <option <?php if(($_POST['endmonth_job1'])=="July") { echo "selected='selected'";} ?>>July</option>
                    <option <?php if(($_POST['endmonth_job1'])=="August") { echo "selected='selected'";} ?>>August</option>
                    <option <?php if(($_POST['endmonth_job1'])=="September") { echo "selected='selected'";} ?>>September</option>
                    <option <?php if(($_POST['endmonth_job1'])=="October") { echo "selected='selected'";} ?>>October</option>
                    <option <?php if(($_POST['endmonth_job1'])=="November") { echo "selected='selected'";} ?>>November</option>
                    <option <?php if(($_POST['endmonth_job1'])=="December") { echo "selected='selected'";} ?>>December</option>
                </select>
                <select name="endyear_job1">
                    <option>- Year -</option>
                    <option <?php if(($_POST['endyear_job1'])=="2000") { echo "selected='selected'";} ?>>2000</option>
                    <option <?php if(($_POST['endyear_job1'])=="2001") { echo "selected='selected'";} ?>>2001</option>
                    <option <?php if(($_POST['endyear_job1'])=="2002") { echo "selected='selected'";} ?>>2002</option>
                    <option <?php if(($_POST['endyear_job1'])=="2003") { echo "selected='selected'";} ?>>2003</option>
                    <option <?php if(($_POST['endyear_job1'])=="2004") { echo "selected='selected'";} ?>>2004</option>
                    <option <?php if(($_POST['endyear_job1'])=="2005") { echo "selected='selected'";} ?>>2005</option>
                    <option <?php if(($_POST['endyear_job1'])=="2006") { echo "selected='selected'";} ?>>2006</option>
                    <option <?php if(($_POST['endyear_job1'])=="2007") { echo "selected='selected'";} ?>>2007</option>
                    <option <?php if(($_POST['endyear_job1'])=="2008") { echo "selected='selected'";} ?>>2008</option>
                    <option <?php if(($_POST['endyear_job1'])=="2009") { echo "selected='selected'";} ?>>2009</option>
                    <option <?php if(($_POST['endyear_job1'])=="2010") { echo "selected='selected'";} ?>>2010</option>
                    <option <?php if(($_POST['endyear_job1'])=="2011") { echo "selected='selected'";} ?>>2011</option>
                    <option <?php if(($_POST['endyear_job1'])=="2012") { echo "selected='selected'";} ?>>2012</option>
                    <option <?php if(($_POST['endyear_job1'])=="2013") { echo "selected='selected'";} ?>>2013</option>
                    <option <?php if(($_POST['endyear_job1'])=="2014") { echo "selected='selected'";} ?>>2014</option>
                </select>
                <?php if(isset($_POST['submit']) && isset($error[20])) { echo $error[20]; } ?>
                <?php if(isset($_POST['submit']) && isset($error[21])) { echo $error[21]; } ?>
            </li>
            <li>
                <label for="description_job1">Short description</label>
                <textarea id="description_job1" name="description_job1" cols="35" rows="5"><?php if(!empty($_POST['description_job1'])) { echo $_POST['description_job1']; } ?></textarea>
                <?php if(isset($_POST['submit']) && isset($error[22])) { echo $error[22]; } ?>
            </li>
        </ol>
    </fieldset>

    <fieldset>
        <legend>About the Information Mapping&reg; Training You Followed</legend>
        <script>
            function showMe (it, box) {
                var vis = (box.checked) ?  "block" : "none";
                document.getElementById(it).style.display = vis;
            }


        </script>
        <ol>
            <li>
                <input type="checkbox" name="followedtraining" onclick="showMe('imap', this)" value="yes" <?php if(!empty($_POST['followedtraining'])) { echo "checked"; } ?> /> I have followed already followed an Information Mapping training
            </li>
            <div id="imap" style="display:none;">
                <li>
                    <label for="coursetitle_imap">Course title</label>
                    <input id="coursetitle_imap" type="text" name="coursetitle_imap" size="46" value="<?php if(!empty($_POST['coursetitle_imap'])) { echo $_POST['coursetitle_imap']; } ?>">
                    <?php if(isset($_POST['submit']) && isset($error[23])) { echo $error[23]; } ?>
                </li>
                <li>
                    <label for="company_imap">Training company</label>
                    <input id="company_imap" type="text" name="company_imap" size="46" value="<?php if(!empty($_POST['company_imap'])) { echo $_POST['company_imap']; } ?>">
                    <?php if(isset($_POST['submit']) && isset($error[24])) { echo $error[24]; } ?>
                </li>
                <li>
                    <label for="month_imap">Training date</label>
                    <select name="month_imap">
                        <option>- Month -</option>
                        <option <?php if(($_POST['month_imap'])=="January") { echo "selected='selected'";} ?>>January</option>
                        <option <?php if(($_POST['month_imap'])=="February") { echo "selected='selected'";} ?>>February</option>
                        <option <?php if(($_POST['month_imap'])=="March") { echo "selected='selected'";} ?>>March</option>
                        <option <?php if(($_POST['month_imap'])=="April") { echo "selected='selected'";} ?>>April</option>
                        <option <?php if(($_POST['month_imap'])=="May") { echo "selected='selected'";} ?>>May</option>
                        <option <?php if(($_POST['month_imap'])=="June") { echo "selected='selected'";} ?>>June</option>
                        <option <?php if(($_POST['month_imap'])=="July") { echo "selected='selected'";} ?>>July</option>
                        <option <?php if(($_POST['month_imap'])=="August") { echo "selected='selected'";} ?>>August</option>
                        <option <?php if(($_POST['month_imap'])=="September") { echo "selected='selected'";} ?>>September</option>
                        <option <?php if(($_POST['month_imap'])=="October") { echo "selected='selected'";} ?>>October</option>
                        <option <?php if(($_POST['month_imap'])=="November") { echo "selected='selected'";} ?>>November</option>
                        <option <?php if(($_POST['month_imap'])=="December") { echo "selected='selected'";} ?>>December</option>
                    </select>
                    <select name="year_imap">
                        <option>- Year -</option>
                        <option <?php if(($_POST['year_imap'])=="1990") { echo "selected='selected'";} ?>>1990</option>
                        <option <?php if(($_POST['year_imap'])=="1991") { echo "selected='selected'";} ?>>1991</option>
                        <option <?php if(($_POST['year_imap'])=="1992") { echo "selected='selected'";} ?>>1992</option>
                        <option <?php if(($_POST['year_imap'])=="1993") { echo "selected='selected'";} ?>>1993</option>
                        <option <?php if(($_POST['year_imap'])=="1994") { echo "selected='selected'";} ?>>1994</option>
                        <option <?php if(($_POST['year_imap'])=="1995") { echo "selected='selected'";} ?>>1995</option>
                        <option <?php if(($_POST['year_imap'])=="1996") { echo "selected='selected'";} ?>>1996</option>
                        <option <?php if(($_POST['year_imap'])=="1997") { echo "selected='selected'";} ?>>1997</option>
                        <option <?php if(($_POST['year_imap'])=="1998") { echo "selected='selected'";} ?>>1998</option>
                        <option <?php if(($_POST['year_imap'])=="1999") { echo "selected='selected'";} ?>>1999</option>
                        <option <?php if(($_POST['year_imap'])=="2000") { echo "selected='selected'";} ?>>2000</option>
                        <option <?php if(($_POST['year_imap'])=="2001") { echo "selected='selected'";} ?>>2001</option>
                        <option <?php if(($_POST['year_imap'])=="2002") { echo "selected='selected'";} ?>>2002</option>
                        <option <?php if(($_POST['year_imap'])=="2003") { echo "selected='selected'";} ?>>2003</option>
                        <option <?php if(($_POST['year_imap'])=="2004") { echo "selected='selected'";} ?>>2004</option>
                        <option <?php if(($_POST['year_imap'])=="2005") { echo "selected='selected'";} ?>>2005</option>
                        <option <?php if(($_POST['year_imap'])=="2006") { echo "selected='selected'";} ?>>2006</option>
                        <option <?php if(($_POST['year_imap'])=="2007") { echo "selected='selected'";} ?>>2007</option>
                        <option <?php if(($_POST['year_imap'])=="2008") { echo "selected='selected'";} ?>>2008</option>
                        <option <?php if(($_POST['year_imap'])=="2009") { echo "selected='selected'";} ?>>2009</option>
                        <option <?php if(($_POST['year_imap'])=="2010") { echo "selected='selected'";} ?>>2010</option>
                        <option <?php if(($_POST['year_imap'])=="2011") { echo "selected='selected'";} ?>>2011</option>
                        <option <?php if(($_POST['year_imap'])=="2012") { echo "selected='selected'";} ?>>2012</option>
                        <option <?php if(($_POST['year_imap'])=="2013") { echo "selected='selected'";} ?>>2013</option>
                        <option <?php if(($_POST['year_imap'])=="2014") { echo "selected='selected'";} ?>>2014</option>
                    </select>
                    <?php if(isset($_POST['submit']) && isset($error[25])) { echo $error[25]; } ?>
                    <?php if(isset($_POST['submit']) && isset($error[26])) { echo $error[26]; } ?>
                </li>
                <li>
                    <label for="trainer_imap">Instructor</label>
                    <input id="trainer_imap" type="text" name="trainer_imap" size="46" value="<?php if(!empty($_POST['trainer_imap'])) { echo $_POST['trainer_imap']; } ?>">
                    <?php if(isset($_POST['submit']) && isset($error[27])) { echo $error[27]; } ?>
                </li>
                <li>
                    <label for="certificate_imap">Certificate</label>
                    <input id="certificate_imap" type="file" name="certificate_imap" size="18"> <small>(PDF, max. 2MB)</small>
                    <?php if(isset($_POST['submit']) && isset($error[28])) { echo $error[28]; } ?>
                    <?php if(isset($_POST['submit']) && isset($error[17])) { echo $error[17]; } ?>
                    <?php if(isset($_POST['submit']) && isset($error[18])) { echo $error[18]; } ?>
                </li>
                <li>
                    <label for="comments_imap">Comments</label>
                    <textarea id="comments_imap" name="comments_imap" cols="35" rows="5"><?php if(!empty($_POST['comments_imap'])) { echo $_POST['comments_imap']; } ?></textarea>
                </li>
            </div>
        </ol>
        <p class="add"><a href="#" onclick="Toggle('toggle_training1'); return false;">Add another training</a></p>
    </fieldset>

    <fieldset id="toggle_training1" style="display:none;">
        <legend>Add Another Training Course</legend>
        <ol>
            <li>
                <label for="coursetitle0">Course title</label>
                <input id="coursetitle0" type="text" name="coursetitle0" size="46" value="<?php if(!empty($_POST['coursetitle0'])) { echo $_POST['coursetitle0']; } ?>">
            </li>
            <li>
                <label for="company0">Training company</label>
                <input id="company0" type="text" name="company0" size="46" value="<?php if(!empty($_POST['company0'])) { echo $_POST['company0']; } ?>">
            </li>
            <li>
                <label for="month0">Training date</label>

                <select name="month0">
                    <option>- Month -</option>
                    <option <?php if(($_POST['month0'])=="January") { echo "selected='selected'";} ?>>January</option>
                    <option <?php if(($_POST['month0'])=="February") { echo "selected='selected'";} ?>>February</option>
                    <option <?php if(($_POST['month0'])=="March") { echo "selected='selected'";} ?>>March</option>
                    <option <?php if(($_POST['month0'])=="April") { echo "selected='selected'";} ?>>April</option>
                    <option <?php if(($_POST['month0'])=="May") { echo "selected='selected'";} ?>>May</option>
                    <option <?php if(($_POST['month0'])=="June") { echo "selected='selected'";} ?>>June</option>
                    <option <?php if(($_POST['month0'])=="July") { echo "selected='selected'";} ?>>July</option>
                    <option <?php if(($_POST['month0'])=="August") { echo "selected='selected'";} ?>>August</option>
                    <option <?php if(($_POST['month0'])=="September") { echo "selected='selected'";} ?>>September</option>
                    <option <?php if(($_POST['month0'])=="October") { echo "selected='selected'";} ?>>October</option>
                    <option <?php if(($_POST['month0'])=="November") { echo "selected='selected'";} ?>>November</option>
                    <option <?php if(($_POST['month0'])=="December") { echo "selected='selected'";} ?>>December</option>
                </select>
                <select name="year0">
                    <option>- Year -</option>
                    <option <?php if(($_POST['year0'])=="2000") { echo "selected='selected'";} ?>>2000</option>
                    <option <?php if(($_POST['year0'])=="2001") { echo "selected='selected'";} ?>>2001</option>
                    <option <?php if(($_POST['year0'])=="2002") { echo "selected='selected'";} ?>>2002</option>
                    <option <?php if(($_POST['year0'])=="2003") { echo "selected='selected'";} ?>>2003</option>
                    <option <?php if(($_POST['year0'])=="2004") { echo "selected='selected'";} ?>>2004</option>
                    <option <?php if(($_POST['year0'])=="2005") { echo "selected='selected'";} ?>>2005</option>
                    <option <?php if(($_POST['year0'])=="2006") { echo "selected='selected'";} ?>>2006</option>
                    <option <?php if(($_POST['year0'])=="2007") { echo "selected='selected'";} ?>>2007</option>
                    <option <?php if(($_POST['year0'])=="2008") { echo "selected='selected'";} ?>>2008</option>
                    <option <?php if(($_POST['year0'])=="2009") { echo "selected='selected'";} ?>>2009</option>
                    <option <?php if(($_POST['year0'])=="2010") { echo "selected='selected'";} ?>>2010</option>
                    <option <?php if(($_POST['year0'])=="2011") { echo "selected='selected'";} ?>>2011</option>
                    <option <?php if(($_POST['year0'])=="2012") { echo "selected='selected'";} ?>>2012</option>
                    <option <?php if(($_POST['year0'])=="2013") { echo "selected='selected'";} ?>>2013</option>
                    <option <?php if(($_POST['year0'])=="2014") { echo "selected='selected'";} ?>>2014</option>
                </select>
            </li>
            <li>
                <label for="comments0">Comments</label>
                <textarea id="comments0" name="comments0" cols="35" rows="5"><?php if(!empty($_POST['comments0'])) { echo $_POST['comments0']; } ?></textarea>
            </li>
        </ol>
        <p class="add"><a href="#" onclick="Toggle('toggle_training2'); return false;">Add another training</a></p>
    </fieldset>

    <fieldset id="toggle_training2" style="display:none;">
        <legend>Add Another Training Course</legend>
        <ol>
            <li>
                <label for="coursetitle1">Course title</label>
                <input id="coursetitle1" type="text" name="coursetitle1" size="46" value="<?php if(!empty($_POST['coursetitle1'])) { echo $_POST['coursetitle1']; } ?>">
            </li>
            <li>
                <label for="company1">Training company</label>
                <input id="company1" type="text" name="company1" size="46" value="<?php if(!empty($_POST['company1'])) { echo $_POST['company1']; } ?>">
            </li>
            <li>
                <label for="month1">Training date</label>

                <select name="month1">
                    <option>- Month -</option>
                    <option <?php if(($_POST['month1'])=="January") { echo "selected='selected'";} ?>>January</option>
                    <option <?php if(($_POST['month1'])=="February") { echo "selected='selected'";} ?>>February</option>
                    <option <?php if(($_POST['month1'])=="March") { echo "selected='selected'";} ?>>March</option>
                    <option <?php if(($_POST['month1'])=="April") { echo "selected='selected'";} ?>>April</option>
                    <option <?php if(($_POST['month1'])=="May") { echo "selected='selected'";} ?>>May</option>
                    <option <?php if(($_POST['month1'])=="June") { echo "selected='selected'";} ?>>June</option>
                    <option <?php if(($_POST['month1'])=="July") { echo "selected='selected'";} ?>>July</option>
                    <option <?php if(($_POST['month1'])=="August") { echo "selected='selected'";} ?>>August</option>
                    <option <?php if(($_POST['month1'])=="September") { echo "selected='selected'";} ?>>September</option>
                    <option <?php if(($_POST['month1'])=="October") { echo "selected='selected'";} ?>>October</option>
                    <option <?php if(($_POST['month1'])=="November") { echo "selected='selected'";} ?>>November</option>
                    <option <?php if(($_POST['month1'])=="December") { echo "selected='selected'";} ?>>December</option>
                </select>
                <select name="year1">
                    <option>- Year -</option>
                    <option <?php if(($_POST['year1'])=="2000") { echo "selected='selected'";} ?>>2000</option>
                    <option <?php if(($_POST['year1'])=="2001") { echo "selected='selected'";} ?>>2001</option>
                    <option <?php if(($_POST['year1'])=="2002") { echo "selected='selected'";} ?>>2002</option>
                    <option <?php if(($_POST['year1'])=="2003") { echo "selected='selected'";} ?>>2003</option>
                    <option <?php if(($_POST['year1'])=="2004") { echo "selected='selected'";} ?>>2004</option>
                    <option <?php if(($_POST['year1'])=="2005") { echo "selected='selected'";} ?>>2005</option>
                    <option <?php if(($_POST['year1'])=="2006") { echo "selected='selected'";} ?>>2006</option>
                    <option <?php if(($_POST['year1'])=="2007") { echo "selected='selected'";} ?>>2007</option>
                    <option <?php if(($_POST['year1'])=="2008") { echo "selected='selected'";} ?>>2008</option>
                    <option <?php if(($_POST['year1'])=="2009") { echo "selected='selected'";} ?>>2009</option>
                    <option <?php if(($_POST['year1'])=="2010") { echo "selected='selected'";} ?>>2010</option>
                    <option <?php if(($_POST['year1'])=="2011") { echo "selected='selected'";} ?>>2011</option>
                    <option <?php if(($_POST['year1'])=="2012") { echo "selected='selected'";} ?>>2012</option>
                    <option <?php if(($_POST['year1'])=="2013") { echo "selected='selected'";} ?>>2013</option>
                    <option <?php if(($_POST['year1'])=="2014") { echo "selected='selected'";} ?>>2014</option>
                </select>
            </li>
            <li>
                <label for="comments1">Comments</label>
                <textarea id="comments1" name="comments1" cols="35" rows="5"><?php if(!empty($_POST['comments1'])) { echo $_POST['comments1']; } ?></textarea>
            </li>
        </ol>
    </fieldset>

    <fieldset>
        <?php if(isset($_POST['submit']) && isset($error[99])) { echo $error[99]; } ?>
        <?php
        //require_once('recaptchalib.php');
        $publickey = "6Lf83uYSAAAAAHLJGvvt3QAkVRRRKju0CKt_3AUx"; // you got this from the signup page
        echo recaptcha_get_html($publickey);
        ?>
    </fieldset>

    <fieldset class="submit">
        <input id="submit" type="submit" name="submit" value="Submit Your Resume">
    </fieldset>
    </form>
    </div>
    </div>
<?
}
?>
</body>
</html>