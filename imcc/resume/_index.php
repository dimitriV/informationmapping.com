<?php
include_once("includes/config.php");
include_once("includes/connect.php");
include("includes/header.php");

// eerst variabelen, dan errorchack
$program = "Information Mapping Certified Consultant Program";
$firstname = htmlspecialchars($_POST['firstname'], ENT_QUOTES);
$lastname = htmlspecialchars($_POST['lastname'], ENT_QUOTES);
$email = htmlspecialchars($_POST['email'], ENT_QUOTES);
$address1 = htmlspecialchars($_POST['address1'], ENT_QUOTES);
$address2 = htmlspecialchars($_POST['address2'], ENT_QUOTES);
$city = htmlspecialchars($_POST['city'], ENT_QUOTES);
$postalcode = htmlspecialchars($_POST['postalcode'], ENT_QUOTES);
$state = htmlspecialchars($_POST['state'], ENT_QUOTES);
$country = htmlspecialchars($_POST['country'], ENT_QUOTES);
$telephone = htmlspecialchars($_POST['telephone'], ENT_QUOTES);
$website = htmlspecialchars($_POST['website'], ENT_QUOTES);

$profsummary = htmlspecialchars($_POST['profsummary'], ENT_QUOTES);
$profsummary=nl2br($profsummary);
$specialties = htmlspecialchars($_POST['specialties'], ENT_QUOTES);
$title_job0 = htmlspecialchars($_POST['title_job0'], ENT_QUOTES);
$company_job0 = htmlspecialchars($_POST['company_job0'], ENT_QUOTES);
$startmonth_job0 = htmlspecialchars($_POST['startmonth_job0'], ENT_QUOTES);
$startyear_job0 = htmlspecialchars($_POST['startyear_job0'], ENT_QUOTES);
$description_job0 = htmlspecialchars($_POST['description_job0'], ENT_QUOTES);
$description_job0=nl2br($description_job0);
$title_job1 = htmlspecialchars($_POST['title_job1'], ENT_QUOTES);
$company_job1 = htmlspecialchars($_POST['company_job1'], ENT_QUOTES);
$startmonth_job1 = htmlspecialchars($_POST['startmonth_job1'], ENT_QUOTES);
$startyear_job1 = htmlspecialchars($_POST['startyear_job1'], ENT_QUOTES);
$endmonth_job1 = htmlspecialchars($_POST['endmonth_job1'], ENT_QUOTES);
$endyear_job1 = htmlspecialchars($_POST['endyear_job1'], ENT_QUOTES);
$description_job1 = htmlspecialchars($_POST['description_job1'], ENT_QUOTES);
$description_job1=nl2br($description_job1);

$followedtraining = htmlspecialchars($_POST['followedtraining']);
$coursetitle_imap = htmlspecialchars($_POST['coursetitle_imap'], ENT_QUOTES);
$company_imap = htmlspecialchars($_POST['company_imap'], ENT_QUOTES);
$month_imap = htmlspecialchars($_POST['month_imap'], ENT_QUOTES);
$year_imap = htmlspecialchars($_POST['year_imap'], ENT_QUOTES);
$trainer_imap = htmlspecialchars($_POST['trainer_imap'], ENT_QUOTES);
$comments_imap = htmlspecialchars($_POST['comments_imap'], ENT_QUOTES);	
$comments_imap=nl2br($comments_imap);
$coursetitle0 = htmlspecialchars($_POST['coursetitle0'], ENT_QUOTES);
$company0 = htmlspecialchars($_POST['company0'], ENT_QUOTES);
$month0 = htmlspecialchars($_POST['month0'], ENT_QUOTES);
$year0 = htmlspecialchars($_POST['year0'], ENT_QUOTES);
$comments0 = htmlspecialchars($_POST['comments0'], ENT_QUOTES);	
$comments0=nl2br($comments0);
$coursetitle1 = htmlspecialchars($_POST['coursetitle1'], ENT_QUOTES);
$company1 = htmlspecialchars($_POST['company1'], ENT_QUOTES);
$month1 = htmlspecialchars($_POST['month1'], ENT_QUOTES);
$year1 = htmlspecialchars($_POST['year1'], ENT_QUOTES);
$comments1 = htmlspecialchars($_POST['comments1'], ENT_QUOTES);	
$comments1=nl2br($comments1);
  
if($_FILES['certificate_imap']['size']>0){
  
$source=$_FILES['certificate_imap']['tmp_name'];
$certificate_name=time() . $_FILES['certificate_imap']['name'];
$dest="certificates/" . $certificate_name;
$check1=copy($source,$dest);
}

// errorcheck

function check_email($email) { 
    $pattern = "/^[\w-]+(\.[\w-]+)*@"; 
    $pattern .= "([0-9a-z][0-9a-z-]*[0-9a-z]\.)+([a-z]{2,4})$/i"; 
    if (preg_match($pattern, $email)) { 
        $parts = explode("@", $email); 
        if (checkdnsrr($parts[1], "MX")){ 
            // echo "The e-mail address is valid."; 
            return true; 
        } else { 
            return false; 
        } 
    } else { 
        return false; 
    } 
}
	
	if(empty($firstname)) {
	  $error[1] = "<br><span class=\"error\">Please fill out your first name.</span>";
	}
	
	if(empty($lastname)) {
	  $error[2] = "<br><span class=\"error\">Please fill out your last name.</span>";
	}
	
	if(!check_email($email)) {
	  $error[3] = "<br><span class=\"error\">Please fill out a valid e-mail address.</span>";
	}
	
	if(empty($address1)) {
	  $error[4] = "<br><span class=\"error\">Please fill out your address.</span>";
	}
	
	if(empty($city)) {
	  $error[5] = "<br><span class=\"error\">Please fill out your city.</span>";
	}
	
	if(empty($postalcode)) {
	  $error[6] = "<br><span class=\"error\">Please fill out your postal code / ZIP.</span>";
	}
	
	if($country=="") {
	  $error[7] = "<br><span class=\"error\">Please select your country.</span>";
	}
	
	if(($country=="US") && empty($postalcode)) {
	  $error[8] = "<br><span class=\"error\">Please fill out your state.</span>";
	}
	
	if(empty($profsummary)) {
	  $error[9] = "<br><span class=\"error\">Please summarize your professional experience.</span>";
	}
	
	if(empty($specialties)) {
	  $error[10] = "<br><span class=\"error\">Please fill out your specialties.</span>";
	}
	
	if(empty($company_job0)) {
	  $error[11] = "<br><span class=\"error\">Please fill out your company.</span>";
	}
	
	if(empty($title_job0)) {
	  $error[12] = "<br><span class=\"error\">Please fill out your position.</span>";
	}
	
	if($startmonth_job0=="- Month -") {
	  $error[13] = "<br><span class=\"error\">Please select a month.</span>";
	}
	
	
	if($startyear_job0=="- Year -") {
	  $error[14] = "<br><span class=\"error\">Please select a year.</span>";
	}
	
	if(empty($description_job0)) {
	  $error[15] = "<br><span class=\"error\">Please provide a short description of your current job.</span>";
	}
	
	$resume_SQL="SELECT email FROM resume";
	$resume_result=mysql_query($resume_SQL) or die(mysql_error()); 
	while($resume=mysql_fetch_array($resume_result)){
	
	if($email==$resume[email]) {
	  $error[16] = "<br><span class=\"error\">This e-mail address already exists in our database.</span>";
	}
	}
	
	// maximale bestandsgrootte in bytes
	$allowed_size = 2000*1024; // Grootte in bytes > 2000*1024 = 2000 kB


	$size = $_FILES['certificate_imap']['size'];

	
	if(!empty($certificate_name)) 
	{
   		// Bestandsgrootte controleren
   		if($size > $allowed_size)
      		$error[17] = "<br><span class=\"error\">The certificate can be max. 2 MB</span>";
   
   		// Extensie en mime controleren
   		if ($_FILES['certificate_imap']['type'] != "application/pdf")
    $error[18] = "<br><span class=\"error\">Please upload your certificate in PDF format</span>";
	}
	
	if(empty($telephone)) {
	  $error[19] = "<br><span class=\"error\">Please fill out your telephone number.</span>";
	}
	
	if(empty($website)) {
	  $error[20] = "<br><span class=\"error\">Please fill out the URL of your company's website.</span>";
	}
	
	if($program=="- Select your program -") {
	  $error[21] = "<br><span class=\"error\">Please select a program.</span>";
	}
	
	
/*
if(!empty($company_job1) || !empty($title_job1)) {
	
	if(empty($company_job1)) {
	  $error[16] = "<br><span class=\"error\">Please fill out your company.</span>";
	}
	
	if(empty($title_job1)) {
	  $error[17] = "<br><span class=\"error\">Please fill out your position.</span>";
	}
	
	if($startmonth_job1=="- Month -") {
	  $error[18] = "<br><span class=\"error\">Please select a month.</span>";
	}
	
	if($startyear_job1=="- Year -") {
	  $error[19] = "<br><span class=\"error\">Please select a year.</span>";
	}
	
	if($endmonth_job1=="- Month -") {
	  $error[20] = "<br><span class=\"error\">Please select a month.</span>";
	}
	
	if($endyear_job1=="- Year -") {
	  $error[21] = "<br><span class=\"error\">Please select a year.</span>";
	}
	
	if(empty($description_job1)) {
	  $error[22] = "<br><span class=\"error\">Please provide a short description of your current job.</span>";
	}

}


	if(empty($coursetitle_imap)) {
	  $error[23] = "<br><span class=\"error\">Please fill out the name of the course you followed.</span>";
	}
	
	if(empty($company_imap)) {
	  $error[24] = "<br><span class=\"error\">Please fill out the company that provided the training.</span>";
	}
	
	if($month_imap=="- Month -") {
	  $error[25] = "<br><span class=\"error\">Please select a month.</span>";
	}
	
	
	if($year_imap=="- Year -") {
	  $error[26] = "<br><span class=\"error\">Please select a year.</span>";
	}
	
	if(empty($trainer_imap)) {
	  $error[27] = "<br><span class=\"error\">Please fill out the name of the instructor.</span>";
	}
	
	if(empty($certificate_name)) {
		$error[28] = "<br><span class=\"error\">Please upload your certificate.</span>";
	}
	
*/



require_once('recaptchalib.php');
  $privatekey = "6Lf83uYSAAAAAB54WFqK9ZlVJtgiEeY0PLtgYGKl";
  $resp = recaptcha_check_answer ($privatekey,
                                $_SERVER["REMOTE_ADDR"],
                                $_POST["recaptcha_challenge_field"],
                                $_POST["recaptcha_response_field"]);

  if (!$resp->is_valid) {
    // What happens when the CAPTCHA was entered incorrectly
    $error[99] = "<span class=\"error\">The reCAPTCHA wasn't entered correctly. Go back and try it again.</span>";
  } 


	

// indien alles ok, dan toevoegen aan DB
if(isset($_POST['submit']) && !isset($error)) {
	
  $resume_SQL_insert="INSERT INTO resume (program, telephone, website,resumedate,firstname,lastname,email,address1,address2,city,postalcode,state,country,profsummary,specialties,title_job0,company_job0,startmonth_job0,startyear_job0,description_job0,title_job1,company_job1,startmonth_job1,startyear_job1,endmonth_job1,endyear_job1,description_job1,followedtraining,coursetitle_imap,company_imap,month_imap,year_imap,trainer_imap,certificate_imap,comments_imap,coursetitle0,company0,month0,year0,comments0,coursetitle1,company1,month1,year1,comments1) VALUES ('$program', '$telephone','$website',NOW(),'$firstname','$lastname','$email','$address1','$address2','$city','$postalcode','$state','$country','$profsummary','$specialties','$title_job0','$company_job0','$startmonth_job0','$startyear_job0','$description_job0','$title_job1','$company_job1','$startmonth_job1','$startyear_job1','$endmonth_job1','$endyear_job1','$description_job1','$followedtraining','$coursetitle_imap','$company_imap','$month_imap','$year_imap','$trainer_imap','$certificate_name','$comments_imap','$coursetitle0','$company0','$month0','$year0','$comments0','$coursetitle1','$company1','$month1','$year1','$comments1')";
  $bool=mysql_query($resume_SQL_insert);
  if($bool<>1) echo "There was an error adding the resume to the database.";
  if($bool==1) {

?>

<h1><?php echo $program; ?></h1>
<div id="content">
<h2>Thank You!</h2>
<p>Dear <?php echo $firstname; ?>,</p>
<p>Thank you for completing your resume. You will receive a confirmation mail upon receipt of your resume.</p>
<h2>What's Next?</h2>
<p>We will evaluate your resume and provide feedback within 5 business days.</p>
<p>Should you have any questions in the mean time, please contact us at <a href="mailto:info@informationmapping.com">info@informationmapping.com</a>.</p>
<p>With kind regards,<br />
Information Mapping International</p>
</div>
<?

$mime_boundary="==Multipart_Boundary_x".md5(mt_rand())."x";

	$message  = "<table cellpadding='0' cellspacing='0' border='0' width='100%'><tr><td align='center'>";
	$message .= "<table cellpadding='0' cellspacing='0' border='0' width='500'><tr><td align='left'>";
	$message .= "<p><img src='http://www.informationmapping.com/imcc/resume/images/logo_email.gif' alt='Information Mapping logo' /></p>";
	$message .= "<p><font size='4' face='Arial, Helvetica, sans-serif' color=#009edf'>$program</font></p>";
	$message .= "<p><font size='2' face='Arial, Helvetica, sans-serif'>Dear $firstname,</font></p>";
	$message .= "<p><font size='2' face='Arial, Helvetica, sans-serif'>Thank you for completing your resume.<br />";
	$message .= "<font size='2' face='Arial, Helvetica, sans-serif'>We will evaluate your resume and provide feedback within 5 business days.</font></p>";
	$message .= "<p><font size='2' face='Arial, Helvetica, sans-serif'>With kind regards,</font><br />";
	$message .= "<font size='2' face='Arial, Helvetica, sans-serif'>Information Mapping International</font></p>";
	$message .= "</td></tr></table></td></tr></table>";
	
	$headers = "From: Information Mapping International <info@informationmapping.com>\n";
	$headers .= "BCC: Filip Vanlerberghe <fvanlerberghe@informationmapping.com>\n";
	$headers .= "BCC: Veronique Wittebolle <vwittebolle@informationmapping.com>\n";
	$headers .= "BCC: Francis Declercq <fdeclercq@informationmapping.com>\n";
	$headers .= "BCC: Wim Van Dessel <wvandessel@informationmapping.com>\n";
	$headers .= "X-Priority: 1\n";  	  
	$headers .= "Content-Type: multipart/mixed;\n" . " boundary=\"{$mime_boundary}\"\n" . "MIME-Version: 1.0\n"; 
	
	if ($program=="Information Mapping Certified Consultant Program") {
	$subject="IMCC Program - Thank you for completing your resume";
	}
	
	if ($program=="Information Mapping Certified Professional Program") {
	$subject="IMCP Program - Thank you for completing your resume";
	}

	$message = "This is a multi-part message in MIME format.\n\n" .
         "--{$mime_boundary}\n" .
         "Content-Type: text/html; charset=\"iso-8859-1\"\n" .
         "Content-Transfer-Encoding: 7bit\n\n" . $message . "\n\n";
		 
    $message .="--{$mime_boundary}--\n";
    mail($email,$subject, $message, $headers); 
}
}else{

?>
<h1>Information Mapping Certified Consultant Program</h1>
<div id="content">
<h2>Complete Your Resume</h2>
<p>Please fill out your resume below.</p>
  <form action="<?php echo $_SERVER['PHP_SELF']; ?>" method="post" enctype="multipart/form-data">
  <div style="display:none;">
  <fieldset>
  <legend>Choose Your Program</legend>
  <ol>
  <li>
<label for="program">I am applying for the</label>
<select name="program">
        <option>- Select your program -</option>
        <option <?php if(($_POST['program'])=="Information Mapping Certified Consultant Program") { echo "selected='selected'";} ?>>Information Mapping Certified Consultant Program</option>
        <option <?php if(($_POST['program'])=="Information Mapping Certified Professional Program") { echo "selected='selected'";} ?>>Information Mapping Certified Professional Program</option>
        </select>
        <?php if(isset($_POST['submit']) && isset($error[21])) { echo $error[21]; } ?>
   </li>
   <li>
        <label>&nbsp;</label>
<a href="http://www.informationmapping.com/en/imcc-overview/certification-programs-overview/" target="_blank">Not sure which one to choose?</a>
        
  </li>
  </ol>
  </fieldset>
  </div>
  <fieldset>
  
  <legend>About You</legend>
<ol>
<li>
<label for="firstname">First name</label>
<input id="firstname" type="text" name="firstname" size="46" value="<?php if(!empty($_POST['firstname'])) { echo $_POST['firstname']; } ?>"> 
<?php if(isset($_POST['submit']) && isset($error[1])) { echo $error[1]; } ?>
</li>
<li>
<label for="lastname">Last name</label>
<input id="lastname" type="text" name="lastname" size="46" value="<?php if(!empty($_POST['lastname'])) { echo $_POST['lastname']; } ?>"> 
<?php if(isset($_POST['submit']) && isset($error[2])) { echo $error[2]; } ?>
</li>
<li>
<label for="email">E-mail address</label>
<input id="email" type="text" name="email" size="46" value="<?php if(!empty($_POST['email'])) { echo $_POST['email']; } ?>"> 
<?php if(isset($_POST['submit']) && isset($error[3])) { echo $error[3]; } ?>
<?php if(isset($_POST['submit']) && isset($error[16])) { echo $error[16]; } ?>
</li>
<li>
<label for="telephone">Telephone number</label>
<input id="telephone" type="text" name="telephone" size="46" value="<?php if(!empty($_POST['telephone'])) { echo $_POST['telephone']; } ?>"> 
<?php if(isset($_POST['submit']) && isset($error[19])) { echo $error[19]; } ?>
</li>
<li>
<label for="address1">Address</label>
<input id="address1" type="text" name="address1" size="46" value="<?php if(!empty($_POST['address1'])) { echo $_POST['address1']; } ?>"> 
<?php if(isset($_POST['submit']) && isset($error[4])) { echo $error[4]; } ?>
</li>
<li>
<label for="address2">&nbsp;</label>
<input id="address2" type="text" name="address2" size="46" value="<?php if(!empty($_POST['address2'])) { echo $_POST['address2']; } ?>">
</li>
<li>
<label for="city">City</label>
<input id="city" type="text" name="city" size="46" value="<?php if(!empty($_POST['city'])) { echo $_POST['city']; } ?>"> 
<?php if(isset($_POST['submit']) && isset($error[5])) { echo $error[5]; } ?>
</li>
<li>
<label for="postalcode">Postal code / ZIP</label>
<input id="postalcode" type="text" name="postalcode" size="46" value="<?php if(!empty($_POST['postalcode'])) { echo $_POST['postalcode']; } ?>"> 
<?php if(isset($_POST['submit']) && isset($error[6])) { echo $error[6]; } ?>
</li>
<li>
<label for="state">State</label>
<input id="state" type="text" name="state" size="46" value="<?php if(!empty($_POST['state'])) { echo $_POST['state']; } ?>">
<?php if(isset($_POST['submit']) && isset($error[8])) { echo $error[8]; } ?>
</li>
<li>
<label for="country">Country</label>
<select name="country" id="country">
<option value=""> </option>
<option value="AF" <?php if(($_POST['country'])=="AF") { echo "selected='selected'";} ?>>Afghanistan</option>
<option value="AL" <?php if(($_POST['country'])=="AL") { echo "selected='selected'";} ?>>Albania</option>
<option value="DZ" <?php if(($_POST['country'])=="DZ") { echo "selected='selected'";} ?>>Algeria</option>
<option value="AS" <?php if(($_POST['country'])=="AS") { echo "selected='selected'";} ?>>American Samoa</option>
<option value="AD" <?php if(($_POST['country'])=="AD") { echo "selected='selected'";} ?>>Andorra</option>
<option value="AO" <?php if(($_POST['country'])=="AO") { echo "selected='selected'";} ?>>Angola</option>
<option value="AI" <?php if(($_POST['country'])=="AI") { echo "selected='selected'";} ?>>Anguilla</option>
<option value="AQ" <?php if(($_POST['country'])=="AQ") { echo "selected='selected'";} ?>>Antarctica</option>
<option value="AG" <?php if(($_POST['country'])=="AG") { echo "selected='selected'";} ?>>Antigua and Barbuda</option>
<option value="AR" <?php if(($_POST['country'])=="AR") { echo "selected='selected'";} ?>>Argentina</option>
<option value="AM" <?php if(($_POST['country'])=="AM") { echo "selected='selected'";} ?>>Armenia</option>
<option value="AW" <?php if(($_POST['country'])=="AW") { echo "selected='selected'";} ?>>Aruba</option>
<option value="AU" <?php if(($_POST['country'])=="AU") { echo "selected='selected'";} ?>>Australia</option>
<option value="AT" <?php if(($_POST['country'])=="AT") { echo "selected='selected'";} ?>>Austria</option>
<option value="AZ" <?php if(($_POST['country'])=="AZ") { echo "selected='selected'";} ?>>Azerbaijan</option>
<option value="BS" <?php if(($_POST['country'])=="BS") { echo "selected='selected'";} ?>>Bahamas</option>
<option value="BH" <?php if(($_POST['country'])=="BH") { echo "selected='selected'";} ?>>Bahrain</option>
<option value="BD" <?php if(($_POST['country'])=="BD") { echo "selected='selected'";} ?>>Bangladesh</option>
<option value="BB" <?php if(($_POST['country'])=="BB") { echo "selected='selected'";} ?>>Barbados</option>
<option value="BY" <?php if(($_POST['country'])=="BY") { echo "selected='selected'";} ?>>Belarus</option>
<option value="BE" <?php if(($_POST['country'])=="BE") { echo "selected='selected'";} ?>>Belgium</option>
<option value="BZ" <?php if(($_POST['country'])=="BZ") { echo "selected='selected'";} ?>>Belize</option>
<option value="BJ" <?php if(($_POST['country'])=="BJ") { echo "selected='selected'";} ?>>Benin</option>
<option value="BM" <?php if(($_POST['country'])=="BM") { echo "selected='selected'";} ?>>Bermuda</option>
<option value="BT" <?php if(($_POST['country'])=="BT") { echo "selected='selected'";} ?>>Bhutan</option>
<option value="BO" <?php if(($_POST['country'])=="BO") { echo "selected='selected'";} ?>>Bolivia</option>
<option value="BA" <?php if(($_POST['country'])=="BA") { echo "selected='selected'";} ?>>Bosnia and Herzegovina</option>
<option value="BW" <?php if(($_POST['country'])=="BW") { echo "selected='selected'";} ?>>Botswana</option>
<option value="BV" <?php if(($_POST['country'])=="BV") { echo "selected='selected'";} ?>>Bouvet Island</option>
<option value="BR" <?php if(($_POST['country'])=="BR") { echo "selected='selected'";} ?>>Brazil</option>
<option value="IO" <?php if(($_POST['country'])=="IO") { echo "selected='selected'";} ?>>British Indian Ocean Territory</option>
<option value="VG" <?php if(($_POST['country'])=="VG") { echo "selected='selected'";} ?>>British Virgin Islands</option>
<option value="BN" <?php if(($_POST['country'])=="BN") { echo "selected='selected'";} ?>>Brunei</option>
<option value="BG" <?php if(($_POST['country'])=="BG") { echo "selected='selected'";} ?>>Bulgaria</option>
<option value="BF" <?php if(($_POST['country'])=="BF") { echo "selected='selected'";} ?>>Burkina Faso</option>
<option value="BI" <?php if(($_POST['country'])=="BI") { echo "selected='selected'";} ?>>Burundi</option>
<option value="KH" <?php if(($_POST['country'])=="KH") { echo "selected='selected'";} ?>>Cambodia</option>
<option value="CM" <?php if(($_POST['country'])=="CM") { echo "selected='selected'";} ?>>Cameroon</option>
<option value="CA" <?php if(($_POST['country'])=="CA") { echo "selected='selected'";} ?>>Canada</option>
<option value="CV" <?php if(($_POST['country'])=="CV") { echo "selected='selected'";} ?>>Cape Verde</option>
<option value="KY" <?php if(($_POST['country'])=="KY") { echo "selected='selected'";} ?>>Cayman Islands</option>
<option value="CF" <?php if(($_POST['country'])=="CF") { echo "selected='selected'";} ?>>Central African Republic</option>
<option value="TD" <?php if(($_POST['country'])=="TD") { echo "selected='selected'";} ?>>Chad</option>
<option value="CL" <?php if(($_POST['country'])=="CL") { echo "selected='selected'";} ?>>Chile</option>
<option value="CN" <?php if(($_POST['country'])=="CN") { echo "selected='selected'";} ?>>China</option>
<option value="CX" <?php if(($_POST['country'])=="CX") { echo "selected='selected'";} ?>>Christmas Island</option>
<option value="CC" <?php if(($_POST['country'])=="CC") { echo "selected='selected'";} ?>>Cocos [Keeling] Islands</option>
<option value="CO" <?php if(($_POST['country'])=="CO") { echo "selected='selected'";} ?>>Colombia</option>
<option value="KM" <?php if(($_POST['country'])=="KM") { echo "selected='selected'";} ?>>Comoros</option>
<option value="CG" <?php if(($_POST['country'])=="CG") { echo "selected='selected'";} ?>>Congo - Brazzaville</option>
<option value="CD" <?php if(($_POST['country'])=="CD") { echo "selected='selected'";} ?>>Congo - Kinshasa</option>
<option value="CK" <?php if(($_POST['country'])=="CK") { echo "selected='selected'";} ?>>Cook Islands</option>
<option value="CR" <?php if(($_POST['country'])=="CR") { echo "selected='selected'";} ?>>Costa Rica</option>
<option value="HR" <?php if(($_POST['country'])=="HR") { echo "selected='selected'";} ?>>Croatia</option>
<option value="CU" <?php if(($_POST['country'])=="CU") { echo "selected='selected'";} ?>>Cuba</option>
<option value="CY" <?php if(($_POST['country'])=="CY") { echo "selected='selected'";} ?>>Cyprus</option>
<option value="CZ" <?php if(($_POST['country'])=="CZ") { echo "selected='selected'";} ?>>Czech Republic</option>
<option value="CI" <?php if(($_POST['country'])=="CI") { echo "selected='selected'";} ?>>Ivory Coast</option>
<option value="DK" <?php if(($_POST['country'])=="DK") { echo "selected='selected'";} ?>>Denmark</option>
<option value="DJ" <?php if(($_POST['country'])=="DJ") { echo "selected='selected'";} ?>>Djibouti</option>
<option value="DM" <?php if(($_POST['country'])=="DM") { echo "selected='selected'";} ?>>Dominica</option>
<option value="DO" <?php if(($_POST['country'])=="DO") { echo "selected='selected'";} ?>>Dominican Republic</option>
<option value="EC" <?php if(($_POST['country'])=="EC") { echo "selected='selected'";} ?>>Ecuador</option>
<option value="EG" <?php if(($_POST['country'])=="EG") { echo "selected='selected'";} ?>>Egypt</option>
<option value="SV" <?php if(($_POST['country'])=="SV") { echo "selected='selected'";} ?>>El Salvador</option>
<option value="GQ" <?php if(($_POST['country'])=="GQ") { echo "selected='selected'";} ?>>Equatorial Guinea</option>
<option value="ER" <?php if(($_POST['country'])=="ER") { echo "selected='selected'";} ?>>Eritrea</option>
<option value="EE" <?php if(($_POST['country'])=="EE") { echo "selected='selected'";} ?>>Estonia</option>
<option value="ET" <?php if(($_POST['country'])=="ET") { echo "selected='selected'";} ?>>Ethiopia</option>
<option value="FK" <?php if(($_POST['country'])=="FK") { echo "selected='selected'";} ?>>Falkland Islands</option>
<option value="FO" <?php if(($_POST['country'])=="FO") { echo "selected='selected'";} ?>>Faroe Islands</option>
<option value="FJ" <?php if(($_POST['country'])=="FJ") { echo "selected='selected'";} ?>>Fiji</option>
<option value="FI" <?php if(($_POST['country'])=="FI") { echo "selected='selected'";} ?>>Finland</option>
<option value="FR" <?php if(($_POST['country'])=="FR") { echo "selected='selected'";} ?>>France</option>
<option value="GF" <?php if(($_POST['country'])=="GF") { echo "selected='selected'";} ?>>French Guiana</option>
<option value="PF" <?php if(($_POST['country'])=="PF") { echo "selected='selected'";} ?>>French Polynesia</option>
<option value="TF" <?php if(($_POST['country'])=="TF") { echo "selected='selected'";} ?>>French Southern Territories</option>
<option value="GA" <?php if(($_POST['country'])=="GA") { echo "selected='selected'";} ?>>Gabon</option>
<option value="GM" <?php if(($_POST['country'])=="GM") { echo "selected='selected'";} ?>>Gambia</option>
<option value="GE" <?php if(($_POST['country'])=="GE") { echo "selected='selected'";} ?>>Georgia</option>
<option value="DE" <?php if(($_POST['country'])=="DE") { echo "selected='selected'";} ?>>Germany</option>
<option value="GH" <?php if(($_POST['country'])=="GH") { echo "selected='selected'";} ?>>Ghana</option>
<option value="GI" <?php if(($_POST['country'])=="GI") { echo "selected='selected'";} ?>>Gibraltar</option>
<option value="GR" <?php if(($_POST['country'])=="GR") { echo "selected='selected'";} ?>>Greece</option>
<option value="GL" <?php if(($_POST['country'])=="GL") { echo "selected='selected'";} ?>>Greenland</option>
<option value="GD" <?php if(($_POST['country'])=="GD") { echo "selected='selected'";} ?>>Grenada</option>
<option value="GP" <?php if(($_POST['country'])=="GP") { echo "selected='selected'";} ?>>Guadeloupe</option>
<option value="GU" <?php if(($_POST['country'])=="GU") { echo "selected='selected'";} ?>>Guam</option>
<option value="GT" <?php if(($_POST['country'])=="GT") { echo "selected='selected'";} ?>>Guatemala</option>
<option value="GG" <?php if(($_POST['country'])=="GG") { echo "selected='selected'";} ?>>Guernsey</option>
<option value="GN" <?php if(($_POST['country'])=="GN") { echo "selected='selected'";} ?>>Guinea</option>
<option value="GW" <?php if(($_POST['country'])=="GW") { echo "selected='selected'";} ?>>Guinea-Bissau</option>
<option value="GY" <?php if(($_POST['country'])=="GY") { echo "selected='selected'";} ?>>Guyana</option>
<option value="HT" <?php if(($_POST['country'])=="HT") { echo "selected='selected'";} ?>>Haiti</option>
<option value="HM" <?php if(($_POST['country'])=="HM") { echo "selected='selected'";} ?>>Heard Island and McDonald Islands</option>
<option value="HN" <?php if(($_POST['country'])=="HN") { echo "selected='selected'";} ?>>Honduras</option>
<option value="HK" <?php if(($_POST['country'])=="HK") { echo "selected='selected'";} ?>>Hong Kong SAR China</option>
<option value="HU" <?php if(($_POST['country'])=="HU") { echo "selected='selected'";} ?>>Hungary</option>
<option value="IS" <?php if(($_POST['country'])=="IS") { echo "selected='selected'";} ?>>Iceland</option>
<option value="IN" <?php if(($_POST['country'])=="IN") { echo "selected='selected'";} ?>>India</option>
<option value="ID" <?php if(($_POST['country'])=="ID") { echo "selected='selected'";} ?>>Indonesia</option>
<option value="IR" <?php if(($_POST['country'])=="IR") { echo "selected='selected'";} ?>>Iran</option>
<option value="IQ" <?php if(($_POST['country'])=="IQ") { echo "selected='selected'";} ?>>Iraq</option>
<option value="IE" <?php if(($_POST['country'])=="IE") { echo "selected='selected'";} ?>>Ireland</option>
<option value="IM" <?php if(($_POST['country'])=="IM") { echo "selected='selected'";} ?>>Isle of Man</option>
<option value="IL" <?php if(($_POST['country'])=="IL") { echo "selected='selected'";} ?>>Israel</option>
<option value="IT" <?php if(($_POST['country'])=="IT") { echo "selected='selected'";} ?>>Italy</option>
<option value="JM" <?php if(($_POST['country'])=="JM") { echo "selected='selected'";} ?>>Jamaica</option>
<option value="JP" <?php if(($_POST['country'])=="JP") { echo "selected='selected'";} ?>>Japan</option>
<option value="JE" <?php if(($_POST['country'])=="JE") { echo "selected='selected'";} ?>>Jersey</option>
<option value="JO" <?php if(($_POST['country'])=="JO") { echo "selected='selected'";} ?>>Jordan</option>
<option value="KZ" <?php if(($_POST['country'])=="KZ") { echo "selected='selected'";} ?>>Kazakhstan</option>
<option value="KE" <?php if(($_POST['country'])=="KE") { echo "selected='selected'";} ?>>Kenya</option>
<option value="KI" <?php if(($_POST['country'])=="KI") { echo "selected='selected'";} ?>>Kiribati</option>
<option value="KW" <?php if(($_POST['country'])=="KW") { echo "selected='selected'";} ?>>Kuwait</option>
<option value="KG" <?php if(($_POST['country'])=="KG") { echo "selected='selected'";} ?>>Kyrgyzstan</option>
<option value="LA" <?php if(($_POST['country'])=="LA") { echo "selected='selected'";} ?>>Laos</option>
<option value="LV" <?php if(($_POST['country'])=="LV") { echo "selected='selected'";} ?>>Latvia</option>
<option value="LB" <?php if(($_POST['country'])=="LB") { echo "selected='selected'";} ?>>Lebanon</option>
<option value="LS" <?php if(($_POST['country'])=="LS") { echo "selected='selected'";} ?>>Lesotho</option>
<option value="LR" <?php if(($_POST['country'])=="LR") { echo "selected='selected'";} ?>>Liberia</option>
<option value="LY" <?php if(($_POST['country'])=="LY") { echo "selected='selected'";} ?>>Libya</option>
<option value="LI" <?php if(($_POST['country'])=="LI") { echo "selected='selected'";} ?>>Liechtenstein</option>
<option value="LT" <?php if(($_POST['country'])=="LT") { echo "selected='selected'";} ?>>Lithuania</option>
<option value="LU" <?php if(($_POST['country'])=="LU") { echo "selected='selected'";} ?>>Luxembourg</option>
<option value="MO" <?php if(($_POST['country'])=="MO") { echo "selected='selected'";} ?>>Macau SAR China</option>
<option value="MK" <?php if(($_POST['country'])=="MK") { echo "selected='selected'";} ?>>Macedonia</option>
<option value="MG" <?php if(($_POST['country'])=="MG") { echo "selected='selected'";} ?>>Madagascar</option>
<option value="MW" <?php if(($_POST['country'])=="MW") { echo "selected='selected'";} ?>>Malawi</option>
<option value="MY" <?php if(($_POST['country'])=="MY") { echo "selected='selected'";} ?>>Malaysia</option>
<option value="MV" <?php if(($_POST['country'])=="MV") { echo "selected='selected'";} ?>>Maldives</option>
<option value="ML" <?php if(($_POST['country'])=="ML") { echo "selected='selected'";} ?>>Mali</option>
<option value="MT" <?php if(($_POST['country'])=="MT") { echo "selected='selected'";} ?>>Malta</option>
<option value="MH" <?php if(($_POST['country'])=="MH") { echo "selected='selected'";} ?>>Marshall Islands</option>
<option value="MQ" <?php if(($_POST['country'])=="MQ") { echo "selected='selected'";} ?>>Martinique</option>
<option value="MR" <?php if(($_POST['country'])=="MR") { echo "selected='selected'";} ?>>Mauritania</option>
<option value="MU" <?php if(($_POST['country'])=="MU") { echo "selected='selected'";} ?>>Mauritius</option>
<option value="YT" <?php if(($_POST['country'])=="YT") { echo "selected='selected'";} ?>>Mayotte</option>
<option value="MX" <?php if(($_POST['country'])=="MX") { echo "selected='selected'";} ?>>Mexico</option>
<option value="FM" <?php if(($_POST['country'])=="FM") { echo "selected='selected'";} ?>>Micronesia</option>
<option value="MD" <?php if(($_POST['country'])=="MD") { echo "selected='selected'";} ?>>Moldova</option>
<option value="MC" <?php if(($_POST['country'])=="MC") { echo "selected='selected'";} ?>>Monaco</option>
<option value="MN" <?php if(($_POST['country'])=="MN") { echo "selected='selected'";} ?>>Mongolia</option>
<option value="ME" <?php if(($_POST['country'])=="ME") { echo "selected='selected'";} ?>>Montenegro</option>
<option value="MS" <?php if(($_POST['country'])=="MS") { echo "selected='selected'";} ?>>Montserrat</option>
<option value="MA" <?php if(($_POST['country'])=="MA") { echo "selected='selected'";} ?>>Morocco</option>
<option value="MZ" <?php if(($_POST['country'])=="MZ") { echo "selected='selected'";} ?>>Mozambique</option>
<option value="MM" <?php if(($_POST['country'])=="MM") { echo "selected='selected'";} ?>>Myanmar [Burma]</option>
<option value="NA" <?php if(($_POST['country'])=="NA") { echo "selected='selected'";} ?>>Namibia</option>
<option value="NR" <?php if(($_POST['country'])=="NR") { echo "selected='selected'";} ?>>Nauru</option>
<option value="NP" <?php if(($_POST['country'])=="NP") { echo "selected='selected'";} ?>>Nepal</option>
<option value="NL" <?php if(($_POST['country'])=="NL") { echo "selected='selected'";} ?>>Netherlands</option>
<option value="AN" <?php if(($_POST['country'])=="AN") { echo "selected='selected'";} ?>>Netherlands Antilles</option>
<option value="NC" <?php if(($_POST['country'])=="NC") { echo "selected='selected'";} ?>>New Caledonia</option>
<option value="NZ" <?php if(($_POST['country'])=="NZ") { echo "selected='selected'";} ?>>New Zealand</option>
<option value="NI" <?php if(($_POST['country'])=="NI") { echo "selected='selected'";} ?>>Nicaragua</option>
<option value="NE" <?php if(($_POST['country'])=="NE") { echo "selected='selected'";} ?>>Niger</option>
<option value="NG" <?php if(($_POST['country'])=="NG") { echo "selected='selected'";} ?>>Nigeria</option>
<option value="NU" <?php if(($_POST['country'])=="NU") { echo "selected='selected'";} ?>>Niue</option>
<option value="NF" <?php if(($_POST['country'])=="NF") { echo "selected='selected'";} ?>>Norfolk Island</option>
<option value="KP" <?php if(($_POST['country'])=="KP") { echo "selected='selected'";} ?>>North Korea</option>
<option value="MP" <?php if(($_POST['country'])=="MP") { echo "selected='selected'";} ?>>Northern Mariana Islands</option>
<option value="NO" <?php if(($_POST['country'])=="NO") { echo "selected='selected'";} ?>>Norway</option>
<option value="OM" <?php if(($_POST['country'])=="CM") { echo "selected='selected'";} ?>>Oman</option>
<option value="PK" <?php if(($_POST['country'])=="PK") { echo "selected='selected'";} ?>>Pakistan</option>
<option value="PW" <?php if(($_POST['country'])=="PW") { echo "selected='selected'";} ?>>Palau</option>
<option value="PS" <?php if(($_POST['country'])=="PS") { echo "selected='selected'";} ?>>Palestinian Territories</option>
<option value="PA" <?php if(($_POST['country'])=="PA") { echo "selected='selected'";} ?>>Panama</option>
<option value="PG" <?php if(($_POST['country'])=="PG") { echo "selected='selected'";} ?>>Papua New Guinea</option>
<option value="PY" <?php if(($_POST['country'])=="PY") { echo "selected='selected'";} ?>>Paraguay</option>
<option value="PE" <?php if(($_POST['country'])=="PE") { echo "selected='selected'";} ?>>Peru</option>
<option value="PH" <?php if(($_POST['country'])=="PH") { echo "selected='selected'";} ?>>Philippines</option>
<option value="PN" <?php if(($_POST['country'])=="PN") { echo "selected='selected'";} ?>>Pitcairn Islands</option>
<option value="PL" <?php if(($_POST['country'])=="PL") { echo "selected='selected'";} ?>>Poland</option>
<option value="PT" <?php if(($_POST['country'])=="PT") { echo "selected='selected'";} ?>>Portugal</option>
<option value="PR" <?php if(($_POST['country'])=="PR") { echo "selected='selected'";} ?>>Puerto Rico</option>
<option value="QA" <?php if(($_POST['country'])=="QA") { echo "selected='selected'";} ?>>Qatar</option>
<option value="RO" <?php if(($_POST['country'])=="RO") { echo "selected='selected'";} ?>>Romania</option>
<option value="RU" <?php if(($_POST['country'])=="RU") { echo "selected='selected'";} ?>>Russia</option>
<option value="RW" <?php if(($_POST['country'])=="RW") { echo "selected='selected'";} ?>>Rwanda</option>
<option value="RE" <?php if(($_POST['country'])=="RE") { echo "selected='selected'";} ?>>Reunion</option>
<option value="BL" <?php if(($_POST['country'])=="BL") { echo "selected='selected'";} ?>>Saint Barthelemy</option>
<option value="SH" <?php if(($_POST['country'])=="SH") { echo "selected='selected'";} ?>>Saint Helena</option>
<option value="KN" <?php if(($_POST['country'])=="KN") { echo "selected='selected'";} ?>>Saint Kitts and Nevis</option>
<option value="LC" <?php if(($_POST['country'])=="LC") { echo "selected='selected'";} ?>>Saint Lucia</option>
<option value="MF" <?php if(($_POST['country'])=="MF") { echo "selected='selected'";} ?>>Saint Martin</option>
<option value="PM" <?php if(($_POST['country'])=="PM") { echo "selected='selected'";} ?>>Saint Pierre and Miquelon</option>
<option value="VC" <?php if(($_POST['country'])=="VC") { echo "selected='selected'";} ?>>Saint Vincent and the Grenadines</option>
<option value="WS" <?php if(($_POST['country'])=="WS") { echo "selected='selected'";} ?>>Samoa</option>
<option value="SM" <?php if(($_POST['country'])=="SM") { echo "selected='selected'";} ?>>San Marino</option>
<option value="SA" <?php if(($_POST['country'])=="SA") { echo "selected='selected'";} ?>>Saudi Arabia</option>
<option value="SN" <?php if(($_POST['country'])=="SN") { echo "selected='selected'";} ?>>Senegal</option>
<option value="RS" <?php if(($_POST['country'])=="RS") { echo "selected='selected'";} ?>>Serbia</option>
<option value="SC" <?php if(($_POST['country'])=="SC") { echo "selected='selected'";} ?>>Seychelles</option>
<option value="SL" <?php if(($_POST['country'])=="SL") { echo "selected='selected'";} ?>>Sierra Leone</option>
<option value="SG" <?php if(($_POST['country'])=="SG") { echo "selected='selected'";} ?>>Singapore</option>
<option value="SK" <?php if(($_POST['country'])=="SK") { echo "selected='selected'";} ?>>Slovakia</option>
<option value="SI" <?php if(($_POST['country'])=="SI") { echo "selected='selected'";} ?>>Slovenia</option>
<option value="SB" <?php if(($_POST['country'])=="SB") { echo "selected='selected'";} ?>>Solomon Islands</option>
<option value="SO" <?php if(($_POST['country'])=="SO") { echo "selected='selected'";} ?>>Somalia</option>
<option value="ZA" <?php if(($_POST['country'])=="ZA") { echo "selected='selected'";} ?>>South Africa</option>
<option value="GS" <?php if(($_POST['country'])=="GS") { echo "selected='selected'";} ?>>South Georgia and the South Sandwich Islands</option>
<option value="KR" <?php if(($_POST['country'])=="KR") { echo "selected='selected'";} ?>>South Korea</option>
<option value="ES" <?php if(($_POST['country'])=="ES") { echo "selected='selected'";} ?>>Spain</option>
<option value="LK" <?php if(($_POST['country'])=="LK") { echo "selected='selected'";} ?>>Sri Lanka</option>
<option value="SD" <?php if(($_POST['country'])=="SD") { echo "selected='selected'";} ?>>Sudan</option>
<option value="SR" <?php if(($_POST['country'])=="SR") { echo "selected='selected'";} ?>>Suriname</option>
<option value="SJ" <?php if(($_POST['country'])=="SJ") { echo "selected='selected'";} ?>>Svalbard and Jan Mayen</option>
<option value="SZ" <?php if(($_POST['country'])=="SZ") { echo "selected='selected'";} ?>>Swaziland</option>
<option value="SE" <?php if(($_POST['country'])=="SE") { echo "selected='selected'";} ?>>Sweden</option>
<option value="CH" <?php if(($_POST['country'])=="CH") { echo "selected='selected'";} ?>>Switzerland</option>
<option value="SY" <?php if(($_POST['country'])=="SY") { echo "selected='selected'";} ?>>Syria</option>
<option value="ST" <?php if(($_POST['country'])=="ST") { echo "selected='selected'";} ?>>Sao Tome and Principe</option>
<option value="TW" <?php if(($_POST['country'])=="TW") { echo "selected='selected'";} ?>>Taiwan</option>
<option value="TJ" <?php if(($_POST['country'])=="TJ") { echo "selected='selected'";} ?>>Tajikistan</option>
<option value="TZ" <?php if(($_POST['country'])=="TZ") { echo "selected='selected'";} ?>>Tanzania</option>
<option value="TH" <?php if(($_POST['country'])=="TH") { echo "selected='selected'";} ?>>Thailand</option>
<option value="TL" <?php if(($_POST['country'])=="TL") { echo "selected='selected'";} ?>>Timor-Leste</option>
<option value="TG" <?php if(($_POST['country'])=="TG") { echo "selected='selected'";} ?>>Togo</option>
<option value="TK" <?php if(($_POST['country'])=="TK") { echo "selected='selected'";} ?>>Tokelau</option>
<option value="TO" <?php if(($_POST['country'])=="TO") { echo "selected='selected'";} ?>>Tonga</option>
<option value="TT" <?php if(($_POST['country'])=="TT") { echo "selected='selected'";} ?>>Trinidad and Tobago</option>
<option value="TN" <?php if(($_POST['country'])=="TN") { echo "selected='selected'";} ?>>Tunisia</option>
<option value="TR" <?php if(($_POST['country'])=="TR") { echo "selected='selected'";} ?>>Turkey</option>
<option value="TM" <?php if(($_POST['country'])=="TM") { echo "selected='selected'";} ?>>Turkmenistan</option>
<option value="TC" <?php if(($_POST['country'])=="TC") { echo "selected='selected'";} ?>>Turks and Caicos Islands</option>
<option value="TV" <?php if(($_POST['country'])=="TV") { echo "selected='selected'";} ?>>Tuvalu</option>
<option value="UM" <?php if(($_POST['country'])=="UM") { echo "selected='selected'";} ?>>U.S. Minor Outlying Islands</option>
<option value="VI" <?php if(($_POST['country'])=="VI") { echo "selected='selected'";} ?>>U.S. Virgin Islands</option>
<option value="UG" <?php if(($_POST['country'])=="UG") { echo "selected='selected'";} ?>>Uganda</option>
<option value="UA" <?php if(($_POST['country'])=="UA") { echo "selected='selected'";} ?>>Ukraine</option>
<option value="AE" <?php if(($_POST['country'])=="AE") { echo "selected='selected'";} ?>>United Arab Emirates</option>
<option value="GB" <?php if(($_POST['country'])=="GB") { echo "selected='selected'";} ?>>United Kingdom</option>
<option value="US" <?php if(($_POST['country'])=="US") { echo "selected='selected'";} ?>>United States</option>
<option value="UY" <?php if(($_POST['country'])=="UY") { echo "selected='selected'";} ?>>Uruguay</option>
<option value="UZ" <?php if(($_POST['country'])=="UZ") { echo "selected='selected'";} ?>>Uzbekistan</option>
<option value="VU" <?php if(($_POST['country'])=="VU") { echo "selected='selected'";} ?>>Vanuatu</option>
<option value="VA" <?php if(($_POST['country'])=="VA") { echo "selected='selected'";} ?>>Vatican City</option>
<option value="VE" <?php if(($_POST['country'])=="VE") { echo "selected='selected'";} ?>>Venezuela</option>
<option value="VN" <?php if(($_POST['country'])=="VN") { echo "selected='selected'";} ?>>Vietnam</option>
<option value="WF" <?php if(($_POST['country'])=="WF") { echo "selected='selected'";} ?>>Wallis and Futuna</option>
<option value="EH" <?php if(($_POST['country'])=="EH") { echo "selected='selected'";} ?>>Western Sahara</option>
<option value="YE" <?php if(($_POST['country'])=="YE") { echo "selected='selected'";} ?>>Yemen</option>
<option value="ZM" <?php if(($_POST['country'])=="ZM") { echo "selected='selected'";} ?>>Zambia</option>
<option value="ZW" <?php if(($_POST['country'])=="ZW") { echo "selected='selected'";} ?>>Zimbabwe</option>
<option value="AX" <?php if(($_POST['country'])=="AX") { echo "selected='selected'";} ?>>Aland Islands</option>
</select> <?php if(isset($_POST['submit']) && isset($error[7])) { echo $error[7]; } ?>
</li>
</ol>  
</fieldset> 

<fieldset>
<legend>Your Professional Experience</legend>
<ol>
<li>
<label for="profsummary">Summary</label>
<textarea id="profsummary" name="profsummary" cols="35" rows="5"><?php if(!empty($_POST['profsummary'])) { echo $_POST['profsummary']; } ?></textarea>
<?php if(isset($_POST['submit']) && isset($error[9])) { echo $error[9]; } ?>
</li>
<li>
<label for="specialties">Specialties</label>
<input id="specialties" type="text" name="specialties" size="46" value="<?php if(!empty($_POST['specialties'])) { echo $_POST['specialties']; } ?>"> 
<?php if(isset($_POST['submit']) && isset($error[10])) { echo $error[10]; } ?>
</li>
</ol>
</fieldset> 

<fieldset>
<legend>Your Current Job</legend>
<ol>
<li>
<label for="company_job0">Company</label>
<input id="company_job0" type="text" name="company_job0" size="46" value="<?php if(!empty($_POST['company_job0'])) { echo $_POST['company_job0']; } ?>"> 
<?php if(isset($_POST['submit']) && isset($error[11])) { echo $error[11]; } ?>
</li>
<li>
<label for="title_job0">Position</label>
<input id="title_job0" type="text" name="title_job0" size="46" value="<?php if(!empty($_POST['title_job0'])) { echo $_POST['title_job0']; } ?>"> 
<?php if(isset($_POST['submit']) && isset($error[12])) { echo $error[12]; } ?>
</li>
<li>

<label for="startmonth_job0">Since</label>
<select name="startmonth_job0">
        <option>- Month -</option>
        <option <?php if(($_POST['startmonth_job0'])=="January") { echo "selected='selected'";} ?>>January</option>
        <option <?php if(($_POST['startmonth_job0'])=="February") { echo "selected='selected'";} ?>>February</option>
        <option <?php if(($_POST['startmonth_job0'])=="March") { echo "selected='selected'";} ?>>March</option>
        <option <?php if(($_POST['startmonth_job0'])=="April") { echo "selected='selected'";} ?>>April</option>
        <option <?php if(($_POST['startmonth_job0'])=="May") { echo "selected='selected'";} ?>>May</option>
        <option <?php if(($_POST['startmonth_job0'])=="June") { echo "selected='selected'";} ?>>June</option>
        <option <?php if(($_POST['startmonth_job0'])=="July") { echo "selected='selected'";} ?>>July</option>
        <option <?php if(($_POST['startmonth_job0'])=="August") { echo "selected='selected'";} ?>>August</option>
        <option <?php if(($_POST['startmonth_job0'])=="September") { echo "selected='selected'";} ?>>September</option>
        <option <?php if(($_POST['startmonth_job0'])=="October") { echo "selected='selected'";} ?>>October</option>
        <option <?php if(($_POST['startmonth_job0'])=="November") { echo "selected='selected'";} ?>>November</option>
        <option <?php if(($_POST['startmonth_job0'])=="December") { echo "selected='selected'";} ?>>December</option>
        </select> 
       <select name="startyear_job0">
       	  <option>- Year -</option>
          <option <?php if(($_POST['startyear_job0'])=="2000") { echo "selected='selected'";} ?>>2000</option>
          <option <?php if(($_POST['startyear_job0'])=="2001") { echo "selected='selected'";} ?>>2001</option>
          <option <?php if(($_POST['startyear_job0'])=="2002") { echo "selected='selected'";} ?>>2002</option>
          <option <?php if(($_POST['startyear_job0'])=="2003") { echo "selected='selected'";} ?>>2003</option>
          <option <?php if(($_POST['startyear_job0'])=="2004") { echo "selected='selected'";} ?>>2004</option>
          <option <?php if(($_POST['startyear_job0'])=="2005") { echo "selected='selected'";} ?>>2005</option>
          <option <?php if(($_POST['startyear_job0'])=="2006") { echo "selected='selected'";} ?>>2006</option>
          <option <?php if(($_POST['startyear_job0'])=="2007") { echo "selected='selected'";} ?>>2007</option>
          <option <?php if(($_POST['startyear_job0'])=="2008") { echo "selected='selected'";} ?>>2008</option>
          <option <?php if(($_POST['startyear_job0'])=="2009") { echo "selected='selected'";} ?>>2009</option>
          <option <?php if(($_POST['startyear_job0'])=="2010") { echo "selected='selected'";} ?>>2010</option>
          <option <?php if(($_POST['startyear_job0'])=="2011") { echo "selected='selected'";} ?>>2011</option>
          <option <?php if(($_POST['startyear_job0'])=="2012") { echo "selected='selected'";} ?>>2012</option>
          <option <?php if(($_POST['startyear_job0'])=="2013") { echo "selected='selected'";} ?>>2013</option>
          <option <?php if(($_POST['startyear_job0'])=="2014") { echo "selected='selected'";} ?>>2014</option>
        </select>
        <?php if(isset($_POST['submit']) && isset($error[13])) { echo $error[13]; } ?>
        <?php if(isset($_POST['submit']) && isset($error[14])) { echo $error[14]; } ?>
</li>
<li>
<label for="description_job0">Short description</label>
<textarea id="description_job0" name="description_job0" cols="35" rows="5"><?php if(!empty($_POST['description_job0'])) { echo $_POST['description_job0']; } ?></textarea>
<?php if(isset($_POST['submit']) && isset($error[15])) { echo $error[15]; } ?>
</li>
<li>
<label for="website">Company website</label>
<input id="website" type="text" name="website" size="46" value="<?php if(!empty($_POST['website'])) { echo $_POST['website']; } ?>"> 
<?php if(isset($_POST['submit']) && isset($error[20])) { echo $error[20]; } ?>
</li>
</ol>
<p class="add"><a href="#" onclick="Toggle('toggle_job1'); return false;">Add another job</a></p>
</fieldset> 

<fieldset id="toggle_job1" style="display:none;">
<legend>Add Another Job</legend>
<ol>
<li>
<label for="company_job1">Company</label>
<input id="company_job1" type="text" name="company_job1" size="46" value="<?php if(!empty($_POST['company_job1'])) { echo $_POST['company_job1']; } ?>"> 
<?php if(isset($_POST['submit']) && isset($error[16])) { echo $error[16]; } ?>
</li>
<li>
<label for="title_job1">Position</label>
<input id="title_job1" type="text" name="title_job1" size="46" value="<?php if(!empty($_POST['title_job1'])) { echo $_POST['title_job1']; } ?>"> 
<?php if(isset($_POST['submit']) && isset($error[17])) { echo $error[17]; } ?>
</li>
<li>
<label for="startmonth_job1">From</label>
<select name="startmonth_job1">
        <option>- Month -</option>
        <option <?php if(($_POST['startmonth_job1'])=="January") { echo "selected='selected'";} ?>>January</option>
        <option <?php if(($_POST['startmonth_job1'])=="February") { echo "selected='selected'";} ?>>February</option>
        <option <?php if(($_POST['startmonth_job1'])=="March") { echo "selected='selected'";} ?>>March</option>
        <option <?php if(($_POST['startmonth_job1'])=="April") { echo "selected='selected'";} ?>>April</option>
        <option <?php if(($_POST['startmonth_job1'])=="May") { echo "selected='selected'";} ?>>May</option>
        <option <?php if(($_POST['startmonth_job1'])=="June") { echo "selected='selected'";} ?>>June</option>
        <option <?php if(($_POST['startmonth_job1'])=="July") { echo "selected='selected'";} ?>>July</option>
        <option <?php if(($_POST['startmonth_job1'])=="August") { echo "selected='selected'";} ?>>August</option>
        <option <?php if(($_POST['startmonth_job1'])=="September") { echo "selected='selected'";} ?>>September</option>
        <option <?php if(($_POST['startmonth_job1'])=="October") { echo "selected='selected'";} ?>>October</option>
        <option <?php if(($_POST['startmonth_job1'])=="November") { echo "selected='selected'";} ?>>November</option>
        <option <?php if(($_POST['startmonth_job1'])=="December") { echo "selected='selected'";} ?>>December</option>
        </select> 
       <select name="startyear_job1">
       	  <option>- Year -</option>
          <option <?php if(($_POST['startyear_job1'])=="2000") { echo "selected='selected'";} ?>>2000</option>
          <option <?php if(($_POST['startyear_job1'])=="2001") { echo "selected='selected'";} ?>>2001</option>
          <option <?php if(($_POST['startyear_job1'])=="2002") { echo "selected='selected'";} ?>>2002</option>
          <option <?php if(($_POST['startyear_job1'])=="2003") { echo "selected='selected'";} ?>>2003</option>
          <option <?php if(($_POST['startyear_job1'])=="2004") { echo "selected='selected'";} ?>>2004</option>
          <option <?php if(($_POST['startyear_job1'])=="2005") { echo "selected='selected'";} ?>>2005</option>
          <option <?php if(($_POST['startyear_job1'])=="2006") { echo "selected='selected'";} ?>>2006</option>
          <option <?php if(($_POST['startyear_job1'])=="2007") { echo "selected='selected'";} ?>>2007</option>
          <option <?php if(($_POST['startyear_job1'])=="2008") { echo "selected='selected'";} ?>>2008</option>
          <option <?php if(($_POST['startyear_job1'])=="2009") { echo "selected='selected'";} ?>>2009</option>
          <option <?php if(($_POST['startyear_job1'])=="2010") { echo "selected='selected'";} ?>>2010</option>
          <option <?php if(($_POST['startyear_job1'])=="2011") { echo "selected='selected'";} ?>>2011</option>
          <option <?php if(($_POST['startyear_job1'])=="2012") { echo "selected='selected'";} ?>>2012</option>
          <option <?php if(($_POST['startyear_job1'])=="2013") { echo "selected='selected'";} ?>>2013</option>
          <option <?php if(($_POST['startyear_job1'])=="2014") { echo "selected='selected'";} ?>>2014</option>
        </select>
        <?php if(isset($_POST['submit']) && isset($error[18])) { echo $error[18]; } ?>
        <?php if(isset($_POST['submit']) && isset($error[19])) { echo $error[19]; } ?>
</li>
<li>
<label for="endmonth_job1">To</label>
<select name="endmonth_job1">
        <option>- Month -</option>
        <option <?php if(($_POST['endmonth_job1'])=="January") { echo "selected='selected'";} ?>>January</option>
        <option <?php if(($_POST['endmonth_job1'])=="February") { echo "selected='selected'";} ?>>February</option>
        <option <?php if(($_POST['endmonth_job1'])=="March") { echo "selected='selected'";} ?>>March</option>
        <option <?php if(($_POST['endmonth_job1'])=="April") { echo "selected='selected'";} ?>>April</option>
        <option <?php if(($_POST['endmonth_job1'])=="May") { echo "selected='selected'";} ?>>May</option>
        <option <?php if(($_POST['endmonth_job1'])=="June") { echo "selected='selected'";} ?>>June</option>
        <option <?php if(($_POST['endmonth_job1'])=="July") { echo "selected='selected'";} ?>>July</option>
        <option <?php if(($_POST['endmonth_job1'])=="August") { echo "selected='selected'";} ?>>August</option>
        <option <?php if(($_POST['endmonth_job1'])=="September") { echo "selected='selected'";} ?>>September</option>
        <option <?php if(($_POST['endmonth_job1'])=="October") { echo "selected='selected'";} ?>>October</option>
        <option <?php if(($_POST['endmonth_job1'])=="November") { echo "selected='selected'";} ?>>November</option>
        <option <?php if(($_POST['endmonth_job1'])=="December") { echo "selected='selected'";} ?>>December</option>
        </select> 
       <select name="endyear_job1">
       	  <option>- Year -</option>
          <option <?php if(($_POST['endyear_job1'])=="2000") { echo "selected='selected'";} ?>>2000</option>
          <option <?php if(($_POST['endyear_job1'])=="2001") { echo "selected='selected'";} ?>>2001</option>
          <option <?php if(($_POST['endyear_job1'])=="2002") { echo "selected='selected'";} ?>>2002</option>
          <option <?php if(($_POST['endyear_job1'])=="2003") { echo "selected='selected'";} ?>>2003</option>
          <option <?php if(($_POST['endyear_job1'])=="2004") { echo "selected='selected'";} ?>>2004</option>
          <option <?php if(($_POST['endyear_job1'])=="2005") { echo "selected='selected'";} ?>>2005</option>
          <option <?php if(($_POST['endyear_job1'])=="2006") { echo "selected='selected'";} ?>>2006</option>
          <option <?php if(($_POST['endyear_job1'])=="2007") { echo "selected='selected'";} ?>>2007</option>
          <option <?php if(($_POST['endyear_job1'])=="2008") { echo "selected='selected'";} ?>>2008</option>
          <option <?php if(($_POST['endyear_job1'])=="2009") { echo "selected='selected'";} ?>>2009</option>
          <option <?php if(($_POST['endyear_job1'])=="2010") { echo "selected='selected'";} ?>>2010</option>
          <option <?php if(($_POST['endyear_job1'])=="2011") { echo "selected='selected'";} ?>>2011</option>
          <option <?php if(($_POST['endyear_job1'])=="2012") { echo "selected='selected'";} ?>>2012</option>
          <option <?php if(($_POST['endyear_job1'])=="2013") { echo "selected='selected'";} ?>>2013</option>
          <option <?php if(($_POST['endyear_job1'])=="2014") { echo "selected='selected'";} ?>>2014</option>
        </select>
        <?php if(isset($_POST['submit']) && isset($error[20])) { echo $error[20]; } ?>
        <?php if(isset($_POST['submit']) && isset($error[21])) { echo $error[21]; } ?>
</li>
<li>
<label for="description_job1">Short description</label>
<textarea id="description_job1" name="description_job1" cols="35" rows="5"><?php if(!empty($_POST['description_job1'])) { echo $_POST['description_job1']; } ?></textarea>
<?php if(isset($_POST['submit']) && isset($error[22])) { echo $error[22]; } ?>
</li>
</ol>
</fieldset> 

<fieldset>
<legend>About the Information Mapping&reg; Training You Took</legend>
<script>
function showMe (it, box) {
  var vis = (box.checked) ?  "block" : "none";
  document.getElementById(it).style.display = vis;
}


</script>
<ol>
<li>
<input type="checkbox" name="followedtraining" onclick="showMe('imap', this)" value="yes" <?php if(!empty($_POST['followedtraining'])) { echo "checked"; } ?> />
 I have  already taken an Information Mapping training
</li>
<div id="imap" style="display:none;">
<li>
<label for="coursetitle_imap">Course title</label>
<input id="coursetitle_imap" type="text" name="coursetitle_imap" size="46" value="<?php if(!empty($_POST['coursetitle_imap'])) { echo $_POST['coursetitle_imap']; } ?>"> 
<?php if(isset($_POST['submit']) && isset($error[23])) { echo $error[23]; } ?>
</li>
<li>
<label for="company_imap">Training company</label>
<input id="company_imap" type="text" name="company_imap" size="46" value="<?php if(!empty($_POST['company_imap'])) { echo $_POST['company_imap']; } ?>"> 
<?php if(isset($_POST['submit']) && isset($error[24])) { echo $error[24]; } ?>
</li>
<li>
<label for="month_imap">Training date</label>
<select name="month_imap">
        <option>- Month -</option>
        <option <?php if(($_POST['month_imap'])=="January") { echo "selected='selected'";} ?>>January</option>
        <option <?php if(($_POST['month_imap'])=="February") { echo "selected='selected'";} ?>>February</option>
        <option <?php if(($_POST['month_imap'])=="March") { echo "selected='selected'";} ?>>March</option>
        <option <?php if(($_POST['month_imap'])=="April") { echo "selected='selected'";} ?>>April</option>
        <option <?php if(($_POST['month_imap'])=="May") { echo "selected='selected'";} ?>>May</option>
        <option <?php if(($_POST['month_imap'])=="June") { echo "selected='selected'";} ?>>June</option>
        <option <?php if(($_POST['month_imap'])=="July") { echo "selected='selected'";} ?>>July</option>
        <option <?php if(($_POST['month_imap'])=="August") { echo "selected='selected'";} ?>>August</option>
        <option <?php if(($_POST['month_imap'])=="September") { echo "selected='selected'";} ?>>September</option>
        <option <?php if(($_POST['month_imap'])=="October") { echo "selected='selected'";} ?>>October</option>
        <option <?php if(($_POST['month_imap'])=="November") { echo "selected='selected'";} ?>>November</option>
        <option <?php if(($_POST['month_imap'])=="December") { echo "selected='selected'";} ?>>December</option>
        </select> 
       <select name="year_imap">
       	  <option>- Year -</option>
          <option <?php if(($_POST['year_imap'])=="1990") { echo "selected='selected'";} ?>>1990</option>
          <option <?php if(($_POST['year_imap'])=="1991") { echo "selected='selected'";} ?>>1991</option>
          <option <?php if(($_POST['year_imap'])=="1992") { echo "selected='selected'";} ?>>1992</option>
          <option <?php if(($_POST['year_imap'])=="1993") { echo "selected='selected'";} ?>>1993</option>
          <option <?php if(($_POST['year_imap'])=="1994") { echo "selected='selected'";} ?>>1994</option>
          <option <?php if(($_POST['year_imap'])=="1995") { echo "selected='selected'";} ?>>1995</option>
          <option <?php if(($_POST['year_imap'])=="1996") { echo "selected='selected'";} ?>>1996</option>
          <option <?php if(($_POST['year_imap'])=="1997") { echo "selected='selected'";} ?>>1997</option>
          <option <?php if(($_POST['year_imap'])=="1998") { echo "selected='selected'";} ?>>1998</option>
          <option <?php if(($_POST['year_imap'])=="1999") { echo "selected='selected'";} ?>>1999</option>
          <option <?php if(($_POST['year_imap'])=="2000") { echo "selected='selected'";} ?>>2000</option>
          <option <?php if(($_POST['year_imap'])=="2001") { echo "selected='selected'";} ?>>2001</option>
          <option <?php if(($_POST['year_imap'])=="2002") { echo "selected='selected'";} ?>>2002</option>
          <option <?php if(($_POST['year_imap'])=="2003") { echo "selected='selected'";} ?>>2003</option>
          <option <?php if(($_POST['year_imap'])=="2004") { echo "selected='selected'";} ?>>2004</option>
          <option <?php if(($_POST['year_imap'])=="2005") { echo "selected='selected'";} ?>>2005</option>
          <option <?php if(($_POST['year_imap'])=="2006") { echo "selected='selected'";} ?>>2006</option>
          <option <?php if(($_POST['year_imap'])=="2007") { echo "selected='selected'";} ?>>2007</option>
          <option <?php if(($_POST['year_imap'])=="2008") { echo "selected='selected'";} ?>>2008</option>
          <option <?php if(($_POST['year_imap'])=="2009") { echo "selected='selected'";} ?>>2009</option>
          <option <?php if(($_POST['year_imap'])=="2010") { echo "selected='selected'";} ?>>2010</option>
          <option <?php if(($_POST['year_imap'])=="2011") { echo "selected='selected'";} ?>>2011</option>
          <option <?php if(($_POST['year_imap'])=="2012") { echo "selected='selected'";} ?>>2012</option>
          <option <?php if(($_POST['year_imap'])=="2013") { echo "selected='selected'";} ?>>2013</option>
          <option <?php if(($_POST['year_imap'])=="2014") { echo "selected='selected'";} ?>>2014</option>
        </select>
        <?php if(isset($_POST['submit']) && isset($error[25])) { echo $error[25]; } ?>
        <?php if(isset($_POST['submit']) && isset($error[26])) { echo $error[26]; } ?>
</li>
<li>
<label for="trainer_imap">Instructor</label>
<input id="trainer_imap" type="text" name="trainer_imap" size="46" value="<?php if(!empty($_POST['trainer_imap'])) { echo $_POST['trainer_imap']; } ?>">
<?php if(isset($_POST['submit']) && isset($error[27])) { echo $error[27]; } ?>
</li>
<li>
<label for="certificate_imap">Certificate</label>
<input id="certificate_imap" type="file" name="certificate_imap" size="18"> <small>(PDF, max. 2MB)</small>
<?php if(isset($_POST['submit']) && isset($error[28])) { echo $error[28]; } ?>
<?php if(isset($_POST['submit']) && isset($error[17])) { echo $error[17]; } ?>
<?php if(isset($_POST['submit']) && isset($error[18])) { echo $error[18]; } ?>
</li>
<li>
<label for="comments_imap">Comments</label>
<textarea id="comments_imap" name="comments_imap" cols="35" rows="5"><?php if(!empty($_POST['comments_imap'])) { echo $_POST['comments_imap']; } ?></textarea>
</li>
</div>
</ol>
<p class="add"><a href="#" onclick="Toggle('toggle_training1'); return false;">Add another training</a></p>
</fieldset> 

<fieldset id="toggle_training1" style="display:none;">
<legend>Add Another Training Course</legend>
<ol>
<li>
<label for="coursetitle0">Course title</label>
<input id="coursetitle0" type="text" name="coursetitle0" size="46" value="<?php if(!empty($_POST['coursetitle0'])) { echo $_POST['coursetitle0']; } ?>"> 
</li>
<li>
<label for="company0">Training company</label>
<input id="company0" type="text" name="company0" size="46" value="<?php if(!empty($_POST['company0'])) { echo $_POST['company0']; } ?>"> 
</li>
<li>
<label for="month0">Training date</label>

<select name="month0">
        <option>- Month -</option>
        <option <?php if(($_POST['month0'])=="January") { echo "selected='selected'";} ?>>January</option>
        <option <?php if(($_POST['month0'])=="February") { echo "selected='selected'";} ?>>February</option>
        <option <?php if(($_POST['month0'])=="March") { echo "selected='selected'";} ?>>March</option>
        <option <?php if(($_POST['month0'])=="April") { echo "selected='selected'";} ?>>April</option>
        <option <?php if(($_POST['month0'])=="May") { echo "selected='selected'";} ?>>May</option>
        <option <?php if(($_POST['month0'])=="June") { echo "selected='selected'";} ?>>June</option>
        <option <?php if(($_POST['month0'])=="July") { echo "selected='selected'";} ?>>July</option>
        <option <?php if(($_POST['month0'])=="August") { echo "selected='selected'";} ?>>August</option>
        <option <?php if(($_POST['month0'])=="September") { echo "selected='selected'";} ?>>September</option>
        <option <?php if(($_POST['month0'])=="October") { echo "selected='selected'";} ?>>October</option>
        <option <?php if(($_POST['month0'])=="November") { echo "selected='selected'";} ?>>November</option>
        <option <?php if(($_POST['month0'])=="December") { echo "selected='selected'";} ?>>December</option>
        </select> 
       <select name="year0">
       	  <option>- Year -</option>
          <option <?php if(($_POST['year0'])=="2000") { echo "selected='selected'";} ?>>2000</option>
          <option <?php if(($_POST['year0'])=="2001") { echo "selected='selected'";} ?>>2001</option>
          <option <?php if(($_POST['year0'])=="2002") { echo "selected='selected'";} ?>>2002</option>
          <option <?php if(($_POST['year0'])=="2003") { echo "selected='selected'";} ?>>2003</option>
          <option <?php if(($_POST['year0'])=="2004") { echo "selected='selected'";} ?>>2004</option>
          <option <?php if(($_POST['year0'])=="2005") { echo "selected='selected'";} ?>>2005</option>
          <option <?php if(($_POST['year0'])=="2006") { echo "selected='selected'";} ?>>2006</option>
          <option <?php if(($_POST['year0'])=="2007") { echo "selected='selected'";} ?>>2007</option>
          <option <?php if(($_POST['year0'])=="2008") { echo "selected='selected'";} ?>>2008</option>
          <option <?php if(($_POST['year0'])=="2009") { echo "selected='selected'";} ?>>2009</option>
          <option <?php if(($_POST['year0'])=="2010") { echo "selected='selected'";} ?>>2010</option>
          <option <?php if(($_POST['year0'])=="2011") { echo "selected='selected'";} ?>>2011</option>
          <option <?php if(($_POST['year0'])=="2012") { echo "selected='selected'";} ?>>2012</option>
          <option <?php if(($_POST['year0'])=="2013") { echo "selected='selected'";} ?>>2013</option>
          <option <?php if(($_POST['year0'])=="2014") { echo "selected='selected'";} ?>>2014</option>
        </select>
</li>
<li>
<label for="comments0">Comments</label>
<textarea id="comments0" name="comments0" cols="35" rows="5"><?php if(!empty($_POST['comments0'])) { echo $_POST['comments0']; } ?></textarea>
</li>
</ol>
<p class="add"><a href="#" onclick="Toggle('toggle_training2'); return false;">Add another training</a></p>
</fieldset> 

<fieldset id="toggle_training2" style="display:none;">
<legend>Add Another Training Course</legend>
<ol>
<li>
<label for="coursetitle1">Course title</label>
<input id="coursetitle1" type="text" name="coursetitle1" size="46" value="<?php if(!empty($_POST['coursetitle1'])) { echo $_POST['coursetitle1']; } ?>"> 
</li>
<li>
<label for="company1">Training company</label>
<input id="company1" type="text" name="company1" size="46" value="<?php if(!empty($_POST['company1'])) { echo $_POST['company1']; } ?>"> 
</li>
<li>
<label for="month1">Training date</label>

<select name="month1">
        <option>- Month -</option>
        <option <?php if(($_POST['month1'])=="January") { echo "selected='selected'";} ?>>January</option>
        <option <?php if(($_POST['month1'])=="February") { echo "selected='selected'";} ?>>February</option>
        <option <?php if(($_POST['month1'])=="March") { echo "selected='selected'";} ?>>March</option>
        <option <?php if(($_POST['month1'])=="April") { echo "selected='selected'";} ?>>April</option>
        <option <?php if(($_POST['month1'])=="May") { echo "selected='selected'";} ?>>May</option>
        <option <?php if(($_POST['month1'])=="June") { echo "selected='selected'";} ?>>June</option>
        <option <?php if(($_POST['month1'])=="July") { echo "selected='selected'";} ?>>July</option>
        <option <?php if(($_POST['month1'])=="August") { echo "selected='selected'";} ?>>August</option>
        <option <?php if(($_POST['month1'])=="September") { echo "selected='selected'";} ?>>September</option>
        <option <?php if(($_POST['month1'])=="October") { echo "selected='selected'";} ?>>October</option>
        <option <?php if(($_POST['month1'])=="November") { echo "selected='selected'";} ?>>November</option>
        <option <?php if(($_POST['month1'])=="December") { echo "selected='selected'";} ?>>December</option>
        </select> 
       <select name="year1">
       	  <option>- Year -</option>
          <option <?php if(($_POST['year1'])=="2000") { echo "selected='selected'";} ?>>2000</option>
          <option <?php if(($_POST['year1'])=="2001") { echo "selected='selected'";} ?>>2001</option>
          <option <?php if(($_POST['year1'])=="2002") { echo "selected='selected'";} ?>>2002</option>
          <option <?php if(($_POST['year1'])=="2003") { echo "selected='selected'";} ?>>2003</option>
          <option <?php if(($_POST['year1'])=="2004") { echo "selected='selected'";} ?>>2004</option>
          <option <?php if(($_POST['year1'])=="2005") { echo "selected='selected'";} ?>>2005</option>
          <option <?php if(($_POST['year1'])=="2006") { echo "selected='selected'";} ?>>2006</option>
          <option <?php if(($_POST['year1'])=="2007") { echo "selected='selected'";} ?>>2007</option>
          <option <?php if(($_POST['year1'])=="2008") { echo "selected='selected'";} ?>>2008</option>
          <option <?php if(($_POST['year1'])=="2009") { echo "selected='selected'";} ?>>2009</option>
          <option <?php if(($_POST['year1'])=="2010") { echo "selected='selected'";} ?>>2010</option>
          <option <?php if(($_POST['year1'])=="2011") { echo "selected='selected'";} ?>>2011</option>
          <option <?php if(($_POST['year1'])=="2012") { echo "selected='selected'";} ?>>2012</option>
          <option <?php if(($_POST['year1'])=="2013") { echo "selected='selected'";} ?>>2013</option>
          <option <?php if(($_POST['year1'])=="2014") { echo "selected='selected'";} ?>>2014</option>
        </select>
</li>
<li>
<label for="comments1">Comments</label>
<textarea id="comments1" name="comments1" cols="35" rows="5"><?php if(!empty($_POST['comments1'])) { echo $_POST['comments1']; } ?></textarea>
</li>
</ol>


</fieldset> 

<fieldset>
<?php if(isset($_POST['submit']) && isset($error[99])) { echo $error[99]; } ?>
<?php
  //require_once('recaptchalib.php');
  $publickey = "6Lf83uYSAAAAAHLJGvvt3QAkVRRRKju0CKt_3AUx"; // you got this from the signup page
  echo recaptcha_get_html($publickey);
?>
</fieldset>
<fieldset class="submit">  

<input id="submit" type="submit" name="submit" value="Submit Your Resume">
</fieldset>
  </form>
  </div>
</div>
<?
}
?>
</body>
</html>