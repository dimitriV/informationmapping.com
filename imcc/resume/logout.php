<?php
include_once("includes/config.php");
include_once("lang/lang_".$lang.".php");
$pml_title = $site_name;
include("includes/header.php");
include_once("includes/connect.php");

session_unset();
session_destroy(); 
if(isset($_COOKIE['user_id'])) {
 setcookie("cookie_id", "", time() - 3600);
 setcookie("cookie_pass", "", time() - 3600);
}
?>
<h1>Information Mapping Certified Consultant Program</h1>
<p>You have successfully logged out.</p>
<p><a href="login.php">Log in again</a></p>
<?
include("includes/footer.php");
?>