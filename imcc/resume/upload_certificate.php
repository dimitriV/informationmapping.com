<?php
$rootdir="";
include_once("includes/config.php");
include_once("includes/connect.php");
include("includes/header.php");

$SQL_resume="SELECT * FROM resume WHERE resume_id=" . $_GET['resume_id'];
$resume_result=mysql_query($SQL_resume);
$resume=mysql_fetch_array($resume_result);

if(isset($_POST['submit'])) {
	
  $resume_id = $_GET['resume_id'];
  $cert_old=mysql_fetch_array(mysql_query("SELECT certificate_imap FROM resume WHERE resume_id=$resume_id")); 

  if($_FILES['certificate_imap']['size']>0){
  
  $source=$_FILES['certificate_imap']['tmp_name'];
  $certificate_name=time() . $_FILES['certificate_imap']['name'];
  $dest="certificates/" . $certificate_name;
  $check=copy($source,$dest);
  
   if($check==1){
   $certificate_SQL_update="UPDATE resume SET certificate_imap='$certificate_name' WHERE resume_id=$resume_id";
   $bool=mysql_query($certificate_SQL_update);
  if($bool<>1) echo "There was an error uploading your certificate.";
  if($bool==1) {

	?>
	<h1>Information Mapping Certified Consultant Program</h1>
	<h2>Thank You!</h2>
	<p>You have successfully sent your certificate to Information Mapping International.</p>
	<p>We will provide feedback within 5 business days.</p>
	<p>Should you have any questions in the mean time, please contact us at <a href="mailto:info@informationmapping.com">info@informationmapping.com</a>.</p>

	<?

	$SQL_resume="SELECT * FROM resume WHERE resume_id=" . $_GET['resume_id'];
	$resume_result=mysql_query($SQL_resume);
	$resume=mysql_fetch_array($resume_result);
	
	$firstname = $resume['firstname'];
	$email = $resume['email'];

	$message  = "<table cellpadding='0' cellspacing='0' border='0' width='100%'><tr><td align='center'>";
	$message .= "<table cellpadding='0' cellspacing='0' border='0' width='500'><tr><td align='left'>";
	$message .= "<p><img src='http://www.informationmapping.com/imcc/images/logo_email.gif' alt='Information Mapping logo' /></p>";
	$message .= "<p><font size='4' face='Arial, Helvetica, sans-serif' color=#009edf'>Information Mapping Certified Consultant Program</font></p>";
	$message .= "<p><font size='2' face='Arial, Helvetica, sans-serif'>Dear $firstname,</font></p>";
	$message .= "<p><font size='2' face='Arial, Helvetica, sans-serif'>Thank you for sending your certificate!</font></p>
				 <p><font size='2' face='Arial, Helvetica, sans-serif'>We will provide feedback within 5 business days.</font></p>";
	$message .= "<p><font size='2' face='Arial, Helvetica, sans-serif'>With kind regards,<br />";
	$message .= "Information Mapping International</font></p>";

	$mime_boundary="==Multipart_Boundary_x".md5(mt_rand())."x";
	
	$headers = "From: Information Mapping International <info@informationmapping.com>\n";
	$headers .= "BCC: Filip Vanlerberghe <fvanlerberghe@informationmapping.com>\n";
	$headers .= "X-Priority: 1\n";  	  
	$headers .= "Content-Type: multipart/mixed;\n" . " boundary=\"{$mime_boundary}\"\n" . "MIME-Version: 1.0\n"; 
	$subject="IMCC Program - Certificate received";

	$message = "This is a multi-part message in MIME format.\n\n" .
         "--{$mime_boundary}\n" .
         "Content-Type: text/html; charset=\"iso-8859-1\"\n" .
         "Content-Transfer-Encoding: 7bit\n\n" . $message . "\n\n";
		 
    $message .="--{$mime_boundary}--\n";

    mail($email,$subject, $message, $headers); 
}
 } 
   }  
}else{

?>
<h1>Information Mapping Certified Consultant Program</h1>
<div id="content">
<h2>Upload your certificate</h2>
<p>Hi <?php echo $resume['firstname']; ?>,</p>
<p>Please upload the certificate you received after taking the  Information Mapping&reg; course <strong>Mastering Business Communications - Instructor-led</strong>:</p>
  
<form action="<?php echo $_SERVER['PHP_SELF']; ?>?resume_id=<?php echo $resume['resume_id'] ?>" method="post" enctype="multipart/form-data">
<fieldset>
<ol>
<li>
<label for="certificate_imap">Certificate</label>
<input id="certificate_imap" type="file" name="certificate_imap" size="32">
<?php if(isset($_POST['submit']) && isset($error[28])) { echo $error[28]; } ?>
</li>
</ol>
</fieldset>
<fieldset class="submit">  
<input id="submit" type="submit" name="submit" value="Send" class="send">
</fieldset>
</form>
<? 
}
?>
</div>
</body>
</html>