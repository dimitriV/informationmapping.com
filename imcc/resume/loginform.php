<h1>Information Mapping Certified Consultant Program</h1>

<div id="new">
<h2>How to apply?</h2>
  <p>To apply for the Information Mapping Certified Consultant Program, please 
    go to the <a href="#">Information Mapping Web Store</a> and pay the certification 
    fee.</p>
  <p>When you have done so, <a href="register.php">fill out the registration form</a> to obtain your login credentials.</p>
  <p>For more information regarding the Certified Consultants Program, please 
    consult <a href="#">our website</a>.</p>
</div>

<div id="existing">
<h2>Registered consultants</h2>
<p>If you have an account with us, please log in.</p>
<?php if ($loginError=="1"): ?>
<p class="error">Invalid username or password.</p>
<?php endif; ?>
<?php if ($loginError=="2"): ?>
<p class="error">Inactive account.</p>
<?php endif; ?>
   <form method="post" action="login.php">
   <fieldset>
    <ol>
    <li>
    <label for="user"><?= $login_username ?></label>
    <input id="user" type="text" name="user" size="30" />
    </li>
    <li>
    <label for="pass"><?= $login_password ?></label>
    <input id="pass" type="password" name="pass" size="30" />
    </li>
    <li><label for="cookie">&nbsp;</label> 
    <input id="cookie" type="checkbox" name="cookie" value="do" style="border: 0px;" />
    <?= $login_cookied ?>
    </li>
    </ol>
    </fieldset>
    <fieldset class="submit">
    <span><a href="forgotpass.php" title="<?= $login_forgotpass ?>"><?= $login_forgotpass ?></a></span>
    <input type="submit" name="submit" value="<?= $login_login ?>" />
    </fieldset>
   </form>