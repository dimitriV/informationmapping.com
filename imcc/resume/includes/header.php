<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml">
<head>
<meta http-equiv="Content-Type" content="text/html; charset=utf-8" />
<title>Information Mapping Certified Consultant Program</title>
<link rel="stylesheet" type="text/css" href="<?php echo $rootdir; ?>style.css" />
<script language="javascript" type="text/javascript" src="scripts/toggle.js"></script>
</head>
<body>
 
<div id="wrapper">
<div id="header">
<div id="logo"><img src="<?php echo $rootdir; ?>images/logo.png" alt="Information Mapping logo" /></div>

<div id="nav">
</div>
</div>
