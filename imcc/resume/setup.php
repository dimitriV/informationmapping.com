<?php
###################################
##   PHPMYLOGON: A LOGIN SYSTEM  ##
##    (c) 2006 Jorik Berkepas    ##
##   Under the GNU GPL license   ##
##     helpdesk90@gmail.com      ##
###################################

// Configfile creator PhpMyLogon

$pml_title = "PhpMyLogon Setup";
include("htmltop.php");

if(isset($_GET['lang'])) {
 if($_GET['lang'] == "en" OR $_GET['lang'] == "nl") {
  // Select language ok, settings language-variables
  if($_GET['lang'] == "en") {
   // English language
   $setup_start = "Welcome to PhpMyLogon. Please fill in the fields on this page, so a config-file can be created. You only will need to do this once.";
   $setup_confignote = "note: Any existing config.php files in the same folder as this script, will be deleted!";
   // MySQL settings
    $setup_mysql = "MySQL Settings";
    $setup_mysql_user = "MySQL username";
    $setup_mysql_passw = "MySQL password";
    $setup_mysql_host = "MySQL host";
    $setup_mysql_db = "MySQL database";
    $setup_mysql_tbl = "MySQL table";
    $setup_mysql_tbl_note = "note: any existing tables with this name, will be deleted!";
   // Settings
    $setup_settings = "Settings";
    $setup_settings_url = "Complete URL to PhpMyLogon";
    $setup_settings_url_note = "note: place a / at the end!";
    $setup_settings_mail = "Site e-mail";
    $setup_settings_name = "Site name";
    $setup_settings_activate = "Mail validation at registration"; 
    $setup_settings_afterlogin = "Page to go to after login";
    $setup_settings_lang = "Language";
    $setup_settings_lang_note = "note: file 'lang/lang_<i>LANGNAME</i>.php' must exists!";
    $setup_submit = "Start installation";
   // Administrator
    $setup_admin = "Administrator-user";
    $setup_admin_name = "Username";
    $setup_admin_passw = "Password";
    $setup_admin_mail = "Mail address";
   // Submit form 1
    $setup_submit1_mysqlconnfalse = "The entered MySQL-data didn't work properly. Please go back and try again.";
    $setup_submit1_mysqldbfalse = "The entered MySQL-databasename didn't work properly. Please go back and try again.";
    $setup_submit1_langfilefalse = "The language file doesn't exists in the /lang/ directory, with prefix lang_ and extension .php. Please check the file.";
   // config-create
    $setup_conf_olddelete = "Old config.php-file deleted ...";
    $setup_conf_create = "Config.php-file created ...";
    $setup_conf_error = "Couldn't write to the config.php-file. Please copy the data and paste it to the config.php file manually.";
   // db
    $setup_db_olddelete = "If there was a table with the same name as the given name, it is deleted ...";
    $setup_db_create = "The table for PhpMyLogon is created ...";
    $setup_db_admin = "Administrator user created, you can login with this user and edit useraccounts ...";
    $setup_db_error = "There was an error while executing one of the MySQl queries ...";
   $setup_delfile = "This file is removed, when the file still is available on your server, please remove it for security reasons!";
   $setup_delfile_err = "The setup.php file isn't deleted caused by an error. Please remove the file manually!";
   $setup_complete = "The installation of PhpMyLogon is now complete, you can now use PhpMyLogon. Thanks for the use of PhpMyLogon.";
  }else{
   // Dutch language
   $setup_start = "Welkom bij PhpMyLogon. Vul onderstaande velden in, zodat er een configuratiebestand aangemaakt kan worden. Je hoeft dit slechts eenmalig te doen.";
   $setup_confignote = "let op: waneer er reeds een config.php bestand in de map aanwezig is, zal deze verwijderd worden!";
   // MySQL settings
    $setup_mysql = "MySQL Instellingen";
    $setup_mysql_user = "MySQL gebruikersnaam";
    $setup_mysql_passw = "MySQL wachtwoord";
    $setup_mysql_host = "MySQL host";
    $setup_mysql_db = "MySQL database";
    $setup_mysql_tbl = "MySQL tabel";
    $setup_mysql_tbl_note = "let op: wanneer er reeds een tabel bestaat met deze naam, zal deze (inclusief data) verwijderd worden!";
   // Settings
    $setup_settings = "Instellingen";
    $setup_settings_url = "Complete URL naar PhpMyLogon";
    $setup_settings_url_note = "let op: plaats een / aan het einde!";
    $setup_settings_mail = "Site e-mail";
    $setup_settings_name = "Site naam";
    $setup_settings_activate = "Mail validatie bij registratie"; 
    $setup_settings_afterlogin = "Naar pagina gaan na login";
    $setup_settings_lang = "Taal";
    $setup_settings_lang_note = "let op: bestand 'lang/lang_<i>TAAL</i>.php' moet bestaan!";
    $setup_submit = "Start installatie";
   // Administrator
    $setup_admin = "Administrator-account";
    $setup_admin_name = "Gebruikersnaam";
    $setup_admin_passw = "Wachtwoord";
    $setup_admin_mail = "Mail adres";
   // Submit form 1
    $setup_submit1_mysqlconnfalse = "De opgegeven MySQL-instellingen werkten niet. Ga terug en probeer het nog eens.";
    $setup_submit1_mysqldbfalse = "De opgegeven MySQL databasenaam werkte niet. Ga terug en probeer het nog eens.";
    $setup_submit1_langfilefalse = "Het taalbestand bestaat niet in de map /lang/, heeft geen prefix lang_ of niet de extensie .php.";
   // config-create
    $setup_conf_olddelete = "Oude config.php-bestand verwijderd ...";
    $setup_conf_create = "Config.php-bestand aangemaakt ...";
    $setup_conf_error = "Er is een fout opgetreden tijdens het maken van het bestand config.php. Kopiee�r de onderstaande data handmatig in het bestand config.php.";
   // db
    $setup_db_olddelete = "Wanneer er reeds een MySQL-tabel bestond met dezelfde naam als opgegeven, is deze verwijderd ...";
    $setup_db_create = "De tabel voor PhpMyLogon is aangemaakt ...";
    $setup_db_admin = "Administrator account aangemaakt, je kunt met dat account inloggen om accounts te bewerken ...";
    $setup_db_error = "Er is een fout opgetreden tijdens het uitvoeren van een MySQL query ...";
   $setup_delfile = "Dit installatie bestand is verwijderd. Wanneer het toch nog lijkt te bestaan, verwijder het dan aub. om veiligheidsoverwegingen!";
   $setup_delfile_err = "Het setup.php bestand is niet verwijderd wegens een error; verwijder het bestand aub handmatig!";
   $setup_complete = "De installatie van PhpMyLogon is voltooid en je kunt PhpMyLogon nu gebruiken. Bedankt voor het kiezen voor PhpMyLogon.";
  }
  
  if(isset($_POST['submit1'])) {
   // Check if everything is OK, if MySQL settings are working
   $mysql_connect = @mysql_connect($_POST['mysql_host'],$_POST['mysql_user'],$_POST['mysql_passw']);
   if($mysql_connect == TRUE) {
    $mysql_db = @mysql_select_db($_POST['mysql_db']);
    if($mysql_db == TRUE) {
     // MySQL settings ok, continue
     if(file_exists("lang/lang_".$_POST['settings_lang'].".php")) {
      // Creating the config.php file
      
      // Beginning note, copyright
       $configfile  = "<?php\r\n";
       $configfile .= "// This config-file is created by the setup-script from PhpMyLogon\r\n";
       $configfile .= "// (c) 2006 Jorik Berkepas; under the GNU GPL license\r\n\r\n";
      // Sessions
       $configfile .= "session_start();\r\n";
       $configfile .= "ob_start();\r\n\r\n";
      // MySQL settings
       $configfile .= "// MySQL settings\r\n";
       $configfile .= "\$db_user    = \"".$_POST['mysql_user']."\"; // MySQL username\r\n";
       $configfile .= "\$db_pass    = \"".$_POST['mysql_passw']."\"; // MySQL password\r\n";
       $configfile .= "\$db_host    = \"".$_POST['mysql_host']."\"; // MySQL host, mostly localhost\r\n";
       $configfile .= "\$db_db      = \"".$_POST['mysql_db']."\"; // MySQL database\r\n";
       $configfile .= "\$db_tbl     = \"".$_POST['mysql_tbl']."\"; // MySQL table\r\n\r\n";
      // Settings
       $configfile .= "// Settings\r\n";
       $configfile .= "\$site_url   = \"".$_POST['settings_url']."\"; // URL to PhpMyLogon, / at end\r\n";
       $configfile .= "\$site_mail  = \"".$_POST['settings_mail']."\"; // Mail address website\r\n";
       $configfile .= "\$site_name  = \"".$_POST['settings_name']."\"; // Website name\r\n";
       $configfile .= "\$activate   = \"".$_POST['settings_activate']."\"; // E-mail validation at registration, TRUE or FALSE\r\n";
       $configfile .= "\$afterlogin = \"".$_POST['settings_afterlogin']."\"; // Page to go to after login\r\n"; 
       $configfile .= "\$lang       = \"".$_POST['settings_lang']."\"; // Language of PhpMyLogon\r\n";
       $configfile .= "?>\r\n";
      
      echo "<b>config.php</b><br />\n";
      if(file_exists("config.php")) {
       unlink("config.php");
       echo $setup_conf_olddelete."<br />\n";
      }
      if(fwrite(fopen("config.php","w"),$configfile) == TRUE) {
       echo $setup_conf_create."<p />\n";
      }else{
       echo "<span style=\"color: red\">".$setup_conf_error."</span><br />";
       echo "<i>config.php</i>:<br />\n";
       $conf = htmlspecialchars($configfile);
       echo "<textarea rows='20' cols='80' style=\"font-family: Courier New;\">".$conf."</textarea><p />";
      }
      
      // Drop old tables with same name as mysql_tbl
      echo "<b>MySQL</b><br />\n";
      $sql = "DROP TABLE IF EXISTS `".$_POST['mysql_tbl']."`";
      if(mysql_query($sql) == TRUE) {
       echo $setup_db_olddelete."<br />\n";
       // Create table mysql_tbl
       $sql = "CREATE TABLE `".$_POST['mysql_tbl']."` (`id` int(11) NOT NULL auto_increment,`name` varchar(50) NOT NULL default '',`password` varchar(50) NOT NULL default '',`cookie_pass` varchar(50) NOT NULL default '',`state` char(1) NOT NULL default '0',`mail` varchar(100) NOT NULL default '',`active` char(1) NOT NULL default '0',`actcode` varchar(15) NOT NULL default '',`lastactive` datetime NOT NULL default '0000-00-00 00:00:00',PRIMARY KEY  (`id`)) TYPE=MyISAM AUTO_INCREMENT=1";
       if(mysql_query($sql) == TRUE) {
        echo $setup_db_create."<br />\n";
        // Admin user
        $sql = "INSERT INTO `".$_POST['mysql_tbl']."` (name,password,state,mail,active) VALUES ('".$_POST['admin_name']."','".md5($_POST['admin_pass'])."',1,'".$_POST['admin_mail']."',1)";
        if(mysql_query($sql) == TRUE) {
         echo $setup_db_admin."<p />\n";
        }else{
         echo "<span style=\"color: red\">".$setup_db_error."<br />\nERROR: ".mysql_error()."</span><p />\n";
        }
       }else{
        echo "<span style=\"color: red\">".$setup_db_error."<br />\nERROR: ".mysql_error()."</span><p />\n";
       }
      }else{
       echo "<span style=\"color: red\">".$setup_db_error."<br />\nERROR: ".mysql_error()."</span><p />\n";
      }
      
      // Delete this file
      if(unlink("setup.php") == TRUE) {
       echo $setup_delfile."<p />\n\n";
      }else{
       echo "<span style=\"color: red\">".$setup_delfile_err."</span><p />\n\n";
      }
      echo $setup_complete;
      
     }else{
      echo $setup_submit1_langfilefalse;
     }
    }else{
     echo $setup_submit1_mysqldbfalse."<br />\n<span style=\"color: red\">Error: ".mysql_error()."</span>";
    }
   }else{
    echo $setup_submit1_mysqlconnfalse."<br />\n<span style=\"color: red\">Error: ".mysql_error()."</span>";
   }
  }else{
   echo $setup_start."<br />\n";
   echo $setup_confignote."<p />\n\n";
   echo "<form method=\"post\" action=\"setup.php?lang=".$_GET['lang']."\">\n";
   // MySQL settings
   echo "<table>\n";
   echo " <tr>\n";
   echo "  <td><b>".$setup_mysql."</b></td><td></td>\n";
   echo " </tr><tr>\n";
   echo "  <td><label for=\"mysql_user\">".$setup_mysql_user.":</label></td><td><input id=\"mysql_user\" type=\"text\" name=\"mysql_user\" /></td>\n";
   echo " </tr><tr>\n";
   echo "  <td><label for=\"mysql_pass\">".$setup_mysql_passw.":</label></td><td><input id=\"mysql_pass\" type=\"text\" name=\"mysql_passw\" /></td>\n";
   echo " </tr><tr>\n";
   echo "  <td><label for=\"mysql_host\">".$setup_mysql_host.":</label></td><td><input id=\"mysql_host\" type=\"text\" name=\"mysql_host\" value=\"localhost\" /></td>\n";
   echo " </tr><tr>\n";
   echo "  <td><label for=\"mysql_db\">".$setup_mysql_db.":</label></td><td><input id=\"mysql_db\" type=\"text\" name=\"mysql_db\" /></td>\n";
   echo " </tr><tr>\n";
   echo "  <td><label for=\"mysql_tbl\">".$setup_mysql_tbl.":</label></td><td><input id=\"mysql_tbl\" type=\"text\" name=\"mysql_tbl\" value=\"phpmylogon\" /> <small>".$setup_mysql_tbl_note."</small></td>\n";
   echo " </tr><tr>\n";
   echo "  <td>&nbsp;</td><td>&nbsp;</td>\n";
   echo " </tr><tr>\n";
   // Settings
   echo "  <td><b>".$setup_settings."</b></td><td></td>\n";
   echo " </tr><tr>\n";
   echo "  <td><label for=\"settings_url\">".$setup_settings_url.":</label></td><td><input id=\"settings_url\" type=\"text\" name=\"settings_url\" /> <small>".$setup_settings_url_note."</small></td>\n";
   echo " </tr><tr>\n";
   echo "  <td><label for=\"settings_mail\">".$setup_settings_mail.":</label></td><td><input id=\"settings_mail\" type=\"text\" name=\"settings_mail\" /></td>\n";
   echo " </tr><tr>\n";
   echo "  <td><label for=\"settings_name\">".$setup_settings_name.":</label></td><td><input id=\"settings_name\" type=\"text\" name=\"settings_name\" /></td>\n";
   echo " </tr><tr>\n";
   echo "  <td><label for=\"settings_activate\">".$setup_settings_activate.":</label></td><td><select id=\"settings_activate\" name=\"settings_activate\" size=\"1\"><option value=\"\" selected=selected>&nbsp;</option><option value=\"TRUE\">TRUE</option><option value=\"FALSE\">FALSE</option></select></td>\n";
   echo " </tr><tr>\n";
   echo "  <td><label for=\"settings_afterlogin\">".$setup_settings_afterlogin.":</label></td><td><input id=\"settings_afterlogin\" type=\"text\" name=\"settings_afterlogin\" /></td>\n";
   echo " </tr><tr>\n";
   echo "  <td><label for=\"settings_lang\">".$setup_settings_lang.":</label></td><td><input id=\"settings_lang\" type=\"text\" name=\"settings_lang\" value=\"".$_GET['lang']."\" /> <small>".$setup_settings_lang_note."</small></td>\n";
   echo " </tr><tr>\n";
   echo "  <td>&nbsp;</td><td>&nbsp;</td>\n";
   echo " </tr><tr>\n";
   // Administrator user
   echo "  <td><b>".$setup_admin."</b></td><td></td>\n";
   echo " </tr><tr>\n";
   echo "  <td><label for=\"admin_name\">".$setup_admin_name.":</label></td><td><input id=\"admin_name\" type=\"text\" name=\"admin_name\" value=\"admin\" /></td>\n";
   echo " </tr><tr>\n";
   echo "  <td><label for=\"admin_pass\">".$setup_admin_passw.":</label></td><td><input id=\"admin_pass\" type=\"text\" name=\"admin_pass\" /></td>\n";
   echo " </tr><tr>\n";
   echo "  <td><label for=\"admin_mail\">".$setup_admin_mail.":</label></td><td><input id=\"admin_mail\" type=\"text\" name=\"admin_mail\" /></td>\n";
   echo " </tr>\n";
   echo "</table><br />\n";
   // Submit-button
   echo "<input type=\"submit\" name=\"submit1\" value=\"".$setup_submit." &raquo;\" />\n";
   echo "</form>\n";
  }  
 }else{
  // The selected language is incorrect, an error will be displayed (in English)
  echo "The requested language '".$_GET['lang']."' doesn't exists. Please remove the ?lang-tag in the URL and try again.";
 }
}else{
 // Select language for setup
 ?>
 Please select language for setup / Selecteer taal  voor setup<br />
 <ul>
  <li><a href="setup.php?lang=en">English</a></li>
  <li><a href="setup.php?lang=nl">Nederlands</a></li>
 </ul>
 <?
}
?>
<p />
<small>&copy; <a href="http://phpmylogon.sf.net" target="_blank">PhpMyLogon</a></small>
<?
include("htmlbottom.php");
?>
