<?php include_once 'include.php'; ?>
<?php header('X-UA-Compatible: IE=edge,chrome=1'); ?>
<!DOCTYPE html>
<html>
<head>
<title>Training Roadshow</title>
<meta charset="utf-8">
<meta name="viewport" content="width=device-width, initial-scale=1.0">
<!-- Bootstrap -->
<link href="css/bootstrap.min.css" rel="stylesheet" media="screen">
<!-- HTML5 shim and Respond.js IE8 support of HTML5 elements and media queries -->
<!--[if lt IE 9]>
      <script src="js/html5shiv.js"></script>
      <script src="js/respond.min.js"></script>
    <![endif]-->
<link href="//fonts.googleapis.com/css?family=Tauri" rel="stylesheet" type="text/css" />
<link href="css/imi.css" rel="stylesheet" media="screen">
<link rel="stylesheet" href="css/font-awesome.min.css" type="text/css" media="all"/>
</head>
<body>
<div class="row" style="border-top:10px solid #009edf;margin-bottom:30px;">
<?php

if (isset($_GET['id'])) {
$id = mysql_real_escape_string($_GET['id']);
}

// update hits
$result_update = mysql_query("UPDATE imi_jeajx_event SET ehits = ehits+1 WHERE id = '$id'");
// read out event data
$result = mysql_query("SELECT * FROM imi_jeajx_event WHERE id = '$id' && published = '1'");
while($row = mysql_fetch_assoc($result))
{
	// main event data
	$city =  $row['city'];
	$title =  $row['title'];
	$startdate = $row['start_date'];
	$enddate = $row['end_date'];
	$country_id = $row['country'];
	$street = $row['street'];
	$desc = $row['desc'];
	// country ophalen
	$result_country = mysql_query("SELECT country_id, country_name FROM imi_jeajx_country WHERE country_id = '$country_id'");
	$country = mysql_fetch_assoc($result_country);
	$country = $country['country_name'];
	// additional event data
	$result_detail_phone = mysql_query("SELECT * FROM imi_jeajx_fields_data WHERE hid = '$id' && fieldid = '7'"); // phone
	$phone = mysql_fetch_assoc($result_detail_phone);
	$phone = $phone['data_txt'];
	$result_detail_website = mysql_query("SELECT * FROM imi_jeajx_fields_data WHERE hid = '$id' && fieldid = '4'"); // website
	$website = mysql_fetch_assoc($result_detail_website);
	$website = $website['data_txt'];
	$result_detail_language = mysql_query("SELECT * FROM imi_jeajx_fields_data WHERE hid = '$id' && fieldid = '3'"); // language
	$language = mysql_fetch_assoc($result_detail_language);
	$language = $language['data_txt'];
	$result_detail_price = mysql_query("SELECT * FROM imi_jeajx_fields_data WHERE hid = '$id' && fieldid = '2'"); // price
	$price = mysql_fetch_assoc($result_detail_price);
	$price = $price['data_txt'];
	$result_detail_target = mysql_query("SELECT * FROM imi_jeajx_fields_data WHERE hid = '$id' && fieldid = '8'"); // target audience
	$target = mysql_fetch_assoc($result_detail_target);
	$target = $target['data_txt'];
	$result_detail_outline = mysql_query("SELECT * FROM imi_jeajx_fields_data WHERE hid = '$id' && fieldid = '9'"); // outline
	$outline = mysql_fetch_assoc($result_detail_outline);
	$outline = $outline['data_txt'];
	$result_detail_package = mysql_query("SELECT * FROM imi_jeajx_fields_data WHERE hid = '$id' && fieldid = '10'"); // package
	$package = mysql_fetch_assoc($result_detail_package);
	$package = $package['data_txt'];
	$result_detail_start = mysql_query("SELECT * FROM imi_jeajx_fields_data WHERE hid = '$id' && fieldid = '11'"); // start hour
	$start = mysql_fetch_assoc($result_detail_start);
	$start = $start['data_txt'];
	$result_detail_end = mysql_query("SELECT * FROM imi_jeajx_fields_data WHERE hid = '$id' && fieldid = '12'"); // end hour
	$end = mysql_fetch_assoc($result_detail_end);
	$end = $end['data_txt'];
	$result_detail_lunch = mysql_query("SELECT * FROM imi_jeajx_fields_data WHERE hid = '$id' && fieldid = '13'"); // lunch
	$lunch = mysql_fetch_assoc($result_detail_lunch);
	$lunch = $lunch['data_txt'];
	$result_detail_day3 = mysql_query("SELECT * FROM imi_jeajx_fields_data WHERE hid = '$id' && fieldid = '14'"); // day 3
	$day3 = mysql_fetch_assoc($result_detail_day3);
	$day3 = $day3['data_txt'];
	//$day3 = explode('-', $day3);
	//$day3 = $day3[0];
	// event photo
	$result_detail_photo = mysql_query("SELECT * FROM imi_jeajx_event_photo WHERE event_id = '$id'"); // photo
	$number_photo = mysql_num_rows($result_detail_photo);
	if($number_photo > 0)
	{
	$photo = mysql_fetch_assoc($result_detail_photo);
	$photo = $photo['image'];
	}
	else
	{
		switch (strtolower($city)) {
    case "atlanta":
        $photo = "Atlanta,-GA.png";
        break;
    case "auckland":
        $photo = "Auckland,-New-Zealand.png";
        break;
    case "austin":
        $photo = "Austin,-TX.png";
        break;
    case "baltimore":
        $photo = "Baltimore,-MD.png";
        break;
	case "bangalore":
        $photo = "Bangalore,-India.png";
        break;
	case "canberra":
        $photo = "Canberra,-Australia.png";
        break;
	case "charlotte":
        $photo = "Charlotte,-NC.png";
        break;
	case "chicago":
        $photo = "Chicago,-IL.png";
        break;
	case "cincinnati":
        $photo = "Cincinnati,-OH.png";
        break;
	case "columbus":
        $photo = "Columbus,-OH.png";
        break;
	case "dallas":
        $photo = "Dallas,-TX.png";
        break;
	case "desenzano":
        $photo = "Desenzano-del-Garda,-Italy.png";
        break;
	case "edison":
        $photo = "Edison,-NJ.png";
        break;	
	case "houston":
        $photo = "Houston,-TX.png";
        break;
	case "jacksonville":
        $photo = "Jacksonville,-FL.png";
        break;
	case "kuala lumpur":
        $photo = "Kuala-Lumpur,-Malaysia.png";
        break;
	case "las vegas":
        $photo = "Las-Vegas,-NV.png";
        break;
	case "los angeles":
        $photo = "Los-Angeles,-CA.png";
        break;
	case "melbourne":
        $photo = "Melbourne,-Australia.png";
        break;
	case "minneapolis":
        $photo = "Minneapolis,-MN.png";
        break;
	case "new york":
        $photo = "New-York,-NY.png";
        break;
	case "oklahoma city":
        $photo = "Oklahoma-City,-OK.png";
        break;
	case "orlando":
        $photo = "Orlando,-FL.png";
        break;
	case "ottawa":
        $photo = "Ottawa,-Canada.png";
        break;
	case "philadelphia":
        $photo = "Philadelphia,-PA.png";
        break;
	case "phoenix":
        $photo = "Phoenix,-AZ.png";
        break;
	case "phoenix, az":
        $photo = "Phoenix,-AZ.png";
        break;
	case "raleigh durham":
        $photo = "Raleigh-Durham,-NC.png";
        break;
	case "salt lake city":
        $photo = "Salt-Lake-City,-UT.png";
        break;
	case "san diego":
        $photo = "San-Diego,-CA.png";
        break;
	case "san francisco":
        $photo = "San-Francisco,-CA.png";
        break;
	case "sydney":
        $photo = "Sydney,-Australia.png";
        break;
	case "tampa":
        $photo = "Tampa,-FL.png";
        break;
	case "toronto":
        $photo = "Toronto,-Canada.png";
        break;
	case "vancouver":
        $photo = "Vancouver,-Canada.png";
        break;
	case "waltham":
        $photo = "Waltham,-MA.png";
        break;
	case "washington":
        $photo = "Washington,-DC.png";
        break;
	case "wellington":
        $photo = "Wellington,-New-Zealand.png";
        break;
		
		
		}
	
	}
	// change date format
	$part_startdate = explode('-', $startdate);
			  $part_enddate = explode('-', $enddate);
			  
			    
			   $month = $part_startdate[1];
			   if($month == "01") { $month = "January"; }
			   if($month == "02") { $month = "February"; }
			   if($month == "03") { $month = "March"; }
			   if($month == "04") { $month = "April"; }
			   if($month == "05") { $month = "May"; }
			   if($month == "06") { $month = "June"; }
			   if($month == "07") { $month = "July"; }
			   if($month == "08") { $month = "August"; }
			   if($month == "09") { $month = "September"; }
			   if($month == "10") { $month = "October"; }
			   if($month == "11") { $month = "November"; }
			   if($month == "12") { $month = "December"; }
}
?>
  <div class="container">
    <section class="col-sm-7 col-sm-offset-1">
      <p><a href="http://www.informationmapping.com/training-roadshow"><img src="<?php echo "http://www.informationmapping.com/components/com_jeajaxeventcalendar/assets/event/images/".$photo; ?>" class="img-responsive" alt="Training Roadshow" style="margin-bottom: -10px;"></a></p>
      <h2><span style="color: #666">
	  <?php 
	  if(!empty($day3)) { echo $day3; }	 
	  else { 
	  echo $month . " " . $part_startdate[2] . "-" . $part_enddate[2] . ", " . $part_startdate[0];
	  }
	   ?></span><br /> 
	  <?php echo $title; ?></h2>
      <div class="text">
      <?php echo $desc; ?>
      </div>
      <ul class="nav nav-tabs">
        <li class="active"><a href="#audience" data-toggle="tab">Target audience</a></li>
        <li><a href="#outline" data-toggle="tab">Course outline</a></li>
        <li><a href="#package" data-toggle="tab">Course package</a></li>
        <li><a href="#details" data-toggle="tab">Details</a></li>
      </ul>
      
      
      <div class="tab-content">
        <div class="tab-pane active" id="audience">
          <?php echo $target; ?>
        </div>
        <div class="tab-pane" id="package">
          <?php echo $package; ?>
        </div>
        <div class="tab-pane" id="outline">
          <?php  
		  $outline = str_replace('src="images', 'src="http://www.informationmapping.com/images', $outline); 
		  echo $outline;
		  ?>
        </div>
        <div class="tab-pane" id="details" style="color: #666666;"> 
          <table>
            <tr>
              <th><i class="icon-calendar" style="color:#009edf;"></i> </th>
              <td><?php
			  
			   
			   
	  if(!empty($day3)) { echo $day3; }	 
	  else { 
	  echo $month . " " . $part_startdate[2] . "-" . $part_enddate[2] . ", " . $part_startdate[0];
	  }
	   ?>  (<?php echo $start; ?> - <?php echo $end; ?>)</td>
            </tr>
            <tr>
              <th><i class="icon-comments" style="color:#009edf;"></i> </th>
              <td><?php echo $language; ?></td>
            </tr>
            <tr>
              <th><i class="icon-money" style="color:#009edf;"></i> </th>
              <td> <?php echo $price; ?> <?php if($lunch == "Yes") { echo " (lunch and refreshments included)"; } ?></td>
            </tr>
            <tr>
              <th><i class="icon-info" style="color:#009edf;"></i> </th>
              <td><?php echo $phone; ?> or <a href="mailto:info@informationmapping.com">e-mail</a></td>
            </tr>
            <tr>
              <th style="vertical-align:top;"><i class="icon-location-arrow" style="color:#009edf;"></i> </th>
              <td><?php 
			  if(!empty($street)) { echo $street . ", "; }
			  ?>
              <?php echo $city; ?> (<?php echo $country; ?>)
              
              <?php

	//$address = "Rue%20des%20Colonies%2011+Brussels+BELGIUM";
	$address = "$street+$city+$country";
	$data = simplexml_load_file("https://maps.googleapis.com/maps/api/geocode/xml?address=" . $address . "&sensor=false");

	$coordinates_lat = $data->result->geometry->location->lat;
	$coordinates_lng = $data->result->geometry->location->lng;

echo "<p>&nbsp;</p><p>
<a href=\"https://maps.google.com/maps?q=$address&amp;z=17\" target=\"_blank\">
<img src=\"http://maps.googleapis.com/maps/api/staticmap?center=$address&amp;zoom=17&amp;size=500x200&amp;maptype=roadmap&amp;markers=color:d4145a%7Clabel:Roadshow%7C" . $coordinates_lat . "," . $coordinates_lng . "&amp;sensor=false\" class=\"img-responsive\" alt=\"View training roadshow location\" /></a></p>";

?>
              
              </td>
            </tr>
          </table>
          
          
          
        </div>
      </div><p><br><a href="http://www.informationmapping.com/training-roadshow">Have a look at the other cities of our Training Roadshow!</a></p>
    </section>
    <section class="col-sm-4">
      <div style="border-top:10px solid #ffffff;margin-top:-10px;">
        <div id="form">
          <h2>I'm interested</h2>
          <?php





/*************************************************
        FORMULIERVARIABELEN BEVEILIGEN
*************************************************/

// zet gevaarlijke tekens in invoervelden om
function cleanup($string="")
{
$string = trim($string); // verwijdert spaties aan begin en einde
//$string = htmlentities($string); // converteert < > & " en andere speciale tekens naar veilige tekens
$string = htmlspecialchars($string);
$string = nl2br($string); // converteert linebreaks in textareas naar <br />
// plaatst escapetekens bij quotes indien nodig
if(!get_magic_quotes_gpc())
  {
  $string = addslashes($string);
  }

return $string;
}

// MX-record e-mailadres beveiligen - checkdnsrr() werkt niet op Windowsservers
function check_email($mail_address) { 
    $pattern = "/^[\w-]+(\.[\w-]+)*@"; 
    $pattern .= "([0-9a-z][0-9a-z-]*[0-9a-z]\.)+([a-z]{2,4})$/i"; 
    if (preg_match($pattern, $mail_address)) { 
        $parts = explode("@", $mail_address); 
        if (checkdnsrr($parts[1], "MX")){ 
            // echo "The e-mail address is valid."; 
            return true; 
        } else { 
            // echo "The e-mail host is not valid."; 
            return false; 
        } 
    } else { 
        // echo "The e-mail address contains invalid characters."; 
        return false; 
    } 
} 

/** GeoIP **/

// source: http://www.geoplugin.com/webservices/php
// uses the famous Maxmind DB
    $client  = @$_SERVER['HTTP_CLIENT_IP'];
    $forward = @$_SERVER['HTTP_X_FORWARDED_FOR'];
    $remote  = $_SERVER['REMOTE_ADDR'];
    $result  = "Unknown";
    if(filter_var($client, FILTER_VALIDATE_IP))
    {
        $ip = $client;
    }
    elseif(filter_var($forward, FILTER_VALIDATE_IP))
    {
        $ip = $forward;
    }
    else
    {
        $ip = $remote;
    }

    $ip_data = @json_decode(file_get_contents("http://www.geoplugin.net/json.gp?ip=".$ip));

    if($ip_data && $ip_data->geoplugin_countryName != null)
    {
        $visitor_country = $ip_data->geoplugin_countryName;
    }

	// if you need 2 lettre code use $ip_data->geoplugin_countryCode

//echo $visitor_country; // Output Coutry name [Ex: United States]



/*************************************************
            FORMULIERGEGEVENS
*************************************************/
$firstname = isset($_POST['firstname']) ? cleanup($_POST['firstname']) : '';
$lastname = isset($_POST['lastname']) ? cleanup($_POST['lastname']) : '';
$email = isset($_POST['email']) ? cleanup($_POST['email']) : '';
$phone = isset($_POST['phone']) ? cleanup($_POST['phone']) : '';
//$job = isset($_POST['job']) ? cleanup($_POST['job']) : '';
$company = isset($_POST['company']) ? cleanup($_POST['company']) : '';
//$country = isset($_POST['country']) ? cleanup($_POST['country']) : '';
$verzenden = isset($_POST['verzenden']) ? cleanup($_POST['verzenden']) : '';
$ip = $_SERVER['REMOTE_ADDR'];
$training = $city . " ($country), " . $month . " " . $part_startdate[2] . "-" . $part_enddate[2] . ", " . $part_startdate[0];

if(isset($verzenden)) {


$country = $visitor_country; 


if(empty($firstname) || strlen($firstname) > 100)
{
	$error_firstname = "<div class=\"alert alert-danger\">First name not filled in.</div>";
}

if(empty($lastname) || strlen($lastname) > 100)
{
	$error_lastname = "<div class=\"alert alert-danger\">Last name not filled in.</div>";
}

if(empty($email) || strlen($email) > 150)
{
	$error_email = "<div class=\"alert alert-danger\">E-mail address not filled in.</div>";
}

if(empty($phone) || strlen($phone) > 150)
{
	$error_phone = "<div class=\"alert alert-danger\">Phone not filled in.</div>";
}

if(empty($company) || strlen($company) > 250)
{
	$error_company = "<div class=\"alert alert-danger\">Company not filled in.</div>";
}

if(empty($error_firstname) && empty($error_lastname) && empty($error_email) && empty($error_phone) && empty($error_company))
{
	
$form_sent = "1";
echo "<p>Thank you for your interest in this training class. We will contact you soon.</p><p><a href=\"http://www.informationmapping.com/en/\">Go back to our website</a></p>";

echo "<iframe src=\"http://www2.informationmapping.com/l/8622/2014-03-12/f9bh5?email=$email&firstname=$firstname&lastname=$lastname&company=$company&phone=$phone&country=$visitor_country&ip=$ip&training=$training\" width=\"1\" height=\"1\" style=\"display: none;\"></iframe>";


$message = "<!DOCTYPE html PUBLIC \"-//W3C//DTD XHTML 1.0 Transitional//EN\" \"http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd\">
<html>
    <head>
        <meta http-equiv=\"Content-Type\" content=\"text/html; charset=utf-8\">
        
        <!-- Facebook sharing information tags -->
        <meta property=\"og:title\" content=\"%%subject%%\">
        
        <title>Information Mapping&reg;</title>
    
  <style type=\"text/css\">
<!--
body {
    background-color: #EFEFEF;
}
-->
</style></head>
    <body leftmargin=\"0\" marginwidth=\"0\" topmargin=\"0\" marginheight=\"0\" offset=\"0\" style=\"-webkit-text-size-adjust: none;margin: 0;padding: 0;background-color: #EEEEEE;width: 100%;\">
<center>
<table id=\"backgroundTable\" style=\"background-color: #eeeeee; width: 100%; height: 100%; padding: 0px; margin: 0px;\" border=\"0\" cellpadding=\"0\" cellspacing=\"0\">
<tbody>
<tr>
<td style=\"border-collapse: collapse;\" align=\"center\" bgcolor=\"#EFEFEF\" valign=\"top\"><!-- // Begin Template Preheader \ -->
<table id=\"templatePreheader\" style=\"background-color: #efefef; width: 600px;\" border=\"0\" cellpadding=\"10\" cellspacing=\"0\">
<tbody>
<tr>
<td class=\"preheaderContent\" style=\"border-collapse: collapse;\" valign=\"top\"><!-- // Begin Module: Standard Preheader  -->
<table style=\"width: 100%;\" border=\"0\" cellpadding=\"0\" cellspacing=\"0\">
<tbody>
<tr>
<td style=\"border-collapse: collapse;\" valign=\"top\">
<div pardot-region=\"std_preheader_content\" style=\"color: #505050; font-family: Arial,Helvetica,sans-serif; font-size: 10px; line-height: 100%; text-align: left;\" class=\"\">Thank you for your interest</div>
</td>
<!--  -->
<td style=\"border-collapse: collapse;\" valign=\"top\" width=\"190\">
<div pardot-data=\"link-color:#00a0df;\" pardot-region=\"std_preheader_links\" style=\"color: #505050; font-family: Arial; font-size: 10px; line-height: 100%; text-align: left;\">
<div style=\"text-align: right;\"></div>
</div>
</td>
<!--  --></tr>
</tbody>
</table>
<!-- // End Module: Standard Preheader  --></td>
</tr>
</tbody>
</table>
<!-- // End Template Preheader \ -->
<table id=\"templateContainer\" style=\"background-color: #ffffff; width: 600px; border-width: 1px; border-color: #dddddd; border-style: solid;\" border=\"0\" cellpadding=\"0\" cellspacing=\"0\">
<tbody>
<tr>
<td style=\"border-collapse: collapse;\" align=\"center\" valign=\"top\"><!-- // Begin Template Header \ -->
<table id=\"templateHeader\" style=\"background-color: #ffffff; border-bottom-width: 0px; border-bottom-style: initial; border-bottom-color: initial; width: 600px;\" border=\"0\" cellpadding=\"0\" cellspacing=\"0\">
<tbody>
<tr>
<td class=\"headerContent\" style=\"border-collapse: collapse; color: #202020; font-family: Arial; font-size: 34px; font-weight: bold; line-height: 100%; padding: 0; text-align: center; vertical-align: middle; margin-bottom:20px;\" pardot-region=\"header_image\"><!-- // Begin Module: Standard Header Image \ --> <img src=\"http://www2.informationmapping.com/l/8622/2012-08-22/7zl26/8622/65115/logobanner.jpg\" alt=\"logobanner\" width=\"600\" height=\"71\" border=\"0\" id=\"headerImage campaign-icon\" style=\"max-width: 600px; border: 0; height: auto; line-height: 100%; outline: none; text-decoration: none;\" title=\"logobanner\"> <!-- // End Module: Standard Header Image \ --></td>


</tr></tbody>
</table>
<!-- // End Template Header \ --></td>
</tr>
<tr><td><table style=\"margin-top:20px; margin-right:0px; margin-bottom:0px; margin-left:20px\" border=\"0\" cellpadding=\"0\" cellspacing=\"0\"><tbody><tr><td><div pardot-data=\"link-color:#00a0df;\" pardot-region=\"std_content00\" style=\"color: #505050; font-family: Arial,Helvetica,sans-serif; font-size: 14px; line-height: 21px; text-align: left; background: none repeat scroll 0% 0% #ffffff;\" class=\"\">
  <p><span style=\"font-size: 18px; color: #009edf;\">Thank you for your interest</span></p></div></td></tr></tbody></table></td></tr>
<tr>
<td style=\"border-collapse: collapse;\" align=\"center\" valign=\"top\"><!-- // Begin Template Body \ -->
<table id=\"templateBody\" style=\"width: 600px;\" border=\"0\" cellpadding=\"0\" cellspacing=\"0\">
<tbody>
<tr><!-- // Begin Sidebar \  -->
<td style=\"border-collapse: collapse;\" valign=\"top\">
<table border=\"0\" cellpadding=\"0\" cellspacing=\"0\">
<tbody>
<tr>
<td class=\"bodyContent\" style=\"border-collapse: collapse; background-color: #ffffff;\" valign=\"top\"><!-- // Begin Module: Standard Content \ -->
  <table style=\"width: 100%;\" border=\"0\" cellpadding=\"20\" cellspacing=\"0\">
  <tbody>
  <tr>
  <td style=\"border-collapse: collapse;\" valign=\"top\">
  <div pardot-data=\"link-color:#00a0df;\" pardot-region=\"std_content00\" style=\"color: #505050; font-family: Arial,Helvetica,sans-serif; font-size: 14px; line-height: 21px; text-align: left; background: none repeat scroll 0% 0% #ffffff;\" class=\"\">
    <p>Hi $firstname,</p>
    <p>Thank you for your interest in one of our public training programs. We will contact you soon to take your registration. If you have any questions please do not hesitate to contact <a href=\"mailto:info@informationmapping.com\" style=\"color: #009EDF;\">info@informationmapping.com</a>.</p>
    <p>Regards,</p>
    <p>The Information Mapping Team</p>
  </div>
  </td>
  </tr>
  </tbody>
  </table>
  <!-- // End Module: Standard Content \ --></td>
</tr>
</tbody>
</table>
</td>
<!-- // End Sidebar \ --></tr>
</tbody>
</table>
</td>
</tr>
</tbody>
</table>
<!-- // End Template Body \ --></td>
</tr>
<tr>
<td style=\"border-collapse: collapse;\" align=\"center\" valign=\"top\"><!-- // Begin Template Footer \ -->
<table id=\"templateFooter\" style=\"background-color: #fafafa; border-top-width: 0px; border-top-style: initial; border-top-color: initial; width: 600px;\" border=\"0\" cellpadding=\"20\" cellspacing=\"0\">
<tbody>
<tr>
<td class=\"footerContent\" style=\"background-color: #fafafa; border-collapse: collapse; border-top: 1px solid #dddddd;\" valign=\"top\"><!-- // Begin Module: Standard Footer \ -->
<table style=\"width: 100%;\" border=\"0\" cellpadding=\"0\" cellspacing=\"0\">
<tbody>
<tr>
<td id=\"social\" style=\"border-collapse: collapse; background-color: #fafafa; border: 0;\" valign=\"middle\" width=\"540\">
<div pardot-region=\"std_social\" style=\"color: #777777; font-family: Arial,Helvetica,sans-serif; font-size: 12px; line-height: 17px; text-align: center;\">
<div style=\"text-align: left;\">
<table style=\"width: 100%;\" border=\"0\" cellspacing=\"0\" cellpadding=\"0\">
<tbody>
<tr>
<td align=\"left\" valign=\"top\">
<p><span style=\"font-family: arial, helvetica, sans-serif; font-size: 11px; color: #696969;\"><strong>INFORMATION MAPPING INTERNATIONAL | </strong>Ottergemsesteenweg Zuid 808, box 340 - 9000 Ghent (Belgium)<br>
  T: +32 9 253 14 25</span> | <span style=\"font-family: arial,helvetica,sans-serif; font-size: 11px;\"><a style=\"color: #009EDF;\" href=\"www.informationmapping.com\" target=\"_self\">www.informationmapping.com</a></span></p>
</td>
</tr>
</tbody>
</table>
</div>
</div>
</td>
</tr>

</tbody>
</table>
</td>
</tr>
</tbody>
</table>
</td>
</tr>
</tbody>
</table>




</center>
</body>
</html>";

$onderwerp = "Training Roadshow - Information Mapping";
$bcc = "wvandessel@informationmapping.com";
$from = "info@informationmapping.com";

// headers seperated by \n on Unix, \r\n on Windows servers
$headers  = "MIME-Version: 1.0\n";
$headers .= "Content-type: text/html; charset=utf-8\n";		
$headers .= "From: Information Mapping International <$from>\n"; // From must be the same as the domain you are sending from, otherwise will not work
$headers .= "Reply-To: Information Mapping International <$from>\n";
$headers .= "Return-Path: IMI <info@informationmapping.com>\n";
$headers .= "BCC: <$bcc>\n";
//$headers .= "X-Priority: $prioriteit\n"; // 3 voor normale prioriteit  	
$headers .= "Date: " . date("r") . "\n"; 


	
	  /*$headers  = "MIME-Version: 1.0".$eol;
	$headers .= "Content-Type: multipart/mixed; boundary=\"".$separator."\"".$eol.$eol; 
    $headers .= "Content-Transfer-Encoding: 7bit".$eol;
    $headers .= "This is a MIME encoded message.".$eol.$eol;
	  $headers .= "From: Information Mapping International <$from>".$eol;
      $headers .= "BCC: <$bcc>".$eol;
	  $headers .= "Reply-To: IMI <info@informationmapping.com>".$eol;
	  $headers .= "Return-Path: IMI <info@informationmapping.com>".$eol;
      $headers .= "X-Priority: $prioriteit".$eol; // 3 voor normale prioriteit 
	  $headers .= "Date: " . date("r") .$eol; 
	  
	      // message
    $headers .= "--".$separator.$eol;
    $headers .= "Content-Type: text/html; charset=\"UTF-8\"".$eol;
    $headers .= "Content-Transfer-Encoding: 8bit".$eol.$eol;
    $headers .= $message.$eol.$eol;

    // attachment
    $headers .= "--".$separator.$eol;
    $headers .= "Content-Type: application/octet-stream; name=\"".$filename."\"".$eol; 
    $headers .= "Content-Transfer-Encoding: base64".$eol;
    $headers .= "Content-Disposition: attachment".$eol.$eol;
    $headers .= $attachment.$eol.$eol;
    $headers .= "--".$separator."--";*/
	  
	  $to = $email; // e-mailadres van de ontvanger
	  
	  mail($to, $onderwerp, $message, $headers, "-f info@informationmapping.com"); // fifth parameter is obliged for some webhosts
}

}// einde isset $verzenden
?>
          <?php
$form_sent = isset($form_sent);
if($form_sent != 1)
{ ?>
          <p>Please complete this form and someone will get back to you shortly to take your registration.          </p>
          <form class="form-horizontal" role="form" action="#" method="post">
            <div class="form-group">
              <div class="col-sm-12">
                <label for="firstname">First name</label>
              </div>
              <div class="col-sm-12">
                <input type="text" class="form-control input-sm" id="firstname" name="firstname" value="<?php if (isset($_POST['verzenden'])) { echo $firstname; } ?>">
                <?php if($error_firstname && isset($_POST['verzenden'])) { echo $error_firstname; } ?>
              </div>
            </div>
            <div class="form-group">
              <div class="col-sm-12">
                <label for="lastname">Last name</label>
              </div>
              <div class="col-sm-12">
                <input type="text" class="form-control input-sm" id="lastname" name="lastname" value="<?php if (isset($_POST['verzenden'])) { echo $lastname; } ?>">
                <?php if($error_lastname && isset($_POST['verzenden'])) { echo $error_lastname; } ?>
              </div>
            </div>
            <div class="form-group">
              <div class="col-sm-12">
                <label for="company">Company</label>
              </div>
              <div class="col-sm-12">
                <input type="text" class="form-control input-sm" id="company" name="company" value="<?php if (isset($_POST['verzenden'])) { echo $company; } ?>">
                <?php if($error_company && isset($_POST['verzenden'])) { echo $error_company; } ?>
              </div>
            </div>
            <div class="form-group">
              <div class="col-sm-12">
                <label for="email">E-mail address</label>
              </div>
              <div class="col-sm-12">
                <input type="email" class="form-control input-sm" id="email" name="email" value="<?php if (isset($_POST['verzenden'])) { echo $email; } ?>">
                <?php if($error_email && isset($_POST['verzenden'])) { echo $error_email; } ?>
              </div>
            </div>
            <div class="form-group">
              <div class="col-sm-12">
                <label for="phone">Phone</label>
              </div>
              <div class="col-sm-12">
                <input type="tel" class="form-control input-sm" id="phone" name="phone" value="<?php if (isset($_POST['verzenden'])) { echo $phone; } ?>">
                <?php if($error_phone && isset($_POST['verzenden'])) { echo $error_phone; } ?>
              </div>
            </div>

        <div class="form-group">
          <div class="col-sm-12">
            <label for="country">Country</label>
          </div>
          <div class="col-sm-12">
            <select name="country" id="country" class="form-control">
              <option value="Afghanistan"<?php if($visitor_country =="Afghanistan") { echo " selected"; } ?>>Afghanistan</option>
              <option value="Albania"<?php if($visitor_country =="Albania") { echo " selected"; } ?>>Albania</option>
              <option value="Algeria"<?php if($visitor_country =="Algeria") { echo " selected"; } ?>>Algeria</option>
              <option value="American Samoa"<?php if($visitor_country =="American Samoa") { echo " selected"; } ?>>American Samoa</option>
              <option value="Andorra"<?php if($visitor_country =="Andorra") { echo " selected"; } ?>>Andorra</option>
              <option value="Angola"<?php if($visitor_country =="Angola") { echo " selected"; } ?>>Angola</option>
              <option value="Anguilla"<?php if($visitor_country =="Anguilla") { echo " selected"; } ?>>Anguilla</option>
              <option value="Antarctica"<?php if($visitor_country =="Antarctica") { echo " selected"; } ?>>Antarctica</option>
              <option value="Antigua and Barbuda"<?php if($visitor_country =="Antigua and Barbuda") { echo " selected"; } ?>>Antigua and Barbuda</option>
              <option value="Argentina"<?php if($visitor_country =="Argentina") { echo " selected"; } ?>>Argentina</option>
              <option value="Armenia"<?php if($visitor_country =="Armenia") { echo " selected"; } ?>>Armenia</option>
              <option value="Aruba"<?php if($visitor_country =="Aruba") { echo " selected"; } ?>>Aruba</option>
              <option value="Australia"<?php if($visitor_country =="Australia") { echo " selected"; } ?>>Australia</option>
              <option value="Austria"<?php if($visitor_country =="Austria") { echo " selected"; } ?>>Austria</option>
              <option value="Azerbaijan"<?php if($visitor_country =="Azerbaijan") { echo " selected"; } ?>>Azerbaijan</option>
              <option value="Bahamas"<?php if($visitor_country =="Bahamas") { echo " selected"; } ?>>Bahamas</option>
              <option value="Bahrain"<?php if($visitor_country =="Bahrain") { echo " selected"; } ?>>Bahrain</option>
              <option value="Bangladesh"<?php if($visitor_country =="Bangladesh") { echo " selected"; } ?>>Bangladesh</option>
              <option value="Barbados"<?php if($visitor_country =="Barbados") { echo " selected"; } ?>>Barbados</option>
              <option value="Belarus"<?php if($visitor_country =="Belarus") { echo " selected"; } ?>>Belarus</option>
              <option value="Belgium"<?php if($visitor_country =="Belgium") { echo " selected"; } ?>>Belgium</option>
              <option value="Belize"<?php if($visitor_country =="Belize") { echo " selected"; } ?>>Belize</option>
              <option value="Benin"<?php if($visitor_country =="Benin") { echo " selected"; } ?>>Benin</option>
              <option value="Bermuda"<?php if($visitor_country =="Bermuda") { echo " selected"; } ?>>Bermuda</option>
              <option value="Bhutan"<?php if($visitor_country =="Bhutan") { echo " selected"; } ?>>Bhutan</option>
              <option value="Bolivia"<?php if($visitor_country =="Bolivia") { echo " selected"; } ?>>Bolivia</option>
              <option value="Bosnia and Herzegovina"<?php if($visitor_country =="Bosnia and Herzegovina") { echo " selected"; } ?>>Bosnia and Herzegovina</option>
              <option value="Botswana"<?php if($visitor_country =="Botswana") { echo " selected"; } ?>>Botswana</option>
              <option value="Bouvet Island"<?php if($visitor_country =="Bouvet Island") { echo " selected"; } ?>>Bouvet Island</option>
              <option value="Brazil"<?php if($visitor_country =="Brazil") { echo " selected"; } ?>>Brazil</option>
              <option value="British Indian Ocean Territory"<?php if($visitor_country =="British Indian Ocean Territory") { echo " selected"; } ?>>British Indian Ocean Territory</option>
              <option value="Brunei Darussalam"<?php if($visitor_country =="Brunei Darussalam") { echo " selected"; } ?>>Brunei Darussalam</option>
              <option value="Bulgaria"<?php if($visitor_country =="Bulgaria") { echo " selected"; } ?>>Bulgaria</option>
              <option value="Burkina Faso"<?php if($visitor_country =="Burkina Faso") { echo " selected"; } ?>>Burkina Faso</option>
              <option value="Burundi"<?php if($visitor_country =="Burundi") { echo " selected"; } ?>>Burundi</option>
              <option value="Cambodia"<?php if($visitor_country =="Cambodia") { echo " selected"; } ?>>Cambodia</option>
              <option value="Cameroon"<?php if($visitor_country =="Cameroon") { echo " selected"; } ?>>Cameroon</option>
              <option value="Canada"<?php if($visitor_country =="Canada") { echo " selected"; } ?>>Canada</option>
              <option value="Cape Verde"<?php if($visitor_country =="Cape Verde") { echo " selected"; } ?>>Cape Verde</option>
              <option value="Cayman Islands"<?php if($visitor_country =="Cayman Islands") { echo " selected"; } ?>>Cayman Islands</option>
              <option value="Central African Republic"<?php if($visitor_country =="Central African Republic") { echo " selected"; } ?>>Central African Republic</option>
              <option value="Chad"<?php if($visitor_country =="Chad") { echo " selected"; } ?>>Chad</option>
              <option value="Chile"<?php if($visitor_country =="Chile") { echo " selected"; } ?>>Chile</option>
              <option value="China"<?php if($visitor_country =="China") { echo " selected"; } ?>>China</option>
              <option value="Christmas Island"<?php if($visitor_country =="Christmas Island") { echo " selected"; } ?>>Christmas Island</option>
              <option value="Cocos (Keeling) Islands"<?php if($visitor_country =="Cocos (Keeling) Islands") { echo " selected"; } ?>>Cocos (Keeling) Islands</option>
              <option value="Colombia"<?php if($visitor_country =="Colombia") { echo " selected"; } ?>>Colombia</option>
              <option value="Comoros"<?php if($visitor_country =="Comoros") { echo " selected"; } ?>>Comoros</option>
              <option value="Congo"<?php if($visitor_country =="Congo") { echo " selected"; } ?>>Congo</option>
              <option value="Congo, The Democratic Republic of The"<?php if($visitor_country =="Congo, The Democratic Republic of The") { echo " selected"; } ?>>Congo, The Democratic Republic of The</option>
              <option value="Cook Islands"<?php if($visitor_country =="Cook Islands") { echo " selected"; } ?>>Cook Islands</option>
              <option value="Costa Rica"<?php if($visitor_country =="Costa Rica") { echo " selected"; } ?>>Costa Rica</option>
              <option value="Cote D'ivoire"<?php if($visitor_country =="Cote D'ivoire") { echo " selected"; } ?>>Cote D'ivoire</option>
              <option value="Croatia"<?php if($visitor_country =="Croatia") { echo " selected"; } ?>>Croatia</option>
              <option value="Cuba"<?php if($visitor_country =="Cuba") { echo " selected"; } ?>>Cuba</option>
              <option value="Cyprus"<?php if($visitor_country =="Cyprus") { echo " selected"; } ?>>Cyprus</option>
              <option value="Czech Republic"<?php if($visitor_country =="Czech Republic") { echo " selected"; } ?>>Czech Republic</option>
              <option value="Denmark"<?php if($visitor_country =="Denmark") { echo " selected"; } ?>>Denmark</option>
              <option value="Djibouti"<?php if($visitor_country =="Djibouti") { echo " selected"; } ?>>Djibouti</option>
              <option value="Dominica"<?php if($visitor_country =="Dominica") { echo " selected"; } ?>>Dominica</option>
              <option value="Dominican Republic"<?php if($visitor_country =="Dominican Republic") { echo " selected"; } ?>>Dominican Republic</option>
              <option value="Ecuador"<?php if($visitor_country =="Ecuador") { echo " selected"; } ?>>Ecuador</option>
              <option value="Egypt"<?php if($visitor_country =="Egypt") { echo " selected"; } ?>>Egypt</option>
              <option value="El Salvador"<?php if($visitor_country =="El Salvador") { echo " selected"; } ?>>El Salvador</option>
              <option value="Equatorial Guinea"<?php if($visitor_country =="Equatorial Guinea") { echo " selected"; } ?>>Equatorial Guinea</option>
              <option value="Eritrea"<?php if($visitor_country =="Eritrea") { echo " selected"; } ?>>Eritrea</option>
              <option value="Estonia"<?php if($visitor_country =="Estonia") { echo " selected"; } ?>>Estonia</option>
              <option value="Ethiopia"<?php if($visitor_country =="Ethiopia") { echo " selected"; } ?>>Ethiopia</option>
              <option value="Falkland Islands (Malvinas)"<?php if($visitor_country =="Falkland Islands (Malvinas)") { echo " selected"; } ?>>Falkland Islands (Malvinas)</option>
              <option value="Faroe Islands"<?php if($visitor_country =="Faroe Islands") { echo " selected"; } ?>>Faroe Islands</option>
              <option value="Fiji"<?php if($visitor_country =="Fiji") { echo " selected"; } ?>>Fiji</option>
              <option value="Finland"<?php if($visitor_country =="Finland") { echo " selected"; } ?>>Finland</option>
              <option value="France"<?php if($visitor_country =="France") { echo " selected"; } ?>>France</option>
              <option value="French Guiana"<?php if($visitor_country =="French Guiana") { echo " selected"; } ?>>French Guiana</option>
              <option value="French Polynesia"<?php if($visitor_country =="French Polynesia") { echo " selected"; } ?>>French Polynesia</option>
              <option value="French Southern Territories"<?php if($visitor_country =="French Southern Territories") { echo " selected"; } ?>>French Southern Territories</option>
              <option value="Gabon"<?php if($visitor_country =="Gabon") { echo " selected"; } ?>>Gabon</option>
              <option value="Gambia"<?php if($visitor_country =="Gambia") { echo " selected"; } ?>>Gambia</option>
              <option value="Georgia"<?php if($visitor_country =="Georgia") { echo " selected"; } ?>>Georgia</option>
              <option value="Germany"<?php if($visitor_country =="Germany") { echo " selected"; } ?>>Germany</option>
              <option value="Ghana"<?php if($visitor_country =="Ghana") { echo " selected"; } ?>>Ghana</option>
              <option value="Gibraltar"<?php if($visitor_country =="Gibraltar") { echo " selected"; } ?>>Gibraltar</option>
              <option value="Greece"<?php if($visitor_country =="Greece") { echo " selected"; } ?>>Greece</option>
              <option value="Greenland"<?php if($visitor_country =="Greenland") { echo " selected"; } ?>>Greenland</option>
              <option value="Grenada"<?php if($visitor_country =="Grenada") { echo " selected"; } ?>>Grenada</option>
              <option value="Guadeloupe"<?php if($visitor_country =="Guadeloupe") { echo " selected"; } ?>>Guadeloupe</option>
              <option value="Guam"<?php if($visitor_country =="Guam") { echo " selected"; } ?>>Guam</option>
              <option value="Guatemala"<?php if($visitor_country =="Guatemala") { echo " selected"; } ?>>Guatemala</option>
              <option value="Guinea"<?php if($visitor_country =="Guinea") { echo " selected"; } ?>>Guinea</option>
              <option value="Guinea-bissau"<?php if($visitor_country =="Guinea-bissau") { echo " selected"; } ?>>Guinea-bissau</option>
              <option value="Guyana"<?php if($visitor_country =="Guyana") { echo " selected"; } ?>>Guyana</option>
              <option value="Haiti"<?php if($visitor_country =="Haiti") { echo " selected"; } ?>>Haiti</option>
              <option value="Heard Island and Mcdonald Islands"<?php if($visitor_country =="Heard Island and Mcdonald Islands") { echo " selected"; } ?>>Heard Island and Mcdonald Islands</option>
              <option value="Holy See (Vatican City State)"<?php if($visitor_country =="Holy See (Vatican City State)") { echo " selected"; } ?>>Holy See (Vatican City State)</option>
              <option value="Honduras"<?php if($visitor_country =="Honduras") { echo " selected"; } ?>>Honduras</option>
              <option value="Hong Kong"<?php if($visitor_country =="Hong Kong") { echo " selected"; } ?>>Hong Kong</option>
              <option value="Hungary"<?php if($visitor_country =="Hungary") { echo " selected"; } ?>>Hungary</option>
              <option value="Iceland"<?php if($visitor_country =="Iceland") { echo " selected"; } ?>>Iceland</option>
              <option value="India"<?php if($visitor_country =="India") { echo " selected"; } ?>>India</option>
              <option value="Indonesia"<?php if($visitor_country =="Indonesia") { echo " selected"; } ?>>Indonesia</option>
              <option value="Iran, Islamic Republic of"<?php if($visitor_country =="Iran, Islamic Republic of") { echo " selected"; } ?>>Iran, Islamic Republic of</option>
              <option value="Iraq"<?php if($visitor_country =="Iraq") { echo " selected"; } ?>>Iraq</option>
              <option value="Ireland"<?php if($visitor_country =="Ireland") { echo " selected"; } ?>>Ireland</option>
              <option value="Israel"<?php if($visitor_country =="Israel") { echo " selected"; } ?>>Israel</option>
              <option value="Italy"<?php if($visitor_country =="Italy") { echo " selected"; } ?>>Italy</option>
              <option value="Jamaica"<?php if($visitor_country =="Jamaica") { echo " selected"; } ?>>Jamaica</option>
              <option value="Japan"<?php if($visitor_country =="Japan") { echo " selected"; } ?>>Japan</option>
              <option value="Jordan"<?php if($visitor_country =="Jordan") { echo " selected"; } ?>>Jordan</option>
              <option value="Kazakhstan"<?php if($visitor_country =="Kazakhstan") { echo " selected"; } ?>>Kazakhstan</option>
              <option value="Kenya"<?php if($visitor_country =="Kenya") { echo " selected"; } ?>>Kenya</option>
              <option value="Kiribati"<?php if($visitor_country =="Kiribati") { echo " selected"; } ?>>Kiribati</option>
              <option value="Korea, Democratic People's Republic of"<?php if($visitor_country =="Korea, Democratic People's Republic of") { echo " selected"; } ?>>Korea, Democratic People's Republic of</option>
              <option value="Korea, Republic of"<?php if($visitor_country =="Korea, Republic of") { echo " selected"; } ?>>Korea, Republic of</option>
              <option value="Kuwait"<?php if($visitor_country =="Kuwait") { echo " selected"; } ?>>Kuwait</option>
              <option value="Kyrgyzstan"<?php if($visitor_country =="Kyrgyzstan") { echo " selected"; } ?>>Kyrgyzstan</option>
              <option value="Lao People's Democratic Republic"<?php if($visitor_country =="Lao People's Democratic Republic") { echo " selected"; } ?>>Lao People's Democratic Republic</option>
              <option value="Latvia"<?php if($visitor_country =="Latvia") { echo " selected"; } ?>>Latvia</option>
              <option value="Lebanon"<?php if($visitor_country =="Lebanon") { echo " selected"; } ?>>Lebanon</option>
              <option value="Lesotho"<?php if($visitor_country =="Lesotho") { echo " selected"; } ?>>Lesotho</option>
              <option value="Liberia"<?php if($visitor_country =="Liberia") { echo " selected"; } ?>>Liberia</option>
              <option value="Libyan Arab Jamahiriya"<?php if($visitor_country =="Libyan Arab Jamahiriya") { echo " selected"; } ?>>Libyan Arab Jamahiriya</option>
              <option value="Liechtenstein"<?php if($visitor_country =="Liechtenstein") { echo " selected"; } ?>>Liechtenstein</option>
              <option value="Lithuania"<?php if($visitor_country =="Lithuania") { echo " selected"; } ?>>Lithuania</option>
              <option value="Luxembourg"<?php if($visitor_country =="Luxembourg") { echo " selected"; } ?>>Luxembourg</option>
              <option value="Macao"<?php if($visitor_country =="Macao") { echo " selected"; } ?>>Macao</option>
              <option value="Macedonia, The Former Yugoslav Republic of"<?php if($visitor_country =="Macedonia, The Former Yugoslav Republic of") { echo " selected"; } ?>>Macedonia, The Former Yugoslav Republic of</option>
              <option value="Madagascar"<?php if($visitor_country =="Madagascar") { echo " selected"; } ?>>Madagascar</option>
              <option value="Malawi"<?php if($visitor_country =="Malawi") { echo " selected"; } ?>>Malawi</option>
              <option value="Malaysia"<?php if($visitor_country =="Malaysia") { echo " selected"; } ?>>Malaysia</option>
              <option value="Maldives"<?php if($visitor_country =="Maldives") { echo " selected"; } ?>>Maldives</option>
              <option value="Mali"<?php if($visitor_country =="Mali") { echo " selected"; } ?>>Mali</option>
              <option value="Malta"<?php if($visitor_country =="Malta") { echo " selected"; } ?>>Malta</option>
              <option value="Marshall Islands"<?php if($visitor_country =="Marshall Islands") { echo " selected"; } ?>>Marshall Islands</option>
              <option value="Martinique"<?php if($visitor_country =="Martinique") { echo " selected"; } ?>>Martinique</option>
              <option value="Mauritania"<?php if($visitor_country =="Mauritania") { echo " selected"; } ?>>Mauritania</option>
              <option value="Mauritius"<?php if($visitor_country =="Mauritius") { echo " selected"; } ?>>Mauritius</option>
              <option value="Mayotte"<?php if($visitor_country =="Mayotte") { echo " selected"; } ?>>Mayotte</option>
              <option value="Mexico"<?php if($visitor_country =="Mexico") { echo " selected"; } ?>>Mexico</option>
              <option value="Micronesia, Federated States of"<?php if($visitor_country =="Micronesia, Federated States of") { echo " selected"; } ?>>Micronesia, Federated States of</option>
              <option value="Moldova, Republic of"<?php if($visitor_country =="Moldova, Republic of") { echo " selected"; } ?>>Moldova, Republic of</option>
              <option value="Monaco"<?php if($visitor_country =="Monaco") { echo " selected"; } ?>>Monaco</option>
              <option value="Mongolia"<?php if($visitor_country =="Mongolia") { echo " selected"; } ?>>Mongolia</option>
              <option value="Montserrat"<?php if($visitor_country =="Montserrat") { echo " selected"; } ?>>Montserrat</option>
              <option value="Morocco"<?php if($visitor_country =="Morocco") { echo " selected"; } ?>>Morocco</option>
              <option value="Mozambique"<?php if($visitor_country =="Mozambique") { echo " selected"; } ?>>Mozambique</option>
              <option value="Myanmar"<?php if($visitor_country =="Myanmar") { echo " selected"; } ?>>Myanmar</option>
              <option value="Namibia"<?php if($visitor_country =="Namibia") { echo " selected"; } ?>>Namibia</option>
              <option value="Nauru"<?php if($visitor_country =="Nauru") { echo " selected"; } ?>>Nauru</option>
              <option value="Nepal"<?php if($visitor_country =="Nepal") { echo " selected"; } ?>>Nepal</option>
              <option value="Netherlands"<?php if($visitor_country =="Netherlands") { echo " selected"; } ?>>Netherlands</option>
              <option value="Netherlands Antilles"<?php if($visitor_country =="Netherlands Antilles") { echo " selected"; } ?>>Netherlands Antilles</option>
              <option value="New Caledonia"<?php if($visitor_country =="New Caledonia") { echo " selected"; } ?>>New Caledonia</option>
              <option value="New Zealand"<?php if($visitor_country =="New Zealand") { echo " selected"; } ?>>New Zealand</option>
              <option value="Nicaragua"<?php if($visitor_country =="Nicaragua") { echo " selected"; } ?>>Nicaragua</option>
              <option value="Niger"<?php if($visitor_country =="Niger") { echo " selected"; } ?>>Niger</option>
              <option value="Nigeria"<?php if($visitor_country =="Nigeria") { echo " selected"; } ?>>Nigeria</option>
              <option value="Niue"<?php if($visitor_country =="Niue") { echo " selected"; } ?>>Niue</option>
              <option value="Norfolk Island"<?php if($visitor_country =="Norfolk Island") { echo " selected"; } ?>>Norfolk Island</option>
              <option value="Northern Mariana Islands"<?php if($visitor_country =="Northern Mariana Islands") { echo " selected"; } ?>>Northern Mariana Islands</option>
              <option value="Norway"<?php if($visitor_country =="Norway") { echo " selected"; } ?>>Norway</option>
              <option value="Oman"<?php if($visitor_country =="Oman") { echo " selected"; } ?>>Oman</option>
              <option value="Pakistan"<?php if($visitor_country =="Pakistan") { echo " selected"; } ?>>Pakistan</option>
              <option value="Palau"<?php if($visitor_country =="Palau") { echo " selected"; } ?>>Palau</option>
              <option value="Palestinian Territory, Occupied"<?php if($visitor_country =="Palestinian Territory, Occupied") { echo " selected"; } ?>>Palestinian Territory, Occupied</option>
              <option value="Panama"<?php if($visitor_country =="Panama") { echo " selected"; } ?>>Panama</option>
              <option value="Papua New Guinea"<?php if($visitor_country =="Papua New Guinea") { echo " selected"; } ?>>Papua New Guinea</option>
              <option value="Paraguay"<?php if($visitor_country =="Paraguay") { echo " selected"; } ?>>Paraguay</option>
              <option value="Peru"<?php if($visitor_country =="Peru") { echo " selected"; } ?>>Peru</option>
              <option value="Philippines"<?php if($visitor_country =="Philippines") { echo " selected"; } ?>>Philippines</option>
              <option value="Pitcairn"<?php if($visitor_country =="Pitcairn") { echo " selected"; } ?>>Pitcairn</option>
              <option value="Poland"<?php if($visitor_country =="Poland") { echo " selected"; } ?>>Poland</option>
              <option value="Portugal"<?php if($visitor_country =="Portugal") { echo " selected"; } ?>>Portugal</option>
              <option value="Puerto Rico"<?php if($visitor_country =="Puerto Rico") { echo " selected"; } ?>>Puerto Rico</option>
              <option value="Qatar"<?php if($visitor_country =="Qatar") { echo " selected"; } ?>>Qatar</option>
              <option value="Reunion"<?php if($visitor_country =="Reunion") { echo " selected"; } ?>>Reunion</option>
              <option value="Romania"<?php if($visitor_country =="Romania") { echo " selected"; } ?>>Romania</option>
              <option value="Russian Federation"<?php if($visitor_country =="Russian Federation") { echo " selected"; } ?>>Russian Federation</option>
              <option value="Rwanda"<?php if($visitor_country =="Rwanda") { echo " selected"; } ?>>Rwanda</option>
              <option value="Saint Helena"<?php if($visitor_country =="Saint Helena") { echo " selected"; } ?>>Saint Helena</option>
              <option value="Saint Kitts and Nevis"<?php if($visitor_country =="Saint Kitts and Nevis") { echo " selected"; } ?>>Saint Kitts and Nevis</option>
              <option value="Saint Lucia"<?php if($visitor_country =="Saint Lucia") { echo " selected"; } ?>>Saint Lucia</option>
              <option value="Saint Pierre and Miquelon"<?php if($visitor_country =="Saint Pierre and Miquelon") { echo " selected"; } ?>>Saint Pierre and Miquelon</option>
              <option value="Saint Vincent and The Grenadines"<?php if($visitor_country =="Saint Vincent and The Grenadines") { echo " selected"; } ?>>Saint Vincent and The Grenadines</option>
              <option value="Samoa"<?php if($visitor_country =="Samoa") { echo " selected"; } ?>>Samoa</option>
              <option value="San Marino"<?php if($visitor_country =="San Marino") { echo " selected"; } ?>>San Marino</option>
              <option value="Sao Tome and Principe"<?php if($visitor_country =="Sao Tome and Principe") { echo " selected"; } ?>>Sao Tome and Principe</option>
              <option value="Saudi Arabia"<?php if($visitor_country =="Saudi Arabia") { echo " selected"; } ?>>Saudi Arabia</option>
              <option value="Senegal"<?php if($visitor_country =="Senegal") { echo " selected"; } ?>>Senegal</option>
              <option value="Serbia and Montenegro"<?php if($visitor_country =="Serbia and Montenegro") { echo " selected"; } ?>>Serbia and Montenegro</option>
              <option value="Seychelles"<?php if($visitor_country =="Seychelles") { echo " selected"; } ?>>Seychelles</option>
              <option value="Sierra Leone"<?php if($visitor_country =="Sierra Leone") { echo " selected"; } ?>>Sierra Leone</option>
              <option value="Singapore"<?php if($visitor_country =="Singapore") { echo " selected"; } ?>>Singapore</option>
              <option value="Slovakia"<?php if($visitor_country =="Slovakia") { echo " selected"; } ?>>Slovakia</option>
              <option value="Slovenia"<?php if($visitor_country =="Slovenia") { echo " selected"; } ?>>Slovenia</option>
              <option value="Solomon Islands"<?php if($visitor_country =="Solomon Islands") { echo " selected"; } ?>>Solomon Islands</option>
              <option value="Somalia"<?php if($visitor_country =="Somalia") { echo " selected"; } ?>>Somalia</option>
              <option value="South Africa"<?php if($visitor_country =="South Africa") { echo " selected"; } ?>>South Africa</option>
              <option value="South Georgia and The South Sandwich Islands"<?php if($visitor_country =="South Georgia and The South Sandwich Islands") { echo " selected"; } ?>>South Georgia and The South Sandwich Islands</option>
              <option value="Spain"<?php if($visitor_country =="Spain") { echo " selected"; } ?>>Spain</option>
              <option value="Sri Lanka"<?php if($visitor_country =="Sri Lanka") { echo " selected"; } ?>>Sri Lanka</option>
              <option value="Sudan"<?php if($visitor_country =="Sudan") { echo " selected"; } ?>>Sudan</option>
              <option value="Suriname"<?php if($visitor_country =="Suriname") { echo " selected"; } ?>>Suriname</option>
              <option value="Svalbard and Jan Mayen"<?php if($visitor_country =="Svalbard and Jan Mayen") { echo " selected"; } ?>>Svalbard and Jan Mayen</option>
              <option value="Swaziland"<?php if($visitor_country =="Swaziland") { echo " selected"; } ?>>Swaziland</option>
              <option value="Sweden"<?php if($visitor_country =="Sweden") { echo " selected"; } ?>>Sweden</option>
              <option value="Switzerland"<?php if($visitor_country =="Switzerland") { echo " selected"; } ?>>Switzerland</option>
              <option value="Syrian Arab Republic"<?php if($visitor_country =="Syrian Arab Republic") { echo " selected"; } ?>>Syrian Arab Republic</option>
              <option value="Taiwan, Province of China"<?php if($visitor_country =="Taiwan, Province of China") { echo " selected"; } ?>>Taiwan, Province of China</option>
              <option value="Tajikistan"<?php if($visitor_country =="Tajikistan") { echo " selected"; } ?>>Tajikistan</option>
              <option value="Tanzania, United Republic of"<?php if($visitor_country =="Tanzania, United Republic of") { echo " selected"; } ?>>Tanzania, United Republic of</option>
              <option value="Thailand"<?php if($visitor_country =="Thailand") { echo " selected"; } ?>>Thailand</option>
              <option value="Timor-leste"<?php if($visitor_country =="Timor-leste") { echo " selected"; } ?>>Timor-leste</option>
              <option value="Togo"<?php if($visitor_country =="Togo") { echo " selected"; } ?>>Togo</option>
              <option value="Tokelau"<?php if($visitor_country =="Tokelau") { echo " selected"; } ?>>Tokelau</option>
              <option value="Tonga"<?php if($visitor_country =="Tonga") { echo " selected"; } ?>>Tonga</option>
              <option value="Trinidad and Tobago"<?php if($visitor_country =="Trinidad and Tobago") { echo " selected"; } ?>>Trinidad and Tobago</option>
              <option value="Tunisia"<?php if($visitor_country =="Tunisia") { echo " selected"; } ?>>Tunisia</option>
              <option value="Turkey"<?php if($visitor_country =="Turkey") { echo " selected"; } ?>>Turkey</option>
              <option value="Turkmenistan"<?php if($visitor_country =="Turkmenistan") { echo " selected"; } ?>>Turkmenistan</option>
              <option value="Turks and Caicos Islands"<?php if($visitor_country =="Turks and Caicos Islands") { echo " selected"; } ?>>Turks and Caicos Islands</option>
              <option value="Tuvalu"<?php if($visitor_country =="Tuvalu") { echo " selected"; } ?>>Tuvalu</option>
              <option value="Uganda"<?php if($visitor_country =="Uganda") { echo " selected"; } ?>>Uganda</option>
              <option value="Ukraine"<?php if($visitor_country =="Ukraine") { echo " selected"; } ?>>Ukraine</option>
              <option value="United Arab Emirates"<?php if($visitor_country =="United Arab Emirates") { echo " selected"; } ?>>United Arab Emirates</option>
              <option value="United Kingdom"<?php if($visitor_country =="United Kingdom") { echo " selected"; } ?>>United Kingdom</option>
              <option value="United States"<?php if($visitor_country =="United States") { echo " selected"; } ?>>United States</option>
              <option value="United States Minor Outlying Islands"<?php if($visitor_country =="United States Minor Outlying Islands") { echo " selected"; } ?>>United States Minor Outlying Islands</option>
              <option value="Uruguay"<?php if($visitor_country =="Uruguay") { echo " selected"; } ?>>Uruguay</option>
              <option value="Uzbekistan"<?php if($visitor_country =="Uzbekistan") { echo " selected"; } ?>>Uzbekistan</option>
              <option value="Vanuatu"<?php if($visitor_country =="Vanuatu") { echo " selected"; } ?>>Vanuatu</option>
              <option value="Venezuela"<?php if($visitor_country =="Venezuela") { echo " selected"; } ?>>Venezuela</option>
              <option value="Viet Nam"<?php if($visitor_country =="Viet Nam") { echo " selected"; } ?>>Viet Nam</option>
              <option value="Virgin Islands, British"<?php if($visitor_country =="Virgin Islands, British") { echo " selected"; } ?>>Virgin Islands, British</option>
              <option value="Virgin Islands, U.S."<?php if($visitor_country =="Virgin Islands, U.S.") { echo " selected"; } ?>>Virgin Islands, U.S.</option>
              <option value="Wallis and Futuna"<?php if($visitor_country =="Wallis and Futuna") { echo " selected"; } ?>>Wallis and Futuna</option>
              <option value="Western Sahara"<?php if($visitor_country =="Western Sahara") { echo " selected"; } ?>>Western Sahara</option>
              <option value="Yemen"<?php if($visitor_country =="Yemen") { echo " selected"; } ?>>Yemen</option>
              <option value="Zambia"<?php if($visitor_country =="Zambia") { echo " selected"; } ?>>Zambia</option>
              <option value="Zimbabwe"<?php if($visitor_country =="Zimbabwe") { echo " selected"; } ?>>Zimbabwe</option>
            </select>
            <?php if($error_country && isset($_POST['verzenden'])) { echo $error_country; } ?>
          </div>
        </div>
            <div class="form-group">
              <div class="col-sm-10">
                <button type="submit" class="btn btn-default" name="verzenden" style="background-color:#009edf; color: #fff;">Submit</button>
              </div>
            </div>
          </form>
          <?php } // einde $form_sent ?>
        </div>
      </div>
    </section>
  </div>
</div>

<div class="row" id="footer">
  <div class="container">
    <div class="col-sm-11 col-sm-offset-1" >
      <div id="footertext">
        <p>&copy; <?php echo date("Y"); ?> Information Mapping International | <a href="http://www.informationmapping.com/">www.informationmapping.com</a> | +32 9 253 14 25 | Contact our office in <a href="http://www.informationmapping.com/us/contact">US</a>, <a href="http://www.informationmapping.com/en/contact-3">Europe</a> or <a href="http://www.informationmapping.com/in/contact-2">Asia</a></p>
      </div>
    </div>
  </div>
</div>

<!-- jQuery (necessary for Bootstrap's JavaScript plugins) --> 
<script src="//code.jquery.com/jquery.js"></script> 
<!-- Include all compiled plugins (below), or include individual files as needed --> 
<script src="js/bootstrap.min.js"></script>
<?php
/* ADD STATISTICS */
// IP
$ip = $_SERVER['REMOTE_ADDR'];
// add referer
$url_ref = $_SERVER['HTTP_REFERER'];
$page = $_SERVER['REQUEST_URI'];
// only INSERT when IP is not IMI HQ office
if($ip != '213.119.25.89') {
$result_stat = mysql_query("INSERT INTO roadshow_referer (country, ip, referer, page, date) VALUES ('$visitor_country','$ip','$url_ref','$page',NOW())"); }
?>
<!-- Google Analytics -->
<script>
    (function(i,s,o,g,r,a,m){i['GoogleAnalyticsObject']=r;i[r]=i[r]||function(){
        (i[r].q=i[r].q||[]).push(arguments)},i[r].l=1*new Date();a=s.createElement(o),
        m=s.getElementsByTagName(o)[0];a.async=1;a.src=g;m.parentNode.insertBefore(a,m)
    })(window,document,'script','//www.google-analytics.com/analytics.js','ga');

    ga('create', 'UA-24028581-1', 'auto', {'allowLinker': true});
    // On every domain.
    ga('require', 'linker');
    // List of every domain to share linker parameters.
    ga('linker:autoLink', ['informationmapping.com', 'informationmapping-webinars.com']);
    ga('send', 'pageview'); // Send hits after initializing the auto-linker plug-in.
    ga('require', 'ecommerce');
</script>
<!-- End Google Analytics -->
</body>
</html>