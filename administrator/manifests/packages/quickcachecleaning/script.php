<?php
/**
 * @Copyright
 *
 * @package    QCC - Quick Cache Cleaning for Joomla! 3
 * @author     Viktor Vogel <admin@kubik-rubik.de>
 * @version    3-5 - 2015-02-22
 * @link       https://joomla-extensions.kubik-rubik.de/qcc-quick-cache-cleaning
 *
 * @license    GNU/GPL
 *  This program is free software: you can redistribute it and/or modify
 *  it under the terms of the GNU General Public License as published by
 *  the Free Software Foundation, either version 3 of the License, or
 *  (at your option) any later version.
 *
 *  This program is distributed in the hope that it will be useful,
 *  but WITHOUT ANY WARRANTY; without even the implied warranty of
 *  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *  GNU General Public License for more details.
 *
 *  You should have received a copy of the GNU General Public License
 *  along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */
defined('_JEXEC') or die('Restricted access');

class Pkg_QuickCacheCleaningInstallerScript
{
    function install($parent)
    {
        // Not needed at the moment
    }

    function uninstall($parent)
    {
        // Not needed at the moment
    }

    function update($parent)
    {
        // Not needed at the moment
    }

    function postflight($type, $parent)
    {
        // Not needed at the moment

        // Enable the content plugin
        // $db = JFactory::getDbo();
        // $db->setQuery("UPDATE ".$db->quoteName('#__extensions')." SET ".$db->quoteName('enabled')." = 1 WHERE ".$db->quoteName('element')." = 'quickcachecleaning' AND ".$db->quoteName('type')." = 'plugin'");
        // $db->execute();
    }
}