<?php
/*------------------------------------------------------------------------
com_wallfactory - Wall Factory
------------------------------------------------------------------------
author    TheFactory
copyright Copyright (C) 2010 SKEPSIS Consult SRL. All Rights Reserved.
@license - http://www.gnu.org/licenses/gpl-2.0.html GNU/GPL
Websites: http://www.thefactory.ro
Technical Support:  Forum - http://www.thefactory.ro/joomla-forum/
-------------------------------------------------------------------------*/

defined('_JEXEC') or die('Restricted access');

jimport('joomla.application.component.model');

class BackendModelReport extends JModel
{
  var $_report;

  function __construct()
  {
    parent::__construct();
  }

  function delete()
  {
    $cids   = JRequest::getVar('cid', array(0), 'post', 'array');
    $report =& $this->getTable('report');

    foreach($cids as $cid)
    {
      if (!$report->delete($cid))
      {
        $this->setError($report->getErrorMsg());
        return false;
      }
    }

    return true;
  }

  function store()
  {
    $id     = JRequest::getVar('id',     0, 'POST', 'integer');
    $status = JRequest::getVar('status', 0, 'POST', 'integer');

    $report =& $this->getTable('report');
    $report->load($id);

    if ($report->reporting_id == null)
    {
      return false;
    }

    $report->status = $status;
    $report->store();

    return true;
  }

  function resolve()
  {
    $id     =  JRequest::getVar('id', 0, 'GET', 'integer');
    $report =& $this->getTable('report');

    $report->load($id);

    if ($report->status != 1)
    {
      $report->status = 1;
      $report->store();

      return true;
    }

    return false;
  }

  function unresolve()
  {
    $id     =  JRequest::getVar('id', 0, 'GET', 'integer');
    $report =& $this->getTable('report');

    $report->load($id);

    if ($report->status == 1)
    {
      $report->status = 0;
      $report->store();

      return true;
    }

    return false;
  }

  function resolveList()
  {
    $cids = JRequest::getVar('cid', array(0), 'post', 'array');

    $query = ' UPDATE #__wallfactory_reports'
           . ' SET status = 1'
           . ' WHERE id IN (' . implode(', ', $cids) . ')';
    $this->_db->setQuery($query);

    return $this->_db->query();
  }

  function unresolveList()
  {
    $cids = JRequest::getVar('cid', array(0), 'post', 'array');

    $query = ' UPDATE #__wallfactory_reports'
           . ' SET status = 0'
           . ' WHERE id IN (' . implode(', ', $cids) . ')';
    $this->_db->setQuery($query);

    return $this->_db->query();
  }

  function getReport()
  {
    if (!isset($this->_report))
    {
      $id = JRequest::getVar('id', 0, 'GET', 'integer');

      //get reported from comments
      $query = ' SELECT r.*, u.username AS reporting_user, u2.username AS reported_user'
             . ' FROM #__wallfactory_reports r'
             . ' LEFT JOIN #__users u ON r.reporting_id = u.id'
             . ' LEFT JOIN #__users u2 ON r.user_id = u2.id'
             . ' WHERE r.id = ' . $id;
      $this->_db->setQuery($query);

      $this->_report = $this->_db->loadObject();
    }

    return $this->_report;
  }

  

  function actionDelete()
  {
    $id     =  JRequest::getVar('id', 0, 'GET', 'integer');
    $report =& $this->getTable('report');
    $report->load($id);

    switch ($report->type_id)
    {
      // Message
      case 1:
        $message =& $this->getTable('message');
        $message->load($report->reported_id);

        $message->delete();
      break;

      // Comment
      case 2:
        $comment =& $this->getTable('comment');
        $comment->load($report->reported_id);

        $comment->delete();
      break;

      // Photo Comment
      case 4:
        $comment =& $this->getTable('photocomment');
        $comment->load($report->reported_id);

        $comment->delete();
      break;

      // Video Comment
      case 5:
        $comment =& $this->getTable('VideoComment');
        $comment->load($report->reported_id);

        $comment->delete();
      break;
    }

    $report->action_delete = 1;
    $report->store();

    return true;
  }

  function actionBan()
  {
    $id     =  JRequest::getVar('id', 0, 'GET', 'integer');

    $wallowner =& $this->getTable('members');
    $wallowner->load($id);

    $wallowner->banned = 1;
    $wallowner->store();

    return true;
  }

}