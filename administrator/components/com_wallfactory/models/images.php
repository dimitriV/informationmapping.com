<?php
/*------------------------------------------------------------------------
com_wallfactory - Wall Factory
------------------------------------------------------------------------
author    TheFactory
copyright Copyright (C) 2010 SKEPSIS Consult SRL. All Rights Reserved.
@license - http://www.gnu.org/licenses/gpl-2.0.html GNU/GPL
Websites: http://www.thefactory.ro
Technical Support:  Forum - http://www.thefactory.ro/joomla-forum/
-------------------------------------------------------------------------*/

defined('_JEXEC') or die('Restricted access');

jimport('joomla.application.component.model');

class BackendModelImages extends JModel
{
  var $_data;
  var $_total;
  var $_pagination;

  function __construct()
  {
    parent::__construct();

    global $mainframe, $option;
    $limit      = $mainframe->getUserStateFromRequest('global.list.limit', 'limit', $mainframe->getCfg('list_limit'), 'int');
    $limitstart = $mainframe->getUserStateFromRequest($option.'.limitstart', 'limitstart', 0, 'int');

    $limitstart = ($limit != 0 ? (floor($limitstart / $limit) * $limit) : 0);

    $this->setState('limit', $limit);
    $this->setState('limitstart', $limitstart);
  
    $cids = JRequest::getVar('cid', 0, '', 'array');
    
    $this->setId((int)$cids[0]);
  }
 
  function setId($id)
  {
    $this->_id   = $id;
    $this->_data = null;
    $this->_total = null;
  }
  
  function getData()
  {
    if (empty($this->_data))
    {
      $query = $this->_buildQuery();
      $this->_data = $this->_getList($query, $this->getState('limitstart'), $this->getState('limit'));
    }

    return $this->_data;
  }

  function _buildQuery()
  {
    $orderby = $this->_buildContentOrderBy();
    $where   = $this->_buildContentWhere();
   
  	$query = ' SELECT m.*, u.username, w.title AS wall_title, w.id AS wall_id'
           . ' FROM #__wallfactory_media m'
           . ' LEFT JOIN #__wallfactory_walls w ON w.id = m.wall_id'
           . ' LEFT JOIN #__users u ON u.id = m.user_id'
           . $where
           . ' AND m.folder = "images" '
           . $orderby;
 
    return $query;
  }

  function _buildContentOrderBy()
  {
    global $mainframe, $option;

    $filter_order     = $mainframe->getUserStateFromRequest($option . '.images.filter_order',     'filter_order',     'title', 'cmd');
    $filter_order_Dir = $mainframe->getUserStateFromRequest($option . '.images.filter_order_Dir', 'filter_order_Dir', 'asc',   'word');

    $orderby = '';

    if (in_array($filter_order, array('m.title', 'm.name', 'm.date_added', 'u.username', 'wall_title')))
    {
      $orderby = ' ORDER BY ' . $filter_order . ' ' . $filter_order_Dir;
    }

    return $orderby;
  }

  function _buildContentWhere()
  {
    global $mainframe, $option;

    $search = $mainframe->getUserStateFromRequest($option . '.images.search',       'search',       '', 'cmd');
    $state  = $mainframe->getUserStateFromRequest($option . '.images.filter_state', 'filter_state', '', 'cmd');

    $where = ' WHERE 1';

    if ($search != '')
    {
      $where .= ' AND m.name LIKE "%' . $search . '%"';
    }
   
    $where .= ($this->_id != 0) ? (' AND m.user_id = '.$this->_id) : ('');   
     
    return $where;
  }

  function getTotal()
  {
    if (empty($this->_total))
    {
      $query = $this->_buildQuery();
      $this->_total = $this->_getListCount($query);
    }

    return $this->_total;
  }

  function getPagination()
  {
    if (empty($this->_pagination))
    {
      jimport('joomla.html.pagination');
      $this->_pagination = new JPagination($this->getTotal(), $this->getState('limitstart'), $this->getState('limit'));
    }

    return $this->_pagination;
  }
}