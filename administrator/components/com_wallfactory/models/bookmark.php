<?php
/*------------------------------------------------------------------------
com_wallfactory - Wall Factory
------------------------------------------------------------------------
author    TheFactory
copyright Copyright (C) 2010 SKEPSIS Consult SRL. All Rights Reserved.
@license - http://www.gnu.org/licenses/gpl-2.0.html GNU/GPL
Websites: http://www.thefactory.ro
Technical Support:  Forum - http://www.thefactory.ro/joomla-forum/
-------------------------------------------------------------------------*/

defined('_JEXEC') or die('Restricted access');

jimport('joomla.application.component.model');

class BackendModelBookmark extends JModel
{
  function __construct()
  {
    parent::__construct();

    $cids = JRequest::getVar('cid', 0, '', 'array');
    $this->setId((int)$cids[0]);
  }

  function setId($id)
  {
    $this->_id   = $id;
    $this->_data = null;
  }

  function &getData()
  {
    if (empty($this->_data))
    {
      $query = ' SELECT * FROM #__wallfactory_bookmarks'
             . ' WHERE id = '.$this->_id;
      $this->_db->setQuery($query);

      $this->_data = $this->_db->loadObject();
    }

    if (!$this->_data)
    {
      $this->_data = $this->getTable();
      $this->_data->published = 1;
    }

    return $this->_data;
  }

  function change()
  {
    $field = JRequest::getVar('field', '', 'GET', 'string');
    $value = JRequest::getVar('value',  0, 'GET', 'integer');
    $id    = JRequest::getVar('id',     0, 'GET', 'integer');

    if (!in_array($field, array('published')))
    {
      return false;
    }

    $query = ' UPDATE #__wallfactory_bookmarks'
           . ' SET ' . $field . ' = ' . $value
           . ' WHERE id = ' . $id;
    $this->_db->setQuery($query);

    return $this->_db->query();
  }

  function publish()
  {
    $cids = JRequest::getVar('cid', array(0), 'post', 'array');
    JRequest::setVar('field', 'published', 'GET', true);
    JRequest::setVar('value', 1, 'GET', true);

    foreach ($cids as $cid)
    {
      JRequest::setVar('id', $cid, 'GET', true);

      if (!$this->change())
      {
        $this->setError($field->getErrorMsg());
        return false;
      }
    }

    return true;
  }

  function unpublish()
  {
    $cids = JRequest::getVar('cid', array(0), 'post', 'array');
    JRequest::setVar('field', 'published', 'GET', true);
    JRequest::setVar('value', 0, 'GET', true);

    foreach ($cids as $cid)
    {
      JRequest::setVar('id', $cid, 'GET', true);

      if (!$this->change())
      {
        $this->setError($field->getErrorMsg());
        return false;
      }
    }

    return true;
  }

  function store()
  {
    $bookmark =& $this->getTable();
    $data =  JRequest::get('post');

    $delete = JRequest::getVar('delete', '', 'POST', 'string');
    if ($delete == 'on')
    {
      $data['extension'] = '';
    }

    $icon = JRequest::getVar('icon', array(), 'FILES', 'array');
    if (!$icon['error'])
    {
      jimport('joomla.filesystem.file');

      $extension = strtolower(JFile::getExt($icon['name']));
      $data['extension'] = $extension;
    }

    if (!$bookmark->bind($data))
    {
      $this->setError($this->_db->getErrorMsg());
      return false;
    }

    if (!$bookmark->check())
    {
      $this->setError($this->_db->getErrorMsg());
      return false;
    }

    if (!$bookmark->store())
    {
      $this->setError($this->_db->getErrorMsg());
      return false;
    }

    // Check for icon delete
    if ($delete == 'on')
    {
      $extension = JRequest::getVar('extension', '', 'POST', 'string');
      $current_icon = JPATH_COMPONENT_ADMINISTRATOR.DS.'storage'.DS.'bookmarks'.DS.$bookmark->id.'.'.$extension;

      if (file_exists($current_icon))
      {
        @unlink($current_icon);
      }
    }

    // Check for icon upload
    if (!$icon['error'])
    {
      require_once(JPATH_COMPONENT_ADMINISTRATOR.DS.'lib'.DS.'resize'.DS.'resize.class.php');

      $new_size = JRequest::getVar('resize_icon', 16, 'POST', 'intger');

      $filepath = JPath::clean(JPATH_COMPONENT_ADMINISTRATOR.DS.'storage'.DS.'bookmarks'.DS.$bookmark->id.'.'.$extension);
      $resize = new RESIZEIMAGE($icon['tmp_name']);
      $resize->resize_scale($new_size, $new_size, $filepath);
      $resize->close();
    }

    return true;
  }

  function delete()
  {
    $cids = JRequest::getVar('cid', array(0), 'post', 'array');
    $bookmark =& $this->getTable();

    foreach($cids as $cid)
    {
      $bookmark->load($cid);
      $extension = $bookmark->extension;

      if (!$bookmark->delete($cid))
      {
        $this->setError($field->getErrorMsg());
        return false;
      }

      if (!empty($extension))
      {
        $icon = JPATH_COMPONENT_ADMINISTRATOR.DS.'storage'.DS.'bookmarks'.DS.$cid.'.'.$extension;

        if (file_exists($icon))
        {
          @unlink($icon);
        }
      }
    }
    return true;
  }
};