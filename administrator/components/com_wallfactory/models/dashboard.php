<?php
/*------------------------------------------------------------------------
com_wallfactory - Wall Factory
------------------------------------------------------------------------
author    TheFactory
copyright Copyright (C) 2010 SKEPSIS Consult SRL. All Rights Reserved.
@license - http://www.gnu.org/licenses/gpl-2.0.html GNU/GPL
Websites: http://www.thefactory.ro
Technical Support:  Forum - http://www.thefactory.ro/joomla-forum/
-------------------------------------------------------------------------*/

defined('_JEXEC') or die('Restricted access');

jimport('joomla.application.component.model');

class BackendModelDashboard extends JModel
{
  function __construct()
  {
    parent::__construct();
    
    $cids = JRequest::getVar('cid', 0, '', 'array');
    $this->setId((int)$cids[0]);
  }
 
  function setId($id)
  {
    $this->_id   = $id;
    $this->_data = null;
  }
  
  function &getData()
  {
    if (empty($this->_data))
    {
    	$where = ($this->_id != 0) ? (' WHERE s.user_id = '.$this->_id) : ('');   
    
    	$query = ' SELECT w.id, w.user_id, w.title, w.alias, w.date_created, u.username, '
    	   . ' s.posts, s.urls, s.video_links, s.images, s.mp3, s.files'
           . ' FROM #__wallfactory_statistics s '
           . ' LEFT JOIN #__wallfactory_walls w ON w.id = s.wall_id'
           . ' LEFT JOIN #__users u ON u.id = w.user_id'
           . $where;
              
      $this->_db->setQuery($query);

      $this->_data = $this->_db->loadObjectList();
    }

    return $this->_data;
  }

  
  function getLatestMp3Files() 
  {
  	$where = ($this->_id != 0) ? (' AND m.user_id = '.$this->_id) : (''); 
  	
  	$query = ' SELECT m.*, u.username'
           . ' FROM #__wallfactory_media m'
           . ' LEFT JOIN #__users u ON u.id = m.user_id'
           . ' WHERE m.folder = "mp3" '
           . $where 
           . ' ORDER BY m.date_added DESC'
           . ' LIMIT 0, 5';

	$this->_db->setQuery($query);
    return $this->_db->loadObjectList();
    
  }
   
  function getLatestFiles() 
  {
	$where = ($this->_id != 0) ? (' AND m.user_id = '.$this->_id) : (''); 
  
  	$query = ' SELECT m.*, u.username'
           . ' FROM #__wallfactory_media m'
           . ' LEFT JOIN #__users u ON u.id = m.user_id'
           . ' WHERE m.folder = "files" '
           . $where
           . ' ORDER BY m.date_added DESC'
           . ' LIMIT 0, 5';

	$this->_db->setQuery($query);
    return $this->_db->loadObjectList();
								 
  }
  
  function getLatestUsers()
  {
  	$where = ($this->_id != 0) ? (' WHERE w.user_id = '.$this->_id) : (''); 
  	
  	$query = ' SELECT w.*, u.username'
           . ' FROM #__wallfactory_walls w'
           . ' LEFT JOIN #__users u ON u.id = w.user_id'
           . $where
           . ' ORDER BY w.date_created DESC'
           . ' LIMIT 0, 5';
      	
    $this->_db->setQuery($query);
    return $this->_db->loadObjectList();
  }
  
  function getLatestPosts()
  {
  	$where = ($this->_id != 0) ? (' WHERE p.user_id = '.$this->_id) : (''); 
  	
    $query = ' SELECT p.id, p.content, p.wall_id, p.date_created, w.title AS wall_title, w.user_id, w.date_created AS wall_created_at, u.username'
           . ' FROM #__wallfactory_posts p'
           . ' LEFT JOIN #__wallfactory_walls w ON w.id = p.wall_id'
           . ' LEFT JOIN #__users u ON u.id = w.user_id'
           . $where
           . ' ORDER BY p.date_created DESC'
           . ' LIMIT 0,8';
                       
    $this->_db->setQuery($query);
    return $this->_db->loadObjectList();
  }

  function getLatestImages()
  {
    $where = ($this->_id != 0) ? (' AND m.user_id = '.$this->_id) : ('');
  	$query = ' SELECT m.*, u.username'
           . ' FROM #__wallfactory_media m'
           . ' LEFT JOIN #__users u ON u.id = m.user_id'
           . ' WHERE m.folder = "images" '
           . $where
           . ' ORDER BY m.date_added DESC'
           . ' LIMIT 0, 3';
 
    $this->_db->setQuery($query);
    return $this->_db->loadObjectList();
  }

  
  function getLatestVideos()
  {
	$where = ($this->_id != 0) ? (' WHERE l.user_id = '.$this->_id) : (''); 
	
    $query = ' SELECT l.id, l.user_id, l.wall_id, l.video_link, l.video_title, l.date_added, u.username'
           . ' FROM #__wallfactory_urls l'
           . ' LEFT JOIN #__users u ON u.id = l.user_id'
           . $where
           . ' AND LENGTH(l.video_link) > 7 '
           . ' ORDER BY l.date_added DESC'
           . ' LIMIT 0, 5';
            
    $this->_db->setQuery($query);
    return $this->_db->loadObjectList();
  }

  function getLatestUrls()
  {
  	$where = ($this->_id != 0) ? (' WHERE l.user_id = '.$this->_id) : ('');
  	
    $query = ' SELECT l.id, l.user_id, l.wall_id, l.url, l.url_title, l.date_added, u.username'
           . ' FROM #__wallfactory_urls l'
           . ' LEFT JOIN #__users u ON u.id = l.user_id'
           . $where
           . ' AND LENGTH(l.url) > 7 '
           . ' ORDER BY l.date_added DESC'
           . ' LIMIT 0, 3';
    $this->_db->setQuery($query);
    return $this->_db->loadObjectList();
  }

 
  function getTotalPosts()
  {
  	$where = ($this->_id != 0) ? (' WHERE p.user_id = '.$this->_id) : ('');
    $query = ' SELECT COUNT(p.id)'
           . ' FROM #__wallfactory_posts p'
           . $where;
    $this->_db->setQuery($query);
    return $this->_db->loadResult();
  }

  function getNewPosts()
  {
    $timestamp = mktime(0, 0, 0, date('m'), date('d') - date('w'), date('Y'));
	$where = ($this->_id != 0) ? (' AND p.user_id = '.$this->_id) : ('');
	
    $query = ' SELECT COUNT(p.id)'
           . ' FROM #__wallfactory_posts p'
           . ' WHERE UNIX_TIMESTAMP(p.date_created) >= ' . $timestamp
           . $where;
    $this->_db->setQuery($query);
    return $this->_db->loadResult();
  }
  
  function getTotalVideos()
  {
  	$where = ($this->_id != 0) ? (' WHERE l.user_id = '.$this->_id) : ('');   
  
  	$query = ' SELECT COUNT(l.video_link)'
           . ' FROM #__wallfactory_urls l'
           . $where;
          
    $this->_db->setQuery($query);
    return $this->_db->loadResult();
  }
  
  function getNewVideos()
  {
    $timestamp = mktime(0, 0, 0, date('m'), date('d') - date('w'), date('Y'));
    $where = ($this->_id != 0) ? (' AND l.user_id = '.$this->_id) : ('');

    $query = ' SELECT COUNT(l.video_link)'
           . ' FROM #__wallfactory_urls l'
           . ' WHERE UNIX_TIMESTAMP(l.date_added) >= ' . $timestamp
           . $where;

    $this->_db->setQuery($query);
    return $this->_db->loadResult();
  }

  function getTotalUrls()
  {
  	$where = ($this->_id != 0) ? (' WHERE l.user_id = '.$this->_id) : ('');  
  
  	$query = ' SELECT COUNT(l.url)'
           . ' FROM #__wallfactory_urls l'
           . $where;
    $this->_db->setQuery($query);
    return $this->_db->loadResult();
  }
  
  function getNewUrls()
  {
    $timestamp = mktime(0, 0, 0, date('m'), date('d') - date('w'), date('Y'));
    $where = ($this->_id != 0) ? (' AND l.user_id = '.$this->_id) : ('');

    $query = ' SELECT COUNT(l.url)'
           . ' FROM #__wallfactory_urls l'
           . ' WHERE UNIX_TIMESTAMP(l.date_added) >= ' . $timestamp
           . $where;
    $this->_db->setQuery($query);
    return $this->_db->loadResult();
  }
  
  function getTotalImages()
  {
    $where = ($this->_id != 0) ? (' AND m.user_id = '.$this->_id) : ('');
    
  	$query = ' SELECT COUNT(m.id)'
           . ' FROM #__wallfactory_media m'
           . ' WHERE m.folder = "images" '
           . $where;
           
    $this->_db->setQuery($query);
    return $this->_db->loadResult();
  }
  
  function getNewImages()
  {
    $timestamp = mktime(0, 0, 0, date('m'), date('d') - date('w'), date('Y'));
	$where = ($this->_id != 0) ? (' AND m.user_id = '.$this->_id) : ('');
    
    $query = ' SELECT COUNT(m.id)'
           . ' FROM #__wallfactory_media m'
           . ' WHERE UNIX_TIMESTAMP(m.date_added) >= ' . $timestamp
           . ' AND m.folder = "images" '
           . $where;
   
    $this->_db->setQuery($query);
    return $this->_db->loadResult();
  }
  
  function getTotalMp3Files()
  {
    $where = ($this->_id != 0) ? (' AND m.user_id = '.$this->_id) : ('');
    
  	$query = ' SELECT COUNT(m.id)'
           . ' FROM #__wallfactory_media m'
           . ' WHERE m.folder = "mp3" '
           . $where;
   
    $this->_db->setQuery($query);
    return $this->_db->loadResult();
  }
  
  function getNewMp3Files()
  {
    $timestamp = mktime(0, 0, 0, date('m'), date('d') - date('w'), date('Y'));
    $where = ($this->_id != 0) ? (' AND m.user_id = '.$this->_id) : ('');

    $query = ' SELECT COUNT(m.id)'
           . ' FROM #__wallfactory_media m'
           . ' WHERE UNIX_TIMESTAMP(m.date_added) >= ' . $timestamp
           . ' AND m.folder = "mp3" '
		   . $where;
             
    $this->_db->setQuery($query);
    return $this->_db->loadResult();
  }
  
  function getTotalFiles()
  {
    $where = ($this->_id != 0) ? (' AND m.user_id = '.$this->_id) : ('');
    
  	$query = ' SELECT COUNT(m.id)'
           . ' FROM #__wallfactory_media m'
           . ' WHERE m.folder = "files" '
		   . $where;

    $this->_db->setQuery($query);
    return $this->_db->loadResult();
  }
  
  function getNewFiles()
  {
    $timestamp = mktime(0, 0, 0, date('m'), date('d') - date('w'), date('Y'));
    $where = ($this->_id != 0) ? (' AND m.user_id = '.$this->_id) : ('');

    $query = ' SELECT COUNT(m.id)'
           . ' FROM #__wallfactory_media m'
           . ' WHERE UNIX_TIMESTAMP(m.date_added) >= ' . $timestamp
           . ' AND m.folder = "files" '
           . $where;
           
    $this->_db->setQuery($query);
    return $this->_db->loadResult();
  }
  
  function getTotalPostReports()
  {
    
  	$where = ($this->_id != 0) ? (' AND p.user_id = '.$this->_id) : ('');
    
  	$query = ' SELECT COUNT(p.reported)'
           . ' FROM #__wallfactory_posts p'
           . ' WHERE p.reported = 1'
           . $where;
           
    $this->_db->setQuery($query);
    return $this->_db->loadResult();
  }
  
  function getTotalCommentReports()
  {
    $where = ($this->_id != 0) ? (' AND c.user_id = '.$this->_id) : ('');
    
  	$query = ' SELECT COUNT(c.reported)'
           . ' FROM #__wallfactory_comments c'
           . ' WHERE c.reported = 1'
           . $where;
          
    $this->_db->setQuery($query);
    return $this->_db->loadResult();
  }
  
  function getTotalUsers()
  {
    $where = ($this->_id != 0) ? (' WHERE w.user_id = '.$this->_id) : ('');
    
  	$query = ' SELECT COUNT(w.id)'
           . ' FROM #__wallfactory_walls w'
           . $where;

    $this->_db->setQuery($query);
    return $this->_db->loadResult();
  }
  
  function getTotalComments()
  {
    $where = ($this->_id != 0) ? (' WHERE c.user_id = '.$this->_id) : ('');
    
  	$query = ' SELECT COUNT(c.id)'
           . ' FROM #__wallfactory_comments c'
           . $where;

    $this->_db->setQuery($query);
    return $this->_db->loadResult();
  }

  function getNewUsers()
  {
    $timestamp = mktime(0, 0, 0, date('m'), date('d') - date('w'), date('Y'));
    $where = ($this->_id != 0) ? (' AND w.user_id = '.$this->_id) : ('');

    $query = ' SELECT COUNT(w.id)'
           . ' FROM #__wallfactory_walls w'
           . ' WHERE UNIX_TIMESTAMP(w.date_created) >= ' . $timestamp
           . $where;
 
    $this->_db->setQuery($query);
    return $this->_db->loadResult();
  }
  
  function getNewPostReports()
  {
    $timestamp = mktime(0, 0, 0, date('m'), date('d') - date('w'), date('Y'));
    $where = ($this->_id != 0) ? (' AND p.user_id = '.$this->_id) : ('');

    $query = ' SELECT COUNT(p.reported)'
           . ' FROM #__wallfactory_posts p'
           . ' WHERE UNIX_TIMESTAMP(p.date_created) >= ' . $timestamp
           . $where
           . ' AND p.reported = 1';

    $this->_db->setQuery($query);
    return $this->_db->loadResult();
  }
  
  function getNewCommentReports()
  {
    $timestamp = mktime(0, 0, 0, date('m'), date('d') - date('w'), date('Y'));
    $where = ($this->_id != 0) ? (' AND c.user_id = '.$this->_id) : ('');

    $query = ' SELECT COUNT(c.reported)'
           . ' FROM #__wallfactory_comments c'
           . ' WHERE UNIX_TIMESTAMP(c.date_created) >= ' . $timestamp
           . $where
           . ' AND c.reported = 1';
 
	$this->_db->setQuery($query);
    return $this->_db->loadResult();
  }
  
  function getNewComments()
  {
    $timestamp = mktime(0, 0, 0, date('m'), date('d') - date('w'), date('Y'));
    $where = ($this->_id != 0) ? (' AND c.user_id = '.$this->_id) : ('');

    $query = ' SELECT COUNT(c.id)'
           . ' FROM #__wallfactory_comments c'
           . ' WHERE UNIX_TIMESTAMP(c.date_created) >= ' . $timestamp
           . $where;

    $this->_db->setQuery($query);
    return $this->_db->loadResult();
  }

  function getTotalFollowers()
  {
    $where = ($this->_id != 0) ? (' WHERE f.user_id = '.$this->_id) : ('');
    
  	$query = ' SELECT COUNT(DISTINCT f.user_id)'
           . ' FROM #__wallfactory_followers f'
           . ' LEFT JOIN #__wallfactory_walls w ON w.user_id = f.user_id '
           . $where;

    $this->_db->setQuery($query);
    return $this->_db->loadResult();
  }

  function getTotalFollowedWalls()
  {
    $where = ($this->_id != 0) ? (' WHERE f.wall_id = '.$this->_id) : ('');
     
  	$query = ' SELECT COUNT(DISTINCT f.wall_id)'
           . ' FROM #__wallfactory_followers f'
           . $where;
    $this->_db->setQuery($query);
    return $this->_db->loadResult();
  }

  function getLatestComments()
  {
    $where = ($this->_id != 0) ? (' WHERE c.user_id = '.$this->_id) : ('');
    
  	$query = ' SELECT c.*'
           . ' FROM #__wallfactory_comments c'
           . $where
           . ' ORDER BY c.date_created DESC'
           . ' LIMIT 0, 8';

    $this->_db->setQuery($query);

    return $this->_db->loadObjectList();
  }
}