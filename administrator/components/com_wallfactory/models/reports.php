<?php
/*------------------------------------------------------------------------
com_wallfactory - Wall Factory
------------------------------------------------------------------------
author    TheFactory
copyright Copyright (C) 2010 SKEPSIS Consult SRL. All Rights Reserved.
@license - http://www.gnu.org/licenses/gpl-2.0.html GNU/GPL
Websites: http://www.thefactory.ro
Technical Support:  Forum - http://www.thefactory.ro/joomla-forum/
-------------------------------------------------------------------------*/

defined('_JEXEC') or die('Restricted access');

jimport('joomla.application.component.model');

class BackendModelReports extends JModel
{
  var $_total_images;
  var $_total_comments;
  var $_limitstart;
  var $_items_per_page;

  function __construct()
  {
    $this->_items_per_page = 10;
    $this->_limitstart =  JRequest::getVar('limitstart', 0, 'GET', 'integer');

    parent::__construct();
  }

  function getCountReports()
  {
    return array(
      'comments' => $this->getCountReportsComments(),
      'posts'   => $this->getCountReportsPosts(),
    );
  }

  function getCountReportsPosts()
  {
    if (isset($this->_total_posts))
    {
      return $this->_total_posts;
    }

    $database =& JFactory::getDBO();

    $query = ' SELECT COUNT(id)'
    		. ' FROM #__wallfactory_posts'
    		. ' WHERE reported = 1';
    $database->setQuery($query);
    $posts = $database->loadResult();

    return $posts;
  }

  function getCountReportsComments()
  {
    if (isset($this->_total_comments))
    {
      return $this->_total_comments;
    }

    $database =& JFactory::getDBO();
	$query = ' SELECT COUNT(id)'
    		. ' FROM #__wallfactory_comments'
    		. ' WHERE reported = 1';
    		
    $database->setQuery($query);
    $comments = $database->loadResult();

    return $comments;
  }

  function getReportsPosts()
  {
    $database =& JFactory::getDBO();

    $query = ' SELECT p.id, p.user_id, p.alias, p.content, p.date_created, IF(p.user_id <> 0, u.username,"' . JText::_('Guest') . '") AS username'
              . ' FROM #__wallfactory_posts p'
              . ' LEFT JOIN #__users u ON u.id = p.user_id '
              . ' WHERE p.reported = 1'
              . ' ORDER BY p.date_created DESC';

    $posts = $this->_getList($query, $this->_limitstart, $this->_items_per_page);

    return $posts;
  }

  function getReportsComments()
  {
    $database =& JFactory::getDBO();

    $query = ' SELECT c.id, c.content, IF(c.author_alias <> "", author_alias,"' . JText::_('Guest') . '") AS author_alias '
           . ' FROM #__wallfactory_comments c'
           . ' WHERE c.reported = 1'
           . ' ORDER BY c.date_created DESC';

    $comments = $this->_getList($query, $this->_limitstart, $this->_items_per_page);

    return $comments;
  }

  function getPaginationPosts()
  {
    jimport('joomla.html.pagination');

    $pagination = new JPagination($this->getCountReportsPosts(), $this->_limitstart, $this->_items_per_page);
    return $pagination;
  }

  function getPaginationComments()
  {
    jimport('joomla.html.pagination');

    $pagination = new JPagination($this->getCountReportsComments(), $this->_limitstart, $this->_items_per_page);
    return $pagination;
  }
}