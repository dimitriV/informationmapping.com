<?php
/*------------------------------------------------------------------------
com_wallfactory - Wall Factory
------------------------------------------------------------------------
author    TheFactory
copyright Copyright (C) 2010 SKEPSIS Consult SRL. All Rights Reserved.
@license - http://www.gnu.org/licenses/gpl-2.0.html GNU/GPL
Websites: http://www.thefactory.ro
Technical Support:  Forum - http://www.thefactory.ro/joomla-forum/
-------------------------------------------------------------------------*/

defined('_JEXEC') or die('Restricted access');

jimport('joomla.application.component.model');

class BackendModelSettings extends JModel
{
  function __construct()
  {
    parent::__construct();

    $this->settings = array(
      'allowed_wallowners_groups' 					=> 'array',
      'allowed_wallowners_users'  					=> 'array',
      'allowed_wallowners_all'    					=> 'integer',
      'enable_guest_view'       					=> 'integer',
      'enable_guest_write'      					=> 'integer',
      'allow_guest_comments'    					=> 'integer',
      'captcha_comment'         					=> 'integer',
      'enable_bookmarks'        					=> 'integer',
   	  'wall_title_display'      					=> 'integer',
      'banned_words'           		 				=> 'banned_words',
      'enable_avatars'          					=> 'integer',
      'avatar_max_width'        					=> 'integer',
      'avatar_max_height'       					=> 'integer',
      'image_max_width'         					=> 'integer',
      'image_max_height'        					=> 'integer',
      'comments_per_page'       					=> 'integer',
      'recaptcha_public_key'    					=> 'string',
      'recaptcha_private_key'   					=> 'string',
      'enable_notification_email_html' 				=> 'integer',
      'enable_gravatar'                				=> 'integer',
      'allow_cb_avatars'        					=> 'integer',
      'enable_notification_new_post'  				=> 'integer',
      'notification_new_post_subject' 				=> 'string',
      'notification_new_post_message' 				=> 'string',
      'enable_notification_new_comment'  			=> 'integer',
      'notification_new_comment_subject' 			=> 'string',
      'notification_new_comment_message' 			=> 'string',
      'enable_notification_new_report_comment'  	=> 'integer',
      'notification_new_report_comment_subject' 	=> 'string',
      'notification_new_report_comment_message' 	=> 'string',
      'enable_notification_new_report_post'  		=> 'integer',
      'notification_new_report_post_subject' 		=> 'string',
      'notification_new_report_post_message' 		=> 'string',
      'enable_notification_follow_wall'  			=> 'integer',
      'notification_follow_wall_subject' 			=> 'string',
      'notification_follow_wall_message' 			=> 'string',
      'enable_notify_new_post_followers' 			=> 'integer',
      'enable_notify_new_post_admin' 				=> 'integer',
      'enable_notify_new_comment_followers' 		=> 'integer',
      'enable_notify_new_comment_admin' 			=> 'integer',
      'notification_new_post_receivers'           	=> 'array',
      'notification_new_comment_receivers'        	=> 'array',
      'allow_post_images'							=> 'integer',
      'allow_post_videos'							=> 'integer',
      'allow_post_mp3files'							=> 'integer',
      'allow_post_links'							=> 'integer',
	  'allow_post_files'							=> 'integer',
	  'allow_comments'								=> 'integer',
    );

    $this->wallSettings = new wallSettings();
  
  }

  function store()
  {
    $this->updateRouterPlugin();
    $this->updateSettings();

    return $this->writeSettings();
  }

  function updateSettings()
  {
  	foreach ($this->settings as $setting => $type)
    {
      
      switch ($type)
      {
        case 'string':
          $this->wallSettings->$setting = JRequest::getVar($setting, $this->wallSettings->$setting, 'POST', $type);
          $this->wallSettings->$setting = base64_encode($this->wallSettings->$setting);
        break;

        case 'array':
          $this->wallSettings->$setting = JRequest::getVar($setting, array(), 'POST', $type);
        break;

        case 'integer':
          $this->wallSettings->$setting = JRequest::getVar($setting, $this->wallSettings->$setting, 'POST', $type);
        break;
 
        case 'banned_words':
          $words = JRequest::getVar($setting, '', 'POST', 'string');
          $words = explode("\n", $words);

          $this->wallSettings->$setting = array();

          $array = array();
          foreach ($words as $word)
          {
            if ('' != $word)
            {
              $array[] = base64_encode(trim($word));
            }
          }
          $this->wallSettings->$setting = $array;
        break;
      }
    }
  }

  function writeSettings()
  {
    // Check if file writable
    if (!is_writable(JPATH_COMPONENT_ADMINISTRATOR.DS.'wallSettings.class.php'))
    {
      $this->setError(JText::_('Settings file is not writable!'));
      return false;
    }

    $handle = fopen(JPATH_COMPONENT_ADMINISTRATOR.DS.'wallSettings.class.php', 'w');
    fwrite($handle, $this->templateSettings());
    fclose($handle);

    return true;
  }

  function templateSettings()
  {
    $max_length = 0;
    foreach ($this->settings as $setting => $type)
    {
      $max_length = (strlen($setting) > $max_length) ? strlen($setting) : $max_length;
    }

    $template = '<?php

defined(\'_JEXEC\') or die(\'Restricted access\');

class wallSettings
{';

    foreach ($this->settings as $setting => $type)
    {
      $template .= "\n";
      $template .=
'  var $' . $setting . str_repeat(' ', $max_length - strlen($setting)) . ' = ';

      switch ($type)
      {
        case 'array':
        case 'banned_words':
          $template .= 'array(';
          if (count($this->wallSettings->$setting))
          {
            $template .= '\'' . implode('\', \'', $this->wallSettings->$setting) . '\'';
          }
          $template .= ')';
        break;

        case 'integer':
          $template .= $this->wallSettings->$setting;
        break;

        case 'string':
          $template .= '\'' . $this->wallSettings->$setting . '\'';
        break;
      }

      $template .= ';';
    }

    $template .= "\n";
    $template .=
'}';

    return $template;
  }

  function getAdmins()
  {
    $query = ' SELECT u.id, u.username'
           . ' FROM #__users u'
           . ' WHERE u.gid IN (24, 25)';
    $this->_db->setQuery($query);
    return $this->_db->loadObjectList();
  }

  function restoreBackup()
  {
    $wallSettingsBackup = new wallSettingsBackup();

    foreach ($this->settings as $setting => $type)
    {
      if (!isset($wallSettingsBackup->$setting))
      {
        continue;
      }

      if ('user_group_folder_quote' == $setting)
      {
        $array = array();

        foreach ($wallSettingsBackup->$setting as $key => $value)
        {
          $array[] = $key . ' => ' . $value;
        }

        $this->wallSettings->$setting = $array;
      }
      else
      {
        $this->wallSettings->$setting = $wallSettingsBackup->$setting;
      }
    }

    return $this->writeSettings();
  }

  function getRouterPlugin()
  {
    $query = ' SELECT p.published'
           . ' FROM #__plugins p'
           . ' WHERE element = "wallrouter"';
    $this->_db->setQuery($query);

    return $this->_db->loadResult();
  }

  function updateRouterPlugin()
  {
    $published =& JRequest::getVar('enable_router_plugin', 0, 'POST', 'integer');

    $query = ' UPDATE #__plugins'
           . ' SET published = ' . $published
           . ' WHERE element = "wallrouter"';
    $this->_db->setQuery($query);

    return $this->_db->loadResult();
  }
}