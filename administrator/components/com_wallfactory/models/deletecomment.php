<?php
/*------------------------------------------------------------------------
com_wallfactory - Wall Factory
------------------------------------------------------------------------
author    TheFactory
copyright Copyright (C) 2010 SKEPSIS Consult SRL. All Rights Reserved.
@license - http://www.gnu.org/licenses/gpl-2.0.html GNU/GPL
Websites: http://www.thefactory.ro
Technical Support:  Forum - http://www.thefactory.ro/joomla-forum/
-------------------------------------------------------------------------*/

defined('_JEXEC') or die('Restricted access');

jimport('joomla.application.component.model');

class BackendModelDeletecomment extends JModel
{
  function __construct()
  {
    parent::__construct();
  }

  function getDelete()
  {
    $type = JRequest::getVar('type', 0, 'GET', 'integer');

    if (!in_array($type, array(1,2)))
    {
      return false;
    }

    $id = JRequest::getVar('id', 0, 'GET', 'integer');

	if ($type == 2)
      $query = ' DELETE FROM #__wallfactory_comments '
             . ' WHERE id = ' . $id;

    $database =& JFactory::getDBO();
    $database->setQuery($query);

    return $database->query();
  }
}