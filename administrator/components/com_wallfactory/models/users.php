<?php
/*------------------------------------------------------------------------
com_wallfactory - Wall Factory
------------------------------------------------------------------------
author    TheFactory
copyright Copyright (C) 2010 SKEPSIS Consult SRL. All Rights Reserved.
@license - http://www.gnu.org/licenses/gpl-2.0.html GNU/GPL
Websites: http://www.thefactory.ro
Technical Support:  Forum - http://www.thefactory.ro/joomla-forum/
-------------------------------------------------------------------------*/

defined('_JEXEC') or die('Restricted access');

jimport('joomla.application.component.model');

class BackendModelUsers extends JModel
{
  var $_data;
  var $_data_guest;
  var $_total;
  var $_pagination;

  function __construct()
  {
    parent::__construct();

    global $mainframe, $option;
    $limit      = $mainframe->getUserStateFromRequest('global.list.limit', 'limit', $mainframe->getCfg('list_limit'), 'int');
    $limitstart = $mainframe->getUserStateFromRequest($option.'.limitstart', 'limitstart', 0, 'int');

    $limitstart = ($limit != 0 ? (floor($limitstart / $limit) * $limit) : 0);

    $this->setState('limit', $limit);
    $this->setState('limitstart', $limitstart);
  }

  function getData()
  {
    if (empty($this->_data))
    {
      $query = $this->_buildQuery();
      $this->_data = $this->_getList($query, $this->getState('limitstart'), $this->getState('limit'));
    }

    return $this->_data;
  }
  
  function getDataGuest()
  {
    if (empty($this->_data_guest))
    {
      $query = $this->_buildQueryGuest();
      $this->_data_guest = $this->_getList($query, $this->getState('limitstart'), $this->getState('limit'));
    }

    return $this->_data_guest;
  }

  function _buildQuery()
  {
    $orderby = $this->_buildContentOrderBy();
    $where   = $this->_buildContentWhere();
    
    $query = ' SELECT m.*, u.username, w.id AS wall_id '
   		   . ' , w.alias AS wall_title, w.date_created, w.published, '
   		   . ' m.name, s.posts, s.urls, s.video_links, s.images, s.mp3, s.files'
   		   . ' FROM #__wallfactory_members m'
           . ' LEFT JOIN #__users u ON u.id = m.user_id'
           . ' LEFT JOIN #__wallfactory_walls w ON w.user_id = m.user_id'
           . ' LEFT JOIN #__wallfactory_statistics s ON w.id = s.wall_id '
           . $where
           . $orderby;

    return $query;
  }
  
  function _buildQueryGuest()
  {
    $query = ' SELECT  "' . JText::_('Guest') . '" AS username, '
    	   . ' s.posts, s.urls, s.video_links, s.images, s.mp3, s.files'
   		   . ' FROM #__wallfactory_statistics s  '
           . ' WHERE s.user_id = 0 ';
             
    return $query;
  }

  function _buildContentOrderBy()
  {
    global $mainframe, $option;

    $filter_order     = $mainframe->getUserStateFromRequest($option . '.users.filter_order',     'filter_order',     'title', 'cmd');
    $filter_order_Dir = $mainframe->getUserStateFromRequest($option . '.users.filter_order_Dir', 'filter_order_Dir', 'asc',   'word');

    $orderby = '';
    
    if (in_array($filter_order, array('u.username', 'm.name', 'wall_title', 'w.date_created', 's.posts', 's.urls', 's.video_links', 's.images', 's.mp3', 's.files')))
    {
      $orderby = ' ORDER BY ' . $filter_order . ' ' . $filter_order_Dir;
    }

    return $orderby;
  }

  function _buildContentWhere()
  {
    global $mainframe, $option;

    $search     = $mainframe->getUserStateFromRequest($option . '.users.search',     'search',     '', 'cmd');
   // $wall_owner = $mainframe->getUserStateFromRequest($option . '.users.wall_owner', 'wall_owner', '', 'string');
    $state  = $mainframe->getUserStateFromRequest($option . '.users.filter_state', 'filter_state', '', 'cmd');

    $where = ' WHERE 1';

    if ($search != '')
    {
      $temp   = array();
      $temp[] = ' (u.username LIKE "%' . $search . '%")';
      $temp[] = ' (s.name LIKE "%' . $search . '%")';
      $temp[] = ' (w.title LIKE "%' . $search . '%")';

      $where .= ' AND ' . '(' . implode(' OR ', $temp) . ')';
    }

   /* if ($wall_owner != '')
    {
      if ($wall_owner == '0')
      {
        $where .= ' AND w.id IS NULL';
      }
      else
      {
        $where .= ' AND w.id IS NOT NULL';
      }
    }
    */
    if ($state != '')
    {
      $state  = ($state == 'P') ? 1 : 0;
    }

    return $where;
  }

  function getTotal()
  {
    if (empty($this->_total))
    {
      $query = $this->_buildQuery();
      $this->_total = $this->_getListCount($query);
    }

    return $this->_total;
  }

  function getPagination()
  {
    if (empty($this->_pagination))
    {
      jimport('joomla.html.pagination');
      $this->_pagination = new JPagination($this->getTotal(), $this->getState('limitstart'), $this->getState('limit'));
    }

    return $this->_pagination;
  }
}