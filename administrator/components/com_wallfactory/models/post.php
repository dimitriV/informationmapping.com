<?php
/*------------------------------------------------------------------------
com_wallfactory - Wall Factory
------------------------------------------------------------------------
author    TheFactory
copyright Copyright (C) 2010 SKEPSIS Consult SRL. All Rights Reserved.
@license - http://www.gnu.org/licenses/gpl-2.0.html GNU/GPL
Websites: http://www.thefactory.ro
Technical Support:  Forum - http://www.thefactory.ro/joomla-forum/
-------------------------------------------------------------------------*/

defined('_JEXEC') or die('Restricted access');

jimport('joomla.application.component.model');

class BackendModelPost extends JModel
{
  function __construct()
  {
    parent::__construct();

    $cids = JRequest::getVar('cid', 0, '', 'array');
    $this->setId((int)$cids[0]);
  }

  function setId($id)
  {
    $this->_id   = $id;
    $this->_data = null;
  }

  function &getData()
  {
    if (empty($this->_data))
    {
      $query = ' SELECT p.*, u.username'
      . ' FROM #__wallfactory_posts p'
      . ' LEFT JOIN #__wallfactory_walls w ON w.id = p.wall_id'
      . ' LEFT JOIN #__users u ON u.id = w.user_id'
      . ' WHERE p.id = ' . $this->_id;
      $this->_db->setQuery($query);

      $this->_data = $this->_db->loadObject();
    }

    if (!$this->_data)
    {
      $this->_data = $this->getTable();
    }

    return $this->_data;
  }

  function getWalls()
  {
    $query = ' SELECT w.id, w.title'
    . ' FROM #__wallfactory_walls w'
    . ' ORDER BY w.date_created ASC';

    $this->_db->setQuery($query);
    return $this->_db->loadObjectList();
  }

  function change()
  {
    $field = JRequest::getVar('field', '', 'GET', 'string');
    $value = JRequest::getVar('value',  0, 'GET', 'integer');
    $id    = JRequest::getVar('id',     0, 'GET', 'integer');

    if (!in_array($field, array('published', 'archived')))
    {
      return false;
    }

    $query = ' UPDATE #__wallfactory_posts'
    . ' SET ' . $field . ' = ' . $value
    . ' WHERE id = ' . $id;
    $this->_db->setQuery($query);

    return $this->_db->query();
  }
/*
  function publish()
  {
    $cids = JRequest::getVar('cid', array(0), 'post', 'array');
    JRequest::setVar('field', 'published', 'GET', true);
    JRequest::setVar('value', 1, 'GET', true);

    foreach ($cids as $cid)
    {
      JRequest::setVar('id', $cid, 'GET', true);

      if (!$this->change())
      {
        $this->setError($field->getErrorMsg());
        return false;
      }
    }

    return true;
  }

  function unpublish()
  {
    $cids = JRequest::getVar('cid', array(0), 'post', 'array');
    JRequest::setVar('field', 'published', 'GET', true);
    JRequest::setVar('value', 0, 'GET', true);

    foreach ($cids as $cid)
    {
      JRequest::setVar('id', $cid, 'GET', true);

      if (!$this->change())
      {
        $this->setError($field->getErrorMsg());
        return false;
      }
    }

    return true;
  }
*/
  function archive()
  {
    $cids = JRequest::getVar('cid', array(0), 'post', 'array');
    JRequest::setVar('field', 'archived', 'GET', true);
    JRequest::setVar('value', 1, 'GET', true);

    foreach ($cids as $cid)
    {
      JRequest::setVar('id', $cid, 'GET', true);

      if (!$this->change())
      {
        $this->setError($field->getErrorMsg());
        return false;
      }
    }

    return true;
  }

  function unarchive()
  {
    $cids = JRequest::getVar('cid', array(0), 'post', 'array');
    JRequest::setVar('field', 'archived', 'GET', true);
    JRequest::setVar('value', 0, 'GET', true);

    foreach ($cids as $cid)
    {
      JRequest::setVar('id', $cid, 'GET', true);

      if (!$this->change())
      {
        $this->setError($field->getErrorMsg());
        return false;
      }
    }

    return true;
  }

  function store()
  {
    $post =& $this->getTable();
    $data =  JRequest::get('post');

    $data['content'] = JRequest::getVar('content', '', 'post', 'string', JREQUEST_ALLOWRAW);

    if (!$post->bind($data))
    {
      $this->setError($this->_db->getErrorMsg());
      return false;
    }

    if (!$post->check())
    {
      $this->setError($this->_db->getErrorMsg());
      return false;
    }

    if (empty($post->id))
    {
      $post->date_created = date('Y-m-d H:i:s');
    }
 
    $post->date_updated = date('Y-m-d H:i:s');

    if (!$post->store())
    {
      $this->setError($this->_db->getErrorMsg());
      return false;
    }

    return true;
  }

  function delete()
  {
    $cids = JRequest::getVar('cid', array(0), 'post', 'array');
    $post =& $this->getTable();

    foreach($cids as $cid)
    {
      if (!$post->delete($cid))
      {
        $this->setError($post->getErrorMsg());
        return false;
      }
    }
    return true;
  }

}