<?php
/*------------------------------------------------------------------------
com_wallfactory - Wall Factory
------------------------------------------------------------------------
author    TheFactory
copyright Copyright (C) 2010 SKEPSIS Consult SRL. All Rights Reserved.
@license - http://www.gnu.org/licenses/gpl-2.0.html GNU/GPL
Websites: http://www.thefactory.ro
Technical Support:  Forum - http://www.thefactory.ro/joomla-forum/
-------------------------------------------------------------------------*/

defined('_JEXEC') or die('Restricted access');

jimport('joomla.application.component.model');

class BackendModelUser extends JModel
{
  function __construct()
  {
    parent::__construct();

    $cids = JRequest::getVar('cid', 0, '', 'array');
    $this->setId((int)$cids[0]);
  }

  function setId($id)
  {
    $this->_id   = $id;
    $this->_data = null;
  }

  function &getData()
  {
    if (empty($this->_data))
    {
      $query = $this->_buildQuery();
      $this->_db->setQuery($query);

      $this->_data = $this->_db->loadObject();
    }

    if (!$this->_data)
    {
      $this->_data = $this->getTable('members');
    }

    return $this->_data;
  }

  function _buildQuery()
  {
    $query = ' SELECT s.*, u.username'
           . ' FROM #__wallfactory_members s'
           . ' LEFT JOIN #__users u ON u.id = s.user_id'
           . ' WHERE s.user_id = '.$this->_id;

    return $query;
  }

  // Tasks
  function store()
  {
    $user =& $this->getTable('members');
    $data =  JRequest::get('post');
    
    if (!$user->bind($data))
    {
      $this->setError($this->_db->getErrorMsg());
      return false;
    }

    if (!$user->check())
    {
      $this->setError($this->_db->getErrorMsg());
      return false;
    }

    if (!$user->store())
    {
      $this->setError($this->_db->getErrorMsg());
      return false;
    }

    return true;
  }
  
  function ban()
  {
    $id = JRequest::getVar('id', 0, 'get', 'integer');
    $wallmembers =& $this->getTable('members');
	$wallmembers->findOneByUserId($id);
	
    if ($wallmembers->banned != null)
    {
      $wallmembers->banned = 1;
      $wallmembers->store();

      return true;
    }

    return false;
  }

  function banList()
  {
    $cids = JRequest::getVar('cid', array(0), 'post', 'array');
	
    $query = ' UPDATE #__wallfactory_members'
           . ' SET banned = 1'
           . ' WHERE user_id IN (' . implode(', ', $cids) . ')';
    $this->_db->setQuery($query);

    return $this->_db->query();
  }

  function unban()
  {
    $id = JRequest::getVar('id', 0, 'get', 'integer');
    $wallmembers =& $this->getTable('members');
	$wallmembers->findOneByUserId($id);

    if ($wallmembers->banned != null)
    {
      $wallmembers->banned = 0;
      $wallmembers->store();

      return true;
    }

    return false;
  }

  function unbanList()
  {
    $cids = JRequest::getVar('cid', array(0), 'post', 'array');
	
    $query = ' UPDATE #__wallfactory_members'
           . ' SET banned = 0'
           . ' WHERE user_id IN (' . implode(', ', $cids) . ')';
    $this->_db->setQuery($query);

    return $this->_db->query();
  }
  
  function delete()
  {
    $cids    =  JRequest::getVar('cid', array(0), 'post', 'array');
    $wallmembers =& $this->getTable('members', 'Table');
    
    foreach ($cids as $cid)
    {
      $wallmembers->findOneByUserId($cid);
      if (!$wallmembers->delete($cid))
      {
        $this->setError($this->_db->getErrorMsg());
        return false;
      }
      
     $users_folder = JPATH_COMPONENT_SITE.DS.'storage'.DS.'users'; 
     $user_folder  = $users_folder.DS.$cid;       

	 $this->deleteDirectory($user_folder);
 
    }

    return true;
  }
  
  function deleteDirectory($dir)
	{
	  if (!file_exists($dir)) return true;
	  if (!is_dir($dir)) return unlink($dir);
	  foreach (scandir($dir) as $item) {
	    if ($item == '.' || $item == '..') continue;
	    if (!$this->deleteDirectory($dir.DS.$item)) return false;
	  }
	  return rmdir($dir);
  }
}