<?php
/*------------------------------------------------------------------------
com_wallfactory - Wall Factory
------------------------------------------------------------------------
author    TheFactory
copyright Copyright (C) 2010 SKEPSIS Consult SRL. All Rights Reserved.
@license - http://www.gnu.org/licenses/gpl-2.0.html GNU/GPL
Websites: http://www.thefactory.ro
Technical Support:  Forum - http://www.thefactory.ro/joomla-forum/
-------------------------------------------------------------------------*/

defined('_JEXEC') or die('Restricted access');

jimport('joomla.application.component.model');

class BackendModelDeletereport extends JModel
{
  function __construct()
  {
    parent::__construct();
  }

  function getDelete()
  {
    $type = JRequest::getVar('type', 0, 'GET', 'integer');

    if (!in_array($type, array(1,2)))
    {
      return false;
    }

    $id = JRequest::getVar('id', 0, 'GET', 'integer');

    if ($type == 1)
    {
      $query = ' UPDATE #__wallfactory_posts'
             . ' SET reported = 0'
             . ' WHERE id = ' . $id;
    }
    else
    {
      $query = ' UPDATE #__wallfactory_comments'
             . ' SET reported = 0'
             . ' WHERE id = ' . $id;
    }

    $database =& JFactory::getDBO();
    $database->setQuery($query);

    return $database->query();
  }
}