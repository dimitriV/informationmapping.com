<?php
/*------------------------------------------------------------------------
com_wallfactory - Wall Factory
------------------------------------------------------------------------
author    TheFactory
copyright Copyright (C) 2010 SKEPSIS Consult SRL. All Rights Reserved.
@license - http://www.gnu.org/licenses/gpl-2.0.html GNU/GPL
Websites: http://www.thefactory.ro
Technical Support:  Forum - http://www.thefactory.ro/joomla-forum/
-------------------------------------------------------------------------*/

defined('_JEXEC') or die('Restricted access');

jimport('joomla.application.component.model');

class BackendModelComments extends JModel
{
  var $_data;
  var $_total;
  var $_pagination;

  function __construct()
  {
    parent::__construct();

    global $mainframe, $option;
    $limit      = $mainframe->getUserStateFromRequest('global.list.limit', 'limit', $mainframe->getCfg('list_limit'), 'int');
    $limitstart = $mainframe->getUserStateFromRequest($option.'.limitstart', 'limitstart', 0, 'int');

    $limitstart = ($limit != 0 ? (floor($limitstart / $limit) * $limit) : 0);

    $this->setState('limit', $limit);
    $this->setState('limitstart', $limitstart);
  }

  function getData()
  {
    if (empty($this->_data))
    {
      $query = $this->_buildQuery();
      $this->_data = $this->_getList($query, $this->getState('limitstart'), $this->getState('limit'));
    }

    return $this->_data;
  }

  function _buildQuery()
  {
    $orderby = $this->_buildContentOrderBy();
    $where   = $this->_buildContentWhere();

    $query = ' SELECT c.*, p.id AS post_title, u.username'
           . ' FROM #__wallfactory_comments c'
           . ' LEFT JOIN #__users u ON u.id = c.user_id'
           . ' LEFT JOIN #__wallfactory_posts p ON p.id = c.post_id'
           . $where
           . $orderby;

    return $query;
  }

  function _buildContentOrderBy()
  {
    global $mainframe, $option;

    $filter_order     = $mainframe->getUserStateFromRequest($option . '.comments.filter_order',     'filter_order',     'title', 'cmd');
    $filter_order_Dir = $mainframe->getUserStateFromRequest($option . '.comments.filter_order_Dir', 'filter_order_Dir', 'asc',   'word');

    $orderby = '';

    if (in_array($filter_order, array('c.content', 'c.id', 'p.id', 'c.published', 'c.date_created', 'c.approved', 'c.reported')))
    {
      $orderby = ' ORDER BY ' . $filter_order . ' ' . $filter_order_Dir;
    }

    return $orderby;
  }

  function _buildContentWhere()
  {
    global $mainframe, $option;

    $search   = $mainframe->getUserStateFromRequest($option . '.comments.search',       'search',       '', 'cmd');
    $state    = $mainframe->getUserStateFromRequest($option . '.comments.filter_state', 'filter_state', '', 'cmd');
    $approved = $mainframe->getUserStateFromRequest($option . '.comments.approved',     'approved',     '', 'string');
    $reported = $mainframe->getUserStateFromRequest($option . '.comments.reported',     'reported',     '', 'string');

    $where = ' WHERE 1';

    if ($search != '')
    {
      $where .= ' AND c.content LIKE "%' . $search . '%"';
    }

    if ($state != '')
    {
      $state  = ($state == 'P') ? 1 : 0;
      $where .= ' AND c.published = ' . $state;
    }

    if ($reported != '')
    {
      $reported  = intval($reported);
      $where    .= ' AND c.reported = ' . $reported;
    }

    return $where;
  }

  function getTotal()
  {
    if (empty($this->_total))
    {
      $query = $this->_buildQuery();
      $this->_total = $this->_getListCount($query);
    }

    return $this->_total;
  }

  function getPagination()
  {
    if (empty($this->_pagination))
    {
      jimport('joomla.html.pagination');
      $this->_pagination = new JPagination($this->getTotal(), $this->getState('limitstart'), $this->getState('limit'));
    }

    return $this->_pagination;
  }
}