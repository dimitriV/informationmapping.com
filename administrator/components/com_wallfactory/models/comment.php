<?php
/*------------------------------------------------------------------------
com_wallfactory - Wall Factory
------------------------------------------------------------------------
author    TheFactory
copyright Copyright (C) 2010 SKEPSIS Consult SRL. All Rights Reserved.
@license - http://www.gnu.org/licenses/gpl-2.0.html GNU/GPL
Websites: http://www.thefactory.ro
Technical Support:  Forum - http://www.thefactory.ro/joomla-forum/
-------------------------------------------------------------------------*/

defined('_JEXEC') or die('Restricted access');

jimport('joomla.application.component.model');

class BackendModelComment extends JModel
{
  function __construct()
  {
    parent::__construct();

    $cids = JRequest::getVar('cid', 0, '', 'array');
    $this->setId((int)$cids[0]);
  }

  function setId($id)
  {
    $this->_id   = $id;
    $this->_data = null;
  }

  function &getData()
  {
    if (empty($this->_data))
    {
      $query = ' SELECT * FROM #__wallfactory_comments'
             . ' WHERE id = '.$this->_id;
      $this->_db->setQuery($query);

      $this->_data = $this->_db->loadObject();
    }

    if (!$this->_data)
    {
      $this->_data = $this->getTable();
    }

    return $this->_data;
  }

  function store()
  {
    $comment =& $this->getTable();
    $data    =  JRequest::get('post');

    if (!$comment->bind($data))
    {
      $this->setError($this->_db->getErrorMsg());
      return false;
    }

    if (!$comment->check())
    {
      $this->setError($this->_db->getErrorMsg());
      return false;
    }

    if (!$comment->store())
    {
      $this->setError($this->_db->getErrorMsg());
      return false;
    }

    return true;
  }

  function change()
  {
    $field = JRequest::getVar('field', '', 'GET', 'string');
    $value = JRequest::getVar('value',  0, 'GET', 'integer');
    $id    = JRequest::getVar('id',     0, 'GET', 'integer');

    if (!in_array($field, array('published', 'approved', 'reported')))
    {
      return false;
    }

    $query = ' UPDATE #__wallfactory_comments'
           . ' SET ' . $field . ' = ' . $value
           . ' WHERE id = ' . $id;
    $this->_db->setQuery($query);

    return $this->_db->query();
  }

  function publish()
  {
    $cids = JRequest::getVar('cid', array(0), 'post', 'array');
    JRequest::setVar('field', 'published', 'GET', true);
    JRequest::setVar('value', 1, 'GET', true);

    foreach ($cids as $cid)
    {
      JRequest::setVar('id', $cid, 'GET', true);

      if (!$this->change())
      {
        $this->setError($db->getErrorMsg());
        return false;
      }
    }

    return true;
  }

  function unpublish()
  {
    $cids = JRequest::getVar('cid', array(0), 'post', 'array');
    JRequest::setVar('field', 'published', 'GET', true);
    JRequest::setVar('value', 0, 'GET', true);

    foreach ($cids as $cid)
    {
      JRequest::setVar('id', $cid, 'GET', true);

      if (!$this->change())
      {
        $this->setError($db->getErrorMsg());
        return false;
      }
    }

    return true;
  }

  function approve()
  {
    $cids = JRequest::getVar('cid', array(0), 'post', 'array');
    JRequest::setVar('field', 'approved', 'GET', true);
    JRequest::setVar('value', 1, 'GET', true);

    foreach ($cids as $cid)
    {
      JRequest::setVar('id', $cid, 'GET', true);

      if (!$this->change())
      {
        $this->setError($db->getErrorMsg());
        return false;
      }
    }

    return true;
  }

  function unapprove()
  {
    $cids = JRequest::getVar('cid', array(0), 'post', 'array');
    JRequest::setVar('field', 'approved', 'GET', true);
    JRequest::setVar('value', 0, 'GET', true);

    foreach ($cids as $cid)
    {
      JRequest::setVar('id', $cid, 'GET', true);

      if (!$this->change())
      {
        $this->setError($db->getErrorMsg());
        return false;
      }
    }

    return true;
  }

  function delete()
  {
    $cids    = JRequest::getVar('cid', array(0), 'post', 'array');
    $comment =& $this->getTable();

    foreach ($cids as $cid)
    {
      if (!$comment->delete($cid))
      {
        $this->setError($this->_db->getErrorMsg());
        return false;
      }
    }
    return true;
  }
}