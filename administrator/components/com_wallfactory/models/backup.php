<?php
/*------------------------------------------------------------------------
com_wallfactory - Wall Factory
------------------------------------------------------------------------
author    TheFactory
copyright Copyright (C) 2010 SKEPSIS Consult SRL. All Rights Reserved.
@license - http://www.gnu.org/licenses/gpl-2.0.html GNU/GPL
Websites: http://www.thefactory.ro
Technical Support:  Forum - http://www.thefactory.ro/joomla-forum/
-------------------------------------------------------------------------*/

defined('_JEXEC') or die('Restricted access');

jimport('joomla.application.component.model');

class BackendModelBackup extends JModel
{
  var $temp_ids = array();

  function __construct()
  {
    parent::__construct();

    $this->tables = array(
      '#__wallfactory_bookmarks',
      '#__wallfactory_comments',
      '#__wallfactory_followers',
      '#__wallfactory_ips',

      '#__wallfactory_media',
      '#__wallfactory_members',
      '#__wallfactory_posts',
      '#__wallfactory_statistics',
      '#__wallfactory_urls',
      '#__wallfactory_walls',
    );

    if (!function_exists('array_diff_key'))
    {
      function array_diff_key()
      {
        $arrs = func_get_args();
        $result = array_shift($arrs);
        foreach ($arrs as $array) {
          foreach ($result as $key => $v) {
            if (array_key_exists($key, $array)) {
              unset($result[$key]);
            }
          }
        }
        return $result;
      }
    }

    if (!function_exists('fputcsv'))
    {
      function fputcsv($fh, $arr)
      {
        $csv = "";
        while (list($key, $val) = each($arr))
        {
          $val = str_replace('"', '""', $val);
          $csv .= '"'.$val.'",';
        }
        $csv = substr($csv, 0, -1);
        $csv .= "\n";
        if (!@fwrite($fh, $csv))
        return FALSE;
      }
    }
  }

  // Tasks
  function create()
  {
    jimport('joomla.filesystem.archive');
    jimport('joomla.filesystem.file');
    jimport('joomla.filesystem.folder');

    $save_settings =& JRequest::getVar('save_settings',    0, 'POST', 'integer');
    $file_name     =  'WallFactory_Backup_' . date('Y-m-d_His');
    $temp_name     =  md5(time() . rand(0, 100000));
    //$temp_name     =  'temp';
    $temp_folder   =  JPATH_COMPONENT_ADMINISTRATOR.DS.$temp_name;

    $save_bookmarks_logo =& JRequest::getVar('save_bookmarks_logo', 0, 'POST', 'integer');

    // Create temp folder
    if (!is_dir($temp_folder))
    {
      mkdir($temp_folder);
    }

    $this->temp_folder = $temp_folder;

    // Save the settings
    JFile::copy(JPATH_COMPONENT_ADMINISTRATOR.DS.'wallSettings.class.php', $temp_folder.DS.'wallSettings.class.php');
    $contents = file_get_contents($temp_folder.DS.'wallSettings.class.php');
    $contents = str_replace('wallSettings', 'wallSettingsBackup', $contents);
    file_put_contents($temp_folder.DS.'wallSettings.class.php', $contents);

    $zip =& JArchive::getAdapter('zip');

    // Save the bookmarks' logo
    if ($save_bookmarks_logo)
    {
      $files        = JFolder::files(JPATH_COMPONENT_ADMINISTRATOR.DS.'storage'.DS.'bookmarks');
      $files_to_add = array();

      foreach ($files as $file)
      {
        $data           = JFile::read(JPATH_COMPONENT_ADMINISTRATOR.DS.'storage'.DS.'bookmarks'.DS.$file);
        $files_to_add[] = array('name' => $file, 'data' => $data);
      }
      $zip->create($temp_folder.DS.'bookmarks.zip', $files_to_add);
    }

    // Create tables
    $this->createTables();

    // Create the info file
    $info = '<?php

$wallfactory_version = "' . $this->getWallfactoryVersion() . '";
$timestamp           = "' . time() . '";
$save_settings       =  ' . $save_settings . ';
$save_bookmarks_logo =  ' . $save_bookmarks_logo . ';';
    JFile::write($temp_folder.DS.'info.php', $info);

    // Create the archive
    $files        = JFolder::files($temp_folder);
    $files_to_add = array();

    foreach ($files as $file)
    {
      $data           = JFile::read($temp_folder.DS.$file);
      $files_to_add[] = array('name' => $file, 'data' => $data);
    }

    $zip->create($temp_folder.DS.$file_name.'.zip', $files_to_add);

    $archive = $temp_folder.DS.$file_name.'.zip';
    $filesize = file($archive);

    // Send the archive
    header("Pragma: public");
    header("Expires: 0");
    header("Cache-Control: must-revalidate,post-check=0,pre-check=0");
    header("Content-Type: application/zip");
    header('Content-Disposition: attachment; filename="'.$file_name.'.zip'.'"');
    header("Content-Length: ".$filesize);
    header("Content-size: ".$filesize);
    header('Content-Transfer-Encoding: binary');

    @ob_end_clean();
    readfile($archive);
    @ob_start(false);

    // Cleanup
    $this->cleanup($archive);

    exit();
  }

  function restore()
  {
    $file   =& JRequest::getVar('backup_file', array(), 'FILES', 'array');
    $append =& JRequest::getVar('append_data', 0,       'POST',  'integer');

    // Check if file uploaded
    if ($file['error'] == 4)
    {
      JError::raiseWarning('ERROR', JText::_('No file uploaded!'));
      return false;
    }

    // Check for errors
    if ($file['error'] || $file['size'] < 1)
		{
			JError::raiseWarning('ERROR', JText::_('Error code: ') . $file['error']);
      return false;
		}

    // Extract archive contents
    $temp_name =  md5(time() . rand(0, 100000));
    //$temp_name   =  'temp';
    $temp_folder =  JPATH_COMPONENT_ADMINISTRATOR.DS.$temp_name;

    // Create temp folder
    if (!is_dir($temp_folder))
    {
      mkdir($temp_folder);
    }

    $this->temp_folder = $temp_folder;
    $this->append      = $append;

    jimport('joomla.filesystem.archive');
    jimport('joomla.filesystem.file');

		$zip =& JArchive::getAdapter('zip');
		$zip->extract($file['tmp_name'], $temp_folder);

		// Check for info.php
		if (!file_exists($temp_folder.DS.'info.php'))
		{
		  JError::raiseWarning('ERROR', JText::_('Not a valid Wall Factory Backup File!'));
		  $this->cleanup();

		  return false;
		}

		require_once($temp_folder.DS.'info.php');
		$backup_version = intval(str_replace('.', '', $wallfactory_version));

		// Check if the settings are saved
    if (isset($save_settings) && $save_settings == 1 && file_exists($temp_folder.DS.'wallSettings.class.php'))
    {
      if ($backup_version <= 111)
      {
        $this->prepareBackupSettings($temp_folder);
      }

      require_once($temp_folder.DS.'wallSettings.class.php');

      $model = JModel::getInstance('settings', 'BackendModel');
      $model->restoreBackup();
    }

    // Restore tables
    $this->restoreTables();

    if ($backup_version < 120)
    {
      $this->setAliases();
    }

    // Restore bookmarks' logos
    if ($save_bookmarks_logo)
    {
      $this->restoreBookmarksLogos($temp_folder);
    }

    $this->cleanup();

		return true;
  }

  // Helpers
  function getWallfactoryVersion()
  {
    $file = JPATH_COMPONENT_ADMINISTRATOR.DS.'com_wallfactory.xml';
    $data = JApplicationHelper::parseXMLInstallFile($file);

    return $data['version'];
  }

  function prepareBackupSettings($temp_folder)
  {
    $path = $temp_folder.DS.'wallSettings.class.php';
    file_put_contents($path, str_replace('public static', 'var', file_get_contents($path)));
  }



  function createTables()
  {
    foreach ($this->tables as $table)
    {
      $this->createTable($table);
    }
  }

  function createTable($table)
  {
    // Get rows
    $query = ' SELECT t.*'
           . ' FROM ' . $table . ' t';
    $this->_db->setQuery($query);
    $rows = $this->_db->loadAssocList();

    // Check if there is any data
    if (!count($rows))
    {
      return false;
    }

    $file_name = $this->temp_folder.DS.$table.'.txt';
    $handle    = fopen($file_name, 'w');
    $temp      = $this->_db->getTableFields($table);
    $fields    = array();

    // Write header
    foreach ($temp[$table] as $field => $type)
    {
      $fields[] = $field;
    }

    fputcsv($handle, $fields);

    // Write rows
    foreach ($rows as $row)
    {
      $temp = array();
      foreach ($row as $field)
      {
        $temp[] = $this->_db->getEscaped($field);
      }
      fputcsv($handle, $temp);
    }

    fclose($handle);
  }


  function restoreTables()
  {
    // Restore tables
    foreach ($this->tables as $table)
    {
      $this->restoreTable($table);
    }
  }

  function restoreTable($table)
  {
    $wallSettingsBackup = new wallSettingsBackup();
    $wallSettings       = new wallSettings();

    $file_name = $this->temp_folder.DS.$table.'.txt';

    // Clear the table
    $this->clearTable($table);

    if (!file_exists($file_name))
    {
      return false;
    }

    $rows   = array();
    $row    = 1;
    $handle = fopen($file_name, 'r');

    while (($data = fgetcsv($handle, 1000)) !== false)
    {
      switch ($row)
      {
        case 1:
          $fields = $data;

          // Unset the fields
          $fields_to_unset = $this->getFieldsToUnset($table, $fields);
          $fields          = array_diff_key($fields, $fields_to_unset);
          break;

        default:
          // Unset the data
          $data = array_diff_key($data, $fields_to_unset);

          $rows[] = $data;
          break;
      }
      $row++;
    }

    fclose($handle);

    $this->bulkInsert($table, $fields, $rows);
  }


  function clearTable($table)
  {
    if (!$this->append)
    {
      $query = ' TRUNCATE TABLE ' . $table;
      $this->_db->setQuery($query);
      $this->_db->query();
    }
  }

  function bulkInsert($table, $fields, $rows)
  {
    $temp = array();
    foreach ($rows as $row)
    {
      $temp[] = '\'' . implode('\', \'', $row) . '\'';
    }

    $query = ' INSERT INTO ' . $table . ' (`' . implode('`, `', $fields) . '`) VALUES (' . implode('), (', $temp) . ')';
    $this->_db->setQuery($query);
    $this->_db->query();
  }

  function rowInsert($table, $fields, $row)
  {
    $query = ' INSERT INTO ' . $table . ' (`' . implode('`, `', $fields) . '`) VALUES (\'' . implode('\', \'', $row) . '\')';
    $this->_db->setQuery($query);
    $this->_db->query();
  }

  function rowUpdate($table, $fields, $row, $id)
  {
    $array = array();
    foreach ($fields as $i => $field)
    {
      if ($field != 'id')
      {
        $array[] = $field . ' = "' . $row[$i] . '"';
      }
    }

    $query = ' UPDATE ' . $table
           . ' SET ' . implode(',', $array)
           . ' WHERE id = ' . $id;
    $this->_db->setQuery($query);
    $this->_db->query();
  }

  function restoreBookmarksLogos($temp_folder)
  {
    $zip =& JArchive::getAdapter('zip');
		$zip->extract($temp_folder.DS.'bookmarks.zip', JPATH_COMPONENT_ADMINISTRATOR.DS.'storage'.DS.'bookmarks');

		$target = JPATH_COMPONENT_ADMINISTRATOR.DS.'storage'.DS.'bookmarks';

		foreach ($this->temp_ids['bookmarks'] as $id => $bookmark)
		{
		  if (JFile::exists($target.DS.$bookmark[0].'.'.$bookmark[1]))
		  {
		    JFile::delete($target.DS.$bookmark[0].'.'.$bookmark[1]);
		  }

		  JFile::move($temp_folder.DS.'bookmarks'.DS.$id.'.'.$bookmark[1], $target.DS.$bookmark[0].'.'.$bookmark[1]);
		}
  }

  function cleanup($archive = null)
  {
    $files = JFolder::files($this->temp_folder);
    foreach ($files as $file)
    {
      unlink($this->temp_folder.DS.$file);
    }

    // Delete bookmarks folder
    if (is_dir($this->temp_folder.DS.'bookmarks'))
    {
      $files = JFolder::files($this->temp_folder.DS.'bookmarks');
      foreach ($files as $file)
      {
        unlink($this->temp_folder.DS.'bookmarks'.DS.$file);
      }

      rmdir($this->temp_folder.DS.'bookmarks');
    }

    // Folder
    rmdir($this->temp_folder);
  }

  function getFieldsToUnset($table, $fields)
  {
    $fields_to_unset = array();

    $special = array(
      '#__wallfactory_walls',
      '#__wallfactory_posts',
      '#__wallfactory_categories',
      '#__wallfactory_comments',
      '#__wallfactory_bookmarks'
    );

    // Get the available fields and types
    $current_fields = $this->_db->getTableFields($table);

    // Compare and unset
    foreach ($fields as $i => $field)
    {
      $found = false;
      foreach ($current_fields[$table] as $current_field => $current_type)
      {
        // Check if the name and type are the same
        if ($current_field == $field)
        {
          $found = true;
          continue;
        }
      }

      if (!$found)
      {
        $fields_to_unset[$i] = $i;
      }
    }

    return $fields_to_unset;
  }

  function setAliases()
  {
    // 1. Set aliases for walls
    $query = ' SELECT b.id, b.title'
           . ' FROM #__wallfactory_walls b';
    $this->_db->setQuery($query);
    $walls = $this->_db->loadObjectList();

    foreach ($walls as $wall)
    {
      $table =& $this->getTable('wall');

      $table->id    = $wall->id;
      $table->alias = $table->_generateUniqueAlias($wall->title);

      $table->store();
    }

    // 2. Set aliasses for posts
    $wall =& $this->getTable('wall');
    $query = ' SELECT p.id '
           . ' FROM #__wallfactory_posts p';
    $this->_db->setQuery($query);
    $posts = $this->_db->loadObjectList();

    foreach ($posts as $post)
    {
      $table =& $this->getTable('post');

      $table->bind($post);

      $table->store();
    }
  }
}