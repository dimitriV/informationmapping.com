<?php
/*------------------------------------------------------------------------
com_wallfactory - Wall Factory
------------------------------------------------------------------------
author    TheFactory
copyright Copyright (C) 2010 SKEPSIS Consult SRL. All Rights Reserved.
@license - http://www.gnu.org/licenses/gpl-2.0.html GNU/GPL
Websites: http://www.thefactory.ro
Technical Support:  Forum - http://www.thefactory.ro/joomla-forum/
-------------------------------------------------------------------------*/

defined('_JEXEC') or die('Restricted access');

jimport('joomla.application.component.model');

class BackendModelUrls extends JModel
{
  var $_data;
  var $_total;
  var $_pagination;

  function __construct()
  {
    parent::__construct();

    global $mainframe, $option;
    $limit      = $mainframe->getUserStateFromRequest('global.list.limit', 'limit', $mainframe->getCfg('list_limit'), 'int');
    $limitstart = $mainframe->getUserStateFromRequest($option.'.limitstart', 'limitstart', 0, 'int');

    $limitstart = ($limit != 0 ? (floor($limitstart / $limit) * $limit) : 0);

    $this->setState('limit', $limit);
    $this->setState('limitstart', $limitstart);
   
    $cids = JRequest::getVar('cid', 0, '', 'array');
   
    $this->setId((int)$cids[0]);
  }
 
  function setId($id)
  {
    $this->_id   = $id;
    $this->_data = null;
    $this->_total = null;
  }

  function getData()
  {
    if (empty($this->_data))
    {
      $query = $this->_buildQuery();
      $this->_data = $this->_getList($query, $this->getState('limitstart'), $this->getState('limit'));
    }

    return $this->_data;
  }

  function _buildQuery()
  {
    $orderby = $this->_buildContentOrderBy();
    $where   = $this->_buildContentWhere();

    $query = ' SELECT l.id, l.url, l.url_title, l.url_description, l.date_added, l.wall_id, w.title AS wall_title, w.user_id, u.username'
           . ' FROM #__wallfactory_urls l'
           . ' LEFT JOIN #__wallfactory_walls w ON w.id = l.wall_id'
           . ' LEFT JOIN #__users u ON u.id = l.user_id'
           . $where
           . ' AND LENGTH(l.url) > 7 '
           . $orderby;
           ;

    return $query;
  }

  function _buildContentOrderBy()
  {
    global $mainframe, $option;

    $filter_order     = $mainframe->getUserStateFromRequest($option . '.posts.filter_order',     'filter_order',     'title', 'cmd');
    $filter_order_Dir = $mainframe->getUserStateFromRequest($option . '.posts.filter_order_Dir', 'filter_order_Dir', 'asc',   'word');

    $orderby = '';

    if (in_array($filter_order, array('l.url', 'l.url_title', 'l.url_description', 'l.date_added', 'u.username', 'wall_title')))
    {
      $orderby = ' ORDER BY ' . $filter_order . ' ' . $filter_order_Dir;
    }

    return $orderby;
  }

  function _buildContentWhere()
  {
    global $mainframe, $option;

    $search = $mainframe->getUserStateFromRequest($option . '.urls.search',       'search',       '', 'cmd');
    $state  = $mainframe->getUserStateFromRequest($option . '.urls.filter_state', 'filter_state', '', 'cmd');

    $where = ' WHERE 1';

    if ($search != '')
    {
      $where .= ' AND l.url LIKE "%' . $search . '%"';
    }
    
    $where .= ($this->_id != 0) ? (' AND l.user_id = '.$this->_id) : ('');   
   
    return $where;
  }

  function getTotal()
  {
    if (empty($this->_total))
    {
      $query = $this->_buildQuery();
      $this->_total = $this->_getListCount($query);
    }

    return $this->_total;
  }

  function getPagination()
  {
    if (empty($this->_pagination))
    {
      jimport('joomla.html.pagination');
      $this->_pagination = new JPagination($this->getTotal(), $this->getState('limitstart'), $this->getState('limit'));
    }

    return $this->_pagination;
  }
}