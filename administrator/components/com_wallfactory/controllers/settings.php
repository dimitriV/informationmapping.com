<?php
/*------------------------------------------------------------------------
com_wallfactory - Wall Factory
------------------------------------------------------------------------
author    TheFactory
copyright Copyright (C) 2010 SKEPSIS Consult SRL. All Rights Reserved.
@license - http://www.gnu.org/licenses/gpl-2.0.html GNU/GPL
Websites: http://www.thefactory.ro
Technical Support:  Forum - http://www.thefactory.ro/joomla-forum/
-------------------------------------------------------------------------*/

defined('_JEXEC') or die('Restricted access');

class BackendControllerSettings extends BackendController
{
	function __construct()
	{
		parent::__construct();
	}

  function save()
  {
    $model = $this->getModel('settings');

    if ($model->store())
    {
      $msg = JText::_('Settings Saved!');
    }
    else
    {
      $msg = JText::_('Error Saving Settings!') . ' ' . $model->getError();
    }

    $this->setRedirect('index.php?option=com_wallfactory&task=settings', $msg);
  }

  function cancel()
  {
    $msg = JText::_('Operation Cancelled');
    $this->setRedirect('index.php?option=com_wallfactory&task=settings', $msg);
  }
}