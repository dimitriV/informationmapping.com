<?php
defined('_JEXEC') or die('Restricted access');

/*------------------------------------------------------------------------
com_wallfactory - Wall Factory
------------------------------------------------------------------------
author    TheFactory
copyright Copyright (C) 2010 SKEPSIS Consult SRL. All Rights Reserved.
@license - http://www.gnu.org/licenses/gpl-2.0.html GNU/GPL
Websites: http://www.thefactory.ro
Technical Support:  Forum - http://www.thefactory.ro/joomla-forum/
-------------------------------------------------------------------------*/

class BackendControllerFile extends BackendController
{
	function __construct()
	{
		parent::__construct();

		$this->registerTask('add', 'edit');
	}

	function edit()
  {
    JRequest::setVar('view',   'file');
    JRequest::setVar('layout', 'form');
    JRequest::setVar('hidemainmenu', 1);

    parent::display();
  }

	function change()
  {
    $model = $this->getModel('file');

    if ($model->change())
    {
      $msg = JText::_('State changed!');
    }
    else
    {
      $msg = JText::_('Error Changing State');
    }

    $this->setRedirect('index.php?option=com_wallfactory&task=files', $msg);
  }
 
  function remove()
  {
    $model = $this->getModel('mp3file');

    if (!$model->delete())
    {
      $msg = JText::_('Error: One or More Files Could not be Deleted');
    }
    else
    {
      $msg = JText::_('File(s) Deleted');
    }

    $this->setRedirect('index.php?option=com_wallfactory&task=files', $msg);
  }

  function save()
  {
    $model = $this->getModel('file');
    
    if ($model->store())
    {
      $msg = JText::_('File Saved!');
    }
    else
    {
      $msg = JText::_('Error Saving File!');
    }

    $this->setRedirect('index.php?option=com_wallfactory&task=files', $msg);
  }

  function cancel()
  {
    $msg = JText::_('Operation Cancelled');
    $this->setRedirect('index.php?option=com_wallfactory&task=files', $msg);
  }
/*
  function archive()
  {
    $model = $this->getModel('post');

    if ($model->archive())
    {
      $msg = JText::_('Post(s) archived!');
    }
    else
    {
      $msg = JText::_('Error Archiving Post(s)');
    }

    $this->setRedirect('index.php?option=com_wallfactory&task=posts', $msg);
  }

  function unarchive()
  {
    $model = $this->getModel('post');

    if ($model->unarchive())
    {
      $msg = JText::_('Post(s) unarchived!');
    }
    else
    {
      $msg = JText::_('Error Unarchiving Post(s)');
    }

    $this->setRedirect('index.php?option=com_wallfactory&task=posts', $msg);
  }*/
}