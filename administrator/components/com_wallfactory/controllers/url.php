<?php
/*------------------------------------------------------------------------
com_wallfactory - Wall Factory
------------------------------------------------------------------------
author    TheFactory
copyright Copyright (C) 2010 SKEPSIS Consult SRL. All Rights Reserved.
@license - http://www.gnu.org/licenses/gpl-2.0.html GNU/GPL
Websites: http://www.thefactory.ro
Technical Support:  Forum - http://www.thefactory.ro/joomla-forum/
-------------------------------------------------------------------------*/

defined('_JEXEC') or die('Restricted access');

class BackendControllerUrl extends BackendController
{
	function __construct()
	{
		parent::__construct();

		$this->registerTask('add', 'edit');
	}

	function edit()
  {
    JRequest::setVar('view',   'url');
    JRequest::setVar('layout', 'form');
    JRequest::setVar('hidemainmenu', 1);

    parent::display();
  }

	function change()
  {
    $model = $this->getModel('url');

    if ($model->change())
    {
      $msg = JText::_('State changed!');
    }
    else
    {
      $msg = JText::_('Error Changing State');
    }

    $this->setRedirect('index.php?option=com_wallfactory&task=urls', $msg);
  }

  function remove()
  {
    $model = $this->getModel('url');

    if (!$model->delete())
    {
      $msg = JText::_('Error: One or More Links Could not be Deleted');
    }
    else
    {
      $msg = JText::_('Links(s) Deleted');
    }

    $this->setRedirect('index.php?option=com_wallfactory&task=urls', $msg);
  }

  function save()
  {
    $model = $this->getModel('url');

    if ($model->store())
    {
      $msg = JText::_('Link Saved!');
    }
    else
    {
      $msg = JText::_('Error Saving Link!');
    }

    $this->setRedirect('index.php?option=com_wallfactory&task=urls', $msg);
  }

  function cancel()
  {
    $msg = JText::_('Operation Cancelled');
    $this->setRedirect('index.php?option=com_wallfactory&task=urls', $msg);
  }

}