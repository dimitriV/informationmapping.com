<?php
/*------------------------------------------------------------------------
com_wallfactory - Wall Factory
------------------------------------------------------------------------
author    TheFactory
copyright Copyright (C) 2010 SKEPSIS Consult SRL. All Rights Reserved.
@license - http://www.gnu.org/licenses/gpl-2.0.html GNU/GPL
Websites: http://www.thefactory.ro
Technical Support:  Forum - http://www.thefactory.ro/joomla-forum/
-------------------------------------------------------------------------*/

defined('_JEXEC') or die('Restricted access');

class BackendControllerComment extends BackendController
{
	function __construct()
	{
		parent::__construct();

		$this->registerTask('add', 'edit');
	}

	function edit()
  {
    JRequest::setVar('view',   'comment');
    JRequest::setVar('layout', 'form');
    JRequest::setVar('hidemainmenu', 1);

    parent::display();
  }


  function change()
  {
    $model = $this->getModel('comment');

    if ($model->change())
    {
      $msg = JText::_('State changed!');
    }
    else
    {
      $msg = JText::_('Error Changing State');
    }

    $this->setRedirect('index.php?option=com_wallfactory&task=comments', $msg);
  }

  function publish()
  {
    $model = $this->getModel('comment');

    if ($model->publish())
    {
      $msg = JText::_('Comment(s) published!');
    }
    else
    {
      $msg = JText::_('Error Publishing Comment(s)');
    }

    $this->setRedirect('index.php?option=com_wallfactory&task=comments', $msg);
  }

  function unpublish()
  {
    $model = $this->getModel('comment');

    if ($model->unpublish())
    {
      $msg = JText::_('Comment(s) unpublished!');
    }
    else
    {
      $msg = JText::_('Error Unpublishing Comment(s)');
    }

    $this->setRedirect('index.php?option=com_wallfactory&task=comments', $msg);
  }

  function approve()
  {
    $model = $this->getModel('comment');

    if ($model->approve())
    {
      $msg = JText::_('Comment(s) approved!');
    }
    else
    {
      $msg = JText::_('Error Approving Comment(s)');
    }

    $this->setRedirect('index.php?option=com_wallfactory&task=comments', $msg);
  }

  function unapprove()
  {
    $model = $this->getModel('comment');

    if ($model->unapprove())
    {
      $msg = JText::_('Comment(s) unapproved!');
    }
    else
    {
      $msg = JText::_('Error Unapproving Comment(s)');
    }

    $this->setRedirect('index.php?option=com_wallfactory&task=comments', $msg);
  }

  function remove()
  {
    $model = $this->getModel('comment');

    if (!$model->delete())
    {
      $msg = JText::_('Error: One or More Comments Could not be Deleted');
    }
    else
    {
      $msg = JText::_('Comment(s) Deleted');
    }

    $this->setRedirect('index.php?option=com_wallfactory&task=comments', $msg);
  }

  function cancel()
  {
    $msg = JText::_('Operation Cancelled');

    $this->setRedirect('index.php?option=com_wallfactory&task=comments', $msg);
  }

  function save()
  {
    $model = $this->getModel('comment');

    if ($model->store())
    {
      $msg = JText::_('Comment Saved!');
    }
    else
    {
      $msg = JText::_('Error Saving Comment');
    }

    $this->setRedirect('index.php?option=com_wallfactory&task=comments', $msg);
  }
}