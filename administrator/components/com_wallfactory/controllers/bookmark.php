<?php
/*------------------------------------------------------------------------
com_wallfactory - Wall Factory
------------------------------------------------------------------------
author    TheFactory
copyright Copyright (C) 2010 SKEPSIS Consult SRL. All Rights Reserved.
@license - http://www.gnu.org/licenses/gpl-2.0.html GNU/GPL
Websites: http://www.thefactory.ro
Technical Support:  Forum - http://www.thefactory.ro/joomla-forum/
-------------------------------------------------------------------------*/

defined('_JEXEC') or die('Restricted access');

class BackendControllerBookmark extends BackendController
{
	function __construct()
	{
		parent::__construct();

		$this->registerTask('add', 'edit');
	}

	function edit()
  {
    JRequest::setVar('view',   'bookmark');
    JRequest::setVar('layout', 'form');
    JRequest::setVar('hidemainmenu', 1);

    parent::display();
  }

	function change()
  {
    $model = $this->getModel('bookmark');

    if ($model->change())
    {
      $msg = JText::_('State changed!');
    }
    else
    {
      $msg = JText::_('Error Changing State');
    }

    $this->setRedirect('index.php?option=com_wallfactory&task=bookmarking', $msg);
  }

  function publish()
  {
    $model = $this->getModel('bookmark');

    if ($model->publish())
    {
      $msg = JText::_('Bookmark(s) published!');
    }
    else
    {
      $msg = JText::_('Error Publishing Bookmark(s)');
    }

    $this->setRedirect('index.php?option=com_wallfactory&task=bookmarking', $msg);
  }

  function unpublish()
  {
    $model = $this->getModel('bookmark');

    if ($model->unpublish())
    {
      $msg = JText::_('Bookmark(s) unpublished!');
    }
    else
    {
      $msg = JText::_('Error Unpublishing Bookmark(s)');
    }

    $this->setRedirect('index.php?option=com_wallfactory&task=bookmarking', $msg);
  }

  function remove()
  {
    $model = $this->getModel('bookmark');

    if (!$model->delete())
    {
      $msg = JText::_('Error: One or More Bookmarks Could not be Deleted');
    }
    else
    {
      $msg = JText::_('Bookmarks(s) Deleted');
    }

    $this->setRedirect('index.php?option=com_wallfactory&task=bookmarking', $msg);
  }

  function save()
  {
    $model = $this->getModel('bookmark');

    if ($model->store())
    {
      $msg = JText::_('Bookmark Saved!');
    }
    else
    {
      $msg = JText::_('Error Saving Bookmark');
    }

    $this->setRedirect('index.php?option=com_wallfactory&task=bookmarking', $msg);
  }

  function cancel()
  {
    $msg = JText::_('Operation Cancelled');
    $this->setRedirect('index.php?option=com_wallfactory&task=bookmarking', $msg);
  }
}