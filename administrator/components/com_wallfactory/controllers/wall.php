<?php
/*------------------------------------------------------------------------
com_wallfactory - Wall Factory
------------------------------------------------------------------------
author    TheFactory
copyright Copyright (C) 2010 SKEPSIS Consult SRL. All Rights Reserved.
@license - http://www.gnu.org/licenses/gpl-2.0.html GNU/GPL
Websites: http://www.thefactory.ro
Technical Support:  Forum - http://www.thefactory.ro/joomla-forum/
-------------------------------------------------------------------------*/

defined('_JEXEC') or die('Restricted access');

class BackendControllerWall extends BackendController
{
	function __construct()
	{
		parent::__construct();

		$this->registerTask('add', 'edit');
	}

	function edit()
  {
    JRequest::setVar('view',   'wall');
    JRequest::setVar('layout', 'form');
    JRequest::setVar('hidemainmenu', 1);

    parent::display();
  }

	function change()
  {
    $model = $this->getModel('wall');

    if ($model->change())
    {
      $msg = JText::_('State changed!');
    }
    else
    {
      $msg = JText::_('Error Changing State');
    }

    $this->setRedirect('index.php?option=com_wallfactory&task=walls', $msg);
  }

  function publish()
  {
    $model = $this->getModel('wall');

    if ($model->publish())
    {
      $msg = JText::_('Wall(s) published!');
    }
    else
    {
      $msg = JText::_('Error Publishing Wall(s)');
    }

    $this->setRedirect('index.php?option=com_wallfactory&task=walls', $msg);
  }

  function unpublish()
  {
    $model = $this->getModel('wall');

    if ($model->unpublish())
    {
      $msg = JText::_('Wall(s) unpublished!');
    }
    else
    {
      $msg = JText::_('Error Unpublishing Wall(s)');
    }

    $this->setRedirect('index.php?option=com_wallfactory&task=walls', $msg);
  }

  function remove()
  {
    $model = $this->getModel('wall');

    if (!$model->delete())
    {
      $msg = JText::_('Error: One or More Walls Could not be Deleted');
    }
    else
    {
      $msg = JText::_('Wall(s) Deleted');
    }

    $this->setRedirect('index.php?option=com_wallfactory&task=walls', $msg);
  }

  function save()
  {
    $model = $this->getModel('wall');

    if ($model->store())
    {
      $msg = JText::_('Wall Saved!');
    }
    else
    {
      $msg = JText::_('Error Saving Wall');
    }

    $this->setRedirect('index.php?option=com_wallfactory&task=users', $msg);
  }

  function cancel()
  {
    $msg = JText::_('Operation Cancelled');
    $this->setRedirect('index.php?option=com_wallfactory&task=users', $msg);
  }

}