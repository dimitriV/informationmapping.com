<?php
/*------------------------------------------------------------------------
com_wallfactory - Wall Factory
------------------------------------------------------------------------
author    TheFactory
copyright Copyright (C) 2010 SKEPSIS Consult SRL. All Rights Reserved.
@license - http://www.gnu.org/licenses/gpl-2.0.html GNU/GPL
Websites: http://www.thefactory.ro
Technical Support:  Forum - http://www.thefactory.ro/joomla-forum/
-------------------------------------------------------------------------*/

defined('_JEXEC') or die('Restricted access');

class BackendControllerPost extends BackendController
{
	function __construct()
	{
		parent::__construct();

		$this->registerTask('add', 'edit');
	}

	function edit()
  {
    JRequest::setVar('view',   'post');
    JRequest::setVar('layout', 'form');
    JRequest::setVar('hidemainmenu', 1);

    parent::display();
  }

	function change()
  {
    $model = $this->getModel('post');

    if ($model->change())
    {
      $msg = JText::_('State changed!');
    }
    else
    {
      $msg = JText::_('Error Changing State');
    }

    $this->setRedirect('index.php?option=com_wallfactory&task=posts', $msg);
  }

  function publish()
  {
    $model = $this->getModel('post');

    if ($model->publish())
    {
      $msg = JText::_('Post(s) published!');
    }
    else
    {
      $msg = JText::_('Error Publishing Post(s)');
    }

    $this->setRedirect('index.php?option=com_wallfactory&task=posts', $msg);
  }

  function unpublish()
  {
    $model = $this->getModel('post');

    if ($model->unpublish())
    {
      $msg = JText::_('Post(s) unpublished!');
    }
    else
    {
      $msg = JText::_('Error Unpublishing Post(s)');
    }

    $this->setRedirect('index.php?option=com_wallfactory&task=posts', $msg);
  }

  function remove()
  {
    $model = $this->getModel('post');

    if (!$model->delete())
    {
      $msg = JText::_('Error: One or More Posts Could not be Deleted');
    }
    else
    {
      $msg = JText::_('Post(s) Deleted');
    }

    $this->setRedirect('index.php?option=com_wallfactory&task=posts', $msg);
  }

  function save()
  {
    $model = $this->getModel('post');

    if ($model->store())
    {
      $msg = JText::_('Post Saved!');
    }
    else
    {
      $msg = JText::_('Error Saving Post!');
    }

    $this->setRedirect('index.php?option=com_wallfactory&task=posts', $msg);
  }

  function cancel()
  {
    $msg = JText::_('Operation Cancelled');
    $this->setRedirect('index.php?option=com_wallfactory&task=posts', $msg);
  }

  function import()
  {
    $model = $this->getModel('post');

    $model->import();
  }

  function archive()
  {
    $model = $this->getModel('post');

    if ($model->archive())
    {
      $msg = JText::_('Post(s) archived!');
    }
    else
    {
      $msg = JText::_('Error Archiving Post(s)');
    }

    $this->setRedirect('index.php?option=com_wallfactory&task=posts', $msg);
  }

  function unarchive()
  {
    $model = $this->getModel('post');

    if ($model->unarchive())
    {
      $msg = JText::_('Post(s) unarchived!');
    }
    else
    {
      $msg = JText::_('Error Unarchiving Post(s)');
    }

    $this->setRedirect('index.php?option=com_wallfactory&task=posts', $msg);
  }
}