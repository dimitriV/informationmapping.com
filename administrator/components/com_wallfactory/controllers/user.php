<?php
/*------------------------------------------------------------------------
com_wallfactory - Wall Factory
------------------------------------------------------------------------
author    TheFactory
copyright Copyright (C) 2010 SKEPSIS Consult SRL. All Rights Reserved.
@license - http://www.gnu.org/licenses/gpl-2.0.html GNU/GPL
Websites: http://www.thefactory.ro
Technical Support:  Forum - http://www.thefactory.ro/joomla-forum/
-------------------------------------------------------------------------*/

defined('_JEXEC') or die('Restricted access');

class BackendControllerUser extends BackendController
{
	function __construct()
	{
		parent::__construct();
	}

	function edit()
    {
    	JRequest::setVar('view',   'user');
    	JRequest::setVar('layout', 'form');

    	parent::display();
    }

  /*function view()
  {
    JRequest::setVar('view', 'user');
    JRequest::setVar('layout', 'form');

    parent::display();
  }
  */
  function save()
  {
    $model = $this->getModel('user');

    if ($model->store())
    {
      $msg = JText::_('User Settings Saved!');
    }
    else
    {
      $msg = JText::_('Error Saving User Settings');
    }

    $this->setRedirect('index.php?option=com_wallfactory&task=users', $msg);
  }

  function cancel()
  {
    $msg = JText::_('Operation Cancelled');

    $this->setRedirect('index.php?option=com_wallfactory&task=users', $msg);
  }
  
  function dashboard()
  {
  	$view  =& $this->getView('dashboard', 'html');
    $model =& $this->getModel('dashboard');

    $view->setModel($model, true);
    $view->display();
  }
  
  function videos()
  {
    $view  =& $this->getView('videos', 'html');
    $model =& $this->getModel('videos');

    $view->setModel($model, true);
    $view->display();
  }

  function urls()
  {
    $view  =& $this->getView('urls', 'html');
    $model =& $this->getModel('urls');

    $view->setModel($model, true);
    $view->display();
  }

  function posts()
  {
    $view  =& $this->getView('posts', 'html');
    $model =& $this->getModel('posts');

    $view->setModel($model, true);
    $view->display();
  }
  
  function images()
  {
    $view  =& $this->getView('images', 'html');
    $model =& $this->getModel('images');

    $view->setModel($model, true);
    $view->display();
  }
  
  function mp3files()
  {
    $view  =& $this->getView('mp3files', 'html');
    $model =& $this->getModel('mp3files');

    $view->setModel($model, true);
    $view->display();
  }
  
  function files()
  {
    $view  =& $this->getView('files', 'html');
    $model =& $this->getModel('files');

    $view->setModel($model, true);
    $view->display();
  }
  
  function publish()
  {
    $model = $this->getModel('wall');

    if ($model->publish())
    {
      $msg = JText::_('Wall(s) published!');
    }
    else
    {
      $msg = JText::_('Error Publishing Wall(s)');
    }

    $this->setRedirect('index.php?option=com_wallfactory&task=users', $msg);
  }

  function unpublish()
  {
    $model = $this->getModel('wall');

    if ($model->unpublish())
    {
      $msg = JText::_('Wall(s) unpublished!');
    }
    else
    {
      $msg = JText::_('Error Unpublishing Wall(s)');
    }

    $this->setRedirect('index.php?option=com_wallfactory&task=users', $msg);
  }
  
  function actionBan()
  {
    $id     =  JRequest::getVar('id', 0, 'GET', 'integer');
    $model  =  $this->getModel('report');

    if ($model->actionBan())
    {
      $msg = JText::_('User Banned!');
    }
    else
    {
      $msg = JText::_('Error Banning User!');
    }

    $this->setRedirect('index.php?option=com_wallfactory&task=users', $msg);
  }
  
  function ban()
  {
    $model = $this->getModel('user');

    if (!$model->ban())
    {
      $msg = JText::_('Error Banning User!');
    }
    else
    {
      $msg = JText::_('User Banned');
    }

    $this->setRedirect('index.php?option=com_wallfactory&task=users', $msg);
  }
  
  function unban()
  {
    $model = $this->getModel('user');

    if (!$model->unban())
    {
      $msg = JText::_('Error Unbanning User!');
    }
    else
    {
      $msg = JText::_('User Unbanned');
    }

    $this->setRedirect('index.php?option=com_wallfactory&task=users', $msg);
  }

  

  function unbanList()
  {
    $model = $this->getModel('user');

    if (!$model->unbanList())
    {
      $msg = JText::_('Error Unbanning Users!');
    }
    else
    {
      $msg = JText::_('Users Unbanned');
    }

    $this->setRedirect('index.php?option=com_wallfactory&task=users', $msg);
  }

  function banList()
  {
    $model = $this->getModel('user');

    if (!$model->banList())
    {
      $msg = JText::_('Error Banning Users!');
    }
    else
    {
      $msg = JText::_('Users Banned');
    }

    $this->setRedirect('index.php?option=com_wallfactory&task=users', $msg);
  }
  
  function remove()
  {
    $model = $this->getModel('wall');

    if (!$model->delete())
    {
      $msg = JText::_('Error: One or More Users Could not be Deleted');
    }
    else
    {
      	$model = $this->getModel('user');
	    if (!$model->delete())
	    {
	      $msg = JText::_('Error: One or More Users Could not be Deleted');
	    }
	    else
	    {
	      $msg = JText::_('Wall(s) Deleted');
	    }
    }
    
    $this->setRedirect('index.php?option=com_wallfactory&task=users', $msg);
  }
  
}