<?php
/*------------------------------------------------------------------------
com_wallfactory - Wall Factory
------------------------------------------------------------------------
author    TheFactory
copyright Copyright (C) 2010 SKEPSIS Consult SRL. All Rights Reserved.
@license - http://www.gnu.org/licenses/gpl-2.0.html GNU/GPL
Websites: http://www.thefactory.ro
Technical Support:  Forum - http://www.thefactory.ro/joomla-forum/
-------------------------------------------------------------------------*/

defined('_JEXEC') or die('Restricted access');

class TableUrls extends JTable
{
  var $id               	= null;
  var $user_id          	= null;
  var $wall_id          	= null;
  var $url			    	= null;
  var $url_title 	    	= null;
  var $url_description		= null;
  var $video_link	    	= null;
  var $video_title			= null;
  var $video_description 	= null;
  var $video_thumbnail		= null;
  var $video_sitename		= null;
  var $video_sourceThumb 	= null;
  var $date_added	 		= null;
  var $post_id		    	= null;

  function TableUrls(&$db)
  {
    parent::__construct('#__wallfactory_urls', 'id', $db);
  }
  
   // CRUD
  function delete($oid = null)
  {
    if ($oid)
    {
      $this->id = $oid;
    }

    $this->deleteUrls();

    return parent::delete($oid);
  }
  
  function getUrls()
  {
    $query = ' SELECT l.id'
           . ' FROM #__wallfactory_urls l'
           . ' WHERE l.wall_id = ' . $this->id;
    $this->_db->setQuery($query);

    return $this->_db->loadObjectList();
  }
  
  // Helpers
  function deleteUrls()
  {
    $urls      =  $this->getUrls();
    $urls_table =& JTable::getInstance('urls', 'Table');

    foreach ($urls as $url)
    {
      $urls_table->delete($url->id);
    }
  }
  
}