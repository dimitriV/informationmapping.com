<?php
/*------------------------------------------------------------------------
com_wallfactory - Wall Factory
------------------------------------------------------------------------
author    TheFactory
copyright Copyright (C) 2010 SKEPSIS Consult SRL. All Rights Reserved.
@license - http://www.gnu.org/licenses/gpl-2.0.html GNU/GPL
Websites: http://www.thefactory.ro
Technical Support:  Forum - http://www.thefactory.ro/joomla-forum/
-------------------------------------------------------------------------*/

defined('_JEXEC') or die('Restricted access');

class TablePost extends JTable
{
  var $id               = null;
  var $wall_id          = null;
  var $user_id       	= null;
  var $alias            = null;
  var $content          = null;
  var $notification     = null;
  var $date_created     = null;
  var $date_updated     = null;
  var $enable_comments  = null;
  var $reported  		= null;
  var $reporting_user_id	= null;
 
  function TablePost(&$db)
  {
    parent::__construct('#__wallfactory_posts', 'id', $db);
  }

  // CRUD
  function delete($oid = null)
  {
    if ($oid)
    {
      $this->id = $oid;
    }

    $this->deleteComments();

    return parent::delete($oid);
  }

  function store($updateNulls = false)
  {
    $wall =& JTable::getInstance('wall', 'Table');

    $this->content = wallHelper::stripJavascript($this->content);
   
    return parent::store();
  }

  // Getters
  function getUserId()
  {
    $wall =& JTable::getInstance('wall', 'Table');
    $wall->load($this->wall_id);

    return $wall->user_id;
  }

  function getComments()
  {
    $query = ' SELECT c.id'
           . ' FROM #__wallfactory_comments c'
           . ' WHERE c.post_id = ' . $this->id;
    $this->_db->setQuery($query);

    return $this->_db->loadObjectList();
  }

  // Helpers
  function deleteComments()
  {
    $comments      =  $this->getComments();
    $comment_table =& JTable::getInstance('comment', 'Table');

    foreach ($comments as $comment)
    {
      $comment_table->delete($comment->id);
    }
  }

}
