<?php
/*------------------------------------------------------------------------
com_wallfactory - Wall Factory
------------------------------------------------------------------------
author    TheFactory
copyright Copyright (C) 2010 SKEPSIS Consult SRL. All Rights Reserved.
@license - http://www.gnu.org/licenses/gpl-2.0.html GNU/GPL
Websites: http://www.thefactory.ro
Technical Support:  Forum - http://www.thefactory.ro/joomla-forum/
-------------------------------------------------------------------------*/

defined('_JEXEC') or die('Restricted access');

class TableWall extends JTable
{
  var $id                          	= null;
  var $user_id                     	= null;
  var $published                   	= null;
  var $title                       	= null;
  var $alias                       	= null;
  var $posts_per_page              	= null;
  var $comment_notification        	= null;
  var $report_comment_notification 	= null;
  var $post_notification        	= null;
  var $report_post_notification 	= null;
  var $date_created                 = null;
 
  function TableWall(&$db)
  {
    parent::__construct('#__wallfactory_walls', 'id', $db);
  }
  
  function findOneByUserId($user_id)
  {
    $query = ' SELECT w.*'
           . ' FROM #__wallfactory_walls w'
           . ' WHERE w.user_id = ' . $user_id;
    $this->_db->setQuery($query);

    $this->bind($this->_db->loadObject());
  }
  

  // CRUD
  function delete($oid = null)
  {
    if ($oid)
    {
      $this->id = $oid;
    }
       
    return parent::delete($oid);
  }

  function store($updateNulls = false)
  {
    if (is_null($this->id))
    {
      $this->alias = $this->_generateUniqueAlias($this->title);
    }

    return parent::store($updateNulls);
  }
 

  function _generateUniqueAlias($title)
  {
    $title = JFilterOutput::stringURLSafe($title);
    $valid = false;
    $try   = 1;

    while (!$valid)
    {
      $alias = (1 < $try) ? $title . '-' . $try : $title;

      $queryWall = ' SELECT w.id'
                 . ' FROM #__wallfactory_walls w'
                 . ' WHERE w.alias = "' . $alias . '"'; 

      $queryPost = ' SELECT p.id'
                 . ' FROM #__wallfactory_posts p'
                 . ' WHERE p.alias = "' . $alias . '"'; 

      $query = $queryWall . ' UNION ' . $queryPost;

      $this->_db->setQuery($query, 0, 1);

      if (!$this->_db->loadResult())
      {
        return $alias;
      }

      $try++;
    }
  }
}
