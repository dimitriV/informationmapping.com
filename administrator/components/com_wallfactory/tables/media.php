<?php
/*------------------------------------------------------------------------
com_wallfactory - Wall Factory
------------------------------------------------------------------------
author    TheFactory
copyright Copyright (C) 2010 SKEPSIS Consult SRL. All Rights Reserved.
@license - http://www.gnu.org/licenses/gpl-2.0.html GNU/GPL
Websites: http://www.thefactory.ro
Technical Support:  Forum - http://www.thefactory.ro/joomla-forum/
-------------------------------------------------------------------------*/

defined('_JEXEC') or die('Restricted access');

class TableMedia extends JTable
{
  var $id               = null;
  var $user_id          = null;
  var $wall_id			= null;
  var $folder		    = null;
  var $title			= null;
  var $name             = null;
  var $extension 		= null;
  var $description      = null;
  var $date_added		= null;
  var $post_id			= null;


  function TableMedia(&$db)
  {
    parent::__construct('#__wallfactory_media', 'id', $db);
  }
}