<?php
/*------------------------------------------------------------------------
com_wallfactory - Wall Factory
------------------------------------------------------------------------
author    TheFactory
copyright Copyright (C) 2010 SKEPSIS Consult SRL. All Rights Reserved.
@license - http://www.gnu.org/licenses/gpl-2.0.html GNU/GPL
Websites: http://www.thefactory.ro
Technical Support:  Forum - http://www.thefactory.ro/joomla-forum/
-------------------------------------------------------------------------*/

defined('_JEXEC') or die('Restricted access');

class TableStatistics extends JTable
{
  var $id           = null;
  var $wall_id      = null;
  var $user_id      = null;
  var $posts		= null;
  var $urls			= null;
  var $video_links  = null;
  var $images		= null;
  var $mp3			= null;
  var $files		= null;
  

  function TableStatistics(&$db)
  {
    parent::__construct('#__wallfactory_statistics', 'id', $db);
  }
}
