<?php
/*------------------------------------------------------------------------
com_wallfactory - Wall Factory
------------------------------------------------------------------------
author    TheFactory
copyright Copyright (C) 2010 SKEPSIS Consult SRL. All Rights Reserved.
@license - http://www.gnu.org/licenses/gpl-2.0.html GNU/GPL
Websites: http://www.thefactory.ro
Technical Support:  Forum - http://www.thefactory.ro/joomla-forum/
-------------------------------------------------------------------------*/

defined('_JEXEC') or die('Restricted access');

class TableFollowers extends JTable
{
  var $id           = null;
  var $user_id      = null;
  var $wall_id      = null;
  var $notification = null;
  var $date_created   = null;

  function TableFollowers(&$db)
  {
    parent::__construct('#__wallfactory_followers', 'id', $db);
  }
}
