<?php
/*------------------------------------------------------------------------
com_wallfactory - Wall Factory
------------------------------------------------------------------------
author    TheFactory
copyright Copyright (C) 2010 SKEPSIS Consult SRL. All Rights Reserved.
@license - http://www.gnu.org/licenses/gpl-2.0.html GNU/GPL
Websites: http://www.thefactory.ro
Technical Support:  Forum - http://www.thefactory.ro/joomla-forum/
-------------------------------------------------------------------------*/

defined('_JEXEC') or die('Restricted access');

class TableDashboardSettings extends JTable
{
  var $id      = null;
  var $user_id = null;
  var $setting = null;
  var $value   = null;

  function TableDashboardSettings(&$db)
  {
    parent::__construct('#__wallfactory_dashboard_settings', 'id', $db);
  }
}
