<?php
/*------------------------------------------------------------------------
com_wallfactory - Wall Factory
------------------------------------------------------------------------
author    TheFactory
copyright Copyright (C) 2010 SKEPSIS Consult SRL. All Rights Reserved.
@license - http://www.gnu.org/licenses/gpl-2.0.html GNU/GPL
Websites: http://www.thefactory.ro
Technical Support:  Forum - http://www.thefactory.ro/joomla-forum/
-------------------------------------------------------------------------*/

defined('_JEXEC') or die('Restricted access');

class TableBookmark extends JTable
{
  var $id        = null;
  var $published = null;
  var $title     = null;
  var $link      = null;
  var $extension = null;

  function TableBookmark(&$db)
  {
    parent::__construct('#__wallfactory_bookmarks', 'id', $db);
  }
}
