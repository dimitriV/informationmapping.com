<?php
/*------------------------------------------------------------------------
com_wallfactory - Wall Factory
------------------------------------------------------------------------
author    TheFactory
copyright Copyright (C) 2010 SKEPSIS Consult SRL. All Rights Reserved.
@license - http://www.gnu.org/licenses/gpl-2.0.html GNU/GPL
Websites: http://www.thefactory.ro
Technical Support:  Forum - http://www.thefactory.ro/joomla-forum/
-------------------------------------------------------------------------*/

defined('_JEXEC') or die('Restricted access');

class TableMembers extends JTable
{
  var $id               	= null;
  var $user_id          	= null;
  var $avatar_type      	= null;
  var $avatar_extension 	= null;
  var $name             	= null;
  var $description      	= null;
  var $gender				= null;
  var $birthday				= null;
  var $allow_profile_view	= null;
  var $banned				= null;

  function TableMembers(&$db)
  {
    parent::__construct('#__wallfactory_members', 'id', $db);
  }

  function findOneByUserId($user_id)
  {
    $query = ' SELECT s.*'
           . ' FROM #__wallfactory_members s'
           . ' WHERE s.user_id = ' . $user_id;
    $this->_db->setQuery($query);

    $this->bind($this->_db->loadObject());
  }
  
  function load($oid = null)
  {
	  parent::load($oid);
  }
  
  function getMembers()
  {
    $query = ' SELECT m.id'
           . ' FROM #__wallfactory_members m'
           . ' WHERE m.user_id = ' . $this->id;
    $this->_db->setQuery($query);
    
    return $this->_db->loadObjectList();
  }
  
  function store($updateNulls = false)
  {
	 return parent::store($updateNulls);
  }

  
  function delete($oid = null)
  {
    if ($oid)
    {
      $this->id = $oid;
    }

    $this->deletePosts();
    $this->deleteFollowers();
    $this->deleteUrls();
    $this->deleteMedia();
    $this->deleteStatistics();
       
    $id = $this->findOneByUserId($oid);
    
    return parent::delete($id);
  }
 
  // Getters
  function getPosts()
  {
    $query = ' SELECT p.id'
           . ' FROM #__wallfactory_posts p'
           . ' WHERE p.user_id = ' . $this->id;
    $this->_db->setQuery($query);
    
    return $this->_db->loadObjectList();
  }

  function getFollowers()
  {
    $query = ' SELECT f.id'
           . ' FROM #__wallfactory_followers f'
           . ' WHERE f.user_id = ' . $this->id;
    $this->_db->setQuery($query);
    
    return $this->_db->loadObjectList();
  }
  
  function getUrls()
  {
    $query = ' SELECT l.id'
           . ' FROM #__wallfactory_urls l'
           . ' WHERE l.user_id = ' . $this->id;
    $this->_db->setQuery($query);
    
    return $this->_db->loadObjectList();
  }
  
  function getMedia()
  {
    $query = ' SELECT m.id'
           . ' FROM #__wallfactory_media m'
           . ' WHERE m.user_id = ' . $this->id;
    $this->_db->setQuery($query);
    
    return $this->_db->loadObjectList();
  }
  
  function getStatistics()
  {
    $query = ' SELECT s.id'
           . ' FROM #__wallfactory_statistics s'
           . ' WHERE s.user_id = ' . $this->id;
    $this->_db->setQuery($query);
    
    return $this->_db->loadObjectList();
  }
  
  // Helpers
  function deletePosts()
  {
    $posts      =  $this->getPosts();
    $post_table =& JTable::getInstance('post', 'Table');

    foreach ($posts as $post)
    {
      $post_table->delete($post->id);
    }
  }

  function deleteFollowers()
  {
    $followers      =  $this->getFollowers();
    $follower_table =& JTable::getInstance('followers', 'Table');

    foreach ($followers as $follower)
    {
      $follower_table->delete($follower->id);
    }
  }
  
  function deleteUrls()
  {
    $urls      =  $this->getUrls();
    $urls_table =& JTable::getInstance('urls', 'Table');
    
    foreach ($urls as $url)
    {
      $urls_table->delete($url->id);
    }
  }

  function deleteMedia()
  {
    $media      =  $this->getMedia();
    $media_table =& JTable::getInstance('media', 'Table');
    
    foreach ($media as $m)
    {
      $media_table->delete($m->id);
    }
  }
  
  function deleteStatistics()
  {
    $stats      =  $this->getStatistics();
    $stat_table =& JTable::getInstance('statistics', 'Table');
    
    foreach ($stats as $stat)
    {
      $stat_table->delete($stat->id);
    }
  }
  
}
