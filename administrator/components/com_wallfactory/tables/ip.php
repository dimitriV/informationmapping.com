<?php
/*------------------------------------------------------------------------
com_wallfactory - Wall Factory
------------------------------------------------------------------------
author    TheFactory
copyright Copyright (C) 2010 SKEPSIS Consult SRL. All Rights Reserved.
@license - http://www.gnu.org/licenses/gpl-2.0.html GNU/GPL
Websites: http://www.thefactory.ro
Technical Support:  Forum - http://www.thefactory.ro/joomla-forum/
-------------------------------------------------------------------------*/

defined('_JEXEC') or die('Restricted access');

class TableIp extends JTable
{
	var $id         = null;
	var $user_id    = null;
	var $ip         = null;
	var $created_at = null;
	var $updated_at = null;
	var $visits     = null;

	function TableIp(&$db)
	{
		parent::__construct('#__wallfactory_ips', 'id', $db);
	}

	function update()
	{
	  $ip   =  $this->find();
	  $date =& JFactory::getDate();

	  if (is_null($ip))
	  {
	    $user =& JFactory::getUser();

	    $this->user_id    = $user->id;
	    $this->ip         = $_SERVER['REMOTE_ADDR'];
	    $this->created_at = $date->toMySQL();
	    $this->updated_at = $date->toMySQL();
	    $this->visits     = 1;
	  }
	  else
	  {
	    $this->load($ip->id);

	    $this->updated_at = $date->toMySQL();
	    $this->visits++;
	  }

	  $this->store();
	}

	// Helpers
	function find()
	{
	  $user =& JFactory::getUser();

	  $query = ' SELECT i.id'
	         . ' FROM #__wallfactory_ips i'
	         . ' WHERE i.user_id = ' . $user->id
	         . ' AND i.ip = "' . $_SERVER['REMOTE_ADDR'] . '"';
	  $this->_db->setQuery($query);

	  return $this->_db->loadObject();
	}
}
