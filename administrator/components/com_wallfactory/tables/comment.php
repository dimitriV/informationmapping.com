<?php
/*------------------------------------------------------------------------
com_wallfactory - Wall Factory
------------------------------------------------------------------------
author    TheFactory
copyright Copyright (C) 2010 SKEPSIS Consult SRL. All Rights Reserved.
@license - http://www.gnu.org/licenses/gpl-2.0.html GNU/GPL
Websites: http://www.thefactory.ro
Technical Support:  Forum - http://www.thefactory.ro/joomla-forum/
-------------------------------------------------------------------------*/

defined('_JEXEC') or die('Restricted access');

class TableComment extends JTable
{
  var $id                = null;
  var $post_id           = null;
  var $user_id           = null;
  var $content           = null;
  var $author_name       = null;
  var $author_alias      = null;
  var $approved          = null;
  var $reported          = null;
  var $reporting_user_id = null;
  var $date_created      = null;

  function TableComment(&$db)
  {
    parent::__construct('#__wallfactory_comments', 'id', $db);
  }

  // CRUD
  function delete($oid = null)
  {
    if ($oid)
    {
      $this->id = $oid;
    }

    return parent::delete($oid);
  }

}
