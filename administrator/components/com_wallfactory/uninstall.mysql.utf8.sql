DROP TABLE IF EXISTS `#__wallfactory_bookmarks`;
DROP TABLE IF EXISTS `#__wallfactory_comments`;
DROP TABLE IF EXISTS `#__wallfactory_followers`;
DROP TABLE IF EXISTS `#__wallfactory_ips`;
DROP TABLE IF EXISTS `#__wallfactory_media`;
DROP TABLE IF EXISTS `#__wallfactory_members`;
DROP TABLE IF EXISTS `#__wallfactory_posts`;
DROP TABLE IF EXISTS `#__wallfactory_statistics`;
DROP TABLE IF EXISTS `#__wallfactory_urls`;
DROP TABLE IF EXISTS `#__wallfactory_walls`;