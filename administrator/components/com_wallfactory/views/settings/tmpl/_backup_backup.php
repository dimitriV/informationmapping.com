<?php
/*------------------------------------------------------------------------
com_wallfactory - Wall Factory
------------------------------------------------------------------------
author    TheFactory
copyright Copyright (C) 2010 SKEPSIS Consult SRL. All Rights Reserved.
@license - http://www.gnu.org/licenses/gpl-2.0.html GNU/GPL
Websites: http://www.thefactory.ro
Technical Support:  Forum - http://www.thefactory.ro/joomla-forum/
-------------------------------------------------------------------------*/

defined('_JEXEC') or die('Restricted access'); ?>

<table class="paramlist admintable">
  <tbody>
    <!-- save_settings -->
    <tr>
      <td width="40%" class="paramlist_key hasTip" title="<?php echo JText::_('Include settings'); ?>::<?php echo JText::_('Include settings'); ?>">
        <span class="editlinktip">
          <label for="save_settings"><?php echo JText::_('Include settings'); ?></label>
        </span>
      </td>
      <td class="paramlist_value">
        <select id="save_settings" name="save_settings">
          <option value="0"><?php echo JText::_('No'); ?></option>
          <option value="1" selected><?php echo JText::_('Yes'); ?></option>
        </select>
      </td>
    </tr>

    <!-- save_photo_names -->
    <tr>
      <td width="40%" class="paramlist_key hasTip" title="<?php echo JText::_('Include bookmarks\' logos'); ?>::<?php echo JText::_('Include bookmarks\' logos'); ?>">
        <span class="editlinktip">
          <label for="save_bookmarks_logo"><?php echo JText::_('Include bookmarks\' logos'); ?></label>
        </span>
      </td>
      <td class="paramlist_value">
        <select id="save_bookmarks_logo" name="save_bookmarks_logo">
          <option value="0"><?php echo JText::_('No'); ?></option>
          <option value="1" selected><?php echo JText::_('Yes'); ?></option>
        </select>
      </td>
    </tr>

    <tr>
      <td colspan="2" style="padding-top: 10px;">
        <input type="button" onclick="submitbutton('backup');" value="<?php echo JText::_('Create Backup'); ?>" />
      </td>
    </tr>

    <tr>
      <td colspan="2" style="padding-top: 10px;">
        <h3>Info</h3>
        <?php echo JText::_('To save the users\' avatars and folders, you must manually backup the folder'); ?>
        <b><?php echo JPATH_COMPONENT_SITE.DS.'storage'; ?></b>
        <?php echo JText::_('and restore it after the upgrade.'); ?>
       
      </td>
    </tr>
  </tbody>
</table>
