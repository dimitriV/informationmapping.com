<?php
/*------------------------------------------------------------------------
com_wallfactory - Wall Factory
------------------------------------------------------------------------
author    TheFactory
copyright Copyright (C) 2010 SKEPSIS Consult SRL. All Rights Reserved.
@license - http://www.gnu.org/licenses/gpl-2.0.html GNU/GPL
Websites: http://www.thefactory.ro
Technical Support:  Forum - http://www.thefactory.ro/joomla-forum/
-------------------------------------------------------------------------*/

defined('_JEXEC') or die('Restricted access'); ?>

<form action="index.php" method="post" name="adminForm" style="width: 98%; margin: 0px auto;" enctype="multipart/form-data">

<?php echo $this->pane->startPane('pane'); ?>

  <!-- General -->
  <?php echo $this->pane->startPanel(JText::_('General'), 'general'); ?>
  <table width="100%"><tr>
   <td style="vertical-align: top;">
    <fieldset>
      <legend><?php echo JText::_('Wall media'); ?></legend>
      <?php require_once('_general_wall_media.php'); ?>
    </fieldset>
    </td>
    <td style="vertical-align: top;">
    <fieldset>
      <legend><?php echo JText::_('Miscellaneous'); ?></legend>
      <?php require_once('_general_miscellaneous.php'); ?>
    </fieldset>
    </td>
    </tr>
    <tr>
    <td style="vertical-align: top;">
    <fieldset>
      <legend><?php echo JText::_('Avatars'); ?></legend>
      <?php require_once('_avatars_general.php'); ?>
    </fieldset>
    <fieldset>
      <legend><?php echo JText::_('Community Builder Integration'); ?></legend>
      <?php require_once('_avatars_cb.php'); ?>
    </fieldset>
    </td>
	
    <td style="vertical-align: top;">
    <fieldset>
      <legend><?php echo JText::_('Comments'); ?></legend>
      <?php require_once('_comments.php'); ?>
      <?php require_once('_comments_pagination.php'); ?>
    </fieldset>
    </td>
	
    
    </tr>
   </table>
  <?php echo $this->pane->endPanel(); ?>

  <!-- Allowed bloggers -->
  <?php echo $this->pane->startPanel(JText::_('Allowed bloggers'), 'allowed_bloggers'); ?>
    <fieldset>
      <legend><?php echo JText::_('Allowed bloggers'); ?></legend>
      <?php require_once('_allowed_wallowners.php'); ?>
    </fieldset>
  <?php echo $this->pane->endPanel(); ?>
 
  <?php echo $this->pane->endPanel(); ?>

  <!-- ReCaptcha -->
  <?php echo $this->pane->startPanel(JText::_('ReCaptcha'), 'recaptcha'); ?>
    <fieldset>
      <legend><?php echo JText::_('General'); ?></legend>
      <?php require_once('_recaptcha.php'); ?>
    </fieldset>
  <?php echo $this->pane->endPanel(); ?>

  <!-- Banned words list -->
  <?php echo $this->pane->startPanel(JText::_('Banned words list'), 'banned_words'); ?>
    <fieldset>
      <legend><?php echo JText::_('Banned words list'); ?></legend>
      <?php require_once('_banned_words.php'); ?>
    </fieldset>
  <?php echo $this->pane->endPanel(); ?>

  <!-- Notifications -->
  <?php echo $this->pane->startPanel(JText::_('Notifications'), 'notifications'); ?>
    <fieldset>
      <legend><?php echo JText::_('General'); ?></legend>
      <?php require_once('_notification_general.php'); ?>
    </fieldset>

    <fieldset>
      <legend><?php echo JText::_('New post'); ?></legend>
      <?php require_once('_notification_new_post.php'); ?>
    </fieldset>
    
    <fieldset>
      <legend><?php echo JText::_('New report post'); ?></legend>
      <?php require_once('_notification_new_report_post.php'); ?>
    </fieldset>

    <fieldset>
      <legend><?php echo JText::_('New comment'); ?></legend>
      <?php require_once('_notification_new_comment.php'); ?>
    </fieldset>

    <fieldset>
      <legend><?php echo JText::_('New report comment'); ?></legend>
      <?php require_once('_notification_new_report_comment.php'); ?>
    </fieldset>
  <?php echo $this->pane->endPanel(); ?>

  <!-- Backup and restore -->
  <?php echo $this->pane->startPanel(JText::_('Backup and restore'), 'backup_restore'); ?>
    <fieldset>
      <legend><?php echo JText::_('Backup'); ?></legend>
      <?php require_once('_backup_backup.php'); ?>
    </fieldset>

    <fieldset>
      <legend><?php echo JText::_('Restore'); ?></legend>
      <?php require_once('_backup_restore.php'); ?>
    </fieldset>
  <?php echo $this->pane->endPanel(); ?>

<?php echo $this->pane->endPane(); ?>

  <input type="hidden" name="controller" value="settings" />
  <input type="hidden" name="boxchecked" value="0" />
  <input type="hidden" name="option" value="com_wallfactory" />
  <input type="hidden" name="task" value="" />
</form>
