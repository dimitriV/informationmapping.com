<?php
/*------------------------------------------------------------------------
com_wallfactory - Wall Factory
------------------------------------------------------------------------
author    TheFactory
copyright Copyright (C) 2010 SKEPSIS Consult SRL. All Rights Reserved.
@license - http://www.gnu.org/licenses/gpl-2.0.html GNU/GPL
Websites: http://www.thefactory.ro
Technical Support:  Forum - http://www.thefactory.ro/joomla-forum/
-------------------------------------------------------------------------*/

defined('_JEXEC') or die('Restricted access'); ?>

<table class="paramlist admintable">
  <!-- banned_words -->
  <tr class="hasTip" title="<?php echo JText::_('Banned words'); ?>::<?php echo JText::_('Banned words list.<br /><br />One word per line.'); ?>">
    <td width="40%" class="paramlist_key" style="vertical-align: top;">
      <span class="editlinktip">
        <label for="banned_words"><?php echo JText::_('Banned words'); ?></label>
      </span>
    </td>
    <td class="paramlist_value">
      <textarea id="banned_words" name="banned_words" rows="20" cols="40"><?php foreach ($this->wallSettings->banned_words as $word): ?><?php echo base64_decode($word) . "\n"; ?><?php endforeach; ?></textarea>
    </td>
  </tr>
</table>