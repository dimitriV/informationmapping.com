<?php
/*------------------------------------------------------------------------
com_wallfactory - Wall Factory
------------------------------------------------------------------------
author    TheFactory
copyright Copyright (C) 2010 SKEPSIS Consult SRL. All Rights Reserved.
@license - http://www.gnu.org/licenses/gpl-2.0.html GNU/GPL
Websites: http://www.thefactory.ro
Technical Support:  Forum - http://www.thefactory.ro/joomla-forum/
-------------------------------------------------------------------------*/

defined('_JEXEC') or die('Restricted access'); ?>

<table class="paramlist admintable">
  <!-- allow_guest_comments -->
  <tr class="hasTip" title="<?php echo JText::_('Allow all users'); ?>::<?php echo JText::_('Allow all users'); ?>">
    <td width="40%" class="paramlist_key">
      <span class="editlinktip">
        <label for="allowed_wallowners_all"><?php echo JText::_('Allow all users'); ?></label>
      </span>
    </td>
    <td class="paramlist_value">
      <select id="allowed_wallowners_all" name="allowed_wallowners_all">
        <option value="0" <?php echo (!$this->wallSettings->allowed_wallowners_all) ? 'selected="selected"' : ''; ?>><?php echo JText::_('No'); ?></option>
        <option value="1" <?php echo ($this->wallSettings->allowed_wallowners_all) ? 'selected="selected"' : ''; ?>><?php echo JText::_('Yes'); ?></option>
      </select>
    </td>
  </tr>

  <!-- allowed_wallowners_all_groups -->
  <tr id="user_groups" class="hasTip" title="<?php echo JText::_('Allowed user groups'); ?>::<?php echo JText::_('Allowed user groups'); ?>">
    <td width="40%" class="paramlist_key" style="vertical-align: top;">
      <span class="editlinktip">
        <label for="allowed_wallowners_groups"><?php echo JText::_('Allowed user groups'); ?></label>
      </span>
    </td>
    <td class="paramlist_value">
      <?php echo JHTML::_('select.genericlist', $this->gtree, 'allowed_wallowners_groups[]', 'size="10" multiple', 'value', 'text', $this->wallSettings->allowed_wallowners_groups); ?>
    </td>
  </tr>

  <!-- allowed_wallowners_users -->
  <tr id="user_list" class="hasTip" title="<?php echo JText::_('Allowed users'); ?>::<?php echo JText::_('Allowed users'); ?>">
    <td width="40%" class="paramlist_key" style="vertical-align: top;">
      <span class="editlinktip">
        <label for="allowed_wallowners_users"><?php echo JText::_('Allowed users'); ?></label>
      </span>
    </td>
    <td class="paramlist_value">
      <?php echo JHTML::_('select.genericlist', $this->users, 'allowed_wallowners_users[]', 'size="10" multiple', 'id', 'username', $this->wallSettings->allowed_wallowners_users); ?>
    </td>
  </tr>
</table>

<script>
  	jQueryFactory(document).ready(function ($) {
    $("select").change(function () {

      var rows = "#user_groups, #user_list";

      if ($("#allowed_wallowners_all").val() == 0)
      {
        $(rows).show();
      //  $("#allow_same_category1").attr("disabled",false);
      //  $("#allow_same_category0").attr("disabled",false);
      }
      else
      {
        $(rows).hide();
      //  $("#allow_same_category1").attr("disabled",true);
      //  $("#allow_same_category0").attr("disabled",true);
      }
    });

    $("select").change();
  });
</script>

