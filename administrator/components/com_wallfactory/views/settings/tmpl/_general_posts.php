<?php
/*------------------------------------------------------------------------
com_wallfactory - Wall Factory
------------------------------------------------------------------------
author    TheFactory
copyright Copyright (C) 2010 SKEPSIS Consult SRL. All Rights Reserved.
@license - http://www.gnu.org/licenses/gpl-2.0.html GNU/GPL
Websites: http://www.thefactory.ro
Technical Support:  Forum - http://www.thefactory.ro/joomla-forum/
-------------------------------------------------------------------------*/

defined('_JEXEC') or die('Restricted access'); ?>

<table class="paramlist admintable">
  <!-- allow_post_rating -->
  <tr class="hasTip" title="<?php echo JText::_('Allow post rating'); ?>::<?php echo JText::_('Allow post rating'); ?>">
    <td width="40%" class="paramlist_key">
      <span class="editlinktip">
        <label for="allow_post_rating"><?php echo JText::_('Allow post rating'); ?></label>
      </span>
    </td>
    <td class="paramlist_value">
      <select id="allow_post_rating" name="allow_post_rating">
        <option value="0" <?php echo (!$this->wallSettings->allow_post_rating) ? 'selected="selected"' : ''; ?>><?php echo JText::_('No'); ?></option>
        <option value="1" <?php echo ($this->wallSettings->allow_post_rating) ? 'selected="selected"' : ''; ?>><?php echo JText::_('Yes'); ?></option>
      </select>
    </td>
  </tr>
</table>