<?php
/*------------------------------------------------------------------------
com_wallfactory - Wall Factory
------------------------------------------------------------------------
author    TheFactory
copyright Copyright (C) 2010 SKEPSIS Consult SRL. All Rights Reserved.
@license - http://www.gnu.org/licenses/gpl-2.0.html GNU/GPL
Websites: http://www.thefactory.ro
Technical Support:  Forum - http://www.thefactory.ro/joomla-forum/
-------------------------------------------------------------------------*/

defined('_JEXEC') or die('Restricted access'); ?>

<table class="paramlist admintable">
  <!-- enable_avatars -->
  <tr class="hasTip" title="<?php echo JText::_('Enable avatars'); ?>::<?php echo JText::_('Enable avatars'); ?>">
    <td width="40%" class="paramlist_key">
      <span class="editlinktip">
        <label for="enable_avatars"><?php echo JText::_('Enable avatars'); ?></label>
      </span>
    </td>
    <td class="paramlist_value">
      <select id="enable_avatars" name="enable_avatars">
        <option value="0" <?php echo (!$this->wallSettings->enable_avatars) ? 'selected="selected"' : ''; ?>><?php echo JText::_('No'); ?></option>
        <option value="1" <?php echo ($this->wallSettings->enable_avatars) ? 'selected="selected"' : ''; ?>><?php echo JText::_('Yes'); ?></option>
      </select>
    </td>
  </tr>

  <!-- avatar_max_width -->
  <tr class="hasTip" for="avatar_max_width" title="<?php echo JText::_('Avatar maximum width'); ?>::<?php echo JText::_('Avatar maximum width in pixels'); ?>">
    <td width="40%" class="paramlist_key">
      <span class="editlinktip">
        <label><?php echo JText::_('Avatar maximum width'); ?></label>
      </span>
    </td>
    <td class="paramlist_value">
	    <input type="text" name="avatar_max_width" id="avatar_max_width" value="<?php echo $this->wallSettings->avatar_max_width; ?>"  style="width: 42px; text-align: right;" />
    </td>
  </tr>

  <!-- avatar_max_height -->
  <tr class="hasTip" for="avatar_max_height" title="<?php echo JText::_('Avatar maximum height'); ?>::<?php echo JText::_('Avatar maximum height in pixels'); ?>">
    <td height="40%" class="paramlist_key">
      <span class="editlinktip">
        <label><?php echo JText::_('Avatar maximum height'); ?></label>
      </span>
    </td>
    <td class="paramlist_value">
	    <input type="text" name="avatar_max_height" id="avatar_max_height" value="<?php echo $this->wallSettings->avatar_max_height; ?>"  style="width: 42px; text-align: right;" />
    </td>
  </tr>
  <tr><td colspan="2">&nbsp;</td></tr>

</table>