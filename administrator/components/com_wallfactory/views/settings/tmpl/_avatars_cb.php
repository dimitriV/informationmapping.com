<?php
/*------------------------------------------------------------------------
com_wallfactory - Wall Factory
------------------------------------------------------------------------
author    TheFactory
copyright Copyright (C) 2010 SKEPSIS Consult SRL. All Rights Reserved.
@license - http://www.gnu.org/licenses/gpl-2.0.html GNU/GPL
Websites: http://www.thefactory.ro
Technical Support:  Forum - http://www.thefactory.ro/joomla-forum/
-------------------------------------------------------------------------*/

defined('_JEXEC') or die('Restricted access'); ?>

<?php if (!wallHelper::getIntegration('cb')): ?>
  <?php echo JText::_('You need to install Community Builder to use this option'); ?>
<?php else: ?>
  <table class="paramlist admintable">
    <!-- allow_cb_avatars -->
    <tr class="hasTip" title="<?php echo JText::_('Allow the use of Community Builder avatars'); ?>::<?php echo JText::_('Allow the use of Community Builder avatars'); ?>">
      <td width="40%" class="paramlist_key">
        <span class="editlinktip">
          <label for="allow_cb_avatars"><?php echo JText::_('Allow the use of Community Builder avatars'); ?></label>
        </span>
      </td>
      <td class="paramlist_value">
        <select id="allow_cb_avatars" name="allow_cb_avatars">
          <option value="0" <?php echo (!$this->wallSettings->allow_cb_avatars) ? 'selected="selected"' : ''; ?>><?php echo JText::_('No'); ?></option>
          <option value="1" <?php echo ($this->wallSettings->allow_cb_avatars) ? 'selected="selected"' : ''; ?>><?php echo JText::_('Yes'); ?></option>
        </select>
      </td>
    </tr>
  </table>
<?php endif; ?>