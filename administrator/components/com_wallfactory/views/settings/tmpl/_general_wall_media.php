<?php
/*------------------------------------------------------------------------
com_wallfactory - Wall Factory
------------------------------------------------------------------------
author    TheFactory
copyright Copyright (C) 2010 SKEPSIS Consult SRL. All Rights Reserved.
@license - http://www.gnu.org/licenses/gpl-2.0.html GNU/GPL
Websites: http://www.thefactory.ro
Technical Support:  Forum - http://www.thefactory.ro/joomla-forum/
-------------------------------------------------------------------------*/

defined('_JEXEC') or die('Restricted access'); ?>

<table class="paramlist admintable">
  <!-- allow_post_images -->
  <tr class="hasTip" title="<?php echo JText::_('Allow to post images'); ?>::<?php echo JText::_('Allow to post images'); ?>">
    <td width="40%" class="paramlist_key">
      <span class="editlinktip">
        <label for="allow_post_images"><?php echo JText::_('Allow to post images'); ?></label>
      </span>
    </td>
    <td class="paramlist_value" style="text-align: right;">
      <select id="allow_post_images" name="allow_post_images">
        <option value="0" <?php echo (!$this->wallSettings->allow_post_images) ? 'selected="selected"' : ''; ?>><?php echo JText::_('No'); ?></option>
        <option value="1" <?php echo ($this->wallSettings->allow_post_images) ? 'selected="selected"' : ''; ?>><?php echo JText::_('Yes'); ?></option>
      </select>
    </td>
  </tr>
  
  <!-- images_max_width -->
  <tr class="hasTip" for="image_max_width" title="<?php echo JText::_('Image maximum width'); ?>::<?php echo JText::_('Image maximum width in pixels'); ?>">
    <td width="40%" class="paramlist_key">
      <span class="editlinktip">
        <label><?php echo JText::_('Image maximum width'); ?></label>
      </span>
    </td>
    <td class="paramlist_value">
	    <input type="text" name="image_max_width" id="image_max_width" value="<?php echo $this->wallSettings->image_max_width; ?>"  style="text-align: right; width: 45px;" />
    </td>
  </tr>
  
  <!-- images_max_height -->
  <tr class="hasTip" for="image_max_height" title="<?php echo JText::_('Image maximum height'); ?>::<?php echo JText::_('Image maximum height in pixels'); ?>">
    <td height="40%" class="paramlist_key">
      <span class="editlinktip">
        <label><?php echo JText::_('Image maximum height'); ?></label>
      </span>
    </td>
    <td class="paramlist_value">
	    <input type="text" name="image_max_height" id="image_max_height" value="<?php echo $this->wallSettings->image_max_height; ?>"  style="text-align: right; width: 45px;" />
    </td>
  </tr>

  <!-- allow_post_videos -->
  <tr class="hasTip" title="<?php echo JText::_('Allow to post videos'); ?>::<?php echo JText::_('Allow to post videos'); ?>">
    <td width="40%" class="paramlist_key">
      <span class="editlinktip">
        <label for="allow_post_videos"><?php echo JText::_('Allow to post videos'); ?></label>
      </span>
    </td>
    <td class="paramlist_value" style="text-align: right;">
      <select id="allow_post_videos" name="allow_post_videos">
        <option value="0" <?php echo (!$this->wallSettings->allow_post_videos) ? 'selected="selected"' : ''; ?>><?php echo JText::_('No'); ?></option>
        <option value="1" <?php echo ($this->wallSettings->allow_post_videos) ? 'selected="selected"' : ''; ?>><?php echo JText::_('Yes'); ?></option>
      </select>
    </td>
  </tr>

 <!-- allow_post_mp3files -->
  <tr class="hasTip" title="<?php echo JText::_('Allow to post mp3 files'); ?>::<?php echo JText::_('Allow to post mp3 files'); ?>">
    <td width="40%" class="paramlist_key">
      <span class="editlinktip">
        <label for="allow_post_mp3files"><?php echo JText::_('Allow to post mp3 files'); ?></label>
      </span>
    </td>
    <td class="paramlist_value" style="text-align: right;">
      <select id="allow_post_mp3files" name="allow_post_mp3files">
        <option value="0" <?php echo (!$this->wallSettings->allow_post_mp3files) ? 'selected="selected"' : ''; ?>><?php echo JText::_('No'); ?></option>
        <option value="1" <?php echo ($this->wallSettings->allow_post_mp3files) ? 'selected="selected"' : ''; ?>><?php echo JText::_('Yes'); ?></option>
      </select>
    </td>
  </tr>

  <!-- allow_post_links -->
  <tr class="hasTip" title="<?php echo JText::_('Allow to post URLs'); ?>::<?php echo JText::_('Allow to post URLs'); ?>">
    <td width="40%" class="paramlist_key">
      <span class="editlinktip">
        <label for="allow_post_links"><?php echo JText::_('Allow to post URLs'); ?></label>
      </span>
    </td>
    <td class="paramlist_value" style="text-align: right;">
      <select id="allow_post_links" name="allow_post_links">
        <option value="0" <?php echo (!$this->wallSettings->allow_post_links) ? 'selected="selected"' : ''; ?>><?php echo JText::_('No'); ?></option>
        <option value="1" <?php echo ($this->wallSettings->allow_post_links) ? 'selected="selected"' : ''; ?>><?php echo JText::_('Yes'); ?></option>
      </select>
    </td>
  </tr>
  
  <!-- allow_post_files -->
  <tr class="hasTip" title="<?php echo JText::_('Allow to post files'); ?>::<?php echo JText::_('Allow to post files'); ?>">
    <td width="40%" class="paramlist_key">
      <span class="editlinktip">
        <label for="allow_post_files"><?php echo JText::_('Allow to post files'); ?></label>
      </span>
    </td>
    <td class="paramlist_value" style="text-align: right;">
      <select id="allow_post_files" name="allow_post_files">
        <option value="0" <?php echo (!$this->wallSettings->allow_post_files) ? 'selected="selected"' : ''; ?>><?php echo JText::_('No'); ?></option>
        <option value="1" <?php echo ($this->wallSettings->allow_post_files) ? 'selected="selected"' : ''; ?>><?php echo JText::_('Yes'); ?></option>
      </select>
    </td>
  </tr>
  
  

</table>