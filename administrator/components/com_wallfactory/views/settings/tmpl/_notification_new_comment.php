<?php
/*------------------------------------------------------------------------
com_wallfactory - Wall Factory
------------------------------------------------------------------------
author    TheFactory
copyright Copyright (C) 2010 SKEPSIS Consult SRL. All Rights Reserved.
@license - http://www.gnu.org/licenses/gpl-2.0.html GNU/GPL
Websites: http://www.thefactory.ro
Technical Support:  Forum - http://www.thefactory.ro/joomla-forum/
-------------------------------------------------------------------------*/

defined('_JEXEC') or die('Restricted access'); ?>

<table class="paramlist admintable">
  <!-- enable_notification_new_comment -->
  <tr class="hasTip" title="<?php echo JText::_('Enable new comment notification'); ?>::<?php echo JText::_('Enable new comment notification'); ?>">
    <td width="40%" class="paramlist_key">
      <span class="editlinktip">
        <label for="enable_notification_new_comment"><?php echo JText::_('Enable new comment notification'); ?></label>
      </span>
    </td>
    <td class="paramlist_value">
      <select id="enable_notification_new_comment" name="enable_notification_new_comment">
        <option value="0" <?php echo (!$this->wallSettings->enable_notification_new_comment) ? 'selected="selected"' : ''; ?>><?php echo JText::_('No'); ?></option>
        <option value="1" <?php echo ($this->wallSettings->enable_notification_new_comment) ? 'selected="selected"' : ''; ?>><?php echo JText::_('Yes'); ?></option>
      </select>
    </td>
  </tr>

  <!-- enable_notify_new_comment_followers -->
  <tr class="hasTip" title="<?php echo JText::_('Enable followers notification on new comment'); ?>::<?php echo JText::_('Enable followers notification on new comment'); ?>">
    <td width="40%" class="paramlist_key">
      <span class="editlinktip">
        <label for="enable_notify_new_comment_followers"><?php echo JText::_('Enable followers notification on new comment'); ?></label>
      </span>
    </td>
    <td class="paramlist_value">
      <select id="enable_notify_new_comment_followers" name="enable_notify_new_comment_followers">
        <option value="0" <?php echo (!$this->wallSettings->enable_notify_new_comment_followers) ? 'selected="selected"' : ''; ?>><?php echo JText::_('No'); ?></option>
        <option value="1" <?php echo ($this->wallSettings->enable_notify_new_comment_followers) ? 'selected="selected"' : ''; ?>><?php echo JText::_('Yes'); ?></option>
      </select>
    </td>
  </tr>

  <!-- enable_notify_new_comment_admin -->
  <tr class="hasTip" title="<?php echo JText::_('Enable admin notification on new comment'); ?>::<?php echo JText::_('Enable admin notification on new comment'); ?>">
    <td width="40%" class="paramlist_key">
      <span class="editlinktip">
        <label for="enable_notify_new_comment_admin"><?php echo JText::_('Enable admin notification on new comment'); ?></label>
      </span>
    </td>
    <td class="paramlist_value">
      <select id="enable_notify_new_comment_admin" name="enable_notify_new_comment_admin">
        <option value="0" <?php echo (!$this->wallSettings->enable_notify_new_comment_admin) ? 'selected="selected"' : ''; ?>><?php echo JText::_('No'); ?></option>
        <option value="1" <?php echo ($this->wallSettings->enable_notify_new_comment_admin) ? 'selected="selected"' : ''; ?>><?php echo JText::_('Yes'); ?></option>
      </select>
    </td>
  </tr>
  
  <!-- notification_new_comment_receivers -->
  <!-- <tr class="hasTip" title="<?php //echo JText::_('Notification receivers'); ?>::<?php //echo JText::_('Notification receivers, beside the wall owners'); ?>">
    <td width="40%" class="paramlist_key" style="vertical-align: top;">
      <span class="editlinktip">
        <label for="notification_new_comment_receivers"><?php //echo JText::_('Notification receivers'); ?></label>
      </span>
    </td>
    <td class="paramlist_value">
      <?php //echo JHTML::_('select.genericlist', $this->admins, 'notification_new_comment_receivers[]', 'size="10" multiple', 'id', 'username', $this->wallSettings->notification_new_comment_receivers); ?>
    </td>
  </tr> -->

  <!-- notification_new_comment_subject -->
  <tr class="hasTip" title="<?php echo JText::_('Subject'); ?>::<?php echo JText::_('Email subject'); ?>">
    <td width="40%" class="paramlist_key">
      <span class="editlinktip">
        <label for="notification_new_comment_subject"><?php echo JText::_('Subject'); ?></label>
      </span>
    </td>
    <td class="paramlist_value">
      <input style="width: 200px; " value="<?php echo base64_decode($this->wallSettings->notification_new_comment_subject); ?>" id="notification_new_comment_subject" name="notification_new_comment_subject" />
    </td>
    <td rowspan="2" style="vertical-align: top;">
      <fieldset>
        <legend><?php echo JText::_('Legend'); ?></legend>
        <table>
          <tr>
            <td>%%username%%</td>
            <td>-</td>
            <td><?php echo JText::_('Joomla username'); ?></td>
          </tr>

          <tr>
            <td>%%commenttext%%</td>
            <td>-</td>
            <td><?php echo JText::_('Comment text'); ?></td>
          </tr>

          <tr>
            <td>%%commentlink%%</td>
            <td>-</td>
            <td><?php echo JText::_('Comment link'); ?></td>
          </tr>

          <tr>
            <td>%%walltitle%%</td>
            <td>-</td>
            <td><?php echo JText::_('Wall title'); ?></td>
          </tr>
          <tr>
            <td>%%postlink%%</td>
            <td>-</td>
            <td><?php echo JText::_('Post link'); ?></td>
          </tr>
        </table>
      </fieldset>
    </td>
  </tr>

  <!-- notification_new_comment_message -->
  <tr class="hasTip" title="<?php echo JText::_('Message'); ?>::<?php echo JText::_('Email message'); ?>">
    <td width="40%" class="paramlist_key" style="vertical-align: top;">
      <span class="editlinktip">
        <label for="notification_new_comment_message"><?php echo JText::_('Message'); ?></label>
      </span>
    </td>
    <td class="paramlist_value">
      <textarea id="notification_new_comment_message" name="notification_new_comment_message" rows="10" cols="60"><?php echo base64_decode($this->wallSettings->notification_new_comment_message); ?></textarea>
    </td>
  </tr>
</table>