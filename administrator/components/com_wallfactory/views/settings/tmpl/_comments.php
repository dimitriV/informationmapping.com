<?php
/*------------------------------------------------------------------------
com_wallfactory - Wall Factory
------------------------------------------------------------------------
author    TheFactory
copyright Copyright (C) 2010 SKEPSIS Consult SRL. All Rights Reserved.
@license - http://www.gnu.org/licenses/gpl-2.0.html GNU/GPL
Websites: http://www.thefactory.ro
Technical Support:  Forum - http://www.thefactory.ro/joomla-forum/
-------------------------------------------------------------------------*/

defined('_JEXEC') or die('Restricted access'); ?>

<table class="paramlist admintable">

  <!-- allow_comments -->
  <tr class="hasTip" title="<?php echo JText::_('Allow comments'); ?>::<?php echo JText::_('Allow comments'); ?>">
    <td width="40%" class="paramlist_key">
      <span class="editlinktip">
        <label for="allow_comments"><?php echo JText::_('Allow comments'); ?></label>
      </span>
    </td>
    <td class="paramlist_value" style="text-align: right;">
      <select id="allow_comments" name="allow_comments">
        <option value="0" <?php echo (!$this->wallSettings->allow_comments) ? 'selected="selected"' : ''; ?>><?php echo JText::_('No'); ?></option>
        <option value="1" <?php echo ($this->wallSettings->allow_comments) ? 'selected="selected"' : ''; ?>><?php echo JText::_('Yes'); ?></option>
      </select>
    </td>
  </tr>
  
  <!-- allow_guest_comments -->
  <tr class="hasTip" title="<?php echo JText::_('Allow guest comments'); ?>::<?php echo JText::_('Allow guest comments'); ?>">
    <td width="40%" class="paramlist_key">
      <span class="editlinktip">
        <label for="allow_guest_comments"><?php echo JText::_('Allow guest comments'); ?></label>
      </span>
    </td>
    <td class="paramlist_value" style="text-align: right;">
      <select id="allow_guest_comments" name="allow_guest_comments">
        <option value="0" <?php echo (!$this->wallSettings->allow_guest_comments) ? 'selected="selected"' : ''; ?>><?php echo JText::_('No'); ?></option>
        <option value="1" <?php echo ($this->wallSettings->allow_guest_comments) ? 'selected="selected"' : ''; ?>><?php echo JText::_('Yes'); ?></option>
      </select>
    </td>
  </tr>


  <!-- captcha_comment -->
  <tr class="hasTip" title="<?php echo JText::_('Use ReCaptcha for guest comments'); ?>::<?php echo JText::_('Use ReCaptcha for guest comments'); ?>">
    <td width="40%" class="paramlist_key">
      <span class="editlinktip">
        <label for="captcha_comment"><?php echo JText::_('Use ReCaptcha for guest comments'); ?></label>
      </span>
    </td>
    <td class="paramlist_value" style="text-align: right;">
      <select id="captcha_comment" name="captcha_comment">
        <option value="0" <?php echo (!$this->wallSettings->captcha_comment) ? 'selected="selected"' : ''; ?>><?php echo JText::_('No'); ?></option>
        <option value="1" <?php echo ($this->wallSettings->captcha_comment) ? 'selected="selected"' : ''; ?>><?php echo JText::_('Yes'); ?></option>
      </select>
    </td>
  </tr>


</table>