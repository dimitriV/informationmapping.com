<?php
/*------------------------------------------------------------------------
com_wallfactory - Wall Factory
------------------------------------------------------------------------
author    TheFactory
copyright Copyright (C) 2010 SKEPSIS Consult SRL. All Rights Reserved.
@license - http://www.gnu.org/licenses/gpl-2.0.html GNU/GPL
Websites: http://www.thefactory.ro
Technical Support:  Forum - http://www.thefactory.ro/joomla-forum/
-------------------------------------------------------------------------*/

defined('_JEXEC') or die('Restricted access'); ?>

<table class="paramlist admintable">
  <!-- allow_pingbacks -->
  <tr class="hasTip" title="<?php echo JText::_('Allow pingbacks'); ?>::<?php echo JText::_('Allow pingbacks'); ?>">
    <td width="40%" class="paramlist_key">
      <span class="editlinktip">
        <label for="allow_pingbacks"><?php echo JText::_('Allow pingbacks'); ?></label>
      </span>
    </td>
    <td class="paramlist_value">
      <select id="allow_pingbacks" name="allow_pingbacks">
        <option value="0" <?php echo (!$this->wallSettings->allow_pingbacks) ? 'selected="selected"' : ''; ?>><?php echo JText::_('No'); ?></option>
        <option value="1" <?php echo ($this->wallSettings->allow_pingbacks) ? 'selected="selected"' : ''; ?>><?php echo JText::_('Yes'); ?></option>
      </select>
    </td>
  </tr>

  <!-- allow_update_services -->
  <tr class="hasTip" title="<?php echo JText::_('Allow update services'); ?>::<?php echo JText::_('Allow update services'); ?>">
    <td width="40%" class="paramlist_key">
      <span class="editlinktip">
        <label for="allow_update_services"><?php echo JText::_('Allow update services'); ?></label>
      </span>
    </td>
    <td class="paramlist_value">
      <select id="allow_update_services" name="allow_update_services">
        <option value="0" <?php echo (!$this->wallSettings->allow_update_services) ? 'selected="selected"' : ''; ?>><?php echo JText::_('No'); ?></option>
        <option value="1" <?php echo ($this->wallSettings->allow_update_services) ? 'selected="selected"' : ''; ?>><?php echo JText::_('Yes'); ?></option>
      </select>
    </td>
  </tr>

  <!-- list_update_services -->
  <tr class="hasTip" title="<?php echo JText::_('Update services'); ?>::<?php echo JText::_('Update services.<br /><br />One per line.'); ?>">
    <td width="40%" class="paramlist_key" style="vertical-align: top;">
      <span class="editlinktip">
        <label for="list_update_services"><?php echo JText::_('Update services'); ?></label>
      </span>
    </td>
    <td class="paramlist_value">
      <textarea id="list_update_services" name="list_update_services" rows="5" cols="40"><?php foreach ($this->wallSettings->list_update_services as $service): ?><?php echo base64_decode($service) . "\n"; ?><?php endforeach; ?></textarea>
    </td>
  </tr>
</table>