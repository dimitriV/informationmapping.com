<?php
/*------------------------------------------------------------------------
com_wallfactory - Wall Factory
------------------------------------------------------------------------
author    TheFactory
copyright Copyright (C) 2010 SKEPSIS Consult SRL. All Rights Reserved.
@license - http://www.gnu.org/licenses/gpl-2.0.html GNU/GPL
Websites: http://www.thefactory.ro
Technical Support:  Forum - http://www.thefactory.ro/joomla-forum/
-------------------------------------------------------------------------*/

defined('_JEXEC') or die('Restricted access'); ?>

<p><?php echo JText::_('Wall Factory uses the'); ?> <a href="http://recaptcha.net/">ReCapthca</a> <?php echo JText::_('anti-bot service.'); ?></p>
<p>
  <?php echo JText::_('In order to enable it on your site, you must register for an'); ?> <a href="https://admin.recaptcha.net/accounts/signup/?next=%2Frecaptcha%2Fcreatesite%2F" target="_blank"><?php echo JText::_('account'); ?></a>
  <?php echo JText::_('and then fill in the required information'); ?>.
</p>

<table class="paramlist admintable">
  <!-- recaptcha_public_key -->
  <tr class="hasTip" title="<?php echo JText::_('ReCaptcha public key'); ?>::<?php echo JText::_('ReCaptcha public key'); ?>">
    <td width="40%" class="paramlist_key">
      <span class="editlinktip">
        <label for="recaptcha_public_key"><?php echo JText::_('ReCaptcha public key'); ?></label>
      </span>
    </td>
    <td class="paramlist_value">
	    <input type="text" name="recaptcha_public_key" id="recaptcha_public_key" value="<?php echo base64_decode($this->wallSettings->recaptcha_public_key); ?>" />
    </td>
  </tr>

  <!-- recaptcha_private_key -->
  <tr class="hasTip" title="<?php echo JText::_('ReCaptcha private key'); ?>::<?php echo JText::_('ReCaptcha private key'); ?>">
    <td width="40%" class="paramlist_key">
      <span class="editlinktip">
        <label for="recaptcha_private_key"><?php echo JText::_('ReCaptcha private key'); ?></label>
      </span>
    </td>
    <td class="paramlist_value">
	    <input type="text" name="recaptcha_private_key" id="recaptcha_private_key" value="<?php echo base64_decode($this->wallSettings->recaptcha_private_key); ?>" />
    </td>
  </tr>
</table>