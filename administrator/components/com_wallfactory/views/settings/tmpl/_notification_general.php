<?php
/*------------------------------------------------------------------------
com_wallfactory - Wall Factory
------------------------------------------------------------------------
author    TheFactory
copyright Copyright (C) 2010 SKEPSIS Consult SRL. All Rights Reserved.
@license - http://www.gnu.org/licenses/gpl-2.0.html GNU/GPL
Websites: http://www.thefactory.ro
Technical Support:  Forum - http://www.thefactory.ro/joomla-forum/
-------------------------------------------------------------------------*/

defined('_JEXEC') or die('Restricted access'); ?>

<table class="paramlist admintable">
  <!-- enable_notification_email_html -->
  <tr class="hasTip" title="<?php echo JText::_('Send HTML emails'); ?>::<?php echo JText::_('Send HTML emails'); ?>">
    <td width="40%" class="paramlist_key">
      <span class="editlinktip">
        <label for="enable_notification_email_html"><?php echo JText::_('Send HTML emails'); ?></label>
      </span>
    </td>
    <td class="paramlist_value">
      <select id="enable_notification_email_html" name="enable_notification_email_html">
        <option value="0" <?php echo (!$this->wallSettings->enable_notification_email_html) ? 'selected="selected"' : ''; ?>><?php echo JText::_('No'); ?></option>
        <option value="1" <?php echo ($this->wallSettings->enable_notification_email_html) ? 'selected="selected"' : ''; ?>><?php echo JText::_('Yes'); ?></option>
      </select>
    </td>
  </tr>
</table>