<?php
/*------------------------------------------------------------------------
com_wallfactory - Wall Factory
------------------------------------------------------------------------
author    TheFactory
copyright Copyright (C) 2010 SKEPSIS Consult SRL. All Rights Reserved.
@license - http://www.gnu.org/licenses/gpl-2.0.html GNU/GPL
Websites: http://www.thefactory.ro
Technical Support:  Forum - http://www.thefactory.ro/joomla-forum/
-------------------------------------------------------------------------*/

defined('_JEXEC') or die('Restricted access'); ?>

<table class="paramlist admintable">
  <!-- user_groups -->
  <?php foreach ($this->gtree as $group): ?>
    <tr class="hasTip" title="<?php echo trim(str_replace(array('-&nbsp;', '.', '&nbsp;'), ' ', $group->text)); ?>::<?php echo trim(str_replace(array('-', '.', '&nbsp;'), ' ', $group->text)); ?> <?php echo JText::_('folder quote.<br /><br />Use -1 for unlimited.'); ?>">
      <td width="40%" class="paramlist_key">
        <span class="editlinktip">
          <label for="allowed_bloggers_all"><?php echo trim(str_replace(array('-&nbsp;', '.', '&nbsp;'), ' ', $group->text)); ?></label>
        </span>
      </td>
      <td class="paramlist_value">
        <input value="<?php echo number_format(isset($this->wallSettings->user_group_folder_quote[$group->value]) ? $this->wallSettings->user_group_folder_quote[$group->value] / 1024 : 0, 2); ?>" id="user_group_folder_quote_<?php echo $group->value; ?>" name="user_group_folder_quote[<?php echo $group->value; ?>]" style="text-align: right;" /> Mb
      </td>
    </tr>
  <?php endforeach; ?>
</table>