<?php
/*------------------------------------------------------------------------
com_wallfactory - Wall Factory
------------------------------------------------------------------------
author    TheFactory
copyright Copyright (C) 2010 SKEPSIS Consult SRL. All Rights Reserved.
@license - http://www.gnu.org/licenses/gpl-2.0.html GNU/GPL
Websites: http://www.thefactory.ro
Technical Support:  Forum - http://www.thefactory.ro/joomla-forum/
-------------------------------------------------------------------------*/

defined('_JEXEC') or die('Restricted access'); ?>

<table class="paramlist admintable">
 <!-- comments_per_page -->
  <tr class="hasTip" title="<?php echo JText::_('Visible Comments per post'); ?>::<?php echo JText::_('Number of comments shown per post'); ?>">
    <td width="40%" class="paramlist_key">
      <span class="editlinktip">
        <label for="comments_per_page"><?php echo JText::_('Comments per post'); ?></label>
      </span>
    </td>
    <td class="paramlist_value">
      <input type="text" name="comments_per_page" id="comments_per_page" value="<?php echo $this->wallSettings->comments_per_page; ?>" style="width: 42px; text-align: right;" />
    </td>
  </tr>
  
</table>