<?php
/*------------------------------------------------------------------------
com_wallfactory - Wall Factory
------------------------------------------------------------------------
author    TheFactory
copyright Copyright (C) 2010 SKEPSIS Consult SRL. All Rights Reserved.
@license - http://www.gnu.org/licenses/gpl-2.0.html GNU/GPL
Websites: http://www.thefactory.ro
Technical Support:  Forum - http://www.thefactory.ro/joomla-forum/
-------------------------------------------------------------------------*/

defined('_JEXEC') or die('Restricted access'); ?>

<table class="paramlist admintable">
  <!-- enable_user_folder -->
  <tr class="hasTip" title="<?php echo JText::_('Enable User Folder'); ?>::<?php echo JText::_('Enable User Folder'); ?>">
    <td width="40%" class="paramlist_key">
      <span class="editlinktip">
        <label for="enable_user_folder"><?php echo JText::_('Enable User Folder'); ?></label>
      </span>
    </td>
    <td class="paramlist_value">
      <select id="enable_user_folder" name="enable_user_folder">
        <option value="0" <?php echo (!$this->wallSettings->enable_user_folder) ? 'selected="selected"' : ''; ?>><?php echo JText::_('No'); ?></option>
        <option value="1" <?php echo ($this->wallSettings->enable_user_folder) ? 'selected="selected"' : ''; ?>><?php echo JText::_('Yes'); ?></option>
      </select>
    </td>
  </tr>

  <!-- user_folder_quote -->
  <tr class="hasTip" title="<?php echo JText::_('User Folder quote'); ?>::<?php echo JText::_('User Folder quote'); ?>">
    <td width="40%" class="paramlist_key">
      <span class="editlinktip">
        <label for="user_folder_quote"><?php echo JText::_('User Folder quote'); ?></label>
      </span>
    </td>
    <td class="paramlist_value">
      <select id="user_folder_quote" name="user_folder_quote">
        <option value="0" <?php echo (!$this->wallSettings->user_folder_quote) ? 'selected="selected"' : ''; ?>><?php echo JText::_('Unlimited'); ?></option>
        <option value="1" <?php echo ($this->wallSettings->user_folder_quote) ? 'selected="selected"' : ''; ?>><?php echo JText::_('Per user groups'); ?></option>
      </select>
    </td>
  </tr>
</table>