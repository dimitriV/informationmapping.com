<?php
/*------------------------------------------------------------------------
com_wallfactory - Wall Factory
------------------------------------------------------------------------
author    TheFactory
copyright Copyright (C) 2010 SKEPSIS Consult SRL. All Rights Reserved.
@license - http://www.gnu.org/licenses/gpl-2.0.html GNU/GPL
Websites: http://www.thefactory.ro
Technical Support:  Forum - http://www.thefactory.ro/joomla-forum/
-------------------------------------------------------------------------*/


defined('_JEXEC') or die('Restricted access'); ?>

<table class="paramlist admintable">
  <tbody>
    <!-- backup_file -->
    <tr>
      <td width="40%" class="paramlist_key hasTip" title="<?php echo JText::_('Backup file'); ?>::<?php echo JText::_('Backup file'); ?>">
        <span class="editlinktip">
          <label for="backup_file"><?php echo JText::_('Backup file'); ?></label>
        </span>
      </td>
      <td class="paramlist_value"><input type="file" id="backup_file" name="backup_file" /></td>
    </tr>

    <tr>
      <td colspan="2">
        <h1 style="color: #ff0000;"><?php echo JText::_('Warning!'); ?></h1>
        <?php echo JText::_('Restoring a backup file, will <b>erase</b> all your existing data!'); ?>
      </td>
    </tr>

    <!-- append_data -->
    <tr>
      <td width="40%" class="paramlist_key hasTip" title="<?php echo JText::_('Append data'); ?>::<?php echo JText::_('Append data'); ?>">
        <span class="editlinktip">
          <label for="append_data"><?php echo JText::_('Append data'); ?></label>
        </span>
      </td>
      <td class="paramlist_value">
        <select id="append_data" name="append_data">
          <option value="0" selected><?php echo JText::_('No'); ?></option>
          <option value="1"><?php echo JText::_('Yes'); ?></option>
        </select>
        <span class="error hasTip" title="<?php echo JText::_('Warning');?>::<?php echo JText::_('If you select No, all existing data will be <b>ERASED</b> and replaced with the backup file contents!<br /><br />If you select Yes, the existing data will not be erased and the backup file contents will be appended to the existing data. Duplicates may appear!'); ?>">
          <img src="<?php echo JURI::root(); ?>/includes/js/ThemeOffice/warning.png" />
        </span>
        <br />
        <?php echo JText::_('If you select No, all existing data will be <b>ERASED</b> and replaced with the backup file contents!<br />If you select Yes, the existing data will not be erased and the backup file contents will be appended to the existing data. Duplicates may appear!'); ?>
      </td>
    </tr>

    <tr>
      <td colspan="2" style="padding-top: 10px;">
        <input type="button" onclick="submitbutton('restore');" value="<?php echo JText::_('Restore Backup'); ?>" />
      </td>
    </tr>
  </tbody>
</table>
