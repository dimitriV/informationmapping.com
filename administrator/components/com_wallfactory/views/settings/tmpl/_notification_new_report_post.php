<?php
/*------------------------------------------------------------------------
com_wallfactory - Wall Factory
------------------------------------------------------------------------
author    TheFactory
copyright Copyright (C) 2010 SKEPSIS Consult SRL. All Rights Reserved.
@license - http://www.gnu.org/licenses/gpl-2.0.html GNU/GPL
Websites: http://www.thefactory.ro
Technical Support:  Forum - http://www.thefactory.ro/joomla-forum/
-------------------------------------------------------------------------*/

defined('_JEXEC') or die('Restricted access'); ?>

<table class="paramlist admintable">
  <!-- enable_notification_new_report_post -->
  <tr class="hasTip" title="<?php echo JText::_('Enable new report post notification'); ?>::<?php echo JText::_('Enable new report post notification'); ?>">
    <td width="40%" class="paramlist_key">
      <span class="editlinktip">
        <label for="enable_notification_new_report_post"><?php echo JText::_('Enable new report post notification'); ?></label>
      </span>
    </td>
    <td class="paramlist_value">
      <select id="enable_notification_new_report_post" name="enable_notification_new_report_post">
        <option value="0" <?php echo (!$this->wallSettings->enable_notification_new_report_post) ? 'selected="selected"' : ''; ?>><?php echo JText::_('No'); ?></option>
        <option value="1" <?php echo ($this->wallSettings->enable_notification_new_report_post) ? 'selected="selected"' : ''; ?>><?php echo JText::_('Yes'); ?></option>
      </select>
    </td>
  </tr>

  <!-- notification_new_report_post_receivers -->
  <!--<tr class="hasTip" title="<?php //echo JText::_('Notification receivers'); ?>::<?php //echo JText::_('Notification receivers, beside the wall owner'); ?>">
    <td width="40%" class="paramlist_key" style="vertical-align: top;">
      <span class="editlinktip">
        <label for="notification_new_report_post_receivers"><?php //echo JText::_('Notification receivers'); ?></label>
      </span>
    </td>
    <td class="paramlist_value">
      <?php //echo JHTML::_('select.genericlist', $this->admins, 'notification_new_report_post_receivers[]', 'size="10" multiple', 'id', 'username', $this->wallSettings->notification_new_report_post_receivers); ?>
    </td>
  </tr>-->

  <!-- notification_new_report_post_subject -->
  <tr class="hasTip" title="<?php echo JText::_('Subject'); ?>::<?php echo JText::_('Email subject'); ?>">
    <td width="40%" class="paramlist_key">
      <span class="editlinktip">
        <label for="notification_new_report_post_subject"><?php echo JText::_('Subject'); ?></label>
      </span>
    </td>
    <td class="paramlist_value">
      <input style="width: 200px; " value="<?php echo base64_decode($this->wallSettings->notification_new_report_post_subject); ?>" id="notification_new_report_post_subject" name="notification_new_report_post_subject" />
    </td>
    <td rowspan="2" style="vertical-align: top;">
      <fieldset>
        <legend><?php echo JText::_('Legend'); ?></legend>
        <table>
          <tr>
            <td>%%username%%</td>
            <td>-</td>
            <td><?php echo JText::_('Joomla username'); ?></td>
          </tr>

          <tr>
            <td>%%posttext%%</td>
            <td>-</td>
            <td><?php echo JText::_('Post text'); ?></td>
          </tr>

          <tr>
            <td>%%postlink%%</td>
            <td>-</td>
            <td><?php echo JText::_('Post link'); ?></td>
          </tr>

          <tr>
            <td>%%walltitle%%</td>
            <td>-</td>
            <td><?php echo JText::_('Wall title'); ?></td>
          </tr>
          <tr>
            <td>%%postlink%%</td>
            <td>-</td>
            <td><?php echo JText::_('Post link'); ?></td>
          </tr>
        </table>
      </fieldset>
    </td>
  </tr>

  <!-- notification_new_report_post_message -->
  <tr class="hasTip" title="<?php echo JText::_('Message'); ?>::<?php echo JText::_('Email message'); ?>">
    <td width="40%" class="paramlist_key" style="vertical-align: top;">
      <span class="editlinktip">
        <label for="notification_new_report_post_message"><?php echo JText::_('Message'); ?></label>
      </span>
    </td>
    <td class="paramlist_value">
      <textarea id="notification_new_report_post_message" name="notification_new_report_post_message" rows="10" cols="60"><?php echo base64_decode($this->wallSettings->notification_new_report_post_message); ?></textarea>
    </td>
  </tr>
</table>