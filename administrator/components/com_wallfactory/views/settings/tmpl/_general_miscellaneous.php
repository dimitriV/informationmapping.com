<?php
/*------------------------------------------------------------------------
com_wallfactory - Wall Factory
------------------------------------------------------------------------
author    TheFactory
copyright Copyright (C) 2010 SKEPSIS Consult SRL. All Rights Reserved.
@license - http://www.gnu.org/licenses/gpl-2.0.html GNU/GPL
Websites: http://www.thefactory.ro
Technical Support:  Forum - http://www.thefactory.ro/joomla-forum/
-------------------------------------------------------------------------*/

defined('_JEXEC') or die('Restricted access'); ?>

<table class="paramlist admintable">
  <!-- enable_guest_view -->
  <tr class="hasTip" title="<?php echo JText::_('Enable guest view'); ?>::<?php echo JText::_('Enable guest to view wall posts.'); ?>">
    <td width="50%" class="paramlist_key">
      <span class="editlinktip">
        <label for="enable_guest_view"><?php echo JText::_('Enable guest view'); ?></label>
      </span>
    </td>
    <td class="paramlist_value">
      <select id="enable_guest_view" name="enable_guest_view">
        <option value="0" <?php echo (!$this->wallSettings->enable_guest_view) ? 'selected="selected"' : ''; ?>><?php echo JText::_('No'); ?></option>
        <option value="1" <?php echo ($this->wallSettings->enable_guest_view) ? 'selected="selected"' : ''; ?>><?php echo JText::_('Yes'); ?></option>
      </select>
    </td>
  </tr>
  
  <!-- enable_guest_write -->
  <tr class="hasTip" title="<?php echo JText::_('Enable guest write'); ?>::<?php echo JText::_('Enable guest to post on the wall.'); ?>">
    <td width="50%" class="paramlist_key">
      <span class="editlinktip">
        <label for="enable_guest_write"><?php echo JText::_('Enable guest write'); ?></label>
      </span>
    </td>
    <td class="paramlist_value">
      <select id="enable_guest_write" name="enable_guest_write">
        <option value="0" <?php echo (!$this->wallSettings->enable_guest_write) ? 'selected="selected"' : ''; ?>><?php echo JText::_('No'); ?></option>
        <option value="1" <?php echo ($this->wallSettings->enable_guest_write) ? 'selected="selected"' : ''; ?>><?php echo JText::_('Yes'); ?></option>
      </select>
    </td>
  </tr>

  <!-- enable_bookmarks -->
  <tr class="hasTip" title="<?php echo JText::_('Enable bookmarks'); ?>::<?php echo JText::_('Enable bookmarks'); ?>">
    <td width="50%" class="paramlist_key">
      <span class="editlinktip">
        <label for="enable_bookmarks"><?php echo JText::_('Enable bookmarks'); ?></label>
      </span>
    </td>
    <td class="paramlist_value">
      <select id="enable_bookmarks" name="enable_bookmarks">
        <option value="0" <?php echo (!$this->wallSettings->enable_bookmarks) ? 'selected="selected"' : ''; ?>><?php echo JText::_('No'); ?></option>
        <option value="1" <?php echo ($this->wallSettings->enable_bookmarks) ? 'selected="selected"' : ''; ?>><?php echo JText::_('Yes'); ?></option>
      </select>
    </td>
  </tr>

  <!-- enable_gravatar -->
  <tr class="hasTip" title="<?php echo JText::_('Enable Gravatar'); ?>::<?php echo JText::_('Enable Gravatar support for comments'); ?>">
    <td width="50%" class="paramlist_key">
      <span class="editlinktip">
        <label for="enable_gravatar"><?php echo JText::_('Enable Gravatar'); ?></label>
      </span>
    </td>
    <td class="paramlist_value">
      <select id="enable_gravatar" name="enable_gravatar">
        <option value="0" <?php echo (!$this->wallSettings->enable_gravatar) ? 'selected="selected"' : ''; ?>><?php echo JText::_('No'); ?></option>
        <option value="1" <?php echo ($this->wallSettings->enable_gravatar) ? 'selected="selected"' : ''; ?>><?php echo JText::_('Yes'); ?></option>
      </select>
    </td>
  </tr>

  <!-- enable_router_plugin -->
  <tr>
    <td width="50%" class="paramlist_key">
      <span class="editlinktip">
        <label for="enable_router_plugin"><?php echo JText::_('Enable Router Plugin'); ?></label>
      </span>
    </td>
    <td class="paramlist_value">
      <select id="enable_router_plugin" name="enable_router_plugin">
        <option value="0" <?php echo (!$this->router) ? 'selected="selected"' : ''; ?>><?php echo JText::_('No'); ?></option>
        <option value="1" <?php echo ($this->router) ? 'selected="selected"' : ''; ?>><?php echo JText::_('Yes'); ?></option>
      </select>
      <?php echo JHTML::tooltip(JText::sprintf('ROUTER_PLUGIN_INFO', JURI::root(), JURI::root()), JText::_('Router Plugin'), '../../../components/com_wallfactory/assets/images/help.png'); ?>
    </td>
  </tr>
  
  <!-- wall_title_display -->
  <tr>
    <td width="50%" class="paramlist_key">
      <span class="editlinktip">
        <label for="wall_title_display"><?php echo JText::_('User title display on wall'); ?></label>
      </span>
    </td>
    <td class="paramlist_value">
      <select id="wall_title_display" name="wall_title_display">
        <option value="0" <?php echo (!$this->wallSettings->wall_title_display) ? 'selected="selected"' : ''; ?>><?php echo JText::_('Username'); ?></option>
        <option value="1" <?php echo ($this->wallSettings->wall_title_display) ? 'selected="selected"' : ''; ?>><?php echo JText::_('Alias'); ?></option>
      </select>
      <?php echo JHTML::tooltip(JText::sprintf('WALL_TITLE_DISPLAY', JURI::root(), JURI::root()), JText::_('Wall Title Display'), '../../../components/com_wallfactory/assets/images/help.png'); ?>
    </td>
  </tr>
  
  <tr><td colspan="2">&nbsp;</td></tr>
  <tr><td colspan="2">&nbsp;</td></tr>
</table>
<script>
jQueryFactory(document).ready( function ($) {
    $("#enable_guest_view").change(function () {
	  if ( $('#enable_guest_view').val() == 0 ) {
		$("#enable_guest_write").attr('disabled', 'disabled');
      }
      else
      {
        $("#enable_guest_write").removeAttr('disabled');
      }
    });

    $("#enable_guest_view").change();
  });
</script>
