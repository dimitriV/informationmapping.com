<?php
/*------------------------------------------------------------------------
com_wallfactory - Wall Factory
------------------------------------------------------------------------
author    TheFactory
copyright Copyright (C) 2010 SKEPSIS Consult SRL. All Rights Reserved.
@license - http://www.gnu.org/licenses/gpl-2.0.html GNU/GPL
Websites: http://www.thefactory.ro
Technical Support:  Forum - http://www.thefactory.ro/joomla-forum/
-------------------------------------------------------------------------*/

defined('_JEXEC') or die('Restricted access');

jimport('joomla.application.component.view');
jimport('joomla.html.pane');

class BackendViewSettings extends JView
{
  function display($tpl = null)
  {
    JToolBarHelper::title(JText::_('Settings'), 'generic.png');
    JToolBarHelper::save();

    $acl    =& JFactory::getACL();
    $gtree  =  $acl->get_group_children_tree(null, 'USERS', false);
    $pane   =& JPane::getInstance('tabs', array('startOffset' => 0));

    $wall   = &JModel::getInstance('wall', 'BackendModel');
    $users  = $wall->getUsers();

    $settings =  new wallSettings();

    $router =& $this->get('RouterPlugin');

    $this->assignRef('pane',         $pane);
    $this->assignRef('gtree',        $gtree);
    $this->assignRef('users',        $users);
    $this->assignRef('admins',       $admins);
    $this->assignRef('wallSettings', $settings);
    $this->assignRef('router',       $router);

    JHTML::_('behavior.tooltip');
	JHtmlFactory::jQueryScript();
	
    parent::display($tpl);
  }
}