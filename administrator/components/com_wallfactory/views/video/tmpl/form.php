<?php
/*------------------------------------------------------------------------
com_wallfactory - Wall Factory
------------------------------------------------------------------------
author    TheFactory
copyright Copyright (C) 2010 SKEPSIS Consult SRL. All Rights Reserved.
@license - http://www.gnu.org/licenses/gpl-2.0.html GNU/GPL
Websites: http://www.thefactory.ro
Technical Support:  Forum - http://www.thefactory.ro/joomla-forum/
-------------------------------------------------------------------------*/

defined('_JEXEC') or die('Restricted access'); ?>

<form action="index.php" method="post" name="adminForm" id="adminForm">

<table class="admintable" width="100%">
		<tr valign="top">
			<td> 
				<fieldset>
					<legend><?php echo JText::_('Video'); ?></legend>

					<table width="100%" cellpadding="0" cellspacing="0">
						<?php if ($this->video->id): ?>
						  <tr>
							  <td class="key" width="10%" align="right"><label for="id"><?php echo JText::_('ID'); ?>:</label></td>
							  <td width="80%"><strong><?php echo $this->video->id; ?></strong></td>
						  </tr>
						<?php endif; ?>

						<!-- video_link -->
						<tr>
							<td class="key"><label for="video_link"><?php echo JText::_('Video link'); ?>:</label></td>
							<td>
							  <input name="video_link" id="video_link" value="<?php echo $this->video->video_link; ?>" style="width: 400px;" />
							</td>
						</tr>

						<!-- title -->
						<tr>
							<td class="key"><label for="video_title"><?php echo JText::_('Video title'); ?>:</label></td>
							<td>
							  <input name="video_title" id="video_title" value="<?php echo $this->video->video_title; ?>" style="width: 400px;" />
							</td>
						</tr>
						
						<!-- description -->
						<tr>
							<td class="key"><label for="video_description"><?php echo JText::_('Video description'); ?>:</label></td>
							<td>
							  <textarea name="video_description" id="video_description" rows="8" cols="54" class="mceEditor"><?php echo strip_tags(html_entity_decode($this->video->video_description)); ?></textarea>
							  
							</td>
						</tr>
						
						<!-- thumbnail -->
						<tr>
							<td class="key"><label for="video_thumbnail"><?php echo JText::_('Video thumbnail'); ?>:</label></td>
							<td>
							  <input name="video_thumbnail" id="video_thumbnail" value="<?php echo $this->video->video_thumbnail; ?>" style="width: 400px;" />
							</td>
						</tr>
						<!-- alternative thumbnail -->
						<tr>
							<td class="key"><label for="video_sourceThumb"><?php echo JText::_('Video thumbnail2'); ?>:</label></td>
							<td>
							  <input name="video_sourceThumb" id="video_sourceThumb" value="<?php echo $this->video->video_sourceThumb; ?>" style="width: 400px;" />
							</td>
						</tr>
						
						<!-- wall_id -->
						<tr>
							<td class="key"><label for="wall_id"><?php echo JText::_('Wall'); ?>:</label></td>
							<td>
							  <select name="wall_id" id="wall_id">
							    <?php foreach ($this->walls as $wall): ?>
							      <option value="<?php echo $wall->id; ?>" <?php echo ($wall->id == $this->video->wall_id) ? 'selected="selected"' : ''; ?>>
							        <?php echo $wall->title; ?>
							      </option>
							    <?php endforeach; ?>
							  </select>
							</td>
						</tr>

						<!-- username -->
						<?php if ($this->video->id): ?>
						  <tr>
							  <td class="key" width="10%"><label for="username"><?php echo JText::_('Username'); ?>:</label></td>
							  <td><strong><?php echo $this->video->username; ?></strong></td>
						  </tr>
						<?php endif; ?>
			

					</table>
				</fieldset>
			</td>
		</tr>
	</table>

  <input type="hidden" name="controller" value="video" />
  <input type="hidden" name="id" value="<?php echo $this->video->id; ?>" />
  <input type="hidden" name="option" value="com_wallfactory" />
  <input type="hidden" name="task" value="" />
</form>