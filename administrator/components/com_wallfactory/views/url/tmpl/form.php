<?php
/*------------------------------------------------------------------------
com_wallfactory - Wall Factory
------------------------------------------------------------------------
author    TheFactory
copyright Copyright (C) 2010 SKEPSIS Consult SRL. All Rights Reserved.
@license - http://www.gnu.org/licenses/gpl-2.0.html GNU/GPL
Websites: http://www.thefactory.ro
Technical Support:  Forum - http://www.thefactory.ro/joomla-forum/
-------------------------------------------------------------------------*/

defined('_JEXEC') or die('Restricted access'); ?>

<form action="index.php" method="post" name="adminForm" id="adminForm">

<table class="admintable" width="100%">
		<tr valign="top">
			<td> 
				<fieldset>
					<legend><?php echo JText::_('Link'); ?></legend>

					<table width="100%">
						<?php if ($this->url->id): ?>
						  <tr>
							  <td class="key" width="20%" align="right"><?php echo JText::_('ID'); ?>:</td>
							  <td width="80%"><strong><?php echo $this->url->id; ?></strong></td>
						  </tr>
						<?php endif; ?>

						<!-- link -->
						<tr>
							<td class="key" align="right"><label for="title"><?php echo JText::_('Link'); ?>:</label></td>
							<td>
							  <input name="url" id="url" value="<?php echo $this->url->url; ?>" style="width: 200px;" />
							</td>
						</tr>

						<!-- title -->
						<tr>
							<td class="key" align="right"><label for="title"><?php echo JText::_('Link title'); ?>:</label></td>
							<td>
							  <input name="url_title" id="url_title" value="<?php echo $this->url->url_title; ?>" style="width: 200px;" />
							</td>
						</tr>
						
						<!-- description -->
						<tr>
							<td class="key"><label for="title"><?php echo JText::_('Link description'); ?>:</label></td>
							<td>
							  <textarea name="url_description" id="url_description" rows="8" cols="50" class="mceEditor"><?php echo $this->url->url_description; ?></textarea>
							</td>
						</tr>
												
						<!-- wall_id -->
						<tr>
							<td class="key" align="right"><label for="wall_id"><?php echo JText::_('Wall'); ?>:</label></td>
							<td>
							  <select name="wall_id" id="wall_id">
							    <?php foreach ($this->walls as $wall): ?>
							      <option value="<?php echo $wall->id; ?>" <?php echo ($wall->id == $this->url->wall_id) ? 'selected="selected"' : ''; ?>>
							        <?php echo $wall->title; ?>
							      </option>
							    <?php endforeach; ?>
							  </select>
							</td>
						</tr>

						<!-- username -->
						<?php if ($this->url->id): ?>
						  <tr>
							  <td class="key" width="20%" align="right"><?php echo JText::_('Username'); ?>:</td>
							  <td width="80%"><strong><?php echo $this->url->username; ?></strong></td>
						  </tr>
						<?php endif; ?>
			

					</table>
				</fieldset>
			</td>
		</tr>
	</table>

  <input type="hidden" name="controller" value="url" />
  <input type="hidden" name="id" value="<?php echo $this->url->id; ?>" />
  <input type="hidden" name="option" value="com_wallfactory" />
  <input type="hidden" name="task" value="" />
</form>