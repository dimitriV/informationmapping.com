<?php
/*------------------------------------------------------------------------
com_wallfactory - Wall Factory
------------------------------------------------------------------------
author    TheFactory
copyright Copyright (C) 2010 SKEPSIS Consult SRL. All Rights Reserved.
@license - http://www.gnu.org/licenses/gpl-2.0.html GNU/GPL
Websites: http://www.thefactory.ro
Technical Support:  Forum - http://www.thefactory.ro/joomla-forum/
-------------------------------------------------------------------------*/

defined('_JEXEC') or die('Restricted access'); ?>

<fieldset>
  <legend><?php echo JText::_('Latest links'); ?></legend>

  <table class="adminlist">
    <thead>
	    <tr>
		   <th style="width: 120px;"><?php echo JText::_('Date added'); ?></th>
			  <th><?php echo JText::_('Title'); ?></th>
			  <th width="38%"><?php echo JText::_('Link'); ?></th>
			  <th width="8%"><?php echo JText::_('Alias'); ?></th>
	    </tr>
	  </thead>

	  <tbody>
	     <?php if (count($this->latest_urls)): ?>
        <?php foreach ($this->latest_urls as $i => $link): ?>
          <tr>
            <td><?php echo $link->date_added; ?></td>
            <td><a href="<?php echo JRoute::_('index.php?option=com_wallfactory&controller=url&task=edit&cid[]=' . $link->id); ?>"><?php echo $link->url_title; ?></a></td>
            <td><a href="<?php echo JRoute::_('index.php?option=com_wallfactory&controller=url&task=edit&cid[]=' . $link->id); ?>"><?php echo $link->url; ?></a></td>
            <td><?php echo $link->username; ?></td>
          </tr>
        <?php endforeach; ?>
      <?php else: ?>
        <tr>
          <td colspan="10"><?php echo JText::_('No links found!'); ?></td>
        </tr>
      <?php endif; ?>
    </tbody>

    <tfoot>
      <tr>
        <td colspan="10" style="padding-top: 10px;"><a href="<?php echo JRoute::_('index.php?option=com_wallfactory&task=urls'); ?>"><?php echo JText::_('All links'); ?></td>
      </tr>
    </tfoot>
  </table>
</fieldset>