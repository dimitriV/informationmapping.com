<?php
/*------------------------------------------------------------------------
com_wallfactory - Wall Factory
------------------------------------------------------------------------
author    TheFactory
copyright Copyright (C) 2010 SKEPSIS Consult SRL. All Rights Reserved.
@license - http://www.gnu.org/licenses/gpl-2.0.html GNU/GPL
Websites: http://www.thefactory.ro
Technical Support:  Forum - http://www.thefactory.ro/joomla-forum/
-------------------------------------------------------------------------*/

defined('_JEXEC') or die('Restricted access'); ?>
<?php JHTML::_('behavior.modal'); ?>
<fieldset>
  <legend><?php echo JText::_('Latest images'); ?></legend>

  <table class="adminlist">
    <thead>
	    <tr>
		    <th style="width: 120px;"><?php echo JText::_('Added at'); ?></th>
		    <th style="width: 100px;"><?php echo JText::_('Thumbnail'); ?></th>
			<th><?php echo JText::_('Title'); ?></th>
			<th width="18%"><?php echo JText::_('Image name'); ?></th>
			<th width="8%"><?php echo JText::_('Alias'); ?></th>
	    </tr>
	  </thead>

	  <tbody>
	    <?php if (count($this->latest_images)): ?>
        <?php foreach ($this->latest_images as $i => $image): ?>
          <tr>
            <td><?php echo $image->date_added; ?></td>
            <td style="width: 100px; text-align: center;"> <?php $filename = JPATH_COMPONENT_SITE.DS.'storage'.DS.'users'.DS.$image->user_id.DS.$image->folder.DS.$image->name.'.'.$image->extension;
  	           		if (file_exists($filename)) {
						$size = getimagesize($filename);
						$max_size = 70;
						if ($size[0] > $max_size || $size[1] > $max_size)  {
						    if ($size[0] > $size[1]) {
						        $new_width  = $max_size;
						        $new_height = $max_size * $size[1] / $size[0];
						    }
						    else
						    {
						        $new_width  = $max_size * $size[0] / $size[1];
						        $new_height = $max_size;
						    }
						}
						else
						{
						    $new_width  = $size[0];
						    $new_height = $size[1];
						} 
	           		}
					$path = JURI::root().'components/com_wallfactory/storage/users/'.$image->user_id.DS.$image->folder.DS.$image->name.'.'.$image->extension;	
					$path = JURI::root().'components/com_wallfactory/storage/users/'.$image->user_id.'/'.$image->folder.'/'.$image->name.'.'.$image->extension;
					
					?>
	 	          <a class="modal" href="<?php echo $path; ?>">
	 	          	<img src="<?php echo $path; ?>" style="width: <?php echo $new_width;?>px; height: <?php echo $new_height;?>px; " />
	 	          </a>
            </td>
            <td><a href="<?php echo JRoute::_('index.php?option=com_wallfactory&controller=post&task=edit&cid[]=' . $image->id); ?>"><?php echo $image->title; ?></a></td>
            <td><?php echo $image->name.'.'.$image->extension; ?></td>
            <td><?php echo $image->username; ?></td>
          </tr>
        <?php endforeach; ?>
      <?php else: ?>
        <tr>
          <td colspan="10"><?php echo JText::_('No images found!'); ?></td>
        </tr>
      <?php endif; ?>
    </tbody>

    <tfoot>
      <tr>
        <td colspan="10" style="padding-top: 10px;"><a href="<?php echo JRoute::_('index.php?option=com_wallfactory&task=images'); ?>"><?php echo JText::_('All images'); ?></td>
      </tr>
    </tfoot>
  </table>
</fieldset>