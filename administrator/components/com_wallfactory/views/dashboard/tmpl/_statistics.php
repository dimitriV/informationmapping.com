<?php
/*------------------------------------------------------------------------
com_wallfactory - Wall Factory
------------------------------------------------------------------------
author    TheFactory
copyright Copyright (C) 2010 SKEPSIS Consult SRL. All Rights Reserved.
@license - http://www.gnu.org/licenses/gpl-2.0.html GNU/GPL
Websites: http://www.thefactory.ro
Technical Support:  Forum - http://www.thefactory.ro/joomla-forum/
-------------------------------------------------------------------------*/

defined('_JEXEC') or die('Restricted access'); ?>

<fieldset>
  <legend><?php echo JText::_('Statistics'); ?></legend>

  <table class="adminlist">
	  <tbody>
     
      <tr>
        <td><?php echo JText::_('New posts this week'); ?> / <b><?php echo JText::_('Total posts'); ?></b></td>
        <td style="text-align: center; "><?php echo $this->new_posts; ?> / <b><?php echo $this->total_posts; ?></b></td>
     
     	<td><?php echo JText::_('New urls this week'); ?> / <b><?php echo JText::_('Total urls'); ?></b></td>
        <td style="text-align: center; "><?php echo $this->new_urls; ?> / <b><?php echo $this->total_urls; ?></b></td>
      </tr>

      <tr>
      	<td><?php echo JText::_('New comments this week'); ?> / <b><?php echo JText::_('Total comments'); ?></b></td>
        <td style="text-align: center; "><?php echo $this->new_comments; ?> / <b><?php echo $this->total_comments; ?></b></td>
      
      	<td><?php echo JText::_('New videos this week'); ?> / <b><?php echo JText::_('Total videos'); ?></b></td>
        <td style="text-align: center; "><?php echo $this->new_videos; ?> / <b><?php echo $this->total_videos; ?></b></td>
      </tr>

 	  <tr>
 	   	<td><?php echo JText::_('New users this week'); ?> / <b><?php echo JText::_('Total users'); ?></b></td>
        <td style="text-align: center; "><?php echo $this->new_users; ?> / <b><?php echo $this->total_users; ?></b></td>
        
        <td><?php echo JText::_('New images this week'); ?> / <b><?php echo JText::_('Total images'); ?></b></td>
        <td style="text-align: center; "><?php echo $this->new_images; ?> / <b><?php echo $this->total_images; ?></b></td>
      </tr>

 	  <tr>  
        <td><?php echo JText::_('Total followers'); ?> / <b><?php echo JText::_('Total followed walls'); ?></b></td>
        <td style="text-align: center; "><?php echo $this->total_followers; ?> / <b><?php echo $this->total_followed_walls; ?></b></td>
             
        <td><?php echo JText::_('New mp3 files week'); ?> / <b><?php echo JText::_('Total mp3 files'); ?></b></td>
        <td style="text-align: center; "><?php echo $this->new_mp3files; ?> / <b><?php echo $this->total_mp3files; ?></b></td>
      </tr>

 	  <tr>
 	  	<td><?php echo JText::_('New reports this week'); ?> / <b><?php echo JText::_('Total reports'); ?></b></td>
        <td style="text-align: center; "><?php echo $this->new_post_reports + $this->new_comment_reports ; ?> / <b><?php echo $this->total_post_reports + $this->total_comment_reports; ?></b></td>
        
        <td><?php echo JText::_('New files week'); ?> / <b><?php echo JText::_('Total files'); ?></b></td>
        <td style="text-align: center; "><?php echo $this->new_files; ?> / <b><?php echo $this->total_files; ?></b></td>
      </tr>
    </tbody>
  </table>
</fieldset>