<?php
/*------------------------------------------------------------------------
com_wallfactory - Wall Factory
------------------------------------------------------------------------
author    TheFactory
copyright Copyright (C) 2010 SKEPSIS Consult SRL. All Rights Reserved.
@license - http://www.gnu.org/licenses/gpl-2.0.html GNU/GPL
Websites: http://www.thefactory.ro
Technical Support:  Forum - http://www.thefactory.ro/joomla-forum/
-------------------------------------------------------------------------*/

defined('_JEXEC') or die('Restricted access'); ?>

<fieldset>
  <legend><?php echo JText::_('Latest Mp3 Files'); ?></legend>

  <table class="adminlist">
    <thead>
	    <tr>
		    <th style="width: 120px;"><?php echo JText::_('Added at'); ?></th>
			  <th><?php echo JText::_('Title'); ?></th>
			  <th width="18%"><?php echo JText::_('File name'); ?></th>
			  <th width="8%"><?php echo JText::_('Alias'); ?></th>
	    </tr>
	  </thead>

	  <tbody>
	    <?php if (count($this->latest_mp3files)): ?>
        <?php foreach ($this->latest_mp3files as $i => $mp3file): ?>
          <tr>
            <td><?php echo $mp3file->date_added; ?></td>
            <td><a href="<?php echo JRoute::_('index.php?option=com_wallfactory&controller=post&task=edit&cid[]=' . $mp3file->id); ?>"><?php echo $mp3file->title; ?></a></td>
            <td><?php echo $mp3file->name.'.'.$mp3file->extension; ?></td>
            <td><?php echo $mp3file->username; ?></td>
          </tr>
        <?php endforeach; ?>
      <?php else: ?>
        <tr>
          <td colspan="10"><?php echo JText::_('No mp3 files found!'); ?></td>
        </tr>
      <?php endif; ?>
    </tbody>

    <tfoot>
      <tr>
        <td colspan="10" style="padding-top: 10px;"><a href="<?php echo JRoute::_('index.php?option=com_wallfactory&task=mp3files'); ?>"><?php echo JText::_('All mp3 files'); ?></td>
      </tr>
    </tfoot>
  </table>
</fieldset>