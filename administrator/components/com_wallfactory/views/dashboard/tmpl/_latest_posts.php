<?php
/*------------------------------------------------------------------------
com_wallfactory - Wall Factory
------------------------------------------------------------------------
author    TheFactory
copyright Copyright (C) 2010 SKEPSIS Consult SRL. All Rights Reserved.
@license - http://www.gnu.org/licenses/gpl-2.0.html GNU/GPL
Websites: http://www.thefactory.ro
Technical Support:  Forum - http://www.thefactory.ro/joomla-forum/
-------------------------------------------------------------------------*/

defined('_JEXEC') or die('Restricted access'); ?>

<fieldset>
  <legend><?php echo JText::_('Latest posts'); ?></legend>

  <table class="adminlist">
    <thead>
	    <tr>
		    <th style="width: 120px;"><?php echo JText::_('Created at'); ?></th>
			<th><?php echo JText::_('Content'); ?></th>
			<th width="25%"><?php echo JText::_('Wall'); ?></th>
	    </tr>
	  </thead>

	  <tbody>
	    <?php if (count($this->latest_posts)): ?>
        <?php foreach ($this->latest_posts as $i => $post): ?>
          <tr>
            <td><?php echo $post->date_created; ?></td>
            <td><a href="<?php echo JRoute::_('index.php?option=com_wallfactory&controller=post&task=edit&cid[]=' . $post->id); ?>"><?php echo wallHelper::trimText($post->content, 150); ?></a></td>
            <td><a href="<?php echo JRoute::_('index.php?option=com_wallfactory&controller=wall&task=edit&cid[]=' . $post->wall_id); ?>"><?php echo $post->wall_title; ?></a></td>
          </tr>
        <?php endforeach; ?>
      <?php else: ?>
        <tr>
          <td colspan="10"><?php echo JText::_('No posts found!'); ?></td>
        </tr>
      <?php endif; ?>
    </tbody>

    <tfoot>
      <tr>
        <td colspan="10" style="padding-top: 10px;"><a href="<?php echo JRoute::_('index.php?option=com_wallfactory&task=posts'); ?>"><?php echo JText::_('All posts'); ?></td>
      </tr>
    </tfoot>
  </table>
</fieldset>