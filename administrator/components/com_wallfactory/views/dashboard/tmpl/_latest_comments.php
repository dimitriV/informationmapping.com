<?php
/*------------------------------------------------------------------------
com_wallfactory - Wall Factory
------------------------------------------------------------------------
author    TheFactory
copyright Copyright (C) 2010 SKEPSIS Consult SRL. All Rights Reserved.
@license - http://www.gnu.org/licenses/gpl-2.0.html GNU/GPL
Websites: http://www.thefactory.ro
Technical Support:  Forum - http://www.thefactory.ro/joomla-forum/
-------------------------------------------------------------------------*/

defined('_JEXEC') or die('Restricted access'); ?>

<fieldset>
  <legend><?php echo JText::_('Latest comments'); ?></legend>

  <table class="adminlist">
    <thead>
	    <tr>
		    <th style="width: 120px;"><?php echo JText::_('Date added'); ?></th>
			  <th><?php echo JText::_('Text'); ?></th>
	    </tr>
	  </thead>

	  <tbody>
	    <?php if (count($this->latest_comments)): ?>
        <?php foreach ($this->latest_comments as $i => $comment): ?>
          <tr>
            <td><?php echo $comment->date_created; ?></td>
            <td><?php echo wallHelper::trimText($comment->content, 150); ?></td>
          </tr>
        <?php endforeach; ?>
      <?php else: ?>
        <tr>
          <td colspan="10"><?php echo JText::_('No comments found!'); ?></td>
        </tr>
      <?php endif; ?>
    </tbody>

    <tfoot>
      <tr>
        <td colspan="10" style="padding-top: 10px;"><a href="<?php echo JRoute::_('index.php?option=com_wallfactory&task=comments'); ?>"><?php echo JText::_('All Comments'); ?></td>
      </tr>
    </tfoot>
  </table>
</fieldset>