<?php
/*------------------------------------------------------------------------
com_wallfactory - Wall Factory
------------------------------------------------------------------------
author    TheFactory
copyright Copyright (C) 2010 SKEPSIS Consult SRL. All Rights Reserved.
@license - http://www.gnu.org/licenses/gpl-2.0.html GNU/GPL
Websites: http://www.thefactory.ro
Technical Support:  Forum - http://www.thefactory.ro/joomla-forum/
-------------------------------------------------------------------------*/

defined('_JEXEC') or die('Restricted access'); ?>

<table class="admintable" width="100%">
  	<tr valign="top">
		<td colspan="2" width="100%">
		  <?php require_once('_statistics.php'); ?>
		</td>
	</tr>

	<tr valign="top">
		<td width="50%">
		  <?php require_once('_latest_posts.php'); ?>
		</td>
		<td colspan="2" width="100%">
		  <?php require_once('_latest_comments.php'); ?>
		</td>
		
	</tr>

	<tr valign="top">
		<td width="50%">
		  <?php require_once('_latest_urls.php'); ?>
		</td>
		<td width="50%">
		  <?php require_once('_latest_videos.php'); ?>
		</td>
	</tr>
	<tr>	
		<td width="50%">
		  <?php require_once('_latest_images.php'); ?>
		</td>
		<td width="50%">
		  <?php require_once('_latest_mp3files.php'); ?>
		</td>
	</tr>
	
	<tr valign="top">
		<td width="50%">
		  <?php require_once('_latest_files.php'); ?>
		</td>
		<td width="50%">
		  <?php require_once('_latest_users.php'); ?>
		</td>
	</tr>
	<tr valign="top">
		
	</tr>
</table>