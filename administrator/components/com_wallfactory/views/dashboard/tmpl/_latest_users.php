<?php
/*------------------------------------------------------------------------
com_wallfactory - Wall Factory
------------------------------------------------------------------------
author    TheFactory
copyright Copyright (C) 2010 SKEPSIS Consult SRL. All Rights Reserved.
@license - http://www.gnu.org/licenses/gpl-2.0.html GNU/GPL
Websites: http://www.thefactory.ro
Technical Support:  Forum - http://www.thefactory.ro/joomla-forum/
-------------------------------------------------------------------------*/

defined('_JEXEC') or die('Restricted access'); ?>

<fieldset>
  <legend><?php echo JText::_('Latest users'); ?></legend>

  <table class="adminlist">
    <thead>
	    <tr>
	      <th style="width: 120px;"><?php echo JText::_('Registered at'); ?></th>
		  <th><?php echo JText::_('Name'); ?></th>
		  <th width="30%"><?php echo JText::_('Wall alias'); ?></th>
	    </tr>
	  </thead>

	  <tbody>
	    <?php if (count($this->latest_users)): ?>
        <?php foreach ($this->latest_users as $i => $user): ?>
          <tr>
          	<td><?php echo $user->date_created; ?></td>
            <td><a href="<?php echo JRoute::_('index.php?option=com_wallfactory&controller=user&task=edit&cid[]=' . $user->user_id); ?>"><?php echo $user->username; ?></a></td>
            <td><a href="<?php echo JRoute::_('index.php?option=com_wallfactory&controller=wall&task=edit&cid[]=' . $user->id); ?>"><?php echo $user->alias; ?></a></td>
          </tr>
        <?php endforeach; ?>
      <?php else: ?>
        <tr>
          <td colspan="10"><?php echo JText::_('No users found!'); ?></td>
        </tr>
      <?php endif; ?>
    </tbody>

    <tfoot>
      <tr>
        <td colspan="10" style="padding-top: 10px;"><a href="<?php echo JRoute::_('index.php?option=com_wallfactory&task=users'); ?>"><?php echo JText::_('All users'); ?></td>
      </tr>
    </tfoot>
  </table>
</fieldset>