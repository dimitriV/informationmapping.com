<?php
/*------------------------------------------------------------------------
com_wallfactory - Wall Factory
------------------------------------------------------------------------
author    TheFactory
copyright Copyright (C) 2010 SKEPSIS Consult SRL. All Rights Reserved.
@license - http://www.gnu.org/licenses/gpl-2.0.html GNU/GPL
Websites: http://www.thefactory.ro
Technical Support:  Forum - http://www.thefactory.ro/joomla-forum/
-------------------------------------------------------------------------*/

defined('_JEXEC') or die('Restricted access'); ?>

<fieldset>
  <legend><?php echo JText::_('Latest videos'); ?></legend>

  <table class="adminlist">
    <thead>
	    <tr>
		    <th style="width: 120px;"><?php echo JText::_('Date added'); ?></th>
			  <th><?php echo JText::_('Title'); ?></th>
			  <th width="38%"><?php echo JText::_('Video Link'); ?></th>
			  <th width="8%"><?php echo JText::_('Alias'); ?></th>
	    </tr>
	  </thead>

	  <tbody>
	    <?php if (count($this->latest_videos)): ?>
        <?php foreach ($this->latest_videos as $i => $video): ?>
          <tr>
            <td><?php echo $video->date_added; ?></td>
            <td><a href="<?php echo JRoute::_('index.php?option=com_wallfactory&controller=video&task=edit&cid[]=' . $video->id); ?>"><?php echo $video->video_title; ?></a></td>
            <td><a href="<?php echo JRoute::_('index.php?option=com_wallfactory&controller=video&task=edit&cid[]=' . $video->id); ?>"><?php echo $video->video_link; ?></a></td>
            <td><?php echo $post->username; ?></td>
          </tr>
        <?php endforeach; ?>
      <?php else: ?>
        <tr>
          <td colspan="10"><?php echo JText::_('No videos found!'); ?></td>
        </tr>
      <?php endif; ?>
    </tbody>

    <tfoot>
      <tr>
        <td colspan="10" style="padding-top: 10px;"><a href="<?php echo JRoute::_('index.php?option=com_wallfactory&task=videos'); ?>"><?php echo JText::_('All videos'); ?></td>
      </tr>
    </tfoot>
  </table>
</fieldset>