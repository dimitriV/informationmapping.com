<?php
/*------------------------------------------------------------------------
com_wallfactory - Wall Factory
------------------------------------------------------------------------
author    TheFactory
copyright Copyright (C) 2010 SKEPSIS Consult SRL. All Rights Reserved.
@license - http://www.gnu.org/licenses/gpl-2.0.html GNU/GPL
Websites: http://www.thefactory.ro
Technical Support:  Forum - http://www.thefactory.ro/joomla-forum/
-------------------------------------------------------------------------*/

defined('_JEXEC') or die('Restricted access');

jimport('joomla.application.component.view');

class BackendViewDashboard extends JView
{
  function display($tpl = null)
  {
    $user_info 			  =& $this->get('Data');
 
    $latest_posts         =& $this->get('LatestPosts');
    $latest_images        =& $this->get('LatestImages');
    $latest_mp3files      =& $this->get('LatestMp3Files');
	$latest_videos        =& $this->get('LatestVideos');
	$latest_urls          =& $this->get('LatestUrls');
	$latest_files         =& $this->get('LatestFiles');

	$total_posts          =& $this->get('TotalPosts');
    $new_posts            =& $this->get('NewPosts');
    $total_videos	      =& $this->get('TotalVideos');
    $new_videos           =& $this->get('NewVideos');
    $total_urls	      	  =& $this->get('TotalUrls');
    $new_urls             =& $this->get('NewUrls');
    $total_images	      =& $this->get('TotalImages');
    $new_images           =& $this->get('NewImages');
    $total_mp3files	      =& $this->get('TotalMp3Files');
    $new_mp3files         =& $this->get('NewMp3Files');
    $total_files	      =& $this->get('TotalFiles');
    $new_files         	  =& $this->get('NewFiles');
    
    $total_comments       =& $this->get('TotalComments');
    $new_comments         =& $this->get('NewComments');
    $total_followers      =& $this->get('TotalFollowers');
    $total_followed_walls =& $this->get('TotalFollowedWalls');
    $latest_comments      =& $this->get('LatestComments');
   
    $latest_users      	  =& $this->get('LatestUsers');
    $new_users            =& $this->get('NewUsers');
    $total_users	      =& $this->get('TotalUsers');
    $new_post_reports     =& $this->get('NewPostReports');
    $total_post_reports	  =& $this->get('TotalPostReports');
    $new_comment_reports  	=& $this->get('NewCommentReports');
    $total_comment_reports	=& $this->get('TotalCommentReports');

    $this->assignRef('latest_posts',         $latest_posts);
    $this->assignRef('latest_images',        $latest_images);
    $this->assignRef('latest_mp3files',      $latest_mp3files);
    $this->assignRef('latest_videos',        $latest_videos);
    $this->assignRef('latest_urls',          $latest_urls);
    $this->assignRef('latest_files',         $latest_files);
    $this->assignRef('total_posts',          $total_posts);
    $this->assignRef('total_videos',         $total_videos);
    $this->assignRef('total_urls',           $total_urls);
    $this->assignRef('total_images',         $total_images);
    $this->assignRef('total_mp3files',       $total_mp3files);
    $this->assignRef('total_files',          $total_files);
    
    $this->assignRef('new_posts',            $new_posts);
    $this->assignRef('new_videos',           $new_videos);
    $this->assignRef('new_urls',             $new_urls);
    $this->assignRef('new_mp3files',         $new_mp3files);
    $this->assignRef('new_images',           $new_images);
    $this->assignRef('new_files',            $new_files);
    $this->assignRef('total_comments',       $total_comments);
    $this->assignRef('new_comments',         $new_comments);
    $this->assignRef('total_followers',      $total_followers);
    $this->assignRef('total_followed_walls', $total_followed_walls);
    $this->assignRef('latest_comments',      $latest_comments);
    $this->assignRef('latest_users',      	 $latest_users);
    
    $this->assignRef('new_users',      		 $new_users);
    $this->assignRef('total_users',      	 $total_users);
    $this->assignRef('new_post_reports',     $new_post_reports);
    $this->assignRef('total_post_reports',   $total_post_reports);
    $this->assignRef('new_comment_reports',  $new_comment_reports);
    $this->assignRef('total_comment_reports',$total_comment_reports);
    

    if (count($user_info) == 1)
    	$text = $user_info[0]->title;
    else 
    	$text = 'All users';
  
	JToolBarHelper::title(JText::_('Dashboard').': <small><small>[ ' . $text.' ]</small></small>');
	
    parent::display($tpl);
  }
}