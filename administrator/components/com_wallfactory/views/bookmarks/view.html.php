<?php
/*------------------------------------------------------------------------
com_wallfactory - Wall Factory
------------------------------------------------------------------------
author    TheFactory
copyright Copyright (C) 2010 SKEPSIS Consult SRL. All Rights Reserved.
@license - http://www.gnu.org/licenses/gpl-2.0.html GNU/GPL
Websites: http://www.thefactory.ro
Technical Support:  Forum - http://www.thefactory.ro/joomla-forum/
-------------------------------------------------------------------------*/

defined('_JEXEC') or die('Restricted access');

jimport('joomla.application.component.view');

class BackendViewBookmarks extends JView
{
  function display($tpl = null)
  {
    JToolBarHelper::title(JText::_('Bookmarks'), 'generic.png');
    JToolBarHelper::publishList();
    JToolBarHelper::unpublishList();
    JToolBarHelper::spacer();
    JToolBarHelper::addNew();
    JToolBarHelper::editList();
    JToolBarHelper::deleteList(JText::_('Are you sure?'));

    $bookmarks  =& $this->get('Data');
    $pagination =& $this->get('Pagination');
    $lists      =& $this->_getViewLists();

    $this->assignRef('bookmarks',  $bookmarks);
    $this->assignRef('pagination', $pagination);
    $this->assignRef('lists',      $lists);

    parent::display($tpl);
  }

  function &_getViewLists()
	{
		global $mainframe, $option;

		$filter_order     = $mainframe->getUserStateFromRequest($option . '.bookmarks.filter_order',     'filter_order',     'title', 'cmd');
    $filter_order_Dir = $mainframe->getUserStateFromRequest($option . '.bookmarks.filter_order_Dir', 'filter_order_Dir', 'asc',   'word');
    $filter_state		  = $mainframe->getUserStateFromRequest($option . '.bookmarks.filter_state',		  'filter_state',		  '',			 'word' );
    $search				    = $mainframe->getUserStateFromRequest($option . '.bookmarks.search',			      'search',			      '',			 'string');
		$search				    = JString::strtolower($search);

		// table ordering
		$lists['order_Dir']	= $filter_order_Dir;
		$lists['order']		  = $filter_order;

		// search
		$lists['search'] = $search;

		// state
		$lists['state'] = $filter_state;

		return $lists;
	}
}