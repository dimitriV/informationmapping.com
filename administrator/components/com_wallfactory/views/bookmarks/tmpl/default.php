<?php
/*------------------------------------------------------------------------
com_wallfactory - Wall Factory
------------------------------------------------------------------------
author    TheFactory
copyright Copyright (C) 2010 SKEPSIS Consult SRL. All Rights Reserved.
@license - http://www.gnu.org/licenses/gpl-2.0.html GNU/GPL
Websites: http://www.thefactory.ro
Technical Support:  Forum - http://www.thefactory.ro/joomla-forum/
-------------------------------------------------------------------------*/

defined('_JEXEC') or die('Restricted access'); ?>

<form action="index.php" method="post" name="adminForm" style="width: 98%; margin: 0px auto;">

	<table>
		<tr>
			<td align="left" width="100%">
				<label for="search"><?php echo JText::_('Filter'); ?>:</label>
				<input type="text" id="search" name="search" id="search" value="<?php echo $this->lists['search'];?>" class="text_area" onchange="document.adminForm.submit();" />
				<button onclick="this.form.submit();"><?php echo JText::_('Go'); ?></button>
				<button onclick="document.getElementById('search').value='';this.form.getElementById('filter_state').value='';this.form.submit();"><?php echo JText::_('Reset'); ?></button>
			</td>
			<td>
			  <?php echo JHTML::_('grid.state', $this->lists['state']); ?>
			</td>
		</tr>
	</table>

<table class="adminlist">
	<thead>
		<tr>
			<th width="20px"><?php echo JText::_('NUM'); ?></th>
			<th width="20px"><input type="checkbox" name="toggle" value="" onclick="checkAll(<?php echo count($this->bookmarks); ?>);" /></th>
			<th width="48px" style="text-align: center;"><?php echo JText::_('Thumbnail'); ?></th>
			<th class="title"><?php echo JHTML::_('grid.sort', JText::_('Title'), 'title', $this->lists['order_Dir'], $this->lists['order']); ?></th>
			<th width="8%"><?php echo JHTML::_('grid.sort', JText::_('Published'), 'published', $this->lists['order_Dir'], $this->lists['order']); ?></th>
		</tr>
	</thead>

	<tfoot>
		<tr>
			<td colspan="12">
				<?php echo $this->pagination->getListFooter(); ?>
			</td>
		</tr>
	</tfoot>

	<tbody>
	  <?php foreach ($this->bookmarks as $i => $bookmark): ?>
	    <tr class="row<?php echo $i % 2; ?>">
	      <td width="20px"><?php echo ($i + 1 + $this->pagination->limitstart); ?></td>
	      <td width="20px"><?php echo JHTML::_('grid.id', $i, $bookmark->id); ?></td>
	      <td style="text-align: center;">
	        <?php if (!empty($bookmark->extension)): ?>
	          <img src="<?php echo JURI::root(); ?>administrator/components/com_wallfactory/storage/bookmarks/<?php echo $bookmark->id . '.' . $bookmark->extension; ?>" />
	        <?php endif; ?>
	      </td>
	      <td>
	        <a href="<?php echo JRoute::_('index.php?option=com_wallfactory&controller=bookmark&task=edit&cid[]=' . $bookmark->id); ?>"><?php echo $bookmark->title; ?></a>
	      </td>
	      <td style="text-align: center;"><a href="<?php echo JRoute::_('index.php?option=com_wallfactory&controller=bookmark&task=change&field=published&value=' . !$bookmark->published . '&id=' . $bookmark->id); ?>"><img src="images/<?php echo $bookmark->published ? 'tick' : 'publish_x'; ?>.png" /></a></td>
	    </tr>
	  <?php endforeach; ?>
	</tbody>
</table>

  <input type="hidden" name="controller" value="bookmark" />
  <input type="hidden" name="boxchecked" value="0" />
  <input type="hidden" name="option" value="com_wallfactory" />
  <input type="hidden" name="task" value="bookmarking" />
  <input type="hidden" name="filter_order" value="<?php echo $this->lists['order']; ?>" />
	<input type="hidden" name="filter_order_Dir" value="<?php echo $this->lists['order_Dir']; ?>" />
</form>
