<?php
/*------------------------------------------------------------------------
com_wallfactory - Wall Factory
------------------------------------------------------------------------
author    TheFactory
copyright Copyright (C) 2010 SKEPSIS Consult SRL. All Rights Reserved.
@license - http://www.gnu.org/licenses/gpl-2.0.html GNU/GPL
Websites: http://www.thefactory.ro
Technical Support:  Forum - http://www.thefactory.ro/joomla-forum/
-------------------------------------------------------------------------*/

defined('_JEXEC') or die('Restricted access'); ?>

<form action="index.php" method="post" name="adminForm" id="adminForm">

<table class="admintable" width="100%">
		<tr valign="top">
			<td> 
				<fieldset>
					<legend><?php echo JText::_('Mp3file'); ?></legend>

					<table width="100%">
						<?php if ($this->mp3file->id): ?>
						  <tr>
							  <td class="key" width="20%" align="right"><label for="id"><?php echo JText::_('ID'); ?>:</label></td>
							  <td width="80%"><strong><?php echo $this->mp3file->id; ?></strong></td>
						  </tr>
						<?php endif; ?>

						<!-- mp3file_link -->
						<tr>
							<td class="key" align="right"><label for="title"><?php echo JText::_('Mp3file folder'); ?>:</label></td>
							<td>
								<select name="folder" id="folder">
							    	<option value="mp3files" <?php echo ($this->mp3file->folder == 'images') ? 'selected="selected"' : ''; ?>>
							        	<?php echo JText::_('Images'); ?>
							      	</option>
							      	<option value="mp3" <?php echo ($this->mp3file->folder == 'mp3') ? 'selected="selected"' : ''; ?>>
							        	<?php echo JText::_('Mp3'); ?>
							      	</option>
							      	<option value="files" <?php echo ($this->mp3file->folder == 'files') ? 'selected="selected"' : ''; ?>>
							        	<?php echo JText::_('Files'); ?>
							      	</option>
							    </select>
							</td>
						</tr>

						<!-- title -->
						<tr>
							<td class="key" align="right"><label for="title"><?php echo JText::_('Mp3file title'); ?>:</label></td>
							<td>
							  <input name="title" id="title" value="<?php echo $this->mp3file->title; ?>" style="width: 200px;" />
							</td>
						</tr>
						
						<!-- description -->
						<tr>
							<td class="key" align="right"><label for="title"><?php echo JText::_('Mp3file name'); ?>:</label></td>
							<td>
							  <input name="name" id="name" value="<?php echo $this->mp3file->name; ?>" style="width: 420px;" />
							</td>
						</tr>
															
						<!-- wall_id -->
						<tr>
							<td class="key" align="right"><label for="wall_id"><?php echo JText::_('Wall'); ?>:</label></td>
							<td>
							  <select name="wall_id" id="wall_id">
							    <?php foreach ($this->walls as $wall): ?>
							      <option value="<?php echo $wall->id; ?>" <?php echo ($wall->id == $this->mp3file->wall_id) ? 'selected="selected"' : ''; ?>>
							        <?php echo $wall->title; ?>
							      </option>
							    <?php endforeach; ?>
							  </select>
							</td>
						</tr>

						<!-- username -->
						<?php if ($this->mp3file->id): ?>
						  <tr>
							  <td class="key" width="20%" align="right"><label for="username"><?php echo JText::_('Username'); ?>:</label></td>
							  <td><strong><?php echo $this->mp3file->username; ?></strong></td>
						  </tr>
						<?php endif; ?>
			

					</table>
				</fieldset>
			</td>
		</tr>
	</table>

  <input type="hidden" name="controller" value="mp3file" />
  <input type="hidden" name="id" value="<?php echo $this->mp3file->id; ?>" />
  <input type="hidden" name="option" value="com_wallfactory" />
  <input type="hidden" name="task" value="" />
</form>