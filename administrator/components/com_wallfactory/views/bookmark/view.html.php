<?php
/*------------------------------------------------------------------------
com_wallfactory - Wall Factory
------------------------------------------------------------------------
author    TheFactory
copyright Copyright (C) 2010 SKEPSIS Consult SRL. All Rights Reserved.
@license - http://www.gnu.org/licenses/gpl-2.0.html GNU/GPL
Websites: http://www.thefactory.ro
Technical Support:  Forum - http://www.thefactory.ro/joomla-forum/
-------------------------------------------------------------------------*/

defined('_JEXEC') or die('Restricted access');

jimport('joomla.application.component.view');

class BackendViewBookmark extends JView
{
  function display($tpl = null)
  {
    $bookmark =& $this->get('Data');
    $isNew    = ($bookmark->id < 1);

    $text = $isNew ? JText::_('New') : '"' . $bookmark->title . '"';
    JToolBarHelper::title(JText::_('Bookmark').': <small><small>[ ' . $text.' ]</small></small>');

    JToolBarHelper::save();
    ($isNew) ? JToolBarHelper::cancel() : JToolBarHelper::cancel('cancel', 'Close');

    $this->assignRef('bookmark', $bookmark);

    parent::display($tpl);
  }
}