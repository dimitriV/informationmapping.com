<?php
/*------------------------------------------------------------------------
com_wallfactory - Wall Factory
------------------------------------------------------------------------
author    TheFactory
copyright Copyright (C) 2010 SKEPSIS Consult SRL. All Rights Reserved.
@license - http://www.gnu.org/licenses/gpl-2.0.html GNU/GPL
Websites: http://www.thefactory.ro
Technical Support:  Forum - http://www.thefactory.ro/joomla-forum/
-------------------------------------------------------------------------*/

defined('_JEXEC') or die('Restricted access'); ?>

<form action="index.php" method="post" name="adminForm" id="adminForm" enctype="multipart/form-data">

<table class="admintable" width="100%">
		<tr valign="top">
			<td>
				<fieldset>
					<legend><?php echo JText::_('Bookmark'); ?></legend>

					<table width="100%">
						<?php if ($this->bookmark->id): ?>
						  <tr>
							  <td class="key" width="20%" align="right"><label for="id"><?php echo JText::_('ID'); ?>:</label></td>
							  <td width="80%"><strong><?php echo $this->bookmark->id; ?></strong></td>
						  </tr>
						<?php endif; ?>

						<!-- published -->
						<tr>
							<td class="key" align="right"><label for="published"><?php echo JText::_('Published'); ?>:</label></td>
							<td>
							  <select name="published" id="published">
							    <option value="0" <?php echo ($this->bookmark->published == 0) ? 'selected="selected"' : ''; ?>><?php echo JText::_('No'); ?></option>
							    <option value="1" <?php echo ($this->bookmark->published == 1) ? 'selected="selected"' : ''; ?>><?php echo JText::_('Yes'); ?></option>
							  </select>
							</td>
						</tr>

						<!-- title -->
						<tr>
							<td class="key" align="right"><label for="title"><?php echo JText::_('Title'); ?>:</label></td>
							<td>
							  <input name="title" id="title" value="<?php echo $this->bookmark->title; ?>" style="width: 200px;" />
							</td>
						</tr>

						<!-- link -->
						<tr>
							<td class="key" align="right" style="vertical-align: top;"><label for="link"><?php echo JText::_('Link'); ?>:</label></td>
							<td>
							  <input name="link" id="link" value="<?php echo $this->bookmark->link; ?>" style="width: 400px;" />
							  <div style="color: #666666;"><?php echo JText::_('Use: <br /><b>%%url%%</b> for current url, <br /><b>%%title%%</b> for post title, <br /><b>%%bodytext%%</b> for the first 500 characters of the post.'); ?></div>
							</td>
						</tr>

						<!-- current_icon -->
						<?php if (!empty($this->bookmark->extension)): ?>
						  <tr>
							  <td class="key" align="right" style="vertical-align: top;"><label for="current_icon"><?php echo JText::_('Current icon'); ?>:</label></td>
							  <td>
							    <img src="<?php echo JURI::root(); ?>administrator/components/com_wallfactory/storage/bookmarks/<?php echo $this->bookmark->id . '.' . $this->bookmark->extension; ?>" />
							    <br />
							    <input type="checkbox" id="delete" name="delete" /><label for="delete"><?php echo JText::_('delete'); ?></label>
							  </td>
						  </tr>
						<?php endif; ?>

						<!-- icon -->
						<tr>
							<td class="key" align="right"><label for="icon"><?php echo JText::_('Icon'); ?>:</label></td>
							<td>
							  <input type="file" name="icon" id="icon" value="<?php echo $this->bookmark->link; ?>" />
							</td>
						</tr>

						<!-- resize_icon -->
						<tr>
							<td class="key" align="right"><label for="resize_icon"><?php echo JText::_('Resize icon'); ?>:</label></td>
							<td>
							  <select id="resize_icon" name="resize_icon">
							    <option value="16" selected="selected">16 x 16</option>
							    <option value="24">24 x 24</option>
							    <option value="32">32 x 32</option>
							    <option value="48">48 x 48</option>
							  </select>px
							</td>
						</tr>
					</table>
				</fieldset>
			</td>
		</tr>
	</table>

  <input type="hidden" name="controller" value="bookmark" />
  <input type="hidden" name="id" value="<?php echo $this->bookmark->id; ?>" />
  <input type="hidden" name="extension" value="<?php echo $this->bookmark->extension; ?>" />
  <input type="hidden" name="option" value="com_wallfactory" />
  <input type="hidden" name="task" value="" />
</form>