<?php
/*------------------------------------------------------------------------
com_wallfactory - Wall Factory
------------------------------------------------------------------------
author    TheFactory
copyright Copyright (C) 2010 SKEPSIS Consult SRL. All Rights Reserved.
@license - http://www.gnu.org/licenses/gpl-2.0.html GNU/GPL
Websites: http://www.thefactory.ro
Technical Support:  Forum - http://www.thefactory.ro/joomla-forum/
-------------------------------------------------------------------------*/

defined('_JEXEC') or die('Restricted access'); ?>

<style>
  .icon-32-refresh { background-image: url(../images/toolbar/icon-32-refresh.png); }
</style>

<form action="index.php" method="post" name="adminForm" style="width: 98%; margin: 0px auto;">

	<table>
		<tr>
			<td align="left" width="100%">
				<label for="search"><?php echo JText::_('Filter'); ?>:</label>
				<input type="text" id="search" name="search" id="search" value="<?php echo $this->lists['search'];?>" class="text_area" onchange="document.adminForm.submit();" />
				<button onclick="this.form.submit();"><?php echo JText::_('Go'); ?></button>
				<button onclick="document.getElementById('search').value='';this.form.getElementById('filter_state').value='';this.form.submit();"><?php echo JText::_('Reset'); ?></button>
			</td>
			
		</tr>
	</table>

<table class="adminlist"> <!--l.video_thumbnail, l.video_sitename, l.video_sourceThumb, w.title AS wall_title, w.user_id, u.username-->
	<thead>
		<tr>
			<th width="20px"><?php echo JText::_('NUM'); ?></th>
			<th width="20px"><input type="checkbox" name="toggle" value="" onclick="checkAll(<?php echo count($this->images); ?>);" /></th>
			<th class="title"><?php echo JHTML::_('grid.sort', JText::_('Title'), 'm.title', $this->lists['order_Dir'], $this->lists['order']); ?></th>
			<th><?php echo JText::_('Image'); ?></th>
			<th width="14%"><?php echo JHTML::_('grid.sort', JText::_('Wall'), 'wall_title', $this->lists['order_Dir'], $this->lists['order']); ?></th>
			<th width="8%"><?php echo JHTML::_('grid.sort', JText::_('Owner'), 'u.username', $this->lists['order_Dir'], $this->lists['order']); ?></th>
			<th width="10%"><?php echo JHTML::_('grid.sort', JText::_('Added at'), 'm.date_added', $this->lists['order_Dir'], $this->lists['order']); ?></th>
		</tr>
	</thead>

	<tfoot>
		<tr>
			<td colspan="12">
				<?php echo $this->pagination->getListFooter(); ?>
			</td>
		</tr>
	</tfoot>

	<tbody>
	  <?php foreach ($this->images as $i => $image): ?>
	    <tr class="row<?php echo $i % 2; ?>">
	      <td width="20px"><?php echo ($i + 1 + $this->pagination->limitstart); ?></td>
	      <td width="20px"><?php echo JHTML::_('grid.id', $i, $image->id); ?></td>
	      <td><?php echo $image->title; ?></td>
	      <td><?php echo $image->name.'.'.$image->extension; ?></td>
	      <td><a href="<?php echo JRoute::_('index.php?option=com_wallfactory&controller=wall&task=edit&cid[]=' . $image->wall_id); ?>"><?php echo $image->wall_title; ?></a></td>
	      <td><a href="<?php echo JRoute::_('index.php?option=com_wallfactory&controller=user&task=edit&cid[]=' . $image->user_id); ?>"><?php echo $image->username; ?></a></td>
	      <td style="text-align: center;"><?php echo $image->date_added; ?></td>
	            
	    </tr>
	  <?php endforeach; ?>
	</tbody>
</table>

  <input type="hidden" name="controller" value="image" />
  <input type="hidden" name="boxchecked" value="0" />
  <input type="hidden" name="option" value="com_wallfactory" />
  <input type="hidden" name="task" value="images" />
  <input type="hidden" name="filter_order" value="<?php echo $this->lists['order']; ?>" />
  <input type="hidden" name="filter_order_Dir" value="<?php echo $this->lists['order_Dir']; ?>" />
</form>

<script>
  window.addEvent('domready', function() {

			SqueezeBox.initialize({});

			$$('a.modal').each(function(el) {
				el.addEvent('click', function(e) {

				  if (document.adminForm.boxchecked.value == 0)
				  {
				    return false;
				  }

				  new Event(e).stop();
					SqueezeBox.fromElement(el);
				});
			});
		});

</script>