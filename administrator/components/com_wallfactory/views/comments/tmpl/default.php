<?php
/*------------------------------------------------------------------------
com_wallfactory - Wall Factory
------------------------------------------------------------------------
author    TheFactory
copyright Copyright (C) 2010 SKEPSIS Consult SRL. All Rights Reserved.
@license - http://www.gnu.org/licenses/gpl-2.0.html GNU/GPL
Websites: http://www.thefactory.ro
Technical Support:  Forum - http://www.thefactory.ro/joomla-forum/
-------------------------------------------------------------------------*/

defined('_JEXEC') or die('Restricted access'); ?>

<form action="index.php" method="post" name="adminForm" style="width: 98%; margin: 0px auto;">

	<table>
		<tr>
			<!-- Search filter -->
		  <td align="left" width="100%">
				<label for="search"><?php echo JText::_('Filter'); ?>:</label>
				<input type="text" id="search" name="search" id="search" value="<?php echo $this->lists['search'];?>" class="text_area" onchange="document.adminForm.submit();" />
				<button onclick="this.form.submit();"><?php echo JText::_('Go'); ?></button>
				<button onclick="document.getElementById('search').value='';this.form.getElementById('filter_state').value='';this.form.getElementById('approved').value='';this.form.submit();"><?php echo JText::_('Reset'); ?></button>
			</td>

			<!-- Reported filter -->
			<td>
			  <select id="reported" name="reported" onchange="submitform();">
  			  <option value="" <?php echo $this->lists['reported'] == '' ? 'selected="selected"' : ''; ?>><?php echo JText::_('- Select Reported -'); ?></option>
  			  <option value="1" <?php echo $this->lists['reported'] == '1' ? 'selected="selected"' : ''; ?>><?php echo JText::_('Reported'); ?></option>
  			  <option value="0" <?php echo $this->lists['reported'] == '0' ? 'selected="selected"' : ''; ?>><?php echo JText::_('Not reported'); ?></option>
			  </select>
			</td>

			<!-- Published filter -->
			<td>
			  <?php echo JHTML::_('grid.state', $this->lists['state']); ?>
			</td>
		</tr>
	</table>

<table class="adminlist">
	<thead>
		<tr>
			<th width="20px"><?php echo JText::_('NUM'); ?></th>
			<th width="20px"><input type="checkbox" name="toggle" value="" onclick="checkAll(<?php echo count($this->comments); ?>);" /></th>
			<th class="title"><?php echo JHTML::_('grid.sort', JText::_('Text'), 'c.content', $this->lists['order_Dir'], $this->lists['order']); ?></th>
			<th width="14%"><?php echo JHTML::_('grid.sort', JText::_('Post'), 'p.id', $this->lists['order_Dir'], $this->lists['order']); ?></th>
			<th width="14%"><?php echo JText::_('Author Info'); ?></th>
			<th width="14%"><?php echo JHTML::_('grid.sort', JText::_('Created at'), 'c.date_created', $this->lists['order_Dir'], $this->lists['order']); ?></th>
			<th width="8%"><?php echo JHTML::_('grid.sort', JText::_('Reported'), 'c.reported', $this->lists['order_Dir'], $this->lists['order']); ?></th>
						
		</tr>
	</thead>

	<tfoot>
		<tr>
			<td colspan="12">
				<?php echo $this->pagination->getListFooter(); ?>
			</td>
		</tr>
	</tfoot>

	<tbody>
	  <?php foreach ($this->comments as $i => $comment): ?>
	    <tr class="row<?php echo $i % 2; ?>">
	      <td width="20px"><?php echo ($i + 1 + $this->pagination->limitstart); ?></td>
	      <td width="20px"><?php echo JHTML::_('grid.id', $i, $comment->id); ?></td>
	      <td><?php echo $comment->content; ?></td>
	      <td><?php echo $comment->post_id; ?></td>
	      <td>
	        <b><?php echo JText::_('Username'); ?>:</b> <?php echo $comment->username; ?>
	        <br />
	        <b><?php echo JText::_('Alias'); ?>:</b> <?php echo $comment->author_alias; ?>
	        
	      </td>
	      <td style="text-align: center;"><?php echo $comment->date_created; ?></td>
	      <td style="text-align: center;"><a href="<?php echo JRoute::_('index.php?option=com_wallfactory&controller=comment&task=change&field=reported&value=' . !$comment->reported . '&id=' . $comment->id); ?>"><img src="images/<?php echo $comment->reported ? 'tick' : 'publish_x'; ?>.png" /></a></td>
	     	      
	    </tr>
	  <?php endforeach; ?>
	</tbody>
</table>

  <input type="hidden" name="controller" value="comment" />
  <input type="hidden" name="boxchecked" value="0" />
  <input type="hidden" name="option" value="com_wallfactory" />
  <input type="hidden" name="task" value="comments" />
  <input type="hidden" name="filter_order" value="<?php echo $this->lists['order']; ?>" />
	<input type="hidden" name="filter_order_Dir" value="<?php echo $this->lists['order_Dir']; ?>" />
</form>