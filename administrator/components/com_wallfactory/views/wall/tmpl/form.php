<?php
/*------------------------------------------------------------------------
com_wallfactory - Wall Factory
------------------------------------------------------------------------
author    TheFactory
copyright Copyright (C) 2010 SKEPSIS Consult SRL. All Rights Reserved.
@license - http://www.gnu.org/licenses/gpl-2.0.html GNU/GPL
Websites: http://www.thefactory.ro
Technical Support:  Forum - http://www.thefactory.ro/joomla-forum/
-------------------------------------------------------------------------*/

defined('_JEXEC') or die('Restricted access'); ?>

<form action="index.php" method="post" name="adminForm" id="adminForm">

<table class="admintable" width="100%">
		<tr valign="top">
			<td>
				<fieldset>
					<legend><?php echo JText::_('Wall'); ?></legend>

					<table width="100%" cellpadding="0" cellspacing="0">
						<?php if ($this->wall->id): ?>
						  <tr>
							  <td class="key" width="10%" align="right"><label for="id"><?php echo JText::_('ID'); ?>:</label></td>
							  <td><strong><?php echo $this->wall->id; ?></strong></td>
						  </tr>
						<?php endif; ?>

						<!-- user_id -->
						<tr>
							<td class="key" align="right"><label for="user_id"><?php echo JText::_('Owner'); ?>:</label></td>
							<td>
							  <select name="user_id" id="user_id">
							    <?php foreach ($this->users as $user): ?>
							      <option value="<?php echo $user->id; ?>" <?php echo ($user->id == $this->wall->user_id) ? 'selected="selected"' : ''; ?>>
							        <?php echo $user->username; ?>
							      </option>
							    <?php endforeach; ?>
							  </select>
							</td>
						</tr>

						<!-- published -->
						<tr>
							<td class="key" align="right"><label for="published"><?php echo JText::_('Published'); ?>:</label></td>
							<td>
							  <select name="published" id="published">
							    <option value="0" <?php echo ($this->wall->published == 0) ? 'selected="selected"' : ''; ?>><?php echo JText::_('No'); ?></option>
							    <option value="1" <?php echo ($this->wall->published == 1) ? 'selected="selected"' : ''; ?>><?php echo JText::_('Yes'); ?></option>
							  </select>
							</td>
						</tr>

						<!-- title -->
						<tr>
							<td class="key" align="right"><label for="title"><?php echo JText::_('Wall title'); ?>:</label></td>
							<td>
							  <input name="title" id="title" value="<?php echo $this->wall->title; ?>" style="width: 200px;" />
							</td>
						</tr>
					</table>
				</fieldset>
			</td>
		</tr>
	</table>

  <input type="hidden" name="controller" value="wall" />
  <input type="hidden" name="id" value="<?php echo $this->wall->id; ?>" />
  <input type="hidden" name="option" value="com_wallfactory" />
  <input type="hidden" name="task" value="" />
</form>