<?php
/*------------------------------------------------------------------------
com_wallfactory - Wall Factory
------------------------------------------------------------------------
author    TheFactory
copyright Copyright (C) 2010 SKEPSIS Consult SRL. All Rights Reserved.
@license - http://www.gnu.org/licenses/gpl-2.0.html GNU/GPL
Websites: http://www.thefactory.ro
Technical Support:  Forum - http://www.thefactory.ro/joomla-forum/
-------------------------------------------------------------------------*/

defined('_JEXEC') or die('Restricted access');

jimport('joomla.application.component.view');

class BackendViewWall extends JView
{
  function display($tpl = null)
  {
    $wall  =& $this->get('Data');
    $users =& $this->get('Users');
    $isNew = ($wall->id < 1);

    $text = $isNew ? JText::_('New') : '"' . $wall->title . '"';
    JToolBarHelper::title(JText::_('Wall').': <small><small>[ ' . $text.' ]</small></small>');

    JToolBarHelper::save();
    ($isNew) ? JToolBarHelper::cancel() : JToolBarHelper::cancel('cancel', 'Close');

    $this->assignRef('wall',  $wall);
    $this->assignRef('users', $users);

    parent::display($tpl);
  }
}