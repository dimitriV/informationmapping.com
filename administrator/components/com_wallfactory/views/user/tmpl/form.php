<?php
/*------------------------------------------------------------------------
com_wallfactory - Wall Factory
------------------------------------------------------------------------
author    TheFactory
copyright Copyright (C) 2010 SKEPSIS Consult SRL. All Rights Reserved.
@license - http://www.gnu.org/licenses/gpl-2.0.html GNU/GPL
Websites: http://www.thefactory.ro
Technical Support:  Forum - http://www.thefactory.ro/joomla-forum/
-------------------------------------------------------------------------*/

defined('_JEXEC') or die('Restricted access'); ?>
<?php JHTML::_('behavior.calendar'); ?>

<form action="index.php" method="post" name="adminForm" id="adminForm">

<table class="admintable" width="100%">
		<tr valign="top">
			<td>
				<fieldset>
					<legend><?php echo JText::_('User'); ?></legend>

					<table width="100%">
						<tr>
						  <td class="key" width="10%" align="right"><label for="id"><?php echo JText::_('ID'); ?>:</label></td>
						  <td><strong><?php echo $this->user->user_id; ?></strong></td>
						</tr>

						<tr>
						  <td class="key" width="10%" align="right"><label for="username"><?php echo JText::_('Username'); ?>:</label></td>
						  <td><strong><?php echo $this->user->username; ?></strong></td>
						</tr>

						<!-- name -->
						<tr>
							<td class="key" align="right"><label for="name"><?php echo JText::_('Name'); ?>:</label></td>
							<td>
							  <input name="name" id="name" value="<?php echo $this->user->name; ?>" style="width: 200px;" />
							</td>
						</tr>

						<!-- gender -->
						<tr>
							<td class="key" align="right"><label for="gender"><?php echo JText::_('Gender'); ?>:</label></td>
							<td>
								<select id="gender" name="gender">
								  <option value="m" <?php echo ($this->user->gender == "m") ? 'selected="selected"' : ''; ?>><?php echo JText::_('m'); ?></option>
								  <option value="f" <?php echo ($this->user->gender == "f") ? 'selected="selected"' : ''; ?>><?php echo JText::_('f'); ?></option>
							  	</select>
							</td>
						</tr>
						
						<!-- birthday-->
						<tr>
							<td class="key" align="right"><label for="birthday"><?php echo JText::_('Birthday'); ?>:</label></td>
							<td>
								<?php $birthday = $this->user->birthday; 
								echo JHTML::calendar("$birthday",'birthday','birthday','%Y-%m-%d'); ?>
							</td>
						</tr>
						
						<!-- description -->
						<tr>
							<td class="key" align="right"><label for="description"><?php echo JText::_('Description'); ?>:</label></td>
							<td>
							  <textarea name="description" id="description" style="width: 200px;" /><?php echo $this->user->description; ?></textarea>
							</td>
						</tr>

					</table>
				</fieldset>
			</td>
		</tr>
		<tr valign="top">
			<td>
				<fieldset>
					<legend><?php echo JText::_('Reports'); ?></legend>

					<table width="100%">
						<tr>
						  <td class="key" width="10%" align="right"><label for="reported_id"><?php echo JText::_('Reported user id'); ?>:</label></td>
						  <td><strong><?php echo $this->user->user_id; ?></strong></td>
						</tr>

						<tr>
						  <td class="key" width="10%" align="right"><label for="action"><?php echo JText::_('Action'); ?>:</label></td>
						  <td width="80%">
						  		<?php if (!$this->user->banned): ?>
						          <a href="<?php echo JRoute::_('index.php?option=com_wallfactory&controller=user&task=actionBan&id=' . $this->user->id); ?>"><?php echo JText::_('Ban user'); ?></a>
						        <?php else: ?>
						          <?php echo JText::_('User banned'); ?>
						        <?php endif; ?>
						  </td>
						</tr>
						
						<tr>
						  <td class="paramlist_key" align="right"><label for="banned"><?php echo JText::_('Banned'); ?>:</label></td>
							<td>
							  <select id="banned" name="banned"><!-- user banned from members table -->
								  <option value="1" <?php echo ($this->user->banned) ? 'selected="selected"' : ''; ?>><?php echo JText::_('Yes'); ?></option>
								  <option value="0" <?php echo (!$this->user->banned) ? 'selected="selected"' : ''; ?>><?php echo JText::_('No'); ?></option>
							  </select>
							</td>
						</tr>
					</table>
				</fieldset>
			</td>
		</tr>
	</table>

  <input type="hidden" name="controller" value="user" />
  <input type="hidden" name="id" value="<?php echo $this->user->id; ?>" />
  <input type="hidden" name="option" value="com_wallfactory" />
  <input type="hidden" name="task" value="" />
</form>