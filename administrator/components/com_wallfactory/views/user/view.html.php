<?php
/*------------------------------------------------------------------------
com_wallfactory - Wall Factory
------------------------------------------------------------------------
author    TheFactory
copyright Copyright (C) 2010 SKEPSIS Consult SRL. All Rights Reserved.
@license - http://www.gnu.org/licenses/gpl-2.0.html GNU/GPL
Websites: http://www.thefactory.ro
Technical Support:  Forum - http://www.thefactory.ro/joomla-forum/
-------------------------------------------------------------------------*/

defined('_JEXEC') or die('Restricted access');

jimport('joomla.application.component.view');

class BackendViewUser extends JView
{
  function display($tpl = null)
  {
    $user =& $this->get('Data');

    $types = array(1 => JText::_('Message'),
                   2 => JText::_('Comment'),
    );
    if ($user->id > 0)
    {
      $text = $user->username;
      JToolBarHelper::title(JText::_('User').': <small><small>[ ' . $text.' ]</small></small>');

      JToolBarHelper::save();
      JToolBarHelper::cancel('cancel', 'Close');

      $this->assignRef('user', $user);
    }
    else
    {
      $this->_layout = 'not_found';
    }

    parent::display($tpl);
  }
}