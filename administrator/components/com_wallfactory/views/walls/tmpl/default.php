<?php
/*------------------------------------------------------------------------
com_wallfactory - Wall Factory
------------------------------------------------------------------------
author    TheFactory
copyright Copyright (C) 2010 SKEPSIS Consult SRL. All Rights Reserved.
@license - http://www.gnu.org/licenses/gpl-2.0.html GNU/GPL
Websites: http://www.thefactory.ro
Technical Support:  Forum - http://www.thefactory.ro/joomla-forum/
-------------------------------------------------------------------------*/

defined('_JEXEC') or die('Restricted access'); ?>
<?php JHTML::_('behavior.modal'); 
JHTML::_('behavior.mootools'); ?>
<form action="index.php" method="post" name="adminForm" style="width: 98%; margin: 0px auto;">

	<table>
		<tr>
			<td align="left" width="100%">
				<label for="search"><?php echo JText::_('Filter'); ?>:</label>
				<input type="text" id="search" name="search" id="search" value="<?php echo $this->lists['search'];?>" class="text_area" onchange="document.adminForm.submit();" />
				<button onclick="this.form.submit();"><?php echo JText::_('Go'); ?></button>
				<button onclick="document.getElementById('search').value='';this.form.getElementById('filter_state').value='';this.form.submit();"><?php echo JText::_('Reset'); ?></button>
			</td>
			<td>
			  <?php echo JHTML::_('grid.state', $this->lists['state']); ?>
			</td>
		</tr>
	</table>

<table class="adminlist">
	<thead>
		<tr>
			<th width="20px"><?php echo JText::_('NUM'); ?></th>
			<th width="20px"><input type="checkbox" name="toggle" value="" onclick="checkAll(<?php echo count($this->walls); ?>);" /></th>
			<th class="title"><?php echo JHTML::_('grid.sort', JText::_('Title'), 'w.title', $this->lists['order_Dir'], $this->lists['order']); ?></th>  
			<th width="8%"><?php echo JHTML::_('grid.sort', JText::_('User'), 'u.username', $this->lists['order_Dir'], $this->lists['order']); ?></th>
			<th width="14%"><?php echo JHTML::_('grid.sort', JText::_('Created at'), 'w.date_created', $this->lists['order_Dir'], $this->lists['order']); ?></th>
			<th width="8%"><?php echo JHTML::_('grid.sort', JText::_('Posts'), 's.posts', $this->lists['order_Dir'], $this->lists['order']); ?></th>
			<th width="8%"><?php echo JHTML::_('grid.sort', JText::_('Links'), 's.urls', $this->lists['order_Dir'], $this->lists['order']); ?></th>
			<th width="8%"><?php echo JHTML::_('grid.sort', JText::_('Videos'), 's.video_links', $this->lists['order_Dir'], $this->lists['order']); ?></th>
			<th width="8%"><?php echo JHTML::_('grid.sort', JText::_('Images'), 's.images', $this->lists['order_Dir'], $this->lists['order']); ?></th>
			<th width="8%"><?php echo JHTML::_('grid.sort', JText::_('Mp3 files'), 's.mp3', $this->lists['order_Dir'], $this->lists['order']); ?></th>
			<th width="8%"><?php echo JHTML::_('grid.sort', JText::_('No. of files'), 's.files', $this->lists['order_Dir'], $this->lists['order']); ?></th>
		</tr>
	</thead>

	<tfoot>
		<tr>
			<td colspan="12">
				<?php echo $this->pagination->getListFooter(); ?>
			</td>
		</tr>
	</tfoot>

	<tbody>
	  <?php foreach ($this->walls as $i => $wall): ?>
	    <tr class="row<?php echo $i % 2; ?>">
	      <td width="20px"><?php echo ($i + 1 + $this->pagination->limitstart); ?></td>
	      <td width="20px"><?php echo JHTML::_('grid.id', $i, $wall->id); ?></td>
	        <?php echo $wall->title; ?>
	      </td>
	      <td><a href="<?php echo JRoute::_('index.php?option=com_wallfactory&controller=user&task=dashboard&cid[]=' . $wall->user_id); ?>"><?php echo $wall->username; ?></a></td>
	      <td style="text-align: center;"><?php echo $wall->date_created; ?></td>
	      <td style="text-align: center;">
	      	<a title="posts" class="modal" rel="{handler: 'iframe', size: {x: 800, y: 700}}" href="index.php?option=com_wallfactory&format=raw&controller=user&task=posts&cid[]=<?php echo $wall->user_id; ?>" name="show_posts" id="postsButton" >
		<span class="icon-32-print"></span><?php echo $wall->posts; ?></a>
	      </td>
	      
	      <td style="text-align: center;">
	      	<a title="links" class="modal" rel="{handler: 'iframe', size: {x: 800, y: 700}}" href="index.php?option=com_wallfactory&format=raw&controller=user&task=urls&cid[]=<?php echo $wall->user_id; ?>" name="show_urls" id="urlsButton" >
		<span class="icon-32-print"></span><?php echo $wall->urls; ?></a>
		  </td>
	      <td style="text-align: center;">
	      	<a title="videos" class="modal" rel="{handler: 'iframe', size: {x: 800, y: 700}}" href="index.php?option=com_wallfactory&format=raw&controller=user&task=videos&cid[]=<?php echo $wall->user_id; ?>" name="show_videos" id="videosButton" >
		<span class="icon-32-print"></span><?php echo $wall->video_links; ?></a>
		  </td>
	      <td style="text-align: center;">
	      	<a title="images" class="modal" rel="{handler: 'iframe', size: {x: 800, y: 700}}" href="index.php?option=com_wallfactory&format=raw&controller=user&task=images&cid[]=<?php echo $wall->user_id; ?>" name="show_images" id="imagesButton" >
		<span class="icon-32-print"></span><?php echo $wall->images; ?></a>
		  </td>
	      <td style="text-align: center;">
	      	<a title="mp3Files" class="modal" rel="{handler: 'iframe', size: {x: 800, y: 700}}" href="index.php?option=com_wallfactory&format=raw&controller=user&task=mp3files&cid[]=<?php echo $wall->user_id; ?>" name="show_mp3" id="mp3Button" >
		<span class="icon-32-print"></span><?php echo $wall->mp3; ?></a>
		  </td>
	      <td style="text-align: center;">
	      	<a title="files" class="modal" rel="{handler: 'iframe', size: {x: 800, y: 700}}" href="index.php?option=com_wallfactory&format=raw&controller=user&task=files&cid[]=<?php echo $wall->user_id; ?>" name="show_images" id="filesButton" >
		<span class="icon-32-print"></span><?php echo $wall->files; ?></a>
		  </td>
	    </tr>
	  <?php endforeach; ?>
	</tbody>
</table>

  <input type="hidden" name="controller" value="wall" />
  <input type="hidden" name="boxchecked" value="0" />
  <input type="hidden" name="option" value="com_wallfactory" />
  <input type="hidden" name="task" value="walls" />
  <input type="hidden" name="filter_order" value="<?php echo $this->lists['order']; ?>" />
	<input type="hidden" name="filter_order_Dir" value="<?php echo $this->lists['order_Dir']; ?>" />
</form>

<script>
  window.addEvent('domready', function() {

			SqueezeBox.initialize({});

			$$('a.modal').each(function(el) {
				el.addEvent('click', function(e) {

				  if (document.adminForm.boxchecked.value == 0)
				  {
				    return false;
				  }

				  new Event(e).stop();
					SqueezeBox.fromElement(el);
				});
			});
		});

</script>