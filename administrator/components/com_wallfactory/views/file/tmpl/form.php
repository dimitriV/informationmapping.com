<?php 
/*------------------------------------------------------------------------
com_wallfactory - Wall Factory
------------------------------------------------------------------------
author    TheFactory
copyright Copyright (C) 2010 SKEPSIS Consult SRL. All Rights Reserved.
@license - http://www.gnu.org/licenses/gpl-2.0.html GNU/GPL
Websites: http://www.thefactory.ro
Technical Support:  Forum - http://www.thefactory.ro/joomla-forum/
-------------------------------------------------------------------------*/

defined('_JEXEC') or die('Restricted access'); ?>

<form action="index.php" method="post" name="adminForm" id="adminForm">

<table class="adminlist">
	<tr valign="top">
		<td> 
			<fieldset>
				<legend><?php echo JText::_('File'); ?></legend>
	
					<table width="100%" cellpadding="0" cellspacing="0">
						<?php if ($this->file->id): ?>
						  <tr>
							  <td class="key" width="10%"><label for="id"><?php echo JText::_('ID'); ?>:</label></td>
							  <td><strong><?php echo $this->file->id; ?></strong></td>
						  </tr>
						<?php endif; ?>

						<!-- file_link -->
						<tr>
							<td class="key"><label for="title"><?php echo JText::_('File folder'); ?>:</label></td>
							<td>
								<select name="folder" id="folder">
							    	<option value="files" <?php echo ($this->file->folder == 'images') ? 'selected="selected"' : ''; ?>>
							        	<?php echo JText::_('Images'); ?>
							      	</option>
							      	<option value="mp3" <?php echo ($this->file->folder == 'mp3') ? 'selected="selected"' : ''; ?>>
							        	<?php echo JText::_('Mp3'); ?>
							      	</option>
							      	<option value="files" <?php echo ($this->file->folder == 'files') ? 'selected="selected"' : ''; ?>>
							        	<?php echo JText::_('Files'); ?>
							      	</option>
							    </select>
							</td>
						</tr>

						<!-- title -->
						<tr>
							<td class="key"><label for="title"><?php echo JText::_('File title'); ?>:</label></td>
							<td>
							  <input name="title" id="title" value="<?php echo $this->file->title; ?>" style="width: 200px;" />
							</td>
						</tr>
						
						<!-- description -->
						<tr>
							<td class="key"><label for="title"><?php echo JText::_('File name'); ?>:</label></td>
							<td>
							  <input name="name" id="name" value="<?php echo $this->file->name; ?>" style="width: 420px;" />
							</td>
						</tr>
												
						<!-- wall_id -->
						<tr>
							<td class="key"><label for="wall_id"><?php echo JText::_('Wall'); ?>:</label></td>
							<td>
							  <select name="wall_id" id="wall_id">
							    <?php foreach ($this->walls as $wall): ?>
							      <option value="<?php echo $wall->id; ?>" <?php echo ($wall->id == $this->file->wall_id) ? 'selected="selected"' : ''; ?>>
							        <?php echo $wall->title; ?>
							      </option>
							    <?php endforeach; ?>
							  </select>
							</td>
						</tr>

						<!-- username -->
						<?php if ($this->file->id): ?>
						  <tr>
							  <td class="key" width="20%"><label for="username"><?php echo JText::_('Username'); ?>:</label></td>
							  <td><strong><?php echo $this->file->username; ?></strong></td>
						  </tr>
						<?php endif; ?>
			

					</table>
				</fieldset>
			</td>
		</tr>
	</table>

  <input type="hidden" name="controller" value="file" />
  <input type="hidden" name="id" value="<?php echo $this->file->id; ?>" />
  <input type="hidden" name="option" value="com_wallfactory" />
  <input type="hidden" name="task" value="" />
</form>