<?php
/*------------------------------------------------------------------------
com_wallfactory - Wall Factory
------------------------------------------------------------------------
author    TheFactory
copyright Copyright (C) 2010 SKEPSIS Consult SRL. All Rights Reserved.
@license - http://www.gnu.org/licenses/gpl-2.0.html GNU/GPL
Websites: http://www.thefactory.ro
Technical Support:  Forum - http://www.thefactory.ro/joomla-forum/
-------------------------------------------------------------------------*/

defined('_JEXEC') or die('Restricted access');

jimport('joomla.application.component.view');

class BackendViewFile extends JView
{
  function display($tpl = null)
  {
    $file      =& $this->get('Data');
    $walls      =& $this->get('Walls');
    
    $isNew      =  ($file->id < 1);
   
    $wallSettings = new wallSettings();

    $text = $isNew ? JText::_('New') : '"' . $file->id . '"';
    JToolBarHelper::title(JText::_('File').': <small><small>[ ' . $text.' ]</small></small>');

    JToolBarHelper::save();
    ($isNew) ? JToolBarHelper::cancel() : JToolBarHelper::cancel('cancel', 'Close');

    $this->assignRef('file',      	$file);
    $this->assignRef('walls',      	$walls);
    $this->assignRef('wallSettings', $wallSettings);

    parent::display($tpl);
  }
}