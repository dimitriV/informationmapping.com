<?php
/*------------------------------------------------------------------------
com_wallfactory - Wall Factory
------------------------------------------------------------------------
author    TheFactory
copyright Copyright (C) 2010 SKEPSIS Consult SRL. All Rights Reserved.
@license - http://www.gnu.org/licenses/gpl-2.0.html GNU/GPL
Websites: http://www.thefactory.ro
Technical Support:  Forum - http://www.thefactory.ro/joomla-forum/
-------------------------------------------------------------------------*/

defined('_JEXEC') or die('Restricted access'); ?>

<form action="index.php" method="post" name="adminForm" id="adminForm">

<table class="admintable" width="100%">
		<tr valign="top">
			<td>
				<fieldset>
					<legend><?php echo JText::_('Post'); ?></legend>

					<table width="100%" cellpadding="0" cellspacing="0">
						<?php if ($this->post->id): ?>
						  <tr>
							  <td class="key" width="20%" align="right"><label for="id"><?php echo JText::_('ID'); ?>:</label></td>
							  <td width="80%"><strong><?php echo $this->post->id; ?></strong></td>
						  </tr>
						<?php endif; ?>

						<!-- wall_id -->
						<tr>
							<td class="key" align="right"><label for="wall_id"><?php echo JText::_('Wall'); ?>:</label></td>
							<td>
							  <select name="wall_id" id="wall_id">
							    <?php foreach ($this->walls as $wall): ?>
							      <option value="0" <?php echo (0 == $this->post->wall_id) ? 'selected="selected"' : ''; ?>>
							        <?php echo 'No wall'; ?>
							      </option>
							      <option value="<?php echo $wall->id; ?>" <?php echo ($wall->id == $this->post->wall_id) ? 'selected="selected"' : ''; ?>>
							        <?php echo $wall->title; ?>
							      </option>
							    <?php endforeach; ?>
							  </select>
							</td>
						</tr>

						<!-- username -->
						<?php if ($this->post->id): ?>
						  <tr>
							  <td class="key" width="20%" align="right"><label for="user"><?php echo JText::_('Username'); ?>:</label></td>
							  <td width="80%"><strong><?php echo $this->post->username; ?></strong></td>
						  </tr>
						<?php endif; ?>

						<!-- enable_comments -->
						<tr>
							<td class="key" align="right"><label for="enable_comments"><?php echo JText::_('Enable comments'); ?>:</label></td>
							<td>
							  <select name="enable_comments" id="enable_comments">
							    <option value="0" <?php echo ($this->post->enable_comments == 0) ? 'selected="selected"' : ''; ?>><?php echo JText::_('No'); ?></option>
							    <option value="1" <?php echo ($this->post->enable_comments == 1) ? 'selected="selected"' : ''; ?>><?php echo JText::_('Yes'); ?></option>
							  </select>
							</td>
						</tr>

						<!-- content -->
						<tr>
							<td class="key" align="right" style="vertical-align: top;"><label for="content"><?php echo JText::_('Content'); ?>:</label></td>
							<td>
							  <textarea name="content" id="content" rows="12" cols="80" class="mceEditor"><?php echo $this->post->content; ?></textarea>
							</td>
						</tr>
					</table>
				</fieldset>
			</td>
		</tr>
	</table>

  <input type="hidden" name="controller" value="post" />
  <input type="hidden" name="id" value="<?php echo $this->post->id; ?>" />
  <input type="hidden" name="option" value="com_wallfactory" />
  <input type="hidden" name="task" value="" />
</form>