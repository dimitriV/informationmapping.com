<?php
/*------------------------------------------------------------------------
com_wallfactory - Wall Factory
------------------------------------------------------------------------
author    TheFactory
copyright Copyright (C) 2010 SKEPSIS Consult SRL. All Rights Reserved.
@license - http://www.gnu.org/licenses/gpl-2.0.html GNU/GPL
Websites: http://www.thefactory.ro
Technical Support:  Forum - http://www.thefactory.ro/joomla-forum/
-------------------------------------------------------------------------*/

defined('_JEXEC') or die('Restricted access');

jimport('joomla.application.component.view');

class BackendViewPost extends JView
{
  function display($tpl = null)
  {
    $post       =& $this->get('Data');
    $walls      =& $this->get('Walls');
    
    $isNew      =  ($post->id < 1);
   
    $wallSettings = new wallSettings();

    $text = $isNew ? JText::_('New') : '"' . $post->id . '"';
    JToolBarHelper::title(JText::_('Post').': <small><small>[ ' . $text.' ]</small></small>');

    JToolBarHelper::save();
    ($isNew) ? JToolBarHelper::cancel() : JToolBarHelper::cancel('cancel', 'Close');

    $this->assignRef('post',       $post);
    $this->assignRef('walls',      $walls);
    $this->assignRef('tags',       $tags);
    $this->assignRef('wallSettings', $wallSettings);

    parent::display($tpl);
  }
}