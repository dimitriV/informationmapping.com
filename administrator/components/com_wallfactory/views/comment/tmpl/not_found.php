<?php
/*------------------------------------------------------------------------
com_wallfactory - Wall Factory
------------------------------------------------------------------------
author    TheFactory
copyright Copyright (C) 2010 SKEPSIS Consult SRL. All Rights Reserved.
@license - http://www.gnu.org/licenses/gpl-2.0.html GNU/GPL
Websites: http://www.thefactory.ro
Technical Support:  Forum - http://www.thefactory.ro/joomla-forum/
-------------------------------------------------------------------------*/<br>

defined('_JEXEC') or die('Restricted access'); ?>

<h3><?php echo JText::_('Comment not found'); ?></h3>
<p><?php echo JText::_('Return to'); ?> <a href="javascript: history.go(-1);"><?php echo JText::_('previous page'); ?></a></p>