<?php
/*------------------------------------------------------------------------
com_wallfactory - Wall Factory
------------------------------------------------------------------------
author    TheFactory
copyright Copyright (C) 2010 SKEPSIS Consult SRL. All Rights Reserved.
@license - http://www.gnu.org/licenses/gpl-2.0.html GNU/GPL
Websites: http://www.thefactory.ro
Technical Support:  Forum - http://www.thefactory.ro/joomla-forum/
-------------------------------------------------------------------------*/

defined('_JEXEC') or die('Restricted access'); ?>

<form action="index.php" method="post" name="adminForm" id="adminForm">

<table class="admintable" width="100%">
		<tr valign="top">
			<td>
				<fieldset>
					<legend><?php echo JText::_('Comment'); ?></legend>

					<table width="100%">
						<tr>
						  <td class="key" width="20%" align="right"><?php echo JText::_('ID'); ?>:</td>
						  <td width="80%"><strong><?php echo $this->comment->id; ?></strong></td>
						</tr>

						<!-- approved -->
						<tr>
							<td class="key" align="right"><label for="approved"><?php echo JText::_('Approved'); ?>:</label></td>
							<td>
							  <select name="approved" id="approved">
							    <option value="0" <?php echo ($this->comment->approved == 0) ? 'selected="selected"' : ''; ?>><?php echo JText::_('No'); ?></option>
							    <option value="1" <?php echo ($this->comment->approved == 1) ? 'selected="selected"' : ''; ?>><?php echo JText::_('Yes'); ?></option>
							  </select>
							</td>
						</tr>

						<!-- published -->
						<tr>
							<td class="key" align="right"><label for="published"><?php echo JText::_('Published'); ?>:</label></td>
							<td>
							  <select name="published" id="published">
							    <option value="0" <?php echo ($this->comment->published == 0) ? 'selected="selected"' : ''; ?>><?php echo JText::_('No'); ?></option>
							    <option value="1" <?php echo ($this->comment->published == 1) ? 'selected="selected"' : ''; ?>><?php echo JText::_('Yes'); ?></option>
							  </select>
							</td>
						</tr>

						<!-- content -->
						<tr>
							<td class="key" align="right" style="vertical-align: top;"><label for="content"><?php echo JText::_('Comment'); ?>:</label></td>
							<td>
							  <textarea name="content" id="content" rows="5" cols="40"><?php echo $this->comment->content; ?></textarea>
							</td>
						</tr>

					</table>
				</fieldset>
			</td>
		</tr>
	</table>

  <input type="hidden" name="controller" value="comment" />
  <input type="hidden" name="id" value="<?php echo $this->comment->id; ?>" />
  <input type="hidden" name="option" value="com_wallfactory" />
  <input type="hidden" name="task" value="" />
</form>