<?php
/*------------------------------------------------------------------------
com_wallfactory - Wall Factory
------------------------------------------------------------------------
author    TheFactory
copyright Copyright (C) 2010 SKEPSIS Consult SRL. All Rights Reserved.
@license - http://www.gnu.org/licenses/gpl-2.0.html GNU/GPL
Websites: http://www.thefactory.ro
Technical Support:  Forum - http://www.thefactory.ro/joomla-forum/
-------------------------------------------------------------------------*/

defined('_JEXEC') or die('Restricted access');

jimport('joomla.application.component.view');

class BackendViewUrls extends JView
{
  function display($tpl = null)
  {
    JToolBarHelper::title(JText::_('Urls'), 'generic.png');

    $bar =& JToolBar::getInstance('toolbar');
    //JToolBarHelper::addNew();
    JToolBarHelper::editList();
    JToolBarHelper::deleteList(JText::_('Are you sure?'));

    $urls      =& $this->get('Data');
    $pagination =& $this->get('Pagination');
    $lists      =& $this->_getViewLists();

    $this->assignRef('urls',     	$urls);
    $this->assignRef('pagination', 	$pagination);
    $this->assignRef('lists',      	$lists);

    // Javascripts
    JHtmlFactory::jQueryScript();
    JHTML::script('modal.js',             'media/system/js/');
    JHTML::script('jquery-1.3.2.min.js',  'components/com_wallfactory/assets/js/');
   // JHTML::script('jquery.noConflict.js', 'components/com_wallfactory/assets/js/');

    // Stylesheets
    JHTML::stylesheet('modal.css', 'media/system/css/');

    parent::display($tpl);
  }

  function &_getViewLists()
	{
		global $mainframe, $option;

		$filter_order     	= $mainframe->getUserStateFromRequest($option . '.urls.filter_order',     'filter_order',     'title', 'cmd');
    	$filter_order_Dir 	= $mainframe->getUserStateFromRequest($option . '.urls.filter_order_Dir', 'filter_order_Dir', 'asc',   'word');
    	$filter_state		= $mainframe->getUserStateFromRequest($option . '.urls.filter_state',		 'filter_state',		 '',			'word');
    	$search				= $mainframe->getUserStateFromRequest($option . '.urls.search',			     'search',			     '',			'string');
		$search				= JString::strtolower($search);

		// table ordering
		$lists['order_Dir']	= $filter_order_Dir;
		$lists['order']		  = $filter_order;

		// search
		$lists['search'] = $search;
		
		// state
		$lists['state'] = $filter_state;

		return $lists;
	}
}