<?php
/*------------------------------------------------------------------------
com_wallfactory - Wall Factory
------------------------------------------------------------------------
author    TheFactory
copyright Copyright (C) 2010 SKEPSIS Consult SRL. All Rights Reserved.
@license - http://www.gnu.org/licenses/gpl-2.0.html GNU/GPL
Websites: http://www.thefactory.ro
Technical Support:  Forum - http://www.thefactory.ro/joomla-forum/
-------------------------------------------------------------------------*/

defined('_JEXEC') or die('Restricted access');

jimport('joomla.application.component.view');

class BackendViewUsers extends JView
{
  function display($tpl = null)
  {
    JToolBarHelper::title(JText::_('Users'), 'generic.png');
    
    JToolBarHelper::editList();
    JToolBarHelper::spacer();
    JToolBarHelper::publishList();
    JToolBarHelper::unpublishList();
    JToolBarHelper::spacer();
    JToolBarHelper::unpublishList('unbanList', JText::_('Unban'));
    JToolBarHelper::publishList('banList', JText::_('Ban'));
    JToolBarHelper::deleteList(JText::_('Are you sure you want to delete all wall(s) content?'));

    $users      =& $this->get('Data');
    $guests		=& $this->get('DataGuest');
    $pagination =& $this->get('Pagination');
    $lists      =& $this->_getViewLists();
    
    $this->assignRef('users',      $users);
    $this->assignRef('guests',      $guests);
    $this->assignRef('pagination', $pagination);
    $this->assignRef('lists',      $lists);

    JHTML::script('modal.js',             'media/system/js/');
    //JHTML::script('jquery.noConflict.js', 'components/com_wallfactory/assets/js/');
    
    // Stylesheets
    JHTML::stylesheet('modal.css', 'media/system/css/');
    
    parent::display($tpl);
  }

  function &_getViewLists()
	{
		global $mainframe, $option;

		$filter_order     	= $mainframe->getUserStateFromRequest($option . '.users.filter_order',     'filter_order',     'title', 'cmd');
	    $filter_order_Dir 	= $mainframe->getUserStateFromRequest($option . '.users.filter_order_Dir', 'filter_order_Dir', 'asc',   'word');
	    $banned		        = $mainframe->getUserStateFromRequest($option . '.users.banned',		   'banned',		   '',		'int');
	    $wall_owner		    = $mainframe->getUserStateFromRequest($option . '.users.wall_owner',	   'wall_owner',	   '',		'string');
	    $filter_state		= $mainframe->getUserStateFromRequest($option . '.users.filter_state',	   'filter_state',	   '',		'word' );
	    $search				= $mainframe->getUserStateFromRequest($option . '.users.search',		   'search',		   '',		'string');
	    $search				= JString::strtolower($search);

		// table ordering
		$lists['order_Dir']	= $filter_order_Dir;
		$lists['order']		= $filter_order;

		// search
		$lists['search'] = $search;
		// state
		$lists['state'] = $filter_state;
		
		// wall owner
		$lists['wall_owner'] = $wall_owner;
		
		$lists['banned'] = $banned;

		$lists['bans']   = array();
	    $lists['bans'][] = JHTML::_('select.option', '-1', JText::_('- Select Banned -'));
	    $lists['bans'][] = JHTML::_('select.option', '0', JText::_('Not banned'));
    	$lists['bans'][] = JHTML::_('select.option', '1', JText::_('Banned'));

		return $lists;
	}
}