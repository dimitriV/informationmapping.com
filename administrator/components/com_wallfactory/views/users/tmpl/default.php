<?php
/*------------------------------------------------------------------------
com_wallfactory - Wall Factory
------------------------------------------------------------------------
author    TheFactory
copyright Copyright (C) 2010 SKEPSIS Consult SRL. All Rights Reserved.
@license - http://www.gnu.org/licenses/gpl-2.0.html GNU/GPL
Websites: http://www.thefactory.ro
Technical Support:  Forum - http://www.thefactory.ro/joomla-forum/
-------------------------------------------------------------------------*/

defined('_JEXEC') or die('Restricted access'); ?>
<?php JHTML::_('behavior.modal'); 
JHTML::_('behavior.mootools'); ?>
<form action="index.php" method="post" name="adminForm" style="width: 98%; margin: 0px auto;">

	<table>
		<tr>
			<td align="left" width="100%">
				<label for="search"><?php echo JText::_('Filter'); ?>:</label>
				<input type="text" id="search" name="search" id="search" value="<?php echo $this->lists['search'];?>" class="text_area" onchange="document.adminForm.submit();" />
				<button onclick="this.form.submit();"><?php echo JText::_('Go'); ?></button>
				<button onclick="document.getElementById('search').value='';this.form.getElementById('filter_state').value='';this.form.submit();"><?php echo JText::_('Reset'); ?></button>
			</td>
			<td>
			  	<?php echo JHTML::_('select.genericlist', $this->lists['bans'],        'banned',     'size="1" onchange="document.adminForm.submit();"', 'value', 'text', $this->lists['banned']); ?>
			</td>
			<td>
		  		<?php echo JHTML::_('grid.state', $this->lists['state']); ?>
			</td>
		</tr>
		
	</table>

<table class="adminlist">
	<thead>
		<tr>
			<th width="20px"><?php echo JText::_('NUM'); ?></th>
			<th width="20px"><input type="checkbox" name="toggle" value="" onclick="checkAll(<?php echo count($this->users); ?>);" /></th>
			<th width="15%" style="text-align: left;"><?php echo JHTML::_('grid.sort', JText::_('Name'),     	'm.name', $this->lists['order_Dir'], $this->lists['order']); ?></th>
			<th width="13%" style="text-align: left;"><?php echo JHTML::_('grid.sort', JText::_('Username'), 	'u.username', $this->lists['order_Dir'], $this->lists['order']); ?></th>
			<th width="9%"><?php echo JHTML::_('grid.sort', JText::_('Created at'), 	'w.date_created', $this->lists['order_Dir'], $this->lists['order']); ?></th>
			<th width="7%"><?php echo JHTML::_('grid.sort', JText::_('Posts'), 			's.posts', $this->lists['order_Dir'], $this->lists['order']); ?></th>
			<th width="7%"><?php echo JHTML::_('grid.sort', JText::_('Links'), 			's.urls', $this->lists['order_Dir'], $this->lists['order']); ?></th>
			<th width="7%"><?php echo JHTML::_('grid.sort', JText::_('Videos'), 		's.video_links', $this->lists['order_Dir'], $this->lists['order']); ?></th>
			<th width="7%"><?php echo JHTML::_('grid.sort', JText::_('Images'), 		's.images', $this->lists['order_Dir'], $this->lists['order']); ?></th>
			<th width="7%"><?php echo JHTML::_('grid.sort', JText::_('Mp3 files'), 		's.mp3', $this->lists['order_Dir'], $this->lists['order']); ?></th>
			<th width="7%"><?php echo JHTML::_('grid.sort', JText::_('Files'), 			's.files', $this->lists['order_Dir'], $this->lists['order']); ?></th>
			<th width="10%"><?php echo JHTML::_('grid.sort', JText::_('Banned'),  		's.banned', $this->lists['order_Dir'], $this->lists['order']); ?></th>
			<th width="8%"><?php echo JHTML::_('grid.sort', JText::_('Published'), 		'w.published', $this->lists['order_Dir'], $this->lists['order']); ?></th>
		</tr>
	</thead>

	<tfoot>
		<tr>
			<td colspan="14">
				<?php echo $this->pagination->getListFooter(); ?>
			</td>
		</tr>
	</tfoot>

	<tbody>
		<tr>
		  <td width="20px"><?php echo JText::_('0'); ?></td>
	      <td width="20px" style="text-align: center;"><?php echo JText::_('#'); ?></td>
	      <td>&nbsp;</td>
	      <td><?php echo $this->guests[0]->username; ?></td> 		
		  <td>&nbsp;</td>
		  <td style="text-align: center;"><?php echo $this->guests[0]->posts; ?></td>
	      
	      <td style="text-align: center;"><?php echo $this->guests[0]->urls; ?></td>
	      <td style="text-align: center;"><?php echo $this->guests[0]->video_links; ?></td>
	      <td style="text-align: center;"><?php echo $this->guests[0]->images; ?></td>
	      <td style="text-align: center;"><?php echo $this->guests[0]->mp3; ?></td>
	      <td style="text-align: center;"><?php echo $this->guests[0]->files; ?></td>
	      <td>&nbsp;</td>
	      <td>&nbsp;</td>
		</tr>
	
	  <?php foreach ($this->users as $i => $user): ?>
	    <tr class="row<?php echo $i % 2; ?>">
	      <td width="20px"><?php echo ($i + 1 + $this->pagination->limitstart); ?></td>
	      <td width="20px"><?php echo JHTML::_('grid.id', $i, $user->user_id); ?></td>
	      <td><a href="<?php echo JRoute::_('index.php?option=com_wallfactory&controller=user&task=dashboard&cid[]=' . $user->user_id); ?>"><?php echo $user->name; ?></a></td>
	      <td>
	       <?php if ($user->user_id == 0): ?>
	       		<?php echo $user->username; ?>
	       <?php else: ?>
	      		<a href="<?php echo JRoute::_('index.php?option=com_wallfactory&controller=wall&task=edit&cid[]=' . $user->wall_id); ?>"><?php echo $user->username; ?></a>
	       <?php endif; ?>
	      </td>
	      <td style="text-align: center;"><?php echo $user->date_created; ?></td>
	      <td style="text-align: center;">
	      	<a title="posts" class="modal" rel="{handler: 'iframe', size: {x: 800, y: 700}}" href="index.php?option=com_wallfactory&format=raw&controller=user&task=posts&cid[]=<?php echo $user->user_id; ?>" name="show_posts" id="postsButton" >
		<span class="icon-32-print"></span><?php echo $user->posts; ?></a>
	      </td>
	      
	      <td style="text-align: center;">
	      	<a title="links" class="modal" rel="{handler: 'iframe', size: {x: 800, y: 700}}" href="index.php?option=com_wallfactory&format=raw&controller=user&task=urls&cid[]=<?php echo $user->user_id; ?>" name="show_urls" id="urlsButton" >
		<span class="icon-32-print"></span><?php echo $user->urls; ?></a>
		  </td>
	      <td style="text-align: center;">
	      	<a title="videos" class="modal" rel="{handler: 'iframe', size: {x: 800, y: 700}}" href="index.php?option=com_wallfactory&format=raw&controller=user&task=videos&cid[]=<?php echo $user->user_id; ?>" name="show_videos" id="videosButton" >
		<span class="icon-32-print"></span><?php echo $user->video_links; ?></a>
		  </td>
	      <td style="text-align: center;">
	      	<a title="images" class="modal" rel="{handler: 'iframe', size: {x: 800, y: 700}}" href="index.php?option=com_wallfactory&format=raw&controller=user&task=images&cid[]=<?php echo $user->user_id; ?>" name="show_images" id="imagesButton" >
		<span class="icon-32-print"></span><?php echo $user->images; ?></a>
		  </td>
	      <td style="text-align: center;">
	      	<a title="mp3Files" class="modal" rel="{handler: 'iframe', size: {x: 800, y: 700}}" href="index.php?option=com_wallfactory&format=raw&controller=user&task=mp3files&cid[]=<?php echo $user->user_id; ?>" name="show_mp3" id="mp3Button" >
		<span class="icon-32-print"></span><?php echo $user->mp3; ?></a>
		  </td>
	      <td style="text-align: center;">
	      	<a title="files" class="modal" rel="{handler: 'iframe', size: {x: 800, y: 700}}" href="index.php?option=com_wallfactory&format=raw&controller=user&task=files&cid[]=<?php echo $user->user_id; ?>" name="show_images" id="filesButton" >
		<span class="icon-32-print"></span><?php echo $user->files; ?></a>
		  </td>
	      <td style="text-align: center;"><a href="<?php echo JRoute::_('index.php?option=com_wallfactory&controller=user&task=' . (($user->banned) ? 'unban' : 'ban' ) . '&value=&id=' . $user->user_id); ?>"><img src="images/<?php echo $user->banned ? 'tick' : 'publish_x'; ?>.png" /></a></td>
	      <td style="text-align: center;"><a href="<?php echo JRoute::_('index.php?option=com_wallfactory&controller=wall&task=change&field=published&value=' . !$user->published . '&id=' . $user->wall_id); ?>"><img src="images/<?php echo $user->published ? 'tick' : 'publish_x'; ?>.png" /></a></td>
	    </tr>
	  <?php endforeach; ?>
	</tbody>
</table>

  <input type="hidden" name="controller" value="user" />
  <input type="hidden" name="boxchecked" value="0" />
  <input type="hidden" name="option" value="com_wallfactory" />
  <input type="hidden" name="task" value="users" />
  <input type="hidden" name="filter_order" value="<?php echo $this->lists['order']; ?>" />
	<input type="hidden" name="filter_order_Dir" value="<?php echo $this->lists['order_Dir']; ?>" />
</form>