<?php
/*------------------------------------------------------------------------
com_wallfactory - Wall Factory
------------------------------------------------------------------------
author    TheFactory
copyright Copyright (C) 2010 SKEPSIS Consult SRL. All Rights Reserved.
@license - http://www.gnu.org/licenses/gpl-2.0.html GNU/GPL
Websites: http://www.thefactory.ro
Technical Support:  Forum - http://www.thefactory.ro/joomla-forum/
-------------------------------------------------------------------------*/

defined('_JEXEC') or die('Restricted access');

jimport('joomla.application.component.view');
jimport('joomla.html.pane');

class BackendViewReports extends JView
{
  function display($tpl = null)
  {
    JToolBarHelper::title(JText::_('Reports'), 'generic.png');
    
  	$document =& JFactory::getDocument();
    $document->addScriptDeclaration('var root = "' . JUri::root() . '";');
 	
    JHtmlFactory::jQueryScript();
    JHTML::_('script', 'jquery.noconflict.js', 'components/com_wallfactory/assets/js/');
    
	$post_reports       =& $this->get('ReportsPosts');
    $post_pagination    =& $this->get('PaginationPosts');
	$pane   =& JPane::getInstance('tabs', array('startOffset' => 0));
        
    $comment_reports       =& $this->get('ReportsComments');
    $comment_pagination    =& $this->get('PaginationComments');

    $this->assignRef('pane',         		$pane);
    $this->assignRef('post_reports',    	$post_reports);
    $this->assignRef('post_pagination', 	$post_pagination);
    $this->assignRef('comment_reports',    	$comment_reports);
    $this->assignRef('comment_pagination', 	$comment_pagination);
    
    $pane   =& JPane::getInstance('tabs', array('startOffset' => 0));
	
  	parent::display($tpl);
  }

 }