<?php
/*------------------------------------------------------------------------
com_wallfactory - Wall Factory
------------------------------------------------------------------------
author    TheFactory
copyright Copyright (C) 2010 SKEPSIS Consult SRL. All Rights Reserved.
@license - http://www.gnu.org/licenses/gpl-2.0.html GNU/GPL
Websites: http://www.thefactory.ro
Technical Support:  Forum - http://www.thefactory.ro/joomla-forum/
-------------------------------------------------------------------------*/

defined('_JEXEC') or die('Restricted access'); ?>

<script>
  jQueryFactory(document).ready(function($){
    $('#tab-comments div.loading').hide();

    $('#pagination-comments a').each( function () {
      var limitstart = $(this).attr('onclick');
      limitstart += '';
      limitstart = limitstart.match(/[0-9]{1,10}/);
      $(this).attr('onclick', '');
      $(this).attr('id', limitstart);
    });

    $('#pagination-comments a').click( function () {
      $('#tab-comments div.loading').show();
      $('#comments-update').load('index.php?option=com_wallfactory&task=reportscomments&limitstart=' + $(this).attr('id') + '&format=raw');

      return false;
    });

    $('.delete-comment').click( function () {
      if (confirm('Are you sure you want to delete this comment?'))
      {
        $(this).parent().load($(this).attr('href'));
        $(this).parent().html($('#wallfactory-loading').html());
      }
      return false;
    });

  });
</script>

<style>
  td.left { text-align: left !important; }
  td.center { text-align: center !important; }
</style>

<div id="wallfactory-loading" style="display: none;"><img src="<?php echo JURI::root(); ?>components/com_wallfactory/assets/images/ajax-loader.gif" class="wallfactory-icon" /><?php echo JText::_('Loading, please wait...'); ?></div>

<table class="paramlist admintable">
  <?php if(count($this->comment_reports) > 0):   ?>
	  <thead>
	    <th class="left">Comment author</th>
	    <th class="left">Comment</th>
	    <th>Delete comment</th>
	    <th>Delete report</th>
	  </thead>
	  <tbody>
  
    <?php foreach ($this->comment_reports as $comment): ?>
      <tr>
        <td class="paramlist_key left" style="width: 400px;"><?php echo $comment->author_alias; ?></td>
        <td class="paramlist_key left" style="width: 400px;"><?php echo $comment->content; ?></td>
        <td class="paramlist_key center"><img src="<?php echo JURI::root(); ?>components/com_wallfactory/assets/images/comment_delete.png" class="wallfactory-icon" /><a href="index.php?option=com_wallfactory&task=deletecomment&format=raw&type=2&id=<?php echo $comment->id; ?>" class="delete-comment"><?php echo JText::_(' delete comment'); ?></a></td>
        <td class="paramlist_key center"><img src="<?php echo JURI::root(); ?>components/com_wallfactory/assets/images/delete.png" class="wallfactory-icon" /><a href="index.php?option=com_wallfactory&task=deletereport&format=raw&type=2&id=<?php echo $comment->id; ?>" class="delete-comment"><?php echo JText::_(' delete report'); ?></a></td>
      </tr>
    <?php endforeach; ?>
  <?php else: ?>
  	  <tr>
        <td style="border-left:1px solid #E9E9E9;  font-weight: bold; width: 200px; border-bottom:1px solid #E9E9E9; color:#666666;"><?php echo JText::_('There are no reports'); ?></td>
      </tr>  
        
  <?php endif; ?>
  </tbody>
</table>

<div id="pagination-comments"><?php echo $this->comment_pagination->getPagesLinks(); ?></div>