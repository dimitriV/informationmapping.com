<?php
/*------------------------------------------------------------------------
com_wallfactory - Wall Factory
------------------------------------------------------------------------
author    TheFactory
copyright Copyright (C) 2010 SKEPSIS Consult SRL. All Rights Reserved.
@license - http://www.gnu.org/licenses/gpl-2.0.html GNU/GPL
Websites: http://www.thefactory.ro
Technical Support:  Forum - http://www.thefactory.ro/joomla-forum/
-------------------------------------------------------------------------*/

defined('_JEXEC') or die('Restricted access'); ?>

<script>
  jQueryFactory(document).ready(function($){

    var JTooltips = new Tips($$('.hasTip'), { maxTitleChars: 50, fixed: true });

    $('#tab-posts div.loading').hide();

    $('#pagination-posts a').each( function () {
      var limitstart = $(this).attr('onclick');
      limitstart += '';
      limitstart = limitstart.match(/[0-9]{1,10}/);
      $(this).attr('onclick', '');
      $(this).attr('id', limitstart);
    });

    $('#pagination-posts a').click( function () {
      $('#tab-posts div.loading').show();
      $('#posts-update').load('index.php?option=com_wallfactory&task=reportsposts&limitstart=' + $(this).attr('id') + '&format=raw');

      $('.tool-tip').remove();

      return false;
    });

    $('.delete-post').click( function () {
      if (confirm('Are you sure you want to delete this post?'))
      {
        $(this).parent().load($(this).attr('href'));
        $(this).parent().html($('#wallfactory-loading').html());
      }
      return false;
    });

    $('.delete-post-report').click( function () {
      if (confirm('Are you sure you want to delete this report?'))
      {
        $(this).parent().load($(this).attr('href'));
        $(this).parent().html($('#wallfactory-loading').html());
      }
      return false;
    });

  });
</script>

<style>
  td.left   { text-align: left !important; }
  td.center { text-align: center !important; }
  .tool-tip { max-width: <?php echo $this->settings->image_max_width; ?>px; }
</style>

<div id="wallfactory-loading" style="display: none;"><img src="<?php echo JURI::root(); ?>components/com_wallfactory/assets/images/ajax-loader.gif" class="wallfactory-icon" /><?php echo JText::_('Loading, please wait...'); ?></div>

<table class="paramlist admintable">
<?php if(count($this->post_reports) > 0):   ?>
  <thead>
    <th class="left">Date</th>
    <th class="left">Post author</th>
    <th class="left">Content</th>
   <!-- <th>Report</th> --> 
    <th class="left">Frontend Link</th>
    <th>Delete Post</th>
    <th>Delete Report</th>
  </thead>
  <tbody>
  
    <?php foreach ($this->post_reports as $post): ?>
     
      <tr>
        <td class="paramlist_key left"><?php echo $post->date_created; ?></td>
        <td class="paramlist_key left"><?php echo $post->username; ?></td>
        <td style="font-weight: normal;" class="paramlist_key left"><?php echo $post->content; ?></td>
        <td class="paramlist_key left">
        	<a href="<?php echo JURI::root(); ?>index.php?option=com_wallfactory&view=post&id=<?php echo $post->id ;?>&alias=<?php echo $post->alias ;?>" target="_blank"><?php echo JText::_('link'); ?></a>
        </td>
        <td class="paramlist_key center"><img src="<?php echo JURI::root(); ?>components/com_wallfactory/assets/images/image_delete.png" class="wallfactory-icon" /><a href="index.php?option=com_wallfactory&task=deletepost&format=raw&type=1&id=<?php echo $post->id; ?>" class="delete-post"><?php echo JText::_('delete post'); ?></a></td>
        <td class="paramlist_key center"><img src="<?php echo JURI::root(); ?>components/com_wallfactory/assets/images/delete.png" class="wallfactory-icon" /><a href="index.php?option=com_wallfactory&task=deletereport&format=raw&type=1&id=<?php echo $post->id; ?>" class="delete-post-report"><?php echo JText::_('remove report'); ?></a></td>
      </tr>
    <?php endforeach; ?>
  <?php else: ?>
  	  <tr>
        <td style="border-left:1px solid #E9E9E9;  font-weight: bold; width: 200px; border-bottom:1px solid #E9E9E9; color:#666666;"><?php echo JText::_('There are no reports'); ?></td>
      </tr>  
        
  <?php endif; ?>
  </tbody>
</table>

<div id="pagination-posts"><?php echo $this->post_pagination->getPagesLinks(); ?></div>