<?php
/*------------------------------------------------------------------------
com_wallfactory - Wall Factory
------------------------------------------------------------------------
author    TheFactory
copyright Copyright (C) 2010 SKEPSIS Consult SRL. All Rights Reserved.
@license - http://www.gnu.org/licenses/gpl-2.0.html GNU/GPL
Websites: http://www.thefactory.ro
Technical Support:  Forum - http://www.thefactory.ro/joomla-forum/
-------------------------------------------------------------------------*/

defined('_JEXEC') or die('Restricted access'); ?>

<?php JHTML::_('script', 'jquery-ui-tabs-factory.js', 'components/com_wallfactory/assets/js/'); ?>

<script>
  jQueryFactory(document).ready(function($){
    $('#posts-update').load('index.php?option=com_photobattle&task=reportsposts&limitstart=0&format=raw');
    $('#comments-update').load('index.php?option=com_photobattle&task=reportscomments&limitstart=0&format=raw');
  });
</script>

<?php JToolBarHelper::title(JText::_('Reports'), 'generic.png'); ?>
<div id="tabs">
<?php echo $this->pane->startPane('pane'); ?>

  <?php echo $this->pane->startPanel(JText::_('Post messages'), 'posts'); ?>
    <fieldset>
      <legend><?php echo JText::_('Post messages'); ?></legend>
      <?php require_once('default_posts.php'); ?>
    </fieldset>
 <?php echo $this->pane->endPanel(); ?>
 
  <?php echo $this->pane->startPanel(JText::_('Comments'), 'comments'); ?>
    <fieldset>
      <legend><?php echo JText::_('Comments', 'comments'); ?></legend>
      <?php require_once('default_comments.php'); ?>
    </fieldset>
  <?php echo $this->pane->endPanel(); ?>

<?php echo $this->pane->endPane(); ?>

</div>