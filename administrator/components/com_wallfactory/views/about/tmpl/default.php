<?php
/*------------------------------------------------------------------------
com_wallfactory - Wall Factory
------------------------------------------------------------------------
author    TheFactory
copyright Copyright (C) 2010 SKEPSIS Consult SRL. All Rights Reserved.
@license - http://www.gnu.org/licenses/gpl-2.0.html GNU/GPL
Websites: http://www.thefactory.ro
Technical Support:  Forum - http://www.thefactory.ro/joomla-forum/
-------------------------------------------------------------------------*/

defined('_JEXEC') or die('Restricted access'); ?>

<?php foreach ($this->information as $name => $text): ?>
  <?php if (!empty($text)): ?>
    <div style="padding: 10px;">
      <h1 style="text-decoration: underline; "><?php echo JText::_($name); ?></h1>
      <?php if ($name == 'latestversion'): ?>
        <?php require_once('_version.php'); ?>
      <?php else: ?>
        <?php echo $text; ?>
      <?php endif; ?>
    </div>
  <?php endif; ?>
<?php endforeach; ?>