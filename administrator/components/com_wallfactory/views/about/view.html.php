<?php
/*------------------------------------------------------------------------
com_wallfactory - Wall Factory
------------------------------------------------------------------------
author    TheFactory
copyright Copyright (C) 2010 SKEPSIS Consult SRL. All Rights Reserved.
@license - http://www.gnu.org/licenses/gpl-2.0.html GNU/GPL
Websites: http://www.thefactory.ro
Technical Support:  Forum - http://www.thefactory.ro/joomla-forum/
-------------------------------------------------------------------------*/

defined('_JEXEC') or die('Restricted access');

jimport('joomla.application.component.view');
jimport('joomla.html.pane');

class BackendViewAbout extends JView
{
  function display($tpl = null)
  {
    JToolBarHelper::title(JText::_('About'), 'generic.png');

    $information     =& $this->get('Information');
    $current_version =& $this->get('Version');
    $new_version      = (intval(str_replace('.', '', $current_version)) < intval(str_replace('.', '', $information['latestversion'])));

    $this->assignRef('information',     $information);
    $this->assignRef('new_version',     $new_version);
    $this->assignRef('current_version', $current_version);

    parent::display($tpl);
  }
}