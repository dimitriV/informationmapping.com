<?php
/*------------------------------------------------------------------------
com_wallfactory - Wall Factory
------------------------------------------------------------------------
author    TheFactory
copyright Copyright (C) 2010 SKEPSIS Consult SRL. All Rights Reserved.
@license - http://www.gnu.org/licenses/gpl-2.0.html GNU/GPL
Websites: http://www.thefactory.ro
Technical Support:  Forum - http://www.thefactory.ro/joomla-forum/
-------------------------------------------------------------------------*/

defined('_JEXEC') or die('Restricted access');

define("COMPONENT_HOME_PAGE","http://www.thefactory.ro/");
define("COMPONENT_VERSION","1.1.7");
define("COMPONENT_NAME","com_wallfactory");
define("WF_COMPONENT_PATH",JPATH_ADMINISTRATOR.DS."components".DS."com_wallfactory".DS);

require_once (JPATH_COMPONENT.DS.'controller.php');

if ($controller = JRequest::getCmd('controller'))
{
	require_once (JPATH_COMPONENT.DS.'controllers'.DS.$controller.'.php');
}

$classname = 'BackendController'.$controller;
$controller = new $classname();

$controller->execute(JRequest::getVar('task'));

$controller->redirect();
