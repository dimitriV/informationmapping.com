<?php
/*------------------------------------------------------------------------
com_wallfactory - Wall Factory
------------------------------------------------------------------------
author    TheFactory
copyright Copyright (C) 2010 SKEPSIS Consult SRL. All Rights Reserved.
@license - http://www.gnu.org/licenses/gpl-2.0.html GNU/GPL
Websites: http://www.thefactory.ro
Technical Support:  Forum - http://www.thefactory.ro/joomla-forum/
-------------------------------------------------------------------------*/

defined('_JEXEC') or die('Restricted access');

jimport('joomla.application.component.controller');

class BackendController extends JController
{
  function __construct()
  {
    parent::__construct();

    require_once(JPATH_COMPONENT_ADMINISTRATOR.DS.'wallSettings.class.php');
    require_once(JPATH_COMPONENT_ADMINISTRATOR.DS.'lib'.DS.'factory'.DS.'wallHelper.class.php');
	require_once(JPATH_COMPONENT_ADMINISTRATOR.DS.'lib'.DS.'factory'.DS.'html.php');
 
  }

  function display()
  {
    parent::display();
  }

  function dashboard()
  {
    $view  =& $this->getView('dashboard', 'html');
    $model =& $this->getModel('dashboard');

    $view->setModel($model, true);
    $view->display();
  }

  /*function walls()
  {
    $view  =& $this->getView('walls', 'html');
    $model =& $this->getModel('walls');

    $view->setModel($model, true);
    $view->display();
  }*/

  function posts()
  {
    $view  =& $this->getView('posts', 'html');
    $model =& $this->getModel('posts');

    $view->setModel($model, true);
    $view->display();
  }
  
  function videos()
  {
    $view  =& $this->getView('videos', 'html');
    $model =& $this->getModel('videos');

    $view->setModel($model, true);
    $view->display();
  }

  function urls()
  {
    $view  =& $this->getView('urls', 'html');
    $model =& $this->getModel('urls');

    $view->setModel($model, true);
    $view->display();
  }
  
  function images()
  {
    $view  =& $this->getView('images', 'html');
    $model =& $this->getModel('images');

    $view->setModel($model, true);
    $view->display();
  }

  function mp3files()
  {
    $view  =& $this->getView('mp3files', 'html');
    $model =& $this->getModel('mp3files');

    $view->setModel($model, true);
    $view->display();
  }

  function reports()
  {
    $view  =& $this->getView('reports', 'html');
    $model =& $this->getModel('reports');

    $view->setModel($model, true);
    $view->display();
  }
  
  function deletereport()
  {
    $view  =& $this->getView('deletereport', 'raw');
    $model =& $this->getModel('deletereport');

    $view->setModel($model, true);
    $view->display();
  }
  
  function deletecomment()
  {
    $view  =& $this->getView('deletecomment', 'raw');
    $model =& $this->getModel('deletecomment');

    $view->setModel($model, true);
    $view->display();
  }
  
  function files()
  {
    $view  =& $this->getView('files', 'html');
    $model =& $this->getModel('files');

    $view->setModel($model, true);
    $view->display();
  }

  function settings()
  {
    $view  =& $this->getView('settings', 'html');
    $model =& $this->getModel('settings');

    $view->setModel($model, true);
    $view->display();
  }

  function bookmarking()
  {
    $view  =& $this->getView('bookmarks', 'html');
    $model =& $this->getModel('bookmarks');

    $view->setModel($model, true);
    $view->display();
  }


  function users()
  {
    $view  =& $this->getView('users', 'html');
    $model =& $this->getModel('users');

    $view->setModel($model, true);
    $view->display();
  }

  function comments()
  {
    $view  =& $this->getView('comments', 'html');
    $model =& $this->getModel('comments');

    $view->setModel($model, true);
    $view->display();
  }
  
  function about(){
		require_once(JPATH_ADMINISTRATOR."/components/com_wallfactory/version_info.php");
		$version=new JTheFactoryAbout;
		$version->GetInfo();
		$version->showAbout();
  }	
  
  function backup()
  {
    $model =& $this->getModel('backup');

    if (!$model->create())
    {
      $msg = JText::_('Backup Error!');
    }

    $this->setRedirect('index.php?option=com_wallfactory&task=settings', $msg);
  }

  function restore()
  {
    $model =& $this->getModel('backup');

    if ($model->restore())
    {
      $msg = JText::_('Backup Restored Successfully!');
    }
    else
    {
      $msg = JText::_('Error Restoring Backup!');
    }

    $this->setRedirect('index.php?option=com_wallfactory&task=settings', $msg);
  }
}