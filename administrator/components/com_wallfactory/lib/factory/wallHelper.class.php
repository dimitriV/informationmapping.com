<?php
/*------------------------------------------------------------------------
com_wallfactory - Wall Factory
------------------------------------------------------------------------
author    TheFactory
copyright Copyright (C) 2010 SKEPSIS Consult SRL. All Rights Reserved.
@license - http://www.gnu.org/licenses/gpl-2.0.html GNU/GPL
Websites: http://www.thefactory.ro
Technical Support:  Forum - http://www.thefactory.ro/joomla-forum/
-------------------------------------------------------------------------*/

defined('_JEXEC') or die('Restricted access');

class wallHelper
{
  var $_integration = array();

  function getInstance()
  {
    static $instance;

    if (!isset($instance))
    {
      $instance = new wallHelper();
    }

    return $instance;
  }
  
  function getIntegration($component)
  {
    $database =& JFactory::getDBO();

    switch ($component)
    {
      case 'cb':
        $query = ' SELECT count(*)'
               . ' FROM #__components'
               . ' WHERE `option` = "com_comprofiler"';
        $database->setQuery($query);
        return (boolean) $database->loadResult();
        break;
    }
  }


  function timeSince($date)
  {
    $days = floor((time() - strtotime($date)) / 86400);

    switch ($days)
    {
      case 0:
        return JText::_('today');
      break;

      case 1:
        return $days . ' ' . JText::_('day ago');
      break;

      default:
        return $days . ' ' . JText::_('days ago');
      break;
    }
  }

  function trimText($text, $length = 100)
  {
    if (strlen($text) > $length)
    {
      return substr($text, 0, $length) . '...';
    }

    return $text;
  }

  function formatDate($date, $format = 'n')
  {
    switch ($format)
    {
      case 'n':
        return date('d.m.y, H:i', strtotime($date));
      break;
    }
  }
 
  function postDate($date)
  {
    $output = '<div class="wallfactory-calendar">'
            . '  <div class="wallfactory-day-name">' . date('D', JText::_(strtotime($date))) . '</div>'
            . '  <div class="wallfactory-day">' . date('d', strtotime($date)) . '</div>'
            . '  <div class="wallfactory-month">' . Jtext::_(date('M', strtotime($date))) . '</div>'
            . '  <div class="wallfactory-year">' . date('Y', strtotime($date)) . '</div>'
            . '</div>';
    return $output;
  }

  function checkGuestView($force_redirect = null)
  {
    global $mainframe, $Itemid;

    $user =& JFactory::getUser();
    $settings = new wallSettings();

    if ((!$settings->enable_guest_view || $force_redirect) && $user->guest)
    {
      $session =& JFactory::getSession();
      $session->set('referer', $_SERVER['REQUEST_URI']);

      $mainframe->redirect(JRoute::_('index.php?option=com_wallfactory&view=login&Itemid=' . $Itemid, false));
    }
  }

  function checkWallOwner($redirect = true)
  {
    $helper = wallHelper::getInstance();
    $helper->checkGuestView();

    global $mainframe, $Itemid;
    $wallSettings = new wallSettings();

    $user     =& JFactory::getUser();
    $database =& JFactory::getDBO();

    if ($user->guest && !($wallSettings->enable_guest_view))
    {
      $mainframe->redirect(JRoute::_('index.php?option=com_wallfactory&view=login&Itemid=' . $Itemid, false), JText::_('Please login first !'));
    }

    $query = ' SELECT w.id'
           . ' FROM #__wallfactory_walls w'
           . ' WHERE w.user_id = ' . $user->id;
    $database->setQuery($query);
   
    if (!$database->loadResult())
    {
      if ($redirect)
      {
        $mainframe->redirect(JRoute::_('index.php?option=com_wallfactory&view=newwall&Itemid=' . $Itemid, false), JText::_('Please register your wall first!'));
      }

      return false;
    }

    return true;
  }
  
  function checkProfileView($alias_req) {
  	global $mainframe, $Itemid;
  	
  	$database =& JFactory::getDBO();
  	$query = ' SELECT s.allow_profile_view '
  		 	. ' FROM #__wallfactory_members s'
           	. ' INNER JOIN #__wallfactory_walls w'
           	. ' ON w.user_id = s.user_id'
           	. ' WHERE w.alias = "'. $alias_req .'"';
    $database->setQuery($query);
       
    return $database->loadResult();
    
  }
  
  function checkIsLoggedIn()
  {
    global $mainframe, $Itemid;

    $user =& JFactory::getUser();

    if ($user->guest)
    {
      $session =& JFactory::getSession();
      $session->set('referer', $_SERVER['REQUEST_URI']);

      $mainframe->redirect(JRoute::_('index.php?option=com_wallfactory&view=login&Itemid=' . $Itemid, false));
    }
  }

  function getAvatar($user_id, $avatar_type, $avatar_extension)
  {
    $src = false;

    if (!$user_id)
    {
      $avatar_type = 3;
    }

    switch ($avatar_type)
    {
      // Use Community Builder avatar
      case 1:
        $database =& JFactory::getDBO();
        $query = ' SELECT avatar'
               . ' FROM #__comprofiler'
               . ' WHERE user_id = ' . $user_id;
        $database->setQuery($query);
        $src = $database->loadResult();

        if (!is_null($src))
        {
          if (strpos($src, 'gallery/') === false)
          {
            $src = 'tn' . $src;
          }

          $src = JURI::root() . 'images/comprofiler/' . $src;
        }
      break;
      
      // Use own avatar
      case 2:
        $src = JURI::root() . 'components/com_wallfactory/storage/users/' . $user_id . '/avatar/avatar.' . $avatar_extension . '?' . time() . $user_id;
      break;

      // Gravatar
      case 3:
        $settings = new wallSettings();

        if (!$settings->enable_gravatar)
        {
          break;
        }

        $email = md5(strtolower(trim($email)));
        $size = max(array($settings->avatar_max_width, $settings->avatar_max_height));
        $src = 'http://www.gravatar.com/avatar/'.$email.'?s='.$size.'.jpg';
      break;
    }

    return $src ? '<img class="user-avatar" src="' . $src . '" />' : false;
  }

  function getSlug($title)
  {
    $slug = preg_replace("/[^a-zA-Z0-9 ]/", "", $title);
    $slug = str_replace(" ", "-", $slug);
    $slug = strtolower($slug);

    return $slug;
  }

  function addScriptDeclarations($declarations)
  {
    $doc =& JFactory::getDocument();

    foreach ($declarations as $var => $val)
    {
      $content = 'var ' . $var . ' = ';

      if (is_numeric($val))
      {
        $content .= $val;
      }
      elseif (is_array($val))
      {
        $content .= 'new Array("' . implode('", "', $val) . '")';
      }
      else
      {
        $content .= '"' . $val . '"';
      }

      $content .= ';';

      $doc->addScriptDeclaration($content);
    }
  }

  function stripJavascript($document)
  {
    $search = array('@<script[^>]*?>.*?</script>@si',  // Strip out javascript
                    '@<iframe[^>]*?>.*?</iframe>@si',  // Strip out iframes
                    '@<style[^>]*?>.*?</style>@siU',    // Strip style tags properly
                    '@<![\s\S]*?--[ \t\n\r]*>@'         // Strip multi-line comments including CDATA
    );

    $text = preg_replace($search, '', $document);

    $aDisabledAttributes = array('onabort', 'onactivate', 'onafterprint', 'onafterupdate', 'onbeforeactivate', 'onbeforecopy', 'onbeforecut', 'onbeforedeactivate', 'onbeforeeditfocus', 'onbeforepaste', 'onbeforeprint', 'onbeforeunload', 'onbeforeupdate', 'onblur', 'onbounce', 'oncellchange', 'onchange', 'onclick', 'oncontextmenu', 'oncontrolselect', 'oncopy', 'oncut', 'ondataavaible', 'ondatasetchanged', 'ondatasetcomplete', 'ondblclick', 'ondeactivate', 'ondrag', 'ondragdrop', 'ondragend', 'ondragenter', 'ondragleave', 'ondragover', 'ondragstart', 'ondrop', 'onerror', 'onerrorupdate', 'onfilterupdate', 'onfinish', 'onfocus', 'onfocusin', 'onfocusout', 'onhelp', 'onkeydown', 'onkeypress', 'onkeyup', 'onlayoutcomplete', 'onload', 'onlosecapture', 'onmousedown', 'onmouseenter', 'onmouseleave', 'onmousemove', 'onmoveout', 'onmouseover', 'onmouseup', 'onmousewheel', 'onmove', 'onmoveend', 'onmovestart', 'onpaste', 'onpropertychange', 'onreadystatechange', 'onreset', 'onresize', 'onresizeend', 'onresizestart', 'onrowexit', 'onrowsdelete', 'onrowsinserted', 'onscroll', 'onselect', 'onselectionchange', 'onselectstart', 'onstart', 'onstop', 'onsubmit', 'onunload');
    $text = preg_replace('/<(.*?)>/ie', "'<' . preg_replace(array('/javascript:[^\"\']*/i', '/(" . implode('|', $aDisabledAttributes) . ")[ \\t\\n]*=[ \\t\\n]*[\"\'][^\"\']*[\"\']/i', '/\s+/'), array('', '', ' '), stripslashes('\\1')) . '>'", $text);

    return $text;
  }

}