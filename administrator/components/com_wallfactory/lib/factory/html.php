<?php
/*------------------------------------------------------------------------
com_wallfactory - Wall Factory
------------------------------------------------------------------------
author    TheFactory
copyright Copyright (C) 2010 SKEPSIS Consult SRL. All Rights Reserved.
@license - http://www.gnu.org/licenses/gpl-2.0.html GNU/GPL
Websites: http://www.thefactory.ro
Technical Support:  Forum - http://www.thefactory.ro/joomla-forum/
-------------------------------------------------------------------------*/

defined('_JEXEC') or die('Restricted access');

if (class_exists('JHtmlFactory'))
{
  return false;
}

class JHtmlFactory
{
  function jQueryScript()
	{
	  $option = 'com_wallfactory';

	  $document  =& JFactory::getDocument();
	  $filenames =  func_get_args();

	  if (!count($filenames) || 'jquery' != $filenames[0])
	  {
	    array_unshift($filenames, 'jquery');
	  }

	  foreach ($filenames as $i => $filename)
	  {
	    $filename = ($i ? 'jquery-' : '') . $filename . '-factory.js';

	    // 1. Check if the file has already been added
	    foreach ($document->_scripts as $script => $params)
	    {
	      if (false !== strpos($script, $filename))
	      {
	        // File was already added, continue
	        continue;
	      }
	    }

	    $path = JURI::root(true) . '/components/' . $option . '/assets/js/jquery-factory/';

	    // Add the script file
	    $document->addScript($path . $filename);
	  }

		return true;
	}
}
