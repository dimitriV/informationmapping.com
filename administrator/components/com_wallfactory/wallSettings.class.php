<?php

defined('_JEXEC') or die('Restricted access');

class wallSettings
{
  var $allowed_wallowners_groups               = array();
  var $allowed_wallowners_users                = array();
  var $allowed_wallowners_all                  = 1;
  var $enable_guest_view                       = 1;
  var $enable_guest_write                      = 1;
  var $allow_guest_comments                    = 0;
  var $captcha_comment                         = 0;
  var $enable_bookmarks                        = 1;
  var $wall_title_display                      = 1;
  var $banned_words                            = array('cnJy');
  var $enable_avatars                          = 1;
  var $avatar_max_width                        = 100;
  var $avatar_max_height                       = 100;
  var $image_max_width                         = 600;
  var $image_max_height                        = 400;
  var $comments_per_page                       = 5;
  var $recaptcha_public_key                    = '';
  var $recaptcha_private_key                   = '';
  var $enable_notification_email_html          = 1;
  var $enable_gravatar                         = 1;
  var $allow_cb_avatars                        = 1;
  var $enable_notification_new_post            = 1;
  var $notification_new_post_subject           = 'TmV3IHBvc3Qh';
  var $notification_new_post_message           = 'RGVhciAlJXVzZXJuYW1lJSUsDQoNCkEgbmV3IHBvc3Qgd2FzIGFkZGVkIQ==';
  var $enable_notification_new_comment         = 1;
  var $notification_new_comment_subject        = 'TmV3IGNvbW1lbnQh';
  var $notification_new_comment_message        = 'RGVhciAlJXVzZXJuYW1lJSUsDQoNCkEgbmV3IGNvbW1lbnQgd2FzIGFkZGVkIQ==';
  var $enable_notification_new_report_comment  = 1;
  var $notification_new_report_comment_subject = 'Q29tbWVudCByZXBvcnQ=';
  var $notification_new_report_comment_message = 'RGVhciAlJXVzZXJuYW1lJSUsDQoNCkEgY29tbWVudCB3YXMgcmVwb3J0ZWQh';
  var $enable_notification_new_report_post     = 0;
  var $notification_new_report_post_subject    = 'UG9zdCByZXBvcnQ=';
  var $notification_new_report_post_message    = 'RGVhciAlJXVzZXJuYW1lJSUsDQoNCkEgcG9zdCB3YXMgcmVwb3J0ZWQhDQpHbyB0byBwb3N0ICUlcG9zdGxpbmslJS4=';
  var $enable_notification_follow_wall         = 1;
  var $notification_follow_wall_subject        = '';
  var $notification_follow_wall_message        = '';
  var $enable_notify_new_post_followers        = 0;
  var $enable_notify_new_post_admin            = 0;
  var $enable_notify_new_comment_followers     = 0;
  var $enable_notify_new_comment_admin         = 0;
  var $notification_new_post_receivers         = array();
  var $notification_new_comment_receivers      = array();
  var $allow_post_images                       = 1;
  var $allow_post_videos                       = 1;
  var $allow_post_mp3files                     = 1;
  var $allow_post_links                        = 1;
  var $allow_post_files                        = 1;
  var $allow_comments                          = 1;
}