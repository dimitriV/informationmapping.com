<?php
/*------------------------------------------------------------------------
com_wallfactory - Wall Factory
------------------------------------------------------------------------
author    TheFactory
copyright Copyright (C) 2010 SKEPSIS Consult SRL. All Rights Reserved.
@license - http://www.gnu.org/licenses/gpl-2.0.html GNU/GPL
Websites: http://www.thefactory.ro
Technical Support:  Forum - http://www.thefactory.ro/joomla-forum/
-------------------------------------------------------------------------*/

defined('_JEXEC') or die('Restricted access');

// Component installer
function com_install()
{
  	// Install the menu
  	installMenu();
  	
  	jimport('joomla.filesystem.file');
	installModules();

	// Install router plugin
	JFile::move(JPATH_ADMINISTRATOR.DS.'components'.DS.'com_wallfactory'.DS.'modules'.DS.'wallrouter.php', JPATH_ROOT.DS.'plugins'.DS.'system'.DS.'wallrouter.php');
	JFile::move(JPATH_ADMINISTRATOR.DS.'components'.DS.'com_wallfactory'.DS.'modules'.DS.'wallrouter.xml', JPATH_ROOT.DS.'plugins'.DS.'system'.DS.'wallrouter.xml');
	
	$database =& JFactory::getDBO();
	$query = ' INSERT INTO `#__plugins` (`id`, `name`, `element`, `folder`, `access`, `ordering`, `published`, `iscore`, `client_id`, `checked_out`, `checked_out_time`, `params`)'
	           . ' VALUES ("", "Wall Factory Router Plugin", "wallrouter", "system", 0, -9999, 0, 0, 0, 0, "0000-00-00 00:00:00", "")';
	
	$database->setQuery($query);
	$database->query();
}

function installModules()
{
  $modules = array(
	array('name' => 'mod_wallfactory_search',          'title' => 'Wall Factory Search'),
  );

  foreach ($modules as $module)
  {
	installModule($module);
  }
}
	
function installModule($module)
{
	// Copy the language file
	rename(JPATH_ADMINISTRATOR.DS.'components'.DS.'com_wallfactory'.DS.'modules'.DS.$module['name'].DS.'en-GB.'.$module['name'].'.ini',
	JPATH_ROOT.DS.'language'.DS.'en-GB'.DS.'en-GB.'.$module['name'].'.ini');

	// Copy the module's files
	rename(JPATH_ADMINISTRATOR.DS.'components'.DS.'com_wallfactory'.DS.'modules'.DS.$module['name'],
	JPATH_ROOT.DS.'modules'.DS.$module['name']);

	// Register the module in the db
	$database =& JFactory::getDBO();

	$query = ' INSERT INTO #__modules'
	         . ' VALUES ("", "' . $module['title'] . '", "", 0, "left", 0, "0000-00-00 00:00:00", 0, "' . $module['name'] . '", 0, 0, 1, "", 0, 0, "")';
	$database->setQuery($query);
	$database->query();

	$id = $database->insertid();

	$query = ' INSERT INTO #__modules_menu'
	         . ' VALUES(' . $id . ', 0)';
	$database->setQuery($query);
	$database->query();
}

function installMenu()
{
  $database =& JFactory::getDBO();
  // Install the menu 

  // 1. Create menu
  $query = ' INSERT'
         . ' INTO #__menu_types'
         . ' VALUES (NULL , "wallfactory", "Wall Factory Menu", "Wall Factory Menu")';
  $database->setQuery($query);
  $database->query();

  $menu_id = $database->insertid();

  // 2. Create module
  $query = ' INSERT INTO #__modules'
         . ' VALUES ("", "Wall Factory Menu", "", 0, "left", 0, "0000-00-00 00:00:00", 1, "mod_mainmenu", 0, 0, 1, "menutype=wallfactory", 0, 0, "")';
  $database->setQuery($query);
  $database->query();

  $module_id = $database->insertid();

  $query = ' INSERT INTO #__modules_menu'
         . ' VALUES(' . $module_id . ', 0)';
  $database->setQuery($query);
  $database->query();

  $module_menu_id = $database->insertid();

  // Get the component id
  $query = ' SELECT id'
         . ' FROM #__components'
         . ' WHERE link="option=com_wallfactory"';
  $database->setQuery($query);
  $component_id = $database->loadResult();

  // 3. Create menu items
  // News feed
  $menu_item =& JTable::getInstance('menu');
  $menu_item->menutype    = 'wallfactory';
  $menu_item->name        = 'Wall';
  $menu_item->alias       = 'posts';
  $menu_item->link        = 'index.php?option=com_wallfactory&view=posts';
  $menu_item->type        = 'component';
  $menu_item->published   = 1;
  $menu_item->componentid = $component_id;
  $menu_item->ordering    = 1;
  $menu_item->access	  = 0;	
  $menu_item->params      = 'num_walls_per_page=10
order_walls=0
order_walls_order=1
page_header=News feed
page_description=Latest posts.
page_title=
show_page_title=1
pageclass_sfx=
menu_image=-1
secure=0';
  $menu_item->store();
  

  // My wall
  $menu_item =& JTable::getInstance('menu');
  $menu_item->menutype    = 'wallfactory';
  $menu_item->name        = 'My Posts';
  $menu_item->alias       = 'my-posts';
  $menu_item->link        = 'index.php?option=com_wallfactory&view=wall';
  $menu_item->type        = 'component';
  $menu_item->published   = 1;
  $menu_item->componentid = $component_id;
  $menu_item->ordering    = 2;
  $menu_item->access	  = 1;	
  $menu_item->params      = 'page_title=
show_page_title=1
pageclass_sfx=
menu_image=-1
secure=0';
  $menu_item->store();

 

  // Write a New Post
  $menu_item =& JTable::getInstance('menu');
  $menu_item->menutype    = 'wallfactory';
  $menu_item->name        = 'New Post';
  $menu_item->alias       = 'new-post';
  $menu_item->link        = 'index.php?option=com_wallfactory&view=writepost';
  $menu_item->type        = 'component';
  $menu_item->published   = 1;
  $menu_item->componentid = $component_id;
  $menu_item->ordering    = 3;
  $menu_item->access	  = 1;	
  $menu_item->params      = 'page_title=
show_page_title=1
pageclass_sfx=
menu_image=-1
secure=0';
  $menu_item->store();

  // Followed walls
  $menu_item =& JTable::getInstance('menu');
  $menu_item->menutype    = 'wallfactory';
  $menu_item->name        = 'My Friends';
  $menu_item->alias       = 'my-friends';
  $menu_item->link        = 'index.php?option=com_wallfactory&view=managefollowed';
  $menu_item->type        = 'component';
  $menu_item->published   = 1;
  $menu_item->componentid = $component_id;
  $menu_item->ordering    = 4;
  $menu_item->access	  = 1;	
  $menu_item->params      = 'page_title=
show_page_title=1
pageclass_sfx=
menu_image=-1
secure=0';
  $menu_item->store();


 // Followers 
  $menu_item =& JTable::getInstance('menu');
  $menu_item->menutype    = 'wallfactory';
  $menu_item->name        = 'My Followers';
  $menu_item->alias       = 'my-followers';
  $menu_item->link        = 'index.php?option=com_wallfactory&view=managefollowers';
  $menu_item->type        = 'component';
  $menu_item->published   = 1;
  $menu_item->componentid = $component_id;
  $menu_item->ordering    = 5;
  $menu_item->access	  = 1;	
  $menu_item->params      = 'page_title=
show_page_title=1
pageclass_sfx=
menu_image=-1
secure=0';
  $menu_item->store();
  
   // Settings
  $menu_item =& JTable::getInstance('menu');
  $menu_item->menutype    = 'wallfactory';
  $menu_item->name        = 'Account settings';
  $menu_item->alias       = 'wallfactory-settings';
  $menu_item->link        = 'index.php?option=com_wallfactory&view=settings';
  $menu_item->type        = 'component';
  $menu_item->published   = 1;
  $menu_item->componentid = $component_id;
  $menu_item->ordering    = 6;
  $menu_item->access	  = 1;	
  $menu_item->params      = 'page_title=
show_page_title=1
pageclass_sfx=
menu_image=-1
secure=0';
  $menu_item->store();

}
