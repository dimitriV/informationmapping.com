DROP TABLE IF EXISTS `#__wallfactory_bookmarks`;
DROP TABLE IF EXISTS `#__wallfactory_comments`;
DROP TABLE IF EXISTS `#__wallfactory_followers`;
DROP TABLE IF EXISTS `#__wallfactory_ips`;
DROP TABLE IF EXISTS `#__wallfactory_media`;
DROP TABLE IF EXISTS `#__wallfactory_members`;
DROP TABLE IF EXISTS `#__wallfactory_posts`;
DROP TABLE IF EXISTS `#__wallfactory_statistics`;
DROP TABLE IF EXISTS `#__wallfactory_urls`;
DROP TABLE IF EXISTS `#__wallfactory_walls`;

CREATE TABLE IF NOT EXISTS `#__wallfactory_bookmarks` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `title` varchar(255) NOT NULL,
  `published` tinyint(1) NOT NULL,
  `link` text NOT NULL,
  `extension` varchar(10) NOT NULL,
  PRIMARY KEY (`id`)
) ENGINE=MyISAM  DEFAULT CHARSET=utf8 AUTO_INCREMENT=19 ;

CREATE TABLE IF NOT EXISTS `#__wallfactory_comments` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `post_id` int(11) NOT NULL,
  `user_id` int(11) NOT NULL,
  `content` mediumtext NOT NULL,
  `author_name` varchar(255) NOT NULL,
  `author_alias` varchar(255) NOT NULL,
  `approved` tinyint(1) NOT NULL,
  `reported` tinyint(1) NOT NULL,
  `reporting_user_id` int(11) NOT NULL,
  `date_created` datetime NOT NULL,
  PRIMARY KEY (`id`)
) ENGINE=MyISAM  DEFAULT CHARSET=utf8 AUTO_INCREMENT=1 ;

CREATE TABLE IF NOT EXISTS `#__wallfactory_followers` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `user_id` int(11) NOT NULL,
  `wall_id` int(11) NOT NULL,
  `notification` tinyint(1) NOT NULL,
  `date_created` datetime NOT NULL,
  PRIMARY KEY (`id`)
) ENGINE=MyISAM  DEFAULT CHARSET=utf8 AUTO_INCREMENT=1 ;

CREATE TABLE IF NOT EXISTS `#__wallfactory_ips` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `user_id` int(11) NOT NULL,
  `ip` varchar(15) NOT NULL,
  `created_at` datetime NOT NULL,
  `updated_at` datetime NOT NULL,
  `visits` int(11) NOT NULL,
  PRIMARY KEY (`id`),
  KEY `idx_user_id` (`user_id`)
) ENGINE=MyISAM  DEFAULT CHARSET=utf8 AUTO_INCREMENT=1 ;

CREATE TABLE IF NOT EXISTS `#__wallfactory_media` (
  `id` bigint(20) NOT NULL AUTO_INCREMENT,
  `user_id` bigint(20) NOT NULL,
  `wall_id` int(11) NOT NULL,
  `folder` varchar(100) NOT NULL,
  `title` varchar(255) NOT NULL,
  `name` varchar(255) NOT NULL,
  `extension` varchar(10) NOT NULL,
  `description` mediumtext NOT NULL,
  `date_added` datetime DEFAULT NULL,
  `post_id` int(11) NOT NULL,
  PRIMARY KEY (`id`)
) ENGINE=MyISAM  DEFAULT CHARSET=utf8 AUTO_INCREMENT=1 ;

CREATE TABLE IF NOT EXISTS `#__wallfactory_members` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `user_id` int(11) NOT NULL,
  `avatar_type` tinyint(4) NOT NULL,
  `avatar_extension` varchar(10) NOT NULL,
  `name` varchar(255) NOT NULL,
  `description` mediumtext NOT NULL,
  `gender` varchar(4) NOT NULL,
  `birthday` datetime NOT NULL,
  `allow_profile_view` tinyint(4) NOT NULL,
  `banned` tinyint(4) NOT NULL,
  PRIMARY KEY (`id`)
) ENGINE=MyISAM  DEFAULT CHARSET=utf8 AUTO_INCREMENT=1 ;

CREATE TABLE IF NOT EXISTS `#__wallfactory_posts` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `wall_id` int(11) NOT NULL,
  `user_id` int(11) NOT NULL,
  `alias` varchar(255) NOT NULL,
  `content` mediumtext NOT NULL,
  `notification` tinyint(1) NOT NULL,
  `date_created` datetime NOT NULL,
  `date_updated` datetime NOT NULL,
  `enable_comments` tinyint(1) NOT NULL DEFAULT '1',
  `reported` tinyint(1) NOT NULL,
  `reporting_user_id` int(11) NOT NULL,
  PRIMARY KEY (`id`)
) ENGINE=MyISAM  DEFAULT CHARSET=utf8 AUTO_INCREMENT=1 ;

CREATE TABLE IF NOT EXISTS `#__wallfactory_statistics` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `posts` int(11) NOT NULL,
  `urls` int(11) NOT NULL,
  `video_links` int(11) NOT NULL,
  `images` int(11) NOT NULL,
  `mp3` int(11) NOT NULL,
  `files` int(11) NOT NULL,
  `user_id` int(11) NOT NULL,
  `wall_id` int(11) NOT NULL,
  PRIMARY KEY (`id`)
) ENGINE=MyISAM  DEFAULT CHARSET=utf8 AUTO_INCREMENT=1 ;


CREATE TABLE IF NOT EXISTS `#__wallfactory_urls` (
  `id` bigint(20) NOT NULL AUTO_INCREMENT,
  `user_id` bigint(20) NOT NULL,
  `wall_id` int(11) NOT NULL,
  `url` varchar(255) NOT NULL,
  `url_title` text NOT NULL,
  `url_description` text NOT NULL,
  `video_link` varchar(255) NOT NULL,
  `video_title` text NOT NULL,
  `video_description` text NOT NULL,
  `video_thumbnail` text NOT NULL,
  `video_sitename` varchar(100) NOT NULL,
  `video_sourceThumb` text NOT NULL,
  `date_added` datetime DEFAULT NULL,
  `post_id` int(11) NOT NULL,
  PRIMARY KEY (`id`)
) ENGINE=MyISAM  DEFAULT CHARSET=utf8 AUTO_INCREMENT=1 ;

CREATE TABLE IF NOT EXISTS `#__wallfactory_walls` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `user_id` int(11) NOT NULL,
  `published` tinyint(1) NOT NULL,
  `title` varchar(255) NOT NULL,
  `alias` varchar(255) NOT NULL,
  `posts_per_page` int(11) NOT NULL,
  `comment_notification` tinyint(1) NOT NULL,
  `report_comment_notification` tinyint(1) NOT NULL,
  `date_created` datetime NOT NULL,
  `report_post_notification` tinyint(1) NOT NULL,
  `post_notification` tinyint(1) NOT NULL,
  PRIMARY KEY (`id`)
) ENGINE=MyISAM  DEFAULT CHARSET=utf8 AUTO_INCREMENT=1 ;


INSERT INTO `#__wallfactory_bookmarks` (`id`, `title`, `published`, `link`, `extension`) VALUES
(1, 'Delicious', 1, 'http://del.icio.us/post?url=%%url%%&title=%%title%%', 'png'),
(2, 'Digg', 1, 'http://digg.com/submit?phase=2&url=%%url%%&title=%%title%%', 'png'),
(3, 'Google Bookmarks', 1, 'http://www.google.com/bookmarks/mark?op=add&bkmk=%%url%%&title=%%title%%', 'png'),
(4, 'Facebook', 1, 'http://www.facebook.com/share.php?u=%%url%%&t=%%title%%', 'png'),
(5, 'Technorati', 1, 'http://technorati.com/faves/?add=%%url%%&title=%%title%%', 'png'),
(6, 'Stumbleupon', 1, 'http://www.stumbleupon.com/submit?url=%%url%%&title=%%title%%', 'png'),
(7, 'Reddit', 1, 'http://reddit.com/submit?url=%%url%%&title=%%title%%', 'png'),
(8, 'My Space', 1, 'http://www.myspace.com/Modules/PostTo/Pages/?u=%%url%%&t=%%title%%', 'png'),
(9, 'Amazon', 1, 'http://www.amazon.com/wishlist/add?u=%%url%%&t=%%title%%', 'png'),
(10, 'Twitter', 1, 'http://twitter.com/home?status=%%title%%%3A+%%url%%', 'png'),
(11, 'Bebo', 1, 'http://bebo.com/c/share?Url=%%url%%&Title=%%title%%', 'png'),
(12, 'Linkedin', 1, 'http://www.linkedin.com/shareArticle?mini=true&url=%%url%%&title=%%title%%', 'png'),
(13, 'Mixx', 1, 'http://www.mixx.com/submit?page_url=%%url%%', 'png'),
(14, 'Netvibes', 1, 'http://netvibes.com/share?url=%%url%%&title=%%title%%', 'png'),
(15, 'Newsvine', 1, 'http://www.newsvine.com/_tools/seed&save?u=%%url%%&h=%%title%%', 'png'),
(16, 'Tumblr', 1, 'http://www.tumblr.com/share?v=3&u=%%url%%&t=%%title%%&s=', 'png'),
(17, 'Virb', 1, 'http://virb.com/share?external&v=2&url=%%url%%&title=%%title%%', 'png'),
(18, 'Yahoo Mail', 1, 'http://compose.mail.yahoo.com/?To=&Subject=%%title%%&body=%%bodytext%%', 'png');
