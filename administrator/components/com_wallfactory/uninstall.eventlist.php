<?php
/*------------------------------------------------------------------------
com_wallfactory - Wall Factory
------------------------------------------------------------------------
author    TheFactory
copyright Copyright (C) 2010 SKEPSIS Consult SRL. All Rights Reserved.
@license - http://www.gnu.org/licenses/gpl-2.0.html GNU/GPL
Websites: http://www.thefactory.ro
Technical Support:  Forum - http://www.thefactory.ro/joomla-forum/
-------------------------------------------------------------------------*/

defined('_JEXEC') or die('Restricted access');

// Component uninstaller
function com_uninstall()
{
  $database =& JFactory::getDBO();
  $modules = array(
    array('name' => 'mod_wallfactory_search',             'title' => 'Wall Factory Search'),
  );

  // Uninstall the modules
  foreach ($modules as $module)
  {
    uninstallModule($module);
  }

  // Uninstall the menu
  uninstallMenu();
}

// Helpers
function uninstallModule($module)
{
  $database =& JFactory::getDBO();

  // Delete the language file
  @unlink(JPATH_ROOT.DS.'language'.DS.'en-GB'.DS.'en-GB.'.$module['name'].'.ini');

  // Delete the module's files
  recursive_remove_directory(JPATH_ROOT.DS.'modules'.DS.$module['name']);

  // Remove the module from the db
  $database =& JFactory::getDBO();
  $query = ' SELECT id'
         . ' FROM #__modules'
         . ' WHERE module = "' . $module['name'] . '"';
  $database->setQuery($query);
  $id = $database->loadResult();

  if ($id)
  {
    $query = ' DELETE'
           . ' FROM #__modules'
           . ' WHERE id = ' . $id;
    $database->setQuery($query);
    $database->query();

    $query = ' DELETE'
           . ' FROM #__modules_menu'
           . ' WHERE moduleid = ' . $id;
    $database->setQuery($query);
    $database->query();
  }

  // Delete the language file
}

function recursive_remove_directory($directory, $empty=FALSE)
{
	if(substr($directory,-1) == '/')
	{
		$directory = substr($directory,0,-1);
	}
	if(!file_exists($directory) || !is_dir($directory))
	{
		return FALSE;
	}elseif(is_readable($directory))
	{
		$handle = opendir($directory);
		while (FALSE !== ($item = readdir($handle)))
		{
			if($item != '.' && $item != '..')
			{
				$path = $directory.'/'.$item;
				if(is_dir($path))
				{
					recursive_remove_directory($path);
				}else{
					unlink($path);
				}
			}
		}
		closedir($handle);
		if($empty == FALSE)
		{
			if(!rmdir($directory))
			{
				return FALSE;
			}
		}
	}
	return TRUE;
}

function uninstallMenu()
{
  $database =& JFactory::getDBO();

  // 1. Remove the menu
  $query = ' DELETE'
         . ' FROM #__menu_types'
         . ' WHERE menutype = "wallfactory"';
  $database->setQuery($query);
  $database->query();

  // 2. Remove the module
  $query = ' SELECT id'
         . ' FROM #__modules'
         . ' WHERE params LIKE "%menutype=wallfactory%"';
  $database->setQuery($query);
  $ids = $database->loadResultArray();

  foreach ($ids as $id)
  {
    $query = ' DELETE'
           . ' FROM #__modules'
           . ' WHERE id = ' . $id;
    $database->setQuery($query);
    $database->query();

    $query = ' DELETE'
           . ' FROM #__modules_menu'
           . ' WHERE moduleid = ' . $id;
    $database->setQuery($query);
    $database->query();
  }

  // 3. Remove menu items
  $menu_items = array('index.php?option=com_wallfactory&view=posts',
    'index.php?option=com_wallfactory&view=wall',
    'index.php?option=com_wallfactory&view=writepost',
    'index.php?option=com_wallfactory&view=managefollowed',
  'index.php?option=com_wallfactory&view=managefollowers',
  'index.php?option=com_wallfactory&view=settings');

  $query = ' DELETE FROM #__menu'
         . ' WHERE link IN ("' . implode('", "', $menu_items) . '")';
  $database->setQuery($query);
  $database->query();
}