<?php
/**
 * @version		$Id: view.html.php 21655 2011-06-23 05:43:24Z chdemko $
 * @copyright	Copyright (C) 2005 - 2011 Open Source Matters, Inc. All rights reserved.
 * @license		GNU General Public License version 2 or later; see LICENSE.txt
 */

// No direct access
defined('_JEXEC') or die;

jimport('joomla.application.component.view');

/**
 * View to edit a frontpagearticle.
 *
 * @package		Joomla.Administrator
 * @subpackage	com_frontpagearticles
 * @since		1.5
 */
class FrontpagearticlesViewFrontpagearticle extends JView
{
	protected $state;
	protected $item;
	protected $form;

	/**
	 * Display the view
	 */
	public function display($tpl = null)
	{
		$this->state	= $this->get('State');
		$this->item		= $this->get('Item');
		$this->form		= $this->get('Form');

		// Check for errors.
		if (count($errors = $this->get('Errors'))) {
			JError::raiseError(500, implode("\n", $errors));
			return false;
		}

		$this->addToolbar();
		parent::display($tpl);
	}

	/**
	 * Add the page title and toolbar.
	 *
	 * @since	1.6
	 */
	protected function addToolbar()
	{
		JRequest::setVar('hidemainmenu', true);

		$user		= JFactory::getUser();
		$isNew		= ($this->item->id == 0);
		$checkedOut	= !($this->item->checked_out == 0 || $this->item->checked_out == $user->get('id'));
		$canDo		= FrontpagearticlesHelper::getActions($this->state->get('filter.category_id'), $this->item->id);

		JToolBarHelper::title(JText::_('COM_FRONTPAGEARTICLES_MANAGER_FRONTPAGEARTICLE'), 'frontpagearticles.png');

		// If not checked out, can save the item.
		if (!$checkedOut && ($canDo->get('core.edit')||(count($user->getAuthorisedCategories('com_frontpagearticles', 'core.create')))))
		{
			JToolBarHelper::apply('frontpagearticle.apply');
			JToolBarHelper::save('frontpagearticle.save');
		}
		if (!$checkedOut && (count($user->getAuthorisedCategories('com_frontpagearticles', 'core.create')))){
			JToolBarHelper::save2new('frontpagearticle.save2new');
		}
		// If an existing item, can save to a copy.
		if (!$isNew && (count($user->getAuthorisedCategories('com_frontpagearticles', 'core.create')) > 0)) {
			JToolBarHelper::save2copy('frontpagearticle.save2copy');
		}
		if (empty($this->item->id)) {
			JToolBarHelper::cancel('frontpagearticle.cancel');
		}
		else {
			JToolBarHelper::cancel('frontpagearticle.cancel', 'JTOOLBAR_CLOSE');
		}

		JToolBarHelper::divider();
		JToolBarHelper::help('JHELP_COMPONENTS_FRONTPAGEARTICLES_LINKS_EDIT');
	}
}