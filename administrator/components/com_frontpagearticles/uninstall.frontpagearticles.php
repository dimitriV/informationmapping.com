<?php
/**
 * @version     $Id: uninstall.Frontpagearticles.php  2010-10-23 04:13:25Z $
 * @package     Informationmapping
 * @subpackage  com_frontpagearticles
 * @author      ITOffshore Nepal
 * @copyright   Copyright (C) 2011 ITOffshore Nepal.
 * @license     ITOffshore Nepal
 */

// no direct access
defined('_JEXEC') or die('Restricted access');

// Your custom code here

function rmdir_recursive($dir) {
    $files = scandir($dir);
    array_shift($files);    // remove '.' from array
    array_shift($files);    // remove '..' from array
    foreach ($files as $file) {
        $file = $dir . '/' . $file;
        if (is_dir($file)) {
            rmdir_recursive($file);
            @rmdir($file);
        } else {
            @unlink($file);
        }
    }
    @rmdir($dir);
}
$dir = JPATH_ROOT.'/images/frontpagearticles';
rmdir_recursive($dir);


?>