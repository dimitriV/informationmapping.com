<?php
/**
* Frontpagearticles - A Joomla Component for managing frontpagearticle images.
 * @version     $Id: install.frontpagearticles.php  2010-10-23 04:13:25Z $
 * @package     Informationmapping
 * @subpackage  com_frontpagearticles
 * @author      ITOffshore Nepal
 * @copyright   Copyright (C) 2011 ITOffshore Nepal.
 * @license     ITOffshore Nepal
 */

// no direct access
defined( '_JEXEC' ) or die( 'Restricted access' );

function com_install() {

  # Show installation result to user
  ?>
  <center>
  <table width="100%" border="0">
    <tr>
      <td>
        <font class="small">&copy; Copyright 2010 by IT Offshore Nepal</font><br/>
		<br />
		<?php
			// Create directory for storing article images
			if(file_exists(JPATH_ROOT.DS."images".DS."frontpagearticles")){
				echo 'The directory "frontpagearticles" already exist in "'.JPATH_ROOT.DS. 'images"<br />';
			}
			else{
				mkdir(JPATH_ROOT.DS."images".DS."frontpagearticles", 0777);
				echo 'The directory "frontpagearticles" has been created in "'.JPATH_ROOT.DS.'images"<br />';
			}
		    if(file_exists(JPATH_ROOT.DS."images".DS."frontpagearticles")){
				echo 'The directory "frontpagearticles" already exist in "'.JPATH_ROOT.DS.'images'.DS.'frontpagearticles"<br />';
			}
			else{
				mkdir(JPATH_ROOT.DS."images".DS."frontpagearticles", 0777);
				echo 'The directory "frontpagearticles" has been created in "'.JPATH_ROOT.DS.'images'.DS.'frontpagearticles"<br />';
			}
            
		?>
		<br />
		Please upload images in the directories inside "frontpagearticles".
      </td>
    </tr>
  </table>
  </center>
  <?php
}
?>