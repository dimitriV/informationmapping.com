<?php
/**
 * @version     $Id: articleimage.php  2010-10-23 04:13:25Z $
 * @package     Interline Design
 * @subpackage  com_articleimages
 * @author      ITOffshore Nepal
 * @copyright   Copyright (C) 2011 ITOffshore Nepal.
 * @license     ITOffshore Nepal
 */
// No direct access
defined('_JEXEC') or die('Restricted access');
// import Joomla controller library
jimport('joomla.application.component.controllerform');
/**
 * ArticleImages ArticleImage Controller
 *
 * @package    Interline Design
 * @subpackage Components
 */
class ArticleImagesControllerArticleImage extends JControllerForm {
	/**
	 * constructor (registers additional tasks to methods)
	 * @return void
	 */
	function __construct() {
		parent::__construct();
		// Register Extra tasks
		$this->registerTask('add', 'edit');
      	}
	/**
	 * display the edit form
	 * @return void
	 */
     
	function edit() {
		$model = & $this->getModel('articleimage');
		$view = & $this->getView('articleimage');
		$view->setModel($model, true);
		$view->display();
	}
    
	/**
	 * save a record (and redirect to main page)
	 * @return void
	 */
	function save() {
		$model = $this->getModel('articleimage');
        $newid = $model->save();
        
		if ($newid) {
			$msg = JText::_('Article image has been saved successfully.');
			$type = 'message';
                        
            if(JRequest::getVar('task')=='apply')
            {
               $db = JFactory::getDbo();
               $id = $db->insertid();
               $data = $model->getData();
               
               $id = $newid;
               
                $this->setRedirect('index.php?option=com_articleimages&task=articleimage.edit&cid[]='.$id, $msg, $type);
            }
            elseif(JRequest::getVar('task')=='save2new')
            {
                $this->setRedirect('index.php?option=com_articleimages&task=articleimage.add', $msg, $type);
            }
            else{
			    $this->setRedirect('index.php?option=com_articleimages', $msg, $type);
            }
		} else {
			$msg = JText::sprintf('Error Saving', $model->getError());
			$type = 'error';
			$app = & JFactory::getApplication();
			$app->enqueueMessage($msg, $type);
			$view = & $this->getView('articleimage');
			$view->setModel($model, true);
			$view->display();
		}
	}
   
    
	/**
	 * cancel editing a record
	 * @return void
	 */
	function cancel() {
		$msg = JText::_('Canceled');
		$this->setRedirect('index.php?option=com_articleimages', $msg);
	}
}

