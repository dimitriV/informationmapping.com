<?php
/**
 * @version     $Id: articleimages.php  2010-10-23 04:13:25Z $
 * @package     Interline Design
 * @subpackage  com_articleimages
 * @author      ITOffshore Nepal
 * @copyright   Copyright (C) 2011 ITOffshore Nepal.
 * @license     ITOffshore Nepal
 */
// No direct access
defined('_JEXEC') or die('Restricted access');
// import Hello controller
require_once JPATH_COMPONENT.DS.'controller.php';
/**
 * ArticleImages ArticleImages Controller
 *
 * @package    Interline Design
 * @subpackage Components
 */
class ArticleImagesControllerArticleImages extends ArticleImagesController {
    
    protected $_viewname = 'articleimages';
	protected $_mainmodel = 'articleimages';
	protected $_itemname = 'Component';

	/**
	 * display record(s)
	 * @return void
	 */
     /**
	 * Constructor.
	 *
	 * @param	array An optional associative array of configuration settings.
	 * @see		JController
	 * @since	1.6
	 */
	public function __construct($config = array())
	{
		parent::__construct($config);
		$this->registerTask('orderup',		'reorder');
		$this->registerTask('orderdown',	'reorder');

	}

	public function display() {
		// Load the submenu (this file is also automatically used by the com_categories component)
		require_once JPATH_COMPONENT.DS.'helpers'.DS.'articleimages.php';
		ArticleImagesHelper::addSubmenu('banners');
		// Display the view
      	$model = & $this->getModel('articleimages');
      	$view = & $this->getView('articleimages');
        $view->setModel($model, true);
        $view->display();
	}
	/**
	 * remove record(s)
	 * @return void
	 */
	function remove() {
		$model = $this->getModel('articleimages');
		if ($model->remove()) {
			$msg = JText::_('Article Image Removed');
			$type = 'message';
		} else {
			$msg = JText::sprintf('Deleted',$model->getError());
			$type = 'error';
		}
		$this->setRedirect('index.php?option=com_articleimages', $msg, $type);
	}
   /**
	 * Changes the order of one or more records.
	 *
	 * @since	1.6
	 */
	public function reorder()
	{
		// Check for request forgeries.
		JRequest::checkToken() or jexit(JText::_('JINVALID_TOKEN'));

		// Initialise variables.
		$user	= JFactory::getUser();
		$ids	= JRequest::getVar('cid', null, 'post', 'array');
		$inc	= ($this->getTask() == 'orderup') ? -1 : +1;

		$model = $this->getModel();
		$return = $model->reorder($ids, $inc);
		if ($return === false) {
			// Reorder failed.
			$message = JText::sprintf('JLIB_APPLICATION_ERROR_REORDER_FAILED', $model->getError());
			$this->setRedirect(JRoute::_('index.php?option=com_articleimages', false), $message, 'error');
			return false;
		} else {
			// Reorder succeeded.
			$message = JText::_('JLIB_APPLICATION_SUCCESS_ITEM_REORDERED');
			$this->setRedirect(JRoute::_('index.php?option=com_articleimages', false), $message);
			return true;
		}
	}

	/**
	 * Method to save the submitted ordering values for records.
	 *
	 * @since	1.6
	 */
	public function saveorder()
	{
		// Check for request forgeries.
		JRequest::checkToken() or jexit(JText::_('JINVALID_TOKEN'));

		// Get the input
		$pks	= JRequest::getVar('cid',	null,	'post',	'array');
		$order	= JRequest::getVar('order',	null,	'post',	'array');

		// Sanitize the input
		JArrayHelper::toInteger($pks);
		JArrayHelper::toInteger($order);

		// Get the model
		$model = $this->getModel();

		// Save the ordering
		$return = $model->saveorder($pks, $order);

		if ($return === false)
		{
			// Reorder failed
			$message = JText::sprintf('JLIB_APPLICATION_ERROR_REORDER_FAILED', $model->getError());
			$this->setRedirect(JRoute::_('index.php?option=com_articleimages', false), $message, 'error');
			return false;
		} else
		{
			// Reorder succeeded.
			$this->setMessage(JText::_('JLIB_APPLICATION_SUCCESS_ORDERING_SAVED'));
			$this->setRedirect(JRoute::_('index.php?option=com_articleimages', false));
			return true;
		}
	}
  }

