<?php
/**
* Article Images - A Joomla Component for managing article images.
 * @version     $Id: install.articleimages.php  2010-10-23 04:13:25Z $
 * @package     Interline Design
 * @subpackage  com_articleimages
 * @author      ITOffshore Nepal
 * @copyright   Copyright (C) 2011 ITOffshore Nepal.
 * @license     ITOffshore Nepal
 */

// no direct access
defined( '_JEXEC' ) or die( 'Restricted access' );

function com_install() {

  # Show installation result to user
  ?>
  <center>
  <table width="100%" border="0">
    <tr>
      <td>
        <font class="small">&copy; Copyright 2010 by IT Offshore Nepal</font><br/>
		<br />
		<?php
			// Create directory for storing article images
			if(file_exists(JPATH_ROOT.DS."images".DS."articleimages")){
				echo 'The directory "articleimages" already exist in "'.JPATH_ROOT.DS. 'images"<br />';
			}
			else{
				mkdir(JPATH_ROOT.DS."images".DS."articleimages", 0777);
				echo 'The directory "articleimages" has been created in "'.JPATH_ROOT.DS.'images"<br />';
			}
		    if(file_exists(JPATH_ROOT.DS."images".DS."articleimages".DS."small")){
				echo 'The directory "small" already exist in "'.JPATH_ROOT.DS.'images'.DS.'articleimages"<br />';
			}
			else{
				mkdir(JPATH_ROOT.DS."images".DS."articleimages".DS."small", 0777);
				echo 'The directory "small" has been created in "'.JPATH_ROOT.DS.'images'.DS.'articleimages"<br />';
			}
		    if(file_exists(JPATH_ROOT.DS."images".DS."articleimages".DS."large")){
				echo 'The directory "large" already exist in "'.JPATH_ROOT.DS.'images'.DS.'articleimages"<br />';
			}
			else{
				mkdir(JPATH_ROOT.DS."images".DS."articleimages".DS."large", 0777);
				echo 'The directory "large" has been created in "'.JPATH_ROOT.DS.'images'.DS.'articleimages"<br />';
			}
		    
            
		?>
		<br />
		Please upload images in the directories "small" and "large" inside "articleimages".
      </td>
    </tr>
  </table>
  </center>
  <?php
}
?>