<?php
/**
 * @version     $Id: articleimage.php  2010-10-23 04:13:25Z $
 * @package     Interline Design
 * @subpackage  com_articleimages
 * @author      ITOffshore Nepal
 * @copyright   Copyright (C) 2011 ITOffshore Nepal.
 * @license     ITOffshore Nepal
 */

// no direct access
defined( '_JEXEC' ) or die( 'Restricted access' );

class TableArticleImage extends JTable
{
	var $id 				= null;
	var $article_id 		= null;
	var $small_image 		= null;
    var $large_image 		= null;
	var $image_title 		= null;
    var $link               = '';
	var $ordering	 		= null;
	var $checked_out 		= 0;
	var $checked_out_time 	= 0;
    var $catid 	            = 0;
	
	function TableArticleImage(& $db) {
		parent::__construct('#__article_images', 'id', $db);
	}
	

}
?>