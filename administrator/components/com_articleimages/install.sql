-- $Id: install.sql 2011-01-11 $

/*Table structure for table `#__article_images` */

DROP TABLE IF EXISTS `#__article_images`;

CREATE TABLE IF NOT EXISTS #__article_images (            
  `id` int(10) NOT NULL auto_increment,        
  `article_id` int(11) NOT NULL,               
  `small_image` varchar(255) NOT NULL,               
  `large_image` varchar(255) NOT NULL,         
  `image_title` varchar(255) NOT NULL,
  `link` mediumtext,
  `ordering` int(11) NOT NULL,         
  `checked_out` int(11) NOT NULL,              
  `checked_out_time` datetime NOT NULL,
  `catid` INT(11) DEFAULT NULL,        
  PRIMARY KEY  (`id`)    
) ENGINE=MyISAM AUTO_INCREMENT=0 DEFAULT CHARSET=utf8;