<?php
/**
 * @version     $Id: articleimages.php  2010-10-23 04:13:25Z $
 * @package     Interline Design
 * @subpackage  com_articleimages
 * @author      ITOffshore Nepal
 * @copyright   Copyright (C) 2011 ITOffshore Nepal.
 * @license     ITOffshore Nepal
 */
// No direct access
defined('_JEXEC') or die('Restricted access');
// Verify access rights
$user = & JFactory::getUser();
if (!$user->authorize('com_articleimages.manage') && false /* to be removed when acl will be OK*/) {
	JFactory::getApplication()->redirect('index.php', JText::_('ALERTNOTAUTH'));
}
// import joomla controller library
jimport('joomla.application.component.controller');
// Set the default task
JRequest::setVar('task', JRequest::getVar('task', 'articleimages.display'));
// Get an instance of the controller prefixed by Sliderbanners
$controller = JController::getInstance('Articleimages');
// Perform the Request task
$controller->execute(JRequest::getCmd('task'));
// Redirect if set by the controller
$controller->redirect();

