<?php
/**
 * @version     $Id: articleimages.php  2010-10-23 04:13:25Z $
 * @package     Interline Design
 * @subpackage  com_articleimages
 * @author      ITOffshore Nepal
 * @copyright   Copyright (C) 2011 ITOffshore Nepal.
 * @license     ITOffshore Nepal
 */

// No direct access
defined('_JEXEC') or die;

/**
 * Article Images component helper.
 *
 * @package    Interline Design
 * @subpackage Components
 */
class ArticleImagesHelper
{
	public static $extension = 'com_articleimages';

    /**
	 * Configure the Linkbar.
	 */
	public static function addSubmenu($submenu)
	{
		
	/*JSubMenuHelper::addEntry(
			JText::_('COM_ARTICLEIMAGES_IMAGES'),
			'index.php?option=com_articleimages',
			$submenu=='banners');
            JSubMenuHelper::addEntry(
			JText::_('COM_ARTICLEIMAGES_CATEGORIES'),
			'index.php?option=com_categories&view=categories&extension=com_articleimages',
			$submenu=='categories');*/
	}
    
    public static function getActions()
	{
		$user		= JFactory::getUser();
		$result		= new JObject;
		$assetName	= 'com_articleimages';

		$actions = array(
			'core.admin', 'core.manage', 'core.create', 'core.edit', 'core.edit.state', 'core.edit.own', 'core.delete'
		);

		foreach ($actions as $action) {
			$result->set($action,	$user->authorise($action, $assetName));
		}

		return $result;
	}

}
