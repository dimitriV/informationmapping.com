<?php
/**
 * @version     $Id: default.php  2010-10-23 04:13:25Z $
 * @package     Interline Design
 * @subpackage  com_articleimages
 * @author      ITOffshore Nepal
 * @copyright   Copyright (C) 2011 ITOffshore Nepal.
 * @license     ITOffshore Nepal
 */
// No direct access
defined('_JEXEC') or die('Restricted access');
JHtml::addIncludePath(JPATH_COMPONENT.DS.'helpers'.DS.'html');
JHTML::_('behavior.tooltip');
$user		= JFactory::getUser();
$listOrder	= $this->state->get('list.ordering');
$listDirn	= $this->state->get('list.direction');
$ordering	= ($listOrder == 'a.ordering');
$document = & JFactory::getDocument();
$document->setTitle(JText::_('Article Image Administrator').' - '.JText::_('List'));
$document->addScriptDeclaration("
function submitbutton(pressbutton) {
	submitform(pressbutton);
}
");
?>
<form action="index.php" method="post" name="adminForm">
<fieldset id="filter-bar">
		<div class="filter-search fltlft">
			<label class="filter-search-lbl" for="filter_search"><?php echo JText::_('JSEARCH_FILTER_LABEL'); ?></label>
			<input type="text" name="filter_search" id="filter_search" value="<?php echo $this->escape($this->state->get('filter.search')); ?>" title="<?php echo JText::_('COM_CATEGORIES_ITEMS_SEARCH_FILTER'); ?>" />
			<button type="submit"><?php echo JText::_('JSEARCH_FILTER_SUBMIT'); ?></button>
			<button type="button" onclick="document.id('filter_search').value='';this.form.submit();"><?php echo JText::_('JSEARCH_FILTER_CLEAR'); ?></button>
		</div>
		<div class="filter-select fltrt">

		<select name="filter_category_id" class="inputbox" onchange="this.form.submit()">
				<option value=""><?php echo JText::_('JOPTION_SELECT_CATEGORY');?></option>
				<?php echo JHtml::_('select.options', JHtml::_('category.options', 'com_content'), 'value', 'text', $this->state->get('filter.category_id'));?>
			</select>
     		</div>
	</fieldset>
	<div class="clr"> </div>

	<table class="adminlist">
		<thead>
                <tr>
        	
        	<th width="20">
        		<input type="checkbox" name="toggle" value="" onclick="checkAll(<?php echo count( $this->items ); ?>);" />
        	</th>			
        	<th>
        		<?php echo JHtml::_('grid.sort', 'COM_ARTICLEIMAGES_TITLE', 'c.title', $listDirn, $listOrder); ?>
        	</th>
             <th>
        		<?php echo JHtml::_('grid.sort', 'COM_ARTICLEIMAGES_ORDER','a.ordering', $listDirn, $listOrder); ?>
                		<?php echo JHtml::_('grid.order',  $this->items, 'filesave.png', 'articleimages.saveorder'); ?>
				</th>
            
             <th>
        		<?php echo JHtml::_('grid.sort', 'COM_ARTICLEIMAGES_CATEGORY_TITLE', 'cc.title', $listDirn, $listOrder); ?>
                
        	</th>
             <th>
        		<?php echo JHtml::_('grid.sort', 'COM_ARTICLEIMAGES_IMAGE_TITLE', 'a.image_title', $listDirn, $listOrder); ?>
                
        	</th>
             <th>
        		<?php echo JHtml::_('grid.sort', 'COM_ARTICLEIMAGES_IMAGE_TITLE_URL', 'a.link', $listDirn, $listOrder); ?>
                
        	</th>
        	<th>
        	<?php echo JHtml::_('grid.sort', 'COM_ARTICLEIMAGES_SMALL_IMAGE', 'a.small_image', $listDirn, $listOrder); ?>
        	</th>
                    
        </tr>


        
        </thead>
		<tfoot>
        <tr>
         <td colspan="8"><?php echo $this->pagination->getListFooter(); ?></td>
        </tr>

        </tfoot>
		<tbody>
      <?php foreach ($this->items as $i => $item) :
            $canEdit	= $user->authorise('core.edit',			'com_articleimages');
			$canChange	= $user->authorise('core.edit.state',	'com_articleimages');
        ?>

	<tr class="row<?php echo $i % 2; ?>">
		
    		<td width="20" align="center">
    			<?php echo JHTML::_('grid.id',   $i, $item->id ); ?>
    		</td>
    		<td align="center">
            	<?php if ($canEdit) : ?>
    			<a href="<?php echo JRoute::_( 'index.php?option=com_articleimages&task=articleimage.edit&cid[]='. $item->id ); ?>">
    				<?php echo $item->article; ?>
    			</a>
                <?php else : ?>
    			<?php echo $item->article; ?>
    			<?php endif; ?>
    		</td>
            <td class="order">
    				<?php if ($canChange) : ?>
						<?php if ($canEdit) : ?>
							<?php if ($listDirn == 'asc') : ?>
								<span><?php echo $this->pagination->orderUpIcon($i, (@$this->items[$i-1]->catid == $item->catid), 'articleimages.orderup', 'JLIB_HTML_MOVE_UP', $ordering); ?></span>
								<span><?php echo $this->pagination->orderDownIcon($i, $this->pagination->total, (@$this->items[$i+1]->catid == $item->catid), 'articleimages.orderdown', 'JLIB_HTML_MOVE_DOWN', $ordering); ?></span>
							<?php elseif ($listDirn == 'desc') : ?>
								<span><?php echo $this->pagination->orderUpIcon($i, (@$this->items[$i-1]->catid == $item->catid), 'articleimages.orderdown', 'JLIB_HTML_MOVE_UP', $ordering); ?></span>
								<span><?php echo $this->pagination->orderDownIcon($i, $this->pagination->total, (@$this->items[$i+1]->catid == $item->catid), 'articleimages.orderup', 'JLIB_HTML_MOVE_DOWN', $ordering); ?></span>
							<?php endif; ?>
						<?php endif; ?>
						<?php $disabled = $canEdit ?  '' : 'disabled="disabled"'; ?>
						<input type="text" name="order[]" size="5" value="<?php echo $item->ordering;?>" <?php echo $disabled;?> class="text-area-order" />
					<?php else : ?>
						<?php echo $item->ordering; ?>
					<?php endif; ?>
    		</td>
          
    	    <td align="center">
            <?php if($item->category):?>
			<?php echo $item->category; ?>
            <p class="smallsub" title="<?php echo $this->escape($item->path);?>">
			<?php echo str_repeat('<span class="gtr">|&mdash;</span>', $item->level-1) ?>
			<?php if (empty($row->note)) : ?>
			<?php echo JText::sprintf('JGLOBAL_LIST_ALIAS', $this->escape($item->alias));?>
			<?php else : ?>
			<?php echo JText::sprintf('JGLOBAL_LIST_ALIAS_NOTE', $this->escape($item->alias), $this->escape($item->note));?>
			<?php endif; ?>
       </p>
       <?php endif;?>
		</td>
            <td align="center">
    		<?php echo $item->image_title; ?>
    		</td>
            <td align="center">
    		<?php echo $item->link; ?>
    		</td>
            <td align="center">
    				<img src="<?php echo JURI::root().'images/articleimages/small/'.$item->small_image; ?>" width="60"/>
    		</td>
           
           
       
	</tr>
<?php endforeach; ?>
</tbody>
	</table>
	<input type="hidden" name="option" value="com_articleimages" />
	<input type="hidden" name="boxchecked" value="0" />
	<input type="hidden" name="task" value="articleimages.display" />
    <input type="hidden" name="filter_order" value="<?php echo $listOrder; ?>" />
	<input type="hidden" name="filter_order_Dir" value="<?php echo $listDirn; ?>" />
    <?php echo JHtml::_('form.token'); ?>
</form>



