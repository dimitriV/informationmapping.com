<?php
/**
 * @version     $Id: view.articleimages.php  2010-10-23 04:13:25Z $
 * @package     Interline Design
 * @subpackage  com_articleimages
 * @author      ITOffshore Nepal
 * @copyright   Copyright (C) 2011 ITOffshore Nepal.
 * @license     ITOffshore Nepal
 */
// No direct access
defined('_JEXEC') or die('Restricted access');
// import Joomla view library
jimport('joomla.application.component.view');
/**
 * Article Images View
 *
 * @package    Interline Design
 * @subpackage Components
 */
class ArticleImagesViewArticleImages extends JView {
	/**
	 * items to be displayed
	 */
	protected $_items;
	protected $_pagination;
	protected $_state;

	/**
	 * Article Images view display method
	 * @return void
	 */
	function display($tpl = null)
	{
	  	// Get data from the model
        $this->state		= $this->get('State');
		$this->items		= $this->get('Items');
		$this->pagination	= $this->get('Pagination');
       
	        
        // Set the toolbar
		$this->setToolBar();
		// Display the template
		parent::display($tpl);
	}
	/**
	 * Setting the toolbar
	 */
	protected function setToolBar()
	{	    
    	        
        $state	= $this->get('State');
        require_once JPATH_COMPONENT.'/helpers/articleimages.php';
		$canDo	= ArticleImagesHelper::getActions();

	JToolBarHelper::title(JText::_('COM_ARTICLEIMAGES_MANAGER'), 'generic.png');
		if ($canDo->get('core.create')) {
			JToolBarHelper::addNew('articleimage.add','JTOOLBAR_NEW');
		}
		if ($canDo->get('core.edit')) {
			JToolBarHelper::editList('articleimage.edit','JTOOLBAR_EDIT');
		}
	/*	if ($canDo->get('core.edit.state')) {
			if ($state->get('filter.published') != 2){
				JToolBarHelper::divider();
				JToolBarHelper::custom('articleimages.publish', 'publish.png', 'publish_f2.png','JTOOLBAR_PUBLISH', true);
				JToolBarHelper::custom('articleimages.unpublish', 'unpublish.png', 'unpublish_f2.png', 'JTOOLBAR_UNPUBLISH', true);
			}
			
		}*/
       
		if ($canDo->get('core.delete')) {
            JToolBarHelper::divider();
            JToolBarHelper::deleteListX('ARE_YOU_SURE_WANT_TO_DELETE','articleimages.remove');
		} 
    /*    	JToolBarHelper::divider();
        if ($canDo->get('core.admin')) {
			JToolBarHelper::preferences('com_articleimages');
			JToolBarHelper::divider();
		}*/
	//	JToolBarHelper::help('JHELP_COMPONENTS_REDIRECT_MANAGER');
	   
              
        		
	}	
}

