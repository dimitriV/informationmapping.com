<?php
/**
 * @version     $Id: default.php  2010-10-23 04:13:25Z $
 * @package     Interline Design
 * @subpackage  com_articleimages
 * @author      ITOffshore Nepal
 * @copyright   Copyright (C) 2011 ITOffshore Nepal.
 * @license     ITOffshore Nepal
 */
// No direct access
defined('_JEXEC') or die('Restricted access');
JHtml::addIncludePath(JPATH_COMPONENT.'/helpers/html');
JHTML::_('behavior.tooltip');
JHTML::_('behavior.formvalidation');
$document = & JFactory::getDocument();
$document->setTitle(JText::_('COM_ARTICLEIMAGES_ADMINISTRATOR') . ' - ' . JText::_('EDITING'));
$document->addScriptDeclaration("
Joomla.submitbutton = function(task)
{
        if (task == '')
        {
                return false;
        }
        else
        {
                var isValid=true;
                var action = task.split('.');
                if (action[1] != 'cancel' && action[1] != 'close')
                {
                        var forms = $$('form.form-validate');
                        for (var i=0;i<forms.length;i++)
                        {
                                if (!document.formvalidator.isValid(forms[i]))
                                {
                                        isValid = false;
                                        break;
                                }
                        }
                         if(document.getElementById('jform_article_small_image').value=='' && document.getElementById('jform_small_image').value==''){
                            isValid = false;
                            alert('Select Images')
                            }
                }
 
                if (isValid)
                {
                        Joomla.submitform(task);
                        return true;
                }
                else
                {
                        //alert(Joomla.JText._('COM_ARTICLEIMAGES_ARTILCEIMAGE_ERROR_UNACCEPTABLE','Some values are unacceptable'));
                        return false;
                }
        }
}

");

?>

<form action="index.php" method="post" name="adminForm" id="adminForm" class="form-validate" enctype="multipart/form-data">

 <div class="width-60 fltlft">
<fieldset class="adminform">
		<legend><?php echo JText::_( 'COM_ARTICLEIMAGES_ARTILCEIMAGE_FORM' ); ?></legend>
		<table class="admintable">
			<?php foreach($this->form->getFieldset() as $field): ?>
				<?php if ($field->hidden): ?>
                <?php if($field->name=='jform[small_image]' && $field->value!=''):?>
                        <tr>
                        <td width="40%" class="key"><?php echo JText::_('COM_ARTICLEIMAGES_IMAGE_PREVIEW')?></td>
                        <td><img src="<?php echo JURI::root().'images/articleimages/small/'.$field->value;?>" height="60"/></td>
                        </tr>
                  <?php endif;?>
					<?php echo $field->input; ?>
				<?php else: ?>
					<tr>
						<td width="40%" class="key"><?php echo $field->label; ?></td>
						<td><?php echo $field->input; ?></td>
					</tr>
				<?php endif; ?>
			<?php endforeach; ?>
		</table>
	</fieldset>
    </div>
    
	<input type="hidden" name="option" value="com_articleimages" />
	<input type="hidden" name="task" value="articleimage.edit" />
    <?php echo JHtml::_('form.token'); ?>
</form>


