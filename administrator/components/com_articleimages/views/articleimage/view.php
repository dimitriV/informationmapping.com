<?php
/**
 * @version     $Id: view.articleimage.php  2010-10-23 04:13:25Z $
 * @package     Interline Design
 * @subpackage  com_articleimages
 * @author      ITOffshore Nepal
 * @copyright   Copyright (C) 2011 ITOffshore Nepal.
 * @license     ITOffshore Nepal
 */
// No direct access
defined('_JEXEC') or die('Restricted access');
// import Joomla view library
jimport('joomla.application.component.view');
/**
 * Article Image View
 *
 * @package    Interline Design
 * @subpackage Components
 */
class ArticleImagesViewArticleImage extends JView {
	/**
	 * View form
	 *
	 * @var		form
	 */
	protected $form = null;
	/**
	 * display method of Hello view
	 * @return void
	 */
	public function display($tpl = null)
	{
	   
		// get the Form
		$form = & $this->get('Form');
		// get the Data
		$data = & $this->get('Data');
        
        // Check for errors.
                if (count($errors = $this->get('Errors'))) 
                {
                        JError::raiseError(500, implode('<br />', $errors));
                        return false;
                }

               
        // Bind the Data
		$form->bind($data);
		// Assign the Form
		$this->form = $form;
        
        // Set the toolbar
		$this->_setToolBar();
		// Display the template
		parent::display($tpl);
	}
	/**
	 * Setting the toolbar
	 */
	protected function _setToolBar()
	{  
	   
        JRequest::setVar('hidemainmenu', 1);
        require_once JPATH_COMPONENT.'/helpers/articleimages.php';
    	$user		= JFactory::getUser();
		$canDo		= ArticleImagesHelper::getActions();
       
		$isNew = ($this->form->getValue('id') < 1);
        
       
		$text = $isNew ? JText::_('COM_ARTICLEIMAGES_NEW') : JText::_('COM_ARTICLEIMAGES_EDIT');
		JToolBarHelper::title(JText::_('COM_ARTICLEIMAGES_TITLE') . ': <small><small>[ ' . $text . ' ]</small></small>');
        JToolBarHelper::apply('articleimage.apply', 'JTOOLBAR_APPLY');
		JToolBarHelper::save('articleimage.save');
        
        
        if ($canDo->get('core.edit') && $canDo->get('core.create')) {
			JToolBarHelper::custom('articleimage.save2new', 'save-new.png', 'save-new_f2.png', 'JTOOLBAR_SAVE_AND_NEW', false);
		}
		if ($isNew) {
			JToolBarHelper::cancel('articleimage.cancel');
		} else {
			// for existing items the button is renamed `close`
            JToolBarHelper::cancel('articleimage.cancel', 'JTOOLBAR_CANCEL');
		}
	}	
}

