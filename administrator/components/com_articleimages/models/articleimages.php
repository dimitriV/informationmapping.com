<?php
/**
 * @version     $Id: articleimages.php  2010-10-23 04:13:25Z $
 * @package     Interline Design
 * @subpackage  com_articleimages
 * @author      ITOffshore Nepal
 * @copyright   Copyright (C) 2011 ITOffshore Nepal.
 * @license     ITOffshore Nepal
 */
// No direct access
defined('_JEXEC') or die('Restricted access');
// import the Joomla modellist library
jimport('joomla.application.component.modellist');
/**
 * Article Images Model
 *
 * @package    Interline Design
 * @subpackage Components
 */
class ArticleImagesModelArticleImages extends JModelList {

  protected $_context = 'com_articleimages.articleimages';
  
  /**
	 * Constructor.
	 *
	 * @param	array	An optional associative array of configuration settings.
	 * @see		JController
	 * @since	1.6
	 */
	public function __construct($config = array())
	{
		if (empty($config['filter_fields'])) {
			$config['filter_fields'] = array(
				'a.id',
				'a.small_image',
                'a.large_image',
                'a.image_title',
				'c.title article',
                'a.link',
                'a.ordering',
				'cc.title',
				
			);
		}

		parent::__construct($config);
	}
  	/**
	 * Method to auto-populate the model state.
	 *
	 * This method should only be called once per instantiation and is designed
	 * to be called on the first call to the getState() method unless the model
	 * configuration flag to ignore the request is set.
	 *
	 * @return	void
	 */  
  
  protected function populateState($ordering = null, $direction = null)
	{
		// Initialise variables.
		$app		= JFactory::getApplication();
	

		// Load the filter state.
		$search = $app->getUserStateFromRequest($this->context.'.filter.search', 'filter_search');
		$this->setState('filter.search', $search);
        
        $access = $app->getUserStateFromRequest($this->context.'.filter.access', 'filter_access', 0, 'int');
		$this->setState('filter.access', $access);
        
        $categoryId = $this->getUserStateFromRequest($this->context.'.filter.category_id', 'filter_category_id', '');
		$this->setState('filter.category_id', $categoryId);

		$language = $this->getUserStateFromRequest($this->context.'.filter.language', 'filter_language', '');
		$this->setState('filter.language', $language);
		// List state information.
		parent::populateState('c.title', 'asc');
        
        
	}

	/**
	 * Method to get a store id based on model configuration state.
	 *
	 * This is necessary because the model is used by the component and
	 * different modules that might need different sets of data or different
	 * ordering requirements.
	 *
	 * @param	string		$id	A prefix for the store id.
	 *
	 * @return	string		A store id.
	 */
	protected function getStoreId($id = '')
	{
		// Compile the store id.
		$id	.= ':'.$this->getState('filter.search');
		$id	.= ':'.$this->getState('filter.access');
        $id	.= ':'.$this->getState('filter.category_id');
		$id .= ':'.$this->getState('filter.language');
		return parent::getStoreId($id);
	}

	/**
	 * Build an SQL query to load the list data.
	 *
	 * @return	JDatabaseQuery
	 */
	protected function getListQuery()
	{
		// Create a new query object.
		$db = $this->getDbo();
		$query = $db->getQuery(true);

		// Select the required fields from the table.
		$query->select(
			$this->getState(
				'list.select',
                'a.*,'.
                'c.title article,'.
                'cc.title category' 
				                
			)
		);
        $query->select('cc.title category, cc.alias, cc.note, cc.path, cc.level');
		$query->from('`#__article_images` AS a');

	   
        // Join over the categories 
       	$query->join('LEFT', '#__content AS c ON c.id = a.article_id');
        
        // Join over the categories 
       	$query->join('LEFT', '#__categories AS cc ON cc.id = c.catid');
        
        $query->where('c.state = 1');
        
        // Filter by category.
		$categoryId = $this->getState('filter.category_id');
		if (is_numeric($categoryId)) {
			$query->where('cc.id = '.(int) $categoryId);
		}

        
		// Filter by search in title
		$search = $this->getState('filter.search');
		if (!empty($search)) {
			if (stripos($search, 'id:') === 0) {
				$query->where('a.id = '.(int) substr($search, 3));
			}
			
			else {
				$search = $db->Quote('%'.$db->getEscaped($search, true).'%');
				$query->where('(c.title LIKE '.$search.' OR a.image_title LIKE '.$search.' OR cc.title LIKE '.$search.' )');
			}
		} 

		// Add the list ordering clause.
		  $query->order($db->getEscaped($this->getState('list.ordering', 'a.id')).' '.$db->getEscaped($this->getState('list.direction', 'ASC')));
          #echo nl2br(str_replace('#__','jos_',$query));

       	 return $query;
	}
    	/**
	 * Method to remove the selected items
	 *
	 * @return	boolean	true of false in case of failure
	 */

	 public function remove() {
		// Get the selected items
		$selected = &JRequest::getVar('cid', 0, '', 'array');
		// Get a Skool Domain row instance
		$table = $this->getTable('articleimage');
		for ($i = 0, $count = count($selected);$i < $count;$i++) {
			// Load the row.
			$return = $table->load($selected[$i]);
			// Check for an error.
			if ($return === false) {
				$this->setError($table->getError());
				return false;
			}
            $unlinkImage = $this->getImageName($selected[$i]);
			// Delete the row.
			$return = $table->delete();
			// Check for an error.
			if ($return === false) {
				$this->setError($table->getError());
				return false;
			}
            @unlink(JPATH_ROOT.DS.'images'.DS.'articleimages'.DS.'small'.DS.$unlinkImage);
		}
        
                
		return true;
	}
    public function getImageName($id){
        $db = JFactory::getDbo();
        $sql = "SELECT small_image FROM #__article_images WHERE id=".$id;
        $db->setQuery($sql);
        return $db->loadResult();
    }
    	/**
	 * A protected method to get a set of ordering conditions.
	 *
	 * @param	object	$table	A JTable object.
	 *
	 * @return	array	An array of conditions to add to ordering queries.
	 * @since	1.6
	 */
	protected function getReorderConditions($table)
	{
		return array();
	}
    /**
	 * Saves the manually set order of records.
	 *
	 * @param	array	$pks	An array of primary key ids.
	 * @param	int		$order	+/-1
	 *
	 * @return	mixed
	 * @since	1.6
	 */
	function saveorder($pks = null, $order = null)
	{
		// Initialise variables.
		$table		= $this->getTable('ArticleImage');
		$conditions	= array();
		$user = JFactory::getUser();

		if (empty($pks)) {
			return JError::raiseWarning(500, JText::_($this->text_prefix.'_ERROR_NO_ITEMS_SELECTED'));
		}

		// update ordering values
		foreach ($pks as $i => $pk) {
			$table->load((int) $pk);

		  if ($table->ordering != $order[$i]) {
				$table->ordering = $order[$i];

				if (!$table->store()) {
					$this->setError($table->getError());
					return false;
				}

				// remember to reorder within position and client_id
				$condition = $this->getReorderConditions($table);
				$found = false;

				foreach ($conditions as $cond) {
					if ($cond[1] == $condition) {
						$found = true;
						break;
					}
				}

				if (!$found) {
					$key = $table->getKeyName();
					$conditions[] = array ($table->$key, $condition);
				}
			}
		}

		// Execute reorder for each category.
		foreach ($conditions as $cond) {
			$table->load($cond[0]);
			$table->reorder($cond[1]);
		}

		// Clear the component's cache
		$cache = JFactory::getCache($this->option);
		$cache->clean();

		return true;
	}
    	/**
	 * Method to adjust the ordering of a row.
	 *
	 * Returns NULL if the user did not have edit
	 * privileges for any of the selected primary keys.
	 *
	 * @param	int				$pks	The ID of the primary key to move.
	 * @param	integer			$delta	Increment, usually +1 or -1
	 *
	 * @return	boolean|null	False on failure or error, true on success.
	 * @since	1.6
	 */
	public function reorder($pks, $delta = 0)
	{
		// Initialise variables.
		$user	= JFactory::getUser();
		$table	= $this->getTable('ArticleImage');
		$pks	= (array) $pks;
		$result	= true;

		$allowed = true;

		foreach ($pks as $i => $pk) {
			$table->reset();

			if ($table->load($pk)) {
				$where = array();
				$where = $this->getReorderConditions($table);

				if (!$table->move($delta, $where)) {
					$this->setError($table->getError());
					unset($pks[$i]);
					$result = false;
				}

			} else {
				$this->setError($table->getError());
				unset($pks[$i]);
				$result = false;
			}
		}

		if ($allowed === false && empty($pks)) {
			$result = null;
		}

		if ($result == true) {
			// Clear the component's cache
			$cache = JFactory::getCache($this->option);
			$cache->clean();
		}

		return $result;
	}
    
}

