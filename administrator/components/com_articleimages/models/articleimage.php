<?php
/**
 * @version     $Id: articleimage.php  2010-10-23 04:13:25Z $
 * @package     Interline Design
 * @subpackage  com_articleimages
 * @author      ITOffshore Nepal
 * @copyright   Copyright (C) 2011 ITOffshore Nepal.
 * @license     ITOffshore Nepal
 */
// No direct access
defined('_JEXEC') or die('Restricted access');
// import Joomla modelitem library
jimport('joomla.application.component.modelform');
/**
 * Article Image Model
 *
 * @package    Interline Design
 * @subpackage Components
 */
class ArticleImagesModelArticleImage extends JModelForm {
	/**
	 * @var array data
	 */
        protected $data = null;
                
                        
        public function &getData() 
        {
                  		
        if (empty($this->data)) 
                {
                        $data = & JRequest::getVar('jform');
                        if (empty($data)) 
                        {
                                $selected = &JRequest::getVar('cid', 0, '', 'array');
				
                				// Select all fields from the skool_domains table.
                                $db = $this->getDbo();
                		        $query = $db->getQuery(true);
                				$query->select('a.*');
                				$query->from('`#__article_images` AS a');
                                $query->where('a.id = ' . (int)$selected[0]);
                				$this->_db->setQuery((string)$query);
                				$data = & $this->_db->loadAssoc();
                        }
                        $this->data = $data;
                }
                return $this->data;

        }
        /**
         * Method to get the SkoolDomain form.
         *
         * @access      public
         * @return      mixed   JForm object on success, false on failure.
         * @since       1.0
         */
         
             
       
       public function getForm($data = array(), $loadData = true)
	     {
	      
		  $form = $this->loadForm('com_.articleimages', 'articleimage', array('control' => 'jform', 'load_data' => $loadData));
		  if (empty($form)) {
			return false;
		}

		return $form;
	   }
   
              
       function save() {
            $data = &$this->getData();
            $img_path = '';
            if($_FILES['jform']['tmp_name']['article_small_image']){
                    $source = $_FILES['jform']['tmp_name']['article_small_image'];
                    $file_name =  mktime().'-'.strtolower(str_replace(' ','-',$_FILES['jform']['name']['article_small_image']));
                    
                    // Small image full path
                    $img_path = JPATH_ROOT.DS.'images'.DS.'articleimages'.DS.'small'.DS.$file_name;
                    
                    // Large image full path
                    $img_path_large = JPATH_ROOT.DS.'images'.DS.'articleimages'.DS.'large'.DS.$file_name;
                   
                    $system=explode('.',$file_name);
                    $ext = $system[count($system)-1];
           
        		if (preg_match('/jpg|jpeg|JPG|JPEG|gif|GIF|png|PNG|bmp|BMP/',$ext))
        		{
                    // Creating large images
                    if(copy($source, $img_path_large))
                    {
                        list($w,$h) = getimagesize($source);    
                          if($w >= 490){
                               $this->createthumb($img_path_large,$img_path_large,490,290);
                            }
                             list($w1,$h1) = getimagesize($img_path_large); 
                          if($h1 >= 290){
                            $this->createthumb($img_path_large,$img_path_large,490,290);
                          }                   
                          if(isset($data['large_image']))
                            $backup = $data['large_image'];
                        $data['large_image'] =  $file_name;
                    }
                    
                    // Creating small images
      	             if(copy($source, $img_path))
                        {
                        copy($source, $img_path_large);
                        list($w,$h) = getimagesize($source);    
                          if($w >= 200){
                               $this->createthumb($img_path,$img_path,200,200);
                            }
                          list($w1,$h1) = getimagesize($img_path); 
                          if($h1 >= 200){
                            $this->createthumb($img_path,$img_path,200,200);
                          }
                      if(isset($data['small_image']))
                        $backup = $data['small_image'];
                      
                        $data['small_image'] =  $file_name;
                		}
                		else
                		{
                	
                		$msg = JText::_('upload failer try later');
                	    $this->setError($msg);
            			return false;  
                		}
                  }
                  else{
                        $msg = JText::_('file not supported');
                	    $this->setError($msg);
            			return false;  
                  }
            }
            // Database processing
    		$row = & $this->getTable();
    		// Bind the form fields to the skool_domains table
    		if (!$row->save($data)) {
                @unlink($img_path);
    			$this->setError($row->getErrorMsg());
    			return false;
    		}
            else{
                if($_FILES['jform']['tmp_name']['article_small_image'] && $data['id']){
                    @unlink(JPATH_ROOT.DS.'images'.DS.'articleimages'.DS.'small'.DS.$backup);
                }
                //JRequest::setVar('_cid', $row->id);
                return $row->id;
            }
            
            
    		return true;
    	}
        function createthumb($name,$filename,$new_w,$new_h)
        {
    		$system=explode('.',$name);
            $ext = $system[count($system)-1];
    		if (preg_match('/jpg|jpeg/',$ext)){
    		  $src_img=imagecreatefromjpeg($name);
    		}
    		elseif (preg_match('/png/',$ext)){
    		  $src_img=imagecreatefrompng($name);
    		}
            elseif (preg_match('/gif/',$ext)){
    		  $src_img=imagecreatefromgif($name);
    		}
            elseif (preg_match('/bmp/',$ext)){
    		  $src_img=$this->imagecreatefrombmp($name);
    		}
    		else{
        		$src_img=imagecreatefromjpeg($name);
    		}
    
    		$old_x=imageSX($src_img);
           	$old_y=imageSY($src_img);
            
            if($new_w == $new_h){
                $thumb_h = $new_h;
                $thumb_w = floor($old_x-($old_x*(($old_y-$new_h)/$old_y)));
            }
            else{
        		$thumb_w=$new_w;
        		$thumb_h=floor($old_y-($old_y*(($old_x-$new_w)/$old_x)));
            }
            
            //$thumb_w = 493;
            //$thumb_h = 290;
            
    		$dst_img=imagecreatetruecolor($thumb_w,$thumb_h);
            
            if (preg_match("/png|gif/",$ext)){
                imagealphablending($dst_img, false);
                imagesavealpha($dst_img,true);
                $transparent = imagecolorallocatealpha($dst_img, 255, 255, 255, 127);
                imagefilledrectangle($dst_img, 0, 0, $thumb_w, $thumb_h, $transparent);
            }
    		imagecopyresampled($dst_img,$src_img,0,0,0,0,$thumb_w,$thumb_h,$old_x,$old_y); 
    
    		if (preg_match("/png/",$ext)){
                imagepng($dst_img,$filename); 
            }
            elseif (preg_match('/gif/',$ext)){
                imagegif($dst_img,$filename);
    		}
            elseif (preg_match('/bmp/',$ext)){
                #$this->imagebmp($dst_img,$filename);
                 imagejpeg($dst_img,$filename,100);
    		}
            else{
                imagejpeg($dst_img,$filename,100);
    		}
    		imagedestroy($dst_img); 
    		imagedestroy($src_img); 
	}
///////////////////////////////////////////////////////////////   
    public function imagecreatefrombmp($p_sFile)
    {
        //    Load the image into a string
        $file    =    fopen($p_sFile,"rb");
        $read    =    fread($file,10);
        while(!feof($file)&&($read<>""))
            $read    .=    fread($file,1024);
       
        $temp    =    unpack("H*",$read);
        $hex    =    $temp[1];
        $header    =    substr($hex,0,108);
       
        //    Process the header
        //    Structure: http://www.fastgraph.com/help/bmp_header_format.html
        if (substr($header,0,4)=="424d")
        {
            //    Cut it in parts of 2 bytes
            $header_parts    =    str_split($header,2);
           
            //    Get the width        4 bytes
            $width            =    hexdec($header_parts[19].$header_parts[18]);
           
            //    Get the height        4 bytes
            $height            =    hexdec($header_parts[23].$header_parts[22]);
           
            //    Unset the header params
            unset($header_parts);
        }
       
        //    Define starting X and Y
        $x                =    0;
        $y                =    1;
       
        //    Create newimage
        $image            =    imagecreatetruecolor($width,$height);
       
        //    Grab the body from the image
        $body            =    substr($hex,108);

        //    Calculate if padding at the end-line is needed
        //    Divided by two to keep overview.
        //    1 byte = 2 HEX-chars
        $body_size        =    (strlen($body)/2);
        $header_size    =    ($width*$height);

        //    Use end-line padding? Only when needed
        $usePadding        =    ($body_size>($header_size*3)+4);
       
        //    Using a for-loop with index-calculation instaid of str_split to avoid large memory consumption
        //    Calculate the next DWORD-position in the body
        for ($i=0;$i<$body_size;$i+=3)
        {
            //    Calculate line-ending and padding
            if ($x>=$width)
            {
                //    If padding needed, ignore image-padding
                //    Shift i to the ending of the current 32-bit-block
                if ($usePadding)
                    $i    +=    $width%4;
               
                //    Reset horizontal position
                $x    =    0;
               
                //    Raise the height-position (bottom-up)
                $y++;
               
                //    Reached the image-height? Break the for-loop
                if ($y>$height)
                    break;
            }
           
            //    Calculation of the RGB-pixel (defined as BGR in image-data)
            //    Define $i_pos as absolute position in the body
            $i_pos    =    $i*2;
            $r        =    hexdec($body[$i_pos+4].$body[$i_pos+5]);
            $g        =    hexdec($body[$i_pos+2].$body[$i_pos+3]);
            $b        =    hexdec($body[$i_pos].$body[$i_pos+1]);
           
            //    Calculate and draw the pixel
            $color    =    imagecolorallocate($image,$r,$g,$b);
            imagesetpixel($image,$x,$height-$y,$color);
           
            //    Raise the horizontal position
            $x++;
        }
       
        //    Unset the body / free the memory
        unset($body);
       
        //    Return image-object
        return $image;
    }
      
}
