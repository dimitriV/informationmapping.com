<?php
/**
 * @version		$Id: bannerclient.php 16825 2010-05-05 12:10:37Z louis $
 * @copyright	Copyright (C) 2005 - 2010 Open Source Matters, Inc. All rights reserved.
 * @license		GNU General Public License version 2 or later; see LICENSE.txt
 */

defined('JPATH_BASE') or die;

jimport('joomla.form.formfield');


/**
 * Template Field class for the Joomla Framework.
 *
 * @package		Joomla.Framework
 * @subpackage	com_skooldomains
 * @since		1.6
 * */
class JFormFieldArticleMedia extends JFormFieldList
{
	/**
	 * The form field type.
	 *
	 * @var		string
	 * @since	1.6
	 */
	protected $type = 'Media';

	/**
	 * Method to get the field options.
	 *
	 * @return	array	The field option objects.
	 * @since	1.6
	 */
	public function getInput()
	{
	    $html = array();
        $folder = 'images/articleimages/large';
        $link = 'index.php?option=com_media&amp;view=images&amp;tmpl=component&amp;asset=com_articleimages&amp;fieldid='.$this->id.'&amp;folder='.$folder;
 	    $textButton	= 'COM_ARTICLEIMAGES_FORM_SELECT_MEDIA';
 	    //print_r($script);die();
        
        	// Initialize some field attributes.
		$attr = $this->element['class'] ? ' class="'.(string) $this->element['class'].'"' : '';
		$attr .= $this->element['size'] ? ' size="'.(int) $this->element['size'].'"' : '';
        
		// Initialize JavaScript field attributes.
		$onchange = (string) $this->element['onchange'];
        
		// Load the modal behavior script.
		JHtml::_('behavior.modal', 'a.modal_'.$this->id);
		
        // Build the script.
		$script = array();
		$script[] = '	function f_'.$this->id.'(value,id) {';
		$script[] = '		document.getElementById("'.$this->id.'_url").value = value;';
     	$script[] = '		'.$onchange;
		$script[] = '		SqueezeBox.close();';
		$script[] = '	}';
        
        JFactory::getDocument()->addScriptDeclaration(implode("\n", $script));

		$html[] = '<div class="fltlft">';
		$html[] = '	<input type="text" readonly=readonly id="'.$this->id.'_url" name="'.$this->name.'" value="'. $this->value.'"' .
					' '.$attr.' />';
		$html[] = '</div>';
		// Create the user select button.
		$html[] = '<div class="button2-left">';
		$html[] = '  <div class="blank">';
		$html[] = '		<a class="modal_'.$this->id.'" title="'.JText::_($textButton).'"' .
							' href="'.$link.'"' .
							' rel="{handler: \'iframe\', target:\'ext_id\'}">';
		$html[] = '			'.JText::_($textButton).'</a>';
		$html[] = '  </div>';
		$html[] = '</div>';

		return implode("\n", $html);
	}
}

