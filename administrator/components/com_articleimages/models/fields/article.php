<?php
/**
 * @version     $Id: article.php  2010-10-23 04:13:25Z $
 * @package     Interline Design
 * @subpackage  com_articleimages
 * @author      ITOffshore Nepal
 * @copyright   Copyright (C) 2011 ITOffshore Nepal.
 * @license     ITOffshore Nepal
 */

defined('JPATH_BASE') or die;

jimport('joomla.html.html');
jimport('joomla.form.formfield');
jimport('joomla.form.helper');
JFormHelper::loadFieldClass('list');

/**
 *
 * @package		Interline Design
 * @subpackage	com_articleimages
 * @since		1.5
 */
class JFormFieldArticle extends JFormFieldList
{
	/**
	 * The form field type.
	 *
	 * @var		string
	 * @since	1.6
	 */
	protected $type = 'Article';

	/**
	 * Method to get the field options.
	 *
	 * @return	array	The field option objects.
	 * @since	1.6
	 */
	public function getOptions()
	{
		// Initialize variables.
		$options = array();
        $db		= JFactory::getDbo();
        $query	= $db->getQuery(true);
        $query = "SELECT 
                    	a.id AS `value`,
                    	CASE 
                            WHEN CHAR_LENGTH(cc.title) THEN CONCAT_WS(' / ', cc.title, a.title)
                            ELSE CONCAT(' Uncategorised / ', a.title) END as `text`
                    FROM 
                    	#__content AS a 
                    	LEFT JOIN #__categories AS cc ON a.catid = cc.id
                   	WHERE 
                    	a.state = 1
                    ORDER BY 
                    	cc.title, a.title";
		$db->setQuery( $query );
		$options = $db->loadObjectList();

		// Check for a database error.
		if ($db->getErrorNum()) {
			JError::raiseWarning(500, $db->getErrorMsg());
		}

		// Merge any additional options in the XML definition.
		$options = array_merge(parent::getOptions(), $options);

		array_unshift($options, JHtml::_('select.option', '', JText::_('COM_ARTICLEIMAGES_NO_ARTICLE')));

		return $options;
	
  }
}
