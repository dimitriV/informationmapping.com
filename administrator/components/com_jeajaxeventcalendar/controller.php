<?php
/*
 * @package Joomla 1.6
 * @copyright Copyright (C) 2005 Open Source Matters. All rights reserved.
 * @license http://www.gnu.org/copyleft/gpl.html GNU/GPL, see LICENSE.php
 *
 * @package   JE Ajax Event Calendar
 * @copyright Copyright (C) 2009 - 2010 Open Source Matters. All rights reserved.
 * @license   http://www.gnu.org/licenses/lgpl.html GNU/LGPL, see LICENSE.php
 * Contact to : emailtohardik@gmail.com, joomextensions@gmail.com
 * Visit : http://www.joomlaextensions.co.in/
 */
jimport('joomla.application.component.controller');

$l['hm']	= 'Home';
$l['ct']	= 'Category';
$l['et']	= 'Event';
$l['fd']	= 'Fields';
//$l['fl']	= 'Form Layouy';
$l['es']	= 'Event Setting';
$l['etmp']	= 'Event Template';
$l['ab']	= 'About';
// Submenu view
$view	= JRequest::getVar( 'view', '', '', 'string', JREQUEST_ALLOWRAW );
if ($view == '' || $view == 'home') {
	JSubMenuHelper::addEntry(JText::_($l['hm']), 'index.php?option=com_jeajaxeventcalendar&view=home', true);
	JSubMenuHelper::addEntry(JText::_($l['ct']), 'index.php?option=com_jeajaxeventcalendar&view=category');
	JSubMenuHelper::addEntry(JText::_($l['et']), 'index.php?option=com_jeajaxeventcalendar&view=event' );
	JSubMenuHelper::addEntry(JText::_($l['fd']), 'index.php?option=com_jeajaxeventcalendar&view=fields' );
	//JSubMenuHelper::addEntry(JText::_($l['fl']), 'index.php?option=com_jeajaxeventcalendar&view=form_layout');
	JSubMenuHelper::addEntry(JText::_($l['es']), 'index.php?option=com_jeajaxeventcalendar&view=event_configration');
	JSubMenuHelper::addEntry(JText::_($l['etmp']), 'index.php?option=com_jeajaxeventcalendar&view=event_tempsetting');
	JSubMenuHelper::addEntry(JText::_($l['ab']), 'index.php?option=com_jeajaxeventcalendar&view=about');
}

if ($view == 'category') {
	JSubMenuHelper::addEntry(JText::_($l['hm']), 'index.php?option=com_jeajaxeventcalendar&view=home');
	JSubMenuHelper::addEntry(JText::_($l['ct']), 'index.php?option=com_jeajaxeventcalendar&view=category', true);
	JSubMenuHelper::addEntry(JText::_($l['et']), 'index.php?option=com_jeajaxeventcalendar&view=event' );
	JSubMenuHelper::addEntry(JText::_($l['fd']), 'index.php?option=com_jeajaxeventcalendar&view=fields' );
	//JSubMenuHelper::addEntry(JText::_($l['fl']), 'index.php?option=com_jeajaxeventcalendar&view=form_layout');
	JSubMenuHelper::addEntry(JText::_($l['es']), 'index.php?option=com_jeajaxeventcalendar&view=event_configration');
	JSubMenuHelper::addEntry(JText::_($l['etmp']), 'index.php?option=com_jeajaxeventcalendar&view=event_tempsetting');
	JSubMenuHelper::addEntry(JText::_($l['ab']), 'index.php?option=com_jeajaxeventcalendar&view=about');
}

if ($view == 'event') {
	JSubMenuHelper::addEntry(JText::_($l['hm']), 'index.php?option=com_jeajaxeventcalendar&view=home');
	JSubMenuHelper::addEntry(JText::_($l['ct']), 'index.php?option=com_jeajaxeventcalendar&view=category');
	JSubMenuHelper::addEntry(JText::_($l['et']), 'index.php?option=com_jeajaxeventcalendar&view=event', true );
	JSubMenuHelper::addEntry(JText::_($l['fd']), 'index.php?option=com_jeajaxeventcalendar&view=fields' );
	//JSubMenuHelper::addEntry(JText::_($l['fl']), 'index.php?option=com_jeajaxeventcalendar&view=form_layout');
	JSubMenuHelper::addEntry(JText::_($l['es']), 'index.php?option=com_jeajaxeventcalendar&view=event_configration');
	JSubMenuHelper::addEntry(JText::_($l['etmp']), 'index.php?option=com_jeajaxeventcalendar&view=event_tempsetting');
	JSubMenuHelper::addEntry(JText::_($l['ab']), 'index.php?option=com_jeajaxeventcalendar&view=about');
}

if ($view == 'fields') {
	JSubMenuHelper::addEntry(JText::_($l['hm']), 'index.php?option=com_jeajaxeventcalendar&view=home');
	JSubMenuHelper::addEntry(JText::_($l['ct']), 'index.php?option=com_jeajaxeventcalendar&view=category');
	JSubMenuHelper::addEntry(JText::_($l['et']), 'index.php?option=com_jeajaxeventcalendar&view=event' );
	JSubMenuHelper::addEntry(JText::_($l['fd']), 'index.php?option=com_jeajaxeventcalendar&view=fields', true );
	//JSubMenuHelper::addEntry(JText::_($l['fl']), 'index.php?option=com_jeajaxeventcalendar&view=form_layout');
	JSubMenuHelper::addEntry(JText::_($l['es']), 'index.php?option=com_jeajaxeventcalendar&view=event_configration');
	JSubMenuHelper::addEntry(JText::_($l['etmp']), 'index.php?option=com_jeajaxeventcalendar&view=event_tempsetting');
	JSubMenuHelper::addEntry(JText::_($l['ab']), 'index.php?option=com_jeajaxeventcalendar&view=about');
}

/*if ($view == 'form_layout') {
	JSubMenuHelper::addEntry(JText::_($l['hm']), 'index.php?option=com_jeajaxeventcalendar');
	JSubMenuHelper::addEntry(JText::_($l['ct']), 'index.php?option=com_jeajaxeventcalendar&view=category');
	JSubMenuHelper::addEntry(JText::_($l['et']), 'index.php?option=com_jeajaxeventcalendar&view=event' );
	JSubMenuHelper::addEntry(JText::_($l['fd']), 'index.php?option=com_jeajaxeventcalendar&view=fields' );
	JSubMenuHelper::addEntry(JText::_($l['fl']), 'index.php?option=com_jeajaxeventcalendar&view=form_layout', true);
	JSubMenuHelper::addEntry(JText::_($l['es']), 'index.php?option=com_jeajaxeventcalendar&view=event_configration');
	JSubMenuHelper::addEntry(JText::_($l['etmp']), 'index.php?option=com_jeajaxeventcalendar&view=event_tempsetting');
	JSubMenuHelper::addEntry(JText::_($l['ab']), 'index.php?option=com_jeajaxeventcalendar&view=about');
}*/

if ($view == 'event_configration') {
	JSubMenuHelper::addEntry(JText::_($l['hm']), 'index.php?option=com_jeajaxeventcalendar&view=home');
	JSubMenuHelper::addEntry(JText::_($l['ct']), 'index.php?option=com_jeajaxeventcalendar&view=category');
	JSubMenuHelper::addEntry(JText::_($l['et']), 'index.php?option=com_jeajaxeventcalendar&view=event' );
	JSubMenuHelper::addEntry(JText::_($l['fd']), 'index.php?option=com_jeajaxeventcalendar&view=fields' );
	//JSubMenuHelper::addEntry(JText::_($l['fl']), 'index.php?option=com_jeajaxeventcalendar&view=form_layout');
	JSubMenuHelper::addEntry(JText::_($l['es']), 'index.php?option=com_jeajaxeventcalendar&view=event_configration', true);
	JSubMenuHelper::addEntry(JText::_($l['etmp']), 'index.php?option=com_jeajaxeventcalendar&view=event_tempsetting');
	JSubMenuHelper::addEntry(JText::_($l['ab']), 'index.php?option=com_jeajaxeventcalendar&view=about');
}

if ($view == 'event_tempsetting') {
	JSubMenuHelper::addEntry(JText::_($l['hm']), 'index.php?option=com_jeajaxeventcalendar&view=home');
	JSubMenuHelper::addEntry(JText::_($l['ct']), 'index.php?option=com_jeajaxeventcalendar&view=category');
	JSubMenuHelper::addEntry(JText::_($l['et']), 'index.php?option=com_jeajaxeventcalendar&view=event' );
	JSubMenuHelper::addEntry(JText::_($l['fd']), 'index.php?option=com_jeajaxeventcalendar&view=fields' );
	//JSubMenuHelper::addEntry(JText::_($l['fl']), 'index.php?option=com_jeajaxeventcalendar&view=form_layout');
	JSubMenuHelper::addEntry(JText::_($l['es']), 'index.php?option=com_jeajaxeventcalendar&view=event_configration');
	JSubMenuHelper::addEntry(JText::_($l['etmp']), 'index.php?option=com_jeajaxeventcalendar&view=event_tempsetting', true);
	JSubMenuHelper::addEntry(JText::_($l['ab']), 'index.php?option=com_jeajaxeventcalendar&view=about');
}

if ($view == 'about') {
	JSubMenuHelper::addEntry(JText::_($l['hm']), 'index.php?option=com_jeajaxeventcalendar&view=home');
	JSubMenuHelper::addEntry(JText::_($l['ct']), 'index.php?option=com_jeajaxeventcalendar&view=category');
	JSubMenuHelper::addEntry(JText::_($l['et']), 'index.php?option=com_jeajaxeventcalendar&view=event' );
	JSubMenuHelper::addEntry(JText::_($l['fd']), 'index.php?option=com_jeajaxeventcalendar&view=fields' );
	//JSubMenuHelper::addEntry(JText::_($l['fl']), 'index.php?option=com_jeajaxeventcalendar&view=form_layout');
	JSubMenuHelper::addEntry(JText::_($l['es']), 'index.php?option=com_jeajaxeventcalendar&view=event_configration');
	JSubMenuHelper::addEntry(JText::_($l['etmp']), 'index.php?option=com_jeajaxeventcalendar&view=event_tempsetting');
	JSubMenuHelper::addEntry(JText::_($l['ab']), 'index.php?option=com_jeajaxeventcalendar&view=about', true);
}

?>