<?php 
/**
* @package   JE Ajax Event Calendar
* @copyright Copyright (C) 2009 - 2010 Open Source Matters. All rights reserved.
* @license   http://www.gnu.org/licenses/lgpl.html GNU/LGPL, see LICENSE.php
* Contact to : emailtohardik@gmail.com, joomextensions@gmail.com
* Visit : http://www.joomlaextensions.co.in/
**/

defined('_JEXEC') or die('Restricted access');
$option = JRequest::getVar('option','','request','string');
$search = JRequest::getVar('field_search');
// ================================= For Ordering =======================================================//
$listOrder	= $this->escape($this->lists['order']);
$listDirn	= $this->escape($this->lists['order_Dir']);
$saveOrder	= $listOrder == 'ordering';
if ($saveOrder)
{
	$saveOrderingUrl = 'index.php?tmpl=component&option='.$option.'&view=fields&task=saveOrderAjax';
	JHtml::_('sortablelist.sortable', 'list2', 'adminForm', strtolower($listDirn), $saveOrderingUrl);
}
$sortFields = $this->getSortFields();
// ================================= For Ordering =======================================================//
?>
<!---------------------------------------- Ordering Script ------------------------------------------>
<script type="text/javascript">
	Joomla.orderTable = function() {
		table = document.getElementById("sortTable");
		direction = document.getElementById("directionTable");
		order = table.options[table.selectedIndex].value;
		
		if (order != '<?php echo $listOrder; ?>') {
			dirn = 'asc';
		} else {
			dirn = direction.options[direction.selectedIndex].value;
		}
		Joomla.tableOrdering(order, dirn, '');
	}
</script>
<!---------------------------------------- End Ordering Script ------------------------------------------>
<script language="javascript" type="text/javascript">

Joomla.submitform =function submitform(pressbutton){
 
var form = document.adminForm;
   if (pressbutton)
    {form.task.value=pressbutton;}
     
	 if ((pressbutton=='add')||(pressbutton=='edit')||(pressbutton=='publish')||(pressbutton=='unpublish')
	 ||(pressbutton=='remove') )
	 {
	 //alert("any button is pressed");		 
	  form.view.value="fields_detail";
	 }
	try {
		form.onsubmit();
		}
	catch(e){}
	
	form.submit();
}

function submitform(pressbutton){
var form = document.adminForm;
   if (pressbutton)
    {form.task.value=pressbutton;}
	 if ((pressbutton=='publish')||(pressbutton=='unpublish')||(pressbutton=='saveorder'))
	 {		 
	  form.view.value="category_detail";
	 }
	try {
		form.onsubmit();
		}
	catch(e){}
	
	form.submit();

}

</script>

<form action="<?php echo 'index.php?option='.$option; ?>" method="post" name="adminForm" id="adminForm" >
  <div id="editcell">
    <table class="table table-striped" id="list2">
      <label style="font-weight:bold"></label>
      <thead>
        <tr>
			
          <td colspan="7"><b><?php echo JText::_( 'FORM' ); ?>:</b> <?php echo $this->lists['formdata']; ?>
            <input type="submit" name="search" value="Go" /></td>
        </tr>
        <tr>
          <td colspan="7"><b><?php echo JText::_( 'FIELD_SEARCH' ); ?>:</b>
            <input type="text" name="field_search" id="field_search" value="<?php echo $search;?>"/>
            <input type="submit" name="search1" value="Search" /></td>
        </tr>
        <tr>
		<!----------------------------------------For Ordering ------------------------------------------>
			<th width="1%" class="nowrap center hidden-phone">
				<?php echo JHtml::_('grid.sort', '<i class="icon-menu-2"></i>', 'ordering', $listDirn, $listOrder, null, 'asc', 'JGRID_HEADING_ORDERING'); ?>
			</th>
			<!----------------------------------------For Ordering ------------------------------------------>
          <th width="5"> <?php echo JText::_( 'NUM' ); ?> </th>
          <th width="20"> <input type="checkbox" name="toggle" value="" onclick="Joomla.checkAll(this);" />
          </th>
          <th class="title"> <?php echo JHTML::_('grid.sort', 'FIELD_TITLE', 'field_title', $this->lists['order_Dir'], $this->lists['order'] ); ?> </th>
          <th class="title"> <?php echo JHTML::_('grid.sort', 'FIELD_NAME', 'field_name', $this->lists['order_Dir'], $this->lists['order'] ); ?> </th>
          <th class="title"> <?php echo JHTML::_('grid.sort', 'FIELD_TYPE', 'field_type', $this->lists['order_Dir'], $this->lists['order'] ); ?> </th>
          <th> <?php echo JHTML::_('grid.sort','FORM_NAME', 'field_section', $this->lists['order_Dir'], $this->lists['order']); ?> </th>
		  
          <th width="5%" nowrap="nowrap"> <?php echo JHTML::_('grid.sort', 'PUBLISHED', 'published', $this->lists['order_Dir'], $this->lists['order'] ); ?> </th>
          <th width="5%" nowrap="nowrap"> <?php echo JHTML::_('grid.sort', 'ID', 'field_id', $this->lists['order_Dir'], $this->lists['order'] ); ?> </th>
        </tr>
      </thead>
      <?php
	$k = 0;
	//echo '<pre>';
//	print_r($this->fields);
//	exit;
	for ($i=0, $n=count( $this->fields ); $i < $n; $i++)
	{
		$row = &$this->fields[$i];
        $row->id = $row->field_id;
		$link 	= JRoute::_( 'index.php?option='.$option.'&view=fields_detail&task=edit&cid[]='. $row->field_id );
		
		$published 	= JHTML::_('grid.published', $row, $i );		
		
		?>
       <tr class="row<?php echo $i % 2; ?>" itemID="<?php echo $row->id;?>">
	<!----------------------------------------For Ordering ------------------------------------------>
		<td class="order nowrap center hidden-phone">
			<?php 
				$disableClassName = '';
				$disabledLabel	  = '';

				if (!$saveOrder) :
					$disabledLabel    = JText::_('JORDERINGDISABLED');
					$disableClassName = 'inactive tip-top';
				endif; ?>
				<span class="sortable-handler hasTooltip <?php echo $disableClassName; ?>" title="<?php echo $disabledLabel; ?>">
					<i class="icon-menu"></i>
				</span>
				<input type="text" style="display:none" name="order[]" size="5" value="<?php echo $row->ordering; ?>" class="width-20 text-area-order " />
		</td>
	<!----------------------------------------For Ordering ------------------------------------------>
        <td ><?php echo $this->pagination->getRowOffset( $i ); ?> </td>
        <td><?php echo JHTML::_('grid.id', $i, $row->id ); ?> </td>
        <td width="30%"><a href="<?php echo $link; ?>" title="<?php echo JText::_( 'EDIT_FIELDS' ); ?>"><?php echo $row->field_title; ?></a> </td>
        <td width="30%"><?php echo $row->field_name; ?> </td>
        <td width="30%"><?php
			    if ($row->field_type == 1) echo JText::_( 'TEXT_FIELD' );
			elseif ($row->field_type == 2) echo JText::_( 'TEXT_AREA' );
			elseif ($row->field_type == 3) echo JText::_( 'CHECK_BOX' );
			elseif ($row->field_type == 4) echo JText::_( 'RADIO_BUTTON' );
			elseif ($row->field_type == 5) echo JText::_( 'SELECT_BOX_SINGLE_SELECT' );
			elseif ($row->field_type == 6) echo JText::_( 'SELECT_BOX_MULTIPLE_SELECT' );
			elseif ($row->field_type == 7) echo JText::_( 'SELECT_COUNTRY_BOX' );
			elseif ($row->field_type == 9) echo JText::_( 'FILE' );
			elseif ($row->field_type == 8) echo JText::_( 'WYSIWYG' );
			elseif ($row->field_type == 10) echo JText::_( 'HR_TAG' );
			elseif ($row->field_type == 11) echo JText::_( 'LABEL' );
			elseif ($row->field_type == 12) echo JText::_( 'DATE' );
			elseif ($row->field_type == 13) echo JText::_( 'PASSWORD' );
			
			else  echo JText::_( 'SELECT_BOX' );
			 
			?>
        </td>
        <td class="order" width="30%"><?php // This field is taken model fields.php as  frm.name as frmname in query //
						echo $row->frmname;
					 //-----------------------------------------------------------------------//
				?>
        </td>
		 
        <td class="center">
			<div class="btn-group">
				<?php echo JHtml::_('jgrid.published', $row->published, $i, '', 'cb'); ?>
			</div>
		</td>
        <td align="center" width="5%"><?php echo $row->field_id; ?> </td>
      </tr>
      <?php
		$k = 1 - $k;
	}
	?>
      <tfoot>
      <td colspan="9"><?php echo $this->pagination->getListFooter(); ?> </td>
        </tfoot>
    </table>
  </div>
  <input type="hidden" name="view" value="fields" />
  <input type="hidden" name="task" value="" />
  <input type="hidden" name="boxchecked" value="0" />
   <!----------------------------------------For Ordering ------------------------------------------>
<input type="hidden" name="filter_order" id="filter_order" value="<?php echo $listOrder; ?>" />
<input type="hidden" name="filter_order_Dir" id="filter_order_Dir" value="<?php echo $listDirn; ?>" />
<!----------------------------------------For Ordering ------------------------------------------>
<?php echo JHTML::_( 'form.token' ); ?>
</form>
