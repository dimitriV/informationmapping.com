<?php
/**
* @package   JE Ajax Event Calendar
* @copyright Copyright (C) 2009 - 2010 Open Source Matters. All rights reserved.
* @license   http://www.gnu.org/licenses/lgpl.html GNU/LGPL, see LICENSE.php
* Contact to : emailtohardik@gmail.com, joomextensions@gmail.com
* Visit : http://www.joomlaextensions.co.in/
**/ 

defined( '_JEXEC' ) or die( 'Restricted access' );
jimport( 'joomla.application.component.view' );

class event_detailViewevent_detail extends JViewLegacy
{
	function display($tpl = null)
	{
		$document = & JFactory::getDocument();
		$document->setTitle( JText::_('EVENT_DETAIL') );
		$uri 		=& JFactory::getURI();
		$option = JRequest::getVar('option','','request','string');
		
		$url= $uri->root();
		$document->addScript($url.'components/'.$option.'/assets/color_picker/js/jquery.js');
		$document->addScript($url.'components/'.$option.'/assets/color_picker/js/colorpicker.js');
		$document->addScript($url.'components/'.$option.'/assets/color_picker/js/eye.js');
		$document->addScript($url.'components/'.$option.'/assets/color_picker/js/utils.js');
		$document->addScript($url.'components/'.$option.'/assets/color_picker/js/layout.js?ver=1.0.2');
		
		$document->addStyleSheet ($url.'components/'.$option.'/assets/colorpicker/css/colorpicker.css' ); 
		$document->addStyleSheet($url.'components/'.$option.'/assets/color_picker/css/colorpicker.css');
		
		$this->setLayout('default');
		$lists = array();
		$detail	=& $this->get('data');
		
		$isNew		= ($detail->id < 1);
		$text = $isNew ? JText::_( 'NEW' ) : JText::_( 'EDIT' );
		JToolBarHelper::title(   JText::_( 'event' ).': <small><small>[ ' . $text.' ]</small></small>' );
		
		JToolBarHelper::apply();
		JToolBarHelper::save();
		
		if ($isNew)  {
			JToolBarHelper::cancel();
		} else {
			JToolBarHelper::cancel( 'cancel', 'Close' );
		}
		
		$category	=& $this->get('categories');
		$sel_category = array();
		$sel_category[0]->value="0";
		$sel_category[0]->text=JText::_('SELECT_CATEGORY');
		$category=@array_merge($sel_category,$category);
		$lists['category'] 	= JHTML::_('select.genericlist',$category,'category', 'class="inputtext" onchange="category_availablity(this.value)" ', 'value', 'text', $detail->category );
		
		$user	=& $this->get('users');
		
		$sel_user = array();
		$sel_user[0]->value="0";
		$sel_user[0]->text=JText::_('ALL');
		$user=@array_merge($sel_user,$user);
		if($detail->usr!="")
			$user_selected	= explode('`',$detail->usr);
		else
			$user_selected	= 0;
		$lists['user'] 	= JHTML::_('select.genericlist',$user,  'usr[]', 'class="inputtext" multiple="multiple" ', 'value', 'text',$user_selected  );
		
		$options[] = JHtml::_('select.option', '0', JText::sprintf('No'));
		$options[] = JHtml::_('select.option', '1', JText::sprintf('Yes')); 
		
		$lists['published'] 	= JHTML::_('select.genericlist',$options,  'published', 'class="inputtext" ', 'value', 'text', $detail->published );

        	require_once JPATH_COMPONENT_SITE.'/helpers/template.php';
        	$lists['template'] = eventcrlistHelperTemplate::getTemplateSelect();
		
		$event_repeat = array();
		$event_repeat[]   = JHTML::_('select.option', '0"checked"',JText::_('NO_REPEAT'));
		$event_repeat[]   = JHTML::_('select.option', '1', JText::_('YEAR'));
		$event_repeat[]   = JHTML::_('select.option', '2', JText::_('MONTH'));
		$event_repeat[]   = JHTML::_('select.option', '3', JText::_('WEEK'));
		$event_repeat[]   = JHTML::_('select.option', '4', JText::_('MULTIPLE_DATES'));
		
		$lists['erepeat'] 		= JHTML::_('select.genericlist',$event_repeat,'erepeat', 'class="inputbox" onchange="select_repeat(this.value)" size="1" ', 'value', 'text', $detail->erepeat ); 
		
		$eventphoto	=& $this->get('eventphoto');
		
		$this->assignRef('lists',		$lists);
		$this->assignRef('usr',		$usr);
		$this->assignRef('eventphoto',		$eventphoto);
		$this->assignRef('detail',		$detail);
		$this->assignRef('request_url',	$uri->toString());

		parent::display($tpl);
	}
}
?>
