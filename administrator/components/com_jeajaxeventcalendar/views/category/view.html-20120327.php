<?php
/**
* @package   JE Ajax Event Calendar
* @copyright Copyright (C) 2009 - 2010 Open Source Matters. All rights reserved.
* @license   http://www.gnu.org/licenses/lgpl.html GNU/LGPL, see LICENSE.php
* Contact to : emailtohardik@gmail.com, joomextensions@gmail.com
* Visit : http://www.joomlaextensions.co.in/
**/ 

defined('_JEXEC') or die ('restricted access');
jimport('joomla.application.component.view');

class categoryViewcategory extends JView
{ 
	function display ($tpl=null)
	{
		global $context; 
		$post = JRequest::get ( 'post' );
   		$mainframe = JFactory::getApplication();
		$option	= JRequest::getVar('option', 'com_jeajaxeventcalendar','','string');
		$document = & JFactory::getDocument();
		$document->setTitle( JText::_('CATEGORY') );
   		JToolBarHelper::title(   JText::_( 'CATEGORY_MANAGMENT' ) ); 
		JToolBarHelper::addNewX();
 		JToolBarHelper::editListX();
		JToolBarHelper::deleteList();		
		JToolBarHelper::publishList();
		JToolBarHelper::unpublishList();	
		
		$filter_order     = $mainframe->getUserStateFromRequest( $context.'filter_order',      'filter_order', 	  'ename' );
		$filter_order_Dir = $mainframe->getUserStateFromRequest( $context.'filter_order_Dir',  'filter_order_Dir', 'asc' );
		$list['order'] 	= $filter_order;  
		$list['order_Dir'] = $filter_order_Dir;
		
		$event_id	= JRequest::getVar('event_id','','','int');
		$lists		= & $this->get( 'Data');
		
		$searchlists	= array();
		
		$category	= $this->get('category');
		$sel_cat	= array();
		$sel_cat[0]->text	= JText::_('SELECT_CATEGORY');	
		$sel_cat[0]->value	= '0';
		$category	= @array_merge($sel_cat,$category);
		$catid		= JRequest::getVar('catid', '','request','int');
		$searchlists['category'] 	= JHTML::_('select.genericlist',$category,  'catid', 'class="inputbox" size="1" onchange="selectsearch(this.value)"', 'value', 'text',$catid ); 
		
		$publish_op = array();
		$publish_op[]   	= JHTML::_('select.option', '-1',JText::_('SELECT_STATE'));
		$publish_op[]   	= JHTML::_('select.option', '1',JText::_('PUBLISHED'));
		$publish_op[]   	= JHTML::_('select.option', '0', JText::_('UNPUBLISHED'));
		
		$published	= JRequest::getVar('published', '-1','request','int');
		$searchlists['published'] = JHTML::_('select.genericlist',$publish_op,'published', 'class="inputbox" size="1" onchange="selectsearch(this.value)" ','value','text' ,$published); 
		
		$pagination = & $this->get( 'Pagination' );
		$this->assignRef('lists',	$lists);
		$this->assignRef('list',	$list);
		$this->assignRef('searchlists',	$searchlists);
		$this->assignRef('pagination',	$pagination);
		parent::display($tpl);
	}
}?>