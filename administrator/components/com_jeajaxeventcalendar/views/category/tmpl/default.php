<?php
/**
* @package   JE Ajax Event Calendar
* @copyright Copyright (C) 2009 - 2010 Open Source Matters. All rights reserved.
* @license   http://www.gnu.org/licenses/lgpl.html GNU/LGPL, see LICENSE.php
* Contact to : emailtohardik@gmail.com, joomextensions@gmail.com
* Visit : http://www.joomlaextensions.co.in/
**/ 

defined('_JEXEC') or die('Restricted access');
$option = JRequest::getVar('option','','','string');
$filter = JRequest::getVar('filter');
$model 	= $this->getModel ( 'category' );
$dynemicfields	= $model->checkdynemicfields(); 

// ================================= For Ordering =======================================================//
$listOrder	= $this->escape($this->lists['order']);
$listDirn	= $this->escape($this->lists['order_Dir']);
$saveOrder	= $listOrder == 'ordering';
if ($saveOrder)
{
	$saveOrderingUrl = 'index.php?tmpl=component&option='.$option.'&view=category&task=saveOrderAjax';
	JHtml::_('sortablelist.sortable', 'list2', 'adminForm', strtolower($listDirn), $saveOrderingUrl);
}
$sortFields = $this->getSortFields();
// ================================= For Ordering =======================================================//
?>
<!---------------------------------------- Ordering Script ------------------------------------------>
<script type="text/javascript">
	Joomla.orderTable = function() {
		table = document.getElementById("sortTable");
		direction = document.getElementById("directionTable");
		order = table.options[table.selectedIndex].value;
		
		if (order != '<?php echo $listOrder; ?>') {
			dirn = 'asc';
		} else {
			dirn = direction.options[direction.selectedIndex].value;
		}
		Joomla.tableOrdering(order, dirn, '');
	}
</script>
<!---------------------------------------- End Ordering Script ------------------------------------------>
<script language="javascript" type="text/javascript">

Joomla.submitform =function submitform(pressbutton){
var form = document.adminForm;
   if (pressbutton)
    {form.task.value=pressbutton;}
     
	 if ((pressbutton=='add')||(pressbutton=='edit')||(pressbutton=='publish')||(pressbutton=='unpublish')
	 ||(pressbutton=='remove')|| (pressbutton=='copy') )
	 {	

	  form.view.value="category_detail";
	 }
	try {
		form.onsubmit();
		}
	catch(e){}
	
	form.submit();
}

function submitform(pressbutton){
var form = document.adminForm;
   if (pressbutton)
    {form.task.value=pressbutton;}
	 if ((pressbutton=='publish')||(pressbutton=='unpublish'))
	 {		 
	  form.view.value="category_detail";
	 }
	try {
		form.onsubmit();
		}
	catch(e){}
	
	form.submit();

}

function selectsearch() {
	var form = document.adminForm;
	form.submit();
}

</script>

<form action="<?php echo 'index.php?option='.$option; ?>" method="post" name="adminForm" id="adminForm" >
  <table width="100%" border="0">
    <tr>
      <td></td>
      <td></td>
      <td align="right"><b><?php echo JText::_( 'Search :' ); ?></b></td>
      <td align="right" width="130px"><?php echo $this->searchlists['category']; ?></td>
      <td align="right" width="130px"><?php echo $this->searchlists['published']; ?></td>
    </tr>
  </table>
  <div id="editcell">
    <table class="table table-striped" id="list2">
      <thead>
        <tr>
		<!----------------------------------------For Ordering ------------------------------------------>
			<th width="1%" class="nowrap center hidden-phone">
				<?php echo JHtml::_('grid.sort', '<i class="icon-menu-2"></i>', 'ordering', $listDirn, $listOrder, null, 'asc', 'JGRID_HEADING_ORDERING'); ?>
			</th>
			<!----------------------------------------For Ordering ------------------------------------------>
          <th width="5%"> <?php echo JText::_( 'NUM' ); ?> </th>
          <th width="5%" class="title"> <input type="checkbox" name="toggle" value="" onclick="Joomla.checkAll(this);" />
          </th >
          <th width="5%" nowrap="nowrap"> <?php echo JHTML::_('grid.sort', 'ID', 'id', ''); ?> </th>
          <th > <?php echo JHTML::_('grid.sort','CATEGORY_TITLE', 'title',''); ?> </th>
          <?php if(count($dynemicfields)!=0) { ?>
          <th> <?php echo JText::_('DYNEMIC_FIELD_ORDER'); ?> </th>
          <?php } ?>
		   <th > <?php echo JHTML::_('grid.sort','PARENT', 'pcat_id',''); ?> </th>
		  
          <th width="5%" nowrap="nowrap"> <?php echo JHTML::_('grid.sort', 'PUBLISHED', 'published','' ); ?> </th>
        </tr>
      </thead>
<?php
	$k = 0; 
	for ($i=0, $n=count( $this->mycategory ); $i < $n; $i++)
	{
		$row = &$this->mycategory[$i];
        $row->id = $row->id;
		$link 	= JRoute::_( 'index.php?option='.$option.'&view=category_detail&task=edit&cid[]='. $row->id );
		
	 	$published 	= JHTML::_('grid.published', $row, $i );
		
		$parentcategory	= $model->getparentcategory($row->pcat_id);
		if(count($parentcategory)!=0)
			$pcat_name	= $parentcategory->ename;
		else
			$pcat_name	= '-';
		?>
      <tr class="row<?php echo $i % 2; ?>" itemID="<?php echo $row->id;?>">
	<!----------------------------------------For Ordering ------------------------------------------>
		<td class="order nowrap center hidden-phone">
			<?php 
				$disableClassName = '';
				$disabledLabel	  = '';

				if (!$saveOrder) :
					$disabledLabel    = JText::_('JORDERINGDISABLED');
					$disableClassName = 'inactive tip-top';
				endif; ?>
				<span class="sortable-handler hasTooltip <?php echo $disableClassName; ?>" title="<?php echo $disabledLabel; ?>">
					<i class="icon-menu"></i>
				</span>
				<input type="text" style="display:none" name="order[]" size="5" value="<?php echo $row->ordering; ?>" class="width-20 text-area-order " />
		</td>
	<!----------------------------------------For Ordering ------------------------------------------>
        <td class="order"><?php echo $this->pagination->getRowOffset( $i ); ?> </td>
        <td class="order"><?php echo JHTML::_('grid.id', $i, $row->id ); ?> </td>
        <td align="center"><?php echo $row->id; ?> </td>
        <td align="center"><a href="<?php echo $link; ?>" title="<?php echo JText::_( 'EDIT_TAG' ); ?>"><?php echo $row->ename; ?></a> </td>
        <?php if(count($dynemicfields)!=0) { 
			$formlaylink = JRoute::_( 'index.php?option='.$option.'&view=form_layout&field_section=1');
?>
        <td align="center"><a href="<?php echo $formlaylink; ?>">
          <?php  echo JText::_( 'CLICK_HERE_TO_SEE_LAYOUT' ); ?>
          </a></td>
        <?php	} ?>
		<td align="center">
			<?php echo $pcat_name; ?>
		</td>
		
        <td class="center">
			<div class="btn-group">
				<?php echo JHtml::_('jgrid.published', $row->published, $i, '', 'cb'); ?>
			</div>
		</td>
      </tr>
      <?php
		$k = 1 - $k;
	}
	?>
      <tfoot>
      <td colspan="9"><?php echo $this->pagination->getListFooter(); ?> </td>
        </tfoot>
    </table>
  </div>
  <input type="hidden" name="view" value="category" />
  <input type="hidden" name="task" value="" />
  <input type="hidden" name="option" value="<?php echo $option; ?>" />
  <input type="hidden" name="boxchecked" value="0" />
  <!----------------------------------------For Ordering ------------------------------------------>
<input type="hidden" name="filter_order" id="filter_order" value="<?php echo $listOrder; ?>" />
<input type="hidden" name="filter_order_Dir" id="filter_order_Dir" value="<?php echo $listDirn; ?>" />
<!----------------------------------------For Ordering ------------------------------------------>
<?php echo JHTML::_( 'form.token' ); ?>
</form>
