<?php
/**
* @package   JE Ajax Event Calendar
* @copyright Copyright (C) 2009 - 2010 Open Source Matters. All rights reserved.
* @license   http://www.gnu.org/licenses/lgpl.html GNU/LGPL, see LICENSE.php
* Contact to : emailtohardik@gmail.com, joomextensions@gmail.com
* Visit : http://www.joomlaextensions.co.in/
**/ 

defined( '_JEXEC' ) or die( 'Restricted access' );
jimport( 'joomla.application.component.view' );
 
class eventViewevent extends JView
{
	function __construct( $config = array())
	{
		 parent::__construct( $config );
	}
    
	function display($tpl = null)
	{	
		global $context;
		$mainframe = JFactory::getApplication();
		$document = & JFactory::getDocument();
		$document->setTitle( JText::_('EVENT') );
   		 
   		JToolBarHelper::title(   JText::_( 'EVENT_MANAGMENT' ) );   		
   		JToolBarHelper::addNewX();
 		JToolBarHelper::editListX();
		JToolBarHelper::deleteList();		
		JToolBarHelper::publishList();
		JToolBarHelper::unpublishList();
	   	
	   	$uri	=& JFactory::getURI();
		$filter_order     = $mainframe->getUserStateFromRequest( $context.'filter_order',      'filter_order', 	  'title' );
		$filter_order_Dir = $mainframe->getUserStateFromRequest( $context.'filter_order_Dir',  'filter_order_Dir', 'asc' );
		$event = $mainframe->getUserStateFromRequest( $context.'event','event',0 );
		$lists['order'] 		= $filter_order;  
		$lists['order_Dir'] = $filter_order_Dir;
		$event			= & $this->get( 'Data');
		$total			= & $this->get( 'Total');
		$pagination = & $this->get( 'Pagination' );
		
	 	$searchlists	= array();
		
		$category	= $this->get('category');
		$sel_cat	= array();
		$sel_cat[0]->text	= JText::_('SELECT_CATEGORY');	
		$sel_cat[0]->value	= '0';
		$category	= @array_merge($sel_cat,$category);
		$sel_catid		= JRequest::getVar('catid', '0','request','int');
		$searchlists['category'] 	= JHTML::_('select.genericlist',$category, 'catid', 'class="inputbox" size="1" onchange="selectsearch(this.value)"', 'value', 'text',$sel_catid );
		
		$allevent	= $this->get('allevent');
		$sel_allevent	= array();
		$sel_allevent[0]->text	= JText::_('SELECT_EVENT');	
		$sel_allevent[0]->value	= '0';
		$allevent	= @array_merge($sel_allevent,$allevent);
		$sel_eventid		= JRequest::getVar('eventid', '0','request','int');
		
		$searchlists['allevent'] 	= JHTML::_('select.genericlist',$allevent,'eventid', 'class="inputbox" size="1" onchange="selectsearch(this.value)"', 'value', 'text',$sel_eventid ); 
		
		
		$publish_op = array();
		$publish_op[]   	= JHTML::_('select.option', '-1',JText::_('SELECT_STATE'));
		$publish_op[]   	= JHTML::_('select.option', '1',JText::_('PUBLISHED'));
		$publish_op[]   	= JHTML::_('select.option', '0', JText::_('UNPUBLISHED'));
		
		$published	= JRequest::getVar('published', '-1','request','int');
		$searchlists['published'] = JHTML::_('select.genericlist',$publish_op,'published', 'class="inputbox" size="1" onchange="selectsearch(this.value)" ','value','text' ,$published); 
	
		$this->assignRef('lists',$lists); 
		$this->assignRef('searchlists',	$searchlists);    
		$this->assignRef('event',$event); 		
		$this->assignRef('pagination',$pagination);
		$this->assignRef('request_url',	$uri->toString());    	
    	parent::display($tpl);
  }
}
?>
