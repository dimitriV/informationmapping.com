<?php 
/**
* @package   JE Ajax Event Calendar
* @copyright Copyright (C) 2009 - 2010 Open Source Matters. All rights reserved.
* @license   http://www.gnu.org/licenses/lgpl.html GNU/LGPL, see LICENSE.php
* Contact to : emailtohardik@gmail.com, joomextensions@gmail.com
* Visit : http://www.joomlaextensions.co.in/
**/ 

defined('_JEXEC') or die('Restricted access');
 
$option = JRequest::getVar('option','','','string');
$filter = JRequest::getVar('filter');
$model 	= $this->getModel ( 'event' ); 
$dynemicfields	= $model->checkdynemicfields();
?>
<script language="javascript" type="text/javascript">

Joomla.submitform =function submitform(pressbutton){
var form = document.adminForm;
   if (pressbutton)
    {form.task.value=pressbutton;}
     
	 if ((pressbutton=='add')||(pressbutton=='edit')||(pressbutton=='publish')||(pressbutton=='unpublish')
	 ||(pressbutton=='remove')|| (pressbutton=='copy') )
	 {		 
	  form.view.value="event_detail";
	 }
	try {
		form.onsubmit();
		}
	catch(e){}
	
	form.submit();
}

function submitform(pressbutton){
var form = document.adminForm;
   if (pressbutton)
    {form.task.value=pressbutton;}
	 if ((pressbutton=='publish')||(pressbutton=='unpublish'))
	 {		 
	  form.view.value="event_detail";
	 }
	try {
		form.onsubmit();
		}
	catch(e){}
	
	form.submit();
}

function selectsearch() {
	var form = document.adminForm;
	form.submit();
}

</script>

<form action="<?php echo 'index.php?option='.$option; ?>" method="post" name="adminForm" id="adminForm">
  <div id="editcell">
    <table width="100%" border="0">
      <tr>
        <td></td>
        <td></td>
        <td></td>
        <td></td>
		<td></td>
        <td></td>
        <td align="right"><b><?php echo JText::_( 'Search :' ); ?></b></td>
        <td align="right" width="130px"><?php echo $this->searchlists['category']; ?></td>
        <td align="right" width="130px"><?php echo $this->searchlists['allevent']; ?></td>
        <td align="right" width="130px"><?php echo $this->searchlists['published']; ?></td>
      </tr>
    </table>
    <table class="table table-striped">
      <thead>
        <tr>
          <th width="5%"> <?php echo JText::_( 'NUM' ); ?> </th>
          <th width="5%" class="title"> <input type="checkbox" name="toggle" value="" onclick="Joomla.checkAll(this);" />
          </th >
          <th width="5%" nowrap="nowrap"> <?php echo JHTML::_('grid.sort', 'ID', 'id', $this->lists['order_Dir'], $this->lists['order'] ); ?> </th>
          <th > <?php echo JHTML::_('grid.sort','EVENT_TITLE', 'title', $this->lists['order_Dir'], $this->lists['order']); ?> </th>
          <th > <?php echo JHTML::_('grid.sort','CATEGORY', 'category', $this->lists['order_Dir'], $this->lists['order']); ?> </th>
          <?php if(count($dynemicfields)!=0) { ?>
          <th > <?php echo JText::_('DYNEMIC_FIELD_ORDER'); ?> </th>
          <?php } ?>
          <th > <?php echo JHTML::_('grid.sort', 'EVENT_START_DATE', 'start_date', $this->lists['order_Dir'], $this->lists['order'] ); ?> </th>
          <th > <?php echo JHTML::_('grid.sort', 'EVENT_END_DATE', 'end_date', $this->lists['order_Dir'], $this->lists['order'] ); ?> </th>
		  <th > <?php echo JHTML::_('grid.sort', 'REPEAT_EVENT', 'erepeat', $this->lists['order_Dir'], $this->lists['order'] ); ?> </th>
		  <th > <?php echo JHTML::_('grid.sort', 'HITS', 'ehits', $this->lists['order_Dir'], $this->lists['order'] ); ?> </th>
		  
          <th width="5%" nowrap="nowrap"> <?php echo JHTML::_('grid.sort', 'PUBLISHED', 'published', $this->lists['order_Dir'], $this->lists['order'] ); ?> </th>
        </tr>
      </thead>
      <?php
		
	$k = 0;
	for ($i=0, $n=count( $this->event ); $i < $n; $i++)
	{
		$row = &$this->event[$i];
        $row->id = $row->id;
		$link 	= JRoute::_( 'index.php?option='.$option.'&view=event_detail&task=edit&cid[]='. $row->id );
		
		$published 	= JHTML::_('grid.published', $row, $i );		
		if($row->erepeat==0)
			$event_repeat	= JText::_('NO_REPEAT');
		if($row->erepeat==1)
			$event_repeat	= JText::_('YEAR');
		if($row->erepeat==2)
			$event_repeat	= JText::_('MONTH');
		if($row->erepeat==3)
			$event_repeat	= JText::_('WEEK');
		if($row->erepeat==4)
			$event_repeat	= JText::_('MULTIPLE_DATES');
		
?>
      <tr class="<?php echo "row$k"; ?>">
        <td class="order"><?php echo $this->pagination->getRowOffset( $i ); ?> </td>
        <td class="order"><?php echo JHTML::_('grid.id', $i, $row->id ); ?> </td>
        <td align="center"><?php echo $row->id; ?> </td>
        <td align="center"><a href="<?php echo $link; ?>" title="<?php echo JText::_( 'EDIT_TAG' ); ?>"><?php echo $row->title; ?></a> </td>
        <td align="center"><?php echo $row->ename; ?> </td>
        <?php 	if(count($dynemicfields)!=0) { 
			$formlaylink = JRoute::_( 'index.php?option='.$option.'&view=form_layout&field_section=2');
			?>
        <td align="center"><a href="<?php echo $formlaylink; ?>">
          <?php  echo JText::_( 'CLICK_HERE_TO_SEE_LAYOUT' ); ?>
          </a></td>
        <?php	} ?>
        <td align="center"><?php echo $row->start_date; ?> </td>
        <td align="center"><?php echo $row->end_date; ?> </td>
		
		<td align="center"><?php echo $event_repeat; ?> </td>
		<td align="center"><?php echo $row->ehits; ?> </td>
         <td class="center">
			<div class="btn-group">
				<?php echo JHtml::_('jgrid.published', $row->published, $i, '', 'cb'); ?>
			</div>
		</td>
      </tr>
      <?php
		$k = 1 - $k;
	}
	?>
      <tfoot>
      <td colspan="10"><?php echo $this->pagination->getListFooter(); ?> </td>
        </tfoot>
    </table>
  </div>
  <input type="hidden" name="view" value="event" />
  <input type="hidden" name="task" value="" />
  <input type="hidden" name="boxchecked" value="0" />
  <input type="hidden" name="filter_order" value="<?php echo $this->lists['order']; ?>" />
  <input type="hidden" name="filter_order_Dir" value="<?php echo $this->lists['order_Dir']; ?>" />
</form>
