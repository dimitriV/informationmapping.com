<?php
 /**
* @package   JE Ajax Event Calendar
* @copyright Copyright (C) 2009 - 2010 Open Source Matters. All rights reserved.
* @license   http://www.gnu.org/licenses/lgpl.html GNU/LGPL, see LICENSE.php
* Contact to : emailtohardik@gmail.com, joomextensions@gmail.com
* Visit : http://www.joomlaextensions.co.in/
**/ 

defined('_JEXEC') or die('Restricted access');
JHTML::_('behavior.tooltip');
JHTMLBehavior::modal();
$uri =& JURI::getInstance();
$url= $uri->root();
$editor =& JFactory::getEditor();
JHTML::_('behavior.calendar');
$option = JRequest::getVar('option','','','string');
$res=new extra_field();
$fields= $res->list_all_field(1,$this->detail->id);
$extra=explode("`",$fields);

$image_dir	= $url."components/".$option."/assets/event/images/";

?>
<script language="javascript" type="text/javascript">
	function submitbutton(pressbutton) {
		var form = document.adminForm;
		if (pressbutton == 'cancel') {
			submitform( pressbutton );
			return;
		}
		submitform( pressbutton );
	}
</script>

<form action="index.php" method="post" name="adminForm" id="adminForm" enctype="multipart/form-data">
  <div class="">
    <fieldset class="adminform">
    <legend><?php echo JText::_( 'DETAILS' ); ?></legend>
    <table class="admintable" border="0">
      <tr>
        <td align="right" class="key"><label for="name"> <?php echo JText::_( 'CATEGORY_TITLE' ); ?>: </label>
        </td>
        <td><input class="text_area" type="text" name="ename" id="ename" value="<?php echo $this->detail->ename;?>" />
        </td>
      </tr>
	  
	  <tr>
			<td align="right" class="key">
				<label for="name">
					<?php echo JText::_( 'PARENT' ); ?>:
				</label>
			</td>
			<td>
				<?php echo $this->lists['pcat_id']; ?>				
			</td>
	</tr>
	 <tr>
			<td align="right" class="key" valign="top">
				<label for="name">
					<?php echo JText::_( 'IMAGE' ); ?>:
				</label>
			</td>
			<td>
				<input type="file" name="cimage" />
				<?php if($this->detail->cimage!='') { ?>
					<br /><a class="modal" href="<?php echo $image_dir.$this->detail->cimage; ?>"><img src="<?php echo $image_dir.'thumb_'.$this->detail->cimage; ?>" /></a>
				<?php }	 ?>
				<input type="hidden" name="old_cimage" value="<?php echo $this->detail->cimage;?>" />
			</td>
		</tr>
		<tr valign="top">
        	<td align="right" class="key"><label for="name"> <?php echo JText::_( 'DESCRIPTION' ); ?>: </label>
        </td>
        <td><?php $editor =& JFactory::getEditor(); ?>
          <?php echo $editor->display("edesc",$this->detail->edesc,600,500,'100','20','0');	?> 
		</td>
      </tr>
      <tr>
        <td align="right" class="key" valign="top"><label for="name"> <?php echo JText::_( 'PUBLISHED' ); ?>: </label>
        </td>
        <td><?php echo $this->lists['published']; ?> </td>
      </tr>
      <tr>
        <td colspan="2"><?php echo $extra[0];?>
    </table>
    </td>
    </tr>
    </table>
    </fieldset>
  </div>
  <div class="clr"></div>
  <input type="hidden" name="cid[]" value="<?php echo $this->detail->id; ?>" />
  <input type="hidden" name="task" value="" />
  <input type="hidden" name="view" value="category_detail" />
  <input type="hidden" name="option" value="<?php echo $option;?>" />
</form>
