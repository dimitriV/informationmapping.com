<?php
 /**
* @package   JE Ajax Event Calendar
* @copyright Copyright (C) 2009 - 2010 Open Source Matters. All rights reserved.
* @license   http://www.gnu.org/licenses/lgpl.html GNU/LGPL, see LICENSE.php
* Contact to : emailtohardik@gmail.com, joomextensions@gmail.com
* Visit : http://www.joomlaextensions.co.in/
**/ 


defined( '_JEXEC' ) or die( 'Restricted access' );
jimport( 'joomla.application.component.view' );

class category_detailViewcategory_detail extends JViewLegacy
{
	function display($tpl = null)
	{
		$document = & JFactory::getDocument();
		$document->setTitle( JText::_('CATEGORY_DETAIL') );
		$uri 		=& JFactory::getURI();
		$option = JRequest::getVar('option','','request','string');
		
		$this->setLayout('default');
		$lists = array();
		$detail	=& $this->get('data');
				
		$isNew		= ($detail->id < 1);
		$text = $isNew ? JText::_( 'NEW' ) : JText::_( 'EDIT' );
		JToolBarHelper::title(   JText::_( 'CATEGORY' ).': <small><small>[ ' . $text.' ]</small></small>' );
		
		JToolBarHelper::apply();
		JToolBarHelper::save();
		
		if ($isNew)  {
			JToolBarHelper::cancel();
		} else {
		
			JToolBarHelper::cancel( 'cancel', 'Close' );
		}
		 
		$options[] = JHtml::_('select.option', '0', JText::sprintf('No'));
		$options[] = JHtml::_('select.option', '1', JText::sprintf('Yes')); 
		
		$lists['published'] 	= JHTML::_('select.genericlist',$options,  'published', 'class="inputtext" ', 'value', 'text', $detail->published );
		
		
		$allpcategory	= $this->get('categories');
		$sel_pcat	= array();
		$sel_pcat[0]->text	= JText::_('SELECT_PARENT');	
		$sel_pcat[0]->value	= '0';
		$allpcategory	= @array_merge($sel_pcat,$allpcategory);
		
		$lists['pcat_id'] 	= JHTML::_('select.genericlist',$allpcategory,'pcat_id', 'class="inputbox" size="1" ', 'value', 'text',$detail->pcat_id );  
		
		
		$this->assignRef('lists',		$lists);
		$this->assignRef('detail',		$detail);
		$this->assignRef('request_url',	$uri->toString());

		parent::display($tpl);
	}
}
?>