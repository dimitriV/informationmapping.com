<?php
 /**
* @package   JE Ajax Event Calendar
* @copyright Copyright (C) 2009 - 2010 Open Source Matters. All rights reserved.
* @license   http://www.gnu.org/licenses/lgpl.html GNU/LGPL, see LICENSE.php
* Contact to : emailtohardik@gmail.com, joomextensions@gmail.com
* Visit : http://www.joomlaextensions.co.in/
**/ 

defined( '_JEXEC' ) or die( 'Restricted access' );
jimport( 'joomla.application.component.controller' );
 
class category_detailController extends JControllerLegacy
{
	function __construct( $default = array())
	{
		parent::__construct( $default );
	}	
	
	function display() {
		
		parent::display();
	}
	
	function save() {
		$mainframe = JFactory::getApplication();
		$post = JRequest::get ( 'post' );
		$desc = JRequest::getVar( 'edesc', '', 'post', 'string', JREQUEST_ALLOWRAW );
		$post["edesc"]=$desc;
		$post["calias"] = JRequest::getVar( 'calias', '', 'post', 'string', JREQUEST_ALLOWRAW );
		$option = JRequest::getVar('option','','request','string');
		$view = JRequest::getVar('view','','request','string');
		$cid = JRequest::getVar ( 'cid', array (0 ), 'post', 'array' );
		$post ['id'] = $cid [0];
		$model = $this->getModel ( 'category_detail' );
		$arr 	= JRequest::getVar ( 'calias', '', 'string', '' );
		$file 	= JRequest::getVar ( 'cimage', '', 'files', 'array' );
		
		$thumb	= $model->getconfigration();
		$myimage_maxsize	= $thumb->max_img_size;
		
		if($file['name']!='') {
			$filetype = strtolower(JFile::getExt($file['name']));//Get extension of the file
			$ufile_size	= $file['size']/1048576; // Get the file size in Mb
			
			if($filetype =='jpeg' || $filetype=='jpg' || $filetype =='png' || $filetype=='gif' ||$filetype =='bmp')
			{
				if($ufile_size<=$myimage_maxsize || $ufile_size==0)
				{
					if($post['old_cimage']!='') {
						$dest 	= JPATH_ROOT.'/'.'components/'.$option.'/assets/event/images/'.$post["old_cimage"];
							unlink($dest);
						$dest12 = JPATH_ROOT.'/'.'components/'.$option.'/assets/event/images/thumb_'.$post["old_cimage"];
							unlink($dest12);
					}
					
					$src	= $file['tmp_name'];
					$catimg_name	= time().'_'.$file['name'];
					
					$dest 	= JPATH_ROOT.'/'.'components/'.$option.'/assets/event/images/'.$catimg_name; 
						JFile::upload($src,$dest);	
					$dest1 	= JPATH_ROOT.'/'.'components/'.$option.'/assets/event/images/thumb_'.$catimg_name;
						copy($dest,$dest1);
					$img 	= new thumbnail();
					$dest 	= JPATH_ROOT.'/'.'components/'.$option.'/assets/event/images/'.$catimg_name;				
						
					$img->CreatThumb($filetype,$dest1,$dest,$thumb->thumb_width,$thumb->thumb_height);
					
					$post['cimage']	= $catimg_name;
				} else {
					$mylink = 'index.php?option='.$option.'&view='.$view.'&task=edit&cid[]='.$post ['id'];
					$msg = JText::_ ( 'YOU_CAN_UPLOAD' ).'&nbsp;'.$myimage_maxsize.JText::_('MB').'&nbsp;'.JText::_ ( 'IMAGE_FILE' );
					$mainframe->redirect( $mylink,$msg );
				}
			} else {
				$mylink = 'index.php?option='.$option.'&view='.$view.'&task=edit&cid[]='.$post ['id'];
				$msg = JText::_ ( 'PLEASE_UPLOAD_VALID_IMAGE_FILE' );
				$mainframe->redirect( $mylink,$msg );
			}
			
		} else {
			$post['cimage']	= $post['old_cimage'];
		}
		
		
		//++++++++++++++++++++++++++++++ STRING REPLACE  ++++++++++++++++++++++++++++++//
		$find = array(";",".","'",":",","," ","!","@","#","$","%","^","&","&","*","(",")","[","]","|","?","`","~","/","\",","|","-","<",">","=","+");
		$replace = "_";
		$newstr  = str_replace($find,$replace,$arr);
		$post['calias']	= $newstr;
		$catid=$model->store ( $post );
		
		if ($catid) {
		//----------------------------- Ordering ----------------------------------------------//
		if($cid[0]==0){	
			$sql = "SELECT max(ordering) As ordering FROM #__jeajx_cal_category ";
			$db		=& JFactory::getDBO();
			$db->setQuery($sql);
			$max  = $db->loadObject();
			
			if($max->ordering)
				$order = $max->ordering + 1;
			else
				$order = 1;
			
			$query = "UPDATE #__jeajx_cal_category SET ordering = ".$order." WHERE ename = '".$post['ename']."' "; 
			$db->setQuery($query);
			$db->query();
		}
		//----------------------------- Ordering ----------------------------------------------//	
			$msg = JText::_ ( 'CATEGORY_DETAIL_SAVED' );
		} else {
			$msg = JText::_ ( 'ERROR_SAVING_CATEGORY_DETAIL' );
		}
		if($post ['id'])
		{
			$count1=$post ['id'];
		}
		else
		{
			$db = JFactory::getDbo();
			$sql="SELECT id FROM  #__jeajx_cal_category where ename='".$post['ename']."' ";
			$db->setQuery($sql);
			$row_data=$db->loadObject();
			$count1=$row_data->id;
		}
		// =============== Code for Saving dynemic fields value =======================================
			$res11=new extra_field();
			$res11->extra_field_save($post,1,$count1);
		// ============================================================================================
		$this->setRedirect ( 'index.php?option=' . $option . '&view=category', $msg );
	}
	
	function apply() {
		
		$mainframe = JFactory::getApplication();
		$post = JRequest::get ( 'post' );
		$desc = JRequest::getVar( 'edesc', '', 'post', 'string', JREQUEST_ALLOWRAW );
		$post["edesc"]=$desc;
		$option = JRequest::getVar('option','','request','string');
		$view = JRequest::getVar('view','','request','string');
		$cid = JRequest::getVar ( 'cid', array (0 ), 'post', 'array' );
		$post ['id'] = $cid [0];
		$model = $this->getModel ( 'category_detail' );
		
		$file 	= JRequest::getVar ( 'cimage', '', 'files', 'array' );
		
		$thumb	= $model->getconfigration();
		$myimage_maxsize	= $thumb->max_img_size;
		
		if($file['name']!='') {
			$filetype = strtolower(JFile::getExt($file['name']));//Get extension of the file
			$ufile_size	= $file['size']/1048576; // Get the file size in Mb
			
			if($filetype =='jpeg' || $filetype=='jpg' || $filetype =='png' || $filetype=='gif' ||$filetype =='bmp')
			{
				if($ufile_size<=$myimage_maxsize || $ufile_size==0)
				{
					if($post['old_cimage']!='') {
						$dest 	= JPATH_ROOT.'/'.'components/'.$option.'/assets/event/images/'.$post["old_cimage"];
							unlink($dest);
						$dest12 = JPATH_ROOT.'/'.'components/'.$option.'/assets/event/images/thumb_'.$post["old_cimage"];
							unlink($dest12);
					}
					
					$src	= $file['tmp_name'];
					$catimg_name	= time().'_'.$file['name'];
					
					$dest 	= JPATH_ROOT.'/'.'components/'.$option.'/assets/event/images/'.$catimg_name; 
						JFile::upload($src,$dest);	
					$dest1 	= JPATH_ROOT.'/'.'components/'.$option.'/assets/event/images/thumb_'.$catimg_name;
						copy($dest,$dest1);
					$img 	= new thumbnail();
					$dest 	= JPATH_ROOT.'/'.'components/'.$option.'/assets/event/images/'.$catimg_name;				
						
					$img->CreatThumb($filetype,$dest1,$dest,$thumb->thumb_width,$thumb->thumb_height);
					
					$post['cimage']	= $catimg_name;
				} else {
					$mylink = 'index.php?option='.$option.'&view='.$view.'&task=edit&cid[]='.$post ['id'];
					$msg = JText::_ ( 'YOU_CAN_UPLOAD' ).'&nbsp;'.$myimage_maxsize.JText::_('MB').'&nbsp;'.JText::_ ( 'IMAGE_FILE' );
					$mainframe->redirect( $mylink,$msg );
				}
			} else {
				$mylink = 'index.php?option='.$option.'&view='.$view.'&task=edit&cid[]='.$post ['id'];
				$msg = JText::_ ( 'PLEASE_UPLOAD_VALID_IMAGE_FILE' );
				$mainframe->redirect( $mylink,$msg );
			}
			
		} else {
			$post['cimage']	= $post['old_cimage'];
		}
		
		//++++++++++++++++++++++++++++++ STRING REPLACE  ++++++++++++++++++++++++++++++//
		$find = array(";",".","'",":",","," ","!","@","#","$","%","^","&","&","*","(",")","[","]","|","?","`","~","/","\",","|","-","<",">","=","+");
		$replace = "_";
		$newstr  = str_replace($find,$replace,$arr);
		$post['calias']	= $newstr;
		
		if ($row=$model->store ( $post )) {
		//----------------------------- Ordering ----------------------------------------------//
		if($cid[0]==0){	
			$sql = "SELECT max(ordering) As ordering FROM #__jeajx_cal_category ";
			$db		=& JFactory::getDBO();
			$db->setQuery($sql);
			$max  = $db->loadObject();
			
			if($max->ordering)
				$order = $max->ordering + 1;
			else
				$order = 1;
			
			$query = "UPDATE #__jeajx_cal_category SET ordering = ".$order." WHERE ename = '".$post['ename']."' "; 
			$db->setQuery($query);
			$db->query();
		}
		//----------------------------- Ordering ----------------------------------------------//
			$msg = JText::_ ( 'CATEGORY_DETAIL_SAVED' );
		} else {
			$msg = JText::_ ( 'ERROR_SAVING_CATEGORY_DETAIL' );
		}
		if($post ['id'])
		{
			$count1=$post ['id'];
		}
		else
		{
			$db = JFactory::getDbo();
			$sql="SELECT id FROM  #__jeajx_cal_category where ename='".$post['ename']."' ";
			$db->setQuery($sql);
			$row_data=$db->loadObject();
			$count1=$row_data->id;
		}
		// =============== Code for Saving dynemic fields value =======================================
			$res11=new extra_field();
			$res11->extra_field_save($post,1,$count1);
		// ============================================================================================
		$this->setRedirect ( 'index.php?option='.$option .'&view=category_detail&task=edit&cid[]='.$row->id, $msg );
	}
	
	function remove() {
		$option = JRequest::getVar ('option','','','string');
		$cid = JRequest::getVar ( 'cid', array (0 ), 'post', 'array' );
		if (! is_array ( $cid ) || count ( $cid ) < 1) {
			JError::raiseError ( 500, JText::_ ( 'SELECT_AN_ITEM_TO_DELETE' ) );
		}
		$model = $this->getModel ( 'category_detail' );
		if (! $model->delete ( $cid )) {
			echo "<script> alert('" . $model->getError ( true ) . "'); window.history.go(-1); </script>\n";
		}
		$msg = JText::_ ( 'CATEGORY_DELETED_SUCCESSFULLY' );
		$this->setRedirect ( 'index.php?option='.$option.'&view=category',$msg );
	}
	
	function publish() {
		$option = JRequest::getVar ('option','','','string');
		$cid = JRequest::getVar ( 'cid', array (0 ), 'post', 'array' );
		if (! is_array ( $cid ) || count ( $cid ) < 1) {
			JError::raiseError ( 500, JText::_ ( 'SELECT_AN_ITEM_TO_PUBLISH' ) );
		}
		$model = $this->getModel ( 'category_detail' );
		if (! $model->publish ( $cid, 1 )) {
			echo "<script> alert('" . $model->getError ( true ) . "'); window.history.go(-1); </script>\n";
		}
		$msg = JText::_ ( 'CATEGORY_PUBLISHED_SUCCESSFULLY' );
		$this->setRedirect ( 'index.php?option='.$option.'&view=category',$msg );
	}
	
	function unpublish() {
		$option = JRequest::getVar ('option','','','string');
		$cid = JRequest::getVar ( 'cid', array (0 ), 'post', 'array' );
		if (! is_array ( $cid ) || count ( $cid ) < 1) {
			JError::raiseError ( 500, JText::_ ( 'SELECT_AN_ITEM_TO_UNPUBLISH' ) );
		}
		$model = $this->getModel ( 'category_detail' );
		if (! $model->publish ( $cid, 0 )) {
			echo "<script> alert('" . $model->getError ( true ) . "'); window.history.go(-1); </script>\n";
		}
		$msg = JText::_ ( 'CATEGORY_UNPUBLISHED_SUCCESSFULLY' );
		$this->setRedirect ( 'index.php?option='.$option.'&view=category',$msg );
	}
	
	function cancel() {
		$option = JRequest::getVar('option','','request','string');
		$msg = JText::_ ( 'CATEGORY_DETAIL_EDITING_CANCELLED' );
		$this->setRedirect ( 'index.php?option='.$option.'&view=category',$msg );
	}	 
	
}
?>