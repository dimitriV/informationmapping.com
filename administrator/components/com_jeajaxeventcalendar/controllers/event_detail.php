<?php
 /**
* @package   JE Ajax Event Calendar
* @copyright Copyright (C) 2009 - 2010 Open Source Matters. All rights reserved.
* @license   http://www.gnu.org/licenses/lgpl.html GNU/LGPL, see LICENSE.php
* Contact to : emailtohardik@gmail.com, joomextensions@gmail.com
* Visit : http://www.joomlaextensions.co.in/
**/ 

defined ( '_JEXEC' ) or die ( 'Restricted access' );
jimport ( 'joomla.application.component.controller' );
 
class event_detailController extends JControllerLegacy {

	function __construct($default = array()) {
		parent::__construct ( $default );
		$this->_table_prefix = '#__jeajx_';
		$this->registerTask ( 'add', 'edit' );
	}
	
	function edit() {
		JRequest::setVar ( 'view', 'event_detail' );
		JRequest::setVar ( 'layout', 'default' );
		JRequest::setVar ( 'hidemainmenu', 1 );
		parent::display ();
	
	}
	
	function save() { 
		$mainframe	= JFactory::getApplication();
		$db= & JFactory :: getDBO();
		$post = JRequest::get ( 'post' );
		$cnt=implode('`',$post['usr']);
  		$post['usr']=$cnt;
		$desc = JRequest::getVar( 'desc', '', 'post', 'string', JREQUEST_ALLOWRAW );
		$post["desc"]=$desc;
		$option = JRequest::getVar('option','','request','string');
		$view 	= JRequest::getVar('view','','request','string');
		
		$cid = JRequest::getVar ( 'cid', array (0 ), 'post', 'array' );
		$post ['id'] = $cid [0];
		
		$model = $this->getModel ( 'event_detail' );
		
		// ================================== Video Link Code ================================================= //
		if($post['youtubelink']) {
			$post['googlelink'] = '';
		}
		if($post['googlelink']) {
			$post['youtubelink'] = '';
		}
		// ================================== EOF Video Link Code ============================================= //
		
		if($post['erepeat']==0 || $post['erepeat']==1 || $post['erepeat']==2 || $post['erepeat']==3) {
			if($post['id']!=0) {
				$model->deletedateevent($post['id']);
			}
		}
		
		if($post['erepeat']==4) {
			$dailysdate 	= JRequest::getVar ( 'dailysdate', '', 'post', 'array' );
			$dailyedate 	= JRequest::getVar ( 'dailyedate', '', 'post', 'array' );
			
			$post['start_date']	= $dailysdate[0];
			$post['end_date']	= $dailyedate[0];
		}
		
		
// +++++++++++++++++++++++++++++++++++++++++++++++++++ Multiple image code ++++++++++++++++++++++++++++++++++++++++++++++++++ //		
		$file 	= JRequest::getVar ( 'extra_name', '', 'files', 'array' );
		$len 	= count ($file['name']);
		$pid 	= $post['mainphoto']-1;
		$main	=0;
		if($file["name"][$pid]=="")
			$pname = $post['value_id'][$pid];
		else
			$pname=$file["name"][$pid];
		
		
		$myeventid	= $model->store ( $post );
		if ($myeventid) {
			$msg = JText::_ ( 'EVENT_DETAIL_SAVED' );
		} else {
			$msg = JText::_ ( 'ERROR_SAVING_EVENT_DETAIL' );
		}
		
	// =========================================== Code for daily selected Event ============================================= //
		if($post['erepeat']==4) {
			$dailysdate 	= JRequest::getVar ( 'dailysdate', '', 'post', 'array' );
			$dailyedate 	= JRequest::getVar ( 'dailyedate', '', 'post', 'array' );
			if($dailysdate[0]!='') {
				$model->deletedateevent($myeventid);
				for($l=0;$l<count($dailysdate);$l++) 
					$model->insertdateevent($myeventid,$dailysdate[$l],$dailyedate[$l]);
			}
		}
	// ========================================= EOF Code for daily selected Event =========================================== //	
		$thumb	= $model->getconfigration();
		$myimage_maxsize	= $thumb->max_img_size;
		
		for($i=0;$i<$len;$i++)
		{
			if($file['name'][$i]!="")
			{
				$iname='extra_name';
				$file =& JRequest::getVar($iname, '', 'files', 'array' );//Get File name, tmp_name
				$row->image= JPath::clean(time().'_'.$file['name'][$i]);//Make the filename unique
				$filetype = strtolower(JFile::getExt($file['name'][$i]));//Get extension of the file
				$ufile_size	= $file['size'][$i]/1048576; // Get the file size in Mb
				
				if($filetype =='jpeg' || $filetype=='jpg' || $filetype =='png' || $filetype=='gif' ||$filetype =='bmp')
				{					
					if($ufile_size<=$myimage_maxsize || $ufile_size==0) {
						$src	= $file['tmp_name'][$i];
						$dest 	= JPATH_ROOT.'/'.'components/'.$option.'/assets/event/images/'.$row->image ; 
							JFile::upload($src,$dest);	
						$dest1 	= JPATH_ROOT.'/'.'components/'.$option.'/assets/event/images/thumb_'.$row->image;
							copy($dest,$dest1);
						$img 	= new thumbnail();
						$dest 	= JPATH_ROOT.'/'.'components/'.$option.'/assets/event/images/'.$row->image;				
						
						//$thumb	= $model->getconfigration();
						$img->CreatThumb($filetype,$dest1,$dest,$thumb->thumb_width,$thumb->thumb_height);
						
						if($post['value_id'][$i])
						{
							$sql 	= "DELETE FROM ".$this->_table_prefix."event_photo WHERE image='".$post["value_id"][$i]."'";
							$db->setQuery($sql);
							$db->query();
							$dest 	= JPATH_ROOT.'/'.'components/'.$option.'/assets/event/images/'.$post["value_id"][$i];
								unlink($dest);
							$dest12 = JPATH_ROOT.'/'.'components/'.$option.'/assets/event/images/thumb_'.$post["value_id"][$i];
								unlink($dest12);
						}
						
						if($file['name'][$i]==$pname)
						{
							$pname		= $row->image;
						}
						
						if($post['id']!=0)
						{
							$sqlphoto	= "INSERT INTO ".$this->_table_prefix."event_photo VALUES('',".$post['id'].",'".$row->image."',".$main.")"; 
						}
						else
						{
							$sql		= 'SELECT id FROM  '.$this->_table_prefix.'event WHERE title="'.$post['title'].'" and category='.$post['category'];
							
							$db->setQuery($sql);
							$row_data	= $db->loadRow();
							$id			= $row_data[0];
							$sqlphoto	= "INSERT INTO ".$this->_table_prefix."event_photo VALUES('',".$id.",'".$row->image."',".$main.")";		
						} 
						$db->setQuery($sqlphoto);
						$db->query(); 
					} else {
						$errlink = 'index.php?option='.$option.'&view='.$view.'&task=edit&cid[]='.$post ['id'];
						$msg1 = JText::_ ( 'YOU_CAN_UPLOAD' ).'&nbsp;'.$myimage_maxsize.JText::_('MB').'&nbsp;'.JText::_ ( 'IMAGE_FILE' );
						$mainframe->redirect( $errlink,$msg1 );
					}
				}
				else
				{
					$msg 	= JText::_ ( 'PLEASE_UPLOAD_VALID_IMAGE_FILE' );
					$view 	= JRequest::getVar ('view');
					$task 	= 'edit';
					$sql	= "SELECT id FROM  ".$this->_table_prefix."event WHERE title= ".$post['title']." and category=".$post['category'];
					$db->setQuery($sql);
					$row_data	= $db->loadRow();
					$id			= $row_data[0];
					if($post['id'])
						$cid 	= $post['id'];
					else
						$cid 	= $id;
									 
					$mylink = 'index.php?option='.$option.'&view='.$view.'&task=edit&cid[]='.$post ['id'];
					$msg = JText::_ ( 'PLEASE_UPLOAD_VALID_IMAGE_FILE' );
					$mainframe->redirect( $mylink,$msg );
				}	
			}
		
		}
		
		$sqluser="UPDATE ".$this->_table_prefix."event_photo SET main_image=0 WHERE event_id=".$post['id'];	
		$db->setQuery($sqluser);
	 	$db->query();
	 	
		$sql="UPDATE ".$this->_table_prefix."event_photo SET main_image= 1 WHERE image= '".$pname."'";	
		$db->setQuery($sql);
	 	$db->query();
		
// +++++++++++++++++++++++++++++++++++++++++++++++ EOF multiple image code ++++++++++++++++++++++++++++++++++++++++++++++++++ //		
		$sql="SELECT id FROM #__jeajx_event where title='".$post['title']."' and category='".$post['category']."'";;
		$db->setQuery($sql);
		$rr=$db->loadObject();

		$sql="UPDATE #__jeajx_event SET usr='".$cnt."' where id=".$rr->id;
		$db->setQuery($sql);
		$db->Query();
		if($post ['id'])
		{
			$count1=$post ['id'];
		}
		else
		{
			$db = JFactory::getDbo();
			$sql="SELECT id FROM  #__jeajx_event WHERE title= '".$post['title']."' AND category='".$post['category']."' ";
			$db->setQuery($sql);
			$row_data=$db->loadObject();
			$count1=$row_data->id;
		}
		// =============== Code for Saving dynemic fields value =======================================
		$res11=new extra_field();
		$res11->extra_field_save($post,2,$count1);
		// ============================================================================================
		$this->setRedirect ( 'index.php?option=' . $option . '&view=event', $msg );
	}
	
	function apply() { 
		$mainframe	= JFactory::getApplication();
		$db= & JFactory :: getDBO();
		$post = JRequest::get ( 'post' );
		$cnt=implode('`',$post['usr']);
  		$post['usr']=$cnt;
		$desc = JRequest::getVar( 'desc', '', 'post', 'string', JREQUEST_ALLOWRAW );
		$post["desc"]=$desc;
		$option = JRequest::getVar('option','','request','string');
		$view 	= JRequest::getVar('view','','request','string');
		$cid = JRequest::getVar ( 'cid', array (0 ), 'post', 'array' );
		$post ['id'] = $cid [0];
		
		$model = $this->getModel ( 'event_detail' );
		
		// ================================== Video Link Code ================================================= //
		if($post['youtubelink']) {
			$post['googlelink'] = '';
		}
		if($post['googlelink']) {
			$post['youtubelink'] = '';
		}
		// ================================== EOF Video Link Code ============================================= //
		
		
		if($post['erepeat']==0 || $post['erepeat']==1 || $post['erepeat']==2 || $post['erepeat']==3) {
			if($post['id']!=0) {
				$model->deletedateevent($post['id']);
			}
		}
		
		if($post['erepeat']==4) {
			$dailysdate 	= JRequest::getVar ( 'dailysdate', '', 'post', 'array' );
			$dailyedate 	= JRequest::getVar ( 'dailyedate', '', 'post', 'array' );
			
			$post['start_date']	= $dailysdate[0];
			$post['end_date']	= $dailyedate[0];
		}
	// +++++++++++++++++++++++++++++++++++++++++++++++++++ Multiple image code ++++++++++++++++++++++++++++++++++++++++++++++++++ //		
		$file 	= JRequest::getVar ( 'extra_name', '', 'files', 'array' );
		$len 	= count ($file['name']);
		$pid 	= $post['mainphoto']-1;
		$main	=0;
		if($file["name"][$pid]=="")
			$pname = $post['value_id'][$pid];
		else
			$pname=$file["name"][$pid];
		
		$myeventid	= $model->store ( $post );
		if ($myeventid) {
			$msg = JText::_ ( 'EVENT_DETAIL_SAVED' );
		} else {
			$msg = JText::_ ( 'ERROR_SAVING_EVENT_DETAIL' );
		}
		
	// =========================================== Code for daily selected Event ============================================= //
		if($post['erepeat']==4) {
			$dailysdate 	= JRequest::getVar ( 'dailysdate', '', 'post', 'array' );
			$dailyedate 	= JRequest::getVar ( 'dailyedate', '', 'post', 'array' );
			if($dailysdate[0]!='') {
				$model->deletedateevent($myeventid);
				for($l=0;$l<count($dailysdate);$l++) 
					$model->insertdateevent($myeventid,$dailysdate[$l],$dailyedate[$l]);
			}
		}
	// ========================================= EOF Code for daily selected Event =========================================== //	
		$thumb	= $model->getconfigration();
		$myimage_maxsize	= $thumb->max_img_size;
		
		for($i=0;$i<$len;$i++)
		{
			if($file['name'][$i]!="")
			{
				$iname='extra_name';
				$file =& JRequest::getVar($iname, '', 'files', 'array' );//Get File name, tmp_name
				$row->image= JPath::clean(time().'_'.$file['name'][$i]);//Make the filename unique
				$filetype = strtolower(JFile::getExt($file['name'][$i]));//Get extension of the file
				$ufile_size	= $file['size'][$i]/1048576; // Get the file size in Mb
				
				if($filetype =='jpeg' || $filetype=='jpg' || $filetype =='png' || $filetype=='gif' ||$filetype =='bmp')
				{					
					if($ufile_size<=$myimage_maxsize || $ufile_size==0) {	
						$src	= $file['tmp_name'][$i];
						$dest 	= JPATH_ROOT.'/'.'components/'.$option.'/assets/event/images/'.$row->image ; 
							JFile::upload($src,$dest);	
						$dest1 	= JPATH_ROOT.'/'.'components/'.$option.'/assets/event/images/thumb_'.$row->image;
							copy($dest,$dest1);
						$img 	= new thumbnail();
						$dest 	= JPATH_ROOT.'/'.'components/'.$option.'/assets/event/images/'.$row->image;				
						
						$thumb	= $model->getconfigration();
						$img->CreatThumb($filetype,$dest1,$dest,$thumb->thumb_width,$thumb->thumb_height);
						
						if($post['value_id'][$i])
						{
							$sql 	= "DELETE FROM ".$this->_table_prefix."event_photo WHERE image='".$post["value_id"][$i]."'";
							$db->setQuery($sql);
							$db->query();
							$dest 	= JPATH_ROOT.'/'.'components/'.$option.'/assets/event/images/'.$post["value_id"][$i];
								unlink($dest);
							$dest12 = JPATH_ROOT.'/'.'components/'.$option.'/assets/event/images/thumb_'.$post["value_id"][$i];
								unlink($dest12);
						}
						
						if($file['name'][$i]==$pname)
						{
							$pname		= $row->image;
						}
						
						if($post['id']!=0)
						{
							$sqlphoto	= "INSERT INTO ".$this->_table_prefix."event_photo VALUES('',".$post['id'].",'".$row->image."',".$main.")"; 
						}
						else
						{
							$sql		= 'SELECT id FROM  '.$this->_table_prefix.'event WHERE title="'.$post['title'].'" and category='.$post['category'];
							
							$db->setQuery($sql);
							$row_data	= $db->loadRow();
							$id			= $row_data[0];
							$sqlphoto	= "INSERT INTO ".$this->_table_prefix."event_photo VALUES('',".$id.",'".$row->image."',".$main.")";		
						} 
						$db->setQuery($sqlphoto);
						$db->query();
					} else {
						$errlink = 'index.php?option='.$option.'&view='.$view.'&task=edit&cid[]='.$post ['id'];
						$msg1 = JText::_ ( 'YOU_CAN_UPLOAD' ).'&nbsp;'.$myimage_maxsize.JText::_('MB').'&nbsp;'.JText::_ ( 'IMAGE_FILE' );
						$mainframe->redirect( $errlink,$msg1 );
					}
				}
				else
				{
					$msg 	= JText::_ ( 'PLEASE_UPLOAD_VALID_IMAGE_FILE' );
					$view 	= JRequest::getVar ('view');
					$task 	= 'edit';
					$sql	= "SELECT id FROM  ".$this->_table_prefix."event WHERE title= ".$post['title']." and category=".$post['category'];
					$db->setQuery($sql);
					$row_data	= $db->loadRow();
					$id			= $row_data[0];
					if($post['id'])
						$cid 	= $post['id'];
					else
						$cid 	= $id;
									 
					$mylink = 'index.php?option='.$option.'&view='.$view.'&task='.$task.'&cid[]='.$post ['id'];
					$msg = JText::_ ( 'PLEASE_UPLOAD_VALID_IMAGE_FILE' );
					$mainframe->redirect( $mylink,$msg );
				}	
			}
		
		}
		
		$sqluser="UPDATE ".$this->_table_prefix."event_photo SET main_image=0 WHERE event_id=".$post['id'];	
		$db->setQuery($sqluser);
	 	$db->query();
	 	
		$sql="UPDATE ".$this->_table_prefix."event_photo SET main_image= 1 WHERE image= '".$pname."'";	
		$db->setQuery($sql);
	 	$db->query();
		
// +++++++++++++++++++++++++++++++++++++++++++++++ EOF multiple image code ++++++++++++++++++++++++++++++++++++++++++++++++++ //		
		$sql="SELECT id FROM #__jeajx_event where title='".$post['title']."' and category='".$post['category']."'";;
		$db->setQuery($sql);
		$rr=$db->loadObject();

		$sql="UPDATE #__jeajx_event SET usr='".$cnt."' where id=".$rr->id;
		$db->setQuery($sql);
		$db->Query();
		if($post ['id'])
		{
			$count1=$post ['id'];
		}
		else
		{
			$db = JFactory::getDbo();
			$sql="SELECT id FROM  #__jeajx_event WHERE title= '".$post['title']."' AND category='".$post['category']."' ";
			$db->setQuery($sql);
			$row_data=$db->loadObject();
			$count1=$row_data->id;
		}
		// =============== Code for Saving dynemic fields value =======================================
		$res11=new extra_field();
		$res11->extra_field_save($post,2,$count1);
		// ============================================================================================
		$this->setRedirect ( 'index.php?option='.$option.'&view=event_detail&task=edit&cid[]='.$myeventid, $msg );
	}
	
	function remove() {
		$option = JRequest::getVar('option','','request','string');
		$cid = JRequest::getVar ( 'cid', array (0 ), 'post', 'array' );
		if (! is_array ( $cid ) || count ( $cid ) < 1) {
			JError::raiseError ( 500, JText::_ ( 'SELECT_AN_ITEM_TO_DELETE' ) );
		}
		$model = $this->getModel ( 'event_detail' );
		if (! $model->delete ( $cid )) {
			echo "<script> alert('" . $model->getError ( true ) . "'); window.history.go(-1); </script>\n";
		}
		$msg = JText::_ ( 'EVENT_DETAIL_DELETED_SUCCESSFULLY' );
		$this->setRedirect ( 'index.php?option='.$option.'&view=event',$msg );
	}
	
	function publish() {
		$option = JRequest::getVar('option','','request','string');
		$cid = JRequest::getVar ( 'cid', array (0 ), 'post', 'array' );
		if (! is_array ( $cid ) || count ( $cid ) < 1) {
			JError::raiseError ( 500, JText::_ ( 'SELECT_AN_ITEM_TO_PUBLISH' ) );
		}
		$model = $this->getModel ( 'event_detail' );
		if (! $model->publish ( $cid, 1 )) {
			echo "<script> alert('" . $model->getError ( true ) . "'); window.history.go(-1); </script>\n";
		}
		$msg = JText::_ ( 'EVENT_DETAIL_PUBLISHED_SUCCESSFULLY' );
		$this->setRedirect ( 'index.php?option='.$option.'&view=event',$msg );
	}
	
	function unpublish() {
		$option = JRequest::getVar('option','','request','string');
		$cid = JRequest::getVar ( 'cid', array (0 ), 'post', 'array' );
		if (! is_array ( $cid ) || count ( $cid ) < 1) {
			JError::raiseError ( 500, JText::_ ( 'SELECT_AN_ITEM_TO_UNPUBLISH' ) );
		}
		$model = $this->getModel ( 'event_detail' );
		if (! $model->publish ( $cid, 0 )) {
			echo "<script> alert('" . $model->getError ( true ) . "'); window.history.go(-1); </script>\n";
		}
		$msg = JText::_ ( 'EVENT_DETAIL_UNPUBLISHED_SUCCESSFULLY' );
		$this->setRedirect ( 'index.php?option='.$option.'&view=event',$msg );
	}
	
	function cancel() {
		$option = JRequest::getVar('option','','request','string');
		$msg = JText::_ ( 'EVENT_DETAIL_EDITING_CANCELLED' );
		$this->setRedirect ( 'index.php?option='.$option.'&view=event',$msg );
	}
	
	function unlink2()
	{
	  	$image = JRequest::getVar ('pname');
		$db = JFactory::getDbo();
		$dest = JPATH_ROOT.'/'.'components/com_jeajaxeventcalendar/assets/event/images/'.$image; //specific path of the file															
			unlink($dest);
		$dest = JPATH_ROOT.'/'.'components/com_jeajaxeventcalendar/assets/event/images/thumb_'.$image; //specific path of the file															
			unlink($dest);
		
		$sql = 'DELETE FROM #__jeajx_event_photo WHERE image = "'.$image.'"';
		$db->setQuery($sql);
		$temp = $db->query();
		if($temp)
			echo "true";
		else
			echo "false";
		exit;
	}
	
	function del_dailyevent()
	{
	  	$dailyevent_id = JRequest::getInt('dailyevent_id');
		$db = JFactory::getDbo();
		$sql = 'DELETE FROM #__jeajx_dateevent WHERE id='.$dailyevent_id;
		$db->setQuery($sql);
		$temp = $db->query();
		
		if($temp)
			echo "true";
		else
			echo "false";
		
		exit;
	} 
}
?>