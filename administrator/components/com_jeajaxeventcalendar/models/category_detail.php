<?php
 /**
* @package   JE Ajax Event Calendar
* @copyright Copyright (C) 2009 - 2010 Open Source Matters. All rights reserved.
* @license   http://www.gnu.org/licenses/lgpl.html GNU/LGPL, see LICENSE.php
* Contact to : emailtohardik@gmail.com, joomextensions@gmail.com
* Visit : http://www.joomlaextensions.co.in/
**/ 
 
defined( '_JEXEC' ) or die( 'Restricted access' );
jimport('joomla.application.component.model');


class category_detailModelcategory_detail extends JModelLegacy
{
	var $_id = null;
	var $_data = null;
	var $_table_prefix = null;
	var $_fielddata = null;
	
	function __construct()
	{
		parent::__construct();
		$this->_table_prefix = '#__jeajx_';		
	  	$array = JRequest::getVar('cid',  0, '', 'array');
		$this->setId((int)$array[0]);
		
	}
	
	function setId($id)
	{
		$this->_id		= $id;
		$this->_data	= null;
	}

	function &getData()
	{
		if ($this->_loadData())
		{
			
		}else  $this->_initData();

	   	return $this->_data;
	}
	
	function _loadData()
	{
		if (empty($this->_data))
		{
			$query = 'SELECT * FROM '.$this->_table_prefix.'cal_category  WHERE id = '. $this->_id;
			$this->_db->setQuery($query);
			$this->_data = $this->_db->loadObject();
			return (boolean) $this->_data;
		}
		return true;
	}
	
	function _initData()
	{
		if (empty($this->_data))
		{
			$detail = new stdClass();
			$detail->id			= 0;
			$detail->ename		= null;
			$detail->calias		= null;
			$detail->edesc		= null;
			$detail->published	= 1;
			$detail->cimage		= null;
			$detail->ordering	= null;
			$detail->pcat_id	= 0;
			$this->_data		= $detail;
			return (boolean) $this->_data;
		}
		return true;
	}
	
  	function store($data)
	{
		$row =& $this->getTable();
		if (!$row->bind($data)) {
			$this->setError($this->_db->getErrorMsg());
			return false;
		}
		if (!$row->store()) {
			$this->setError($this->_db->getErrorMsg());
			return false;
		}
		return $row;
	}
	
	function delete($cid = array())
	{
		if (count( $cid ))
		{
			$cids = implode( ',', $cid );
			$query = 'DELETE FROM '.$this->_table_prefix.'cal_category WHERE id IN ( '.$cids.' )';
			$this->_db->setQuery( $query );
			if(!$this->_db->query()) {
				$this->setError($this->_db->getErrorMsg());
				return false;
			}
		}
		return true;
	}

	function publish($cid = array(), $publish = 1)
	{		
		if (count( $cid ))
		{
			$cids = implode( ',', $cid );
			$query = 'UPDATE '.$this->_table_prefix.'cal_category'
				. ' SET published = ' . intval( $publish )
				. ' WHERE id IN ( '.$cids.' )';
			$this->_db->setQuery( $query );
			if (!$this->_db->query()) {
				$this->setError($this->_db->getErrorMsg());
				return false;
			}
		}
		return true;
	}
	
	function getconfigration()
	{
	    $db= & JFactory :: getDBO();
		$query = "SELECT * FROM ".$this->_table_prefix."event_configration WHERE id=1";
		$db->setQuery($query);
		return $db->loadObject();
	}
	
	function getcategories() {
		$db= & JFactory :: getDBO();
			
		if($this->_id!=0)
			$addquery	= " AND id NOT IN(".$this->_id.")";
		else
			$addquery	= "";
				
		$q = "SELECT id AS value,ename AS text FROM ".$this->_table_prefix."cal_category WHERE pcat_id=0".$addquery;
		$db->setQuery($q); 
		$parents=$db->loadObjectList();
		if(count($parents)!=0) {	
			for($i=0;$i<count($parents);$i++){
				$this->_cat_list[]= $parents[$i];
				$this->get_child($parents[$i]->value,0);
			}
			return $this->_cat_list;
		} else {
			return $parents;
		}
		
			
	}
	
	function get_child($id="",$count){
		$count++;
		$db= & JFactory :: getDBO();
		
		if($this->_id!=0)
			$addquery	= " AND id NOT IN(".$this->_id.")";
		else
			$addquery	= "";
			
		$q = "SELECT id AS value,ename AS text FROM ".$this->_table_prefix."cal_category WHERE pcat_id=".$id.$addquery;
		$db->setQuery($q);
		$child=$db->loadObjectList();
			
		for($i=0;$i<count($child);$i++){
			$des ='';
			for($k=0;$k<$count;$k++) {
				$des.=' - ';	
			}
				
			$child[$i]->text = $des.$child[$i]->text;
			$this->_cat_list[]= $child[$i];
			$this->get_child($child[$i]->value,$count);
		}
		
	}
	
}
?>