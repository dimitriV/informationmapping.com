<?php
 /**
* @package   JE Ajax Event Calendar
* @copyright Copyright (C) 2009 - 2010 Open Source Matters. All rights reserved.
* @license   http://www.gnu.org/licenses/lgpl.html GNU/LGPL, see LICENSE.php
* Contact to : emailtohardik@gmail.com, joomextensions@gmail.com
* Visit : http://www.joomlaextensions.co.in/
**/  

defined( '_JEXEC' ) or die( 'Restricted access' );
jimport('joomla.application.component.model');


class eventModelevent extends JModelLegacy
{
	var $_data = null;
	var $_total = null;
	var $_pagination = null;
	var $_table_prefix = null;
	
	function __construct()
	{
		parent::__construct();
		global $context; 
		$mainframe = JFactory::getApplication();
		$context='event';
	  	$this->_table_prefix = '#__jeajx_';			
		$limit			= $mainframe->getUserStateFromRequest( $context.'limit', 'limit', $mainframe->getCfg('list_limit'), 0);
		$limitstart = $mainframe->getUserStateFromRequest( $context.'limitstart', 'limitstart', 0 );
		$event     = $mainframe->getUserStateFromRequest( $context.'event','event',0);
		$filter     = $mainframe->getUserStateFromRequest( $context.'filter','filter',0);
		$this->setState('limit', $limit);
		$this->setState('limitstart', $limitstart);
		$this->setState('event', $event);
		$this->setState('filter', $filter);

	}
	
	function getData()
	{		
		if (empty($this->_data))
		{
			$query = $this->_buildQuery();
			$this->_data = $this->_getList($query, $this->getState('limitstart'), $this->getState('limit'));
		}

		return $this->_data;
	}
	
	function getTotal()
	{
		if (empty($this->_total))
		{
			$query = $this->_buildQuery();
			$this->_total = $this->_getListCount($query);
		}

		return $this->_total;
	}
	
	function getPagination()
	{
		if (empty($this->_pagination))
		{
			jimport('joomla.html.pagination');
			$this->_pagination = new JPagination( $this->getTotal(), $this->getState('limitstart'), $this->getState('limit') );
		}

		return $this->_pagination;
	}
  	
	function _buildQuery()
	{
		$where = "";    
	    $orderby	= $this->_buildContentOrderBy();
		$eventid	= JRequest::getVar('eventid', '0','request','int');
		$published	= JRequest::getVar('published', '-1','request','int');
		$catid		= JRequest::getVar('catid', '0','request','int');

		
		if($eventid!=0 && $catid!=0 && $published!='-1') {
			$where .= 'WHERE e.id='.$eventid.' AND e.category='.$catid.' AND e.published='.$published;
		} else if($catid!=0 && $published!='-1') {
			$where .= 'WHERE e.category='.$catid.' AND e.published='.$published;
		} else if($eventid!=0 && $published!='-1') {
			$where .= 'WHERE e.id='.$eventid.' AND e.published='.$published;
		} else if($eventid!=0 && $catid!='-1') {
			$where .= 'WHERE e.id='.$eventid.' AND e.category='.$catid;
		} else if($eventid!=0) {
			$where .= 'WHERE e.id='.$eventid;
		} else if($catid!=0) {
			$where .= 'WHERE e.category='.$catid;
		} else if($published!=-1) {
			$where .= 'WHERE e.published='.$published;    
		}
		
		
		$query = ' SELECT e.*,c.ename '
			. ' FROM '.$this->_table_prefix.'event AS e LEFT JOIN '.$this->_table_prefix.'cal_category AS c ON c.id=e.category '.$where.' ORDER BY e.id ASC ';
		return $query;
	}
	
	function _buildContentOrderBy()
	{
		global $context;
		$mainframe = JFactory::getApplication();
		$filter_order     = $mainframe->getUserStateFromRequest( $context.'filter_order',      'filter_order', 	  'id' );
		$filter_order_Dir = $mainframe->getUserStateFromRequest( $context.'filter_order_Dir',  'filter_order_Dir', '' );		
		$orderby 	= ' ORDER BY '.$filter_order.' '.$filter_order_Dir;			
		return $orderby;
	}
	
	function getcategory() 
	{
		$db= & JFactory :: getDBO();
		$sql = "SELECT id AS value,ename AS text FROM ".$this->_table_prefix."cal_category";
		$db->setQuery($sql);
		return $db->loadObjectlist();
	}
	
	function getallevent() 
	{
		$db= & JFactory :: getDBO();
		$sql = "SELECT id AS value,title AS text FROM ".$this->_table_prefix."event";
		$db->setQuery($sql);
		return $db->loadObjectlist();
	}
	
	function checkdynemicfields()
	{
		$db= & JFactory :: getDBO();
		$query = 'SELECT * FROM '.$this->_table_prefix.'fields WHERE field_section=2';
		$db->setQuery($query);
		return $db->LoadObjectList();
	}
}	

