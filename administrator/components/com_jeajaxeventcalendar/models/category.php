<?php
 /**
* @package   JE Ajax Event Calendar
* @copyright Copyright (C) 2009 - 2010 Open Source Matters. All rights reserved.
* @license   http://www.gnu.org/licenses/lgpl.html GNU/LGPL, see LICENSE.php
* Contact to : emailtohardik@gmail.com, joomextensions@gmail.com
* Visit : http://www.joomlaextensions.co.in/
**/  

defined( '_JEXEC' ) or die( 'Restricted access' );
jimport('joomla.application.component.model');


class categoryModelcategory extends JModelLegacy
{
	var $_data = null;
	var $_total = null;
	var $_pagination = null;
	var $_table_prefix = null;
	
	function __construct()
	{
		parent::__construct();
		global $context; 
		$mainframe = JFactory::getApplication();
		$context='ordering';
	  	$this->_table_prefix = '#__jeajx_';			
		$limit			= $mainframe->getUserStateFromRequest( $context.'limit', 'limit', $mainframe->getCfg('list_limit'), 0);
		$limitstart = $mainframe->getUserStateFromRequest( $context.'limitstart', 'limitstart', 0 );
		$event     = $mainframe->getUserStateFromRequest( $context.'category','category',0);
		$filter     = $mainframe->getUserStateFromRequest( $context.'filter','filter',0);
		$this->setState('limit', $limit);
		$this->setState('limitstart', $limitstart);
		$this->setState('category', $event);
		$this->setState('filter', $filter);

	}
	function getData()
	{		
		if (empty($this->_data))
		{
		 $query = $this->_buildQuery();
			$this->_data = $this->_getList($query, $this->getState('limitstart'), $this->getState('limit'));
		}

		return $this->_data;
}
	function getTotal()
	{
		if (empty($this->_total))
		{
			$query = $this->_buildQuery();
			$this->_total = $this->_getListCount($query);
		}

		return $this->_total;
	}
	function getPagination()
	{
		if (empty($this->_pagination))
		{
			jimport('joomla.html.pagination');
			$this->_pagination = new JPagination( $this->getTotal(), $this->getState('limitstart'), $this->getState('limit') );
		}
 $this->_pagination;
		return $this->_pagination;
	}
  	
	function _buildQuery()
	{
		$catid		= JRequest::getVar('catid', '0','request','int');
		$published	= JRequest::getVar('published', '-1','request','int');
		$where = '';
		
		if($catid!=0 && $published!='-1') {
			$where .= 'WHERE id='.$catid.' AND published='.$published;
		} else if($catid!=0) {
			$where .= 'WHERE id='.$catid;
		} else if($published!=-1) {
			$where .= 'WHERE published='.$published;    
		}
		
			
	    $orderby	= $this->_buildContentOrderBy();
		$query = ' SELECT * '. ' FROM '.$this->_table_prefix.'cal_category '.$where.$orderby; 
		return $query;
	}
	
	function _buildContentOrderBy()
	{
		global $context;
		$mainframe = JFactory::getApplication();
		$filter_order     = $mainframe->getUserStateFromRequest( $context.'filter_order',      'filter_order', 	  'ordering' );
		$filter_order_Dir = $mainframe->getUserStateFromRequest( $context.'filter_order_Dir',  'filter_order_Dir', '' );		
		$orderby 	= ' ORDER BY '.$filter_order.' '.$filter_order_Dir;			
		return $orderby;
	}
	
	// ================================= Ordering Function =======================================================//
		function saveorder(){
			$mainframe = JFactory::getApplication();
			$db = JFactory::getDBO();
			$cid = JRequest::getVar('cid', array(0), 'post', 'array');
			$total = count($cid);
			$order = JRequest::getVar('order', array(0), 'post', 'array');
			JArrayHelper::toInteger($order, array(0));
			$row = JTable::getInstance('category_detail', 'Table');
			$groupings = array();
			for ($i = 0; $i < $total; $i++){
				$row->load((int)$cid[$i]);
				$groupings[] = $row->ename;
				if ($row->ordering != $order[$i]){
					$row->ordering = $order[$i];
					if (!$row->store()){
						JError::raiseError(500, $db->getErrorMsg());
					}
				}
			}
		   
			$groupings = array_unique($groupings);
			foreach ($groupings as $group){
				$row->reorder('id = '.(int)$group);
			}
			$cache = JFactory::getCache('com_jeajaxeventcalendar');
			$cache->clean();
			return true;
		}
	// ================================= End Ordering Function =======================================================//
	
	function getcategory() 
	{
		$db= & JFactory :: getDBO();
		$sql = "SELECT id AS value,ename AS text FROM ".$this->_table_prefix."cal_category";
		$db->setQuery($sql);
		return $db->loadObjectlist();
	}
	
	function checkdynemicfields()
	{
		$db= & JFactory :: getDBO();
		$query = 'SELECT * FROM '.$this->_table_prefix.'fields WHERE field_section=1';
		$db->setQuery($query);
		return $db->LoadObjectList();
	}
	
	function getparentcategory($cat_id) 
	{
		$db= & JFactory :: getDBO();
		$sql = "SELECT id,ename FROM ".$this->_table_prefix."cal_category WHERE id=".$cat_id;
		$db->setQuery($sql);
		return $db->loadObject();
	}	

}