DROP TABLE IF EXISTS `#__jeajx_cal_category`;
CREATE TABLE IF NOT EXISTS `#__jeajx_cal_category` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `ename` varchar(50) NOT NULL,
  `edesc` text NOT NULL,
  `published` tinyint(4) NOT NULL,
  `cimage` varchar(150) NOT NULL,
  `pcat_id` int(11) NOT NULL,
  `ordering` int(11) NOT NULL,
  `calias` varchar(200) NOT NULL,
  PRIMARY KEY (`id`)
);

DROP TABLE IF EXISTS `#__jeajx_dateevent`;
CREATE TABLE IF NOT EXISTS `#__jeajx_dateevent` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `event_id` int(11) NOT NULL,
  `dailysdate` date NOT NULL,
  `dailyedate` date NOT NULL,
  `meridian` varchar(10) NOT NULL,
  `ehour` int(11) NOT NULL,
  `emin` int(11) NOT NULL,
  PRIMARY KEY (`id`)
);

DROP TABLE IF EXISTS `#__jeajx_event`;
CREATE TABLE IF NOT EXISTS `#__jeajx_event` (
`id` int(11) NOT NULL AUTO_INCREMENT,
  `title` varchar(250) NOT NULL,
  `category` varchar(50) NOT NULL,
  `usr` text NOT NULL,
  `desc` longtext NOT NULL,
  `start_date` date NOT NULL,
  `end_date` date NOT NULL DEFAULT '0000-00-00',
  `published` tinyint(4) NOT NULL,
  `bgcolor` varchar(100) NOT NULL,
  `txtcolor` varchar(100) NOT NULL,
  `street` varchar(300) NOT NULL,
  `city` varchar(300) NOT NULL,
  `country` varchar(300) NOT NULL,
  `postcode` varchar(250) NOT NULL,
  `insert_user` int(11) NOT NULL,
  `youtubelink` varchar(400) NOT NULL,
  `googlelink` varchar(400) NOT NULL,
  `erepeat` int(11) NOT NULL,
  `ehits` int(11) NOT NULL,
  PRIMARY KEY (`id`)
);


DROP TABLE IF EXISTS `#__jeajx_event_configration`;
CREATE TABLE IF NOT EXISTS `#__jeajx_event_configration` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `pattern` varchar(100) NOT NULL,
  `iscreate` tinyint(4) NOT NULL,
  `title` text NOT NULL,
  `head1` text NOT NULL,
  `head2` text NOT NULL,
  `head3` text NOT NULL,
  `head4` text NOT NULL,
  `autopub` tinyint(4) NOT NULL,
  `gmap_api` varchar(500) NOT NULL,
  `gmap_width` int(11) NOT NULL,
  `gmap_height` int(11) NOT NULL,
  `gmap_display` tinyint(11) NOT NULL,
  `thumb_width` int(11) NOT NULL,
  `thumb_height` int(11) NOT NULL,
  `show_rss` tinyint(4) NOT NULL,
  `max_img_size` float NOT NULL,
  `head1_txtcolor` varchar(20) NOT NULL,
  `head2_txtcolor` varchar(20) NOT NULL,
  `head3_txtcolor` varchar(20) NOT NULL,
  PRIMARY KEY (`id`)
)AUTO_INCREMENT=2 ;

INSERT INTO `#__jeajx_event_configration` (`id`, `pattern`, `iscreate`, `title`, `head1`, `head2`, `head3`, `head4`, `autopub`, `gmap_api`, `gmap_width`, `gmap_height`, `gmap_display`, `thumb_width`, `thumb_height`, `show_rss`, `max_img_size`, `head1_txtcolor`, `head2_txtcolor`, `head3_txtcolor`) VALUES
(1, 'pattern1', 1, 'Ajax Event ', 'fa9bfa', '97c7b6', 'ad5ead', 'fa9bfa', 1, 'ABQIAAAA-cNYGMHzX4x8adaDHYtTnxRffXD3c7srYel6NP06xj76H8hm1hSrXoztSnyjFi9AJ0asTv6k1m21_A', 300, 250, 1, 100, 100, 1, 2, 'FFFFFF', 'FFFFFF', 'FFFFFF');


DROP TABLE IF EXISTS `#__jeajx_event_photo`;
CREATE TABLE IF NOT EXISTS `#__jeajx_event_photo` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `event_id` int(11) NOT NULL,
  `image` varchar(255) NOT NULL,
  `main_image` int(11) NOT NULL,
  PRIMARY KEY (`id`)
);

DROP TABLE IF EXISTS `#__jeajx_fields`;
CREATE TABLE IF NOT EXISTS `#__jeajx_fields` (
  `field_id` int(11) NOT NULL AUTO_INCREMENT,
  `field_title` varchar(250) NOT NULL,
  `field_name` varchar(20) NOT NULL,
  `field_type` varchar(20) NOT NULL,
  `field_desc` longtext NOT NULL,
  `field_class` varchar(20) NOT NULL,
  `field_section` varchar(20) NOT NULL,
  `field_maxlength` int(11) NOT NULL,
  `field_cols` int(11) NOT NULL,
  `field_rows` int(11) NOT NULL,
  `date` varchar(100) NOT NULL,
  `field_show_in_front` tinyint(4) NOT NULL,
  `is_required` tinyint(4) NOT NULL,
  `radio_cols` int(11) NOT NULL,
  `published` tinyint(4) NOT NULL,
  `ordering` int(11) NOT NULL,
  `label_fontsize` int(11) NOT NULL,
  `label_bgcolor` varchar(100) NOT NULL,
  `label_textcolor` varchar(100) NOT NULL,
  `calias` int(11) NOT NULL,
  PRIMARY KEY (`field_id`)
);

DROP TABLE IF EXISTS `#__jeajx_event_fields_data`;
CREATE TABLE IF NOT EXISTS `#__jeajx_fields_data` (
  `data_id` int(11) NOT NULL AUTO_INCREMENT,
  `fieldid` int(11) DEFAULT NULL,
  `data_txt` longtext,
  `section` varchar(200) DEFAULT NULL,
  `hid` int(11) NOT NULL,
  PRIMARY KEY (`data_id`),
  KEY `fieldid` (`fieldid`)
);

DROP TABLE IF EXISTS `#__jeajx_event_fields_value`;
CREATE TABLE IF NOT EXISTS `#__jeajx_fields_value` (
  `value_id` int(11) NOT NULL AUTO_INCREMENT,
  `field_id` int(11) NOT NULL,
  `field_value` varchar(250) NOT NULL,
  `field_name` varchar(250) NOT NULL,
  PRIMARY KEY (`value_id`)
);

DROP TABLE IF EXISTS `#__jeajx_form`;
CREATE TABLE IF NOT EXISTS `#__jeajx_form` (
   `id` int(6) NOT NULL auto_increment,
  `name` varchar(50) NOT NULL,
  `description` varchar(100) NOT NULL,
  `published` tinyint(2) NOT NULL,
  `show_fields_title_top` tinyint(4) NOT NULL,
  `tool_on_off` tinyint(4) NOT NULL,
  `bgcolor` varchar(100) NOT NULL,
  `text_color` varchar(100) NOT NULL,
  `btn_bgcolor` varchar(100) NOT NULL,
  `btn_text_color` varchar(100) NOT NULL,
  PRIMARY KEY  (`id`)
)AUTO_INCREMENT=3;
INSERT INTO `#__jeajx_form` VALUES (1, 'Category', 'Category', 1, 1, 1, '', '', '', '');
INSERT INTO `#__jeajx_form` VALUES (2, 'Event', 'Event Form', 1, 1, 1, '', '', '', '');

DROP TABLE IF EXISTS `#__jeajx_tempsetting`;
CREATE TABLE IF NOT EXISTS `#__jeajx_tempsetting` (
 `id` int(11) NOT NULL AUTO_INCREMENT,
  `alleventlist_tempt` longtext NOT NULL,
  `alleventlist_more_tempt` longtext NOT NULL,
  `eventcrlist_tempt` longtext NOT NULL,
  `dateevent_tempt` longtext NOT NULL,
  PRIMARY KEY (`id`)
);


INSERT INTO `#__jeajx_tempsetting` (`id`, `alleventlist_tempt`, `alleventlist_more_tempt`, `eventcrlist_tempt`, `dateevent_tempt`) VALUES
(1, '<table border="0" width="100%">\r\n<tbody>\r\n<tr>\r\n<td width="105px" valign="top">\r\n<div class="event_image" style="text-align: center; width: 100px; background-color: #ffffff; float: left; border: medium none; margin: 5px;">{event_photo}</div>\r\n</td>\r\n<td valign="top">\r\n<table border="0" width="100%">\r\n<tbody>\r\n<tr>\r\n<td width="30%">{event_date}</td>\r\n<td valign="top">\r\n<table border="0" width="100%">\r\n<tbody>\r\n<tr>\r\n<td>{title}</td>\r\n<td width="10%">{hits}</td>\r\n<td width="7%">{gcal}</td>\r\n<td width="7%">{ical}</td>\r\n<td width="7%">{outlook}</td>\r\n</tr>\r\n</tbody>\r\n</table>\r\n</td>\r\n</tr>\r\n<tr>\r\n<td width="30%">{address}{gmap_icon}</td>\r\n<td>{catname}</td>\r\n</tr>\r\n<tr>\r\n<td colspan="2">{description}</td>\r\n</tr>\r\n</tbody>\r\n</table>\r\n</td>\r\n</tr>\r\n</tbody>\r\n</table>\r\n<p> </p>', '<table border="0" cellpadding="0" width="100%">\r\n<tbody>\r\n<tr>\r\n<td width="150px" valign="top">{event_photo}</td>\r\n<td valign="top">\r\n<table border="0" cellspacing="0" cellpadding="0" width="100%">\r\n<tbody>\r\n<tr>\r\n<td valign="top">\r\n<table border="0" cellspacing="0" cellpadding="0" width="100%">\r\n<tbody>\r\n<tr>\r\n<td><span style="font: bold 12pt Arial; color: #135cae;">{title}</span></td>\r\n<td width="10%">{hits}</td>\r\n<td width="7%">{gcal}</td>\r\n<td width="7%">{ical}</td>\r\n<td width="7%">{outlook}</td>\r\n</tr>\r\n</tbody>\r\n</table>\r\n</td>\r\n</tr>\r\n<tr>\r\n<td><span style="font-weight: bold;">{event_date}</span></td>\r\n</tr>\r\n<tr>\r\n<td>{address}</td>\r\n</tr>\r\n<tr>\r\n<td>{description}</td>\r\n</tr>\r\n</tbody>\r\n</table>\r\n</td>\r\n</tr>\r\n</tbody>\r\n</table>\r\n<p>{event_detail_tab}{google_map_tab}{event_video_tab}{event_photo_tab}</p>\r\n<p>{related_event}</p>', '<table border="0" width="100%">\r\n<tbody>\r\n<tr>\r\n<td width="105px" valign="top">\r\n<div class="event_image" style="text-align: center; width: 100px; background-color: #ffffff; float: left; border: medium none; margin: 5px;">{event_photo}</div>\r\n</td>\r\n<td valign="top">\r\n<table border="0" width="100%">\r\n<tbody>\r\n<tr>\r\n<td>{event_title}</td>\r\n<td width="30%">{catname}</td>\r\n<td valign="top">\r\n<table border="0" width="100%">\r\n<tbody>\r\n<tr>\r\n<td align="right">{edit} | {delete}</td>\r\n</tr>\r\n</tbody>\r\n</table>\r\n</td>\r\n</tr>\r\n<tr>\r\n<td width="30%">{address}</td>\r\n<td><br /></td>\r\n</tr>\r\n<tr>\r\n<td colspan="3">{description}</td>\r\n</tr>\r\n</tbody>\r\n</table>\r\n</td>\r\n</tr>\r\n</tbody>\r\n</table>', '<table border="0" width="100%">\r\n<tbody>\r\n<tr>\r\n<td width="105px" valign="top">\r\n<div class="event_image" style="text-align: center; width: 100px; background-color: #ffffff; float: left; border: medium none; margin: 5px;">{event_photo}</div>\r\n</td>\r\n<td valign="top">\r\n<table border="0" width="100%">\r\n<tbody>\r\n<tr>\r\n<td>{event_title}</td>\r\n<td width="30%">{catname}</td>\r\n<td valign="top">\r\n<table border="0" width="100%">\r\n<tbody>\r\n<tr>\r\n<td>{hits}</td>\r\n<td width="10%"><br /></td>\r\n<td width="7%">{gcal}</td>\r\n<td width="7%">{ical}</td>\r\n<td width="7%">{outlook}</td>\r\n</tr>\r\n</tbody>\r\n</table>\r\n</td>\r\n</tr>\r\n<tr>\r\n<td width="30%">{address}</td>\r\n<td><br /></td>\r\n</tr>\r\n<tr>\r\n<td colspan="3">{description}</td>\r\n</tr>\r\n</tbody>\r\n</table>\r\n</td>\r\n</tr>\r\n</tbody>\r\n</table>');



DROP TABLE IF EXISTS `#__jeajx_country`;
CREATE TABLE `#__jeajx_country` (
  `country_id` int(11) NOT NULL auto_increment,
  `country_name` varchar(64) default NULL,
  `country_3_code` char(3) default NULL,
  `country_2_code` char(2) default NULL,
  PRIMARY KEY  (`country_id`),
  KEY `idx_country_name` (`country_name`)
);

INSERT INTO `#__jeajx_country` VALUES (1, 'Afghanistan', 'AFG', 'AF');
INSERT INTO `#__jeajx_country` VALUES (2, 'Albania', 'ALB', 'AL');
INSERT INTO `#__jeajx_country` VALUES (3, 'Algeria', 'DZA', 'DZ');
INSERT INTO `#__jeajx_country` VALUES (4, 'American Samoa', 'ASM', 'AS');
INSERT INTO `#__jeajx_country` VALUES (5, 'Andorra', 'AND', 'AD');
INSERT INTO `#__jeajx_country` VALUES (6, 'Angola', 'AGO', 'AO');
INSERT INTO `#__jeajx_country` VALUES (7, 'Anguilla', 'AIA', 'AI');
INSERT INTO `#__jeajx_country` VALUES (8, 'Antarctica', 'ATA', 'AQ');
INSERT INTO `#__jeajx_country` VALUES (9, 'Antigua and Barbuda', 'ATG', 'AG');
INSERT INTO `#__jeajx_country` VALUES (10, 'Argentina', 'ARG', 'AR');
INSERT INTO `#__jeajx_country` VALUES (11, 'Armenia', 'ARM', 'AM');
INSERT INTO `#__jeajx_country` VALUES (12, 'Aruba', 'ABW', 'AW');
INSERT INTO `#__jeajx_country` VALUES (13, 'Australia', 'AUS', 'AU');
INSERT INTO `#__jeajx_country` VALUES (14, 'Austria', 'AUT', 'AT');
INSERT INTO `#__jeajx_country` VALUES (15, 'Azerbaijan', 'AZE', 'AZ');
INSERT INTO `#__jeajx_country` VALUES (16, 'Bahamas', 'BHS', 'BS');
INSERT INTO `#__jeajx_country` VALUES (17, 'Bahrain', 'BHR', 'BH');
INSERT INTO `#__jeajx_country` VALUES (18, 'Bangladesh', 'BGD', 'BD');
INSERT INTO `#__jeajx_country` VALUES (19, 'Barbados', 'BRB', 'BB');
INSERT INTO `#__jeajx_country` VALUES (20, 'Belarus', 'BLR', 'BY');
INSERT INTO `#__jeajx_country` VALUES (21, 'Belgium', 'BEL', 'BE');
INSERT INTO `#__jeajx_country` VALUES (22, 'Belize', 'BLZ', 'BZ');
INSERT INTO `#__jeajx_country` VALUES (23, 'Benin', 'BEN', 'BJ');
INSERT INTO `#__jeajx_country` VALUES (24, 'Bermuda', 'BMU', 'BM');
INSERT INTO `#__jeajx_country` VALUES (25, 'Bhutan', 'BTN', 'BT');
INSERT INTO `#__jeajx_country` VALUES (26, 'Bolivia', 'BOL', 'BO');
INSERT INTO `#__jeajx_country` VALUES (27, 'Bosnia and Herzegowina', 'BIH', 'BA');
INSERT INTO `#__jeajx_country` VALUES (28, 'Botswana', 'BWA', 'BW');
INSERT INTO `#__jeajx_country` VALUES (29, 'Bouvet Island', 'BVT', 'BV');
INSERT INTO `#__jeajx_country` VALUES (30, 'Brazil', 'BRA', 'BR');
INSERT INTO `#__jeajx_country` VALUES (31, 'British Indian Ocean Territory', 'IOT', 'IO');
INSERT INTO `#__jeajx_country` VALUES (32, 'Brunei Darussalam', 'BRN', 'BN');
INSERT INTO `#__jeajx_country` VALUES (33, 'Bulgaria', 'BGR', 'BG');
INSERT INTO `#__jeajx_country` VALUES (34, 'Burkina Faso', 'BFA', 'BF');
INSERT INTO `#__jeajx_country` VALUES (35, 'Burundi', 'BDI', 'BI');
INSERT INTO `#__jeajx_country` VALUES (36, 'Cambodia', 'KHM', 'KH');
INSERT INTO `#__jeajx_country` VALUES (37, 'Cameroon', 'CMR', 'CM');
INSERT INTO `#__jeajx_country` VALUES (38, 'Canada', 'CAN', 'CA');
INSERT INTO `#__jeajx_country` VALUES (39, 'Cape Verde', 'CPV', 'CV');
INSERT INTO `#__jeajx_country` VALUES (40, 'Cayman Islands', 'CYM', 'KY');
INSERT INTO `#__jeajx_country` VALUES (41, 'Central African Republic', 'CAF', 'CF');
INSERT INTO `#__jeajx_country` VALUES (42, 'Chad', 'TCD', 'TD');
INSERT INTO `#__jeajx_country` VALUES (43, 'Chile', 'CHL', 'CL');
INSERT INTO `#__jeajx_country` VALUES (44, 'China', 'CHN', 'CN');
INSERT INTO `#__jeajx_country` VALUES (45, 'Christmas Island', 'CXR', 'CX');
INSERT INTO `#__jeajx_country` VALUES (46, 'Cocos (Keeling) Islands', 'CCK', 'CC');
INSERT INTO `#__jeajx_country` VALUES (47, 'Colombia', 'COL', 'CO');
INSERT INTO `#__jeajx_country` VALUES (48, 'Comoros', 'COM', 'KM');
INSERT INTO `#__jeajx_country` VALUES (49, 'Congo', 'COG', 'CG');
INSERT INTO `#__jeajx_country` VALUES (50, 'Cook Islands', 'COK', 'CK');
INSERT INTO `#__jeajx_country` VALUES (51, 'Costa Rica', 'CRI', 'CR');
INSERT INTO `#__jeajx_country` VALUES (52, 'Cote D''Ivoire', 'CIV', 'CI');
INSERT INTO `#__jeajx_country` VALUES (53, 'Croatia', 'HRV', 'HR');
INSERT INTO `#__jeajx_country` VALUES (54, 'Cuba', 'CUB', 'CU');
INSERT INTO `#__jeajx_country` VALUES (55, 'Cyprus', 'CYP', 'CY');
INSERT INTO `#__jeajx_country` VALUES (56, 'Czech Republic', 'CZE', 'CZ');
INSERT INTO `#__jeajx_country` VALUES (57, 'Denmark', 'DNK', 'DK');
INSERT INTO `#__jeajx_country` VALUES (58, 'Djibouti', 'DJI', 'DJ');
INSERT INTO `#__jeajx_country` VALUES (59, 'Dominica', 'DMA', 'DM');
INSERT INTO `#__jeajx_country` VALUES (60, 'Dominican Republic', 'DOM', 'DO');
INSERT INTO `#__jeajx_country` VALUES (61, 'East Timor', 'TMP', 'TP');
INSERT INTO `#__jeajx_country` VALUES (62, 'Ecuador', 'ECU', 'EC');
INSERT INTO `#__jeajx_country` VALUES (63, 'Egypt', 'EGY', 'EG');
INSERT INTO `#__jeajx_country` VALUES (64, 'El Salvador', 'SLV', 'SV');
INSERT INTO `#__jeajx_country` VALUES (65, 'Equatorial Guinea', 'GNQ', 'GQ');
INSERT INTO `#__jeajx_country` VALUES (66, 'Eritrea', 'ERI', 'ER');
INSERT INTO `#__jeajx_country` VALUES (67, 'Estonia', 'EST', 'EE');
INSERT INTO `#__jeajx_country` VALUES (68, 'Ethiopia', 'ETH', 'ET');
INSERT INTO `#__jeajx_country` VALUES (69, 'Falkland Islands (Malvinas)', 'FLK', 'FK');
INSERT INTO `#__jeajx_country` VALUES (70, 'Faroe Islands', 'FRO', 'FO');
INSERT INTO `#__jeajx_country` VALUES (71, 'Fiji', 'FJI', 'FJ');
INSERT INTO `#__jeajx_country` VALUES (72, 'Finland', 'FIN', 'FI');
INSERT INTO `#__jeajx_country` VALUES (73, 'France', 'FRA', 'FR');
INSERT INTO `#__jeajx_country` VALUES (74, 'France, Metropolitan', 'FXX', 'FX');
INSERT INTO `#__jeajx_country` VALUES (75, 'French Guiana', 'GUF', 'GF');
INSERT INTO `#__jeajx_country` VALUES (76, 'French Polynesia', 'PYF', 'PF');
INSERT INTO `#__jeajx_country` VALUES (77, 'French Southern Territories', 'ATF', 'TF');
INSERT INTO `#__jeajx_country` VALUES (78, 'Gabon', 'GAB', 'GA');
INSERT INTO `#__jeajx_country` VALUES (79, 'Gambia', 'GMB', 'GM');
INSERT INTO `#__jeajx_country` VALUES (80, 'Georgia', 'GEO', 'GE');
INSERT INTO `#__jeajx_country` VALUES (81, 'Germany', 'DEU', 'DE');
INSERT INTO `#__jeajx_country` VALUES (82, 'Ghana', 'GHA', 'GH');
INSERT INTO `#__jeajx_country` VALUES (83, 'Gibraltar', 'GIB', 'GI');
INSERT INTO `#__jeajx_country` VALUES (84, 'Greece', 'GRC', 'GR');
INSERT INTO `#__jeajx_country` VALUES (85, 'Greenland', 'GRL', 'GL');
INSERT INTO `#__jeajx_country` VALUES (86, 'Grenada', 'GRD', 'GD');
INSERT INTO `#__jeajx_country` VALUES (87, 'Guadeloupe', 'GLP', 'GP');
INSERT INTO `#__jeajx_country` VALUES (88, 'Guam', 'GUM', 'GU');
INSERT INTO `#__jeajx_country` VALUES (89, 'Guatemala', 'GTM', 'GT');
INSERT INTO `#__jeajx_country` VALUES (90, 'Guinea', 'GIN', 'GN');
INSERT INTO `#__jeajx_country` VALUES (91, 'Guinea-bissau', 'GNB', 'GW');
INSERT INTO `#__jeajx_country` VALUES (92, 'Guyana', 'GUY', 'GY');
INSERT INTO `#__jeajx_country` VALUES (93, 'Haiti', 'HTI', 'HT');
INSERT INTO `#__jeajx_country` VALUES (94, 'Heard and Mc Donald Islands', 'HMD', 'HM');
INSERT INTO `#__jeajx_country` VALUES (95, 'Honduras', 'HND', 'HN');
INSERT INTO `#__jeajx_country` VALUES (96, 'Hong Kong', 'HKG', 'HK');
INSERT INTO `#__jeajx_country` VALUES (97, 'Hungary', 'HUN', 'HU');
INSERT INTO `#__jeajx_country` VALUES (98, 'Iceland', 'ISL', 'IS');
INSERT INTO `#__jeajx_country` VALUES (99, 'India', 'IND', 'IN');
INSERT INTO `#__jeajx_country` VALUES (100, 'Indonesia', 'IDN', 'ID');
INSERT INTO `#__jeajx_country` VALUES (101, 'Iran (Islamic Republic of)', 'IRN', 'IR');
INSERT INTO `#__jeajx_country` VALUES (102, 'Iraq', 'IRQ', 'IQ');
INSERT INTO `#__jeajx_country` VALUES (103, 'Ireland', 'IRL', 'IE');
INSERT INTO `#__jeajx_country` VALUES (104, 'Israel', 'ISR', 'IL');
INSERT INTO `#__jeajx_country` VALUES (105, 'Italy', 'ITA', 'IT');
INSERT INTO `#__jeajx_country` VALUES (106, 'Jamaica', 'JAM', 'JM');
INSERT INTO `#__jeajx_country` VALUES (107, 'Japan', 'JPN', 'JP');
INSERT INTO `#__jeajx_country` VALUES (108, 'Jordan', 'JOR', 'JO');
INSERT INTO `#__jeajx_country` VALUES (109, 'Kazakhstan', 'KAZ', 'KZ');
INSERT INTO `#__jeajx_country` VALUES (110, 'Kenya', 'KEN', 'KE');
INSERT INTO `#__jeajx_country` VALUES (111, 'Kiribati', 'KIR', 'KI');
INSERT INTO `#__jeajx_country` VALUES (112, 'Korea, Democratic People''s Republic of', 'PRK', 'KP');
INSERT INTO `#__jeajx_country` VALUES (113, 'Korea, Republic of', 'KOR', 'KR');
INSERT INTO `#__jeajx_country` VALUES (114, 'Kuwait', 'KWT', 'KW');
INSERT INTO `#__jeajx_country` VALUES (115, 'Kyrgyzstan', 'KGZ', 'KG');
INSERT INTO `#__jeajx_country` VALUES (116, 'Lao People''s Democratic Republic', 'LAO', 'LA');
INSERT INTO `#__jeajx_country` VALUES (117, 'Latvia', 'LVA', 'LV');
INSERT INTO `#__jeajx_country` VALUES (118, 'Lebanon', 'LBN', 'LB');
INSERT INTO `#__jeajx_country` VALUES (119, 'Lesotho', 'LSO', 'LS');
INSERT INTO `#__jeajx_country` VALUES (120, 'Liberia', 'LBR', 'LR');
INSERT INTO `#__jeajx_country` VALUES (121, 'Libyan Arab Jamahiriya', 'LBY', 'LY');
INSERT INTO `#__jeajx_country` VALUES (122, 'Liechtenstein', 'LIE', 'LI');
INSERT INTO `#__jeajx_country` VALUES (123, 'Lithuania', 'LTU', 'LT');
INSERT INTO `#__jeajx_country` VALUES (124, 'Luxembourg', 'LUX', 'LU');
INSERT INTO `#__jeajx_country` VALUES (125, 'Macau', 'MAC', 'MO');
INSERT INTO `#__jeajx_country` VALUES (126, 'Macedonia, The Former Yugoslav Republic of', 'MKD', 'MK');
INSERT INTO `#__jeajx_country` VALUES (127, 'Madagascar', 'MDG', 'MG');
INSERT INTO `#__jeajx_country` VALUES (128, 'Malawi', 'MWI', 'MW');
INSERT INTO `#__jeajx_country` VALUES (129, 'Malaysia', 'MYS', 'MY');
INSERT INTO `#__jeajx_country` VALUES (130, 'Maldives', 'MDV', 'MV');
INSERT INTO `#__jeajx_country` VALUES (131, 'Mali', 'MLI', 'ML');
INSERT INTO `#__jeajx_country` VALUES (132, 'Malta', 'MLT', 'MT');
INSERT INTO `#__jeajx_country` VALUES (133, 'Marshall Islands', 'MHL', 'MH');
INSERT INTO `#__jeajx_country` VALUES (134, 'Martinique', 'MTQ', 'MQ');
INSERT INTO `#__jeajx_country` VALUES (135, 'Mauritania', 'MRT', 'MR');
INSERT INTO `#__jeajx_country` VALUES (136, 'Mauritius', 'MUS', 'MU');
INSERT INTO `#__jeajx_country` VALUES (137, 'Mayotte', 'MYT', 'YT');
INSERT INTO `#__jeajx_country` VALUES (138, 'Mexico', 'MEX', 'MX');
INSERT INTO `#__jeajx_country` VALUES (139, 'Micronesia, Federated States of', 'FSM', 'FM');
INSERT INTO `#__jeajx_country` VALUES (140, 'Moldova, Republic of', 'MDA', 'MD');
INSERT INTO `#__jeajx_country` VALUES (141, 'Monaco', 'MCO', 'MC');
INSERT INTO `#__jeajx_country` VALUES (142, 'Mongolia', 'MNG', 'MN');
INSERT INTO `#__jeajx_country` VALUES (143, 'Montserrat', 'MSR', 'MS');
INSERT INTO `#__jeajx_country` VALUES (144, 'Morocco', 'MAR', 'MA');
INSERT INTO `#__jeajx_country` VALUES (145, 'Mozambique', 'MOZ', 'MZ');
INSERT INTO `#__jeajx_country` VALUES (146, 'Myanmar', 'MMR', 'MM');
INSERT INTO `#__jeajx_country` VALUES (147, 'Namibia', 'NAM', 'NA');
INSERT INTO `#__jeajx_country` VALUES (148, 'Nauru', 'NRU', 'NR');
INSERT INTO `#__jeajx_country` VALUES (149, 'Nepal', 'NPL', 'NP');
INSERT INTO `#__jeajx_country` VALUES (150, 'Netherlands', 'NLD', 'NL');
INSERT INTO `#__jeajx_country` VALUES (151, 'Netherlands Antilles', 'ANT', 'AN');
INSERT INTO `#__jeajx_country` VALUES (152, 'New Caledonia', 'NCL', 'NC');
INSERT INTO `#__jeajx_country` VALUES (153, 'New Zealand', 'NZL', 'NZ');
INSERT INTO `#__jeajx_country` VALUES (154, 'Nicaragua', 'NIC', 'NI');
INSERT INTO `#__jeajx_country` VALUES (155, 'Niger', 'NER', 'NE');
INSERT INTO `#__jeajx_country` VALUES (156, 'Nigeria', 'NGA', 'NG');
INSERT INTO `#__jeajx_country` VALUES (157, 'Niue', 'NIU', 'NU');
INSERT INTO `#__jeajx_country` VALUES (158, 'Norfolk Island', 'NFK', 'NF');
INSERT INTO `#__jeajx_country` VALUES (159, 'Northern Mariana Islands', 'MNP', 'MP');
INSERT INTO `#__jeajx_country` VALUES (160, 'Norway', 'NOR', 'NO');
INSERT INTO `#__jeajx_country` VALUES (161, 'Oman', 'OMN', 'OM');
INSERT INTO `#__jeajx_country` VALUES (162, 'Pakistan', 'PAK', 'PK');
INSERT INTO `#__jeajx_country` VALUES (163, 'Palau', 'PLW', 'PW');
INSERT INTO `#__jeajx_country` VALUES (164, 'Panama', 'PAN', 'PA');
INSERT INTO `#__jeajx_country` VALUES (165, 'Papua New Guinea', 'PNG', 'PG');
INSERT INTO `#__jeajx_country` VALUES (166, 'Paraguay', 'PRY', 'PY');
INSERT INTO `#__jeajx_country` VALUES (167, 'Peru', 'PER', 'PE');
INSERT INTO `#__jeajx_country` VALUES (168, 'Philippines', 'PHL', 'PH');
INSERT INTO `#__jeajx_country` VALUES (169, 'Pitcairn', 'PCN', 'PN');
INSERT INTO `#__jeajx_country` VALUES (170, 'Poland', 'POL', 'PL');
INSERT INTO `#__jeajx_country` VALUES (171, 'Portugal', 'PRT', 'PT');
INSERT INTO `#__jeajx_country` VALUES (172, 'Puerto Rico', 'PRI', 'PR');
INSERT INTO `#__jeajx_country` VALUES (173, 'Qatar', 'QAT', 'QA');
INSERT INTO `#__jeajx_country` VALUES (174, 'Reunion', 'REU', 'RE');
INSERT INTO `#__jeajx_country` VALUES (175, 'Romania', 'ROM', 'RO');
INSERT INTO `#__jeajx_country` VALUES (176, 'Russian Federation', 'RUS', 'RU');
INSERT INTO `#__jeajx_country` VALUES (177, 'Rwanda', 'RWA', 'RW');
INSERT INTO `#__jeajx_country` VALUES (178, 'Saint Kitts and Nevis', 'KNA', 'KN');
INSERT INTO `#__jeajx_country` VALUES (179, 'Saint Lucia', 'LCA', 'LC');
INSERT INTO `#__jeajx_country` VALUES (180, 'Saint Vincent and the Grenadines', 'VCT', 'VC');
INSERT INTO `#__jeajx_country` VALUES (181, 'Samoa', 'WSM', 'WS');
INSERT INTO `#__jeajx_country` VALUES (182, 'San Marino', 'SMR', 'SM');
INSERT INTO `#__jeajx_country` VALUES (183, 'Sao Tome and Principe', 'STP', 'ST');
INSERT INTO `#__jeajx_country` VALUES (184, 'Saudi Arabia', 'SAU', 'SA');
INSERT INTO `#__jeajx_country` VALUES (185, 'Senegal', 'SEN', 'SN');
INSERT INTO `#__jeajx_country` VALUES (186, 'Seychelles', 'SYC', 'SC');
INSERT INTO `#__jeajx_country` VALUES (187, 'Sierra Leone', 'SLE', 'SL');
INSERT INTO `#__jeajx_country` VALUES (188, 'Singapore', 'SGP', 'SG');
INSERT INTO `#__jeajx_country` VALUES (189, 'Slovakia (Slovak Republic)', 'SVK', 'SK');
INSERT INTO `#__jeajx_country` VALUES (190, 'Slovenia', 'SVN', 'SI');
INSERT INTO `#__jeajx_country` VALUES (191, 'Solomon Islands', 'SLB', 'SB');
INSERT INTO `#__jeajx_country` VALUES (192, 'Somalia', 'SOM', 'SO');
INSERT INTO `#__jeajx_country` VALUES (193, 'South Africa', 'ZAF', 'ZA');
INSERT INTO `#__jeajx_country` VALUES (194, 'South Georgia and the South Sandwich Islands', 'SGS', 'GS');
INSERT INTO `#__jeajx_country` VALUES (195, 'Spain', 'ESP', 'ES');
INSERT INTO `#__jeajx_country` VALUES (196, 'Sri Lanka', 'LKA', 'LK');
INSERT INTO `#__jeajx_country` VALUES (197, 'St. Helena', 'SHN', 'SH');
INSERT INTO `#__jeajx_country` VALUES (198, 'St. Pierre and Miquelon', 'SPM', 'PM');
INSERT INTO `#__jeajx_country` VALUES (199, 'Sudan', 'SDN', 'SD');
INSERT INTO `#__jeajx_country` VALUES (200, 'Suriname', 'SUR', 'SR');
INSERT INTO `#__jeajx_country` VALUES (201, 'Svalbard and Jan Mayen Islands', 'SJM', 'SJ');
INSERT INTO `#__jeajx_country` VALUES (202, 'Swaziland', 'SWZ', 'SZ');
INSERT INTO `#__jeajx_country` VALUES (203, 'Sweden', 'SWE', 'SE');
INSERT INTO `#__jeajx_country` VALUES (204, 'Switzerland', 'CHE', 'CH');
INSERT INTO `#__jeajx_country` VALUES (205, 'Syrian Arab Republic', 'SYR', 'SY');
INSERT INTO `#__jeajx_country` VALUES (206, 'Taiwan', 'TWN', 'TW');
INSERT INTO `#__jeajx_country` VALUES (207, 'Tajikistan', 'TJK', 'TJ');
INSERT INTO `#__jeajx_country` VALUES (208, 'Tanzania, United Republic of', 'TZA', 'TZ');
INSERT INTO `#__jeajx_country` VALUES (209, 'Thailand', 'THA', 'TH');
INSERT INTO `#__jeajx_country` VALUES (210, 'Togo', 'TGO', 'TG');
INSERT INTO `#__jeajx_country` VALUES (211, 'Tokelau', 'TKL', 'TK');
INSERT INTO `#__jeajx_country` VALUES (212, 'Tonga', 'TON', 'TO');
INSERT INTO `#__jeajx_country` VALUES (213, 'Trinidad and Tobago', 'TTO', 'TT');
INSERT INTO `#__jeajx_country` VALUES (214, 'Tunisia', 'TUN', 'TN');
INSERT INTO `#__jeajx_country` VALUES (215, 'Turkey', 'TUR', 'TR');
INSERT INTO `#__jeajx_country` VALUES (216, 'Turkmenistan', 'TKM', 'TM');
INSERT INTO `#__jeajx_country` VALUES (217, 'Turks and Caicos Islands', 'TCA', 'TC');
INSERT INTO `#__jeajx_country` VALUES (218, 'Tuvalu', 'TUV', 'TV');
INSERT INTO `#__jeajx_country` VALUES (219, 'Uganda', 'UGA', 'UG');
INSERT INTO `#__jeajx_country` VALUES (220, 'Ukraine', 'UKR', 'UA');
INSERT INTO `#__jeajx_country` VALUES (221, 'United Arab Emirates', 'ARE', 'AE');
INSERT INTO `#__jeajx_country` VALUES (222, 'United Kingdom', 'GBR', 'GB');
INSERT INTO `#__jeajx_country` VALUES (223, 'United States', 'USA', 'US');
INSERT INTO `#__jeajx_country` VALUES (224, 'United States Minor Outlying Islands', 'UMI', 'UM');
INSERT INTO `#__jeajx_country` VALUES (225, 'Uruguay', 'URY', 'UY');
INSERT INTO `#__jeajx_country` VALUES (226, 'Uzbekistan', 'UZB', 'UZ');
INSERT INTO `#__jeajx_country` VALUES (227, 'Vanuatu', 'VUT', 'VU');
INSERT INTO `#__jeajx_country` VALUES (228, 'Vatican City State (Holy See)', 'VAT', 'VA');
INSERT INTO `#__jeajx_country` VALUES (229, 'Venezuela', 'VEN', 'VE');
INSERT INTO `#__jeajx_country` VALUES (230, 'Viet Nam', 'VNM', 'VN');
INSERT INTO `#__jeajx_country` VALUES (231, 'Virgin Islands (British)', 'VGB', 'VG');
INSERT INTO `#__jeajx_country` VALUES (232, 'Virgin Islands (U.S.)', 'VIR', 'VI');
INSERT INTO `#__jeajx_country` VALUES (233, 'Wallis and Futuna Islands', 'WLF', 'WF');
INSERT INTO `#__jeajx_country` VALUES (234, 'Western Sahara', 'ESH', 'EH');
INSERT INTO `#__jeajx_country` VALUES (235, 'Yemen', 'YEM', 'YE');
INSERT INTO `#__jeajx_country` VALUES (236, 'Yugoslavia', 'YUG', 'YU');
INSERT INTO `#__jeajx_country` VALUES (237, 'The Democratic Republic of Congo', 'DRC', 'DC');
INSERT INTO `#__jeajx_country` VALUES (238, 'Zambia', 'ZMB', 'ZM');
INSERT INTO `#__jeajx_country` VALUES (239, 'Zimbabwe', 'ZWE', 'ZW');
INSERT INTO `#__jeajx_country` VALUES (240, 'East Timor', 'XET', 'XE');
INSERT INTO `#__jeajx_country` VALUES (241, 'Jersey', 'XJE', 'XJ');
INSERT INTO `#__jeajx_country` VALUES (242, 'St. Barthelemy', 'XSB', 'XB');
INSERT INTO `#__jeajx_country` VALUES (243, 'St. Eustatius', 'XSE', 'XU');
INSERT INTO `#__jeajx_country` VALUES (244, 'Canary Islands', 'XCA', 'XC');
