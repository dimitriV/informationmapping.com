<?php
/**
 * @version        $Id: edit.php 21529 2011-06-11 22:17:15Z chdemko $
 * @package        Joomla.Administrator
 * @subpackage    com_sliderbanners
 * @copyright    Copyright (C) 2005 - 2011 Open Source Matters, Inc. All rights reserved.
 * @license        GNU General Public License version 2 or later; see LICENSE.txt
 */

// no direct access
defined('_JEXEC') or die;

JHtml::addIncludePath(JPATH_COMPONENT.'/helpers/html');
JHtml::_('behavior.tooltip');
JHtml::_('behavior.formvalidation');
?>
<script type="text/javascript">
    Joomla.submitbutton = function(task)
    {
        if (task == 'sliderbanner.cancel' || document.formvalidator.isValid(document.id('sliderbanner-form'))) {
            <?php echo $this->form->getField('description')->save(); ?>
            Joomla.submitform(task, document.getElementById('sliderbanner-form'));
        }
        else {
            alert('<?php echo $this->escape(JText::_('JGLOBAL_VALIDATION_FORM_FAILED'));?>');
        }
    }
</script>

<form action="<?php echo JRoute::_('index.php?option=com_sliderbanners&layout=edit&id='.(int) $this->item->id); ?>" method="post" name="adminForm" enctype="multipart/form-data" id="sliderbanner-form" class="form-validate">
<div class="row-fluid form-horizontal-desktop">
    <div class="span7">
        <fieldset class="adminform">
            <legend><?php echo empty($this->item->id) ? JText::_('COM_SLIDERBANNERS_NEW_SLIDERBANNER') : JText::sprintf('COM_SLIDERBANNERS_EDIT_SLIDERBANNER', $this->item->id); ?></legend>

            <div class="control-label"><?php echo $this->form->getLabel('title'); ?></div>
            <div class="control"><?php echo $this->form->getInput('title'); ?></div>

            <div class="control-label"><?php echo $this->form->getLabel('alias'); ?></div>
            <div class="control"><?php echo $this->form->getInput('alias'); ?></div>

            <div class="control-label"><?php echo $this->form->getLabel('catid'); ?></div>
            <div class="control"><?php echo $this->form->getInput('catid'); ?></div>
                
            <div class="control-label"><?php echo $this->form->getLabel('button_label1'); ?></div>
            <div class="control"><?php echo $this->form->getInput('button_label1'); ?></div>
                
            <div class="control-label"><?php echo $this->form->getLabel('button_link1'); ?></div>
            <div class="control"><?php echo $this->form->getInput('button_link1'); ?></div>
                
            <div class="control-label"><?php echo $this->form->getLabel('target_link1'); ?></div>
            <div class="control"><?php echo $this->form->getInput('target_link1'); ?></div>
                
            <div class="control-label"><?php echo $this->form->getLabel('button_label2'); ?></div>
            <div class="control"><?php echo $this->form->getInput('button_label2'); ?></div>
                
            <div class="control-label"><?php echo $this->form->getLabel('button_link2'); ?></div>
            <div class="control"><?php echo $this->form->getInput('button_link2'); ?></div>
                
            <div class="control-label"><?php echo $this->form->getLabel('target_link2'); ?></div>
            <div class="control"><?php echo $this->form->getInput('target_link2'); ?></div>
                
            <div class="control-label"><?php echo $this->form->getLabel('filename'); ?></div>
            <div class="control"><?php echo $this->form->getInput('filename'); ?>
                <span style="float:left;width: 300px;padding: 10px 0 0 5px;"><?php echo
                JText::_('BEST_DIMENSIONS_WIDTH:493PX_HEIGHT:290PX');?></span></div>
                
            <?php if($this->item->filename):?>
            <div class="control-label">
                <label><img src="<?php echo JURI::root().'images/sliderbanners/'.$this->item->filename;?>" height="40" width="40"/></label>
            </div>
            <?php endif;?>
                
            <div class="control-label"><?php echo $this->form->getLabel('file_link'); ?></div>
            <div class="control"><?php echo $this->form->getInput('file_link'); ?></div>
                
            <div class="control-label"><?php echo $this->form->getLabel('state'); ?></div>
            <div class="control"><?php echo $this->form->getInput('state'); ?></div>

            <div class="control-label"><?php echo $this->form->getLabel('access'); ?></div>
            <div class="control"><?php echo $this->form->getInput('access'); ?></div>

            <div class="control-label"><?php echo $this->form->getLabel('ordering'); ?></div>
            <div class="control"><?php echo $this->form->getInput('ordering'); ?></div>

            <div class="control-label"><?php echo $this->form->getLabel('language'); ?></div>
            <div class="control"><?php echo $this->form->getInput('language'); ?></div>

            <div class="control-label"><?php echo $this->form->getLabel('id'); ?></div>
            <div class="control"><?php echo $this->form->getInput('id'); ?></div>

        </fieldset>
    </div>
    <div class="span5">
        <fieldset class="panelform">
            <legend><?php echo JText::_('JGLOBAL_FIELDSET_PUBLISHING') ; ?></legend>

            <div class="control-label"><?php echo $this->form->getLabel('created_by'); ?></div>
            <div class="control"><?php echo $this->form->getInput('created_by'); ?></div>

            <div class="control-label"><?php echo $this->form->getLabel('created_by_alias'); ?></div>
            <div class="control"><?php echo $this->form->getInput('created_by_alias'); ?></div>

            <div class="control-label"><?php echo $this->form->getLabel('created'); ?></div>
            <div class="control"><?php echo $this->form->getInput('created'); ?></div>

            <div class="control-label"><?php echo $this->form->getLabel('publish_up'); ?></div>
            <div class="control"><?php echo $this->form->getInput('publish_up'); ?></div>

            <div class="control-label"><?php echo $this->form->getLabel('publish_down'); ?></div>
            <div class="control"><?php echo $this->form->getInput('publish_down'); ?></div>

            <?php if ($this->item->modified_by) : ?>
                <div class="control-label"><?php echo $this->form->getLabel('modified_by'); ?></div>
                <div class="control"><?php echo $this->form->getInput('modified_by'); ?></div>

                <div class="control-label"><?php echo $this->form->getLabel('modified'); ?></div>
                <div class="control"><?php echo $this->form->getInput('modified'); ?></div>
            <?php endif; ?>

            <?php if ($this->item->hits) : ?>
                <div class="control-label"><?php echo $this->form->getLabel('hits'); ?></div>
                <div class="control"><?php echo $this->form->getInput('hits'); ?></div>
            <?php endif; ?>

        </fieldset>

        <?php echo $this->loadTemplate('params'); ?>

        <?php echo $this->loadTemplate('metadata'); ?>

    </div>
</div>
<div class="row-fluid form-horizontal-desktop">
    <div class="span12">
        <?php echo $this->form->getLabel('description'); ?>
        <div class="clr"></div>
        <?php echo $this->form->getInput('description'); ?>
    </div>
</div>
<input type="hidden" name="task" value="" />
<?php echo JHtml::_('form.token'); ?>
</form>
