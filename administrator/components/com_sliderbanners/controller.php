<?php
/**
 * @version		$Id: controller.php 20196 2011-01-09 02:40:25Z ian $
 * @package		Joomla.Administrator
 * @subpackage	com_sliderbanners
 * @copyright	Copyright (C) 2005 - 2011 Open Source Matters, Inc. All rights reserved.
 * @license		GNU General Public License version 2 or later; see LICENSE.txt
 */

// No direct access
defined('_JEXEC') or die;

/**
 * Sliderbanners Sliderbanner Controller
 *
 * @package		Joomla.Administrator
 * @subpackage	com_sliderbanners
 * @since		1.5
 */
class SliderbannersController extends JControllerLegacy
{
	/**
	 * Method to display a view.
	 *
	 * @param	boolean			$cachable	If true, the view output will be cached
	 * @param	array			$urlparams	An array of safe url parameters and their variable types, for valid values see {@link JFilterInput::clean()}.
	 *
	 * @return	JController		This object to support chaining.
	 * @since	1.5
	 */
	public function display($cachable = false, $urlparams = false)
	{
		require_once JPATH_COMPONENT.'/helpers/sliderbanners.php';

		// Load the submenu.
		SliderbannersHelper::addSubmenu(JRequest::getCmd('view', 'sliderbanners'));

		$view		= JRequest::getCmd('view', 'sliderbanners');
		$layout 	= JRequest::getCmd('layout', 'default');
		$id			= JRequest::getInt('id');

		// Check for edit form.
		if ($view == 'sliderbanner' && $layout == 'edit' && !$this->checkEditId('com_sliderbanners.edit.sliderbanner', $id)) {
			// Somehow the person just went to the form - we don't allow that.
			$this->setError(JText::sprintf('JLIB_APPLICATION_ERROR_UNHELD_ID', $id));
			$this->setMessage($this->getError(), 'error');
			$this->setRedirect(JRoute::_('index.php?option=com_sliderbanners&view=sliderbanners', false));

			return false;
		}

		parent::display();

		return $this;
	}
}
