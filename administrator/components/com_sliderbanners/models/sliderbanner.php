<?php
/**
 * @version		$Id: sliderbanner.php 21700 2011-06-28 04:32:41Z dextercowley $
 * @copyright	Copyright (C) 2005 - 2011 Open Source Matters, Inc. All rights reserved.
 * @license		GNU General Public License version 2 or later; see LICENSE.txt
 */

// No direct access.
defined('_JEXEC') or die;

jimport('joomla.application.component.modeladmin');

/**
 * Sliderbanners model.
 *
 * @package		Joomla.Administrator
 * @subpackage	com_sliderbanners
 * @since		1.5
 */
class SliderbannersModelSliderbanner extends JModelAdmin
{
	/**
	 * @var		string	The prefix to use with controller messages.
	 * @since	1.6
	 */
	protected $text_prefix = 'COM_SLIDERBANNERS';

	/**
	 * Method to test whether a record can be deleted.
	 *
	 * @param	object	A record object.
	 * @return	boolean	True if allowed to delete the record. Defaults to the permission set in the component.
	 * @since	1.6
	 */
	protected function canDelete($record)
	{
		if (!empty($record->id)) {
			if ($record->state != -2) {
				return ;
			}
			$user = JFactory::getUser();

			if ($record->catid) {
				return $user->authorise('core.delete', 'com_sliderbanners.category.'.(int) $record->catid);
			}
			else {
				return parent::canDelete($record);
			}
		}
	}

	/**
	 * Method to test whether a record can have its state changed.
	 *
	 * @param	object	A record object.
	 * @return	boolean	True if allowed to change the state of the record. Defaults to the permission set in the component.
	 * @since	1.6
	 */
	protected function canEditState($record)
	{
		$user = JFactory::getUser();

		if (!empty($record->catid)) {
			return $user->authorise('core.edit.state', 'com_sliderbanners.category.'.(int) $record->catid);
		}
		else {
			return parent::canEditState($record);
		}
	}
	/**
	 * Returns a reference to the a Table object, always creating it.
	 *
	 * @param	type	The table type to instantiate
	 * @param	string	A prefix for the table class name. Optional.
	 * @param	array	Configuration array for model. Optional.
	 * @return	JTable	A database object
	 * @since	1.6
	 */
	public function getTable($type = 'Sliderbanner', $prefix = 'SliderbannersTable', $config = array())
	{
		return JTable::getInstance($type, $prefix, $config);
	}

	/**
	 * Method to get the record form.
	 *
	 * @param	array	$data		An optional array of data for the form to interogate.
	 * @param	boolean	$loadData	True if the form is to load its own data (default case), false if not.
	 * @return	JForm	A JForm object on success, false on failure
	 * @since	1.6
	 */
	public function getForm($data = array(), $loadData = true)
	{
		// Initialise variables.
		$app	= JFactory::getApplication();

		// Get the form.
		$form = $this->loadForm('com_sliderbanners.sliderbanner', 'sliderbanner', array('control' => 'jform', 'load_data' => $loadData));
		if (empty($form)) {
			return false;
		}

		// Determine correct permissions to check.
		if ($this->getState('sliderbanner.id')) {
			// Existing record. Can only edit in selected categories.
			$form->setFieldAttribute('catid', 'action', 'core.edit');
		} else {
			// New record. Can only create in selected categories.
			$form->setFieldAttribute('catid', 'action', 'core.create');
		}

		// Modify the form based on access controls.
		if (!$this->canEditState((object) $data)) {
			// Disable fields for display.
			$form->setFieldAttribute('ordering', 'disabled', 'true');
			$form->setFieldAttribute('state', 'disabled', 'true');
			$form->setFieldAttribute('publish_up', 'disabled', 'true');
			$form->setFieldAttribute('publish_down', 'disabled', 'true');

			// Disable fields while saving.
			// The controller has already verified this is a record you can edit.
			$form->setFieldAttribute('ordering', 'filter', 'unset');
			$form->setFieldAttribute('state', 'filter', 'unset');
			$form->setFieldAttribute('publish_up', 'filter', 'unset');
			$form->setFieldAttribute('publish_down', 'filter', 'unset');
		}

		return $form;
	}

	/**
	 * Method to get the data that should be injected in the form.
	 *
	 * @return	mixed	The data for the form.
	 * @since	1.6
	 */
	protected function loadFormData()
	{
		// Check the session for previously entered form data.
		$data = JFactory::getApplication()->getUserState('com_sliderbanners.edit.sliderbanner.data', array());

		if (empty($data)) {
			$data = $this->getItem();

			// Prime some default values.
			if ($this->getState('sliderbanner.id') == 0) {
				$app = JFactory::getApplication();
				$data->set('catid', JRequest::getInt('catid', $app->getUserState('com_sliderbanners.sliderbanners.filter.category_id')));
			}
		}

		return $data;
	}

	/**
	 * Method to get a single record.
	 *
	 * @param	integer	The id of the primary key.
	 *
	 * @return	mixed	Object on success, false on failure.
	 * @since	1.6
	 */
	public function getItem($pk = null)
	{
		if ($item = parent::getItem($pk)) {
			// Convert the params field to an array.
			$registry = new JRegistry;
			$registry->loadString($item->metadata);
			$item->metadata = $registry->toArray();
		}

		return $item;
	}


    public function save()
    {$data      = JRequest::getVar('jform', array(), 'post', 'array');
      if($data['id'] != '0')  
      {
        $prev = $this->getprevoiusdata($data['id']);  
      }  
    
    //$file = JRequest::getVar('jform', array(), 'files', 'array');
    /*echo '<pre>';
    print_r($file);
    print_r($data);
    echo '</pre>';
        die('test');*/
      //Image processing
        
        if($_FILES['jform']['tmp_name']['filename']){
            if($prev)
            {
                $img_pathold = JPATH_ROOT.DIRECTORY_SEPARATOR .'images'.DIRECTORY_SEPARATOR .'sliderbanners'.DIRECTORY_SEPARATOR .$prev;
                @unlink($img_pathold);
            }
                    $source = $_FILES['jform']['tmp_name']['filename'];
                    $file_name =  mktime().'-'.strtolower(str_replace(' ','-',$_FILES['jform']['name']['filename']));
                    
                    $img_path = JPATH_ROOT.DIRECTORY_SEPARATOR .'images'.DIRECTORY_SEPARATOR .'sliderbanners'.DIRECTORY_SEPARATOR .$file_name;
                   
                    $system=explode('.',$file_name);
                    $ext = $system[count($system)-1];
           
        		if (preg_match('/jpg|jpeg|JPG|JPEG|gif|GIF|png|PNG|bmp|BMP/',$ext))
        		{
      	             if(copy($source, $img_path))
                        {
                        list($w,$h) = getimagesize($source);    
                          if($w >= 493){
                               $this->createthumb($img_path,$img_path,493,290);
                            }
                          list($w1,$h1) = getimagesize($img_path); 
                          if($h1 >= 290){
                            $this->createthumb($img_path,$img_path,493,290);
                          }
                      if(isset($data['filename']))
                        $backup = $data['filename'];
                      
                        $data['filename'] =  $file_name;
                		}
                		else
                		{
                	     @unlink($img_path);
                		$msg = JText::_('upload failer try later');
                	    $this->setError($msg);
            			return false;  
                		}
                  }
                  else{
                        $msg = JText::_('file not supported');
                	    $this->setError($msg);
            			return false;  
                  }
            }
     
        //End of image processing. 
       $dispatcher = JDispatcher::getInstance();
		$table		= $this->getTable();
		$key			= $table->getKeyName();
		$pk			= (!empty($data[$key])) ? $data[$key] : (int)$this->getState($this->getName().'.id');
		$isNew		= true;

		// Include the content plugins for the on save events.
		JPluginHelper::importPlugin('content');

		// Allow an exception to be thrown.
		try
		{
			// Load the row if saving an existing record.
			if ($pk > 0) {
				$table->load($pk);
				$isNew = false;
			}

			// Bind the data.
			if (!$table->bind($data)) {
				$this->setError($table->getError());
				return false;
			}

			// Prepare the row for saving
			$this->prepareTable($table);

			// Check the data.
			if (!$table->check()) {
				$this->setError($table->getError());
				return false;
			}

			// Trigger the onContentBeforeSave event.
			$result = $dispatcher->trigger($this->event_before_save, array($this->option.'.'.$this->name, &$table, $isNew));
			if (in_array(false, $result, true)) {
				$this->setError($table->getError());
				return false;
			}

			// Store the data.
			if (!$table->store()) {
			    @unlink($img_path);
				$this->setError($table->getError());
				return false;
			}

			// Clean the cache.
			$this->cleanCache();

			// Trigger the onContentAfterSave event.
			$dispatcher->trigger($this->event_after_save, array($this->option.'.'.$this->name, &$table, $isNew));
		}
		catch (Exception $e)
		{
			$this->setError($e->getMessage());

			return false;
		}

		$pkName = $table->getKeyName();

		if (isset($table->$pkName)) {
			$this->setState($this->getName().'.id', $table->$pkName);
		}
		$this->setState($this->getName().'.new', $isNew);

		return true; 
    }

	/**
	 * Prepare and sanitise the table prior to saving.
	 *
	 * @since	1.6
	 */
	protected function prepareTable(&$table)
	{
		jimport('joomla.filter.output');
		$date = JFactory::getDate();
		$user = JFactory::getUser();

		$table->title		= htmlspecialchars_decode($table->title, ENT_QUOTES);
		$table->alias		= JApplication::stringURLSafe($table->alias);

		if (empty($table->alias)) {
			$table->alias = JApplication::stringURLSafe($table->title);
		}

		if (empty($table->id)) {
			// Set the values

			// Set ordering to the last item if not set
			if (empty($table->ordering)) {
				$db = JFactory::getDbo();
				$db->setQuery('SELECT MAX(ordering) FROM #__sliderbanners');
				$max = $db->loadResult();

				$table->ordering = $max+1;
			}
		}
		else {
			// Set the values
		}
	}

    public function getprevoiusdata($id)
    {
        $db = JFactory::getDbo();
        $db->setQuery('SELECT filename FROM #__sliderbanners where id='.$id);
        $file = $db->loadResult();
        return $file;
    }
	/**
	 * A protected method to get a set of ordering conditions.
	 *
	 * @param	object	A record object.
	 * @return	array	An array of conditions to add to add to ordering queries.
	 * @since	1.6
	 */
	protected function getReorderConditions($table)
	{
		$condition = array();
		$condition[] = 'catid = '.(int) $table->catid;
		return $condition;
	}
    
     function createthumb($name,$filename,$new_w,$new_h)
        {
    		$system=explode('.',$name);
            $ext = $system[count($system)-1];
    		if (preg_match('/jpg|jpeg/',$ext)){
    		  $src_img=imagecreatefromjpeg($name);
    		}
    		elseif (preg_match('/png/',$ext)){
    		  $src_img=imagecreatefrompng($name);
    		}
            elseif (preg_match('/gif/',$ext)){
    		  $src_img=imagecreatefromgif($name);
    		}
            elseif (preg_match('/bmp/',$ext)){
    		  $src_img=$this->imagecreatefrombmp($name);
    		}
    		else{
        		$src_img=imagecreatefromjpeg($name);
    		}
    
    		$old_x=imageSX($src_img);
           	$old_y=imageSY($src_img);
            
            if($new_w == $new_h){
                $thumb_h = $new_h;
                $thumb_w = floor($old_x-($old_x*(($old_y-$new_h)/$old_y)));
            }
            else{
        		$thumb_w=$new_w;
        		$thumb_h=floor($old_y-($old_y*(($old_x-$new_w)/$old_x)));
            }
            
            //$thumb_w = 493;
            //$thumb_h = 290;
            
    		$dst_img=imagecreatetruecolor($thumb_w,$thumb_h);
            
            if (preg_match("/png|gif/",$ext)){
                imagealphablending($dst_img, false);
                imagesavealpha($dst_img,true);
                $transparent = imagecolorallocatealpha($dst_img, 255, 255, 255, 127);
                imagefilledrectangle($dst_img, 0, 0, $thumb_w, $thumb_h, $transparent);
            }
    		imagecopyresampled($dst_img,$src_img,0,0,0,0,$thumb_w,$thumb_h,$old_x,$old_y); 
    
    		if (preg_match("/png/",$ext)){
                imagepng($dst_img,$filename); 
            }
            elseif (preg_match('/gif/',$ext)){
                imagegif($dst_img,$filename);
    		}
            elseif (preg_match('/bmp/',$ext)){
                #$this->imagebmp($dst_img,$filename);
                 imagejpeg($dst_img,$filename,100);
    		}
            else{
                imagejpeg($dst_img,$filename,100);
    		}
    		imagedestroy($dst_img); 
    		imagedestroy($src_img); 
	}
///////////////////////////////////////////////////////////////   
    public function imagecreatefrombmp($p_sFile)
    {
        //    Load the image into a string
        $file    =    fopen($p_sFile,"rb");
        $read    =    fread($file,10);
        while(!feof($file)&&($read<>""))
            $read    .=    fread($file,1024);
       
        $temp    =    unpack("H*",$read);
        $hex    =    $temp[1];
        $header    =    substr($hex,0,108);
       
        //    Process the header
        //    Structure: http://www.fastgraph.com/help/bmp_header_format.html
        if (substr($header,0,4)=="424d")
        {
            //    Cut it in parts of 2 bytes
            $header_parts    =    str_split($header,2);
           
            //    Get the width        4 bytes
            $width            =    hexdec($header_parts[19].$header_parts[18]);
           
            //    Get the height        4 bytes
            $height            =    hexdec($header_parts[23].$header_parts[22]);
           
            //    Unset the header params
            unset($header_parts);
        }
       
        //    Define starting X and Y
        $x                =    0;
        $y                =    1;
       
        //    Create newimage
        $image            =    imagecreatetruecolor($width,$height);
       
        //    Grab the body from the image
        $body            =    substr($hex,108);

        //    Calculate if padding at the end-line is needed
        //    Divided by two to keep overview.
        //    1 byte = 2 HEX-chars
        $body_size        =    (strlen($body)/2);
        $header_size    =    ($width*$height);

        //    Use end-line padding? Only when needed
        $usePadding        =    ($body_size>($header_size*3)+4);
       
        //    Using a for-loop with index-calculation instaid of str_split to avoid large memory consumption
        //    Calculate the next DWORD-position in the body
        for ($i=0;$i<$body_size;$i+=3)
        {
            //    Calculate line-ending and padding
            if ($x>=$width)
            {
                //    If padding needed, ignore image-padding
                //    Shift i to the ending of the current 32-bit-block
                if ($usePadding)
                    $i    +=    $width%4;
               
                //    Reset horizontal position
                $x    =    0;
               
                //    Raise the height-position (bottom-up)
                $y++;
               
                //    Reached the image-height? Break the for-loop
                if ($y>$height)
                    break;
            }
           
            //    Calculation of the RGB-pixel (defined as BGR in image-data)
            //    Define $i_pos as absolute position in the body
            $i_pos    =    $i*2;
            $r        =    hexdec($body[$i_pos+4].$body[$i_pos+5]);
            $g        =    hexdec($body[$i_pos+2].$body[$i_pos+3]);
            $b        =    hexdec($body[$i_pos].$body[$i_pos+1]);
           
            //    Calculate and draw the pixel
            $color    =    imagecolorallocate($image,$r,$g,$b);
            imagesetpixel($image,$x,$height-$y,$color);
           
            //    Raise the horizontal position
            $x++;
        }
       
        //    Unset the body / free the memory
        unset($body);
       
        //    Return image-object
        return $image;
    }
}