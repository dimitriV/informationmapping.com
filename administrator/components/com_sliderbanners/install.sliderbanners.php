<?php
/**
* Sliderbanners - A Joomla Component for managing sliderbanner images.
 * @version     $Id: install.sliderbanners.php  2010-10-23 04:13:25Z $
 * @package     Iinformationmapping
 * @subpackage  com_sliderbanners
 * @author      ITOffshore Nepal
 * @copyright   Copyright (C) 2011 ITOffshore Nepal.
 * @license     ITOffshore Nepal
 */

// no direct access
defined( '_JEXEC' ) or die( 'Restricted access' );

function com_install() {

  # Show installation result to user
  ?>
  <center>
  <table width="100%" border="0">
    <tr>
      <td>
        <font class="small">&copy; Copyright 2010 by IT Offshore Nepal</font><br/>
		<br />
		<?php
			// Create directory for storing article images
			if(file_exists(JPATH_ROOT.DS."images".DS."sliderbanners")){
				echo 'The directory "sliderbanners" already exist in "'.JPATH_ROOT.DS. 'images"<br />';
			}
			else{
				mkdir(JPATH_ROOT.DS."images".DS."sliderbanners", 0777);
				echo 'The directory "sliderbanners" has been created in "'.JPATH_ROOT.DS.'images"<br />';
			}
		    if(file_exists(JPATH_ROOT.DS."images".DS."sliderbanners")){
				echo 'The directory "sliderbanners" already exist in "'.JPATH_ROOT.DS.'images'.DS.'sliderbanners"<br />';
			}
			else{
				mkdir(JPATH_ROOT.DS."images".DS."sliderbanners", 0777);
				echo 'The directory "sliderbanners" has been created in "'.JPATH_ROOT.DS.'images'.DS.'sliderbanners"<br />';
			}
            
		?>
		<br />
		Please upload images in the directories inside "sliderbanners".
      </td>
    </tr>
  </table>
  </center>
  <?php
}
?>