<?php
/**
 * @version $Id: edit.php 26 2015-06-30 17:26:42Z szymon $
 * @package DJ-ImageSlider
 * @subpackage DJ-ImageSlider Component
 * @copyright Copyright (C) 2012 DJ-Extensions.com, All rights reserved.
 * @license http://www.gnu.org/licenses GNU/GPL
 * @author url: http://dj-extensions.com
 * @author email contact@dj-extensions.com
 * @developer Szymon Woronowski - szymon.woronowski@design-joomla.eu
 *
 *
 * DJ-ImageSlider is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * DJ-ImageSlider is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with DJ-ImageSlider. If not, see <http://www.gnu.org/licenses/>.
 *
 */

// No direct access.
defined('_JEXEC') or die;

JHtml::_('behavior.framework');
JHtml::_('behavior.tooltip');
JHtml::_('behavior.formvalidation');
if(version_compare(JVERSION, '3.0', '>=')) JHtml::_('formbehavior.chosen', 'select'); /* J!3.0 only */
?>

<script type="text/javascript">
	Joomla.submitbutton = function(task)
	{
		if (task == 'item.cancel' || document.formvalidator.isValid(document.id('item-form'))) {
			<?php echo $this->form->getField('description')->save(); ?>
			Joomla.submitform(task, document.getElementById('item-form'));
		}
		else {
			alert("<?php echo $this->escape(JText::_('COM_DJIMAGESLIDER_VALIDATION_FORM_FAILED'));?>");
		}
	}
</script>

<form action="<?php echo JRoute::_('index.php?option=com_djimageslider&layout=edit&id='.(int) $this->item->id); ?>" method="post" name="adminForm" id="item-form" class="form-validate form-horizontal">
	<div class="row-fluid">	
	<div class="width-60 fltlft span7 well">
		<fieldset class="adminform">
		
			<div class="control-group">
				<div class="row-fluid">	
					<div class="span2">
						<div class="control-label"><?php echo $this->form->getLabel('catid'); ?></div>
					</div>
					<div class="span5">
						<div class="controls" style="margin-left: 0px !important"><?php echo $this->form->getInput('catid'); ?></div>
					</div>
					<div class="span5">
					</div>
				</div>
			</div>

			<div class="control-group">
				<div class="control-label"><?php echo $this->form->getLabel('image'); ?></div>
				<div class="controls"><?php echo $this->form->getInput('image'); ?></div>
			</div>
			<div class="control-group">
				<div class="control-label"><?php echo $this->form->getLabel('image_mobile'); ?></div>
				<div class="controls"><?php echo $this->form->getInput('image_mobile'); ?></div>
			</div>

			<div class="control-group">
				<div class="control-label"><?php echo $this->form->getLabel('title_color'); ?></div>
				<div class="controls"><?php echo $this->form->getInput('title_color'); ?></div>
			</div>
			<div class="control-group">
				<div class="control-label"><?php echo $this->form->getLabel('text_color'); ?></div>
				<div class="controls"><?php echo $this->form->getInput('text_color'); ?></div>
			</div>
			<div class="control-group">
				<div class="control-label"><?php echo $this->form->getLabel('background_color'); ?></div>
				<div class="controls"><?php echo $this->form->getInput('background_color'); ?></div>
			</div>
			<div class="control-group">
				<div class="control-label"><?php echo $this->form->getLabel('background_trans'); ?></div>
				<div class="controls"><?php echo $this->form->getInput('background_trans'); ?></div>
			</div>

			<div class="row-fluid">	
				<div class="span2">
					<h3><?php echo empty($this->item->id) ? JText::_('COM_DJIMAGESLIDER_NEW') : JText::sprintf('COM_DJIMAGESLIDER_EDIT', $this->item->id); ?></h3>							
				</div>
				<div class="span5">
					<h3>Default</h3>							
				</div>
				<div class="span5">
					<h3>Mobile</h3>							
				</div>
			</div>
			
			<div class="tab-content">

				<div class="control-group">
					<div class="row-fluid">	
						<div class="span2">
							<div class="control-label"><?php echo $this->form->getLabel('title'); ?></div>
						</div>
						<div class="span5">
							<div class="controls" style="margin-left: 0px !important"><?php echo $this->form->getInput('title'); ?></div>
						</div>
						<div class="span5">
							<div class="controls" style="margin-left: 0px !important"><?php echo $this->form->getInput('title_mobile'); ?></div>
						</div>
					</div>
				</div>

				<div class="control-group">
					<div class="row-fluid">	
						<div class="span2">
							<div class="control-label"><?php echo $this->form->getLabel('offset_x'); ?></div>
						</div>
						<div class="span5">
							<div class="controls" style="margin-left: 0px !important"><?php echo $this->form->getInput('offset_x'); ?></div>
						</div>
						<div class="span5">
							<div class="controls" style="margin-left: 0px !important"><?php echo $this->form->getInput('offset_x_mobile'); ?></div>
						</div>
					</div>
				</div>

				<div class="control-group">
					<div class="row-fluid">	
						<div class="span2">
							<div class="control-label"><?php echo $this->form->getLabel('offset_y'); ?></div>
						</div>
						<div class="span5">
							<div class="controls" style="margin-left: 0px !important"><?php echo $this->form->getInput('offset_y'); ?></div>
						</div>
						<div class="span5">
							<div class="controls" style="margin-left: 0px !important"><?php echo $this->form->getInput('offset_y_mobile'); ?></div>
						</div>
					</div>
				</div>

				<div style="clear:both"></div>
				
				<div class="control-group">
					<h3><?php echo $this->form->getLabel('description'); ?></h3>
					<div style="clear:both"></div>
					<div class="controls" style="margin-left: 0px !important"><?php echo $this->form->getInput('description'); ?></div>
				</div>
				
				<div style="clear:both"></div>

				<div class="control-group">
					<h3><?php echo $this->form->getLabel('description_mobile'); ?></h3>
					<div style="clear:both"></div>
					<div class="controls" style="margin-left: 0px !important"><?php echo $this->form->getInput('description_mobile'); ?></div>
				</div>

			</div>
		</fieldset>
	</div>

	<div class="width-40 fltrt span5 well">
		
		<fieldset class="panelform" >
		
			<h3><?php echo JText::_('COM_DJIMAGESLIDER_PUBLISHING_OPTIONS'); ?></h3>
			
				<div class="control-group">
					<div class="control-label"><?php echo $this->form->getLabel('published'); ?></div>
					<div class="controls"><?php echo $this->form->getInput('published'); ?></div>
				</div>
				<div class="control-group">
					<div class="control-label"><?php echo $this->form->getLabel('publish_up'); ?></div>
					<div class="controls"><?php echo $this->form->getInput('publish_up'); ?></div>
				</div>
				<div class="control-group">
					<div class="control-label"><?php echo $this->form->getLabel('publish_down'); ?></div>
					<div class="controls"><?php echo $this->form->getInput('publish_down'); ?></div>
				</div>
				<div class="control-group">
					<div class="control-label"><?php echo $this->form->getLabel('id'); ?></div>
					<div class="controls"><?php echo $this->form->getInput('id'); ?></div>
				</div>
			
		</fieldset>
		
		<?php echo $this->loadTemplate('params'); ?>
		
		<input type="hidden" name="task" value="" />
		<?php echo JHtml::_('form.token'); ?>
	</div>
	</div>
</form>

<div class="clr"></div>
