CREATE TABLE IF NOT EXISTS `#__djimageslider` (
  `id` int(10) UNSIGNED NOT NULL,
  `catid` int(10) UNSIGNED NOT NULL DEFAULT '0',
  `title` varchar(255) NOT NULL,
  `title_mobile` varchar(255) NOT NULL,
  `alias` varchar(255) NOT NULL DEFAULT '',
  `image` varchar(255) NOT NULL,
  `image_mobile` varchar(255) NOT NULL,
  `description` text NOT NULL,
  `offset_x` varchar(10) NOT NULL,
  `offset_y` varchar(10) NOT NULL,
  `description_mobile` text NOT NULL,
  `offset_x_mobile` varchar(10) NOT NULL,
  `offset_y_mobile` varchar(10) NOT NULL,
  `title_color` varchar(10) NOT NULL,
  `text_color` varchar(10) NOT NULL,
  `textblock_width` varchar(10) NOT NULL,
  `background_color` varchar(10) NOT NULL,
  `background_trans` varchar(10) NOT NULL,
  `published` tinyint(1) NOT NULL DEFAULT '0',
  `publish_up` datetime NOT NULL DEFAULT '0000-00-00 00:00:00',
  `publish_down` datetime NOT NULL DEFAULT '0000-00-00 00:00:00',
  `checked_out` int(10) UNSIGNED NOT NULL DEFAULT '0',
  `checked_out_time` datetime NOT NULL DEFAULT '0000-00-00 00:00:00',
  `ordering` int(11) NOT NULL DEFAULT '0',
  `params` text NOT NULL,
  PRIMARY KEY  (`id`),
  KEY `catid` (`catid`,`published`)
) DEFAULT CHARSET=utf8;
ALTER TABLE `#__djimageslider`
  MODIFY `id` int(10) UNSIGNED NOT NULL AUTO_INCREMENT;
