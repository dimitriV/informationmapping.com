<?php
/**
 * @version     $Id:Course.php  2011-12-20 04:13:25Z $
 * @package     IMI 
 * @subpackage  com_courses
 * @author      ITOffshore Nepal
 * @copyright   Copyright (C) 2011 ITOffshore Nepal.
 * @license     ITOffshore Nepal
 */
// No direct access
defined('_JEXEC') or die;

/**
 * Courses helper.
 *
 * @package		Joomla.Administrator
 * @subpackage	com_courses
 * @since		1.6
 */
class CoursesHelper
{
	/**
	 * Configure the Linkbar.
	 *
	 * @param	string	The name of the active view.
	 * @since	1.6
	 */
	public static function addSubmenu($vName = 'courselanguages')
	{
		JSubMenuHelper::addEntry(
			JText::_('COM_COURSES_SUBMENU_LANGUAGE'),
			'index.php?option=com_courses&view=courselanguages',
			$vName == 'courselanguages'
		);
        JSubMenuHelper::addEntry(
			JText::_('COM_COURSES_SUBMENU_LANGUAGENAME'),
			'index.php?option=com_courses&view=courses',
			$vName == 'courses'
		);
         JSubMenuHelper::addEntry(
			JText::_('COM_COURSES_SUBMENU_COUNTRY'),
			'index.php?option=com_courses&view=countries',
			$vName == 'countries'
		);
        JSubMenuHelper::addEntry(
			JText::_('COM_COURSES_SUBMENU_PARTNERPAGE'),
			'index.php?option=com_courses&view=partners',
			$vName == 'partners'
		);
        JSubMenuHelper::addEntry(
			JText::_('COM_COURSES_SUBMENU_INSTRUCTOR'),
			'index.php?option=com_courses&view=instructors',
			$vName == 'instructors'
		);
        JSubMenuHelper::addEntry(
			JText::_('COM_COURSES_SUBMENU_STUDENTKEY'),
			'index.php?option=com_courses&view=students',
			$vName == 'students'
		);
        JSubMenuHelper::addEntry(
			JText::_('COM_COURSES_SUBMENU_CERTIFICATION'),
			'index.php?option=com_courses&view=certifications',
			$vName == 'Certifications'
		);
		 JSubMenuHelper::addEntry(
			JText::_('COM_COURSES_SUBMENU_EMAILTEMPLATE'),
			'index.php?option=com_courses&view=emailtemplates',
			$vName == 'emailtemplates'
		);
	
	}

	/**
	 * Gets a list of the actions that can be performed.
	 *
	 * @param	int		The category ID.
	 * @return	JObject
	 * @since	1.6
	 */
	public static function getActions($categoryId = 0)
	{
		$user	= JFactory::getUser();
		$result	= new JObject;

		if (empty($categoryId)) {
			$assetName = 'com_courses';
		} else {
			$assetName = 'com_courses.category.'.(int) $categoryId;
		}

		$actions = array(
			'core.admin', 'core.manage', 'core.create', 'core.edit', 'core.edit.own', 'core.edit.state', 'core.delete'
		);

		foreach ($actions as $action) {
			$result->set($action,	$user->authorise($action, $assetName));
		}

		return $result;
	}
}
?>
