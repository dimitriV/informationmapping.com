<?php
/**
 * @version     $Id: courses.php  2011-12-20 04:13:25Z $
 * @package     IMI
 * @subpackage  com_courses
 * @author      ITOffshore Nepal
 * @copyright   Copyright (C) 2011 ITOffshore Nepal.
 * @license     ITOffshore Nepal
 */

// no direct access
defined('_JEXEC') or die;
ini_set('memory_limit','256M');
// Access check.
if (!JFactory::getUser()->authorise('core.manage', 'com_courses')) {
	return JError::raiseWarning(404, JText::_('JERROR_ALERTNOAUTHOR'));
}
// Include dependancies
jimport('joomla.application.component.controller');
require_once JPATH_ROOT.'/components/com_courses/helpers/download.php';

// handle download request...
if ((JRequest::getCmd('task') == 'downloadpdf') && (JRequest::getCmd('filename'))) {
	$filename = JRequest::getCmd('filename');
	$fullPath = 'coursecertificate/'. $filename;
	downloadCertificateHelper::downloadPdf($fullPath);
	exit;
}

$controller	= JControllerLegacy::getInstance('Courses');
$controller->execute(JFactory::getApplication()->input->get('task'));
$controller->redirect();
