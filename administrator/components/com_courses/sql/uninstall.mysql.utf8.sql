DROP TABLE IF EXISTS `#__course_courses`;
DROP TABLE IF EXISTS `#__course_courselanguages`;
DROP TABLE IF EXISTS `#__course_studentkeys`;
DROP TABLE IF EXISTS `#__course_partners`;
DROP TABLE IF EXISTS `#__course_instructors`;
DROP TABLE IF EXISTS `#__course_countries`;
DROP TABLE IF EXISTS `#__course_certificates`;
DROP TABLE IF EXISTS `#__course_emailtemplates`;