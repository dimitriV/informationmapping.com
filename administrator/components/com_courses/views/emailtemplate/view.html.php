<?php
/**
 * @version     $Id:view.html.php  2011-12-20 04:13:25Z $
 * @package     IMI 
 * @subpackage  com_courses
 * @author      ITOffshore Nepal
 * @copyright   Copyright (C) 2011 ITOffshore Nepal.
 * @license     ITOffshore Nepal
 */

// No direct access
defined('_JEXEC') or die;

jimport('joomla.application.component.view');

/**
 * View to edit a course.
 *
 * @package		Joomla.Administrator
 * @subpackage	com_courses
 * @since		1.5
 */
class CoursesViewEmailTemplate extends JViewLegacy
{
	protected $state;
	protected $item;
	protected $form;

	/**
	 * Display the view
	 */
	public function display($tpl = null)
	{
		$this->state	= $this->get('State');
		$this->item		= $this->get('Item');
		$this->form		= $this->get('Form');

		// Check for errors.
		if (count($errors = $this->get('Errors'))) {
			JError::raiseError(500, implode("\n", $errors));
			return false;
		}

		$this->addToolbar();
		parent::display($tpl);
	}

	/**
	 * Add the page title and toolbar.
	 *
	 * @since	1.6
	 */
	protected function addToolbar()
	{
		JRequest::setVar('hidemainmenu', true);

		JToolBarHelper::title(JText::_('COM_COURSES_MANAGER_EMAILTEMPLATE'), 'courses.png');
		JToolBarHelper::apply('emailtemplate.apply');
		JToolBarHelper::save('emailtemplate.save');
		JToolBarHelper::cancel('emailtemplate.cancel', 'JTOOLBAR_CLOSE');

	}
}
?>
