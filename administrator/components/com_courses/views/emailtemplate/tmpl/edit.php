<?php
/**
 * @version     $Id:edit.php  2011-12-20 04:13:25Z $
 * @package     IMI
 * @subpackage  com_courses
 * @author      ITOffshore Nepal
 * @copyright   Copyright (C) 2011 ITOffshore Nepal.
 * @license     ITOffshore Nepal
 */

// no direct access
defined('_JEXEC') or die;

JHtml::addIncludePath(JPATH_COMPONENT.'/helpers/html');
JHtml::_('behavior.tooltip');
JHtml::_('behavior.formvalidation');
?>
<script type="text/javascript">
    Joomla.submitbutton = function(task)
    {
        if (task == 'emailtemplate.cancel' || document.formvalidator.isValid(document.id('emailtemplate-form'))) {
            <?php echo $this->form->getField('description')->save(); ?>
            Joomla.submitform(task, document.getElementById('emailtemplate-form'));
        }
        else {
            alert('<?php echo $this->escape(JText::_('JGLOBAL_VALIDATION_FORM_FAILED'));?>');
        }
    }
</script>

<form action="<?php echo JRoute::_('index.php?option=com_courses&view=emailtemplate&layout=edit&id='.(int) $this->item->id); ?>" method="post" name="adminForm" id="emailtemplate-form" class="form-validate">
<div class="row-fluid form-horizontal-desktop">
    <div class="span7">
        <fieldset class="adminform">
            <legend><?php echo empty($this->item->id) ? JText::_('COM_COURSES_NEW_EMAILTEMPLATE') : JText::sprintf('COM_COURSES_EDIT_EMAILTEMPLATE', $this->item->id); ?></legend>
        
            <div class="control-label"><?php echo $this->form->getLabel('template_key'); ?></div>
            <div class="control"><?php echo $this->form->getInput('template_key'); ?></div>

            <div class="control-label"><?php echo $this->form->getLabel('template_name'); ?></div>
            <div class="control"><?php echo $this->form->getInput('template_name'); ?></div>

            <div class="control-label"><?php echo $this->form->getLabel('email_subject'); ?></div>
            <div class="control"><?php echo $this->form->getInput('email_subject'); ?></div>
            
            <div class="control-label"><?php echo $this->form->getLabel('id'); ?></div>
            <div class="control"><?php echo $this->form->getInput('id'); ?></div>

        </fieldset>
    </div>

    <div class="span5">
        <fieldset class="panelform">
            <legend><?php echo JText::_('COM_COURSES_EMAIL_KEY') ; ?></legend>

            <div class="control-label">
            <?php
            switch(trim($this->form->getValue('template_key'))) {
                case 'STUDENT_WAITING':
                    echo JText::_('COM_COURSES_EMAIL_TEMPLATEINFO_STUDENT_WAITING');
                    break;
                case 'INSTRUCTOR_WAITING':
                    echo JText::_('COM_COURSES_EMAIL_TEMPLATEINFO_INSTRUCTOR_WAITING');
                    break;
                case 'STUDENT_DENIED':
                    echo JText::_('COM_COURSES_EMAIL_TEMPLATEINFO_STUDENT_DENIED');
                    break;
                case 'INSTRUCTOR_DENIED':
                    echo JText::_('COM_COURSES_EMAIL_TEMPLATEINFO_INSTRUCTOR_DENIED');
                    break;
                case 'STUDENT_APPROVED':
                    echo JText::_('COM_COURSES_EMAIL_TEMPLATEINFO_STUDENT_APPROVED');
                    break;
                case 'INSTRUCTOR_APPROVED':
                    echo JText::_('COM_COURSES_EMAIL_TEMPLATEINFO_INSTRUCTOR_APPROVED');
                    break;
                default:
                    echo JText::_('COM_COURSES_INVALID_EMAIL_TEMPLATEKEY');
                    break;
            }
            ?>
            </div>
        </fieldset>

    </div>
</div>

<div class="row-fluid form-horizontal-desktop">
    <div class="span12">
        <div class="control-label"><?php echo $this->form->getLabel('description'); ?></div>
        <div class="control"><?php echo $this->form->getInput('description'); ?></div>
    </div>
</div>
        
<input type="hidden" name="task" value="" />
<?php echo JHtml::_('form.token'); ?>
</form>
