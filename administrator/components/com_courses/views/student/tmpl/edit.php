<?php
/**
 * @version     $Id:edit.php  2011-12-20 04:13:25Z $
 * @package     IMI 
 * @subpackage  com_courses
 * @author      ITOffshore Nepal
 * @copyright   Copyright (C) 2011 ITOffshore Nepal.
 * @license     ITOffshore Nepal
 */


// no direct access
defined('_JEXEC') or die;

JHtml::addIncludePath(JPATH_COMPONENT.'/helpers/html');
JHtml::_('behavior.tooltip');
JHtml::_('behavior.formvalidation');
?>
<script type="text/javascript">
    Joomla.submitbutton = function(task)
    {
        if (task == 'student.cancel' || document.formvalidator.isValid(document.id('student-form'))) {
            
            Joomla.submitform(task, document.getElementById('student-form'));
        }
        else {
            alert('<?php echo $this->escape(JText::_('JGLOBAL_VALIDATION_FORM_FAILED'));?>');
        }
    }
</script>

<form action="<?php echo JRoute::_('index.php?option=com_courses&view=student&layout=edit&id='.(int) $this->item->id); ?>" method="post" name="adminForm" id="student-form" class="form-validate">
<div class="row-fluid form-horizontal-desktop">
    <div class="span7">
        <fieldset class="adminform">
            <legend><?php echo empty($this->item->id) ? JText::_('COM_COURSES_NEW_STUDENT') : JText::sprintf('COM_COURSES_EDIT_STUDENT', $this->item->id); ?></legend>

            <div class="control-label"><?php echo $this->form->getLabel('studentkey'); ?></div>
            <div class="control"><?php echo $this->form->getInput('studentkey'); ?></div>
            
            <div class="control-label"><?php echo $this->form->getLabel('status'); ?></div>
            <div class="control"><?php echo $this->form->getInput('status'); ?></div>

            <div class="control-label"><?php echo $this->form->getLabel('status_imp'); ?></div>
            <div class="control"><?php echo $this->form->getInput('status_imp'); ?></div>
            
            <div class="control-label"><?php echo $this->form->getLabel('state'); ?></div>
            <div class="control"><?php echo $this->form->getInput('state'); ?></div>

            <div class="control-label"><?php echo $this->form->getLabel('id'); ?></div>
            <div class="control"><?php echo $this->form->getInput('id'); ?></div>

        </fieldset>
    </div>

    <div class="span5">
        <fieldset class="panelform">
            <legend><?php echo JText::_('JGLOBAL_FIELDSET_PUBLISHING') ; ?></legend>

            <div class="control-label"><?php echo $this->form->getLabel('created_by'); ?></div>
            <div class="control"><?php echo $this->form->getInput('created_by'); ?></div>

            <div class="control-label"><?php echo $this->form->getLabel('created_by_alias'); ?></div>
            <div class="control"><?php echo $this->form->getInput('created_by_alias'); ?></div>

            <div class="control-label"><?php echo $this->form->getLabel('created'); ?></div>
            <div class="control"><?php echo $this->form->getInput('created'); ?></div>

            <div class="control-label"><?php echo $this->form->getLabel('publish_up'); ?></div>
            <div class="control"><?php echo $this->form->getInput('publish_up'); ?></div>

            <div class="control-label"><?php echo $this->form->getLabel('publish_down'); ?></div>
            <div class="control"><?php echo $this->form->getInput('publish_down'); ?></div>

            <?php if ($this->item->modified_by) : ?>
            <div class="control-label"><?php echo $this->form->getLabel('modified_by'); ?></div>
            <div class="control"><?php echo $this->form->getInput('modified_by'); ?></div>

            <div class="control-label"><?php echo $this->form->getLabel('modified'); ?></div>
            <div class="control"><?php echo $this->form->getInput('modified'); ?></div>
            <?php endif; ?>

        </fieldset>

        <input type="hidden" name="task" value="" />
        <?php echo JHtml::_('form.token'); ?>
    </div>
</div>
</form>
