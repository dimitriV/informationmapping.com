<?php
/**
 * @version     $Id:default.php  2011-12-20 04:13:25Z $
 * @package     IMI
 * @subpackage  com_courses
 * @author      ITOffshore Nepal
 * @copyright   Copyright (C) 2011 ITOffshore Nepal.
 * @license     ITOffshore Nepal
 */
// no direct access
defined('_JEXEC') or die;
defined('_JEXEC') or die;
JHtml::addIncludePath(JPATH_COMPONENT . '/helpers/html');
JHtml::_('behavior.tooltip');
JHtml::_('behavior.formvalidation');

$doc = JFactory::getDocument();
$doc->addScript('components/com_courses/js/jquery-1.7.1.js', 'text/javascript');
?>
<script type="text/javascript">

    var status = new Array();

    status[0] = '<?php echo JText::_('COM_COURSES_PENDING'); ?>';
    status[1] = '<?php echo JText::_('COM_COURSES_APPROVED'); ?>';
    status[-1] = '<?php echo JText::_('COM_COURSES_DENIED'); ?>';


    Joomla.submitbutton = function(task)
    {
        current_status = jQuery('#jform_status').find("option:selected").val();

        // detect status change..
        if((current_status!=previous_status) && (task == 'certification.apply')) {
            msg = '<?php echo JText::_('COM_COURSES_CHANGE_STATUS_FROM'); ?>'+ status[previous_status] + '<?php echo JText::_('COM_COURSES_TO'); ?>' + status[current_status]+'?';
            if(confirm(msg)) {
                Joomla.submitform(task, document.getElementById('certification-form'));
            }
        } else {
            Joomla.submitform(task, document.getElementById('certification-form'));
        }


    }
</script>
<form method="post" name="adminForm" id="certification-form" action="<?php echo JRoute::_('index.php?option=com_courses&view=certification&layout=edit&id=' . (int) $this->form->getValue('id')); ?>" >
<div class="row-fluid form-horizontal-desktop">
    <div class="span7">
        <fieldset class="adminform">
            <legend><?php echo JText::_('COM_COURSES_FORM_DETAIL'); ?></legend>

            <div class="control-label"><?php echo $this->form->getLabel('first_name'); ?></div>
            <div class="control"><?php echo $this->form->getInput('first_name'); ?></div>

            <div class="control-label"><?php echo $this->form->getLabel('last_name'); ?></div>
            <div class="control"><?php echo $this->form->getInput('last_name'); ?></div>

            <div class="control-label"><?php echo $this->form->getLabel('studentemail'); ?></div>
            <div class="control"><?php echo $this->form->getInput('studentemail'); ?></div>

            <div class="control-label"><?php echo $this->form->getLabel('username'); ?></div>
            <div class="control"><?php echo $this->form->getInput('username'); ?></div>

            <div class="control-label"><?php echo $this->form->getLabel('course_language_id'); ?></div>
            <div class="control"><?php echo $this->form->getInput('course_language_id'); ?></div>

            <div class="control-label"><?php echo $this->form->getLabel('course_id'); ?></div>
            <div class="control"><?php echo $this->form->getInput('course_id'); ?></div>

            <div class="control-label"><?php echo $this->form->getLabel('partner_id'); ?></div>
            <div class="control"><?php echo $this->form->getInput('partner_id'); ?></div>

            <div class="control-label"><?php echo $this->form->getLabel('instructor_id'); ?></div>
            <div class="control"><?php echo $this->form->getInput('instructor_id'); ?></div>

            <div class="control-label" id="instructor_name" style="display:none"><?php echo $this->form->getLabel('instructor_name');
            ?></div>
            <div class="control"><?php echo $this->form->getInput('instructor_name'); ?></div>

            <div class="control-label"><?php echo $this->form->getLabel('student_key'); ?></div>
            <div class="control"><?php echo $this->form->getInput('student_key'); ?></div>

            <div class="control-label"><?php echo $this->form->getLabel('date_from'); ?></div>
            <div class="control"><?php echo $this->form->getInput('date_from'); ?></div>

            <div class="control-label"><?php echo $this->form->getLabel('date_to'); ?></div>
            <div class="control"><?php echo $this->form->getInput('date_to'); ?></div>

            <div class="control-label"><?php echo $this->form->getLabel('created'); ?></div>
            <div class="control"><?php echo $this->form->getInput('created'); ?></div>

            <div class="control-label"><?php echo $this->form->getLabel('status'); ?></div>
            <div class="control"><?php echo $this->form->getInput('status'); ?></div>

            <?php if ($this->form->getValue('filename')) : ?>
            <div class="control-label"><?php echo $this->form->getLabel('filename'); ?></div>
            <div class="control">
                <!--<a href="../coursecertificate/<?php echo $this->form->getValue('filename'); ?>" title="<?php echo JText::_('CLICK TO DOWNLOAD CERTIFICATE'); ?>">-->
                <a href="index.php?option=com_courses&task=downloadpdf&filename=<?php echo $this->form->getValue('filename'); ?>" title="<?php echo JText::_('CLICK TO DOWNLOAD CERTIFICATE'); ?>">
                    <img src="./components/com_courses/style/images/download.png" />
                </a>
            </div>
            <?php endif; ?>

            <?php echo $this->form->getInput('uniqid'); ?>

        </fieldset>
    </div>
</div>
<input type="hidden" name="task" value="" />
<?php echo JHtml::_('form.token'); ?>
</form>

<script type="text/javascript">
    jQuery(document).ready(function(){
        // track previous state.. to detect the state change
        previous_status = jQuery('#jform_status').find("option:selected").val();

        // load course name
        courselangugeid = jQuery('#jform_course_language_id').find("option:selected").val();
        course_id =  jQuery('#jform_course_id').find("option:selected").val();
        jQuery.ajax({
            url: "index.php?option=com_courses&task=populateCoursename",
            data: { courselangugeid: courselangugeid, course_id: course_id },
            success: function(html) {
                jQuery('#jform_course_id').html(html);
            }
        });

        // if instructor is of type other then show instructor name
        instructor_id= jQuery('#jform_instructor_id').find("option:selected").val();

        if(instructor_id==0) {
            jQuery('#instructor_name').show();
        }

        jQuery('#jform_instructor_id').change(function(){
            instructor_id = jQuery('#jform_instructor_id').find("option:selected").val();

            if(instructor_id == 'other') {
                jQuery('#instructor_name').show();
            } else {
                jQuery('#jform_instructor_name').val('');
                jQuery('#instructor_name').hide();
            }


        })

        // load selected instructor for first time
        partnerid = jQuery('#jform_partner_id').find("option:selected").val();
        jQuery.ajax({
            url: "index.php?option=com_courses&task=populateInstructor",
            data: { partnerid: partnerid, instructor_id: instructor_id },
            success: function(html) {
                jQuery('#jform_instructor_id').html(html);
            }
        });

        // re populate course name on course language change
        jQuery('#jform_course_language_id').change(function(){
            courselangugeid = jQuery('#jform_course_language_id').find("option:selected").val();
            jQuery.ajax({
                url: "index.php?option=com_courses&task=populateCoursename",
                data: "courselangugeid="+courselangugeid,
                success: function(html) {
                    jQuery('#jform_course_id').html(html);
                }
            });
        })

        // re populate instructor on course partner change
        jQuery('#jform_partner_id').change(function(){
            partnerid = jQuery('#jform_partner_id').find("option:selected").val();

            jQuery('#jform_instructor_name').val('');
            jQuery('#instructor_name').hide();

            jQuery.ajax({
                url: "index.php?option=com_courses&task=populateInstructor",
                data: "partnerid="+partnerid,
                success: function(html) {
                    jQuery('#jform_instructor_id').html(html);
                }
            });
        })
    });
</script>
