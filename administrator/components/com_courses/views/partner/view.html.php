<?php
/**
 * @version     $Id:view.html.php  2011-12-20 04:13:25Z $
 * @package     IMI 
 * @subpackage  com_courses
 * @author      ITOffshore Nepal
 * @copyright   Copyright (C) 2011 ITOffshore Nepal.
 * @license     ITOffshore Nepal
 */


// No direct access
defined('_JEXEC') or die;

jimport('joomla.application.component.view');

/**
 * View to edit a sliderbanner.
 *
 * @package		Joomla.Administrator
 * @subpackage	com_sliderbanners
 * @since		1.5
 */
class CoursesViewPartner extends JViewLegacy
{
	protected $state;
	protected $item;
	protected $form;

	/**
	 * Display the view
	 */
	public function display($tpl = null)
	{
		$this->state	= $this->get('State');
		$this->item		= $this->get('Item');
		$this->form		= $this->get('Form');

		// Check for errors.
		if (count($errors = $this->get('Errors'))) {
			JError::raiseError(500, implode("\n", $errors));
			return false;
		}

		$this->addToolbar();
		parent::display($tpl);
	}

	/**
	 * Add the page title and toolbar.
	 *
	 * @since	1.6
	 */
	protected function addToolbar()
	{
		JRequest::setVar('hidemainmenu', true);

		$user		= JFactory::getUser();
		$isNew		= ($this->item->id == 0);
		$checkedOut	= !($this->item->checked_out == 0 || $this->item->checked_out == $user->get('id'));
		$canDo		= CoursesHelper::getActions($this->state->get('filter.category_id'), $this->item->id);

		JToolBarHelper::title(JText::_('COM_COURSES_MANAGER_PARTNER'), 'courses.png');

		// If not checked out, can save the item.
		if (!$checkedOut && ($canDo->get('core.edit')))
		{
			JToolBarHelper::apply('partner.apply');
			JToolBarHelper::save('partner.save');
		}
		if (!$checkedOut){
			JToolBarHelper::save2new('partner.save2new');
		}
		// If an existing item, can save to a copy.
		if (!$isNew) {
			JToolBarHelper::save2copy('partner.save2copy');
		}
		if (empty($this->item->id)) {
			JToolBarHelper::cancel('partner.cancel');
		}
		else {
			JToolBarHelper::cancel('partner.cancel', 'JTOOLBAR_CLOSE');
		}

	}
}
?>
