<?php
/**
 * @version     $Id:edit.php  2011-12-20 04:13:25Z $
 * @package     IMI 
 * @subpackage  com_courses
 * @author      ITOffshore Nepal
 * @copyright   Copyright (C) 2011 ITOffshore Nepal.
 * @license     ITOffshore Nepal
 */

// no direct access
defined('_JEXEC') or die;

JHtml::addIncludePath(JPATH_COMPONENT.'/helpers/html');
JHtml::_('behavior.tooltip');
JHtml::_('behavior.formvalidation');
?>
<script type="text/javascript">
    Joomla.submitbutton = function(task)
    {
        if (task == 'partner.cancel' || document.formvalidator.isValid(document.id('partner-form'))) {
            <?php echo $this->form->getField('description')->save(); ?>
            Joomla.submitform(task, document.getElementById('partner-form'));
        }
        else {
            alert('<?php echo $this->escape(JText::_('JGLOBAL_VALIDATION_FORM_FAILED'));?>');
        }
    }
</script>

<form action="<?php echo JRoute::_('index.php?option=com_courses&view=partner&layout=edit&id='.(int) $this->item->id); ?>" method="post" name="adminForm" enctype="multipart/form-data" id="partner-form" class="form-validate">
<div class="row-fluid form-horizontal-desktop">
    <div class="span7">
        <fieldset class="adminform">
            <legend><?php echo empty($this->item->id) ? JText::_('COM_COURSES_NEW_PARTNER') : JText::sprintf('COM_COURSES_EDIT_PARTNER', $this->item->id); ?></legend>
            
            <div class="control-label"><?php echo $this->form->getLabel('countryid'); ?>
            <div class="control"><?php echo $this->form->getInput('countryid'); ?></div>
            
            <div class="control-label"><?php echo $this->form->getLabel('title'); ?>
            <div class="control"><?php echo $this->form->getInput('title'); ?></div>

            <div class="control-label"><?php echo $this->form->getLabel('filename'); ?>
            <div class="control"><?php echo $this->form->getInput('filename'); ?> <span style="float:left;width: 300px;padding: 10px 0 0 5px;"><?php echo JText::_('COM_COURSES_BEST_DIMENSIONS_WIDTH');?></span></div>
                
            <?php $file = JPATH_ROOT.'/images/partners/'.$this->item->filename; ?>
            <?php if(file_exists($file)) : ?>
            <div class="control-label">
                <label><img src="<?php echo JURI::root().'images/partners/'.$this->item->filename;?>" height="40" width="40"/></label>
            </div>
            <?php else: ?>
            <div class="control-label"><label><?php echo $this->item->filename;?></label></div>
            <?php endif; ?>
                  
            <div class="control-label"><?php echo $this->form->getLabel('address'); ?></div>
            <div class="control"><?php echo $this->form->getInput('address'); ?></div>
            
            <div class="control-label"><?php echo $this->form->getLabel('city'); ?></div>
            <div class="control"><?php echo $this->form->getInput('city'); ?></div>
            
            <div class="control-label"><?php echo $this->form->getLabel('website'); ?></div>
            <div class="control"><?php echo $this->form->getInput('website'); ?></div>
            
            <div class="control-label"><?php echo $this->form->getLabel('email'); ?></div>
            <div class="control"><?php echo $this->form->getInput('email'); ?></div>
            
            <div class="control-label"><?php echo $this->form->getLabel('phone'); ?></div>
            <div class="control"><?php echo $this->form->getInput('phone'); ?></div>
            
            <div class="control-label"><?php echo $this->form->getLabel('fax'); ?></div>
            <div class="control"><?php echo $this->form->getInput('fax'); ?></div>
                
            <div class="control-label"><?php echo $this->form->getLabel('language'); ?></div>
            <div class="control"><?php echo $this->form->getInput('language'); ?></div>

            <div class="control-label"><?php echo $this->form->getLabel('id'); ?></div>
            <div class="control"><?php echo $this->form->getInput('id'); ?></div>

        </fieldset>
    </div>
    <div class="span5">
        <fieldset class="panelform">
            <legend><?php echo JText::_('JGLOBAL_FIELDSET_PUBLISHING') ; ?></legend>

            <div class="control-label"><?php echo $this->form->getLabel('state'); ?></div>
            <div class="control"><?php echo $this->form->getInput('state'); ?></div>

            <div class="control-label"><?php echo $this->form->getLabel('access'); ?></div>
            <div class="control"><?php echo $this->form->getInput('access'); ?></div>

            <div class="control-label"><?php echo $this->form->getLabel('ordering'); ?></div>
            <div class="control"><?php echo $this->form->getInput('ordering'); ?></div>

            <div class="control-label"><?php echo $this->form->getLabel('created_by'); ?></div>
            <div class="control"><?php echo $this->form->getInput('created_by'); ?></div>

            <div class="control-label"><?php echo $this->form->getLabel('created_by_alias'); ?></div>
            <div class="control"><?php echo $this->form->getInput('created_by_alias'); ?></div>

            <div class="control-label"><?php echo $this->form->getLabel('created'); ?></div>
            <div class="control"><?php echo $this->form->getInput('created'); ?></div>

            <div class="control-label"><?php echo $this->form->getLabel('publish_up'); ?></div>
            <div class="control"><?php echo $this->form->getInput('publish_up'); ?></div>

            <div class="control-label"><?php echo $this->form->getLabel('publish_down'); ?></div>
            <div class="control"><?php echo $this->form->getInput('publish_down'); ?></div>

            <?php if ($this->item->modified_by) : ?>
            <div class="control-label"><?php echo $this->form->getLabel('modified_by'); ?></div>
            <div class="control"><?php echo $this->form->getInput('modified_by'); ?></div>

            <div class="control-label"><?php echo $this->form->getLabel('modified'); ?></div>
            <div class="control"><?php echo $this->form->getInput('modified'); ?></div>
            <?php endif; ?>
        </fieldset>

        <input type="hidden" name="task" value="" />
        <?php echo JHtml::_('form.token'); ?>
    </div>
</div>

<div class="row-fluid form-horizontal-desktop">
    <div class="span12">
            <div class="control-label"><?php echo $this->form->getLabel('description'); ?></div>
            <div class="control"><?php echo $this->form->getInput('description'); ?></div>

    </div>
</div>

</form>
