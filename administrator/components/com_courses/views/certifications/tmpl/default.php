<?php
/**
 * @version     $Id:view.html.php  2011-12-20 04:13:25Z $
 * @package     IMI
 * @subpackage  com_courses
 * @author      ITOffshore Nepal
 * @copyright   Copyright (C) 2011 ITOffshore Nepal.
 * @license     ITOffshore Nepal
 */
// no direct access
defined('_JEXEC') or die;

JHtml::addIncludePath(JPATH_COMPONENT . '/helpers/html');
JHtml::_('bootstrap.tooltip');
//JHtml::_('behavior.tooltip');
JHtml::_('behavior.multiselect');
JHtml::_('formbehavior.chosen', 'select');

$app		= JFactory::getApplication();
$user = JFactory::getUser();
$userId = $user->get('id');
$listOrder = $this->escape($this->state->get('list.ordering'));
$listDirn = $this->escape($this->state->get('list.direction'));
$canOrder    = $user->authorise('core.edit.state', 'com_courses');
$saveOrder    = $listOrder == 'a.ordering';

if ($saveOrder)
{
    $saveOrderingUrl = 'index.php?option=com_courses&task=certifications.saveOrderAjax&tmpl=component';
    JHtml::_('sortablelist.sortable', 'itemList', 'adminForm', strtolower($listDirn), $saveOrderingUrl);
}
?>
<script type="text/javascript">
    Joomla.orderTable = function()
    {
        table = document.getElementById("sortTable");
        direction = document.getElementById("directionTable");
        order = table.options[table.selectedIndex].value;
        if (order != '<?php echo $listOrder; ?>')
        {
            dirn = 'asc';
        }
        else
        {
            dirn = direction.options[direction.selectedIndex].value;
        }
        Joomla.tableOrdering(order, dirn, '');
    }
</script>

<form action="<?php echo JRoute::_('index.php?option=com_courses&view=certifications'); ?>" method="post" name="adminForm" id="adminForm">
    <?php if (!empty( $this->sidebar)) : ?>
    <div id="j-sidebar-container" class="span2">
        <?php echo $this->sidebar; ?>
    </div>
    <div id="j-main-container" class="span10">
        <?php else : ?>
        <div id="j-main-container">
            <?php endif;?>
    <div id="filter-bar" class="btn-toolbar">
        <div class="filter-search btn-group pull-left">
            <label for="filter_search" class="element-invisible"><?php echo JText::_('COM_CONTACT_FILTER_SEARCH_DESC');?></label>
            <input type="text" name="filter_search" id="filter_search" placeholder="<?php echo JText::_('JSEARCH_FILTER'); ?>" value="<?php echo $this->escape($this->state->get('filter.search')); ?>" class="hasTooltip" title="<?php echo JHtml::tooltipText('COM_CONTACT_SEARCH_IN_NAME'); ?>" />
        </div>
        <div class="btn-group pull-left">
            <button type="submit" class="btn hasTooltip" title="<?php echo JHtml::tooltipText('JSEARCH_FILTER_SUBMIT'); ?>"><i class="icon-search"></i></button>
            <button type="button" class="btn hasTooltip" title="<?php echo JHtml::tooltipText('JSEARCH_FILTER_CLEAR'); ?>" onclick="document.id('filter_search').value='';this.form.submit();"><i class="icon-remove"></i></button>
        </div>
        <div class="btn-group pull-right hidden-phone">
            <label for="limit" class="element-invisible"><?php echo JText::_('JFIELD_PLG_SEARCH_SEARCHLIMIT_DESC');?></label>
            <?php echo $this->pagination->getLimitBox(); ?>
        </div>
        <?php
        $statusoption = array();
        $statusoption[] = JHTML::_('select.option', '0', JText::_('COM_COURSES_PENDING'));
        $statusoption[] = JHTML::_('select.option', '1', JText::_('COM_COURSES_APPROVED'));
        $statusoption[] = JHTML::_('select.option', '-1', JText::_('COM_COURSES_DENIED'));
        ?>
        <div class="btn-group pull-right hidden-phone">
        <select name="filter_status" class="inputbox" onchange="this.form.submit()">
            <option value=""><?php echo JText::_('COM_COURSES_SELECT_STATUS'); ?></option>
            <?php echo JHtml::_('select.options', $statusoption, 'value', 'text', $this->state->get('filter.status')); ?>
        </select>
    </div>
    </div>
    <div class="clearfix"> </div>
    <table class="table table-striped" id="itemList">
        <thead>
            <tr>
                <th width="1%" class="nowrap center hidden-phone">
                    <?php echo JHtml::_('searchtools.sort', '', 'a.ordering', $listDirn, $listOrder, null, 'asc', 'JGRID_HEADING_ORDERING', 'icon-menu-2'); ?>
                </th>
                <th width="1%">
                    <input type="checkbox" name="checkall-toggle" value="" title="<?php echo JText::_('JGLOBAL_CHECK_ALL'); ?>" onclick="Joomla.checkAll(this)" />
                </th>
                <th class="title"  width="10%">
                    <?php echo JHtml::_('grid.sort', 'Name', 'a.first_name', $listDirn, $listOrder); ?>
                </th>

                <th width="5%">
                    <?php echo JHtml::_('grid.sort', 'Course Language', 'cl.language_name', $listDirn, $listOrder); ?>
                </th>
                <th width="5%">
                    <?php echo JHtml::_('grid.sort', 'Course Title', 'c.title', $listDirn, $listOrder); ?>
                </th>
                <th width="5%">
                    <?php echo JHtml::_('grid.sort', 'Partner', 'p.title', $listDirn, $listOrder); ?>
                </th>
                <th width="5%">
                    <?php echo JHtml::_('grid.sort', 'Student Key', 'a.student_key', $listDirn, $listOrder); ?>
                </th>

                <th width="1%" class="nowrap">
                    <?php echo JHtml::_('grid.sort', 'User Email', 'u.email', $listDirn, $listOrder); ?>

                </th>
                <th width="5%">
                    <?php echo JHtml::_('grid.sort', 'Status', 'a.status', $listDirn, $listOrder); ?>
                </th>
                <th width="5%">
                    <?php echo JText::_('COM_COURSES_DOWNLOAD'); ?>
                </th>
                <th width="1%" class="nowrap center hidden-phone">
                    <?php echo JHtml::_('grid.sort', 'JGRID_HEADING_ID', 'a.id', $listDirn, $listOrder); ?>
                </th>
            </tr>
        </thead>
        <tfoot>
            <tr>
                <td colspan="10">
                    <?php echo $this->pagination->getListFooter(); ?>
                </td>
            </tr>
        </tfoot>
        <tbody>


            <?php
            foreach ($this->items as $i => $item) :
                $ordering    = ($listOrder == 'a.ordering');
                $canCheckin    = $user->authorise('core.manage',        'com_checkin') || $item->checked_out==$user->get('id') || $item->checked_out==0;
                $canChange    = $user->authorise('core.edit.state',    'com_courses'.$item->id) && $canCheckin;

                ?>
                <tr class="row<?php echo $i % 2; ?>">
                    <td class="order nowrap center hidden-phone">
                        <?php
                        if (!$canChange)
                        {
                            $iconClass = ' inactive';
                        }
                        elseif (!$saveOrder)
                        {
                            $iconClass = ' inactive tip-top hasTooltip" title="' . JHtml::tooltipText('JORDERINGDISABLED');
                        }
                        ?>
                        <span class="sortable-handler<?php echo $iconClass ?>">
                                <i class="icon-menu"></i>
                            </span>
                        <?php  if ($canChange && $saveOrder) : ?>
                            <input type="text" style="display:none" name="order[]" size="5" value="<?php echo $item->ordering; ?>" class="width-20 text-area-order " />
                        <?php endif; ?>
                    </td>
                    <td class="center">
                        <?php echo JHtml::_('grid.id', $i, $item->id); ?>
                    </td>
                    <td>
                        <?php // if ($item->checked_out) : ?>
                            <?php // echo JHtml::_('jgrid.checkedout', $i, $item->editor, $item->checked_out_time, 'certification.', $canCheckin); ?>
                        <?php //endif; ?>
                        <?php //if ($canEdit) : ?>
                        <a href="<?php echo JRoute::_('index.php?option=com_courses&view=certification&task=certification.edit&id='.(int) $item->id); ?>">
                            <?php echo $this->escape($item->first_name) . ' ' . $this->escape($item->last_name); ?>
                        </a>
                    </td>


                    <td class="order">
                        <?php echo $this->escape($item->language_name); ?>
                    </td>
                    <td class="left nowrap">
                        <?php echo $this->escape($item->course_title); ?>
                    </td>
                    <td class="left">
                        <?php echo $this->escape($item->partner_title); ?>
                    </td>
                    <td class="left">
                        <?php echo $this->escape($item->student_key); ?>
                    </td>
                    <td class="left">
                        <?php echo $item->user_email; ?>
                    </td>
                    <td class="left">
                        <?php
                        $status = $this->escape($item->status);
                        if ($status === '0') {
                            echo JText::_('COM_COURSES_PENDING');
                        } elseif ($status === '1') {
                            echo JText::_('COM_COURSES_APPROVED');
                        } elseif ($status == '-1') {
                            echo JText::_('COM_COURSES_DENIED');
                        } else {
                            echo JText::_('COM_COURSES_UNKNOWN');
                        }
                        ?>
                    </td>
                    <td class="center">
                        <?php if ($item->filename) : ?>
                            <!--<a href="../coursecertificate/<?php echo $item->filename; ?>" title="<?php echo JText::_('CLICK TO DOWNLOAD CERTIFICATE'); ?>">-->
                            <a href="index.php?option=com_courses&task=downloadpdf&filename=<?php echo $item->filename; ?>" title="<?php echo JText::_('CLICK TO DOWNLOAD CERTIFICATE'); ?>">
                                <img src="./components/com_courses/style/images/download.png" />
                            </a>
                        <?php endif; ?>
                    </td>
                    <td align="center hidden-phone">
                        <?php echo $item->id; ?>
                    </td>
                </tr>
            <?php endforeach; ?>
        </tbody>
    </table>


    <div>
        <input type="hidden" name="task" value="" />
        <input type="hidden" name="boxchecked" value="0" />
        <input type="hidden" name="filter_order" value="<?php echo $listOrder; ?>" />
        <input type="hidden" name="filter_order_Dir" value="<?php echo $listDirn; ?>" />
        <?php echo JHtml::_('form.token'); ?>
    </div>
</form>
