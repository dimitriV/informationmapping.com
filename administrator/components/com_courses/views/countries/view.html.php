<?php
/**
 * @version     $Id:view.html.php  2011-12-20 04:13:25Z $
 * @package     IMI 
 * @subpackage  com_courses
 * @author      ITOffshore Nepal
 * @copyright   Copyright (C) 2011 ITOffshore Nepal.
 * @license     ITOffshore Nepal
 */

// No direct access
defined('_JEXEC') or die;

/**
 * View class for a list of courses.
 *
 * @package		Joomla.Administrator
 * @subpackage	com_courses
 * @since		1.5
 */
class CoursesViewCountries extends JViewLegacy
{
	protected $items;
	protected $pagination;
	protected $state;

	/**
	 * Display the view
	 */
	public function display($tpl = null)
	{
		$this->state		= $this->get('State');
		$this->items		= $this->get('Items');
		$this->pagination	= $this->get('Pagination');

		// Check for errors.
		if (count($errors = $this->get('Errors'))) {
			JError::raiseError(500, implode("\n", $errors));
			return false;
		}

		$this->addToolbar();
		parent::display($tpl);
	}

	/**
	 * Add the page title and toolbar.
	 *
	 * @since	1.6
	 */
	protected function addToolbar()
	{
		require_once JPATH_COMPONENT.'/helpers/courses.php';

		$state	= $this->get('State');
		$canDo	= CoursesHelper::getActions($state->get('filter.category_id'));
		$user	= JFactory::getUser();

			JToolBarHelper::title(JText::_('COM_COURSES_MANAGER_COUNTRIES'), 'courses.png');
			JToolBarHelper::addNew('country.add');
	
		if ($canDo->get('core.edit')) {
			JToolBarHelper::editList('country.edit');
		}
		if ($canDo->get('core.edit.state')) {

			JToolBarHelper::divider();
			JToolBarHelper::publish('countries.publish', 'JTOOLBAR_PUBLISH', true);
			JToolBarHelper::unpublish('countries.unpublish', 'JTOOLBAR_UNPUBLISH', true);
			JToolBarHelper::divider();
			JToolBarHelper::archiveList('countries.archive');
			JToolBarHelper::checkin('countries.checkin');
			
		}
		if ($state->get('filter.state') == -2 && $canDo->get('core.delete')) {
			JToolBarHelper::deleteList('', 'countries.delete', 'JTOOLBAR_EMPTY_TRASH');
			JToolBarHelper::divider();
			
		} else if ($canDo->get('core.edit.state')) {
			JToolBarHelper::trash('countries.trash');
			JToolBarHelper::divider();
		}
		
	}
}
?>
