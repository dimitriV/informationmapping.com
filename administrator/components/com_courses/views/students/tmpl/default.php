<?php
/**
 * @version     $Id:default.php  2011-12-20 04:13:25Z $
 * @package     IMI 
 * @subpackage  com_courses
 * @author      ITOffshore Nepal
 * @copyright   Copyright (C) 2011 ITOffshore Nepal.
 * @license     ITOffshore Nepal
 */


// no direct access
defined('_JEXEC') or die;

JHtml::addIncludePath(JPATH_COMPONENT.'/helpers/html');
JHtml::_('behavior.tooltip');
JHtml::_('behavior.multiselect');

$user		= JFactory::getUser();
$userId		= $user->get('id');

?>
<form action="<?php echo JRoute::_('index.php?option=com_courses&view=students'); ?>" method="post" name="adminForm" id="adminForm">
	<fieldset id="filter-bar" style="height:120px">
		<div class="filter-search fltlft">
			<label class="filter-search-lbl" for="filter_search"><?php echo JText::_('JSEARCH_FILTER_LABEL'); ?></label>
			<input type="text" name="filter_search" id="filter_search" value="<?php echo $this->escape($this->state->get('filter.search')); ?>" title="<?php echo JText::_('COM_COURSES_SEARCH_IN_TITLE'); ?>" />
			<button type="submit"><?php echo JText::_('JSEARCH_FILTER_SUBMIT'); ?></button>
			<button type="button" onclick="document.id('filter_search').value='';this.form.submit();"><?php echo JText::_('JSEARCH_FILTER_CLEAR'); ?></button>
		</div>
		<div class="filter-select fltrt">
		
			<select class="inputbox" onchange="this.form.submit()" name="filter_status">
				
				<option selected= "selected" value=""><?php echo JText::_('COM_COURSES_SELECT_AVILABILITY');?></option>
			
				<option <?php if($this->state->get('filter.status')== 1){?>
						selected ="selected"
			<?php }?> value="1">Used</option>
				<option <?php if($this->state->get('filter.status')== '0'){?>
						selected ="selected"
			<?php }?> value="0">Not Used</option>
			</select>
	
			<select name="filter_published" class="inputbox" onchange="this.form.submit()">
				<option value=""><?php echo JText::_('JOPTION_SELECT_PUBLISHED');?></option>
				<?php echo JHtml::_('select.options', JHtml::_('jgrid.publishedOptions'), 'value', 'text', $this->state->get('filter.state'), true);?>
			</select>

		</div>
	</fieldset>
	<div class="clr"> </div>

	<table class="table table-striped">
		<thead>
			<tr>
				<th width="1%">
					<input type="checkbox" name="checkall-toggle" value="" title="<?php echo JText::_('JGLOBAL_CHECK_ALL'); ?>" onclick="Joomla.checkAll(this)" />
				</th>
				<th class="title"  width="25%">
					<?php echo JText::_('COM_COURSES_STUDENTKEY'); ?>
				</th>
					<th width="8%">
					<?php echo JText::_('COM_COURSES_AVAILABILITY'); ?>
				</th>
					<th width="8%">
					<?php echo JText::_('COM_COURSES_IMP_AVAILABILITY'); ?>
				</th>

				<th width="5%">
					<?php echo JText::_('COM_COURSES_STATUS'); ?>
				</th>

				<th width="1%" class="nowrap">
					<?php echo JText::_('JGRID_HEADING_ID'); ?>
				</th>
			</tr>
		</thead>
		<tfoot>
			<tr>
				<td colspan="10">
					<?php echo $this->pagination->getListFooter(); ?>
				</td>
			</tr>
		</tfoot>
		<tbody>

		<?php foreach ($this->items as $i => $item) :
		
			$canCheckin	= $user->authorise('core.manage',		'com_checkin') || $item->checked_out==$user->get('id') || $item->checked_out==0;
			$canChange	= $user->authorise('core.edit.state',	'com_courses'.$item->id) && $canCheckin;
			?>
			<tr class="row<?php echo $i % 2; ?>">
				<td class="center">
					<?php echo JHtml::_('grid.id', $i, $item->id); ?>
				</td>
				
				<td>
					<?php // if ($item->checked_out) : ?>
						<?php // echo JHtml::_('jgrid.checkedout', $i, $item->editor, $item->checked_out_time, 'students.', $canCheckin); ?>
					<?php // endif; ?>
					<?php //if ($canEdit) : ?>
						<a href="<?php echo JRoute::_('index.php?option=com_courses&view=student&task=student.edit&id='.(int) $item->id); ?>">
							<?php echo $this->escape($item->studentkey); ?></a>

				</td>
				<td class="left"> 
					<?php $availability= $this->escape($item->status);
					if($availability == 1){
						echo JText::_('COM_COURSES_USED');
					}
					else{
						echo JText::_('COM_COURSES_NOT_USED');
					}
					 ?></a>
				</td>
				<td class="left">
					<?php $availability= $this->escape($item->status_imp);
					if($availability == 1){
						echo JText::_('COM_COURSES_USED');
					}
					else{
						echo JText::_('COM_COURSES_NOT_USED');
					}
					 ?></a>
				</td>
				<td class="center">
					<?php echo JHtml::_('jgrid.published', $item->state, $i, 'students.', $canChange, 'cb', $item->publish_up, $item->publish_down); ?>
				</td>

				<td class="center">
					<?php echo (int) $item->id; ?>
				</td>
			</tr>
			<?php endforeach; ?>
		</tbody>
	</table>


	<div>
		<input type="hidden" name="task" value="" />
		<input type="hidden" name="boxchecked" value="0" />
		<input type="hidden" name="filter_order" value="<?php echo $listOrder; ?>" />
		<input type="hidden" name="filter_order_Dir" value="<?php echo $listDirn; ?>" />
		<?php echo JHtml::_('form.token'); ?>
	</div>
</form>
