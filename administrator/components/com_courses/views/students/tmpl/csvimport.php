<?php
/**
 * @version     $Id:csvimport.php  2011-12-20 04:13:25Z $
 * @package     IMI 
 * @subpackage  com_courses
 * @author      ITOffshore Nepal
 * @copyright   Copyright (C) 2011 ITOffshore Nepal.
 * @license     ITOffshore Nepal
 */

defined('_JEXEC') or die('Restricted access');
?>
<form name="imgform" method="post" action="index.php?option=com_courses&task=csvimport&tmpl=component" enctype="multipart/form-data" onSubmit="if(file_upload.value=='') {alert('Choose a file!');return false;}">
	<?php echo JText::_('CSV Import');?><hr>
	<?php echo JText::_('SELECT CSV'); ?> : <input type="file" name="file_upload" size="10" />
	<input name="submit" type="submit" value="Upload" />
</form>