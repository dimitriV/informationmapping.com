<?php

/**
 * @version     $Id:certification.php  2011-12-20 04:13:25Z $
 * @package     IMI
 * @subpackage  com_courses
 * @author      ITOffshore Nepal
 * @copyright   Copyright (C) 2011 ITOffshore Nepal.
 * @license     ITOffshore Nepal
 */
// No direct access
defined('_JEXEC') or die;

jimport('joomla.application.component.controllerform');

/* including pdf generation class */
require_once(JPATH_SITE . DS . 'components' . DS . 'com_courses' . DS . 'tcpdf' . DS . 'config' . DS . 'lang' . DS . 'eng.php');
require_once(JPATH_SITE . DS . 'components' . DS . 'com_courses' . DS . 'tcpdf' . DS . 'tcpdf.php');

/**
 * Certification controller class.
 *
 * @package		Joomla.Administrator
 * @subpackage	com_courses
 * @since		1.6
 */
class CoursesControllerCertification extends JControllerForm
{

	public function close()
	{
		$this->setRedirect('index.php?option=com_courses&view=certifications');
	}

	/**
	 * Overriding postSave Hook for preparing email and pdf certificate
	 * @param JModel $model
	 * @param type $validData
	 */
	protected function postSaveHook(JModel &$model, $validData = array())
	{
		$status = (string) $validData['status'];

		switch ($status) {
			case '1':
				$this->approve($validData['student_key'], $validData['uniqid'], $validData['id']);
				break;
			case '-1':
				$this->disapprove($validData['student_key'], $validData['uniqid'], $validData['id']);
				break;
			default:
				return true;
				break;
		}
	}

	/**
	 * Certificaion disapprove or denied
	 * will be called by the link clicked form the email of the instructor
	 * @param NULL
	 * @return redirect with appropriate msg
	 */
	public function disapprove($studentKey, $filename, $id)
	{
		$model = $this->getModel('certification');

		// change status of student key
		$model->disableUsedStudentKeys($studentKey);

		$result = $model->disapproveCertification($studentKey, $filename);

		if (is_array($result)) {
			// send email to student and instructor
			$instructorEmailTemplate = $model->getEmailTemplate('INSTRUCTOR_DENIED');
			$studentEmailTemplate = $model->getEmailTemplate('STUDENT_DENIED');

			$sendEmailInstructor = $this->_disapproveEmail($instructorEmailTemplate, $result);
			$sendEmailStudent = $this->_disapproveEmail($studentEmailTemplate, $result);

			if (($sendEmailInstructor === true) && ($sendEmailStudent === true)) {
				$msg = JText::_('COM_COURSES_CERTIFICATION_REQUEST_DENIED_SUCESSFULLY');
				$this->setRedirect('index.php?option=com_courses&view=certification&layout=edit&id=' . $id, $msg);
			} else {
				$msg = JText::_('COM_COURSES_CERTIFICATION_REQUEST_DENIED_SUCESSFULLY_BUT_FAILED_TO_SEND_EMAIL_TO_USER_OR_INSTRUCTOR');
				$this->setRedirect('index.php?option=com_courses&view=certification&layout=edit&id=' . $id, $msg, 'error');
			}
		} else {
			$msg = JText::_('COM_COURSES_CERTIFICATION_DENIED_PROCESS_FAILED');
			$this->setRedirect('index.php?option=com_courses&view=certification&layout=edit&id=' . $id, $msg, 'error');
		}
	}

	/**
	 * prepare the disapproval or denay email template with token replacement
	 * @param $emailTemplate, email template created form back end
	 * @param $information, data array requred to prepare email template
	 * @return bool
	 */
	private function _disapproveEmail($emailTemplate, $information)
	{
		/* access component paramter */
		$params = & JComponentHelper::getParams('com_courses');

		/* for disapproval email for both case the sender is defiend form back end */
		$sender = array($params->get('defaultemail'), $params->get('defaultname'));

		/* prepare values to replace token */
		$instructorName = $information['instructorname'] ? $information['instructorname'] : $information['instructor_name'];
		$instructorEmail = $information['instructoremail'] ? $information['instructoremail'] : $params->get('defaultemail');
		$studentName = $information['first_name'] . ' ' . $information['last_name'];
		$studentEmail = $information['studentemail'];
		$submitedDate = date('F j, Y ', strtotime($information['created']));
		$studentKey = $information['student_key'];
		$coursename = $information['title'];
		$dataFrom = $information['start_date'];
		$dateTo = $information['end_date'];

		//preapre recipient
		switch ($emailTemplate['template_key']) {
			case 'STUDENT_DENIED':
				$recipient = array($studentEmail);
				break;
			case 'INSTRUCTOR_DENIED':
				$recipient = array($instructorEmail);
				break;
			default :
				exit('Unknown Template key');
				break;
		}

		/* prepare body */
		$body = $emailTemplate['description'];

		/* token replacment */
		$body = str_replace('[COURSE_NAME]', $coursename, $body);
		$body = str_replace('[COURSE_START_DATE]', $dataFrom, $body);
		$body = str_replace('[COURSE_END_DATE]', $dateTo, $body);


		$body = str_replace('[INSTRUCTOR_NAME]', $instructorName, $body);
		$body = str_replace('[INSTRUCTOR_EMAIL]', $instructorEmail, $body);

		$body = str_replace('[STUDENT_NAME]', $studentName, $body);
		$body = str_replace('[STUDENT_EMAIL]', $studentEmail, $body);

		$body = str_replace('[SUBMITED_DATE]', $submitedDate, $body);
		$body = str_replace('[STUDENT_KEY]', $studentKey, $body);

		// current date..
		$date = & JFactory::getDate();
		$currentDate = $date->toFormat('%A, %d %B %Y');

		$body = str_replace('[DATE]', $currentDate, $body);

		$subject = $emailTemplate['email_subject'];

		$sendEmail = $this->_sendEmail($sender, $recipient, $subject, $body);

		return $sendEmail;
	}

	/**
	 * approve certification request
	 * @param type $studentKey
	 * @param type $filename
	 * @param type $id
	 * note: this method is almost same as front end controller
	 */
	public function approve($studentKey, $filename, $id)
	{
		$model = $this->getModel('certification');
		// change status of student key
		$model->disableUsedStudentKeys($studentKey);

		$result = $model->approveCertification($studentKey, $filename);

		if (is_array($result)) {
			/* generate certificaion on pdf format */
			$filename = $this->_preparePdfTemplate($result);

			// send email to student and instructor
			$instructorEmailTemplate = $model->getEmailTemplate('INSTRUCTOR_APPROVED');
			$studentEmailTemplate = $model->getEmailTemplate('STUDENT_APPROVED');

			$sendEmailInstructor = $this->_prepareApproveEmail($instructorEmailTemplate, $result);
			$sendEmailStudent = $this->_prepareApproveEmail($studentEmailTemplate, $result);

			if (($sendEmailInstructor === true) && ($sendEmailStudent === true)) {
				$msg = JText::_('COM_COURSES_CERTIFICATION_REQUEST_APPROVED_SUCESSFULLY');
				$this->setRedirect('index.php?option=com_courses&view=certification&layout=edit&id=' . $id, $msg);
			} else {
				$msg = JText::_('COM_COURSES_CERTIFICATION_REQUEST_APPROVED_BUT_EMAIL_COULD_NOT_BE_SENT_TO_USER_OR_INSTRUCTOR');
				$this->setRedirect('index.php?option=com_courses&view=certification&layout=edit&id=' . $id, $msg, 'error');
			}
		} else {
			$msg = JText::_('COM_COURSES_CERTIFICATION_REQUEST_APPROVAL_PROCESS_FAILED');
			$this->setRedirect('index.php?option=com_courses&view=certification&layout=edit&id=' . $id, $msg, 'error');
		}
	}

	/**
	 * prepare pdf template
	 * @param type $information
	 * @return type filename or false
	 */
	private function _preparePdfTemplate($information)
	{
		$filename = $information['filename'];
		$instructorName = $information['instructorname'] ? $information['instructorname'] : $information['instructor_name'];

		/* access component paramter */
		//$params = & JComponentHelper::getParams('com_courses');
		//$instructorEmail = $information['instructoremail'] ? $information['instructoremail'] : $params->get('defaultemail');

		$pdfTemplate = array();

		$pdfTemplate['full_name'] = $information['first_name'] . ' ' . $information['last_name'];

		$pdfTemplate['completion_date'] = $information['date_to'];

		$pdfTemplate['instructor_name'] = $instructorName;

		$pdfTemplate['course_name'] = $information['title'];

		return $this->_generatePdf($pdfTemplate, $filename);
	}

	/**
	 * prepare email , when certificaion is approved by instructor
	 * @param $emailTemplate , template created from back end
	 * @param nformation, information required to prepare the complete email template
	 * @return redirect with appropriate msg
	 */
	private function _prepareApproveEmail($emailTemplate, $information)
	{
		/* access component paramter */
		$params = & JComponentHelper::getParams('com_courses');

		/* for disapproval email for both case the sender is defiend form back end */
		$sender = array($params->get('defaultemail'), $params->get('defaultname'));

		/* prepare the values to replace token */
		$attachment = '';

		$instructorName = $information['instructorname'] ? $information['instructorname'] : $information['instructor_name'];
		$instructorEmail = $information['instructoremail'] ? $information['instructoremail'] : $params->get('defaultemail');
		$studentName = $information['first_name'] . ' ' . $information['last_name'];
		$studentEmail = $information['studentemail'];
		$submitedDate = date('F j, Y ', strtotime($information['created']));
		$studentKey = $information['student_key'];

		$coursename = $information['title'];
		$dataFrom = $information['start_date'];
		$dateTo = $information['end_date'];

		/* prepare body */
		$body = $emailTemplate['description'];

		//preapre recipient
		switch ($emailTemplate['template_key']) {
			case 'STUDENT_APPROVED':
				$attachment = JPATH_SITE . DS . 'coursecertificate' . DS . $information['filename'];
				$recipient = array($studentEmail);
				break;
			case 'INSTRUCTOR_APPROVED':
				$recipient = array($instructorEmail);
				break;
			default :
				exit('Unknown Template key');
				break;
		}


		/* token replacment */
		$body = str_replace('[COURSE_NAME]', $coursename, $body);
		$body = str_replace('[COURSE_START_DATE]', $dataFrom, $body);
		$body = str_replace('[COURSE_END_DATE]', $dateTo, $body);

		$body = str_replace('[INSTRUCTOR_NAME]', $instructorName, $body);
		$body = str_replace('[INSTRUCTOR_EMAIL]', $instructorEmail, $body);

		$body = str_replace('[STUDENT_NAME]', $studentName, $body);
		$body = str_replace('[STUDENT_EMAIL]', $studentEmail, $body);

		$body = str_replace('[SUBMITED_DATE]', $submitedDate, $body);
		$body = str_replace('[STUDENT_KEY]', $studentKey, $body);

		// current date..
		$date = & JFactory::getDate();
		$currentDate = $date->toFormat('%A, %d %B %Y');

		$body = str_replace('[DATE]', $currentDate, $body);

		$subject = $emailTemplate['email_subject'];

		$sendEmail = $this->_sendEmail($sender, $recipient, $subject, $body, $attachment);

		return $sendEmail;
	}

	/**
	 * generate pdf
	 * @param type $pdfTemplate
	 * @param type $filename
	 * @return type filename or bool
	 */
	private function _generatePdf($pdfTemplate, $filename)
	{
		// create new PDF document
		$pdf = new TCPDF(PDF_PAGE_ORIENTATION, PDF_UNIT, PDF_PAGE_FORMAT, true, 'UTF-8', false);

		// set document information
		$pdf->SetCreator(PDF_CREATOR);


		// set header and footer fonts
		$pdf->setHeaderFont(Array(PDF_FONT_NAME_MAIN, '', PDF_FONT_SIZE_MAIN));

		// set default monospaced font
		$pdf->SetDefaultMonospacedFont(PDF_FONT_MONOSPACED);

		//set margins
		//$pdf->SetMargins(PDF_MARGIN_LEFT, PDF_MARGIN_TOP, PDF_MARGIN_RIGHT);
		$pdf->SetMargins(0, 0, 0);
		$pdf->SetHeaderMargin(0);
		$pdf->SetFooterMargin(0);

		$pdf->setPrintHeader(false);

		// remove default footer
		$pdf->setPrintFooter(false);

		//set auto page breaks
		$pdf->SetAutoPageBreak(TRUE, PDF_MARGIN_BOTTOM);

		//set image scale factor
		$pdf->setImageScale(PDF_IMAGE_SCALE_RATIO);

		// add a page
		$pdf->AddPage();

		$bMargin = $pdf->getBreakMargin();

		$auto_page_break = $pdf->getAutoPageBreak();

		$pdf->SetAutoPageBreak(false, 0);

		$img_file = JPATH_SITE . '/components/com_courses/tcpdf/images/certificate.png';
		$pdf->Image($img_file, 0, 0, 310, 200, '', '', 'C', true, 200, 'C', false, false, 0, 0);

		$pdf->SetTextColor(0, 171, 230);

		$pdf->SetFont('helveticaB', '', 14);

		$pdf->writeHTMLCell(140, 10, 80, 75, $pdfTemplate['full_name'], 0, 0, false, true, 'C', true);

		$pdf->writeHTMLCell(140, 10, 80, 100, $pdfTemplate['course_name'], 0, 0, false, true, 'C', true);

		$pdf->SetFont('helveticaB', '', 12);
		$pdf->writeHTMLCell(80, 10, 124, 122, $pdfTemplate['completion_date'], 0, 0, false, true, 'L', true);

		$pdf->SetTextColor(31, 51, 105);
		$pdf->writeHTMLCell(140, 10, 122, 174, $pdfTemplate['instructor_name'], 0, 0, false, true, 'C', true);

		$filename = $filename ? $filename : uniqid() . '.pdf';

		$destination = JPATH_SITE . DS . 'coursecertificate' . DS . $filename;

		$createPdf = $pdf->Output($destination, 'F');

		if ($createPdf) {
			return $filename;
		} else {
			return false;
		}
	}

	/**
	 * send email
	 * @param type $recipient, email reciver
	 * @param type $subject, email subject
	 * @param type $body, email content
	 * @param type string, path to file , that should be attached
	 * @return type bool
	 */
	private function _sendEmail($sender, $recipient, $subject, $body, $attachment = '')
	{
		$mailer = & JFactory::getMailer();
		$config = & JFactory::getConfig();

		/* sender and recipient information */
		if (empty($sender)) {
			$sender = array(
				$config->getValue('config.mailfrom'),
				$config->getValue('config.fromname'));
		}

		$mailer->setSender($sender);

		$mailer->addRecipient($recipient);

		/* creating the email */
		$mailer->setSubject($subject);

		/* allow html in email */
		$mailer->isHTML(true);
		$mailer->Encoding = 'base64';
		/* set email body */
		$mailer->setBody($body);

		if ($attachment) {
			//$mailer->addAttachment($attachment);
		}

		/* send email */
		$send = & $mailer->Send();

		if ($send !== true) {
			return false;
		} else {
			return true;
		}
	}

}

?>