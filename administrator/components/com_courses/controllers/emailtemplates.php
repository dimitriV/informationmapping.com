<?php
/**
 * @version     $Id:emailtempaltes.php  2011-12-20 04:13:25Z $
 * @package     IMI 
 * @subpackage  com_courses
 * @author      ITOffshore Nepal
 * @copyright   Copyright (C) 2011 ITOffshore Nepal.
 * @license     ITOffshore Nepal
 */

// No direct access.
defined('_JEXEC') or die;

jimport('joomla.application.component.controlleradmin');

/**
 * EmailTempaltes list controller class.
 *
 * @package		Joomla.Administrator
 * @subpackage	com_courses
 * @since		1.6
 */
class CoursesControllerEmailTemplates extends JControllerAdmin
{
	/**
	 * Proxy for getModel.
	 * @since	1.6
	 */
     
    protected $text_prefix = 'COM_COURSES_EMAILTEMPLATES'; 
    
	public function getModel($name = 'EmailTemplate', $prefix = 'CoursesModel', $config = array('ignore_request' => true))
	{
		$model = parent::getModel($name, $prefix, $config);
		return $model;
	}
}
?>