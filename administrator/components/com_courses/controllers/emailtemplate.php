<?php
/**
 * @version     $Id:emailtemplate.php  2011-12-20 04:13:25Z $
 * @package     IMI 
 * @subpackage  com_courses
 * @author      ITOffshore Nepal
 * @copyright   Copyright (C) 2011 ITOffshore Nepal.
 * @license     ITOffshore Nepal
 */

// No direct access
defined('_JEXEC') or die;

jimport('joomla.application.component.controllerform');

/**
 * EmailTemplate controller class.
 *
 * @package		Joomla.Administrator
 * @subpackage	com_courses
 * @since		1.6
 */
class CoursesControllerEmailTemplate extends JControllerForm
{
	/**
	 * Method override to check if you can add a new record.
	 *
	 * @param	array	$data	An array of input data.
	 * @return	boolean
	 * @since	1.6
	 */
	protected function allowAdd($data = array())
	{
		// Initialise variables.
		$user		= JFactory::getUser();
		$allow		= null;

//
		if ($allow === null) {
			// In the absense of better information, revert to the component permissions.
			return parent::allowAdd($data);
		} else {
			return $allow;
		}
	}

	/**
	 * Method to check if you can add a new record.
	 *
	 * @param	array	$data	An array of input data.
	 * @param	string	$key	The name of the key for the primary key.
	 *
	 * @return	boolean
	 * @since	1.6
	 */
	protected function allowEdit($data = array(), $key = 'id')
	{
		// Initialise variables.
		$recordId	= (int) isset($data[$key]) ? $data[$key] : 0;
	
			return parent::allowEdit($data, $key);
	
	}

}
?>