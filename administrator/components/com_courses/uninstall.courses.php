<?php
/**
 * @version     $Id: uninstall.courses.php  2011-12-20 04:13:25Z $
 * @package     IMI 
 * @subpackage  com_courses
 * @author      ITOffshore Nepal
 * @copyright   Copyright (C) 2011 ITOffshore Nepal.
 * @license     ITOffshore Nepal
 */

// no direct access
defined('_JEXEC') or die('Restricted access');

// Your custom code here

function rmdir_recursive($dir) {
    $files = scandir($dir);
    array_shift($files);    // remove '.' from array
    array_shift($files);    // remove '..' from array
    foreach ($files as $file) {
        $file = $dir . '/' . $file;
        if (is_dir($file)) {
            rmdir_recursive($file);
            @rmdir($file);
        } else {
            @unlink($file);
        }
    }
    @rmdir($dir);
}
$dir = JPATH_ROOT.'/images/partners';
rmdir_recursive($dir);
?>