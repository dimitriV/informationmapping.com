<?php

/**
 * @version     $Id: controller.php  2011-12-20 04:13:25Z $
 * @package     IMI
 * @subpackage  com_courses
 * @author      ITOffshore Nepal
 * @copyright   Copyright (C) 2011 ITOffshore Nepal.
 * @license     ITOffshore Nepal
 */
// No direct access
defined('_JEXEC') or die;

/**
 * Courses Course Controller
 *
 * @package		Joomla.Administrator
 * @subpackage	com_courses
 * @since		1.5
 */
class CoursesController extends JControllerLegacy
//class CoursesController extends JController

{

	/**
	 * auto populate course name on  change course language
	 * will be called by ajax request
	 */
	public function populateCoursename()
	{
		$courselangugeid = JRequest::getVar('courselangugeid');

		$course_id = JRequest::getVar('course_id', '-');

		$model = $this->getModel('certification');

		$lists = $model->getCourseName($courselangugeid);

		// prepare option
		$option = '<option value="">' . JText::_('COM_COURSES_SELECT') . '</option>';
		foreach ($lists as $list) {
			$selected = '';
			if ($course_id == $list['id']) {
				$selected = "selected='selected'";
			}
			$option .= '<option value="' . $list['id'] . '" ' . $selected . '>' . $list['title'] . '</option>';
		}

		echo $option;
		exit;
	}

	/**
	 * auto populate instructor on change on partner
	 * will be called by ajax request
	 */
	public function populateInstructor()
	{
		$partnerid = JRequest::getVar('partnerid');
		$instructor_id = JRequest::getVar('instructor_id', false);

		$model = $this->getModel('certification');

		$lists = $model->getCourseInstructor($partnerid);

		// prepare option
		$option = '<option value="">' . JText::_('COM_COURSES_SELECT') . '</option>';
		foreach ($lists as $list) {
			$selected = '';
			if ($instructor_id == $list['id']) {
				$selected = "selected='selected'";
			}
			$option .= '<option value="' . $list['id'] . '" ' . $selected . '>' . $list['name'] . '</option>';
		}
		$selected = '';
		if ($instructor_id === '0') {
			$selected = "selected='selected'";
		}
		$option.= '<option value="other" ' . $selected . '>' . JText::_('COM_COURSES_OTHER') .'</option>';

		echo $option;
		exit;
	}

	/**
	 * Method to display a view.
	 *
	 * @param	boolean			$cachable	If true, the view output will be cached
	 * @param	array			$urlparams	An array of safe url parameters and their variable types, for valid values see {@link JFilterInput::clean()}.
	 *
	 * @return	JController		This object to support chaining.
	 * @since	1.5
	 */
	public function display($cachable = false, $urlparams = false)
	{
		require_once JPATH_COMPONENT . '/helpers/courses.php';

		// Load the submenu.
		CoursesHelper::addSubmenu(JRequest::getCmd('view', 'courselanguages'));

		$view = JRequest::getCmd('view', 'courselanguages');
		$layout = JRequest::getCmd('layout', 'default');
		$id = JRequest::getInt('id');
		JRequest::setVar('view', $view);

		$session = JFactory::getSession();

		// Check for edit form.
		if ($view == 'courselanguage' && $layout == 'edit' && !$this->checkEditId('com_courses.edit.courselanguage', $id)) {
			// Somehow the person just went to the form - we don't allow that.
			$this->setError(JText::sprintf('JLIB_APPLICATION_ERROR_UNHELD_ID', $id));
			$this->setMessage($this->getError(), 'error');
			$this->setRedirect(JRoute::_('index.php?option=com_courses&view=courselanguages', false));

			return false;
		} elseif ($view == 'course' && $layout == 'edit' && !$this->checkEditId('com_courses.edit.course', $id)) {

			// Somehow the person just went to the form - we don't allow that.
			$this->setError(JText::sprintf('JLIB_APPLICATION_ERROR_UNHELD_ID', $id));
			$this->setMessage($this->getError(), 'error');
			$this->setRedirect(JRoute::_('index.php?option=com_courses&view=courses', false));

			return false;
		} elseif ($view == 'student' && $layout == 'edit' && !$this->checkEditId('com_courses.edit.student', $id)) {

			// Somehow the person just went to the form - we don't allow that.
			$this->setError(JText::sprintf('JLIB_APPLICATION_ERROR_UNHELD_ID', $id));
			$this->setMessage($this->getError(), 'error');
			$this->setRedirect(JRoute::_('index.php?option=com_courses&view=students', false));

			return false;
		} elseif ($view == 'partner' && $layout == 'edit' && !$this->checkEditId('com_courses.edit.partner', $id)) {

			// Somehow the person just went to the form - we don't allow that.
			$this->setError(JText::sprintf('JLIB_APPLICATION_ERROR_UNHELD_ID', $id));
			$this->setMessage($this->getError(), 'error');
			$this->setRedirect(JRoute::_('index.php?option=com_courses&view=partners', false));

			return false;
		} elseif ($view == 'instructor' && $layout == 'edit' && !$this->checkEditId('com_courses.edit.instructor', $id)) {

			// Somehow the person just went to the form - we don't allow that.
			$this->setError(JText::sprintf('JLIB_APPLICATION_ERROR_UNHELD_ID', $id));
			$this->setMessage($this->getError(), 'error');
			$this->setRedirect(JRoute::_('index.php?option=com_courses&view=instructors', false));

			return false;
		} elseif ($view == 'country' && $layout == 'edit' && !$this->checkEditId('com_courses.edit.country', $id)) {

			// Somehow the person just went to the form - we don't allow that.
			$this->setError(JText::sprintf('JLIB_APPLICATION_ERROR_UNHELD_ID', $id));
			$this->setMessage($this->getError(), 'error');
			$this->setRedirect(JRoute::_('index.php?option=com_courses&view=countries', false));

			return false;
		} elseif ($view == 'students' && $session->get('csvmsg')) {
			// get message from session
			$msg = $session->get('csvmsg');
			$type = $session->get('type');
			$session->clear('csvmsg');
			$session->clear('type');
			switch ($type) {
				case true:
					$this->setRedirect(JRoute::_('index.php?option=com_courses&view=students', false), $msg);
					break;
				case false:
					$this->setRedirect(JRoute::_('index.php?option=com_courses&view=students', false), $msg, 'error');
					break;
				default:
					$this->setRedirect(JRoute::_('index.php?option=com_courses&view=students', false), $msg, 'message');
					break;
			}
		}
		parent::display();

		return $this;
	}

	/**
	 *  upload csv file and processing
	 */
	public function csvimport()
	{
		// get required model object
		$model = $this->getModel('students');

		$max = ini_get('upload_max_filesize');
		$uploadPath = 'components/com_courses/csv';
		$file_type = 'text/csv';

		// upload file
		$upload = $model->fileUpload($max, $uploadPath, $file_type);
		// setting msg into session untill the page get refreshed
		$session = JFactory::getSession();

		if (is_array($upload)) {
			$path = current($upload);
			// csv to database..
			$csvProcess = $model->csvTodb($path);

			if ($csvProcess) {
				$msg = JText::_('COM_COURSES_KEYS_IMPORTED_SUCESSFULLY');
				$session->set('csvmsg', $msg);
				$session->set('type', true);
				echo '<script>parent.window.location.reload(true)</script>';
			} else {
				$msg = JText::_('COM_COURSES_FAILED_TO_IMPORT_KEYS');
				$session->set('csvmsg', $msg);
				$session->set('type', false);
				echo '<script>parent.window.location.reload(true)</script>';
			}
		} else {
			// faild to upload file
			$session->set('csvmsg', $upload);
			$session->set('type', false);
			echo '<script>parent.window.location.reload(true)</script>';
		}
	}

}
