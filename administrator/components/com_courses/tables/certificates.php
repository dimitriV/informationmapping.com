<?php

/**
 * @version     $Id: certificates.php  2011-12-20 04:13:25Z $
 * @package     IMI
 * @subpackage  com_courses
 * @author      ITOffshore Nepal
 * @copyright   Copyright (C) 2011 ITOffshore Nepal.
 * @license     ITOffshore Nepal
 */
// No direct access
defined('_JEXEC') or die;

/**
 * Certificate Table class
 *
 * @package		Joomla.Administrator
 * @subpackage          com_courses
 * @since		1.7
 */
class CoursesTableCertificates extends JTable
{

	/**
	 * Constructor
	 *
	 * @param JDatabase A database connector object
	 */
	public function __construct(&$db)
	{
		parent::__construct('#__course_certificates', 'id', $db);
	}

	/**
	 * Overloaded check method to ensure data integrity
	 * for validation purpose;
	 * @access public
	 * @return boolean True on success
	 */
	public function check()
	{
		// haystack array to check valid keys
		$studentKeys = $this->_getCurrentstudentkeys();

		//trim spaces before and after keys
		$trimspace = function($string) {
					return trim($string);
				};

		//clean up spaces
		$studentKeys = array_map($trimspace, $studentKeys);

		if ($this->first_name == '') {
			$this->setError(JText::_('Invalid  first name!'));
			return false;
		} elseif ($this->last_name == '') {
			$this->setError(JText::_('Invalid  last name!'));
			return false;
		} elseif (!$this->course_language_id) {
			$this->setError(JText::_('Invalid  course language !'));
			return false;
		} elseif (!$this->course_id) {
			$this->setError(JText::_('Invalid  course !'));
			return false;
		} elseif (!$this->instructor_id) {
			$this->setError(JText::_('Invalid  instructor !'));
			return false;
		} else if (($this->instructor_id == "other") && ($this->instructor_name == '')) {
			$this->setError(JText::_('Invalid  Instructor name !'));
			return false;
		} elseif ((!$this->student_key) || (!in_array(trim($this->student_key), $studentKeys, true))) {
			$this->setError(JText::_('Invalid  student key !'));
			return false;
		}
		// date validation
		if ($this->date_from && $this->date_to) {
			$format = 'Y-m-d';
			$from = DateTime::createFromFormat($format, $this->date_from);
			$to = DateTime::createFromFormat($format, $this->date_to);

			if ($from > $to) {
				$this->setError(JText::_('Course start date is greater than course end date !'));
				return false;
			}
		} else {
			$this->setError(JText::_('Invalid Course Date !'));
			return false;
		}

		$this->created = date('Y-m-d', strtotime($this->created));
		$this->student_key = trim($this->student_key);
		$this->date_from = date('Y-m-d', strtotime($this->date_from));
		$this->date_to = date('Y-m-d', strtotime($this->date_to));

		return true;
	}

	/**
	 * get avtive student key
	 * prepare haystack array of the active sutudent key
	 * @return type
	 */
	private function _getCurrentstudentkeys()
	{
		$db = $this->getDbo();
		$query = "SELECT DISTINCT(studentkey) FROM #__course_studentkeys WHERE state != '-2' ";
		$db->setQuery($query);
		return $db->loadColumn();
	}

	public function publish($pks = null, $state = 1, $userId = 0)
	{
		// Initialise variables.
		$k = $this->_tbl_key;

		// Sanitize input.
		JArrayHelper::toInteger($pks);
		$userId = (int) $userId;
		$state = (int) $state;

		// If there are no primary keys set check to see if the instance key is set.
		if (empty($pks)) {
			if ($this->$k) {
				$pks = array($this->$k);
			}
			// Nothing to set publishing state on, return false.
			else {
				$this->setError(JText::_('JLIB_DATABASE_ERROR_NO_ROWS_SELECTED'));
				return false;
			}
		}

		// Build the WHERE clause for the primary keys.
		$where = $k . '=' . implode(' OR ' . $k . '=', $pks);

		// Update the publishing state for rows with the given primary keys.
		$this->_db->setQuery(
				'UPDATE `' . $this->_tbl . '`' .
				' SET `state` = ' . (int) $state .
				' WHERE (' . $where . ')' .
				$checkin
		);
		$this->_db->query();

		// Check for a database error.
		if ($this->_db->getErrorNum()) {
			$this->setError($this->_db->getErrorMsg());
			return false;
		}

		// If checkin is supported and all rows were adjusted, check them in.
		if ($checkin && (count($pks) == $this->_db->getAffectedRows())) {
			// Checkin the rows.
			foreach ($pks as $pk) {
				$this->checkin($pk);
			}
		}

		// If the JTable instance value is in the list of primary keys that were set, set the instance.
		if (in_array($this->$k, $pks)) {
			$this->state = $state;
		}

		$this->setError('');
		return true;
	}

}

?>
