<?php
/**
 * @version     $Id: emailtemplate.php  2011-12-20 04:13:25Z $
 * @package     IMI 
 * @subpackage  com_courses
 * @author      ITOffshore Nepal
 * @copyright   Copyright (C) 2011 ITOffshore Nepal.
 * @license     ITOffshore Nepal
 */

// No direct access
defined('_JEXEC') or die;

/**
 * EmailTemplate Table class
 *
 * @package		Joomla.Administrator
 * @subpackage	com_courses
 * @since		1.7
 */
class CoursesTableEmailTemplate extends JTable
{
	/**
	 * Constructor
	 *
	 * @param JDatabase A database connector object
	 */
	public function __construct(&$db)
	{
		parent::__construct('#__course_emailtemplates', 'id', $db);
	}

	/**
	 * Overload the store method for the Courses table.
	 *
	 * @param	boolean	Toggle whether null values should be updated.
	 * @return	boolean	True on success, false on failure.
	 * @since	1.6
	 */
	public function store($updateNulls = false)
	{
		$date	= JFactory::getDate();
		
		// Verify that the alias is unique
		$table = JTable::getInstance('EmailTemplate', 'CoursesTable');
		
		// Attempt to store the user data.
		return parent::store($updateNulls);
	}

}
?>