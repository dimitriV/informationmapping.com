<?php
/**
 * @version     $Id:country.php  2011-12-20 04:13:25Z $
 * @package     IMI
 * @subpackage  com_courses
 * @author      ITOffshore Nepal
 * @copyright   Copyright (C) 2011 ITOffshore Nepal.
 * @license     ITOffshore Nepal
 */
defined('_JEXEC') or die;

jimport('joomla.application.component.modellist');

/**
 * Methods supporting a list of course records.
 *
 * @package		Joomla.Administrator
 * @subpackage	com_courses
 * @since		1.6
 */
class CoursesModelStudents extends JModelList
{

	/**
	 * Constructor.
	 *
	 * @param	array	An optional associative array of configuration settings.
	 * @see		JController
	 * @since	1.6
	 */
	public function __construct($config = array())
	{
		if (empty($config['filter_fields'])) {
			$config['filter_fields'] = array(
				'id', 'a.id',
				'studentkey', 'a.studentkey',
				'checked_out', 'a.checked_out',
				'checked_out_time', 'a.checked_out_time',
				'status', 'a.status',
				'status_imp', 'a.status_imp',
				'state', 'a.state',
				'created', 'a.created',
				'created_by', 'a.created_by',
				'publish_up', 'a.publish_up',
				'publish_down', 'a.publish_down',
			);
		}

		parent::__construct($config);
	}

	/**
	 * Method to auto-populate the model state.
	 *
	 * Note. Calling getState in this method will result in recursion.
	 *
	 * @since	1.6
	 */
	protected function populateState($ordering = null, $direction = null)
	{
		// Initialise variables.
		$app = JFactory::getApplication('administrator');

		// Load the filter state.
		$search = $this->getUserStateFromRequest($this->context . '.filter.search', 'filter_search');
		$this->setState('filter.search', $search);

		$published = $this->getUserStateFromRequest($this->context . '.filter.state', 'filter_published', '', 'string');
		$this->setState('filter.state', $published);

		$status = $this->getUserStateFromRequest($this->context . '.filter.status', 'filter_status', '', 'string');
		$this->setState('filter.status', $status);

		// List state information.
		parent::populateState('a.studentkey', 'asc');
	}

	/**
	 * Method to get a store id based on model configuration state.
	 *
	 * This is necessary because the model is used by the component and
	 * different modules that might need different sets of data or different
	 * ordering requirements.
	 *
	 * @param	string		$id	A prefix for the store id.
	 * @return	string		A store id.
	 * @since	1.6
	 */
	protected function getStoreId($id = '')
	{
		// Compile the store id.
		$id.= ':' . $this->getState('filter.search');
		$id.= ':' . $this->getState('filter.state');
		$id.= ':' . $this->getState('filter.status');

		return parent::getStoreId($id);
	}

	/**
	 * Build an SQL query to load the list data.
	 *
	 * @return	JDatabaseQuery
	 * @since	1.6
	 */
	protected function getListQuery()
	{
		// Create a new query object.
		$db = $this->getDbo();
		$query = $db->getQuery(true);
		$user = JFactory::getUser();

		// Select the required fields from the table.
		$query->select(
				$this->getState(
						'list.select', 'a.id, a.studentkey, a.checked_out, a.checked_out_time,' .
						'a.state, a.status, a.status_imp, a.publish_up, a.publish_down'
				)
		);
		$query->from('`#__course_studentkeys` AS a');


		// Join over the users for the checked out user.
		$query->select('uc.name AS editor');
		$query->join('LEFT', '#__users AS uc ON uc.id=a.checked_out');


		// Implement View Level Access
		if (!$user->authorise('core.admin')) {
			$groups = implode(',', $user->getAuthorisedViewLevels());
			$query->where('a.access IN (' . $groups . ')');
		}

			// Filter by category.
		$studentstatus = $this->getState('filter.status');
		if (is_numeric($studentstatus)) {
			$query->where('a.status = '.(int) $studentstatus);
		}else if ($studentstatus === '') {
			$query->where('(a.status IN (0, 1))');
		}

		// Filter by published state
		$published = $this->getState('filter.state');
		if (is_numeric($published)) {
			$query->where('a.state = ' . (int) $published);
		} else if ($published === '') {
			$query->where('(a.state IN (0, 1))');
		}


		// Filter by search in title
		$search = $this->getState('filter.search');
		if (!empty($search)) {
			if (stripos($search, 'id:') === 0) {
				$query->where('a.id = ' . (int) substr($search, 3));
			} else {
				$search = $db->Quote('%' . $db->getEscaped($search, true) . '%');
				$query->where('(a.studentkey LIKE ' . $search . ')');
			}
		}


        if(!empty($orderCol)) {
    		$query->order($db->quote($orderCol). ' ' . $orderDirn);
        }

		return $query;

	}

	/**
	 * file uplaod
	 * @param type $max
	 * @param type $module_dir
	 * @param type $file_type
	 * @return type
	 */
	public function fileUpload($max, $path, $file_type)
	{
		$msg = '';
		//Retrieve file details from uploaded file, sent from upload form
		$file = JRequest::getVar('file_upload', null, 'files', 'array');

		if (!isset($file)) {
			return false;
		}

		//Clean up filename to get rid of strange characters like spaces etc
		$filename = JFile::makeSafe($file['name']);

		// file size limit check
		if ($file['size'] > $max) {
			$msg = JText::_('ONLY_FILES_UNDER') . ' ' . $max;
		}

		//Set up the source and destination of the file
		$src = $file['tmp_name'];
		$dest = $path . '/' . $filename;

		// check for valid file extension before uploading
		if ($file['type'] == $file_type || $file_type == '*' || $file['type'] == "application/vnd.ms-excel") {
			if (JFile::upload($src, $dest)) {
				return array($dest);
			} else {
				$msg = JText::_('COM_COURSES_ERROR_IN_UPLOAD');
			}
		} else {
			$msg = JText::_('COM_COURSES_FILE_TYPE_INVALID');
		}

		return $msg;
	}

	/**
	 * get avtive student key
	 * prepare haystack array of the active sutudent key
	 * @return type
	 */
	private function _getCurrentstudentkeys()
	{
		$db = $this->getDbo();
		$query = "SELECT DISTINCT(studentkey) FROM #__course_studentkeys WHERE state != '-2' ";
		$db->setQuery($query);
		return $db->loadResultArray();
	}

	/**
	 * read csv file and write data into database
	 * @param type $path
	 * @return type bool
	 */
	public function csvTodb($path)
	{
		// check path
		if (!$path) {
			return false;
		}
		// currently active keys form db
		$currentActivekeys = $this->_getCurrentstudentkeys();

		// set column's default data
		$date = JFactory::getDate();
		$created = $date->format('%A, %d %B %Y');

		$user = JFactory::getUser();
		$createdBy = $user->id;

		$approved = 1;

		$status = 0;
                
                $status_imp = 0;

		$state = 1;

		$approved = 1;

		// read the csv file
		$handle = fopen($path, "r");

		// max number of allowed row
		$length = 1000;

		// inilitiaze the value array
		$values = array();

		$query = "INSERT INTO #__course_studentkeys (
					`id` ,
					`studentkey` ,
					`status`,
					`state`,
					`approved` ,
					`language` ,
					`created` ,
					`created_by`,
                                        `status_imp`
					)
					VALUES ";

		while (($data = fgetcsv($handle, $length, ",")) !== FALSE) {
			$num = count($data);
			// csv must have only one colum and the key must be completly unique
			if (($num > 1) || (in_array($data[0], $currentActivekeys, true))) {
				continue;
			}

			// prepare values
			$values[] = "(NULL,'" . trim($data[0]) . "','" . $status . "','" . $state . "','" . $approved . "', '*' ,'" . $created . "','" . $createdBy . "','" . $status_imp . "')";

			// push used key into haystack harray
			array_push($currentActivekeys, $data[0]);
		}

		fclose($handle);

		//delete the file after extracting the data
		@unlink($path);

		// seperate each row by comma
		$valueString = implode(',', $values);

		// prepare complete insert query
		$query .= $valueString;

		$db = $this->getDbo();
		$db->setQuery($query);
		$result = $db->query();

		return $result;


	}

}
?>
