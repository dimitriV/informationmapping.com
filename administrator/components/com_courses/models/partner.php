<?php
/**
 * @version     $Id:partner.php  2011-12-20 04:13:25Z $
 * @package     IMI 
 * @subpackage  com_courses
 * @author      ITOffshore Nepal
 * @copyright   Copyright (C) 2011 ITOffshore Nepal.
 * @license     ITOffshore Nepal
 */
// No direct access.
defined('_JEXEC') or die;

jimport('joomla.application.component.modeladmin');

/**
 * Partners model.
 *
 * @package		Joomla.Administrator
 * @subpackage	com_courses
 * @since		1.5
 */
class CoursesModelPartner extends JModelAdmin
{

	/**
	 * Method to test whether a record can be deleted.
	 *
	 * @param	object	A record object.
	 * @return	boolean	True if allowed to delete the record. Defaults to the permission set in the component.
	 * @since	1.6
	 */
	protected function canDelete($record)
	{
		if (!empty($record->id)) {
			if ($record->state != -2) {
				return ;
			}
			$user = JFactory::getUser();

			return parent::canDelete($record);
			
		}
	}

	/**
	 * Method to test whether a record can have its state changed.
	 *
	 * @param	object	A record object.
	 * @return	boolean	True if allowed to change the state of the record. Defaults to the permission set in the component.
	 * @since	1.6
	 */
	protected function canEditState($record)
	{
		$user = JFactory::getUser();

		if (!empty($record->catid)) {
			return $user->authorise('core.edit.state', 'com_courses.category.'.(int) $record->catid);
		}
		else {
			return parent::canEditState($record);
		}
	}
	/**
	 * Returns a reference to the a Table object, always creating it.
	 *
	 * @param	type	The table type to instantiate
	 * @param	string	A prefix for the table class name. Optional.
	 * @param	array	Configuration array for model. Optional.
	 * @return	JTable	A database object
	 * @since	1.6
	 */
	public function getTable($type = 'Partner', $prefix = 'CoursesTable', $config = array())
	{
		return JTable::getInstance($type, $prefix, $config);
	}

	/**
	 * Method to get the record form.
	 *
	 * @param	array	$data		An optional array of data for the form to interogate.
	 * @param	boolean	$loadData	True if the form is to load its own data (default case), false if not.
	 * @return	JForm	A JForm object on success, false on failure
	 * @since	1.6
	 */
	public function getForm($data = array(), $loadData = true)
	{
		// Initialise variables.
		$app	= JFactory::getApplication();

		// Get the form.
		$form = $this->loadForm('com_courses.partner', 'partner', array('control' => 'jform', 'load_data' => $loadData));
      
		if (empty($form)) {
			return false;
		}

		// Determine correct permissions to check.
		if ($this->getState('partner.id')) {
			// Existing record. Can only edit in selected categories.
			$form->setFieldAttribute('countryid', 'action', 'core.edit');
		} else {
			// New record. Can only create in selected categories.
			$form->setFieldAttribute('countryid', 'action', 'core.create');
		}

		// Modify the form based on access controls.
		if (!$this->canEditState((object) $data)) {
			// Disable fields for display.
			$form->setFieldAttribute('ordering', 'disabled', 'true');
			$form->setFieldAttribute('state', 'disabled', 'true');
			$form->setFieldAttribute('publish_up', 'disabled', 'true');
			$form->setFieldAttribute('publish_down', 'disabled', 'true');

			// Disable fields while saving.
			// The controller has already verified this is a record you can edit.
			$form->setFieldAttribute('ordering', 'filter', 'unset');
			$form->setFieldAttribute('state', 'filter', 'unset');
			$form->setFieldAttribute('publish_up', 'filter', 'unset');
			$form->setFieldAttribute('publish_down', 'filter', 'unset');
		}
      
		return $form;
	}

	/**
	 * Method to get the data that should be injected in the form.
	 *
	 * @return	mixed	The data for the form.
	 * @since	1.6
	 */
	protected function loadFormData()
	{
		// Check the session for previously entered form data.
		$data = JFactory::getApplication()->getUserState('com_courses.edit.partner.data', array());

		if (empty($data)) {
			$data = $this->getItem();

			// Prime some default values.
			if ($this->getState('partner.id') == 0) {
				$app = JFactory::getApplication();
				$data->set('countryid', JRequest::getInt('countryid', $app->getUserState('com_courses.partners.filter.category_id')));
			}
		}

		return $data;
	}

	/**
	 * Method to get a single record.
	 *
	 * @param	integer	The id of the primary key.
	 *
	 * @return	mixed	Object on success, false on failure.
	 * @since	1.6
	 */
	public function getItem($pk = null)
	{
		if ($item = parent::getItem($pk)) {
		
		}

		return $item;
	}
    
    
	public function save()
    {
    	$thumb_width = 165;
		//$thumb_height = 73;
		$data      = JRequest::getVar('jform', array(), 'post', 'array');
		if($data['id'])  
		{
			$prev = $this->getprevoiusdata($data['id']);  
		}
		else{
			$prev = 0;
		}
  
		//Image processing
        if($_FILES['jform']['tmp_name']['filename']){
            if($prev){
                $img_pathold = JPATH_ROOT.'/images/partners/'.$prev;
                //@unlink($img_pathold);
				if(file_exists($img_pathold)){
					@unlink($img_pathold);
				}
            }
            $source = $_FILES['jform']['tmp_name']['filename'];
            $file_name =  mktime().'-'.strtolower(str_replace(' ','-',$_FILES['jform']['name']['filename']));
            
            $img_path = JPATH_ROOT.'/images/partners/'.$file_name;
           
            $system=explode('.',$file_name);
            $ext = $system[count($system)-1];
          	
    		if (preg_match('/jpg|jpeg|JPG|JPEG|gif|GIF|png|PNG/',$ext)){
  	            if(copy($source, $img_path)){
					list($w,$h) = getimagesize($source); 
					if($w > $thumb_width){                      
                        $this->createthumb($img_path,$img_path,$thumb_width);
                    }
					if(isset($data['filename']))
                    	$backup = $data['filename'];
	                    $data['filename'] =  $file_name;
            		}
            		else{
            	    	// @unlink($img_path);
            	     	if(file_exists($img_path)){
							@unlink($img_path);
						}
	            		$msg = JText::_('Upload failure');
    	        	    $this->setError($msg);
        				return false;  
            		}
              }
              else{
                    $msg = JText::_('File not supported. Only .jpg, .jpeg, .gif and .png are allowed');
            	    $this->setError($msg);
        			return false;  
              }
			}
     
        //End of image processing. 
       	$dispatcher = JDispatcher::getInstance();
		$table		= $this->getTable();
		$key			= $table->getKeyName();
		$pk			= (!empty($data[$key])) ? $data[$key] : (int)$this->getState($this->getName().'.id');
		$isNew		= true;

		// Include the content plugins for the on save events.
		JPluginHelper::importPlugin('content');

		// Allow an exception to be thrown.
		try
		{
			// Load the row if saving an existing record.
			if ($pk > 0) {
				$table->load($pk);
				$isNew = false;
			}

			// Bind the data.
			if (!$table->bind($data)) {
				$this->setError($table->getError());
				return false;
			}

			// Prepare the row for saving
			$this->prepareTable($table);

			// Check the data.
			if (!$table->check()) {
				$this->setError($table->getError());
				return false;
			}

			// Trigger the onContentBeforeSave event.
			$result = $dispatcher->trigger($this->event_before_save, array($this->option.'.'.$this->name, &$table, $isNew));
			if (in_array(false, $result, true)) {
				$this->setError($table->getError());
				return false;
			}

			// Store the data.
			if (!$table->store()) {
			    @unlink($img_path);
				$this->setError($table->getError());
				return false;
			}

			// Clean the cache.
			$this->cleanCache();

			// Trigger the onContentAfterSave event.
			$dispatcher->trigger($this->event_after_save, array($this->option.'.'.$this->name, &$table, $isNew));
		}
		catch (Exception $e)
		{
			$this->setError($e->getMessage());

			return false;
		}

		$pkName = $table->getKeyName();

		if (isset($table->$pkName)) {
			$this->setState($this->getName().'.id', $table->$pkName);
		}
		$this->setState($this->getName().'.new', $isNew);

		return true; 
    }



	/**
	 * Prepare and sanitise the table prior to saving.
	 *
	 * @since	1.6
	 */
	protected function prepareTable(&$table)
	{
		if (empty($table->id)) {
			// Set ordering to the last item if not set
			if (empty($table->ordering)) {
				$db = JFactory::getDbo();
				$db->setQuery('SELECT (MAX(ordering)+1) max_ordering FROM #__course_partners WHERE countryid = '.$table->countryid);
				$table->ordering = $db->loadResult();
			}
		}
	}

	/**
	 * A protected method to get a set of ordering conditions.
	 *
	 * @param	object	A record object.
	 * @return	array	An array of conditions to add to add to ordering queries.
	 * @since	1.6
	 */
	protected function getReorderConditions($table)
	{
		$condition = array();
		$condition[] = 'countryid = '.(int) $table->countryid;
		return $condition;
	}
    
    function createthumb($name, $filename, $new_w, $new_h = '')
  	{
		$filename_parts = explode('.', $name);
		$filetype = strtolower($filename_parts[count($filename_parts)-1]);
		
		if( $filetype == 'gif'){
			$src_img = imagecreatefromgif($name);
		}			
		else if($filetype == 'png'){
			$src_img = imagecreatefrompng($name);
		}
		else{
			$src_img = imagecreatefromjpeg($name);
		}

		$old_x = imageSX($src_img);
		$old_y = imageSY($src_img);
		
		/* find the "desired height" of this thumbnail, relative to the desired width  */
		$thumb_w = $new_w;
  		$thumb_h = floor($old_y*($thumb_w/$old_x));

		$dst_img = imagecreatetruecolor($thumb_w, $thumb_h);
		
		if($filetype == 'png' || $filetype == 'gif'){
            imagealphablending($dst_img, false);
            imagesavealpha($dst_img, true);
            $transparent = imagecolorallocatealpha($dst_img, 255, 255, 255, 127);
            imagefilledrectangle($dst_img, 0, 0, $thumb_w, $thumb_h, $transparent);
        }
        
		imagecopyresampled($dst_img,$src_img,0,0,0,0,$thumb_w,$thumb_h,$old_x,$old_y); 
		if ( $filetype == 'gif'){
			imagegif($dst_img, $filename);
		}
		elseif($filetype == 'png'){
			imagepng($dst_img, $filename);
		}
		else{
			imagejpeg($dst_img, $filename, 100);
		} 
	
		imagedestroy($dst_img); 
		imagedestroy($src_img); 
	}
	

	public function getprevoiusdata($id)
    {
        $db = JFactory::getDbo();
        $db->setQuery('SELECT filename FROM #__course_partners where id='.$id);
        $file = $db->loadResult();
        return $file;
    }   
    
    
    public function checkvalue()
	{
		$selected = &JRequest::getVar('cid', 0, '', 'array');
		$selected_data = implode("," ,$selected);
		
		$db = JFactory::getDbo();
		$db->setQuery('SELECT id FROM #__course_instructors WHERE partnerid IN ('.$selected_data.')');
		$partnerid = $db->loadResult();
		
		$db->setQuery('SELECT id FROM #__course_certificates WHERE partner_id IN ('.$selected_data.')');
		$certificatepartner = $db->loadResult();
		
		if($partnerid || $certificatepartner)
		{
			return true;
		}
		else
		{
			return false;
		}
		
	}
}
?>
