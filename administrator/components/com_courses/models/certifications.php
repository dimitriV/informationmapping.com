<?php

/**
 * @version     $Id:certifications.php  2011-12-20 04:13:25Z $
 * @package     IMI
 * @subpackage  com_courses
 * @author      ITOffshore Nepal
 * @copyright   Copyright (C) 2011 ITOffshore Nepal.
 * @license     ITOffshore Nepal
 */
defined('_JEXEC') or die;

jimport('joomla.application.component.modellist');

/**
 * Methods supporting a list of course records.
 *
 * @package		Joomla.Administrator
 * @subpackage	com_courses
 * @since		1.6
 */
class CoursesModelCertifications extends JModelList
{

	/**
	 * Constructor.
	 *
	 * @param	array	An optional associative array of configuration settings.
	 * @see		JController
	 * @since	1.6
	 */
	public function __construct($config = array())
	{
		if (empty($config['filter_fields'])) {
			$config['filter_fields'] = array(
				'id', 'a.id',
				'first_name', 'a.first_name',
				'last_name', 'a.last_name',
				'course_language_id', 'a.course_language_id',
				'course_id', 'a.course_id',
				'partner_id', 'a.partner_id',
				'instructor_id', 'a.instructor_id',
				'instructor_name', 'a.instructor_name',
				'student_key', 'a.student_key',
				'state', 'a.state',
				'date_from', 'a.date_from',
				'date_to', 'a.date_to',
				'filename', 'a.filename',
				'filename', 'a.filename',
				'created', 'a.created',
				'status', 'a.status',
				'language_name', 'cl.language_name',
				'c.title',
				'p.title',
				'u.email',
			);
		}

		parent::__construct($config);
	}

	/**
	 * Method to auto-populate the model state.
	 *
	 * Note. Calling getState in this method will result in recursion.joomla 1.6 hello world component
	 *
	 * @since	1.6
	 */
	protected function populateState($ordering = null, $direction = null)
	{
		// Initialise variables.
		$app = JFactory::getApplication('administrator');

		// Load the filter state.
		$search = $this->getUserStateFromRequest($this->context . '.filter.search', 'filter_search');
		$this->setState('filter.search', $search);

		$coursename = $this->getUserStateFromRequest($this->context . '.filter.coursename', 'filter_coursename', '');
		$this->setState('filter.coursename', $coursename);

		$partnername = $this->getUserStateFromRequest($this->context . '.filter.partnername', 'filter_partnername', '');
		$this->setState('filter.partnername', $partnername);

		$language = $this->getUserStateFromRequest($this->context . '.filter.language_name', 'filter_language', '');
		$this->setState('filter.language_name', $language);

		$status = $this->getUserStateFromRequest($this->context . '.filter.status', 'filter_status', '');
		$this->setState('filter.status', $status);

		// List state information.
		parent::populateState('a.first_name', 'asc');
	}

	/**
	 * Method to get a store id based on model configuration state.
	 *
	 * This is necessary because the model is used by the component and
	 * different modules that might need different sets of data or different
	 * ordering requirements.
	 *
	 * @param	string		$id	A prefix for the store id.
	 * @return	string		A store id.
	 * @since	1.6
	 */
	protected function getStoreId($id = '')
	{
		// Compile the store id.
		$id.= ':' . $this->getState('filter.search');
		$id.= ':' . $this->getState('filter.partnername');
		$id.= ':' . $this->getState('filter.coursename');
		$id.= ':' . $this->getState('filter.status');
		$id.= ':' . $this->getState('filter.language_name');

		return parent::getStoreId($id);
	}

	public function getDropdown()
	{
		$type = JRequest::getVar('tableName');
		$db = $this->getDbo();
		$query = $db->getQuery(true);

		switch ($type) {
			case 'courseLanguages':
				$query->select('DISTINCT(language_name)')
						->from('#__course_courselanguages')
                                                ->where('state = 1')
						->order('language_name');
                                                
				break;

			case 'courses':
				$query->select('DISTINCT(title)')
						->from('#__course_courses')
                                                ->where('state = 1')
						->order('title');
				break;
			case 'partners':
				$query->select('DISTINCT(title)')
						->from('#__course_partners')
                                                ->where('state = 1')
						->order('title');
				break;
			default:
				return array();
				break;
		}

		$db->setQuery($query);

		$columns = $db->loadResultArray();

		$option = array();

        if(!empty($columns)) {
    		foreach ($columns as $column) {
	    		$option[] = JHTML::_('select.option', $column, $column);
            }
		}

		return $option;
	}

	/**
	 * Build an SQL query to load the list data.
	 *
	 * @return	JDatabaseQuery
	 * @since	1.6
	 */
	protected function getListQuery()
	{
		// Create a new query object.
		$db = $this->getDbo();
		$query = $db->getQuery(true);
		$user = JFactory::getUser();
		// Select the required fields from the table.user_id
		$query->select(
				$this->getState(
						'list.select', 'a.id, a.first_name, a.last_name, a.course_language_id,' .
						'a.course_id, a.partner_id, a.instructor_id,' .
						'a.date_to, a.filename, a.created, a.status, a.user_id, ' .
						'a.instructor_name, a.student_key, a.date_from, a.filename'
				)
		);
		$query->from('`#__course_certificates` AS a');

		// Join over the courselanguage
		$query->select('cl.language_name AS language_name');
		$query->join('LEFT', '`#__course_courselanguages` AS cl ON cl.id = a.course_language_id');

		// Join over the course
		$query->select('c.title AS course_title');
		$query->join('LEFT', '#__course_courses AS c ON c.id=a.course_id');

		// Join over the partner
		$query->select('p.title AS partner_title');
		$query->join('LEFT', '#__course_partners AS p ON p.id = a.partner_id');

		//join over the user table
		$query->select('u.email  AS user_email');
		$query->join('LEFT', '#__users AS u ON u.id = a.user_id');

		// Filter by search in title
		$search = $this->getState('filter.search');
		if (!empty($search)) {
			if (stripos($search, 'id:') === 0) {
				$query->where('a.id = ' . (int) substr($search, 3));
			} else {
				$search = $db->Quote('%' . $db->getEscaped($search, true) . '%');
				$query->where('(a.first_name LIKE ' . $search . ')');
			}
		}

		// Filter on the course language.
		if ($language = $this->getState('filter.language_name')) {
			$query->where('cl.language_name = ' . $db->quote($language));
		}

		// Filter on the course name
		if ($coursename = $this->getState('filter.coursename')) {
			$query->where('c.title = ' . $db->quote($coursename));
		}

		// Filter on the partner name
		if ($partnername = $this->getState('filter.partnername')) {
			$query->where('p.title = ' . $db->quote($partnername));
		}

		// Filter on the partner name
		if (($status = $this->getState('filter.status')) || ($this->getState('filter.status') === '0')) {
			$query->where('a.status = ' . $db->quote($status));
		}

			// Filter by published state
		$published = $this->getState('filter.state');
		if (is_numeric($published)) {
			$query->where('a.state = '.(int) $published);
		} else if ($published === '') {
			$query->where('(a.state IN (0, 1))');
		}



		// Add the list ordering clause.
		$orderCol = $this->state->get('list.ordering');
		$orderDirn = $this->state->get('list.direction');
        if(!empty($orderCol)) {
		    $query->order($db->quote($orderCol . ' ' . $orderDirn));
        }

		return $query;
	}


}

?>
