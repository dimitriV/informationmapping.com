<?php

// Check to ensure this file is included in Joomla!
defined('_JEXEC') or die('Restricted access');

jimport('joomla.form.formfield');

class JFormFieldCourseinstructor extends JFormField
{

	protected $type = 'Courseinstructor';

	public function getLabel()
	{
		return '<span style="text-decoration: underline;">' . parent::getLabel() . '</span>';
	}

	public function getInput()
	{
		return '<select id="' . $this->id . '" name="' . $this->name . '">'
				.'<option value="'.$this->value.'" selected="selected">'.JText::_('COM_COURSES_SELECT').'</option>'
				. '</select>';
	}

}