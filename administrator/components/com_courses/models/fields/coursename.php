<?php
/**
 * @version		$Id: categoryparent.php 20196 2011-01-09 02:40:25Z ian $
 * @copyright	Copyright (C) 2005 - 2011 Open Source Matters, Inc. All rights reserved.
 * @license		GNU General Public License version 2 or later; see LICENSE.txt
 */

defined('JPATH_BASE') or die;

jimport('joomla.html.html');
jimport('joomla.form.formfield');
jimport('joomla.form.helper');
JFormHelper::loadFieldClass('list');

/**
 * Form Field class for the Joomla Framework.
 *
 * @package		Joomla.Administrator
 * @subpackage	com_categories
 * @since		1.6
 */
class JFormFieldCourseName extends JFormFieldList
{
	/**
	 * The form field type.
	 *
	 * @var		string
	 * @since	1.6
	 */
	protected $type = 'CourseName';

	/**
	 * Method to get the field options.
	 *
	 * @return	array	The field option objects.
	 * @since	1.6
	 */
	protected function getOptions()
	{
		// Initialise variables.
		$options = array();


		$db		= JFactory::getDbo();
		$query	= $db->getQuery(true);
		$query->select('a.id AS value, a.language_name AS text');
		$query->from('#__course_courselanguages AS a');
        $query->where('a.state = 1 ');


		// Prevent parenting to children of this item.
		if ($id = $this->form->getValue('id')) {

        	$rowQuery	= $db->getQuery(true);
			$rowQuery->select('a.id AS value, a.language_name AS text');
			$rowQuery->from('#__course_courselanguages AS a');
            $rowQuery->where('a.state = 1');
			$db->setQuery($rowQuery);
			$row = $db->loadObject();
		}

    	$query->group('a.id');
		// Get the options.
		$db->setQuery($query);

	    $options = $db->loadObjectList();

		// Check for a database error.
		if ($db->getErrorNum()) {
			JError::raiseWarning(500, $db->getErrorMsg());
		}

		return $options;
	}
    	public function getOption()
	{
		// Initialize variables.
		$option = array();

		$db		= JFactory::getDbo();
		$query	= $db->getQuery(true);

		$query->select('a.id AS value, a.language_name AS text');
		$query->from('#__course_courselanguages AS a');
        $query->where('a.state = 1 ');
		$query->order('a.language_name');

		// Get the options.
		$db->setQuery($query);

		$option = $db->loadObjectList();

		// Check for a database error.
		if ($db->getErrorNum()) {
			JError::raiseWarning(500, $db->getErrorMsg());
		}

		return $option;
	}
}