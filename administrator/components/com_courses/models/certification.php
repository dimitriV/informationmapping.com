<?php

/**
 * @version     $Id:certification.php  2011-12-20 04:13:25Z $
 * @package     IMI
 * @subpackage  com_courses
 * @author      ITOffshore Nepal
 * @copyright   Copyright (C) 2011 ITOffshore Nepal.
 * @license     ITOffshore Nepal
 */
// No direct access.
defined('_JEXEC') or die;

jimport('joomla.application.component.modeladmin');

/**
 * Certification model.
 *
 * @package		Joomla.Administrator
 * @subpackage          com_courses
 * @since		1.6
 */
class CoursesModelCertification extends JModelAdmin
{

	/**
	 * change status of used key
	 * so that it cant be used further
	 * @param type $key
	 * @return type
	 */
	public function disableUsedStudentKeys($key)
	{
		$query = 'UPDATE #__course_studentkeys SET status = "1"  WHERE studentkey="' . $key . '"';
		$this->_db->setQuery($query);
		if (!$this->_db->query()) {
			return false;
		}
		return true;
	}

	/**
	 * Returns a reference to the a Table object, always creating it.
	 *
	 * @param	type	The table type to instantiate
	 * @param	string	A prefix for the table class name. Optional.
	 * @param	array	Configuration array for model. Optional.
	 * @return	JTable	A database object
	 * @since	1.6
	 */
	public function getTable($type = 'Certificates', $prefix = 'CoursesTable', $config = array())
	{
		return JTable::getInstance($type, $prefix, $config);
	}

	/**
	 * get published coure instructors
	 * @return type  array
	 */
	public function getCourseInstructor($partnerId = "")
	{
		$lang = & JFactory::getLanguage();
		$langTag = $lang->getTag();
		if (!$partnerId) {
			return array();
		}
		$query = $this->_db->getQuery(true);

		$query->select('id, partnerid, name')
				->from('#__course_instructors')
				->where('state = 1')
				//->where('language in (' . '"' . $langTag . '"' . ',' . '"*"' . ')')
				->where('partnerid=' . (int) $partnerId)
				->order('name');
		$this->_db->setQuery($query);

		return $this->_db->loadAssocList();
	}

	/**
	 * get published coures
	 * @return type  array
	 */
	public function getCourseName($id = '')
	{
		$lang = & JFactory::getLanguage();
		$langTag = $lang->getTag();
		if (!$id) {
			return array();
		}
		$query = $this->_db->getQuery(true);

		$query->select('id ,courselanguageid, title')
				->from('#__course_courses')
				->where('state = 1')
				//->where('language in (' . '"' . $langTag . '"' . ',' . '"*"' . ')')
				->where('courselanguageid=' . (int) $id)
				->order('title');

		$this->_db->setQuery($query);

		return $this->_db->loadAssocList();
	}

	/**
	 * Method to get the record form.
	 *
	 * @param	array	$data		An optional array of data for the form to interogate.
	 * @param	boolean	$loadData	True if the form is to load its own data (default case), false if not.
	 * @return	JForm	A JForm object on success, false on failure
	 * @since	1.6
	 */
	public function getForm($data = array(), $loadData = true)
	{
		// Initialise variables.
		$app = JFactory::getApplication();

		// Get the form.
		$form = $this->loadForm('com_courses.certification', 'certification', array('control' => 'jform', 'load_data' => $loadData));

		if (empty($form)) {
			return false;
		}
		return $form;
	}

	/**
	 * Method to get the data that should be injected in the form.
	 *
	 * @return	mixed	The data for the form.
	 * @since	1.6
	 */
	protected function loadFormData()
	{
		// Check the session for previously entered form data.
		$data = JFactory::getApplication()->getUserState('com_courses.edit.certification.data', array());

		if (empty($data)) {
			$data = $this->getItem();
		}

		return $data;
	}

	/**
	 * Method to get a single record.
	 *
	 * @param	integer	The id of the primary key.
	 *
	 * @return	mixed	Object on success, false on failure.
	 * @since	1.6
	 */
	public function getItem($pk = null)
	{
		if(!$pk > 0) $pk = JRequest::getInt('id');
        if(!empty($this->item)) {
            return $this->item;
        }

		$params = JComponentHelper::getParams('com_courses');
		$item = array();
		$query = $this->_db->getQuery(true);

		$query->select('c.*, i.name AS instructorname, i.email AS instructoremail, u.email as studentemail, u.username')
				//->select('(CASE WHEN `status`=0 THEN "Pending" WHEN `status`=1 THEN "Approved" ELSE "Denied" END) as status')
				->select('DATE_FORMAT(c.date_from, "%M %d, %Y") as date_from, DATE_FORMAT(c.date_to, "%M %d, %Y") as date_to, DATE_FORMAT(c.created, "%M %d, %Y") as created')
				->select('co.title as coursetitle, p.title as partnertitle, cl.language_name as languagetitle')
				->from('#__course_certificates as c')
				->join('LEFT', '`#__course_courses` AS co ON co.id = c.course_id')
				->join('LEFT', '`#__course_courselanguages` AS cl ON cl.id = c.course_language_id')
				->join('LEFT', '`#__course_partners` As p ON p.id = c.partner_id')
				->join('LEFT', '`#__course_instructors` AS i ON i.id = c.instructor_id')
				->join('LEFT', '`#__users` AS u ON u.id = c.user_id')
				->where('c.id =' . (int) $pk);

		$this->_db->setQuery($query);

		$item = $this->_db->loadObject();

		$item->instructoremail = empty($item->instructoremail) ? $params->get('defaultemail') : $item->instructoremail;

		if (empty($item->instructorname)) {
			if (isset($item->instructor_name))
				$item->instructorname = $item->instructor_name;
		}
		else {
			$item->instructorname = $item->instructorname;
		}

        $this->item = $item;
		return $item;
	}

	/**
	 * approve certification
	 * @param type $studentKey
	 * @param type $filename
	 * @return type
	 */
	public function approveCertification($studentKey, $uniqid)
	{
		if (!$studentKey || !$uniqid) {
			return false;
		}

		// checking the combination of the student key and uniqid i.e filename ...for now

		$query = $this->_db->getQuery(true);

		$query->select('c.id, c.user_id, c.first_name, c.last_name, c.instructor_id, c.instructor_name, c.created, DATE_FORMAT(c.date_to, "%M %d, %Y") date_to, c.date_from as start_date, c.date_to as end_date, c.student_key, i.name AS instructorname, i.email AS instructoremail, u.email as studentemail, co.title')
				->from('#__course_certificates as c')
				->join('LEFT', '`#__course_instructors` AS i ON i.id = c.instructor_id')
				->join('LEFT', '`#__course_courses` AS co ON c.course_id = co.id')
				->join('LEFT', '`#__users` AS u ON u.id = c.user_id')
				->where('c.student_key = "' . trim($studentKey) . '"')
				->where('c.uniqid = "' . $uniqid . '"');
		//->where('c.status = 1');

		$this->_db->setQuery($query);

		$row = $this->_db->loadAssoc();

		if (!is_array($row)) {
			return false;
		}

		// update the status to approve on certifactes table
		$pdfFileName = uniqid() . '.pdf';
		$query = "UPDATE #__course_certificates SET status = '1', filename = '" . $pdfFileName . "'  WHERE id =" . (int) $row['id'];
		$this->_db->setQuery($query);
		if (!$this->_db->query()) {
			return false;
		}
		// update row array...
		$row['filename'] = $pdfFileName;
		return $row;
	}

	/**
	 * disapprove certification
	 * @param type $studentKey
	 * @param type $filename
	 * @return type
	 */
	public function disapproveCertification($studentKey, $uniqid)
	{
		if (!$studentKey || !$uniqid) {
			return false;
		}

		// checking the combination of the student key and uniqid i.e filename ...for now

		$query = $this->_db->getQuery(true);

		$query->select('c.id, c.user_id, c.filename,  DATE_FORMAT(c.date_to, "%M %d, %Y") date_to, c.date_from as start_date, c.date_to as end_date, c.first_name, c.last_name, c.instructor_id, c.instructor_name, c.created , c.student_key,  i.name AS instructorname, i.email AS instructoremail, u.email as studentemail, co.title')
				->from('#__course_certificates as c')
				->join('LEFT', '`#__course_instructors` AS i ON i.id = c.instructor_id')
				->join('LEFT', '`#__course_courses` AS co ON c.course_id = co.id')
				->join('LEFT', '`#__users` AS u ON u.id = c.user_id')
				->where('c.student_key = "' . trim($studentKey) . '"')
				->where('c.uniqid = "' . $uniqid . '"');
		//->where('c.status = 0');

		$this->_db->setQuery($query);

		$row = $this->_db->loadAssoc();

		if (!is_array($row)) {
			return false;
		}

		// update the status to disapprove on certifactes table
		$query = "UPDATE #__course_certificates SET status = '-1', filename = ''  WHERE id =" . (int) $row['id'];
		$this->_db->setQuery($query);

		if (!$this->_db->query()) {
			return false;
		}

		return $row;
	}

	/**
	 * get email template according to template key
	 * @param type $template_key, template key refering particular template
	 * @return type array
	 */
	public function getEmailTemplate($template_key)
	{
		$lang = & JFactory::getLanguage();
		$langTag = $lang->getTag();

		$query = $this->_db->getQuery(true);

		$query->select('email_subject , description, template_key')
				->from('#__course_emailtemplates');

		if ($template_key) {
			$query->where("template_key='" . $template_key . "'");
		}

		$query->where('language="' . $langTag . '"');

		$this->_db->setQuery($query);

		return $this->_db->loadAssoc();
	}

}

?>
