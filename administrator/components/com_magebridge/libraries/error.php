<?php
/**
 * Joomla! component Dynamic404
 *
 * @author Yireo
 * @package Dynamic404
 * @copyright Copyright 2011
 * @license GNU Public License
 * @link http://www.yireo.com/
 */

// No direct access
defined( '_JEXEC' ) or die( 'Restricted access' );

// Include the 404 Helper
require_once JPATH_ADMINISTRATOR.DS.'components'.DS.'com_dynamic404'.DS.'helpers'.DS.'helper.php';

// Instantiate the helper with the argument of how many matches to show
if($this instanceof Dynamic404Helper) {
    $helper = $this;
} else {
    $helper = new Dynamic404Helper($this->error);
}

// Parse empty variables and/or objects
if(empty($this->error)) $this->error = $helper->getErrorObject();
if(empty($this->title)) $this->title = JText::_('Not Found');

// Get the possible matches
$matches = $helper->getMatches();

// Get the last segment - nice for searching
$urilast = $helper->getLast();

// If no redirect is available or performed, show the page below
?>
<!DOCTYPE HTML>
<html>
<head>
	<title><?php echo $this->error->get('code') ?> - <?php echo $this->title; ?></title>
	<link rel="stylesheet" href="<?php echo JURI::base(); ?>/templates/system/css/error.css" type="text/css" />
</head>
<body>
	<div align="center">
		<div id="outline">
		<div id="errorboxoutline">
			<div id="errorboxheader"><?php echo $this->error->get('code') ?> - <?php echo $this->error->get('message') ?></div>
			<div id="errorboxbody">
            <p>
                <?php if(!empty($matches)) { ?>
                <?php echo JText::_( 'The following matches were found' ); ?>:
                <ul>
                    <?php foreach($matches as $item) { ?>
                    <li><a href="<?php echo $item->url; ?>"><?php echo $item->name; ?></a> (<?php echo $item->rating; ?>%)</li>
                    <?php } ?>
                </ul>
                <?php } else { ?>
                    <?php echo JText::_('No matches found'); ?>
                <?php } ?>
            </p>
			<p>
                <?php echo JText::_( 'Alternative actions' ); ?>:
				<ul>
					<li><a href="<?php echo JURI::base(); ?>" title="<?php echo JText::_('Go to the home page'); ?>"><?php echo JText::_('Home Page'); ?></a></li>
					<li><a href="<?php echo JRoute::_( 'index.php?option=com_search&searchword='.$urilast ); ?>" title="<?php echo JText::_('Search');
					?>"><?php echo JText::_('Search for'); ?>: <?php echo $urilast; ?></a></li>
				</ul>
			</p>
			<p><?php echo JText::_('If difficulties persist, please contact the system administrator of this site.'); ?></p>
			<div id="techinfo">
			<p><?php echo $this->error->get('message'); ?></p>
			<p>
				<?php if(isset($this->debug) && $this->debug == true) : ?>
					<?php echo $this->renderBacktrace(); ?>
				<?php endif; ?>
			</p>
			</div>
			</div>
		</div>
		</div>
	</div>
</body>
</html>
