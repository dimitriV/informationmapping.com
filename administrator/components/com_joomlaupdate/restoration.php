<?php
defined('_AKEEBA_RESTORATION') or die('Restricted access');
$restoration_setup = array(
	'kickstart.security.password' => '4ZFA7JijUHDaDSSWgm3VwBgovr2xAkGt',
	'kickstart.tuning.max_exec_time' => '5',
	'kickstart.tuning.run_time_bias' => '75',
	'kickstart.tuning.min_exec_time' => '0',
	'kickstart.procengine' => 'direct',
	'kickstart.setup.sourcefile' => '/var/www/html/www.informationmapping.com/tmp/Joomla_3.5.1-Stable-Update_Package.zip',
	'kickstart.setup.destdir' => '/var/www/html/www.informationmapping.com',
	'kickstart.setup.restoreperms' => '0',
	'kickstart.setup.filetype' => 'zip',
	'kickstart.setup.dryrun' => '0');