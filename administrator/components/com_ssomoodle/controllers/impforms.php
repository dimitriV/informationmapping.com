<?php
/**
 * @version     $Id:impforms.php  2011-12-20 04:13:25Z $
 * @package     IMI 
 * @subpackage  com_ssomoodle
 * @author      ITOffshore Nepal
 * @copyright   Copyright (C) 2011 ITOffshore Nepal.
 * @license     ITOffshore Nepal
 */

// No direct access.
defined('_JEXEC') or die;

jimport('joomla.application.component.controlleradmin');

/**
 * CourseNames list controller class.
 *
 * @package		Joomla.Administrator
 * @subpackage	com_ssomoodle
 * @since		1.6
 */
class SsoMoodleControlleImpforms extends JControllerAdmin
{
	/**
	 * Proxy for getModel.
	 * @since	1.6
	 */
    protected $text_prefix = 'COM_SSOMOODLE';
     
	public function getModel($name = 'Impform', $prefix = 'SsoMoodleModel', $config = array('ignore_request' => true))
	{
		$model = parent::getModel($name, $prefix, $config);
		return $model;
	}


}
?>