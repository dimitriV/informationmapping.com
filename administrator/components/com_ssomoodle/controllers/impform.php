<?php
/**
 * @version     $Id:impform.php  2011-12-20 04:13:25Z $
 * @package     IMI 
 * @subpackage  com_ssomoodle
 * @author      ITOffshore Nepal
 * @copyright   Copyright (C) 2011 ITOffshore Nepal.
 * @license     ITOffshore Nepal
 */

// No direct access
defined('_JEXEC') or die;

jimport('joomla.application.component.controller');

/**
 * Coursename controller class.
 *
 * @package		Joomla.Administrator
 * @subpackage	com_courses
 * @since		1.6
 */
class SsoMoodleControllerImpform extends JControllerLegacy
{
	/**
	 * cancel editing a record
	 * @return void
	 */
	public function cancel() 
    {
		$msg = JText::_('Canceled');
		$this->setRedirect('index.php?option=com_ssomoodle&view=impforms', $msg);
	}
}
