<?php
/**
 * @version     $Id:coursename.php  2011-12-20 04:13:25Z $
 * @package     IMI 
 * @subpackage  com_courses
 * @author      ITOffshore Nepal
 * @copyright   Copyright (C) 2011 ITOffshore Nepal.
 * @license     ITOffshore Nepal
 */

// No direct access.
defined('_JEXEC') or die;

jimport('joomla.application.component.controlleradmin');

/**
 * CourseNames list controller class.
 *
 * @package		Joomla.Administrator
 * @subpackage	com_courses
 * @since		1.6
 */
class SsoMoodleControllerCourses extends JControllerAdmin
{
	/**
	 * Proxy for getModel.
	 * @since	1.6
	 */
    protected $text_prefix = 'COM_SSOMOODLE';
     
	public function getModel($name = 'Course', $prefix = 'SsoMoodleModel', $config = array('ignore_request' => true))
	{
		$model = parent::getModel($name, $prefix, $config);
		return $model;
	}
	
	
		
		/**
	 * Method to publish a list of taxa
	 *
	 * @return  void
	 *
	 * @since   11.1
	 */
	function publish()
	{
		// Check for request forgeries
		JRequest::checkToken() or die(JText::_('JINVALID_TOKEN'));

		$session	= JFactory::getSession();
		$registry	= $session->get('registry');

		// Get items to publish from the request.
		$cid	= JRequest::getVar('cid', array(), '', 'array');
		$data	= array('publish' => 1, 'unpublish' => 0, 'archive'=> 2, 'trash' => -2, 'report'=>-3);
		$task 	= $this->getTask();
		$value	= JArrayHelper::getValue($data, $task, 0, 'int');
		/*$checkvalue ='';
		if($value == -2)
		{
				$model	=	$this->getModel('course');
				$checkvalue = $model->checkvalue();
		}*/
		
		if (empty($cid)) {
			JError::raiseWarning(500, JText::_($this->text_prefix.'_NO_ITEM_SELECTED'));
		}
		else {
			// Get the model.
			/*if($checkvalue)
			{
				$msg = JText::sprintf('COM_COURSES_CANNOT_DELETED');
				$type = 'error';
				$this->setRedirect('index.php?option=com_courses&view=courses', $msg, $type);
			}
			else{	*/
			
				$model = $this->getModel('course');
				
				// Make sure the item ids are integers
				JArrayHelper::toInteger($cid);
	
				// Publish the items.
				if (!$model->publish($cid, $value)) {
					JError::raiseWarning(500, $model->getError());
				}
				else {
					if ($value == 1) {
						$ntext = $this->text_prefix.'_N_ITEMS_PUBLISHED';
					}
					else if ($value == 0) {
						$ntext = $this->text_prefix.'_N_ITEMS_UNPUBLISHED';
					}
					else if ($value == 2) {
						$ntext = $this->text_prefix.'_N_ITEMS_ARCHIVED';
					}
					else {
						$ntext = $this->text_prefix.'_N_ITEMS_TRASHED';
					}
					$this->setMessage(JText::plural($ntext, count($cid)));
				}
			//}
			$extension = JRequest::getCmd('extension');
			$extensionURL = ($extension) ? '&extension=' . JRequest::getCmd('extension') : '';
			$this->setRedirect(JRoute::_('index.php?option='.$this->option.'&view='.$this->view_list.$extensionURL, false));
		}
		

	}


}
?>