<?php
/**
 * @version     $Id:view.html.php  2011-12-20 04:13:25Z $
 * @package     IMI 
 * @subpackage  com_ssomoodle
 * @author      ITOffshore Nepal
 * @copyright   Copyright (C) 2011 ITOffshore Nepal.
 * @license     ITOffshore Nepal
 */

// No direct access
defined('_JEXEC') or die;

jimport('joomla.application.component.view');

/**
 * View class for a list of courses.
 *
 * @package		Joomla.Administrator
 * @subpackage	com_courses
 * @since		1.5
 */
class SsoMoodleViewCourses extends JViewLegacy
{
	protected $items;
	protected $pagination;
	protected $state;

	/**
	 * Display the view
	 */
	public function display($tpl = null)
	{
		$this->state		= $this->get('State');
		$this->items		= $this->get('Items');
		$this->pagination	= $this->get('Pagination');

		// Check for errors.
		if (count($errors = $this->get('Errors'))) {
			JError::raiseError(500, implode("\n", $errors));
			return false;
		}

		$this->addToolbar();
        
		parent::display($tpl);
	}

	/**
	 * Add the page title and toolbar.
	 *
	 * @since	1.6
	 */
	protected function addToolbar()
	{
		require_once JPATH_COMPONENT.'/helpers/ssomoodle.php';

		$state	= $this->get('State');
		$canDo	= SsoMoodleHelper::getActions();
		$user	= JFactory::getUser();
        
		JToolBarHelper::title(JText::_('COM_SSOMOODLE_MANAGER_COURSES'), 'courses.png');
		JToolBarHelper::addNew('course.add');
	
		if ($canDo->get('core.edit')) {
			JToolBarHelper::editList('course.edit');
		}
		if ($canDo->get('core.edit.state')) {
			JToolBarHelper::divider();
			JToolBarHelper::publish('courses.publish', 'JTOOLBAR_PUBLISH', true);
			JToolBarHelper::unpublish('courses.unpublish', 'JTOOLBAR_UNPUBLISH', true);
			JToolBarHelper::divider();
			JToolBarHelper::archiveList('courses.archive');
			JToolBarHelper::checkin('courses.checkin');
		}
		if ($state->get('filter.state') == -2 && $canDo->get('core.delete')) {
			JToolBarHelper::deleteList('', 'courses.delete', 'JTOOLBAR_EMPTY_TRASH');
			JToolBarHelper::divider();
		} else if ($canDo->get('core.edit.state')) {
			JToolBarHelper::trash('courses.trash');
			JToolBarHelper::divider();
		}
		
	}
}
?>