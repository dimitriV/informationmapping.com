<?php
/**
 * @version     $Id:default.php  2011-12-20 04:13:25Z $
 * @package     IMI 
 * @subpackage  com_ssomoodle
 * @author      ITOffshore Nepal
 * @copyright   Copyright (C) 2011 ITOffshore Nepal.
 * @license     ITOffshore Nepal
 */

// no direct access
defined('_JEXEC') or die;
?>

<script type="text/javascript">
	Joomla.submitbutton = function(task)
	{
		if (task == 'impform.cancel') {
	
            window.location = '<?php echo JRoute::_('index.php?option=com_ssomoodle&view=impforms', false); ?>';
		}
	
	}
</script>
<form action="#" method="post" name="adminForm" id="impform-form">
	<div class="full_width">
		<fieldset class="adminform">
			<legend><?php echo JText::sprintf('COM_SSOMOODLE_IMPFORM_DETAIL'); ?></legend>
			<table>
					<tr>
						<td><label>	<?php echo JText::_('COM_SSOMOODLE_USER_NAME'); ?></label></td>
						<td> <?php echo $this->item->name; ?></td>
					</tr>
					<tr>
						<td><label><?php echo JText::_('COM_SSOMOODLE_ADDRESS'); ?></label></td>
						<td><?php echo $this->item->address; ?></td>
					</tr>
					<tr>
						<td><label><?php echo JText::_('COM_SSOMOODLE_POSTAL_CODE'); ?></label></td>
						<td><?php echo $this->item->postal_code; ?></td>
					</tr>
					<tr>
						<td><label><?php echo JText::_('COM_SSOMOODLE_TELEPHONE'); ?></label></td>
						<td><?php echo $this->item->telephone; ?></td>
					</tr>
					<tr>
						<td><label><?php echo JText::_('COM_SSOMOODLE_FAX'); ?></label></td>
						<td><?php echo $this->item->fax; ?></td>
					</tr>
					<tr>
						<td><label><?php echo JText::_('COM_SSOMOODLE_EXPERIENCE'); ?></label></td>
						<td><?php echo $this->item->experience; ?></td>
					</tr>
					<tr>
						<td><label><?php echo JText::_('COM_SSOMOODLE_CURRENT_COMPANY'); ?></label></td>
						<td><?php echo $this->item->current_company; ?></td>
					</tr>
					<tr>
						<td><label><?php echo JText::_('COM_SSOMOODLE_CURRENT_FUNCTION'); ?></label></td>
						<td><?php echo $this->item->current_function; ?></td>
					</tr>
					<tr>
						<td><label><?php echo JText::_('COM_SSOMOODLE_CURRENT_FUNCTION_DESC'); ?></label></td>
						<td><?php echo $this->item->current_function_desc; ?></td>
					</tr>
					<tr>
						<td><label><?php echo JText::_('COM_SSOMOODLE_WEBSITE'); ?></label></td>
						<td><?php echo $this->item->company_website; ?></td>
					</tr>
					<tr>
						<td><label><?php echo JText::_('COM_SSOMOODLE_IMI_TRANNING'); ?></label></td>
						<td><?php echo ($this->item->followed_im_training ? JText::_('JYES') : JText::_('JNO')); ?></td>
					</tr>
					<tr>
						<td><label><?php echo JText::_('COM_SSOMOODLE_TRAINING_COMPANY'); ?></label></td>
						<td><?php echo $this->item->training_company; ?></td>
					</tr>
					<tr>
						<td><label><?php echo JText::_('COM_SSOMOODLE_COURSE'); ?></label></label></td>
						<td><?php echo $this->item->course; ?></td>
					</tr>
					<tr>
						<td><label><?php echo JText::_('COM_SSOMOODLE_COURSE_DATE'); ?></label></td>
						<td><?php echo $this->item->course_date; ?></td>
					</tr>
					<tr>
						<td><label><?php echo JText::_('COM_SSOMOODLE_STUDENT_KEY'); ?></label></td>
						<td><?php echo $this->item->student_key; ?></td>
					</tr>
					<tr>
						<td><label><?php echo JText::_('COM_SSOMOODLE_CREATED'); ?></label></td>
						<td><?php echo $this->item->created; ?></td>
					</tr>
				</table>
		

		</fieldset>
			<input type="hidden" name="task" value="" />
	</div>

	<div class="clr"></div>
</form>





