<?php
/**
 * @version     1.0.0
 * @package     com_ssomoodle
 * @copyright   Copyright (C) 2012. All rights reserved.
 * @license     GNU General Public License version 2 or later; see LICENSE.txt
 * @author      Created by com_combuilder - http://www.notwebdesign.com
 */

// No direct access
defined('_JEXEC') or die;

jimport('joomla.application.component.view');

class SsoMoodleViewSsoMoodle extends JViewLegacy
{
	/**
	 * Display the view
	 */
	public function display($tpl = null)
	{
		$this->addToolbar();
		parent::display($tpl);
	}

	/**
	 * Add the page title and toolbar.
	 *
	 * @since	1.6
	 */
	protected function addToolbar()
	{
		require_once JPATH_COMPONENT.DIRECTORY_SEPARATOR.'helpers'.DIRECTORY_SEPARATOR.'ssomoodle.php';

		$canDo	= SsoMoodleHelper::getActions();

		JToolBarHelper::title(JText::_('COM_SSOMOODLE_TITLE'));

		if ($canDo->get('core.admin')) {
			JToolBarHelper::preferences('com_ssomoodle');
		}
	}
}
