<?php
/**
 * @version     $Id:default.php  2011-12-20 04:13:25Z $
 * @package     IMI 
 * @subpackage  com_ssomoodle
 * @author      ITOffshore Nepal
 * @copyright   Copyright (C) 2011 ITOffshore Nepal.
 * @license     ITOffshore Nepal
 */

// no direct access
defined('_JEXEC') or die;

JHtml::addIncludePath(JPATH_COMPONENT.'/helpers/html');
JHtml::_('behavior.tooltip');
JHtml::_('behavior.multiselect');
$listOrder	= $this->escape($this->state->get('list.ordering'));
$listDirn	= $this->escape($this->state->get('list.direction'));

?>

<form action="<?php echo JRoute::_('index.php?option=com_ssomoodle&view=impforms'); ?>" method="post" name="adminForm" id="adminForm">
	<fieldset id="filter-bar" style="height:120px">
		<div class="filter-search fltlft">
			<label class="filter-search-lbl" for="filter_search"><?php echo JText::_('JSEARCH_FILTER_LABEL'); ?></label>
			<input type="text" name="filter_search" id="filter_search" value="<?php echo $this->escape($this->state->get('filter.search')); ?>" title="<?php echo JText::_('COM_SSOMOODLE_SEARCH_IN_TITLE'); ?>" />
			<button type="submit"><?php echo JText::_('JSEARCH_FILTER_SUBMIT'); ?></button>
			<button type="button" onclick="document.id('filter_search').value='';this.form.submit();"><?php echo JText::_('JSEARCH_FILTER_CLEAR'); ?></button>
		</div>
		<div class="filter-select fltrt">
		  	<select name="filter_category_id" class="inputbox" onchange="this.form.submit()">
            	<option value=""><?php echo JText::_('COM_SSOMOODLE_SELECT_COURSENAME');?></option>
            	<?php echo JHtml::_('select.options', JFormFieldCourse::getOption(), 'value', 'text', $this->state->get('filter.category_id'));?>
            </select>
		</div>
	</fieldset>
	<div class="clr"> </div>

	<table class="table table-striped">
		<thead>
			<tr>
				<th width="1%">
					<input type="checkbox" name="checkall-toggle" value="" title="<?php echo JText::_('JGLOBAL_CHECK_ALL'); ?>" onclick="Joomla.checkAll(this)" />
				</th>
				<th class="title">
					<?php echo JHtml::_('grid.sort','COM_SSOMOODLE_USER_NAME_LIST','u.name', $listDirn, $listOrder); ?>
				</th>
					<th class="title">
					<?php echo JHtml::_('grid.sort','COM_SSOMOODLE_ADDRESS_LIST' ,'a.address', $listDirn, $listOrder); ?>
				</th>
					<th class="title">
					<?php echo  JHtml::_('grid.sort','COM_SSOMOODLE_COURSE_LIST','m.title', $listDirn, $listOrder); ?>
				</th>
					<th class="title">
					<?php echo  JHtml::_('grid.sort','COM_SSOMOODLE_STUDENT_KEY_LIST', 'a.student_key', $listDirn, $listOrder); ?>
				</th>
			
				<th width="1%" class="nowrap">
					<?php echo JHtml::_('grid.sort',  'JGRID_HEADING_ID', 'a.id', $listDirn, $listOrder); ?>
				</th>
			</tr>
		</thead>
		<tfoot>
			<tr>
				<td colspan="10">
					<?php echo $this->pagination->getListFooter(); ?>
				</td>
			</tr>
		</tfoot>
		<tbody>
	
		<?php foreach ($this->items as $i => $item) :
			?>
			<tr class="row<?php echo $i % 2; ?>">
				<td class="center">
					<?php echo JHtml::_('grid.id', $i, $item->id); ?>
				</td>
				<td class="center">
					<a href="<?php echo JRoute::_('index.php?option=com_ssomoodle&view=impform&task=impform.edit&id='.(int) $item->id); ?>">
							<?php echo $this->escape($item->name); ?></a>
				</td>
				<td class="center">
					<?php echo $item->address; ?>
				</td>
				<td class="center">
					<?php echo $item->course; ?>
				</td>
				<td class="center">
					<?php echo $item->student_key; ?>
				</td>
				<td class="center">
					<?php echo (int) $item->id; ?>
				</td>
			</tr>
			<?php endforeach; ?>
		</tbody>
	</table>

	<div>
		<input type="hidden" name="task" value="" />
		<input type="hidden" name="boxchecked" value="0" />
		<input type="hidden" name="filter_order" value="<?php echo $listOrder; ?>" />
		<input type="hidden" name="filter_order_Dir" value="<?php echo $listDirn; ?>" />
		<?php echo JHtml::_('form.token'); ?>
	</div>
</form>
