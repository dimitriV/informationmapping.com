<?php
/**
 * @version     $Id: frontpagearticle.php  2010-10-23 04:13:25Z $
 * @package     Information mapping
 * @subpackage  com_frontpagearticles
 * @author      ITOffshore Nepal
 * @copyright   Copyright (C) 2011 ITOffshore Nepal.
 * @license     ITOffshore Nepal
 **/
// Check to ensure this file is included in Joomla!
defined('_JEXEC') or die('Restricted access'); 

jimport( 'joomla.application.component.modelitem' );

class SsoMoodleModelImpform extends JModelItem
{
	/**
	 * Model context string.
	 *
	 * @var		string
	 */
	protected $_context = 'com_ssomoodle.impform';

	/**
	 * Method to auto-populate the model state.
	 *
	 * Note. Calling getState in this method will result in recursion.
	 *
	 * @since	1.6
	 */
	protected function populateState()
	{
		$app = JFactory::getApplication('site');

		// Load state from the request.
		$pk = JRequest::getInt('id');
		$this->setState('id', $pk);

		// Load the parameters.
		//$params = $app->getParams();
		//$this->setState('params', $params);
	}
	
	/**
	 * Method to get article data.
	 *
	 * @param	integer	The id of the article.
	 *
	 * @return	mixed	Menu item data object on success, false on failure.
	 */
	public function &getItem($pk = null)
	{
		// Initialise variables.
		$pk = (!empty($pk)) ? $pk : (int) $this->getState('id');

		if ($this->_item === null) {
			$this->_item = array();
		}

		if (!isset($this->_item[$pk])) {

			try {
				$db = $this->getDbo();
				$query = $db->getQuery(true);


				$query->select('a.*');
				$query->from('`#__moodle_impforms` AS a');

				//// Join over the usertable
				$query->select('u.username AS username,u.name AS name');
				$query->join('LEFT', '`#__users` AS u ON u.id = a.userid');
	
				// Join over the moodle course.
				$query->select('m.title AS course');
				$query->join('LEFT', '#__moodle_courses AS m ON m.id=a.course_id where a.id='.$pk);

				
				$db->setQuery($query);

				$data = $db->loadObject();

				if ($error = $db->getErrorMsg()) {
					throw new Exception($error);
				}

				if (empty($data)) {
					return JError::raiseError(404,JText::_('COM_SSOMOODEL_ERROR_IMPFORM_NOT_FOUND'));
				}

				$this->_item[$pk] = $data;
			}
			catch (JException $e)
			{
				if ($e->getCode() == 404) {
					// Need to go thru the error handler to allow Redirect to work.
					JError::raiseError(404, $e->getMessage());
				}
				else {
					$this->setError($e);
					$this->_item[$pk] = false;
				}
			}
		}

		return $this->_item[$pk];
	}
}
?>