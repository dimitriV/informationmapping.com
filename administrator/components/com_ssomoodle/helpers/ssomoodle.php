<?php
/**
 * @version     1.0.0
 * @package     com_ssomoodle
 * @copyright   Copyright (C) 2012. All rights reserved.
 * @author      IT Offshore Nepal
 */

// No direct access
defined('_JEXEC') or die;

/**
 * SsoMoodle helper.
 */
class SsoMoodleHelper
{
	/**
	 * Configure the Linkbar.
	 *
	 * @param	string	The name of the active view.
	 * @since	1.6
	 */
	public static function addSubmenu($vName = 'ssomoodle')
	{
		JSubMenuHelper::addEntry(
			JText::_('COM_SSOMOODLE'),
			'index.php?option=com_ssomoodle&view=ssomoodle',
			$vName == 'ssomoodle'
		);
        JSubMenuHelper::addEntry(
			JText::_('COM_SSOMOODLE_COURSES'),
			'index.php?option=com_ssomoodle&view=courses',
			$vName == 'courses'
		);
		
		JSubMenuHelper::addEntry(
			JText::_('COM_SSOMOODLE_IMPFORM'),
			'index.php?option=com_ssomoodle&view=impforms',
			$vName == 'impforms'
		);
	}
	
	/**
	 * Gets a list of the actions that can be performed.
	 *
	 * @return	JObject
	 * @since	1.6
	 */
	public static function getActions()
	{
		$user	= JFactory::getUser();
		$result	= new JObject;

		$assetName = 'com_ssomoodle';

		$actions = array(
			'core.admin', 'core.manage', 'core.create', 'core.edit', 'core.edit.own', 'core.edit.state', 'core.delete'
		);

		foreach ($actions as $action) {
			$result->set($action,	$user->authorise($action, $assetName));
		}

		return $result;
	}
}
