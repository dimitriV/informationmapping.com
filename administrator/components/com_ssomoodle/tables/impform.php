<?php
/**
 * @package     IMI 
 * @subpackage  com_ssomoodle
 * @author      ITOffshore Nepal
 * @copyright   Copyright (C) 2011 ITOffshore Nepal.
 * @license     ITOffshore Nepal
 */

// No direct access
defined('_JEXEC') or die;
jimport( 'joomla.database.table' );

class SsoMoodleTableImpForm extends JTable
{
	/**
	 * Constructor
	 *
	 * @param JDatabase A database connector object
	 */
	public function __construct(&$db)
	{
		parent::__construct('#__moodle_impforms', 'id', $db);
	}


	/**
	 * Overload the store method for the Courses table.
	 *
	 * @param	boolean	Toggle whether null values should be updated.
	 * @return	boolean	True on success, false on failure.
	 * @since	1.6
	 */
	public function store($updateNulls = false)
	{
		$date	= JFactory::getDate();
		$user	= JFactory::getUser();
		if (!$this->id) {
			$this->created = $date->toSQL();
			$this->userid = $user->get('id');
		}
	
		// Attempt to store the user data.
		return parent::store($updateNulls);
	}
}
?>
