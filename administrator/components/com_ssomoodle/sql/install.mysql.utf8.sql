DROP TABLE IF EXISTS `#__moodle_courses`;

CREATE TABLE `#__moodle_courses` (
  `id` int(10) NOT NULL auto_increment,
  `title` varchar(250) NOT NULL default '',
  `state` tinyint(1) NOT NULL default '0',
  `checked_out` int(11) NOT NULL default '0',
  `checked_out_time` datetime NOT NULL default '0000-00-00 00:00:00',
  `ordering` int(11) NOT NULL default '0',
  `archived` tinyint(1) NOT NULL default '0',
  `approved` tinyint(1) NOT NULL default '1',
  `access` int(11) NOT NULL default '1',
  `language` char(7) NOT NULL default '',
  `created` datetime NOT NULL default '0000-00-00 00:00:00',
  `created_by` int(10) unsigned NOT NULL default '0',
  `created_by_alias` varchar(255) NOT NULL default '',
  `modified` datetime NOT NULL default '0000-00-00 00:00:00',
  `modified_by` int(10) unsigned NOT NULL default '0',
  `publish_up` datetime NOT NULL default '0000-00-00 00:00:00',
  `publish_down` datetime NOT NULL default '0000-00-00 00:00:00',
  PRIMARY KEY  (`id`)
) ENGINE=MyISAM AUTO_INCREMENT=1 DEFAULT CHARSET=utf8;

DROP TABLE IF EXISTS `#__moodle_impforms`;

CREATE TABLE `#__moodle_impforms` (
  `id` int(11) NOT NULL auto_increment,
  `userid` int(11) default NULL,
  `address` varchar(255) default NULL,
  `postal_code` varchar(50) default NULL,
  `state` varchar(50) default NULL,
  `telephone` varchar(50) default NULL,
  `fax` varchar(50) default NULL,
  `experience` text,
  `specialities` text,
  `current_company` varchar(255) default NULL,
  `current_function` varchar(255) default NULL,
  `current_function_desc` text,
  `company_website` varchar(255) default NULL,
  `followed_im_training` tinyint(1) default '0',
  `training_company` varchar(255) default NULL,
  `course_id` int(11) default NULL,
  `course_date` date default NULL,
  `student_key` varchar(50) default NULL,
  `created` datetime default NULL,
  PRIMARY KEY  (`id`)
) ENGINE=MyISAM AUTO_INCREMENT=1 DEFAULT CHARSET=utf8;