<?php
/**
 * @Copyright
 *
 * @package    QCC - Quick Cache Cleaning for Joomla! 3
 * @author     Viktor Vogel <admin@kubik-rubik.de>
 * @version    3-5 - 2015-02-22
 * @link       https://joomla-extensions.kubik-rubik.de/qcc-quick-cache-cleaning
 *
 * @license    GNU/GPL
 *  This program is free software: you can redistribute it and/or modify
 *  it under the terms of the GNU General Public License as published by
 *  the Free Software Foundation, either version 3 of the License, or
 *  (at your option) any later version.
 *
 *  This program is distributed in the hope that it will be useful,
 *  but WITHOUT ANY WARRANTY; without even the implied warranty of
 *  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *  GNU General Public License for more details.
 *
 *  You should have received a copy of the GNU General Public License
 *  along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */
defined('_JEXEC') or die('Restricted access');

class ModQuickCacheCleaningHelper extends JObject
{
    function __construct()
    {
        $quickcachecleaning = JFactory::getApplication()->input->get('quickcachecleaning');

        if(!empty($quickcachecleaning))
        {
            $this->executeCleanCheckin($quickcachecleaning);
        }
    }

    /**
     * Does the dirty work! :-)
     *
     * @param string $quickcachecleaning
     */
    private function executeCleanCheckin($quickcachecleaning)
    {
        if($quickcachecleaning == 'clean_cache' OR $quickcachecleaning == 'purge_cache')
        {
            // Get model from cache component to use its functions
            JLoader::import('cache', JPATH_ADMINISTRATOR.'/components/com_cache/models');
            $model = JModelLegacy::getInstance('cache', 'CacheModel');

            if($quickcachecleaning == 'clean_cache')
            {
                $model->clean();
                $message = JText::_('MOD_QUICKCACHECLEANING_CACHE_CLEANED');
            }
            elseif($quickcachecleaning == 'purge_cache')
            {
                $model->purge();
                $message = JText::_('MOD_QUICKCACHECLEANING_CACHE_PURGED');
            }
        }
        elseif($quickcachecleaning == 'check_in')
        {
            // Get model from checkin component to use its functions
            JLoader::import('checkin', JPATH_ADMINISTRATOR.'/components/com_checkin/models');
            $model = JModelLegacy::getInstance('checkin', 'CheckinModel');

            $model->checkin(array_flip(array_filter($model->getItems())));
            $message = JText::_('MOD_QUICKCACHECLEANING_CHECKED_IN');
        }

        $url = preg_replace('@[?|&]quickcachecleaning=(.+)$@', '', JUri::getInstance()->toString());
        JFactory::getApplication()->redirect($url, $message, 'notice');
    }

    /**
     * Creates the output for the module template.
     *
     * @return string
     */
    public function createOutput()
    {
        $url = JUri::getInstance()->toString();

        if(preg_match('@\?@', $url))
        {
            $url = $url.'&quickcachecleaning=';
        }
        else
        {
            $url = $url.'?quickcachecleaning=';
        }

        $output = '<ul class="nav pull-right">
                    <li class="dropdown">
                        <a href="#" data-toggle="dropdown" class="dropdown-toggle">'.JText::_('MOD_QUICKCACHECLEANING_SELECTION_QUICKCACHECLEANING').' <span class="caret"></span></a>
                        <ul class="dropdown-menu">
                            <li><a href="'.$url.'clean_cache">'.JText::_('MOD_QUICKCACHECLEANING_SELECTION_CACHE_CLEAN').'</a></li>
                            <li class="separator"><span></span></li>
                            <li><a href="'.$url.'purge_cache">'.JText::_('MOD_QUICKCACHECLEANING_SELECTION_CACHE_PURGE').'</a></li>
                            <li class="separator"><span></span></li>
                            <li><a href="'.$url.'check_in">'.JText::_('MOD_QUICKCACHECLEANING_SELECTION_GLOBAL_CHECKIN').'</a></li>
                        </ul>
                    </li>
                </ul>';

        return $output;
    }
}
