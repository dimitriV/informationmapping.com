<!DOCTYPE html>
<html>
<head>
<title>Thank you for installing FS Pro 2013</title>
<meta name="viewport" content="width=device-width, initial-scale=1.0">
<meta http-equiv="X-UA-Compatible" content="IE=edge">
<!-- Bootstrap -->
<link href="css/bootstrap.min.css" rel="stylesheet" media="screen">
<!-- HTML5 shim and Respond.js IE8 support of HTML5 elements and media queries -->
<!--[if lt IE 9]>
      <script src="js/html5shiv.js"></script>
      <script src="js/respond.min.js"></script>
    <![endif]-->
<link href="//fonts.googleapis.com/css?family=Tauri" rel="stylesheet" type="text/css" />
<link href="css/imi.css" rel="stylesheet" media="screen">
<link rel="stylesheet" href="css/font-awesome.min.css" type="text/css" media="all"/>
</head>
<body>
<div class="container">
  <div class="row">
    <div class="col-sm-12" id="header">
      <p>&nbsp;</p>
      <img src="images/logo-informationmapping.jpg" width="169" height="71" alt="Information Mapping"></div>
  </div>
  <div class="row">
    <h1>Thank you for installing FS Pro 2013</h1>
  </div>
  <div class="row">
    <div class="col-sm-4">
      <!--<h2>Special offer</h2>-->
      <p><a href="https://www.informationmapping.com/en/shop/software/fspro-2013/fs-pro-2013"><img src="images/download.jpg" class="img-responsive" alt="Download the free e-book"></a></p>
      <!--<p>To receive a discount of $ 25, use discount code <span style="color: #d4145a;">DWNL</span> when ordering <a href="https://www.informationmapping.com/en/shop/software/fspro-2013/fs-pro-2013">FS Pro 2013 on our web store</a>.</p>-->
    </div>
    <div class="col-sm-4">
      <h2>FS Pro helps you</h2>
      <ul>
        <li>break content into small, manageable pieces</li>
        <li>present your content visually using tables and lists rather than lengthy text</li>
        <li>label all the pieces for easy scanning and retrieval of information</li>
        <li>provide advance organizers so the reader can anticipate the content that follows</li>
      </ul>
      <p>Learn more with these useful FS Pro 2013 resources:</p>
      <p><a href="http://support.informationmapping.com/" target="_blank"><em class="icon-file-text-alt"></em> FS Pro 2013 Knowledge Base</a></p>
      <p> <a href="http://www.informationmapping.com/fspro2013-tutorial/" target="_blank"><em class="icon-youtube-play"></em> FS Pro 2013 video tutorial</a> </p>
    </div>
    <div class="col-sm-4" id="form">
      <h2>Download your free e-book</h2>
      <?php
// implement bootstrap 3
// emailadres mag maar 1 keer per dag deelnemen
// emailadres mag wel op een andere dag deelnemen
// er wordt een uniek random nummer gemaakt tussen 1000 en 4000
// er wordt een email verstuurd met daarin het lucky number

//include_once("connect.php");

/*************************************************
            VERZENDINGSGEGEVENS
*************************************************/

$from = "info@informationmapping.com";
/*$reply = "webmaster@wvdwebdesign.be";
$cc ="";*/
$bcc = "wvandessel@informationmapping.com";

// onderwerp mail
$onderwerp = "Your FS Pro 2013 e-book is waiting";   //See if you have won this e-reader
$onderwerp = html_entity_decode (  $onderwerp, ENT_QUOTES, 'UTF-8' ); // will convert special characters in readable format e.g. &hearts; will be converted to a heart

// Prioriteit: 1 = dringend, 3 = normaal, 5 low
$prioriteit = "1";

/*************************************************
        FORMULIERVARIABELEN BEVEILIGEN
*************************************************/

// zet gevaarlijke tekens in invoervelden om
function cleanup($string="")
{
$string = trim($string); // verwijdert spaties aan begin en einde
//$string = htmlentities($string); // converteert < > & " en andere speciale tekens naar veilige tekens
$string = htmlspecialchars($string);
$string = nl2br($string); // converteert linebreaks in textareas naar <br />
// plaatst escapetekens bij quotes indien nodig
if(!get_magic_quotes_gpc())
  {
  $string = addslashes($string);
  }

return $string;
}

// MX-record e-mailadres beveiligen - checkdnsrr() werkt niet op Windowsservers
function check_email($mail_address) { 
    $pattern = "/^[\w-]+(\.[\w-]+)*@"; 
    $pattern .= "([0-9a-z][0-9a-z-]*[0-9a-z]\.)+([a-z]{2,4})$/i"; 
    if (preg_match($pattern, $mail_address)) { 
        $parts = explode("@", $mail_address); 
        if (checkdnsrr($parts[1], "MX")){ 
            // echo "The e-mail address is valid."; 
            return true; 
        } else { 
            // echo "The e-mail host is not valid."; 
            return false; 
        } 
    } else { 
        // echo "The e-mail address contains invalid characters."; 
        return false; 
    } 
} 

/** GeoIP **/

// source: http://www.geoplugin.com/webservices/php
// uses the famous Maxmind DB
    $client  = @$_SERVER['HTTP_CLIENT_IP'];
    $forward = @$_SERVER['HTTP_X_FORWARDED_FOR'];
    $remote  = $_SERVER['REMOTE_ADDR'];
    $result  = "Unknown";
    if(filter_var($client, FILTER_VALIDATE_IP))
    {
        $ip = $client;
    }
    elseif(filter_var($forward, FILTER_VALIDATE_IP))
    {
        $ip = $forward;
    }
    else
    {
        $ip = $remote;
    }

    $ip_data = @json_decode(file_get_contents("http://www.geoplugin.net/json.gp?ip=".$ip));

    if($ip_data && $ip_data->geoplugin_countryName != null)
    {
        $visitor_country = $ip_data->geoplugin_countryName;
    }

	// if you need 2 lettre code use $ip_data->geoplugin_countryCode


//echo $visitor_country; // Output Coutry name [Ex: United States]


/*************************************************
            FORMULIERGEGEVENS
*************************************************/
$firstname = isset($_POST['firstname']) ? cleanup($_POST['firstname']) : '';
$lastname = isset($_POST['lastname']) ? cleanup($_POST['lastname']) : '';
$email = isset($_POST['email']) ? cleanup($_POST['email']) : '';
$verzenden = isset($_POST['verzenden']) ? cleanup($_POST['verzenden']) : '';

if(isset($verzenden)) {




if(empty($firstname) || strlen($firstname) > 100)
{
	$error_firstname = "<div class=\"alert alert-danger\">First name not filled in.</div>";
}

if(empty($lastname) || strlen($lastname) > 100)
{
	$error_lastname = "<div class=\"alert alert-danger\">Last name not filled in.</div>";
}

if(empty($email) || strlen($email) > 150)
{
	$error_email = "<div class=\"alert alert-danger\">E-mail address not filled in.</div>";
}

if(empty($error_firstname) && empty($error_lastname) && empty($error_email))
{
	
$form_sent = "1";
echo "<p>Thank you!</p><p>We have sent you an e-mail with the link to download the e-book <strong>Getting started with FS Pro 2013</strong>.</p>";	

echo "<iframe src=\"http://www2.informationmapping.com/l/8622/2013-11-20/dvq41?email=$email&firstname=$firstname&lastname=$lastname&country=$visitor_country\" width=\"1\" height=\"1\" style=\"display: none;\"></iframe>";


$message = "<!DOCTYPE html PUBLIC \"-//W3C//DTD XHTML 1.0 Transitional//EN\" \"http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd\">
<html>
    <head>
        <meta http-equiv=\"Content-Type\" content=\"text/html; charset=utf-8\">
        
        <!-- Facebook sharing information tags -->
        <meta property=\"og:title\" content=\"%%subject%%\">
        
        <title>Information Mapping&reg;</title>
    
  <style type=\"text/css\">
<!--
body {
    background-color: #EFEFEF;
}
-->
</style></head>
    <body leftmargin=\"0\" marginwidth=\"0\" topmargin=\"0\" marginheight=\"0\" offset=\"0\" style=\"-webkit-text-size-adjust: none;margin: 0;padding: 0;background-color: #EEEEEE;width: 100%;\">
<center>
<table id=\"backgroundTable\" style=\"background-color: #eeeeee; width: 100%; height: 100%; padding: 0px; margin: 0px;\" border=\"0\" cellpadding=\"0\" cellspacing=\"0\">
<tbody>
<tr>
<td style=\"border-collapse: collapse;\" align=\"center\" bgcolor=\"#EFEFEF\" valign=\"top\"><!-- // Begin Template Preheader \ -->
<table id=\"templatePreheader\" style=\"background-color: #efefef; width: 600px;\" border=\"0\" cellpadding=\"10\" cellspacing=\"0\">
<tbody>
<tr>
<td class=\"preheaderContent\" style=\"border-collapse: collapse;\" valign=\"top\"><!-- // Begin Module: Standard Preheader  -->
<table style=\"width: 100%;\" border=\"0\" cellpadding=\"0\" cellspacing=\"0\">
<tbody>
<tr>
<td style=\"border-collapse: collapse;\" valign=\"top\">
<div pardot-region=\"std_preheader_content\" style=\"color: #505050; font-family: Arial,Helvetica,sans-serif; font-size: 10px; line-height: 100%; text-align: left;\" class=\"\">Download the FS Pro 2013 e-book</div>
</td>
<!--  -->
<td style=\"border-collapse: collapse;\" valign=\"top\" width=\"190\">
<div pardot-data=\"link-color:#00a0df;\" pardot-region=\"std_preheader_links\" style=\"color: #505050; font-family: Arial; font-size: 10px; line-height: 100%; text-align: left;\">
<div style=\"text-align: right;\"></div>
</div>
</td>
<!--  --></tr>
</tbody>
</table>
<!-- // End Module: Standard Preheader  --></td>
</tr>
</tbody>
</table>
<!-- // End Template Preheader \ -->
<table id=\"templateContainer\" style=\"background-color: #ffffff; width: 600px; border-width: 1px; border-color: #dddddd; border-style: solid;\" border=\"0\" cellpadding=\"0\" cellspacing=\"0\">
<tbody>
<tr>
<td style=\"border-collapse: collapse;\" align=\"center\" valign=\"top\"><!-- // Begin Template Header \ -->
<table id=\"templateHeader\" style=\"background-color: #ffffff; border-bottom-width: 0px; border-bottom-style: initial; border-bottom-color: initial; width: 600px;\" border=\"0\" cellpadding=\"0\" cellspacing=\"0\">
<tbody>
<tr>
<td class=\"headerContent\" style=\"border-collapse: collapse; color: #202020; font-family: Arial; font-size: 34px; font-weight: bold; line-height: 100%; padding: 0; text-align: center; vertical-align: middle; margin-bottom:20px;\" pardot-region=\"header_image\"><!-- // Begin Module: Standard Header Image \ --> <img src=\"http://www2.informationmapping.com/l/8622/2012-08-22/7zl26/8622/65115/logobanner.jpg\" alt=\"logobanner\" width=\"600\" height=\"71\" border=\"0\" id=\"headerImage campaign-icon\" style=\"max-width: 600px; border: 0; height: auto; line-height: 100%; outline: none; text-decoration: none;\" title=\"logobanner\"> <!-- // End Module: Standard Header Image \ --></td>


</tr></tbody>
</table>
<!-- // End Template Header \ --></td>
</tr>
<tr><td><table style=\"margin-top:20px; margin-right:0px; margin-bottom:0px; margin-left:20px\" border=\"0\" cellpadding=\"0\" cellspacing=\"0\"><tbody><tr><td><div pardot-data=\"link-color:#00a0df;\" pardot-region=\"std_content00\" style=\"color: #505050; font-family: Arial,Helvetica,sans-serif; font-size: 14px; line-height: 21px; text-align: left; background: none repeat scroll 0% 0% #ffffff;\" class=\"\">
  <p><span style=\"font-size: 18px; color: #009edf;\">Thank you for installing FS Pro 2013</span></p></div></td></tr></tbody></table></td></tr>
<tr>
<td style=\"border-collapse: collapse;\" align=\"center\" valign=\"top\"><!-- // Begin Template Body \ -->
<table id=\"templateBody\" style=\"width: 600px;\" border=\"0\" cellpadding=\"0\" cellspacing=\"0\">
<tbody>
<tr><!-- // Begin Sidebar \  -->
<td style=\"border-collapse: collapse;\" valign=\"top\">
<table border=\"0\" cellpadding=\"0\" cellspacing=\"0\">
<tbody>
<tr>
<td class=\"bodyContent\" style=\"border-collapse: collapse; background-color: #ffffff;\" valign=\"top\"><!-- // Begin Module: Standard Content \ -->
  <table style=\"width: 100%;\" border=\"0\" cellpadding=\"20\" cellspacing=\"0\">
  <tbody>
  <tr>
  <td style=\"border-collapse: collapse;\" valign=\"top\">
  <div pardot-data=\"link-color:#00a0df;\" pardot-region=\"std_content00\" style=\"color: #505050; font-family: Arial,Helvetica,sans-serif; font-size: 14px; line-height: 21px; text-align: left; background: none repeat scroll 0% 0% #ffffff;\" class=\"\">
    <p>Hi $firstname,</p>
    <p>Download the <a style=\"color: #009EDF;\" href=\"http://www.informationmapping.com/installation-successful/Getting_started_with_fspro2013.zip\">Getting started with FS Pro 2013</a> e-book now, and get the most out of your new software.</p>
    <p>Happy Mapping!</p>
    <p>The Information Mapping Team</p>
  </div>
  </td>
  </tr>
  </tbody>
  </table>
  <!-- // End Module: Standard Content \ --></td>
</tr>
</tbody>
</table>
</td>
<!-- // End Sidebar \ --></tr>
</tbody>
</table>
</td>
</tr>
</tbody>
</table>
<!-- // End Template Body \ --></td>
</tr>
<tr>
<td style=\"border-collapse: collapse;\" align=\"center\" valign=\"top\"><!-- // Begin Template Footer \ -->
<table id=\"templateFooter\" style=\"background-color: #fafafa; border-top-width: 0px; border-top-style: initial; border-top-color: initial; width: 600px;\" border=\"0\" cellpadding=\"20\" cellspacing=\"0\">
<tbody>
<tr>
<td class=\"footerContent\" style=\"background-color: #fafafa; border-collapse: collapse; border-top: 1px solid #dddddd;\" valign=\"top\"><!-- // Begin Module: Standard Footer \ -->
<table style=\"width: 100%;\" border=\"0\" cellpadding=\"0\" cellspacing=\"0\">
<tbody>
<tr>
<td id=\"social\" style=\"border-collapse: collapse; background-color: #fafafa; border: 0;\" valign=\"middle\" width=\"540\">
<div pardot-region=\"std_social\" style=\"color: #777777; font-family: Arial,Helvetica,sans-serif; font-size: 12px; line-height: 17px; text-align: center;\">
<div style=\"text-align: left;\">
<table style=\"width: 100%;\" border=\"0\" cellspacing=\"0\" cellpadding=\"0\">
<tbody>
<tr>
<td align=\"left\" valign=\"top\">
<p><span style=\"font-family: arial, helvetica, sans-serif; font-size: 11px; color: #696969;\"><strong>INFORMATION MAPPING INTERNATIONAL | </strong>Beekstraat 46, 9031 Drongen (Belgium)<br>
  T: +32 9 253 14 25</span> | <span style=\"font-family: arial,helvetica,sans-serif; font-size: 11px;\"><a style=\"color: #009EDF;\" href=\"www.informationmapping.com\" target=\"_self\">www.informationmapping.com</a></span></p>
</td>
</tr>
</tbody>
</table>
</div>
</div>
</td>
</tr>

</tbody>
</table>
</td>
</tr>
</tbody>
</table>
</td>
</tr>
</tbody>
</table>




</center>
</body>
</html>";

   /* $separator = md5(time());

    $eol = PHP_EOL;

    // attachment name
    $filename = "FSPro2013Guide.pdf";

    // encode data (puts attachment in proper format)
    $pdfdoc = "http://www.informationmapping.com/installation-successful/guide_19112013.pdf"; // werkt enkel als beveiliging niet via .htaccess verloopt, vandaar folder fpdf gedupliceerd buiten admin folder
    $attachment = chunk_split(base64_encode(file_get_contents($pdfdoc)));*/

// headers seperated by \n on Unix, \r\n on Windows servers
$headers  = "MIME-Version: 1.0\n";
$headers .= "Content-type: text/html; charset=utf-8\n";		
$headers .= "From: Information Mapping International <$from>\n"; // From must be the same as the domain you are sending from, otherwise will not work
$headers .= "Reply-To: Information Mapping International <$from>\n";
$headers .= "Return-Path: IMI <info@informationmapping.com>\n";
$headers .= "BCC: <$bcc>\n";
$headers .= "X-Priority: $prioriteit\n"; // 3 voor normale prioriteit  	
$headers .= "Date: " . date("r") . "\n"; 


	
	  /*$headers  = "MIME-Version: 1.0".$eol;
	$headers .= "Content-Type: multipart/mixed; boundary=\"".$separator."\"".$eol.$eol; 
    $headers .= "Content-Transfer-Encoding: 7bit".$eol;
    $headers .= "This is a MIME encoded message.".$eol.$eol;
	  $headers .= "From: Information Mapping International <$from>".$eol;
      $headers .= "BCC: <$bcc>".$eol;
	  $headers .= "Reply-To: IMI <info@informationmapping.com>".$eol;
	  $headers .= "Return-Path: IMI <info@informationmapping.com>".$eol;
      $headers .= "X-Priority: $prioriteit".$eol; // 3 voor normale prioriteit 
	  $headers .= "Date: " . date("r") .$eol; 
	  
	      // message
    $headers .= "--".$separator.$eol;
    $headers .= "Content-Type: text/html; charset=\"UTF-8\"".$eol;
    $headers .= "Content-Transfer-Encoding: 8bit".$eol.$eol;
    $headers .= $message.$eol.$eol;

    // attachment
    $headers .= "--".$separator.$eol;
    $headers .= "Content-Type: application/octet-stream; name=\"".$filename."\"".$eol; 
    $headers .= "Content-Transfer-Encoding: base64".$eol;
    $headers .= "Content-Disposition: attachment".$eol.$eol;
    $headers .= $attachment.$eol.$eol;
    $headers .= "--".$separator."--";*/
	  
	  $to = $email; // e-mailadres van de ontvanger
	  
	  mail($to, $onderwerp, $message, $headers, "-f info@informationmapping.com"); // fifth parameter is obliged for some webhosts
}

}// einde isset $verzenden
?>
      <?php
$form_sent = isset($form_sent);
if($form_sent != 1)
{ ?>
      <p>Complete the form below to receive the free e-book "Getting started with FS Pro 2013".</p>
      <form class="form-horizontal" role="form" action="#" method="post">
        <div class="form-group">
          <div class="col-sm-10">
            <label for="firstname">First name</label>
          </div>
          <div class="col-sm-10">
            <input type="text" class="form-control" id="firstname" name="firstname" value="<?php if (isset($_POST['verzenden'])) { echo $firstname; } ?>">
            <?php if($error_firstname && isset($_POST['verzenden'])) { echo $error_firstname; } ?>
          </div>
        </div>
        <div class="form-group">
          <div class="col-sm-10">
            <label for="lastname">Last name</label>
          </div>
          <div class="col-sm-10">
            <input type="text" class="form-control" id="lastname" name="lastname" value="<?php if (isset($_POST['verzenden'])) { echo $lastname; } ?>">
            <?php if($error_lastname && isset($_POST['verzenden'])) { echo $error_lastname; } ?>
          </div>
        </div>
        <div class="form-group">
          <div class="col-sm-10">
            <label for="email">E-mail address</label>
          </div>
          <div class="col-sm-10">
            <input type="email" class="form-control" id="email" name="email" value="<?php if (isset($_POST['verzenden'])) { echo $email; } ?>">
            <?php if($error_email && isset($_POST['verzenden'])) { echo $error_email; } ?>
          </div>
        </div>
        <div class="form-group">
          <div class="col-sm-10">
            <button type="submit" class="btn btn-default" name="verzenden" style="background-color:#428bca; color: #fff;">Send me the e-book!</button>
          </div>
        </div>
      </form>
      <p class="icons"> <a href="https://twitter.com/infomap"><i class="icon-twitter-sign icon-2x"></i></a> <a href="https://www.facebook.com/iminv"><i class="icon-facebook-sign icon-2x"></i></a> <a href="http://www.linkedin.com/groups?gid=983927"><i class="icon-linkedin-sign icon-2x"></i></a> <a href="http://www.youtube.com/user/InformationMapping"><i class="icon-youtube-sign icon-2x"></i></a> </p>
      <p id="db">Your e-mail address will be added to our newsletter database. You can unsubscribe at any time. We never share or sell your e-mail address with other parties.</p>
      <?php } // einde $form_sent ?>
    </div>
  </div>
  <div class="row" id="footer">
    <p>&copy; <?php echo date("Y"); ?> Information Mapping International | <a href="http://www.informationmapping.com/">www.informationmapping.com</a> | +32 9 253 14 25 | Contact our office in <a href="http://www.informationmapping.com/us/contact">US</a>, <a href="http://www.informationmapping.com/en/contact-3">Europe</a> or <a href="http://www.informationmapping.com/in/contact-2">Asia</a></p>
  </div>
</div>
<?php
$db=mysql_connect("localhost","mysql_1003","0wtKVxYK") or die ("The connection to the database could not be established. Please try again later or inform info@informationmapping.com");
mysql_set_charset('utf8',$db); // added to avoid problems in the SQL DB with e.g. Nestlé
mysql_select_db("imf_new",$db);
  $query_z = "SELECT * FROM fspro_statistics"; 
  $result_z = mysql_query($query_z) or die("FOUT : " . mysql_error()); 
  if (mysql_num_rows($result_z) > 0){
	$query_z = "UPDATE fspro_statistics SET counter = counter +1, date = NOW()";
  }
  else {
	  $aantal = '1';
    $query_z = "INSERT INTO fspro_statistics (counter, date) VALUES ('$aantal', NOW())";
  }

mysql_query($query_z);
?>

<!-- softonic tracking code --> 
<script type="text/javascript">
var img_ct = (( "https:" == document.location.protocol) ? "https://" : "http://" );
img_ct += "t1.softonicads.com/conversion.php?c=";
img_ct += "AD43tPXB0nfnBsORELp0AULKLmpRDF4me61Ce4o7AF0%3D";
img_ct +=  "&" + Math.random();
document.write( unescape("%3Cimg src='") + img_ct + unescape("' width='1' height='1' /%3E") );
</script> 

<!-- jQuery (necessary for Bootstrap's JavaScript plugins) --> 
<script src="//code.jquery.com/jquery.js"></script> 
<!-- Include all compiled plugins (below), or include individual files as needed --> 
<script src="js/bootstrap.min.js"></script>
</body>
</html>