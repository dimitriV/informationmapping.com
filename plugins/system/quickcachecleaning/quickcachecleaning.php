<?php
/**
 * @Copyright
 *
 * @package    QCC - Quick Cache Cleaning for Joomla! 3
 * @author     Viktor Vogel <admin@kubik-rubik.de>
 * @version    3-5 - 2015-02-22
 * @link       https://joomla-extensions.kubik-rubik.de/qcc-quick-cache-cleaning
 *
 * @license    GNU/GPL
 *  This program is free software: you can redistribute it and/or modify
 *  it under the terms of the GNU General Public License as published by
 *  the Free Software Foundation, either version 3 of the License, or
 *  (at your option) any later version.
 *
 *  This program is distributed in the hope that it will be useful,
 *  but WITHOUT ANY WARRANTY; without even the implied warranty of
 *  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *  GNU General Public License for more details.
 *
 *  You should have received a copy of the GNU General Public License
 *  along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */
defined('_JEXEC') or die('Restricted access');

/**
 * Class PlgSystemQuickCacheCleaning
 */
class PlgSystemQuickCacheCleaning extends JPlugin
{
    function __construct(&$subject, $config)
    {
        parent::__construct($subject, $config);
        $this->loadLanguage();
    }

    /**
     * Executes the plugin in the trigger onContentAfterSave which is triggered by the content component
     *
     * @param string   $context
     * @param stdClass $article
     * @param boolean  $isNew
     */
    public function onContentAfterSave($context, $article, $isNew)
    {
        $onContentAfterSave = (int)$this->params->get('onContentAfterSave', 1);

        if($onContentAfterSave === 1 OR ($onContentAfterSave === 2 AND $isNew))
        {
            $this->cleanCache();
        }
    }

    /**
     * Starts the cleaning process directly in the corresponding cache component
     *
     * @throws Exception
     */
    private function cleanCache()
    {
        // Get model from cache component to use its functions
        JLoader::import('cache', JPATH_ADMINISTRATOR.'/components/com_cache/models');
        $model = JModelLegacy::getInstance('cache', 'CacheModel');
        $model->clean();
        JFactory::getApplication()->enqueueMessage(JText::_('PLG_QUICKCACHECLEANING_CACHE_CLEANED'));
    }
}
