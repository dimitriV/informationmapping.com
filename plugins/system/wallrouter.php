<?php
/*------------------------------------------------------------------------
plgSystemWallRouter - Wall Factory
------------------------------------------------------------------------
author    TheFactory
copyright Copyright (C) 2010 SKEPSIS Consult SRL. All Rights Reserved.
@license - http://www.gnu.org/licenses/gpl-2.0.html GNU/GPL
Websites: http://www.thefactory.ro
Technical Support:  Forum - http://www.thefactory.ro/joomla-forum/
-------------------------------------------------------------------------*/

defined('_JEXEC') or die('Restricted access');

class plgSystemWallRouter extends JPlugin
{
	function plgSystemWallRouter(&$subject, $config)
	{
	  parent::__construct($subject);

	  $this->_db =& JFactory::getDBO();
	}

	function onAfterInitialise()
	{
	  global $mainframe;

	  // Not in backend
	  if ('site' != $mainframe->getName())
	  {
			return true;
		}

		// Not if SEF is disabled
    $config =& JFactory::getConfig();
    if (!$config->getValue('sef'))
    {
      return true;
    }

	  $router =& $mainframe->getRouter();

	  $router->attachParseRule(array($this, 'parse'));
	  $router->attachBuildRule(array($this, 'build'));
	}

  function parse($router, &$uri)
  {
    $vars  = array();
    $path  = $uri->getPath();
    $path  = explode('/', $path);
    $alias = $path[0];

    // More than 1 segments or alias is empty
    if (1 < count($path) || '' == $alias)
    {
      return $vars;
    }

    // Find item (wall)
    $item = $this->_findItemByAlias($alias);

    // Item was not found
    if (!$item)
    {
      return $vars;
    }

    // Set the vars
    $vars = array(
      'option' => 'com_wallfactory',
      'view'   => $item->view,
      'alias'  => $item->alias);

    return $vars;
  }

  function build($router, &$uri)
  {
    if ('com_wallfactory' == $uri->getVar('option') &&
        false === strpos($uri->_uri, 'index.php?Itemid='))
    {
      switch ($uri->getVar('view'))
      {
        case 'wall':
          $alias      = $uri->getVar('alias');
          $limitstart = $uri->getVar('limitstart');

          if (!is_null($limitstart))
          {
            $alias .= '?limitstart=' . $limitstart;
          }

		      $uri->setQuery(null);
		      $uri->setPath('index.php/' . $alias);
		    break;
      }
    }
  }

  function _findItemByAlias($alias)
  {
    $alias = $this->_db->getEscaped($alias);

    $query = ' SELECT w.id, "wall" AS view, w.alias'
               . ' FROM #__wallfactory_walls w'
               . ' WHERE w.alias = "' . $alias . '"';

    $this->_db->setQuery($query, 0, 1);

    return $this->_db->loadObject();
  }
}