<?php

/**
 * @version		$Id: partnersredirect.php 2012-03-30 $
 * @package		IMI
 * @subpackage	plg_partnersredirect
 * @author      IT Offshore Nepal
 * @copyright   Copyright (C) 2012 IT Offshore Nepal. All rights reserved.
 * @license     IT Offshore Nepal
 */
// No direct access
defined('_JEXEC') or die('Restricted access');

// Import library dependencies
jimport('joomla.plugin.plugin');

/**
 * Plugin that redirects partner users to correct partner stores
 */
class plgSystemPartnersRedirect extends JPlugin
{
    public function plgSystemPartnersRedirect(&$subject, $config)
    {
        $app = JFactory::getApplication();
        if ($app ->isAdmin()) {
            return;
        }
        
        parent::__construct($subject, $config);
    }

    public function onAfterDispatch()
    {
        if(JRequest::getVar('option') == 'com_magebridge'){
        	$user = JFactory::getUser();
        	$usergroups = $this->params->get('usergroups');
        	$redirect = false;
        	
        	foreach($user->groups as $grp){
				if(in_array($grp, $usergroups)){
					$redirect = true;
					break;
				}
			}

			if($redirect){
				$app = JFactory::getApplication();
				$search_arr = explode(',', $this->params->get('otherstoresmenualias'));
				//$search_arr = explode(',', $this->params->get('rootmenualias'));
				foreach($search_arr as $srch){
					$search[] = '/'.$srch;
				}
				$replace = '/'.$this->params->get('partnermenualias');
				$redirect_url = str_replace($search, $replace, JURI::current());
				if(JURI::current() != $redirect_url && count($search)){
                    //echo "Debug Partner Redirect: $redirect_url";exit;
					$app->redirect($redirect_url);
					exit();
				}
			}
		}
    }

}
