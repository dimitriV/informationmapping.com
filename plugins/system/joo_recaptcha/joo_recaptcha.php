<?php
/**
* Joo ReCaptcha Plugin.
* @version 1.6.2
* @package Joo ReCaptcha Plugin 01.06.2011
* @author CAMMARO/JooTools
* @copyright (C) 2010 by StefySoft.com
* @url http://joomla.stefysoft.com
* @license http://www.gnu.org/copyleft/gpl.html GNU/GPL
**/

defined( '_JEXEC') or ( '_VALID_MOS' ) or die( 'Direct Access to this location is not allowed.' );

jimport('joomla.plugin.plugin');

class plgSystemJoo_Recaptcha extends JPlugin{
	var $errror;
	function plgSystemJoo_Recaptcha( &$subject, $config)
	{
		parent::__construct($subject, $config);
		
		require_once(dirname(__FILE__).'/joo_recaptcha/joo_api.php');
		JooReCaptcha::setKeys(
			$this->params->get('public', JooRecaptcha::get('publicKey')),
			$this->params->get('private', JooRecaptcha::get('privateKey'))
		);
		if ($this->params->get('ajax') == 1)
			JooReCaptcha::setAjaxMode(true);
		else 
			JooReCaptcha::setAjaxMode(false);
		$theme = $this->_get_heme_name();
		JooReCaptcha::setTheme($theme);
	}
	
	function processPage()
	{
		$option = JRequest::getCmd('option');
		$view = JRequest::getCmd('view');
		$task = JRequest::getCmd('task');
		
		if( $this->params->get('addToContact',1) == 1 &&
		   $option == 'com_contact' &&
		   $task == 'contact.submit'
		){
			return true;
		}
		if( $this->params->get('addToUserRegistration',1) == 1 &&
		   $option == 'com_users' &&
		   $task == 'registration.register'
		){
			
			return true;
		}
		if( $this->params->get('addToFUser',1) == 1 &&
		   $option == 'com_users' &&
		   $task == 'remind.remind'
		){
			
			return true;
		}
		if( $this->params->get('addToFPass',1) == 1 &&
		   $option == 'com_users' &&
		   $task == 'reset.request'
		){
			
			return true;
		}
		if( $this->params->get('addToLogin',1) == 1 &&
		   $option == 'com_users' &&
		   $task == 'user.login'
		){
			
			return true;
		}
		return false;
	}
	
	function addFormToBuffer()
	{
		$option = JRequest::getCmd('option');
		$view = JRequest::getCmd('view');
		$task = JRequest::getCmd('task');
		if( $this->params->get('addToContact',1) == 1 && $option == 'com_contact' && $view == 'contact' ){
			
			return true;
		}
		if( $this->params->get('addToUserRegistration',1) == 1 && $option == 'com_users' && $view == 'registration'){
			
			return true;
		}
		if( $this->params->get('addToFUser',1) == 1 && $option == 'com_users' && $view == 'remind'){
			
			return true;
		}
		if( $this->params->get('addToFPass',1) == 1 && $option == 'com_users' && $view == 'reset' && !JRequest::getCmd('layout')){
			
			return true;
		}
		if( $this->params->get('addToLogin',1) == 1 && $option == 'com_users' && $view == 'login'){
			
			return true;
		}		
		return false;
	}
	
	function onAfterInitialise()
	{
		JooReCaptcha::process();
	}
	
	function onAfterRoute()
	{
		$mainframe = JFactory::getApplication();;
		if( !$this->processPage() ){
			return;
		}
		$submited = JooReCaptcha::get('submitted');
		$success = JooReCaptcha::get('success');
		if( !$success ){
			$mainframe->enqueueMessage('ReCAPTCHA Text Error.', 'error');
			$option = JRequest::getCmd('option');
			$view = JRequest::getCmd('view');
			$task = JRequest::getCmd('task');
			if ($option == 'com_contact' && $task == "contact.submit")
			{
				JRequest::setVar('task','0');
				JRequest::setVar('view','contact');
				unset($_GET['task'], $_POST['task']);
			}
			if ($option == 'com_users' && $task == 'registration.register')
			{
				JRequest::setVar('task','0');
				JRequest::setVar('view','registration');
				$_GET['task'] = '0';
				$_POST['task'] = '0';
				unset($_GET['task'], $_POST['task']);
				//echo JRequest::getCmd('task');
			}
			if ($option == 'com_users' && $task == 'remind.remind')
			{
				JRequest::setVar('view','remind');
				JRequest::setVar('task','0');
				unset($_GET['task'], $_POST['task']);
			}
			if ($option == 'com_users' && $task == 'reset.request')
			{
				JRequest::setVar('view','reset');
				JRequest::setVar('task','0');
				unset($_GET['task'], $_POST['task']);
			}
			if ($option == 'com_users' && $task == 'user.login')
			{
				JRequest::setVar('view','login');
				JRequest::setVar('task','0');
				unset($_GET['task'], $_POST['task']);
			}
		}
	}
	
	function onAfterDispatch()
	{
		if( !$this->addFormToBuffer() ){
			return;
		}
		else
		{
		$document =& JFactory::getDocument();
		$buffer = $document->getBuffer('component');
		
		//add recaptcha before the submit button
		$re = "/<(button|input)(.*type=['\"](submit|button)['\"].*)?>/i";
		$buffer = preg_replace_callback($re, array(&$this,'_addFormCallback'), $buffer);
		
		// seting values...
		$inputsRe = "/<input(.*name=(['\"])(.+?)\\2.*)?>/i";
		$inputsReLnk = "/<input(.*name=['\"].*.['\"].*)?>/i";
		$textareaRe = "/<textarea(.*name=(['\"])(.+?)\\2.*)?>(.*)?<\/textarea>/i";
		
		$buffer = preg_replace_callback($inputsRe, array(&$this,'_addInputValues'), $buffer);
		$buffer = preg_replace_callback($textareaRe, array(&$this,'_addTextareaValue'), $buffer);
		
		$document->setBuffer($buffer,'component');
		}
	}
	
	function _addFormCallback($matches)
	{
		return "\n<!-- Joo ReCaptcha Plugin Start by Joomla.StefySoft.com -->\n".JooReCaptcha::get('html').'<br />'.$matches[0]."\n<!-- Joo ReCaptcha Plugin End by Joomla.StefySoft.com -->\n";
	}
	
	function _get_heme_name()
	{
		switch ($this->params->get('template'))
		{
			case '0':
				return 'red';
				break;
			case '1':
				return 'white';
				break;
			case '2':
				return 'blackglass';
				break;
			case '3':
				return 'clean';
				break;
		}
	}
	
	function _addInputValues($matches)
	{
		/*foreach ($_POST as $key => $value) 
				{
					echo $key." -=> ".$value."<br />";
				}*/
		switch($matches[3]){
			case 'name':
			case 'jform[name]':
			case 'email':
			case 'jform[email]':
			case 'jform[email1]':
			case 'jform[email2]':
			case 'subject':
			case 'jform[subject]':
			case 'jform[url]':
			case 'jform[title]':
			case 'password2':
			case 'password':
			case 'jform[password]':
			case 'jform[password1]':
			case 'jform[password2]':
			case 'username':
			case 'jform[username]':
			case 'jform[contact_name]':
			case 'jform[contact_email]':
			case 'jform[contact_subject]':
				$re = "/value=(['\"])(.*?)\\1/i";
				$jform = JRequest::getVar('jform');
				$var = str_replace(array('jform[',']'), array('',''), $matches[3]);
				$this->_replacementValue = JRequest::getVar($matches[3]);
				if(empty($this->_replacementValue))
					$this->_replacementValue = $jform[$var];
				$matches[0] = preg_replace_callback($re, array(&$this, '_replaceValue'), $matches[0]);
				break;
		}
		return $matches[0];
	}
	
	function _addTextareaValue($matches)
	{
		$attrs = $matches[1];
		$jform = JRequest::getVar('jform');
		$val = $jform['contact_message'];
		return "<textarea$attrs>$val</textarea>";
	}
	
	function _replaceValue($matches)
	{
		$val = addslashes($this->_replacementValue);
		return "value='$val'";
	}
	
}
?>