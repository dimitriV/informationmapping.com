<?php
/**
 * @version		$Id: geoipredirect.php 21744 2011-07-06 08:40:23Z chdemko $
 * @copyright	Copyright (C) 2011 IT Offshore Nepal. All rights reserved.
 */

// no direct access
defined('_JEXEC') or die;

jimport('joomla.language.helper');
jimport('joomla.plugin.plugin');

/**
 * Joomla! Geo IP Redirect Plugin
 *
 * @package		Joomla.Plugin
 * @subpackage	System.geoipredirect
 * @since		1.7
 */
class plgSystemGeoIPRedirect extends JPlugin
{
	protected static $tag;

	public function __construct(&$subject, $config)
	{
		$app = JFactory::getApplication();
		if($app->isAdmin()) return;

		parent::__construct($subject, $config);
	}
	
	function onAfterInitialise(){
        $app = JFactory::getApplication();
		if($app->isAdmin()) return;

		$lang_code = JRequest::getString(JUtility::getHash('language'), null, 'cookie');
		if(!$lang_code){
			$country = $this->getCountryByIP($_SERVER['REMOTE_ADDR']);
			if($country == 'US'){
				$app->redirect(JRoute::_('index.php?lang=us'));
			}
			else{
				$app->redirect(JRoute::_('index.php?lang=en'));
			}
		}
		
		return true;
	}
	
	function getCountryByIP($ip_address){
		#$ip_address = '107.0.160.40'; IP of US (for testing purpose)
		$db = JFactory::getDbo();
		$ip_num = sprintf("%u", ip2long($ip_address));
		$sql = "SELECT cc FROM #__geoip_ip NATURAL JOIN #__geoip_cc WHERE ".$ip_num." BETWEEN start AND end";
		$db->setQuery($sql);
		return $db->loadResult();
	}

}
