<?php
/**
 * BoxBridge for Joomla by 2Value - Sander Potjer
 * Copyright (c) 2010 2Value. All rights reserved.
 * Released under the GNU/GPL license: http://www.gnu.org/copyleft/gpl.html
 * More info at http://www.boxbridge.net
 */

// no direct access
defined( '_JEXEC' ) or die( 'Restricted access' );
JHTML::_('behavior.tooltip');
?>
<?php if(count($tree)!==1): ?>
<div class="files">
	<div class="boxitem">
		<?php if($thumbSize == 'small'):?>
		<div class="thumbsmall" style="background-image: url(<?php echo $tree['file_thumb_small'];?>);">&nbsp;</div>
		<?php elseif($thumbSize == 'large'): ?>
		<div class="thumblarge" style="background-image: url(<?php echo $tree['file_thumb_larger'];?>);">&nbsp;</div>
		<?php endif; ?>

		<div class="item_data">
			<?php if($showTitle):?>
			<div class="filename">
				<span>
					<?php if($showDownloadTitle == 'download'):?>
						<a href="<?php echo $tree['file_download_link']; ?>" title="<?php echo JText::_('PLG_BOXBRIDGE_DOWNLOAD'); ?> <?php echo $tree['file_name']; ?>"><?php echo $tree['file_name']; ?></a>
					<?php elseif($showDownloadTitle == 'view'): ?>
						<a target="_blank" href="<?php echo $tree['file_shared_link']; ?>" title="<?php echo JText::_('PLG_BOXBRIDGE_VIEW'); ?> <?php echo $tree['file_name']; ?>"><?php echo $tree['file_name']; ?></a>
					<?php else :?>
						<?php echo $tree['file_name']; ?>
					<?php endif; ?>
				</span>
			</div>
			<?php endif; ?>

			<div class="additional_info">
				<?php if($showDescription):?>
					<span class="description"><?php echo $tree['file_description']; ?></span>
				<?php endif; ?>
				<?php if($showCreated):?>
				 	<span class="created"><span class="label"><?php echo JText::_('PLG_BOXBRIDGE_UPLOADED'); ?></span> <?php echo $tree['file_created']; ?></span> 
				<?php endif; ?>
				<?php if(($showCreated) && ($showModified)):?>
					<span class="separator">|</span> 
				<?php endif; ?>
				<?php if($showModified):?>
					<span class="updated"><span class="label"><?php echo JText::_('PLG_BOXBRIDGE_MODIFIED'); ?></span> <?php echo $tree['file_updated']; ?></span>
				<?php endif; ?>
				<?php if((($showCreated) || ($showModified)) && ($showSize)):?>
					<span class="separator">|</span> 
				<?php endif; ?>
				<?php if($showSize):?>
					<span class="size"><span class="label"><?php echo JText::_('PLG_BOXBRIDGE_SIZE'); ?></span> <?php echo $tree['file_size']; ?></span> 
				<?php endif; ?>
				<?php if((($showCreated) || ($showModified) || ($showSize)) && ($showSharedLink)):?>
					<span class="separator">|</span> 
				<?php endif; ?>
				<?php if($showSharedLink):?>
					<span class="shared"><a target="_blank" href="<?php echo $tree['file_shared_link']; ?>" title="<?php echo JText::_('PLG_BOXBRIDGE_VIEW_DESC'); ?>"><?php echo JText::_('PLG_BOXBRIDGE_VIEW'); ?></a></span> 
				<?php endif; ?>
				<?php if((($showCreated) || ($showModified) || ($showSize) || ($showSharedLink) ) && ($showDownloadLink)):?>
					<span class="separator">|</span> 
				<?php endif; ?>
				<?php if($showDownloadLink):?>
				 	<span class="shared"><a href="<?php echo $tree['file_download_link']; ?>" title="<?php echo JText::_('PLG_BOXBRIDGE_DOWNLOAD'); ?> <?php echo $tree['file_name']; ?>"><?php echo JText::_('PLG_BOXBRIDGE_DOWNLOAD'); ?></a></span>
				<?php endif; ?>
			</div>

		</div>
	</div>
</div>
<?php endif; ?>