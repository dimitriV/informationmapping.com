<?php
/**
 * Box REST Client Library for PHP5 Developers
 *
 *
 * @author James Levy <james@box.net>
 * @link http://enabled.box.net
 * @access public
 * @version 1.0
 * copyright Box.net 2007
 * Available for use and distribution under GPL-license
 * Go to http://www.gnu.org/licenses/gpl-3.0.txt for full text
 * Modifed by 2Value - Sander Potjer for BoxBridge for Joomla
 * More info at http://www.boxbridge.net
 */

require_once 'class.curl.php';

class boxclient {
	private $api_key;
	private $auth_token;
	private $tagcontent;
	private $date_format;
	private $subfolders;
	private $box_api_url;
	private $box_upload_url;
	private $box_download_url;
	private $box_shared_url;
	private $thumb_small_url;
	private $thumb_large_url;
	private $thumb_larger_url;
	private $thumb_preview_url;
	private $thumb_ext;
	public $folders;
	public $files;
	public $fileinfo;

    public function __construct($apiKey, $authToken, $tagcontent, $dateFormat, $subfolders) {
        $this->api_key = $apiKey;
        $this->auth_token = $authToken;
        $this->tagcontent = $tagcontent;
        $this->date_format = $dateFormat;
        $this->subfolders = $subfolders;

		$this->box_api_url = 'http://www.box.net/api/1.0/rest';
		$this->box_upload_url = 'http://upload.box.net/api/1.0/upload';
		$this->box_download_url = 'https://www.box.net/shared/static/';
		$this->box_shared_url = 'http://www.box.net/shared/';
		$this->thumb_small_url = 'http://www.box.net/api/thumbs/27x30';
        $this->thumb_large_url = 'http://www.box.net/api/thumbs/43x51';
        $this->thumb_larger_url = 'http://www.box.net/api/thumbs/43x51';
        $this->thumb_preview_url = 'http://www.box.net/api/thumbs/242x286';
        $this->thumb_ext = '.gif';
		$this->folders = array();
		$this->tree = array();
		$this->tree_count = 0;
		$this->fileinfo = array();
    }

    // Setup variables
    var $_error_code = '';
    var $_error_msg = '';

	/**
	 * Setup for Functions
	 */
    function makeRequest($method, $params = array()) {
        $this->_clearErrors();
        $useCURL = in_array('curl', get_loaded_extensions());

        if ($method == 'upload') {
            $args = array();
            foreach ($params as $k=>$v) {
                array_push($args, urlencode($v));
                $query_str = implode('/', $args);
            }
            $request = $this->box_upload_url.'/'.$query_str;
            if ($this->_debug) {
                echo "Upload Request: ".$request;
            }

        } else {
            $args = array();
            foreach ($params as $k=>$v) {
                array_push($args, urlencode($k).'='.urlencode($v));
                $query_str = implode('&', $args);
            }
            $request = $this->box_api_url.'?'.$method.'&'.$query_str;
            if ($this->_debug) {
                echo "Request: ".htmlspecialchars($request);
            }
        }

        if ($useCURL) {
            $c = new boxcurl($request);
            $c->setopt(CURLOPT_FOLLOWLOCATION, true);
            $xml = $c->exec();
            $error = $c->hasError();
            if ($error) {
                $this->_error_msg = $error;
                return false;
            }
            $c->close();
        } else {
            $url_parsed = parse_url($request);
            $host = $url_parsed["host"];
            $port = ($url_parsed['port'] == 0) ? 80 : $url_parsed['port'];
            $path = $url_parsed["path"].(($url_parsed['query'] != '') ? $path .= "?{$url_parsed[query]}" : '');
            $headers = "GET $path HTTP/1.0\r\n";
            $headers .= "Host: $host\r\n\r\n";
            $fp = fsockopen($host, $port, $errno, $errstr, 30);
            if (!$fp) {
                $this->_error_msg = $errstr;
                $this->_error_code = $errno;
                return false;
            } else {
                fwrite($fp, $headers);
                while (!feof($fp)) {
                    $xml .= fgets($fp, 1024);
                }
                fclose($fp);


                $xml_start = strpos($xml, '<?xml');
                $xml = substr($xml, $xml_start, strlen($xml));
            }
        }

        if ($this->_debug) {
            echo '<h2>XML Response</h2>';
            echo '<pre class="xml">';
            echo htmlspecialchars($xml);
            echo '</pre>';
        }

        $xml_parser = xml_parser_create();
        xml_parse_into_struct($xml_parser, $xml, $data);
        xml_parser_free($xml_parser);

        return $data;
    }

    /**
	 * Retrieve File Info (http://developers.box.net/ApiFunction_get_file_info)
	 */
    function getFileInfo($params = array()) {
        $params['api_key'] = $this->api_key;
        $params['auth_token'] = $this->auth_token;
        $params['file_id'] = $this->tagcontent;

        $this->fileinfo = array();

        $data = $this->makeRequest('action=get_file_info', $params);

        if ($this->_checkForError($data)) {
            return false;
        }

		if(count($data) && is_array($data)) {
	        foreach ($data as $a) {
	            switch ($a['tag']) {
	                case 'FILE_ID':
	                    $this->fileinfo['file_id'] = $a['value'];
	                    break;
	                case 'FILE_NAME':
	                    $this->fileinfo['file_name'] = $a['value'];
	                    $this->fileinfo['file_img'] = BoxBridgeHelper::findimg($a['value']);
	                    $this->fileinfo['file_ext'] = BoxBridgeHelper::findext($a['value']);
	                    $this->fileinfo['file_thumb_small'] = $this->thumb_small_url."/".BoxBridgeHelper::findtype($a['value'])."/".BoxBridgeHelper::findext($a['value'])."".$this->thumb_ext;
	                    $this->fileinfo['file_thumb_large'] = $this->thumb_large_url."/".BoxBridgeHelper::findtype($a['value'])."/".BoxBridgeHelper::findext($a['value'])."".$this->thumb_ext;
	                    $this->fileinfo['file_thumb_larger'] = $this->thumb_larger_url."/".BoxBridgeHelper::findtype($a['value'])."/".BoxBridgeHelper::findext($a['value'])."".$this->thumb_ext;
	                    $this->fileinfo['file_thumb_preview'] = $this->thumb_preview_url."/".BoxBridgeHelper::findtype($a['value'])."/".BoxBridgeHelper::findext($a['value'])."".$this->thumb_ext;
	                    break;
	                case 'DESCRIPTION':
	                    $this->fileinfo['file_description'] = key_exists('value', $a) ? $a['value'] : '';
	                    break;
	                case 'SHARED':
	                    $this->fileinfo['file_shared'] = $a['value'];
	                    break;
	                case 'SHARED_NAME':
	                    $this->fileinfo['file_shared_id'] = $a['value'];
	                    $this->fileinfo['file_shared_link'] = $this->box_shared_url.$a['value'];
	                    $this->fileinfo['file_download_link'] = $this->box_download_url.$this->fileinfo['file_shared_id'].".".$this->fileinfo['file_ext'];
	                    break;
	                case 'CREATED':
	                    $this->fileinfo['file_created'] = strftime($this->date_format, ($a['value']));
	                    break;
	                case 'UPDATED':
	                    $this->fileinfo['file_updated'] = strftime($this->date_format, ($a['value']));
	                    break;
	                case 'SIZE':
	                    $this->fileinfo['file_size'] = BoxBridgeHelper::ConvertSize($a['value']);
	                    break;
	            }
	        }
		}
        if ($this->_debug) {
            echo '<h2>File Info  Return</h2>';
            $this->_a($this->fileinfo);
            "<br/>";
            print_r($a);
            echo '<hr />';
        }
		
		if ($this->fileinfo['file_shared']) {
			return $this->fileinfo;
		} else {
          	$this->_error_code = 'file_not_shared';
			return false;
		}
		
    }

	/**
	 * Retrieve Account Tree (http://developers.box.net/ApiFunction_get_account_tree)
	 */
    function getAccountTree($params = array()) {
        $params['api_key'] = $this->api_key;
        $params['auth_token'] = $this->auth_token;
        $params['folder_id'] = $this->tagcontent;

        $this->tree = array();
		
		if ($this->subfolders) {
        	$data = $this->makeRequest('action=get_account_tree&params[1]=nozip', $params);
        } else {
        	$data = $this->makeRequest('action=get_account_tree&params[1]=nozip&params[2]=onelevel', $params);
        }

        if ($this->_checkForError($data)) {
           return false;
        }
        $this->tree_count = count($data);

        $entry_count = 0;

        for ($i = 0; $i < $this->tree_count; $i++) {
            $a = $data[$i];

            if ($a['tag'] == 'FILE' && $a['type'] == 'open') {
                $this->tree = $this->parseFile($a, $i, $entry_count);
            }

        }
        if ($this->_debug) {
            echo '<h2>Account Tree Return</h2>';
            $this->_a($this->tree);
            "<br/>";
            print_r($a);
            echo '<hr />';
        }

        return $this->tree;
    }
    
    function parseFile($a, $i, $entry_count) {
        if (is_array($a['attributes'])) {
            if ($a['attributes']['SHARED']) {
                $this->files[$i]['file_id'] = $a['attributes']['ID'];
                $this->files[$i]['file_name'] = $a['attributes']['FILE_NAME'];
                $this->files[$i]['file_ext'] = BoxBridgeHelper::findext($a['attributes']['FILE_NAME']);
                $this->files[$i]['file_img'] = BoxBridgeHelper::findimg($a['attributes']['FILE_NAME']);
                $this->files[$i]['file_description'] = $a['attributes']['DESCRIPTION'];
                $this->files[$i]['file_shared'] = $a['attributes']['SHARED'];
                $this->files[$i]['file_shared_id'] = str_replace($this->box_shared_url,'',$a['attributes']['SHARED_LINK']);
                $this->files[$i]['file_shared_link'] = $a['attributes']['SHARED_LINK'];
                $this->files[$i]['file_download_link'] = $this->box_download_url.$this->files[$i]['file_shared_id'].".".$this->files[$i]['file_ext'];
                $this->files[$i]['file_created'] = strftime($this->date_format, ($a['attributes']['CREATED']));
                $this->files[$i]['file_updated'] = strftime($this->date_format, ($a['attributes']['UPDATED']));
                $this->files[$i]['file_size'] = BoxBridgeHelper::ConvertSize($a['attributes']['SIZE']);
                $this->files[$i]['file_thumb_small'] = $a['attributes']['SMALL_THUMBNAIL'];
                $this->files[$i]['file_thumb_large'] = $a['attributes']['LARGE_THUMBNAIL'];
                $this->files[$i]['file_thumb_larger'] = $a['attributes']['LARGER_THUMBNAIL'];
                $this->files[$i]['file_thumb_preview'] = $a['attributes']['PREVIEW_THUMBNAIL'];
                $entry_count++;
            }
        }

        return $this->files;
    }


    /**
	 * Debugging & Error Codes
	 */
    function _checkForError($data) {
    	if ($data[1]['tag'] == 'STATUS' && $data[1]['value'] !== 'listing_ok' && $data[1]['value'] !== 's_get_file_info') {
            $this->_error_code = $data[1]['value'];
            $this->_error_msg = $data[1]['value'];
            return true;
        }
        return false;
    }

    function isError() {
        if ($this->_error_msg != '') {
            return true;
        }
        return false;
    }

    function getErrorMsg() {
    	if ($this->_error_code == 'not_logged_in') {
   	 		$error = JText::_('PLG_BOXBRIDGE_INVALID_AUTHENTICATION');
    	} 
    	if ($this->_error_code == 'e_access_denied') {
   	 		$error = JText::sprintf('PLG_BOXBRIDGE_INVALID_FILE',$this->tagcontent);
    	}
    	if ($this->_error_code == 'e_folder_id') {
   	 		$error = JText::sprintf('PLG_BOXBRIDGE_INVALID_FOLDER',$this->tagcontent);
    	}
    	if ($this->_error_code == 'file_not_shared') {
   	 		$error = JText::sprintf('PLG_BOXBRIDGE_NOT_SHARED',$this->fileinfo['file_name']);
    	}
    	return $error;
    }

    function getErrorCode() {
        return $this->_error_code;
    }

	function _clearErrors() {
        $this->_error_code = '';
        $this->_error_msg = '';
    }

    function setDebug($debug) {
        $this->_debug = $debug;
    }

    function _a($array) {
        echo '<pre>';
        print_r($array);
        echo '</pre>';
    }
}

?>