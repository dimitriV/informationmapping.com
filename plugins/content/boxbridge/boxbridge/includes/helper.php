<?php
/**
 * BoxBridge for Joomla by 2Value - Sander Potjer
 * Copyright (c) 2010 2Value. All rights reserved.
 * Released under the GNU/GPL license: http://www.gnu.org/copyleft/gpl.html
 * More info at http://www.boxbridge.net
 */
 
// no direct access
defined( '_JEXEC' ) or die( 'Restricted access' );

class BoxBridgeHelper {
	
	
	/**
	 * Check for template overrides
	 */
	function getTemplatePath($pluginName,$file,$jversion){
		$mainframe= &JFactory::getApplication();
		$p = new JObject;
		if(file_exists(JPATH_SITE.DS.'templates'.DS.$mainframe->getTemplate().DS.'html'.DS.$pluginName.DS.str_replace('/',DS,$file))){
			$p->file = JPATH_SITE.DS.'templates'.DS.$mainframe->getTemplate().DS.'html'.DS.$pluginName.DS.$file;
			$p->http = JURI::root()."templates/".$mainframe->getTemplate()."/html/{$pluginName}/{$file}";
		} else {
			if($jversion == '1.5') {
				$p->file = JPATH_SITE.DS.'plugins'.DS.'content'.DS.$pluginName.DS.'tmpl'.DS.$file;
				$p->http = JURI::root()."plugins/content/{$pluginName}/tmpl/{$file}";
			} else {
				$p->file = JPATH_SITE.DS.'plugins'.DS.'content'.DS.$pluginName.DS.$pluginName.DS.'tmpl'.DS.$file;
				$p->http = JURI::root()."plugins/content/{$pluginName}/{$pluginName}/tmpl/{$file}";
			}
		}
		return $p;
	}
	
	/**
	 * File size converter
	 */
	function ConvertSize($filesize){ 
	     if ($filesize >= 1073741824) 
	      $filesize = round($filesize / 1073741824 * 100) / 100 . " Gb"; 
	     elseif ($filesize >= 1048576) 
	      $filesize = round($filesize / 1048576 * 100) / 100 . " Mb"; 
	     elseif ($filesize >= 1024) 
	      $filesize = round($filesize / 1024 * 100) / 100 . " Kb"; 
	     else 
	      $filesize = $filesize . " b"; 
	     return $filesize; 
	} 	

	/**
	 * Check if file is image
	 */
	function findimg($filename) { 
		 $exts = substr(strrchr($filename, '.'), 1); 
		 if (($exts == 'jpg') or ($exts == 'png') or ($exts == 'gif') or ($exts == 'png') or ($exts == 'jpeg') )
		 	$exts = 1;
		 else
			$exts = 0;
		 return $exts; 
	} 
	
	/**
	 * Check file type
	 */
	function findtype($filename) { 
		 $exts = substr(strrchr($filename, '.'), 1);  
		 if (($exts == 'jpg') or ($exts == 'png') or ($exts == 'gif') or ($exts == 'png') or ($exts == 'jpeg') )
		 	$exts = 'image';
		 elseif ($exts == 'webdoc')
		 	$exts = 'text';
		 else
			$exts = 'application';
		 return $exts; 
	} 

	/**
	 * Get extension of file
	 */
	function findext($filename) { 
		 $exts = substr(strrchr($filename, '.'), 1); 
		 return $exts; 
	} 
	
}
