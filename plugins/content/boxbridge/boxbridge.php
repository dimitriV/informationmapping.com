<?php
/**
 * BoxBridge for Joomla by 2Value - Sander Potjer
 * Copyright (c) 2010 2Value. All rights reserved.
 * Released under the GNU/GPL license: http://www.gnu.org/copyleft/gpl.html
 * More info at http://www.boxbridge.net
 */

// no direct access
defined( '_JEXEC' ) or die( 'Restricted access' );

jimport( 'joomla.plugin.plugin' );

class plgContentBoxBridge extends JPlugin {

	function plgContentBoxBridge( &$subject, $params ) {
		parent::__construct( $subject, $params );
		
		$this->loadLanguage();
	}
	
	function onPrepareContent( &$article, &$params ) {
		$this->BoxBridge( $article, $params );
	}
	
	function onContentPrepare($context, &$article, &$params, $page = 0) {
		$this->BoxBridge( $article, $params );
	}

	function BoxBridge( &$article, &$params ) {

		// Plugin Name
		$plg_name	= 'boxbridge';

		// API Key from Box.net
		$apiKey 	= 'letftjd737m0hxnlupjy8mhz0ncg9s72';

		// API
		$mainframe= &JFactory::getApplication();
		$document = &JFactory::getDocument();

		// Check if plugin is enabled
		if(JPluginHelper::isEnabled('content',$plg_name)==false) return;

		// Load the plugin language file the proper way
		if($mainframe->isAdmin()){
			JPlugin::loadLanguage( 'plg_content_'.$plg_name );
		} else {
			JPlugin::loadLanguage( 'plg_content_'.$plg_name, 'administrator' );
		}
		
		// Check for Joomla Version 
		$joomla = new JVersion;
		$jversion = $joomla->RELEASE;

		// ----------------------------------- Get plugin parameters -----------------------------------

		// Outside Parameters
		if($jversion == '1.5') {
			if(!$params) $params = new JParameter(null);
			$plugin =& JPluginHelper::getPlugin('content',$plg_name);
			$pluginParams = new JParameter( $plugin->params );
		} else {
			$pluginParams = $this->params;
		}

		// BoxBridge Parameters
		$authToken			= $pluginParams->def('auth_token');
		$showTitle			= $pluginParams->def('title',1);
		$thumbSize			= $pluginParams->def('thumbnailsize',1);
		$showSize			= $pluginParams->def('size',1);
		$showId				= $pluginParams->def('id',1);
		$showShared			= $pluginParams->def('shared',1);
		$showSharedLink		= $pluginParams->def('sharedlink',1);
		$showCreated		= $pluginParams->def('created',1);
		$showModified		= $pluginParams->def('modified',1);
		$dateFormat			= $pluginParams->def('dateformat', '%d %b %Y, %H:%M');
		$showDescription	= $pluginParams->def('description',1);
		$showDownloadLink	= $pluginParams->def('downloadlink',0);
		$showDownloadTitle	= $pluginParams->def('downloadtitle','view');
		$showTooltip		= $pluginParams->def('tooltip',1);
		$bordercolor		= $pluginParams->def('bordercolor', '#D9E3FF');
		$hovercolor			= $pluginParams->def('hovercolor', '#EFF7FC');
		$subfolders			= $pluginParams->def('subfolders',0);

		// ----------------------------------- Prepare elements -----------------------------------

		// Includes
		require_once(dirname(__FILE__).DIRECTORY_SEPARATOR.$plg_name.DIRECTORY_SEPARATOR.'includes'.DIRECTORY_SEPARATOR.'boxlibrary.php');
		require_once(dirname(__FILE__).DIRECTORY_SEPARATOR.$plg_name.DIRECTORY_SEPARATOR.'includes'.DIRECTORY_SEPARATOR.'helper.php');

		// Simple performance check to determine whether plugin should process further
		$tagReplace = array("boxfolder" => "", "boxfile" => "");
		$grabTags = str_replace("(","",str_replace(")","",implode(array_keys($tagReplace),"|")));
		if (preg_match("#{(".$grabTags.")(.*?)}#s",$article->text)==false) return;

		// ----------------------------------- Head tag includes -----------------------------------
		$bbCSS 		= BoxBridgeHelper::getTemplatePath($plg_name,'css/template.css',$jversion);
		$bbCSS 		= $bbCSS->http;

		$bbhead = '
		<style type="text/css" media="all">
			@import "'.$bbCSS.'";
			div.files {border-color: '.$bordercolor.';}
			div.files div.boxitem {border-color: '.$bordercolor.';}
			div.files div.boxitem:hover {background-color: '.$hovercolor.' !important;}
			div.tool-tip {border-color: '.$bordercolor.';}
		</style>
		';

		// Append head includes
		$document->addCustomTag($bbhead);

		// ----------------------------------- Render the output -----------------------------------
		// START REPLACE LOOP
		foreach ($tagReplace as $plg_tag => $value) {
			// expression to search for
			$regex = "/{".$plg_tag."(.*?)}/i";

			if ($plg_tag == 'boxfolder'){
				// process tags
				if (preg_match_all($regex, $article->text, $matches, PREG_PATTERN_ORDER) > 0) {
					// start the replace loop

					foreach ($matches[0] as $key => $match) {
						$tagcontent 	= preg_replace("/ /","", preg_replace("/}/","", preg_replace("/{".$plg_tag."/", "", $match)));
						$box 			= new boxclient($apiKey, $authToken, $tagcontent, $dateFormat, $subfolders);
						$box->_debug = $pluginParams->def('debug', false) ? true : false;
						$tree 			= $box->getAccountTree ();
						$output->player = JFilterOutput::ampReplace($tagReplace[$plg_tag]);

						// check for errors
						if($tree === false) {
							JError::raiseWarning($box->getErrorCode(), $box->getErrorMsg());
						}

						// Fetch the template
						ob_start();
						$getTemplatePath = BoxBridgeHelper::getTemplatePath($plg_name,'folder.php',$jversion);
						$getTemplatePath = $getTemplatePath->file;
						include($getTemplatePath);
						$getTemplate = ob_get_contents();
						ob_end_clean();

						// Do the replace
						$article->text = preg_replace('{'.$match.'}', $getTemplate , $article->text);

					} // end foreach

				} // end if
			}


			if ($plg_tag == 'boxfile'){
				// process tags
				if (preg_match_all($regex, $article->text, $matches, PREG_PATTERN_ORDER) > 0) {
					// start the replace loop

					foreach ($matches[0] as $key => $match) {
						$tagcontent 	= preg_replace("/ /","", preg_replace("/}/","", preg_replace("/{".$plg_tag."/", "", $match)));
						$box 			= new boxclient($apiKey, $authToken, $tagcontent, $dateFormat, $subfolders);
						$box->_debug = $pluginParams->def('debug', false) ? true : false;
						$tree 			= $box->getFileInfo ();
						$output->player = JFilterOutput::ampReplace($tagReplace[$plg_tag]);

						// check for errors
						if($tree === false) {
							JError::raiseWarning($box->getErrorCode(), $box->getErrorMsg());
						}

						// Fetch the template
						ob_start();
						$getTemplatePath = BoxBridgeHelper::getTemplatePath($plg_name,'file.php',$jversion);
						$getTemplatePath = $getTemplatePath->file;
						include($getTemplatePath);
						$getTemplate = ob_get_contents();
						ob_end_clean();

						// Do the replace
						$article->text = preg_replace('{'.$match.'}', $getTemplate , $article->text);

					} // end foreach

				} // end if
			}

		} // end replace loop

	}

}