<?php
error_reporting(E_ALL);
ini_set('display_errors', 1);

require_once '../magento/app/Mage.php';

require_once '../magento/app/code/local/Storefront/VendorLoader/Model/Loader.php';
Storefront_VendorLoader_Model_Loader::enableLoader();

Mage::app('base', 'website');
Mage::getSingleton('core/session', array('name' => 'frontend'));
$session = Mage::getSingleton('customer/session');
?>
<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml">
<head>
    <meta http-equiv="Content-Type" content="text/html; charset=utf-8" />
    <meta http-equiv="X-UA-Compatible" content="IE=10" />
    <title>Renew Your FS Pro 2013 Subscription Today</title>
    <meta name="viewport" content="width=device-width, initial-scale=1.0" />
    <link href="css/bootstrap.css" rel="stylesheet" media="screen" />
    <link href="css/bootstrap-responsive.css" rel="stylesheet" media="screen" />
    <link href="css/font-awesome.min.css" rel="stylesheet" media="screen" />
    <link href="//fonts.googleapis.com/css?family=Tauri" rel="stylesheet" type="text/css" />
    <link href="css/imi.css" rel="stylesheet" />
    <script src="js/jquery-1.11.1.min.js"></script>
</head>

<body>
<div class="container">

    <div class="row-fluid" id="header">
        <div class="span12">
            <img src="images/logo-informationmapping.jpg" width="169" height="71" alt="Information Mapping" />
        </div>
    </div> <!-- end row fluid -->


    <div class="row-fluid">
        <div class="span12">
            <h1>Renew Your <strong>FS Pro 2013 Subscription</strong> Today</h1>
        </div>
    </div><!-- end row fluid -->

    <div class="row-fluid">
        <div class="span12">

            <?php
            $key = isset($_GET['key']) ? htmlentities($_GET['key']) : '';
            if(!$key)
            {
                //echo "geen key";    
                // geen key ingevuld dus toon formulier


                $verzenden = isset($_POST['submit']) ? htmlentities($_POST['submit']) : '';
                if($verzenden)
                {
                    //echo "verzonden";
                    $lickey = isset($_POST['lickey']) ? htmlentities($_POST['lickey']) : '';
                    $isValidLicense = Mage::helper('addlicense')->isValidLicense($lickey);
                    $tempkey = 'OvGEC-xGP93-9E7IQ-1tM8X-xQU5k-gN14B';
                    if(true)
                    {
                        header('Location: /subscription-renewal/index.php?key=' . $lickey);
                        exit;
                    }
                    elseif($lickey == $tempkey)
                    {
                        header('Location: /subscription-renewal/index.php?key=ffGEv-5GP4b-9E8Ir-3tM8X-pQU5k-6Fi4z');
                        exit;
                        ?>
                        <div class="alert alert-error">
                            <button type="button" class="close" data-dismiss="alert">&times;</button>
                            <strong>Invalid license key</strong>. Please try again.
                        </div>
                        <?php
                    }

                }
            } // einde if(!key)
            ?>

            <div class="well extra">

                <?php


                // e.g. key xjHE1-5GP4b-9E2Is-3tM8X-HQU5k-6ei4g with 120 activations

                $key = isset($_GET['key']) ? htmlentities($_GET['key']) : '';
                $addLicenseHelper = Mage::helper('addlicense');
                $isValidLicense = $addLicenseHelper->isValidLicense($key);
                if(!empty($key) && $isValidLicense == false)
                {
                   // header('Location: /subscription-renewal/index.php');
                    // exit;
                }

                $amount = $addLicenseHelper->getActivations($key);

                // Get Pack prices
                // 5 PACK
                $_productId = 558;
                $_product = Mage::getModel('catalog/product')->load($_productId);
                $_specialPrice = $_product->getFinalPrice();
                // 10 PACK
                $_productId2 = 566;
                $_product2 = Mage::getModel('catalog/product')->load($_productId2);
                $_specialPrice2 = $_product2->getFinalPrice();

                $_productId3 = 572;
                $_product3 = Mage::getModel('catalog/product')->load($_productId3);
                $_specialPrice3 = $_product3->getFinalPrice();
                ?>
                <?php
                if($amount != 0)
                {
                    ?>

                    You currently have
                    <?php
                    echo "<strong>" . $amount . "</strong>";
                    ?>
                    FS Pro 2013 license<?php
                    if($amount > 1) { echo "s"; }
                    ?>.

                    <?php
                }
                else
                {
                    ?>
                    You currently have 1 FS Pro 2013 license
                    <?php
                    if(!$key)
                        // geen key ingevuld dus toon formulier
                    {
                        ?>
                        <p>Please enter your FS Pro 2013 license key</p>

                        <form class="form-inline" action="" method="post" name="formulierke">
                            <div class="input-append">
                                <input class="input-large" type="text" value="" name="lickey" id="lickey" />
                                <input type="submit" name="submit" class="btn btn-info" value="Validate" />
                                <!--<button class="btn btn-info" name="submit" type="submit"><i class="icon-ok"></i>&nbsp;&nbsp;Validate</button>-->
                            </div>
                            <p class="help"><small>You can find your license key by clicking <strong>Help</strong> > <strong>Viewer</strong> on the FS Pro ribbon, or on your invoice e-mail.</small></p>
                        </form>

                        <?php
                    } // if(key)
                }
                ?>

            </div>
        </div>
    </div> <!-- end row fluid -->

    <?php
    if(!$key)
    { // geen key ingevuld
        ?>
        <div class="row-fluid">
            <div class="span6">
                <h2>Where can I find my license key?</h2>
                <div class="accordion" id="accordion2">

                    <div class="accordion-group">
                        <div id="collapseOne" class="accordion-body collapse in">
                            <div class="accordion-inner">
                                <p>In the FS Pro 2013 tab, go to <strong>Help</strong> and click on <strong>Viewer</strong>. You will see your license key in the License Viewer.</p>
                                <p><img src="images/licensekey.png" width="457" height="296" alt="License Viewer" /></p>
                            </div>
                        </div>
                    </div>

                </div>
            </div>
            <div class="span6 hidden-phone" id="image">
                <p class="text-center"><img src="images/fspro-banner.png" width="493" height="290" alt="FS Pro 2013" /></p>
            </div>
        </div><!--/row-fluid-->
        <?php
    }
    else
    { // wel key ingevuld, dus toon de producten
        ?>

        <div class="row-fluid" id="content">
            <div class="span3 regular">
                <h3>1 Year Subscription</h3>
                <div class="product-info-box">
                    <div class="product-essential">
                        <p class="newprice">US $
                            <?php
                            //     CALCULATE TIER PRICING
                            //$amount = 130;
                            if($amount >= 10 && $amount < 50)
                            {
                                //echo "tier 10";
                                echo "118.75"; // -5%
                            }
                            elseif($amount >= 50 && $amount < 100)
                            {
                                //echo "tier 50";
                                echo "115.625"; // -7,5%
                            }
                            elseif($amount >= 100)
                            {
                                //echo "tier 100";
                                echo "112.50"; // -10%
                            }
                            else
                            {
                                //echo $amount;
                                echo "125";
                            }
                            ?>
                        </p>
                        <p>Add 1 year to your current <br />
                            expiration date</p>
                        <form class="form-inline" action="/en/shop/checkout/cart/add/uenc/aHR0cDovL3d3dy5pbmZvcm1hdGlvbm1hcHBpbmcuY29tL2VuL3Nob3Avc29mdHdhcmUvZnNwcm8tMjAxMy9mcy1wcm8tMjAxMy1yZW5ld2FsLTEteWVhci1zdWJzY3JpcHRpb24_bGFuZ3VhZ2U9ZW4tR0I,/product/504/" method="post" id="product_addtocart_form">
                            <div class="input-append">
                                <input class="input-imi" type="text" value="<?php if($amount != 0) { echo $amount; } else { echo "1"; } ?>" maxlength="12" name="qty" id="qty">
                                <button type="button" class="btn btn-info" onclick="this.form.submit();"><i class="icon-shopping-cart"></i>&nbsp;&nbsp;Add to Cart</button>
                            </div>
                            <input type="hidden" name="bundle_option[677]" value="1059" />
                            <input type="hidden" name="key" value="<?php echo $key; ?>" />
                        </form>
                    </div>
                </div>
            </div>
            <div class="span3">
                <div <?php if($amount<5 ) {?> class="well" id="mostpopular" <?php }?> >
                    <h3>2 Year Subscription</h3>

                    <div class="product-info-box">
                        <div class="product-essential">
                            <p class="newprice">US $
                                <?php
                                //     CALCULATE TIER PRICING
                                if($amount >= 10 && $amount < 50)
                                {
                                    //echo "tier 10";
                                    echo "213.75"; // -5%
                                }
                                elseif($amount >= 50 && $amount < 100)
                                {
                                    //echo "tier 50";
                                    echo "208.125"; // -7,5%
                                }
                                elseif($amount >= 100)
                                {
                                    //echo "tier 100";
                                    echo "202.55"; // -10%
                                }
                                else
                                {
                                    //echo $amount;
                                    echo "225";
                                }
                                ?>
                            </p>
                            <p>Add 2 years to your current expiration date</p>
                            <form class="inline"
                                  action="/en/shop/checkout/cart/add/uenc/aHR0cDovL3d3dy5pbmZvcm1hdGlvbm1hcHBpbmcuY29tL2VuL3Nob3Avc29mdHdhcmUvZnNwcm8tMjAxMy9mcy1wcm8tMjAxMy1yZW5ld2FsLTIteWVhci1zdWJzY3JpcHRpb24_bGFuZ3VhZ2U9ZW4tR0I,/product/505/" method="post" id="product_addtocart_form">
                                <div class="input-append">
                                    <input class="input-imi" type="text" value="<?php if($amount != 0) { echo $amount; } else { echo "1"; } ?>" maxlength="12" name="qty" id="qty">
                                    <button type="button" class="btn btn-info" onclick="this.form.submit();"><i class="icon-shopping-cart"></i>&nbsp;&nbsp;Add to Cart</button>
                                </div>
                                <input type="hidden" name="bundle_option[678]" value="1060" />
                                <input type="hidden" name="key" value="<?php echo $key; ?>" />
                            </form>
                        </div>
                    </div>
                </div>
            </div>
            <div class="span3 regular">
                <h3>3 Year Subscription </h3>
                <div class="product-info-box">
                    <div class="product-essential">
                        <p class="newprice">US $
                            <?php
                            //     CALCULATE TIER PRICING
                            if($amount >= 10 && $amount < 50)
                            {
                                //echo "tier 10";
                                echo "284.05"; // -5%
                            }
                            elseif($amount >= 50 && $amount < 100)
                            {
                                //echo "tier 50";
                                echo "276.575"; // -7,5%
                            }
                            elseif($amount >= 100)
                            {
                                //echo "tier 100";
                                echo "269.1"; // -10%
                            }
                            else
                            {
                                //echo $amount;
                                echo "299";
                            }
                            ?>
                        </p>
                        <p>Add 3 years to your current <br />
                            expiration date</p>
                        <form class="inline" action="/en/shop/checkout/cart/add/uenc/aHR0cDovL3d3dy5pbmZvcm1hdGlvbm1hcHBpbmcuY29tL2VuL3Nob3Avc29mdHdhcmUvZnNwcm8tMjAxMy9mcy1wcm8tMjAxMy1yZW5ld2FsLTMteWVhci1zdWJzY3JpcHRpb24_bGFuZ3VhZ2U9ZW4tR0I,/product/506/" method="post" id="product_addtocart_form">

                            <div class="input-append">
                                <input class="input-imi" type="text" value="<?php if($amount != 0) { echo $amount; } else { echo "1"; } ?>" maxlength="12" name="qty" id="qty">
                                <button type="button" class="btn btn-info" onclick="this.form.submit();"><i class="icon-shopping-cart"></i>&nbsp;&nbsp;Add to Cart</button>
                            </div>
                            <input type="hidden" name="bundle_option[679]" value="1061" />
                            <input type="hidden" name="key" value="<?php echo $key; ?>" />
                        </form>
                    </div>
                </div>
            </div>
            <div class="span3 regular">
                <h3>Perpetual License</h3>
                <div class="product-info-box">
                    <div class="product-essential">
                        <p class="newprice">US $
                            <?php
                            //     CALCULATE TIER PRICING
                            if($amount >= 10 && $amount < 50)
                            {
                                //echo "tier 10";
                                echo "356.25"; // -5%
                            }
                            elseif($amount >= 50 && $amount < 100)
                            {
                                //echo "tier 50";
                                echo "346.875"; // -7,5%
                            }
                            elseif($amount >= 100)
                            {
                                //echo "tier 100";
                                echo "337.50"; // -10%
                            }
                            else
                            {
                                //echo $amount;
                                echo "375";
                            }
                            ?>
                        </p>
                        <p>Your FS Pro 2013 license <br />
                            does not expire</p>
                        <form class="inline" action="/en/shop/checkout/cart/add/uenc/aHR0cDovL3d3dy5pbmZvcm1hdGlvbm1hcHBpbmcuY29tL2VuL3Nob3Avc29mdHdhcmUvZnNwcm8tMjAxMy9mcy1wcm8tMjAxMy1yZW5ld2FsLXBlcnBldHVhbD9sYW5ndWFnZT1lbi1HQg,,/product/503/" method="post" id="product_addtocart_form">
                            <div class="input-append">
                                <input class="input-imi" type="text" value="<?php if($amount != 0) { echo $amount; } else { echo "1"; } ?>" maxlength="12" name="qty" id="qty">
                                <button type="button" class="btn btn-info" onclick="this.form.submit();"><i class="icon-shopping-cart"></i>&nbsp;&nbsp;Add to Cart</button>
                            </div>
                            <input type="hidden" name="bundle_option[676]" value="1058" />
                            <input type="hidden" name="key" value="<?php echo $key; ?>" />
                        </form>
                    </div>
                </div>
            </div>

        </div><!-- end row fluid -->
        <div class="row-fluid" id="content">
            <div class="span3 regular">
                <div <?php if($amount >= 5 && $amount<10 ) {?> class="well" id="mostpopular" <?php }?> >
                    <h3>5 Pack</h3>
                    <div class="product-info-box">
                        <div class="product-essential">
                            <p class="startprice">AS LOW AS</p>
                            <p class="newprice" id="newp5">US $559,00</p>
                            <div class="radio">
                                <label>
                                    <input type="radio" id="5P" name="optionsRadios" value="558"  onchange="fivepack()" checked>1 Year Subscription
                                </label>
                            </div>
                            <div class="radio">
                                <label>
                                    <input type="radio" id="5P2" name="optionsRadios" value="562"  onchange="fivepack()">2 Year Subscription
                                </label>
                            </div>
                            <div class="radio">
                                <label>
                                    <input type="radio" id="5P3" name="optionsRadios" value="563" onchange="fivepack()">3 Year Subscription
                                </label>
                            </div>
                            <div class="radio">
                                <label>
                                    <input type="radio" id="5P4" name="optionsRadios" value="564" onchange="fivepack()">Perpetual License
                                </label>
                            </div>
                            <script>
                                function fivepack() {
                                    if (document.getElementById("5P").checked){
                                        document.getElementById("newp5").innerHTML = "US $559,00";
                                        document.getElementById("5PS").value = "1197";
                                    }
                                    if (document.getElementById("5P2").checked){
                                        document.getElementById("newp5").innerHTML = "US $899,00";
                                        document.getElementById("5PS").value = "1198";
                                    }
                                    if (document.getElementById("5P3").checked){
                                        document.getElementById("newp5").innerHTML = "US $1.339,00";
                                        document.getElementById("5PS").value = "1199";
                                    }
                                    if (document.getElementById("5P4").checked){
                                        document.getElementById("newp5").innerHTML = "US $1.689,00";
                                        document.getElementById("5PS").value = "1200";
                                    }
                                }
                            </script>
                            <form class="form-inline" action="/en/shop/checkout/cart/add/uenc/aHR0cHM6Ly93d3cuaW5mb3JtYXRpb25tYXBwaW5nLmNvbS9lbi9zaG9wL3NvZnR3YXJlL2ZzcHJvLTIwMTMvZnMtcHJvLTIwMTMtNXBhY2sv/product/557/" method="post" id="product_addtocart_form">
                                <div class="input-append">
                                    <input class="input-imi" type="text" value="<?php if($amount != 0) { echo ceil(($amount/5)); } else { echo "1"; } ?>" maxlength="12" name="qty" id="qty">
                                    <button type="button" class="btn btn-info" onclick="this.form.submit();"><i class="icon-shopping-cart"></i>&nbsp;&nbsp;Add to Cart</button>
                                </div>
                                <input type="hidden" name="bundle_option[729]" id="5PS" value="1197"  />
                                <input type="hidden" name="key" value="<?php echo $key; ?>" />
                            </form>
                        </div>
                    </div>
                </div>
            </div>
            <div class="span3">
                <div <?php if($amount >= 10 && $amount<50 ) {?> class="well" id="mostpopular" <?php }?> >
                    <h3>10 Pack</h3>
                    <div class="product-info-box">
                        <div class="product-essential">
                            <p class="startprice">AS LOW AS</p>
                            <p class="newprice" id="newp10">US $999,00</p>
                            <div class="radio">
                                <label>
                                    <input type="radio" name="rbtn" value="10P1Y" id="10P" onchange="tenpack()" checked>1 Year Subscription
                                </label>
                            </div>
                            <div class="radio">
                                <label>
                                    <input type="radio" name="rbtn" value="10P2Y" id="10P2" onchange="tenpack()" >2 Year Subscription
                                </label>
                            </div>
                            <div class="radio">
                                <label>
                                    <input type="radio" name="rbtn" value="10P3Y" id="10P3" onchange="tenpack()" >3 Year Subscription
                                </label>
                            </div>
                            <div class="radio">
                                <label>
                                    <input type="radio" name="rbtn" value="10PP" id="10P4" onchange="tenpack()" >Perpetual License
                                </label>
                            </div>
                            <script>
                                function tenpack() {
                                    if (document.getElementById("10P").checked){
                                        document.getElementById("newp10").innerHTML = "US $999,00";
                                        document.getElementById("10PS").value = "1205";
                                    }
                                    if (document.getElementById("10P2").checked){
                                        document.getElementById("newp10").innerHTML = "US $1,799.00";
                                        document.getElementById("10PS").value = "1206";
                                    }
                                    if (document.getElementById("10P3").checked){
                                        document.getElementById("newp10").innerHTML = "US $2,389.00";
                                        document.getElementById("10PS").value = "1207";
                                    }
                                    if (document.getElementById("10P4").checked){
                                        document.getElementById("newp10").innerHTML = "US $2,999.00";
                                        document.getElementById("10PS").value = "1208";
                                    }
                                }
                            </script>
                            <form class="form-inline" action="/en/shop/checkout/cart/add/uenc/aHR0cHM6Ly93d3cuaW5mb3JtYXRpb25tYXBwaW5nLmNvbS9lbi9zaG9wL3NvZnR3YXJlL2ZzcHJvLTIwMTMvZnMtcHJvLTIwMTMtNXBhY2sv/product/565/" method="post" id="product_addtocart_form">
                                <div class="input-append">
                                    <input class="input-imi" type="text" value="<?php if($amount != 0) { echo ceil(($amount/10)); } else { echo "1"; } ?>" maxlength="12" name="qty" id="qty">
                                    <button type="button" class="btn btn-info" onclick="this.form.submit();"><i class="icon-shopping-cart"></i>&nbsp;&nbsp;Add to Cart</button>
                                </div>
                                <input type="hidden" id="10PS" name="bundle_option[730]" value="1205"  />
                                <input type="hidden" name="key" value="<?php echo $key; ?>" />
                            </form>
                        </div>
                    </div>
                </div>
            </div>
            <div class="span3 regular">
                <div <?php if($amount >= 50) {?> class="well" id="mostpopular" <?php }?> >
                    <h3>50 Pack</h3>
                    <div class="product-info-box">
                        <div class="product-essential">
                            <p class="startprice">AS LOW AS</p>
                            <p class="newprice" id="newp50">US $4,675.00</p>
                            <div class="radio">
                                <label>
                                    <input type="radio" name="rbtn2" value="50P1Y" id="50P" checked onchange="fiftypack()" >1 Year Subscription
                                </label>
                            </div>
                            <div class="radio">
                                <label>
                                    <input type="radio" name="rbtn2" value="50P2Y" id="50P2" onchange="fiftypack()" >2 Year Subscription
                                </label>
                            </div>
                            <div class="radio">
                                <label>
                                    <input type="radio" name="rbtn2" value="50P3Y" id="50P3" onchange="fiftypack()" >3 Year Subscription
                                </label>
                            </div>
                            <div class="radio">
                                <label>
                                    <input type="radio" name="rbtn2" value="50PP" id="50P4" onchange="fiftypack()" >Perpetual
                                </label>
                            </div>
                            <script>
                                function fiftypack() {
                                    if (document.getElementById("50P").checked){
                                        document.getElementById("newp50").innerHTML = "US $4,675.00";
                                        document.getElementById("50PS").value = "1214";
                                    }
                                    if (document.getElementById("50P2").checked){
                                        document.getElementById("newp50").innerHTML = "US $8,435.00";
                                        document.getElementById("50PS").value = "1215";
                                    }
                                    if (document.getElementById("50P3").checked){
                                        document.getElementById("newp50").innerHTML = "US $11,215.00";
                                        document.getElementById("50PS").value = "1216";
                                    }
                                    if (document.getElementById("50P4").checked){
                                        document.getElementById("newp50").innerHTML = "US $14,065.00";
                                        document.getElementById("50PS").value = "1217";
                                    }
                                }
                            </script>
                            <form class="form-inline" action="/en/shop/checkout/cart/add/uenc/aHR0cHM6Ly93d3cuaW5mb3JtYXRpb25tYXBwaW5nLmNvbS9lbi9zaG9wL3NvZnR3YXJlL2ZzcHJvLTIwMTMvZnMtcHJvLTIwMTMtNXBhY2sv/product/570/" method="post" id="product_addtocart_form">
                                <div class="input-append">
                                    <input class="input-imi" type="text" value="<?php if($amount != 0) { echo ceil(($amount/50)); } else { echo "1"; } ?>" maxlength="12" name="qty" id="qty">
                                    <button type="button" class="btn btn-info" onclick="this.form.submit();"><i class="icon-shopping-cart"></i>&nbsp;&nbsp;Add to Cart</button>
                                </div>
                                <input type="hidden" id="50PS" name="bundle_option[731]" value="1214" />
                                <input type="hidden" name="key" value="<?php echo $key; ?>" />
                            </form>
                        </div>
                    </div>
                </div>
            </div>


        </div><!-- end row fluid -->

        <div class="row-fluid">
            <div class="span6 hidden-phone" id="image">
                <p class="text-center"><img src="images/fspro-banner.png" width="493" height="290" alt="FS Pro 2013" /></p>
            </div>
            <div class="span6">
                <h2>Frequently Asked Questions</h2>
                <div class="accordion" id="accordion2">

                    <div class="accordion-group">
                        <div class="accordion-heading">
                            <a class="accordion-toggle" data-toggle="collapse" data-parent="#accordion2" href="#collapseOne">
                                When does my FS Pro 2013 subscription expire?
                            </a>
                        </div>
                        <div id="collapseOne" class="accordion-body collapse">
                            <div class="accordion-inner">
                                <p>In the FS Pro 2013 tab, go to <strong>Help</strong> and click on <strong>Viewer</strong>. You can find the expiration date of your subscription in the License Viewer.</p>
                                <p><em><strong>Note</strong></em>: if you see &quot;No end date&quot; there, then you have a perpetual license and do not need to renew your subscription.</p>
                                <p><img src="images/licenseviewer.png" width="457" height="296" alt="License Viewer" /></p>
                            </div>
                        </div>
                    </div>

                    <div class="accordion-group">
                        <div class="accordion-heading">
                            <a class="accordion-toggle" data-toggle="collapse" data-parent="#accordion2" href="#collapseTwo">
                                What happens if I don't renew my subscription in time?
                            </a>
                        </div>
                        <div id="collapseTwo" class="accordion-body collapse">
                            <div class="accordion-inner">
                                <p>If you don't renew your subscription in time, you will not be able to attach FS Pro to a document anymore. You will get the following message:</p>
                                <p><img src="images/expired.png" width="400" height="315" alt="Subscription expired" /></p>
                                <p>You can still renew your subscription then. To do so, enter your  new key on this screen and click <strong>Apply Key</strong>.</p>
                            </div>
                        </div>
                    </div>

                    <div class="accordion-group">
                        <div class="accordion-heading">
                            <a class="accordion-toggle" data-toggle="collapse" data-parent="#accordion2" href="#collapseThree">
                                What happens if I renew my subscription before it has expired?
                            </a>
                        </div>
                        <div id="collapseThree" class="accordion-body collapse">
                            <div class="accordion-inner">You will never &quot;lose&quot; any days from   your subscription. If you enter your new  key for instance one week before your subscription expires, it will work for one year plus one week. </div>
                        </div>
                    </div>

                    <div class="accordion-group">
                        <div class="accordion-heading">
                            <a class="accordion-toggle" data-toggle="collapse" data-parent="#accordion2" href="#collapseFour">
                                How do I renew my subscription in FS Pro 2013?
                            </a>
                        </div>
                        <div id="collapseFour" class="accordion-body collapse">
                            <div class="accordion-inner">
                                <p>If your license has not expired yet, you can enter your new  key in FS Pro by clicking <strong>Help</strong>, and then <strong>Renew Subscription</strong>. </p>
                                <p><img src="images/renewsubscription.png" width="251" height="135" alt="Renew Subscription" /></p>
                                <p><strong><em>Result</em></strong>: You will see a dialog window where you can enter your new  key.</p>
                            </div>
                        </div>
                    </div>

                </div>
            </div>

        </div><!--/row-fluid-->

        <?php
    }
    ?>
    <div class="row-fluid" id="footer">
        <div class="span12">
            <p>&copy; <?php echo date("Y"); ?> Information Mapping International | <a href="http://www.informationmapping.com">www.informationmapping.com</a> <span class="visible-phone">| <a href="tel://3292531425">+32 9 253 14 25</a></span> | Contact our office in <a href="http://www.informationmapping.com/us/contact">US</a>, <a href="http://www.informationmapping.com/en/contact-3">Europe</a> or <a href="http://www.informationmapping.com/in/contact-2">Asia</a></p>
        </div>
    </div>

</div> <!-- end container -->


<script src="http://code.jquery.com/jquery.js" type="text/javascript"></script>
<script src="js/bootstrap.min.js" type="text/javascript"></script>
<script src="js/blueimp-gallery.min.js" type="text/javascript"></script>

<!-- Google Analytics -->
<script>
    (function(i,s,o,g,r,a,m){i['GoogleAnalyticsObject']=r;i[r]=i[r]||function(){
            (i[r].q=i[r].q||[]).push(arguments)},i[r].l=1*new Date();a=s.createElement(o),
        m=s.getElementsByTagName(o)[0];a.async=1;a.src=g;m.parentNode.insertBefore(a,m)
    })(window,document,'script','//www.google-analytics.com/analytics.js','ga');

    ga('create', 'UA-24028581-1', 'auto', {'allowLinker': true});
    // On every domain.
    ga('require', 'linker');
    // List of every domain to share linker parameters.
    ga('linker:autoLink', ['informationmapping.com', 'informationmapping-webinars.com']);
    ga('send', 'pageview'); // Send hits after initializing the auto-linker plug-in.
    ga('require', 'ecommerce');
</script>
<!-- End Google Analytics -->

</body>
</html>
