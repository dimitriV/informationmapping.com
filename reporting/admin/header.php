<?php
session_start();
?>
<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml">
<head>
<meta http-equiv="Content-Type" content="text/html; charset=utf-8" />
<title>Partner Reporting - Information Mapping International nv</title>
    <link href="../../admin/bootstrap/css/bootstrap.min.css" rel="stylesheet" media="screen">
    <link href="../../admin/bootstrap/css/bootstrap-responsive.min.css" rel="stylesheet" media="screen">
    <link href="../../admin/bootstrap/css/datepicker.css" rel="stylesheet">
    <link href="<?php echo $rootdir; ?>admin/assets/styles.css" rel="stylesheet" media="screen">
    <!-- HTML5 shim, for IE6-8 support of HTML5 elements -->
    <!--[if lt IE 9]>
            <script src="http://html5shim.googlecode.com/svn/trunk/html5.js"></script>
        <![endif]-->

    <?php
		if($page == "FS Pro 2013: license files")
		{
		?>
    <!-- we need jquery library and jquery ui library and jquery css to run this sciprt-->
    <link rel="stylesheet" href="http://code.jquery.com/ui/1.10.2/themes/smoothness/jquery-ui.css" />
    <script src="http://code.jquery.com/jquery-1.9.1.js"></script>
    <script src="http://code.jquery.com/ui/1.10.2/jquery-ui.js"></script>
    <script>
	$(document).ready(function(){
		$( "#suggestionbox" ).autocomplete({
			source: "suggestions.php"
		});
	})
	</script>
    <?php	
		}
		?>

<link rel="stylesheet" type="text/css" href="<?php echo $rootdir; ?>style.css" />
</head>
