<?php 
session_start();
session_unset(); // alle variabelen vrijgeven
session_destroy(); // sessie afsluiten
$page = "Logout";
?>
<!DOCTYPE html>
<html class="no-js">
    <head>
    <title>CRM | <?php echo $page; ?></title>
    <link href="bootstrap/css/bootstrap.min.css" rel="stylesheet" media="screen">
    <link href="bootstrap/css/bootstrap-responsive.min.css" rel="stylesheet" media="screen">
    <!--<link href="vendors/easypiechart/jquery.easy-pie-chart.css" rel="stylesheet" media="screen">-->
    <link href="assets/styles.css" rel="stylesheet" media="screen">
    <!-- HTML5 shim, for IE6-8 support of HTML5 elements -->
    <!--[if lt IE 9]>
            <script src="http://html5shim.googlecode.com/svn/trunk/html5.js"></script>
        <![endif]-->
    <!--<script src="vendors/modernizr-2.6.2-respond-1.1.0.min.js"></script>-->
    </head>
<body>

<div class="container-fluid">
            <div class="row-fluid">

<div id="content">

<h2>Logout</h2>

<p>You are logged out successfully.</p>
<p><a href="http://www.informationmapping.com/reporting/admin/">Log in again</a></p>

</div>
</div>
</div>


</body>
</html>