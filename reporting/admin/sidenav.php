<div class="span2" id="sidebar">
                    <ul class="nav nav-list bs-docs-sidenav nav-collapse collapse">
                        <li <?php if($page=="Dashboard") { echo "class='active'"; } ?>>
                        	<a href="index.php">Dashboard</a>
                        </li>
                        <?php if($_SESSION["function"] != "it") { ?>
                        <li <?php if($page=="FS Pro live demo") { echo "class='active'"; } ?>>
                        	<a href="fspro_live_demo.php">FS Pro live demo</a>
                        </li>
                        <?php } ?>
                        <li <?php if($page=="Search license key") { echo "class='active'"; } ?>>
                        	<a href="search_key.php">Search/Replace Key</a>
                        </li>
                        <?php if($_SESSION["function"] != "it") { ?>
                        <li <?php if($page=="contact") { echo "class='active'"; } ?>>
                        	<a href="contact.php">Add info@ senders</a>
                        </li>
                        <?php } ?>
                        <li class="nav-header">Web Store</li>
                        <?php if($_SESSION["function"] != "it") { ?>
                        <li <?php if($page=="Store orders per month") { echo "class='active'"; } ?>>
                        	<a href="store_orders_month.php" target="_blank">Store orders per month</a>
                        </li>
                        <?php } ?>
                        <?php if($_SESSION["function"] != "it") { ?>
                        <li <?php if($page=="Partner web store totals") { echo "class='active'"; } ?>>
                        	<a href="partner_orders.php">Partner web store totals</a>
                        </li>
                        <?php } ?>
                        <li <?php if($page=="Web Store Orders US") { echo "class='active'"; } ?>>
                        	<a href="us_orders_keys.php">Web Store Orders US</a>
                        </li> 
                       <?php if($_SESSION["function"] != "it") { ?>
                       <li <?php if($page=="Products: amounts ordered") { echo "class='active'"; } ?>>
                       		<a href="product_amount_ordered.php">Ordered Products</a>
                       </li>
                       <?php } ?>
                       <?php if($_SESSION["function"] != "it") { ?>
                       <li <?php if($page=="FS Pro: amounts ordered") { echo "class='active'"; } ?>>
                       		<a href="fspro_amount_ordered.php">Ordered FS Pro</a>
                       </li>
                       <?php } ?>
                       <?php if($_SESSION["function"] != "it") { ?>
                       <li <?php if($page=="E-learning: amounts ordered") { echo "class='active'"; } ?>>
                       		<a href="elearning_amount_ordered.php">Ordered E-learning</a>
                       </li>
                       <?php } ?>
                       <?php if($_SESSION["function"] != "it") { ?>
                       <li <?php if($page=="Certification: amounts ordered") { echo "class='active'"; } ?>>
                       		<a href="certification_amount_ordered.php">Ordered Certification</a>
                       </li>
                       <?php } ?>
                       <?php if($_SESSION["function"] != "it") { ?>
                       <li <?php if($page=="Ordered Training Material") { echo "class='active'"; } ?>>
                       		<a href="delta_shop_reporting.php">Ordered Training Material</a>
                       </li>
                       <?php } ?>
                       <?php if($_SESSION["function"] != "it") { ?>
                       <li <?php if($page=="Ordered Per Product Group") { echo "class='active'"; } ?>>
                       		<a href="store_orders_productgroup.php">Ordered Per Product Group</a>
                       </li>
                       <?php } ?>
                       <?php if($_SESSION["function"] != "it") { ?>
                       <li <?php if($page=="Products: never sold") { echo "class='active'"; } ?>>
                       		<a href="product_never_sold.php">Never sold</a>
                       </li>
                       <?php } ?>
                       <?php if($_SESSION["function"] != "it") { ?>
                       <li <?php if($page=="Products: promotion codes") { echo "class='active'"; } ?>>
                       		<a href="product_promotion.php">Promotion codes</a>
                       </li>
                       <?php } ?>
                       <?php if($_SESSION["function"] != "it") { ?>
                       <li <?php if($page=="FS Pro 2013: renewals") { echo "class='active'"; } ?>>
                       		<a href="fspro_renewals.php">FS Pro renewal orders</a>
                       </li>
                       <?php } ?>
                       <?php if($_SESSION["function"] != "it") { ?>
                       <li <?php if($page=="Customers: buyers per product") { echo "class='active'"; } ?>>
                       		<a href="customer_buyer.php">Customers: buyers per product</a>
                       </li>
                       <?php } ?>
                       <?php if($_SESSION["function"] != "it") { ?>
                       <li <?php if($page=="Customers: order total") { echo "class='active'"; } ?>>
                       		<a href="customer_ordertotal.php">Customers: order total</a>
                       </li>
                       <?php } ?>
                       <li class="nav-header">Academy</li>
                       <?php if($_SESSION["function"] != "it") { ?>
                       <li <?php if($page=="Current enrollments") { echo "class='active'"; } ?>>
                       		<a href="moodle_current_enrollments.php" target="_blank">Current enrollments</a>
                       </li>
                       <?php } ?>
                       <?php if($_SESSION["function"] != "it") { ?>
                        <li <?php if($page=="All enrollments") { echo "class='active'"; } ?>>
                        	<a href="moodle_all_enrollments.php">All enrollments</a>
                        </li>
                        <?php } ?>
                        <?php if($_SESSION["function"] != "it") { ?>
                        <li <?php if($page=="IMP Exam results") { echo "class='active'"; } ?>>
                        	<a href="moodle_imp.php">IMP Exam results</a>
                        </li>
                        <?php } ?>
                        <li class="nav-header">Activation server</li>
                        <li <?php if($page=="FS Pro 2013: license files") { echo "class='active'"; } ?>>
                        	<a href="fspro_license_files.php">FS Pro license files</a>
                        </li>
                       <li class="nav-header">Admin</li>
                       <?php if($_SESSION["function"] != "it") { ?>
                       <li <?php if($page=="Admin: associate guest purchase") { echo "class='active'"; } ?>>
                       		<a href="associate_guest_purchase.php">Associate guest purchase with customer account</a>
                       </li>
                       <?php } ?>
                        <?php if($_SESSION["function"] != "it") { ?>
                       <li <?php if($page=="Admin: Extend Enrollments") { echo "class='active'"; } ?>>
                       		<a href="extend_enrollments.php">Extend Enrollments</a>
                       </li>
                        <?php } ?>
                       <li <?php if($page=="Admin: add volume key") { echo "class='active'"; } ?>>
                       		<a href="add_volume_key.php">Add volume key</a>
                       </li>

                       <?php if($_SESSION["function"] != "it") { ?>
                       <li <?php if($page=="Admin: create student keys") { echo "class='active'"; } ?>>
                       		<a href="studentkey_create.php">Create student keys</a>
                       </li>
                       <?php } ?>
                    </ul>
                </div>
 				<!--/span-->