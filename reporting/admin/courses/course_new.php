<?php
$rootdir="../../";
$page="Courses";
include("../header.php");
include("../connect.php");
?>

<body>
<?php
include("../topnav.php");
?>
<div class="container-fluid">
            <div class="row-fluid">
                
                <?php
				//include("../sidenav.php");
				?>
                
                <div class="span10" id="content">
                    <div class="row-fluid">
                    	<div class="span12">
                        
                            <!-- block -->
                            <div class="block">
                                <div class="navbar navbar-inner block-header">
                                <h3>Add a course</h3>     
                                </div>
                                <div class="block-content collapse in">
<?php 
if (!isset($_SESSION["username"])){
	echo $niet_ingelogd;
}
// hieronder staan de functies die toegelaten worden tot deze pagina
elseif($_SESSION["function"] != "admin"){
	echo $geen_toegang;
}else{
?>

<form action="course_list.php" method="post">
<table cellpadding="0" cellspacing="0" width="500">
<tr>
<th>Course</th>
<td><input name="course_name" type="text" size="50" /> <input type="hidden" name="action" value="insert"> <input type="submit" name="Submit" value="Add"></td>
</tr>
</table>
</form>
<?php
}
?>

                                </div>
                            </div>
                            <!-- /block -->


</div>
</div>
</div>
</div>
            <hr>
<?php
include("../footer.php");
?>
</html>
