<?php
$rootdir="../../";
$page="Courses";
include("../header.php");
include("../connect.php");

$action = isset($_POST['action']) ? trim($_POST['action']) : '';
$action = isset($_GET['action']) ? trim($_GET['action']) : '';


if($action=="delete"){
  $course_id=$_GET['course_id'];
  	
  $course_SQL_del="DELETE FROM reporting_courses WHERE course_id=$course_id";
  $bool=mysql_query($course_SQL_del);
  if($bool==1) echo "<div id='adminok'>The course has been deleted.</div>";
  if($bool<>1) echo "<div id='adminnok'>An error occurred when trying to delete the course.</div>";
}

if($action=="insert"){

  $course_name=mysql_real_escape_string($_POST['course_name']);	
$active_default = "yes";
  $course_SQL_insert="INSERT INTO reporting_courses (course_name, active) VALUES ('$course_name','$active_default')";
  $bool=mysql_query($course_SQL_insert);
  if($bool==1) echo "<div id='adminok'>The course has been added.</div>";
  if($bool<>1) echo "<div id='adminnok'>An error occurred when trying to add the course.</div>";
}

if($action=="update"){

  $course_name=$_POST['course_name'];
  $course_id=$_POST['course_id'];
  $active=$_POST['active'];
	
  $course_SQL_update="UPDATE reporting_courses SET course_name='$course_name', active='$active' WHERE course_id='$course_id'";
  $bool=mysql_query($course_SQL_update);
  if($bool==1) echo "<div id='adminok'>The course has been saved.</div>";
  if($bool<>1) echo "<div id='adminnok'>An error occurred when trying to save the course.</div>";  
}

$course_SQL="SELECT * FROM reporting_courses ORDER BY active DESC, course_name ASC";
$course_result=mysql_query($course_SQL);
?>
<body>
<?php
include("../topnav.php");
?>
<div class="container-fluid">
            <div class="row-fluid">
                
                <?php
				//include("../sidenav.php");
				?>
                
                <div class="span10" id="content">
                    <div class="row-fluid">
                    	<div class="span12">
                        
                            <!-- block -->
                            <div class="block">
                                <div class="navbar navbar-inner block-header">
                                <h3>Available courses</h3>     
                                </div>
                                <div class="block-content collapse in">
<?php 
if (!isset($_SESSION["username"])){
	echo $niet_ingelogd;
}
// hieronder staan de functies die toegelaten worden tot deze pagina
elseif($_SESSION["function"] != "admin"){
	echo $geen_toegang;
}else{
?>
    <p class="buttons"><a href="course_new.php" class="add">Add New Course</a></p>
	<table width="100%" border="0" cellspacing="0" cellpadding="0" class="table table-striped" id="table">
  <tr>
    <th colspan="3">Course name</th>
  </tr>

<?php
while($course=mysql_fetch_array($course_result)){
?>
    
	<tr <? if($course['active']=="no") { echo "style='color:#ff0000;'"; } ?>>
    <td><?php echo $course['course_name'] ?></td>
    <td width="50" align="center"><a href=course_edit.php?course_id=<?php echo $course['course_id'] ?>><img src="../../images/edit.png" alt="Edit" /></a></td>
    <td width="50" align="center"><a href=course_list.php?course_id=<?php echo $course['course_id'] ?>&action=delete onClick="return confirm('Are you sure you want to delete this course?')"><img src="../../images/delete.jpg" alt="Delete" /></a></td>
  </tr>

<?php
}
mysql_close();
?>          
 </table>
 <?php
 }
 ?>
 

                                </div>
                            </div>
                            <!-- /block -->


</div>
</div>
</div>
</div>
            <hr>
<?php
include("../footer.php");
?>
</html>