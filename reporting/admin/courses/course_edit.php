<?php
$rootdir="../../";
$page="Courses";
include("../header.php");
include("../connect.php");

$SQL_course="SELECT * FROM reporting_courses WHERE course_id=" . $_GET['course_id'];
$course_result=mysql_query($SQL_course);
$course=mysql_fetch_array($course_result);
mysql_close();
?>

<body>
<?php
include("../topnav.php");
?>
<div class="container-fluid">
            <div class="row-fluid">
                
                <?php
				//include("../sidenav.php");
				?>
                
                <div class="span10" id="content">
                    <div class="row-fluid">
                    	<div class="span12">
                        
                            <!-- block -->
                            <div class="block">
                                <div class="navbar navbar-inner block-header">
                                <h3>Edit a course</h3>     
                                </div>
                                <div class="block-content collapse in">
<?php 
if (!isset($_SESSION["username"])){
	echo $niet_ingelogd;
}
// hieronder staan de functies die toegelaten worden tot deze pagina
elseif($_SESSION["function"] != "admin"){
	echo $geen_toegang;
}else{
?>

<form action="course_list.php?action=update" method="post">
<table cellpadding="3" cellspacing="0" width="500">
<tr>
<th>Course</th>
<td><input name="course_name" type="text" value="<?php echo $course['course_name'] ?>" size="50" /> 

<input type="hidden" name="course_id" value="<?php echo $course['course_id'] ?>"> 
</td>
</tr>
<tr>
<th>Is active</th>
<td><select name="active">
<?
if ($course['active']=="yes") {
			 echo "<option value='yes' selected='selected'>yes</option>
			       <option value='no'>no</option>";
			 } else {
			 echo "<option value='no' selected='selected'>no</option>
			       <option value='yes'>yes</option>";	 
			 }
?>
</select></td>
</tr>
<tr>
<td>



<input type="hidden" name="action" value="update">
<input type="submit" name="Submit" class="submit" value="Save"></td>
<td>&nbsp;</td>
</tr>
</table>
</form>
<?php
}
?>

                                </div>
                            </div>
                            <!-- /block -->


</div>
</div>
</div>
</div>
            <hr>
<?php
include("../footer.php");
?>
</html>