<?php
$rootdir="../../";
$page="Reports";
include("../header.php");
include("../connect.php");

$SQL_report="SELECT * FROM reporting_report WHERE report_id=" . $_GET['report_id'];
$report_result=mysql_query($SQL_report);
$report=mysql_fetch_array($report_result);
?>

<body>
<?php
include("../topnav.php");
?>
<div class="container-fluid">
<div class="row-fluid">

<?php
//include("../sidenav.php");
?>

<div class="span10" id="content">
<div class="row-fluid">
<div class="span12">

<!-- block -->
<div class="block">
<div class="navbar navbar-inner block-header">
    <h3><?php echo $report['company'] ?> Report Details for <?php echo $report['month'] ?> <?php echo $report['year'] ?></h3>
</div>
<div class="block-content collapse in">
<?php
if (!isset($_SESSION["username"])){
    echo $niet_ingelogd;
}
// hieronder staan de functies die toegelaten worden tot deze pagina
elseif($_SESSION["function"] != "admin"){
    echo $geen_toegang;
}else{
    ?>

    <p class="buttons"><a href="javascript:history.back()" class="back">Back</a> <a href="javascript:window.print()" class="print">Print</a></p>

    <table width="100%"  border="0" cellpadding="0" cellspacing="0">
    <tr>
    <td width="50%" style="padding-right:20px;" valign="top">

        <?
        /* info om eerste blok te editeren */

        if(isset($_GET['action'])) $action=$_GET['action'];
        if(isset($_POST['action'])) $action=$_POST['action'];

        if(isset($_POST['editperson'])) {

            $SQL_person="SELECT * FROM reporting_report, reporting_partners WHERE reporting_report.partner_id=reporting_partners.partner_id AND report_id=" . $_GET['report_id'];
            $person_result=mysql_query($SQL_person);
            $person=mysql_fetch_array($person_result);
            ?>

            <form action="<?php echo $_SERVER['PHP_SELF']; ?>?report_id=<?php echo $person['report_id'] ?>" method="post">
                <table width="100%" border="0" cellpadding="0" cellspacing="0" class="report">
                    <tr>
                        <th colspan="6" class="detail">Report submitted by
                            <input type="hidden" name="report_id" value="<?php echo $person['report_id'] ?>">
                            <input type="hidden" name="action" value="update">
                            <input type="submit" name="saveperson" class="save" value="Save" style="width:20px;float:right;">
                        </th>
                    </tr>
                    <tr>
                        <td width="120"><strong>First name</strong></td>
                        <td colspan="5"><input type="text" size="40" name="first" value="<?php echo $person['first'] ?>" /></td>
                    </tr>
                    <tr>
                        <td><strong>Last name</strong></td>
                        <td colspan="5"><input type="text" size="40" name="last" value="<?php echo $person['last'] ?>" /></td>
                    </tr>
                    <tr>
                        <td><strong>E-mail</strong></td>
                        <td colspan="5"><input type="text" size="40" name="email" value="<?php echo $person['email'] ?>" /></td>
                    </tr>
                </table>
            </form>

        <?
        }else{

            if(isset($_POST['saveperson'])) {
                if($action=="update"){

                    $first=$_POST['first'];
                    $last=$_POST['last'];
                    $email=$_POST['email'];
                    $report_id=$_POST['report_id'];

                    $person_SQL_update="UPDATE reporting_report SET first='$first',last='$last',email='$email' WHERE report_id='$report_id'";
                    $bool=mysql_query($person_SQL_update);
                    if($bool==1) echo "<div id='adminok_detail'>The changes have been saved.</div>";
                    if($bool<>1) echo "<div id='adminnok_detail'>An error occurred when trying to save the changes.</div>";

                }}

            $SQL_person="SELECT * FROM reporting_report, reporting_partners WHERE reporting_report.partner_id=reporting_partners.partner_id AND report_id=" . $_GET['report_id'];
            $person_result=mysql_query($SQL_person);
            $person=mysql_fetch_array($person_result);
            ?>

            <table width="100%" border="0" cellpadding="0" cellspacing="0" class="report">
                <tr>
                    <th colspan="6" class="detail">Report submitted by
                        <form action="<?php echo $_SERVER['PHP_SELF']; ?>?report_id=<?php echo $person['report_id'] ?>" method="post" style="width:20px;float:right;">
                            <input type="hidden" name="action" value="edit">
                            <input type="submit" name="editperson" class="edit" value="Edit" >
                        </form>
                    </th>
                </tr>
                <tr>
                    <td width="120"><strong>First name</strong></td>
                    <td colspan="5"><?php echo $person['first'] ?></td>
                </tr>
                <tr>
                    <td><strong>Last name</strong></td>
                    <td colspan="5"><?php echo $person['last'] ?></td>
                </tr>
                <tr>
                    <td><strong>E-mail</strong></td>
                    <td colspan="5"><?php echo $person['email'] ?></td>
                </tr>
                <tr>
                    <td><strong>Submitted on</strong></td>
                    <td colspan="5"><?php echo $person['submit_time'] ?></td>
                </tr>
            </table>
        <?
        }
        ?>

    </td>
    <td width="50%" style="padding-left:20px;" valign="top">

        <?
        /* info om tweede blok te editeren */

        if(isset($_GET['action'])) $action=$_GET['action'];
        if(isset($_POST['action'])) $action=$_POST['action'];

        if(isset($_POST['edittime'])) {

            $SQL_person="SELECT * FROM reporting_report, reporting_partners WHERE reporting_report.partner_id=reporting_partners.partner_id AND report_id=" . $_GET['report_id'];
            $time_result=mysql_query($SQL_time);
            $time=mysql_fetch_array($time_result);
            ?>

            <form action="<?php echo $_SERVER['PHP_SELF']; ?>?report_id=<?php echo $time['report_id'] ?>" method="post">
                <table width="100%" border="0" cellpadding="0" cellspacing="0" class="report">
                    <tr>
                        <th colspan="6" class="detail">Reporting for
                            <input type="hidden" name="report_id" value="<?php echo $time['report_id'] ?>">
                            <input type="hidden" name="action" value="update">
                            <input type="submit" name="savetime" class="save" value="Save" style="width:20px;float:right;">
                        </th>
                    </tr>
                    <tr>
                        <td width="120"><strong>Company</strong></td>
                        <td colspan="5">

                            <select name="partner_id">
                                <?php
                                $partner_SQL="SELECT * FROM reporting_partners WHERE active='yes' ORDER BY partner_company";
                                $partner_result=mysql_query($partner_SQL);
                                while($partner=mysql_fetch_array($partner_result)){
                                    ?>
                                    <option value="<?php echo $partner['partner_id'] ?>" <?php if(($_POST['partner_id'])==$time['partner_id']) { echo "selected='selected'";} ?>><?php echo $partner['partner_company'] ?></option>
                                <?php
                                }
                                ?>
                            </select>
                        </td>
                    </tr>
                    <tr>
                        <td><strong>Period</strong></td>
                        <td colspan="5">

                            <select name="month">
                                <option <?php if(($time['month'])=="January") { echo "selected='selected'";} ?>>January</option>
                                <option <?php if(($time['month'])=="February") { echo "selected='selected'";} ?>>February</option>
                                <option <?php if(($time['month'])=="March") { echo "selected='selected'";} ?>>March</option>
                                <option <?php if(($time['month'])=="April") { echo "selected='selected'";} ?>>April</option>
                                <option <?php if(($time['month'])=="May") { echo "selected='selected'";} ?>>May</option>
                                <option <?php if(($time['month'])=="June") { echo "selected='selected'";} ?>>June</option>
                                <option <?php if(($time['month'])=="July") { echo "selected='selected'";} ?>>July</option>
                                <option <?php if(($time['month'])=="August") { echo "selected='selected'";} ?>>August</option>
                                <option <?php if(($time['month'])=="September") { echo "selected='selected'";} ?>>September</option>
                                <option <?php if(($time['month'])=="October") { echo "selected='selected'";} ?>>October</option>
                                <option <?php if(($time['month'])=="November") { echo "selected='selected'";} ?>>November</option>
                                <option <?php if(($time['month'])=="December") { echo "selected='selected'";} ?>>December</option>
                            </select>

                            <select name="year">
                                <option <?php if(($time['year'])=="2010") { echo "selected='selected'";} ?>>2010</option>
                                <option <?php if(($time['year'])=="2011") { echo "selected='selected'";} ?>>2011</option>
                                <option <?php if(($time['year'])=="2012") { echo "selected='selected'";} ?>>2012</option>
                                <option <?php if(($time['year'])=="2013") { echo "selected='selected'";} ?>>2013</option>
                                <option <?php if(($time['year'])=="2014") { echo "selected='selected'";} ?>>2014</option>
                                <option <?php if(($time['year'])=="2015") { echo "selected='selected'";} ?>>2015</option>
                            </select>

                        </td>
                    </tr>
                </table>
            </form>

        <?
        }else{

            if(isset($_POST['savetime'])) {
                if($action=="update"){

                    $company=$_POST['company'];
                    $month=$_POST['month'];
                    $year=$_POST['year'];
                    $report_id=$_POST['report_id'];

                    $time_SQL_update="UPDATE reporting_report SET company='$company',month='$month',year='$year' WHERE report_id='$report_id'";
                    $bool=mysql_query($time_SQL_update);
                    if($bool==1) echo "<div id='adminok_detail'>The changes have been saved.</div>";
                    if($bool<>1) echo "<div id='adminnok_detail'>An error occurred when trying to save the changes.</div>";

                }}

            $SQL_time="SELECT * FROM reporting_report, reporting_partners WHERE reporting_report.partner_id=reporting_partners.partner_id AND report_id=" . $_GET['report_id'];
            $time_result=mysql_query($SQL_time);
            $time=mysql_fetch_array($time_result);
            ?>

            <table width="100%" border="0" cellpadding="0" cellspacing="0" class="report">
                <tr>
                    <th colspan="6" class="detail">Reporting for
                        <form action="<?php echo $_SERVER['PHP_SELF']; ?>?report_id=<?php echo $person['report_id'] ?>" method="post" style="width:20px;float:right;">
                            <input type="hidden" name="action" value="edit">
                            <input type="submit" name="edittime" class="edit" value="Edit" >
                        </form>
                    </th>
                </tr>
                <tr>
                    <td width="120"><strong>Company</strong></td>
                    <td colspan="5"><?php echo $time['partner_company'] ?></td>
                </tr>
                <tr>
                    <td><strong>Period</strong></td>
                    <td colspan="5"><?php echo $time['month'] ?> <?php echo $time['year'] ?></td>
                </tr>
            </table>
        <?
        }
        ?>

    </td>
    </tr>
    </table>




    <table width="100%" border="0" cellpadding="0" cellspacing="0" class="report">
    <tr>
        <th colspan="7" class="detail">Classroom training</th>
    </tr>
    <tr>
        <td><strong>Title</strong></td>
        <td><strong>Starting date</strong></td>
        <td><strong>Duration</strong></td>
        <td><strong>Participants</strong></td>
        <?php
        if(($report['course1_rev'])!=="0") {
            ?>
            <td><strong>Client revenue (Euro)</strong></td>
        <?php
        }
        ?>
        <td><strong>Type</strong></td>
        <td><strong>CLP ID</strong></td>
    </tr>
    <?php
    if(($report['course1'])!=="- Select -") {
        ?>
        <tr>
            <td><?php echo $report['course1'] ?></td>
            <td><?php echo $report['course1_start'] ?></td>
            <td><?php echo $report['course1_dur'] ?> days</td>
            <td><?php echo $report['course1_part'] ?></td>
            <?php
            if(($report['course1_rev'])!=="0") {
                ?>
                <td>&euro; <?php echo $report['course1_rev'] ?></td>
            <?php
            }
            ?>
            <td><?php echo $report['course1_type'] ?></td>
            <td><?php echo $report['course1_code'] ?></td>
        </tr>
    <?php
    } else {
        ?>
        <tr>
            <td colspan="6">No courses reported</td>
        </tr>
    <?php
    }

    if(($report['course2'])!=="- Select -") {
        ?>
        <tr>
            <td><?php echo $report['course2'] ?></td>
            <td><?php echo $report['course2_start'] ?></td>
            <td><?php echo $report['course2_dur'] ?> days</td>
            <td><?php echo $report['course2_part'] ?></td>
            <?php
            if(($report['course2_rev'])!=="0") {
                ?>
                <td>&euro; <?php echo $report['course2_rev'] ?></td>
            <?php
            }
            ?>
            <td><?php echo $report['course2_type'] ?></td>
            <td><?php echo $report['course2_code'] ?></td>
        </tr>
    <?php
    }

    if(($report['course3'])!=="- Select -") {
        ?>

        <tr>
            <td><?php echo $report['course3'] ?></td>
            <td><?php echo $report['course3_start'] ?></td>
            <td><?php echo $report['course3_dur'] ?> days</td>
            <td><?php echo $report['course3_part'] ?></td>
            <?php
            if(($report['course3_rev'])!=="0") {
                ?>
                <td>&euro; <?php echo $report['course3_rev'] ?></td>
            <?php
            }
            ?>
            <td><?php echo $report['course3_type'] ?></td>
            <td><?php echo $report['course3_code'] ?></td>
        </tr>
    <?php
    }

    if(($report['course4'])!=="- Select -") {
        ?>
        <tr>
            <td><?php echo $report['course4'] ?></td>
            <td><?php echo $report['course4_start'] ?></td>
            <td><?php echo $report['course4_dur'] ?> days</td>
            <td><?php echo $report['course4_part'] ?></td>
            <?php
            if(($report['course4_rev'])!=="0") {
                ?>
                <td>&euro; <?php echo $report['course4_rev'] ?></td>
            <?php
            }
            ?>
            <td><?php echo $report['course4_type'] ?></td>
            <td><?php echo $report['course4_code'] ?></td>
        </tr>
    <?php
    }

    if(($report['course5'])!=="- Select -") {
        ?>
        <tr>
            <td><?php echo $report['course5'] ?></td>
            <td><?php echo $report['course5_start'] ?></td>
            <td><?php echo $report['course5_dur'] ?> days</td>
            <td><?php echo $report['course5_part'] ?></td>
            <?php
            if(($report['course5_rev'])!=="0") {
                ?>
                <td>&euro; <?php echo $report['course5_rev'] ?></td>
            <?php
            }
            ?>
            <td><?php echo $report['course5_type'] ?></td>
            <td><?php echo $report['course5_code'] ?></td>
        </tr>
    <?php
    }

    if(($report['course6'])!=="- Select -") {
        ?>
        <tr>
            <td><?php echo $report['course6'] ?></td>
            <td><?php echo $report['course6_start'] ?></td>
            <td><?php echo $report['course6_dur'] ?> days</td>
            <td><?php echo $report['course6_part'] ?></td>
            <?php
            if(($report['course6_rev'])!=="0") {
                ?>
                <td>&euro; <?php echo $report['course6_rev'] ?></td>
            <?php
            }
            ?>
            <td><?php echo $report['course6_type'] ?></td>
            <td><?php echo $report['course6_code'] ?></td>
        </tr>
    <?php
    }

    if(($report['course7'])!=="- Select -") {
        ?>
        <tr>
            <td><?php echo $report['course7'] ?></td>
            <td><?php echo $report['course7_start'] ?></td>
            <td><?php echo $report['course7_dur'] ?> days</td>
            <td><?php echo $report['course7_part'] ?></td>
            <?php
            if(($report['course7_rev'])!=="0") {
                ?>
                <td>&euro; <?php echo $report['course7_rev'] ?></td>
            <?php
            }
            ?>
            <td><?php echo $report['course7_type'] ?></td>
            <td><?php echo $report['course7_code'] ?></td>
        </tr>
    <?php
    }

    if(($report['course8'])!=="- Select -") {
        ?>
        <tr>
            <td><?php echo $report['course8'] ?></td>
            <td><?php echo $report['course8_start'] ?></td>
            <td><?php echo $report['course8_dur'] ?> days</td>
            <td><?php echo $report['course8_part'] ?></td>
            <?php
            if(($report['course8_rev'])!=="0") {
                ?>
                <td>&euro; <?php echo $report['course8_rev'] ?></td>
            <?php
            }
            ?>
            <td><?php echo $report['course8_type'] ?></td>
            <td><?php echo $report['course8_code'] ?></td>
        </tr>
    <?php
    }

    if(($report['course9'])!=="- Select -") {
        ?>
        <tr>
            <td><?php echo $report['course9'] ?></td>
            <td><?php echo $report['course9_start'] ?></td>
            <td><?php echo $report['course9_dur'] ?> days</td>
            <td><?php echo $report['course9_part'] ?></td>
            <?php
            if(($report['course9_rev'])!=="0") {
                ?>
                <td>&euro; <?php echo $report['course9_rev'] ?></td>
            <?php
            }
            ?>
            <td><?php echo $report['course9_type'] ?></td>
            <td><?php echo $report['course9_code'] ?></td>
        </tr>
    <?php
    }

    if(($report['course10'])!=="- Select -") {
        ?>
        <tr>
            <td><?php echo $report['course10'] ?></td>
            <td><?php echo $report['course10_start'] ?></td>
            <td><?php echo $report['course10_dur'] ?> days</td>
            <td><?php echo $report['course10_part'] ?></td>
            <?php
            if(($report['course10_rev'])!=="0") {
                ?>
                <td>&euro; <?php echo $report['course10_rev'] ?></td>
            <?php
            }
            ?>
            <td><?php echo $report['course10_type'] ?></td>
            <td><?php echo $report['course10_code'] ?></td>
        </tr>
    <?php
    }

    if((($report['course11'])!=="- Select -") && (!empty($report['course11']))) {
        ?>
        <tr>
            <td><?php echo $report['course11'] ?></td>
            <td><?php echo $report['course11_start'] ?></td>
            <td><?php echo $report['course11_dur'] ?> days</td>
            <td><?php echo $report['course11_part'] ?></td>
            <?php
            if(($report['course11_rev'])!=="0") {
                ?>
                <td>&euro; <?php echo $report['course11_rev'] ?></td>
            <?php
            }
            ?>
            <td><?php echo $report['course11_type'] ?></td>
            <td><?php echo $report['course11_code'] ?></td>
        </tr>
    <?php
    }

    if((($report['course12'])!=="- Select -") && (!empty($report['course12']))) {
        ?>
        <tr>
            <td><?php echo $report['course12'] ?></td>
            <td><?php echo $report['course12_start'] ?></td>
            <td><?php echo $report['course12_dur'] ?> days</td>
            <td><?php echo $report['course12_part'] ?></td>
            <?php
            if(($report['course12_rev'])!=="0") {
                ?>
                <td>&euro; <?php echo $report['course12_rev'] ?></td>
            <?php
            }
            ?>
            <td><?php echo $report['course12_type'] ?></td>
            <td><?php echo $report['course12_code'] ?></td>
        </tr>
    <?php
    }

    if((($report['course13'])!=="- Select -") && (!empty($report['course13']))) {
        ?>
        <tr>
            <td><?php echo $report['course13'] ?></td>
            <td><?php echo $report['course13_start'] ?></td>
            <td><?php echo $report['course13_dur'] ?> days</td>
            <td><?php echo $report['course13_part'] ?></td>
            <?php
            if(($report['course13_rev'])!=="0") {
                ?>
                <td>&euro; <?php echo $report['course13_rev'] ?></td>
            <?php
            }
            ?>
            <td><?php echo $report['course13_type'] ?></td>
            <td><?php echo $report['course13_code'] ?></td>
        </tr>
    <?php
    }

    if((($report['course14'])!=="- Select -") && (!empty($report['course14']))) {
        ?>
        <tr>
            <td><?php echo $report['course14'] ?></td>
            <td><?php echo $report['course14_start'] ?></td>
            <td><?php echo $report['course14_dur'] ?> days</td>
            <td><?php echo $report['course14_part'] ?></td>
            <?php
            if(($report['course14_rev'])!=="0") {
                ?>
                <td>&euro; <?php echo $report['course14_rev'] ?></td>
            <?php
            }
            ?>
            <td><?php echo $report['course14_type'] ?></td>
            <td><?php echo $report['course14_code'] ?></td>
        </tr>
    <?php
    }

    if((($report['course15'])!=="- Select -") && (!empty($report['course15']))) {
        ?>
        <tr>
            <td><?php echo $report['course15'] ?></td>
            <td><?php echo $report['course15_start'] ?></td>
            <td><?php echo $report['course15_dur'] ?> days</td>
            <td><?php echo $report['course15_part'] ?></td>
            <?php
            if(($report['course15_rev'])!=="0") {
                ?>
                <td>&euro; <?php echo $report['course15_rev'] ?></td>
            <?php
            }
            ?>
            <td><?php echo $report['course15_type'] ?></td>
            <td><?php echo $report['course15_code'] ?></td>
        </tr>
    <?php
    }
    ?>
    </table>

    <table width="100%"  border="0" cellpadding="0" cellspacing="0">
        <tr>
            <td width="50%" style="padding-right:20px;" valign="top">

                <table width="100%" border="0" cellpadding="0" cellspacing="0" class="report">
                    <tr>
                        <th colspan="2" class="detail">Consultancy and writing services</th>
                    </tr>
                    <tr>
                        <td width="120"><strong>Consultancy</strong></td>
                        <td>&euro; <?php echo $report['consult'] ?></td>
                    </tr>
                    <tr>
                        <td><strong>Writing services</strong></td>
                        <td>&euro; <?php echo $report['writing'] ?></td>
                    </tr>
                </table>

            </td>
            <td width="50%" style="padding-left:20px;" valign="top">

                <table width="100%" border="0" cellpadding="0" cellspacing="0" class="report">
                    <tr>
                        <th colspan="2" class="detail">Monthly commitment fee</th>
                    </tr>
                    <tr>
                        <td width="150"><strong>Training (quarterly)</strong></td>
                        <td>&euro; <?php echo $report['cftraining'] ?></td>
                    </tr>
                    <tr>
                        <td><strong>Services (monthly)</strong></td>
                        <td>&euro; <?php echo $report['cfservices'] ?></td>
                    </tr>
                </table>

            </td>
        </tr>
    </table>

    <table width="100%"  border="0" cellpadding="0" cellspacing="0">
        <tr>
            <td width="50%" style="padding-right:20px;" valign="top">

                <table width="100%" border="0" cellpadding="0" cellspacing="0" class="report">
                    <tr>
                        <th colspan="2" class="detail">Software revenue</th>
                    </tr>
                    <tr>
                        <td width="120"><strong>Publimap revenue</strong></td>
                        <td>&euro; <?php echo $report['publimap_rev'] ?></td>
                    </tr>
                    <tr>
                        <td width="120"><strong>iMapper revenue</strong></td>
                        <td>&euro; <?php echo $report['imapper_rev'] ?></td>
                    </tr>
                </table>

            </td>
            <td width="50%" style="padding-left:20px;" valign="top">

                <table width="100%" border="0" cellpadding="0" cellspacing="0" class="report">
                    <tr>
                        <th colspan="2" class="detail">Additional comments</th>
                    </tr>
                    <tr>
                        <td>
                            <?php echo $report['comments'] ?>
                        </td>
                    </tr>
                </table>

            </td>
        </tr>
    </table>
<?php
}
?>


</div>
</div>
<!-- /block -->


</div>
</div>
</div>
</div>
<hr>
<?php
include("../footer.php");
?>
</html>