<?php
$rootdir="../../";
$page="Reports";
include("../header.php");
include("../connect.php");

if(isset($_GET['action'])) $action=$_GET['action'];
if(isset($_POST['action'])) $action=$_POST['action'];

?>

<body>
<?php
include("../topnav.php");
?>
<div class="container-fluid">
            <div class="row-fluid">
                
                <?php
				//include("../sidenav.php");
				?>
                
                <div class="span10" id="content">
                    <div class="row-fluid">
                    	<div class="span12">
                        
                            <!-- block -->
                            <div class="block">
                                <div class="navbar navbar-inner block-header">
                                <h3>List of reports</h3>     
                                </div>
                                <div class="block-content collapse in">
  <?php 
if (!isset($_SESSION["username"])){
	echo $niet_ingelogd;
}
// hieronder staan de functies die toegelaten worden tot deze pagina
elseif($_SESSION["function"] != "admin"){
	echo $geen_toegang;
}else{
	
$current_year = date("Y");
	
$company = isset($_POST['choice_partner']) ? $_POST['choice_partner'] : '';
$month = isset($_POST['choice_month']) ? trim($_POST['choice_month']) : '';
$year = isset($_POST['choice_year']) ? $_POST['choice_year'] : $current_year; // if year not filled in, select current year as default
$course = isset($_POST['choice_course']) ? trim($_POST['choice_course']) : '';
$clp = isset($_POST['choice_clp']) ? trim($_POST['choice_clp']) : '';
$verzenden = isset($_POST['verzenden']) ? trim($_POST['verzenden']) : '';
?>

  <p>Please choose a partner and specify a year.</p>
  <form action="#" method="post">
    <select name="choice_partner">
      <option value="">- Please select a partner-</option>
      <?php
		$partner_SQL="SELECT * FROM reporting_partners WHERE active='yes' ORDER BY partner_company ASC";
		$partner_result=mysql_query($partner_SQL);
		while($partner=mysql_fetch_array($partner_result)){
			
		
		?>
      <option value="<?php echo $partner['partner_id'];  ?>" <?php if($company==$partner['partner_id']) { echo "selected='selected'";} ?>"><?php echo $partner['partner_company']; ?></option>
      <?php
		}
		?>
    </select>
    <select name="choice_month">
      <option value="">- Please select a month-</option>
      <?php
		$partner_SQL="SELECT * FROM reporting_months ORDER BY month_id ASC";
		$partner_result=mysql_query($partner_SQL);
		while($partner=mysql_fetch_array($partner_result)){
		
		?>
      <option value="<?php echo $partner['month'];  ?>" <?php if($month==$partner['month']) { echo "selected='selected'";} ?>"><?php echo $partner['month']; ?></option>
      <?php
		}
		?>
    </select>
    <select name="choice_year">
      <?php
        // lees alle jaren uit die in de databasetabel zitten
		$select_SQL="SELECT * FROM reporting_year ORDER BY year_id ASC";
		$select_result=mysql_query($select_SQL);
		while($partner=mysql_fetch_array($select_result)){

		?>
      <option value="<?php echo $partner['year'] ?>" <?php if(($year)==$partner['year']) { echo "selected='selected'";}  ?>> <?php echo $partner['year'] ?> </option>
      <?php
		}
		?>
    </select>
    *
    <select name="choice_course">
      <option value="">- Select course -</option>
      <?php
		$course_SQL="SELECT * FROM reporting_courses ORDER BY course_name ASC";
		$course_result=mysql_query($course_SQL);
		while($course_row=mysql_fetch_array($course_result)){
			
		?>
      <option value="<?php echo $course_row['course_id'] ?>" <?php if(($course)==$course_row['course_id']) { echo "selected='selected'";} ?>> <?php echo $course_row['course_name'] ?> </option>
      <?php
		}
		?>
    </select>
    <select name="choice_clp">
      <option value="">- Select Corporate or Public</option>
      <option value="corporate" <?php if(($clp)=="corporate") { echo "selected='selected'";} ?>>Corporate</option>
      <option value="public" <?php if(($clp)=="public") { echo "selected='selected'";} ?>>Public</option>
    </select>
    <input type="submit" value="Show report" name="verzenden" />
  </form>
  <?php

$where = array();
if(!empty($company)) $where[] = " partner_id = '" . $company . "'";
if(!empty($month)) $where[] = " month = '" . $month . "'";
if(!empty($year)) $where[] = " year = '" . $year . "'";
if(!empty($course)) $where[] = " course_id = '" . $course . "'";
if($clp == "public") $where[] = " clp_number = '0'";
if($clp == "corporate") $where[] = " clp_number != '0'";

$where = implode(' AND ', $where);

$totaal_part=0;

if(empty($where))
{
	
}
else
{
	$query = "SELECT * FROM reporting_participants WHERE $where";
}


if(!empty($company))
{	
$result_partner = mysql_query("SELECT * FROM reporting_partners WHERE partner_id ='$company'");
$row = mysql_fetch_assoc($result_partner);
 echo "<p><strong>Your selected partner: " . $row["partner_company"] . "</strong></p>"; 
}

	?>
  <table width="100%" border="0" cellspacing="0" cellpadding="0" class="table table-striped" id="table">
    <tr>
      <th>Month</th>
      <th># Attendees</th>
      <th>Details</th>
    </tr>
    <?php if($verzenden){
	$result = mysql_query($query);
	
			// wat doen als je pas op de pagina komt of als er geen resultaten zijn
			$aantal = mysql_num_rows($result); 
			if ($aantal == 0) { echo "<tr><td colspan=\"4\">No results found. Please choose a partner and correct year in the menu.</td></tr>"; }
			// loop alle resultaten af in de participant tbl
			while($report=mysql_fetch_array($result)){ 
			echo "<tr>";	
		
			echo "<td>";

			// cursusnaam weergeven + maanden uitlezen	
			
			$result_course = mysql_query("SELECT * FROM reporting_courses WHERE course_id='" . $report['course_id'] . "'"); 
						while($course_qry=mysql_fetch_array($result_course))
						{
							$course_name = $course_qry['course_name'];
						}
			
 			echo  $course_name . " (" . $report['month'] . " " . $year . ")"; 
			echo "</td>";
		
			// aantal deelnemers uitlezen
			echo "<td>";
			echo $report['participant_amount'] . "<br />";	
			echo "</td>";
			// aantal deelnemers optellen per cursus voor gekozen partner en jaar
			$totaal_part+=$report['participant_amount'];
	
?>
    
      <td width="50" align="center"><a href="report_details.php?report_id=<?php echo $report['report_id'] ?>"><img src="../../images/view.png" alt="Details" /></a></td>
      <!--<td width="50" align="center"><a href=report_delete.php?report_id=<?php //echo $report['report_id'] ?>&amp;action=delete onclick="return confirm('Are you sure you want to delete this report?')"><img src="../../images/delete.jpg" alt="Delete" /></a></td>--> 
    </tr>
    <?php

	 
	}
}
	?>
  </table>
  <p><?php echo "<strong>TOTAL: " . $totaal_part . " attendees for $year </strong>"; ?></p>
  <?php

 //echo "<a href=\"download_csv.php?month=" . $_GET['month'] . "&year=" . $_GET['year'] . "&company=" . $_GET['company'] . "&course=" . $_GET['course'] . "\">Download csv</a>";

 ?>
  <!--<p><a href="delta.php">View delta between Foundation &amp; Master Course</a></p>-->
  <?php
}
?>

                                </div>
                            </div>
                            <!-- /block -->


</div>
</div>
</div>
</div>
            <hr>
<?php
include("../footer.php");
?>
</html>