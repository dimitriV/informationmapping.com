<?php
$rootdir="../../";
$page="Reports";
include("../header.php");
include("../connect.php");

$SQL_report="SELECT * FROM reporting_report WHERE report_id=" . $_GET['report_id'];
$report_result=mysql_query($SQL_report);
$report=mysql_fetch_array($report_result);
?>

<div id="admin">
<?php 
if (!isset($_SESSION["username"])){
	echo $niet_ingelogd;
}
// hieronder staan de functies die toegelaten worden tot deze pagina
elseif($_SESSION["function"] != "admin"){
	echo $geen_toegang;
}else{
?>
<h3>Report details<a href="javascript:window.print()" style="float:right;"><img src="../../images/print.png" width="32" height="32" alt="Print this report" /></a></h3>
<form action="course_list.php" method="post">
 <table width="500" border="0" cellpadding="0" cellspacing="0" class="report">
  <tr>
  <th colspan="6">Report submitted by</th>
  </tr>
  <tr class="odd">
    <td width="120"><strong>First name</strong></td>
    <td colspan="5"><input name="course_name" type="text" value="<?php echo $report['first'] ?>" size="40" /></td>
  </tr>
  <tr>
    <td><strong>Last name</strong></td>
    <td colspan="5"><input name="course_name" type="text" value="<?php echo $report['last'] ?>" size="40" /></td>
  </tr>
  <tr class="odd">
    <td><strong>E-mail</strong></td>
    <td colspan="5"><input name="course_name" type="text" value="<?php echo $report['email'] ?>" size="40" /></td>
  </tr>
  </table>

  <table width="500" border="0" cellpadding="0" cellspacing="0" class="report">
    <tr>
    <th colspan="6">Reporting for</th>
    </tr>
    <tr class="odd">
    <td width="120"><strong>Company</strong></td>
    <td colspan="5"><input name="course_name" type="text" value="<?php echo $report['company'] ?>" size="40" /></td>
  </tr>
  <tr>
    <td><strong>Period</strong></td>
    <td colspan="5"><?php echo $report['month'] ?> <?php echo $report['year'] ?></td>
  </tr>
  </table>

  <table width="960" border="0" cellpadding="0" cellspacing="0" class="report">
  <tr>
  <th colspan="7">Classroom training</th>
  </tr>
  <tr class="odd">
    <td><strong>Title</strong></td>
    <td><strong>Starting date</strong></td>
    <td><strong>Duration</strong></td>
    <td><strong>Participants</strong></td>
    <?php
    if(($report['course1_rev'])!=="0") {
	?>
    <td><strong>Client revenue (Euro)</strong></td>
    <?php
	}
	?>
    <td><strong>Type</strong></td>
    <td><strong>CLP ID</strong></td>
  </tr>
  <?php
    if(($report['course1'])!=="- Select -") {
	?>
  <tr>
    <td><select name="course1">
    	<option>- Select -</option>
        <?php
		$course_SQL="SELECT * FROM reporting_courses ORDER BY course_name ASC";
		$course_result=mysql_query($course_SQL);
		while($course=mysql_fetch_array($course_result)){
			?>
		<option <?php if(($_POST['course1'])==$course['course_name']) { echo "selected='selected'";} ?>><?php echo $course['course_name'] ?></option>
		<?
        }
		?>
        </select>
		</td>
    <td><?php echo $report['course1_start'] ?></td>
    <td><?php echo $report['course1_dur'] ?> days</td>
    <td><?php echo $report['course1_part'] ?></td>
    <?php
    if(($report['course1_rev'])!=="0") {
	?>
    <td>&euro; <?php echo $report['course1_rev'] ?></td>
    <?php
	}
	?>
    <td><?php echo $report['course1_type'] ?></td>
    <td><?php echo $report['course1_code'] ?></td>
  </tr>
  <?php
	} else {
	?>
    <tr>
    <td>No courses reported</td>
    </tr>
    <?php
	}

    if(($report['course2'])!=="- Select -") {
	?>
    <tr>
    <td><?php echo $report['course2'] ?></td>
    <td><?php echo $report['course2_start'] ?></td>
    <td><?php echo $report['course2_dur'] ?> days</td>
    <td><?php echo $report['course2_part'] ?></td>
    <?php
    if(($report['course2_rev'])!=="0") {
	?>
    <td>&euro; <?php echo $report['course2_rev'] ?></td>
    <?php
	}
	?>
    <td><?php echo $report['course2_type'] ?></td>
    <td><?php echo $report['course2_code'] ?></td>
  </tr>
  <?php
	}

    if(($report['course3'])!=="- Select -") {
	?>
  
    <tr>
    <td><?php echo $report['course3'] ?></td>
    <td><?php echo $report['course3_start'] ?></td>
    <td><?php echo $report['course3_dur'] ?> days</td>
    <td><?php echo $report['course3_part'] ?></td>
    <?php
    if(($report['course3_rev'])!=="0") {
	?>
    <td>&euro; <?php echo $report['course3_rev'] ?></td>
    <?php
	}
	?>
    <td><?php echo $report['course3_type'] ?></td>
    <td><?php echo $report['course3_code'] ?></td>
  </tr>
   <?php
	}

    if(($report['course4'])!=="- Select -") {
	?>
    <tr>
    <td><?php echo $report['course4'] ?></td>
    <td><?php echo $report['course4_start'] ?></td>
    <td><?php echo $report['course4_dur'] ?> days</td>
    <td><?php echo $report['course4_part'] ?></td>
    <?php
    if(($report['course4_rev'])!=="0") {
	?>
    <td>&euro; <?php echo $report['course4_rev'] ?></td>
    <?php
	}
	?>
    <td><?php echo $report['course4_type'] ?></td>
    <td><?php echo $report['course4_code'] ?></td>
  </tr>
   <?php
	}

    if(($report['course5'])!=="- Select -") {
	?>
    <tr>
    <td><?php echo $report['course5'] ?></td>
    <td><?php echo $report['course5_start'] ?></td>
    <td><?php echo $report['course5_dur'] ?> days</td>
    <td><?php echo $report['course5_part'] ?></td>
    <?php
    if(($report['course5_rev'])!=="0") {
	?>
    <td>&euro; <?php echo $report['course5_rev'] ?></td>
    <?php
	}
	?>
    <td><?php echo $report['course5_type'] ?></td>
    <td><?php echo $report['course5_code'] ?></td>
  </tr>
   <?php
	}

    if(($report['course6'])!=="- Select -") {
	?>
    <tr>
    <td><?php echo $report['course6'] ?></td>
    <td><?php echo $report['course6_start'] ?></td>
    <td><?php echo $report['course6_dur'] ?> days</td>
    <td><?php echo $report['course6_part'] ?></td>
    <?php
    if(($report['course6_rev'])!=="0") {
	?>
    <td>&euro; <?php echo $report['course6_rev'] ?></td>
    <?php
	}
	?>
    <td><?php echo $report['course6_type'] ?></td>
    <td><?php echo $report['course6_code'] ?></td>
  </tr>
   <?php
	}

    if(($report['course7'])!=="- Select -") {
	?>
    <tr>
    <td><?php echo $report['course7'] ?></td>
    <td><?php echo $report['course7_start'] ?></td>
    <td><?php echo $report['course7_dur'] ?> days</td>
    <td><?php echo $report['course7_part'] ?></td>
    <?php
    if(($report['course7_rev'])!=="0") {
	?>
    <td>&euro; <?php echo $report['course7_rev'] ?></td>
    <?php
	}
	?>
    <td><?php echo $report['course7_type'] ?></td>
    <td><?php echo $report['course7_code'] ?></td>
  </tr>
   <?php
	}

    if(($report['course8'])!=="- Select -") {
	?>
    <tr>
    <td><?php echo $report['course8'] ?></td>
    <td><?php echo $report['course8_start'] ?></td>
    <td><?php echo $report['course8_dur'] ?> days</td>
    <td><?php echo $report['course8_part'] ?></td>
    <?php
    if(($report['course8_rev'])!=="0") {
	?>
    <td>&euro; <?php echo $report['course8_rev'] ?></td>
    <?php
	}
	?>
    <td><?php echo $report['course8_type'] ?></td>
    <td><?php echo $report['course8_code'] ?></td>
  </tr>
   <?php
	}

    if(($report['course9'])!=="- Select -") {
	?>
    <tr>
    <td><?php echo $report['course9'] ?></td>
    <td><?php echo $report['course9_start'] ?></td>
    <td><?php echo $report['course9_dur'] ?> days</td>
    <td><?php echo $report['course9_part'] ?></td>
    <?php
    if(($report['course9_rev'])!=="0") {
	?>
    <td>&euro; <?php echo $report['course9_rev'] ?></td>
    <?php
	}
	?>
    <td><?php echo $report['course9_type'] ?></td>
    <td><?php echo $report['course9_code'] ?></td>
  </tr>
   <?php
	}

    if(($report['course10'])!=="- Select -") {
	?>
    <tr>
    <td><?php echo $report['course10'] ?></td>
    <td><?php echo $report['course10_start'] ?></td>
    <td><?php echo $report['course10_dur'] ?> days</td>
    <td><?php echo $report['course10_part'] ?></td>
    <?php
    if(($report['course10_rev'])!=="0") {
	?>
    <td>&euro; <?php echo $report['course10_rev'] ?></td>
    <?php
	}
	?>
    <td><?php echo $report['course10_type'] ?></td>
    <td><?php echo $report['course10_code'] ?></td>
  </tr>
   <?php
	}
	
	  if(($report['course11'])!=="- Select -") {
	?>
    <tr>
    <td><?php echo $report['course11'] ?></td>
    <td><?php echo $report['course11_start'] ?></td>
    <td><?php echo $report['course11_dur'] ?> days</td>
    <td><?php echo $report['course11_part'] ?></td>
    <?php
    if(($report['course11_rev'])!=="0") {
	?>
    <td>&euro; <?php echo $report['course11_rev'] ?></td>
    <?php
	}
	?>
    <td><?php echo $report['course11_type'] ?></td>
    <td><?php echo $report['course11_code'] ?></td>
  </tr>
   <?php
	}
	
	  if(($report['course12'])!=="- Select -") {
	?>
    <tr>
    <td><?php echo $report['course12'] ?></td>
    <td><?php echo $report['course12_start'] ?></td>
    <td><?php echo $report['course12_dur'] ?> days</td>
    <td><?php echo $report['course12_part'] ?></td>
    <?php
    if(($report['course12_rev'])!=="0") {
	?>
    <td>&euro; <?php echo $report['course12_rev'] ?></td>
    <?php
	}
	?>
    <td><?php echo $report['course12_type'] ?></td>
    <td><?php echo $report['course12_code'] ?></td>
  </tr>
   <?php
	}
	
	  if(($report['course13'])!=="- Select -") {
	?>
    <tr>
    <td><?php echo $report['course13'] ?></td>
    <td><?php echo $report['course13_start'] ?></td>
    <td><?php echo $report['course13_dur'] ?> days</td>
    <td><?php echo $report['course13_part'] ?></td>
    <?php
    if(($report['course13_rev'])!=="0") {
	?>
    <td>&euro; <?php echo $report['course13_rev'] ?></td>
    <?php
	}
	?>
    <td><?php echo $report['course13_type'] ?></td>
    <td><?php echo $report['course13_code'] ?></td>
  </tr>
   <?php
	}
	
	  if(($report['course14'])!=="- Select -") {
	?>
    <tr>
    <td><?php echo $report['course14'] ?></td>
    <td><?php echo $report['course14_start'] ?></td>
    <td><?php echo $report['course14_dur'] ?> days</td>
    <td><?php echo $report['course14_part'] ?></td>
    <?php
    if(($report['course14_rev'])!=="0") {
	?>
    <td>&euro; <?php echo $report['course14_rev'] ?></td>
    <?php
	}
	?>
    <td><?php echo $report['course14_type'] ?></td>
    <td><?php echo $report['course14_code'] ?></td>
  </tr>
   <?php
	}
	
	  if(($report['course15'])!=="- Select -") {
	?>
    <tr>
    <td><?php echo $report['course15'] ?></td>
    <td><?php echo $report['course15_start'] ?></td>
    <td><?php echo $report['course15_dur'] ?> days</td>
    <td><?php echo $report['course15_part'] ?></td>
    <?php
    if(($report['course15_rev'])!=="0") {
	?>
    <td>&euro; <?php echo $report['course15_rev'] ?></td>
    <?php
	}
	?>
    <td><?php echo $report['course15_type'] ?></td>
    <td><?php echo $report['course15_code'] ?></td>
  </tr>
   <?php
	}
	?>
  </table>
  
  <table width="500" border="0" cellpadding="0" cellspacing="0" class="report">
  <tr>
  <th colspan="2">Consultancy and writing services (Euro)</th>
  </tr>
    <tr>
    <td width="120"><strong>Consultancy</strong></td>
    <td>&euro; <?php echo $report['consult'] ?></td>
  </tr>
  <tr class="odd">
    <td><strong>Writing services</strong></td>
    <td>&euro; <?php echo $report['writing'] ?></td>
  </tr>
  </table>

 <table width="500" border="0" cellpadding="0" cellspacing="0" class="report">
  <tr>
  <th colspan="2">Monthly commitment fee (Euro)</th>
  </tr>
    <tr>
    <td width="120"><strong>Training</strong></td>
    <td>&euro; <?php echo $report['cftraining'] ?></td>
  </tr>
  <tr class="odd">
    <td><strong>Services</strong></td>
    <td>&euro; <?php echo $report['cfservices'] ?></td>
  </tr>
  </table>
   
    
  <table width="500" border="0" cellpadding="0" cellspacing="0" class="report">
  <tr>
  <th colspan="2">Additional comments</th>
  </tr>
    <tr>
    <td>
    <?php echo $report['comments'] ?>
    </td>
  </tr>
  </table>
  <input type="hidden" name="report_id" value="<?php echo $report['report_id'] ?>"> 
<input type="hidden" name="action" value="update">
<input type="submit" name="Submit" class="submit" value="Save">
</form>
<?php
}
?>
</div>
</body>
</html>