 <div class="navbar navbar-fixed-top">
            <div class="navbar-inner">
                <div class="container-fluid">
                    <a class="btn btn-navbar" data-toggle="collapse" data-target=".nav-collapse"> <span class="icon-bar"></span>
                     <span class="icon-bar"></span>
                     <span class="icon-bar"></span>
                    </a>
                    <a class="brand" href="crm/includes/index.php"><!--<img src="<?php echo $rootdir; ?>images/logo.png" alt="Information Mapping logo" />--></a>
                    <div class="nav-collapse collapse">
                        <ul class="nav pull-right">
                            <li class="dropdown">
                                <a href="../logout.php"> <i class="icon-user"></i> <?php  echo (isset($_SESSION["username"])) ? $_SESSION["username"] : ''; ?> - Logout</a>    
                            </li>
                        </ul>
                        <ul class="nav">
<li><a href="../../admin/crm/overview.php" <? if ($page == "Home") { echo "class='currentnav'";} ?>>Overview</a></li>
<li><a href="../../admin/partners/partner_list.php" <? if ($page == "Partners") { echo "class='currentnav'";} ?>>Partners</a></li>
<li><a href="../../admin/courses/course_list.php" <? if ($page == "Courses") { echo "class='currentnav'";} ?>>Courses</a></li>
<li><a href="../../admin/reports/index.php" <? if ($page == "Reports") { echo "class='currentnav'";} ?>>Reporting</a></li>
<li><a href="../../admin/clp/clp_list1.php" <? if ($page == "CLP IDs") { echo "class='currentnav'";} ?>>CLP IDs</a></li>
<li><a href="../../admin/imcc/resume_list.php">IMCC</a></li>
<li><a href="../../admin/crm/index.php" <? if ($page == "CRM") { echo "class='currentnav'";} ?>>CRM</a></li>    
                        </ul>
                    </div>
                    <!--/.nav-collapse -->
                </div>
            </div>
        </div> <!-- end navbar -->