<?php
$rootdir="../../";
$page="Partners";
include("../header.php");
include("../connect.php");
?>

<body>
<?php
include("../topnav.php");
?>
<div class="container-fluid">
    <div class="row-fluid">

        <?php
        //include("../sidenav.php");
        ?>

        <div class="span10" id="content">
            <div class="row-fluid">
                <div class="span12">

                    <!-- block -->
                    <div class="block">
                        <div class="navbar navbar-inner block-header">
                            <h3>Add a Partner</h3>
                        </div>
                        <div class="block-content collapse in">
                            <?php
                            if (!isset($_SESSION["username"])){
                                echo $niet_ingelogd;
                            }
// hieronder staan de functies die toegelaten worden tot deze pagina
                            elseif($_SESSION["function"] != "admin"){
                                echo $geen_toegang;
                            }else{
                                ?>

                                <form action="partner_list.php?action=insert" method="post">
                                    <table cellpadding="3" cellspacing="0">
                                        <tr>
                                            <th>First name</th>
                                            <td><input name="partner_first" type="text" size="40" /></td>
                                        </tr>
                                        <tr>
                                            <th>Last name</th>
                                            <td><input name="partner_last" type="text" size="40" /></td>
                                        </tr>
                                        <tr>
                                            <th>E-mail</th>
                                            <td><input name="partner_email" type="text" size="40" /></td>
                                        </tr>
                                        <tr>
                                            <th>Company</th>
                                            <td><input name="partner_company" type="text" size="40" /></td>
                                        </tr>
                                        <tr>
                                            <th>User name</th>
                                            <td><input name="partner_user" type="text" size="40" /></td>
                                        </tr>
                                        <tr>
                                            <th>Password</th>
                                            <td><input name="partner_pw" type="text" size="40" /></td>
                                        </tr>
                                        <tr>
                                            <th>Commitment Fee Training</th>
                                            <td>&euro; <input name="cftraining" type="text" size="10" /> (quarterly)</td>
                                        </tr>
                                        <tr>
                                            <th>Commitment Fee Services</th>
                                            <td>&euro; <input name="cfservices" type="text" size="10" /> (monthly)</td>
                                        </tr>
                                        <tr>
                                            <th>Sees Revenue field</th>
                                            <td><select name="rev_vis">
                                                    <option value="yes">yes</option>
                                                    <option value="no">no</option>
                                                </select></td>
                                        </tr>
                                        <tr>
                                            <th>Sees Commitment Fee block</th>
                                            <td><select name="cf_vis">
                                                    <option value="yes">yes</option>
                                                    <option value="no">no</option>
                                                </select></td>
                                        </tr>
                                        <tr>
                                            <th>Sees Services block</th>
                                            <td><select name="serv_vis">
                                                    <option value="yes">yes</option>
                                                    <option value="no">no</option>
                                                </select></td>
                                        </tr>
                                        <tr>
                                            <th>Sees iMapper block</th>
                                            <td><select name="imapper_vis">
                                                    <option value="yes">yes</option>
                                                    <option value="no">no</option>
                                                </select></td>
                                        </tr>
                                        <tr>
                                            <th>Sees Publimap block</th>
                                            <td><select name="publimap_vis">
                                                    <option value="yes">yes</option>
                                                    <option value="no">no</option>
                                                </select></td>
                                        </tr>
                                        <tr>
                                            <th>Is active</th>
                                            <td><select name="active">
                                                    <option value="yes">yes</option>
                                                    <option value="no">no</option>
                                                </select></td>
                                        </tr>
                                        <tr>
                                            <th colspan="2"><input type="hidden" name="action" value="insert">
                                            <input type="submit" name="Submit" value="Add" style="float:right;"></th>
                                        </tr>
                                    </table>
                                </form>
                            <?php
                            }
                            ?>
                        </div>
                    </div>
                    <!-- /block -->


                </div>
            </div>
        </div>
    </div>
    <hr>
    <?php
    include("../footer.php");
    ?>
</div>
</body>
</html>
