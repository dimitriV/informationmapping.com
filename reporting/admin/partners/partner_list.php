<?php
$rootdir="../../";
$page="Partners";
include("../header.php");
include("../connect.php");

$action = isset($_POST['action']) ? trim($_POST['action']) : '';
$action = isset($_GET['action']) ? trim($_GET['action']) : '';

if($action=="delete"){
  $partner_id=$_GET['partner_id'];
  	
  $partner_SQL_del="DELETE FROM reporting_partners WHERE partner_id=$partner_id";
  $bool=mysql_query($partner_SQL_del);
  if($bool==1) echo "<div id='adminok'>The Partner has been deleted.</div>";
  if($bool<>1) echo "<div id='adminnok'>An error occurred when trying to delete the Partner.</div>";
}

if($action=="insert"){

  $partner_first=$_POST['partner_first'];	
  $partner_last=$_POST['partner_last'];
  $partner_email=$_POST['partner_email'];
  $partner_company=$_POST['partner_company'];
  $partner_user=$_POST['partner_user'];
  $partner_pw=$_POST['partner_pw'];
  $rev_vis=$_POST['rev_vis'];
  $cf_vis=$_POST['cf_vis'];
  $serv_vis=$_POST['serv_vis'];
  $cfservices=$_POST['cfservices'];
  $cftraining=$_POST['cftraining'];
  $imapper_vis=$_POST['imapper_vis'];
  $publimap_vis=$_POST['publimap_vis'];
  $active=$_POST['active'];

  $partner_SQL_insert="INSERT INTO reporting_partners (partner_first,partner_last,partner_email,partner_company,partner_user,partner_pw,rev_vis,cf_vis,serv_vis,cfservices,cftraining,imapper_vis,publimap_vis,active) VALUES ('$partner_first','$partner_last','$partner_email','$partner_company','$partner_user','$partner_pw','$rev_vis','$cf_vis','$serv_vis','$cfservices','$cftraining','$imapper_vis','$publimap_vis','$active')";
  $bool=mysql_query($partner_SQL_insert);
  if($bool==1) echo "<div id='adminok'>The Partner has been added.</div>";
  if($bool<>1) echo "<div id='adminnok'>An error occurred when trying to add the Partner.</div>";
}

if($action=="update"){

  $partner_first=$_POST['partner_first'];	
  $partner_last=$_POST['partner_last'];
  $partner_email=$_POST['partner_email'];
  $partner_company=$_POST['partner_company'];
  $partner_id=$_POST['partner_id'];
  $partner_user=$_POST['partner_user'];
  $partner_pw=$_POST['partner_pw'];
  $rev_vis=$_POST['rev_vis'];
  $cf_vis=$_POST['cf_vis'];
  $serv_vis=$_POST['serv_vis'];
  $cfservices=$_POST['cfservices'];
  $cftraining=$_POST['cftraining'];
  $imapper_vis=$_POST['imapper_vis'];
  $publimap_vis=$_POST['publimap_vis'];
  $active=$_POST['active'];
  
  $partner_SQL_update="UPDATE reporting_partners SET partner_first='$partner_first',partner_last='$partner_last',partner_email='$partner_email',partner_company='$partner_company',partner_user='$partner_user',partner_pw='$partner_pw',rev_vis='$rev_vis',cf_vis='$cf_vis',serv_vis='$serv_vis',cftraining='$cftraining',cfservices='$cfservices',imapper_vis='$imapper_vis',publimap_vis='$publimap_vis',active='$active' WHERE partner_id='$partner_id'";
  $bool=mysql_query($partner_SQL_update);
  if($bool==1) echo "<div id='adminok'>The Partner has been saved.</div>";
  if($bool<>1) echo "<div id='adminnok'>An error occurred when trying to save the Partner.</div>";  
}

$partner_SQL="SELECT * FROM reporting_partners ORDER BY active DESC, partner_company ASC";
$partner_result=mysql_query($partner_SQL);
?>
<body>
<?php
include("../topnav.php");
?>
<div class="container-fluid">
            <div class="row-fluid">
                
                <?php
				//include("../sidenav.php");
				?>
                
                <div class="span10" id="content">
                    <div class="row-fluid">
                    	<div class="span12">
                        
                            <!-- block -->
                            <div class="block">
                                <div class="navbar navbar-inner block-header">
                                <h3>List of Partners</h3>   <?php echo $action;  ?>
                                </div>
                                <div class="block-content collapse in">
<?php 
if (!isset($_SESSION["username"])){
	echo $niet_ingelogd;
}
// hieronder staan de functies die toegelaten worden tot deze pagina
elseif($_SESSION["function"] != "admin"){
	echo $geen_toegang;
}else{
?>

    <p class="buttons"><a href="partner_new.php" class="add">Add New Partner</a></p>
	<table width="100%" border="0" cellspacing="0" cellpadding="0" class="table table-striped" >
  <tr>
    <th>Company name</th>
    <th>User name</th>
    <th>Password</th>
    <th>CF Tr.</th>
    <th>CF Serv</th>
    <th>View rights</th>
    <th></th>
    <th></th>
  </tr>

<?php
while($partner=mysql_fetch_array($partner_result)){
?>
    
	<tr <? if($partner['active']=="no") { echo "style='color:#ff0000;'"; } ?>>
    <td><?php echo $partner['partner_company'] ?></td>
    <td><?php echo $partner['partner_user'] ?></td>
    <td><?php echo $partner['partner_pw'] ?></td>
    <td><? if(!empty($partner['cftraining'])){ echo "&euro;" . $partner['cftraining']; } ?></td>
    <td><? if(!empty($partner['cfservices'])){ echo "&euro;" . $partner['cfservices']; } ?></td>
    <td width="200">
    <? if($partner['partner_id']!='4') { ?>
    <table width="100%" cellpadding="0" cellspacing="0" border="0">
    <tr>
    <td>Revenue field</td>
    <td><?php echo $partner['rev_vis'] ?></td>
    </tr>
    <tr>
    <td>Commitment Fee block</td>
    <td><?php echo $partner['cf_vis'] ?></td>
    </tr>
    <tr>
    <td>Services Block</td>
    <td><?php echo $partner['serv_vis'] ?></td>
    </tr>
    <tr>
    <td>iMapper Revenue block</td>
    <td><?php echo $partner['imapper_vis'] ?></td>
    </tr>
    <tr>
    <td>Publimap Revenue block</td>
    <td><?php echo $partner['publimap_vis'] ?></td>
    </tr>
    </table>
	<? } ?></td>
    <td width="50" align="center"><?php if($partner['partner_id']!="4") { ?><a href=partner_edit.php?partner_id=<?php echo $partner['partner_id'] ?>><img src="../../images/edit.png" alt="Edit" /></a><?php } ?></td>
    <td width="50" align="center"><?php if($partner['partner_id']!="4") { ?><a href=partner_list.php?partner_id=<?php echo $partner['partner_id'] ?>&action=delete onClick="return confirm('Are you sure you want to delete this Partner?')"><img src="../../images/delete.jpg" alt="Delete" /></a><?php } ?></td>
  </tr>

<?php
}
mysql_close();
?>          
 </table>
 <?php
}
?>

                                </div>
                            </div>
                            <!-- /block -->


</div>
</div>
</div>
</div>
            <hr>
<?php
include("../footer.php");
?>
</div>
</body>
</html>