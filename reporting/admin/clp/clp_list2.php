<?php
$rootdir="../../";
$page="CLP IDs";
include("../header.php");
include("../connect.php");

$action = isset($_POST['action']) ? trim($_POST['action']) : '';
$action = isset($_GET['action']) ? trim($_GET['action']) : '';






if($action=="delete"){
  $clp_id=$_GET['clp_id'];
  	
  $request_SQL_del="DELETE FROM reporting_clp WHERE clp_id=$clp_id";
  $bool=mysql_query($request_SQL_del);
  if($bool==1) echo "<div id='adminok'>The CLP application has been deleted.</div>";
  if($bool<>1) echo "<div id='adminnok'>An error occurred when trying to delete the CLP application.</div>";
}

$clp_SQL="SELECT * FROM reporting_clp_number,reporting_clp WHERE reporting_clp_number.clp_number_id=reporting_clp.clp_number_id AND reporting_clp.company !='-' ORDER BY reporting_clp.clp_id";
//$clp_SQL ="SELECT clp_id, clp_number_id FROM clp WHERE clp_number_id !='' ORDER BY clp_id DESC";
$clp_result=mysql_query($clp_SQL) or die(mysql_error());
$aantal_assigned = mysql_num_rows($clp_result); 

$request_SQL="SELECT * FROM reporting_clp WHERE clp_number_id = '' AND clp_id!='1'";
$request_result=mysql_query($request_SQL);
$aantal_unassigned = mysql_num_rows($request_result); 
?>

<body>
<?php
include("../topnav.php");
?>
<div class="container-fluid">
            <div class="row-fluid">
                
                <?php
				//include("../sidenav.php");
				?>
                
                <div class="span10" id="content">
                    <div class="row-fluid">
                    	<div class="span12">
                        
                            <!-- block -->
                            <div class="block">
                                <div class="navbar navbar-inner block-header">
                                <h3>CLP IDs</h3>     
                                </div>
                                <div class="block-content collapse in">
<?php 
if (!isset($_SESSION["username"])){
	echo $niet_ingelogd;
}
// hieronder staan de functies die toegelaten worden tot deze pagina
elseif($_SESSION["function"] != "admin"){
	echo $geen_toegang;
}else{
?> 
    
    <p><a href="clp_list1.php">Assigned (<? echo $aantal_assigned; ?>)</a> | <a href="clp_list2.php">Unassigned (<? echo $aantal_unassigned; ?>)</a></p>
	<table width="100%" border="0" cellspacing="0" cellpadding="0" class="table table-striped" id="table">
  <tr>
    <th width="30">ID</th>
    <th width="150">Partner</th>
    <th width="50">CLP ID</th>
    <th width="200">Customer</th>
    <th width="80">Country</th>
    <th width="50">Details</th>
    <th width="50">Delete</th>
  </tr>

<?php
while($request=mysql_fetch_array($request_result)){
?>
    
	<tr>
    <td><?php echo $request['clp_id'] ?></td>
    <td><?php echo $request['partner'] ?></td>
    <td><a href=clp_list3.php?clp_id=<?php echo $request['clp_id'] ?>>Assign</a></td>
    <td><?php echo $request['company'] ?></td>
    <td><?php echo $request['country'] ?></td>
    <td width="50" align="center"><a href="clp_details.php?clp_id=<?php echo $request['clp_id'] ?>" onClick="window.open('clp_details.php?clp_id=<?php echo $request['clp_id'] ?>','popup','width=750,height=750,scrollbars=yes,toolbar=no,location=no'); return false"><img src="../../images/view.png" alt="Details" /></a></td>
    <td width="50" align="center"><a href=clp_list2.php?clp_id=<?php echo $request['clp_id'] ?>&action=delete onClick="return confirm('Are you sure you want to delete this CLP request?')"><img src="../../images/delete.jpg" alt="Delete" /></a></td>
  </tr>

<?php
}
mysql_close();
?>          
 </table>
 <?php
}
?>

                                </div>
                            </div>
                            <!-- /block -->


</div>
</div>
</div>
</div>
            <hr>
<?php
include("../footer.php");
?>