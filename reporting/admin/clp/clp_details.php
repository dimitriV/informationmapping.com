<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml">
<head>
<meta http-equiv="Content-Type" content="text/html; charset=utf-8" />
<title>Report details</title>
<link rel="stylesheet" type="text/css" href="../../style.css" />
</head>
<body id="popup">
<?php
$rootdir="../../";
include("../connect.php");

if(isset($_GET['action'])) $action=$_GET['action'];
if(isset($_POST['action'])) $action=$_POST['action'];


$request_SQL="SELECT * FROM reporting_clp WHERE clp_id=" . $_GET['clp_id'];
$request_result=mysql_query($request_SQL);
$request=mysql_fetch_array($request_result);
?>

<div id="admin"> 
    <h3>CLP Request Details</h3>
    <p>Requested by:</p>
	<table width="500" border="0" cellspacing="0" cellpadding="0" class="report">
  <tr>
    <th width="150">Partner</th>
    <td><?php echo $request['partner'] ?></td>
  </tr>
  <tr>
    <th>Name</th>
    <td><?php echo $request['firstname'] ?></td>
   </tr>
	<tr>
    <th>E-mail</th>
    <td><?php echo $request['email'] ?></td>
  </tr>
	<tr>
    <th>Date</th>
    <td>
    <?php
	// datum ophalen volgens formaat Feb 1, 2013
	$submit_time_result = mysql_query("SELECT DATE_FORMAT(submit_time, '%b %d, %Y') AS datum FROM reporting_clp WHERE clp_id='" . $_GET['clp_id'] . "'");
	
    while($row = mysql_fetch_assoc($submit_time_result))
    {
        echo $row['datum'];
    }
	?>
    </td>
  </tr>
 </table>
 
     <p>Requested for:</p>
     
	<table width="500" border="0" cellspacing="0" cellpadding="0" class="report">
  <tr>
    <th width="150">Customer</th>
    <td><?php echo $request['company'] ?></td>
  </tr>
  <tr>
    <th>Customer address</th>
    <td><?php echo $request['address1'] ?><br />
<?php echo $request['address2'] ?><br />
<?php echo $request['country'] ?></td>
   </tr>
   <tr>
    <th>Customer website</th>
    <td><?php echo $request['website'] ?></td>
  </tr>
	<tr>
    <th>Contact name</th>
    <td><?php echo $request['contactname_first'] . " " . $request['contactname_last']; ?></td>
  </tr>
  <tr>
    <th>Contact function</th>
    <td><?php echo $request['contactfunction'] ?></td>
  </tr>
  <tr>
    <th>Contact e-mail</th>
    <td><?php echo $request['contactemail'] ?></td>
  </tr>
  <tr>
    <th>Expected budget 2010</th>
    <td>&euro; <?php echo $request['budget10'] ?></td>
  </tr>
  <tr>
    <th>Expected budget 2011</th>
    <td>&euro; <?php echo $request['budget11'] ?></td>
  </tr>
    <tr>
    <th>Expected budget 2012</th>
    <td>&euro; <?php echo $request['budget12'] ?></td>
  </tr>
    <tr>
    <th>Expected budget 2013</th>
    <td>&euro; <?php echo $request['budget13'] ?></td>
  </tr>
  <tr>
    <th>Comments</th>
    <td><?php echo $request['comments'] ?></td>
  </tr>
 </table>   
 
 Comments IMI:
 <?php 
  if($action=="update"){
  $comments_imi=htmlspecialchars($_POST['comments_imi'], ENT_QUOTES); 
  
  $comments_SQL_update="UPDATE reporting_clp SET comments_imi='$comments_imi' WHERE clp_id=" . $_GET['clp_id'];
  $bool=mysql_query($comments_SQL_update);
  if($bool==1) echo "<strong>Saved</strong>";
  if($bool<>1) echo "<strong>Error</strong>";  
  }
	
  $request2_SQL="SELECT * FROM reporting_clp WHERE clp_id=" . $_GET['clp_id'];
  $request2_result=mysql_query($request2_SQL);
  $request2=mysql_fetch_array($request2_result);
  ?>
  
  <form action="<?php echo $_SERVER['PHP_SELF']; ?>?clp_id=<?php echo $request2['clp_id'] ?>" method="post">
  <table width="500" border="0" cellspacing="0" cellpadding="0" class="report">
  <tr>
    <th width="150">Comments</th>
    <td><textarea name="comments_imi" cols="35" rows="4"><?php echo $request2['comments_imi'] ?></textarea><br />
<input type="hidden" name="clp_id" value="<?php echo $request2['clp_id'] ?>"><input type="hidden" name="action" value="update"> <input type="submit" name="submit" value="Save comments"></td>
  </tr>  
  </table>
  </form>
</div>
</body>
</html>