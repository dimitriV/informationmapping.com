<?php
$rootdir="../../";
$page="CLP IDs";
include("../header.php");
include("../connect.php");

$action = isset($_POST['action']) ? trim($_POST['action']) : '';
$action = isset($_GET['action']) ? trim($_GET['action']) : '';
//if(isset($_GET['action'])) $action=$_GET['action'];
//if(isset($_POST['action'])) $action=$_POST['action'];

if($action=="update"){

  $clp_number_id=$_POST['clp_number_id'];
  $clp_id=$_GET['clp_id'];
	
  $clp_SQL_update="UPDATE reporting_clp,reporting_clp_number SET reporting_clp.clp_number_id='$clp_number_id',reporting_clp_number.clp_id='$clp_id' WHERE reporting_clp.clp_id='$clp_id' AND reporting_clp_number.clp_number_id='$clp_number_id'";
  $bool=mysql_query($clp_SQL_update);
  if($bool==1) echo "<div id='adminok'>The CLP number ID has been successfully assigned.</div>";
  if($bool<>1) echo "<div id='adminnok'>An error occurred when trying to assign the CLP ID.</div>"; 
  
}


if($action=="delete"){
  $clp_id=$_GET['clp_id'];
  	
  $request_SQL_del="DELETE FROM reporting_clp WHERE clp_id='$clp_id'";
  $request_SQL_del2="DELETE FROM reporting_clp_number WHERE clp_id='$clp_id'";
  $bool=mysql_query($request_SQL_del);
  mysql_query($request_SQL_del2);
  if($bool==1) echo "<div id='adminok'>The CLP ID has been deleted.</div>";
  if($bool<>1) echo "<div id='adminnok'>An error occurred when trying to delete the CLP ID.</div>";
}


$clp_SQL="SELECT * FROM reporting_clp_number,reporting_clp WHERE reporting_clp_number.clp_number_id=reporting_clp.clp_number_id AND reporting_clp.company !='-' ORDER BY reporting_clp.clp_id DESC";
$clp_result=mysql_query($clp_SQL) or die(mysql_error());
$aantal_assigned = mysql_num_rows($clp_result); 

$request_SQL="SELECT * FROM reporting_clp WHERE clp_number_id = '' AND clp_id!='1'";
$request_result=mysql_query($request_SQL);
$aantal_unassigned = mysql_num_rows($request_result); 

?>

<body>
<?php
include("../topnav.php");
?>
<div class="container-fluid">
            <div class="row-fluid">
                
                <?php
				//include("../sidenav.php");
				?>
                
                <div class="span10" id="content">
                    <div class="row-fluid">
                    	<div class="span12">
                        
                            <!-- block -->
                            <div class="block">
                                <div class="navbar navbar-inner block-header">
                                <h3>CLP IDs</h3>     
                                </div>
                                <div class="block-content collapse in">
<?php 
if (!isset($_SESSION["username"])){
	echo $niet_ingelogd;
}
// hieronder staan de functies die toegelaten worden tot deze pagina
elseif($_SESSION["function"] != "admin"){
	echo $geen_toegang;
}else{
?>
    
    <p><a href="clp_list1.php">Assigned (<? echo $aantal_assigned; ?>)</a> | <a href="clp_list2.php">Unassigned (<? echo $aantal_unassigned; ?>)</a> | <a href="clp_list_endcustomer.php">View list per end customer</a></p>
	<table width="100%" border="0" cellspacing="0" cellpadding="0" class="table table-striped" id="table">
  <tr>
  <th width="30">ID</th>
    <th width="150">Partner</th>
    <th width="50">CLP ID</th>
    <th width="200">Customer</th>
    <th width="80">Country</th>
    <th>Contact e-mail</th>
    <th width="100">Comments IMI</th>
    <th width="50">Details</th>
    <th width="50">PDF</th>
    <th>Delete</th>
  </tr>

<?php
while($clp=mysql_fetch_array($clp_result)){
?>
    
	<tr>
    <td><?php echo $clp['clp_id'] ?></td>
    <td><?php echo $clp['partner'] ?></td>
    <td><?php echo $clp['clp_number'] ?></td>
    <td><?php echo $clp['company'] ?></td>
    <td><?php echo $clp['country'] ?></td>
    <td><?php echo $clp['contactemail'] ?></td>
    <td><?php echo $clp['comments_imi']; ?></td>
    
    <td align="center">
	<a href="clp_details.php?clp_id=<?php echo $clp['clp_id'] ?>" onClick="window.open('clp_details.php?clp_id=<?php echo $clp['clp_id'] ?>','popup','width=750,height=750,scrollbars=yes,toolbar=no,location=no'); return false"><img src="../../images/view.png" alt="Details" /></a></td>
    <td align="center">
	<a href="pdf.php?clp_id=<?php echo $clp['clp_id'] ?>" target="_blank"><img src="../../images/pdf.png" alt="PDF" /></a></td>
    <td><a href="?clp_id=<?php echo $clp['clp_id']; ?>&amp;action=delete"><img src="../images/delete.jpg" alt="Delete CLP" /></a>
    </td>
  </tr>

<?php
}
?>          
 </table>
 <?php
}
?>

                                </div>
                            </div>
                            <!-- /block -->


</div>
</div>
</div>
</div>
            <hr>
<?php
include("../footer.php");
?>