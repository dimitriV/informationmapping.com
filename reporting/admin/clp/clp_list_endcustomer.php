<?php
$rootdir="../../";
$page="CLP IDs";
include("../header.php");
include("../connect.php");

$action = isset($_POST['action']) ? trim($_POST['action']) : '';
$action = isset($_GET['action']) ? trim($_GET['action']) : '';

if(isset($_POST['verzenden'])) {
header("Location: clp_list_endcustomer.php?year=" . $_POST['choice_year'] . "&company=" . $_POST['choice_partner'] . "&clp=" . $_POST['choice_clp']); 
}
?>
<script type="text/javascript">
 /*function check(i,j,k) {
  if(document.f[i].value.length > 5)    
  {
  document.f[j].disabled = true;
  document.f[k].disabled = true; 
  }
  };*/
</script>

<body>
<?php
include("../topnav.php");
?>
<div class="container-fluid">
            <div class="row-fluid">
                
                <?php
				//include("../sidenav.php");
				?>
                
                <div class="span10" id="content">
                    <div class="row-fluid">
                    	<div class="span12">
                        
                            <!-- block -->
                            <div class="block">
                                <div class="navbar navbar-inner block-header">
                                <h3>CLP IDs</h3>     
                                </div>
                                <div class="block-content collapse in">
<?php 
if (!isset($_SESSION["username"])){
	echo $niet_ingelogd;
}
// hieronder staan de functies die toegelaten worden tot deze pagina
elseif($_SESSION["function"] != "admin"){
	echo $geen_toegang;
}else{
?>
      <form action="clp_list_endcustomer.php" method="post" name="f">
      
      Enter the CLP number here: <input type="text" name="choice_clp" /> 
        <select name="choice_partner">
        <option value="">- Please select a partner-</option>
        <?php
		$partner_SQL="SELECT * FROM reporting_partners WHERE active='yes' ORDER BY partner_company ASC";
		$partner_result=mysql_query($partner_SQL);
		while($partner=mysql_fetch_array($partner_result)){
		?>
        <option value="<?php echo $partner['partner_id'];  ?>" <?php if(($_GET['company'])==$partner['partner_id']) { echo "selected='selected'";} ?>"><?php echo $partner['partner_company']; ?></option>

       <?php
		}
		?>
      </select>
      
      
  <select name="choice_year">
        <?php
        // lees alle jaren uit die in de databasetabel zitten
		$select_SQL="SELECT * FROM reporting_year ORDER BY year_id ASC";
		$select_result=mysql_query($select_SQL);
		while($partner=mysql_fetch_array($select_result)){
		
		$current_year = date("Y");
        // wanneer de pagina geen GET waarde heeft, veronderstellen we het huidige jaar, deze code is ook nodig om de selected='selected' hieronder goed te laten werken
		if(empty($_GET['year']))
		{
		$_GET['year'] = $current_year;
		}
		?>
       <option value="<?php echo $partner['year'] ?>" <?php if(($_GET['year'])==$partner['year']) { echo "selected='selected'";} ?>>
        <?php echo $partner['year'] ?>
        </option>
        
  
       <?php
		}
		?> 
      </select>*
         <input type="submit" value="Show report" name="verzenden" />
      </form>   

<?php
$totaal_part=0;
$company_voor_query = $_GET['company'];
$clp_voor_query = $_GET['clp'];
$year_voor_query = $_GET['year'];

//if(empty($_GET['year'])) { $year_voor_query = date("Y"); } else { $year_voor_query = $_GET['year']; }

/*if(!empty($_GET['clp'])) 
{ 
$clp_voor_query = $_GET['clp']; 
$clp_SQL="SELECT * FROM participants WHERE clp_number = '$clp_voor_query' ORDER BY year DESC";
}
else {
$clp_SQL="SELECT * FROM participants WHERE year = '$year_voor_query' AND partner_id = '$company_voor_query' AND clp_number != '0'";
}
$clp_result=mysql_query($clp_SQL) or die(mysql_error());*/

	if(empty($_GET['clp']) && empty($_GET['company'])) 
	{ 
		$clp_result = mysql_query("SELECT * FROM reporting_participants WHERE year = '$year_voor_query' AND clp_number != '0' ORDER BY report_id ASC"); 	
	}

	if(!empty($_GET['clp']) && !empty($_GET['company'])) 
	{ 
		$clp_result = mysql_query("SELECT * FROM reporting_participants WHERE clp_number = '$clp_voor_query' AND partner_id = '$company_voor_query' AND year = '$year_voor_query'  ORDER BY report_id ASC"); 	
	}

	if(!empty($_GET['clp']) && empty($_GET['company'])) 
	{ 
		$clp_result = mysql_query("SELECT * FROM reporting_participants WHERE clp_number = '$clp_voor_query' AND year = '$year_voor_query'  ORDER BY report_id ASC"); 	
	}

	if(empty($_GET['clp']) && !empty($_GET['company'])) 
	{ 
		$clp_result = mysql_query("SELECT * FROM reporting_participants WHERE partner_id = '$company_voor_query' AND year = '$year_voor_query' AND clp_number != '0' ORDER BY report_id ASC"); 	
	}

?>

<div id="admin"> 
    <h3>Trainees per end customer</h3>
    <p>The following table gives you an overview of the total amount of trainees per end customer.</p>
	<table width="100%" border="0" cellspacing="0" cellpadding="0" class="report" id="table">
  <tr>
  <th width="30">CLP ID</th>
    <th width="40">Customer</th>
    <th>Course</th>
    <th>Participants</th>
    <th>Date</th>
    <th>CLP Details</th>
    <th>PDF</th>
  </tr>

<?php
while($clp=mysql_fetch_array($clp_result)){
?>
    
	<tr>
    <?php
	// query om clp nummer op te halen
	$result_overv = mysql_query("SELECT * FROM reporting_clp_number WHERE clp_number = '" . $clp['clp_number'] . "'"); 
	$clpo=mysql_fetch_array($result_overv);	
	?>
    <td><?php echo $clpo['clp_number']; ?></td>
    <td><?php 

		$result_part = mysql_query("SELECT * FROM reporting_clp WHERE clp_id = '" . $clpo['clp_id'] . "'");
		while($clpop=mysql_fetch_array($result_part)) {
		echo $clpop['company'] . "</td>";
		
		$res_course = mysql_query("SELECT * FROM reporting_courses WHERE course_id = '" . $clp['course_id'] . "'");
		$clpoc=mysql_fetch_array($res_course);
		echo "<td>" . $clpoc['course_name'] . "</td>";
		//echo $clp['course_id'] . "<br />";
		echo "<td align=\"right\">" . $clp['participant_amount'] . "</td><td>" . $clp['month'] . " " . $clp['year'] . "</td>";
		echo "<td><a href=\"clp_details.php?clp_id=" .  $clpo['clp_id'] . "\" onclick=\"window.open('clp_details.php?clp_id=" . $clpo['clp_id'] . "','popup','width=750,height=750,scrollbars=yes,toolbar=no,location=no'); return false\"><img src=\"../../images/view.png\" alt=\"Details\" /></a></td>";
		echo "<td><a href=\"pdf.php?clp_id=" . $clpo['clp_id'] . "\" target=\"_blank\"><img src=\"../../images/pdf.png\" alt=\"PDF\" /></a></td></td>";
		$totaal_part+=$clp['participant_amount'];
		}
	
	?></td>
    
  </tr>

<?php
}
?>          
<tr><td colspan="4" align="right">
 <?php echo "<strong>" . $totaal_part . "</strong>"; ?></td><td colspan="3">&nbsp;</td></tr>
  </table>
 <?php
// toon totalen opgeteld per waarde in veldnaam

/*$result2 = mysql_query("SELECT course_id, SUM(participant_amount) FROM participants GROUP BY course_id"); 
	 

// Print out result
while($row = mysql_fetch_array($result2)){

	echo "Total ". $row['course_id']. " = ". $row['SUM(participant_amount)'];
	echo "<br />";

}*/
?>

 </div> 
 <?php
}
?>
                                </div>
                            </div>
                            <!-- /block -->


</div>
</div>
</div>
</div>
            <hr>
<?php
include("../footer.php");
?>