﻿<?php
$rootdir="../../";
$page="CLP IDs";
include("../header.php");
include("../connect.php");

if(isset($_GET['action'])) $action=$_GET['action'];
if(isset($_POST['action'])) $action=$_POST['action'];
?>
<div id="admin"> 

    <h3>CLP IDs</h3>
<?php


$request_SQL="SELECT * FROM reporting_clp_number WHERE clp_id=" . $_GET['clp_id'];
$request_result=mysql_query($request_SQL);
$aantal = mysql_num_rows($request_result);





// uniek transactienummer creëren
function randomize()
{
	// transactienummer genereren
	$transactienummer=mt_rand(100000,999999);

	// checken of transactienummer wel uniek is
	$result = mysql_query("SELECT clp_number FROM reporting_clp_number WHERE clp_number = '$transactienummer'");
	while ($row = mysql_fetch_assoc($result)) { 
		$nr = $row['clp_number'];
	}

	if($nr != $transactienummer)
	{	
		return $transactienummer;	 
	}
	else 
	{
		return randomize();	
	}
}
		  
$transactienummer = randomize();


if ($aantal > 0) {
	// als de record bestaat is er al een CLP ID toegewezen
	echo "<p>CLP ID is already assigned for this request. You probably clicked more than once on the link in your email.</p>
	<p><a href=\"clp_list1.php\">Back to the CLP overview.</a></p>";
}
else
{
// record toevoegen in tbl clp_number en record in tbl clp updaten met het id

$result_insert = mysql_query("INSERT INTO reporting_clp_number (clp_number, clp_id) VALUES ('$transactienummer','" . $_GET['clp_id'] . "')");
$idvanreports = mysql_insert_id();
$result_update=mysql_query("UPDATE reporting_clp SET clp_number_id='$idvanreports' WHERE clp_id='" . $_GET['clp_id'] . "'");
// het toegekende CLP nummer mailen naar IMI 
if($result_insert == true && $result_update == true) {
	echo "<p>The CLP id has been assigned with CLP number <strong>$transactienummer</strong>.</p>
	<p>We have sent you an e-mail with more details.</p>";
	// db gegevens ophalen om in mail te tonen
	
	$result_ophalen = mysql_query("SELECT * FROM reporting_clp WHERE clp_id = '" . $_GET['clp_id'] . "'");
	$row = mysql_fetch_assoc($result_ophalen);
	$name = $row['name'];
	$email = $row['email'];
	$partner = $row['partner'];
	//$company = $row['company'];
	$company = str_replace(",", "", $row['company']); // replace a comma by nothing to avoid comma's in the filename of the pdf (otherwise in Junk folder)
	$address1 = $row['address1'];
	$address2 = $row['address2'];
	$country = $row['country'];
	$contactname = $row['contactname'];
	$contactfunction = $row['contactfunction'];
	$contactemail = $row['contactemail'];
	$website = $row['website'];
	$budget12 = $row['budget12'];
	$budget13 = $row['budget13'];
	$comments = $row['comments'];

    $YearNow= date('Y');

    $YearNext= date('Y', strtotime('+1 year'));
	
	// mailen naar IMI
	$messageemail     = "<html>
<meta http-equiv=\"Content-Type\" content=\"text/html charset=UTF-8\" />
  <body>
  <h1><font size='4' face='Arial, Helvetica, sans-serif' color='#135284'>Assigned CLP ID $transactienummer</font></h1>
  <p><font size='2' face='Arial, Helvetica, sans-serif'>CLP ID requested by $name from $partner</font></p>
  <h2><font size='3' face='Arial, Helvetica, sans-serif' color='#5D8EB4'>CLP ID requested for:</font></h2>
  <table cellpadding=\"0\" cellspacing=\"0\" width=\"450\" style=\"border-left: 1px solid #666666;border-top: 1px solid #666666;\">
  <tr>
  <td style=\"border-right: 1px solid #666666;border-bottom: 1px solid #666666;padding:2px 4px;width:175px; \" ><font size='2' face='Arial, Helvetica, sans-serif'><strong>Customer</strong></font></td>
  <td style=\"border-right: 1px solid #666666;border-bottom: 1px solid #666666;padding:2px 4px;\"><font size='2' face='Arial, Helvetica, sans-serif'>$company</font></td>
  </tr>
  
  <tr>
  <td style=\"border-right: 1px solid #666666;border-bottom: 1px solid #666666;padding:2px 4px;\" ><font size='2' face='Arial, Helvetica, sans-serif'><strong>Customer address</strong></font></td>
  <td style=\"border-right: 1px solid #666666;border-bottom: 1px solid #666666;padding:2px 4px;\"><font size='2' face='Arial, Helvetica, sans-serif'>$address1<br>$address2<br>$country</font></td>
  </tr>

  <tr>
  <td style=\"border-right: 1px solid #666666;border-bottom: 1px solid #666666;padding:2px 4px;\" ><font size='2' face='Arial, Helvetica, sans-serif'><strong>Contact name</strong></font></td>
  <td style=\"border-right: 1px solid #666666;border-bottom: 1px solid #666666;padding:2px 4px;\"><font size='2' face='Arial, Helvetica, sans-serif'>$contactname</font></td>
  </tr>
  
  <tr>
  <td style=\"border-right: 1px solid #666666;border-bottom: 1px solid #666666;padding:2px 4px;\" ><font size='2' face='Arial, Helvetica, sans-serif'><strong>Contact function</strong></font></td>
  <td style=\"border-right: 1px solid #666666;border-bottom: 1px solid #666666;padding:2px 4px;\"><font size='2' face='Arial, Helvetica, sans-serif'>$contactfunction</font></td>
  </tr>
  
    <tr>
  <td style=\"border-right: 1px solid #666666;border-bottom: 1px solid #666666;padding:2px 4px;\" ><font size='2' face='Arial, Helvetica, sans-serif'><strong>Contact e-mail</strong></font></td>
  <td style=\"border-right: 1px solid #666666;border-bottom: 1px solid #666666;padding:2px 4px;\"><font size='2' face='Arial, Helvetica, sans-serif'>$contactemail</font></td>
  </tr>
  
    <tr>
  <td style=\"border-right: 1px solid #666666;border-bottom: 1px solid #666666;padding:2px 4px;\" ><font size='2' face='Arial, Helvetica, sans-serif'><strong>Customer website</strong></font></td>
  <td style=\"border-right: 1px solid #666666;border-bottom: 1px solid #666666;padding:2px 4px;\"><font size='2' face='Arial, Helvetica, sans-serif'>$website</font></td>
  </tr>
  
    <tr>
  <td style=\"border-right: 1px solid #666666;border-bottom: 1px solid #666666;padding:2px 4px;\" ><font size='2' face='Arial, Helvetica, sans-serif'><strong>Expected budget $YearNow</strong></font></td>
  <td style=\"border-right: 1px solid #666666;border-bottom: 1px solid #666666;padding:2px 4px;\"><font size='2' face='Arial, Helvetica, sans-serif'>&euro; $budget12</font></td>
  </tr>
  
  <tr>
  <td style=\"border-right: 1px solid #666666;border-bottom: 1px solid #666666;padding:2px 4px;\" ><font size='2' face='Arial, Helvetica, sans-serif'><strong>Expected budget $YearNext</strong></font></td>
  <td style=\"border-right: 1px solid #666666;border-bottom: 1px solid #666666;padding:2px 4px;\"><font size='2' face='Arial, Helvetica, sans-serif'>&euro; $budget13</font></td>
  </tr>
  
  <tr>
  <td style=\"border-right: 1px solid #666666;border-bottom: 1px solid #666666;padding:2px 4px;\" ><font size='2' face='Arial, Helvetica, sans-serif'><strong>Comments</strong></font></td>
  <td style=\"border-right: 1px solid #666666;border-bottom: 1px solid #666666;padding:2px 4px;\"><font size='2' face='Arial, Helvetica, sans-serif'>$comments</font></td>
  </tr>
  
  <tr>
  <td style=\"border-right: 1px solid #666666;border-bottom: 1px solid #666666;padding:2px 4px;\" ><font size='2' face='Arial, Helvetica, sans-serif'><strong>Assigned CLP ID</strong></font></td>
  <td style=\"border-right: 1px solid #666666;border-bottom: 1px solid #666666;padding:2px 4px;\"><font size='2' face='Arial, Helvetica, sans-serif'>$transactienummer</font></td>
  </tr>
  </table>
  </body></html>";
	$emailsubject = "Assigned CLP ID for partner " . htmlspecialchars_decode($partner);
    // random hash necessary to send mixed content

    // attachment name
    $filenameclp = "CLP_" . $company . "_" . $transactienummer . ".pdf";
    $pdfdoc = "http://www.informationmapping.com/reporting/clp_mail/pdf.php?clp_id=" . $_GET['clp_id'];

    $my_file = $filenameclp;
    $my_name = "Information Mapping International";
    $my_mail = "info@informationmapping.com";
    $my_replyto = "info@informationmapping.com";
    $to_email = $email;
    $my_subject = $emailsubject;
    $my_attach = $pdfdoc;
    $message = $messageemail;
    mail_attachment($my_file, $my_attach, $to_email, $my_mail, $my_name, $my_replyto, $my_subject, $message);

    ///////////HEADERS INFORMATION////////////
    // main header (multipart mandatory) message
    //$headers  = "From: $name <$email>".$eol;
//    $headers  = "From: Information Mapping <info@informationmapping.com>".$eol;
//	$headers .= "CC: Francis Declercq <fdeclercq@informationmapping.com>".$eol;
//    $headers .= "BCC: Sofian Mourabit <smourabit@informationmapping.com>".$eol;
//    $headers .= "MIME-Version: 1.0".$eol;
//    $headers .= "Content-Type: multipart/mixed; boundary=\"".$separator."\"".$eol;
//    $headers .= "Content-Transfer-Encoding: 7bit".$eol;
//    $headers .= "This is a MIME encoded message.".$eol.$eol;
//
//    // message
//    $message = "--".$separator.$eol;
//    $message .= "Content-Type: text/html; charset=\"UTF-8\"".$eol;
//    $message .= "Content-Transfer-Encoding: 8bit".$eol;
//    $message .= $message.$eol;
//
//    // attachment
//    $message .= "--".$separator.$eol;
//    $message .= "Content-Type: application/octet-stream; name=\"".$filename."\"".$eol;
//    $message .= "Content-Transfer-Encoding: base64".$eol;
//    $message .= "Content-Disposition: attachment".$eol;
//    $message .= $attachment.$eol;
//    $message .= "--".$separator."--";


    //Email message
   //$mail_sent =  mail($emailto, $emailsubject, $message, $headers, "-f info@informationmapping.com");

//
//    if($mail_sent) echo 'sent!';
//    else print_r(error_get_last());

	
	
}
?>





    

<?php
}
?>          

 </div>            
</div>
</body>
</html>


<?php
function mail_attachment($filename, $attach, $mailto, $from_mail, $from_name, $replyto, $subject, $message) {
    $body = '';
    $headers = '';

    $file = $filename;
    $separator = md5(time());
    $attachment = chunk_split(base64_encode(file_get_contents($attach)));
    $headers = "From: ".$from_name."<".$from_mail.">" . PHP_EOL;
    $headers .= "Cc: fdeclercq@informationmapping.com" . PHP_EOL;
    $headers .= "Bcc: smourabit@informationmapping.com" . PHP_EOL;
    $headers .= "Reply-To: ".$replyto . PHP_EOL;
    $headers .= "MIME-Version: 1.0".PHP_EOL;
    $headers .= "Content-Type: multipart/mixed; boundary=\"" . $separator . "\"" . PHP_EOL . PHP_EOL;
    $body .= "Content-Transfer-Encoding: 7bit" . PHP_EOL;
    $body .= "This is a MIME encoded message." . PHP_EOL;
    $body .= "--" . $separator . PHP_EOL;
    $body .= "Content-Type: text/html; charset=\"iso-8859-1\"" . PHP_EOL;
    $body .= "Content-Transfer-Encoding: 8bit" . PHP_EOL . PHP_EOL;
    $body .= $message . PHP_EOL;
    $body .= "--" . $separator . PHP_EOL;
    $body .= "Content-Type: application/octet-stream; name=\"" . $filename . "\"" . PHP_EOL;
    $body .= "Content-Transfer-Encoding: base64" . PHP_EOL;
    $body .= "Content-Disposition: attachment" . PHP_EOL . PHP_EOL;
    $body .= $attachment . PHP_EOL;
    $body .= "--" . $separator . "--";
    if (mail($mailto, $subject, $body, $headers)) {
        echo "Message sent!";
    } else {
        print_r(error_get_last());
    }
}


?>