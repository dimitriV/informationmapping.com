<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml">
<head>
<meta http-equiv="Content-Type" content="text/html; charset=utf-8" />
<title>Renewals FS Pro 2013 Subscription</title>
<style type="text/css">
th { background-color: #009ddf; color: white; }
body, table { font-family: Arial, Helvetica, sans-serif; font-size: 12px; }
td { border-bottom: 1px dotted #999; }
</style>
</head>

<body>
<h3>Renewals FS Pro 2013 Subscription</h3>
<form action="" method="post">
  <p>
  STEP 1: 
  <input type="submit" name="qhd002" value="Delete QHD002" />
  </p>
  <p>
  STEP 2: 
    <input type="submit" name="uniquekey" value="Extract unique keys" />
  </p>
  <p>
    STEP 3: 
      <input type="submit" name="check" value="Check keys without order" />
  </p>
  <p>STEP 4: 
    <input type="submit" name="update1y" value="Update 1 year" />
    <input type="submit" name="update2y" value="Update 2 year" />
    <input type="submit" name="update3y" value="Update 3 year" />
    <input type="submit" name="updateperp" value="Update Perpetual" />
  </p>
  <!--<p>
    STEP 5: 
      <input type="submit" name="upd_date" value="Update activation date" />
  </p>-->
  <p>
    STEP 5: 
      <input type="submit" name="upd_buyer" value="Update buyer" />
  </p>
  <p>
    STEP 6: 
      <input type="submit" name="upd_enduser" value="Update end user name and email" />
  </p>
</form>

  <?php
$db = mysql_connect("localhost","root","") or die ("The connection to the database could not be established. Please try again later or inform info@informationmapping.com");
mysql_set_charset('utf8',$db); // added to avoid problems in the SQL DB with e.g. Nestlé
mysql_select_db("imishop",$db) or die("db error");

// license file table must be uploaded in same DB as webstore
// moduleid = QHD001, de QHD002 is het serverproduct en niet relevant voor ons
// eerst check doen: alle license keys (distinct) vergelijken met orders en tonen van welke keys er geen order kan gevonden worden

$sql_license_files = "SELECT `entrykey` FROM sheet1";
$sql_keys = "SELECT DISTINCT `entrykey` FROM sheet1";
$sql_keys_qhd = "SELECT DISTINCT `entrykey` FROM sheet1 WHERE moduleid = 'QHD001'"; // wanneer DB te groot wordt, deze schifting al uitvoeren vooraleer DB wordt geïmporteerd

$amount_license_files = mysql_num_rows(mysql_query($sql_license_files));
$amount_sql_keys = mysql_num_rows(mysql_query($sql_keys));
$amount_sql_keys_qhd = mysql_num_rows(mysql_query($sql_keys_qhd));
echo "<p>There are $amount_sql_keys unique keys in $amount_license_files license files, of which $amount_sql_keys_qhd with QHD001.</p>";

ini_set('max_execution_time', 300); // highers up the default 30 seconds script executing time to 5 minutes, works on localhost, not tested on hosting environment

$unique_key_table="CREATE TABLE IF NOT EXISTS `AS_unique_key2013` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `uproduct_name` varchar(16) DEFAULT NULL,
  `ukey` varchar(36) DEFAULT NULL,
  `ucreated` date DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=MyISAM  DEFAULT CHARSET=utf8 AUTO_INCREMENT=1 ;";
mysql_query($unique_key_table) or die(mysql_error());

$qhd002 = isset($_POST['qhd002']) ? $_POST['qhd002'] : '';
if($qhd002)
{
	$result_delete = mysql_query("DELETE FROM sheet1 WHERE moduleid = 'QHD002'");
	echo "<p>Records QHD002 successfully deleted.</p>";
}

$uniquekey = isset($_POST['uniquekey']) ? $_POST['uniquekey'] : '';
// enkel unieke keys in nieuwe tabel plaatsen
// een license key kan meerdere license files hebben, bij elke update van de expiry date komt er een record bij
// dus niet enkel de unieke license key rows moeten getrokken worden, maar meteen ook die met de recentste expiry date
if($uniquekey)
{	
	//$query_distinct = "SELECT DISTINCT `entrykey` FROM sheet1"; // backquote's verplicht in DISTINCT query, anders werkt dit niet!!!
	// to be able to fetch at the same time the most recent date record (lvvalue), do not use DISTINCT but GROUP BY. distinct and group by return the same results
	$query_distinct = "SELECT entrykey, MAX( lvvalue ) AS lvvalue FROM sheet1 WHERE lvvalue IS NOT NULL GROUP BY entrykey"; // enkel de records met expiry date ingevuld
	$result_distinct = mysql_query($query_distinct);
	while($row = mysql_fetch_array($result_distinct))
	{
	 echo $row['entrykey'] . $row['lvvalue'] . "<br />";
	$query_create = "INSERT INTO AS_unique_key2013 (ukey, ucreated) VALUES ('" . $row['entrykey'] . "','" . $row['lvvalue'] . "')";
	$result_create = mysql_query($query_create);
	// creëer een tabel met unieke keys om hierin bij te houden welke mails zijn verstuurd voor welke key
	// deze tabel mag bij updates niet gewist worden!
	// mag slechts 1 keer aangemaakt worden, vandaar in commentaar hier beneden
	//$query_create2 = "INSERT INTO AS_cron (`key`) VALUES ('" . $row['entrykey'] . "')";
	//$result_create2 = mysql_query($query_create2);
	echo "<p>Unique keys with most recent expiry date has been added.</p>";	
	}
}
/*$upd_date = isset($_POST['upd_date']) ? $_POST['upd_date'] : '';
if($upd_date)
{
		$result_update_date = mysql_query("UPDATE AS_unique_key2013 AS un, sheet1 AS sh SET un.ucreated = sh.lvvalue WHERE sh.entrykey = un.ukey && sh.lvvalue IS NOT NULL");
		echo "<p>Dates added</p>";
}*/

$check = isset($_POST['check']) ? $_POST['check'] : '';
if($check)
{
	echo "<p style=color:red;>The following license keys have a License File but are not known in the web store:</p>";
	// toon alle license keys die in tbl unique_key2013 zitten, maar niet in de store zitten
	$result_check =	mysql_query("SELECT ukey FROM AS_unique_key2013 WHERE ukey NOT IN (SELECT serial_number FROM license_serialnumbers WHERE serial_number = ukey)");
	while($row_check=mysql_fetch_array($result_check)){
		
		echo $row_check['ukey'] . "<br />";
	}
}

$upd_buyer = isset($_POST['upd_buyer']) ? $_POST['upd_buyer'] : '';
if($upd_buyer)
{
	$column_buyer = mysql_query("ALTER TABLE `AS_unique_key2013` ADD `buyer` varchar(120) NOT NULL"); // moet met backquote's of werkt niet!
	echo "<p>Column date added</p>";
	$result_buyer = mysql_query("UPDATE AS_unique_key2013 AS u, license_serialnumbers AS l SET u.buyer = l.customer_id WHERE u.ukey = l.serial_number ");
	//$result_buyer = mysql_query("UPDATE AS_unique_key2013 AS u, license_info AS l SET u.buyer = l.email WHERE u.ukey = l.last_license_key ");
	echo "<p>Added buyer IDs.</p>";
	// lookup buyer
	$column_email = mysql_query("ALTER TABLE `AS_unique_key2013` ADD `buyer_email` varchar(120) NOT NULL"); // moet met backquote's of werkt niet!
	$column_firstname = mysql_query("ALTER TABLE `AS_unique_key2013` ADD `buyer_firstname` varchar(100) NOT NULL"); // moet met backquote's of werkt niet!
	$result_endcustomer = mysql_query("UPDATE AS_unique_key2013 AS u, customer_entity AS c
	SET u.buyer_email = c.email WHERE u.buyer = c.entity_id
	");
	echo "<p>Added email address</p>";
	$result_firstname = mysql_query("UPDATE AS_unique_key2013 AS u, customer_entity_varchar AS c
	SET u.buyer_firstname = c.value WHERE u.buyer = c.entity_id && c.attribute_id = '5'
	");
	echo "<p>Added first name</p>";
	//$result_buyer2 = mysql_query("UPDATE AS_unique_key2013 AS u, license_serialnumbers AS l SET u.buyer = l.customer_id WHERE u.ukey = l.serial_number ");
	//echo "<p>Added buyer email</p>";
}

$update1y = isset($_POST['update1y']) ? $_POST['update1y'] : '';
$update2y = isset($_POST['update2y']) ? $_POST['update2y'] : '';
$update3y = isset($_POST['update3y']) ? $_POST['update3y'] : '';
$updateperp = isset($_POST['updateperp']) ? $_POST['updateperp'] : '';

if($update1y)
{
	// read out product id's in tbl license_serialnumbers
	// this means when new Virtual Products with license keys attached to them, are released, I have to add these IDs to this script!!!!!!
	$result_product1 = mysql_query("UPDATE AS_unique_key2013 AS u, license_serialnumbers AS l SET u.uproduct_name = '1 year' 
	WHERE u.ukey = l.serial_number && l.product_id = '401' 
	OR (u.ukey = l.serial_number && l.product_id = '406')
	OR (u.ukey = l.serial_number && l.product_id = '389')
	OR (u.ukey = l.serial_number && l.product_id = '397')
	OR (u.ukey = l.serial_number && l.product_id = '418')
	OR (u.ukey = l.serial_number && l.product_id = '421')
	OR (u.ukey = l.serial_number && l.product_id = '410')
	");	
}
if($update2y)
{
	$result_product2 = mysql_query("UPDATE AS_unique_key2013 AS u, license_serialnumbers AS l SET u.uproduct_name = '2 year' 
	WHERE u.ukey = l.serial_number && l.product_id = '390' 
	OR (u.ukey = l.serial_number && l.product_id = '398')
	OR (u.ukey = l.serial_number && l.product_id = '419')
	OR (u.ukey = l.serial_number && l.product_id = '402')
	OR (u.ukey = l.serial_number && l.product_id = '422')
	");	
}
if($update3y)
{
	$result_product3 = mysql_query("UPDATE AS_unique_key2013 AS u, license_serialnumbers AS l SET u.uproduct_name = '3 year' 
	WHERE u.ukey = l.serial_number && l.product_id = '391' 
	OR (u.ukey = l.serial_number && l.product_id = '399')
	OR (u.ukey = l.serial_number && l.product_id = '420')
	OR (u.ukey = l.serial_number && l.product_id = '403')
	OR (u.ukey = l.serial_number && l.product_id = '423')
	");	
}
if($updateperp)
{
	$result_productp = mysql_query("UPDATE AS_unique_key2013 AS u, license_serialnumbers AS l SET u.uproduct_name = 'perpetual' 
	WHERE u.ukey = l.serial_number && l.product_id = '385' 
	OR (u.ukey = l.serial_number && l.product_id = '396')
	OR (u.ukey = l.serial_number && l.product_id = '405')
	OR (u.ukey = l.serial_number && l.product_id = '415')
	OR (u.ukey = l.serial_number && l.product_id = '414')
	");	
	echo "<p>Product updated</p>";
}
// idea: to limit the amount of rows, you could remove now all records which are perpetual

$upd_enduser = isset($_POST['upd_enduser']) ? $_POST['upd_enduser'] : '';
if($upd_enduser)
{
	$column_enduser_email = mysql_query("ALTER TABLE `AS_unique_key2013` ADD `enduser_email` varchar(120) NOT NULL"); // moet met backquote's of werkt niet!	
	$column_enduser_name = mysql_query("ALTER TABLE `AS_unique_key2013` ADD `enduser_name` varchar(120) NOT NULL"); // moet met backquote's of werkt niet!
	
	echo "<p>Columns end user added.</p>";
	$result_enduser_name = mysql_query("UPDATE AS_unique_key2013 AS u, AS_customerinfo AS l SET u.enduser_name = l.COL2 WHERE u.ukey = l.COL25 ");
	$result_enduser_email = mysql_query("UPDATE AS_unique_key2013 AS u, AS_customerinfo AS l SET u.enduser_email = l.COL16 WHERE u.ukey = l.COL25  ");
	echo "<p>Added end user name and email.</p>";
}

// cron job, maar niet sturen naar partners!!
?>

</body>
</html>