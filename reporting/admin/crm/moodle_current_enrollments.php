<?php
$rootdir="../../";
$page="Current enrollments";
include("../connect_db234.php");
include("../header.php");
?>

<body>
<?php
include("../topnav.php");
?>
<div class="container-fluid">
            <div class="row-fluid">
                
                <?php
				include("../sidenav.php");
				?>
                
                <div class="span10" id="content">
                    <div class="row-fluid">
                    	<div class="span12">
                        
                            <!-- block -->
                            <div class="block">
                                <div class="navbar navbar-inner block-header">
                                <h3>Current enrollments</h3>     
                                </div>
                                <div class="block-content collapse in">
<?php 
if (!isset($_SESSION["username"])){
	echo $niet_ingelogd;
}
// hieronder staan de functies die toegelaten worden tot deze pagina
elseif($_SESSION["function"] != "admin"){
	echo $geen_toegang;
}else{
?>
<?php

function replace_enrolid($nummerke)
{
	$sql_user = mysql_query("SELECT enr.id, enr.courseid, cou.id, cou.fullname FROM mdl_enrol AS enr, mdl_course AS cou WHERE enr.courseid = cou.id && enr.id = '$nummerke'", $db2);
	$report_user = mysql_fetch_assoc($sql_user);
	
	$out = $report_user['fullname'];
	return $out;
}
?>
<table class="table table-striped">
<tr><th>User</th><th>Email</th><th>First name</th><th>Course</th><th>End date enrollment</th></tr>
<?php

$current_date = date("Ymd");

$sql = mysql_query("SELECT * FROM mdl_user_enrolments ORDER BY timeend", $db2);
// in moodle, dates are stored as bigint in the mdl_user_enrolments table, so you cannot use built in SQL date functions
while($enr=mysql_fetch_array($sql)){
	
	$date = date("Ymd", $enr['timeend']); // convert unix timestamp in Moodle db to readable format
	$enrolid = $enr['enrolid'];
	
		// only display date als de datum hoger is dan vandaag 20130325
		if($date > $current_date) {
			echo "<tr><td> " . $enr['userid'] . "</td>";
			$id = $enr['userid'];
			$user = mysql_query("SELECT * FROM mdl_user WHERE id = '$id'", $db2);
			$partner=mysql_fetch_array($user);
			echo "<td>" . $partner['email'] . "</td>";
			echo "<td>" . $partner['firstname'] . "</td>";
			echo "<td>" . replace_enrolid($enrolid) . "</td>";
			
			
			echo "<td>" . date("Y F d", $enr['timeend']) . "</td></tr>";
		}
	
	
}

 ?>
</table>
<?php
}
?>
                                </div>
                            </div>
                            <!-- /block -->


</div>
</div>
</div>
</div>
            <hr>
<?php
include("../footer.php");
?>
</html>