<?php
$rootdir="../../";
$page="Admin: associate guest purchase";
include("../connect.php");
include("../header.php");
?>

<body>
<?php
include("../topnav.php");
?>
<div class="container-fluid">
            <div class="row-fluid">
                
                <?php
				include("../sidenav.php");
				?>
                
                <div class="span10" id="content">
                    <div class="row-fluid">
                    	<div class="span12">
                        
                            <!-- block -->
                            <div class="block">
                                <div class="navbar navbar-inner block-header">
                                <h3>Associate guest purchases manually</h3>     
                                </div>
                                <div class="block-content collapse in">
<?php 
if (!isset($_SESSION["username"])){
	echo $niet_ingelogd;
}
// hieronder staan de functies die toegelaten worden tot deze pagina
elseif($_SESSION["function"] != "admin"){
	echo $geen_toegang;
}else{
?>

<p><strong>Warning: this system is replaced by a cron job that runs every hour!</strong></p>
<p>Fill out the form below and click <strong>Save</strong> to assign a guest order to a registered customer account. <br />Warning: Changes cannot be undone.</p>

<form action="" method="post">
	Order ID: <input type="text" name="orderid" /> (NOT order number)
    <br />
    Customer ID:
<input type="text" name="customerid" />
    <br />
    Customer first name:
<input type="text" name="firstname" />
    <br />
    Customer last name:
<input type="text" name="lastname" />
    <br />
    <input type="submit" name="verzenden" value="Save" class="btn" />
</form>


<?php

// HIERONDER JUIST ZOEKSCRIPT VOOR MULTIPLE ZOEKWAARDEN

$orderid = isset($_POST['orderid']) ? $_POST['orderid'] : '';
$customerid = isset($_POST['customerid']) ? $_POST['customerid'] : '';
$firstname = isset($_POST['firstname']) ? $_POST['firstname'] : '';
$lastname = isset($_POST['lastname']) ? $_POST['lastname'] : '';
$verzenden = isset($_POST['verzenden']) ? $_POST['verzenden'] : '';

if($verzenden)
{
	if(empty($orderid))
     $error .= "<li>You don't have entered an order ID.</li>\n";

	if(empty($customerid))
     $error .= "<li>You don't have entered a customer ID.</li>\n";	 

	if(empty($firstname))
     $error .= "<li>You don't have entered a first name.</li>\n";

	if(empty($lastname))
     $error .= "<li>You don't have entered a last name.</li>\n";

	if(isset($error))    
   {    
      // Afdrukken van de errors  
      echo "<p><strong>Your action was not successful:</strong></p>\n";   
      echo "<ul>"; 
      echo $error;    
      echo "</ul>\n"; 

    }
	else
	{
	
	  $result = mysql_query("UPDATE sales_flat_order SET customer_id = '$customerid', customer_is_guest = '0', customer_group_id = '1', customer_firstname = '$firstname', customer_lastname = '$lastname' WHERE entity_id = '$orderid'");
	  if($result) { echo "<p>Saved query 1 successfully</p>"; } else { echo "<p>Error: Query 1 not saved successfully</p>";  }
	  
	  $result2 = mysql_query("UPDATE sales_flat_order_address SET customer_id = '$customerid' WHERE parent_id = '$orderid'");
	  if($result2) { echo "<p>Saved query 2 successfully</p>"; } else { echo "<p>Error: Query 2 not saved successfully</p>";  }
	  
	  $result3 = mysql_query("UPDATE sales_flat_order_grid SET customer_id = '$customerid' WHERE entity_id = '$orderid'");
	  if($result3) { echo "<p>Saved query 3 successfully</p>"; } else { echo "<p>Error: Query 3 not saved successfully</p>";  }
	}
}
?>
<?php
}
?>

                                </div>
                            </div>
                            <!-- /block -->


</div>
</div>
</div>
</div>
            <hr>
<?php
include("../footer.php");
?>
</html>