<?php
$rootdir="../../";
$page="Customer details";
include("../connect.php");
include("../connect_db234.php");
include("../header.php");
?>

<body>
<?php
include("../topnav.php");
?>
<div class="container-fluid">
<div class="row-fluid">

<?php
include("../sidenav.php");
?>

<div class="span10" id="content">
<div class="row-fluid">
<div class="span12">

<!-- block -->
<div class="block">
<div class="navbar navbar-inner block-header">
    <h3>Customer details</h3>
</div>
<div class="block-content collapse in">

<?php
//echo $_SESSION['username'] . $_SESSION['function'];
if (!isset($_SESSION["username"])){
    echo $niet_ingelogd;
}

// hieronder staan de functies die toegelaten worden tot deze pagina
elseif($_SESSION["function"] != "admin" && $_SESSION["function"] != "it" && $_SESSION["function"] != "marketing"){ // make it accessible with function admin and it
    echo $geen_toegang;
}else{

    ?>




    <?php

    $entity_email = $_GET['entity_email'];
    echo "<p><strong>User: </strong>" . $entity_email;
    $r_imcc = mysql_query("SELECT * FROM resume AS r, resumefeedback AS f
WHERE 
r.email = '$entity_email' &&
r.program = 'Information Mapping Certified Consultant Program' && r.resume_id = f.resume_id && (f.exam_passed = '1' OR f.exam_passed = '3')", $db);
    $row_imcc = mysql_fetch_assoc($r_imcc);
    if(!empty($row_imcc))
    {
        echo "<strong> (Information Mapping Certified Consultant)</strong>";
    }
    echo "</p>";

    $userid = mysql_query("SELECT * FROM customer_entity WHERE email = '$entity_email'", $db);
    $userid_row = mysql_fetch_array($userid);
    $magento_userid = $userid_row['entity_id']; // Magento ID

    echo "<p>Magento ID: ";
    if(empty($magento_userid)) { $magento_userid = "none"; echo $magento_userid; } else { echo $magento_userid; }

    $userid_academy = mysql_query("SELECT * FROM mdl_user WHERE email = '$entity_email'", $db2);
    $userid_row_academy = mysql_fetch_array($userid_academy);
    $academy_userid = $userid_row_academy['id'];

    echo " | Academy ID: ";
    if(empty($academy_userid)) { $academy_userid = "none"; echo $academy_userid; } else { echo $academy_userid; }

    $userid_joomla = mysql_query("SELECT * FROM imi_users WHERE email = '$entity_email'", $db3);
    $userid_row_joomla = mysql_fetch_array($userid_joomla);
    $joomla_userid = $userid_row_joomla['id'];

    echo " | Joomla ID: ";
    if(empty($joomla_userid)) { $joomla_userid = "none"; echo $joomla_userid; } else { echo $joomla_userid; }
    echo "</p>";

    ?>

    <table class="table table-striped">
        <tr><th>Orders</th><th>E-learning enrollments</th><th>Activated FS Pro <br />(results not live)</th><th>Certificate of Attendance</th><th>IMP Exam</th></tr>
        <tr>
            <td valign="top">
                <?php
                // enkel uitvoeren als er een Magento id gevonden is!
                if($magento_userid != "none") {
                    $orders = mysql_query("SELECT entity_id, increment_id, DATE_FORMAT(created_at, '%d/%m/%Y') AS newDate, customer_id, subtotal, status FROM sales_flat_order WHERE customer_id = '$magento_userid'", $db);
                }
                $orders_aantal = mysql_num_rows($orders);

                if($orders_aantal > 0)
                {
                    while($row=mysql_fetch_array($orders)){
                        echo "#" . $row['increment_id'] . " | " . $row['newDate'] . " (" . $row['status'] . ")<br />";
                        // detail van order uitlezen
                        $ord_id = $row['entity_id'];
                        //echo $ord_id;
                        $res_detail = mysql_query("SELECT * FROM sales_flat_order_item WHERE order_id = '$ord_id' && product_type = 'bundle'", $db);
                        echo "<ul>";
                        while($row_detail = mysql_fetch_array($res_detail))
                        {

                            echo "<li>" . $row_detail['name'] . "</li>";

                        }
                        echo "</ul>";
                    }
                }
                else { echo "No orders placed"; }
                ?>
            </td>
            <td valign="top">
                <?php



                // enkel uitvoeren als er een Moodle id gevonden is!
                if($academy_userid != "none") {
                    $enrollments = mysql_query("SELECT * FROM mdl_user_enrolments WHERE userid = '$academy_userid'", $db2);
                }
                $enrollments_aantal = mysql_num_rows($enrollments);

                if($enrollments_aantal > 0)
                {

                    while($row2=mysql_fetch_array($enrollments)){

                        $enrolid = $row2['enrolid'];

                        // enrolid must be read in mdl_enrol to get the courseid
                        $sqlsql = mysql_query("SELECT id,courseid FROM mdl_enrol WHERE id ='$enrolid'", $db2);
                        $rowsql = mysql_fetch_array($sqlsql);
                        // end mdl_enrol, nu nog course name uitlezen (geldt ook voor quiz)
                        $tooncursus = $rowsql['courseid'];

                        $sql_course = mysql_query("SELECT * FROM mdl_course WHERE id = '$tooncursus'", $db2);
                        $report_course = mysql_fetch_assoc($sql_course);

                        $out = $report_course['fullname'];

                        echo $out;
                        echo " | ";
                        if($row2['timestart'] == "0") { echo "no start date"; } else { echo date('d F Y', $row2['timestart']); }
                        echo " until ";
                        if($row2['timeend'] == "0") { echo "no end date"; } else { echo date('d F Y', $row2['timeend']); }
                        echo ")<br />";
                    }

                    echo "<p><strong>Results: </strong></p>";

                    $result_grades = mysql_query("SELECT gg.itemid, gg.userid, gg.rawgrade, gg.rawgrademax, gg.timemodified, gi.id, gi.itemname FROM mdl_grade_grades AS gg, mdl_grade_items AS gi WHERE gg.itemid = gi.id && gg.userid = '$academy_userid' && gg.rawgrade IS NOT NULL", $db2);
                    while($row_grades=mysql_fetch_array($result_grades))
                    {
                        echo $row_grades['itemname'] . ": ";
                        echo round($row_grades['rawgrade'],2) . " / " . round($row_grades['rawgrademax'],0);
                        echo " (" . date('d F Y', $row_grades['timemodified']) . ")<br />";
                    }

                }
                else { echo "No enrollments found"; }
                ?>
                <p><strong>Last access: </strong>
                    <?php

                    $acc = mysql_query("SELECT from_unixtime(timeaccess, '%Y %D %M %h:%i:%s') AS conv_lastaccess FROM mdl_user_lastaccess WHERE userid = '$academy_userid'", $db2);
                    $acc_academy = mysql_fetch_assoc($acc);
                    $academy_timeaccess = $acc_academy['conv_lastaccess'];
                    echo $academy_timeaccess;
                    ?>
                </p>
            </td>
            <td valign="top">
                <?php
                // read FS Pro 2013 keys
                //$res_lic = mysql_query("SELECT * FROM as_unique_key2013 WHERE enduser_email = '$entity_email'", $db);
                $res_lic = mysql_query("SELECT DISTINCT lickey FROM as_unique_key2013 WHERE enduser_email = '$entity_email'", $db);
                $aantal_lic = mysql_num_rows($res_lic);
                echo "<strong>FS Pro 2013: </strong><br />" . $aantal_lic . " license keys<br />";
                if($aantal_lic > 0)
                {
                    while($row = mysql_fetch_array($res_lic))
                    {
                        echo $row['lickey'] . "<br />";
                    }
                }
                else
                {
                    //echo "No";
                }

                // read FS Pro 4.3 or older
                $res_lic4 = mysql_query("SELECT * FROM reporting_nalpeiron WHERE EmailAddress = '$entity_email'", $db);
                $aantal_lic4 = mysql_num_rows($res_lic4);
                echo "<strong>Other FS Pro versions:</strong> <br />" . $aantal_lic4 . " license keys<br />";
                if($aantal_lic4 > 0)
                {
                    while($row4 = mysql_fetch_array($res_lic4))
                    {
                        echo $row4['LicenseCode'] . "<br />";
                    }
                }
                else
                {
                    //echo "No";
                }
                ?>

            </td>
            <td valign="top">
                <?php
                // enkel uitvoeren als er een Joomla id gevonden is!
                if($joomla_userid != "none") {
                    $certificate = mysql_query("SELECT * FROM imi_course_certificates WHERE user_id = '$joomla_userid'", $db3); // user_id kan uit imi_users gehaald worden op basis van email
                }
                $certificate_aantal = mysql_num_rows($certificate);

                if($certificate_aantal > 0)
                {
                    while($row=mysql_fetch_array($certificate)){
                        echo $row['first_name'] . " " . $row['last_name'] . " | " . $row['date_from'] . " | <a href=\"http://www.informationmapping.com/coursecertificate/" . $row['filename'] . "\" target=\"_blank\">Download</a><br />";
                    }
                }
                else { echo "No Certificate of Attendance generated"; }
                ?>
            </td>
            <td valign="top">
                <?php
                // enkel uitvoeren als er een Moodle id gevonden is!
                if($academy_userid != "none") {

                    $imp = mysql_query("SELECT *
FROM mdl_quiz_grades WHERE userid = '$academy_userid' && quiz = '1' 
OR (userid = '$academy_userid' && quiz = '16') 

", $db2);

                    $imp_aantal = mysql_num_rows($imp);

                    if($imp_aantal > 0)
                    {
                        while($row3=mysql_fetch_array($imp)){

                            echo round($row3['grade'],2) . " / 20 (" . date('d F Y H:i:s', $row3['timemodified']) . ")<br />";
                        }
                    }
                    else { echo "No grades found for IMP Exam"; }


                }
                ?>
            </td>
        </tr>
    </table>
<?php
}
?>

</div>
</div>
<!-- /block -->


</div>
</div>
</div>
</div>
<hr>
<?php
include("../footer.php");
?>
</html>