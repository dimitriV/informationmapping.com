<?php
$rootdir="../../";
$page="FS Pro live demo";
include("../connect.php");
include("../header.php");
?>

<body>
<?php
include("../topnav.php");
?>
<div class="container-fluid">
            <div class="row-fluid">
                
                <?php
				include("../sidenav.php");
				?>
                
                <div class="span10" id="content">
                    <div class="row-fluid">
                    	<div class="span12">
                        
                            <!-- block -->
                            <div class="block">
                                <div class="navbar navbar-inner block-header">
                                     
                                </div>
                                <div class="block-content collapse in">
<?php 
if (!isset($_SESSION["username"])){
	echo $niet_ingelogd;
}
// hieronder staan de functies die toegelaten worden tot deze pagina
elseif($_SESSION["function"] != "admin" && $_SESSION["function"] != "marketing"){ // make it accessible with function admin and it
	echo $geen_toegang;
}else{
?>
<h3>FS Pro live demo: edit prospect</h3>
<?php
/*************************************************
        FORMULIERVARIABELEN BEVEILIGEN
*************************************************/

// zet gevaarlijke tekens in invoervelden om
function cleanup($string="")
{
$string = trim($string); // verwijdert spaties aan begin en einde
//$string = htmlentities($string); // converteert < > & " en andere speciale tekens naar veilige tekens
$string = htmlspecialchars($string);
//$string = nl2br($string); // converteert linebreaks in textareas naar <br />
// plaatst escapetekens bij quotes indien nodig
if(!get_magic_quotes_gpc())
  {
  $string = addslashes($string);
  }

return $string;
}

$id = isset($_GET['id']) ? cleanup($_GET['id']) : '';
$firstname = isset($_POST['firstname']) ? cleanup($_POST['firstname']) : '';
$lastname = isset($_POST['lastname']) ? cleanup($_POST['lastname']) : '';
$email = isset($_POST['email']) ? cleanup($_POST['email']) : '';
$phone = isset($_POST['phone']) ? cleanup($_POST['phone']) : '';
$country = isset($_POST['country']) ? cleanup($_POST['country']) : '';
$comment = isset($_POST['comment']) ? cleanup($_POST['comment']) : '';
$status = isset($_POST['status']) ? cleanup($_POST['status']) : '';
$job = isset($_POST['job']) ? cleanup($_POST['job']) : '';
$verzenden = isset($_POST['verzenden']) ? cleanup($_POST['verzenden']) : '';

if($verzenden) {

		if(empty($firstname) || strlen($firstname) > 100)
		{
			$error_firstname = "<div class=\"alert alert-danger\">First name not filled in.</div>";
		}

		/*if(empty($job) || strlen($job) > 150)
		{
			$error_job = "<div class=\"alert alert-danger\">Job title not filled in.</div>";
		}*/		
		if(empty($lastname) || strlen($lastname) > 100)
		{
			$error_lastname = "<div class=\"alert alert-danger\">Last name not filled in.</div>";
		}
		
		if(empty($email) || strlen($email) > 150)
		{
			$error_email = "<div class=\"alert alert-danger\">E-mail address not filled in.</div>";
		}
		
		if(empty($phone) || strlen($phone) > 150)
		{
			$error_phone = "<div class=\"alert alert-danger\">Phone not filled in.</div>";
		}

		if(empty($country) || strlen($country) > 150)
		{
			$error_country = "<div class=\"alert alert-danger\">Country not filled in.</div>";
		}
		
		if(empty($error_firstname) && empty($error_lastname) && empty($error_email) && empty($error_phone) && empty($error_country) && empty($error_job))
		{
			
		$form_sent = "1";
		echo "<p>Update successful.</p><p><a href=\"fspro_live_demo.php\">Go back to the overview</a></p>";	
		// record updaten in app
		$result = mysql_query("UPDATE reporting_live_demo SET 
		firstname='$firstname', 
		lastname='$lastname', 
		email='$email', 
		phone='$phone', 
		country='$country', 
		status='$status', 
		job = '$job',
		comment='$comment'
		WHERE id = '$id'");
		}

} // einde if verzenden

$result = mysql_query("SELECT * FROM reporting_live_demo WHERE id = '$id'");
$aantal = mysql_num_rows($result); 


?>
<?php
$form_sent = isset($form_sent);
if($form_sent != 1)
{ ?>
<form action="#" method="post">
<?php


while($report=mysql_fetch_assoc($result))
{
?>	
<p>Prospect added on <?php echo $report['date_insert']; ?> </p>
First name: <input type="text" name="firstname" value="<?php echo $report['firstname']; ?>" /><?php if($error_firstname && isset($_POST['verzenden'])) { echo $error_firstname; } ?><br />
Last name: <input type="text" name="lastname" value="<?php echo $report['lastname']; ?>" /><?php if($error_lastname && isset($_POST['verzenden'])) { echo $error_lastname; } ?><br />
Email: <input type="text" name="email" value="<?php echo $report['email']; ?>" /><?php if($error_email && isset($_POST['verzenden'])) { echo $error_email; } ?><br />
Job title: <input type="text" name="job" value="<?php echo $report['job']; ?>" /><?php if($error_job && isset($_POST['verzenden'])) { echo $error_job; } ?><br />
Phone: <input type="text" name="phone" value="<?php echo $report['phone']; ?>" /><?php if($error_phone && isset($_POST['verzenden'])) { echo $error_phone; } ?><br />
Country: <input type="text" name="country" value="<?php echo $report['country']; ?>" /><?php if($error_country && isset($_POST['verzenden'])) { echo $error_country; } ?><br />
Status: 
<select name="status">
<option value="New"<?php if($report['status'] == "New") { echo " selected"; } ?>>New</option>
<option value="In progress"<?php if($report['status'] == "In progress") { echo " selected"; } ?>>In progress</option>
<option value="Appointment"<?php if($report['status'] == "Appointment") { echo " selected"; } ?>>Appointment</option>
<option value="Demo"<?php if($report['status'] == "Demo") { echo " selected"; } ?>>Demo</option>
<option value="Won"<?php if($report['status'] == "Won") { echo " selected"; } ?>>Won</option>
<option value="Lost"<?php if($report['status'] == "Lost") { echo " selected"; } ?>>Lost</option>
</select><br />
Code: <input type="text" name="code" value="<?php echo $report['code']; ?>" disabled /><br />
<script type="text/javascript">
/*function insert_date()
{
var textArea = document.getElementById("mytextarea"); // assuming there is a textarea with id = mytextarea
var textToAppend = document.createTextNode("<?php echo date("l, j F Y") . ": ";  ?>");
textArea.appendChild(textToAppend);	
}*/
function getInputSelection(el) {
    var start = 0, end = 0, normalizedValue, range,
        textInputRange, len, endRange;

    if (typeof el.selectionStart == "number" && typeof el.selectionEnd == "number") {
        start = el.selectionStart;
        end = el.selectionEnd;
    } else {
        range = document.selection.createRange();

        if (range && range.parentElement() == el) {
            len = el.value.length;
            normalizedValue = el.value.replace(/\r\n/g, "\n");

            // Create a working TextRange that lives only in the input
            textInputRange = el.createTextRange();
            textInputRange.moveToBookmark(range.getBookmark());

            // Check if the start and end of the selection are at the very end
            // of the input, since moveStart/moveEnd doesn't return what we want
            // in those cases
            endRange = el.createTextRange();
            endRange.collapse(false);

            if (textInputRange.compareEndPoints("StartToEnd", endRange) > -1) {
                start = end = len;
            } else {
                start = -textInputRange.moveStart("character", -len);
                start += normalizedValue.slice(0, start).split("\n").length - 1;

                if (textInputRange.compareEndPoints("EndToEnd", endRange) > -1) {
                    end = len;
                } else {
                    end = -textInputRange.moveEnd("character", -len);
                    end += normalizedValue.slice(0, end).split("\n").length - 1;
                }
            }
        }
    }

    return {
        start: start,
        end: end
    };
}

function offsetToRangeCharacterMove(el, offset) {
    return offset - (el.value.slice(0, offset).split("\r\n").length - 1);
}

function setSelection(el, start, end) {
    if (typeof el.selectionStart == "number" && typeof el.selectionEnd == "number") {
        el.selectionStart = start;
        el.selectionEnd = end;
    } else if (typeof el.createTextRange != "undefined") {
        var range = el.createTextRange();
        var startCharMove = offsetToRangeCharacterMove(el, start);
        range.collapse(true);
        if (start == end) {
            range.move("character", startCharMove);
        } else {
            range.moveEnd("character", offsetToRangeCharacterMove(el, end));
            range.moveStart("character", startCharMove);
        }
        range.select();
    }
}
function insertTextAtCaret(el, text) {
    var pos = getInputSelection(el).end;
    var newPos = pos + text.length;
    var val = el.value;
    el.value = val.slice(0, pos) + text + val.slice(pos);
    setSelection(el, newPos, newPos);
}

//document.getElementById("insertButton").onclick = function() {
	function insertb() {
    var textarea = document.getElementById("ta");
    textarea.focus();
    insertTextAtCaret(textarea, "<?php echo date("l, j F Y") . ": ";  ?>");
    return false;
};


</script>
Comments: <br /><input type="button" id="insertButton" value="Insert current date" onClick="insertb()"><br /><br />
<textarea rows="10" name="comment" style="width: 500px;" id="ta">
<?php echo $report['comment']; ?>
</textarea><br />
<?php if($report['direct'] == '1') { echo "<input type=\"checkbox\" checked /> Added to list by IMI<br /><br />"; } ?>




<?php	
}
?>
<input type="submit" name="verzenden" />
</form>
<?php } // einde $form_sent ?>
<?php
}
?>

                                </div>
                            </div>
                            <!-- /block -->


</div>
</div>
</div>
</div>
            <hr>
<?php
include("../footer.php");
?>
</html>