<?php
$rootdir="../../";
$page="Customers: buyers per product";
include("../connect.php");
include("../header.php");
?>

<body>
<?php
include("../topnav.php");
?>
<div class="container-fluid">
            <div class="row-fluid">
                
                <?php
				include("../sidenav.php");
				?>
                
                <div class="span10" id="content">
                    <div class="row-fluid">
                    	<div class="span12">
                        
                            <!-- block -->
                            <div class="block">
                                <div class="navbar navbar-inner block-header">
                                <h3>Customers: buyer per product</h3>     
                                </div>
                                <div class="block-content collapse in">
<?php 
if (!isset($_SESSION["username"])){
	echo $niet_ingelogd;
}
// hieronder staan de functies die toegelaten worden tot deze pagina
elseif($_SESSION["function"] != "admin"){
	echo $geen_toegang;
}else{
?>
<form action="" method="post">
<select name="product">
<?php

// read only the virtual products
$result_all = mysql_query("SELECT entity_id, sku FROM catalog_product_entity WHERE type_id = 'virtual' ORDER BY entity_id ASC",$db); 
while($row1 = mysql_fetch_array($result_all))
{
	echo "<option value=\"" . $row1['entity_id'] . "\"";
	if(($_POST['product'])== $row1['entity_id']) { echo " selected='selected'";}
	echo ">" . $row1['sku'] . "</option>\n";	
}
?>
</select>
 Buyer: <input type="text" name="buyer" />
<input type="submit" name="verzenden" value="Search" class="btn" />
</form>
<p><a href="customer_buyer_fspro.php">Report for all FS Pro 2013 purchases</a></p>

<table class="table table-striped">
<tr><th>Order ID</th><th>E-mail</th><th>First name</th><th>Last name</th><th>Partner</th><th>Country</th><th>Created at</th><th>Product name</th><th>Qty</th></tr>
  <?php

function replace_userid($nummerke)
{
	$sql_user = mysql_query("SELECT * FROM customer_entity WHERE entity_id = '$nummerke'");
	$report_user = mysql_fetch_assoc($sql_user);
	
	$out = $report_user['email'];
	return $out;
}
function replace_firstname($nummerke)
{
	$sql_user = mysql_query("SELECT * FROM customer_entity_varchar WHERE entity_id = '$nummerke' && attribute_id = '5'");
	$report_user = mysql_fetch_assoc($sql_user);
	
	$out = $report_user['value'];
	return $out;
}

function replace_lastname($nummerke)
{
	$sql_user = mysql_query("SELECT * FROM customer_entity_varchar WHERE entity_id = '$nummerke' && attribute_id = '7'");
	$report_user = mysql_fetch_assoc($sql_user);
	
	$out = $report_user['value'];
	return $out;
}

function replace_country($nummerke)
{
	$sql_user = mysql_query("SELECT * FROM sales_flat_order_address WHERE parent_id = '$nummerke'");
	$report_user = mysql_fetch_assoc($sql_user);
	
	$out = $report_user['country_id'];
	return $out;
}

function replace_partner($nummerke)
{
	$sql_user = mysql_query("SELECT * FROM customer_entity_int WHERE entity_id = '$nummerke' && attribute_id = '545'");
	$report_user = mysql_fetch_assoc($sql_user);
	
	$sql_user_partner = mysql_query("SELECT * FROM partner WHERE partner_id = '" . $report_user['value'] . "'");
	$report_user_partner = mysql_fetch_assoc($sql_user_partner);
	
	$out = $report_user_partner['name'];
	return $out;
}

if($_POST['verzenden'])
{
	$product_id = $_POST['product'];
	$buyer = $_POST['buyer'];
}
else
{
	$product_id = '0';
}

if(!empty($buyer))
{
$result = mysql_query("SELECT it.order_id,ord.entity_id,ord.status,ord.customer_email,it.product_id,it.created_at,it.name,it.qty_ordered 
FROM sales_flat_order_item AS it, sales_flat_order AS ord 
WHERE it.order_id = ord.entity_id && ord.status = 'complete' && it.product_id = '$product_id' && ord.customer_email LIKE '%$buyer%' ORDER BY it.qty_ordered DESC");	
}
else
{
$result = mysql_query("SELECT it.order_id,ord.entity_id,ord.status,it.product_id,it.created_at,it.name,it.qty_ordered 
FROM sales_flat_order_item AS it, sales_flat_order AS ord 
WHERE it.order_id = ord.entity_id && ord.status = 'complete' && it.product_id = '$product_id' ORDER BY it.qty_ordered DESC");
}

$aantal = mysql_num_rows($result); 
echo "<p>Total order lines: " . $aantal . "</p>";

$totaal_qty_1y=0;
while($report=mysql_fetch_array($result)){
	
	$result_order = mysql_query("SELECT * FROM sales_flat_order WHERE entity_id = '" . $report['order_id'] . "'");
	$report_order = mysql_fetch_assoc($result_order);
	$order_number = $report_order['increment_id'];
	$user_id = $report_order['customer_id'];
	
	echo "<tr>";
	//echo "<td>" . $report['order_id'] . "</td>";
	echo "<td>" . $order_number . "</td>";
	echo "<td>" . replace_userid($user_id) . "</td>";
	echo "<td>" . replace_firstname($user_id) . "</td>"; 
	echo "<td>" . replace_lastname($user_id) . "</td>"; 
	echo "<td>" . replace_partner($user_id) . "</td>"; 
	echo "<td>" . replace_country($report['order_id']) . "</td>"; 
	echo "<td>" . $report['created_at'] . "</td>";
	echo "<td>" . $report['name'] . "</td>";
	echo "<td>" . round($report['qty_ordered'],0) . "</td>";
	$totaal_qty_1y+=$report['qty_ordered'];
	echo "</tr>";
	
}
echo "<tr><td></td><td></td><td></td><td></td><td></td><td></td><td></td><td></td><td>" . $totaal_qty_1y . "</td></tr>";
?>
</table>
</table>
<?php
}
?>

                                </div>
                            </div>
                            <!-- /block -->


</div>
</div>
</div>
</div>
            <hr>
<?php
include("../footer.php");
?>
</html>