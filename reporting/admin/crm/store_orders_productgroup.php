<?php
$rootdir="../../";
$page="Ordered Per Product Group";
include("../connect.php");
include("../header.php");
?>

<body>
<?php
include("../topnav.php");
?>
<div class="container-fluid">
            <div class="row-fluid">
                
                <?php
				include("../sidenav.php");
				?>
                
                <div class="span10" id="content">
                    <div class="row-fluid">
                    	<div class="span12">
                        
                            <!-- block -->
                            <div class="block">
                                <div class="navbar navbar-inner block-header">
                                <h3>Store orders per product group</h3>     
                                </div>
                                <div class="block-content collapse in">
<?php 
if (!isset($_SESSION["username"])){
	echo $niet_ingelogd;
}
 //hieronder staan de functies die toegelaten worden tot deze pagina
elseif($_SESSION["function"] != "admin"){
	echo $geen_toegang;
}else{
?>

<?php

function replace_number($nummerke)
{
                /*$sql_user = mysql_query("SELECT * FROM customer_entity WHERE entity_id = '$nummerke'");
                $report_user = mysql_fetch_assoc($sql_user);
                
                $email = $report_user['email'];
                return $email;*/
				$format_number = number_format($nummerke, 2, ',', '.');
				return $format_number;
}


$verzenden = isset($_POST['verzenden']) ? $_POST['verzenden'] : '';

if($verzenden)
{

	$year = isset($_POST['year']) ? $_POST['year'] : '';
	$store = isset($_POST['store']) ? $_POST['store'] : '';
	/*if($store== "17") { $currency = " USD"; }
	if($store== "5") { $currency = " USD"; }
	if($store== "1") { $currency = " EUR"; }
	if($store== "16") { $currency = " EUR"; }*/
	$currency = "";
}
else
{
	$year = date("Y");
	$store = "5"; // US store default
	//$currency = " USD";
	$currency = "";
}
?>

<form action="" method="post">
<select name="year">
<option value="2010" <?php if($year== "2010") { echo "selected='selected'";} ?>>2010</option>
<option value="2011" <?php if($year== "2011") { echo "selected='selected'";} ?>>2011</option>
<option value="2012" <?php if($year== "2012") { echo "selected='selected'";} ?>>2012</option>
<option value="2013" <?php if($year== "2013") { echo "selected='selected'";} ?>>2013</option>
<option value="2014" <?php if($year== "2014") { echo "selected='selected'";} ?>>2014</option>
<option value="2015" <?php if($year== "2015") { echo "selected='selected'";} ?>>2015</option>
<option value="2016" <?php if($year== "2016") { echo "selected='selected'";} ?>>2016</option>
</select>

<select name="store">
<option value="17" <?php if($store== "17") { echo "selected='selected'";} ?>>Partner Store US</option>
<option value="5" <?php if($store== "5") { echo "selected='selected'";} ?>>Public Store US</option>
<option value="1" <?php if($store== "1") { echo "selected='selected'";} ?>>Public Store EUR</option>
<option value="16" <?php if($store== "16") { echo "selected='selected'";} ?>>Partner Store EUR</option>
</select>

<input type="submit" name="verzenden" value="Search" class="btn" />
</form>


<table width="100%" border="0" class="table table-striped">
  <tr>
    <td>&nbsp;</td>
    <th style="text-align: right;">E-LEARNING</th>
    <th style="text-align: right;">SOFTWARE</th>
    <th style="text-align: right;">TRAINING MATERIAL &amp; OTHER</th>
  </tr>
  <tr>
    <th>Jan</th>
    <td style="text-align: right;">
    <?php
	$month = "1";
	// calculate month total


if($month == "1") { $start_date = ($year-1)."-12-31 23:00:00"; $end_date = $year."-01-31 23:59:59"; }
	if($month == "2") { $start_date = $year."-01-31 23:00:00"; $end_date = $year."-02-28 23:59:59"; }
	if($month == "3") { $start_date = $year."-02-31 23:00:00"; $end_date = $year."-03-31 23:59:59"; }
	if($month == "4") { $start_date = $year."-03-31 23:00:00"; $end_date = $year."-04-31 23:59:59"; }
	if($month == "5") { $start_date = $year."-04-31 23:00:00"; $end_date = $year."-05-31 23:59:59"; }
	if($month == "6") { $start_date = $year."-05-31 23:00:00"; $end_date = $year."-06-31 23:59:59"; }
	if($month == "7") { $start_date = $year."-06-31 23:00:00"; $end_date = $year."-07-31 23:59:59"; }
	if($month == "8") { $start_date = $year."-07-31 23:00:00"; $end_date = $year."-08-31 23:59:59"; }
	if($month == "9") { $start_date = $year."-08-31 23:00:00"; $end_date = $year."-09-31 23:59:59"; }
	if($month == "10") { $start_date = $year."-09-31 23:00:00"; $end_date = $year."-10-31 23:59:59"; }
	if($month == "11") { $start_date = $year."-10-31 23:00:00"; $end_date = $year."-11-31 23:59:59"; }
	if($month == "12") { $start_date = $year."-11-31 23:00:00"; $end_date = $year."-12-31 23:59:59"; }
// calculate elearning group
$result_url = mysql_query("SELECT o.entity_id, o.store_id, o.status, DATE_FORMAT(o.created_at, '%d/%m/%Y') AS newDate, i.order_id, i.product_type, i.product_id, SUM(i.row_total) AS som
	FROM sales_flat_order AS o, sales_flat_order_item AS i, catalog_product_entity_int AS c
WHERE 
o.status = 'complete' && o.created_at > '$start_date' && o.created_at < '$end_date'
&& o.store_id = '$store'
&& o.entity_id = i.order_id
&& i.product_type = 'bundle'
&& i.product_id = c.entity_id
&& c.attribute_id = '579'
&& c.value = '84'
");
$row = mysql_fetch_assoc($result_url);
echo replace_number($row['som']);
echo $currency;
	?> 
    </td>
    <td style="text-align: right;">
   <?php
    // calculate software group


	if($month == "1") { $start_date = ($year-1)."-12-31 23:00:00"; $end_date = $year."-01-31 23:59:59"; }
	if($month == "2") { $start_date = $year."-01-31 23:00:00"; $end_date = $year."-02-28 23:59:59"; }
	if($month == "3") { $start_date = $year."-02-31 23:00:00"; $end_date = $year."-03-31 23:59:59"; }
	if($month == "4") { $start_date = $year."-03-31 23:00:00"; $end_date = $year."-04-31 23:59:59"; }
	if($month == "5") { $start_date = $year."-04-31 23:00:00"; $end_date = $year."-05-31 23:59:59"; }
	if($month == "6") { $start_date = $year."-05-31 23:00:00"; $end_date = $year."-06-31 23:59:59"; }
	if($month == "7") { $start_date = $year."-06-31 23:00:00"; $end_date = $year."-07-31 23:59:59"; }
	if($month == "8") { $start_date = $year."-07-31 23:00:00"; $end_date = $year."-08-31 23:59:59"; }
	if($month == "9") { $start_date = $year."-08-31 23:00:00"; $end_date = $year."-09-31 23:59:59"; }
	if($month == "10") { $start_date = $year."-09-31 23:00:00"; $end_date = $year."-10-31 23:59:59"; }
	if($month == "11") { $start_date = $year."-10-31 23:00:00"; $end_date = $year."-11-31 23:59:59"; }
	if($month == "12") { $start_date = $year."-11-31 23:00:00"; $end_date = $year."-12-31 23:59:59"; }


$result_url = mysql_query("SELECT o.entity_id, o.store_id, o.status, DATE_FORMAT(o.created_at, '%d/%m/%Y') AS newDate, i.order_id, i.product_type, i.product_id, SUM(i.row_total) AS som
	FROM sales_flat_order AS o, sales_flat_order_item AS i, catalog_product_entity_int AS c
WHERE 
o.status = 'complete' && o.created_at > '$start_date' && o.created_at < '$end_date'
&& o.store_id = '$store'
&& o.entity_id = i.order_id
&& i.product_type = 'bundle'
&& i.product_id = c.entity_id
&& c.attribute_id = '579'
&& c.value = '83'
");
$row = mysql_fetch_assoc($result_url);
echo replace_number($row['som']);
echo $currency;
?> 
    </td>
    <td style="text-align: right;">
    <?php
    // calculate training & other group

	if($month == "1") { $start_date = ($year-1)."-12-31 23:00:00"; $end_date = $year."-01-31 23:59:59"; }
	if($month == "2") { $start_date = $year."-01-31 23:00:00"; $end_date = $year."-02-28 23:59:59"; }
	if($month == "3") { $start_date = $year."-02-31 23:00:00"; $end_date = $year."-03-31 23:59:59"; }
	if($month == "4") { $start_date = $year."-03-31 23:00:00"; $end_date = $year."-04-31 23:59:59"; }
	if($month == "5") { $start_date = $year."-04-31 23:00:00"; $end_date = $year."-05-31 23:59:59"; }
	if($month == "6") { $start_date = $year."-05-31 23:00:00"; $end_date = $year."-06-31 23:59:59"; }
	if($month == "7") { $start_date = $year."-06-31 23:00:00"; $end_date = $year."-07-31 23:59:59"; }
	if($month == "8") { $start_date = $year."-07-31 23:00:00"; $end_date = $year."-08-31 23:59:59"; }
	if($month == "9") { $start_date = $year."-08-31 23:00:00"; $end_date = $year."-09-31 23:59:59"; }
	if($month == "10") { $start_date = $year."-09-31 23:00:00"; $end_date = $year."-10-31 23:59:59"; }
	if($month == "11") { $start_date = $year."-10-31 23:00:00"; $end_date = $year."-11-31 23:59:59"; }
	if($month == "12") { $start_date = $year."-11-31 23:00:00"; $end_date = $year."-12-31 23:59:59"; }


$result_url = mysql_query("SELECT o.entity_id, o.store_id, o.status, DATE_FORMAT(o.created_at, '%d/%m/%Y') AS newDate, i.order_id, i.product_type, i.product_id, SUM(i.row_total) AS som
	FROM sales_flat_order AS o, sales_flat_order_item AS i, catalog_product_entity_int AS c
WHERE 
o.status = 'complete' && o.created_at > '$start_date' && o.created_at < '$end_date'
&& o.store_id = '$store'
&& o.entity_id = i.order_id
&& i.product_type = 'bundle'
&& i.product_id = c.entity_id
&& c.attribute_id = '579'
&& c.value = '85'
");
$row = mysql_fetch_assoc($result_url);
echo replace_number($row['som']);
echo $currency;
?>
    </td>
  </tr>
  <tr>
    <th>Feb</th>
    <td style="text-align: right;">
    <?php
	$month = "2";
	// calculate month total


if($month == "1") { $start_date = ($year-1)."-12-31 23:00:00"; $end_date = $year."-01-31 23:59:59"; }
	if($month == "2") { $start_date = $year."-01-31 23:00:00"; $end_date = $year."-02-28 23:59:59"; }
	if($month == "3") { $start_date = $year."-02-31 23:00:00"; $end_date = $year."-03-31 23:59:59"; }
	if($month == "4") { $start_date = $year."-03-31 23:00:00"; $end_date = $year."-04-31 23:59:59"; }
	if($month == "5") { $start_date = $year."-04-31 23:00:00"; $end_date = $year."-05-31 23:59:59"; }
	if($month == "6") { $start_date = $year."-05-31 23:00:00"; $end_date = $year."-06-31 23:59:59"; }
	if($month == "7") { $start_date = $year."-06-31 23:00:00"; $end_date = $year."-07-31 23:59:59"; }
	if($month == "8") { $start_date = $year."-07-31 23:00:00"; $end_date = $year."-08-31 23:59:59"; }
	if($month == "9") { $start_date = $year."-08-31 23:00:00"; $end_date = $year."-09-31 23:59:59"; }
	if($month == "10") { $start_date = $year."-09-31 23:00:00"; $end_date = $year."-10-31 23:59:59"; }
	if($month == "11") { $start_date = $year."-10-31 23:00:00"; $end_date = $year."-11-31 23:59:59"; }
	if($month == "12") { $start_date = $year."-11-31 23:00:00"; $end_date = $year."-12-31 23:59:59"; }
// calculate elearning group
$result_url = mysql_query("SELECT o.entity_id, o.store_id, o.status, DATE_FORMAT(o.created_at, '%d/%m/%Y') AS newDate, i.order_id, i.product_type, i.product_id, SUM(i.row_total) AS som
	FROM sales_flat_order AS o, sales_flat_order_item AS i, catalog_product_entity_int AS c
WHERE 
o.status = 'complete' && o.created_at > '$start_date' && o.created_at < '$end_date'
&& o.store_id = '$store'
&& o.entity_id = i.order_id
&& i.product_type = 'bundle'
&& i.product_id = c.entity_id
&& c.attribute_id = '579'
&& c.value = '84'
");
$row = mysql_fetch_assoc($result_url);
echo replace_number($row['som']);
echo $currency;

	?> 
    </td>
    <td style="text-align: right;">
   <?php
    // calculate software group


	if($month == "1") { $start_date = ($year-1)."-12-31 23:00:00"; $end_date = $year."-01-31 23:59:59"; }
	if($month == "2") { $start_date = $year."-01-31 23:00:00"; $end_date = $year."-02-28 23:59:59"; }
	if($month == "3") { $start_date = $year."-02-31 23:00:00"; $end_date = $year."-03-31 23:59:59"; }
	if($month == "4") { $start_date = $year."-03-31 23:00:00"; $end_date = $year."-04-31 23:59:59"; }
	if($month == "5") { $start_date = $year."-04-31 23:00:00"; $end_date = $year."-05-31 23:59:59"; }
	if($month == "6") { $start_date = $year."-05-31 23:00:00"; $end_date = $year."-06-31 23:59:59"; }
	if($month == "7") { $start_date = $year."-06-31 23:00:00"; $end_date = $year."-07-31 23:59:59"; }
	if($month == "8") { $start_date = $year."-07-31 23:00:00"; $end_date = $year."-08-31 23:59:59"; }
	if($month == "9") { $start_date = $year."-08-31 23:00:00"; $end_date = $year."-09-31 23:59:59"; }
	if($month == "10") { $start_date = $year."-09-31 23:00:00"; $end_date = $year."-10-31 23:59:59"; }
	if($month == "11") { $start_date = $year."-10-31 23:00:00"; $end_date = $year."-11-31 23:59:59"; }
	if($month == "12") { $start_date = $year."-11-31 23:00:00"; $end_date = $year."-12-31 23:59:59"; }


$result_url = mysql_query("SELECT o.entity_id, o.store_id, o.status, DATE_FORMAT(o.created_at, '%d/%m/%Y') AS newDate, i.order_id, i.product_type, i.product_id, SUM(i.row_total) AS som
	FROM sales_flat_order AS o, sales_flat_order_item AS i, catalog_product_entity_int AS c
WHERE 
o.status = 'complete' && o.created_at > '$start_date' && o.created_at < '$end_date'
&& o.store_id = '$store'
&& o.entity_id = i.order_id
&& i.product_type = 'bundle'
&& i.product_id = c.entity_id
&& c.attribute_id = '579'
&& c.value = '83'
");
$row = mysql_fetch_assoc($result_url);
echo replace_number($row['som']);
echo $currency;
?> 
    </td>
    <td style="text-align: right;">
    <?php
    // calculate training & other group

	if($month == "1") { $start_date = ($year-1)."-12-31 23:00:00"; $end_date = $year."-01-31 23:59:59"; }
	if($month == "2") { $start_date = $year."-01-31 23:00:00"; $end_date = $year."-02-28 23:59:59"; }
	if($month == "3") { $start_date = $year."-02-31 23:00:00"; $end_date = $year."-03-31 23:59:59"; }
	if($month == "4") { $start_date = $year."-03-31 23:00:00"; $end_date = $year."-04-31 23:59:59"; }
	if($month == "5") { $start_date = $year."-04-31 23:00:00"; $end_date = $year."-05-31 23:59:59"; }
	if($month == "6") { $start_date = $year."-05-31 23:00:00"; $end_date = $year."-06-31 23:59:59"; }
	if($month == "7") { $start_date = $year."-06-31 23:00:00"; $end_date = $year."-07-31 23:59:59"; }
	if($month == "8") { $start_date = $year."-07-31 23:00:00"; $end_date = $year."-08-31 23:59:59"; }
	if($month == "9") { $start_date = $year."-08-31 23:00:00"; $end_date = $year."-09-31 23:59:59"; }
	if($month == "10") { $start_date = $year."-09-31 23:00:00"; $end_date = $year."-10-31 23:59:59"; }
	if($month == "11") { $start_date = $year."-10-31 23:00:00"; $end_date = $year."-11-31 23:59:59"; }
	if($month == "12") { $start_date = $year."-11-31 23:00:00"; $end_date = $year."-12-31 23:59:59"; }


$result_url = mysql_query("SELECT o.entity_id, o.store_id, o.status, DATE_FORMAT(o.created_at, '%d/%m/%Y') AS newDate, i.order_id, i.product_type, i.product_id, SUM(i.row_total) AS som
	FROM sales_flat_order AS o, sales_flat_order_item AS i, catalog_product_entity_int AS c
WHERE 
o.status = 'complete' && o.created_at > '$start_date' && o.created_at < '$end_date'
&& o.store_id = '$store'
&& o.entity_id = i.order_id
&& i.product_type = 'bundle'
&& i.product_id = c.entity_id
&& c.attribute_id = '579'
&& c.value = '85'
");
$row = mysql_fetch_assoc($result_url);
echo replace_number($row['som']);
echo $currency;
?>
    </td>
  </tr>
  <tr>
    <th>Mar</th>
    <td style="text-align: right;">
    <?php
	$month = "3";
	// calculate month total


if($month == "1") { $start_date = ($year-1)."-12-31 23:00:00"; $end_date = $year."-01-31 23:59:59"; }
	if($month == "2") { $start_date = $year."-01-31 23:00:00"; $end_date = $year."-02-28 23:59:59"; }
	if($month == "3") { $start_date = $year."-02-31 23:00:00"; $end_date = $year."-03-31 23:59:59"; }
	if($month == "4") { $start_date = $year."-03-31 23:00:00"; $end_date = $year."-04-31 23:59:59"; }
	if($month == "5") { $start_date = $year."-04-31 23:00:00"; $end_date = $year."-05-31 23:59:59"; }
	if($month == "6") { $start_date = $year."-05-31 23:00:00"; $end_date = $year."-06-31 23:59:59"; }
	if($month == "7") { $start_date = $year."-06-31 23:00:00"; $end_date = $year."-07-31 23:59:59"; }
	if($month == "8") { $start_date = $year."-07-31 23:00:00"; $end_date = $year."-08-31 23:59:59"; }
	if($month == "9") { $start_date = $year."-08-31 23:00:00"; $end_date = $year."-09-31 23:59:59"; }
	if($month == "10") { $start_date = $year."-09-31 23:00:00"; $end_date = $year."-10-31 23:59:59"; }
	if($month == "11") { $start_date = $year."-10-31 23:00:00"; $end_date = $year."-11-31 23:59:59"; }
	if($month == "12") { $start_date = $year."-11-31 23:00:00"; $end_date = $year."-12-31 23:59:59"; }
// calculate elearning group
$result_url = mysql_query("SELECT o.entity_id, o.store_id, o.status, DATE_FORMAT(o.created_at, '%d/%m/%Y') AS newDate, i.order_id, i.product_type, i.product_id, SUM(i.row_total) AS som
	FROM sales_flat_order AS o, sales_flat_order_item AS i, catalog_product_entity_int AS c
WHERE 
o.status = 'complete' && o.created_at > '$start_date' && o.created_at < '$end_date'
&& o.store_id = '$store'
&& o.entity_id = i.order_id
&& i.product_type = 'bundle'
&& i.product_id = c.entity_id
&& c.attribute_id = '579'
&& c.value = '84'
");
$row = mysql_fetch_assoc($result_url);
echo replace_number($row['som']);
echo $currency;

	?> 
    </td>
    <td style="text-align: right;">
   <?php
    // calculate software group


	if($month == "1") { $start_date = ($year-1)."-12-31 23:00:00"; $end_date = $year."-01-31 23:59:59"; }
	if($month == "2") { $start_date = $year."-01-31 23:00:00"; $end_date = $year."-02-28 23:59:59"; }
	if($month == "3") { $start_date = $year."-02-31 23:00:00"; $end_date = $year."-03-31 23:59:59"; }
	if($month == "4") { $start_date = $year."-03-31 23:00:00"; $end_date = $year."-04-31 23:59:59"; }
	if($month == "5") { $start_date = $year."-04-31 23:00:00"; $end_date = $year."-05-31 23:59:59"; }
	if($month == "6") { $start_date = $year."-05-31 23:00:00"; $end_date = $year."-06-31 23:59:59"; }
	if($month == "7") { $start_date = $year."-06-31 23:00:00"; $end_date = $year."-07-31 23:59:59"; }
	if($month == "8") { $start_date = $year."-07-31 23:00:00"; $end_date = $year."-08-31 23:59:59"; }
	if($month == "9") { $start_date = $year."-08-31 23:00:00"; $end_date = $year."-09-31 23:59:59"; }
	if($month == "10") { $start_date = $year."-09-31 23:00:00"; $end_date = $year."-10-31 23:59:59"; }
	if($month == "11") { $start_date = $year."-10-31 23:00:00"; $end_date = $year."-11-31 23:59:59"; }
	if($month == "12") { $start_date = $year."-11-31 23:00:00"; $end_date = $year."-12-31 23:59:59"; }


$result_url = mysql_query("SELECT o.entity_id, o.store_id, o.status, DATE_FORMAT(o.created_at, '%d/%m/%Y') AS newDate, i.order_id, i.product_type, i.product_id, SUM(i.row_total) AS som
	FROM sales_flat_order AS o, sales_flat_order_item AS i, catalog_product_entity_int AS c
WHERE 
o.status = 'complete' && o.created_at > '$start_date' && o.created_at < '$end_date'
&& o.store_id = '$store'
&& o.entity_id = i.order_id
&& i.product_type = 'bundle'
&& i.product_id = c.entity_id
&& c.attribute_id = '579'
&& c.value = '83'
");
$row = mysql_fetch_assoc($result_url);
echo replace_number($row['som']);
echo $currency;
?> 
    </td>
    <td style="text-align: right;">
    <?php
    // calculate training & other group

	if($month == "1") { $start_date = ($year-1)."-12-31 23:00:00"; $end_date = $year."-01-31 23:59:59"; }
	if($month == "2") { $start_date = $year."-01-31 23:00:00"; $end_date = $year."-02-28 23:59:59"; }
	if($month == "3") { $start_date = $year."-02-31 23:00:00"; $end_date = $year."-03-31 23:59:59"; }
	if($month == "4") { $start_date = $year."-03-31 23:00:00"; $end_date = $year."-04-31 23:59:59"; }
	if($month == "5") { $start_date = $year."-04-31 23:00:00"; $end_date = $year."-05-31 23:59:59"; }
	if($month == "6") { $start_date = $year."-05-31 23:00:00"; $end_date = $year."-06-31 23:59:59"; }
	if($month == "7") { $start_date = $year."-06-31 23:00:00"; $end_date = $year."-07-31 23:59:59"; }
	if($month == "8") { $start_date = $year."-07-31 23:00:00"; $end_date = $year."-08-31 23:59:59"; }
	if($month == "9") { $start_date = $year."-08-31 23:00:00"; $end_date = $year."-09-31 23:59:59"; }
	if($month == "10") { $start_date = $year."-09-31 23:00:00"; $end_date = $year."-10-31 23:59:59"; }
	if($month == "11") { $start_date = $year."-10-31 23:00:00"; $end_date = $year."-11-31 23:59:59"; }
	if($month == "12") { $start_date = $year."-11-31 23:00:00"; $end_date = $year."-12-31 23:59:59"; }


$result_url = mysql_query("SELECT o.entity_id, o.store_id, o.status, DATE_FORMAT(o.created_at, '%d/%m/%Y') AS newDate, i.order_id, i.product_type, i.product_id, SUM(i.row_total) AS som
	FROM sales_flat_order AS o, sales_flat_order_item AS i, catalog_product_entity_int AS c
WHERE 
o.status = 'complete' && o.created_at > '$start_date' && o.created_at < '$end_date'
&& o.store_id = '$store'
&& o.entity_id = i.order_id
&& i.product_type = 'bundle'
&& i.product_id = c.entity_id
&& c.attribute_id = '579'
&& c.value = '85'
");
$row = mysql_fetch_assoc($result_url);
echo replace_number($row['som']);
echo $currency;
?>
    </td>
  </tr>
  <tr>
    <th>Apr</th>
    <td style="text-align: right;">
    <?php
	$month = "4";
	// calculate month total


if($month == "1") { $start_date = ($year-1)."-12-31 23:00:00"; $end_date = $year."-01-31 23:59:59"; }
	if($month == "2") { $start_date = $year."-01-31 23:00:00"; $end_date = $year."-02-28 23:59:59"; }
	if($month == "3") { $start_date = $year."-02-31 23:00:00"; $end_date = $year."-03-31 23:59:59"; }
	if($month == "4") { $start_date = $year."-03-31 23:00:00"; $end_date = $year."-04-31 23:59:59"; }
	if($month == "5") { $start_date = $year."-04-31 23:00:00"; $end_date = $year."-05-31 23:59:59"; }
	if($month == "6") { $start_date = $year."-05-31 23:00:00"; $end_date = $year."-06-31 23:59:59"; }
	if($month == "7") { $start_date = $year."-06-31 23:00:00"; $end_date = $year."-07-31 23:59:59"; }
	if($month == "8") { $start_date = $year."-07-31 23:00:00"; $end_date = $year."-08-31 23:59:59"; }
	if($month == "9") { $start_date = $year."-08-31 23:00:00"; $end_date = $year."-09-31 23:59:59"; }
	if($month == "10") { $start_date = $year."-09-31 23:00:00"; $end_date = $year."-10-31 23:59:59"; }
	if($month == "11") { $start_date = $year."-10-31 23:00:00"; $end_date = $year."-11-31 23:59:59"; }
	if($month == "12") { $start_date = $year."-11-31 23:00:00"; $end_date = $year."-12-31 23:59:59"; }
// calculate elearning group
$result_url = mysql_query("SELECT o.entity_id, o.store_id, o.status, DATE_FORMAT(o.created_at, '%d/%m/%Y') AS newDate, i.order_id, i.product_type, i.product_id, SUM(i.row_total) AS som
	FROM sales_flat_order AS o, sales_flat_order_item AS i, catalog_product_entity_int AS c
WHERE 
o.status = 'complete' && o.created_at > '$start_date' && o.created_at < '$end_date'
&& o.store_id = '$store'
&& o.entity_id = i.order_id
&& i.product_type = 'bundle'
&& i.product_id = c.entity_id
&& c.attribute_id = '579'
&& c.value = '84'
");
$row = mysql_fetch_assoc($result_url);
echo replace_number($row['som']);
echo $currency;

	?> 
    </td>
    <td style="text-align: right;">
   <?php
    // calculate software group


	if($month == "1") { $start_date = ($year-1)."-12-31 23:00:00"; $end_date = $year."-01-31 23:59:59"; }
	if($month == "2") { $start_date = $year."-01-31 23:00:00"; $end_date = $year."-02-28 23:59:59"; }
	if($month == "3") { $start_date = $year."-02-31 23:00:00"; $end_date = $year."-03-31 23:59:59"; }
	if($month == "4") { $start_date = $year."-03-31 23:00:00"; $end_date = $year."-04-31 23:59:59"; }
	if($month == "5") { $start_date = $year."-04-31 23:00:00"; $end_date = $year."-05-31 23:59:59"; }
	if($month == "6") { $start_date = $year."-05-31 23:00:00"; $end_date = $year."-06-31 23:59:59"; }
	if($month == "7") { $start_date = $year."-06-31 23:00:00"; $end_date = $year."-07-31 23:59:59"; }
	if($month == "8") { $start_date = $year."-07-31 23:00:00"; $end_date = $year."-08-31 23:59:59"; }
	if($month == "9") { $start_date = $year."-08-31 23:00:00"; $end_date = $year."-09-31 23:59:59"; }
	if($month == "10") { $start_date = $year."-09-31 23:00:00"; $end_date = $year."-10-31 23:59:59"; }
	if($month == "11") { $start_date = $year."-10-31 23:00:00"; $end_date = $year."-11-31 23:59:59"; }
	if($month == "12") { $start_date = $year."-11-31 23:00:00"; $end_date = $year."-12-31 23:59:59"; }


$result_url = mysql_query("SELECT o.entity_id, o.store_id, o.status, DATE_FORMAT(o.created_at, '%d/%m/%Y') AS newDate, i.order_id, i.product_type, i.product_id, SUM(i.row_total) AS som
	FROM sales_flat_order AS o, sales_flat_order_item AS i, catalog_product_entity_int AS c
WHERE 
o.status = 'complete' && o.created_at > '$start_date' && o.created_at < '$end_date'
&& o.store_id = '$store'
&& o.entity_id = i.order_id
&& i.product_type = 'bundle'
&& i.product_id = c.entity_id
&& c.attribute_id = '579'
&& c.value = '83'
");
$row = mysql_fetch_assoc($result_url);
echo replace_number($row['som']);
echo $currency;
?> 
    </td>
    <td style="text-align: right;">
    <?php
    // calculate training & other group

	if($month == "1") { $start_date = ($year-1)."-12-31 23:00:00"; $end_date = $year."-01-31 23:59:59"; }
	if($month == "2") { $start_date = $year."-01-31 23:00:00"; $end_date = $year."-02-28 23:59:59"; }
	if($month == "3") { $start_date = $year."-02-31 23:00:00"; $end_date = $year."-03-31 23:59:59"; }
	if($month == "4") { $start_date = $year."-03-31 23:00:00"; $end_date = $year."-04-31 23:59:59"; }
	if($month == "5") { $start_date = $year."-04-31 23:00:00"; $end_date = $year."-05-31 23:59:59"; }
	if($month == "6") { $start_date = $year."-05-31 23:00:00"; $end_date = $year."-06-31 23:59:59"; }
	if($month == "7") { $start_date = $year."-06-31 23:00:00"; $end_date = $year."-07-31 23:59:59"; }
	if($month == "8") { $start_date = $year."-07-31 23:00:00"; $end_date = $year."-08-31 23:59:59"; }
	if($month == "9") { $start_date = $year."-08-31 23:00:00"; $end_date = $year."-09-31 23:59:59"; }
	if($month == "10") { $start_date = $year."-09-31 23:00:00"; $end_date = $year."-10-31 23:59:59"; }
	if($month == "11") { $start_date = $year."-10-31 23:00:00"; $end_date = $year."-11-31 23:59:59"; }
	if($month == "12") { $start_date = $year."-11-31 23:00:00"; $end_date = $year."-12-31 23:59:59"; }


$result_url = mysql_query("SELECT o.entity_id, o.store_id, o.status, DATE_FORMAT(o.created_at, '%d/%m/%Y') AS newDate, i.order_id, i.product_type, i.product_id, SUM(i.row_total) AS som
	FROM sales_flat_order AS o, sales_flat_order_item AS i, catalog_product_entity_int AS c
WHERE 
o.status = 'complete' && o.created_at > '$start_date' && o.created_at < '$end_date'
&& o.store_id = '$store'
&& o.entity_id = i.order_id
&& i.product_type = 'bundle'
&& i.product_id = c.entity_id
&& c.attribute_id = '579'
&& c.value = '85'
");
$row = mysql_fetch_assoc($result_url);
echo replace_number($row['som']);
echo $currency;
?>
    </td>
  </tr>
  <tr>
    <th>May</th>
    <td style="text-align: right;">
    <?php
	$month = "5";
	// calculate month total


if($month == "1") { $start_date = ($year-1)."-12-31 23:00:00"; $end_date = $year."-01-31 23:59:59"; }
	if($month == "2") { $start_date = $year."-01-31 23:00:00"; $end_date = $year."-02-28 23:59:59"; }
	if($month == "3") { $start_date = $year."-02-31 23:00:00"; $end_date = $year."-03-31 23:59:59"; }
	if($month == "4") { $start_date = $year."-03-31 23:00:00"; $end_date = $year."-04-31 23:59:59"; }
	if($month == "5") { $start_date = $year."-04-31 23:00:00"; $end_date = $year."-05-31 23:59:59"; }
	if($month == "6") { $start_date = $year."-05-31 23:00:00"; $end_date = $year."-06-31 23:59:59"; }
	if($month == "7") { $start_date = $year."-06-31 23:00:00"; $end_date = $year."-07-31 23:59:59"; }
	if($month == "8") { $start_date = $year."-07-31 23:00:00"; $end_date = $year."-08-31 23:59:59"; }
	if($month == "9") { $start_date = $year."-08-31 23:00:00"; $end_date = $year."-09-31 23:59:59"; }
	if($month == "10") { $start_date = $year."-09-31 23:00:00"; $end_date = $year."-10-31 23:59:59"; }
	if($month == "11") { $start_date = $year."-10-31 23:00:00"; $end_date = $year."-11-31 23:59:59"; }
	if($month == "12") { $start_date = $year."-11-31 23:00:00"; $end_date = $year."-12-31 23:59:59"; }
// calculate elearning group
$result_url = mysql_query("SELECT o.entity_id, o.store_id, o.status, DATE_FORMAT(o.created_at, '%d/%m/%Y') AS newDate, i.order_id, i.product_type, i.product_id, SUM(i.row_total) AS som
	FROM sales_flat_order AS o, sales_flat_order_item AS i, catalog_product_entity_int AS c
WHERE 
o.status = 'complete' && o.created_at > '$start_date' && o.created_at < '$end_date'
&& o.store_id = '$store'
&& o.entity_id = i.order_id
&& i.product_type = 'bundle'
&& i.product_id = c.entity_id
&& c.attribute_id = '579'
&& c.value = '84'
");
$row = mysql_fetch_assoc($result_url);
echo replace_number($row['som']);
echo $currency;

	?> 
    </td>
    <td style="text-align: right;">
   <?php
    // calculate software group


	if($month == "1") { $start_date = ($year-1)."-12-31 23:00:00"; $end_date = $year."-01-31 23:59:59"; }
	if($month == "2") { $start_date = $year."-01-31 23:00:00"; $end_date = $year."-02-28 23:59:59"; }
	if($month == "3") { $start_date = $year."-02-31 23:00:00"; $end_date = $year."-03-31 23:59:59"; }
	if($month == "4") { $start_date = $year."-03-31 23:00:00"; $end_date = $year."-04-31 23:59:59"; }
	if($month == "5") { $start_date = $year."-04-31 23:00:00"; $end_date = $year."-05-31 23:59:59"; }
	if($month == "6") { $start_date = $year."-05-31 23:00:00"; $end_date = $year."-06-31 23:59:59"; }
	if($month == "7") { $start_date = $year."-06-31 23:00:00"; $end_date = $year."-07-31 23:59:59"; }
	if($month == "8") { $start_date = $year."-07-31 23:00:00"; $end_date = $year."-08-31 23:59:59"; }
	if($month == "9") { $start_date = $year."-08-31 23:00:00"; $end_date = $year."-09-31 23:59:59"; }
	if($month == "10") { $start_date = $year."-09-31 23:00:00"; $end_date = $year."-10-31 23:59:59"; }
	if($month == "11") { $start_date = $year."-10-31 23:00:00"; $end_date = $year."-11-31 23:59:59"; }
	if($month == "12") { $start_date = $year."-11-31 23:00:00"; $end_date = $year."-12-31 23:59:59"; }


$result_url = mysql_query("SELECT o.entity_id, o.store_id, o.status, DATE_FORMAT(o.created_at, '%d/%m/%Y') AS newDate, i.order_id, i.product_type, i.product_id, SUM(i.row_total) AS som
	FROM sales_flat_order AS o, sales_flat_order_item AS i, catalog_product_entity_int AS c
WHERE 
o.status = 'complete' && o.created_at > '$start_date' && o.created_at < '$end_date'
&& o.store_id = '$store'
&& o.entity_id = i.order_id
&& i.product_type = 'bundle'
&& i.product_id = c.entity_id
&& c.attribute_id = '579'
&& c.value = '83'
");
$row = mysql_fetch_assoc($result_url);
echo replace_number($row['som']);
echo $currency;
?> 
    </td>
    <td style="text-align: right;">
    <?php
    // calculate training & other group

	if($month == "1") { $start_date = ($year-1)."-12-31 23:00:00"; $end_date = $year."-01-31 23:59:59"; }
	if($month == "2") { $start_date = $year."-01-31 23:00:00"; $end_date = $year."-02-28 23:59:59"; }
	if($month == "3") { $start_date = $year."-02-31 23:00:00"; $end_date = $year."-03-31 23:59:59"; }
	if($month == "4") { $start_date = $year."-03-31 23:00:00"; $end_date = $year."-04-31 23:59:59"; }
	if($month == "5") { $start_date = $year."-04-31 23:00:00"; $end_date = $year."-05-31 23:59:59"; }
	if($month == "6") { $start_date = $year."-05-31 23:00:00"; $end_date = $year."-06-31 23:59:59"; }
	if($month == "7") { $start_date = $year."-06-31 23:00:00"; $end_date = $year."-07-31 23:59:59"; }
	if($month == "8") { $start_date = $year."-07-31 23:00:00"; $end_date = $year."-08-31 23:59:59"; }
	if($month == "9") { $start_date = $year."-08-31 23:00:00"; $end_date = $year."-09-31 23:59:59"; }
	if($month == "10") { $start_date = $year."-09-31 23:00:00"; $end_date = $year."-10-31 23:59:59"; }
	if($month == "11") { $start_date = $year."-10-31 23:00:00"; $end_date = $year."-11-31 23:59:59"; }
	if($month == "12") { $start_date = $year."-11-31 23:00:00"; $end_date = $year."-12-31 23:59:59"; }


$result_url = mysql_query("SELECT o.entity_id, o.store_id, o.status, DATE_FORMAT(o.created_at, '%d/%m/%Y') AS newDate, i.order_id, i.product_type, i.product_id, SUM(i.row_total) AS som
	FROM sales_flat_order AS o, sales_flat_order_item AS i, catalog_product_entity_int AS c
WHERE 
o.status = 'complete' && o.created_at > '$start_date' && o.created_at < '$end_date'
&& o.store_id = '$store'
&& o.entity_id = i.order_id
&& i.product_type = 'bundle'
&& i.product_id = c.entity_id
&& c.attribute_id = '579'
&& c.value = '85'
");
$row = mysql_fetch_assoc($result_url);
echo replace_number($row['som']);
echo $currency;
?>
    </td>
  </tr>
  <tr>
    <th>Jun</th>
    <td style="text-align: right;">
    <?php
	$month = "6";
	// calculate month total


if($month == "1") { $start_date = ($year-1)."-12-31 23:00:00"; $end_date = $year."-01-31 23:59:59"; }
	if($month == "2") { $start_date = $year."-01-31 23:00:00"; $end_date = $year."-02-28 23:59:59"; }
	if($month == "3") { $start_date = $year."-02-31 23:00:00"; $end_date = $year."-03-31 23:59:59"; }
	if($month == "4") { $start_date = $year."-03-31 23:00:00"; $end_date = $year."-04-31 23:59:59"; }
	if($month == "5") { $start_date = $year."-04-31 23:00:00"; $end_date = $year."-05-31 23:59:59"; }
	if($month == "6") { $start_date = $year."-05-31 23:00:00"; $end_date = $year."-06-31 23:59:59"; }
	if($month == "7") { $start_date = $year."-06-31 23:00:00"; $end_date = $year."-07-31 23:59:59"; }
	if($month == "8") { $start_date = $year."-07-31 23:00:00"; $end_date = $year."-08-31 23:59:59"; }
	if($month == "9") { $start_date = $year."-08-31 23:00:00"; $end_date = $year."-09-31 23:59:59"; }
	if($month == "10") { $start_date = $year."-09-31 23:00:00"; $end_date = $year."-10-31 23:59:59"; }
	if($month == "11") { $start_date = $year."-10-31 23:00:00"; $end_date = $year."-11-31 23:59:59"; }
	if($month == "12") { $start_date = $year."-11-31 23:00:00"; $end_date = $year."-12-31 23:59:59"; }
// calculate elearning group
$result_url = mysql_query("SELECT o.entity_id, o.store_id, o.status, DATE_FORMAT(o.created_at, '%d/%m/%Y') AS newDate, i.order_id, i.product_type, i.product_id, SUM(i.row_total) AS som
	FROM sales_flat_order AS o, sales_flat_order_item AS i, catalog_product_entity_int AS c
WHERE 
o.status = 'complete' && o.created_at > '$start_date' && o.created_at < '$end_date'
&& o.store_id = '$store'
&& o.entity_id = i.order_id
&& i.product_type = 'bundle'
&& i.product_id = c.entity_id
&& c.attribute_id = '579'
&& c.value = '84'
");
$row = mysql_fetch_assoc($result_url);
echo replace_number($row['som']);
echo $currency;

	?> 
    </td>
    <td style="text-align: right;">
   <?php
    // calculate software group


	if($month == "1") { $start_date = ($year-1)."-12-31 23:00:00"; $end_date = $year."-01-31 23:59:59"; }
	if($month == "2") { $start_date = $year."-01-31 23:00:00"; $end_date = $year."-02-28 23:59:59"; }
	if($month == "3") { $start_date = $year."-02-31 23:00:00"; $end_date = $year."-03-31 23:59:59"; }
	if($month == "4") { $start_date = $year."-03-31 23:00:00"; $end_date = $year."-04-31 23:59:59"; }
	if($month == "5") { $start_date = $year."-04-31 23:00:00"; $end_date = $year."-05-31 23:59:59"; }
	if($month == "6") { $start_date = $year."-05-31 23:00:00"; $end_date = $year."-06-31 23:59:59"; }
	if($month == "7") { $start_date = $year."-06-31 23:00:00"; $end_date = $year."-07-31 23:59:59"; }
	if($month == "8") { $start_date = $year."-07-31 23:00:00"; $end_date = $year."-08-31 23:59:59"; }
	if($month == "9") { $start_date = $year."-08-31 23:00:00"; $end_date = $year."-09-31 23:59:59"; }
	if($month == "10") { $start_date = $year."-09-31 23:00:00"; $end_date = $year."-10-31 23:59:59"; }
	if($month == "11") { $start_date = $year."-10-31 23:00:00"; $end_date = $year."-11-31 23:59:59"; }
	if($month == "12") { $start_date = $year."-11-31 23:00:00"; $end_date = $year."-12-31 23:59:59"; }


$result_url = mysql_query("SELECT o.entity_id, o.store_id, o.status, DATE_FORMAT(o.created_at, '%d/%m/%Y') AS newDate, i.order_id, i.product_type, i.product_id, SUM(i.row_total) AS som
	FROM sales_flat_order AS o, sales_flat_order_item AS i, catalog_product_entity_int AS c
WHERE 
o.status = 'complete' && o.created_at > '$start_date' && o.created_at < '$end_date'
&& o.store_id = '$store'
&& o.entity_id = i.order_id
&& i.product_type = 'bundle'
&& i.product_id = c.entity_id
&& c.attribute_id = '579'
&& c.value = '83'
");
$row = mysql_fetch_assoc($result_url);
echo replace_number($row['som']);
echo $currency;
?> 
    </td>
    <td style="text-align: right;">
    <?php
    // calculate training & other group

	if($month == "1") { $start_date = ($year-1)."-12-31 23:00:00"; $end_date = $year."-01-31 23:59:59"; }
	if($month == "2") { $start_date = $year."-01-31 23:00:00"; $end_date = $year."-02-28 23:59:59"; }
	if($month == "3") { $start_date = $year."-02-31 23:00:00"; $end_date = $year."-03-31 23:59:59"; }
	if($month == "4") { $start_date = $year."-03-31 23:00:00"; $end_date = $year."-04-31 23:59:59"; }
	if($month == "5") { $start_date = $year."-04-31 23:00:00"; $end_date = $year."-05-31 23:59:59"; }
	if($month == "6") { $start_date = $year."-05-31 23:00:00"; $end_date = $year."-06-31 23:59:59"; }
	if($month == "7") { $start_date = $year."-06-31 23:00:00"; $end_date = $year."-07-31 23:59:59"; }
	if($month == "8") { $start_date = $year."-07-31 23:00:00"; $end_date = $year."-08-31 23:59:59"; }
	if($month == "9") { $start_date = $year."-08-31 23:00:00"; $end_date = $year."-09-31 23:59:59"; }
	if($month == "10") { $start_date = $year."-09-31 23:00:00"; $end_date = $year."-10-31 23:59:59"; }
	if($month == "11") { $start_date = $year."-10-31 23:00:00"; $end_date = $year."-11-31 23:59:59"; }
	if($month == "12") { $start_date = $year."-11-31 23:00:00"; $end_date = $year."-12-31 23:59:59"; }


$result_url = mysql_query("SELECT o.entity_id, o.store_id, o.status, DATE_FORMAT(o.created_at, '%d/%m/%Y') AS newDate, i.order_id, i.product_type, i.product_id, SUM(i.row_total) AS som
	FROM sales_flat_order AS o, sales_flat_order_item AS i, catalog_product_entity_int AS c
WHERE 
o.status = 'complete' && o.created_at > '$start_date' && o.created_at < '$end_date'
&& o.store_id = '$store'
&& o.entity_id = i.order_id
&& i.product_type = 'bundle'
&& i.product_id = c.entity_id
&& c.attribute_id = '579'
&& c.value = '85'
");
$row = mysql_fetch_assoc($result_url);
echo replace_number($row['som']);
echo $currency;
?>
    </td>
  </tr>
  <tr>
    <th>Jul</th>
    <td style="text-align: right;">
    <?php
	$month = "7";
	// calculate month total
	/*$result_url = mysql_query("SELECT o.entity_id, o.store_id, o.status, DATE_FORMAT(o.created_at, '%d/%m/%Y') AS newDate, i.order_id, i.product_type, i.product_id, SUM(i.row_total) AS som
	FROM sales_flat_order AS o, sales_flat_order_item AS i
WHERE 
o.status = 'complete' && o.created_at > '$start_date' && o.created_at < '$end_date'
&& o.store_id = '$store'
&& o.entity_id = i.order_id
&& i.product_type = 'bundle'
");*/

if($month == "1") { $start_date = ($year-1)."-12-31 23:00:00"; $end_date = $year."-01-31 23:59:59"; }
	if($month == "2") { $start_date = $year."-01-31 23:00:00"; $end_date = $year."-02-28 23:59:59"; }
	if($month == "3") { $start_date = $year."-02-31 23:00:00"; $end_date = $year."-03-31 23:59:59"; }
	if($month == "4") { $start_date = $year."-03-31 23:00:00"; $end_date = $year."-04-31 23:59:59"; }
	if($month == "5") { $start_date = $year."-04-31 23:00:00"; $end_date = $year."-05-31 23:59:59"; }
	if($month == "6") { $start_date = $year."-05-31 23:00:00"; $end_date = $year."-06-31 23:59:59"; }
	if($month == "7") { $start_date = $year."-06-31 23:00:00"; $end_date = $year."-07-31 23:59:59"; }
	if($month == "8") { $start_date = $year."-07-31 23:00:00"; $end_date = $year."-08-31 23:59:59"; }
	if($month == "9") { $start_date = $year."-08-31 23:00:00"; $end_date = $year."-09-31 23:59:59"; }
	if($month == "10") { $start_date = $year."-09-31 23:00:00"; $end_date = $year."-10-31 23:59:59"; }
	if($month == "11") { $start_date = $year."-10-31 23:00:00"; $end_date = $year."-11-31 23:59:59"; }
	if($month == "12") { $start_date = $year."-11-31 23:00:00"; $end_date = $year."-12-31 23:59:59"; }
// calculate elearning group
$result_url = mysql_query("SELECT o.entity_id, o.store_id, o.status, DATE_FORMAT(o.created_at, '%d/%m/%Y') AS newDate, i.order_id, i.product_type, i.product_id, SUM(i.row_total) AS som
	FROM sales_flat_order AS o, sales_flat_order_item AS i, catalog_product_entity_int AS c
WHERE 
o.status = 'complete' && o.created_at > '$start_date' && o.created_at < '$end_date'
&& o.store_id = '$store'
&& o.entity_id = i.order_id
&& i.product_type = 'bundle'
&& i.product_id = c.entity_id
&& c.attribute_id = '579'
&& c.value = '84'
");
$row = mysql_fetch_assoc($result_url);
echo replace_number($row['som']);
echo $currency;

	?> 
    </td>
    <td style="text-align: right;">
   <?php
    // calculate software group


	if($month == "1") { $start_date = ($year-1)."-12-31 23:00:00"; $end_date = $year."-01-31 23:59:59"; }
	if($month == "2") { $start_date = $year."-01-31 23:00:00"; $end_date = $year."-02-28 23:59:59"; }
	if($month == "3") { $start_date = $year."-02-31 23:00:00"; $end_date = $year."-03-31 23:59:59"; }
	if($month == "4") { $start_date = $year."-03-31 23:00:00"; $end_date = $year."-04-31 23:59:59"; }
	if($month == "5") { $start_date = $year."-04-31 23:00:00"; $end_date = $year."-05-31 23:59:59"; }
	if($month == "6") { $start_date = $year."-05-31 23:00:00"; $end_date = $year."-06-31 23:59:59"; }
	if($month == "7") { $start_date = $year."-06-31 23:00:00"; $end_date = $year."-07-31 23:59:59"; }
	if($month == "8") { $start_date = $year."-07-31 23:00:00"; $end_date = $year."-08-31 23:59:59"; }
	if($month == "9") { $start_date = $year."-08-31 23:00:00"; $end_date = $year."-09-31 23:59:59"; }
	if($month == "10") { $start_date = $year."-09-31 23:00:00"; $end_date = $year."-10-31 23:59:59"; }
	if($month == "11") { $start_date = $year."-10-31 23:00:00"; $end_date = $year."-11-31 23:59:59"; }
	if($month == "12") { $start_date = $year."-11-31 23:00:00"; $end_date = $year."-12-31 23:59:59"; }


$result_url = mysql_query("SELECT o.entity_id, o.store_id, o.status, DATE_FORMAT(o.created_at, '%d/%m/%Y') AS newDate, i.order_id, i.product_type, i.product_id, SUM(i.row_total) AS som
	FROM sales_flat_order AS o, sales_flat_order_item AS i, catalog_product_entity_int AS c
WHERE 
o.status = 'complete' && o.created_at > '$start_date' && o.created_at < '$end_date'
&& o.store_id = '$store'
&& o.entity_id = i.order_id
&& i.product_type = 'bundle'
&& i.product_id = c.entity_id
&& c.attribute_id = '579'
&& c.value = '83'
");
$row = mysql_fetch_assoc($result_url);
echo replace_number($row['som']);
echo $currency;
?> 
    </td>
    <td style="text-align: right;">
    <?php
    // calculate training & other group


	if($month == "1") { $start_date = ($year-1)."-12-31 23:00:00"; $end_date = $year."-01-31 23:59:59"; }
	if($month == "2") { $start_date = $year."-01-31 23:00:00"; $end_date = $year."-02-28 23:59:59"; }
	if($month == "3") { $start_date = $year."-02-31 23:00:00"; $end_date = $year."-03-31 23:59:59"; }
	if($month == "4") { $start_date = $year."-03-31 23:00:00"; $end_date = $year."-04-31 23:59:59"; }
	if($month == "5") { $start_date = $year."-04-31 23:00:00"; $end_date = $year."-05-31 23:59:59"; }
	if($month == "6") { $start_date = $year."-05-31 23:00:00"; $end_date = $year."-06-31 23:59:59"; }
	if($month == "7") { $start_date = $year."-06-31 23:00:00"; $end_date = $year."-07-31 23:59:59"; }
	if($month == "8") { $start_date = $year."-07-31 23:00:00"; $end_date = $year."-08-31 23:59:59"; }
	if($month == "9") { $start_date = $year."-08-31 23:00:00"; $end_date = $year."-09-31 23:59:59"; }
	if($month == "10") { $start_date = $year."-09-31 23:00:00"; $end_date = $year."-10-31 23:59:59"; }
	if($month == "11") { $start_date = $year."-10-31 23:00:00"; $end_date = $year."-11-31 23:59:59"; }
	if($month == "12") { $start_date = $year."-11-31 23:00:00"; $end_date = $year."-12-31 23:59:59"; }


$result_url = mysql_query("SELECT o.entity_id, o.store_id, o.status, DATE_FORMAT(o.created_at, '%d/%m/%Y') AS newDate, i.order_id, i.product_type, i.product_id, SUM(i.row_total) AS som
	FROM sales_flat_order AS o, sales_flat_order_item AS i, catalog_product_entity_int AS c
WHERE 
o.status = 'complete' && o.created_at > '$start_date' && o.created_at < '$end_date'
&& o.store_id = '$store'
&& o.entity_id = i.order_id
&& i.product_type = 'bundle'
&& i.product_id = c.entity_id
&& c.attribute_id = '579'
&& c.value = '85'
");
$row = mysql_fetch_assoc($result_url);
echo replace_number($row['som']);
echo $currency;
?>
    </td>
  </tr>
  <tr>
    <th>Aug</th>
    <td style="text-align: right;">
    <?php
	$month = "8";
	// calculate month total


if($month == "1") { $start_date = ($year-1)."-12-31 23:00:00"; $end_date = $year."-01-31 23:59:59"; }
	if($month == "2") { $start_date = $year."-01-31 23:00:00"; $end_date = $year."-02-28 23:59:59"; }
	if($month == "3") { $start_date = $year."-02-31 23:00:00"; $end_date = $year."-03-31 23:59:59"; }
	if($month == "4") { $start_date = $year."-03-31 23:00:00"; $end_date = $year."-04-31 23:59:59"; }
	if($month == "5") { $start_date = $year."-04-31 23:00:00"; $end_date = $year."-05-31 23:59:59"; }
	if($month == "6") { $start_date = $year."-05-31 23:00:00"; $end_date = $year."-06-31 23:59:59"; }
	if($month == "7") { $start_date = $year."-06-31 23:00:00"; $end_date = $year."-07-31 23:59:59"; }
	if($month == "8") { $start_date = $year."-07-31 23:00:00"; $end_date = $year."-08-31 23:59:59"; }
	if($month == "9") { $start_date = $year."-08-31 23:00:00"; $end_date = $year."-09-31 23:59:59"; }
	if($month == "10") { $start_date = $year."-09-31 23:00:00"; $end_date = $year."-10-31 23:59:59"; }
	if($month == "11") { $start_date = $year."-10-31 23:00:00"; $end_date = $year."-11-31 23:59:59"; }
	if($month == "12") { $start_date = $year."-11-31 23:00:00"; $end_date = $year."-12-31 23:59:59"; }
// calculate elearning group
$result_url = mysql_query("SELECT o.entity_id, o.store_id, o.status, DATE_FORMAT(o.created_at, '%d/%m/%Y') AS newDate, i.order_id, i.product_type, i.product_id, SUM(i.row_total) AS som
	FROM sales_flat_order AS o, sales_flat_order_item AS i, catalog_product_entity_int AS c
WHERE 
o.status = 'complete' && o.created_at > '$start_date' && o.created_at < '$end_date'
&& o.store_id = '$store'
&& o.entity_id = i.order_id
&& i.product_type = 'bundle'
&& i.product_id = c.entity_id
&& c.attribute_id = '579'
&& c.value = '84'
");
$row = mysql_fetch_assoc($result_url);
echo replace_number($row['som']);
echo $currency;

	?> 
    </td>
    <td style="text-align: right;">
   <?php
    // calculate software group


	if($month == "1") { $start_date = ($year-1)."-12-31 23:00:00"; $end_date = $year."-01-31 23:59:59"; }
	if($month == "2") { $start_date = $year."-01-31 23:00:00"; $end_date = $year."-02-28 23:59:59"; }
	if($month == "3") { $start_date = $year."-02-31 23:00:00"; $end_date = $year."-03-31 23:59:59"; }
	if($month == "4") { $start_date = $year."-03-31 23:00:00"; $end_date = $year."-04-31 23:59:59"; }
	if($month == "5") { $start_date = $year."-04-31 23:00:00"; $end_date = $year."-05-31 23:59:59"; }
	if($month == "6") { $start_date = $year."-05-31 23:00:00"; $end_date = $year."-06-31 23:59:59"; }
	if($month == "7") { $start_date = $year."-06-31 23:00:00"; $end_date = $year."-07-31 23:59:59"; }
	if($month == "8") { $start_date = $year."-07-31 23:00:00"; $end_date = $year."-08-31 23:59:59"; }
	if($month == "9") { $start_date = $year."-08-31 23:00:00"; $end_date = $year."-09-31 23:59:59"; }
	if($month == "10") { $start_date = $year."-09-31 23:00:00"; $end_date = $year."-10-31 23:59:59"; }
	if($month == "11") { $start_date = $year."-10-31 23:00:00"; $end_date = $year."-11-31 23:59:59"; }
	if($month == "12") { $start_date = $year."-11-31 23:00:00"; $end_date = $year."-12-31 23:59:59"; }


$result_url = mysql_query("SELECT o.entity_id, o.store_id, o.status, DATE_FORMAT(o.created_at, '%d/%m/%Y') AS newDate, i.order_id, i.product_type, i.product_id, SUM(i.row_total) AS som
	FROM sales_flat_order AS o, sales_flat_order_item AS i, catalog_product_entity_int AS c
WHERE 
o.status = 'complete' && o.created_at > '$start_date' && o.created_at < '$end_date'
&& o.store_id = '$store'
&& o.entity_id = i.order_id
&& i.product_type = 'bundle'
&& i.product_id = c.entity_id
&& c.attribute_id = '579'
&& c.value = '83'
");
$row = mysql_fetch_assoc($result_url);
echo replace_number($row['som']);
echo $currency;
?> 
    </td>
    <td style="text-align: right;">
    <?php
    // calculate training & other group

	if($month == "1") { $start_date = ($year-1)."-12-31 23:00:00"; $end_date = $year."-01-31 23:59:59"; }
	if($month == "2") { $start_date = $year."-01-31 23:00:00"; $end_date = $year."-02-28 23:59:59"; }
	if($month == "3") { $start_date = $year."-02-31 23:00:00"; $end_date = $year."-03-31 23:59:59"; }
	if($month == "4") { $start_date = $year."-03-31 23:00:00"; $end_date = $year."-04-31 23:59:59"; }
	if($month == "5") { $start_date = $year."-04-31 23:00:00"; $end_date = $year."-05-31 23:59:59"; }
	if($month == "6") { $start_date = $year."-05-31 23:00:00"; $end_date = $year."-06-31 23:59:59"; }
	if($month == "7") { $start_date = $year."-06-31 23:00:00"; $end_date = $year."-07-31 23:59:59"; }
	if($month == "8") { $start_date = $year."-07-31 23:00:00"; $end_date = $year."-08-31 23:59:59"; }
	if($month == "9") { $start_date = $year."-08-31 23:00:00"; $end_date = $year."-09-31 23:59:59"; }
	if($month == "10") { $start_date = $year."-09-31 23:00:00"; $end_date = $year."-10-31 23:59:59"; }
	if($month == "11") { $start_date = $year."-10-31 23:00:00"; $end_date = $year."-11-31 23:59:59"; }
	if($month == "12") { $start_date = $year."-11-31 23:00:00"; $end_date = $year."-12-31 23:59:59"; }


$result_url = mysql_query("SELECT o.entity_id, o.store_id, o.status, DATE_FORMAT(o.created_at, '%d/%m/%Y') AS newDate, i.order_id, i.product_type, i.product_id, SUM(i.row_total) AS som
	FROM sales_flat_order AS o, sales_flat_order_item AS i, catalog_product_entity_int AS c
WHERE 
o.status = 'complete' && o.created_at > '$start_date' && o.created_at < '$end_date'
&& o.store_id = '$store'
&& o.entity_id = i.order_id
&& i.product_type = 'bundle'
&& i.product_id = c.entity_id
&& c.attribute_id = '579'
&& c.value = '85'
");
$row = mysql_fetch_assoc($result_url);
echo replace_number($row['som']);
echo $currency;
?>
    </td>
  </tr>
  <tr>
    <th>Sept</th>
    <td style="text-align: right;">
    <?php
	$month = "9";
	// calculate month total


if($month == "1") { $start_date = ($year-1)."-12-31 23:00:00"; $end_date = $year."-01-31 23:59:59"; }
	if($month == "2") { $start_date = $year."-01-31 23:00:00"; $end_date = $year."-02-28 23:59:59"; }
	if($month == "3") { $start_date = $year."-02-31 23:00:00"; $end_date = $year."-03-31 23:59:59"; }
	if($month == "4") { $start_date = $year."-03-31 23:00:00"; $end_date = $year."-04-31 23:59:59"; }
	if($month == "5") { $start_date = $year."-04-31 23:00:00"; $end_date = $year."-05-31 23:59:59"; }
	if($month == "6") { $start_date = $year."-05-31 23:00:00"; $end_date = $year."-06-31 23:59:59"; }
	if($month == "7") { $start_date = $year."-06-31 23:00:00"; $end_date = $year."-07-31 23:59:59"; }
	if($month == "8") { $start_date = $year."-07-31 23:00:00"; $end_date = $year."-08-31 23:59:59"; }
	if($month == "9") { $start_date = $year."-08-31 23:00:00"; $end_date = $year."-09-31 23:59:59"; }
	if($month == "10") { $start_date = $year."-09-31 23:00:00"; $end_date = $year."-10-31 23:59:59"; }
	if($month == "11") { $start_date = $year."-10-31 23:00:00"; $end_date = $year."-11-31 23:59:59"; }
	if($month == "12") { $start_date = $year."-11-31 23:00:00"; $end_date = $year."-12-31 23:59:59"; }
// calculate elearning group
$result_url = mysql_query("SELECT o.entity_id, o.store_id, o.status, DATE_FORMAT(o.created_at, '%d/%m/%Y') AS newDate, i.order_id, i.product_type, i.product_id, SUM(i.row_total) AS som
	FROM sales_flat_order AS o, sales_flat_order_item AS i, catalog_product_entity_int AS c
WHERE 
o.status = 'complete' && o.created_at > '$start_date' && o.created_at < '$end_date'
&& o.store_id = '$store'
&& o.entity_id = i.order_id
&& i.product_type = 'bundle'
&& i.product_id = c.entity_id
&& c.attribute_id = '579'
&& c.value = '84'
");
$row = mysql_fetch_assoc($result_url);
echo replace_number($row['som']);
echo $currency;

	?> 
    </td>
    <td style="text-align: right;">
   <?php
    // calculate software group


	if($month == "1") { $start_date = ($year-1)."-12-31 23:00:00"; $end_date = $year."-01-31 23:59:59"; }
	if($month == "2") { $start_date = $year."-01-31 23:00:00"; $end_date = $year."-02-28 23:59:59"; }
	if($month == "3") { $start_date = $year."-02-31 23:00:00"; $end_date = $year."-03-31 23:59:59"; }
	if($month == "4") { $start_date = $year."-03-31 23:00:00"; $end_date = $year."-04-31 23:59:59"; }
	if($month == "5") { $start_date = $year."-04-31 23:00:00"; $end_date = $year."-05-31 23:59:59"; }
	if($month == "6") { $start_date = $year."-05-31 23:00:00"; $end_date = $year."-06-31 23:59:59"; }
	if($month == "7") { $start_date = $year."-06-31 23:00:00"; $end_date = $year."-07-31 23:59:59"; }
	if($month == "8") { $start_date = $year."-07-31 23:00:00"; $end_date = $year."-08-31 23:59:59"; }
	if($month == "9") { $start_date = $year."-08-31 23:00:00"; $end_date = $year."-09-31 23:59:59"; }
	if($month == "10") { $start_date = $year."-09-31 23:00:00"; $end_date = $year."-10-31 23:59:59"; }
	if($month == "11") { $start_date = $year."-10-31 23:00:00"; $end_date = $year."-11-31 23:59:59"; }
	if($month == "12") { $start_date = $year."-11-31 23:00:00"; $end_date = $year."-12-31 23:59:59"; }


$result_url = mysql_query("SELECT o.entity_id, o.store_id, o.status, DATE_FORMAT(o.created_at, '%d/%m/%Y') AS newDate, i.order_id, i.product_type, i.product_id, SUM(i.row_total) AS som
	FROM sales_flat_order AS o, sales_flat_order_item AS i, catalog_product_entity_int AS c
WHERE 
o.status = 'complete' && o.created_at > '$start_date' && o.created_at < '$end_date'
&& o.store_id = '$store'
&& o.entity_id = i.order_id
&& i.product_type = 'bundle'
&& i.product_id = c.entity_id
&& c.attribute_id = '579'
&& c.value = '83'
");
$row = mysql_fetch_assoc($result_url);
echo replace_number($row['som']);
echo $currency;
?> 
    </td>
    <td style="text-align: right;">
    <?php
    // calculate training & other group

	if($month == "1") { $start_date = ($year-1)."-12-31 23:00:00"; $end_date = $year."-01-31 23:59:59"; }
	if($month == "2") { $start_date = $year."-01-31 23:00:00"; $end_date = $year."-02-28 23:59:59"; }
	if($month == "3") { $start_date = $year."-02-31 23:00:00"; $end_date = $year."-03-31 23:59:59"; }
	if($month == "4") { $start_date = $year."-03-31 23:00:00"; $end_date = $year."-04-31 23:59:59"; }
	if($month == "5") { $start_date = $year."-04-31 23:00:00"; $end_date = $year."-05-31 23:59:59"; }
	if($month == "6") { $start_date = $year."-05-31 23:00:00"; $end_date = $year."-06-31 23:59:59"; }
	if($month == "7") { $start_date = $year."-06-31 23:00:00"; $end_date = $year."-07-31 23:59:59"; }
	if($month == "8") { $start_date = $year."-07-31 23:00:00"; $end_date = $year."-08-31 23:59:59"; }
	if($month == "9") { $start_date = $year."-08-31 23:00:00"; $end_date = $year."-09-31 23:59:59"; }
	if($month == "10") { $start_date = $year."-09-31 23:00:00"; $end_date = $year."-10-31 23:59:59"; }
	if($month == "11") { $start_date = $year."-10-31 23:00:00"; $end_date = $year."-11-31 23:59:59"; }
	if($month == "12") { $start_date = $year."-11-31 23:00:00"; $end_date = $year."-12-31 23:59:59"; }


$result_url = mysql_query("SELECT o.entity_id, o.store_id, o.status, DATE_FORMAT(o.created_at, '%d/%m/%Y') AS newDate, i.order_id, i.product_type, i.product_id, SUM(i.row_total) AS som
	FROM sales_flat_order AS o, sales_flat_order_item AS i, catalog_product_entity_int AS c
WHERE 
o.status = 'complete' && o.created_at > '$start_date' && o.created_at < '$end_date'
&& o.store_id = '$store'
&& o.entity_id = i.order_id
&& i.product_type = 'bundle'
&& i.product_id = c.entity_id
&& c.attribute_id = '579'
&& c.value = '85'
");
$row = mysql_fetch_assoc($result_url);
echo replace_number($row['som']);
echo $currency;
?>
    </td>
  </tr>
  <tr>
    <th>Oct</th>
    <td style="text-align: right;">
    <?php
	$month = "10";
	// calculate month total


if($month == "1") { $start_date = ($year-1)."-12-31 23:00:00"; $end_date = $year."-01-31 23:59:59"; }
	if($month == "2") { $start_date = $year."-01-31 23:00:00"; $end_date = $year."-02-28 23:59:59"; }
	if($month == "3") { $start_date = $year."-02-31 23:00:00"; $end_date = $year."-03-31 23:59:59"; }
	if($month == "4") { $start_date = $year."-03-31 23:00:00"; $end_date = $year."-04-31 23:59:59"; }
	if($month == "5") { $start_date = $year."-04-31 23:00:00"; $end_date = $year."-05-31 23:59:59"; }
	if($month == "6") { $start_date = $year."-05-31 23:00:00"; $end_date = $year."-06-31 23:59:59"; }
	if($month == "7") { $start_date = $year."-06-31 23:00:00"; $end_date = $year."-07-31 23:59:59"; }
	if($month == "8") { $start_date = $year."-07-31 23:00:00"; $end_date = $year."-08-31 23:59:59"; }
	if($month == "9") { $start_date = $year."-08-31 23:00:00"; $end_date = $year."-09-31 23:59:59"; }
	if($month == "10") { $start_date = $year."-09-31 23:00:00"; $end_date = $year."-10-31 23:59:59"; }
	if($month == "11") { $start_date = $year."-10-31 23:00:00"; $end_date = $year."-11-31 23:59:59"; }
	if($month == "12") { $start_date = $year."-11-31 23:00:00"; $end_date = $year."-12-31 23:59:59"; }
// calculate elearning group
$result_url = mysql_query("SELECT o.entity_id, o.store_id, o.status, DATE_FORMAT(o.created_at, '%d/%m/%Y') AS newDate, i.order_id, i.product_type, i.product_id, SUM(i.row_total) AS som
	FROM sales_flat_order AS o, sales_flat_order_item AS i, catalog_product_entity_int AS c
WHERE 
o.status = 'complete' && o.created_at > '$start_date' && o.created_at < '$end_date'
&& o.store_id = '$store'
&& o.entity_id = i.order_id
&& i.product_type = 'bundle'
&& i.product_id = c.entity_id
&& c.attribute_id = '579'
&& c.value = '84'
");
$row = mysql_fetch_assoc($result_url);
echo replace_number($row['som']);
echo $currency;

	?> 
    </td>
    <td style="text-align: right;">
   <?php
    // calculate software group


	if($month == "1") { $start_date = ($year-1)."-12-31 23:00:00"; $end_date = $year."-01-31 23:59:59"; }
	if($month == "2") { $start_date = $year."-01-31 23:00:00"; $end_date = $year."-02-28 23:59:59"; }
	if($month == "3") { $start_date = $year."-02-31 23:00:00"; $end_date = $year."-03-31 23:59:59"; }
	if($month == "4") { $start_date = $year."-03-31 23:00:00"; $end_date = $year."-04-31 23:59:59"; }
	if($month == "5") { $start_date = $year."-04-31 23:00:00"; $end_date = $year."-05-31 23:59:59"; }
	if($month == "6") { $start_date = $year."-05-31 23:00:00"; $end_date = $year."-06-31 23:59:59"; }
	if($month == "7") { $start_date = $year."-06-31 23:00:00"; $end_date = $year."-07-31 23:59:59"; }
	if($month == "8") { $start_date = $year."-07-31 23:00:00"; $end_date = $year."-08-31 23:59:59"; }
	if($month == "9") { $start_date = $year."-08-31 23:00:00"; $end_date = $year."-09-31 23:59:59"; }
	if($month == "10") { $start_date = $year."-09-31 23:00:00"; $end_date = $year."-10-31 23:59:59"; }
	if($month == "11") { $start_date = $year."-10-31 23:00:00"; $end_date = $year."-11-31 23:59:59"; }
	if($month == "12") { $start_date = $year."-11-31 23:00:00"; $end_date = $year."-12-31 23:59:59"; }


$result_url = mysql_query("SELECT o.entity_id, o.store_id, o.status, DATE_FORMAT(o.created_at, '%d/%m/%Y') AS newDate, i.order_id, i.product_type, i.product_id, SUM(i.row_total) AS som
	FROM sales_flat_order AS o, sales_flat_order_item AS i, catalog_product_entity_int AS c
WHERE 
o.status = 'complete' && o.created_at > '$start_date' && o.created_at < '$end_date'
&& o.store_id = '$store'
&& o.entity_id = i.order_id
&& i.product_type = 'bundle'
&& i.product_id = c.entity_id
&& c.attribute_id = '579'
&& c.value = '83'

");
$row = mysql_fetch_assoc($result_url);
echo replace_number($row['som']);
echo $currency;
?> 
    </td>
    <td style="text-align: right;">
    <?php
    // calculate training & other group

	if($month == "1") { $start_date = ($year-1)."-12-31 23:00:00"; $end_date = $year."-01-31 23:59:59"; }
	if($month == "2") { $start_date = $year."-01-31 23:00:00"; $end_date = $year."-02-28 23:59:59"; }
	if($month == "3") { $start_date = $year."-02-31 23:00:00"; $end_date = $year."-03-31 23:59:59"; }
	if($month == "4") { $start_date = $year."-03-31 23:00:00"; $end_date = $year."-04-31 23:59:59"; }
	if($month == "5") { $start_date = $year."-04-31 23:00:00"; $end_date = $year."-05-31 23:59:59"; }
	if($month == "6") { $start_date = $year."-05-31 23:00:00"; $end_date = $year."-06-31 23:59:59"; }
	if($month == "7") { $start_date = $year."-06-31 23:00:00"; $end_date = $year."-07-31 23:59:59"; }
	if($month == "8") { $start_date = $year."-07-31 23:00:00"; $end_date = $year."-08-31 23:59:59"; }
	if($month == "9") { $start_date = $year."-08-31 23:00:00"; $end_date = $year."-09-31 23:59:59"; }
	if($month == "10") { $start_date = $year."-09-31 23:00:00"; $end_date = $year."-10-31 23:59:59"; }
	if($month == "11") { $start_date = $year."-10-31 23:00:00"; $end_date = $year."-11-31 23:59:59"; }
	if($month == "12") { $start_date = $year."-11-31 23:00:00"; $end_date = $year."-12-31 23:59:59"; }


$result_url = mysql_query("SELECT o.entity_id, o.store_id, o.status, DATE_FORMAT(o.created_at, '%d/%m/%Y') AS newDate, i.order_id, i.product_type, i.product_id, SUM(i.row_total) AS som
	FROM sales_flat_order AS o, sales_flat_order_item AS i, catalog_product_entity_int AS c
WHERE 
o.status = 'complete' && o.created_at > '$start_date' && o.created_at < '$end_date'
&& o.store_id = '$store'
&& o.entity_id = i.order_id
&& i.product_type = 'bundle'
&& i.product_id = c.entity_id
&& c.attribute_id = '579'
&& c.value = '85'
");
$row = mysql_fetch_assoc($result_url);
echo replace_number($row['som']);
echo $currency;
?>
    </td>
  </tr>
  <tr>
    <th>Nov</th>
    <td style="text-align: right;">
    <?php
	$month = "11";
	// calculate month total


if($month == "1") { $start_date = ($year-1)."-12-31 23:00:00"; $end_date = $year."-01-31 23:59:59"; }
	if($month == "2") { $start_date = $year."-01-31 23:00:00"; $end_date = $year."-02-28 23:59:59"; }
	if($month == "3") { $start_date = $year."-02-31 23:00:00"; $end_date = $year."-03-31 23:59:59"; }
	if($month == "4") { $start_date = $year."-03-31 23:00:00"; $end_date = $year."-04-31 23:59:59"; }
	if($month == "5") { $start_date = $year."-04-31 23:00:00"; $end_date = $year."-05-31 23:59:59"; }
	if($month == "6") { $start_date = $year."-05-31 23:00:00"; $end_date = $year."-06-31 23:59:59"; }
	if($month == "7") { $start_date = $year."-06-31 23:00:00"; $end_date = $year."-07-31 23:59:59"; }
	if($month == "8") { $start_date = $year."-07-31 23:00:00"; $end_date = $year."-08-31 23:59:59"; }
	if($month == "9") { $start_date = $year."-08-31 23:00:00"; $end_date = $year."-09-31 23:59:59"; }
	if($month == "10") { $start_date = $year."-09-31 23:00:00"; $end_date = $year."-10-31 23:59:59"; }
	if($month == "11") { $start_date = $year."-10-31 23:00:00"; $end_date = $year."-11-31 23:59:59"; }
	if($month == "12") { $start_date = $year."-11-31 23:00:00"; $end_date = $year."-12-31 23:59:59"; }
// calculate elearning group
$result_url = mysql_query("SELECT o.entity_id, o.store_id, o.status, DATE_FORMAT(o.created_at, '%d/%m/%Y') AS newDate, i.order_id, i.product_type, i.product_id, SUM(i.row_total) AS som
	FROM sales_flat_order AS o, sales_flat_order_item AS i, catalog_product_entity_int AS c
WHERE 
o.status = 'complete' && o.created_at > '$start_date' && o.created_at < '$end_date'
&& o.store_id = '$store'
&& o.entity_id = i.order_id
&& i.product_type = 'bundle'
&& i.product_id = c.entity_id
&& c.attribute_id = '579'
&& c.value = '84'
");
$row = mysql_fetch_assoc($result_url);
echo replace_number($row['som']);
echo $currency;

	?> 
    </td>
    <td style="text-align: right;">
   <?php
    // calculate software group


	if($month == "1") { $start_date = ($year-1)."-12-31 23:00:00"; $end_date = $year."-01-31 23:59:59"; }
	if($month == "2") { $start_date = $year."-01-31 23:00:00"; $end_date = $year."-02-28 23:59:59"; }
	if($month == "3") { $start_date = $year."-02-31 23:00:00"; $end_date = $year."-03-31 23:59:59"; }
	if($month == "4") { $start_date = $year."-03-31 23:00:00"; $end_date = $year."-04-31 23:59:59"; }
	if($month == "5") { $start_date = $year."-04-31 23:00:00"; $end_date = $year."-05-31 23:59:59"; }
	if($month == "6") { $start_date = $year."-05-31 23:00:00"; $end_date = $year."-06-31 23:59:59"; }
	if($month == "7") { $start_date = $year."-06-31 23:00:00"; $end_date = $year."-07-31 23:59:59"; }
	if($month == "8") { $start_date = $year."-07-31 23:00:00"; $end_date = $year."-08-31 23:59:59"; }
	if($month == "9") { $start_date = $year."-08-31 23:00:00"; $end_date = $year."-09-31 23:59:59"; }
	if($month == "10") { $start_date = $year."-09-31 23:00:00"; $end_date = $year."-10-31 23:59:59"; }
	if($month == "11") { $start_date = $year."-10-31 23:00:00"; $end_date = $year."-11-31 23:59:59"; }
	if($month == "12") { $start_date = $year."-11-31 23:00:00"; $end_date = $year."-12-31 23:59:59"; }


$result_url = mysql_query("SELECT o.entity_id, o.store_id, o.status, DATE_FORMAT(o.created_at, '%d/%m/%Y') AS newDate, i.order_id, i.product_type, i.product_id, SUM(i.row_total) AS som
	FROM sales_flat_order AS o, sales_flat_order_item AS i, catalog_product_entity_int AS c
WHERE 
o.status = 'complete' && o.created_at > '$start_date' && o.created_at < '$end_date'
&& o.store_id = '$store'
&& o.entity_id = i.order_id
&& i.product_type = 'bundle'
&& i.product_id = c.entity_id
&& c.attribute_id = '579'
&& c.value = '83'
");
$row = mysql_fetch_assoc($result_url);
echo replace_number($row['som']);
echo $currency;
?> 
    </td>
    <td style="text-align: right;">
    <?php
    // calculate training & other group

	if($month == "1") { $start_date = ($year-1)."-12-31 23:00:00"; $end_date = $year."-01-31 23:59:59"; }
	if($month == "2") { $start_date = $year."-01-31 23:00:00"; $end_date = $year."-02-28 23:59:59"; }
	if($month == "3") { $start_date = $year."-02-31 23:00:00"; $end_date = $year."-03-31 23:59:59"; }
	if($month == "4") { $start_date = $year."-03-31 23:00:00"; $end_date = $year."-04-31 23:59:59"; }
	if($month == "5") { $start_date = $year."-04-31 23:00:00"; $end_date = $year."-05-31 23:59:59"; }
	if($month == "6") { $start_date = $year."-05-31 23:00:00"; $end_date = $year."-06-31 23:59:59"; }
	if($month == "7") { $start_date = $year."-06-31 23:00:00"; $end_date = $year."-07-31 23:59:59"; }
	if($month == "8") { $start_date = $year."-07-31 23:00:00"; $end_date = $year."-08-31 23:59:59"; }
	if($month == "9") { $start_date = $year."-08-31 23:00:00"; $end_date = $year."-09-31 23:59:59"; }
	if($month == "10") { $start_date = $year."-09-31 23:00:00"; $end_date = $year."-10-31 23:59:59"; }
	if($month == "11") { $start_date = $year."-10-31 23:00:00"; $end_date = $year."-11-31 23:59:59"; }
	if($month == "12") { $start_date = $year."-11-31 23:00:00"; $end_date = $year."-12-31 23:59:59"; }


$result_url = mysql_query("SELECT o.entity_id, o.store_id, o.status, DATE_FORMAT(o.created_at, '%d/%m/%Y') AS newDate, i.order_id, i.product_type, i.product_id, SUM(i.row_total) AS som
	FROM sales_flat_order AS o, sales_flat_order_item AS i, catalog_product_entity_int AS c
WHERE 
o.status = 'complete' && o.created_at > '$start_date' && o.created_at < '$end_date'
&& o.store_id = '$store'
&& o.entity_id = i.order_id
&& i.product_type = 'bundle'
&& i.product_id = c.entity_id
&& c.attribute_id = '579'
&& c.value = '85'
");
$row = mysql_fetch_assoc($result_url);
echo replace_number($row['som']);
echo $currency;
?>
    </td>
  </tr>
  <tr>
    <th>Dec</th>
    <td style="text-align: right;">
    <?php
	$month = "12";
	// calculate month total


if($month == "1") { $start_date = ($year-1)."-12-31 23:00:00"; $end_date = $year."-01-31 23:59:59"; }
	if($month == "2") { $start_date = $year."-01-31 23:00:00"; $end_date = $year."-02-28 23:59:59"; }
	if($month == "3") { $start_date = $year."-02-31 23:00:00"; $end_date = $year."-03-31 23:59:59"; }
	if($month == "4") { $start_date = $year."-03-31 23:00:00"; $end_date = $year."-04-31 23:59:59"; }
	if($month == "5") { $start_date = $year."-04-31 23:00:00"; $end_date = $year."-05-31 23:59:59"; }
	if($month == "6") { $start_date = $year."-05-31 23:00:00"; $end_date = $year."-06-31 23:59:59"; }
	if($month == "7") { $start_date = $year."-06-31 23:00:00"; $end_date = $year."-07-31 23:59:59"; }
	if($month == "8") { $start_date = $year."-07-31 23:00:00"; $end_date = $year."-08-31 23:59:59"; }
	if($month == "9") { $start_date = $year."-08-31 23:00:00"; $end_date = $year."-09-31 23:59:59"; }
	if($month == "10") { $start_date = $year."-09-31 23:00:00"; $end_date = $year."-10-31 23:59:59"; }
	if($month == "11") { $start_date = $year."-10-31 23:00:00"; $end_date = $year."-11-31 23:59:59"; }
	if($month == "12") { $start_date = $year."-11-31 23:00:00"; $end_date = $year."-12-31 23:59:59"; }
// calculate elearning group
$result_url = mysql_query("SELECT o.entity_id, o.store_id, o.status, DATE_FORMAT(o.created_at, '%d/%m/%Y') AS newDate, i.order_id, i.product_type, i.product_id, SUM(i.row_total) AS som
	FROM sales_flat_order AS o, sales_flat_order_item AS i, catalog_product_entity_int AS c
WHERE 
o.status = 'complete' && o.created_at > '$start_date' && o.created_at < '$end_date'
&& o.store_id = '$store'
&& o.entity_id = i.order_id
&& i.product_type = 'bundle'
&& i.product_id = c.entity_id
&& c.attribute_id = '579'
&& c.value = '84'
");
$row = mysql_fetch_assoc($result_url);
echo replace_number($row['som']);
echo $currency;

	?> 
    </td>
    <td style="text-align: right;">
   <?php
    // calculate software group


	if($month == "1") { $start_date = ($year-1)."-12-31 23:00:00"; $end_date = $year."-01-31 23:59:59"; }
	if($month == "2") { $start_date = $year."-01-31 23:00:00"; $end_date = $year."-02-28 23:59:59"; }
	if($month == "3") { $start_date = $year."-02-31 23:00:00"; $end_date = $year."-03-31 23:59:59"; }
	if($month == "4") { $start_date = $year."-03-31 23:00:00"; $end_date = $year."-04-31 23:59:59"; }
	if($month == "5") { $start_date = $year."-04-31 23:00:00"; $end_date = $year."-05-31 23:59:59"; }
	if($month == "6") { $start_date = $year."-05-31 23:00:00"; $end_date = $year."-06-31 23:59:59"; }
	if($month == "7") { $start_date = $year."-06-31 23:00:00"; $end_date = $year."-07-31 23:59:59"; }
	if($month == "8") { $start_date = $year."-07-31 23:00:00"; $end_date = $year."-08-31 23:59:59"; }
	if($month == "9") { $start_date = $year."-08-31 23:00:00"; $end_date = $year."-09-31 23:59:59"; }
	if($month == "10") { $start_date = $year."-09-31 23:00:00"; $end_date = $year."-10-31 23:59:59"; }
	if($month == "11") { $start_date = $year."-10-31 23:00:00"; $end_date = $year."-11-31 23:59:59"; }
	if($month == "12") { $start_date = $year."-11-31 23:00:00"; $end_date = $year."-12-31 23:59:59"; }


$result_url = mysql_query("SELECT o.entity_id, o.store_id, o.status, DATE_FORMAT(o.created_at, '%d/%m/%Y') AS newDate, i.order_id, i.product_type, i.product_id, SUM(i.row_total) AS som
	FROM sales_flat_order AS o, sales_flat_order_item AS i, catalog_product_entity_int AS c
WHERE 
o.status = 'complete' && o.created_at > '$start_date' && o.created_at < '$end_date'
&& o.store_id = '$store'
&& o.entity_id = i.order_id
&& i.product_type = 'bundle'
&& i.product_id = c.entity_id
&& c.attribute_id = '579'
&& c.value = '83'
");
$row = mysql_fetch_assoc($result_url);
echo replace_number($row['som']);
echo $currency;
?> 
    </td>
    <td style="text-align: right;">
    <?php
    // calculate training & other group

	if($month == "1") { $start_date = ($year-1)."-12-31 23:00:00"; $end_date = $year."-01-31 23:59:59"; }
	if($month == "2") { $start_date = $year."-01-31 23:00:00"; $end_date = $year."-02-28 23:59:59"; }
	if($month == "3") { $start_date = $year."-02-31 23:00:00"; $end_date = $year."-03-31 23:59:59"; }
	if($month == "4") { $start_date = $year."-03-31 23:00:00"; $end_date = $year."-04-31 23:59:59"; }
	if($month == "5") { $start_date = $year."-04-31 23:00:00"; $end_date = $year."-05-31 23:59:59"; }
	if($month == "6") { $start_date = $year."-05-31 23:00:00"; $end_date = $year."-06-31 23:59:59"; }
	if($month == "7") { $start_date = $year."-06-31 23:00:00"; $end_date = $year."-07-31 23:59:59"; }
	if($month == "8") { $start_date = $year."-07-31 23:00:00"; $end_date = $year."-08-31 23:59:59"; }
	if($month == "9") { $start_date = $year."-08-31 23:00:00"; $end_date = $year."-09-31 23:59:59"; }
	if($month == "10") { $start_date = $year."-09-31 23:00:00"; $end_date = $year."-10-31 23:59:59"; }
	if($month == "11") { $start_date = $year."-10-31 23:00:00"; $end_date = $year."-11-31 23:59:59"; }
	if($month == "12") { $start_date = $year."-11-31 23:00:00"; $end_date = $year."-12-31 23:59:59"; }


$result_url = mysql_query("SELECT o.entity_id, o.store_id, o.status, DATE_FORMAT(o.created_at, '%d/%m/%Y') AS newDate, i.order_id, i.product_type, i.product_id, SUM(i.row_total) AS som
	FROM sales_flat_order AS o, sales_flat_order_item AS i, catalog_product_entity_int AS c
WHERE 
o.status = 'complete' && o.created_at > '$start_date' && o.created_at < '$end_date'
&& o.store_id = '$store'
&& o.entity_id = i.order_id
&& i.product_type = 'bundle'
&& i.product_id = c.entity_id
&& c.attribute_id = '579'
&& c.value = '85'
");
$row = mysql_fetch_assoc($result_url);
echo replace_number($row['som']);
echo $currency;
?>
    </td>
  </tr>
  <tr>
</table>
<p><strong>Remark:</strong> report does not takes into account discount codes</p>


<?php
}
?>
                                </div>
                            </div>
                            <!-- /block -->


</div>
</div>
</div>
</div>
            <hr>
<?php
include("../footer.php");
?>
</html>