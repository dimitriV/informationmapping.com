<?php
$rootdir="../../";
$page="FS Pro: amounts ordered";
include("../connect.php");
include("../header.php");
?>

<body>
<?php
include("../topnav.php");
?>
<div class="container-fluid">
            <div class="row-fluid">
                
                <?php
				include("../sidenav.php");
				?>
                
                <div class="span10" id="content">
                    <div class="row-fluid">
                    	<div class="span12">
                        
                            <!-- block -->
                            <div class="block">
                                <div class="navbar navbar-inner block-header">
                                <h3>FS Pro versions: amounts ordered</h3>     
                                </div>
                                <div class="block-content collapse in">
<?php 
if (!isset($_SESSION["username"])){
	echo $niet_ingelogd;
}
// hieronder staan de functies die toegelaten worden tot deze pagina
elseif($_SESSION["function"] != "admin"){
	echo $geen_toegang;
}else{
?>
<form action="" method="post">
Start date: <input type="text" name="startdate" value="2012-12-31 23:59:59" />
End date: <input type="text" name="enddate" value="2013-12-31 23:59:59" />

<input type="submit" name="verzenden" value="Search" class="btn" />
</form>
<table class="table table-striped">
<tr><th>ID</th><th width="400">FS Pro 4.0</th><th>Quantity sold</th></tr>

  <?php


$result = mysql_query("SELECT * FROM catalog_product_entity ORDER BY entity_id ASC"); 
$aantal = mysql_num_rows($result); 

function replace_product($nummerke)
{
	$sql_user = mysql_query("SELECT * FROM catalog_product_entity_varchar WHERE entity_id = '$nummerke' && attribute_id = '56'");
	$report_user = mysql_fetch_assoc($sql_user);
	
	$out = $report_user['value'];
	return $out;
}


// FS Pro 4.0
$verzenden = isset($_POST['verzenden']) ? $_POST['verzenden'] : '';
if($verzenden)
{
$start_date = $_POST['startdate']; 
$end_date = $_POST['enddate'];
$result2 = mysql_query("SELECT *,SUM( it.qty_ordered ) AS cnt_rij, it.product_id AS product FROM sales_flat_order_item AS it, sales_flat_order AS ord WHERE it.order_id = ord.entity_id && ord.status = 'complete' 
&& ord.created_at > '$start_date' && ord.created_at < '$end_date'
&& (it.product_id = '23' OR it.product_id = '38')
GROUP BY it.product_id
ORDER BY cnt_rij DESC");
}
else
{
$result2 = mysql_query("SELECT *,SUM( it.qty_ordered ) AS cnt_rij, it.product_id AS product FROM sales_flat_order_item AS it, sales_flat_order AS ord WHERE it.order_id = ord.entity_id && ord.status = 'complete' && (it.product_id = '23' OR it.product_id = '38')
GROUP BY it.product_id
ORDER BY cnt_rij DESC");
}
$totaal_fspro40 = 0;

while($report=mysql_fetch_array($result2)){
	echo "<tr>";
	echo "<td>" . $report['product'] . "</td>";
	echo "<td>" . replace_product($report['product']) . "</td>";
	$aant = $report['cnt_rij'];
	$totaal_fspro40+=$aant;
	echo "<td>" . round($aant,0) . "</td>";
	echo "</tr>";
}

echo "<tr><td>&nbsp;</td><td>&nbsp;</td><td>" . $totaal_fspro40 . "</td></tr>";
echo "</table>";

// FS Pro 4.1
$verzenden = isset($_POST['verzenden']) ? $_POST['verzenden'] : '';
if($verzenden)
{
$start_date = $_POST['startdate']; 
$end_date = $_POST['enddate'];
$result3 = mysql_query("SELECT *,SUM( it.qty_ordered ) AS cnt_rij, it.product_id AS product FROM sales_flat_order_item AS it, sales_flat_order AS ord WHERE it.order_id = ord.entity_id && ord.status = 'complete' 
&& ord.created_at > '$start_date' && ord.created_at < '$end_date'
&& (it.product_id = '175' OR it.product_id = '180'
 OR it.product_id = '239' OR it.product_id = '240' OR it.product_id = '245' OR it.product_id = '246' OR it.product_id = '281')
GROUP BY it.product_id
ORDER BY cnt_rij DESC");
}
else
{
$result3 = mysql_query("SELECT *,SUM( it.qty_ordered ) AS cnt_rij, it.product_id AS product FROM sales_flat_order_item AS it, sales_flat_order AS ord WHERE it.order_id = ord.entity_id && ord.status = 'complete' && (it.product_id = '175' OR it.product_id = '180'
 OR it.product_id = '239' OR it.product_id = '240' OR it.product_id = '245' OR it.product_id = '246' OR it.product_id = '281')
GROUP BY it.product_id
ORDER BY cnt_rij DESC");
}
$totaal_fspro40 = 0;
echo "<table class=\"table table-striped\">";
echo "<tr><th>ID</th><th width=\"400\">FS Pro 4.1</th><th>Quantity sold</th></tr>";

while($report=mysql_fetch_array($result3)){
	echo "<tr>";
	echo "<td>" . $report['product'] . "</td>";
	echo "<td>" . replace_product($report['product']) . "</td>";
	$aant = $report['cnt_rij'];
	$totaal_fspro40+=$aant;
	echo "<td>" . round($aant,0) . "</td>";
	echo "</tr>";
}

echo "<tr><td>&nbsp;</td><td>&nbsp;</td><td>" . $totaal_fspro40 . "</td></tr>";

// FS Pro 4.2
$verzenden = isset($_POST['verzenden']) ? $_POST['verzenden'] : '';
if($verzenden)
{
$start_date = $_POST['startdate']; 
$end_date = $_POST['enddate'];
$result3 = mysql_query("SELECT *,SUM( it.qty_ordered ) AS cnt_rij, it.product_id AS product FROM sales_flat_order_item AS it, sales_flat_order AS ord WHERE it.order_id = ord.entity_id && ord.status = 'complete' 
&& ord.created_at > '$start_date' && ord.created_at < '$end_date'
&& (it.product_id = '280' OR it.product_id = '282'
 OR it.product_id = '285' OR it.product_id = '288' OR it.product_id = '291' OR it.product_id = '292' OR it.product_id = '293' OR it.product_id = '294' OR it.product_id = '431' OR it.product_id = '432')
GROUP BY it.product_id
ORDER BY cnt_rij DESC");
}
else
{
$result3 = mysql_query("SELECT *,SUM( it.qty_ordered ) AS cnt_rij, it.product_id AS product FROM sales_flat_order_item AS it, sales_flat_order AS ord WHERE it.order_id = ord.entity_id && ord.status = 'complete' && (it.product_id = '280' OR it.product_id = '282'
 OR it.product_id = '285' OR it.product_id = '288' OR it.product_id = '291' OR it.product_id = '292' OR it.product_id = '293' OR it.product_id = '294' OR it.product_id = '431' OR it.product_id = '432')
GROUP BY it.product_id
ORDER BY cnt_rij DESC");
}
$totaal_fspro40 = 0;
echo "<table class=\"table table-striped\">";
echo "<tr><th>ID</th><th width=\"400\">FS Pro 4.2</th><th>Quantity sold</th></tr>";

while($report=mysql_fetch_array($result3)){
	echo "<tr>";
	echo "<td>" . $report['product'] . "</td>";
	echo "<td>" . replace_product($report['product']) . "</td>";
	$aant = $report['cnt_rij'];
	$totaal_fspro40+=$aant;
	echo "<td>" . round($aant,0) . "</td>";
	echo "</tr>";
}

echo "<tr><td>&nbsp;</td><td>&nbsp;</td><td>" . $totaal_fspro40 . "</td></tr>";
echo "</table>";

// FS Pro 4.3
$verzenden = isset($_POST['verzenden']) ? $_POST['verzenden'] : '';
if($verzenden)
{
$start_date = $_POST['startdate']; 
$end_date = $_POST['enddate'];
$result3 = mysql_query("SELECT *,SUM( it.qty_ordered ) AS cnt_rij, it.product_id AS product FROM sales_flat_order_item AS it, sales_flat_order AS ord WHERE it.order_id = ord.entity_id && ord.status = 'complete' 
&& ord.created_at > '$start_date' && ord.created_at < '$end_date'
&& (it.product_id = '475' OR it.product_id = '477'
 OR it.product_id = '479' OR it.product_id = '484' OR it.product_id = '485')
GROUP BY it.product_id
ORDER BY cnt_rij DESC");
}
else
{
$result3 = mysql_query("SELECT *,SUM( it.qty_ordered ) AS cnt_rij, it.product_id AS product FROM sales_flat_order_item AS it, sales_flat_order AS ord WHERE it.order_id = ord.entity_id && ord.status = 'complete' 
&& (it.product_id = '475' OR it.product_id = '477'
 OR it.product_id = '479' OR it.product_id = '484' OR it.product_id = '485')
GROUP BY it.product_id
ORDER BY cnt_rij DESC");
}
$totaal_fspro40 = 0;
echo "<table class=\"table table-striped\">";
echo "<tr><th>ID</th><th width=\"400\">FS Pro 4.3</th><th>Quantity sold</th></tr>";

while($report=mysql_fetch_array($result3)){
	echo "<tr>";
	echo "<td>" . $report['product'] . "</td>";
	echo "<td>" . replace_product($report['product']) . "</td>";
	$aant = $report['cnt_rij'];
	$totaal_fspro40+=$aant;
	echo "<td>" . round($aant,0) . "</td>";
	echo "</tr>";
}

echo "<tr><td>&nbsp;</td><td>&nbsp;</td><td>" . $totaal_fspro40 . "</td></tr>";
echo "</table>";

// FS Pro 2013
$verzenden = isset($_POST['verzenden']) ? $_POST['verzenden'] : '';
if($verzenden)
{
$start_date = $_POST['startdate']; 
$end_date = $_POST['enddate'];
$result3 = mysql_query("SELECT *,SUM( it.qty_ordered ) AS cnt_rij, it.product_id AS product FROM sales_flat_order_item AS it, sales_flat_order AS ord 
WHERE it.order_id = ord.entity_id && ord.status = 'complete' 
&& ord.created_at > '$start_date' && ord.created_at < '$end_date'
&& (it.product_id = '385' OR it.product_id = '389'
 OR it.product_id = '390' OR it.product_id = '391' OR it.product_id = '396' OR it.product_id = '397' OR it.product_id = '398' OR it.product_id = '399' OR it.product_id = '401' OR it.product_id = '402' OR it.product_id = '403' OR it.product_id = '405' OR it.product_id = '406' OR it.product_id = '410' OR it.product_id = '414' OR it.product_id = '415' OR it.product_id = '418' OR it.product_id = '419' OR it.product_id = '420' OR it.product_id = '421' OR it.product_id = '422' OR it.product_id = '423'
 OR it.product_id = '487' OR it.product_id = '488' OR it.product_id = '489' OR it.product_id = '490' OR it.product_id = '492' OR it.product_id = '493' OR it.product_id = '494' OR it.product_id = '495' OR it.product_id = '509' OR it.product_id = '510' OR it.product_id = '511' OR it.product_id = '508' OR it.product_id = '525' OR it.product_id = '526' OR it.product_id = '527'
 )
GROUP BY it.product_id
ORDER BY cnt_rij DESC");
}
else
{
$result3 = mysql_query("SELECT *,SUM( it.qty_ordered ) AS cnt_rij, it.product_id AS product FROM sales_flat_order_item AS it, sales_flat_order AS ord 
WHERE it.order_id = ord.entity_id && ord.status = 'complete' 
&& (it.product_id = '385' OR it.product_id = '389'
 OR it.product_id = '390' OR it.product_id = '391' OR it.product_id = '396' OR it.product_id = '397' OR it.product_id = '398' OR it.product_id = '399' OR it.product_id = '401' OR it.product_id = '402' OR it.product_id = '403' OR it.product_id = '405' OR it.product_id = '406' OR it.product_id = '410' OR it.product_id = '414' OR it.product_id = '415' OR it.product_id = '418' OR it.product_id = '419' OR it.product_id = '420' OR it.product_id = '421' OR it.product_id = '422' OR it.product_id = '423'
  OR it.product_id = '487' OR it.product_id = '488' OR it.product_id = '489' OR it.product_id = '490' OR it.product_id = '492' OR it.product_id = '493' OR it.product_id = '494' OR it.product_id = '495' OR it.product_id = '509' OR it.product_id = '510' OR it.product_id = '511' OR it.product_id = '508' OR it.product_id = '525' OR it.product_id = '526' OR it.product_id = '527'
 )
GROUP BY it.product_id
ORDER BY cnt_rij DESC");
}
$totaal_fspro40 = 0;
echo "<table class=\"table table-striped\">";
echo "<tr><th>ID</th><th width=\"400\">FS Pro 2013</th><th>Quantity sold</th></tr>";

while($report=mysql_fetch_array($result3)){
	echo "<tr>";
	echo "<td>" . $report['product'] . "</td>";
	echo "<td>" . replace_product($report['product']) . "</td>";
	$aant = $report['cnt_rij'];
	$totaal_fspro40+=$aant;
	echo "<td>" . round($aant,0) . "</td>";
	echo "</tr>";
}

echo "<tr><td>&nbsp;</td><td>&nbsp;</td><td>" . $totaal_fspro40 . "</td></tr>";

?>

</table>
<?php
}
?>
                                </div>
                            </div>
                            <!-- /block -->


</div>
</div>
</div>
</div>
            <hr>
<?php
include("../footer.php");
?>
</html>