<?php
$rootdir="../../";
$page="FS Pro 2013: license files";
include("../connect.php");
include("../header.php");
?>

<body>
<?php
include("../topnav.php");
?>
<div class="container-fluid">
            <div class="row-fluid">
                
                <?php
				include("../sidenav.php");
				?>
                
                <div class="span10" id="content">
                    <div class="row-fluid">
                    	<div class="span12">
                        
                            <!-- block -->
                            <div class="block">
                                <div class="navbar navbar-inner block-header">
                                     
                                </div>
                                <div class="block-content collapse in">
<?php 
if (!isset($_SESSION["username"])){
	echo $niet_ingelogd;
}
// hieronder staan de functies die toegelaten worden tot deze pagina
elseif($_SESSION["function"] != "admin"){
	echo $geen_toegang;
}else{
?>
<div class="ui-widget">

<?php
$key = isset($_GET['key']) ? $_GET['key'] : '';
// wegens guest aankopen kan customer_id niet gehaald worden uit tbl license_serialnumbers
$result = mysql_query("SELECT l.serial_number, l.product_id, l.order_id, s.entity_id, s.increment_id, s.customer_email, s.created_at, a.parent_id, a.email, a.firstname, a.lastname
FROM license_serialnumbers AS l, sales_flat_order AS s, sales_flat_order_address AS a
WHERE l.serial_number = '$key' && l.order_id = s.entity_id && s.entity_id = a.parent_id
");
$row = mysql_fetch_assoc($result);
?>
<h3>Purchase data for key <?php echo $key; ?></h3>


<strong>Purchase date:</strong> <?php echo $row['created_at']; ?><br />
<strong>E-mail buyer:</strong> <?php echo $row['customer_email']; ?><br/>
<strong>Name buyer:</strong> <?php echo $row['firstname'] . " " . $row['lastname']; ?><br />
<strong>Order number:</strong> #<?php echo $row['increment_id']; ?><br />
<?php
// product naam uitlezen
$result_product = mysql_query("SELECT * FROM catalog_product_entity_varchar WHERE attribute_id = '56' && entity_id = '" . $row['product_id'] . "'");
$row_product = mysql_fetch_assoc($result_product);
?>
<strong>Product:</strong> <?php echo $row_product['value'] . " (ID ". $row['product_id'] . ")"; ?>

</div>
<?php
}
?>

                                </div>
                            </div>
                            <!-- /block -->


</div>
</div>
</div>
</div>
            <hr>
<?php
include("../footer.php");
?>
</html>