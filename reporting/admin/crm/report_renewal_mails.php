<?php
$rootdir="../../";
$page="FS Pro 2013: renewal mails";
include("../connect.php");
include("../header.php");
?>

<body>
<?php
include("../topnav.php");
?>
<div class="container-fluid">
            <div class="row-fluid">
                
                <?php
				include("../sidenav.php");
				?>
                
                <div class="span10" id="content">
                    <div class="row-fluid">
                    	<div class="span12">
                        
                            <!-- block -->
                            <div class="block">
                                <div class="navbar navbar-inner block-header">
                                <h3>FS Pro 2013 renewal notification mails</h3>     
                                </div>
                                <div class="block-content collapse in">
<div class="ui-widget">
<?php 
//echo $_SESSION['username'] . $_SESSION['function'];
if (!isset($_SESSION["username"])){
	echo $niet_ingelogd;
}

// hieronder staan de functies die toegelaten worden tot deze pagina
elseif($_SESSION["function"] != "admin" && $_SESSION["function"] != "it"){ // make it accessible with function admin and it
	echo $geen_toegang;
}else{

?>


<form action="" method="post">
    License Key: <input type="text" name="key" />
    Activation Key: <input type="text" name="actkey" />
    <input type="submit" name="verzenden" value="Search" />
</form>

<table class="table table-striped">
<tr>
  <th><a href="?sort=lickey">License Key</a></th><th><a href="?sort=actkey">Activation Key</a></th><th><a href="?sort=2month_n">2 months before</a></th><th><a href="?sort=1month_n">1 month before</a></th><th><a href="?sort=expiry_n">expiry</a></th><th><a href="?sort=renewed">renewed</a></th>

<?php

// HIERONDER JUIST ZOEKSCRIPT VOOR MULTIPLE ZOEKWAARDEN

$key = isset($_POST['key']) ? $_POST['key'] : '';
$actkey = isset($_POST['actkey']) ? $_POST['actkey'] : '';
//$name = isset($_POST['name']) ? $_POST['name'] : '';
//$email = isset($_POST['email']) ? $_POST['email'] : '';
//$ucreated = isset($_POST['ucreated']) ? $_POST['ucreated'] : '';
$sort = isset($_GET['sort']) ? $_GET['sort'] : '';

$where = array();
if(!empty($actkey)) $where[] = " actkey LIKE '%" . $actkey . "%'";
if(!empty($key)) $where[] = " lickey LIKE '%" . $key . "%'";

$where = implode(' AND ', $where);

// uitbreiding kolommen sorteren
if (!$sort){
$sort = "expiry";
} else {
$sort = $_GET['sort'];
//$arrow = isset($_GET['arrow']) ? $_GET['arrow'] : '';
}

if(empty($where))
{
	//$query = "SELECT * FROM AS_unique_key2013 WHERE ucreated !='' ORDER BY " . $sort . " " . $arrow; 
	$query = "SELECT * FROM as_unique_key2013 WHERE expiry !='' ORDER BY " . $sort;
}
else
{
	//$query = "SELECT * FROM AS_unique_key2013 WHERE $where && ucreated !='' ORDER BY " . $sort . " " . $arrow;
	$query = "SELECT * FROM as_unique_key2013 WHERE $where && expiry !='' ORDER BY " . $sort;
}

$result = mysql_query($query);
$aantal = mysql_num_rows($result);
echo "Number of license files: " . $aantal;
	while($row = mysql_fetch_array($result))
	{
		
		$key = $row['lickey'];
		echo "<tr>";
		echo "<td>" . $row['lickey'] . "</td>";
		echo "<td>" . $row['actkey'] . "</td>";
		echo "<td>" . $row['2month_n'] . "</td>";
		echo "<td>" . $row['1month_n'] . "</td>";
		echo "<td>" . $row['expiry_n'] . "</td>";
		echo "<td>" . $row['renewed'] . "</td>";
		echo "</tr>";
	}
?>

</table>
<?php
}
?>
</div>

                                </div>
                            </div>
                            <!-- /block -->


</div>
</div>
</div>
</div>
            <hr>
<?php
include("../footer.php");
?>
</html>