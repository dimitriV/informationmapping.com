<?php
$rootdir="../../";
$page="FS Pro 2013: renewals";
include("../connect.php");
include("../header.php");
?>

<body>
<?php
include("../topnav.php");
?>
<div class="container-fluid">
            <div class="row-fluid">
                
                <?php
				include("../sidenav.php");
				?>
                
                <div class="span10" id="content">
                    <div class="row-fluid">
                    	<div class="span12">
                        
                            <!-- block -->
                            <div class="block">
                                <div class="navbar navbar-inner block-header">
                                <h3>FS Pro renewal orders</h3>     
                                </div>
                                <div class="block-content collapse in">
<?php 
//echo $_SESSION['username'] . $_SESSION['function'];
if (!isset($_SESSION["username"])){
	echo $niet_ingelogd;
}

// hieronder staan de functies die toegelaten worden tot deze pagina
elseif($_SESSION["function"] != "admin" && $_SESSION["function"] != "it" && $_SESSION["function"] != "marketing"){ // make it accessible with function admin and it
	echo $geen_toegang;
}else{

?>
<div class="ui-widget">


<form action="" method="post">
    Renewal Key: <input type="text" name="renewalkey" />
    Original license key: <input type="text" name="lickey" />
    <input type="submit" name="verzenden" value="Search" class="btn" />
</form>

<table class="table table-striped">
<tr>
  <th>Order #</th><th>Product SKU</th><th>Original license key</th><th>Renewal key</th><th>Customer email</th>

<?php

$limit = $_GET['limit'];
if (!($limit)){
     $limit = 10;} // Default results per-page.

$pagecount = $_GET['pagecount'];
if (!($pagecount)){

     $pagecount = 0;} // Default page value.
// HIERONDER JUIST ZOEKSCRIPT VOOR MULTIPLE ZOEKWAARDEN

$verzenden = isset($_POST['verzenden']) ? $_POST['verzenden'] : '';
$actkey = isset($_POST['renewalkey']) ? $_POST['renewalkey'] : '';
$lickey = isset($_POST['lickey']) ? $_POST['lickey'] : '';


$where = array();
if(!empty($actkey)) $where[] = " activation_key = '" . $actkey . "'";
if(!empty($lickey)) $where[] = " license_key = '" . $lickey . "'";

$where = implode(' AND ', $where);




$query = "SELECT * FROM license_renew_history";

$result = mysql_query($query);
$numrows = mysql_num_rows($result);
echo "<p>Number of renewal orders: " . $numrows . "</p>";
/* multiple page script */
$pages = intval($numrows/$limit); // Number of results pages.
// $pages now contains int of pages, unless there is a remainder from division.

if ($numrows % $limit) {

$pages++;} // has remainder so add one page
$current = ($pagecount/$limit) + 1; // Current page number.

if (($pages < 1)  ||  ($pages == 0)) {

$total = 1;} // If $pages is less than one or equal to 0, total pages is 1.
else {

$total = $pages;} // Else total pages is $pages value.
$first = $pagecount + 1; // The first result.

if (!((($pagecount + $limit) / $limit) >= $pages) && $pages != 1) {

$last = $pagecount + $limit;} //If not last results page, last result equals $page plus $limit.
 
else{
$last = $numrows;} // If last results page, last result equals total number of results.



if(empty($where))
{
	$query2 = "SELECT * FROM license_renew_history LIMIT " . mysql_real_escape_string($pagecount) . ", " . mysql_real_escape_string($limit);
}
else
{
	$query2 = "SELECT * FROM license_renew_history WHERE $where";
}

$result2 = mysql_query($query2);

	while($row = mysql_fetch_array($result2))
	{
		
		$key = $row['lickey'];
		echo "<tr>";
		echo "<td>" . $row['order_increment_id'] . "</td>";
		$result_pr = mysql_query("SELECT entity_id, value FROM catalog_product_entity_varchar WHERE entity_id = '" . $row['product_id'] . "'");
		$row_pr = mysql_fetch_assoc($result_pr);
		echo "<td>" . $row_pr['value'] . "</td>";
		echo "<td>" . $row['license_key'] . "</td>";
		echo "<td>" . $row['activation_key'] . "</td>";
		echo "<td>";
		echo $row['customer_email'];
		echo "</td>";
		echo "</tr>";
	}
?>

</table>

<table width="100%" border="0">
 <tr>
  <td width="50%" align="left">
Results <b><?=$first?></b> - <b><?=$last?></b> of <b><?=$numrows?></b>
  </td>
  <td width="50%" align="right">
Page <b><?=$current?></b> of <b><?=$total?></b>
  </td>
 </tr>
 <tr>
  <td colspan="2" align="right">
&nbsp;
  </td>
 </tr>
 <tr>
  <td colspan="2" align="right">
Results per-page: <a href="<?=$PHP_SELF?>?limit=5">5</a>   <a href="<?=$PHP_SELF?>?limit=10">10</a>   <a href="<?=$PHP_SELF?>?limit=20">20</a>   <a href="<?=$PHP_SELF?>?limit=50">50</a>
  </td>
 </tr>
</table>


<p align="center">
<?
if ($pagecount != 0) { // Don't show back link if current page is first page.
$back_page = $pagecount - $limit;
echo("<a href=\"$PHP_SELF?pagecount=$back_page&limit=$limit\">back</a>    
");}
for ($i=1; $i <= $pages; $i++) // loop through each page and give link to it.

{
 $ppage = $limit*($i - 1);
 if ($ppage == $pagecount){
 echo("<b>$i</b>
");} // If current page don't give link, just text.
 else{
 echo("<a href=\"$PHP_SELF?pagecount=$ppage&limit=$limit\">$i</a> 
");}
}
if (!((($pagecount+$limit) / $limit) >= $pages) && $pages != 1) { // If last page don't give next link.

$next_page = $pagecount + $limit;
echo("    <a href=\"$PHP_SELF?pagecount=$next_page&limit=$limit\">next</a>");}
?>
</p>


</div>
<?php
}
?>
                                </div>
                            </div>
                            <!-- /block -->


</div>
</div>
</div>
</div>
            <hr>
<?php
include("../footer.php");
?>
</html>