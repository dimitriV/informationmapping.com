<?php
$rootdir="../../";
$page="Search license key";
include("../connect.php");
include("../header.php");
?>

<body>
<?php
include("../topnav.php");
?>
<div class="container-fluid">
  <div class="row-fluid">
    <?php
				include("../sidenav.php");
				?>
    <div class="span10" id="content">
      <div class="row-fluid">
        <div class="span12"> 
          
          <!-- block -->
          <div class="block">
            <div class="navbar navbar-inner block-header">
              <h3>Search license key</h3>
            </div>
            <div class="block-content collapse in">
              <?php 
if (!isset($_SESSION["username"])){
	echo $niet_ingelogd;
}
elseif($_SESSION["function"] != "admin" && $_SESSION["function"] != "it"){ // make it accessible with function admin and it
	echo $geen_toegang;
}else{
?>
              <p>Enter your license key to discover who is using it:</p>
              <form action="" method="post">
                License key:
                <input type="text" name="key" class="input-large" />
                <br />
                <strong>Note:</strong> Database FS Pro 2013 is updated every week. Older FS Pro versions once a month.<br />
                <input type="submit" name="verzenden" value="Search" class="btn" />
              </form>
              <table class="table table-striped">
                <tr>
                  <th>Key</th>
                  <th width="400">E-mail</th>
                  <th>Name</th>
                  <th>Activations</th>
                  <th>Order</th>
                </tr>
                <?php

$key = isset($_POST['key']) ? trim($_POST['key']) : '';
// remove koppeltekens for FS Pro 4 keys
if(strlen($key) == 20) { $key = str_replace('-','', $key); }
$email = isset($_POST['email']) ? $_POST['email'] : '';
$sort = isset($_GET['sort']) ? $_GET['sort'] : '';



	// search in 2 tables
	// number of columns must be the same in both columns
	// because of first name and last name are 3 columns, we have to join them with concat to one column
	
if(!empty($key))
{
$sql = "SELECT n.LicenseCode, n.EmailAddress, 
	concat(n.firstname,' ' , n.lastname) AS fullname FROM reporting_nalpeiron AS n WHERE n.LicenseCode LIKE '%$key%'
UNION
SELECT COL2, COL1, COL3 FROM as_customerinfo AS a WHERE a.COL2 LIKE '%$key%'
";
	
//echo $sql;


$result = mysql_query($sql);
$aantal = mysql_num_rows($result);

//echo "Number of license keys: " . $aantal;
	while($row = mysql_fetch_array($result))
	{
		
		echo "<tr>";
		echo "<td>" . $row['LicenseCode'] . "</td>";
		echo "<td>" . $row['EmailAddress'] . "</td>";
		echo "<td>" . $row['fullname'] . "</td>";
		
// read out activations
$result_act = mysql_query("SELECT * FROM as_tries WHERE entrykey = '$key'");	
$row_act = mysql_fetch_assoc($result_act);
echo "<td>" . $row_act['tries'] . " / " . $row_act['maxtries'] . "</td>";	

// wegens guest aankopen kan customer_id niet gehaald worden uit tbl license_serialnumbers
$result = mysql_query("SELECT l.serial_number, l.product_id, l.order_id, s.entity_id, s.increment_id, s.customer_email, s.created_at, a.parent_id, a.email, a.firstname, a.lastname
FROM license_serialnumbers AS l, sales_flat_order AS s, sales_flat_order_address AS a
WHERE l.serial_number = '$key' && l.order_id = s.entity_id && s.entity_id = a.parent_id
");
$row = mysql_fetch_assoc($result);
echo "<td># " . $row['increment_id'] . "</td>";
		
		echo "</tr>";
	}
}

// indien key niet geactiveerd is toch record tonen met order nummer
else
{
// wegens guest aankopen kan customer_id niet gehaald worden uit tbl license_serialnumbers
$result2 = mysql_query("SELECT l.serial_number, l.product_id, l.order_id, s.entity_id, s.increment_id, s.customer_email, s.created_at, a.parent_id, a.email, a.firstname, a.lastname
FROM license_serialnumbers AS l, sales_flat_order AS s, sales_flat_order_address AS a
WHERE l.serial_number = '$key' && l.order_id = s.entity_id && s.entity_id = a.parent_id
");
$row = mysql_fetch_assoc($result2);
echo "<tr><td>$key</td><td>&nbsp;</td><td>&nbsp;</td><td>&nbsp;</td><td># " . $row['increment_id'] . "</td></tr>";	
}
?>
              </table>
              <?php
}
?>
            </div>
          </div>
          <!-- /block --> 
          
        </div>
      </div>
    </div>
  </div>
  <hr>
  <?php
include("../footer.php");
?>
</html>