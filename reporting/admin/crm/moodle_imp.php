<?php
$rootdir="../../";
$page="IMP Exam results";
include("../connect_db234.php");
include("../header.php");
?>

<body>
<?php
include("../topnav.php");
?>
<div class="container-fluid">
            <div class="row-fluid">
                
                <?php
				include("../sidenav.php");
				?>
                
                <div class="span10" id="content">
                    <div class="row-fluid">
                    	<div class="span12">
                        
                            <!-- block -->
                            <div class="block">
                                <div class="navbar navbar-inner block-header">
                                <h3>IMP Exam results</h3>     
                                </div>
                                <div class="block-content collapse in">
<?php 
if (!isset($_SESSION["username"])){
	echo $niet_ingelogd;
}
// hieronder staan de functies die toegelaten worden tot deze pagina
elseif($_SESSION["function"] != "admin"){
	echo $geen_toegang;
}else{
?>

<p>
  <?php 

$imp = mysql_query("SELECT gr.userid, gr.grade, gr.timemodified, us.id, us.email, us.firstname, us.lastname, us.country FROM mdl_quiz_grades AS gr, mdl_user AS us WHERE gr.userid = us.id && gr.quiz = '1' OR (gr.userid = us.id && gr.quiz = '16') ORDER BY gr.timemodified DESC", $db2);

$imp_aantal = mysql_num_rows($imp);
echo "<p>Amount: " . $imp_aantal . "</p>";
?>

<table class="table table-striped">
<tr><th>Name</th><th><a href="?sort=email">E-mail</a></th><th><a href="?sort=country">Country</a></th><th><a href="?sort=grade">Score</a></th><th><a href="?sort=timemodified">Date</a></th></tr>

  <?php
/*$name = isset($_POST['name']) ? $_POST['name'] : '';
$country = isset($_POST['country']) ? $_POST['country'] : '';

$where = array();
if(!empty($name)) $where[] = " lastname LIKE '%" . $name . "%'";
if(!empty($country)) $where[] = " country LIKE '%" . $email . "%'";

$where = implode(' AND ', $where);

if(empty($where))
{
	$query = "SELECT gr.userid, gr.grade, gr.timemodified, us.id, us.email, us.firstname, us.lastname, us.country FROM mdl_quiz_grades AS gr, mdl_user AS us WHERE gr.userid = us.id && gr.quiz = '1' OR (gr.userid = us.id && gr.quiz = '16') ORDER BY gr.timemodified DESC"; // ORDER BY clause toegevoegd om sorteren mogelijk te maken
}
else
{
	$query = "SELECT gr.userid, gr.grade, gr.timemodified, us.id, us.email, us.firstname, us.lastname, us.country FROM mdl_quiz_grades AS gr, mdl_user AS us WHERE gr.userid = us.id && gr.quiz = '1' OR (gr.userid = us.id && gr.quiz = '16') ORDER BY gr.timemodified DESC";
}*/

// uitbreiding kolommen sorteren
if (!$_GET['sort']){
$sort = "timemodified";
} else {
$sort = $_GET['sort'];
$arrow = !empty($_GET['arrow']) ? $_GET['arrow'] : '';
}
$imp = mysql_query("SELECT gr.userid, gr.grade, gr.timemodified, us.id, us.email, us.firstname, us.lastname, us.country FROM mdl_quiz_grades AS gr, mdl_user AS us WHERE gr.userid = us.id && gr.quiz = '1' OR (gr.userid = us.id && gr.quiz = '16') ORDER BY " . $sort . " " . $arrow, $db2);

$imp_geslaagd = mysql_num_rows(mysql_query("SELECT gr.userid, gr.grade, gr.timemodified, us.id, us.email, us.firstname, us.lastname, us.country FROM mdl_quiz_grades AS gr, mdl_user AS us 
WHERE gr.userid = us.id && gr.quiz = '1' && gr.grade < 14 OR (gr.userid = us.id && gr.quiz = '16' && gr.grade < 14)", $db2));
$failed_perc = ($imp_geslaagd / $imp_aantal)*100;
echo "Failed exam: " . $imp_geslaagd . " (" . round($failed_perc,0) . " %)";

if($imp_aantal > 0)
{
	while($row3=mysql_fetch_array($imp)){
		echo "<tr>";
		echo "<td>" . $row3['firstname'] . " " . $row3['lastname'] . "</td>" ;
		echo "<td>" . $row3['email'] . "</td>";
		echo "<td>" . $row3['country'] . "</td>";
		echo "<td>";
		//echo round($row3['grade'],2);
		// use intval to make it comparable
		if(intval(round($row3['grade'],2)) < 14) { echo "<span style=color:red>" . round($row3['grade'],2) . "</span>"; } else { echo round($row3['grade'],2); }
		echo " / 20</td>";
		echo "<td>" . date('d F Y H:i:s', $row3['timemodified']) . "</td>";
		echo "</tr>\n";
	}
}

?>

</table>

<?php
}
?>
                                </div>
                            </div>
                            <!-- /block -->


</div>
</div>
</div>
</div>
            <hr>
<?php
include("../footer.php");
?>
</html>