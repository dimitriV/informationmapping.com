<?php
$rootdir="../../";
$page="contact";
include("../connect.php");
include("../header.php");
?>

<body>
<?php
include("../topnav.php");
?>
<div class="container-fluid">
            <div class="row-fluid">
                
                <?php
				include("../sidenav.php");
				?>
                
                <div class="span10" id="content">
                    <div class="row-fluid">
                    	<div class="span12">
                        
                            <!-- block -->
                            <div class="block">
                                <div class="navbar navbar-inner block-header">
                                     
                                </div>
                                <div class="block-content collapse in">
<?php 
if (!isset($_SESSION["username"])){
	echo $niet_ingelogd;
}
// hieronder staan de functies die toegelaten worden tot deze pagina
elseif($_SESSION["function"] != "admin" && $_SESSION["function"] != "marketing"){ // make it accessible with function admin and it
	echo $geen_toegang;
}else{
?>
<h3>Add info@ senders to Pardot</h3>




          <?php


/*************************************************
        FORMULIERVARIABELEN BEVEILIGEN
*************************************************/

// zet gevaarlijke tekens in invoervelden om
function cleanup($string="")
{
$string = trim($string); // verwijdert spaties aan begin en einde
//$string = htmlentities($string); // converteert < > & " en andere speciale tekens naar veilige tekens
$string = htmlspecialchars($string);
$string = nl2br($string); // converteert linebreaks in textareas naar <br />
// plaatst escapetekens bij quotes indien nodig
if(!get_magic_quotes_gpc())
  {
  $string = addslashes($string);
  }

return $string;
}

// MX-record e-mailadres beveiligen - checkdnsrr() werkt niet op Windowsservers
function check_email($mail_address) { 
    $pattern = "/^[\w-]+(\.[\w-]+)*@"; 
    $pattern .= "([0-9a-z][0-9a-z-]*[0-9a-z]\.)+([a-z]{2,4})$/i"; 
    if (preg_match($pattern, $mail_address)) { 
        $parts = explode("@", $mail_address); 
        if (checkdnsrr($parts[1], "MX")){ 
            // echo "The e-mail address is valid."; 
            return true; 
        } else { 
            // echo "The e-mail host is not valid."; 
            return false; 
        } 
    } else { 
        // echo "The e-mail address contains invalid characters."; 
        return false; 
    } 
} 




/*************************************************
            FORMULIERGEGEVENS
*************************************************/
$firstname = isset($_POST['firstname']) ? cleanup($_POST['firstname']) : '';
$lastname = isset($_POST['lastname']) ? cleanup($_POST['lastname']) : '';
$email = isset($_POST['email']) ? cleanup($_POST['email']) : '';
$phone = isset($_POST['phone']) ? cleanup($_POST['phone']) : '';
$job = isset($_POST['job']) ? cleanup($_POST['job']) : '';
$company = isset($_POST['company']) ? cleanup($_POST['company']) : '';
$visitor_country = isset($_POST['country']) ? cleanup($_POST['country']) : '';
$comments = isset($_POST['comments']) ? cleanup($_POST['comments']) : '';
$verzenden = isset($_POST['verzenden']) ? cleanup($_POST['verzenden']) : '';

if(isset($verzenden)) {


$country = $visitor_country; 


if(empty($firstname) || strlen($firstname) > 100)
{
	$error_firstname = "<div class=\"alert alert-danger\">First name not filled in.</div>";
}

if(empty($lastname) || strlen($lastname) > 100)
{
	$error_lastname = "<div class=\"alert alert-danger\">Last name not filled in.</div>";
}

if(empty($email) || strlen($email) > 150)
{
	$error_email = "<div class=\"alert alert-danger\">E-mail address not filled in.</div>";
}

if(empty($error_firstname) && empty($error_lastname) && empty($error_email))
{
	
$form_sent = "1";
echo "<p>Prospect successfully saved.</p><p><a href=\"contact.php\">Go back to the form</a></p>";	

echo "<iframe src=\"http://www2.informationmapping.com/l/8622/2014-03-03/f7rqw?email=$email&firstname=$firstname&lastname=$lastname&job=$job&company=$company&phone=$phone&country=$visitor_country&comments=$comments\" width=\"1\" height=\"1\" style=\"display: none;\"></iframe>";


}

}// einde isset $verzenden
?>
          <?php
$form_sent = isset($form_sent);
if($form_sent != 1)
{ ?>
<p>Fill out the form below to add prospects to the Pardot list <strong>IMI - Contact via info@informationmapping.com</strong>.</p>

          <form class="form-horizontal" role="form" action="#" method="post">
            <div class="form-group">
              <div class="col-sm-12">
                <label for="firstname">First name</label>
              </div>
              <div class="col-sm-12">
                <input type="text" class="form-control input-sm" id="firstname" name="firstname" value="<?php if (isset($_POST['verzenden'])) { echo $firstname; } ?>">
                <?php if($error_firstname && isset($_POST['verzenden'])) { echo $error_firstname; } ?>
              </div>
            </div>
            <div class="form-group">
              <div class="col-sm-12">
                <label for="lastname">Last name</label>
              </div>
              <div class="col-sm-12">
                <input type="text" class="form-control input-sm" id="lastname" name="lastname" value="<?php if (isset($_POST['verzenden'])) { echo $lastname; } ?>">
                <?php if($error_lastname && isset($_POST['verzenden'])) { echo $error_lastname; } ?>
              </div>
            </div>
            <div class="form-group">
              <div class="col-sm-12">
                <label for="job">Job title</label>
              </div>
              <div class="col-sm-12">
                <input type="text" class="form-control input-sm" id="job" name="job" value="<?php if (isset($_POST['verzenden'])) { echo $job; } ?>">
              </div>
            </div>
            <div class="form-group">
              <div class="col-sm-12">
                <label for="company">Company</label>
              </div>
              <div class="col-sm-12">
                <input type="text" class="form-control input-sm" id="company" name="company" value="<?php if (isset($_POST['verzenden'])) { echo $company; } ?>">
              </div>
            </div>
            <div class="form-group">
              <div class="col-sm-12">
                <label for="email">E-mail address</label>
              </div>
              <div class="col-sm-12">
                <input type="email" class="form-control input-sm" id="email" name="email" value="<?php if (isset($_POST['verzenden'])) { echo $email; } ?>">
                <?php if($error_email && isset($_POST['verzenden'])) { echo $error_email; } ?>
              </div>
            </div>
            <div class="form-group">
              <div class="col-sm-12">
                <label for="phone">Phone</label>
              </div>
              <div class="col-sm-12">
                <input type="tel" class="form-control input-sm" id="phone" name="phone" value="<?php if (isset($_POST['verzenden'])) { echo $phone; } ?>">
              </div>
            </div>
            
        <div class="form-group">
          <div class="col-sm-10">
            <label for="country">Country</label>
          </div>
          <div class="col-sm-10">
            <select name="country" id="country" class="form-control">
              <option value="Afghanistan"<?php if($visitor_country =="Afghanistan") { echo " selected"; } ?>>Afghanistan</option>
              <option value="Albania"<?php if($visitor_country =="Albania") { echo " selected"; } ?>>Albania</option>
              <option value="Algeria"<?php if($visitor_country =="Algeria") { echo " selected"; } ?>>Algeria</option>
              <option value="American Samoa"<?php if($visitor_country =="American Samoa") { echo " selected"; } ?>>American Samoa</option>
              <option value="Andorra"<?php if($visitor_country =="Andorra") { echo " selected"; } ?>>Andorra</option>
              <option value="Angola"<?php if($visitor_country =="Angola") { echo " selected"; } ?>>Angola</option>
              <option value="Anguilla"<?php if($visitor_country =="Anguilla") { echo " selected"; } ?>>Anguilla</option>
              <option value="Antarctica"<?php if($visitor_country =="Antarctica") { echo " selected"; } ?>>Antarctica</option>
              <option value="Antigua and Barbuda"<?php if($visitor_country =="Antigua and Barbuda") { echo " selected"; } ?>>Antigua and Barbuda</option>
              <option value="Argentina"<?php if($visitor_country =="Argentina") { echo " selected"; } ?>>Argentina</option>
              <option value="Armenia"<?php if($visitor_country =="Armenia") { echo " selected"; } ?>>Armenia</option>
              <option value="Aruba"<?php if($visitor_country =="Aruba") { echo " selected"; } ?>>Aruba</option>
              <option value="Australia"<?php if($visitor_country =="Australia") { echo " selected"; } ?>>Australia</option>
              <option value="Austria"<?php if($visitor_country =="Austria") { echo " selected"; } ?>>Austria</option>
              <option value="Azerbaijan"<?php if($visitor_country =="Azerbaijan") { echo " selected"; } ?>>Azerbaijan</option>
              <option value="Bahamas"<?php if($visitor_country =="Bahamas") { echo " selected"; } ?>>Bahamas</option>
              <option value="Bahrain"<?php if($visitor_country =="Bahrain") { echo " selected"; } ?>>Bahrain</option>
              <option value="Bangladesh"<?php if($visitor_country =="Bangladesh") { echo " selected"; } ?>>Bangladesh</option>
              <option value="Barbados"<?php if($visitor_country =="Barbados") { echo " selected"; } ?>>Barbados</option>
              <option value="Belarus"<?php if($visitor_country =="Belarus") { echo " selected"; } ?>>Belarus</option>
              <option value="Belgium"<?php if($visitor_country =="Belgium") { echo " selected"; } ?>>Belgium</option>
              <option value="Belize"<?php if($visitor_country =="Belize") { echo " selected"; } ?>>Belize</option>
              <option value="Benin"<?php if($visitor_country =="Benin") { echo " selected"; } ?>>Benin</option>
              <option value="Bermuda"<?php if($visitor_country =="Bermuda") { echo " selected"; } ?>>Bermuda</option>
              <option value="Bhutan"<?php if($visitor_country =="Bhutan") { echo " selected"; } ?>>Bhutan</option>
              <option value="Bolivia"<?php if($visitor_country =="Bolivia") { echo " selected"; } ?>>Bolivia</option>
              <option value="Bosnia and Herzegovina"<?php if($visitor_country =="Bosnia and Herzegovina") { echo " selected"; } ?>>Bosnia and Herzegovina</option>
              <option value="Botswana"<?php if($visitor_country =="Botswana") { echo " selected"; } ?>>Botswana</option>
              <option value="Bouvet Island"<?php if($visitor_country =="Bouvet Island") { echo " selected"; } ?>>Bouvet Island</option>
              <option value="Brazil"<?php if($visitor_country =="Brazil") { echo " selected"; } ?>>Brazil</option>
              <option value="British Indian Ocean Territory"<?php if($visitor_country =="British Indian Ocean Territory") { echo " selected"; } ?>>British Indian Ocean Territory</option>
              <option value="Brunei Darussalam"<?php if($visitor_country =="Brunei Darussalam") { echo " selected"; } ?>>Brunei Darussalam</option>
              <option value="Bulgaria"<?php if($visitor_country =="Bulgaria") { echo " selected"; } ?>>Bulgaria</option>
              <option value="Burkina Faso"<?php if($visitor_country =="Burkina Faso") { echo " selected"; } ?>>Burkina Faso</option>
              <option value="Burundi"<?php if($visitor_country =="Burundi") { echo " selected"; } ?>>Burundi</option>
              <option value="Cambodia"<?php if($visitor_country =="Cambodia") { echo " selected"; } ?>>Cambodia</option>
              <option value="Cameroon"<?php if($visitor_country =="Cameroon") { echo " selected"; } ?>>Cameroon</option>
              <option value="Canada"<?php if($visitor_country =="Canada") { echo " selected"; } ?>>Canada</option>
              <option value="Cape Verde"<?php if($visitor_country =="Cape Verde") { echo " selected"; } ?>>Cape Verde</option>
              <option value="Cayman Islands"<?php if($visitor_country =="Cayman Islands") { echo " selected"; } ?>>Cayman Islands</option>
              <option value="Central African Republic"<?php if($visitor_country =="Central African Republic") { echo " selected"; } ?>>Central African Republic</option>
              <option value="Chad"<?php if($visitor_country =="Chad") { echo " selected"; } ?>>Chad</option>
              <option value="Chile"<?php if($visitor_country =="Chile") { echo " selected"; } ?>>Chile</option>
              <option value="China"<?php if($visitor_country =="China") { echo " selected"; } ?>>China</option>
              <option value="Christmas Island"<?php if($visitor_country =="Christmas Island") { echo " selected"; } ?>>Christmas Island</option>
              <option value="Cocos (Keeling) Islands"<?php if($visitor_country =="Cocos (Keeling) Islands") { echo " selected"; } ?>>Cocos (Keeling) Islands</option>
              <option value="Colombia"<?php if($visitor_country =="Colombia") { echo " selected"; } ?>>Colombia</option>
              <option value="Comoros"<?php if($visitor_country =="Comoros") { echo " selected"; } ?>>Comoros</option>
              <option value="Congo"<?php if($visitor_country =="Congo") { echo " selected"; } ?>>Congo</option>
              <option value="Congo, The Democratic Republic of The"<?php if($visitor_country =="Congo, The Democratic Republic of The") { echo " selected"; } ?>>Congo, The Democratic Republic of The</option>
              <option value="Cook Islands"<?php if($visitor_country =="Cook Islands") { echo " selected"; } ?>>Cook Islands</option>
              <option value="Costa Rica"<?php if($visitor_country =="Costa Rica") { echo " selected"; } ?>>Costa Rica</option>
              <option value="Cote D'ivoire"<?php if($visitor_country =="Cote D'ivoire") { echo " selected"; } ?>>Cote D'ivoire</option>
              <option value="Croatia"<?php if($visitor_country =="Croatia") { echo " selected"; } ?>>Croatia</option>
              <option value="Cuba"<?php if($visitor_country =="Cuba") { echo " selected"; } ?>>Cuba</option>
              <option value="Cyprus"<?php if($visitor_country =="Cyprus") { echo " selected"; } ?>>Cyprus</option>
              <option value="Czech Republic"<?php if($visitor_country =="Czech Republic") { echo " selected"; } ?>>Czech Republic</option>
              <option value="Denmark"<?php if($visitor_country =="Denmark") { echo " selected"; } ?>>Denmark</option>
              <option value="Djibouti"<?php if($visitor_country =="Djibouti") { echo " selected"; } ?>>Djibouti</option>
              <option value="Dominica"<?php if($visitor_country =="Dominica") { echo " selected"; } ?>>Dominica</option>
              <option value="Dominican Republic"<?php if($visitor_country =="Dominican Republic") { echo " selected"; } ?>>Dominican Republic</option>
              <option value="Ecuador"<?php if($visitor_country =="Ecuador") { echo " selected"; } ?>>Ecuador</option>
              <option value="Egypt"<?php if($visitor_country =="Egypt") { echo " selected"; } ?>>Egypt</option>
              <option value="El Salvador"<?php if($visitor_country =="El Salvador") { echo " selected"; } ?>>El Salvador</option>
              <option value="Equatorial Guinea"<?php if($visitor_country =="Equatorial Guinea") { echo " selected"; } ?>>Equatorial Guinea</option>
              <option value="Eritrea"<?php if($visitor_country =="Eritrea") { echo " selected"; } ?>>Eritrea</option>
              <option value="Estonia"<?php if($visitor_country =="Estonia") { echo " selected"; } ?>>Estonia</option>
              <option value="Ethiopia"<?php if($visitor_country =="Ethiopia") { echo " selected"; } ?>>Ethiopia</option>
              <option value="Falkland Islands (Malvinas)"<?php if($visitor_country =="Falkland Islands (Malvinas)") { echo " selected"; } ?>>Falkland Islands (Malvinas)</option>
              <option value="Faroe Islands"<?php if($visitor_country =="Faroe Islands") { echo " selected"; } ?>>Faroe Islands</option>
              <option value="Fiji"<?php if($visitor_country =="Fiji") { echo " selected"; } ?>>Fiji</option>
              <option value="Finland"<?php if($visitor_country =="Finland") { echo " selected"; } ?>>Finland</option>
              <option value="France"<?php if($visitor_country =="France") { echo " selected"; } ?>>France</option>
              <option value="French Guiana"<?php if($visitor_country =="French Guiana") { echo " selected"; } ?>>French Guiana</option>
              <option value="French Polynesia"<?php if($visitor_country =="French Polynesia") { echo " selected"; } ?>>French Polynesia</option>
              <option value="French Southern Territories"<?php if($visitor_country =="French Southern Territories") { echo " selected"; } ?>>French Southern Territories</option>
              <option value="Gabon"<?php if($visitor_country =="Gabon") { echo " selected"; } ?>>Gabon</option>
              <option value="Gambia"<?php if($visitor_country =="Gambia") { echo " selected"; } ?>>Gambia</option>
              <option value="Georgia"<?php if($visitor_country =="Georgia") { echo " selected"; } ?>>Georgia</option>
              <option value="Germany"<?php if($visitor_country =="Germany") { echo " selected"; } ?>>Germany</option>
              <option value="Ghana"<?php if($visitor_country =="Ghana") { echo " selected"; } ?>>Ghana</option>
              <option value="Gibraltar"<?php if($visitor_country =="Gibraltar") { echo " selected"; } ?>>Gibraltar</option>
              <option value="Greece"<?php if($visitor_country =="Greece") { echo " selected"; } ?>>Greece</option>
              <option value="Greenland"<?php if($visitor_country =="Greenland") { echo " selected"; } ?>>Greenland</option>
              <option value="Grenada"<?php if($visitor_country =="Grenada") { echo " selected"; } ?>>Grenada</option>
              <option value="Guadeloupe"<?php if($visitor_country =="Guadeloupe") { echo " selected"; } ?>>Guadeloupe</option>
              <option value="Guam"<?php if($visitor_country =="Guam") { echo " selected"; } ?>>Guam</option>
              <option value="Guatemala"<?php if($visitor_country =="Guatemala") { echo " selected"; } ?>>Guatemala</option>
              <option value="Guinea"<?php if($visitor_country =="Guinea") { echo " selected"; } ?>>Guinea</option>
              <option value="Guinea-bissau"<?php if($visitor_country =="Guinea-bissau") { echo " selected"; } ?>>Guinea-bissau</option>
              <option value="Guyana"<?php if($visitor_country =="Guyana") { echo " selected"; } ?>>Guyana</option>
              <option value="Haiti"<?php if($visitor_country =="Haiti") { echo " selected"; } ?>>Haiti</option>
              <option value="Heard Island and Mcdonald Islands"<?php if($visitor_country =="Heard Island and Mcdonald Islands") { echo " selected"; } ?>>Heard Island and Mcdonald Islands</option>
              <option value="Holy See (Vatican City State)"<?php if($visitor_country =="Holy See (Vatican City State)") { echo " selected"; } ?>>Holy See (Vatican City State)</option>
              <option value="Honduras"<?php if($visitor_country =="Honduras") { echo " selected"; } ?>>Honduras</option>
              <option value="Hong Kong"<?php if($visitor_country =="Hong Kong") { echo " selected"; } ?>>Hong Kong</option>
              <option value="Hungary"<?php if($visitor_country =="Hungary") { echo " selected"; } ?>>Hungary</option>
              <option value="Iceland"<?php if($visitor_country =="Iceland") { echo " selected"; } ?>>Iceland</option>
              <option value="India"<?php if($visitor_country =="India") { echo " selected"; } ?>>India</option>
              <option value="Indonesia"<?php if($visitor_country =="Indonesia") { echo " selected"; } ?>>Indonesia</option>
              <option value="Iran, Islamic Republic of"<?php if($visitor_country =="Iran, Islamic Republic of") { echo " selected"; } ?>>Iran, Islamic Republic of</option>
              <option value="Iraq"<?php if($visitor_country =="Iraq") { echo " selected"; } ?>>Iraq</option>
              <option value="Ireland"<?php if($visitor_country =="Ireland") { echo " selected"; } ?>>Ireland</option>
              <option value="Israel"<?php if($visitor_country =="Israel") { echo " selected"; } ?>>Israel</option>
              <option value="Italy"<?php if($visitor_country =="Italy") { echo " selected"; } ?>>Italy</option>
              <option value="Jamaica"<?php if($visitor_country =="Jamaica") { echo " selected"; } ?>>Jamaica</option>
              <option value="Japan"<?php if($visitor_country =="Japan") { echo " selected"; } ?>>Japan</option>
              <option value="Jordan"<?php if($visitor_country =="Jordan") { echo " selected"; } ?>>Jordan</option>
              <option value="Kazakhstan"<?php if($visitor_country =="Kazakhstan") { echo " selected"; } ?>>Kazakhstan</option>
              <option value="Kenya"<?php if($visitor_country =="Kenya") { echo " selected"; } ?>>Kenya</option>
              <option value="Kiribati"<?php if($visitor_country =="Kiribati") { echo " selected"; } ?>>Kiribati</option>
              <option value="Korea, Democratic People's Republic of"<?php if($visitor_country =="Korea, Democratic People's Republic of") { echo " selected"; } ?>>Korea, Democratic People's Republic of</option>
              <option value="Korea, Republic of"<?php if($visitor_country =="Korea, Republic of") { echo " selected"; } ?>>Korea, Republic of</option>
              <option value="Kuwait"<?php if($visitor_country =="Kuwait") { echo " selected"; } ?>>Kuwait</option>
              <option value="Kyrgyzstan"<?php if($visitor_country =="Kyrgyzstan") { echo " selected"; } ?>>Kyrgyzstan</option>
              <option value="Lao People's Democratic Republic"<?php if($visitor_country =="Lao People's Democratic Republic") { echo " selected"; } ?>>Lao People's Democratic Republic</option>
              <option value="Latvia"<?php if($visitor_country =="Latvia") { echo " selected"; } ?>>Latvia</option>
              <option value="Lebanon"<?php if($visitor_country =="Lebanon") { echo " selected"; } ?>>Lebanon</option>
              <option value="Lesotho"<?php if($visitor_country =="Lesotho") { echo " selected"; } ?>>Lesotho</option>
              <option value="Liberia"<?php if($visitor_country =="Liberia") { echo " selected"; } ?>>Liberia</option>
              <option value="Libyan Arab Jamahiriya"<?php if($visitor_country =="Libyan Arab Jamahiriya") { echo " selected"; } ?>>Libyan Arab Jamahiriya</option>
              <option value="Liechtenstein"<?php if($visitor_country =="Liechtenstein") { echo " selected"; } ?>>Liechtenstein</option>
              <option value="Lithuania"<?php if($visitor_country =="Lithuania") { echo " selected"; } ?>>Lithuania</option>
              <option value="Luxembourg"<?php if($visitor_country =="Luxembourg") { echo " selected"; } ?>>Luxembourg</option>
              <option value="Macao"<?php if($visitor_country =="Macao") { echo " selected"; } ?>>Macao</option>
              <option value="Macedonia, The Former Yugoslav Republic of"<?php if($visitor_country =="Macedonia, The Former Yugoslav Republic of") { echo " selected"; } ?>>Macedonia, The Former Yugoslav Republic of</option>
              <option value="Madagascar"<?php if($visitor_country =="Madagascar") { echo " selected"; } ?>>Madagascar</option>
              <option value="Malawi"<?php if($visitor_country =="Malawi") { echo " selected"; } ?>>Malawi</option>
              <option value="Malaysia"<?php if($visitor_country =="Malaysia") { echo " selected"; } ?>>Malaysia</option>
              <option value="Maldives"<?php if($visitor_country =="Maldives") { echo " selected"; } ?>>Maldives</option>
              <option value="Mali"<?php if($visitor_country =="Mali") { echo " selected"; } ?>>Mali</option>
              <option value="Malta"<?php if($visitor_country =="Malta") { echo " selected"; } ?>>Malta</option>
              <option value="Marshall Islands"<?php if($visitor_country =="Marshall Islands") { echo " selected"; } ?>>Marshall Islands</option>
              <option value="Martinique"<?php if($visitor_country =="Martinique") { echo " selected"; } ?>>Martinique</option>
              <option value="Mauritania"<?php if($visitor_country =="Mauritania") { echo " selected"; } ?>>Mauritania</option>
              <option value="Mauritius"<?php if($visitor_country =="Mauritius") { echo " selected"; } ?>>Mauritius</option>
              <option value="Mayotte"<?php if($visitor_country =="Mayotte") { echo " selected"; } ?>>Mayotte</option>
              <option value="Mexico"<?php if($visitor_country =="Mexico") { echo " selected"; } ?>>Mexico</option>
              <option value="Micronesia, Federated States of"<?php if($visitor_country =="Micronesia, Federated States of") { echo " selected"; } ?>>Micronesia, Federated States of</option>
              <option value="Moldova, Republic of"<?php if($visitor_country =="Moldova, Republic of") { echo " selected"; } ?>>Moldova, Republic of</option>
              <option value="Monaco"<?php if($visitor_country =="Monaco") { echo " selected"; } ?>>Monaco</option>
              <option value="Mongolia"<?php if($visitor_country =="Mongolia") { echo " selected"; } ?>>Mongolia</option>
              <option value="Montserrat"<?php if($visitor_country =="Montserrat") { echo " selected"; } ?>>Montserrat</option>
              <option value="Morocco"<?php if($visitor_country =="Morocco") { echo " selected"; } ?>>Morocco</option>
              <option value="Mozambique"<?php if($visitor_country =="Mozambique") { echo " selected"; } ?>>Mozambique</option>
              <option value="Myanmar"<?php if($visitor_country =="Myanmar") { echo " selected"; } ?>>Myanmar</option>
              <option value="Namibia"<?php if($visitor_country =="Namibia") { echo " selected"; } ?>>Namibia</option>
              <option value="Nauru"<?php if($visitor_country =="Nauru") { echo " selected"; } ?>>Nauru</option>
              <option value="Nepal"<?php if($visitor_country =="Nepal") { echo " selected"; } ?>>Nepal</option>
              <option value="Netherlands"<?php if($visitor_country =="Netherlands") { echo " selected"; } ?>>Netherlands</option>
              <option value="Netherlands Antilles"<?php if($visitor_country =="Netherlands Antilles") { echo " selected"; } ?>>Netherlands Antilles</option>
              <option value="New Caledonia"<?php if($visitor_country =="New Caledonia") { echo " selected"; } ?>>New Caledonia</option>
              <option value="New Zealand"<?php if($visitor_country =="New Zealand") { echo " selected"; } ?>>New Zealand</option>
              <option value="Nicaragua"<?php if($visitor_country =="Nicaragua") { echo " selected"; } ?>>Nicaragua</option>
              <option value="Niger"<?php if($visitor_country =="Niger") { echo " selected"; } ?>>Niger</option>
              <option value="Nigeria"<?php if($visitor_country =="Nigeria") { echo " selected"; } ?>>Nigeria</option>
              <option value="Niue"<?php if($visitor_country =="Niue") { echo " selected"; } ?>>Niue</option>
              <option value="Norfolk Island"<?php if($visitor_country =="Norfolk Island") { echo " selected"; } ?>>Norfolk Island</option>
              <option value="Northern Mariana Islands"<?php if($visitor_country =="Northern Mariana Islands") { echo " selected"; } ?>>Northern Mariana Islands</option>
              <option value="Norway"<?php if($visitor_country =="Norway") { echo " selected"; } ?>>Norway</option>
              <option value="Oman"<?php if($visitor_country =="Oman") { echo " selected"; } ?>>Oman</option>
              <option value="Pakistan"<?php if($visitor_country =="Pakistan") { echo " selected"; } ?>>Pakistan</option>
              <option value="Palau"<?php if($visitor_country =="Palau") { echo " selected"; } ?>>Palau</option>
              <option value="Palestinian Territory, Occupied"<?php if($visitor_country =="Palestinian Territory, Occupied") { echo " selected"; } ?>>Palestinian Territory, Occupied</option>
              <option value="Panama"<?php if($visitor_country =="Panama") { echo " selected"; } ?>>Panama</option>
              <option value="Papua New Guinea"<?php if($visitor_country =="Papua New Guinea") { echo " selected"; } ?>>Papua New Guinea</option>
              <option value="Paraguay"<?php if($visitor_country =="Paraguay") { echo " selected"; } ?>>Paraguay</option>
              <option value="Peru"<?php if($visitor_country =="Peru") { echo " selected"; } ?>>Peru</option>
              <option value="Philippines"<?php if($visitor_country =="Philippines") { echo " selected"; } ?>>Philippines</option>
              <option value="Pitcairn"<?php if($visitor_country =="Pitcairn") { echo " selected"; } ?>>Pitcairn</option>
              <option value="Poland"<?php if($visitor_country =="Poland") { echo " selected"; } ?>>Poland</option>
              <option value="Portugal"<?php if($visitor_country =="Portugal") { echo " selected"; } ?>>Portugal</option>
              <option value="Puerto Rico"<?php if($visitor_country =="Puerto Rico") { echo " selected"; } ?>>Puerto Rico</option>
              <option value="Qatar"<?php if($visitor_country =="Qatar") { echo " selected"; } ?>>Qatar</option>
              <option value="Reunion"<?php if($visitor_country =="Reunion") { echo " selected"; } ?>>Reunion</option>
              <option value="Romania"<?php if($visitor_country =="Romania") { echo " selected"; } ?>>Romania</option>
              <option value="Russian Federation"<?php if($visitor_country =="Russian Federation") { echo " selected"; } ?>>Russian Federation</option>
              <option value="Rwanda"<?php if($visitor_country =="Rwanda") { echo " selected"; } ?>>Rwanda</option>
              <option value="Saint Helena"<?php if($visitor_country =="Saint Helena") { echo " selected"; } ?>>Saint Helena</option>
              <option value="Saint Kitts and Nevis"<?php if($visitor_country =="Saint Kitts and Nevis") { echo " selected"; } ?>>Saint Kitts and Nevis</option>
              <option value="Saint Lucia"<?php if($visitor_country =="Saint Lucia") { echo " selected"; } ?>>Saint Lucia</option>
              <option value="Saint Pierre and Miquelon"<?php if($visitor_country =="Saint Pierre and Miquelon") { echo " selected"; } ?>>Saint Pierre and Miquelon</option>
              <option value="Saint Vincent and The Grenadines"<?php if($visitor_country =="Saint Vincent and The Grenadines") { echo " selected"; } ?>>Saint Vincent and The Grenadines</option>
              <option value="Samoa"<?php if($visitor_country =="Samoa") { echo " selected"; } ?>>Samoa</option>
              <option value="San Marino"<?php if($visitor_country =="San Marino") { echo " selected"; } ?>>San Marino</option>
              <option value="Sao Tome and Principe"<?php if($visitor_country =="Sao Tome and Principe") { echo " selected"; } ?>>Sao Tome and Principe</option>
              <option value="Saudi Arabia"<?php if($visitor_country =="Saudi Arabia") { echo " selected"; } ?>>Saudi Arabia</option>
              <option value="Senegal"<?php if($visitor_country =="Senegal") { echo " selected"; } ?>>Senegal</option>
              <option value="Serbia and Montenegro"<?php if($visitor_country =="Serbia and Montenegro") { echo " selected"; } ?>>Serbia and Montenegro</option>
              <option value="Seychelles"<?php if($visitor_country =="Seychelles") { echo " selected"; } ?>>Seychelles</option>
              <option value="Sierra Leone"<?php if($visitor_country =="Sierra Leone") { echo " selected"; } ?>>Sierra Leone</option>
              <option value="Singapore"<?php if($visitor_country =="Singapore") { echo " selected"; } ?>>Singapore</option>
              <option value="Slovakia"<?php if($visitor_country =="Slovakia") { echo " selected"; } ?>>Slovakia</option>
              <option value="Slovenia"<?php if($visitor_country =="Slovenia") { echo " selected"; } ?>>Slovenia</option>
              <option value="Solomon Islands"<?php if($visitor_country =="Solomon Islands") { echo " selected"; } ?>>Solomon Islands</option>
              <option value="Somalia"<?php if($visitor_country =="Somalia") { echo " selected"; } ?>>Somalia</option>
              <option value="South Africa"<?php if($visitor_country =="South Africa") { echo " selected"; } ?>>South Africa</option>
              <option value="South Georgia and The South Sandwich Islands"<?php if($visitor_country =="South Georgia and The South Sandwich Islands") { echo " selected"; } ?>>South Georgia and The South Sandwich Islands</option>
              <option value="Spain"<?php if($visitor_country =="Spain") { echo " selected"; } ?>>Spain</option>
              <option value="Sri Lanka"<?php if($visitor_country =="Sri Lanka") { echo " selected"; } ?>>Sri Lanka</option>
              <option value="Sudan"<?php if($visitor_country =="Sudan") { echo " selected"; } ?>>Sudan</option>
              <option value="Suriname"<?php if($visitor_country =="Suriname") { echo " selected"; } ?>>Suriname</option>
              <option value="Svalbard and Jan Mayen"<?php if($visitor_country =="Svalbard and Jan Mayen") { echo " selected"; } ?>>Svalbard and Jan Mayen</option>
              <option value="Swaziland"<?php if($visitor_country =="Swaziland") { echo " selected"; } ?>>Swaziland</option>
              <option value="Sweden"<?php if($visitor_country =="Sweden") { echo " selected"; } ?>>Sweden</option>
              <option value="Switzerland"<?php if($visitor_country =="Switzerland") { echo " selected"; } ?>>Switzerland</option>
              <option value="Syrian Arab Republic"<?php if($visitor_country =="Syrian Arab Republic") { echo " selected"; } ?>>Syrian Arab Republic</option>
              <option value="Taiwan, Province of China"<?php if($visitor_country =="Taiwan, Province of China") { echo " selected"; } ?>>Taiwan, Province of China</option>
              <option value="Tajikistan"<?php if($visitor_country =="Tajikistan") { echo " selected"; } ?>>Tajikistan</option>
              <option value="Tanzania, United Republic of"<?php if($visitor_country =="Tanzania, United Republic of") { echo " selected"; } ?>>Tanzania, United Republic of</option>
              <option value="Thailand"<?php if($visitor_country =="Thailand") { echo " selected"; } ?>>Thailand</option>
              <option value="Timor-leste"<?php if($visitor_country =="Timor-leste") { echo " selected"; } ?>>Timor-leste</option>
              <option value="Togo"<?php if($visitor_country =="Togo") { echo " selected"; } ?>>Togo</option>
              <option value="Tokelau"<?php if($visitor_country =="Tokelau") { echo " selected"; } ?>>Tokelau</option>
              <option value="Tonga"<?php if($visitor_country =="Tonga") { echo " selected"; } ?>>Tonga</option>
              <option value="Trinidad and Tobago"<?php if($visitor_country =="Trinidad and Tobago") { echo " selected"; } ?>>Trinidad and Tobago</option>
              <option value="Tunisia"<?php if($visitor_country =="Tunisia") { echo " selected"; } ?>>Tunisia</option>
              <option value="Turkey"<?php if($visitor_country =="Turkey") { echo " selected"; } ?>>Turkey</option>
              <option value="Turkmenistan"<?php if($visitor_country =="Turkmenistan") { echo " selected"; } ?>>Turkmenistan</option>
              <option value="Turks and Caicos Islands"<?php if($visitor_country =="Turks and Caicos Islands") { echo " selected"; } ?>>Turks and Caicos Islands</option>
              <option value="Tuvalu"<?php if($visitor_country =="Tuvalu") { echo " selected"; } ?>>Tuvalu</option>
              <option value="Uganda"<?php if($visitor_country =="Uganda") { echo " selected"; } ?>>Uganda</option>
              <option value="Ukraine"<?php if($visitor_country =="Ukraine") { echo " selected"; } ?>>Ukraine</option>
              <option value="United Arab Emirates"<?php if($visitor_country =="United Arab Emirates") { echo " selected"; } ?>>United Arab Emirates</option>
              <option value="United Kingdom"<?php if($visitor_country =="United Kingdom") { echo " selected"; } ?>>United Kingdom</option>
              <option value="United States"<?php if($visitor_country =="United States") { echo " selected"; } ?>>United States</option>
              <option value="United States Minor Outlying Islands"<?php if($visitor_country =="United States Minor Outlying Islands") { echo " selected"; } ?>>United States Minor Outlying Islands</option>
              <option value="Uruguay"<?php if($visitor_country =="Uruguay") { echo " selected"; } ?>>Uruguay</option>
              <option value="Uzbekistan"<?php if($visitor_country =="Uzbekistan") { echo " selected"; } ?>>Uzbekistan</option>
              <option value="Vanuatu"<?php if($visitor_country =="Vanuatu") { echo " selected"; } ?>>Vanuatu</option>
              <option value="Venezuela"<?php if($visitor_country =="Venezuela") { echo " selected"; } ?>>Venezuela</option>
              <option value="Viet Nam"<?php if($visitor_country =="Viet Nam") { echo " selected"; } ?>>Viet Nam</option>
              <option value="Virgin Islands, British"<?php if($visitor_country =="Virgin Islands, British") { echo " selected"; } ?>>Virgin Islands, British</option>
              <option value="Virgin Islands, U.S."<?php if($visitor_country =="Virgin Islands, U.S.") { echo " selected"; } ?>>Virgin Islands, U.S.</option>
              <option value="Wallis and Futuna"<?php if($visitor_country =="Wallis and Futuna") { echo " selected"; } ?>>Wallis and Futuna</option>
              <option value="Western Sahara"<?php if($visitor_country =="Western Sahara") { echo " selected"; } ?>>Western Sahara</option>
              <option value="Yemen"<?php if($visitor_country =="Yemen") { echo " selected"; } ?>>Yemen</option>
              <option value="Zambia"<?php if($visitor_country =="Zambia") { echo " selected"; } ?>>Zambia</option>
              <option value="Zimbabwe"<?php if($visitor_country =="Zimbabwe") { echo " selected"; } ?>>Zimbabwe</option>
            </select>
            
          </div>
        </div>
        
        <div class="form-group">
              <div class="col-sm-12">
                <label for="comments">Comments</label>
              </div>
              <div class="col-sm-12">
              <textarea name="comments" class="form-control input-sm" rows="3" cols= "80"><?php if (isset($_POST['verzenden'])) { echo $comments; } ?></textarea>

              </div>
            </div>
        
            <div class="form-group">
              <div class="col-sm-10">
                <button type="submit" class="btn btn-default" name="verzenden">Submit</button>
              </div>
            </div>
          </form>
          <?php } // einde $form_sent ?>




<?php
}
?>

                                </div>
                            </div>
                            <!-- /block -->


</div>
</div>
</div>
</div>
            <hr>
<?php
include("../footer.php");
?>
</html>