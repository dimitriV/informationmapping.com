<?php
$rootdir="../../";
$page="Admin: create student keys";
include("../connect.php");
include("../header.php");
?>

<body>
<?php
include("../topnav.php");
?>
<div class="container-fluid">
            <div class="row-fluid">
                
                <?php
				include("../sidenav.php");
				?>
                
                <div class="span10" id="content">
                    <div class="row-fluid">
                    	<div class="span12">
                        
                            <!-- block -->
                            <div class="block">
                                <div class="navbar navbar-inner block-header">
                                <h3>Overview student keys</h3>     
                                </div>
                                <div class="block-content collapse in">
<?php 
if (!isset($_SESSION["username"])){
	echo $niet_ingelogd;
}
// hieronder staan de functies die toegelaten worden tot deze pagina
elseif($_SESSION["function"] != "admin"){
	echo $geen_toegang;
}else{
?>
<p><a href="studentkey_list.php">Back to overview</a></p>
<?php

$partner_id=$_GET['partner_id'];
$course_id=$_GET['course_id'];


$tk_SQL="SELECT * FROM reporting_trainingkey,reporting_courses,reporting_partners WHERE reporting_trainingkey.partner_id=reporting_partners.partner_id AND reporting_trainingkey.course_id=reporting_courses.course_id AND reporting_courses.course_id=$course_id AND reporting_partners.partner_id=$partner_id";
$tk_result=mysql_query($tk_SQL);

?>


	<table class="table table-striped">
  <tr>
    <th>Student key</th>
    <th>Assigned to</th>
    <th>For</th>

  </tr>

<?php
while($tk=mysql_fetch_array($tk_result)){
?>
    
	<tr>
    <td><?php echo $tk['partner_trainingkey'] . $tk['course_trainingkey'] . $tk['tk'] ?></td>
    <td><?php echo $tk['partner_company'] ?></td>
    <td><?php echo $tk['course_name'] ?></td>
    
  </tr>

<?php
}
?>          
 </table>


<?php
}
?>
                                </div>
                            </div>
                            <!-- /block -->


</div>
</div>
</div>
</div>
            <hr>
<?php
include("../footer.php");
?>
</html> 
 
 </body>
 </html>        
