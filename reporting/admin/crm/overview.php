<?php
$rootdir="../";
$page="Home";
include("../header.php");
include("../connect.php");

$activepart = mysql_query("SELECT * FROM reporting_partners WHERE active='yes'");
$inactivepart = mysql_query("SELECT * FROM reporting_partners WHERE active='no'");
$aantal_part = mysql_num_rows($activepart);
$aantal_inpart = mysql_num_rows($inactivepart);
if(isset($_POST['verzenden'])) {
    header("Location: overview.php?year=" . $_POST['choice_year']);
}
?>


<body>
<?php
include("../topnav.php");
?>
<div class="container-fluid">
<div class="row-fluid">

<?php
//include("../sidenav.php");
?>

<div class="span10" id="content">
<div class="row-fluid">
<div class="span12">

<!-- block -->
<div class="block">
<div class="navbar navbar-inner block-header">
    <!--<br /><br />
    <h3>IMI Administration Dashboard</h3>    -->
</div>
<div class="block-content collapse in">

<?php
if (!isset($_SESSION["username"])){
    echo $niet_ingelogd;
}
// hieronder staan de functies die toegelaten worden tot deze pagina
elseif($_SESSION["function"] != "admin"){
    echo $geen_toegang;
}else{
    ?>

    <p><? echo $aantal_part; ?> active /
        <? echo $aantal_inpart; ?> inactive partners</p>

    <div style="float: right; margin-bottom: 10px;">
        <form action="overview.php" method="post">
            <select name="choice_year">
                <?php
                // lees alle jaren uit die in de databasetabel zitten
                $select_SQL="SELECT * FROM reporting_year ORDER BY year_id ASC";
                $select_result=mysql_query($select_SQL);
                while($partner=mysql_fetch_array($select_result)){

                    $current_year = date("Y");
                    // wanneer de pagina geen GET waarde heeft, veronderstellen we het huidige jaar, deze code is ook nodig om de selected='selected' hieronder goed te laten werken
                    if(empty($_GET['year']))
                    {
                        $_GET['year'] = $current_year;
                    }
                    ?>

                    <option value="<?php echo $partner['year'] ?>" <?php if(($_GET['year'])==$partner['year']) { echo "selected='selected'";} ?>>
                        <?php echo $partner['year'] ?>
                    </option>


                <?php
                }
                ?>
            </select>
            <input type="submit" value="OK" name="verzenden" />
        </form>
    </div>

    <table width="100%" border="0" cellspacing="0" cellpadding="0" class="table table-striped">
    <tr>
        <th width="200">Partner</th>
        <th>January
            <?php
            if(isset($_GET['year']))
            {
                echo $_GET['year'];
                $jaar_voor_query = $_GET['year'];
            }
            else { echo $current_year;
                $jaar_voor_query = $current_year;
            }
            ?></th>
        <th>February
            <?php
            if(isset($_GET['year']))
            {
                echo $_GET['year'];
                $jaar_voor_query = $_GET['year'];
            }
            else { echo $current_year;
                $jaar_voor_query = $current_year;
            }
            ?></th>
        <th>March
            <?php
            if(isset($_GET['year']))
            {
                echo $_GET['year'];
                $jaar_voor_query = $_GET['year'];
            }
            else { echo $current_year;
                $jaar_voor_query = $current_year;
            }
            ?></th>
        <th>April
            <?php
            if(isset($_GET['year']))
            {
                echo $_GET['year'];
                $jaar_voor_query = $_GET['year'];
            }
            else { echo $current_year;
                $jaar_voor_query = $current_year;
            }
            ?></th>
        <th>May
            <?php
            if(isset($_GET['year']))
            {
                echo $_GET['year'];
                $jaar_voor_query = $_GET['year'];
            }
            else { echo $current_year;
                $jaar_voor_query = $current_year;
            }
            ?></th>
        <th>June
            <?php
            if(isset($_GET['year']))
            {
                echo $_GET['year'];
                $jaar_voor_query = $_GET['year'];
            }
            else { echo $current_year;
                $jaar_voor_query = $current_year;
            }
            ?></th>
        <th>July     <?php
            if(isset($_GET['year']))
            {
                echo $_GET['year'];
                $jaar_voor_query = $_GET['year'];
            }
            else { echo $current_year;
                $jaar_voor_query = $current_year;
            }
            ?></th>
        <th>August
            <?php
            if(isset($_GET['year']))
            {
                echo $_GET['year'];
                $jaar_voor_query = $_GET['year'];
            }
            else { echo $current_year;
                $jaar_voor_query = $current_year;
            }
            ?></th>
        <th>September
            <?php
            if(isset($_GET['year']))
            {
                echo $_GET['year'];
                $jaar_voor_query = $_GET['year'];
            }
            else { echo $current_year;
                $jaar_voor_query = $current_year;
            }
            ?></th>
        <th>October
            <?php
            if(isset($_GET['year']))
            {
                echo $_GET['year'];
                $jaar_voor_query = $_GET['year'];
            }
            else { echo $current_year;
                $jaar_voor_query = $current_year;
            }
            ?></th>
        <th>November     <?php
            if(isset($_GET['year']))
            {
                echo $_GET['year'];
                $jaar_voor_query = $_GET['year'];
            }
            else { echo $current_year;
                $jaar_voor_query = $current_year;
            }
            ?></th>
        <th>December
            <?php
            if(isset($_GET['year']))
            {
                echo $_GET['year'];
                $jaar_voor_query = $_GET['year'];
            }
            else { echo $current_year;
                $jaar_voor_query = $current_year;
            }
            ?></th>
    </tr>

    <?php

    $query = "SELECT * FROM reporting_partners WHERE active='yes' ORDER BY partner_company ASC"; // alfabetisch gerangschikt, enkel actieve partners
    $result = mysql_query($query);

    while ($row = mysql_fetch_assoc($result)) {

        echo "<tr>";
        echo "<td>".$row['partner_company']."</td>";
        echo "<td>";

        $query_jan = "SELECT * FROM reporting_report WHERE month ='January' AND partner_id ='" . $row['partner_id'] . "' AND year = '$jaar_voor_query'";
        $result_jan = mysql_query($query_jan);
        $aantal_jan = mysql_num_rows($result_jan);
        if($aantal_jan > 0) // als er een record is voor de gekozen maand, partner en jaar dan is deze ingevuld
        {
            echo "<img src=\"../../images/checkmark.gif\" alt=\"Report has been filled in.\" />";
        }
        else {
            echo "<img src=\"../../images/x.gif\" alt=\"Report has not been filled in.\" />";
        }

        echo "</td>";
        echo "<td>";

        $query_jan = "SELECT * FROM reporting_report WHERE month ='February' AND partner_id ='" . $row['partner_id'] . "' AND year = '$jaar_voor_query'";
        $result_jan = mysql_query($query_jan);
        $aantal_jan = mysql_num_rows($result_jan);
        if($aantal_jan > 0) // als er een record is voor de gekozen maand, partner en jaar dan is deze ingevuld
        {
            echo "<img src=\"../../images/checkmark.gif\" alt=\"Report has been filled in.\" />";
        }
        else {
            echo "<img src=\"../../images/x.gif\" alt=\"Report has not been filled in.\" />";
        }

        echo "</td>";
        echo "<td>";

        $query_jan = "SELECT * FROM reporting_report WHERE month ='March' AND partner_id ='" . $row['partner_id'] . "' AND year = '$jaar_voor_query'";
        $result_jan = mysql_query($query_jan);
        $aantal_jan = mysql_num_rows($result_jan);
        if($aantal_jan > 0) // als er een record is voor de gekozen maand, partner en jaar dan is deze ingevuld
        {
            echo "<img src=\"../../images/checkmark.gif\" alt=\"Report has been filled in.\" />";
        }
        else {
            echo "<img src=\"../../images/x.gif\" alt=\"Report has not been filled in.\" />";
        }

        echo "</td>";
        echo "<td>";

        $query_jan = "SELECT * FROM reporting_report WHERE month ='April' AND partner_id ='" . $row['partner_id'] . "' AND year = '$jaar_voor_query'";
        $result_jan = mysql_query($query_jan);
        $aantal_jan = mysql_num_rows($result_jan);
        if($aantal_jan > 0) // als er een record is voor de gekozen maand, partner en jaar dan is deze ingevuld
        {
            echo "<img src=\"../../images/checkmark.gif\" alt=\"Report has been filled in.\" />";
        }
        else {
            echo "<img src=\"../../images/x.gif\" alt=\"Report has not been filled in.\" />";
        }

        echo "</td>";
        echo "<td>";

        $query_jan = "SELECT * FROM reporting_report WHERE month ='May' AND partner_id ='" . $row['partner_id'] . "' AND year = '$jaar_voor_query'";
        $result_jan = mysql_query($query_jan);
        $aantal_jan = mysql_num_rows($result_jan);
        if($aantal_jan > 0) // als er een record is voor de gekozen maand, partner en jaar dan is deze ingevuld
        {
            echo "<img src=\"../../images/checkmark.gif\" alt=\"Report has been filled in.\" />";
        }
        else {
            echo "<img src=\"../../images/x.gif\" alt=\"Report has not been filled in.\" />";
        }

        echo "</td>";
        echo "<td>";

        $query_jan = "SELECT * FROM reporting_report WHERE month ='June' AND partner_id ='" . $row['partner_id'] . "' AND year = '$jaar_voor_query'";
        $result_jan = mysql_query($query_jan);
        $aantal_jan = mysql_num_rows($result_jan);
        if($aantal_jan > 0) // als er een record is voor de gekozen maand, partner en jaar dan is deze ingevuld
        {
            echo "<img src=\"../../images/checkmark.gif\" alt=\"Report has been filled in.\" />";
        }
        else {
            echo "<img src=\"../../images/x.gif\" alt=\"Report has not been filled in.\" />";
        }

        echo "</td>";
        echo "<td>";

        $query_jan = "SELECT * FROM reporting_report WHERE month ='July' AND partner_id ='" . $row['partner_id'] . "' AND year = '$jaar_voor_query'";
        $result_jan = mysql_query($query_jan);
        $aantal_jan = mysql_num_rows($result_jan);
        if($aantal_jan > 0) // als er een record is voor de gekozen maand, partner en jaar dan is deze ingevuld
        {
            echo "<img src=\"../../images/checkmark.gif\" alt=\"Report has been filled in.\" />";
        }
        else {
            echo "<img src=\"../../images/x.gif\" alt=\"Report has not been filled in.\" />";
        }

        echo "</td>";
        echo "<td>";

        $query_jan = "SELECT * FROM reporting_report WHERE month ='August' AND partner_id ='" . $row['partner_id'] . "' AND year = '$jaar_voor_query'";
        $result_jan = mysql_query($query_jan);
        $aantal_jan = mysql_num_rows($result_jan);
        if($aantal_jan > 0) // als er een record is voor de gekozen maand, partner en jaar dan is deze ingevuld
        {
            echo "<img src=\"../../images/checkmark.gif\" alt=\"Report has been filled in.\" />";
        }
        else {
            echo "<img src=\"../../images/x.gif\" alt=\"Report has not been filled in.\" />";
        }

        echo "</td>";
        echo "<td>";

        $query_jan = "SELECT * FROM reporting_report WHERE month ='September' AND partner_id ='" . $row['partner_id'] . "' AND year = '$jaar_voor_query'";
        $result_jan = mysql_query($query_jan);
        $aantal_jan = mysql_num_rows($result_jan);
        if($aantal_jan > 0) // als er een record is voor de gekozen maand, partner en jaar dan is deze ingevuld
        {
            echo "<img src=\"../../images/checkmark.gif\" alt=\"Report has been filled in.\" />";
        }
        else {
            echo "<img src=\"../../images/x.gif\" alt=\"Report has not been filled in.\" />";
        }

        echo "</td>";
        echo "<td>";

        $query_jan = "SELECT * FROM reporting_report WHERE month ='October' AND partner_id ='" . $row['partner_id'] . "' AND year = '$jaar_voor_query'";
        $result_jan = mysql_query($query_jan);
        $aantal_jan = mysql_num_rows($result_jan);
        if($aantal_jan > 0) // als er een record is voor de gekozen maand, partner en jaar dan is deze ingevuld
        {
            echo "<img src=\"../../images/checkmark.gif\" alt=\"Report has been filled in.\" />";
        }
        else {
            echo "<img src=\"../../images/x.gif\" alt=\"Report has not been filled in.\" />";
        }

        echo "</td>";
        echo "<td>";

        $query_jan = "SELECT * FROM reporting_report WHERE month ='November' AND partner_id ='" . $row['partner_id'] . "' AND year = '$jaar_voor_query'";
        $result_jan = mysql_query($query_jan);
        $aantal_jan = mysql_num_rows($result_jan);
        if($aantal_jan > 0) // als er een record is voor de gekozen maand, partner en jaar dan is deze ingevuld
        {
            echo "<img src=\"../../images/checkmark.gif\" alt=\"Report has been filled in.\" />";
        }
        else {
            echo "<img src=\"../../images/x.gif\" alt=\"Report has not been filled in.\" />";
        }

        echo "</td>";
        echo "<td>";

        $query_jan = "SELECT * FROM reporting_report WHERE month ='December' AND partner_id ='" . $row['partner_id'] . "' AND year = '$jaar_voor_query'";
        $result_jan = mysql_query($query_jan);
        $aantal_jan = mysql_num_rows($result_jan);
        if($aantal_jan > 0) // als er een record is voor de gekozen maand, partner en jaar dan is deze ingevuld
        {
            echo "<img src=\"../../images/checkmark.gif\" alt=\"Report has been filled in.\" />";
        }
        else {
            echo "<img src=\"../../images/x.gif\" alt=\"Report has not been filled in.\" />";
        }

        echo "</td>";

        echo "</tr>\n";
    }
    ?>

    </table>



<?php
}
?>

</div>
</div>
<!-- /block -->


</div>
</div>
</div>
</div>
<hr>
<?php
include("../footer.php");
?>
