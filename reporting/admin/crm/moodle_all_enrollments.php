<?php
$rootdir="../../";
$page="All enrollments";
include("../connect_db234.php");
include("../header.php");
?>

<body>
<?php
include("../topnav.php");
?>
<div class="container-fluid">
            <div class="row-fluid">
                
                <?php
				include("../sidenav.php");
				?>
                
                <div class="span10" id="content">
                    <div class="row-fluid">
                    	<div class="span12">
                        
                            <!-- block -->
                            <div class="block">
                                <div class="navbar navbar-inner block-header">
                                <h3>All enrollments</h3>     
                                </div>
                                <div class="block-content collapse in">
<?php 
if (!isset($_SESSION["username"])){
	echo $niet_ingelogd;
}
// hieronder staan de functies die toegelaten worden tot deze pagina
elseif($_SESSION["function"] != "admin"){
	echo $geen_toegang;
}else{
?>
<?php

/*$db = mysql_connect("localhost","academy","vapob2Shlo") or die ("The connection to the database could not be established. Please try again later or inform info@informationmapping.com");
mysql_set_charset('utf8',$db); // added to avoid problems in the SQL DB with e.g. Nestlé
mysql_select_db("academy",$db);*/

function replace_enrolid($nummerke)
{
	$sql_user = mysql_query("SELECT enr.id, enr.courseid, cou.id, cou.fullname FROM mdl_enrol AS enr, mdl_course AS cou WHERE enr.courseid = cou.id && enr.id = '$nummerke'", $db2);
	$report_user = mysql_fetch_assoc($sql_user);
	
	$out = $report_user['fullname'];
	return $out;
}
?>

Filter results: <form action="" method="post">
Company: <input type="text" name="company" />
<select name="course">
<option value="">Select course</option>
<?php
// enrolid
$sql_enrolid = mysql_query("SELECT enr.id AS idee, enr.courseid, cou.id, cou.fullname FROM mdl_enrol AS enr, mdl_course AS cou WHERE enr.courseid = cou.id ORDER BY cou.fullname ASC", $db2);
while($row_enrolid = mysql_fetch_array($sql_enrolid))
{
	echo "<option value=" . $row_enrolid['idee'] . ">" . $row_enrolid['fullname'] . "</option>\n";
}
?>
</select>
<input type="submit" name="verzenden" value="OK" class="btn" />
</form>


<table class="table table-striped">
<tr><th>User</th><th>Email</th><th>First name</th><th>Last name</th><th>Course</th><th>Start date enrollment</th><th>End date enrollment</th></tr>
<?php

$current_date = date("Ymd");

if($_POST['verzenden'])
{
	$company = mysql_real_escape_string($_POST['company']);
	$course = mysql_real_escape_string($_POST['course']);
	
	if(!empty($company)) { $sql = mysql_query("SELECT enr.id, enr.enrolid, enr.userid, enr.timestart, enr.timeend, usr.id, usr.firstname, usr.lastname, usr.email, usr.institution FROM mdl_user_enrolments AS enr, mdl_user AS usr WHERE enr.userid = usr.id && usr.email LIKE '%$company%' ORDER BY enr.timeend"); }
	if(!empty($course)) { $sql = mysql_query("SELECT enr.id, enr.enrolid, enr.userid, enr.timestart, enr.timeend, usr.id, usr.firstname, usr.lastname, usr.email, usr.institution FROM mdl_user_enrolments AS enr, mdl_user AS usr WHERE enr.userid = usr.id && enr.enrolid = '$course' ORDER BY enr.timeend"); }
	if(!empty($course) && !empty($company)) { $sql = mysql_query("SELECT enr.id, enr.enrolid, enr.userid, enr.timestart, enr.timeend, usr.id, usr.firstname, usr.lastname, usr.email, usr.institution FROM mdl_user_enrolments AS enr, mdl_user AS usr WHERE enr.userid = usr.id && usr.email LIKE '%$company%' && enr.enrolid = '$course' ORDER BY enr.timeend"); }
	

}
else {
$sql = mysql_query("SELECT enr.id, enr.enrolid, enr.userid, enr.timestart, enr.timeend, usr.id, usr.firstname, usr.lastname, usr.email, usr.institution FROM mdl_user_enrolments AS enr, mdl_user AS usr WHERE enr.userid = usr.id ORDER BY enr.timeend", $db2);
}
// in moodle, dates are stored as bigint in the mdl_user_enrolments table, so you cannot use built in SQL date functions
while($enr=mysql_fetch_array($sql)){
	
	$date = date("Ymd", $enr['timeend']); // convert unix timestamp in Moodle db to readable format
	$enrolid = $enr['enrolid'];
	

			echo "<tr><td> " . $enr['userid'] . "</td>";
			$id = $enr['userid'];
			echo "<td>" . $enr['email'] . "</td>";
			echo "<td>" . $enr['firstname'] . "</td>";
			echo "<td>" . $enr['lastname'] . "</td>";
			echo "<td>" . replace_enrolid($enrolid) . "</td>";
			echo "<td>" . date("Y F d", $enr['timestart']) . "</td>";
			
			
			echo "<td>";
			if($enr['timeend'] == "0")
			{
				echo "no end date";
			}
			else {
			echo date("Y F d", $enr['timeend']);
			}
			echo "</td></tr>";
}

 ?>
</table>

<?php
}
?>
                                </div>
                            </div>
                            <!-- /block -->


</div>
</div>
</div>
</div>
            <hr>
<?php
include("../footer.php");
?>
</html>