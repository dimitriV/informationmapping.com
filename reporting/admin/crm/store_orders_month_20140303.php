<?php
$rootdir="../../";
$page="Customers: buyers per product";
include("../connect.php");
include("../header.php");
?>

<body>
<?php 
if (!isset($_SESSION["username"])){
	echo $niet_ingelogd;
}
// hieronder staan de functies die toegelaten worden tot deze pagina
elseif($_SESSION["function"] != "admin"){
	echo $geen_toegang;
}else{
?>
<?php
$month = isset($_POST['month']) ? $_POST['month'] : '';
$year = isset($_POST['year']) ? $_POST['year'] : '';
?>
<form action="" method="post">
<select name="month">
<option value="1" <?php if($month== "1") { echo "selected='selected'";} ?>>January</option>
<option value="2" <?php if($month== "2") { echo "selected='selected'";} ?>>February</option>
<option value="3" <?php if($month== "3") { echo "selected='selected'";} ?>>March</option>
<option value="4" <?php if($month== "4") { echo "selected='selected'";} ?>>April</option>
<option value="5" <?php if($month== "5") { echo "selected='selected'";} ?>>May</option>
<option value="6" <?php if($month== "6") { echo "selected='selected'";} ?>>June</option>
<option value="7" <?php if($month== "7") { echo "selected='selected'";} ?>>July</option>
<option value="8" <?php if($month== "8") { echo "selected='selected'";} ?>>August</option>
<option value="9" <?php if($month== "9") { echo "selected='selected'";} ?>>September</option>
<option value="10" <?php if($month== "10") { echo "selected='selected'";} ?>>October</option>
<option value="11" <?php if($month== "11") { echo "selected='selected'";} ?>>November</option>
<option value="12" <?php if($month== "12") { echo "selected='selected'";} ?>>December</option>
</select>

<select name="year">
<option value="2013" <?php if($year== "2013") { echo "selected='selected'";} ?>>2013</option>
<option value="2014" <?php if($year== "2014") { echo "selected='selected'";} ?>>2014</option>
<option value="2015" <?php if($year== "2015") { echo "selected='selected'";} ?>>2015</option>
<option value="2016" <?php if($year== "2016") { echo "selected='selected'";} ?>>2016</option>
</select>
Euro to dollar rate: <input type="text" name="convertrate" size="5" value="<?php if($_POST['verzenden']) { echo $_POST['convertrate']; } else { echo "1,3"; } ?>" />
<input type="submit" name="verzenden" value="Search" />
</form>
<p>
  <?php

// DIT SCRIPT HOUDT REKENING MET REFUNDS
// deze krijgen de status Closed en zijn dus niet opgenomen in de $result_url query

$verzenden = isset($_POST['verzenden']) ? $_POST['verzenden'] : '';
if($verzenden)
{
	//$month = $_POST['month'];
	//$year = $_POST['year'];
	if($month == "1") { $start_date = ($year-1)."-12-31 23:00:00"; $end_date = $year."-01-31 23:59:59"; }
	if($month == "2") { $start_date = $year."-01-31 23:00:00"; $end_date = $year."-02-28 23:59:59"; }
	if($month == "3") { $start_date = $year."-02-31 23:00:00"; $end_date = $year."-03-31 23:59:59"; }
	if($month == "4") { $start_date = $year."-03-31 23:00:00"; $end_date = $year."-04-31 23:59:59"; }
	if($month == "5") { $start_date = $year."-04-31 23:00:00"; $end_date = $year."-05-31 23:59:59"; }
	if($month == "6") { $start_date = $year."-05-31 23:00:00"; $end_date = $year."-06-31 23:59:59"; }
	if($month == "7") { $start_date = $year."-06-31 23:00:00"; $end_date = $year."-07-31 23:59:59"; }
	if($month == "8") { $start_date = $year."-07-31 23:00:00"; $end_date = $year."-08-31 23:59:59"; }
	if($month == "9") { $start_date = $year."-08-31 23:00:00"; $end_date = $year."-09-31 23:59:59"; }
	if($month == "10") { $start_date = $year."-09-31 23:00:00"; $end_date = $year."-10-31 23:59:59"; }
	if($month == "11") { $start_date = $year."-10-31 23:00:00"; $end_date = $year."-11-31 23:59:59"; }
	if($month == "12") { $start_date = $year."-11-31 23:00:00"; $end_date = $year."-12-31 23:59:59"; }
	
	$convertrate = isset($_POST['convertrate']) ? $_POST['convertrate'] : '';
	
	$convertrate = str_replace(',','.',$convertrate); // bedrag moet in punt ingegeven worden, converteer komma
	$convertrate = 1 / $convertrate; // euro to dollar rate e.g. 1,3
}
else
{

// may
//$start_date = "2013-04-31 23:00:00"; 
//$end_date = "2013-05-31 23:59:59";
}
// april
//$start_date = "2013-03-31 23:00:00"; // DB+1 hour = value shown in frontend, tested and verified to be correct
//$end_date = "2013-04-31 23:59:59"; // not tested yet, could be another hour format to be entered here

$result_url = mysql_query("SELECT *, DATE_FORMAT(created_at, '%d/%m/%Y') AS newDate FROM sales_flat_order WHERE status = 'complete' && created_at > '$start_date' && created_at < '$end_date'");
// issue met orders die geplaatst werden op de 1e van de maand die in AM gekocht werden
// MONTH(created_at) = '2' && YEAR(created_at) = '2013'

$aantal_sub = mysql_num_rows($result_url); 
echo "Results for this query: " . $aantal_sub;

function replace_store($code)
{
	if($code == "17") {$storeview = str_replace("17", "USD Store view", $code); }
	if($code == "5") { $storeview = str_replace("5", "US Store view", $code); }
	if($code == "1") { $storeview = str_replace("1", "Default Store view", $code); }
	if($code == "16") { $storeview = str_replace("16", "EUR Store view", $code); }

	return $storeview;
}


function replace_amounts($nummerke)
{
	
	$nummer = number_format($nummerke, 2, ',', ''); // afgerond op 2 cijfers na de komma
	return $nummer;
}
?>
  
</p>
<table>
  <tr>
    <th>Order #</th>
    <th>Partner</th>
    <th>Date</th>
    <!--<th>Store view</th>-->

    <th>Qty</th>
    <th>Item price</th>
    <!--<th>Total EUR<br />
    excl. VAT</th>-->
    <th>Total excl VAT</th>
    <th>VAT</th>
    <th>Total incl VAT</th>
    <th>EURO</th>
    <!--<th>Customer name</th>-->
    <th>Customer Email</th>
    <th>Billing Name</th>
    <th>Billing Company</th>
    <th>Billing Street</th>
    <th>Billing Zip</th>
    <th>Billing City</th>
    <th>Billing Country</th>
    <th>Billing phone</th>
    <th>Product</th>
    <th>SKU</th>
  </tr>
  
  	<?php
	if(!empty($_POST['verzenden']))
	{
	
	$totaal_euro=0;
	$totaal_usd=0;
	
	while($report=mysql_fetch_array($result_url)){

	$ordernummer = $report['entity_id']; // order nummer, kan in sales_flat_order_item gebruikt worden als order_id

$query2_result = mysql_query("SELECT * FROM sales_flat_order_item WHERE order_id = '$ordernummer' && product_type='bundle' 
OR (order_id = '$ordernummer' && product_id = '294' && product_type = 'virtual') 
OR (order_id = '$ordernummer' && product_id = '275' && product_type = 'virtual') 
OR (order_id = '$ordernummer' && product_id = '222' && product_type = 'virtual') 
OR (order_id = '$ordernummer' && product_id = '188' && product_type = 'virtual') 
OR (order_id = '$ordernummer' && product_id = '407' && product_type = 'virtual') 
OR (order_id = '$ordernummer' && product_id = '485' && product_type = 'virtual')
OR (order_id = '$ordernummer' && product_id = '189' && product_type = 'virtual')"); // alleen de bundles nemen, uitzondering is product 294 omdat die rechtstreeks als virtual product te koop is, alsook product 275, alsook product 222, alsook 188, 407
while($report2=mysql_fetch_array($query2_result)){

$ordernummer = $report2['order_id'];
  echo "<tr>";
    echo "<td>" . $report['increment_id'] . "</td>";
	
	$customer = $report['customer_id']; // customer id gebruiken om partner weer te geven
	// uitlezen partner die bij user hoort is in customer_entity_int where attribute_id = 545
	// de value wordt dan in partners uitgelezen
	
	$result_partner = mysql_query("SELECT * FROM customer_entity_int WHERE entity_id = '$customer' && attribute_id = '545'");
	$row_partner=mysql_fetch_array($result_partner);
	$partner_id = $row_partner['value'];
		
if($partner_id)
{
	$result_partner2 = mysql_query("SELECT * FROM partner WHERE partner_id = '$partner_id'");
	$row_partner2=mysql_fetch_array($result_partner2);
	echo "<td>" . $row_partner2['name'] . "</td>";
}
else {
echo "<td>None</td>";	
}
	
		echo "<td>" . $report['newDate'] . "</td>"; // created_at
    
	

	
	echo "<td>" . round($report2['qty_ordered'],0) . "</td>"; // er zijn ook nog andere qty velden in de table!!
	

	if($report['store_id'] == "17" || $report['store_id'] == "5")
	{
		
		echo "<td>$ ";
		echo replace_amounts($report2['base_price']); // item price, voor display mag afgerond worden, niet voor berekeningen mee te maken!
		echo "</td>";
		// discount amount kan je halen uit sales_flat_order, maar mag enkel afgetrokken worden op het Bundle product, niet op het virtual product ook nog eens
		if($report2['product_type']=="bundle")
		{
		$usd = $report2['row_total']+$report['base_discount_amount']; // plus because discount amount is a negative number
		}
		else
		{
		$usd = $report2['row_total'];
		}
	  $omgerekend = $usd; // excluded vat
	
		//$usd_formatted = replace_amounts($usd); // formatted is variable just for display, if you need mathematical functions you cannot use the number_format function
		$usd_formatted = replace_amounts($omgerekend);
		$totaal_usd+=$usd;
		echo "<td>";
	  echo "$ " . $usd_formatted; // total excl vat
	  echo "</td>";
	  echo "<td>";
	  echo "$ " . replace_amounts($report2['base_tax_amount']); // vat
	  echo "</td>";
	  echo "<td>$ "; // total incl VAT
	  //echo replace_amounts($usd+round($report2['base_tax_amount'],2)); // echo $usd+round($report2['base_tax_amount'],2);
	  $subtotal_inclvat = $usd+$report2['base_tax_amount'];
	  $new_total = $subtotal_inclvat;
	  echo replace_amounts($new_total);
	  echo "</td>";
	  echo "<td>€ " . replace_amounts($usd_formatted*$convertrate) . "</td>"; // convertion to euro

	}
	else
	{
		
		echo "<td>€ ";
		echo replace_amounts($report2['base_price']); // item price, afgerond op 2 cijfers en in formaat 2344,75
		echo "</td>";
		if($report2['product_type']=="bundle")
		{
		$euro = $report2['row_total']+$report['base_discount_amount']; // plus because discount amount is a negative number
		}
		else
		{
			$euro = $report2['row_total'];
		}
		$euro_formatted = replace_amounts($euro);
		$totaal_euro+=$euro;
		echo "<td>";
	  echo "€ " . $euro_formatted;  
	  echo "</td>";
	  echo "<td>";
	  echo "€ " . replace_amounts($report2['base_tax_amount']);	
	  echo "</td>"; 
	  echo "<td>€ "; // total incl VAT
	  echo replace_amounts($euro+round($report2['base_tax_amount'],2));
	  echo "</td>";
	  echo "<td>€ $euro_formatted</td>";
	}	

$res_guestname = mysql_query("SELECT parent_id, lastname, firstname, street, postcode, city, country_id, telephone, company FROM sales_flat_order_address WHERE parent_id = '$ordernummer'");
$row_guestname=mysql_fetch_assoc($res_guestname);
	// als het een gast is, dan is klantaccountnaam niet ingevuld 
	//echo "<td>" . $report['customer_firstname'] . " " . $report['customer_lastname'] . "</td>";
	echo "<td>" . $report['customer_email'] . "</td>";
	echo "<td>" . $row_guestname['firstname'] . " " . $row_guestname['lastname'] . "</td>";
	echo "<td>" . $row_guestname['company']  . "</td>";
	echo "<td>" . $row_guestname['street']  . "</td>";
	echo "<td>" . $row_guestname['postcode']  . "</td>";
	echo "<td>" . $row_guestname['city']  . "</td>";
	echo "<td>" . $row_guestname['country_id']  . "</td>";
	echo "<td>" . $row_guestname['telephone']  . "</td>";	

	
	echo "<td>" . $report2['name'] . "</td>"; // productnaam
	echo "<td>" . $report2['sku'] . "</td>"; // productnaam
  echo "</tr>";
} // einde while2


?>
  
  

  <?php
	}
	} // einde if empty check
	?>
</table>
<?php
echo "SUBTOTAL excl. VAT EUR: € " . replace_amounts($totaal_euro);
echo "<br />";
echo "SUBTOTAL excl. VAT USD: $ " . replace_amounts($totaal_usd) . " or € " . replace_amounts($totaal_usd*$convertrate);
echo "<br />";
echo "<strong>TOTAL excl. VAT EUR: € " . replace_amounts($totaal_euro+($totaal_usd*$convertrate));
echo "</strong>";
?>
<p>Remark: Report takes into account all Bundle products + Virtual products: Seminar FS Pro 4.2/4.3 + IMP Exam + e-Handbook + Certification products. Due to changes in products, this report is only valid for data starting from May 2013.</p>
<?php
}
?>
</body>
</html>