<?php
$rootdir="../../";
$page="Admin: create student keys";
include("../connect.php");
include("../header.php");
?>

<body>
<?php
include("../topnav.php");
?>
<div class="container-fluid">
            <div class="row-fluid">
                
                <?php
				include("../sidenav.php");
				?>
                
                <div class="span10" id="content">
                    <div class="row-fluid">
                    	<div class="span12">
                        
                            <!-- block -->
                            <div class="block">
                                <div class="navbar navbar-inner block-header">
                                <h3>Overview student keys</h3>     
                                </div>
                                <div class="block-content collapse in">
<?php 
if (!isset($_SESSION["username"])){
	echo $niet_ingelogd;
}
// hieronder staan de functies die toegelaten worden tot deze pagina
elseif($_SESSION["function"] != "admin"){
	echo $geen_toegang;
}else{
?>


    <p>Total number in DB: <? $keys = mysql_query("SELECT tk_id FROM reporting_trainingkey");
$aantal_keys = mysql_num_rows($keys); echo $aantal_keys; ?></p>
	<table class="table table-striped">
  <tr>
    <th></th>
    <th>IMF</th>
    <th>MSPP</th>
    <th>MPPD</th>
    <th>PL</th>
    <th>MBCO</th>
    <th>EECD</th>
    <th>NS1</th>
    <th>NS2</th>
    <th>NSA</th>
  </tr>
   <tr>
    <td width="180"><strong>Aracane</strong></td>
    <td><a href="studentkey_detail.php?partner_id=6&course_id=18"><? $ara_imf = mysql_query("SELECT tk_id FROM reporting_trainingkey WHERE partner_id='6' AND course_id='18'");
$aantal_ara_imf = mysql_num_rows($ara_imf); echo $aantal_ara_imf; ?></a></td>
    <td><a href="studentkey_detail.php?partner_id=6&course_id=32"><? $ara_mspp = mysql_query("SELECT tk_id FROM reporting_trainingkey WHERE partner_id='6' AND course_id='32'");
$aantal_ara_mspp = mysql_num_rows($ara_mspp); echo $aantal_ara_mspp; ?></a></td>
    <td><a href="studentkey_detail.php?partner_id=6&course_id=33"><? $ara_mppd = mysql_query("SELECT tk_id FROM reporting_trainingkey WHERE partner_id='6' AND course_id='33'");
$aantal_ara_mppd = mysql_num_rows($ara_mppd); echo $aantal_ara_mppd; ?></a></td>
    <td><a href="studentkey_detail.php?partner_id=6&course_id=30"><? $ara_pl = mysql_query("SELECT tk_id FROM reporting_trainingkey WHERE partner_id='6' AND course_id='30'");
$aantal_ara_pl = mysql_num_rows($ara_pl); echo $aantal_ara_pl; ?></a></td>
    <td><a href="studentkey_detail.php?partner_id=6&course_id=16"><? $ara_mbco = mysql_query("SELECT tk_id FROM reporting_trainingkey WHERE partner_id='6' AND course_id='16'");
$aantal_ara_mbco = mysql_num_rows($ara_mbco); echo $aantal_ara_mbco; ?></a></td>
<td>&nbsp;</td>
<td>&nbsp;</td>
<td>&nbsp;</td>
<td>&nbsp;</td>
  </tr>
     <tr>
    <td><strong>Contexto Didactico</strong></td>
    <td><a href="studentkey_detail.php?partner_id=18&course_id=18"><? $con_imf = mysql_query("SELECT tk_id FROM reporting_trainingkey WHERE partner_id='18' AND course_id='18'");
$aantal_con_imf = mysql_num_rows($con_imf); echo $aantal_con_imf; ?></a></td>
    <td><a href="studentkey_detail.php?partner_id=18&course_id=32"><? $con_mspp = mysql_query("SELECT tk_id FROM reporting_trainingkey WHERE partner_id='18' AND course_id='32'");
$aantal_con_mspp = mysql_num_rows($con_mspp); echo $aantal_con_mspp; ?></a></td>
    <td><a href="studentkey_detail.php?partner_id=18&course_id=33"><? $con_mppd = mysql_query("SELECT tk_id FROM reporting_trainingkey WHERE partner_id='18' AND course_id='33'");
$aantal_con_mppd = mysql_num_rows($con_mppd); echo $aantal_con_mppd; ?></a></td>
    <td><a href="studentkey_detail.php?partner_id=18&course_id=30"><? $con_pl = mysql_query("SELECT tk_id FROM reporting_trainingkey WHERE partner_id='18' AND course_id='30'");
$aantal_con_pl = mysql_num_rows($con_pl); echo $aantal_con_pl; ?></a></td>
    <td><a href="studentkey_detail.php?partner_id=18&course_id=16"><? $con_mbco = mysql_query("SELECT tk_id FROM reporting_trainingkey WHERE partner_id='18' AND course_id='16'");
$aantal_con_mbco = mysql_num_rows($con_mbco); echo $aantal_con_mbco; ?></a></td>
<td>&nbsp;</td>
<td>&nbsp;</td>
<td>&nbsp;</td>
<td>&nbsp;</td>
  </tr>
   <tr>
    <td><strong>IMI</strong></td>
    <td><a href="studentkey_detail.php?partner_id=28&course_id=18"><? $imi_imf = mysql_query("SELECT tk_id FROM reporting_trainingkey WHERE partner_id='28' AND course_id='18'");
$aantal_imi_imf = mysql_num_rows($imi_imf); echo $aantal_imi_imf; ?></a></td>
    <td><a href="studentkey_detail.php?partner_id=28&course_id=32"><? $imi_mspp = mysql_query("SELECT tk_id FROM reporting_trainingkey WHERE partner_id='28' AND course_id='32'");
$aantal_imi_mspp = mysql_num_rows($imi_mspp); echo $aantal_imi_mspp; ?></a></td>
    <td><a href="studentkey_detail.php?partner_id=28&course_id=33"><? $imi_mppd = mysql_query("SELECT tk_id FROM reporting_trainingkey WHERE partner_id='28' AND course_id='33'");
$aantal_imi_mppd = mysql_num_rows($imi_mppd); echo $aantal_imi_mppd; ?></a></td>
    <td><a href="studentkey_detail.php?partner_id=28&course_id=30"><? $imi_pl = mysql_query("SELECT tk_id FROM reporting_trainingkey WHERE partner_id='28' AND course_id='30'");
$aantal_imi_pl = mysql_num_rows($imi_pl); echo $aantal_imi_pl; ?></a></td>
    <td><a href="studentkey_detail.php?partner_id=28&course_id=16"><? $imi_mbco = mysql_query("SELECT tk_id FROM reporting_trainingkey WHERE partner_id='28' AND course_id='16'");
$aantal_imi_mbco = mysql_num_rows($imi_mbco); echo $aantal_imi_mbco; ?></a></td>
<td>&nbsp;</td>
<td>&nbsp;</td>
<td>&nbsp;</td>
<td>&nbsp;</td>
  </tr>
     <tr>
    <td><strong>Indocita</strong></td>
    <td><a href="studentkey_detail.php?partner_id=31&course_id=18"><? $ind_imf = mysql_query("SELECT tk_id FROM reporting_trainingkey WHERE partner_id='31' AND course_id='18'");
$aantal_ind_imf = mysql_num_rows($ind_imf); echo $aantal_ind_imf; ?></a></td>
    <td><a href="studentkey_detail.php?partner_id=31&course_id=32"><? $ind_mspp = mysql_query("SELECT tk_id FROM reporting_trainingkey WHERE partner_id='31' AND course_id='32'");
$aantal_ind_mspp = mysql_num_rows($ind_mspp); echo $aantal_ind_mspp; ?></a></td>
    <td><a href="studentkey_detail.php?partner_id=31&course_id=33"><? $ind_mppd = mysql_query("SELECT tk_id FROM reporting_trainingkey WHERE partner_id='31' AND course_id='33'");
$aantal_ind_mppd = mysql_num_rows($ind_mppd); echo $aantal_ind_mppd; ?></a></td>
    <td><a href="studentkey_detail.php?partner_id=31&course_id=30"><? $ind_pl = mysql_query("SELECT tk_id FROM reporting_trainingkey WHERE partner_id='31' AND course_id='30'");
$aantal_ind_pl = mysql_num_rows($ind_pl); echo $aantal_ind_pl; ?></a></td>
    <td><a href="studentkey_detail.php?partner_id=31&course_id=16"><? $ind_mbco = mysql_query("SELECT tk_id FROM reporting_trainingkey WHERE partner_id='31' AND course_id='16'");
$aantal_ind_mbco = mysql_num_rows($ind_mbco); echo $aantal_ind_mbco; ?></a></td>
<td>&nbsp;</td>
<td>&nbsp;</td>
<td>&nbsp;</td>
<td>&nbsp;</td>
  </tr>
  <tr>
    <td><strong>Information Mapping CA</strong></td>
    <td><a href="studentkey_detail.php?partner_id=2&course_id=18"><? $imca_imf = mysql_query("SELECT tk_id FROM reporting_trainingkey WHERE partner_id='2' AND course_id='18'");
$aantal_imca_imf = mysql_num_rows($imca_imf); echo $aantal_imca_imf; ?></a></td>
    <td><a href="studentkey_detail.php?partner_id=2&course_id=32"><? $imca_mspp = mysql_query("SELECT tk_id FROM reporting_trainingkey WHERE partner_id='2' AND course_id='32'");
$aantal_imca_mspp = mysql_num_rows($imca_mspp); echo $aantal_imca_mspp; ?></a></td>
    <td><a href="studentkey_detail.php?partner_id=2&course_id=33"><? $imca_mppd = mysql_query("SELECT tk_id FROM reporting_trainingkey WHERE partner_id='2' AND course_id='33'");
$aantal_imca_mppd = mysql_num_rows($imca_mppd); echo $aantal_imca_mppd; ?></a></td>
    <td><a href="studentkey_detail.php?partner_id=2&course_id=30"><? $imca_pl = mysql_query("SELECT tk_id FROM reporting_trainingkey WHERE partner_id='2' AND course_id='30'");
$aantal_imca_pl = mysql_num_rows($imca_pl); echo $aantal_imca_pl; ?></a></td>
    <td><a href="studentkey_detail.php?partner_id=2&course_id=16"><? $imca_mbco = mysql_query("SELECT tk_id FROM reporting_trainingkey WHERE partner_id='2' AND course_id='16'");
$aantal_imca_mbco = mysql_num_rows($imca_mbco); echo $aantal_imca_mbco; ?></a></td>
<td>&nbsp;</td>
<td>&nbsp;</td>
<td>&nbsp;</td>
<td>&nbsp;</td>
  </tr>
   <tr>
    <td><strong>Information Mapping Inc</strong></td>
    <td><a href="studentkey_detail.php?partner_id=1&course_id=18"><? $imus_imf = mysql_query("SELECT tk_id FROM reporting_trainingkey WHERE partner_id='1' AND course_id='18'");
$aantal_imus_imf = mysql_num_rows($imus_imf); echo $aantal_imus_imf; ?></a></td>
    <td><a href="studentkey_detail.php?partner_id=1&course_id=32"><? $imus_mspp = mysql_query("SELECT tk_id FROM reporting_trainingkey WHERE partner_id='1' AND course_id='32'");
$aantal_imus_mspp = mysql_num_rows($imus_mspp); echo $aantal_imus_mspp; ?></a></td>
    <td><a href="studentkey_detail.php?partner_id=1&course_id=33"><? $imus_mppd = mysql_query("SELECT tk_id FROM reporting_trainingkey WHERE partner_id='1' AND course_id='33'");
$aantal_imus_mppd = mysql_num_rows($imus_mppd); echo $aantal_imus_mppd; ?></a></td>
    <td><a href="studentkey_detail.php?partner_id=1&course_id=30"><? $imus_pl = mysql_query("SELECT tk_id FROM reporting_trainingkey WHERE partner_id='1' AND course_id='30'");
$aantal_imus_pl = mysql_num_rows($imus_pl); echo $aantal_imus_pl; ?></a></td>
    <td><a href="studentkey_detail.php?partner_id=1&course_id=16"><? $imus_mbco = mysql_query("SELECT tk_id FROM reporting_trainingkey WHERE partner_id='1' AND course_id='16'");
$aantal_imus_mbco = mysql_num_rows($imus_mbco); echo $aantal_imus_mbco; ?></a></td>
<td><a href="studentkey_detail.php?partner_id=1&course_id=38"><? $imus_eecd = mysql_query("SELECT tk_id FROM reporting_trainingkey WHERE partner_id='1' AND course_id='38'");
$aantal_imus_eecd = mysql_num_rows($imus_eecd); echo $aantal_imus_eecd; ?></a></td>
<td>&nbsp;</td>
<td>&nbsp;</td>
<td>&nbsp;</td>
  </tr>
    <tr>
    <td><strong>Key2Know</strong></td>
    <td><a href="studentkey_detail.php?partner_id=12&course_id=18"><? $k2k_imf = mysql_query("SELECT tk_id FROM reporting_trainingkey WHERE partner_id='12' AND course_id='18'");
$aantal_k2k_imf = mysql_num_rows($k2k_imf); echo $aantal_k2k_imf; ?></a></td>
    <td><a href="studentkey_detail.php?partner_id=12&course_id=32"><? $k2k_mspp = mysql_query("SELECT tk_id FROM reporting_trainingkey WHERE partner_id='12' AND course_id='32'");
$aantal_k2k_mspp = mysql_num_rows($k2k_mspp); echo $aantal_k2k_mspp; ?></a></td>
    <td><a href="studentkey_detail.php?partner_id=12&course_id=33"><? $k2k_mppd = mysql_query("SELECT tk_id FROM reporting_trainingkey WHERE partner_id='12' AND course_id='33'");
$aantal_k2k_mppd = mysql_num_rows($k2k_mppd); echo $aantal_k2k_mppd; ?></a></td>
    <td><a href="studentkey_detail.php?partner_id=12&course_id=30"><? $k2k_pl = mysql_query("SELECT tk_id FROM reporting_trainingkey WHERE partner_id='12' AND course_id='30'");
$aantal_k2k_pl = mysql_num_rows($k2k_pl); echo $aantal_k2k_pl; ?></a></td>
    <td><a href="studentkey_detail.php?partner_id=12&course_id=16"><? $k2k_mbco = mysql_query("SELECT tk_id FROM reporting_trainingkey WHERE partner_id='12' AND course_id='16'");
$aantal_k2k_mbco = mysql_num_rows($k2k_mbco); echo $aantal_k2k_mbco; ?></a></td>
<td>&nbsp;</td>
<td><a href="studentkey_detail.php?partner_id=12&course_id=35"><? $k2k_ns1 = mysql_query("SELECT tk_id FROM reporting_trainingkey WHERE partner_id='12' AND course_id='35'");
$aantal_k2k_ns1 = mysql_num_rows($k2k_ns1); echo $aantal_k2k_ns1; ?></a></td>
<td><a href="studentkey_detail.php?partner_id=12&course_id=37"><? $k2k_ns2 = mysql_query("SELECT tk_id FROM reporting_trainingkey WHERE partner_id='12' AND course_id='37'");
$aantal_k2k_ns2 = mysql_num_rows($k2k_ns2); echo $aantal_k2k_ns2; ?></a></td>
<td><a href="studentkey_detail.php?partner_id=12&course_id=36"><? $k2k_nsa = mysql_query("SELECT tk_id FROM reporting_trainingkey WHERE partner_id='12' AND course_id='36'");
$aantal_k2k_nsa = mysql_num_rows($k2k_nsa); echo $aantal_k2k_nsa; ?></a></td>
  </tr>
      <tr>
    <td><strong>Principle Consulting</strong></td>
    <td><a href="studentkey_detail.php?partner_id=34&course_id=18"><? $k2k_imf = mysql_query("SELECT tk_id FROM reporting_trainingkey WHERE partner_id='34' AND course_id='18'");
$aantal_k2k_imf = mysql_num_rows($k2k_imf); echo $aantal_k2k_imf; ?></a></td>
    <td><a href="studentkey_detail.php?partner_id=34&course_id=32"><? $k2k_mspp = mysql_query("SELECT tk_id FROM reporting_trainingkey WHERE partner_id='34' AND course_id='32'");
$aantal_k2k_mspp = mysql_num_rows($k2k_mspp); echo $aantal_k2k_mspp; ?></a></td>
    <td><a href="studentkey_detail.php?partner_id=34&course_id=33"><? $k2k_mppd = mysql_query("SELECT tk_id FROM reporting_trainingkey WHERE partner_id='34' AND course_id='33'");
$aantal_k2k_mppd = mysql_num_rows($k2k_mppd); echo $aantal_k2k_mppd; ?></a></td>
    <td><a href="studentkey_detail.php?partner_id=34&course_id=30"><? $k2k_pl = mysql_query("SELECT tk_id FROM reporting_trainingkey WHERE partner_id='34' AND course_id='30'");
$aantal_k2k_pl = mysql_num_rows($k2k_pl); echo $aantal_k2k_pl; ?></a></td>
    <td><a href="studentkey_detail.php?partner_id=34&course_id=16"><? $k2k_mbco = mysql_query("SELECT tk_id FROM reporting_trainingkey WHERE partner_id='34' AND course_id='16'");
$aantal_k2k_mbco = mysql_num_rows($k2k_mbco); echo $aantal_k2k_mbco; ?></a></td>
<td>&nbsp;</td>
<td><a href="studentkey_detail.php?partner_id=34&course_id=35"><? $k2k_ns1 = mysql_query("SELECT tk_id FROM reporting_trainingkey WHERE partner_id='34' AND course_id='35'");
$aantal_k2k_ns1 = mysql_num_rows($k2k_ns1); echo $aantal_k2k_ns1; ?></a></td>
<td><a href="studentkey_detail.php?partner_id=34&course_id=37"><? $k2k_ns2 = mysql_query("SELECT tk_id FROM reporting_trainingkey WHERE partner_id='34' AND course_id='37'");
$aantal_k2k_ns2 = mysql_num_rows($k2k_ns2); echo $aantal_k2k_ns2; ?></a></td>
<td><a href="studentkey_detail.php?partner_id=34&course_id=36"><? $k2k_nsa = mysql_query("SELECT tk_id FROM reporting_trainingkey WHERE partner_id='34' AND course_id='36'");
$aantal_k2k_nsa = mysql_num_rows($k2k_nsa); echo $aantal_k2k_nsa; ?></a></td>
  </tr>
    <tr>
    <td><strong>Synaps</strong></td>
    <td><a href="studentkey_detail.php?partner_id=25&course_id=18"><? $syn_imf = mysql_query("SELECT tk_id FROM reporting_trainingkey WHERE partner_id='25' AND course_id='18'");
$aantal_syn_imf = mysql_num_rows($syn_imf); echo $aantal_syn_imf; ?></a></td>
    <td><a href="studentkey_detail.php?partner_id=25&course_id=32"><? $syn_mspp = mysql_query("SELECT tk_id FROM reporting_trainingkey WHERE partner_id='25' AND course_id='32'");
$aantal_syn_mspp = mysql_num_rows($syn_mspp); echo $aantal_syn_mspp; ?></a></td>
    <td><a href="studentkey_detail.php?partner_id=25&course_id=33"><? $syn_mppd = mysql_query("SELECT tk_id FROM reporting_trainingkey WHERE partner_id='25' AND course_id='33'");
$aantal_syn_mppd = mysql_num_rows($syn_mppd); echo $aantal_syn_mppd; ?></a></td>
    <td><a href="studentkey_detail.php?partner_id=25&course_id=30"><? $syn_pl = mysql_query("SELECT tk_id FROM reporting_trainingkey WHERE partner_id='25' AND course_id='30'");
$aantal_syn_pl = mysql_num_rows($syn_pl); echo $aantal_syn_pl; ?></a></td>
    <td><a href="studentkey_detail.php?partner_id=25&course_id=16"><? $syn_mbco = mysql_query("SELECT tk_id FROM reporting_trainingkey WHERE partner_id='25' AND course_id='16'");
$aantal_syn_mbco = mysql_num_rows($syn_mbco); echo $aantal_syn_mbco; ?></a></td>
<td>&nbsp;</td>
<td>&nbsp;</td>
<td>&nbsp;</td>
<td>&nbsp;</td>
  </tr>
   <tr>
    <td><strong>Tactics NZ</strong></td>
    <td><a href="studentkey_detail.php?partner_id=19&course_id=18"><? $tac_imf = mysql_query("SELECT tk_id FROM reporting_trainingkey WHERE partner_id='19' AND course_id='18'");
$aantal_tac_imf = mysql_num_rows($tac_imf); echo $aantal_tac_imf; ?></a></td>
    <td><a href="studentkey_detail.php?partner_id=19&course_id=32"><? $tac_mspp = mysql_query("SELECT tk_id FROM reporting_trainingkey WHERE partner_id='19' AND course_id='32'");
$aantal_tac_mspp = mysql_num_rows($tac_mspp); echo $aantal_tac_mspp; ?></a></td>
    <td><a href="studentkey_detail.php?partner_id=19&course_id=33"><? $tac_mppd = mysql_query("SELECT tk_id FROM reporting_trainingkey WHERE partner_id='19' AND course_id='33'");
$aantal_tac_mppd = mysql_num_rows($tac_mppd); echo $aantal_tac_mppd; ?></a></td>
    <td><a href="studentkey_detail.php?partner_id=19&course_id=30"><? $tac_pl = mysql_query("SELECT tk_id FROM reporting_trainingkey WHERE partner_id='19' AND course_id='30'");
$aantal_tac_pl = mysql_num_rows($tac_pl); echo $aantal_tac_pl; ?></a></td>
    <td><a href="studentkey_detail.php?partner_id=19&course_id=16"><? $tac_mbco = mysql_query("SELECT tk_id FROM reporting_trainingkey WHERE partner_id='19' AND course_id='16'");
$aantal_tac_mbco = mysql_num_rows($tac_mbco); echo $aantal_tac_mbco; ?></a></td>
<td>&nbsp;</td>
<td>&nbsp;</td>
<td>&nbsp;</td>
<td>&nbsp;</td>
  </tr>      
    <tr>
    <td><strong>Takoma</strong></td>
    <td><a href="studentkey_detail.php?partner_id=17&course_id=18"><? $tak_imf = mysql_query("SELECT tk_id FROM reporting_trainingkey WHERE partner_id='17' AND course_id='18'");
$aantal_tak_imf = mysql_num_rows($tak_imf); echo $aantal_tak_imf; ?></a></td>
    <td><a href="studentkey_detail.php?partner_id=17&course_id=32"><? $tak_mspp = mysql_query("SELECT tk_id FROM reporting_trainingkey WHERE partner_id='17' AND course_id='32'");
$aantal_tak_mspp = mysql_num_rows($tak_mspp); echo $aantal_tak_mspp; ?></a></td>
    <td><a href="studentkey_detail.php?partner_id=17&course_id=33"><? $tak_mppd = mysql_query("SELECT tk_id FROM reporting_trainingkey WHERE partner_id='17' AND course_id='33'");
$aantal_tak_mppd = mysql_num_rows($tak_mppd); echo $aantal_tak_mppd; ?></a></td>
    <td><a href="studentkey_detail.php?partner_id=17&course_id=30"><? $tak_pl = mysql_query("SELECT tk_id FROM reporting_trainingkey WHERE partner_id='17' AND course_id='30'");
$aantal_tak_pl = mysql_num_rows($tak_pl); echo $aantal_tak_pl; ?></a></td>
    <td><a href="studentkey_detail.php?partner_id=17&course_id=16"><? $tak_mbco = mysql_query("SELECT tk_id FROM reporting_trainingkey WHERE partner_id='17' AND course_id='16'");
$aantal_tak_mbco = mysql_num_rows($tak_mbco); echo $aantal_tak_mbco; ?></a></td>
<td>&nbsp;</td>
<td>&nbsp;</td>
<td>&nbsp;</td>
<td>&nbsp;</td>
  </tr>      
   <tr>
    <td><strong>TP3</strong></td>
    <td><a href="studentkey_detail.php?partner_id=20&course_id=18"><? $tp3_imf = mysql_query("SELECT tk_id FROM reporting_trainingkey WHERE partner_id='20' AND course_id='18'");
$aantal_tp3_imf = mysql_num_rows($tp3_imf); echo $aantal_tp3_imf; ?></a></td>
    <td><a href="studentkey_detail.php?partner_id=20&course_id=32"><? $tp3_mspp = mysql_query("SELECT tk_id FROM reporting_trainingkey WHERE partner_id='20' AND course_id='32'");
$aantal_tp3_mspp = mysql_num_rows($tp3_mspp); echo $aantal_tp3_mspp; ?></a></td>
    <td><a href="studentkey_detail.php?partner_id=20&course_id=33"><? $tp3_mppd = mysql_query("SELECT tk_id FROM reporting_trainingkey WHERE partner_id='20' AND course_id='33'");
$aantal_tp3_mppd = mysql_num_rows($tp3_mppd); echo $aantal_tp3_mppd; ?></a></td>
    <td><a href="studentkey_detail.php?partner_id=20&course_id=30"><? $tp3_pl = mysql_query("SELECT tk_id FROM reporting_trainingkey WHERE partner_id='20' AND course_id='30'");
$aantal_tp3_pl = mysql_num_rows($tp3_pl); echo $aantal_tp3_pl; ?></a></td>
    <td><a href="studentkey_detail.php?partner_id=20&course_id=16"><? $tp3_mbco = mysql_query("SELECT tk_id FROM reporting_trainingkey WHERE partner_id='20' AND course_id='16'");
$aantal_tp3_mbco = mysql_num_rows($tp3_mbco); echo $aantal_tp3_mbco; ?></a></td>
<td>&nbsp;</td>
<td>&nbsp;</td>
<td>&nbsp;</td>
<td>&nbsp;</td>
  </tr>      
     <tr>
    <td><strong>Triumph</strong></td>
    <td><a href="studentkey_detail.php?partner_id=32&course_id=18"><? $tri_imf = mysql_query("SELECT tk_id FROM reporting_trainingkey WHERE partner_id='32' AND course_id='18'");
$aantal_tri_imf = mysql_num_rows($tri_imf); echo $aantal_tri_imf; ?></a></td>
    <td><a href="studentkey_detail.php?partner_id=32&course_id=32"><? $tri_mspp = mysql_query("SELECT tk_id FROM reporting_trainingkey WHERE partner_id='32' AND course_id='32'");
$aantal_tri_mspp = mysql_num_rows($tri_mspp); echo $aantal_tri_mspp; ?></a></td>
    <td><a href="studentkey_detail.php?partner_id=32&course_id=33"><? $tri_mppd = mysql_query("SELECT tk_id FROM reporting_trainingkey WHERE partner_id='32' AND course_id='35'");
$aantal_tri_mppd = mysql_num_rows($tri_mppd); echo $aantal_tri_mppd; ?></a></td>
    <td><a href="studentkey_detail.php?partner_id=32&course_id=30"><? $tri_pl = mysql_query("SELECT tk_id FROM reporting_trainingkey WHERE partner_id='32' AND course_id='30'");
$aantal_tri_pl = mysql_num_rows($tri_pl); echo $aantal_tri_pl; ?></a></td>
    <td><a href="studentkey_detail.php?partner_id=32&course_id=16"><? $tri_mbco = mysql_query("SELECT tk_id FROM reporting_trainingkey WHERE partner_id='32' AND course_id='16'");
$aantal_tri_mbco = mysql_num_rows($tri_mbco); echo $aantal_tri_mbco; ?></a></td>
<td>&nbsp;</td>
<td>&nbsp;</td>
<td>&nbsp;</td>
<td>&nbsp;</td>
  </tr>   
   <tr>
    <td><strong>U&I Learning Belgium</strong></td>
     <td><a href="studentkey_detail.php?partner_id=27&course_id=18"><? $unibe_imf = mysql_query("SELECT tk_id FROM reporting_trainingkey WHERE partner_id='27' AND course_id='18'");
$aantal_unibe_imf = mysql_num_rows($unibe_imf); echo $aantal_unibe_imf; ?></a></td>
    <td><a href="studentkey_detail.php?partner_id=27&course_id=32"><? $unibe_mspp = mysql_query("SELECT tk_id FROM reporting_trainingkey WHERE partner_id='27' AND course_id='32'");
$aantal_unibe_mspp = mysql_num_rows($unibe_mspp); echo $aantal_unibe_mspp; ?></a></td>
    <td><a href="studentkey_detail.php?partner_id=27&course_id=33"><? $unibe_mppd = mysql_query("SELECT tk_id FROM reporting_trainingkey WHERE partner_id='27' AND course_id='33'");
$aantal_unibe_mppd = mysql_num_rows($unibe_mppd); echo $aantal_unibe_mppd; ?></a></td>
    <td><a href="studentkey_detail.php?partner_id=27&course_id=30"><? $unibe_pl = mysql_query("SELECT tk_id FROM reporting_trainingkey WHERE partner_id='27' AND course_id='30'");
$aantal_unibe_pl = mysql_num_rows($unibe_pl); echo $aantal_unibe_pl; ?></a></td>
    <td><a href="studentkey_detail.php?partner_id=27&course_id=16"><? $unibe_mbco = mysql_query("SELECT tk_id FROM reporting_trainingkey WHERE partner_id='27' AND course_id='16'");
$aantal_unibe_mbco = mysql_num_rows($unibe_mbco); echo $aantal_unibe_mbco; ?></a></td>
<td>&nbsp;</td>
<td>&nbsp;</td>
<td>&nbsp;</td>
<td>&nbsp;</td>
  </tr>
    <tr>
    <td><strong>U&I Learning Nederland</strong></td>
    <td><a href="studentkey_detail.php?partner_id=10&course_id=18"><? $uninl_imf = mysql_query("SELECT tk_id FROM reporting_trainingkey WHERE partner_id='10' AND course_id='18'");
$aantal_uninl_imf = mysql_num_rows($uninl_imf); echo $aantal_uninl_imf; ?></a></td>
    <td><a href="studentkey_detail.php?partner_id=10&course_id=32"><? $uninl_mspp = mysql_query("SELECT tk_id FROM reporting_trainingkey WHERE partner_id='10' AND course_id='32'");
$aantal_uninl_mspp = mysql_num_rows($uninl_mspp); echo $aantal_uninl_mspp; ?></a></td>
    <td><a href="studentkey_detail.php?partner_id=10&course_id=33"><? $uninl_mppd = mysql_query("SELECT tk_id FROM reporting_trainingkey WHERE partner_id='10' AND course_id='33'");
$aantal_uninl_mppd = mysql_num_rows($uninl_mppd); echo $aantal_uninl_mppd; ?></a></td>
    <td><a href="studentkey_detail.php?partner_id=10&course_id=30"><? $uninl_pl = mysql_query("SELECT tk_id FROM reporting_trainingkey WHERE partner_id='10' AND course_id='30'");
$aantal_uninl_pl = mysql_num_rows($uninl_pl); echo $aantal_uninl_pl; ?></a></td>
    <td><a href="studentkey_detail.php?partner_id=10&course_id=16"><? $uninl_mbco = mysql_query("SELECT tk_id FROM reporting_trainingkey WHERE partner_id='10' AND course_id='16'");
$aantal_uninl_mbco = mysql_num_rows($uninl_mbco); echo $aantal_uninl_mbco; ?></a></td>
<td>&nbsp;</td>
<td>&nbsp;</td>
<td>&nbsp;</td>
<td>&nbsp;</td>
  </tr>
      <tr>
    <td><strong>Writec</strong></td>
    <td><a href="studentkey_detail.php?partner_id=30&course_id=18"><? $wri_imf = mysql_query("SELECT tk_id FROM reporting_trainingkey WHERE partner_id='30' AND course_id='18'");
$aantal_wri_imf = mysql_num_rows($wri_imf); echo $aantal_wri_imf; ?></a></td>
    <td><a href="studentkey_detail.php?partner_id=30&course_id=32"><? $wri_mspp = mysql_query("SELECT tk_id FROM reporting_trainingkey WHERE partner_id='30' AND course_id='32'");
$aantal_wri_mspp = mysql_num_rows($wri_mspp); echo $aantal_wri_mspp; ?></a></td>
    <td><a href="studentkey_detail.php?partner_id=30&course_id=33"><? $wri_mppd = mysql_query("SELECT tk_id FROM reporting_trainingkey WHERE partner_id='30' AND course_id='33'");
$aantal_wri_mppd = mysql_num_rows($wri_mppd); echo $aantal_wri_mppd; ?></a></td>
    <td><a href="studentkey_detail.php?partner_id=30&course_id=30"><? $wri_pl = mysql_query("SELECT tk_id FROM reporting_trainingkey WHERE partner_id='30' AND course_id='30'");
$aantal_wri_pl = mysql_num_rows($wri_pl); echo $aantal_wri_pl; ?></a></td>
    <td><a href="studentkey_detail.php?partner_id=30&course_id=16"><? $wri_mbco = mysql_query("SELECT tk_id FROM reporting_trainingkey WHERE partner_id='30' AND course_id='16'");
$aantal_wri_mbco = mysql_num_rows($wri_mbco); echo $aantal_wri_mbco; ?></a></td>
<td>&nbsp;</td>
<td>&nbsp;</td>
<td>&nbsp;</td>
<td>&nbsp;</td>
  </tr>  
 </table>


<?php
}
?>
                                </div>
                            </div>
                            <!-- /block -->


</div>
</div>
</div>
</div>
            <hr>
<?php
include("../footer.php");
?>
</html> 
 
 </body>
 </html>        
           
