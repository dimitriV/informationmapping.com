<?php
$rootdir="../../";
$page="Partner web store totals";
include("../connect.php");
include("../header.php");
?>

<body>
<?php
include("../topnav.php");
?>
<div class="container-fluid">
            <div class="row-fluid">
                
                <?php
				include("../sidenav.php");
				?>
                
                <div class="span10" id="content">
                    <div class="row-fluid">
                    	<div class="span12">
                        
                            <!-- block -->
                            <div class="block">
                                <div class="navbar navbar-inner block-header">
                                <h3>Partner web store totals</h3>    
                                </div>
                                <div class="block-content collapse in">
<?php 
if (!isset($_SESSION["username"])){
	echo $niet_ingelogd;
}
// hieronder staan de functies die toegelaten worden tot deze pagina
elseif($_SESSION["function"] != "admin"){
	echo $geen_toegang;
}else{
?>
<table class="table table-striped">
<tr><th></th><th>2009</th><th>2010</th><th>2011</th><th>2012</th><th>2013</th><th>2014</th></tr>
<tr>
  <th>Aracane</th>
  <td><?php 
  $result_1 = mysql_query("SELECT status, customer_id, created_at, ROUND(sum(base_grand_total)) AS totaal FROM sales_flat_order WHERE status= 'complete' && YEAR(created_at) = '2009' && customer_id='7415'");
  $row = mysql_fetch_assoc($result_1);
  if($row['totaal'] != NULL) { if($row['totaal'] != NULL) { echo $row['totaal'] . " EUR"; } }
  ?></td>
  <td><?php 
  $result_1 = mysql_query("SELECT status, customer_id, created_at, ROUND(sum(base_grand_total)) AS totaal FROM sales_flat_order WHERE status= 'complete' && YEAR(created_at) = '2010' && customer_id='7415'");
  $row = mysql_fetch_assoc($result_1);
  if($row['totaal'] != NULL) { echo $row['totaal'] . " EUR"; }
  ?></td>
  <td><?php 
  $result_1 = mysql_query("SELECT status, customer_id, created_at, ROUND(sum(base_grand_total)) AS totaal FROM sales_flat_order WHERE status= 'complete' && YEAR(created_at) = '2011' && customer_id='7415'");
  $row = mysql_fetch_assoc($result_1);
  if($row['totaal'] != NULL) { echo $row['totaal'] . " EUR"; }
  ?></td>
  <td><?php 
  $result_1 = mysql_query("SELECT status, customer_id, created_at, ROUND(sum(base_grand_total)) AS totaal FROM sales_flat_order WHERE status= 'complete' && YEAR(created_at) = '2012' && customer_id='7415'");
  $row = mysql_fetch_assoc($result_1);
  if($row['totaal'] != NULL) { echo $row['totaal'] . " EUR"; }
  ?></td>
  <td><?php 
  $result_1 = mysql_query("SELECT status, customer_id, created_at, ROUND(sum(base_grand_total)) AS totaal FROM sales_flat_order WHERE status= 'complete' && YEAR(created_at) = '2013' && customer_id='7415'");
  $row = mysql_fetch_assoc($result_1);
  if($row['totaal'] != NULL) { echo $row['totaal'] . " EUR"; }
  ?></td>
  <td><?php 
  $result_1 = mysql_query("SELECT status, customer_id, created_at, ROUND(sum(base_grand_total)) AS totaal FROM sales_flat_order WHERE status= 'complete' && YEAR(created_at) = '2014' && customer_id='7415'");
  $row = mysql_fetch_assoc($result_1);
  if($row['totaal'] != NULL) { echo $row['totaal'] . " EUR"; }
  ?></td>
</tr>
<tr>
  <th>IM Canada</th>
  <td>
  <?php 
  $result_1 = mysql_query("SELECT status, customer_id, created_at, ROUND(sum(base_grand_total)) AS totaal FROM sales_flat_order WHERE status= 'complete' && YEAR(created_at) = '2009' && customer_id='9'");
  $row = mysql_fetch_assoc($result_1);
  if($row['totaal'] != NULL) { echo $row['totaal'] . " EUR"; }
  ?>
  </td>
  <td>  <?php 
  $result_1 = mysql_query("SELECT status, customer_id, created_at, ROUND(sum(base_grand_total)) AS totaal FROM sales_flat_order WHERE status= 'complete' && YEAR(created_at) = '2010' && customer_id='9'");
  $row = mysql_fetch_assoc($result_1);
  if($row['totaal'] != NULL) { echo $row['totaal'] . " EUR"; }
  ?></td>
  <td>  <?php 
  $result_1 = mysql_query("SELECT status, customer_id, created_at, ROUND(sum(base_grand_total)) AS totaal FROM sales_flat_order WHERE status= 'complete' && YEAR(created_at) = '2011' && customer_id='9'");
  $row = mysql_fetch_assoc($result_1);
  if($row['totaal'] != NULL) { echo $row['totaal'] . " EUR"; }
  ?></td>
  <td>  <?php 
  $result_1 = mysql_query("SELECT status, customer_id, created_at, ROUND(sum(base_grand_total)) AS totaal FROM sales_flat_order WHERE status= 'complete' && YEAR(created_at) = '2012' && customer_id='9'");
  $row = mysql_fetch_assoc($result_1);
  if($row['totaal'] != NULL) { echo $row['totaal'] . " EUR"; }
  ?></td>
  <td>  <?php 
  $result_1 = mysql_query("SELECT status, customer_id, created_at, ROUND(sum(base_grand_total)) AS totaal FROM sales_flat_order WHERE status= 'complete' && YEAR(created_at) = '2013' && customer_id='9'");
  $row = mysql_fetch_assoc($result_1);
  if($row['totaal'] != NULL) { echo $row['totaal'] . " EUR"; }
  ?></td>
  <td>  <?php 
  $result_1 = mysql_query("SELECT status, customer_id, created_at, ROUND(sum(base_grand_total)) AS totaal FROM sales_flat_order WHERE status= 'complete' && YEAR(created_at) = '2014' && customer_id='9'");
  $row = mysql_fetch_assoc($result_1);
  if($row['totaal'] != NULL) { echo $row['totaal'] . " EUR"; }
  ?></td>
</tr>
<tr>
  <th><p>IM India</p></th>
  <td><?php 
  $result_1 = mysql_query("SELECT status, customer_id, created_at, ROUND(sum(base_grand_total)) AS totaal FROM sales_flat_order WHERE status= 'complete' && YEAR(created_at) = '2009' && customer_id='8419'");
  $row = mysql_fetch_assoc($result_1);
  if($row['totaal'] != NULL) { echo $row['totaal'] . " EUR"; }
  ?>
  </td>
  <td><?php 
  $result_1 = mysql_query("SELECT status, customer_id, created_at, ROUND(sum(base_grand_total)) AS totaal FROM sales_flat_order WHERE status= 'complete' && YEAR(created_at) = '2010' && customer_id='8419'");
  $row = mysql_fetch_assoc($result_1);
  if($row['totaal'] != NULL) { echo $row['totaal'] . " EUR"; }
  ?></td>
  <td><?php 
  $result_1 = mysql_query("SELECT status, customer_id, created_at, ROUND(sum(base_grand_total)) AS totaal FROM sales_flat_order WHERE status= 'complete' && YEAR(created_at) = '2011' && customer_id='8419'");
  $row = mysql_fetch_assoc($result_1);
  if($row['totaal'] != NULL) { echo $row['totaal'] . " EUR"; }
  ?></td>
  <td><?php 
  $result_1 = mysql_query("SELECT status, customer_id, created_at, ROUND(sum(base_grand_total)) AS totaal FROM sales_flat_order WHERE status= 'complete' && YEAR(created_at) = '2012' && customer_id='8419'");
  $row = mysql_fetch_assoc($result_1);
  if($row['totaal'] != NULL) { echo $row['totaal'] . " EUR"; }
  ?></td>
  <td><?php 
  $result_1 = mysql_query("SELECT status, customer_id, created_at, ROUND(sum(base_grand_total)) AS totaal FROM sales_flat_order WHERE status= 'complete' && YEAR(created_at) = '2013' && customer_id='8419'");
  $row = mysql_fetch_assoc($result_1);
  if($row['totaal'] != NULL) { echo $row['totaal'] . " EUR"; }
  ?></td>
  <td><?php 
  $result_1 = mysql_query("SELECT status, customer_id, created_at, ROUND(sum(base_grand_total)) AS totaal FROM sales_flat_order WHERE status= 'complete' && YEAR(created_at) = '2014' && customer_id='8419'");
  $row = mysql_fetch_assoc($result_1);
  if($row['totaal'] != NULL) { echo $row['totaal'] . " EUR"; }
  ?></td>
</tr>
<tr>
  <th>IM US</th>
  <td><?php 
  $result_1 = mysql_query("SELECT status, customer_id, created_at, ROUND(sum(base_grand_total)) AS totaal FROM sales_flat_order WHERE status= 'complete' && YEAR(created_at) = '2009' && 
  (customer_id='30' OR customer_id='1409' OR customer_id='949' OR customer_id='846' OR customer_id='7698')
  ");
  $row = mysql_fetch_assoc($result_1);
  echo $row['totaal'] . " USD";
  ?>
  </td>
  <td><?php 
  $result_1 = mysql_query("SELECT status, customer_id, created_at, ROUND(sum(base_grand_total)) AS totaal FROM sales_flat_order WHERE status= 'complete' && YEAR(created_at) = '2010' && 
  (customer_id='30' OR customer_id='1409' OR customer_id='949' OR customer_id='846' OR customer_id='7698')
  ");
  $row = mysql_fetch_assoc($result_1);
  echo $row['totaal'] . " USD";
  ?></td>
  <td><?php 
  $result_1 = mysql_query("SELECT status, customer_id, created_at, ROUND(sum(base_grand_total)) AS totaal FROM sales_flat_order WHERE status= 'complete' && YEAR(created_at) = '2011' && 
  (customer_id='30' OR customer_id='1409' OR customer_id='949' OR customer_id='846' OR customer_id='7698')
  ");
  $row = mysql_fetch_assoc($result_1);
  echo $row['totaal'] . " USD";
  ?></td>
  <td><?php 
  $result_1 = mysql_query("SELECT status, customer_id, created_at, ROUND(sum(base_grand_total)) AS totaal FROM sales_flat_order WHERE status= 'complete' && YEAR(created_at) = '2012' && 
  (customer_id='30' OR customer_id='1409' OR customer_id='949' OR customer_id='846' OR customer_id='7698')
  ");
  $row = mysql_fetch_assoc($result_1);
  echo $row['totaal'] . " USD";
  ?></td>
  <td><?php 
  $result_1 = mysql_query("SELECT status, customer_id, created_at, ROUND(sum(base_grand_total)) AS totaal FROM sales_flat_order WHERE status= 'complete' && YEAR(created_at) = '2013' && 
  (customer_id='30' OR customer_id='1409' OR customer_id='949' OR customer_id='846' OR customer_id='7698')
  ");
  $row = mysql_fetch_assoc($result_1);
  echo $row['totaal'] . " USD";
  ?></td>
  <td><?php 
  $result_1 = mysql_query("SELECT status, customer_id, created_at, ROUND(sum(base_grand_total)) AS totaal FROM sales_flat_order WHERE status= 'complete' && YEAR(created_at) = '2014' && 
  (customer_id='30' OR customer_id='1409' OR customer_id='949' OR customer_id='846' OR customer_id='7698')
  ");
  $row = mysql_fetch_assoc($result_1);
  echo $row['totaal'] . " USD";
  ?></td>
</tr>
<tr>
  <th>Key2Know</th>
  <td><?php 
  $result_1 = mysql_query("SELECT status, customer_id, created_at, ROUND(sum(base_grand_total)) AS totaal FROM sales_flat_order WHERE status= 'complete' && YEAR(created_at) = '2009' && 
  (customer_id='10' OR customer_id='1873' OR customer_id='2041' OR customer_id='8187' OR customer_id='8479')
  ");
  $row = mysql_fetch_assoc($result_1);
  if($row['totaal'] != NULL) { echo $row['totaal'] . " EUR"; }
  ?>
  </td>
  <td><?php 
  $result_1 = mysql_query("SELECT status, customer_id, created_at, ROUND(sum(base_grand_total)) AS totaal FROM sales_flat_order WHERE status= 'complete' && YEAR(created_at) = '2010' && 
  (customer_id='10' OR customer_id='1873' OR customer_id='2041' OR customer_id='8187' OR customer_id='8479')
  ");
  $row = mysql_fetch_assoc($result_1);
  if($row['totaal'] != NULL) { echo $row['totaal'] . " EUR"; }
  ?></td>
  <td><?php 
  $result_1 = mysql_query("SELECT status, customer_id, created_at, ROUND(sum(base_grand_total)) AS totaal FROM sales_flat_order WHERE status= 'complete' && YEAR(created_at) = '2011' && 
  (customer_id='10' OR customer_id='1873' OR customer_id='2041' OR customer_id='8187' OR customer_id='8479')
  ");
  $row = mysql_fetch_assoc($result_1);
  if($row['totaal'] != NULL) { echo $row['totaal'] . " EUR"; }
  ?></td>
  <td><?php 
  $result_1 = mysql_query("SELECT status, customer_id, created_at, ROUND(sum(base_grand_total)) AS totaal FROM sales_flat_order WHERE status= 'complete' && YEAR(created_at) = '2012' && 
  (customer_id='10' OR customer_id='1873' OR customer_id='2041' OR customer_id='8187' OR customer_id='8479')
  ");
  $row = mysql_fetch_assoc($result_1);
  if($row['totaal'] != NULL) { echo $row['totaal'] . " EUR"; }
  ?></td>
  <td><?php 
  $result_1 = mysql_query("SELECT status, customer_id, created_at, ROUND(sum(base_grand_total)) AS totaal FROM sales_flat_order WHERE status= 'complete' && YEAR(created_at) = '2013' && 
  (customer_id='10' OR customer_id='1873' OR customer_id='2041' OR customer_id='8187' OR customer_id='8479')
  ");
  $row = mysql_fetch_assoc($result_1);
  if($row['totaal'] != NULL) { echo $row['totaal'] . " EUR"; }
  ?></td>
  <td><?php 
  $result_1 = mysql_query("SELECT status, customer_id, created_at, ROUND(sum(base_grand_total)) AS totaal FROM sales_flat_order WHERE status= 'complete' && YEAR(created_at) = '2014' && 
  (customer_id='10' OR customer_id='1873' OR customer_id='2041' OR customer_id='8187' OR customer_id='8479')
  ");
  $row = mysql_fetch_assoc($result_1);
  if($row['totaal'] != NULL) { echo $row['totaal'] . " EUR"; }
  ?></td>
</tr>
<tr>
  <th>Kozoka</th>
  <td><?php 
  $result_1 = mysql_query("SELECT status, customer_id, created_at, ROUND(sum(base_grand_total)) AS totaal FROM sales_flat_order WHERE status= 'complete' && YEAR(created_at) = '2009' && 
  customer_id='7415'
  ");
  $row = mysql_fetch_assoc($result_1);
  if($row['totaal'] != NULL) { echo $row['totaal'] . " EUR"; }
  ?>
  </td>
  <td><?php 
  $result_1 = mysql_query("SELECT status, customer_id, created_at, ROUND(sum(base_grand_total)) AS totaal FROM sales_flat_order WHERE status= 'complete' && YEAR(created_at) = '2010' && 
  customer_id='7415'
  ");
  $row = mysql_fetch_assoc($result_1);
  if($row['totaal'] != NULL) { echo $row['totaal'] . " EUR"; }
  ?></td>
  <td><?php 
  $result_1 = mysql_query("SELECT status, customer_id, created_at, ROUND(sum(base_grand_total)) AS totaal FROM sales_flat_order WHERE status= 'complete' && YEAR(created_at) = '2011' && 
  customer_id='7415'
  ");
  $row = mysql_fetch_assoc($result_1);
  if($row['totaal'] != NULL) { echo $row['totaal'] . " EUR"; }
  ?></td>
  <td><?php 
  $result_1 = mysql_query("SELECT status, customer_id, created_at, ROUND(sum(base_grand_total)) AS totaal FROM sales_flat_order WHERE status= 'complete' && YEAR(created_at) = '2012' && 
  customer_id='7415'
  ");
  $row = mysql_fetch_assoc($result_1);
  if($row['totaal'] != NULL) { echo $row['totaal'] . " EUR"; }
  ?></td>
  <td><?php 
  $result_1 = mysql_query("SELECT status, customer_id, created_at, ROUND(sum(base_grand_total)) AS totaal FROM sales_flat_order WHERE status= 'complete' && YEAR(created_at) = '2013' && 
  customer_id='7415'
  ");
  $row = mysql_fetch_assoc($result_1);
  if($row['totaal'] != NULL) { echo $row['totaal'] . " EUR"; }
  ?></td>
  <td><?php 
  $result_1 = mysql_query("SELECT status, customer_id, created_at, ROUND(sum(base_grand_total)) AS totaal FROM sales_flat_order WHERE status= 'complete' && YEAR(created_at) = '2014' && 
  customer_id='7415'
  ");
  $row = mysql_fetch_assoc($result_1);
  if($row['totaal'] != NULL) { echo $row['totaal'] . " EUR"; }
  ?></td>
</tr>
<tr>
  <th>Contexto Didactico</th>
  <td><?php 
  $result_1 = mysql_query("SELECT status, customer_id, created_at, ROUND(sum(base_grand_total)) AS totaal FROM sales_flat_order WHERE status= 'complete' && YEAR(created_at) = '2009' && 
  customer_id='23'
  ");
  $row = mysql_fetch_assoc($result_1);
  if($row['totaal'] != NULL) { echo $row['totaal'] . " EUR"; }
  ?>
  </td>
  <td><?php 
  $result_1 = mysql_query("SELECT status, customer_id, created_at, ROUND(sum(base_grand_total)) AS totaal FROM sales_flat_order WHERE status= 'complete' && YEAR(created_at) = '2010' && 
  customer_id='23'
  ");
  $row = mysql_fetch_assoc($result_1);
  if($row['totaal'] != NULL) { echo $row['totaal'] . " EUR"; }
  ?></td>
  <td><?php 
  $result_1 = mysql_query("SELECT status, customer_id, created_at, ROUND(sum(base_grand_total)) AS totaal FROM sales_flat_order WHERE status= 'complete' && YEAR(created_at) = '2011' && 
  customer_id='23'
  ");
  $row = mysql_fetch_assoc($result_1);
  if($row['totaal'] != NULL) { echo $row['totaal'] . " EUR"; }
  ?></td>
  <td><?php 
  $result_1 = mysql_query("SELECT status, customer_id, created_at, ROUND(sum(base_grand_total)) AS totaal FROM sales_flat_order WHERE status= 'complete' && YEAR(created_at) = '2012' && 
  customer_id='23'
  ");
  $row = mysql_fetch_assoc($result_1);
  if($row['totaal'] != NULL) { echo $row['totaal'] . " EUR"; }
  ?></td>
  <td><?php 
  $result_1 = mysql_query("SELECT status, customer_id, created_at, ROUND(sum(base_grand_total)) AS totaal FROM sales_flat_order WHERE status= 'complete' && YEAR(created_at) = '2013' && 
  customer_id='23'
  ");
  $row = mysql_fetch_assoc($result_1);
  if($row['totaal'] != NULL) { echo $row['totaal'] . " EUR"; }
  ?></td>
  <td><?php 
  $result_1 = mysql_query("SELECT status, customer_id, created_at, ROUND(sum(base_grand_total)) AS totaal FROM sales_flat_order WHERE status= 'complete' && YEAR(created_at) = '2014' && 
  customer_id='23'
  ");
  $row = mysql_fetch_assoc($result_1);
  if($row['totaal'] != NULL) { echo $row['totaal'] . " EUR"; }
  ?></td>
</tr>
<tr>
  <th>Semantix</th>
  <td><?php 
  $result_1 = mysql_query("SELECT status, customer_id, created_at, ROUND(sum(base_grand_total)) AS totaal FROM sales_flat_order WHERE status= 'complete' && YEAR(created_at) = '2009' && 
  customer_id='28'
  ");
  $row = mysql_fetch_assoc($result_1);
  if($row['totaal'] != NULL) { echo $row['totaal'] . " EUR"; }
  ?>
  </td>
  <td><?php 
  $result_1 = mysql_query("SELECT status, customer_id, created_at, ROUND(sum(base_grand_total)) AS totaal FROM sales_flat_order WHERE status= 'complete' && YEAR(created_at) = '2010' && 
  customer_id='28'
  ");
  $row = mysql_fetch_assoc($result_1);
  if($row['totaal'] != NULL) { echo $row['totaal'] . " EUR"; }
  ?></td>
  <td><?php 
  $result_1 = mysql_query("SELECT status, customer_id, created_at, ROUND(sum(base_grand_total)) AS totaal FROM sales_flat_order WHERE status= 'complete' && YEAR(created_at) = '2011' && 
  customer_id='28'
  ");
  $row = mysql_fetch_assoc($result_1);
  if($row['totaal'] != NULL) { echo $row['totaal'] . " EUR"; }
  ?></td>
  <td><?php 
  $result_1 = mysql_query("SELECT status, customer_id, created_at, ROUND(sum(base_grand_total)) AS totaal FROM sales_flat_order WHERE status= 'complete' && YEAR(created_at) = '2012' && 
  customer_id='28'
  ");
  $row = mysql_fetch_assoc($result_1);
  if($row['totaal'] != NULL) { echo $row['totaal'] . " EUR"; }
  ?></td>
  <td><?php 
  $result_1 = mysql_query("SELECT status, customer_id, created_at, ROUND(sum(base_grand_total)) AS totaal FROM sales_flat_order WHERE status= 'complete' && YEAR(created_at) = '2013' && 
  customer_id='28'
  ");
  $row = mysql_fetch_assoc($result_1);
  if($row['totaal'] != NULL) { echo $row['totaal'] . " EUR"; }
  ?></td>
  <td><?php 
  $result_1 = mysql_query("SELECT status, customer_id, created_at, ROUND(sum(base_grand_total)) AS totaal FROM sales_flat_order WHERE status= 'complete' && YEAR(created_at) = '2014' && 
  customer_id='28'
  ");
  $row = mysql_fetch_assoc($result_1);
  if($row['totaal'] != NULL) { echo $row['totaal'] . " EUR"; }
  ?></td>
</tr>
<tr>
  <th>Synaps</th>
  <td><?php 
  $result_1 = mysql_query("SELECT status, customer_id, created_at, ROUND(sum(base_grand_total)) AS totaal FROM sales_flat_order WHERE status= 'complete' && YEAR(created_at) = '2009' && 
  (customer_id='1167' OR customer_id = '1293')
  ");
  $row = mysql_fetch_assoc($result_1);
  if($row['totaal'] != NULL) { echo $row['totaal'] . " EUR"; }
  ?>
  </td>
  <td><?php 
  $result_1 = mysql_query("SELECT status, customer_id, created_at, ROUND(sum(base_grand_total)) AS totaal FROM sales_flat_order WHERE status= 'complete' && YEAR(created_at) = '2010' && 
  (customer_id='1167' OR customer_id = '1293')
  ");
  $row = mysql_fetch_assoc($result_1);
  if($row['totaal'] != NULL) { echo $row['totaal'] . " EUR"; }
  ?></td>
  <td><?php 
  $result_1 = mysql_query("SELECT status, customer_id, created_at, ROUND(sum(base_grand_total)) AS totaal FROM sales_flat_order WHERE status= 'complete' && YEAR(created_at) = '2011' && 
  (customer_id='1167' OR customer_id = '1293')
  ");
  $row = mysql_fetch_assoc($result_1);
  if($row['totaal'] != NULL) { echo $row['totaal'] . " EUR"; }
  ?></td>
  <td><?php 
  $result_1 = mysql_query("SELECT status, customer_id, created_at, ROUND(sum(base_grand_total)) AS totaal FROM sales_flat_order WHERE status= 'complete' && YEAR(created_at) = '2012' && 
  (customer_id='1167' OR customer_id = '1293')
  ");
  $row = mysql_fetch_assoc($result_1);
  if($row['totaal'] != NULL) { echo $row['totaal'] . " EUR"; }
  ?></td>
  <td><?php 
  $result_1 = mysql_query("SELECT status, customer_id, created_at, ROUND(sum(base_grand_total)) AS totaal FROM sales_flat_order WHERE status= 'complete' && YEAR(created_at) = '2013' && 
  (customer_id='1167' OR customer_id = '1293')
  ");
  $row = mysql_fetch_assoc($result_1);
  if($row['totaal'] != NULL) { echo $row['totaal'] . " EUR"; }
  ?></td>
  <td><?php 
  $result_1 = mysql_query("SELECT status, customer_id, created_at, ROUND(sum(base_grand_total)) AS totaal FROM sales_flat_order WHERE status= 'complete' && YEAR(created_at) = '2014' && 
  (customer_id='1167' OR customer_id = '1293')
  ");
  $row = mysql_fetch_assoc($result_1);
  if($row['totaal'] != NULL) { echo $row['totaal'] . " EUR"; }
  ?></td>
</tr>
<tr>
  <th>Tactics</th>
  <td><?php 
  $result_1 = mysql_query("SELECT status, customer_id, created_at, ROUND(sum(base_grand_total)) AS totaal FROM sales_flat_order WHERE status= 'complete' && YEAR(created_at) = '2009' && 
  (customer_id='938' OR customer_id = '3160' OR customer_id = '5052')
  ");
  $row = mysql_fetch_assoc($result_1);
  if($row['totaal'] != NULL) { echo $row['totaal'] . " EUR"; }
  ?>
  </td>
  <td><?php 
  $result_1 = mysql_query("SELECT status, customer_id, created_at, ROUND(sum(base_grand_total)) AS totaal FROM sales_flat_order WHERE status= 'complete' && YEAR(created_at) = '2010' && 
  (customer_id='938' OR customer_id = '3160' OR customer_id = '5052')
  ");
  $row = mysql_fetch_assoc($result_1);
  if($row['totaal'] != NULL) { echo $row['totaal'] . " EUR"; }
  ?></td>
  <td><?php 
  $result_1 = mysql_query("SELECT status, customer_id, created_at, ROUND(sum(base_grand_total)) AS totaal FROM sales_flat_order WHERE status= 'complete' && YEAR(created_at) = '2011' && 
  (customer_id='938' OR customer_id = '3160' OR customer_id = '5052')
  ");
  $row = mysql_fetch_assoc($result_1);
  if($row['totaal'] != NULL) { echo $row['totaal'] . " EUR"; }
  ?></td>
  <td><?php 
  $result_1 = mysql_query("SELECT status, customer_id, created_at, ROUND(sum(base_grand_total)) AS totaal FROM sales_flat_order WHERE status= 'complete' && YEAR(created_at) = '2012' && 
  (customer_id='938' OR customer_id = '3160' OR customer_id = '5052')
  ");
  $row = mysql_fetch_assoc($result_1);
  if($row['totaal'] != NULL) { echo $row['totaal'] . " EUR"; }
  ?></td>
  <td><?php 
  $result_1 = mysql_query("SELECT status, customer_id, created_at, ROUND(sum(base_grand_total)) AS totaal FROM sales_flat_order WHERE status= 'complete' && YEAR(created_at) = '2013' && 
  (customer_id='938' OR customer_id = '3160' OR customer_id = '5052')
  ");
  $row = mysql_fetch_assoc($result_1);
  if($row['totaal'] != NULL) { echo $row['totaal'] . " EUR"; }
  ?></td>
  <td><?php 
  $result_1 = mysql_query("SELECT status, customer_id, created_at, ROUND(sum(base_grand_total)) AS totaal FROM sales_flat_order WHERE status= 'complete' && YEAR(created_at) = '2014' && 
  (customer_id='938' OR customer_id = '3160' OR customer_id = '5052')
  ");
  $row = mysql_fetch_assoc($result_1);
  if($row['totaal'] != NULL) { echo $row['totaal'] . " EUR"; }
  ?></td>
</tr>
<tr>
  <th>Takoma</th>
  <td><?php 
  $result_1 = mysql_query("SELECT status, customer_id, created_at, ROUND(sum(base_grand_total)) AS totaal FROM sales_flat_order WHERE status= 'complete' && YEAR(created_at) = '2009' && 
  (customer_id='14' OR customer_id = '1527')
  ");
  $row = mysql_fetch_assoc($result_1);
  if($row['totaal'] != NULL) { echo $row['totaal'] . " EUR"; }
  ?>
  </td>
  <td><?php 
  $result_1 = mysql_query("SELECT status, customer_id, created_at, ROUND(sum(base_grand_total)) AS totaal FROM sales_flat_order WHERE status= 'complete' && YEAR(created_at) = '2010' && 
  (customer_id='14' OR customer_id = '1527')
  ");
  $row = mysql_fetch_assoc($result_1);
  if($row['totaal'] != NULL) { echo $row['totaal'] . " EUR"; }
  ?></td>
  <td><?php 
  $result_1 = mysql_query("SELECT status, customer_id, created_at, ROUND(sum(base_grand_total)) AS totaal FROM sales_flat_order WHERE status= 'complete' && YEAR(created_at) = '2011' && 
  (customer_id='14' OR customer_id = '1527')
  ");
  $row = mysql_fetch_assoc($result_1);
  if($row['totaal'] != NULL) { echo $row['totaal'] . " EUR"; }
  ?></td>
  <td><?php 
  $result_1 = mysql_query("SELECT status, customer_id, created_at, ROUND(sum(base_grand_total)) AS totaal FROM sales_flat_order WHERE status= 'complete' && YEAR(created_at) = '2012' && 
  (customer_id='14' OR customer_id = '1527')
  ");
  $row = mysql_fetch_assoc($result_1);
  if($row['totaal'] != NULL) { echo $row['totaal'] . " EUR"; }
  ?></td>
  <td><?php 
  $result_1 = mysql_query("SELECT status, customer_id, created_at, ROUND(sum(base_grand_total)) AS totaal FROM sales_flat_order WHERE status= 'complete' && YEAR(created_at) = '2013' && 
  (customer_id='14' OR customer_id = '1527')
  ");
  $row = mysql_fetch_assoc($result_1);
  if($row['totaal'] != NULL) { echo $row['totaal'] . " EUR"; }
  ?></td>
  <td><?php 
  $result_1 = mysql_query("SELECT status, customer_id, created_at, ROUND(sum(base_grand_total)) AS totaal FROM sales_flat_order WHERE status= 'complete' && YEAR(created_at) = '2014' && 
  (customer_id='14' OR customer_id = '1527')
  ");
  $row = mysql_fetch_assoc($result_1);
  if($row['totaal'] != NULL) { echo $row['totaal'] . " EUR"; }
  ?></td>
</tr>
<tr>
  <th>Tecdoc</th>
  <td><?php 
  $result_1 = mysql_query("SELECT status, customer_id, created_at, ROUND(sum(base_grand_total)) AS totaal FROM sales_flat_order WHERE status= 'complete' && YEAR(created_at) = '2009' && 
  customer_id='22'
  ");
  $row = mysql_fetch_assoc($result_1);
  if($row['totaal'] != NULL) { echo $row['totaal'] . " EUR"; }
  ?>
  </td>
  <td><?php 
  $result_1 = mysql_query("SELECT status, customer_id, created_at, ROUND(sum(base_grand_total)) AS totaal FROM sales_flat_order WHERE status= 'complete' && YEAR(created_at) = '2010' && 
  customer_id='22'
  ");
  $row = mysql_fetch_assoc($result_1);
  if($row['totaal'] != NULL) { echo $row['totaal'] . " EUR"; }
  ?></td>
  <td><?php 
  $result_1 = mysql_query("SELECT status, customer_id, created_at, ROUND(sum(base_grand_total)) AS totaal FROM sales_flat_order WHERE status= 'complete' && YEAR(created_at) = '2011' && 
  customer_id='22'
  ");
  $row = mysql_fetch_assoc($result_1);
  if($row['totaal'] != NULL) { echo $row['totaal'] . " EUR"; }
  ?></td>
  <td><?php 
  $result_1 = mysql_query("SELECT status, customer_id, created_at, ROUND(sum(base_grand_total)) AS totaal FROM sales_flat_order WHERE status= 'complete' && YEAR(created_at) = '2012' && 
  customer_id='22'
  ");
  $row = mysql_fetch_assoc($result_1);
  if($row['totaal'] != NULL) { echo $row['totaal'] . " EUR"; }
  ?></td>
  <td><?php 
  $result_1 = mysql_query("SELECT status, customer_id, created_at, ROUND(sum(base_grand_total)) AS totaal FROM sales_flat_order WHERE status= 'complete' && YEAR(created_at) = '2013' && 
  customer_id='22'
  ");
  $row = mysql_fetch_assoc($result_1);
  if($row['totaal'] != NULL) { echo $row['totaal'] . " EUR"; }
  ?></td>
  <td><?php 
  $result_1 = mysql_query("SELECT status, customer_id, created_at, ROUND(sum(base_grand_total)) AS totaal FROM sales_flat_order WHERE status= 'complete' && YEAR(created_at) = '2014' && 
  customer_id='22'
  ");
  $row = mysql_fetch_assoc($result_1);
  if($row['totaal'] != NULL) { echo $row['totaal'] . " EUR"; }
  ?></td>
</tr>
<tr>
  <th>TP3</th>
  <td><?php 
  $result_1 = mysql_query("SELECT status, customer_id, created_at, ROUND(sum(base_grand_total)) AS totaal FROM sales_flat_order WHERE status= 'complete' && YEAR(created_at) = '2009' && 
  (customer_id='1166' OR customer_id = '2405' OR customer_id = '2581' OR customer_id = '7279' OR customer_id = '7281')
  ");
  $row = mysql_fetch_assoc($result_1);
  if($row['totaal'] != NULL) { echo $row['totaal'] . " EUR"; }
  ?>
  </td>
  <td><?php 
  $result_1 = mysql_query("SELECT status, customer_id, created_at, ROUND(sum(base_grand_total)) AS totaal FROM sales_flat_order WHERE status= 'complete' && YEAR(created_at) = '2010' && 
  (customer_id='1166' OR customer_id = '2405' OR customer_id = '2581' OR customer_id = '7279' OR customer_id = '7281')
  ");
  $row = mysql_fetch_assoc($result_1);
  if($row['totaal'] != NULL) { echo $row['totaal'] . " EUR"; }
  ?></td>
  <td><?php 
  $result_1 = mysql_query("SELECT status, customer_id, created_at, ROUND(sum(base_grand_total)) AS totaal FROM sales_flat_order WHERE status= 'complete' && YEAR(created_at) = '2011' && 
  (customer_id='1166' OR customer_id = '2405' OR customer_id = '2581' OR customer_id = '7279' OR customer_id = '7281')
  ");
  $row = mysql_fetch_assoc($result_1);
  if($row['totaal'] != NULL) { echo $row['totaal'] . " EUR"; }
  ?></td>
  <td><?php 
  $result_1 = mysql_query("SELECT status, customer_id, created_at, ROUND(sum(base_grand_total)) AS totaal FROM sales_flat_order WHERE status= 'complete' && YEAR(created_at) = '2012' && 
  (customer_id='1166' OR customer_id = '2405' OR customer_id = '2581' OR customer_id = '7279' OR customer_id = '7281')
  ");
  $row = mysql_fetch_assoc($result_1);
  if($row['totaal'] != NULL) { echo $row['totaal'] . " EUR"; }
  ?></td>
  <td><?php 
  $result_1 = mysql_query("SELECT status, customer_id, created_at, ROUND(sum(base_grand_total)) AS totaal FROM sales_flat_order WHERE status= 'complete' && YEAR(created_at) = '2013' && 
  (customer_id='1166' OR customer_id = '2405' OR customer_id = '2581' OR customer_id = '7279' OR customer_id = '7281')
  ");
  $row = mysql_fetch_assoc($result_1);
  if($row['totaal'] != NULL) { echo $row['totaal'] . " EUR"; }
  ?></td>
  <td><?php 
  $result_1 = mysql_query("SELECT status, customer_id, created_at, ROUND(sum(base_grand_total)) AS totaal FROM sales_flat_order WHERE status= 'complete' && YEAR(created_at) = '2014' && 
  (customer_id='1166' OR customer_id = '2405' OR customer_id = '2581' OR customer_id = '7279' OR customer_id = '7281')
  ");
  $row = mysql_fetch_assoc($result_1);
  if($row['totaal'] != NULL) { echo $row['totaal'] . " EUR"; }
  ?></td>
</tr>
<tr>
  <th>Triumph</th>
  <td><?php 
  $result_1 = mysql_query("SELECT status, customer_id, created_at, ROUND(sum(base_grand_total)) AS totaal FROM sales_flat_order WHERE status= 'complete' && YEAR(created_at) = '2009' && 
  customer_id='6397'
  ");
  $row = mysql_fetch_assoc($result_1);
  if($row['totaal'] != NULL) { echo $row['totaal'] . " EUR"; }
  ?>
  </td>
  <td><?php 
  $result_1 = mysql_query("SELECT status, customer_id, created_at, ROUND(sum(base_grand_total)) AS totaal FROM sales_flat_order WHERE status= 'complete' && YEAR(created_at) = '2010' && 
  customer_id='6397'
  ");
  $row = mysql_fetch_assoc($result_1);
  if($row['totaal'] != NULL) { echo $row['totaal'] . " EUR"; }
  ?></td>
  <td><?php 
  $result_1 = mysql_query("SELECT status, customer_id, created_at, ROUND(sum(base_grand_total)) AS totaal FROM sales_flat_order WHERE status= 'complete' && YEAR(created_at) = '2011' && 
  customer_id='6397'
  ");
  $row = mysql_fetch_assoc($result_1);
  if($row['totaal'] != NULL) { echo $row['totaal'] . " EUR"; }
  ?></td>
  <td><?php 
  $result_1 = mysql_query("SELECT status, customer_id, created_at, ROUND(sum(base_grand_total)) AS totaal FROM sales_flat_order WHERE status= 'complete' && YEAR(created_at) = '2012' && 
  customer_id='6397'
  ");
  $row = mysql_fetch_assoc($result_1);
  if($row['totaal'] != NULL) { echo $row['totaal'] . " EUR"; }
  ?></td>
  <td><?php 
  $result_1 = mysql_query("SELECT status, customer_id, created_at, ROUND(sum(base_grand_total)) AS totaal FROM sales_flat_order WHERE status= 'complete' && YEAR(created_at) = '2013' && 
  customer_id='6397'
  ");
  $row = mysql_fetch_assoc($result_1);
  if($row['totaal'] != NULL) { echo $row['totaal'] . " EUR"; }
  ?></td>
  <td><?php 
  $result_1 = mysql_query("SELECT status, customer_id, created_at, ROUND(sum(base_grand_total)) AS totaal FROM sales_flat_order WHERE status= 'complete' && YEAR(created_at) = '2014' && 
  customer_id='6397'
  ");
  $row = mysql_fetch_assoc($result_1);
  if($row['totaal'] != NULL) { echo $row['totaal'] . " EUR"; }
  ?></td>
</tr>
<tr>
  <th>U&amp;I BE</th>
  <td><?php 
  $result_1 = mysql_query("SELECT status, customer_id, created_at, ROUND(sum(base_grand_total)) AS totaal FROM sales_flat_order WHERE status= 'complete' && YEAR(created_at) = '2009' && 
  (customer_id='12' OR customer_id = '2813' OR customer_id = '7488')
  ");
  $row = mysql_fetch_assoc($result_1);
  if($row['totaal'] != NULL) { echo $row['totaal'] . " EUR"; }
  ?>
  </td>
  <td><?php 
  $result_1 = mysql_query("SELECT status, customer_id, created_at, ROUND(sum(base_grand_total)) AS totaal FROM sales_flat_order WHERE status= 'complete' && YEAR(created_at) = '2010' && 
  (customer_id='12' OR customer_id = '2813' OR customer_id = '7488')
  ");
  $row = mysql_fetch_assoc($result_1);
  if($row['totaal'] != NULL) { echo $row['totaal'] . " EUR"; }
  ?></td>
  <td><?php 
  $result_1 = mysql_query("SELECT status, customer_id, created_at, ROUND(sum(base_grand_total)) AS totaal FROM sales_flat_order WHERE status= 'complete' && YEAR(created_at) = '2011' && 
  (customer_id='12' OR customer_id = '2813' OR customer_id = '7488')
  ");
  $row = mysql_fetch_assoc($result_1);
  if($row['totaal'] != NULL) { echo $row['totaal'] . " EUR"; }
  ?></td>
  <td><?php 
  $result_1 = mysql_query("SELECT status, customer_id, created_at, ROUND(sum(base_grand_total)) AS totaal FROM sales_flat_order WHERE status= 'complete' && YEAR(created_at) = '2012' && 
  (customer_id='12' OR customer_id = '2813' OR customer_id = '7488')
  ");
  $row = mysql_fetch_assoc($result_1);
  if($row['totaal'] != NULL) { echo $row['totaal'] . " EUR"; }
  ?></td>
  <td><?php 
  $result_1 = mysql_query("SELECT status, customer_id, created_at, ROUND(sum(base_grand_total)) AS totaal FROM sales_flat_order WHERE status= 'complete' && YEAR(created_at) = '2013' && 
  (customer_id='12' OR customer_id = '2813' OR customer_id = '7488')
  ");
  $row = mysql_fetch_assoc($result_1);
  if($row['totaal'] != NULL) { echo $row['totaal'] . " EUR"; }
  ?></td>
  <td><?php 
  $result_1 = mysql_query("SELECT status, customer_id, created_at, ROUND(sum(base_grand_total)) AS totaal FROM sales_flat_order WHERE status= 'complete' && YEAR(created_at) = '2014' && 
  (customer_id='12' OR customer_id = '2813' OR customer_id = '7488')
  ");
  $row = mysql_fetch_assoc($result_1);
  if($row['totaal'] != NULL) { echo $row['totaal'] . " EUR"; }
  ?></td>
</tr>
<tr>
  <th>U&amp;I NL</th>
  <td><?php 
  $result_1 = mysql_query("SELECT status, customer_id, created_at, ROUND(sum(base_grand_total)) AS totaal FROM sales_flat_order WHERE status= 'complete' && YEAR(created_at) = '2009' && 
  customer_id='4614'
  ");
  $row = mysql_fetch_assoc($result_1);
  if($row['totaal'] != NULL) { echo $row['totaal'] . " EUR"; }
  ?>
  </td>
  <td><?php 
  $result_1 = mysql_query("SELECT status, customer_id, created_at, ROUND(sum(base_grand_total)) AS totaal FROM sales_flat_order WHERE status= 'complete' && YEAR(created_at) = '2010' && 
  customer_id='4614'
  ");
  $row = mysql_fetch_assoc($result_1);
  if($row['totaal'] != NULL) { echo $row['totaal'] . " EUR"; }
  ?></td>
  <td><?php 
  $result_1 = mysql_query("SELECT status, customer_id, created_at, ROUND(sum(base_grand_total)) AS totaal FROM sales_flat_order WHERE status= 'complete' && YEAR(created_at) = '2011' && 
  customer_id='4614'
  ");
  $row = mysql_fetch_assoc($result_1);
  if($row['totaal'] != NULL) { echo $row['totaal'] . " EUR"; }
  ?></td>
  <td><?php 
  $result_1 = mysql_query("SELECT status, customer_id, created_at, ROUND(sum(base_grand_total)) AS totaal FROM sales_flat_order WHERE status= 'complete' && YEAR(created_at) = '2012' && 
  customer_id='4614'
  ");
  $row = mysql_fetch_assoc($result_1);
  if($row['totaal'] != NULL) { echo $row['totaal'] . " EUR"; }
  ?></td>
  <td><?php 
  $result_1 = mysql_query("SELECT status, customer_id, created_at, ROUND(sum(base_grand_total)) AS totaal FROM sales_flat_order WHERE status= 'complete' && YEAR(created_at) = '2013' && 
  customer_id='4614'
  ");
  $row = mysql_fetch_assoc($result_1);
  if($row['totaal'] != NULL) { echo $row['totaal'] . " EUR"; }
  ?></td>
  <td><?php 
  $result_1 = mysql_query("SELECT status, customer_id, created_at, ROUND(sum(base_grand_total)) AS totaal FROM sales_flat_order WHERE status= 'complete' && YEAR(created_at) = '2014' && 
  customer_id='4614'
  ");
  $row = mysql_fetch_assoc($result_1);
  if($row['totaal'] != NULL) { echo $row['totaal'] . " EUR"; }
  ?></td>
</tr>
<tr>
  <th>Writec</th>
  <td><?php 
  $result_1 = mysql_query("SELECT status, customer_id, created_at, ROUND(sum(base_grand_total)) AS totaal FROM sales_flat_order WHERE status= 'complete' && YEAR(created_at) = '2009' && 
  (customer_id='1970' OR customer_id = '3196')
  ");
  $row = mysql_fetch_assoc($result_1);
  if($row['totaal'] != NULL) { echo $row['totaal'] . " EUR"; }
  ?>
  </td>
  <td><?php 
  $result_1 = mysql_query("SELECT status, customer_id, created_at, ROUND(sum(base_grand_total)) AS totaal FROM sales_flat_order WHERE status= 'complete' && YEAR(created_at) = '2010' && 
  (customer_id='1970' OR customer_id = '3196')
  ");
  $row = mysql_fetch_assoc($result_1);
  if($row['totaal'] != NULL) { echo $row['totaal'] . " EUR"; }
  ?></td>
  <td><?php 
  $result_1 = mysql_query("SELECT status, customer_id, created_at, ROUND(sum(base_grand_total)) AS totaal FROM sales_flat_order WHERE status= 'complete' && YEAR(created_at) = '2011' && 
  (customer_id='1970' OR customer_id = '3196')
  ");
  $row = mysql_fetch_assoc($result_1);
  if($row['totaal'] != NULL) { echo $row['totaal'] . " EUR"; }
  ?></td>
  <td><?php 
  $result_1 = mysql_query("SELECT status, customer_id, created_at, ROUND(sum(base_grand_total)) AS totaal FROM sales_flat_order WHERE status= 'complete' && YEAR(created_at) = '2012' && 
  (customer_id='1970' OR customer_id = '3196')
  ");
  $row = mysql_fetch_assoc($result_1);
  if($row['totaal'] != NULL) { echo $row['totaal'] . " EUR"; }
  ?></td>
  <td><?php 
  $result_1 = mysql_query("SELECT status, customer_id, created_at, ROUND(sum(base_grand_total)) AS totaal FROM sales_flat_order WHERE status= 'complete' && YEAR(created_at) = '2013' && 
  (customer_id='1970' OR customer_id = '3196')
  ");
  $row = mysql_fetch_assoc($result_1);
  if($row['totaal'] != NULL) { echo $row['totaal'] . " EUR"; }
  ?></td>
  <td><?php 
  $result_1 = mysql_query("SELECT status, customer_id, created_at, ROUND(sum(base_grand_total)) AS totaal FROM sales_flat_order WHERE status= 'complete' && YEAR(created_at) = '2014' && 
  (customer_id='1970' OR customer_id = '3196')
  ");
  $row = mysql_fetch_assoc($result_1);
  if($row['totaal'] != NULL) { echo $row['totaal'] . " EUR"; }
  ?></td>
</tr>
<tr>
  <th>TOTAL PARTNER PURCHASES</th>
  <td>*<br /></td>
  <td><?php 
  $result_1 = mysql_query("SELECT status, store_id, created_at, ROUND(sum(base_grand_total)) AS totaal FROM sales_flat_order WHERE status= 'complete' && YEAR(created_at) = '2010' && 
  store_id ='16'");
  $row = mysql_fetch_assoc($result_1);
  if($row['totaal'] != NULL) { echo $row['totaal'] . " EUR"; }
  ?><br />
  <?php 
  $result_1 = mysql_query("SELECT status, store_id, created_at, ROUND(sum(base_grand_total)) AS totaal FROM sales_flat_order WHERE status= 'complete' && YEAR(created_at) = '2010' && 
  store_id ='17'");
  $row = mysql_fetch_assoc($result_1);
  if($row['totaal'] != NULL) { echo $row['totaal'] . " USD"; }
  ?><br />
  <?php 
  $result_a = mysql_num_rows(mysql_query("SELECT status, store_id, created_at FROM sales_flat_order WHERE status= 'complete' && YEAR(created_at) = '2010' && 
  (store_id ='16' OR store_id = '17')"));
  echo "#" .$result_a;
  ?>
  </td>
  <td><?php 
  $result_1 = mysql_query("SELECT status, store_id, created_at, ROUND(sum(base_grand_total)) AS totaal FROM sales_flat_order WHERE status= 'complete' && YEAR(created_at) = '2011' && 
  store_id ='16'");
  $row = mysql_fetch_assoc($result_1);
  if($row['totaal'] != NULL) { echo $row['totaal'] . " EUR"; }
  ?><br />
  <?php 
  $result_1 = mysql_query("SELECT status, store_id, created_at, ROUND(sum(base_grand_total)) AS totaal FROM sales_flat_order WHERE status= 'complete' && YEAR(created_at) = '2011' && 
  store_id ='17'");
  $row = mysql_fetch_assoc($result_1);
  if($row['totaal'] != NULL) { echo $row['totaal'] . " USD"; }
  ?>
  <br />
  <?php 
  $result_a = mysql_num_rows(mysql_query("SELECT status, store_id, created_at FROM sales_flat_order WHERE status= 'complete' && YEAR(created_at) = '2011' && 
  (store_id ='16' OR store_id = '17')"));
  echo "#" .$result_a;
  ?></td>
  <td><?php 
  $result_1 = mysql_query("SELECT status, store_id, created_at, ROUND(sum(base_grand_total)) AS totaal FROM sales_flat_order WHERE status= 'complete' && YEAR(created_at) = '2012' && 
  store_id ='16'");
  $row = mysql_fetch_assoc($result_1);
  if($row['totaal'] != NULL) { echo $row['totaal'] . " EUR"; }
  ?><br />
  <?php 
  $result_1 = mysql_query("SELECT status, store_id, created_at, ROUND(sum(base_grand_total)) AS totaal FROM sales_flat_order WHERE status= 'complete' && YEAR(created_at) = '2012' && 
  store_id ='17'");
  $row = mysql_fetch_assoc($result_1);
  if($row['totaal'] != NULL) { echo $row['totaal'] . " USD"; }
  ?>
  <br />
  <?php 
  $result_a = mysql_num_rows(mysql_query("SELECT status, store_id, created_at FROM sales_flat_order WHERE status= 'complete' && YEAR(created_at) = '2012' && 
  (store_id ='16' OR store_id = '17')"));
  echo "#" .$result_a;
  ?></td>
  <td><?php 
  $result_1 = mysql_query("SELECT status, store_id, created_at, ROUND(sum(base_grand_total)) AS totaal FROM sales_flat_order WHERE status= 'complete' && YEAR(created_at) = '2013' && 
  store_id ='16'");
  $row = mysql_fetch_assoc($result_1);
  if($row['totaal'] != NULL) { echo $row['totaal'] . " EUR"; }
  ?><br />
  <?php 
  $result_1 = mysql_query("SELECT status, store_id, created_at, ROUND(sum(base_grand_total)) AS totaal FROM sales_flat_order WHERE status= 'complete' && YEAR(created_at) = '2013' && 
  store_id ='17'");
  $row = mysql_fetch_assoc($result_1);
  if($row['totaal'] != NULL) { echo $row['totaal'] . " USD"; }
  ?>
  <br />
  <?php 
  $result_a = mysql_num_rows(mysql_query("SELECT status, store_id, created_at FROM sales_flat_order WHERE status= 'complete' && YEAR(created_at) = '2013' && 
  (store_id ='16' OR store_id = '17')"));
  echo "#" .$result_a;
  ?></td>
  <td><?php 
  $result_1 = mysql_query("SELECT status, store_id, created_at, ROUND(sum(base_grand_total)) AS totaal FROM sales_flat_order WHERE status= 'complete' && YEAR(created_at) = '2014' && 
  store_id ='16'");
  $row = mysql_fetch_assoc($result_1);
  if($row['totaal'] != NULL) { echo $row['totaal'] . " EUR"; }
  ?><br />
  <?php 
  $result_1 = mysql_query("SELECT status, store_id, created_at, ROUND(sum(base_grand_total)) AS totaal FROM sales_flat_order WHERE status= 'complete' && YEAR(created_at) = '2014' && 
  store_id ='17'");
  $row = mysql_fetch_assoc($result_1);
  if($row['totaal'] != NULL) { echo $row['totaal'] . " USD"; }
  ?>
  <br />
  <?php 
  $result_a = mysql_num_rows(mysql_query("SELECT status, store_id, created_at FROM sales_flat_order WHERE status= 'complete' && YEAR(created_at) = '2014' && 
  (store_id ='16' OR store_id = '17')"));
  echo "#" .$result_a;
  ?></td>
</tr>
<tr>
  <th>TOTAL NON-PARTNER PURCHASES</th>
  <td><br />
    *</td>
  <td><?php 
  $result_1 = mysql_query("SELECT status, store_id, created_at, ROUND(sum(base_grand_total)) AS totaal FROM sales_flat_order WHERE status= 'complete' && YEAR(created_at) = '2010' && 
  store_id ='1'
  ");
  $row = mysql_fetch_assoc($result_1);
  if($row['totaal'] != NULL) { echo $row['totaal'] . " EUR"; }
  ?><br />
  <?php 
  $result_1 = mysql_query("SELECT status, store_id, created_at, ROUND(sum(base_grand_total)) AS totaal FROM sales_flat_order WHERE status= 'complete' && YEAR(created_at) = '2010' && 
  store_id ='5'
  ");
  $row = mysql_fetch_assoc($result_1);
  if($row['totaal'] != NULL) { echo $row['totaal'] . " USD"; }
  ?>
  <br />
  <?php 
  $result_a = mysql_num_rows(mysql_query("SELECT status, store_id, created_at FROM sales_flat_order WHERE status= 'complete' && YEAR(created_at) = '2010' && 
  (store_id ='1' OR store_id = '5')"));
  echo "#" .$result_a;
  ?></td>
  <td><?php 
  $result_1 = mysql_query("SELECT status, store_id, created_at, ROUND(sum(base_grand_total)) AS totaal FROM sales_flat_order WHERE status= 'complete' && YEAR(created_at) = '2011' && 
  store_id ='1'
  ");
  $row = mysql_fetch_assoc($result_1);
  if($row['totaal'] != NULL) { echo $row['totaal'] . " EUR"; }
  ?><br />
  <?php 
  $result_1 = mysql_query("SELECT status, store_id, created_at, ROUND(sum(base_grand_total)) AS totaal FROM sales_flat_order WHERE status= 'complete' && YEAR(created_at) = '2011' && 
  store_id ='5'
  ");
  $row = mysql_fetch_assoc($result_1);
  if($row['totaal'] != NULL) { echo $row['totaal'] . " USD"; }
  ?>
  <br />
  <?php 
  $result_a = mysql_num_rows(mysql_query("SELECT status, store_id, created_at FROM sales_flat_order WHERE status= 'complete' && YEAR(created_at) = '2011' && 
  (store_id ='1' OR store_id = '5')"));
  echo "#" .$result_a;
  ?></td>
  <td><?php 
  $result_1 = mysql_query("SELECT status, store_id, created_at, ROUND(sum(base_grand_total)) AS totaal FROM sales_flat_order WHERE status= 'complete' && YEAR(created_at) = '2012' && 
  store_id ='1'
  ");
  $row = mysql_fetch_assoc($result_1);
  if($row['totaal'] != NULL) { echo $row['totaal'] . " EUR"; }
  ?><br />
  <?php 
  $result_1 = mysql_query("SELECT status, store_id, created_at, ROUND(sum(base_grand_total)) AS totaal FROM sales_flat_order WHERE status= 'complete' && YEAR(created_at) = '2012' && 
  store_id ='5'
  ");
  $row = mysql_fetch_assoc($result_1);
  if($row['totaal'] != NULL) { echo $row['totaal'] . " USD"; }
  ?>
  <br />
  <?php 
  $result_a = mysql_num_rows(mysql_query("SELECT status, store_id, created_at FROM sales_flat_order WHERE status= 'complete' && YEAR(created_at) = '2012' && 
  (store_id ='1' OR store_id = '5')"));
  echo "#" .$result_a;
  ?></td>
  <td><?php 
  $result_1 = mysql_query("SELECT status, store_id, created_at, ROUND(sum(base_grand_total)) AS totaal FROM sales_flat_order WHERE status= 'complete' && YEAR(created_at) = '2013' && 
  store_id ='1'
  ");
  $row = mysql_fetch_assoc($result_1);
  if($row['totaal'] != NULL) { echo $row['totaal'] . " EUR"; }
  ?><br />
  <?php 
  $result_1 = mysql_query("SELECT status, store_id, created_at, ROUND(sum(base_grand_total)) AS totaal FROM sales_flat_order WHERE status= 'complete' && YEAR(created_at) = '2013' && 
  store_id ='5'
  ");
  $row = mysql_fetch_assoc($result_1);
  if($row['totaal'] != NULL) { echo $row['totaal'] . " USD"; }
  ?>
  <br />
  <?php 
  $result_a = mysql_num_rows(mysql_query("SELECT status, store_id, created_at FROM sales_flat_order WHERE status= 'complete' && YEAR(created_at) = '2013' && 
  (store_id ='1' OR store_id = '5')"));
  echo "#" .$result_a;
  ?></td>
  <td><?php 
  $result_1 = mysql_query("SELECT status, store_id, created_at, ROUND(sum(base_grand_total)) AS totaal FROM sales_flat_order WHERE status= 'complete' && YEAR(created_at) = '2014' && 
  store_id ='1'
  ");
  $row = mysql_fetch_assoc($result_1);
  if($row['totaal'] != NULL) { echo $row['totaal'] . " EUR"; }
  ?><br />
  <?php 
  $result_1 = mysql_query("SELECT status, store_id, created_at, ROUND(sum(base_grand_total)) AS totaal FROM sales_flat_order WHERE status= 'complete' && YEAR(created_at) = '2014' && 
  store_id ='5'
  ");
  $row = mysql_fetch_assoc($result_1);
  if($row['totaal'] != NULL) { echo $row['totaal'] . " USD"; }
  ?>
  <br />
  <?php 
  $result_a = mysql_num_rows(mysql_query("SELECT status, store_id, created_at FROM sales_flat_order WHERE status= 'complete' && YEAR(created_at) = '2014' && 
  (store_id ='1' OR store_id = '5')"));
  echo "#" .$result_a;
  ?></td>
</tr>

  <?php






?>

</table>
*Partner purchases in 2009 not in seperate partner store.<br />
# amount of orders
<?php
}
?>
                                </div>
                            </div>
                            <!-- /block -->


</div>
</div>
</div>
</div>
            <hr>
<?php
include("../footer.php");
?>
</html>