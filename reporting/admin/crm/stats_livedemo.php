<?php
$rootdir="../../";
$page="Statistics livedemo";
include("../connect.php");
include("../connect_db234.php");
include("../header.php");
?>

<body>
<?php
include("../topnav.php");
?>
<div class="container-fluid">
  <div class="row-fluid">
    <?php
				include("../sidenav.php");
				?>

<?php 
if (!isset($_SESSION["username"])){
	echo $niet_ingelogd;
}
// hieronder staan de functies die toegelaten worden tot deze pagina
elseif($_SESSION["function"] != "admin" && $_SESSION["function"] != "marketing"){
	echo $geen_toegang;
}else{
?>
    <div class="span10" id="content">
      <div class="row-fluid">
        <div class="span6"> 
          <!-- block -->
          <div class="block">
            <div class="navbar navbar-inner block-header">
              <div class="muted pull-left">Hits by country</div>
            </div>
            <div class="block-content collapse in">
              <table class="table table-striped">
                <thead>
                  <tr>
                    <th>&nbsp;</th>
                    <th>Hits</th>
                  </tr>
                </thead>
                <tbody>
<?php
$result = mysql_query("SELECT country, COUNT(*) AS totalen FROM reporting_live_demo_results GROUP BY country ORDER BY totalen DESC", $db); 

while($rijtje = mysql_fetch_array($result))
{
echo "<tr><td>";
if($rijtje['country']=='') { echo "<em>unknown</em>"; } else { echo $rijtje['country']; } 
echo "</td><td>" . $rijtje['totalen'] . "</td></tr>";
}?>

                </tbody>
              </table>
            </div>
          </div>
          <!-- /block --> 
        </div>
        <div class="span6"> 
          <!-- block -->
          <div class="block">
            <div class="navbar navbar-inner block-header">
              <div class="muted pull-left">General information (stats started at Feb 27, 2014)</div>
              <div class="pull-right"><span class="badge badge-info"></span> </div>
            </div>
            <div class="block-content collapse in">
              <?php
$result = mysql_query("SELECT * FROM reporting_live_demo_results WHERE ip != '213.119.25.89'",  $db); // exclude HQ
$rijtje = mysql_num_rows($result);
echo "Total hits: <strong>" .$rijtje . "</strong><br />"; 

$result = mysql_query("SELECT * FROM reporting_live_demo_results GROUP BY ip",  $db); 
$rijtje = mysql_num_rows($result);
echo "Unique visitors: <strong>" .$rijtje . "</strong><br />"; 

$result = mysql_query("SELECT * FROM reporting_live_demo_results WHERE country != '' GROUP BY country",  $db); 
$rijtje = mysql_num_rows($result);
echo "Different countries: <strong>" .$rijtje . "</strong><br />"; 

$result = mysql_query("select distinct d.ip, r.ip, r.referer from reporting_live_demo AS d, reporting_live_demo_results AS r
WHERE r.ip = d.ip && referer LIKE '%google%'", $db);
$rijtje = mysql_num_rows($result);
echo "Registrations from Google Adwords: <strong>" .$rijtje . "</strong><br />"; 
?>
            </div>
          </div>
          <!-- /block --> 
        </div>
      </div>
      
      
      
      
      
      
<div class="row-fluid">
        <div class="span12"> 
          <!-- block -->
          <div class="block">
            <div class="navbar navbar-inner block-header">
              <div class="muted pull-left">Hits per week</div>
              <div class="pull-right"> </div>
            </div>
            <div class="block-content collapse in">
              <table class="table table-striped">
                <tr>
                  <th>Week</th>
                  <th>Hits</th>
                  <th>Unique visits</th>
                </tr>
<?php
$result = mysql_query("SELECT COUNT(DISTINCT ip) AS ipe, WEEK(date) AS week, YEAR(date) AS jaar, COUNT(*) AS totalen FROM reporting_live_demo_results GROUP BY WEEK(date)", $db); // overview per week
// COUNT() ignores null values so this declares a small difference

while($rijtje = mysql_fetch_array($result))
{
echo "<tr><td>";
echo $rijtje['week'] . " (" . $rijtje['jaar'] . ")";






echo "</td><td>" . $rijtje['totalen'] . "</td>";
echo "<td>" . $rijtje['ipe'] . "</td>";
echo "</tr>";
}?>
              </table>
            </div>
          </div>
          <!-- /block --> 
        </div>
      </div>      
      
      
      
      
      
      
      
      <div class="row-fluid">
        <div class="span12"> 
          <!-- block -->
          <div class="block">
            <div class="navbar navbar-inner block-header">
              <div class="muted pull-left">Where do visitors come from?</div>
              <div class="pull-right"> </div>
            </div>
            <div class="block-content collapse in">
              <table class="table table-striped">
                <tr>
                  <th>Internet address</th>
                  <th>Qty</th>
                </tr>
<?php
$result = mysql_query("SELECT referer, COUNT(*) AS totalen FROM reporting_live_demo_results WHERE ip != '213.119.25.89' GROUP BY referer ORDER BY totalen DESC", $db); // exclude HQ

while($rijtje = mysql_fetch_array($result))
{
echo "<tr><td>";
//if($rijtje['referer']=='') { echo "<em>unknown</em>"; } else { echo substr($rijtje['referer'],0, 100) . " ..."; } 
if($rijtje['referer']=='') { echo "<em>unknown</em>"; } 

elseif(strpos($rijtje['referer'], 'google'))
{
$pieces = explode("q=", $rijtje['referer']);
//echo $pieces[1]; 
$pieces2 = explode("&", $pieces[1]);
echo "Google search string: " . str_replace("%20", " " , $pieces2[0]); 	
}


else { 
echo substr($rijtje['referer'],0, 100) . " ...";
} 
echo "</td><td>" . $rijtje['totalen'] . "</td></tr>";
}?>
              </table>
            </div>
          </div>
          <!-- /block --> 
        </div>
      </div>
      
      
      
      
      
      
      
      
      
      
      
      
      

      <?php
}
?>
    </div>
  </div>
  <hr>
  <?php
include("../footer.php");
?>