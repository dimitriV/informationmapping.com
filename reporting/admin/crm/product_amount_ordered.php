<?php
$rootdir="../../";
$page="Products: amounts ordered";
include("../connect.php");
include("../header.php");
?>

<body>
<?php
include("../topnav.php");
?>
<div class="container-fluid">
            <div class="row-fluid">
                
                <?php
				include("../sidenav.php");
				?>
                
                <div class="span10" id="content">
                    <div class="row-fluid">
                    	<div class="span12">
                        
                            <!-- block -->
                            <div class="block">
                                <div class="navbar navbar-inner block-header">
                                <h3>Products: amounts ordered</h3>    
                                </div>
                                <div class="block-content collapse in">
<?php 
if (!isset($_SESSION["username"])){
	echo $niet_ingelogd;
}
// hieronder staan de functies die toegelaten worden tot deze pagina
elseif($_SESSION["function"] != "admin"){
	echo $geen_toegang;
}else{
?>
<form action="" method="post">
<input type="text" name="startdate" value="2012-12-31 23:59:59" />
<input type="text" name="enddate" value="2013-12-31 23:59:59" />

<input type="submit" name="verzenden" value="Search" class="btn" />
</form>
<table class="table table-striped">
<tr><th>ID</th><th>Product</th><th>SKU</th><th>Quantity sold</th></tr>

  <?php


$result = mysql_query("SELECT * FROM catalog_product_entity ORDER BY entity_id ASC"); 
$aantal = mysql_num_rows($result); 
echo "<p>Total products: " . $aantal . "</p>";

function replace_product($nummerke)
{
	$sql_user = mysql_query("SELECT * FROM catalog_product_entity_varchar WHERE entity_id = '$nummerke' && attribute_id = '56'");
	$report_user = mysql_fetch_assoc($sql_user);
	
	$out = $report_user['value'];
	return $out;
}

$verzenden = isset($_POST['verzenden']) ? $_POST['verzenden'] : '';
if($verzenden)
{
$start_date = $_POST['startdate']; 
$end_date = $_POST['enddate'];

$result2 = mysql_query("SELECT *,SUM( it.qty_ordered ) AS cnt_rij, it.product_id AS product FROM sales_flat_order_item AS it, sales_flat_order AS ord WHERE it.order_id = ord.entity_id && ord.status = 'complete' && it.product_type != 'bundle'
&& ord.created_at > '$start_date' && ord.created_at < '$end_date'
GROUP BY it.product_id
ORDER BY cnt_rij DESC");
}
else
{

$result2 = mysql_query("SELECT *,SUM( it.qty_ordered ) AS cnt_rij, it.product_id AS product FROM sales_flat_order_item AS it, sales_flat_order AS ord WHERE it.order_id = ord.entity_id && ord.status = 'complete' && it.product_type != 'bundle'
GROUP BY it.product_id
ORDER BY cnt_rij DESC");
}

while($report=mysql_fetch_array($result2)){
	echo "<tr>";
	echo "<td>" . $report['product'] . "</td>";
	echo "<td>" . replace_product($report['product']) . "</td>";
	echo "<td>" . $report['sku'] . "</td>";
	$aant = $report['cnt_rij'];
	echo "<td>" . round($aant,0) . "</td>";
	echo "</tr>";
}





?>

</table>
<?php
}
?>
                                </div>
                            </div>
                            <!-- /block -->


</div>
</div>
</div>
</div>
            <hr>
<?php
include("../footer.php");
?>
</html>