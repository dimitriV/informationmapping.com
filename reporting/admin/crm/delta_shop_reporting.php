<?php
$rootdir="../../";
$page="Ordered Training Material";
include("../connect.php");
include("../header.php");
?>

<body>
<?php
include("../topnav.php");
?>
<div class="container-fluid">
            <div class="row-fluid">
                
                <?php
				include("../sidenav.php");
				?>
                
                <div class="span10" id="content">
                    <div class="row-fluid">
                    	<div class="span12">
                        
                            <!-- block -->
                            <div class="block">
                                <div class="navbar navbar-inner block-header">
                                <h3>Ordered Training Material</h3>     
                                </div>
                                <div class="block-content collapse in">

<?php 
if (!isset($_SESSION["username"])){
	echo $niet_ingelogd;
}
// hieronder staan de functies die toegelaten worden tot deze pagina
elseif($_SESSION["function"] != "admin"){
	echo $geen_toegang;
}else{
?>

  <?php
$verzenden = isset($_POST['verzenden']) ? $_POST['verzenden'] : '';
if($verzenden)
{
$start_date = $_POST['startdate']; 
$end_date = $_POST['enddate'];

function replace_order($prod)
{

$start_date = $_POST['startdate']; 
$end_date = $_POST['enddate'];
	$test = "SELECT o.entity_id, o.status, o.created_at, o.customer_id, i.order_id, i.product_id, i.qty_ordered, SUM(i.qty_ordered) AS sum 
	FROM sales_flat_order AS o, sales_flat_order_item AS i
WHERE o.entity_id = i.order_id && o.status = 'complete' && i.product_id = '$prod' && o.created_at > '$start_date' && o.created_at < '$end_date'";
	$sql_user = mysql_query($test);
	
	$report_user = mysql_fetch_assoc($sql_user);
	
	$out = round($report_user['sum']);
	return $out;
}// einde function


} else {


function replace_order($prod)
{
	$sql_user = mysql_query("SELECT o.entity_id, o.status, o.customer_id, i.order_id, i.product_id, i.qty_ordered, SUM(i.qty_ordered) AS sum 
	FROM sales_flat_order AS o, sales_flat_order_item AS i
WHERE o.entity_id = i.order_id && o.status = 'complete' && i.product_id = '$prod'");
	$report_user = mysql_fetch_assoc($sql_user);
	
	$out = round($report_user['sum']);
	return $out;
}
}// einde function

?>

<form action="" method="post">
Start date: <input type="text" name="startdate" value="2013-12-31" placeholder="2013-12-31" />

<!--<input type="text" name="year" value="<?php //echo date("Y"); ?>">
<select name="month">
<option value="01">January</option>
</select>
-->
End date: <input type="text" name="enddate" value="2014-12-31" placeholder="2014-12-31" />

<input type="submit" name="verzenden" value="Search" class="btn" />
</form>

<table class="table table-striped">
<tr><th>Partner</th><th>Product</th><th>Quantity sold</th></tr>
<tr>
  <td>Communication Design Institute</td>
  <td>IMF+MSPP &amp;
    IMF+MSPP (usb)</td>
  <td><?php echo replace_order(364); ?>
  </td>
</tr>
<tr>
  <td>Contexto Didactico</td>
  <td>IMF+MPPD</td>
  <td><?php echo replace_order(318); ?></td>
</tr>
<tr>
  <td>Global TNA</td>
  <td>IMF+MSPP (usb)<br>
    IMF+MBCO (usb)</td>
  <td><?php echo replace_order(380); ?><br />
  <?php echo replace_order(382); ?>
  </td>
</tr>
<!--<tr>
  <td>Indocita</td>
  <td>&nbsp;</td>
  <td>&nbsp;</td>
</tr>-->
<tr>
  <td>IM Canada</td>
  <td><p>IMF+MPPD<br>
    IMF+MBCO<br>
    Plain language<br>
    IMF+WWC<br>
    IMF+MSPP</p></td>
  <td><?php echo replace_order(258); ?><br />
  <?php echo replace_order(260); ?><br />
  <?php echo replace_order(262); ?><br />
  <?php echo replace_order(499); ?><br />
  <?php echo replace_order(501); ?>
  </td>
</tr>
<!--<tr>
  <td>IM US</td>
  <td><p>IMF+MSPP<br>
    IMF+WWC<br>
    IMF+MBCO Nestle (usb)<br>
    Editing Effective Content &amp; Doc<br>
    Plain language<br>
    IMF+MBCO<br>
    IMF+MPPD<br>
    IMF+MSPP Calpers
  </p></td>
  <td><?php echo replace_order(192); ?><br>
    <?php echo replace_order(413); ?><br>
    <?php echo replace_order(264); ?><br>
    <?php echo replace_order(336); ?><br>
    <?php echo replace_order(278); ?><br>
    <?php //echo replace_order(250); ?><br>
    <?php //echo replace_order(250); ?><br>
    <?php //echo replace_order(250); ?></td>
</tr>-->
<tr>
  <td>IM IN</td>
  <td>IMF+MSPP (usb)</td>
  <td><?php echo replace_order(451); ?></td>
</tr>
<tr>
  <td>Key2Know</td>
  <td>IMF+MSPP<br>
    IMF +MPPD<br>
    IMF+MPPD 3d<br>
    IMF+MSPP usb<br>
    
    Nestle 0.5 day<br>
    Nestle DTBD 2d usb<br>
    Nestle DTBD 1 day usb
   </td>
  <td><?php echo replace_order(200); ?><br>
    <?php echo replace_order(202); ?><br>
    <?php echo replace_order(312); ?><br>
    <?php echo replace_order(442); ?><br>
    
    <?php echo replace_order(320); ?><br>
    <?php echo replace_order(316); ?><br>
    <?php echo replace_order(314); ?>
    </td>
</tr>
<tr>
  <td>Synaps</td>
  <td>DPPD FR<br>
    DPPD NL<br>
    DPPD EN<br>
    IMF+MSPP NL<br>
    IMF+MSPP FR<br>
    IMF+MSPP EN<br>
    AGC IMF+MSPP FR<br>
    AGC IMF+MSPP EN<br>
    AGC IMF+MSPP NL</td>
  <td><?php echo replace_order(169); ?><br>
    <?php echo replace_order(170); ?><br>
    <?php echo replace_order(172); ?><br>
    <?php echo replace_order(250); ?><br>
    <?php echo replace_order(252); ?><br>
    <?php echo replace_order(254); ?><br>
    <?php echo replace_order(424); ?><br>
    <?php echo replace_order(426); ?><br>
    <?php echo replace_order(428); ?></td>
</tr>
<tr>
  <td>TP3</td>
  <td>Powercor IMF+PPD<br>
    IMF+MBCO<br>
    IMF+MPPD<br>
    IMF<br>
    MPPD<br>
    MBCO</td>
  <td>
  <?php echo replace_order(355); ?><br>
    <?php echo replace_order(323); ?><br>
    <?php echo replace_order(325); ?><br>
    <?php echo replace_order(373); ?><br>
    <?php echo replace_order(375); ?><br>
    <?php echo replace_order(377); ?>
  </td>
</tr>
<tr>
  <td>Tactics Ltd</td>
  <td>IMF+MPPD<br>
    IMF+MBCO<br>
    IMF+MSPP</td>
  <td><?php echo replace_order(360); ?><br />
  <?php echo replace_order(362); ?><br />
  <?php echo replace_order(529); ?>
  </td>
</tr>
<tr>
  <td>Takoma</td>
  <td>IMF+MSPP (French)<br>
    IMF+MSPP (English)</td>
  <td><?php echo replace_order(218); ?><br />
  <?php echo replace_order(350); ?>
  </td>
</tr>
<tr>
  <td>Triumph</td>
  <td>IMF+MSPP (usb)</td>
  <td><?php echo replace_order(330); ?></td>
</tr>
<tr>
  <td>U&amp;I Learning BE</td>
  <td>IMF+MSPP (Dutch)<br>
    IMF+MSPP (French)<br>
    IMF+MSPP (English)<br>
    Infrabel IMF+MSPP (Dutch)<br>
    Infrabel IMF+MSPP (French)</td>
  <td><?php echo replace_order(196); ?><br>
    <?php echo replace_order(268); ?><br>
    <?php echo replace_order(220); ?><br>
    <?php echo replace_order(210); ?><br>
    <?php echo replace_order(268); ?></td>
</tr>
<tr>
  <td>U&amp;I Learning NL</td>
  <td>IMF+MSPP (Dutch)<br>
    IMF+MSPP (English)</td>
  <td><?php echo replace_order(198); ?><br /> 
  <?php echo replace_order(216); ?></td>
</tr>
<tr>
  <td>Writec</td>
  <td>IMF+MSPP<br>
    IMF+MPPD<br>
    IMF+MSPP</td>
  <td><?php echo replace_order(214); ?><br>
    <?php echo replace_order(328); ?><br>
    <?php echo replace_order(369); ?>

</td>
</tr>

<?php


/*while($report=mysql_fetch_array($result2)){
	echo "<tr>";
	echo "<td>" . $report['coupon_code'] . "</td>";
	// mysql_query("SELECT * FROM sales_flat_order WHERE coupon_code = ''");

	echo "<td>" . $report['cnt_rij'] . "</td>";
	echo "</tr>";
}*/





?>

</table>
<?php
}
?>
                                </div>
                            </div>
                            <!-- /block -->


</div>
</div>
</div>
</div>
            <hr>
<?php
include("../footer.php");
?>
</html>