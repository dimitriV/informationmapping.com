<?php
$rootdir="../../";
$page="Admin: replace license key";
include("../connect.php");
include("../header.php");
?>

<body>
<?php
include("../topnav.php");
?>
<div class="container-fluid">
            <div class="row-fluid">
                
                <?php
				include("../sidenav.php");
				?>
                
                <div class="span10" id="content">
                    <div class="row-fluid">
                    	<div class="span12">
                        
                            <!-- block -->
                            <div class="block">
                                <div class="navbar navbar-inner block-header">
                                <h3>Replace license keys</h3>     
                                </div>
                                <div class="block-content collapse in">
<?php 
if (!isset($_SESSION["username"])){
	echo $niet_ingelogd;
}
// hieronder staan de functies die toegelaten worden tot deze pagina
elseif($_SESSION["function"] != "admin" && $_SESSION["function"] != "it"){ // make it accessible with function admin and it
	echo $geen_toegang;
}else{
?>

<p><strong>Warning: Do not replace keys in case of a volume key! This form is meant for keys which are replaced due to the tampered with issue.</strong></p>
<p>Fill out the form below and click <strong>Save</strong> to assign the new key to the existing order. <br />Warning: Changes cannot be undone.</p>

<form action="#" method="post">
	Old license key: 
	  <input name="oldlickey" type="text" id="oldlickey" style="width: 300px;" />
	  <br />
    New license key:
    <input name="newlickey" type="text" id="newlickey" style="width: 300px;" />
    <br />
    <br />
    <input type="submit" name="verzenden" value="Save" class="btn" />
</form>


<?php

// HIERONDER JUIST ZOEKSCRIPT VOOR MULTIPLE ZOEKWAARDEN

$oldlickey = isset($_POST['oldlickey']) ? $_POST['oldlickey'] : '';
$newlickey = isset($_POST['newlickey']) ? $_POST['newlickey'] : '';
$verzenden = isset($_POST['verzenden']) ? $_POST['verzenden'] : '';
//$error = "";

if($verzenden)
{
	if(empty($oldlickey))
     $error .= "<li>You don't have entered an old license key.</li>\n";

	if(empty($newlickey))
     $error .= "<li>You don't have entered a new license key.</li>\n";	 

	// read if entered key is a volume key
	$res_vol = mysql_num_rows(mysql_query("SELECT license_id, license_type, last_license_key 
	FROM license_info WHERE last_license_key = '$oldlickey' && license_type = 'volume'"));
	if($res_vol > 0)
     $error .= "<li>You cannot change a volume key.</li>\n";

	// read if key exists in shop
	$res_vol = mysql_num_rows(mysql_query("SELECT last_license_key 
	FROM license_info WHERE last_license_key = '$oldlickey'"));
	if($res_vol < 1)
     $error .= "<li>This key does not exist in the shop database.</li>\n";

	if(isset($error))    
   {    
      // Afdrukken van de errors  
      echo "<p><strong>Your action was not successful:</strong></p>\n";   
      echo "<ul>"; 
      echo $error;    
      echo "</ul>\n"; 

    }
	else
	{
	
	  $result = mysql_query("UPDATE license_info SET last_license_key = '$newlickey' WHERE last_license_key = '$oldlickey'");
	  if($result) { echo "<p>Saved query 1 successfully (tbl license_info)</p>"; } else { echo "<p>Error: Query 1 not saved successfully (tbl license_info)</p>";  }

	  $result2 = mysql_query("UPDATE license_keys SET license_key = '$newlickey' WHERE license_key = '$oldlickey'");
	  if($result2) { echo "<p>Saved query 2 successfully (tbl license_keys)</p>"; } else { echo "<p>Error: Query 2 not saved successfully (tbl license_keys)</p>";  }

	  $result3 = mysql_query("UPDATE license_serialnumbers SET serial_number = '$newlickey' WHERE serial_number = '$oldlickey'");
	  if($result3) { echo "<p>Saved query 3 successfully (tbl license_serialnumbers)</p>"; } else { echo "<p>Error: Query 3 not saved successfully (tbl license_serialnumbers)</p>";  }
	}
}
?>
<?php
}
?>

                                </div>
                            </div>
                            <!-- /block -->


</div>
</div>
</div>
</div>
            <hr>
<?php
include("../footer.php");
?>
</html>