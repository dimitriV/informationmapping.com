<?php
$rootdir="../../";
$page="Customers: order total";
include("../connect.php");
include("../header.php");
?>

<body>
<?php
include("../topnav.php");
?>
<div class="container-fluid">
            <div class="row-fluid">
                
                <?php
				include("../sidenav.php");
				?>
                
                <div class="span10" id="content">
                    <div class="row-fluid">
                    	<div class="span12">
                        
                            <!-- block -->
                            <div class="block">
                                <div class="navbar navbar-inner block-header">
                                <h3>Customers: order total</h3>     
                                </div>
                                <div class="block-content collapse in">

<?php 
if (!isset($_SESSION["username"])){
	echo $niet_ingelogd;
}
// hieronder staan de functies die toegelaten worden tot deze pagina
elseif($_SESSION["function"] != "admin"){
	echo $geen_toegang;
}else{
?>




  <?php


$result = mysql_query("SELECT *
FROM sales_flat_order
"); 
$aantal = mysql_num_rows($result); 
echo "<p>Total orders: " . $aantal . "</p>";

function replace_amounts($nummerke)
{
	
	$nummer = number_format($nummerke, 2, ',', '.'); // afgerond op 2 cijfers na de komma
	return $nummer;
}
?>

<table class="table table-striped">
<tr><th>Customer</th><th>Number of orders</th><th>Total amount excl VAT</th></tr>
<?php



$result2 = mysql_query("
SELECT sales_flat_order.entity_id, sales_flat_order.status, sales_flat_order.store_id, sales_flat_order.customer_id, COUNT(sales_flat_order.base_subtotal_invoiced) AS cnt_rij,
SUM(sales_flat_order.base_subtotal_invoiced) AS sum_rij,
customer_entity.entity_id, customer_entity.email
FROM sales_flat_order, customer_entity
WHERE sales_flat_order.customer_id = customer_entity.entity_id && sales_flat_order.status = 'complete' GROUP BY customer_entity.entity_id ORDER BY sum_rij DESC");








while($report=mysql_fetch_array($result2)){
	echo "<tr>";
	echo "<td>" . $report['email'] . "</td>";
	echo "<td>" . $report['cnt_rij'] . "</td>";
	// currency weergeven
	if($report['store_id'] == "17" || $report['store_id'] == "5")
	{
		$currency = " $";
	}
	else
	{
		$currency = " € ";
	}
	echo "<td>" . replace_amounts($report['sum_rij'],2) . $currency . "</td>";
	echo "</tr>";
}





?>

</table>
<?php
}
?>
                                </div>
                            </div>
                            <!-- /block -->


</div>
</div>
</div>
</div>
            <hr>
<?php
include("../footer.php");
?>
</html>