<?php
$rootdir="../../";
$page="Customers: buyers per product";
include("../connect.php");
include("../header.php");
?>

<body>
<?php
include("../topnav.php");
?>
<div class="container-fluid">
            <div class="row-fluid">
                
                <?php
				include("../sidenav.php");
				?>
                
                <div class="span10" id="content">
                    <div class="row-fluid">
                    	<div class="span12">
                        
                            <!-- block -->
                            <div class="block">
                                <div class="navbar navbar-inner block-header">
                                     
                                </div>
                                <div class="block-content collapse in">
<?php 
if (!isset($_SESSION["username"])){
	echo $niet_ingelogd;
}
// hieronder staan de functies die toegelaten worden tot deze pagina
elseif($_SESSION["function"] != "admin"){
	echo $geen_toegang;
}else{
?>
<h3>Customers: buyers for all FS Pro 2013 versions</h3>

<table class="table table-striped">
<tr><th>Order ID</th><th>E-mail</th><th>First name</th><th>Last name</th><th>Company</th><th>Country</th><th>Created at</th><th>Product name</th><th>Qty</th></tr>
  <?php

function replace_userid($nummerke)
{
	$sql_user = mysql_query("SELECT * FROM customer_entity WHERE entity_id = '$nummerke'");
	$report_user = mysql_fetch_assoc($sql_user);
	
	$out = $report_user['email'];
	return $out;
}
function replace_firstname($nummerke)
{
	$sql_user = mysql_query("SELECT * FROM customer_entity_varchar WHERE entity_id = '$nummerke' && attribute_id = '5'");
	$report_user = mysql_fetch_assoc($sql_user);
	
	$out = $report_user['value'];
	return $out;
}

function replace_lastname($nummerke)
{
	$sql_user = mysql_query("SELECT * FROM customer_entity_varchar WHERE entity_id = '$nummerke' && attribute_id = '7'");
	$report_user = mysql_fetch_assoc($sql_user);
	
	$out = $report_user['value'];
	return $out;
}

function replace_country($nummerke)
{
	$sql_user = mysql_query("SELECT * FROM sales_flat_order_address WHERE parent_id = '$nummerke'");
	$report_user = mysql_fetch_assoc($sql_user);
	
	$out = $report_user['country_id'];
	return $out;
}

function replace_company($nummerke)
{
	$sql_user = mysql_query("SELECT * FROM customer_address_entity_varchar WHERE entity_id = '$nummerke' && attribute_id = '22'");
	$report_user = mysql_fetch_assoc($sql_user);
	
	$out = $report_user['value'];
	return $out;
}

function replace_partner($nummerke)
{
	$sql_user = mysql_query("SELECT * FROM customer_entity_int WHERE entity_id = '$nummerke' && attribute_id = '545'");
	$report_user = mysql_fetch_assoc($sql_user);
	
	$sql_user_partner = mysql_query("SELECT * FROM partner WHERE partner_id = '" . $report_user['value'] . "'");
	$report_user_partner = mysql_fetch_assoc($sql_user_partner);
	
	$out = $report_user_partner['name'];
	return $out;
}

$result = mysql_query("SELECT it.order_id,ord.entity_id,ord.status, ord.increment_id, ord.customer_id,it.product_id,it.created_at,it.name,it.qty_ordered FROM sales_flat_order_item AS it, sales_flat_order AS ord WHERE it.order_id = ord.entity_id && ord.status = 'complete' 
&& (it.product_id = '385' OR it.product_id = '389'
 OR it.product_id = '390' OR it.product_id = '391' OR it.product_id = '396' OR it.product_id = '397' OR it.product_id = '398' OR it.product_id = '399' OR it.product_id = '401' OR it.product_id = '402' OR it.product_id = '403' OR it.product_id = '405' OR it.product_id = '406' OR it.product_id = '410' OR it.product_id = '414' OR it.product_id = '415' OR it.product_id = '418' OR it.product_id = '419' OR it.product_id = '420' OR it.product_id = '421' OR it.product_id = '422' OR it.product_id = '423'
  OR it.product_id = '487' OR it.product_id = '488' OR it.product_id = '489' OR it.product_id = '490' OR it.product_id = '492' OR it.product_id = '493' OR it.product_id = '494' OR it.product_id = '495'
)

ORDER BY it.qty_ordered DESC");

$aantal = mysql_num_rows($result); 
echo "<p>Total order lines: " . $aantal . "</p>";

$totaal_qty_1y=0;
while($report=mysql_fetch_array($result)){
	
	$result_order = mysql_query("SELECT entity_id, parent_id, customer_id, email, firstname, lastname, company FROM sales_flat_order_address WHERE parent_id = '" . $report['entity_id'] . "'");
	$report_order = mysql_fetch_assoc($result_order);
	//$order_number = $report_order['increment_id'];
	//$user_id = $report_order['customer_id'];
	
	echo "<tr>";
	echo "<td>" . $report['increment_id'] . "</td>";
	// als het een guest aankoop betreft, dan email uitlezen uit sales_flat_order_address, zoniet uit customer_entity tbl
	if($report['customer_id']=='') { 
	echo "<td>" . $report_order['email'] . "</td>"; }
	else {
	echo "<td>" . replace_userid($report['customer_id']) . "</td>";	
	}

	echo "<td>" . $report_order['firstname'] . "</td>";
	echo "<td>" . $report_order['lastname'] . "</td>"; 
	// als company niet ingevuld is in het order (billing address), dan kijken of het wel is ingevuld in de customer account
	if($report_order['company']!='')
	{ 
	echo "<td>" . $report_order['company'] . "</td>"; 
	} else {
		echo "<td>" . replace_company($report['customer_id']) . "</td>"; 
	}
	echo "<td>" . replace_country($report['order_id']) . "</td>"; 
	echo "<td>" . $report['created_at'] . "</td>";
	echo "<td>" . $report['name'] . "</td>";
	echo "<td>" . round($report['qty_ordered'],0) . "</td>";
	$totaal_qty_1y+=$report['qty_ordered'];
	echo "</tr>";
	
}
echo "<tr><td></td><td></td><td></td><td></td><td></td><td></td><td></td><td></td><td>" . $totaal_qty_1y . "</td></tr>";
?>
</table>
</table>
<?php
}
?>

                                </div>
                            </div>
                            <!-- /block -->


</div>
</div>
</div>
</div>
            <hr>
<?php
include("../footer.php");
?>
</html>