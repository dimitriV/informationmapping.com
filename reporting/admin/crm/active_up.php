<?php
$rootdir="../../";
$page="Active UP";
include("../connect.php");
include("../header.php");
?>

<body>
<?php
include("../topnav.php");
?>
<div class="container-fluid">
            <div class="row-fluid">
                
                <?php
				include("../sidenav.php");
				?>
                
                <div class="span10" id="content">
                    <div class="row-fluid">
                    	<div class="span12">
                        
                            <!-- block -->
                            <div class="block">
                                <div class="navbar navbar-inner block-header">
                                <h3>Active UP</h3>     
                                </div>
                                <div class="block-content collapse in">


<?php 
if (!isset($_SESSION["username"])){
	echo $niet_ingelogd;
}
// hieronder staan de functies die toegelaten worden tot deze pagina
elseif($_SESSION["function"] != "admin"){
	echo $geen_toegang;
}else{
?>
<form action="" method="post">
<input type="submit" name="exporteren" value="Export to csv" />
</form>
<p><strong>Active UP 1 year</strong></p>
<table class="table table-striped">
<tr><th>Order ID</th><th>E-mail</th><th>First name</th><th>Last name</th><th>Partner</th><th>Country</th><th>Created at</th><th>Product name</th><th>Qty</th></tr>
  <?php
  



function replace_userid($nummerke)
{
	$sql_user = mysql_query("SELECT * FROM customer_entity WHERE entity_id = '$nummerke'");
	$report_user = mysql_fetch_assoc($sql_user);
	
	$out = $report_user['email'];
	return $out;
}

function replace_firstname($nummerke)
{
	$sql_user = mysql_query("SELECT * FROM customer_entity_varchar WHERE entity_id = '$nummerke' && attribute_id = '5'");
	$report_user = mysql_fetch_assoc($sql_user);
	
	$out = $report_user['value'];
	return $out;
}

function replace_lastname($nummerke)
{
	$sql_user = mysql_query("SELECT * FROM customer_entity_varchar WHERE entity_id = '$nummerke' && attribute_id = '7'");
	$report_user = mysql_fetch_assoc($sql_user);
	
	$out = $report_user['value'];
	return $out;
}

function replace_country($nummerke)
{
	$sql_user = mysql_query("SELECT * FROM sales_flat_order_address WHERE parent_id = '$nummerke'");
	$report_user = mysql_fetch_assoc($sql_user);
	
	$out = $report_user['country_id'];
	return $out;
}

function replace_partner($nummerke)
{
	$sql_user = mysql_query("SELECT * FROM customer_entity_int WHERE entity_id = '$nummerke' && attribute_id = '545'");
	$report_user = mysql_fetch_assoc($sql_user);
	
	$sql_user_partner = mysql_query("SELECT * FROM partner WHERE partner_id = '" . $report_user['value'] . "'");
	$report_user_partner = mysql_fetch_assoc($sql_user_partner);
	
	$out = $report_user_partner['name'];
	return $out;
}

$date_1y_up = "2012-07-01 00:00:00"; // current year - 1 year
// Qty_cancelled = 0 && qty_refunded = 0, anders betreft het een cancelled of closed order. Echter in sales_flat_order_item krijgen niet alle producten de status cancelled. Daarom moet er via sales_flat_order de status eerst gecheckt worden. Hiervoor gebruiken we dus een JOIN query

//$result = mysql_query("SELECT item_id, order_id, created_at, product_id, name, qty_ordered FROM sales_flat_order_item WHERE product_id = '164' && created_at > '$date_1y_up' && qty_canceled = '0' && qty_refunded = '0' OR (product_id = '174' && created_at > '$date_1y_up' && qty_canceled = '0' && qty_refunded = '0' ) ORDER BY item_id ASC"); 

// JOIN query
//$result = mysql_query("SELECT * FROM sales_flat_order_item, sales_flat_order WHERE sales_flat_order_item.order_id = sales_flat_order.entity_id && sales_flat_order.status = 'complete' && sales_flat_order_item.created_at > '$date_1y_up' && sales_flat_order_item.product_id = '164' OR (sales_flat_order_item.order_id = sales_flat_order.entity_id && sales_flat_order.status = 'complete' && sales_flat_order_item.created_at > '$date_1y_up' && sales_flat_order_item.product_id = '174') ORDER BY sales_flat_order_item.item_id ASC"); 

// zelfde JOIN query maar verkorte notatie



$result = mysql_query("SELECT * FROM sales_flat_order_item AS it, sales_flat_order AS ord WHERE it.order_id = ord.entity_id && ord.status = 'complete' && it.created_at > '$date_1y_up' && it.product_id = '164' OR (it.order_id = ord.entity_id && ord.status = 'complete' && it.created_at > '$date_1y_up' && it.product_id = '174') ORDER BY it.item_id ASC");

$aantal = mysql_num_rows($result); 
echo "<p>Total order lines: " . $aantal . "</p>";

if($_POST['exporteren'])
{
	  function cleanData(&$str)
  {
    $str = preg_replace("/\t/", "\\t", $str);
    $str = preg_replace("/\r?\n/", "\\n", $str);
    if(strstr($str, '"')) $str = '"' . str_replace('"', '""', $str) . '"';
  }

  // filename for download
  $filename = "Report_active_up_" . date('Ymd') . ".xls";

  header("Content-Disposition: attachment; filename=\"$filename\"");
  header("Content-Type: application/vnd.ms-excel");

  $flag = false;
}

$totaal_qty_1y=0;
while($report=mysql_fetch_array($result)){
	
	$result_order = mysql_query("SELECT * FROM sales_flat_order WHERE entity_id = '" . $report['order_id'] . "'");
	$report_order = mysql_fetch_assoc($result_order);
	$order_number = $report_order['increment_id'];
	$user_id = $report_order['customer_id'];
	
	echo "<tr>";
	//echo "<td>" . $report['order_id'] . "</td>";
	echo "<td>" . $order_number . "</td>";
	echo "<td>" . replace_userid($user_id) . "</td>";
	echo "<td>" . replace_firstname($user_id) . "</td>"; 
	echo "<td>" . replace_lastname($user_id) . "</td>"; 
	echo "<td>" . replace_partner($user_id) . "</td>"; 
	echo "<td>" . replace_country($report['order_id']) . "</td>"; 
	echo "<td>" . $report['created_at'] . "</td>";
	echo "<td>" . $report['name'] . "</td>";
	echo "<td>" . round($report['qty_ordered'],0) . "</td>";
	$totaal_qty_1y+=$report['qty_ordered'];
	echo "</tr>";
	
	// addition xls export
	if(!$flag) {
      // display field/column names as first row
      echo implode("\t", array_keys($row)) . "\r\n";
      $flag = true;
    }
    array_walk($row, 'cleanData');
    echo implode("\t", array_values($row)) . "\r\n";
	
}
echo "<tr><td></td><td></td><td></td><td></td><td></td><td></td><td></td><td></td><td>" . $totaal_qty_1y . "</td></tr>";
?>
</table>
<p><strong>Active UP 2 year</strong></p>
<table class="table table-striped">
<tr><th>Order ID</th><th>E-mail</th><th>First name</th><th>Last name</th><th>Partner</th><th>Country</th><th>Created at</th><th>Product name</th><th>Qty</th></tr>
  <?php

$date_2y_up = "2011-07-01 00:00:00"; // current year - 2 years

$result = mysql_query("SELECT * FROM sales_flat_order_item AS it, sales_flat_order AS ord WHERE it.order_id = ord.entity_id && ord.status = 'complete' && it.created_at > '$date_2y_up' && it.product_id = '167' OR (it.order_id = ord.entity_id && ord.status = 'complete' && it.created_at > '$date_2y_up' && it.product_id = '235') ORDER BY it.item_id ASC");

$aantal = mysql_num_rows($result); 
echo "<p>Total order lines: " . $aantal . "</p>";

$totaal_qty_2y=0;
while($report=mysql_fetch_array($result)){
	
	
	$result_order = mysql_query("SELECT * FROM sales_flat_order WHERE entity_id = '" . $report['order_id'] . "'");
	$report_order = mysql_fetch_assoc($result_order);
	$order_number = $report_order['increment_id'];
	$user_id = $report_order['customer_id'];
	
	echo "<tr>";
	//echo "<td>" . $report['order_id'] . "</td>";
	echo "<td>" . $order_number . "</td>";
	echo "<td>" . replace_userid($user_id) . "</td>";
	echo "<td>" . replace_firstname($user_id) . "</td>"; 
	echo "<td>" . replace_lastname($user_id) . "</td>"; 
	echo "<td>" . replace_partner($user_id) . "</td>"; 
	echo "<td>" . replace_country($report['order_id']) . "</td>"; 
	echo "<td>" . $report['created_at'] . "</td>";
	echo "<td>" . $report['name'] . "</td>";
	echo "<td>" . round($report['qty_ordered'],0) . "</td>";
	$totaal_qty_2y+=$report['qty_ordered'];
	echo "</tr>";
	
}
echo "<tr><td></td><td></td><td></td><td></td><td></td><td></td><td></td><td></td><td>" . $totaal_qty_2y . "</td></tr>";
?>
</table>

<p><strong>Active UP 3 year</strong></p>
<table class="table table-striped">
<tr><th>Order ID</th><th>E-mail</th><th>First name</th><th>Last name</th><th>Partner</th><th>Country</th><th>Created at</th><th>Product name</th><th>Qty</th></tr>
  <?php
 

$date_3y_up = "2010-07-01 00:00:00"; // current year - 3 years

$result = mysql_query("SELECT * FROM sales_flat_order_item AS it, sales_flat_order AS ord WHERE it.order_id = ord.entity_id && ord.status = 'complete' && it.created_at > '$date_3y_up' && it.product_id = '168' OR (it.order_id = ord.entity_id && ord.status = 'complete' && it.created_at > '$date_3y_up' && it.product_id = '236') ORDER BY it.item_id ASC");

$aantal = mysql_num_rows($result); 
echo "<p>Total order lines: " . $aantal . "</p>";

$totaal_qty_3y=0;
while($report=mysql_fetch_array($result)){
	
	$result_order = mysql_query("SELECT * FROM sales_flat_order WHERE entity_id = '" . $report['order_id'] . "'");
	$report_order = mysql_fetch_assoc($result_order);
	$order_number = $report_order['increment_id'];
	$user_id = $report_order['customer_id'];
	
	echo "<tr>";
	//echo "<td>" . $report['order_id'] . "</td>";
	echo "<td>" . $order_number . "</td>";
	echo "<td>" . replace_userid($user_id) . "</td>";
	echo "<td>" . replace_firstname($user_id) . "</td>"; 
	echo "<td>" . replace_lastname($user_id) . "</td>"; 
	echo "<td>" . replace_partner($user_id) . "</td>"; 
	echo "<td>" . replace_country($report['order_id']) . "</td>"; 
	echo "<td>" . $report['created_at'] . "</td>";
	echo "<td>" . $report['name'] . "</td>";
	echo "<td>" . round($report['qty_ordered'],0) . "</td>";
	$totaal_qty_3y+=$report['qty_ordered'];
	echo "</tr>";
	
}
echo "<tr><td></td><td></td><td></td><td></td><td></td><td></td><td></td><td></td><td>" . $totaal_qty_3y . "</td></tr>";
?>
</table>
<?php
}
?>

                                </div>
                            </div>
                            <!-- /block -->


</div>
</div>
</div>
</div>
            <hr>
<?php
include("../footer.php");
?>
</html>