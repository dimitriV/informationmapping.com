<?php
$rootdir="../../";
$page="Statistics webapp";
include("../connect.php");
include("../connect_db234.php");
include("../header.php");
?>

<body>
<?php
include("../topnav.php");
?>
<div class="container-fluid">
  <div class="row-fluid">
    <?php
				include("../sidenav.php");
				?>

<?php 
if (!isset($_SESSION["username"])){
	echo $niet_ingelogd;
}
// hieronder staan de functies die toegelaten worden tot deze pagina
elseif($_SESSION["function"] != "admin"){
	echo $geen_toegang;
}else{
?>
    <div class="span10" id="content">
      <div class="row-fluid">
        <div class="span6"> 
          <!-- block -->
          <div class="block">
            <div class="navbar navbar-inner block-header">
              <div class="muted pull-left">Detailed overview hits</div>
            </div>
            <div class="block-content collapse in">
              <table class="table table-striped">
                <thead>
                  <tr>
                    <th>&nbsp;</th>
                    <th>Hits</th>
                  </tr>
                </thead>
                <tbody>
                  <tr>
                    <th>Homepage demo </th>
                    <td><?php
$result = mysql_query("SELECT * FROM webapp_statistics WHERE page = 'index'", $db4); 
$rijtje = mysql_fetch_assoc($result);
echo $rijtje['counter']; ?></td>
                  </tr>
                  <tr>
                    <th>Information what?</th>
                    <td><?php
$result = mysql_query("SELECT * FROM webapp_statistics WHERE page = 'information_what'", $db4); 
$rijtje = mysql_fetch_assoc($result);
echo $rijtje['counter']; ?></td>
                  </tr>
                  <tr>
                    <th>Did u know?</th>
                    <td><?php
$result = mysql_query("SELECT * FROM webapp_statistics WHERE page = 'did_u_know'", $db4); 
$rijtje = mysql_fetch_assoc($result);
echo $rijtje['counter']; ?></td>
                  </tr>
                  <tr>
                    <th>Find us</th>
                    <td><?php
$result = mysql_query("SELECT * FROM webapp_statistics WHERE page = 'find_us'", $db4); 
$rijtje = mysql_fetch_assoc($result);
echo $rijtje['counter']; ?></td>
                  </tr>
                  <tr>
                    <th>Contact us</th>
                    <td><?php
$result = mysql_query("SELECT * FROM webapp_statistics WHERE page = 'contact'", $db4); 
$rijtje = mysql_fetch_assoc($result);
echo $rijtje['counter']; ?></td>
                  </tr>
                  <tr>
                    <th>Business document</th>
                    <td><?php
$result = mysql_query("SELECT * FROM webapp_statistics WHERE page = 'test_business_document'", $db4); 
$rijtje = mysql_fetch_assoc($result);
echo $rijtje['counter']; ?></td>
                  </tr>
                  <tr>
                    <th>Email</th>
                    <td><?php
$result = mysql_query("SELECT * FROM webapp_statistics WHERE page = 'test_email'", $db4); 
$rijtje = mysql_fetch_assoc($result);
echo $rijtje['counter']; ?></td>
                  </tr>
                  <tr>
                    <th>Web page</th>
                    <td><?php
$result = mysql_query("SELECT * FROM webapp_statistics WHERE page = 'test_web_page'", $db4); 
$rijtje = mysql_fetch_assoc($result);
echo $rijtje['counter']; ?></td>
                  </tr>
                  <tr>
                    <th>Button find training</th>
                    <td><?php
$result = mysql_query("SELECT * FROM webapp_statistics WHERE page = 'btn_find_training'",  $db4); 
$rijtje = mysql_fetch_assoc($result);
echo $rijtje['counter']; ?></td>
                  </tr>
                  <tr>
                    <th>Button learn now</th>
                    <td><?php
$result = mysql_query("SELECT * FROM webapp_statistics WHERE page = 'btn_learn_now'",  $db4); 
$rijtje = mysql_fetch_assoc($result);
echo $rijtje['counter']; ?></td>
                  </tr>
                </tbody>
              </table>
            </div>
          </div>
          <!-- /block --> 
        </div>
        <div class="span6"> 
          <!-- block -->
          <div class="block">
            <div class="navbar navbar-inner block-header">
              <div class="muted pull-left">Score information</div>
              <div class="pull-right"><span class="badge badge-info"></span> </div>
            </div>
            <div class="block-content collapse in">
              <?php
$result = mysql_query("SELECT * FROM webapp_results",  $db4); 
$rijtje = mysql_num_rows($result);
echo "Number of scores: <strong>" .$rijtje . "</strong>, of which:"; 
echo "<ul>";
$aantal_min = mysql_num_rows(mysql_query("SELECT * FROM webapp_results WHERE result LIKE '%-%'", $db4));
$min = round(($aantal_min / $rijtje)*100,1);
echo "<li><strong>$min %</strong> beat the system :)</li>";

$plus = 100 - $min;
echo "<li><strong>$plus %</strong> were quicker with Information Mapping</li>";
echo "</ul>";
?>
            </div>
          </div>
          <!-- /block --> 
        </div>
      </div>
      <div class="row-fluid">
        <div class="span12"> 
          <!-- block -->
          <div class="block">
            <div class="navbar navbar-inner block-header">
              <div class="muted pull-left">Where do visitors come from?</div>
              <div class="pull-right"> </div>
            </div>
            <div class="block-content collapse in">
              <table class="table table-striped">
                <tr>
                  <th>Internet address</th>
                  <th>Qty</th>
                </tr>
                <?php
$result = mysql_query("SELECT * FROM webapp_referer ORDER BY counter DESC",  $db4); 
while($rijtje = mysql_fetch_array($result))
{
	echo "<tr>";
if($rijtje['url'] =='') { echo "<td>not known</td>"; } else { echo "<td>" . $rijtje['url'] . "</td>";}
echo "<td>" . $rijtje['counter'] . "</td>";
echo "</tr>";
}
?>
              </table>
            </div>
          </div>
          <!-- /block --> 
        </div>
      </div>
      <?php
}
?>
    </div>
  </div>
  <hr>
  <?php
include("../footer.php");
?>