<?php
$rootdir="../../";
$page="Admin: add volume key";
include("../connect.php");
include("../header.php");
?>

<body>
<?php
include("../topnav.php");
?>
<div class="container-fluid">
  <div class="row-fluid">
    <?php
				include("../sidenav.php");
				?>
    <div class="span10" id="content">
      <div class="row-fluid">
        <div class="span12"> 
          
          <!-- block -->
          <div class="block">
            <div class="navbar navbar-inner block-header">
              <h3>Add volume key to web store</h3>
            </div>
            <div class="block-content collapse in">
              <?php 
if (!isset($_SESSION["username"])){
	echo $niet_ingelogd;
}
// hieronder staan de functies die toegelaten worden tot deze pagina
elseif($_SESSION["function"] != "admin"){ // make it accessible with function admin 
	echo $geen_toegang;
}else{
?>
              <p><strong>Warning: To add a volume key for FS Pro 4.x please pick an "unassigned" key from the license manager in Magento (update the activations in Nalpeiron!). For FS Pro 2013 you can generate a new license key on Activation Server.</strong></p>
              <p>Fill out the form below and click <strong>Save</strong>. <br />
                Warning: Changes cannot be undone.</p>
              <form action="#" method="post">
                License key:
                <input name="lickey" type="text" id="lickey" style="width: 300px;" />
                <br />
                Customer id:
                <input name="customerid" type="text" id="customerid" style="width: 300px;" />
                <br />
                Order nr:
                #<input name="orderid" type="text" id="orderid" style="width: 300px;" />
                (> 4 digits, increment_id) <br />
                Product:
                <!--<input name="productid" type="text" id="productid" style="width: 300px;" />-->
                <select name="productid" style="width: 300px;">
                <option value="484">FS Pro 4.3 volume license</option>
                <option value="479">Upgrade to FS Pro 4.3 volume license</option>
                <option value="401">FS Pro 2013 1 Year volume license</option>
                <option value="402">FS Pro 2013 2 Year volume license</option>
                <option value="403">FS Pro 2013 3 Year volume license</option>
                <option value="405">FS Pro 2013 Perpetual volume license</option>
                <option value="421">FS Pro 2013 Migrate to 1 year volume license</option>
                <option value="422">FS Pro 2013 Migrate to 2 Year volume license</option>
                <option value="423">FS Pro 2013 Migrate to 3 Year volume license</option>
                <option value="414">FS Pro 2013 Migrate to Perpetual volume license</option>
                </select>
                <br />
                Number of activations:
                <input name="activations" type="text" id="activations" style="width: 300px;" />
                <br />
                <!--Order date:
                <input name="date" type="text" id="date" style="width: 300px;" />
                (format 2014-12-30) <br />
                <br />-->
                <input type="submit" name="verzenden" value="Save" class="btn" />
              </form>
              <?php

// HIERONDER JUIST ZOEKSCRIPT VOOR MULTIPLE ZOEKWAARDEN

$lickey = isset($_POST['lickey']) ? $_POST['lickey'] : '';
$customerid = isset($_POST['customerid']) ? $_POST['customerid'] : '';
$orderid = isset($_POST['orderid']) ? $_POST['orderid'] : '';
$productid = isset($_POST['productid']) ? $_POST['productid'] : '';
$act = isset($_POST['activations']) ? $_POST['activations'] : '';
//$date = isset($_POST['date']) ? $_POST['date'] : '';
$verzenden = isset($_POST['verzenden']) ? $_POST['verzenden'] : '';

if($verzenden)
{
	if(empty($lickey))
     $error .= "<li>You don't have entered a license key.</li>\n";

	if(empty($customerid))
     $error .= "<li>You don't have entered a customer id.</li>\n";	 

	if(empty($orderid))
     $error .= "<li>You don't have entered an order id.</li>\n";	

	if(strlen($orderid) < 5)
     $error .= "<li>You don't have entered a valid increment_id. Example of valid input format: 1700002267</li>\n";

	if(empty($productid))
     $error .= "<li>You don't have entered a product id.</li>\n";	

	if(empty($act))
     $error .= "<li>You don't have entered numbered of activations.</li>\n";	

	//if(empty($date))
     //$error .= "<li>You don't have entered a date.</li>\n";	
	 
	// read license_id
	$res_vol = mysql_query("SELECT order_id, license_id 
	FROM license_orders WHERE order_id = '$orderid'");
	$row = mysql_fetch_assoc($res_vol);
	$licenseid = $row['license_id'];
	if(empty($licenseid))
	     $error .= "<li>The order id could not be found in license_orders.</li>\n";

	$ex = mysql_num_rows(mysql_query("SELECT license_id 
	FROM license_keys WHERE license_id = '$licenseid'"));
	if($ex > 0)
	     $error .= "<li>There is already a row in license_keys for that license id.</li>\n";
// read entity_id of order
$res_ent = mysql_query("SELECT entity_id, increment_id FROM sales_flat_order WHERE increment_id = '$orderid'");
	$row_o = mysql_fetch_assoc($res_ent);
	$orderid_4 = $row_o['entity_id'];


	if(isset($error))    
   {    
      // Afdrukken van de errors  
      echo "<p><strong>Your action was not successful:</strong></p>\n";   
      echo "<ul>"; 
      echo $error;    
      echo "</ul>\n"; 

    }
	else
	{
	
	  $result = mysql_query("UPDATE license_info SET last_license_key = '$lickey' WHERE license_id = '$licenseid'");
	  if($result) { echo "<p>Saved query 1 successfully (tbl license_info)</p>"; } else { echo "<p>Error: Query 1 not saved successfully (tbl license_info)</p>";  }

	  $result2 = mysql_query("INSERT INTO license_keys SET license_key = '$lickey', product_sku = '', number_of_activations = '$act', license_id = '$licenseid'");
	  if($result2) { echo "<p>Saved query 2 successfully (tbl license_keys)</p>"; } else { echo "<p>Error: Query 2 not saved successfully (tbl license_keys)</p>";  }

// fs pro 4 key already in DB, update record, else create new record
	$vier = mysql_num_rows(mysql_query("SELECT serial_number 
	FROM license_serialnumbers WHERE serial_number = '$lickey'"));
	
	// in license_serialnumbers, a value must be filled in for order_item_id to let the keys appear in report us_order_keys
	$res_item = mysql_query("SELECT * FROM sales_flat_order_item WHERE order_id = '$orderid_4' && product_id = '$productid'");
	$item_row = mysql_fetch_assoc($res_item);
	$order_item_id = $item_row['item_id'];
	
	if($vier < 1)
	{
	  // record bestaat nog niet, dus aanmaken
	  $result3 = mysql_query("INSERT INTO license_serialnumbers SET serial_number = '$lickey', status = 'ASSIGNED', customer_id = '$customerid', product_id = '$productid', order_id = '$orderid_4', order_item_id = '$order_item_id'");
	  if($result3) { echo "<p>Saved query 3 successfully (tbl license_serialnumbers)</p>"; } else { echo "<p>Error: Query 3 not saved successfully (tbl license_serialnumbers)</p>";  }
	  
	}
	else {
		// record bestaat al, dus updaten
		$result3 = mysql_query("UPDATE license_serialnumbers SET status = 'ASSIGNED', customer_id = '$customerid', product_id = '$productid', order_id = '$orderid_4', order_item_id = '$order_item_id' WHERE serial_number = '$lickey'");
	  if($result3) { echo "<p>Updated query 3 successfully (tbl license_serialnumbers)</p>"; } else { echo "<p>Error: Query 3 not updated successfully (tbl license_serialnumbers)</p>";  }
	}
	}
}
?>
              <?php
}
?>
            </div>
          </div>
          <!-- /block --> 
          
        </div>
      </div>
    </div>
  </div>
  <hr>
  <?php
include("../footer.php");
?>
</html>