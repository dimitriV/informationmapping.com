<?php
$rootdir="../../";
$page="Search license key";
include("../connect.php");
include("../header.php");
?>

<body>
<?php
include("../topnav.php");
?>
<div class="container-fluid">
<div class="row-fluid">
<?php
include("../sidenav.php");
?>
<div class="span10" id="content">
    <div class="row-fluid">
        <div class="span12">

            <!-- block -->
            <div class="block">
                <div class="navbar navbar-inner block-header">
                    <h3>Search license key</h3>
                </div>
                <div class="block-content collapse in">
                    <?php
                    if (!isset($_SESSION["username"])){
                        echo $niet_ingelogd;
                    }
                    elseif($_SESSION["function"] != "admin" && $_SESSION["function"] != "it"){ // make it accessible with function admin and it
                        echo $geen_toegang;
                    }else{
                        ?>
                        <p>Enter your license key to discover who is using it:</p>
                        <form name="Form1" action="#" method="post">
                            License key:
                            <input type="text" name="key" class="input-large" /> (enter key in this format: 268000-037320-176690)
                            <br />
                            <strong>Note:</strong> Database FS Pro 2013 is updated every week. Older FS Pro versions once a month.<br />
                            <input type="submit" name="verzenden" value="Search" class="btn" />
                        </form>
                        <table class="table table-striped">
                            <tr>
                                <th>Order</th>
                                <th>Order Date</th>
                                <th>Key</th>
                                <th width="200">E-mail</th>
                                <th>Name</th>
                                <th>Activations</th>
                                <th>Product type</th>
                                <th>License type</th>
                            </tr>
                            <?php


                            $key = isset($_POST['key']) ? trim($_POST['key']) : '';
                            $key_raw = isset($_POST['key']) ? trim($_POST['key']) : '';
                            // remove koppeltekens for FS Pro 4 keys
                            if(strlen($key) == 20) { $key = str_replace('-','', $key); }
                            $email = isset($_POST['email']) ? $_POST['email'] : '';
                            $sort = isset($_GET['sort']) ? $_GET['sort'] : '';



                            // search in 2 tables
                            // number of columns must be the same in both columns
                            // because of first name and last name are 3 columns, we have to join them with concat to one column

                            if(!empty($key))
                            {

                                //This part is for old license keys from FS PRO 4.x
                                $sql = "SELECT n.LicenseCode, n.EmailAddress,
            concat(n.firstname,' ' , n.lastname) AS fullname FROM reporting_nalpeiron AS n WHERE n.LicenseCode LIKE '%$key%'
        UNION
        SELECT COL2, COL1, COL3 FROM as_customerinfo AS a WHERE a.COL2 LIKE '%$key%'
        ";

                                $result = mysql_query($sql);
                                $aantal = mysql_num_rows($result);


                                if($aantal > 0)
                                {
                                    while($row = mysql_fetch_array($result))
                                    {

                                        echo "<tr>";
                                        echo "<td>" . $row['LicenseCode'] . "</td>";
                                        echo "<td>" . $row['EmailAddress'] . "</td>";
                                        echo "<td>" . $row['fullname'] . "</td>";

                                        // read out activations
                                        $result_act = mysql_query("SELECT * FROM as_tries WHERE entrykey = '$key'");
                                        $row_act = mysql_fetch_assoc($result_act);
                                        echo "<td>" . $row_act['tries'] . " / " . $row_act['maxtries'] . "</td>";

                                        // wegens guest aankopen kan customer_id niet gehaald worden uit tbl license_serialnumbers
                                        $result = mysql_query("SELECT l.serial_number, l.product_id, l.order_id, s.entity_id, s.increment_id, s.customer_email, s.created_at, a.parent_id, a.email, a.firstname, a.lastname
                FROM license_serialnumbers AS l, sales_flat_order AS s, sales_flat_order_address AS a
                WHERE l.serial_number = '$key_raw' && l.order_id = s.entity_id && s.entity_id = a.parent_id
                ");
                                        $row = mysql_fetch_assoc($result);
                                        echo "<td># " . $row['increment_id'] . "</td>";
                                        echo "<td>&nbsp;</td>";
                                        echo "<td>&nbsp;</td>";
                                        echo "</tr>";
                                    }
                                }
                                //This part is for New license keys from FS PRO 2013
                                else
                                {

                                    $query2 = "SELECT l.serial_number, l.product_id, l.order_id, s.entity_id,s.status, s.increment_id,s.imi_customer_firstname, s.customer_email, s.created_at, k.product_sku, k.number_of_activations, i.license_type
        FROM license_serialnumbers AS l, sales_flat_order AS s, license_keys AS k, imishop.license_info AS i
        WHERE l.serial_number = '$key_raw' && l.order_id = s.entity_id && l.serial_number =k.license_key && l.serial_number =i.last_license_key";


                                    //echo $query2;
                                    $result2 = mysql_query($query2);
                                    $row2 = mysql_fetch_array($result2);

                                    echo "<tr><td># " . $row2['increment_id'] . "</td><td> " . $row2['created_at'] . "</td><td>$key_raw</td><td>" . $row2['customer_email'] . "</td><td>" . $row2['imi_customer_firstname'] . "</td><td>" . $row2['number_of_activations'] . "</td><td>" . $row2['product_sku'] . "</td><td>" . $row2['license_type'] . "</td></tr>";

                                }
                            }

                            ?>
                        </table>
                    <?php
                    }
                    ?>
                </div>
                <div class="block">
                    <div class="navbar navbar-inner block-header">
                        <h3>Replace license key</h3>
                    </div>
                    <div class="block-content collapse in"> //Replace license key

                        <p><strong>Warning: Do not replace keys in case of a volume key! This form is meant for keys which are replaced due to the tampered with issue.</strong></p>
                        <p>Fill out the form below and click <strong>Save</strong> to assign the new key to the existing order. <br />Warning: Changes cannot be undone.</p>

                        <form name="Form2" action="#" method="post">
                            Old license key:
                            <input name="oldlickey" type="text" id="oldlickey" style="width: 300px;" />
                            <br />
                            New license key:
                            <input name="newlickey" type="text" id="newlickey" style="width: 300px;" />
                            <br />
                            <br />
                            <input type="submit" name="verzenden" value="Save" class="btn" />
                        </form>


                        <?php


                        $oldlickey = isset($_POST['oldlickey']) ? $_POST['oldlickey'] : '';
                        $newlickey = isset($_POST['newlickey']) ? $_POST['newlickey'] : '';
                        $verzenden = isset($_POST['verzenden']) ? $_POST['verzenden'] : '';
                        //$error = "";

                        if($verzenden && empty($key))
                        {
                            if(empty($oldlickey))
                                $error .= "<li>You don't have entered an old license key.</li>\n";

                            if(empty($newlickey))
                                $error .= "<li>You don't have entered a new license key.</li>\n";

                            // read if entered key is a volume key
                            $res_vol = mysql_num_rows(mysql_query("SELECT license_id, license_type, last_license_key FROM license_info WHERE last_license_key = '$oldlickey' && license_type = 'volume'"));
                            if($res_vol > 0)
                                $error .= "<li>You cannot change a volume key.</li>\n";

                            // read if key exists in shop
                            $res_vol = mysql_num_rows(mysql_query("SELECT last_license_key FROM license_info WHERE last_license_key = '$oldlickey'"));
                            if($res_vol < 1)
                                $error .= "<li>This key does not exist in the shop database.</li>\n";

                            if(isset($error))
                            {
                                // Afdrukken van de errors
                                echo "<p><strong>Your action was not successful:</strong></p>\n";
                                echo "<ul>";
                                echo $error;
                                echo "</ul>\n";

                            }
                            else
                            {

                                $result = mysql_query("UPDATE license_info SET last_license_key = '$newlickey' WHERE last_license_key = '$oldlickey'");
                                if($result) { echo "<p>Saved query 1 successfully (tbl license_info)</p>"; } else { echo "<p>Error: Query 1 not saved successfully (tbl license_info)</p>";  }

                                $result2 = mysql_query("UPDATE license_keys SET license_key = '$newlickey' WHERE license_key = '$oldlickey'");
                                if($result2) { echo "<p>Saved query 2 successfully (tbl license_keys)</p>"; } else { echo "<p>Error: Query 2 not saved successfully (tbl license_keys)</p>";  }

                                $result3 = mysql_query("UPDATE license_serialnumbers SET serial_number = '$newlickey' WHERE serial_number = '$oldlickey'");
                                if($result3) { echo "<p>Saved query 3 successfully (tbl license_serialnumbers)</p>"; } else { echo "<p>Error: Query 3 not saved successfully (tbl license_serialnumbers)</p>";  }
                            }

                        }
                        ?>
                    </div>
                </div>
                <!-- /block -->

            </div>
        </div>
    </div>
</div>
<hr>
<?php
include("../footer.php");
?>
</html>