        <?php
        $rootdir="../../";
        $page="Extend Enrollments";
        include("../connect.php");
        include("../connect_db234.php");
        include("../header.php");
        ?>
        <body>
        <?php
        include("../topnav.php");
        ?>
        <div class="container-fluid">
            <div class="row-fluid">
                <?php
                include("../sidenav.php");
                ?>
                <div class="span10" id="content">
                    <div class="row-fluid">
                        <div class="span12">
                            <div class="block">
                                <div class="navbar navbar-inner block-header">
                                    <h3>Extend Enrollments</h3>
                                </div>
                                <div class="block-content collapse in">
                            <?php
                                if (!isset($_SESSION["username"])){
                                        echo $niet_ingelogd;
                                }
                                    elseif($_SESSION["function"] != "admin" && $_SESSION["function"] != "it"){ // make it accessible with function admin and it
                                        echo $geen_toegang;
                                }else{
                                        ?>
                                        <form role="form" name="selectCourses" class="form-horizontal" action="<?php echo $_SERVER['PHP_SELF']; ?>" method="post">
                                            <div class="control-group">
                                                <label class="control-label" for="exampleInputEmail1">Enter your email:</label>
                                                <input type="text" class="input-large " name="searchstring" placeholder="Enter email" >
                                            </div>
                                            <div class="control-group">
                                                <label class="control-label" for="zoeken"></label>
                                                <input type="submit" name="zoeken" value="Search" class="btn" />
                                            </div>
                                            <div class="row-fluid">
                                                <table class="table table-striped">
                                                    <tr>
                                                        <th>Name</th>
                                                        <th>E-mail</th>
                                                        <th>Course</th>
                                                        <th>Expiry date</th>
                                                        <th>Select</th>
                                                    </tr>
                                                         <?php
                                                        $searchstring = isset($_POST['searchstring']) ? $_POST['searchstring'] : '';

                                                        if(!empty($searchstring)){

                                                                $user_moodle = mysql_query("SELECT DISTINCT u.id,u.email, concat(u.firstname,' ',u.lastname) as Fullname,
                                                                                c.fullname as CourseName,c.id as CourseID, ue.enrolid as EnrollmentID,con.id as contextid,con.depth,
                                                                                /* ro.name as role, */ FROM_UNIXTIME(ue.timecreated, '%d %M %Y') as Start,FROM_UNIXTIME(ue.timeend,'%d %M %Y') as ExperationDate
                                                                                FROM academy.mdl_user u
                                                                                INNER JOIN academy.mdl_user_enrolments ue on u.id=ue.userid
                                                                                INNER JOIN academy.mdl_enrol en on en.id=ue.enrolid
                                                                                INNER JOIN academy.mdl_course c on en.courseid=c.id
                                                                                INNER JOIN academy.mdl_context con on con.instanceid=c.id
                                                                                /*
                                                                                INNER JOIN academy.mdl_role_assignments r on u.id=r.userid
                                                                                INNER JOIN academy.mdl_role ro on ro.id=r.roleid*/
                                                                                WHERE u.email like '%$searchstring%' and con.depth='3'", $db2);
                                                                $aantal_moodle = mysql_num_rows($user_moodle);
                                                                while($row_user_moodle = mysql_fetch_array($user_moodle))
                                                                {
                                                                    echo "<tr>";
                                                                    echo "<td>" . $row_user_moodle['Fullname'] . "</td>";
                                                                    echo "<td>" . $row_user_moodle['email'] . "</td>";
                                                                    echo "<td>" . $row_user_moodle['CourseName'] . "</td>";
                                                                    echo "<td>" . $row_user_moodle['ExperationDate'] . "</td>";
                                                              ?>
                                                                    <td><input type="checkbox" name="courseCheck[]" value="<?php echo $row_user_moodle['id'].' '.$row_user_moodle['EnrollmentID'].' '.$row_user_moodle['contextid'] ?>" /></td>
                                                                    <?php
                                                                    echo "</tr>";
                                                                    $roles= mysql_query("SELECT * FROM academy.mdl_role_assignments where userid=".$row_user_moodle['id']." and contextid=".$row_user_moodle['contextid'],$db2);
                                                                    $aantal_roles = mysql_num_rows($roles);
                                                                    if($aantal_roles == 0){
                                                                        echo "<p>The user is missing Student Role status for course: ".$row_user_moodle['CourseName']." </p>";
                                                                    }
                                                                    mysql_free_result($roles);
                                                                }
                                                            mysql_free_result($user_moodle);

                                                            }
                                                        ?>
                                                </table>
                                            </div>
                                          <div class="row-fluid">
                                            <div class="form-group" id="sandbox-container">
                                                <label for="inputExtend" class="col-sm-4 control-label">Extend selected courses:</label>
                                                <div class="col-sm-8 input-append date">
                                                    <input type="text" name="extend" class="span10"><span class="add-on"><i class="icon-th"></i></span>
                                                </div>
                                                <p class="bg-info small"><strong>(If student role is missing it will be added automatically)</strong></p>
                                            </div>
                                            <div class="form-group">
                                               <label class="control-label" for="update"></label>
                                                <div class="col-md-5">
                                                    <input type="submit" name="update" value="Apply" class="btn" />
                                                </div>
                                            </div>
                                          </div>
                                            <!-- Load jQuery and bootstrap datepicker scripts -->
                                            <script src="../../admin/bootstrap/js/jquery-1.9.1.min.js"></script>
                                            <script src="../../admin/bootstrap/js/bootstrap.min.js"></script>
                                            <script src="../../admin/bootstrap/js/bootstrap-datepicker.js"></script>
                                            <script>$('#sandbox-container .input-append.date').datepicker({
                                                    autoclose: true,
                                                    todayHighlight: true,
                                                    format: "dd-mm-yyyy"
                                                });</script>

                                        <?php if (!empty($_POST['update'])) {
                                             foreach($_POST['courseCheck'] as $checkbox=>$id){
                                                 $newDate = $_POST['extend'];
                                                 $timestamp = strtotime($newDate);
                                                 $nDate= explode(" ", $id);
                                                 $result = mysql_query("UPDATE academy.mdl_user_enrolments SET timeend='$timestamp' WHERE userid=$nDate[0] and enrolid=$nDate[1];", $db2);
                                                 if($result) { echo "<p>Update query 1 successfully </p>"; } else { echo "<p>Error: Query 1 not saved successfully</p>";  }
                                                 mysql_free_result($result);

                                                 $roles= mysql_query("SELECT * FROM academy.mdl_role_assignments where userid=$nDate[0] and contextid=$nDate[2];",$db2);
                                                 $aantal_roles = mysql_num_rows($roles);
                                                 if($aantal_roles==0){
                                                 $role = mysql_query("INSERT INTO academy.mdl_role_assignments (roleid,contextid,userid,timemodified,modifierid)
                                                                      VALUES (5,$nDate[2],$nDate[0],UNIX_TIMESTAMP(now()),2);", $db2);
                                                    if($role) { echo "<p>Student Role Successfully added</p>"; } else { echo "<p>Error: Student Role not saved successfully</p>";  }
                                                     mysql_free_result($role);
                                                }

                                             }
                                           }
                                        }?>
                                        </form>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>

        <hr>
        <?php include("../footer.php");?>
            </html>