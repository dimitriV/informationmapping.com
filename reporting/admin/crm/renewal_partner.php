<?php
$rootdir="../../";
$page="Renewal update";
include("../connect.php");
include("../header.php");
?>

<body>
<?php
include("../topnav.php");
?>
<div class="container-fluid">
  <div class="row-fluid">
    <?php
				include("../sidenav.php");
				?>
    <div class="span10" id="content">
      <div class="row-fluid">
        <div class="span12"> 
          
          <!-- block -->
          <div class="block">
            <div class="navbar navbar-inner block-header">
              <h3>FS Pro 2013 renewals purchased by partner</h3>
            </div>
            <div class="block-content collapse in">
              <?php 
if (!isset($_SESSION["username"])){
	echo $niet_ingelogd;
}
// hieronder staan de functies die toegelaten worden tot deze pagina
elseif($_SESSION["function"] != "admin"){ // make it accessible with function admin 
	echo $geen_toegang;
}else{
?>
<form method="post" action="#">
<select name="partner">
<option value="">Select a country...</option>
<option value="AU">Australia</option>
<option value="BE">Belgium</option>
<option value="CA">Canada</option>
<option value="FR">France</option>
<option value="IN">India</option>
<option value="ID">Indonesia</option>
<option value="IT">Italy</option>
<option value="JP">Japan</option>
<option value="MY">Malaysa</option>
<option value="MX">Mexicoooooooh - het land van al mn dromen</option>
<option value="NZ">New Zealand</option>
<option value="NL">The Netherlands</option>
<option value="TR">Turkey</option>
<option value="US">United States</option>
</select>
<input type="submit" name="verzenden" value="OK" class="btn" />
</form>

<table class="table table-striped">
<tr><td>First name</td><td>Last name</td><td>Email</td><td>Company</td><td>Telephone</td><td>Product</td><td>Date</td></tr>
<?php
// read out FS Pro renewal orders per partner
// if customer has no partner filled in, do not give date or check first with Francis if royalties have been given

$verzenden = isset($_POST['verzenden']);
$partner = isset($_POST['partner']) ? $_POST['partner'] : '';
if($verzenden)
{
$result = mysql_query("SELECT o.status, o.entity_id, o.created_at, a.parent_id, a.country_id, a.firstname, a.lastname, a.email, a.company, a.telephone, i.order_id, i.product_id 
FROM
sales_flat_order o

inner join sales_flat_order_item i ON i.order_id = o.entity_id 
inner join sales_flat_order_address a ON o.entity_id = a.parent_id 
WHERE 
o.status = 'complete' 
&& a.country_id = '$partner'
&& (i.product_id = '508' OR i.product_id ='509' OR i.product_id = '510' OR i.product_id = '511')", $db);

while($row = mysql_fetch_array($result))
{
	echo "<tr>";
	echo "<td>" . $row['firstname'] . "</td>";
	echo "<td>" . $row['lastname'] . "</td>";
	echo "<td>" . $row['email'] . "</td>";
	echo "<td>" . $row['company'] . "</td>";
	echo "<td>" . $row['telephone'] . "</td>";
	$product = $row['product_id'];
	if($product == "508") { $product = "Perpetual"; }
	if($product == "509") { $product = "1 Year"; }
	if($product == "510") { $product = "2 Year"; }
	if($product == "511") { $product = "3 Year"; }
	echo "<td>" . $product . "</td>";
	echo "<td>" . $row['created_at'] . "</td>";
	echo "</tr>";
}

}
?>
</table>
              <?php
}
?>
            </div>
          </div>
          <!-- /block --> 
          
        </div>
      </div>
    </div>
  </div>
  <hr>
  <?php
include("../footer.php");
?>
</html>