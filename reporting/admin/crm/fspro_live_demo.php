<?php
$rootdir="../../";
$page="FS Pro live demo";
include("../connect.php");
include("../header.php");
?>

<body>
<?php
include("../topnav.php");
?>
<div class="container-fluid">
            <div class="row-fluid">
                
                <?php
				include("../sidenav.php");
				?>
                
                <div class="span10" id="content">
                    <div class="row-fluid">
                    	<div class="span12">
                        
                            <!-- block -->
                            <div class="block">
                                <div class="navbar navbar-inner block-header">
                                <h3>FS Pro live demo</h3>     
                                </div>
                                <div class="block-content collapse in">
<?php 
if (!isset($_SESSION["username"])){
	echo $niet_ingelogd;
}
// hieronder staan de functies die toegelaten worden tot deze pagina
elseif($_SESSION["function"] != "admin" && $_SESSION["function"] != "marketing"){ // make it accessible with function admin and it
	echo $geen_toegang;
}else{
?>

<?php
$action = isset($_GET['action']) ? htmlentities($_GET['action']) : '';
$id = isset($_GET['id']) ? htmlentities($_GET['id']) : '';

if(!empty($action) && !empty($id))
{
mysql_query("UPDATE reporting_live_demo SET deleted = '1' WHERE id = '$id'");	
echo "<div class=\"alert alert-success\">Prospect deleted successfully.</div>";
}

// check if people bought via the coupon and set the status to "won" automatically
$s_update = mysql_query("SELECT increment_id, coupon_code, status FROM sales_flat_order WHERE coupon_code LIKE '%VAL%' && status = 'complete'");
while($s_update = mysql_fetch_array($s_update))
{
	$coupon = $s_update['coupon_code'];
	//echo $coupon;
	// update the record
	$update_prospect = mysql_query("UPDATE reporting_live_demo SET status = 'Won' WHERE code = '$coupon'");
}

// show query
$result = mysql_query("SELECT id, firstname, lastname, email, phone, country, status,code, deleted FROM reporting_live_demo WHERE deleted = '0' ORDER BY id DESC");
$aantal = mysql_num_rows($result); 

$result_c = mysql_query("SELECT rule_id, times_used FROM salesrule_customer WHERE rule_id = '52' && times_used > '0'"); // checken of dit wel goed werkt --> neen want orders met coupon code die nadien gecancelled werden worden ook mee berekend
$aantal_c = mysql_num_rows($result_c); 

$result_a = mysql_query("SELECT id, status FROM reporting_live_demo WHERE status = 'Appointment'");
$aantal_a = mysql_num_rows($result_a); 

$result_d = mysql_query("SELECT id, status FROM reporting_live_demo WHERE status = 'Demo'");
$aantal_d = mysql_num_rows($result_d); 

$result_val = mysql_query("SELECT status, coupon_code FROM sales_flat_order WHERE status = 'complete' && coupon_code LIKE '%VAL%'");
$aantal_val = mysql_num_rows($result_val); 

$result_cart = mysql_query("SELECT reserved_order_id, coupon_code FROM sales_flat_quote WHERE reserved_order_id IS NULL && applied_rule_ids = '52' && coupon_code LIKE '%VAL%'");
$aantal_cart = mysql_num_rows($result_cart); 
?>
<style type="text/css">
.hidden { display: none; }
.unhidden { display: block; }
</style>

<script type="text/javascript">
  function unhide(divID) {
    var item = document.getElementById(divID);
    if (item) {
      item.className=(item.className=='hidden')?'unhidden':'hidden';
    }
  }
</script>

<p><strong>Prospects:</strong> <?php echo $aantal; ?> total, <?php echo $aantal_a; ?> appointments, <?php echo $aantal_d; ?> demo (<a href="stats_livedemo.php">view page statistics</a>)<br />
<strong>Coupon codes:</strong> <a href="javascript:unhide('learnHTML');"><?php echo $aantal_cart; ?> pending</a> in shopping cart, <?php echo $aantal_val; ?> ordered
</p>
<div id="learnHTML" class="hidden" style="background-color: #FCF; padding: 5px;">
<p>Shopping cart information:</p>
<?php
$show_cart = mysql_query("SELECT remote_ip, coupon_code, customer_email, reserved_order_id, applied_rule_ids FROM sales_flat_quote WHERE reserved_order_id IS NULL && applied_rule_ids = '52' && coupon_code LIKE '%VAL%'");
while($row_cart = mysql_fetch_array($show_cart))
{
	
	echo "IP: ". $row_cart['remote_ip']; 
	echo " - ";
	echo $row_cart['coupon_code'];
	echo " - ";
	echo $row_cart['customer_email']; 
	echo "<br />";
}
?>

</div>

<table class="table table-striped">
<tr><th>First name</th><th>Last name</th><th>E-mail</th><th>Phone</th><th>Country</th><th>Status</th><th>Code</th><th>Edit</th></tr>
  <?php

$totaal_qty_1y=0;
while($report=mysql_fetch_array($result))
{
	

	// check which prospects are not won or lost within 30 days and then display row in red
	$result_exp = mysql_query("SELECT * FROM reporting_live_demo WHERE (status != 'Won' OR status != 'Lost') 
	&& date_insert < DATE_ADD(CURDATE(), INTERVAL -30 DAY) && id= '" . $report['id'] . "'" );
	$row_exp = mysql_num_rows($result_exp);
	if($row_exp == "1") { echo "<tr style=\"color: red;\">"; } else {
	echo "<tr>"; }
	echo "<td>" . $report['firstname'] . "</td>";
	echo "<td>" . $report['lastname'] . "</td>"; 
	echo "<td><a href=\"fspro_live_demo_edit.php?action=edit&id=" . $report['id'] . "\">" . $report['email'] . "</a>";
	$query_google = mysql_query("SELECT * FROM reporting_live_demo AS d, reporting_live_demo_results AS r WHERE d.date_insert = r.date && d.ip = r.ip && r.referer LIKE '%google%' && d.email = '" . $report['email'] . "'");
	if(mysql_num_rows($query_google) > 0)
	{ echo " <img src=\"../images/google.png\" alt=\"Via Google\" />"; }
	echo "</td>";
	echo "<td>" . $report['phone'] . "</td>"; 
	echo "<td>" . $report['country'] . "</td>"; 
	echo "<td>" . $report['status'] . "</td>";
	echo "<td>" . $report['code'] . "</td>";
	echo "<td><a href=\"fspro_live_demo_edit.php?action=edit&id=" . $report['id'] . "\"><img src=\"../../images/edit.png\" alt=\"Edit prospect\" /></a>&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;
	<a href=\"fspro_live_demo.php?action=del&id=" . $report['id'] . "\"><img src=\"../../images/delete.jpg\" alt=\"Delete prospect\" /></a>
	</td>";
	echo "</tr>";
	
}
?>
</table>
</table>
<?php
}
?>

                                </div>
                            </div>
                            <!-- /block -->


</div>
</div>
</div>
</div>
            <hr>
<?php
include("../footer.php");
?>
</html>