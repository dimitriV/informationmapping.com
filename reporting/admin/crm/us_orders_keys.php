<?php
$rootdir="../../";
$page="Web Store Orders US";
include("../connect.php");
include("../header.php");
ini_set('max_execution_time', 300); // highers up the default 30 seconds script executing time to 5 minutes, works on localhost, not tested on hosting environment
?>

<body>
<?php
include("../topnav.php");
?>
<div class="container-fluid">
            <div class="row-fluid">
                
                <?php
				include("../sidenav.php");
				?>
                
                <div class="span10" id="content">
                    <div class="row-fluid">
                    	<div class="span12">
                        
                            <!-- block -->
                            <div class="block">
                                <div class="navbar navbar-inner block-header">
                                <h3>Web Store Orders US with license keys</h3>     
                                </div>
                                <div class="block-content collapse in">
<?php 
//echo $_SESSION['username'] . $_SESSION['function'];
if (!isset($_SESSION["username"])){
	echo $niet_ingelogd;
}

// hieronder staan de functies die toegelaten worden tot deze pagina
elseif($_SESSION["function"] != "admin" && $_SESSION["function"] != "it"){ // make it accessible with function admin and it
	echo $geen_toegang;
}else{

?>
<div class="ui-widget">

<form method="post" action="#">
Order number: <input type="text" name="ordernumber" />
<!--Last name: <input type="text" name="name" />
Email address: <input type="text" name="email" />
Company: <input type="text" name="company" />
Product: <input type="text" name="product" />-->
License key: <input type="text" name="key" />
Year: <select name="year">
<option value="">Choose a year</option>
<option value="2009">2009</option>
<option value="2010">2010</option>
<option value="2011">2011</option>
<option value="2012">2012</option>
<option value="2013">2013</option>
<option value="2014">2014</option>
</select>
<input type="submit" value="Search" name="zoeken" class="btn" />
</form>

<?php

function replace_product($code)
{
	if($code == "17") {$storeview = str_replace("17", "USD Store view", $code); }
	if($code == "5") { $storeview = str_replace("5", "US Store view", $code); }
	if($code == "1") { $storeview = str_replace("1", "Default Store view", $code); }
	if($code == "16") { $storeview = str_replace("16", "EUR Store view", $code); }
	$res_product = mysql_query("SELECT attribute_id, entity_id, value FROM catalog_product_entity_varchar WHERE attribute_id = '56' && entity_id = '$code'");
	$row_product = mysql_fetch_assoc($res_product);
	$product = $row_product['value'];
	return $product;
}

$zoeken = isset($_POST['zoeken']) ? $_POST['zoeken'] : '';
$key = isset($_POST['key']) ? trim($_POST['key']) : '';
$year = isset($_POST['year']) ? $_POST['year'] : '';
$ordernumber = isset($_POST['ordernumber']) ? trim($_POST['ordernumber']) : '';

$where = array();
if(!empty($key)) $where[] = " l.serial_number = '" . $key . "'";
if(!empty($year)) $where[] = " DATE_FORMAT(s.created_at, '%Y') = '" . $year . "'";
if(!empty($ordernumber)) $where[] = " o.increment_id = '" . $ordernumber . "'";

$where = implode(' AND ', $where);

if(empty($where))
{
// show only US orders
// read store_id from sales_flat_order and not from sales_flat_order_item!
$currentyear = date("Y");
$result = mysql_query("SELECT l.status, l.serial_number, l.customer_id, l.product_id, l.order_item_id, l.order_id, 
s.item_id, s.order_id, o.store_id, o.status, o.increment_id, DATE_FORMAT(s.created_at, '%d/%m/%Y') AS newDate, DATE_FORMAT(s.created_at, '%Y') AS jaar, s.name, s.qty_ordered,
o.entity_id, o.imi_customer_email
FROM license_serialnumbers AS l, sales_flat_order_item AS s, sales_flat_order AS o
WHERE 
DATE_FORMAT(s.created_at, '%Y') = '$currentyear' 
&& l.order_item_id = s.item_id 
&& l.order_id = o.entity_id 
&& l.status = 'ASSIGNED' 
&& o.store_id = '17' 
&& o.status = 'complete'
OR (DATE_FORMAT(s.created_at, '%Y') = '$currentyear' && l.order_item_id = s.item_id && l.order_id = o.entity_id && l.status = 'ASSIGNED' && o.store_id = '5' && o.status = 'complete') ORDER BY l.order_item_id ASC");	
	
}
else
{
	$result = mysql_query("SELECT l.status, l.serial_number, l.customer_id, l.product_id, l.order_item_id, l.order_id, 
s.item_id, s.order_id, o.store_id, o.status, o.increment_id, DATE_FORMAT(s.created_at, '%d/%m/%Y') AS newDate, DATE_FORMAT(s.created_at, '%Y') AS jaar, s.name, s.qty_ordered,
o.entity_id, o.imi_customer_email
FROM license_serialnumbers AS l, sales_flat_order_item AS s, sales_flat_order AS o
WHERE 
$where &&
l.order_item_id = s.item_id && l.order_id = o.entity_id && l.status = 'ASSIGNED' && o.store_id = '17' && o.status = 'complete'
OR ($where && l.order_item_id = s.item_id && l.order_id = o.entity_id && l.status = 'ASSIGNED' && o.store_id = '5' && o.status = 'complete') ORDER BY l.order_item_id ASC");

}

?>
<table>
  <tr>
    <th>License key</th>
    <th>Email</th>
    <th>Buyer name</th>
    <th>Buyer company</th>
    <th>End customer</th>
    <th>Product</th>
    <th>Qty</th>
    <th>Order number</th>
    <th>Purchase date</th>
  </tr>
  <?php
	
	while($report=mysql_fetch_array($result)){

  echo "<tr>";
    echo "<td>" . $report['serial_number'] . "</td>";
	// uitlezen van customer data
	if(empty($report['customer_id'])) // indien DB value == NULL, dan guest aankoop
	{
			//echo "<td>guest</td>";
			$res_guestname = mysql_query("SELECT parent_id, lastname, firstname, street, postcode, city, country_id, telephone, company, email FROM sales_flat_order_address WHERE parent_id = '" . $report['order_id'] . "'");
		$row_guestname=mysql_fetch_array($res_guestname);
		echo "<td>" . $row_guestname['email'] . "</td>";
		echo "<td>" . $row_guestname['firstname'] . " " . $row_guestname['lastname'] . "</td>";
		echo "<td>" . $row_guestname['company']  . "</td>"; // billing company
	}
	else
	{
	$result_customer = mysql_query("SELECT entity_id, email FROM customer_entity WHERE entity_id = '" . $report['customer_id'] . "'");
	$row_customer=mysql_fetch_array($result_customer);
		echo "<td>" . $row_customer['email']  . "</td>";
		$result_customer_address = mysql_query("SELECT * FROM sales_flat_order_address WHERE customer_id = '" . $report['customer_id'] . "'");
	$row_customer_address=mysql_fetch_array($result_customer_address);
	// billing name
	echo "<td>" . $row_customer_address['firstname'] . " " . $row_customer_address['lastname'] . "</td>";
	echo "<td>" . $row_customer_address['company']  . "</td>"; // billing company

	}

	echo "<td>" . $report['imi_customer_email'] . "</td>";
	
	echo "<td>" . replace_product($report['product_id']) . "</td>";
	
	echo "<td>" . round($report['qty_ordered'],0) . "</td>";
	// ophalen van order number
	$result_order = mysql_query("SELECT * FROM sales_flat_order WHERE entity_id = '" . $report['order_id'] . "'");
	$row_order = mysql_fetch_array($result_order);
	echo "<td>" . $row_order['increment_id'] . "</td>";
	echo "<td>" . $report['newDate'] . "</td>"; // created_at
	



  echo "</tr>\n";
} 
?>
</table>
</div>
<?php
}
?>
                                </div>
                            </div>
                            <!-- /block -->


</div>
</div>
</div>
</div>
            <hr>
<?php
include("../footer.php");
?>
</html>