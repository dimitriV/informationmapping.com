<?php
$rootdir="../../";
$page="Admin: create student keys";
include("../connect.php");
include("../connect_db234.php");
include("../header.php");
?>
<body>
<?php
include("../topnav.php");
?>
<div class="container-fluid">
            <div class="row-fluid">
                
                <?php
				include("../sidenav.php");
				?>
                
                <div class="span10" id="content">
                    <div class="row-fluid">
                    	<div class="span12">
                        
                            <!-- block -->
                            <div class="block">
                                <div class="navbar navbar-inner block-header">
                                <h3>Create student keys</h3>     
                                </div>
                                <div class="block-content collapse in">
<?php 
if (!isset($_SESSION["username"])){
	echo $niet_ingelogd;
}
// hieronder staan de functies die toegelaten worden tot deze pagina
elseif($_SESSION["function"] != "admin"){
	echo $geen_toegang;
}else{
?>


    <p>Total number in DB: <? $keys = mysql_query("SELECT * FROM reporting_trainingkey,reporting_courses,reporting_partners WHERE reporting_trainingkey.partner_id=reporting_partners.partner_id AND reporting_trainingkey.course_id=reporting_courses.course_id", $db);
$aantal_keys = mysql_num_rows($keys); echo $aantal_keys; ?> <a href="studentkey_list.php">(see overview)</a></p>

<?php
// zet gevaarlijke tekens in invoervelden om
function cleanup($string="")
{
$string = trim($string); // verwijdert spaties aan begin en einde
//$string = htmlentities($string); // converteert < > & " en andere speciale tekens naar veilige tekens
$string = htmlspecialchars($string);
$string = nl2br($string); // converteert linebreaks in textareas naar <br />
// plaatst escapetekens bij quotes indien nodig
if(!get_magic_quotes_gpc())
  {
  $string = addslashes($string);
  }

return $string;
}

$verzenden = isset($_POST['verzenden']) ? cleanup($_POST['verzenden']) : '';
if($verzenden)
{
	
	$number_keys = isset($_POST['number_keys']) ? cleanup($_POST['number_keys']) : '';
	if($number_keys > 1000)
	{ echo "Error: You entered an amount bigger than 1000. You can only generate 1000 student keys at one time. Please try again."; exit();}
	$choice_partner = isset($_POST['choice_partner']) ? cleanup($_POST['choice_partner']) : '';
	
	$choice_course = isset($_POST['choice_course']) ? cleanup($_POST['choice_course']) : '';
	
	// read out partner prefix
	$prefix_result = mysql_query("SELECT partner_id, partner_trainingkey FROM reporting_partners WHERE partner_id = '$choice_partner'", $db);
	$row_prefix = mysql_fetch_assoc($prefix_result);
	$prefix = $row_prefix['partner_trainingkey'];
	
	// read out course id
	$course_result = mysql_query("SELECT course_id, course_trainingkey FROM reporting_courses WHERE course_trainingkey = '$choice_course'", $db);
	$row_course = mysql_fetch_assoc($course_result);
	$course_id = $row_course['course_id'];
// uniek transactienummer creëren
function randomize()
{
	
	global $choice_partner; // bring this variable in for use in the function
	$nr = ""; // toegevoegd om notices te vermijden
	// transactienummer genereren
	$transactienummer=mt_rand(10000,99999);
global $db;
	// checken of transactienummer wel uniek is
	$result = mysql_query("SELECT * FROM reporting_trainingkey WHERE tk = '$transactienummer' && partner_id = '$choice_partner'", $db);
	while ($row = mysql_fetch_assoc($result)) { 
		$nr = $row['tk'];
	}

	if($nr != $transactienummer)
	{	
		return $transactienummer;	 
	}
	else 
	{
		return randomize();	
	}
}
	
	
echo "<table>";
for( $i=0; $i<$number_keys; $i++ ) // 1000
{		  
	$transactienummer = randomize();
	
	

	//$sql_insert = mysql_query("INSERT INTO reporting_trainingkey (tk, course_id, partner_id) VALUES ('$transactienummer', '37','28')");
	$sql_insert = mysql_query("INSERT INTO reporting_trainingkey (tk, course_id, partner_id, date) VALUES ('$transactienummer', '$course_id','$choice_partner', NOW())", $db);
	echo "<tr><td>" . $prefix . $choice_course . $transactienummer . "</td></tr>"; // 26 voor MSPP, 15 voor Foundation, 38 voor DTBD 2days
	// also save new student keys directly into imijoomla_int -> imi_course_studentkeys table
	$complete_key = $prefix . $choice_course . $transactienummer;

	mysql_query("INSERT INTO imi_course_studentkeys (studentkey, language, created, created_by) VALUES ('$complete_key', '*',NOW(), '1600')", $db3);
}
echo "</table>";

}
?>

<?php if(!$verzenden) {?>
<form method="post" action="#">
    <select name="choice_partner">
      <option value="">- Please select a partner-</option>
      <?php
	  $choice_partner =""; // added to avoid notices

		$partner_result=mysql_query("SELECT * FROM reporting_partners WHERE active='yes' ORDER BY partner_company ASC", $db);
		while($partner=mysql_fetch_array($partner_result)){
			
		
		?>
      <option value="<?php echo $partner['partner_id'];  ?>" <?php if($choice_partner==$partner['partner_id']) { echo "selected='selected'";} ?>><?php echo $partner['partner_company']; ?></option>
      <?php
		}
		?>
    </select><br />
    
    <select name="choice_course">
      <option value="">- Select course -</option>
      <?php
	  $course = ""; // added to avoid empty variable notices

		$course_result=mysql_query("SELECT * FROM reporting_courses ORDER BY course_name ASC", $db);
		while($course_row=mysql_fetch_array($course_result)){
			// select training_key field, this field is used in the student keys
			
		?>
      <option value="<?php echo $course_row['course_trainingkey'] ?>" <?php if(($course)==$course_row['course_trainingkey']) { echo "selected='selected'";} ?>> <?php echo $course_row['course_name'] ?> </option>
      <?php
		}
		?>
    </select><br />
    <input type="text" name="number_keys" value="1000" /><br />
<input type="submit" name="verzenden" value="Generate student keys" class="btn" />
</form>
<p><strong>Warning:</strong> Do not leave this page until all keys are generated and displayed on the screen.<br /> To avoid a script time-out, only generation of max. 1000 keys per batch is possible.</p>
<?php } // einde if not verzenden ?>

<?php
}
?>
                                </div>
                            </div>
                            <!-- /block -->


</div>
</div>
</div>
</div>
            <hr>
<?php
include("../footer.php");
?>
</html> 
 
 </body>
 </html>        
