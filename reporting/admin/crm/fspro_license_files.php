<?php
$rootdir="../../";
$page="FS Pro 2013: license files";
include("../connect.php");
include("../header.php");
?>

<body>
<?php
include("../topnav.php");
?>
<div class="container-fluid">
            <div class="row-fluid">
                
                <?php
				include("../sidenav.php");
				?>
                
                <div class="span10" id="content">
                    <div class="row-fluid">
                    	<div class="span12">
                        
                            <!-- block -->
                            <div class="block">
                                <div class="navbar navbar-inner block-header">
                                <h3>FS Pro License Files (expiry dates)</h3>     
                                </div>
                                <div class="block-content collapse in">
<?php 
//echo $_SESSION['username'] . $_SESSION['function'];
if (!isset($_SESSION["username"])){
	echo $niet_ingelogd;
}

// hieronder staan de functies die toegelaten worden tot deze pagina
elseif($_SESSION["function"] != "admin" && $_SESSION["function"] != "it" && $_SESSION["function"] != "marketing"){ // make it accessible with function admin and it
	echo $geen_toegang;
}else{

?>
<div class="ui-widget">


<p>Remark: Data is not live! <!--(<a href="../renewal/renewal_update.php">add activation records</a> + <a href="../renewal/renewals.php">update data</a>)--> <a href="report_renewal_mails.php">View report renewal notification mails</a></p>

<form action="" method="post">
  <select name="product_name">
  <option value="">Please select version ...</option>
  <option value="1 year">Subscription 1 year</option>
  <option value="2 year">Subscription 2 year</option>
  <option value="3 year">Subscription 3 year</option>
  <option value="Perpetual">Perpetual</option>
  </select>
    Key: <input type="text" name="key" />
    Name/Organization: 
    <input type="text" name="name" />
    Email: <input type="text" name="email" id="suggestionbox" />
    <input type="submit" name="verzenden" value="Search" class="btn" />
</form>

<table class="table table-striped">
<tr>
  <th><a href="?sort=product_name">FS Pro 2013 version</a></th><th><a href="?sort=lickey">Key</a></th><th><a href="?sort=tries">Tries</a></th><th><a href="?sort=maxtries">Maxtries</a></th><th><a href="?sort=enduser_name">Name/Organization enduser</a></th><th><a href="?sort=enduser_email">Email enduser</a></th><th><a href="?sort=expiry">Expiry date</a></th><th>Note</th>
  <th>Order details</th>

<?php

function replace_firstname($nummerke)
{
	$sql_user = mysql_query("SELECT * FROM customer_entity_varchar WHERE entity_id = '$nummerke' && attribute_id = '5'");
	$report_user = mysql_fetch_assoc($sql_user);
	
	$out = $report_user['value'];
	return $out;
}

// HIERONDER JUIST ZOEKSCRIPT VOOR MULTIPLE ZOEKWAARDEN

$product_name = isset($_POST['product_name']) ? $_POST['product_name'] : '';
$key = isset($_POST['key']) ? $_POST['key'] : '';
$name = isset($_POST['name']) ? $_POST['name'] : '';
$email = isset($_POST['email']) ? $_POST['email'] : '';
//$ucreated = isset($_POST['ucreated']) ? $_POST['ucreated'] : '';
$sort = isset($_GET['sort']) ? $_GET['sort'] : '';

$where = array();
if(!empty($product_name)) $where[] = " product_name = '" . $product_name . "'";
if(!empty($key)) $where[] = " lickey = '" . $key . "'";
if(!empty($name)) $where[] = " enduser_name LIKE '%" . $name . "%'";
if(!empty($email)) $where[] = " enduser_email LIKE '%" . $email . "%'";

$where = implode(' AND ', $where);

// uitbreiding kolommen sorteren
if (!$sort){
$sort = "expiry";
} else {
$sort = $_GET['sort'];
//$arrow = isset($_GET['arrow']) ? $_GET['arrow'] : '';
}

if(empty($where))
{
	//$query = "SELECT * FROM AS_unique_key2013 WHERE ucreated !='' ORDER BY " . $sort . " " . $arrow; 
	$query = "SELECT * FROM as_unique_key2013 WHERE expiry !='' ORDER BY " . $sort;
}
else
{
	//$query = "SELECT * FROM AS_unique_key2013 WHERE $where && ucreated !='' ORDER BY " . $sort . " " . $arrow;
	$query = "SELECT * FROM as_unique_key2013 WHERE $where && expiry !='' ORDER BY " . $sort;
}

$result = mysql_query($query);
$aantal = mysql_num_rows($result);
echo "Number of license files: " . $aantal;
	while($row = mysql_fetch_array($result))
	{
		
		$key = $row['lickey'];
		echo "<tr>";
		echo "<td>" . $row['product_name'] . "</td>";
		echo "<td>" . $row['lickey'] . "</td>";
		echo "<td>" . $row['tries'] . "</td>";
		echo "<td>" . $row['maxtries'] . "</td>";
		echo "<td>";
		/*$result_name = mysql_query("SELECT * FROM as_customerinfo WHERE COL2 = '$key'");
		while($row_name = mysql_fetch_array($result_name))
		{
			echo $row_name['COL3'] ."<br />";	
		}*/
		echo $row['enduser_name'];
		echo "</td>";
		echo "<td>";
		echo $row['enduser_email']; 
		/*$result_email = mysql_query("SELECT * FROM as_customerinfo WHERE COL2 = '$key'");
		while($row_email = mysql_fetch_array($result_email))
		{
			echo $row_email['COL1'] ."<br />";	
		}*/
		echo "</td>";
		echo "<td>" . $row['expiry'] . "</td>";
		echo "<td>" . $row['notice'] . "</td>";
		if(!empty($row['buyer_email'])) { echo "<td><a href=\"fspro_license_files_orderinfo.php?key=" .$key  . "\" target=\"_blank\">view</a></td>"; }
		else { echo "<td>N.A.</td>"; }
		echo "</tr>";
	}
?>

</table>
</div>
<?php
}
?>
                                </div>
                            </div>
                            <!-- /block -->


</div>
</div>
</div>
</div>
            <hr>
<?php
include("../footer.php");
?>
</html>