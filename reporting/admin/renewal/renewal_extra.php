<?php
$rootdir="../../";
$page="Renewal extra";
include("../connect.php");
include("../header.php");
?>

<body>
<?php
include("../topnav.php");
?>
<div class="container-fluid">
  <div class="row-fluid">
    <?php
				//include("../sidenav.php");
				?>
    <div class="span10" id="content">
      <div class="row-fluid">
        <div class="span12"> 
          
          <!-- block -->
          <div class="block">
            <div class="navbar navbar-inner block-header">
              <h3>Renewal extra</h3>
            </div>
            <div class="block-content collapse in">
              <?php 
if (!isset($_SESSION["username"])){
	echo $niet_ingelogd;
}
// hieronder staan de functies die toegelaten worden tot deze pagina
elseif($_SESSION["function"] != "admin"){ // make it accessible with function admin 
	echo $geen_toegang;
}else{
?>
<form action="" method="post">

  <p>
    STEP 1: 
      <input type="submit" name="check" value="Check keys without order" />
  </p>
  <p>STEP 2: 
    <input type="submit" name="update1y" value="Update 1 year" />
    <input type="submit" name="update2y" value="Update 2 year" />
    <input type="submit" name="update3y" value="Update 3 year" />
    <input type="submit" name="updateperp" value="Update Perpetual" />
  </p>
  <p>
    STEP 3: 
      <input type="submit" name="upd_buyer" value="Update buyer" />
  </p>
  <p>
    STEP 4: 
      <input type="submit" name="upd_enduser" value="Update end user name and email" />
  </p>
  <p>
    STEP 5: 
      <input type="submit" name="upd_notice" value="Update note, tries and maxtries" />
  </p>
</form>

  <?php

// license file table must be uploaded in same DB as webstore
// moduleid = QHD001, de QHD002 is het serverproduct en niet relevant voor ons
// eerst check doen: alle license keys (distinct) vergelijken met orders en tonen van welke keys er geen order kan gevonden worden

ini_set('max_execution_time', 300); // highers up the default 30 seconds script executing time to 5 minutes, works on localhost, not tested on hosting environment

/*
$uniquekey = isset($_POST['uniquekey']) ? $_POST['uniquekey'] : '';
// enkel unieke keys in nieuwe tabel plaatsen
// een license key kan meerdere license files hebben, bij elke update van de expiry date komt er een record bij
// dus niet enkel de unieke license key rows moeten getrokken worden, maar meteen ook die met de recentste expiry date
if($uniquekey)
{	
	//$query_distinct = "SELECT DISTINCT `entrykey` FROM sheet1"; // backquote's verplicht in DISTINCT query, anders werkt dit niet!!!
	// to be able to fetch at the same time the most recent date record (lvvalue), do not use DISTINCT but GROUP BY. distinct and group by return the same results
	// change of plan: catch the first license file per key!
	$query_distinct = "SELECT entrykey, lvvalue, MIN( id ) AS eerst FROM `sheet1` WHERE lvvalue IS NOT NULL GROUP BY entrykey"; // enkel de records met expiry date ingevuld
	$result_distinct = mysql_query($query_distinct);
	while($row = mysql_fetch_array($result_distinct))
	{
	 echo $row['entrykey'] . $row['lvvalue'] . "<br />";
	//$query_create = "INSERT INTO as_unique_key2013 (ukey, ucreated, utries) VALUES ('" . $row['entrykey'] . "','" . $row['lvvalue'] . "','" . $row['tries'] . "')";
	$query_create = "INSERT INTO as_unique_key2013 (ukey, ucreated) VALUES ('" . $row['entrykey'] . "','" . $row['lvvalue'] . "')";
	$result_create = mysql_query($query_create);
	// creëer een tabel met unieke keys om hierin bij te houden welke mails zijn verstuurd voor welke key
	// deze tabel mag bij updates niet gewist worden!
	// mag slechts 1 keer aangemaakt worden, vandaar in commentaar hier beneden
	//$query_create2 = "INSERT INTO as_cron (`key`) VALUES ('" . $row['entrykey'] . "')";
	//$result_create2 = mysql_query($query_create2);
	echo "<p>Unique keys with most recent expiry date has been added.</p>";	
	}
}*/
/*$upd_date = isset($_POST['upd_date']) ? $_POST['upd_date'] : '';
if($upd_date)
{
		$result_update_date = mysql_query("UPDATE as_unique_key2013 AS un, sheet1 AS sh SET un.ucreated = sh.lvvalue WHERE sh.entrykey = un.ukey && sh.lvvalue IS NOT NULL");
		echo "<p>Dates added</p>";
}*/

$check = isset($_POST['check']) ? $_POST['check'] : '';
if($check)
{
	echo "<p style=color:red;>The following license keys have a License File but are not known in the web store:</p>";
	// toon alle license keys die in tbl unique_key2013 zitten, maar niet in de store zitten
	$result_check =	mysql_query("SELECT lickey FROM as_unique_key2013 WHERE lickey NOT IN (SELECT serial_number FROM license_serialnumbers WHERE serial_number = lickey)", $db);
	while($row_check=mysql_fetch_array($result_check)){
		
		echo $row_check['lickey'] . "<br />";
	}
}

$upd_buyer = isset($_POST['upd_buyer']) ? $_POST['upd_buyer'] : '';
if($upd_buyer)
{
	//$column_buyer = mysql_query("ALTER TABLE `as_unique_key2013` ADD `buyer` varchar(120) NOT NULL"); // moet met backquote's of werkt niet!
	//echo "<p>Column buyer added</p>";
	//$result_buyer = mysql_query("UPDATE as_unique_key2013 AS u, license_serialnumbers AS l SET u.buyer = l.customer_id WHERE u.lickey = l.serial_number ");
	//$result_buyer = mysql_query("UPDATE as_unique_key2013 AS u, license_info AS l SET u.buyer = l.email WHERE u.ukey = l.last_license_key ");
	//echo "<p>Added buyer IDs.</p>";
	// lookup buyer
	//$column_email = mysql_query("ALTER TABLE `as_unique_key2013` ADD `buyer_email` varchar(120) NOT NULL"); // moet met backquote's of werkt niet!
	//$column_firstname = mysql_query("ALTER TABLE `as_unique_key2013` ADD `buyer_firstname` varchar(100) NOT NULL"); // moet met backquote's of werkt niet!
	/*$result_endcustomer = mysql_query("UPDATE as_unique_key2013 AS u, customer_entity AS c
	SET u.buyer_email = c.email WHERE u.buyer = c.entity_id
	");
	echo "<p>Added email address</p>";*/
	
	// gezien er ook guest orders zijn, niet op customer id zoeken maar rechtstreeks op email in sales_flat_order (altijd ingevuld)
	$result_endcustomer = mysql_query("UPDATE as_unique_key2013 AS u, sales_flat_order AS c, license_serialnumbers AS l
	SET u.buyer_email = c.customer_email WHERE l.order_id = c.entity_id && u.lickey = l.serial_number && u.buyer_email =''
	", $db);
	echo "<p>Added email address</p>";
	
	//$result_firstname = mysql_query("UPDATE as_unique_key2013 AS u, customer_entity_varchar AS c
	//SET u.buyer_firstname = c.value WHERE u.buyer = c.entity_id && c.attribute_id = '5'
	//");
	//echo "<p>Added first name</p>";
	//$result_buyer2 = mysql_query("UPDATE as_unique_key2013 AS u, license_serialnumbers AS l SET u.buyer = l.customer_id WHERE u.ukey = l.serial_number ");
	//echo "<p>Added buyer email</p>";
}

$update1y = isset($_POST['update1y']) ? $_POST['update1y'] : '';
$update2y = isset($_POST['update2y']) ? $_POST['update2y'] : '';
$update3y = isset($_POST['update3y']) ? $_POST['update3y'] : '';
$updateperp = isset($_POST['updateperp']) ? $_POST['updateperp'] : '';

if($update1y)
{
	// read out product id's in tbl license_serialnumbers
	// this means when new Virtual Products with license keys attached to them, are released, I have to add these IDs to this script!!!!!!
	$result_product1 = mysql_query("UPDATE as_unique_key2013 AS u, license_serialnumbers AS l SET u.product_name = '1 year' 
	WHERE u.actkey = l.serial_number && l.product_id = '401' && u.product_name IS NULL  
	OR (u.actkey = l.serial_number && l.product_id = '406' && u.product_name IS NULL)
	OR (u.actkey = l.serial_number && l.product_id = '389' && u.product_name IS NULL)
	OR (u.actkey = l.serial_number && l.product_id = '397' && u.product_name IS NULL)
	OR (u.actkey = l.serial_number && l.product_id = '418' && u.product_name IS NULL)
	OR (u.actkey = l.serial_number && l.product_id = '421' && u.product_name IS NULL)
	OR (u.actkey = l.serial_number && l.product_id = '410' && u.product_name IS NULL)
	OR (u.actkey = l.serial_number && l.product_id = '509' && u.product_name IS NULL)
	OR (u.actkey = l.serial_number && l.product_id = '552' && u.product_name IS NULL)
	OR (u.actkey = l.serial_number && l.product_id = '558' && u.product_name IS NULL)
	OR (u.actkey = l.serial_number && l.product_id = '566' && u.product_name IS NULL)
	OR (u.actkey = l.serial_number && l.product_id = '571' && u.product_name IS NULL)
	", $db);
	echo "1 year added";
}
if($update2y)
{
	$result_product2 = mysql_query("UPDATE as_unique_key2013 AS u, license_serialnumbers AS l SET u.product_name = '2 year' 
	WHERE u.actkey = l.serial_number && l.product_id = '390' && u.product_name IS NULL 
	OR (u.actkey = l.serial_number && l.product_id = '398' && u.product_name IS NULL)
	OR (u.actkey = l.serial_number && l.product_id = '419' && u.product_name IS NULL)
	OR (u.actkey = l.serial_number && l.product_id = '402' && u.product_name IS NULL)
	OR (u.actkey = l.serial_number && l.product_id = '422' && u.product_name IS NULL)
	OR (u.actkey = l.serial_number && l.product_id = '510' && u.product_name IS NULL)
	OR (u.actkey = l.serial_number && l.product_id = '525' && u.product_name IS NULL)
	OR (u.actkey = l.serial_number && l.product_id = '553' && u.product_name IS NULL)
	OR (u.actkey = l.serial_number && l.product_id = '562' && u.product_name IS NULL)
	OR (u.actkey = l.serial_number && l.product_id = '567' && u.product_name IS NULL)
	OR (u.actkey = l.serial_number && l.product_id = '572' && u.product_name IS NULL)
	", $db);
	echo "2 year added";	
}
if($update3y)
{
	$result_product3 = mysql_query("UPDATE as_unique_key2013 AS u, license_serialnumbers AS l SET u.product_name = '3 year' 
	WHERE u.actkey = l.serial_number && l.product_id = '391' && u.product_name IS NULL 
	OR (u.actkey = l.serial_number && l.product_id = '399' && u.product_name IS NULL)
	OR (u.actkey = l.serial_number && l.product_id = '420' && u.product_name IS NULL)
	OR (u.actkey = l.serial_number && l.product_id = '403' && u.product_name IS NULL)
	OR (u.actkey = l.serial_number && l.product_id = '423' && u.product_name IS NULL)
	OR (u.actkey = l.serial_number && l.product_id = '511' && u.product_name IS NULL)
	OR (u.actkey = l.serial_number && l.product_id = '526' && u.product_name IS NULL)
	OR (u.actkey = l.serial_number && l.product_id = '554' && u.product_name IS NULL)
	OR (u.actkey = l.serial_number && l.product_id = '563' && u.product_name IS NULL)
	OR (u.actkey = l.serial_number && l.product_id = '568' && u.product_name IS NULL)
	OR (u.actkey = l.serial_number && l.product_id = '573' && u.product_name IS NULL)
	", $db);
	echo "3 year added";
}
if($updateperp)
{
	$result_productp = mysql_query("UPDATE as_unique_key2013 AS u, license_serialnumbers AS l SET u.product_name = 'perpetual' 
	WHERE u.actkey = l.serial_number && l.product_id = '385' && u.product_name IS NULL  
	OR (u.actkey = l.serial_number && l.product_id = '396' && u.product_name IS NULL)
	OR (u.actkey = l.serial_number && l.product_id = '405' && u.product_name IS NULL)
	OR (u.actkey = l.serial_number && l.product_id = '415' && u.product_name IS NULL)
	OR (u.actkey = l.serial_number && l.product_id = '414' && u.product_name IS NULL)
	OR (u.actkey = l.serial_number && l.product_id = '508' && u.product_name IS NULL)
	OR (u.actkey = l.serial_number && l.product_id = '527' && u.product_name IS NULL)
	OR (u.actkey = l.serial_number && l.product_id = '555' && u.product_name IS NULL)
	OR (u.actkey = l.serial_number && l.product_id = '564' && u.product_name IS NULL)
	OR (u.actkey = l.serial_number && l.product_id = '569' && u.product_name IS NULL)
	OR (u.actkey = l.serial_number && l.product_id = '574' && u.product_name IS NULL)
	", $db);
	echo "<p>perpetual added</p>";
}
// idea: to limit the amount of rows, you could remove now all records which are perpetual

$upd_enduser = isset($_POST['upd_enduser']) ? $_POST['upd_enduser'] : '';
if($upd_enduser)
{
	//$column_enduser_email = mysql_query("ALTER TABLE `as_unique_key2013` ADD `enduser_email` varchar(120) NOT NULL"); // moet met backquote's of werkt niet!	
	//$column_enduser_name = mysql_query("ALTER TABLE `as_unique_key2013` ADD `enduser_name` varchar(120) NOT NULL"); // moet met backquote's of werkt niet!
	
	//echo "<p>Columns end user added.</p>";
	$result_enduser_name = mysql_query("UPDATE as_unique_key2013 AS u, as_customerinfo AS l SET u.enduser_name = l.COL3 WHERE u.lickey = l.COL2 && u.enduser_name =''", $db);
	$result_enduser_email = mysql_query("UPDATE as_unique_key2013 AS u, as_customerinfo AS l SET u.enduser_email = l.COL1 WHERE u.lickey = l.COL2 && u.enduser_email = ''", $db);
	echo "<p>Added end user name and email.</p>";
}

$upd_notice = isset($_POST['upd_notice']) ? $_POST['upd_notice'] : '';
if($upd_notice)
{
	
	$result_notice = mysql_query("UPDATE as_unique_key2013 AS u, as_notice AS n SET u.notice = n.notice WHERE u.actkey = n.entrykey ", $db);
	$result_maxtries = mysql_query("UPDATE as_unique_key2013 AS u, as_tries AS m SET u.maxtries = m.maxtries WHERE u.actkey = m.entrykey ", $db);
	$result_tries = mysql_query("UPDATE as_unique_key2013 AS u, as_tries AS m SET u.tries = m.tries WHERE u.actkey = m.entrykey ", $db);
	echo "<p>Added notice, tries and maxtries.</p>";
}

?>

              <?php
}
?>
            </div>
          </div>
          <!-- /block --> 
          
        </div>
      </div>
    </div>
  </div>
  <hr>
  <?php
include("../footer.php");
?>
</html>