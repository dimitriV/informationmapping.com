<?php
$rootdir="../../";
$page="Renewal update";
include("../connect.php");
include("../header.php");
?>

<body>
<?php
include("../topnav.php");
?>
<div class="container-fluid">
  <div class="row-fluid">
    <?php
				//include("../sidenav.php");
				?>
    <div class="span10" id="content">
      <div class="row-fluid">
        <div class="span12"> 
          
          <!-- block -->
          <div class="block">
            <div class="navbar navbar-inner block-header">
              <h3>Renewal update</h3>
            </div>
            <div class="block-content collapse in">
              <?php 
if (!isset($_SESSION["username"])){
	echo $niet_ingelogd;
}
// hieronder staan de functies die toegelaten worden tot deze pagina
elseif($_SESSION["function"] != "admin"){ // make it accessible with function admin 
	echo $geen_toegang;
}else{
?>
<?php
// DEZE PAGINA MOET GERUND WORDEN BIJ ELKE UPDATE VAN DE DB (BEHALVE DE ALLEREERSTE KEER)
// ACTIVATION KEYS DIE ER BIJGEKOMEN ZIJN WORDEN TOEGEVOEGD
// EXPIRY DATE WORDT TOEGEVOEGD EN ER WORDT GEKEKEN NAAR DE LAATSTE TOEGEVOEGDE EXPIRY DATE

ini_set('max_execution_time', 300); // highers up the default 30 seconds script executing time to 5 minutes, works on localhost, not tested on hosting environment


// aanmaken statische tabel
$res_act = mysql_query("SELECT * FROM as_links", $db);

while($row = mysql_fetch_assoc($res_act)) 
{
 	$lickey = $row['lickey'];	
 	$actkey = $row['actkey'];
 

 	$query_create = mysql_query("INSERT ignore INTO as_unique_key2013 (lickey, actkey, time) VALUES ('$lickey','$actkey', NOW())", $db); // records aanmaken
}

$res_exp = mysql_query("SELECT lic.lickey AS updatelickey , MIN(lic.expiry) AS mine, MAX(lic.expiry) AS maxe FROM as_unique_key2013 m JOIN
		as_lic_files lic ON m.lickey = lic.lickey
group by m.lickey", $db);


// Old query from Wim not working since?
//$res_exp = mysql_query("SELECT lic.key AS updatelickey , MIN(lic.expiry) AS mine, MAX(lic.expiry) AS maxe FROM as_unique_key2013 m JOIN as_lic_files lic ON m.lickey = lic.key
//    group by lic.key");// original query iss too heavy to load so I excluded perpetual and NULL

 	while($row2 = mysql_fetch_array($res_exp))
	{
 		
		$expiry_max = $row2['maxe'];
		//$expiry_min = $row2['mine']; // 1e keer runnen is mine (1 keer runnen slechts)
		$updatelickey = $row2['updatelickey'];
 		//echo $expiry . "<br />";

 		$res_files = mysql_query("UPDATE as_unique_key2013 SET expiry = '$expiry_max' WHERE lickey = '$updatelickey' 
		&& expiry IS NULL", $db);  // query om na allereerste keer te runnen
	}
//}
sleep(1);
// delete all entries in as_unique_key2013 that come from a license that has been locked
mysql_query("DELETE FROM as_unique_key2013 WHERE lickey IN (SELECT COL1 FROM as_locked)", $db);
echo "script done";
?>
              <?php
}
?>
            </div>
          </div>
          <!-- /block --> 
          
        </div>
      </div>
    </div>
  </div>
  <hr>
  <?php
include("../footer.php");
?>
</html>