<?php
$rootdir="../../";
$page="Resume";
include("../connect.php");
include("../header.php");
?>
<?php


$SQL_resume="SELECT * FROM resume WHERE resume_id=" . $_GET['resume_id'];
$resume_result=mysql_query($SQL_resume);
$resume=mysql_fetch_array($resume_result);

if($resume['program']=="Information Mapping Certified Consultant Program") {
	$shortprogram="IMCC";
}

if($resume['program']=="Information Mapping Professional Program") {
	$shortprogram="IMP";
}
?>

<body>
<?php
include("../topnav.php");
?>
<div class="container-fluid">
            <div class="row-fluid">
                
                <?php
				//include("../sidenav.php");
				?>
                
                <div class="span10" id="content">
                    <div class="row-fluid">
                    	<div class="span12">
                        
                            <!-- block -->
                            <div class="block">
                                <div class="navbar navbar-inner block-header">
                                <h3><?php echo $shortprogram; ?>: Resume of <?php echo $resume['firstname'] ?> <?php echo $resume['lastname'] ?></h3>    
                                </div>
                                <div class="block-content collapse in">
<?php 
if (!isset($_SESSION["username"])){
	echo $niet_ingelogd;
}
// hieronder staan de functies die toegelaten worden tot deze pagina
elseif($_SESSION["function"] != "admin"){
	echo $geen_toegang;
}else{
?>

<p class="buttons"><a href="javascript:history.back()" class="back">Back</a> </p>

<table width="100%"  border="0" cellpadding="0" cellspacing="0">
<tr>
<td width="50%" style="padding-right:20px;" valign="top">

<table width="100%" border="0" cellpadding="0" cellspacing="0" class="imcc">
  <tr>
  <th colspan="6" class="detail">Personal details
  </th>
  </tr>
  <tr>
    <td width="150"><strong>First name</strong></td>
    <td colspan="5"><?php echo $resume['firstname'] ?></td>
  </tr>
  <tr>
    <td><strong>Last name</strong></td>
    <td colspan="5"><?php echo $resume['lastname'] ?></td>
  </tr>
  <tr>
    <td><strong>E-mail</strong></td>
    <td colspan="5"><a href="mailto:<?php echo $resume['email'] ?>"><?php echo $resume['email'] ?></a></td>
  </tr>
    <tr>
    <td><strong>Telephone</strong></td>
    <td colspan="5"><?php echo $resume['telephone'] ?></td>
  </tr>
  <tr>
    <td><strong>Resume submitted on</strong></td>
    <td colspan="5"><?php echo $resume['resumedate'] ?></td>
  </tr>
  </table>
  
</td>
<td width="50%" style="padding-left:20px;" valign="top">

<table width="100%" border="0" cellpadding="0" cellspacing="0" class="imcc">
  <tr>
  <th colspan="6" class="detail">Address details
  </th>
  </tr>
  <tr>
    <td width="150"><strong>Address</strong></td>
    <td colspan="5"><?php echo $resume['address1'] ?><br /><?php echo $resume['address2'] ?></td>
  </tr>
  <tr>
    <td><strong>Postal code and city</strong></td>
    <td colspan="5"><?php echo $resume['postalcode'] ?> <?php echo $resume['city'] ?></td>
  </tr>
  <tr>
    <td><strong>State</strong></td>
    <td colspan="5"><?php echo $resume['state'] ?></td>
  </tr>
  <tr>
    <td><strong>Country</strong></td>
    <td colspan="5"><?php echo $resume['country'] ?></td>
  </tr>
  </table>

</td>
</tr>

<tr>
<td width="50%" style="padding-right:20px;" valign="top">

<table width="100%" border="0" cellpadding="0" cellspacing="0" class="imcc">
  <tr>
  <th colspan="6" class="detail">Professional experience
  </th>
  </tr>
  <tr>
    <td width="150"><strong>Summary</strong></td>
    <td colspan="5"><?php echo $resume['profsummary'] ?></td>
  </tr>
  <tr>
    <td><strong>Specialties</strong></td>
    <td colspan="5"><?php echo $resume['specialties'] ?></td>
  </tr>
  </table>

</td>
<td width="50%" style="padding-left:20px;" valign="top">

<? if(!empty($resume['coursetitle_imap'])) { 
	?>

  <table width="100%" border="0" cellpadding="0" cellspacing="0" class="imcc">
  <tr>
  <th colspan="2" class="detail">Information Mapping&reg; Training</th>
  </tr>
  <tr>
    <td width="150"><strong>Course title</strong></td>
    <td><?php echo $resume['coursetitle_imap'] ?></td>
  </tr>
  <tr>
    <td><strong>Training company</strong></td>
    <td><?php echo $resume['company_imap'] ?></td>
  </tr>
  <tr>
    <td><strong>Date</strong></td>
    <td><?php echo $resume['month_imap'] ?> <?php echo $resume['year_imap'] ?></td>
  </tr>
  <tr>
    <td><strong>Trainer</strong></td>
    <td><?php echo $resume['trainer_imap'] ?></td>
  </tr>
  <tr>
    <td><strong>Comments</strong></td>
    <td><?php echo $resume['comments_imap'] ?></td>
  </tr>

  </table>
  <? } ?>
  <? if(!empty($resume['certificate_imap'])) { 
	 ?>
  <table width="100%" border="0" cellpadding="0" cellspacing="0" class="imcc">
  <tr>
    <td width="150"><strong>Certificate</strong></td>
    <td>
	<a href="http://www.informationmapping.com/imcc/resume/certificates/<?php echo $resume['certificate_imap'] ?>" target="_blank">View</a>
	</td>
  </tr>
  </table>
  <? } ?>

</td>
</tr>

<tr>
<td width="50%" style="padding-right:20px;" valign="top">

<table width="100%" border="0" cellpadding="0" cellspacing="0" class="imcc">
  <tr>
  <th colspan="6" class="detail">Current job
  </th>
  </tr>
  <tr>
    <td width="150"><strong>Position</strong></td>
    <td colspan="5"><?php echo $resume['title_job0'] ?></td>
  </tr>
  <tr>
    <td><strong>Company</strong></td>
    <td colspan="5"><?php echo $resume['company_job0'] ?></td>
  </tr>
  <tr>
    <td><strong>Since</strong></td>
    <td colspan="5"><?php echo $resume['startmonth_job0'] ?> <?php echo $resume['startyear_job0'] ?></td>
  </tr>
  <tr>
    <td><strong>Short description</strong></td>
    <td colspan="5"><?php echo $resume['description_job0'] ?></td>
  </tr>
  <tr>
    <td><strong>Company website</strong></td>
    <td colspan="5"><?php echo $resume['website'] ?></td>
  </tr>
  </table>

</td>
<td width="50%" style="padding-left:20px;" valign="top">

<? if(!empty($resume['coursetitle0'])) { 
	?>
  
  <table width="100%" border="0" cellpadding="0" cellspacing="0" class="imcc">
  <tr>
  <th colspan="2" class="detail">Other training</th>
  </tr>
  <tr>
    <td width="150"><strong>Course title</strong></td>
    <td><?php echo $resume['coursetitle0'] ?></td>
  </tr>
  <tr>
    <td><strong>Training company</strong></td>
    <td><?php echo $resume['company0'] ?></td>
  </tr>
  <tr>
    <td><strong>Date</strong></td>
    <td><?php echo $resume['month0'] ?> <?php echo $resume['year0'] ?></td>
  </tr>
  <tr>
    <td><strong>Comments</strong></td>
    <td><?php echo $resume['comments0'] ?></td>
  </tr>

  </table>
<? } ?>
</td>
</tr>

<tr>
<td width="50%" style="padding-right:20px;" valign="top">

<? if(!empty($resume['title_job1'])) { 
	?>

<table width="100%" border="0" cellpadding="0" cellspacing="0" class="imcc">
  <tr>
  <th colspan="6" class="detail">Previous job
  </th>
  </tr>
  <tr>
    <td width="150"><strong>Position</strong></td>
    <td colspan="5"><?php echo $resume['title_job1'] ?></td>
  </tr>
  <tr>
    <td><strong>Company</strong></td>
    <td colspan="5"><?php echo $resume['company_job1'] ?></td>
  </tr>
  <tr>
    <td><strong>Period</strong></td>
    <td colspan="5">From <?php echo $resume['startmonth_job1'] ?> <?php echo $resume['startyear_job1'] ?> to <?php echo $resume['endmonth_job1'] ?> <?php echo $resume['endyear_job1'] ?></td>
  </tr>
  <tr>
    <td><strong>Short description</strong></td>
    <td colspan="5"><?php echo $resume['description_job1'] ?></td>
  </tr>
  </table>
<? } ?>
</td>
<td width="50%" style="padding-left:20px;" valign="top">

<? if(!empty($resume['coursetitle1'])) { 
	?>

<table width="100%" border="0" cellpadding="0" cellspacing="0" class="imcc">
  <tr>
  <th colspan="2" class="detail">Other training</th>
  </tr>
  <tr>
    <td width="150"><strong>Course title</strong></td>
    <td><?php echo $resume['coursetitle1'] ?></td>
  </tr>
  <tr>
    <td><strong>Training company</strong></td>
    <td><?php echo $resume['company1'] ?></td>
  </tr>
  <tr>
    <td><strong>Date</strong></td>
    <td><?php echo $resume['month1'] ?> <?php echo $resume['year1'] ?></td>
  </tr>
  <tr>
    <td><strong>Comments</strong></td>
    <td><?php echo $resume['comments1'] ?></td>
  </tr>

  </table>
<? } ?>
</td>
</tr>
   

    
  
</td>
</tr>
</table>
<?php
}
?>
                                </div>
                            </div>
                            <!-- /block -->


</div>
</div>
</div>
</div>
            <hr>
<?php
include("../footer.php");
?>
</html>