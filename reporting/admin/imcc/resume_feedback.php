<?php
$rootdir="../../";
$page="Resume";
include("../connect.php");
include("../header.php");
?>
<body>
<?php
include("../topnav.php");
?>
<?php


$SQL_resume="SELECT * FROM resume WHERE resume_id=" . $_GET['resume_id'];
$resume_result=mysql_query($SQL_resume);
$resume=mysql_fetch_array($resume_result);

if(isset($_POST['submit'])) {
  $status = trim($_POST['status']);
  $feedback = trim($_POST['feedback']);
  $feedbackdate = trim($_POST['feedbackdate']);
  $resume_id = $_GET['resume_id'];
	
  $feedback_SQL_insert="INSERT INTO resumefeedback (feedback,resume_id,status,feedbackdate) VALUES ('$feedback','$resume_id','$status',NOW()) ";
  $bool=mysql_query($feedback_SQL_insert);
  if($bool<>1) echo "There was an error adding your feedback.";
  if($bool==1) {

?>

<h2>Resume Feedback Sent!</h2>
<?php 
/*if (!isset($_SESSION["username"])){
	echo $niet_ingelogd;
}
elseif($_SESSION["function"] != "admin"){
	echo $geen_toegang;
}else{*/
?>
<p>The feedback on the resume is sent to <?php echo $resume['firstname'] ?> <?php echo $resume['lastname'] ?> (<?php echo $resume['email'] ?>).</p>

<?

$SQL_resume="SELECT * FROM resume,resumefeedback WHERE resume.resume_id=" . $_GET['resume_id'] . " AND resumefeedback.resume_id=" . $_GET['resume_id'];
$resume_result=mysql_query($SQL_resume);
$resume=mysql_fetch_array($resume_result);

$program = $resume['program'];
$firstname = $resume['firstname'];
$email = $resume['email'];
$feedback = $resume['feedback'];
$status = $resume['status'];

	$message  = "<table cellpadding='0' cellspacing='0' border='0' width='100%'><tr><td align='center'>";
	$message .= "<table cellpadding='0' cellspacing='0' border='0' width='500'><tr><td align='left'>";
	$message .= "<p><img src='http://www.informationmapping.com/imcc/images/logo_email.gif' alt='Information Mapping logo' /></p>";
	$message .= "<p><font size='4' face='Arial, Helvetica, sans-serif' color=#009edf'>$program</font></p>";
	$message .= "<p><font size='2' face='Arial, Helvetica, sans-serif'>Dear $firstname,</font></p>";
	$message .= "<p><font size='2' face='Arial, Helvetica, sans-serif'>You have received the following feedback to the resume you submitted for the $program:</font></p>";
	$message .= "<p><font size='2' face='Arial, Helvetica, sans-serif'>$feedback</font></p>";
	$message .= "<p><font size='2' face='Arial, Helvetica, sans-serif'>With kind regards,<br />";
	$message .= "Information Mapping International</font></p>";

$mime_boundary="==Multipart_Boundary_x".md5(mt_rand())."x";
	
	$headers = "From: Information Mapping International <info@informationmapping.com>\n";
	$headers .= "BCC: Filip Vanlerberghe <fvanlerberghe@informationmapping.com>\n";
	$headers .= "BCC: Veronique Wittebolle <vwittebolle@informationmapping.com>\n";
	$headers .= "BCC: Francis Declercq <fdeclercq@informationmapping.com>\n";
	$headers .= "X-Priority: 1\n";  	  
	$headers .= "Content-Type: multipart/mixed;\n" . " boundary=\"{$mime_boundary}\"\n" . "MIME-Version: 1.0\n"; 
	$subject="IMCC Program - Feedback resume";

	$message = "This is a multi-part message in MIME format.\n\n" .
         "--{$mime_boundary}\n" .
         "Content-Type: text/html; charset=\"iso-8859-1\"\n" .
         "Content-Transfer-Encoding: 7bit\n\n" . $message . "\n\n";
		 
    $message .="--{$mime_boundary}--\n";

    mail($email,$subject, $message, $headers); 
}
}else{
?>
<div id="admin">
<h2>Resume Feedback</h2>
<?php echo
"<script language='JavaScript'>
myarr=new Array()
myarr[1]=''
myarr[2]='Congratulations! Your resume has been approved. Now you can go to the Information Mapping Web Store and get your access to the <a href=https://www.informationmapping.com/en/shop/certification/im-certified-consultant/imcc-certification>IMCC Exam</a>. You will then receive an e-mail from Information Mapping International with your account information to log in to the exam.<br /><br />We look forward to receiving your completed exam!'
myarr[3]='We recommend that you follow the instructor-led e-learning course Mastering Business Communications before taking the IMCC Exam. You can enroll to this course by <a href=http://www.informationmapping.com/en/shop/instructor-led-e-learning/mastering-business-communications-instructor-led>purchasing your seat on the Information Mapping Web Store</a>. <br /><br />When you have completed the training, please upload your certificate on http://www.informationmapping.com/imcc/resume/upload_certificate.php?resume_id=" . $_GET['resume_id'] . " and we will get back to you.'
myarr[4]='Your application for the Information Mapping Certified Consultant Program has been denied.'
function doIt(objval)
{
document.getElementById('feedback').innerHTML=myarr[objval]
}
</script>";
?>
<p class="buttons"><a href="javascript:history.back()" class="back">Back</a></p>

<form action="<?php echo $_SERVER['../crm/PHP_SELF']; ?>?resume_id=<?php echo $resume['resume_id'] ?>" method="post">
  <fieldset>
  	<input type="hidden" name="resume_id" value="<?php echo $resume['resume_id'] ?>">
 	<ol>
    <li><label for="status">Status</label>
    <select name="status" id="status" onChange="doIt(this.options[this.selectedIndex].value)">
    <option value="1">- Please select -</option>
    <option value="2">Approved</option>
    <option value="3">Pending</option>
    <option value="4">Denied</option>
    </select></li>
    <li><label for="feedback">IMI Feedback</label>
	<textarea name="feedback" id="feedback" cols="50" rows="10" ></textarea>
    </li>
    </ol>
  </fieldset>
  <fieldset class="submit">
    <input type="submit" name="submit" value="Send feedback" class="feedback">
  </fieldset>
</form>
<?
}
?>
<?php
//}
?>
</div>
</body>
</html>