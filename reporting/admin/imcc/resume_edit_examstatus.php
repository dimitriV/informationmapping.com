<?php
$rootdir="../../";
$page="Resume";
include("../connect.php");
include("../header.php");
?>

<body>
<?php
include("../topnav.php");
?>
<div class="container-fluid">
            <div class="row-fluid">
                
                <?php
				//include("../sidenav.php");
				?>
                
                <div class="span10" id="content">
                    <div class="row-fluid">
                    	<div class="span12">
                        
                            <!-- block -->
                            <div class="block">
                                <div class="navbar navbar-inner block-header">
                                <h3>Edit IMCC Exam Status</h3>    
                                </div>
                                <div class="block-content collapse in">
<?php 
if (!isset($_SESSION["username"])){
	echo $niet_ingelogd;
}
// hieronder staan de functies die toegelaten worden tot deze pagina
elseif($_SESSION["function"] != "admin"){
	echo $geen_toegang;
}else{
?>
<?php

$SQL_resume="SELECT * FROM resume WHERE resume_id=" . $_GET['resume_id'];
$resume_result=mysql_query($SQL_resume);
$resume=mysql_fetch_array($resume_result);

if(isset($_POST['submit'])) {
  $status = trim($_POST['status']); // 0 = no exam / 1 = passed / 2 = failed / 3 = no exam needed
  $feedback = trim($_POST['feedback']);
  //$feedbackdate2 = trim($_POST['feedbackdate2']);
  $resume_id = $_GET['resume_id'];
  
  
  /* toevoegen code om examen te uploaden */
  $size = $_FILES['evaluation']['size'];
  	$toegestane_grootte = 5000*1024; // Grootte in bytes > 400*1024 = 400kB
	// FILES BIGGER THAN 5 MB ARE NOT SEND CORRECTLY

	// Toegestane extensies
	$toegestane_extensies = array("doc","docx","pdf");

	// Toegestane mime types
	$mime= $_FILES['evaluation']['type'];
	$toegestane_mimes = array("application/msword","application/vnd.openxmlformats-officedocument.wordprocessingml.document","application/pdf","application/x-pdf");
	
	


	//$uploadmap = "../../exam_evaluation/";
	$uploadmap = "../../../imcc/resume/exam_evaluation/";
	
	  $datum = date("j-m-Y / H:i");
      $md = md5($datum);
      $md_kort = substr($md, 0, 5); // string inkorten
	  
	  $bestandsnaam = $_FILES['evaluation']['name'];
	  // als er geen bestand is geüpload hoeft code niet uitgevoerd te worden voor bijlage
	  if(!empty($bestandsnaam))
	  {
		  
		// Bestandsgrootte controleren
   		if($size > $toegestane_grootte)
      		{echo "<p>The uploaded file is too big.</p>"; exit; }
			
			// Extensie en mime controleren
   		if(!in_array($mime, $toegestane_mimes))
      		{ echo "<p>You uploaded the wrong file type.</p>"; exit; }  
	  
	  $bestand_nieuw = $uploadmap . $md_kort . "_" . $bestandsnaam;
	  
	  
	  move_uploaded_file($_FILES['evaluation']['tmp_name'], $bestand_nieuw);
	  }
  
  /* einde toevoegen code om examen te uploaden */
	
  $feedback_SQL_update="UPDATE resumefeedback SET exam_passed='$status', exam_passed_date = NOW(), exam_evaluation = '$bestand_nieuw' WHERE resume_id='$resume_id'";

  $bool=mysql_query($feedback_SQL_update);	

  if($bool<>1) echo "There was an error editing the feedback.";
  if($bool==1) {

?>

<h2>Exam Feedback Sent!</h2>
<p>The updated feedback on the resume is sent to <?php echo $resume['firstname'] ?> <?php echo $resume['lastname'] ?> (<?php echo $resume['email'] ?>).</p>

<?

$SQL_resume="SELECT * FROM resume,resumefeedback WHERE resume.resume_id=" . $_GET['resume_id'] . " AND resumefeedback.resume_id=" . $_GET['resume_id'];
$resume_result=mysql_query($SQL_resume);
$resume=mysql_fetch_array($resume_result);

$firstname = $resume['firstname'];
$email = $resume['email'];
$filename = $resume['exam_evaluation'];

//$feedback = $resume['feedback'];
//$status = $resume['status'];
// random hash necessary to send mixed content
    $separator = md5(time());
	$eol = PHP_EOL;
	
	$filename = "http://www.informationmapping.com/imcc/resume/exam_evaluation/".$md_kort . "_" . $bestandsnaam;
	$attachment = chunk_split(base64_encode(file_get_contents($filename)));

	$message  = "<table cellpadding='0' cellspacing='0' border='0' width='100%'><tr><td align='center'>";
	$message .= "<table cellpadding='0' cellspacing='0' border='0' width='500'><tr><td align='left'>";
	$message .= "<p><img src='http://www.informationmapping.com/imcc/images/logo_email.gif' alt='Information Mapping logo' /></p>\n";
	$message .= "<p><font size='4' face='Arial, Helvetica, sans-serif' color=#009edf'>Information Mapping Certified Consultant Program</font></p>\n";
	$message .= "<p><font size='2' face='Arial, Helvetica, sans-serif'>Dear $firstname,</font></p>\n";
	$message .= "<p><font size='2' face='Arial, Helvetica, sans-serif'>You have received the following feedback to the Exam you submitted for the Information Mapping Certified Consultant Program:</font></p>\n";
	$message .= "<p><font size='2' face='Arial, Helvetica, sans-serif'>$feedback</font></p>";
	$message .= "<p><font size='2' face='Arial, Helvetica, sans-serif'>With kind regards,<br />";
	$message .= "Filip Vanlerberghe</font></p>";

$mime_boundary="==Multipart_Boundary_x".md5(mt_rand())."x";
	
	$headers = "From: Information Mapping International <info@informationmapping.com>\n";
	$headers .= "BCC: Filip Vanlerberghe <fvanlerberghe@informationmapping.com>\n";
	$headers .= "X-Priority: 1\n";  	  
	
	
	
	// als er geen bestand is geüpload hoeft code niet uitgevoerd te worden voor bijlage
	  if(!empty($bestandsnaam))
	  {
	    $headers .= "MIME-Version: 1.0".$eol; 
    $headers .= "Content-Type: multipart/mixed; boundary=\"".$separator."\"".$eol.$eol; 
    $headers .= "Content-Transfer-Encoding: 7bit".$eol;
    $headers .= "This is a MIME encoded message.".$eol.$eol;
	
	    // message
    $headers .= "--".$separator.$eol;
    $headers .= "Content-Type: text/html; charset=\"UTF-8\"".$eol;
    $headers .= "Content-Transfer-Encoding: 8bit".$eol.$eol;
    $headers .= $message.$eol.$eol;

    // attachment
    $headers .= "--".$separator.$eol;
    //$headers .= "Content-Type: application/octet-stream; name=\"".$filename."\"".$eol; 
	$headers .= "Content-Type: application/octet-stream; name=\"Evaluation.pdf\"".$eol; 
    $headers .= "Content-Transfer-Encoding: base64".$eol;
    $headers .= "Content-Disposition: attachment".$eol.$eol;
    $headers .= $attachment.$eol.$eol;
    $headers .= "--".$separator."--";
	  }
	  else
	  {
		  
		  
		$headers .= "Content-Type: multipart/mixed;\n" . " boundary=\"{$mime_boundary}\"\n" . "MIME-Version: 1.0\n";   
		  
		$message = "This is a multi-part message in MIME format.\n\n" .
         "--{$mime_boundary}\n" .
         "Content-Type: text/html; charset=\"iso-8859-1\"\n" .
         "Content-Transfer-Encoding: 7bit\n\n" . $message . "\n\n";
		 
    $message .="--{$mime_boundary}--\n";  
	  }
	
	$subject="IMCC Program - Exam feedback";

	

    mail($email,$subject, $message, $headers); 
}
}else{
?>
<!--<div id="admin">
<h2>Edit IMCC Exam Status</h2>-->
<?
$SQL_resume="SELECT * FROM resume,resumefeedback WHERE resume.resume_id=" . $_GET['resume_id'] . " AND resumefeedback.resume_id=" . $_GET['resume_id'];
$resume_result=mysql_query($SQL_resume);
$resume=mysql_fetch_array($resume_result);
?>
<?php echo
"<script language='JavaScript'>
myarr=new Array()
myarr[0]=''
myarr[1]='Congratulations! You passed your IMCC Exam. Now please go to the Information Mapping Web Store to pay the <a href=https://www.informationmapping.com/en/shop/certification/im-certified-consultant/imcc-license>License Fee</a> and become an Information Mapping Certified Consultant.<br /><br />We look forward to welcoming you as an IMCC!'
myarr[2]='<p>Unfortunately you did not pass the IMCC Exam. As a next step, we will grant you access to the following Information Mapping e-learning courses:</p><ul><li>Information Mapping Foundation</li><li>Mastering Successful Policies and Procedures</li></ul> Please go through these courses before taking the IMCC Exam again. When you have completed the training, you will need to purchase the IMCC Exam on the Information Mapping Web Store again. You can use discount code <strong>imcc_re</strong> to receive a 25% discount because this is your second try.<br /><br />Good luck!'
myarr[3]='You do not need to pass the IMCC Exam.'

function doIt(objval)
{
document.getElementById('feedback').innerHTML=myarr[objval]
}
</script>";
?>
<p class="buttons"><a href="javascript:history.back()" class="back">Back</a></p>

<form action="<?php echo $_SERVER['../crm/PHP_SELF']; ?>?resume_id=<?php echo $resume['resume_id'] ?>" method="post" enctype="multipart/form-data">
  <fieldset>
  	<input type="hidden" name="resume_id" value="<?php echo $resume['resume_id'] ?>">
 	<ol>
    <li><label for="status">Status</label>
    <select name="status" id="status" onChange="doIt(this.options[this.selectedIndex].value)">
    <option value="0">- Please select -</option>
    <option value="1" <? if($resume['exam_passed']=="1") { echo "selected";} ?>>Passed</option>
    <option value="2" <? if($resume['exam_passed']=="2") { echo "selected";} ?>>Not passed</option>
    <option value="3" <? if($resume['exam_passed']=="3") { echo "selected";} ?>>No exam needed</option>
    </select>
    <?php
	if($resume['exam_passed_date'] != '0000-00-00') { echo "License start date: " . $resume['exam_passed_date'] . " (yyyy-mm-dd)"; }
	?>
    </li>
    <li><label for="feedback">IMI Feedback</label>
	<textarea name="feedback" id="feedback" cols="50" rows="10" ></textarea>
    </li>
    <li>
    <input name="evaluation" type="file" /> (only PDF file, max. 5 MB)
    </li>
    <?php
	if(!empty($resume['exam_evaluation']))
	{
	?>
    <li>
    <a href="<?php echo $resume['../crm/exam_evaluation']; ?>" target="_blank">Download evaluation sheet</a></li>
    <?php } ?>
    </ol>
  </fieldset>
  <p>* If you save the status to "Passed", the current date will be added as the starting date for the IMCC license.</p>
  <fieldset class="submit">
    <input type="submit" name="submit" value="Send feedback" class="feedback">
  </fieldset>
</form>
<?
}
?>
<?php
}
?>
                                </div>
                            </div>
                            <!-- /block -->


</div>
</div>
</div>
</div>
            <hr>
<?php
include("../footer.php");
?>
</html>