<?php
$rootdir="../../";
$page="Resume";
include("../connect.php");
include("../header.php");
?>

<?php
$action = isset($_POST['action']) ? trim($_POST['action']) : '';
$action = isset($_GET['action']) ? trim($_GET['action']) : '';
//if(isset($_GET['action'])) $action=$_GET['action'];
//if(isset($_POST['action'])) $action=$_POST['action'];

if($action=="delete"){
  $resume_id=$_GET['resume_id'];
  	
  $resume_SQL_del="UPDATE resume SET deleted = '1' WHERE resume_id=$resume_id";
  $bool=mysql_query($resume_SQL_del);
  if($bool==1) echo "<div id='adminok'>The resume has been deleted.</div>";
  if($bool<>1) echo "<div id='adminnok'>An error occurred when trying to delete the resume.</div>";
}

$resume_SQL="SELECT * FROM resume WHERE program='Information Mapping Certified Consultant Program' && deleted = '0' ORDER BY resume_id DESC";
$resume_result=mysql_query($resume_SQL) or die(mysql_error()); 
?>
<body>
<?php
include("../topnav.php");
?>
<div class="container-fluid">
            <div class="row-fluid">
                
                <?php
				//include("../sidenav.php");
				?>
                
                <div class="span10" id="content">
                    <div class="row-fluid">
                    	<div class="span12">
                        
                            <!-- block -->
                            <div class="block">
                                <div class="navbar navbar-inner block-header">
                                <h3>IMCC Resumes &amp; Exams</h3>    
                                </div>
                                <div class="block-content collapse in">
<?php 
if (!isset($_SESSION["username"])){
	echo $niet_ingelogd;
}
// hieronder staan de functies die toegelaten worden tot deze pagina
elseif($_SESSION["function"] != "admin"){
	echo $geen_toegang;
}else{
?>

    
	<table class="table table-striped">
    <tr>
    <th>Name</th>
    <!--<th>Program</th>-->
    <th>Start license</th>
	<th>Resume</th>
    <th>Feedback</th>
	
    <th>Passed exam</th>
    <th>&nbsp;</th>
    </tr>
	<?php
	while($resume=mysql_fetch_array($resume_result)){
		if($resume['program']=="Information Mapping Certified Consultant Program") {
		$shortprogram="IMCC";
		}

		if($resume['program']=="Information Mapping Certified Professional Program") {
		$shortprogram="IMCP";
		}
		
		if($resume['program']=="Information Mapping Professional Program") {
		$shortprogram="IMP";
		}
	?>
    
	<tr>
    <td><?php echo $resume['firstname']; ?> <?php echo $resume['lastname']; ?></td>
    <!--<td><?php echo $shortprogram; ?></td>-->
    <td><?php 
	$lic_SQL="SELECT * FROM resumefeedback WHERE resume_id=" . $resume['resume_id'];
    $lic_result=mysql_query($lic_SQL) or die(mysql_error());
	$row_lic = mysql_fetch_assoc($lic_result);
	if($row_lic['exam_passed_date'] == "0000-00-00") { echo "unknown"; } else
	{
	echo $row_lic['exam_passed_date']; }
	
	 ?></td>
    <td width="50" align="center"><a href=resume_details.php?resume_id=<?php echo $resume['resume_id'] ?>><img src="../../images/view.png" alt="Details" /></a></td>
	<td width="50" align="center">
    <?
    $feedback_SQL="SELECT * FROM resumefeedback WHERE resume_id=" . $resume['resume_id'];
    $feedback_result=mysql_query($feedback_SQL) or die(mysql_error()); 
	$count = mysql_num_rows($feedback_result);
    if($count == 1) {
		while($resume2=mysql_fetch_array($feedback_result)){
			
			
			if ($resume2['status']=="2") {
				?>
				<a href="resume_editfeedback.php?resume_id=<?php echo $resume2['resume_id']; ?>"><img src="../images/ok.gif" alt="Approved" />
                <?
			}
			if ($resume2['status']=="3") {
				?>
            	<a href="resume_editfeedback.php?resume_id=<?php echo $resume2['resume_id']; ?>"><img src="../images/pending.gif" alt="Pending" />
                <?
            }
			if ($resume2['status']=="4") {
				?>
             	<a href="resume_editfeedback.php?resume_id=<?php echo $resume2['resume_id']; ?>"><img src="../images/nok.gif" alt="Denied" />
                <?
            }
			
			// save variable passed exam to use in the next table cell
			$passed = $resume2['exam_passed'];
			$passed_startdate = $resume2['exam_passed_date'];
		}
	} else {
	?>
    <a href="resume_feedback.php?resume_id=<?php echo $resume['resume_id']; ?>"><img src="../images/write.png" alt="Write" /></a>
    <?
	}
	?>
    </td>
    <td>
   <?php
   if($passed == '0') { 
   echo "<a href=\"resume_edit_examstatus.php?resume_id=".$resume['resume_id'] . "\">";
   echo "<img src=\"../images/pending.gif\" alt=\"No exam done\" />"; 
   echo "</a>";
   } 
   
   if($passed == '1') { 
   echo "<a href=\"resume_edit_examstatus.php?resume_id=".$resume['resume_id'] . "\">";
   echo "<img src=\"../images/ok.gif\" alt=\"Passed\" />"; 
   echo "</a>";
   } 
   
   if($passed == '2') { 
   echo "<a href=\"resume_edit_examstatus.php?resume_id=".$resume['resume_id'] . "\">";
   echo "<img src=\"../images/nok.gif\" alt=\"Not passed\" />"; 
   echo "</a>";
   }

   if($passed == '3') { 
   echo "<a href=\"resume_edit_examstatus.php?resume_id=".$resume['resume_id'] . "\">";
   echo "<img src=\"../images/ok.gif\" alt=\"Passed\" /> no exam needed"; 
   echo "</a>";
   }

   
   $result_a = mysql_query("SELECT * FROM resumefeedback AS f 
WHERE f.resume_id = '" . $resume['resume_id'] . "' && (f.exam_passed = '1' OR f.exam_passed = '3') && f.exam_passed_date < DATE_ADD(CURDATE(), INTERVAL -365 DAY) && f.exam_passed_date != '0000-00-00'"); // als de datum niet gekend is (0000-00-00) verscheen ook "license expired", dit mag niet, daarom opgenomen in query
$aant_a = mysql_num_rows($result_a);
if($aant_a > 0) {
   echo " license expired"; }
   else
   {
	   // not yet expired, but show notice when license will expire within 30 days

   $result_b = mysql_query("SELECT * FROM resumefeedback AS f 
WHERE f.resume_id = '" . $resume['resume_id'] . "' && (f.exam_passed = '1' OR f.exam_passed = '3') && 
CURDATE() > DATE_ADD(f.exam_passed_date, INTERVAL +335 DAY)"); // show licenses that will expire in one month
$aant_b = mysql_num_rows($result_b);
if($aant_b > 0) {
   echo "<span style=\"color: red;\"> - expiring soon</span>"; } 
   
   }
   ?>
    </td>
    <td width="50" align="center"><a href="resume_list.php?resume_id=<?php echo $resume['resume_id'] ?>&action=delete" onClick="return confirm('Are you sure you want to delete this resume?')"><img src="../../images/delete.jpg" alt="Delete" /></a></td>
  </tr>

	<?php
	}
	?>          
 	</table>
<?php
}
?>
                                </div>
                            </div>
                            <!-- /block -->


</div>
</div>
</div>
</div>
            <hr>
<?php
include("../footer.php");
?>
</html>