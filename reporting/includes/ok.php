<?php
// als sessie verlopen is, moeten ze opnieuw inloggen
if (!isset($_SESSION["partner_email"])){
    echo "<p>Your login session has been expired. Please <a href=\"index.php\">login</a> with a valid username and password.</p>";
}else
{
    $report_SQL_insert="INSERT INTO reporting_report (partner_id,company,first,last,email,month,year,course1,course1_start,course1_dur,course1_part,course1_type,course1_code,course2,course2_start,course2_dur,course2_part,course2_type,course2_code,course3,course3_start,course3_dur,course3_part,course3_type,course3_code,course4,course4_start,course4_dur,course4_part,course4_type,course4_code,course5,course5_start,course5_dur,course5_part,course5_type,course5_code,course6,course6_start,course6_dur,course6_part,course6_type,course6_code,course7,course7_start,course7_dur,course7_part,course7_type,course7_code,course8,course8_start,course8_dur,course8_part,course8_type,course8_code,course9,course9_start,course9_dur,course9_part,course9_type,course9_code,course10,course10_start,course10_dur,course10_part,course10_type,course10_code,consult,writing,cftraining,cfservices,comments,course1_rev,course2_rev,course3_rev,course4_rev,course5_rev,course6_rev,course7_rev,course8_rev,course9_rev,course10_rev,course11,course11_start,course11_dur,course11_part,course11_type,course11_code,course11_rev,course12,course12_start,course12_dur,course12_part,course12_type,course12_code,course12_rev,course13,course13_start,course13_dur,course13_part,course13_type,course13_code,course13_rev,course14,course14_start,course14_dur,course14_part,course14_type,course14_code,course14_rev,course15,course15_start,course15_dur,course15_part,course15_type,course15_code,course15_rev,publimap_rev,imapper_rev, submit_time) VALUES ('$partner_id','$company','$first','$last','$email','$month','$year','$course1','$course1_start','$course1_dur','$course1_part','$course1_type','$course1_code','$course2','$course2_start','$course2_dur','$course2_part','$course2_type','$course2_code','$course3','$course3_start','$course3_dur','$course3_part','$course3_type','$course3_code','$course4','$course4_start','$course4_dur','$course4_part','$course4_type','$course4_code','$course5','$course5_start','$course5_dur','$course5_part','$course5_type','$course5_code','$course6','$course6_start','$course6_dur','$course6_part','$course6_type','$course6_code','$course7','$course7_start','$course7_dur','$course7_part','$course7_type','$course7_code','$course8','$course8_start','$course8_dur','$course8_part','$course8_type','$course8_code','$course9','$course9_start','$course9_dur','$course9_part','$course9_type','$course9_code','$course10','$course10_start','$course10_dur','$course10_part','$course10_type','$course10_code','$consult','$writing','$cftraining','$cfservices','$comments','$course1_rev','$course2_rev','$course3_rev','$course4_rev','$course5_rev','$course6_rev','$course7_rev','$course8_rev','$course9_rev','$course10_rev','$course11','$course11_start','$course11_dur','$course11_part','$course11_type','$course11_code','$course11_rev','$course12','$course12_start','$course12_dur','$course12_part','$course12_type','$course12_code','$course12_rev','$course13','$course13_start','$course13_dur','$course13_part','$course13_type','$course13_code','$course13_rev','$course14','$course14_start','$course14_dur','$course14_part','$course14_type','$course14_code','$course14_rev','$course15','$course15_start','$course15_dur','$course15_part','$course15_type','$course15_code','$course15_rev','$publimap_rev','$imapper_rev', NOW())";

    $bool=mysql_query($report_SQL_insert);

    if($bool == false)
    {
        $feedback = "<div id='report'><h3>Report not sent</h3><p>An error occurred when trying to submit the report. Please try again.</p></div>";
        echo $feedback;
    }
    if($bool == true) {
        $idvanreports = mysql_insert_id();
// hierna extra code om oude historiek ook te laten werken in reports, hier gaan we de cursusnaam opzoeken in de tabel en de juiste id van de cursusnaam opvragen om deze id te kunnen gebruiken in de tbl participants

// per cursus die ingevuld is een record toevoegen in de participants table, met verwijzing naar de report table
        if($course1 != "- Select -")
        {
            $res_course_id = mysql_query("SELECT * FROM reporting_courses WHERE course_name ='$course1'");
            while($course_id=mysql_fetch_array($res_course_id))
            {
                $course_res = $course_id['course_id'];
            }
            $participants_result = mysql_query("INSERT INTO reporting_participants (partner_id, month, year, course_id, participant_amount, report_id, clp_number) VALUES ('$partner_id', '$month', '$year','$course_res', '$course1_part', '$idvanreports', '$course1_code')"); // of met LAST_INSERT_ID() en met switch case
        }

        if($course2 != "- Select -")
        {
            $res_course_id = mysql_query("SELECT * FROM reporting_courses WHERE course_name ='$course2'");
            while($course_id=mysql_fetch_array($res_course_id))
            {
                $course_res = $course_id['course_id'];
            }
            $participants_result = mysql_query("INSERT INTO reporting_participants (partner_id, month, year, course_id, participant_amount, report_id, clp_number) VALUES ('$partner_id', '$month', '$year','$course_res', '$course2_part', '$idvanreports', '$course2_code')");
        }

        if($course3 != "- Select -")
        {
            $res_course_id = mysql_query("SELECT * FROM reporting_courses WHERE course_name ='$course3'");
            while($course_id=mysql_fetch_array($res_course_id))
            {
                $course_res = $course_id['course_id'];
            }
            $participants_result = mysql_query("INSERT INTO reporting_participants (partner_id, month, year, course_id, participant_amount,report_id, clp_number) VALUES ('$partner_id', '$month', '$year','$course_res', '$course3_part', '$idvanreports', '$course3_code')");
        }

        if($course4 != "- Select -")
        {
            $res_course_id = mysql_query("SELECT * FROM reporting_courses WHERE course_name ='$course4'");
            while($course_id=mysql_fetch_array($res_course_id))
            {
                $course_res = $course_id['course_id'];
            }
            $participants_result = mysql_query("INSERT INTO reporting_participants (partner_id, month, year, course_id, participant_amount,report_id, clp_number) VALUES ('$partner_id', '$month', '$year','$course_res', '$course4_part', '$idvanreports', '$course4_code')");
        }

        if($course5 != "- Select -")
        {
            $res_course_id = mysql_query("SELECT * FROM reporting_courses WHERE course_name ='$course5'");
            while($course_id=mysql_fetch_array($res_course_id))
            {
                $course_res = $course_id['course_id'];
            }
            $participants_result = mysql_query("INSERT INTO reporting_participants (partner_id, month, year, course_id, participant_amount,report_id, clp_number) VALUES ('$partner_id', '$month', '$year','$course_res', '$course5_part', '$idvanreports', '$course5_code')");
        }

        if($course6 != "- Select -")
        {
            $res_course_id = mysql_query("SELECT * FROM reporting_courses WHERE course_name ='$course6'");
            while($course_id=mysql_fetch_array($res_course_id))
            {
                $course_res = $course_id['course_id'];
            }
            $participants_result = mysql_query("INSERT INTO reporting_participants (partner_id, month, year, course_id, participant_amount,report_id, clp_number) VALUES ('$partner_id', '$month', '$year','$course_res', '$course6_part', '$idvanreports', '$course6_code')");
        }

        if($course7 != "- Select -")
        {
            $res_course_id = mysql_query("SELECT * FROM reporting_courses WHERE course_name ='$course7'");
            while($course_id=mysql_fetch_array($res_course_id))
            {
                $course_res = $course_id['course_id'];
            }
            $participants_result = mysql_query("INSERT INTO reporting_participants (partner_id, month, year, course_id, participant_amount,report_id, clp_number) VALUES ('$partner_id', '$month', '$year','$course_res', '$course7_part', '$idvanreports', '$course7_code')");
        }

        if($course8 != "- Select -")
        {
            $res_course_id = mysql_query("SELECT * FROM reporting_courses WHERE course_name ='$course8'");
            while($course_id=mysql_fetch_array($res_course_id))
            {
                $course_res = $course_id['course_id'];
            }
            $participants_result = mysql_query("INSERT INTO reporting_participants (partner_id, month, year, course_id, participant_amount,report_id, clp_number) VALUES ('$partner_id', '$month', '$year','$course_res', '$course8_part', '$idvanreports', '$course8_code')");
        }

        if($course9 != "- Select -")
        {
            $res_course_id = mysql_query("SELECT * FROM reporting_courses WHERE course_name ='$course9'");
            while($course_id=mysql_fetch_array($res_course_id))
            {
                $course_res = $course_id['course_id'];
            }
            $participants_result = mysql_query("INSERT INTO reporting_participants (partner_id, month, year, course_id, participant_amount,report_id, clp_number) VALUES ('$partner_id', '$month', '$year','$course_res', '$course9_part', '$idvanreports', '$course9_code')");
        }

        if($course10 != "- Select -")
        {
            $res_course_id = mysql_query("SELECT * FROM reporting_courses WHERE course_name ='$course10'");
            while($course_id=mysql_fetch_array($res_course_id))
            {
                $course_res = $course_id['course_id'];
            }
            $participants_result = mysql_query("INSERT INTO reporting_participants (partner_id, month, year, course_id, participant_amount,report_id, clp_number) VALUES ('$partner_id', '$month', '$year','$course_res', '$course10_part', '$idvanreports', '$course10_code')");
        }

        if($course11 != "- Select -")
        {
            $res_course_id = mysql_query("SELECT * FROM reporting_courses WHERE course_name ='$course11'");
            while($course_id=mysql_fetch_array($res_course_id))
            {
                $course_res = $course_id['course_id'];
            }
            $participants_result = mysql_query("INSERT INTO reporting_participants (partner_id, month, year, course_id, participant_amount,report_id, clp_number) VALUES ('$partner_id', '$month', '$year','$course_res', '$course11_part', '$idvanreports', '$course11_code')");
        }

        if($course12 != "- Select -")
        {
            $res_course_id = mysql_query("SELECT * FROM reporting_courses WHERE course_name ='$course12'");
            while($course_id=mysql_fetch_array($res_course_id))
            {
                $course_res = $course_id['course_id'];
            }
            $participants_result = mysql_query("INSERT INTO reporting_participants (partner_id, month, year, course_id, participant_amount,report_id, clp_number) VALUES ('$partner_id', '$month', '$year','$course_res', '$course12_part', '$idvanreports', '$course12_code')");
        }

        if($course13 != "- Select -")
        {
            $res_course_id = mysql_query("SELECT * FROM reporting_courses WHERE course_name ='$course13'");
            while($course_id=mysql_fetch_array($res_course_id))
            {
                $course_res = $course_id['course_id'];
            }
            $participants_result = mysql_query("INSERT INTO reporting_participants (partner_id, month, year, course_id, participant_amount,report_id, clp_number) VALUES ('$partner_id', '$month', '$year','$course_res', '$course13_part', '$idvanreports', '$course13_code')");
        }

        if($course14 != "- Select -")
        {
            $res_course_id = mysql_query("SELECT * FROM reporting_courses WHERE course_name ='$course14'");
            while($course_id=mysql_fetch_array($res_course_id))
            {
                $course_res = $course_id['course_id'];
            }
            $participants_result = mysql_query("INSERT INTO reporting_participants (partner_id, month, year, course_id, participant_amount,report_id, clp_number) VALUES ('$partner_id', '$month', '$year','$course_res', '$course14_part', '$idvanreports', '$course14_code')");
        }

        if($course15 != "- Select -")
        {
            $res_course_id = mysql_query("SELECT * FROM reporting_courses WHERE course_name ='$course15'");
            while($course_id=mysql_fetch_array($res_course_id))
            {
                $course_res = $course_id['course_id'];
            }
            $participants_result = mysql_query("INSERT INTO reporting_participants (partner_id, month, year, course_id, participant_amount,report_id, clp_number) VALUES ('$partner_id', '$month', '$year','$course_res', '$course15_part', '$idvanreports', '$course15_code')");
        }

// uitlezen van company name op basis van company id
        $partner_SQL="SELECT * FROM reporting_partners WHERE partner_id=$partner_id";
        $partner_result=mysql_query($partner_SQL);
        while($partner=mysql_fetch_array($partner_result)){
            $company=$partner['partner_company'];
        }
// indien corporate CLP werd ingevuld, naam van de eindklant mee afdrukken in mailtje
// in plaats van deze code 15 keer te kopiëren voor elk veld, heb ik dit in een functie gestopt
        function display_endcustomer($code)
        {

            if(!empty($code)) {
                $result_clp = mysql_query("SELECT * FROM reporting_clp_number WHERE clp_number = '$code'");
                $row_result_clp = mysql_fetch_assoc($result_clp);
                $clp_id = $row_result_clp['clp_number_id'];

                $res_clp = mysql_query("SELECT * FROM reporting_clp WHERE clp_number_id = '$clp_id'");
                $row_res_clp = mysql_fetch_assoc($res_clp);
                $endcustomer = $row_res_clp['company'];

                return " (" . $endcustomer . ")";

            }

        }

        $message     = "<html>
<meta http-equiv=\"Content-Type\" content=\"text/html charset=UTF-8\" />
  <body>
  <h1><font size='4' face='Arial, Helvetica, sans-serif' color='#135284'>Partner Report from " . $_SESSION['partner_company'] . "</font></h1>
  <p><font size='2' face='Arial, Helvetica, sans-serif'><strong>Period:</strong> $month $year</font></p>
  <p><font size='2' face='Arial, Helvetica, sans-serif'><strong>Report submitted by:</strong> $first $last</font></p>
  <h2><font size='3' face='Arial, Helvetica, sans-serif' color='#5D8EB4'>Courses</font></h2>
  <table cellpadding=\"0\" cellspacing=\"0\" width=\"100%\" style=\"border-left: 1px solid #666666;border-top: 1px solid #666666;\">
  <tr>
  <td style=\"border-right: 1px solid #666666;border-bottom: 1px solid #666666;padding:2px 4px;\" ><font size='2' face='Arial, Helvetica, sans-serif'><strong>Course</strong></font></td>
  <td style=\"border-right: 1px solid #666666;border-bottom: 1px solid #666666;padding:2px 4px;\" ><font size='2' face='Arial, Helvetica, sans-serif'><strong>Start date</strong></font></td>
  <td style=\"border-right: 1px solid #666666;border-bottom: 1px solid #666666;padding:2px 4px;\" ><font size='2' face='Arial, Helvetica, sans-serif'><strong>Duration</strong></font></td>
  <td style=\"border-right: 1px solid #666666;border-bottom: 1px solid #666666;padding:2px 4px;\" ><font size='2' face='Arial, Helvetica, sans-serif'><strong>Participants</strong></font></td>";

        if(($_SESSION['rev_vis'])=='yes') {

            $message     .= "
  <td style=\"border-right: 1px solid #666666;border-bottom: 1px solid #666666;padding:2px 4px;\" ><font size='2' face='Arial, Helvetica, sans-serif'><strong>Client revenue</strong></font></td>";

        }

        $message     .= "
  <td style=\"border-right: 1px solid #666666;border-bottom: 1px solid #666666;padding:2px 4px;\" ><font size='2' face='Arial, Helvetica, sans-serif'><strong>Type</strong></font></td>
  <td style=\"border-right: 1px solid #666666;border-bottom: 1px solid #666666;padding:2px 4px;\" ><font size='2' face='Arial, Helvetica, sans-serif'><strong>CLP ID</strong></font></td>
  </tr>
  <tr>
  <td style=\"border-right: 1px solid #666666;border-bottom: 1px solid #666666;padding:2px 4px;\"><font size='2' face='Arial, Helvetica, sans-serif'>$course1</font></td>
  <td style=\"border-right: 1px solid #666666;border-bottom: 1px solid #666666;padding:2px 4px;\"><font size='2' face='Arial, Helvetica, sans-serif'>$course1_start</font></td>
  <td style=\"border-right: 1px solid #666666;border-bottom: 1px solid #666666;padding:2px 4px;\"><font size='2' face='Arial, Helvetica, sans-serif'>$course1_dur</font></td>
  <td style=\"border-right: 1px solid #666666;border-bottom: 1px solid #666666;padding:2px 4px;\"><font size='2' face='Arial, Helvetica, sans-serif'>$course1_part</font></td>";

        if(($_SESSION['rev_vis'])=='yes') {

            $message     .= "
  <td style=\"border-right: 1px solid #666666;border-bottom: 1px solid #666666;padding:2px 4px;\"><font size='2' face='Arial, Helvetica, sans-serif'>&euro; $course1_rev</font></td>";

        }

        $message     .= "
  <td style=\"border-right: 1px solid #666666;border-bottom: 1px solid #666666;padding:2px 4px;\"><font size='2' face='Arial, Helvetica, sans-serif'>$course1_type</font></td>
  <td style=\"border-right: 1px solid #666666;border-bottom: 1px solid #666666;padding:2px 4px;\"><font size='2' face='Arial, Helvetica, sans-serif'>$course1_code" . display_endcustomer($course1_code) . "</font></td>
  </tr>
   <tr>
  <td style=\"border-right: 1px solid #666666;border-bottom: 1px solid #666666;padding:2px 4px;\"><font size='2' face='Arial, Helvetica, sans-serif'>$course2</font></td>
  <td style=\"border-right: 1px solid #666666;border-bottom: 1px solid #666666;padding:2px 4px;\"><font size='2' face='Arial, Helvetica, sans-serif'>$course2_start</font></td>
  <td style=\"border-right: 1px solid #666666;border-bottom: 1px solid #666666;padding:2px 4px;\"><font size='2' face='Arial, Helvetica, sans-serif'>$course2_dur</font></td>
  <td style=\"border-right: 1px solid #666666;border-bottom: 1px solid #666666;padding:2px 4px;\"><font size='2' face='Arial, Helvetica, sans-serif'>$course2_part</font></td>";

        if(($_SESSION['rev_vis'])=='yes') {

            $message     .= "
  <td style=\"border-right: 1px solid #666666;border-bottom: 1px solid #666666;padding:2px 4px;\"><font size='2' face='Arial, Helvetica, sans-serif'>&euro; $course2_rev</font></td>";

        }

        $message     .= "
  <td style=\"border-right: 1px solid #666666;border-bottom: 1px solid #666666;padding:2px 4px;\"><font size='2' face='Arial, Helvetica, sans-serif'>$course2_type</font></td>
  <td style=\"border-right: 1px solid #666666;border-bottom: 1px solid #666666;padding:2px 4px;\"><font size='2' face='Arial, Helvetica, sans-serif'>$course2_code" . display_endcustomer($course2_code) . "</font></td>
  </tr>
    <tr>
  <td style=\"border-right: 1px solid #666666;border-bottom: 1px solid #666666;padding:2px 4px;\"><font size='2' face='Arial, Helvetica, sans-serif'>$course3</font></td>
  <td style=\"border-right: 1px solid #666666;border-bottom: 1px solid #666666;padding:2px 4px;\"><font size='2' face='Arial, Helvetica, sans-serif'>$course3_start</font></td>
  <td style=\"border-right: 1px solid #666666;border-bottom: 1px solid #666666;padding:2px 4px;\"><font size='2' face='Arial, Helvetica, sans-serif'>$course3_dur</font></td>
  <td style=\"border-right: 1px solid #666666;border-bottom: 1px solid #666666;padding:2px 4px;\"><font size='2' face='Arial, Helvetica, sans-serif'>$course3_part</font></td>";

        if(($_SESSION['rev_vis'])=='yes') {

            $message     .= "
  <td style=\"border-right: 1px solid #666666;border-bottom: 1px solid #666666;padding:2px 4px;\"><font size='2' face='Arial, Helvetica, sans-serif'>&euro; $course3_rev</font></td>";

        }

        $message     .= "
  <td style=\"border-right: 1px solid #666666;border-bottom: 1px solid #666666;padding:2px 4px;\"><font size='2' face='Arial, Helvetica, sans-serif'>$course3_type</font></td>
  <td style=\"border-right: 1px solid #666666;border-bottom: 1px solid #666666;padding:2px 4px;\"><font size='2' face='Arial, Helvetica, sans-serif'>$course3_code" . display_endcustomer($course3_code) . "</font></td>
  </tr>
    <tr>
  <td style=\"border-right: 1px solid #666666;border-bottom: 1px solid #666666;padding:2px 4px;\"><font size='2' face='Arial, Helvetica, sans-serif'>$course4</font></td>
  <td style=\"border-right: 1px solid #666666;border-bottom: 1px solid #666666;padding:2px 4px;\"><font size='2' face='Arial, Helvetica, sans-serif'>$course4_start</font></td>
  <td style=\"border-right: 1px solid #666666;border-bottom: 1px solid #666666;padding:2px 4px;\"><font size='2' face='Arial, Helvetica, sans-serif'>$course4_dur</font></td>
  <td style=\"border-right: 1px solid #666666;border-bottom: 1px solid #666666;padding:2px 4px;\"><font size='2' face='Arial, Helvetica, sans-serif'>$course4_part</font></td>";

        if(($_SESSION['rev_vis'])=='yes') {

            $message     .= "
  <td style=\"border-right: 1px solid #666666;border-bottom: 1px solid #666666;padding:2px 4px;\"><font size='2' face='Arial, Helvetica, sans-serif'>&euro; $course4_rev</font></td>";

        }

        $message     .= "
  <td style=\"border-right: 1px solid #666666;border-bottom: 1px solid #666666;padding:2px 4px;\"><font size='2' face='Arial, Helvetica, sans-serif'>$course4_type</font></td>
  <td style=\"border-right: 1px solid #666666;border-bottom: 1px solid #666666;padding:2px 4px;\"><font size='2' face='Arial, Helvetica, sans-serif'>$course4_code" . display_endcustomer($course4_code) . "</font></td>
  </tr>
    <tr>
  <td style=\"border-right: 1px solid #666666;border-bottom: 1px solid #666666;padding:2px 4px;\"><font size='2' face='Arial, Helvetica, sans-serif'>$course5</font></td>
  <td style=\"border-right: 1px solid #666666;border-bottom: 1px solid #666666;padding:2px 4px;\"><font size='2' face='Arial, Helvetica, sans-serif'>$course5_start</font></td>
  <td style=\"border-right: 1px solid #666666;border-bottom: 1px solid #666666;padding:2px 4px;\"><font size='2' face='Arial, Helvetica, sans-serif'>$course5_dur</font></td>
  <td style=\"border-right: 1px solid #666666;border-bottom: 1px solid #666666;padding:2px 4px;\"><font size='2' face='Arial, Helvetica, sans-serif'>$course5_part</font></td>";

        if(($_SESSION['rev_vis'])=='yes') {

            $message     .= "
  <td style=\"border-right: 1px solid #666666;border-bottom: 1px solid #666666;padding:2px 4px;\"><font size='2' face='Arial, Helvetica, sans-serif'>&euro; $course5_rev</font></td>";

        }

        $message     .= "
  <td style=\"border-right: 1px solid #666666;border-bottom: 1px solid #666666;padding:2px 4px;\"><font size='2' face='Arial, Helvetica, sans-serif'>$course5_type</font></td>
  <td style=\"border-right: 1px solid #666666;border-bottom: 1px solid #666666;padding:2px 4px;\"><font size='2' face='Arial, Helvetica, sans-serif'>$course5_code" . display_endcustomer($course5_code) . "</font></td>
  </tr>
    <tr>
  <td style=\"border-right: 1px solid #666666;border-bottom: 1px solid #666666;padding:2px 4px;\"><font size='2' face='Arial, Helvetica, sans-serif'>$course6</font></td>
  <td style=\"border-right: 1px solid #666666;border-bottom: 1px solid #666666;padding:2px 4px;\"><font size='2' face='Arial, Helvetica, sans-serif'>$course6_start</font></td>
  <td style=\"border-right: 1px solid #666666;border-bottom: 1px solid #666666;padding:2px 4px;\"><font size='2' face='Arial, Helvetica, sans-serif'>$course6_dur</font></td>
  <td style=\"border-right: 1px solid #666666;border-bottom: 1px solid #666666;padding:2px 4px;\"><font size='2' face='Arial, Helvetica, sans-serif'>$course6_part</font></td>";

        if(($_SESSION['rev_vis'])=='yes') {

            $message     .= "
  <td style=\"border-right: 1px solid #666666;border-bottom: 1px solid #666666;padding:2px 4px;\"><font size='2' face='Arial, Helvetica, sans-serif'>&euro; $course6_rev</font></td>";

        }

        $message     .= "
  <td style=\"border-right: 1px solid #666666;border-bottom: 1px solid #666666;padding:2px 4px;\"><font size='2' face='Arial, Helvetica, sans-serif'>$course6_type</font></td>
  <td style=\"border-right: 1px solid #666666;border-bottom: 1px solid #666666;padding:2px 4px;\"><font size='2' face='Arial, Helvetica, sans-serif'>$course6_code" . display_endcustomer($course6_code) . "</font></td>
  </tr>
    <tr>
  <td style=\"border-right: 1px solid #666666;border-bottom: 1px solid #666666;padding:2px 4px;\"><font size='2' face='Arial, Helvetica, sans-serif'>$course7</font></td>
  <td style=\"border-right: 1px solid #666666;border-bottom: 1px solid #666666;padding:2px 4px;\"><font size='2' face='Arial, Helvetica, sans-serif'>$course7_start</font></td>
  <td style=\"border-right: 1px solid #666666;border-bottom: 1px solid #666666;padding:2px 4px;\"><font size='2' face='Arial, Helvetica, sans-serif'>$course7_dur</font></td>
  <td style=\"border-right: 1px solid #666666;border-bottom: 1px solid #666666;padding:2px 4px;\"><font size='2' face='Arial, Helvetica, sans-serif'>$course7_part</font></td>";

        if(($_SESSION['rev_vis'])=='yes') {

            $message     .= "
  <td style=\"border-right: 1px solid #666666;border-bottom: 1px solid #666666;padding:2px 4px;\"><font size='2' face='Arial, Helvetica, sans-serif'>&euro; $course7_rev</font></td>";

        }

        $message     .= "
  <td style=\"border-right: 1px solid #666666;border-bottom: 1px solid #666666;padding:2px 4px;\"><font size='2' face='Arial, Helvetica, sans-serif'>$course7_type</font></td>
  <td style=\"border-right: 1px solid #666666;border-bottom: 1px solid #666666;padding:2px 4px;\"><font size='2' face='Arial, Helvetica, sans-serif'>$course7_code" . display_endcustomer($course7_code) . "</font></td>
  </tr>
    <tr>
  <td style=\"border-right: 1px solid #666666;border-bottom: 1px solid #666666;padding:2px 4px;\"><font size='2' face='Arial, Helvetica, sans-serif'>$course8</font></td>
  <td style=\"border-right: 1px solid #666666;border-bottom: 1px solid #666666;padding:2px 4px;\"><font size='2' face='Arial, Helvetica, sans-serif'>$course8_start</font></td>
  <td style=\"border-right: 1px solid #666666;border-bottom: 1px solid #666666;padding:2px 4px;\"><font size='2' face='Arial, Helvetica, sans-serif'>$course8_dur</font></td>
  <td style=\"border-right: 1px solid #666666;border-bottom: 1px solid #666666;padding:2px 4px;\"><font size='2' face='Arial, Helvetica, sans-serif'>$course8_part</font></td>";

        if(($_SESSION['rev_vis'])=='yes') {

            $message     .= "
  <td style=\"border-right: 1px solid #666666;border-bottom: 1px solid #666666;padding:2px 4px;\"><font size='2' face='Arial, Helvetica, sans-serif'>&euro; $course8_rev</font></td>";

        }

        $message     .= "
  <td style=\"border-right: 1px solid #666666;border-bottom: 1px solid #666666;padding:2px 4px;\"><font size='2' face='Arial, Helvetica, sans-serif'>$course8_type</font></td>
  <td style=\"border-right: 1px solid #666666;border-bottom: 1px solid #666666;padding:2px 4px;\"><font size='2' face='Arial, Helvetica, sans-serif'>$course8_code" . display_endcustomer($course8_code) . "</font></td>
  </tr>
    <tr>
  <td style=\"border-right: 1px solid #666666;border-bottom: 1px solid #666666;padding:2px 4px;\"><font size='2' face='Arial, Helvetica, sans-serif'>$course9</font></td>
  <td style=\"border-right: 1px solid #666666;border-bottom: 1px solid #666666;padding:2px 4px;\"><font size='2' face='Arial, Helvetica, sans-serif'>$course9_start</font></td>
  <td style=\"border-right: 1px solid #666666;border-bottom: 1px solid #666666;padding:2px 4px;\"><font size='2' face='Arial, Helvetica, sans-serif'>$course9_dur</font></td>
  <td style=\"border-right: 1px solid #666666;border-bottom: 1px solid #666666;padding:2px 4px;\"><font size='2' face='Arial, Helvetica, sans-serif'>$course9_part</font></td>";

        if(($_SESSION['rev_vis'])=='yes') {

            $message     .= "
  <td style=\"border-right: 1px solid #666666;border-bottom: 1px solid #666666;padding:2px 4px;\"><font size='2' face='Arial, Helvetica, sans-serif'>&euro; $course9_rev</font></td>";

        }

        $message     .= "
  <td style=\"border-right: 1px solid #666666;border-bottom: 1px solid #666666;padding:2px 4px;\"><font size='2' face='Arial, Helvetica, sans-serif'>$course9_type</font></td>
  <td style=\"border-right: 1px solid #666666;border-bottom: 1px solid #666666;padding:2px 4px;\"><font size='2' face='Arial, Helvetica, sans-serif'>$course9_code" . display_endcustomer($course9_code) . "</font></td>
  </tr>
    <tr>
  <td style=\"border-right: 1px solid #666666;border-bottom: 1px solid #666666;padding:2px 4px;\"><font size='2' face='Arial, Helvetica, sans-serif'>$course10</font></td>
  <td style=\"border-right: 1px solid #666666;border-bottom: 1px solid #666666;padding:2px 4px;\"><font size='2' face='Arial, Helvetica, sans-serif'>$course10_start</font></td>
  <td style=\"border-right: 1px solid #666666;border-bottom: 1px solid #666666;padding:2px 4px;\"><font size='2' face='Arial, Helvetica, sans-serif'>$course10_dur</font></td>
  <td style=\"border-right: 1px solid #666666;border-bottom: 1px solid #666666;padding:2px 4px;\"><font size='2' face='Arial, Helvetica, sans-serif'>$course10_part</font></td>";

        if(($_SESSION['rev_vis'])=='yes') {

            $message     .= "
  <td style=\"border-right: 1px solid #666666;border-bottom: 1px solid #666666;padding:2px 4px;\"><font size='2' face='Arial, Helvetica, sans-serif'>&euro; $course10_rev</font></td>";

        }

        $message     .= "
  <td style=\"border-right: 1px solid #666666;border-bottom: 1px solid #666666;padding:2px 4px;\"><font size='2' face='Arial, Helvetica, sans-serif'>$course10_type</font></td>
  <td style=\"border-right: 1px solid #666666;border-bottom: 1px solid #666666;padding:2px 4px;\"><font size='2' face='Arial, Helvetica, sans-serif'>$course10_code" . display_endcustomer($course10_code) . "</font></td>
  </tr>

   <tr>
  <td style=\"border-right: 1px solid #666666;border-bottom: 1px solid #666666;padding:2px 4px;\"><font size='2' face='Arial, Helvetica, sans-serif'>$course11</font></td>
  <td style=\"border-right: 1px solid #666666;border-bottom: 1px solid #666666;padding:2px 4px;\"><font size='2' face='Arial, Helvetica, sans-serif'>$course11_start</font></td>
  <td style=\"border-right: 1px solid #666666;border-bottom: 1px solid #666666;padding:2px 4px;\"><font size='2' face='Arial, Helvetica, sans-serif'>$course11_dur</font></td>
  <td style=\"border-right: 1px solid #666666;border-bottom: 1px solid #666666;padding:2px 4px;\"><font size='2' face='Arial, Helvetica, sans-serif'>$course11_part</font></td>";

        if(($_SESSION['rev_vis'])=='yes') {

            $message     .= "
  <td style=\"border-right: 1px solid #666666;border-bottom: 1px solid #666666;padding:2px 4px;\"><font size='2' face='Arial, Helvetica, sans-serif'>&euro; $course11_rev</font></td>";

        }

        $message     .= "
  <td style=\"border-right: 1px solid #666666;border-bottom: 1px solid #666666;padding:2px 4px;\"><font size='2' face='Arial, Helvetica, sans-serif'>$course11_type</font></td>
  <td style=\"border-right: 1px solid #666666;border-bottom: 1px solid #666666;padding:2px 4px;\"><font size='2' face='Arial, Helvetica, sans-serif'>$course11_code" . display_endcustomer($course11_code) . "</font></td>
  </tr>

   <tr>
  <td style=\"border-right: 1px solid #666666;border-bottom: 1px solid #666666;padding:2px 4px;\"><font size='2' face='Arial, Helvetica, sans-serif'>$course12</font></td>
  <td style=\"border-right: 1px solid #666666;border-bottom: 1px solid #666666;padding:2px 4px;\"><font size='2' face='Arial, Helvetica, sans-serif'>$course12_start</font></td>
  <td style=\"border-right: 1px solid #666666;border-bottom: 1px solid #666666;padding:2px 4px;\"><font size='2' face='Arial, Helvetica, sans-serif'>$course12_dur</font></td>
  <td style=\"border-right: 1px solid #666666;border-bottom: 1px solid #666666;padding:2px 4px;\"><font size='2' face='Arial, Helvetica, sans-serif'>$course12_part</font></td>";

        if(($_SESSION['rev_vis'])=='yes') {

            $message     .= "
  <td style=\"border-right: 1px solid #666666;border-bottom: 1px solid #666666;padding:2px 4px;\"><font size='2' face='Arial, Helvetica, sans-serif'>&euro; $course12_rev</font></td>";

        }

        $message     .= "
  <td style=\"border-right: 1px solid #666666;border-bottom: 1px solid #666666;padding:2px 4px;\"><font size='2' face='Arial, Helvetica, sans-serif'>$course12_type</font></td>
  <td style=\"border-right: 1px solid #666666;border-bottom: 1px solid #666666;padding:2px 4px;\"><font size='2' face='Arial, Helvetica, sans-serif'>$course12_code" . display_endcustomer($course12_code) . "</font></td>
  </tr>

   <tr>
  <td style=\"border-right: 1px solid #666666;border-bottom: 1px solid #666666;padding:2px 4px;\"><font size='2' face='Arial, Helvetica, sans-serif'>$course13</font></td>
  <td style=\"border-right: 1px solid #666666;border-bottom: 1px solid #666666;padding:2px 4px;\"><font size='2' face='Arial, Helvetica, sans-serif'>$course13_start</font></td>
  <td style=\"border-right: 1px solid #666666;border-bottom: 1px solid #666666;padding:2px 4px;\"><font size='2' face='Arial, Helvetica, sans-serif'>$course13_dur</font></td>
  <td style=\"border-right: 1px solid #666666;border-bottom: 1px solid #666666;padding:2px 4px;\"><font size='2' face='Arial, Helvetica, sans-serif'>$course13_part</font></td>";

        if(($_SESSION['rev_vis'])=='yes') {

            $message     .= "
  <td style=\"border-right: 1px solid #666666;border-bottom: 1px solid #666666;padding:2px 4px;\"><font size='2' face='Arial, Helvetica, sans-serif'>&euro; $course13_rev</font></td>";

        }

        $message     .= "
  <td style=\"border-right: 1px solid #666666;border-bottom: 1px solid #666666;padding:2px 4px;\"><font size='2' face='Arial, Helvetica, sans-serif'>$course13_type</font></td>
  <td style=\"border-right: 1px solid #666666;border-bottom: 1px solid #666666;padding:2px 4px;\"><font size='2' face='Arial, Helvetica, sans-serif'>$course13_code" . display_endcustomer($course13_code) . "</font></td>
  </tr>

   <tr>
  <td style=\"border-right: 1px solid #666666;border-bottom: 1px solid #666666;padding:2px 4px;\"><font size='2' face='Arial, Helvetica, sans-serif'>$course14</font></td>
  <td style=\"border-right: 1px solid #666666;border-bottom: 1px solid #666666;padding:2px 4px;\"><font size='2' face='Arial, Helvetica, sans-serif'>$course14_start</font></td>
  <td style=\"border-right: 1px solid #666666;border-bottom: 1px solid #666666;padding:2px 4px;\"><font size='2' face='Arial, Helvetica, sans-serif'>$course14_dur</font></td>
  <td style=\"border-right: 1px solid #666666;border-bottom: 1px solid #666666;padding:2px 4px;\"><font size='2' face='Arial, Helvetica, sans-serif'>$course14_part</font></td>";

        if(($_SESSION['rev_vis'])=='yes') {

            $message     .= "
  <td style=\"border-right: 1px solid #666666;border-bottom: 1px solid #666666;padding:2px 4px;\"><font size='2' face='Arial, Helvetica, sans-serif'>&euro; $course14_rev</font></td>";

        }

        $message     .= "
  <td style=\"border-right: 1px solid #666666;border-bottom: 1px solid #666666;padding:2px 4px;\"><font size='2' face='Arial, Helvetica, sans-serif'>$course14_type</font></td>
  <td style=\"border-right: 1px solid #666666;border-bottom: 1px solid #666666;padding:2px 4px;\"><font size='2' face='Arial, Helvetica, sans-serif'>$course14_code" . display_endcustomer($course14_code) . "</font></td>
  </tr>

   <tr>
  <td style=\"border-right: 1px solid #666666;border-bottom: 1px solid #666666;padding:2px 4px;\"><font size='2' face='Arial, Helvetica, sans-serif'>$course15</font></td>
  <td style=\"border-right: 1px solid #666666;border-bottom: 1px solid #666666;padding:2px 4px;\"><font size='2' face='Arial, Helvetica, sans-serif'>$course15_start</font></td>
  <td style=\"border-right: 1px solid #666666;border-bottom: 1px solid #666666;padding:2px 4px;\"><font size='2' face='Arial, Helvetica, sans-serif'>$course15_dur</font></td>
  <td style=\"border-right: 1px solid #666666;border-bottom: 1px solid #666666;padding:2px 4px;\"><font size='2' face='Arial, Helvetica, sans-serif'>$course15_part</font></td>";

        if(($_SESSION['rev_vis'])=='yes') {

            $message     .= "
  <td style=\"border-right: 1px solid #666666;border-bottom: 1px solid #666666;padding:2px 4px;\"><font size='2' face='Arial, Helvetica, sans-serif'>&euro; $course15_rev</font></td>";

        }

        $message     .= "
  <td style=\"border-right: 1px solid #666666;border-bottom: 1px solid #666666;padding:2px 4px;\"><font size='2' face='Arial, Helvetica, sans-serif'>$course15_type</font></td>
  <td style=\"border-right: 1px solid #666666;border-bottom: 1px solid #666666;padding:2px 4px;\"><font size='2' face='Arial, Helvetica, sans-serif'>$course15_code" . display_endcustomer($course15_code) . "</font></td>
  </tr>
  </table>";

        if(($_SESSION['serv_vis'])=='yes') {

            $message     .= "
  <h2><font size='3' face='Arial, Helvetica, sans-serif' color='#5D8EB4'>Consultancy and writing services</font></h2>
  <p><font size='2' face='Arial, Helvetica, sans-serif'><strong>Consultancy: </strong>&euro; $consult</font></p>
  <p><font size='2' face='Arial, Helvetica, sans-serif'><strong>Writing services: </strong>&euro; $writing</font></p>";

        }

        $message     .= "
  <h2><font size='3' face='Arial, Helvetica, sans-serif' color='#5D8EB4'>Commitment fee</font></h2>
  <p><font size='2' face='Arial, Helvetica, sans-serif'><strong>Training: </strong>&euro; $cftraining (quarterly)</font></p>
  <p><font size='2' face='Arial, Helvetica, sans-serif'><strong>Services: </strong>&euro; $cfservices (monthly)</font></p>";

        if(($_SESSION['publimap_vis'])=='yes') {

            $message     .= "
  <h2><font size='3' face='Arial, Helvetica, sans-serif' color='#5D8EB4'>Publimap revenue</font></h2>
  <p><font size='2' face='Arial, Helvetica, sans-serif'><strong>Revenue: </strong>&euro; $publimap_rev</font></p>";

        }

        if(($_SESSION['imapper_vis'])=='yes') {

            $message     .= "
  <h2><font size='3' face='Arial, Helvetica, sans-serif' color='#5D8EB4'>iMapper revenue</font></h2>
  <p><font size='2' face='Arial, Helvetica, sans-serif'><strong>Revenue: </strong>&euro; $imapper_rev</font></p>";

        }

        $message     .= "
  <h2><font size='3' face='Arial, Helvetica, sans-serif' color='#5D8EB4'>Comments</font></h2>
  <p><font size='2' face='Arial, Helvetica, sans-serif'>$comments</font></p>
  </body>
  </html>";


        $mime_boundary="==Multipart_Boundary_x".md5(mt_rand())."x";
        $headers = "From: $first $last <$email>\n";
        $headers .= "CC: Francis Declercq <fdeclercq@informationmapping.com>\n";
        $headers .= "CC: Veronique Wittebolle <vwittebolle@informationmapping.com>\n";
        $headers .= "BCC: Sofian Mourabit <smourabit@informationmapping.com>\n";
        $headers .= "X-Priority: 1\n";
        $headers .= "Content-Type: multipart/mixed;\n" . " boundary=\"{$mime_boundary}\"\n" . "MIME-Version: 1.0\n";
        $subject="Partner Report $month $year from " . $_SESSION['partner_company'];

        $message = "This is a multi-part message in MIME format.\n\n" .
            "--{$mime_boundary}\n" .
            "Content-Type: text/html; charset=\"UTF-8\"\n" . // iso-8859-1
            "Content-Transfer-Encoding: 7bit\n\n" . $message . "\n\n";

        $message .="--{$mime_boundary}--\n";

        mail($email,$subject, $message, $headers);
        ?>
        <div id='report'>
            <h3>Thank you!</h3>
            <p>Dear <?php echo $first ?>,</p>
            <p>Thank you for submitting the report!<br />You will receive an e-mail with the submitted report shortly.</p>
            <table width="500" border="0" cellpadding="0" cellspacing="0" class="report">
                <tr>
                    <th colspan="6">Report submitted by</th>
                </tr>
                <tr class="odd">
                    <td width="120"><strong>First name</strong></td>
                    <td colspan="5"><?php echo $first ?></td>
                </tr>
                <tr>
                    <td><strong>Last name</strong></td>
                    <td colspan="5"><?php echo $last ?></td>
                </tr>
                <tr class="odd">
                    <td><strong>E-mail</strong></td>
                    <td colspan="5"><?php echo $email ?></td>
                </tr>
            </table>

            <table width="500" border="0" cellpadding="0" cellspacing="0" class="report">
                <tr>
                    <th colspan="6">Reporting for</th>
                </tr>
                <tr class="odd">
                    <td width="120"><strong>Company</strong></td>
                    <td colspan="5"><?php
                        // uitlezen van company name op basis van company id
                        $partner_SQL="SELECT * FROM reporting_partners WHERE partner_email='" . $_SESSION['partner_email'] . "'";
                        $partner_result=mysql_query($partner_SQL);
                        while($partner=mysql_fetch_array($partner_result)){
                            $company=$partner['partner_company'];
                        }
                        echo $company;

                        ?></td>
                </tr>
                <tr>
                    <td><strong>Period</strong></td>
                    <td colspan="5"><?php echo $month ?> <?php echo $year ?></td>
                </tr>
            </table>

            <table width="960" border="0" cellpadding="0" cellspacing="0" class="report">
                <tr>
                    <th colspan="7">Classroom training</th>
                </tr>
                <tr class="odd">
                    <td><strong>Title</strong></td>
                    <td><strong>Starting date</strong></td>
                    <td><strong>Duration</strong></td>
                    <td><strong>Participants</strong></td>
                    <?php
                    if(($_SESSION['rev_vis'])=='yes') {
                        ?>
                        <td><strong>Client revenue</strong></td>
                    <?php
                    }
                    ?>
                    <td><strong>Type</strong></td>
                    <td><strong>CLP ID</strong></td>
                </tr>
                <?php
                if(($course1)!=="- Select -") {
                    ?>
                    <tr>
                        <td><?php echo $course1 ?></td>
                        <td><?php echo $course1_start ?></td>
                        <td><?php echo $course1_dur ?> days</td>
                        <td><?php echo $course1_part ?></td>
                        <?php
                        if(($_SESSION['rev_vis'])=='yes') {
                            ?>
                            <td>&euro; <?php echo $course1_rev ?></td>
                        <?php
                        }
                        ?>
                        <td><?php echo $course1_type ?></td>
                        <td><?php echo $course1_code ?></td>
                    </tr>
                <?php
                } else {
                    ?>
                    <tr>
                        <td>No courses reported</td>
                    </tr>
                <?php
                }

                if(($course2)!=="- Select -") {
                    ?>
                    <tr>
                        <td><?php echo $course2 ?></td>
                        <td><?php echo $course2_start ?></td>
                        <td><?php echo $course2_dur ?> days</td>
                        <td><?php echo $course2_part ?></td>
                        <?php
                        if(($_SESSION['rev_vis'])=='yes') {
                            ?>
                            <td>&euro; <?php echo $course2_rev ?></td>
                        <?php
                        }
                        ?>
                        <td><?php echo $course2_type ?></td>
                        <td><?php echo $course2_code ?></td>
                    </tr>
                <?php
                }

                if(($course3)!=="- Select -") {
                    ?>

                    <tr>
                        <td><?php echo $course3 ?></td>
                        <td><?php echo $course3_start ?></td>
                        <td><?php echo $course3_dur ?> days</td>
                        <td><?php echo $course3_part ?></td>
                        <?php
                        if(($_SESSION['rev_vis'])=='yes') {
                            ?>
                            <td>&euro; <?php echo $course3_rev ?></td>
                        <?php
                        }
                        ?>
                        <td><?php echo $course3_type ?></td>
                        <td><?php echo $course3_code ?></td>
                    </tr>
                <?php
                }

                if(($course4)!=="- Select -") {
                    ?>
                    <tr>
                        <td><?php echo $course4 ?></td>
                        <td><?php echo $course4_start ?></td>
                        <td><?php echo $course4_dur ?> days</td>
                        <td><?php echo $course4_part ?></td>
                        <?php
                        if(($_SESSION['rev_vis'])=='yes') {
                            ?>
                            <td>&euro; <?php echo $course4_rev ?></td>
                        <?php
                        }
                        ?>
                        <td><?php echo $course4_type ?></td>
                        <td><?php echo $course4_code ?></td>
                    </tr>
                <?php
                }

                if(($course5)!=="- Select -") {
                    ?>
                    <tr>
                        <td><?php echo $course5 ?></td>
                        <td><?php echo $course5_start ?></td>
                        <td><?php echo $course5_dur ?> days</td>
                        <td><?php echo $course5_part ?></td>
                        <?php
                        if(($_SESSION['rev_vis'])=='yes') {
                            ?>
                            <td>&euro; <?php echo $course5_rev ?></td>
                        <?php
                        }
                        ?>
                        <td><?php echo $course5_type ?></td>
                        <td><?php echo $course5_code ?></td>
                    </tr>
                <?php
                }

                if(($course6)!=="- Select -") {
                    ?>
                    <tr>
                        <td><?php echo $course6 ?></td>
                        <td><?php echo $course6_start ?></td>
                        <td><?php echo $course6_dur ?> days</td>
                        <td><?php echo $course6_part ?></td>
                        <?php
                        if(($_SESSION['rev_vis'])=='yes') {
                            ?>
                            <td>&euro; <?php echo $course6_rev ?></td>
                        <?php
                        }
                        ?>
                        <td><?php echo $course6_type ?></td>
                        <td><?php echo $course6_code ?></td>
                    </tr>
                <?php
                }

                if(($course7)!=="- Select -") {
                    ?>
                    <tr>
                        <td><?php echo $course7 ?></td>
                        <td><?php echo $course7_start ?></td>
                        <td><?php echo $course7_dur ?> days</td>
                        <td><?php echo $course7_part ?></td>
                        <?php
                        if(($_SESSION['rev_vis'])=='yes') {
                            ?>
                            <td>&euro; <?php echo $course7_rev ?></td>
                        <?php
                        }
                        ?>
                        <td><?php echo $course7_type ?></td>
                        <td><?php echo $course7_code ?></td>
                    </tr>
                <?php
                }

                if(($course8)!=="- Select -") {
                    ?>
                    <tr>
                        <td><?php echo $course8 ?></td>
                        <td><?php echo $course8_start ?></td>
                        <td><?php echo $course8_dur ?> days</td>
                        <td><?php echo $course8_part ?></td>
                        <?php
                        if(($_SESSION['rev_vis'])=='yes') {
                            ?>
                            <td>&euro; <?php echo $course8_rev ?></td>
                        <?php
                        }
                        ?>
                        <td><?php echo $course8_type ?></td>
                        <td><?php echo $course8_code ?></td>
                    </tr>
                <?php
                }

                if(($course9)!=="- Select -") {
                    ?>
                    <tr>
                        <td><?php echo $course9 ?></td>
                        <td><?php echo $course9_start ?></td>
                        <td><?php echo $course9_dur ?> days</td>
                        <td><?php echo $course9_part ?></td>
                        <?php
                        if(($_SESSION['rev_vis'])=='yes') {
                            ?>
                            <td>&euro; <?php echo $course9_rev ?></td>
                        <?php
                        }
                        ?>
                        <td><?php echo $course9_type ?></td>
                        <td><?php echo $course9_code ?></td>
                    </tr>
                <?php
                }

                if(($course10)!=="- Select -") {
                    ?>
                    <tr>
                        <td><?php echo $course10 ?></td>
                        <td><?php echo $course10_start ?></td>
                        <td><?php echo $course10_dur ?> days</td>
                        <td><?php echo $course10_part ?></td>
                        <?php
                        if(($_SESSION['rev_vis'])=='yes') {
                            ?>
                            <td>&euro; <?php echo $course10_rev ?></td>
                        <?php
                        }
                        ?>
                        <td><?php echo $course10_type ?></td>
                        <td><?php echo $course10_code ?></td>
                    </tr>
                <?php
                }

                if(($course11)!=="- Select -") {
                    ?>

                    <tr>
                        <td><?php echo $course11 ?></td>
                        <td><?php echo $course11_start ?></td>
                        <td><?php echo $course11_dur ?> days</td>
                        <td><?php echo $course11_part ?></td>
                        <?php
                        if(($_SESSION['rev_vis'])=='yes') {
                            ?>
                            <td>&euro; <?php echo $course11_rev ?></td>
                        <?php
                        }
                        ?>
                        <td><?php echo $course11_type ?></td>
                        <td><?php echo $course11_code ?></td>
                    </tr>
                <?php
                }

                if(($course12)!=="- Select -") {
                    ?>

                    <tr>
                        <td><?php echo $course12 ?></td>
                        <td><?php echo $course12_start ?></td>
                        <td><?php echo $course12_dur ?> days</td>
                        <td><?php echo $course12_part ?></td>
                        <?php
                        if(($_SESSION['rev_vis'])=='yes') {
                            ?>
                            <td>&euro; <?php echo $course12_rev ?></td>
                        <?php
                        }
                        ?>
                        <td><?php echo $course12_type ?></td>
                        <td><?php echo $course12_code ?></td>
                    </tr>
                <?php
                }

                if(($course13)!=="- Select -") {
                    ?>

                    <tr>
                        <td><?php echo $course13 ?></td>
                        <td><?php echo $course13_start ?></td>
                        <td><?php echo $course13_dur ?> days</td>
                        <td><?php echo $course13_part ?></td>
                        <?php
                        if(($_SESSION['rev_vis'])=='yes') {
                            ?>
                            <td>&euro; <?php echo $course13_rev ?></td>
                        <?php
                        }
                        ?>
                        <td><?php echo $course13_type ?></td>
                        <td><?php echo $course13_code ?></td>
                    </tr>
                <?php
                }

                if(($course14)!=="- Select -") {
                    ?>

                    <tr>
                        <td><?php echo $course14 ?></td>
                        <td><?php echo $course14_start ?></td>
                        <td><?php echo $course14_dur ?> days</td>
                        <td><?php echo $course14_part ?></td>
                        <?php
                        if(($_SESSION['rev_vis'])=='yes') {
                            ?>
                            <td>&euro; <?php echo $course14_rev ?></td>
                        <?php
                        }
                        ?>
                        <td><?php echo $course14_type ?></td>
                        <td><?php echo $course14_code ?></td>
                    </tr>
                <?php
                }

                if(($course15)!=="- Select -") {
                    ?>

                    <tr>
                        <td><?php echo $course15 ?></td>
                        <td><?php echo $course15_start ?></td>
                        <td><?php echo $course15_dur ?> days</td>
                        <td><?php echo $course15_part ?></td>
                        <?php
                        if(($_SESSION['rev_vis'])=='yes') {
                            ?>
                            <td>&euro; <?php echo $course15_rev ?></td>
                        <?php
                        }
                        ?>
                        <td><?php echo $course15_type ?></td>
                        <td><?php echo $course15_code ?></td>
                    </tr>
                <?php
                }
                ?>

            </table>
            <?php
            if(($_SESSION['serv_vis'])=='yes') {

                ?>
                <table width="500" border="0" cellpadding="0" cellspacing="0" class="report">
                    <tr>
                        <th colspan="2">Consultancy and writing services</th>
                    </tr>
                    <tr>
                        <td width="120"><strong>Consultancy</strong></td>
                        <td>&euro; <?php echo $consult ?></td>
                    </tr>
                    <tr class="odd">
                        <td><strong>Writing services</strong></td>
                        <td>&euro; <?php echo $writing ?></td>
                    </tr>
                </table>
            <?php
            }
            ?>
            <table width="500" border="0" cellpadding="0" cellspacing="0" class="report">
                <tr>
                    <th colspan="2">Commitment fee</th>
                </tr>
                <tr>
                    <td width="150"><strong>Training (quarterly)</strong></td>
                    <td>&euro; <?php echo $cftraining ?></td>
                </tr>
                <tr class="odd">
                    <td><strong>Services (monthly)</strong></td>
                    <td>&euro; <?php echo $cfservices ?></td>
                </tr>
            </table>

            <?php
            if(($_SESSION['publimap_vis'])=='yes') {

                ?>

                <table width="500" border="0" cellpadding="0" cellspacing="0" class="report">
                    <tr>
                        <th colspan="2">Publimap revenue</th>
                    </tr>
                    <tr>
                        <td width="150"><strong>Revenue</strong></td>
                        <td>&euro; <?php echo $publimap_rev ?></td>
                    </tr>
                </table>
            <?php
            }
            ?>

            <?php
            if(($_SESSION['imapper_vis'])=='yes') {

                ?>

                <table width="500" border="0" cellpadding="0" cellspacing="0" class="report">
                    <tr>
                        <th colspan="2">iMapper revenue</th>
                    </tr>
                    <tr>
                        <td width="150"><strong>Revenue</strong></td>
                        <td>&euro; <?php echo $imapper_rev ?></td>
                    </tr>
                </table>
            <?php
            }
            ?>

            <table width="500" border="0" cellpadding="0" cellspacing="0" class="report">
                <tr>
                    <th colspan="2">Additional comments</th>
                </tr>
                <tr>
                    <td>
                        <?php echo $comments ?>
                    </td>
                </tr>
            </table>
        </div>
        <?php

    }

}
?>
</body>
</html>