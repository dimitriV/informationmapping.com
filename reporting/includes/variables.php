<?php
	$partner_id=htmlspecialchars($_POST['partner_id'], ENT_QUOTES);
	$first=htmlspecialchars($_SESSION['partner_first'], ENT_QUOTES);
	$last=htmlspecialchars($_SESSION['partner_last'], ENT_QUOTES);
	$email=htmlspecialchars($_SESSION['partner_email'], ENT_QUOTES);
	$month=htmlspecialchars($_POST['month'], ENT_QUOTES);
	$year=htmlspecialchars($_POST['year'], ENT_QUOTES);
	
	$course1=htmlspecialchars($_POST['course1'], ENT_QUOTES);
	$course1_start=htmlspecialchars($_POST['course1_start'], ENT_QUOTES);
	$course1_dur=htmlspecialchars($_POST['course1_dur'], ENT_QUOTES);
	$course1_part=htmlspecialchars($_POST['course1_part'], ENT_QUOTES);
	$course1_type=htmlspecialchars($_POST['course1_type'], ENT_QUOTES);
	$course1_code=htmlspecialchars($_POST['course1_code'], ENT_QUOTES);
	$course1_rev=htmlspecialchars($_POST['course1_rev'], ENT_QUOTES);
	
	$course2=htmlspecialchars($_POST['course2'], ENT_QUOTES);
	$course2_start=htmlspecialchars($_POST['course2_start'], ENT_QUOTES);
	$course2_dur=htmlspecialchars($_POST['course2_dur'], ENT_QUOTES);
	$course2_part=htmlspecialchars($_POST['course2_part'], ENT_QUOTES);
	$course2_type=htmlspecialchars($_POST['course2_type'], ENT_QUOTES);
	$course2_code=htmlspecialchars($_POST['course2_code'], ENT_QUOTES);
	$course2_rev=htmlspecialchars($_POST['course2_rev'], ENT_QUOTES);
	
	$course3=htmlspecialchars($_POST['course3'], ENT_QUOTES);
	$course3_start=htmlspecialchars($_POST['course3_start'], ENT_QUOTES);
	$course3_dur=htmlspecialchars($_POST['course3_dur'], ENT_QUOTES);
	$course3_part=htmlspecialchars($_POST['course3_part'], ENT_QUOTES);
	$course3_type=htmlspecialchars($_POST['course3_type'], ENT_QUOTES);
	$course3_code=htmlspecialchars($_POST['course3_code'], ENT_QUOTES);
	$course3_rev=htmlspecialchars($_POST['course3_rev'], ENT_QUOTES);
	
	$course4=htmlspecialchars($_POST['course4'], ENT_QUOTES);
	$course4_start=htmlspecialchars($_POST['course4_start'], ENT_QUOTES);
	$course4_dur=htmlspecialchars($_POST['course4_dur'], ENT_QUOTES);
	$course4_part=htmlspecialchars($_POST['course4_part'], ENT_QUOTES);
	$course4_type=htmlspecialchars($_POST['course4_type'], ENT_QUOTES);
	$course4_code=htmlspecialchars($_POST['course4_code'], ENT_QUOTES);
	$course4_rev=htmlspecialchars($_POST['course4_rev'], ENT_QUOTES);
	
	$course5=htmlspecialchars($_POST['course5'], ENT_QUOTES);
	$course5_start=htmlspecialchars($_POST['course5_start'], ENT_QUOTES);
	$course5_dur=htmlspecialchars($_POST['course5_dur'], ENT_QUOTES);
	$course5_part=htmlspecialchars($_POST['course5_part'], ENT_QUOTES);
	$course5_type=htmlspecialchars($_POST['course5_type'], ENT_QUOTES);
	$course5_code=htmlspecialchars($_POST['course5_code'], ENT_QUOTES);
	$course5_rev=htmlspecialchars($_POST['course5_rev'], ENT_QUOTES);
	
	$course6=htmlspecialchars($_POST['course6'], ENT_QUOTES);
	$course6_start=htmlspecialchars($_POST['course6_start'], ENT_QUOTES);
	$course6_dur=htmlspecialchars($_POST['course6_dur'], ENT_QUOTES);
	$course6_part=htmlspecialchars($_POST['course6_part'], ENT_QUOTES);
	$course6_type=htmlspecialchars($_POST['course6_type'], ENT_QUOTES);
	$course6_code=htmlspecialchars($_POST['course6_code'], ENT_QUOTES);
	$course6_rev=htmlspecialchars($_POST['course6_rev'], ENT_QUOTES);
	
	$course7=htmlspecialchars($_POST['course7'], ENT_QUOTES);
	$course7_start=htmlspecialchars($_POST['course7_start'], ENT_QUOTES);
	$course7_dur=htmlspecialchars($_POST['course7_dur'], ENT_QUOTES);
	$course7_part=htmlspecialchars($_POST['course7_part'], ENT_QUOTES);
	$course7_type=htmlspecialchars($_POST['course7_type'], ENT_QUOTES);
	$course7_code=htmlspecialchars($_POST['course7_code'], ENT_QUOTES);
	$course7_rev=htmlspecialchars($_POST['course7_rev'], ENT_QUOTES);
	
	$course8=htmlspecialchars($_POST['course8'], ENT_QUOTES);
	$course8_start=htmlspecialchars($_POST['course8_start'], ENT_QUOTES);
	$course8_dur=htmlspecialchars($_POST['course8_dur'], ENT_QUOTES);
	$course8_part=htmlspecialchars($_POST['course8_part'], ENT_QUOTES);
	$course8_type=htmlspecialchars($_POST['course8_type'], ENT_QUOTES);
	$course8_code=htmlspecialchars($_POST['course8_code'], ENT_QUOTES);
	$course8_rev=htmlspecialchars($_POST['course8_rev'], ENT_QUOTES);
	
	$course9=htmlspecialchars($_POST['course9'], ENT_QUOTES);
	$course9_start=htmlspecialchars($_POST['course9_start'], ENT_QUOTES);
	$course9_dur=htmlspecialchars($_POST['course9_dur'], ENT_QUOTES);
	$course9_part=htmlspecialchars($_POST['course9_part'], ENT_QUOTES);
	$course9_type=htmlspecialchars($_POST['course9_type'], ENT_QUOTES);
	$course9_code=htmlspecialchars($_POST['course9_code'], ENT_QUOTES);
	$course9_rev=htmlspecialchars($_POST['course9_rev'], ENT_QUOTES);
	
	$course10=htmlspecialchars($_POST['course10'], ENT_QUOTES);
	$course10_start=htmlspecialchars($_POST['course10_start'], ENT_QUOTES);
	$course10_dur=htmlspecialchars($_POST['course10_dur'], ENT_QUOTES);
	$course10_part=htmlspecialchars($_POST['course10_part'], ENT_QUOTES);
	$course10_type=htmlspecialchars($_POST['course10_type'], ENT_QUOTES);
	$course10_code=htmlspecialchars($_POST['course10_code'], ENT_QUOTES);
	$course10_rev=htmlspecialchars($_POST['course10_rev'], ENT_QUOTES);
	
	$course11=htmlspecialchars($_POST['course11'], ENT_QUOTES);
	$course11_start=htmlspecialchars($_POST['course11_start'], ENT_QUOTES);
	$course11_dur=htmlspecialchars($_POST['course11_dur'], ENT_QUOTES);
	$course11_part=htmlspecialchars($_POST['course11_part'], ENT_QUOTES);
	$course11_type=htmlspecialchars($_POST['course11_type'], ENT_QUOTES);
	$course11_code=htmlspecialchars($_POST['course11_code'], ENT_QUOTES);
	$course11_rev=htmlspecialchars($_POST['course11_rev'], ENT_QUOTES);
	
	$course12=htmlspecialchars($_POST['course12'], ENT_QUOTES);
	$course12_start=htmlspecialchars($_POST['course12_start'], ENT_QUOTES);
	$course12_dur=htmlspecialchars($_POST['course12_dur'], ENT_QUOTES);
	$course12_part=htmlspecialchars($_POST['course12_part'], ENT_QUOTES);
	$course12_type=htmlspecialchars($_POST['course12_type'], ENT_QUOTES);
	$course12_code=htmlspecialchars($_POST['course12_code'], ENT_QUOTES);
	$course12_rev=htmlspecialchars($_POST['course12_rev'], ENT_QUOTES);
	
	$course13=htmlspecialchars($_POST['course13'], ENT_QUOTES);
	$course13_start=htmlspecialchars($_POST['course13_start'], ENT_QUOTES);
	$course13_dur=htmlspecialchars($_POST['course13_dur'], ENT_QUOTES);
	$course13_part=htmlspecialchars($_POST['course13_part'], ENT_QUOTES);
	$course13_type=htmlspecialchars($_POST['course13_type'], ENT_QUOTES);
	$course13_code=htmlspecialchars($_POST['course13_code'], ENT_QUOTES);
	$course13_rev=htmlspecialchars($_POST['course13_rev'], ENT_QUOTES);
	
	$course14=htmlspecialchars($_POST['course14'], ENT_QUOTES);
	$course14_start=htmlspecialchars($_POST['course14_start'], ENT_QUOTES);
	$course14_dur=htmlspecialchars($_POST['course14_dur'], ENT_QUOTES);
	$course14_part=htmlspecialchars($_POST['course14_part'], ENT_QUOTES);
	$course14_type=htmlspecialchars($_POST['course14_type'], ENT_QUOTES);
	$course14_code=htmlspecialchars($_POST['course14_code'], ENT_QUOTES);
	$course14_rev=htmlspecialchars($_POST['course14_rev'], ENT_QUOTES);
	
	$course15=htmlspecialchars($_POST['course15'], ENT_QUOTES);
	$course15_start=htmlspecialchars($_POST['course15_start'], ENT_QUOTES);
	$course15_dur=htmlspecialchars($_POST['course15_dur'], ENT_QUOTES);
	$course15_part=htmlspecialchars($_POST['course15_part'], ENT_QUOTES);
	$course15_type=htmlspecialchars($_POST['course15_type'], ENT_QUOTES);
	$course15_code=htmlspecialchars($_POST['course15_code'], ENT_QUOTES);
	$course15_rev=htmlspecialchars($_POST['course15_rev'], ENT_QUOTES);
	
	$consult=htmlspecialchars($_POST['consult'], ENT_QUOTES);
	$writing=htmlspecialchars($_POST['writing'], ENT_QUOTES);
	$cftraining=htmlspecialchars($_SESSION['cftraining'], ENT_QUOTES);
	$cfservices=htmlspecialchars($_SESSION['cfservices'], ENT_QUOTES);
	
	$comments=htmlspecialchars($_POST['comments'], ENT_QUOTES);
	$publimap_rev=htmlspecialchars($_POST['publimap_rev'], ENT_QUOTES);
	$imapper_rev=htmlspecialchars($_POST['imapper_rev'], ENT_QUOTES);
	
	// Set variables before using them
    if (!isset($_POST['course1']))
	{
	$_POST['course1'] = "- Select -";
	} 
	
	if (!isset($_POST['course2']))
	{
	$_POST['course2'] = "- Select -";
	} 
	
	if (!isset($_POST['course3']))
	{
	$_POST['course3'] = "- Select -";
	} 
	
	if (!isset($_POST['course4']))
	{
	$_POST['course4'] = "- Select -";
	} 
	
	if (!isset($_POST['course5']))
	{
	$_POST['course5'] = "- Select -";
	} 
	
	if (!isset($_POST['course6']))
	{
	$_POST['course6'] = "- Select -";
	} 
	
	if (!isset($_POST['course7']))
	{
	$_POST['course7'] = "- Select -";
	} 
	
	if (!isset($_POST['course8']))
	{
	$_POST['course8'] = "- Select -";
	} 
	
	if (!isset($_POST['course9']))
	{
	$_POST['course9'] = "- Select -";
	} 
	
	if (!isset($_POST['course10']))
	{
	$_POST['course10'] = "- Select -";
	} 
	
	if (!isset($_POST['course11']))
	{
	$_POST['course11'] = "- Select -";
	} 
	
	if (!isset($_POST['course12']))
	{
	$_POST['course12'] = "- Select -";
	} 
	
	if (!isset($_POST['course13']))
	{
	$_POST['course13'] = "- Select -";
	} 
	
	if (!isset($_POST['course14']))
	{
	$_POST['course14'] = "- Select -";
	} 
	
	if (!isset($_POST['course15']))
	{
	$_POST['course15'] = "- Select -";
	} 
	
	if (!isset($_POST['course1_type']))
	{
	$_POST['course1_type'] = "- Select -";
	} 
	
	if (!isset($_POST['course2_type']))
	{
	$_POST['course2_type'] = "- Select -";
	} 
	
	if (!isset($_POST['course3_type']))
	{
	$_POST['course3_type'] = "- Select -";
	} 
	
	if (!isset($_POST['course4_type']))
	{
	$_POST['course4_type'] = "- Select -";
	} 
	
	if (!isset($_POST['course5_type']))
	{
	$_POST['course5_type'] = "- Select -";
	} 
	
	if (!isset($_POST['course6_type']))
	{
	$_POST['course6_type'] = "- Select -";
	} 
	
	if (!isset($_POST['course7_type']))
	{
	$_POST['course7_type'] = "- Select -";
	} 
	
	if (!isset($_POST['course8_type']))
	{
	$_POST['course8_type'] = "- Select -";
	} 
	
	if (!isset($_POST['course9_type']))
	{
	$_POST['course9_type'] = "- Select -";
	} 
	
	if (!isset($_POST['course10_type']))
	{
	$_POST['course10_type'] = "- Select -";
	} 
	
	if (!isset($_POST['course11_type']))
	{
	$_POST['course11_type'] = "- Select -";
	} 
	
	if (!isset($_POST['course12_type']))
	{
	$_POST['course12_type'] = "- Select -";
	} 
	
	if (!isset($_POST['course13_type']))
	{
	$_POST['course13_type'] = "- Select -";
	} 
	
	if (!isset($_POST['course14_type']))
	{
	$_POST['course14_type'] = "- Select -";
	} 
	
	if (!isset($_POST['course15_type']))
	{
	$_POST['course15_type'] = "- Select -";
	} 
	?>