<?php
	
	if(empty($name)) {
	  $error[1] = "<br><span class=\"error\">Please fill out your name.</span>";
	}
	
	if(empty($company)) {
	  $error[3] = "<br><span class=\"error\">Please fill out the customer's name.</span>";
	}
	
	if(empty($website)) {
	  $error[4] = "<br><span class=\"error\">Please fill out the customer's URL.</span>";
	}
	
	if(empty($budget12)) {
	  $error[7] = "<br><span class=\"error\">Please fill out the expected budget for 2013.</span>";
	}
	
	if(empty($budget13)) {
	  $error[8] = "<br><span class=\"error\">Please fill out the expected budget for 2014.</span>";
	}
	
	if(empty($address1)) {
	  $error[10] = "<br><span class=\"error\">Please fill out the customer's address.</span>";
	}
	
	if(empty($country)) {
	  $error[11] = "<br><span class=\"error\">Please fill out the customer's country.</span>";
	}
	
	if($partner=="- Please select -") {
	  $error[12] = "<br><span class=\"error\">Please select your company.</span>";
	}
	
?>