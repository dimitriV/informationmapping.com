<?php
	function check_email($email) { 
    $pattern = "/^[\w-]+(\.[\w-]+)*@"; 
    $pattern .= "([0-9a-z][0-9a-z-]*[0-9a-z]\.)+([a-z]{2,4})$/i"; 
    if (preg_match($pattern, $email)) { 
        $parts = explode("@", $email); 
        if (checkdnsrr($parts[1], "MX")){ 
            // echo "The e-mail address is valid."; 
            return true; 
        } else { 
            // echo "The e-mail host is not valid."; 
            return false; 
        } 
    } else { 
        // echo "The e-mail address contains invalid characters."; 
        return false; 
    } 
} 
// end e-mail check
	
	/*if($month=="- Month -") {
	  $error[1] = "<br><span class=\"error\">Please select a reporting month.</span>";
	}
	
	
	if($year=="- Year -") {
	  $error[2] = "<br><span class=\"error\">Please select a reporting year.</span>";
	}*/
	
	/*if(empty($first)) {
	  $error[3] = "<br><span class=\"error\">Please fill out your first name.</span>";
	}*/
	
	/*if(empty($last)) {
	  $error[4] = "<br><span class=\"error\">Please fill out your last name.</span>";
	}*/
	
/*	if(empty($company)) {
	  $error[5] = "<br><span class=\"error\">Please fill out your company.</span>";
	}*/
	
	/*if(!check_email($email))
   	 $error[6] = "<br><span class=\"error\">You did not supply a valid e-mail address</span>\n";*/
	 
	 
	if($course1!=="- Select -") {
	  
	  if(empty($course1_start)) {
	  $error[7] = "<br><span class=\"error\">Required</span>\n";
	  }
	  
	  if(empty($course1_dur)) {
	  $error[8] = "<br><span class=\"error\">Required</span>\n";
	  }
	  
	  if(empty($course1_part)) {
	  $error[9] = "<br><span class=\"error\">Required</span>\n";
	  }
	  
	  if($course1_type=="- Select -") {
	  $error[10] = "<br><span class=\"error\">Required</span>\n";
	  }
	  
	  if(($course1_type=="Corporate") && (empty($course1_code))) {
	  $error[11] = "<br><span class=\"error\">Required</span>\n";
	  }
	  
	  if(($course1_type=="Public") && (!empty($course1_code))) {
	  $error[57] = "<br><span class=\"error\">Not allowed</span>\n";
	  }
	  
	}
	
	if($course2!=="- Select -") {
	  
	  if(empty($course2_start)) {
	  $error[12] = "<br><span class=\"error\">Required</span>\n";
	  }
	  
	  if(empty($course2_dur)) {
	  $error[13] = "<br><span class=\"error\">Required</span>\n";
	  }
	  
	  if(empty($course2_part)) {
	  $error[14] = "<br><span class=\"error\">Required</span>\n";
	  }
	  
	  if($course2_type=="- Select -") {
	  $error[15] = "<br><span class=\"error\">Required</span>\n";
	  }
	  
	  if(($course2_type=="Corporate") && (empty($course2_code))) {
	  $error[16] = "<br><span class=\"error\">Required</span>\n";
	  }
	  
	  if(($course2_type=="Public") && (!empty($course2_code))) {
	  $error[58] = "<br><span class=\"error\">Not allowed</span>\n";
	  }
	  
	}
	
	if($course3!=="- Select -") {
	  
	  if(empty($course3_start)) {
	  $error[17] = "<br><span class=\"error\">Required</span>\n";
	  }
	  
	  if(empty($course3_dur)) {
	  $error[18] = "<br><span class=\"error\">Required</span>\n";
	  }
	  
	  if(empty($course3_part)) {
	  $error[19] = "<br><span class=\"error\">Required</span>\n";
	  }
	  
	  if($course3_type=="- Select -") {
	  $error[20] = "<br><span class=\"error\">Required</span>\n";
	  }
	  
	  if(($course3_type=="Corporate") && (empty($course3_code))) {
	  $error[21] = "<br><span class=\"error\">Required</span>\n";
	  }
	  
	  if(($course3_type=="Public") && (!empty($course3_code))) {
	  $error[59] = "<br><span class=\"error\">Not allowed</span>\n";
	  }
	  
	}
	
	if($course4!=="- Select -") {
	  
	  if(empty($course4_start)) {
	  $error[22] = "<br><span class=\"error\">Required</span>\n";
	  }
	  
	  if(empty($course4_dur)) {
	  $error[23] = "<br><span class=\"error\">Required</span>\n";
	  }
	  
	  if(empty($course4_part)) {
	  $error[24] = "<br><span class=\"error\">Required</span>\n";
	  }
	  
	  if($course4_type=="- Select -") {
	  $error[25] = "<br><span class=\"error\">Required</span>\n";
	  }
	  
	  if(($course4_type=="Corporate") && (empty($course4_code))) {
	  $error[26] = "<br><span class=\"error\">Required</span>\n";
	  }
	  
	  if(($course4_type=="Public") && (!empty($course4_code))) {
	  $error[60] = "<br><span class=\"error\">Not allowed</span>\n";
	  }
	  
	}
	
	if($course5!=="- Select -") {
	  
	  if(empty($course5_start)) {
	  $error[27] = "<br><span class=\"error\">Required</span>\n";
	  }
	  
	  if(empty($course5_dur)) {
	  $error[28] = "<br><span class=\"error\">Required</span>\n";
	  }
	  
	  if(empty($course5_part)) {
	  $error[29] = "<br><span class=\"error\">Required</span>\n";
	  }
	  
	  if($course5_type=="- Select -") {
	  $error[30] = "<br><span class=\"error\">Required</span>\n";
	  }
	  
	  if(($course5_type=="Corporate") && (empty($course5_code))) {
	  $error[31] = "<br><span class=\"error\">Required</span>\n";
	  }
	  
	  if(($course5_type=="Public") && (!empty($course5_code))) {
	  $error[61] = "<br><span class=\"error\">Not allowed</span>\n";
	  }
	  
	}
	
	if($course6!=="- Select -") {
	  
	  if(empty($course6_start)) {
	  $error[32] = "<br><span class=\"error\">Required</span>\n";
	  }
	  
	  if(empty($course6_dur)) {
	  $error[33] = "<br><span class=\"error\">Required</span>\n";
	  }
	  
	  if(empty($course6_part)) {
	  $error[34] = "<br><span class=\"error\">Required</span>\n";
	  }
	  
	  if($course6_type=="- Select -") {
	  $error[35] = "<br><span class=\"error\">Required</span>\n";
	  }
	  
	  if(($course6_type=="Corporate") && (empty($course6_code))) {
	  $error[36] = "<br><span class=\"error\">Required</span>\n";
	  }
	  
	  if(($course6_type=="Public") && (!empty($course6_code))) {
	  $error[62] = "<br><span class=\"error\">Not allowed</span>\n";
	  }
	  
	}
	
	if($course7!=="- Select -") {
	  
	  if(empty($course7_start)) {
	  $error[37] = "<br><span class=\"error\">Required</span>\n";
	  }
	  
	  if(empty($course7_dur)) {
	  $error[38] = "<br><span class=\"error\">Required</span>\n";
	  }
	  
	  if(empty($course7_part)) {
	  $error[39] = "<br><span class=\"error\">Required</span>\n";
	  }
	  
	  if($course7_type=="- Select -") {
	  $error[40] = "<br><span class=\"error\">Required</span>\n";
	  }
	  
	  if(($course7_type=="Corporate") && (empty($course7_code))) {
	  $error[41] = "<br><span class=\"error\">Required</span>\n";
	  }
	  
	  if(($course7_type=="Public") && (!empty($course7_code))) {
	  $error[63] = "<br><span class=\"error\">Not allowed</span>\n";
	  }
	  
	}
	
	if($course8!=="- Select -") {
	  
	  if(empty($course8_start)) {
	  $error[42] = "<br><span class=\"error\">Required</span>\n";
	  }
	  
	  if(empty($course8_dur)) {
	  $error[43] = "<br><span class=\"error\">Required</span>\n";
	  }
	  
	  if(empty($course8_part)) {
	  $error[44] = "<br><span class=\"error\">Required</span>\n";
	  }
	  
	  if($course8_type=="- Select -") {
	  $error[45] = "<br><span class=\"error\">Required</span>\n";
	  }
	  
	  if(($course8_type=="Corporate") && (empty($course8_code))) {
	  $error[46] = "<br><span class=\"error\">Required</span>\n";
	  }
	  
	  if(($course8_type=="Public") && (!empty($course8_code))) {
	  $error[64] = "<br><span class=\"error\">Not allowed</span>\n";
	  }
	  
	}
	
	if($course9!=="- Select -") {
	  
	  if(empty($course9_start)) {
	  $error[47] = "<br><span class=\"error\">Required</span>\n";
	  }
	  
	  if(empty($course9_dur)) {
	  $error[48] = "<br><span class=\"error\">Required</span>\n";
	  }
	  
	  if(empty($course9_part)) {
	  $error[49] = "<br><span class=\"error\">Required</span>\n";
	  }
	  
	  if($course9_type=="- Select -") {
	  $error[50] = "<br><span class=\"error\">Required</span>\n";
	  }
	  
	  if(($course9_type=="Corporate") && (empty($course9_code))) {
	  $error[51] = "<br><span class=\"error\">Required</span>\n";
	  }
	  
	  if(($course9_type=="Public") && (!empty($course9_code))) {
	  $error[65] = "<br><span class=\"error\">Not allowed</span>\n";
	  }
	  
	}
	
	if($course10!=="- Select -") {
	  
	  if(empty($course10_start)) {
	  $error[52] = "<br><span class=\"error\">Required</span>\n";
	  }
	  
	  if(empty($course10_dur)) {
	  $error[53] = "<br><span class=\"error\">Required</span>\n";
	  }
	  
	  if(empty($course10_part)) {
	  $error[54] = "<br><span class=\"error\">Required</span>\n";
	  }
	  
	  if($course10_type=="- Select -") {
	  $error[55] = "<br><span class=\"error\">Required</span>\n";
	  }
	  
	  if(($course10_type=="Corporate") && (empty($course10_code))) {
	  $error[56] = "<br><span class=\"error\">Required</span>\n";
	  }
	  
	  if(($course10_type=="Public") && (!empty($course10_code))) {
	  $error[66] = "<br><span class=\"error\">Not allowed</span>\n";
	  }
	  
	}
	
	if($course11!=="- Select -") {
	  
	  if(empty($course11_start)) {
	  $error[78] = "<br><span class=\"error\">Required</span>\n";
	  }
	  
	  if(empty($course11_dur)) {
	  $error[79] = "<br><span class=\"error\">Required</span>\n";
	  }
	  
	  if(empty($course11_part)) {
	  $error[80] = "<br><span class=\"error\">Required</span>\n";
	  }
	  
	  if($course11_type=="- Select -") {
	  $error[81] = "<br><span class=\"error\">Required</span>\n";
	  }
	  
	  if(($course11_type=="Corporate") && (empty($course11_code))) {
	  $error[82] = "<br><span class=\"error\">Required</span>\n";
	  }
	  
	  if(($course11_type=="Public") && (!empty($course11_code))) {
	  $error[83] = "<br><span class=\"error\">Not allowed</span>\n";
	  }
	  
	}
	
	if($course12!=="- Select -") {
	  
	  if(empty($course12_start)) {
	  $error[84] = "<br><span class=\"error\">Required</span>\n";
	  }
	  
	  if(empty($course12_dur)) {
	  $error[85] = "<br><span class=\"error\">Required</span>\n";
	  }
	  
	  if(empty($course12_part)) {
	  $error[86] = "<br><span class=\"error\">Required</span>\n";
	  }
	  
	  if($course12_type=="- Select -") {
	  $error[87] = "<br><span class=\"error\">Required</span>\n";
	  }
	  
	  if(($course12_type=="Corporate") && (empty($course12_code))) {
	  $error[88] = "<br><span class=\"error\">Required</span>\n";
	  }
	  
	  if(($course12_type=="Public") && (!empty($course12_code))) {
	  $error[89] = "<br><span class=\"error\">Not allowed</span>\n";
	  }
	  
	}
	
	if($course13!=="- Select -") {
	  
	  if(empty($course13_start)) {
	  $error[90] = "<br><span class=\"error\">Required</span>\n";
	  }
	  
	  if(empty($course13_dur)) {
	  $error[91] = "<br><span class=\"error\">Required</span>\n";
	  }
	  
	  if(empty($course13_part)) {
	  $error[92] = "<br><span class=\"error\">Required</span>\n";
	  }
	  
	  if($course13_type=="- Select -") {
	  $error[93] = "<br><span class=\"error\">Required</span>\n";
	  }
	  
	  if(($course13_type=="Corporate") && (empty($course13_code))) {
	  $error[94] = "<br><span class=\"error\">Required</span>\n";
	  }
	  
	  if(($course13_type=="Public") && (!empty($course13_code))) {
	  $error[95] = "<br><span class=\"error\">Not allowed</span>\n";
	  }
	  
	}
	
	if($course14!=="- Select -") {
	  
	  if(empty($course14_start)) {
	  $error[96] = "<br><span class=\"error\">Required</span>\n";
	  }
	  
	  if(empty($course14_dur)) {
	  $error[97] = "<br><span class=\"error\">Required</span>\n";
	  }
	  
	  if(empty($course14_part)) {
	  $error[98] = "<br><span class=\"error\">Required</span>\n";
	  }
	  
	  if($course14_type=="- Select -") {
	  $error[99] = "<br><span class=\"error\">Required</span>\n";
	  }
	  
	  if(($course14_type=="Corporate") && (empty($course14_code))) {
	  $error[100] = "<br><span class=\"error\">Required</span>\n";
	  }
	  
	  if(($course14_type=="Public") && (!empty($course14_code))) {
	  $error[101] = "<br><span class=\"error\">Not allowed</span>\n";
	  }
	  
	}
	
	if($course15!=="- Select -") {
	  
	  if(empty($course15_start)) {
	  $error[102] = "<br><span class=\"error\">Required</span>\n";
	  }
	  
	  if(empty($course15_dur)) {
	  $error[103] = "<br><span class=\"error\">Required</span>\n";
	  }
	  
	  if(empty($course15_part)) {
	  $error[104] = "<br><span class=\"error\">Required</span>\n";
	  }
	  
	  if($course15_type=="- Select -") {
	  $error[105] = "<br><span class=\"error\">Required</span>\n";
	  }
	  
	  if(($course15_type=="Corporate") && (empty($course15_code))) {
	  $error[106] = "<br><span class=\"error\">Required</span>\n";
	  }
	  
	  if(($course15_type=="Public") && (!empty($course15_code))) {
	  $error[107] = "<br><span class=\"error\">Not allowed</span>\n";
	  }
	  
	}
	
	
// check validity of CLP ID

	if(!empty($course1_code)) {
	
		$code1_sql="SELECT * FROM reporting_clp_number WHERE clp_number='$course1_code'";
		$code1_result = mysql_query($code1_sql);

		if(mysql_num_rows($code1_result) == 0) {
 	 	$error[67] = "<br><span class=\"error\">Invalid CLP ID</span>\n";
		}
	
	}
	
		if(!empty($course2_code)) {
	
		$code2_sql="SELECT * FROM reporting_clp_number WHERE clp_number='$course2_code'";
		$code2_result = mysql_query($code2_sql);

		if(mysql_num_rows($code2_result) == 0) {
 	 	$error[68] = "<br><span class=\"error\">Invalid CLP ID</span>\n";print (mysql_error()); 
		} 
	
	}
	
		if(!empty($course3_code)) {
	
		$code3_sql="SELECT * FROM reporting_clp_number WHERE clp_number='$course3_code'";
		$code3_result = mysql_query($code3_sql);

		if(mysql_num_rows($code3_result) == 0) {
 	 	$error[69] = "<br><span class=\"error\">Invalid CLP ID</span>\n";
		}
	
	}
	
		if(!empty($course4_code)) {
	
		$code4_sql="SELECT * FROM reporting_clp_number WHERE clp_number='$course4_code'";
		$code4_result = mysql_query($code4_sql);

		if(mysql_num_rows($code4_result) == 0) {
 	 	$error[70] = "<br><span class=\"error\">Invalid CLP ID</span>\n";
		}
	
	}
	
		if(!empty($course5_code)) {
	
		$code5_sql="SELECT * FROM reporting_clp_number WHERE clp_number='$course5_code'";
		$code5_result = mysql_query($code5_sql);

		if(mysql_num_rows($code5_result) == 0) {
 	 	$error[71] = "<br><span class=\"error\">Invalid CLP ID</span>\n";
		}
	
	}
	
		if(!empty($course6_code)) {
	
		$code6_sql="SELECT * FROM reporting_clp_number WHERE clp_number='$course6_code'";
		$code6_result = mysql_query($code6_sql);

		if(mysql_num_rows($code6_result) == 0) {
 	 	$error[72] = "<br><span class=\"error\">Invalid CLP ID</span>\n";
		}
	
	}
	
		if(!empty($course7_code)) {
	
		$code7_sql="SELECT * FROM reporting_clp_number WHERE clp_number='$course7_code'";
		$code7_result = mysql_query($code7_sql);

		if(mysql_num_rows($code7_result) == 0) {
 	 	$error[73] = "<br><span class=\"error\">Invalid CLP ID</span>\n";
		}
	
	}
	
		if(!empty($course8_code)) {
	
		$code8_sql="SELECT * FROM reporting_clp_number WHERE clp_number='$course8_code'";
		$code8_result = mysql_query($code8_sql);

		if(mysql_num_rows($code8_result) == 0) {
 	 	$error[74] = "<br><span class=\"error\">Invalid CLP ID</span>\n";
		}
	
	}
	
		if(!empty($course9_code)) {
	
		$code9_sql="SELECT * FROM reporting_clp_number WHERE clp_number='$course9_code'";
		$code9_result = mysql_query($code9_sql);

		if(mysql_num_rows($code9_result) == 0) {
 	 	$error[75] = "<br><span class=\"error\">Invalid CLP ID</span>\n";
		}
	
	}
	
		if(!empty($course10_code)) {
	
		$code10_sql="SELECT * FROM reporting_clp_number WHERE clp_number='$course10_code'";
		$code10_result = mysql_query($code10_sql);

		if(mysql_num_rows($code10_result) == 0) {
 	 	$error[76] = "<br><span class=\"error\">Invalid CLP ID</span>\n";
		}
	
	}
	
	if(!empty($course11_code)) {
	
		$code11_sql="SELECT * FROM reporting_clp_number WHERE clp_number='$course11_code'";
		$code11_result = mysql_query($code11_sql);

		if(mysql_num_rows($code11_result) == 0) {
 	 	$error[108] = "<br><span class=\"error\">Invalid CLP ID</span>\n";
		}
	
	}
	
	if(!empty($course12_code)) {
	
		$code12_sql="SELECT * FROM reporting_clp_number WHERE clp_number='$course12_code'";
		$code12_result = mysql_query($code12_sql);

		if(mysql_num_rows($code12_result) == 0) {
 	 	$error[109] = "<br><span class=\"error\">Invalid CLP ID</span>\n";
		}
	
	}
	
	if(!empty($course13_code)) {
	
		$code13_sql="SELECT * FROM reporting_clp_number WHERE clp_number='$course13_code'";
		$code13_result = mysql_query($code13_sql);

		if(mysql_num_rows($code13_result) == 0) {
 	 	$error[110] = "<br><span class=\"error\">Invalid CLP ID</span>\n";
		}
	
	}
	
	if(!empty($course14_code)) {
	
		$code14_sql="SELECT * FROM reporting_clp_number WHERE clp_number='$course14_code'";
		$code14_result = mysql_query($code14_sql);

		if(mysql_num_rows($code14_result) == 0) {
 	 	$error[111] = "<br><span class=\"error\">Invalid CLP ID</span>\n";
		}
	
	}
	
	if(!empty($course15_code)) {
	
		$code15_sql="SELECT * FROM reporting_clp_number WHERE clp_number='$course15_code'";
		$code15_result = mysql_query($code15_sql);

		if(mysql_num_rows($code15_result) == 0) {
 	 	$error[112] = "<br><span class=\"error\">Invalid CLP ID</span>\n";
		}
	
	}
	
	
/* Check if course is selected */
	
	if(($course1=="- Select -") && ((!empty($course1_start)) || (!empty($course1_dur)) || (!empty($course1_part)))) {
	$error[113] = "<br><span class=\"error\">Please select a course</span>\n";
	}
	  
	
?>