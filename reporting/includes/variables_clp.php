<?php
	$name = isset($_POST['name']) ? htmlspecialchars($_POST['name'], ENT_QUOTES) : '';
	$email = isset($_POST['email']) ? htmlspecialchars($_POST['email'], ENT_QUOTES) : '';
	$company = isset($_POST['company']) ? htmlspecialchars($_POST['company'], ENT_QUOTES) : '';
	$address1 = isset($_POST['address1']) ? htmlspecialchars($_POST['address1'], ENT_QUOTES) : '';
	$address2 = isset($_POST['address2']) ? htmlspecialchars($_POST['address2'], ENT_QUOTES) : '';
	$country = isset($_POST['country']) ? htmlspecialchars($_POST['country'], ENT_QUOTES) : '';
	$contactname_first = isset($_POST['contactname_first']) ? htmlspecialchars($_POST['contactname_first'], ENT_QUOTES) : '';
	$contactname_last = isset($_POST['contactname_last']) ? htmlspecialchars($_POST['contactname_last'], ENT_QUOTES) : '';
	$contactemail = isset($_POST['contactemail']) ? htmlspecialchars($_POST['contactemail'], ENT_QUOTES) : '';
	$contactfunction = isset($_POST['contactfunction']) ? htmlspecialchars($_POST['contactfunction'], ENT_QUOTES) : '';
	$website = isset($_POST['website']) ? htmlspecialchars($_POST['website'], ENT_QUOTES) : '';
	$budget12 = isset($_POST['budget12']) ? htmlspecialchars($_POST['budget12'], ENT_QUOTES) : '';
	$budget13 = isset($_POST['budget13']) ? htmlspecialchars($_POST['budget13'], ENT_QUOTES) : '';
	$partner = isset($_POST['partner']) ? htmlspecialchars($_POST['partner'], ENT_QUOTES) : '';
	$comments = isset($_POST['comments']) ? htmlspecialchars($_POST['comments'], ENT_QUOTES) : '';
	?>