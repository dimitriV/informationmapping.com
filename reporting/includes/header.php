<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml">
<head>
<meta http-equiv="Content-Type" content="text/html; charset=utf-8" />
<title>Partner Reporting - Information Mapping International nv</title>
<link rel="stylesheet" type="text/css" href="style.css" />
</head>
<body>
 
<div id="wrapper">
<div id="header">
<div id="logo"><img src="images/logo.png" alt="Information Mapping logo" /></div>

<?php
if ($page=="index"){ 
?>
<div id="welcome">
<p>Welcome <?php echo $_SESSION['partner_first']; ?> <?php echo $_SESSION['partner_last']; ?> | <a href="logout.php">Logout</a></p>
</div>
<?php 
}
?>

<div id="nav">
<ul>
<li><a href="<?php echo $rootdir; ?>report.php" class="nav">Partner report</a>&nbsp;&nbsp;&nbsp;&nbsp;|&nbsp;&nbsp;&nbsp;&nbsp;</li>
<li><a href="<?php echo $rootdir; ?>clp-form.php" class="nav">CLP ID application form</a>&nbsp;&nbsp;&nbsp;&nbsp;|&nbsp;&nbsp;&nbsp;&nbsp;</li>
</ul>
</div>
</div>