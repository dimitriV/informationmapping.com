<?php

include("../admin/connect.php");
require('fpdf/fpdf.php');

if(isset($_GET['action'])) $action=$_GET['action'];
if(isset($_POST['action'])) $action=$_POST['action'];
$clp_id=$_GET['clp_id'];
// 9 jan 13 toegevoegd utf8_decode() om mooi af te drukken

// datum ophalen volgens formaat Feb 1, 2013
	$submit_time_result = mysql_query("SELECT DATE_FORMAT(submit_time, '%b %d, %Y') AS datum FROM reporting_clp WHERE clp_id='$clp_id'");
	
    while($row = mysql_fetch_assoc($submit_time_result))
    {
        $date = $row['datum'];
    }
//$date=date('Y M d');

$clp_SQL="SELECT * FROM reporting_clp,reporting_clp_number WHERE reporting_clp.clp_id ='$clp_id' AND reporting_clp_number.clp_id='$clp_id'";
$clp_result=mysql_query($clp_SQL) or die(mysql_error());
$clp=mysql_fetch_array($clp_result);


$clp = $GLOBALS['clp'];

class PDF extends FPDF
{

// Page footer
function Footer()
{
    // Position at 1.5 cm from bottom
    $this->SetY(-15);
    $this->SetFont('Times','',8);
    // Page number
	$this->Cell(150,10);
    $this->Cell(0,10,'Page '.$this->PageNo().'/{nb}',0,0,'R');
}


}

// Instanciation of inherited class
$pdf = new PDF();
$pdf->AliasNbPages();
$pdf->AddPage();
$pdf->SetTitle('Information Mapping Corporate License Terms');
$pdf->SetAuthor('Information Mapping International nv');

// Title
$pdf->SetFont('Arial','B',16);
$pdf->Ln(5);
$pdf->Cell(8,10);
$pdf->Cell(200,10,'Information Mapping Corporate License Terms');
$pdf->Ln(20);

// Block 1
$pdf->Line(48,30,192,30);
$pdf->SetFont('Times','B',11);
$pdf->Cell(8,10);
$pdf->Cell(30,5,'Introduction');
$pdf->SetFont('Times','',12);
$pdf->MultiCell(145,5,'These license terms are provided by Information Mapping International NV (hereinafter referred to as �IMI�), a corporation organized under the laws of Belgium, with registered offices at 46 Beekstraat, 9031 GHENT (Belgium) with company registration number 0811.076.881 with its affiliate, Information Mapping Inc, 135 Beaver Street, suite 212 Waltham, Massachusetts (United States).');
$pdf->Ln(4);
$pdf->Cell(8,10);
$pdf->Cell(30,5,'');
$pdf->Cell(30,5,'CUSTOMER:');
$pdf->Cell(120,5,utf8_decode($clp['company']));

$pdf->Ln(5);
$pdf->Cell(8,10);
$pdf->Cell(30,5,'');
$pdf->Cell(30,5,'ADDRESS:');
$pdf->Cell(120,5,utf8_decode($clp['address1']));

$pdf->Ln(5);
$pdf->Cell(8,10);
$pdf->Cell(30,5,'');
$pdf->Cell(30,5,'');
$pdf->Cell(120,5,utf8_decode($clp['address2']));

$pdf->Ln(5);
$pdf->Cell(8,10);
$pdf->Cell(30,5,'');
$pdf->Cell(30,5,'');
$pdf->Cell(120,5,utf8_decode($clp['country']));

$pdf->Ln(5);
$pdf->Cell(8,10);
$pdf->Cell(30,5,'');
$pdf->Cell(30,5,'DATE:');
$pdf->Cell(120,5,$date);

$pdf->Ln(5);
$pdf->Cell(8,10);
$pdf->Cell(30,5,'');
$pdf->Cell(30,5,'CLP ID:');
$pdf->Cell(120,5,$clp['clp_number']);

// Block 2
$pdf->Ln(15);
$pdf->Line(48,99,192,99);
$pdf->SetFont('Times','B',11);
$pdf->Cell(8,10);
$pdf->Cell(30,5,'1. Scope');
$pdf->SetFont('Times','',12);
$pdf->MultiCell(150,5,'These Corporate License Terms apply to any license granted under the Corporate License Program of IMI.  The IMI General Terms and Conditions apply to these IMI Corporate License Terms and complete these terms to the extent that these IMI Corporate License Terms don�t expressly provide otherwise. ');

// Block 3
$pdf->Ln(10);
$pdf->Line(48,130,192,130);
$pdf->SetFont('Times','B',11);
$pdf->Cell(8,10);
$pdf->Cell(30,5,'2. License');
$pdf->SetFont('Times','',12);
$pdf->MultiCell(145,5,'Upon payment of the due amounts, IMI grants to the Licensee as mentioned on the Proposal, Statement of Work or invoice, regarding the license drawn up by IMI or the official IMI Distributor (�the Invoice�), the following license (�the License�):');
$pdf->Ln(3);
$pdf->Cell(8,10);
$pdf->Cell(30,5,'');
$pdf->MultiCell(145,5,'- IM Method. IMI grants to Licensee a non-transferable and non-exclusive license to use the IM Method for internal use only in accordance with its intended use. Licensee is entitled to draft documents intended for the internal use in Licensee�s organization. This comprises internal documents, memo�s, sheets, reports, courses, books, manuals, product manuals, advertisements or any other generic means of communication and any other documents that are intended to be distributed within Licensee�s organization or to its stakeholders (employees, clients, suppliers, shareholders).');
$pdf->Ln(3);
$pdf->Cell(8,10);
$pdf->Cell(30,5,'');
$pdf->MultiCell(145,5,'- License on the IM Products. IMI grants Licensee the right to use the IM Products, including e-learning courses and FS Pro software. FS Pro software can be used under the IMI Software License Terms. The IMI Software License Terms complete these Corporate License Terms to the extent that these terms don�t expressly provide otherwise. ');
$pdf->Line(48,224,192,224);
$pdf->Ln(5);
$pdf->SetFont('Times','I',10);
$pdf->Cell(8,10);
$pdf->Cell(139,5,'');
$pdf->MultiCell(40,5,'Continued on next page');


// Page 2

$pdf->AddPage();

// Title
$pdf->SetFont('Arial','B',16);
$pdf->Ln(5);
$pdf->Cell(8,10);
$pdf->Cell(126,10,'Information Mapping Corporate License Terms');
$pdf->SetFont('Arial','',12);
$pdf->Cell(20,10,', Continued');
$pdf->Ln(20);

// Block 4
$pdf->Line(48,30,192,30);
$pdf->SetFont('Times','B',11);
$pdf->Cell(8,10);
$pdf->Cell(30,5,"3. Protection");
$pdf->SetFont('Times','',12);
$pdf->MultiCell(145,5,'Licensee agrees that the IM Method is protected under various IP laws and under Article 39 of the TRIPS Agreement (1994) and Article 10bis of the Paris Convention (1967) and is confidential and proprietary and a valuable commercial asset of IMI. Licensee agrees to keep the IM Method in confidence, to not disclose the IM Method to any third parties, and: ');

$pdf->Cell(8,10);
$pdf->Cell(30,5,'');
$pdf->SetFont('Times','',12);
$pdf->MultiCell(145,5,'(i) limit the availability of the IM Method to those of its employees who are contractually permitted and need to have access thereto in order to use the IM Method and who have been informed by Licensee of the proprietary nature of the IM Method; ');

$pdf->Cell(8,10);
$pdf->Cell(30,5,'');
$pdf->SetFont('Times','',12);
$pdf->MultiCell(145,5,'(ii)	have such employees treat as confidential the IM Method; ');

$pdf->Cell(8,10);
$pdf->Cell(30,5,'');
$pdf->SetFont('Times','',12);
$pdf->MultiCell(145,5,'(iii) avoid publication or other disclosure of the IM Method to other than those persons described in (i) above; ');

$pdf->Cell(8,10);
$pdf->Cell(30,5,'');
$pdf->SetFont('Times','',12);
$pdf->MultiCell(150,5,'(iv)	not allow anybody to disclose or make available the IM Method to any unit, division, group, or subsidiary of Licensee or Licensee\'s parent company if engaged in activities which are or may be competitive with the IM Method; and ');

$pdf->Cell(8,10);
$pdf->Cell(30,5,'');
$pdf->SetFont('Times','',12);
$pdf->MultiCell(145,5,'(v) ensure that all personnel having access to the IM Method are informed of the provisions of this Article. Licensee agrees not to use the IM Method and/or the underlying principles to develop another method to structure information that is similar to the IM Method. ');

$pdf->Ln(3);
$pdf->Cell(8,10);
$pdf->Cell(30,5,'');
$pdf->SetFont('Times','',12);
$pdf->MultiCell(145,5,'Licensee\'s obligations set forth in this Article shall survive any termination, expiration or discontinuance of the License.');

// Block 5
$pdf->Ln(10);
$pdf->Line(48,148,192,148);
$pdf->SetFont('Times','B',11);
$pdf->Cell(8,10);
$pdf->Cell(30,5,'4. Training');
$pdf->SetFont('Times','',12);
$pdf->MultiCell(145,5,'The Invoice will indicate the number of users that are entitled to use the IM Method and IM Products under this License (�the Users�). A number of Users mentioned on the Invoice are entitled to follow an official IM classroom training given by an official IM Certified Instructor and receive an IM course package as set forth in the Invoice. On Licensee�s request, additional Users may be trained at the then current commercial terms of Licensee�s IMI Distributor. Users that will not follow an official IM classroom training will not receive an IM course package. However, they can elect to have access to the IM e-learning courses as set forth in the Proposal, Statement of Work or Invoice.');

$pdf->Line(48,202,192,202);
$pdf->Ln(5);
$pdf->SetFont('Times','I',10);
$pdf->Cell(8,10);
$pdf->Cell(139,5,'');
$pdf->MultiCell(40,5,'Continued on next page');

// Page 3

$pdf->AddPage();

// Title
$pdf->SetFont('Arial','B',16);
$pdf->Ln(5);
$pdf->Cell(8,10);
$pdf->Cell(126,10,'Information Mapping Corporate License Terms');
$pdf->SetFont('Arial','',12);
$pdf->Cell(20,10,', Continued');
$pdf->Ln(20);

// Block 6
$pdf->Line(48,30,192,30);
$pdf->SetFont('Times','B',11);
$pdf->Cell(8,10);
$pdf->Cell(30,5,"5. Warranty");
$pdf->SetFont('Times','',12);
$pdf->MultiCell(145,5,'The License is provided �as is�. IMI does not warrant that the IM Method will meet Licensee\'s performance requirements or Licensee\'s expectations. Licensee acknowledges that before purchasing the License, Licensee has studied the IM Method and that � to the extent Licensee needed any further information � Licensee contacted an official IMI Distributor as listed on the IMI website to obtain such further information. Therefore, Licensee accepts the responsibility for the selection of the IM Method, its use and the results to be obtained there from. Except as expressly provided for in this Agreement, IMI makes no warranty of any kind, express or implied, and the warranty of fitness for a particular purpose is hereby excluded.');

// Block 7
$pdf->Ln(10);
$pdf->Line(48,89,192,89);
$pdf->SetFont('Times','B',11);
$pdf->Cell(8,10);
$pdf->Cell(30,5,'6. Term');
$pdf->SetFont('Times','',12);
$pdf->MultiCell(145,5,'The License is granted for the term mentioned on the invoice regarding the license drawn up by IMI or the official IMI Distributor. However, following acts of Licensee will cause the License to lapse immediately by law, without intervention of the court ("de plein droit"), and without notice:');

$pdf->Cell(8,10);
$pdf->Cell(30,5,'');
$pdf->SetFont('Times','',12);
$pdf->MultiCell(145,5,'- Licensee breaches these Corporate License Terms and fails to rectify its default within 30 days after being notified by IMI to cure the default; ');

$pdf->Cell(8,10);
$pdf->Cell(30,5,'');
$pdf->SetFont('Times','',12);
$pdf->MultiCell(145,5,'- Licensee uses the IM Method or IMI Products to draft courses, books, manuals, product manuals, advertisements or any other generic means of communication to be sold or leased to third parties other than stakeholders or to draft documents on behalf of such third parties against a fee or not;');

$pdf->Cell(8,10);
$pdf->Cell(30,5,'');
$pdf->SetFont('Times','',12);
$pdf->MultiCell(145,5,'- Licensee infringes upon the intellectual property rights of IMI or IMI�s rights under Article 39 of the TRIPS Agreement (1994) and Article 10bis of the Paris Convention (1967) on the IM Method;');

$pdf->Cell(8,10);
$pdf->Cell(30,5,'');
$pdf->SetFont('Times','',12);
$pdf->MultiCell(145,5,'- Licensee breaches its confidentiality obligations regarding the IM Method.');
$pdf->Line(48,170,192,170);

$pdf->Output();
?>