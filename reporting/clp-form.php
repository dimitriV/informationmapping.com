<?php
include("admin/connect.php");
include("includes/header_clp.php");

if(isset($_GET['action'])) $action=$_GET['action'];
if(isset($_POST['action'])) $action=$_POST['action'];

// geeft een PHP Notice omdat de form fields niet met isset() gecontroleerd worden
include("includes/variables_clp.php");
include("includes/errorcheck_clp.php");

if(isset($_POST['submit']) && !isset($error)) {
include("includes/ok_clp.php");
// echo $feedback;
} else {
?>
<div id="report">
<h3>CLP ID Application Form</h3>
  <form action="<?php echo $_SERVER['PHP_SELF']; ?>" method="post">
  
  <table width="500" border="0" cellpadding="0" cellspacing="0" class="report">
  <tr>
  <th colspan="6">CLP ID requested by</th>
  </tr>
  <tr class="odd">
    <td width="170"><strong>Name</strong></td>
    <td colspan="5">
      <input type="text" size="40" name="name" value="<?php if(!empty($_POST['name'])) { echo $_POST['name']; } ?>" style="width: 300px;" />
      <?php if(isset($_POST['submit']) && isset($error[1])) { echo $error[1]; } ?>
    </td>
  </tr>
  <tr>
    <td><strong>E-mail</strong></td>
    <td colspan="5">
      <input type="text"  size="40" name="email" value="<?php if(!empty($_POST['email'])) { echo $_POST['email']; } ?>" style="width: 300px;" />
      <?php if(isset($_POST['submit']) && isset($error[2])) { echo $error[2]; } ?>
    </td>
  </tr>
    <tr class="odd">
    <td><strong>Partner</strong></td>
    <td colspan="5">
      <select name="partner" style="width: 300px;">
        <option>- Please select -</option>
        <?php
		$partner_SQL="SELECT * FROM reporting_partners WHERE active='yes' ORDER BY partner_company ASC";
		$partner_result=mysql_query($partner_SQL);
		while($partner=mysql_fetch_array($partner_result)){
		?>
        <option><?php echo $partner['partner_company'] ?></option>
       <?php
		}
		?>
      </select>
      <?php if(isset($_POST['submit']) && isset($error[12])) { echo $error[12]; } ?>
    </td>
  </tr>
  </table>
  
    <table width="500" border="0" cellpadding="0" cellspacing="0" class="report">
  <tr>
  <th colspan="6">CLP ID requested for</th>
  </tr>
  <tr class="odd">
    <td width="170"><strong>Customer</strong></td>
    <td colspan="5">
      <input type="text"  size="40" name="company" value="<?php if(!empty($_POST['company'])) { echo $_POST['company']; } ?>" style="width: 300px;" />
      <?php if(isset($_POST['submit']) && isset($error[3])) { echo $error[3]; } ?>
    </td>
  </tr>
    <tr>
    <td width="170"><strong>Customer address</strong></td>
    <td colspan="5">
      <input type="text"  size="40" name="address1" value="<?php if(!empty($_POST['address1'])) { echo $_POST['address1']; } ?>" style="width: 300px;" /><br />
	<input type="text"  size="40" name="address2" value="<?php if(!empty($_POST['address2'])) { echo $_POST['address2']; } ?>" style="width: 300px;" />
      <?php if(isset($_POST['submit']) && isset($error[10])) { echo $error[10]; } ?>
    </td>
  </tr>
    <tr class="odd">
    <td width="170"><strong>Country</strong></td>
    <td colspan="5">
              <select name="country" style="width: 300px;">
              <option value="">Select country</option>
              <option value="United States"<?php if($_POST['country'] =="United States") { echo " selected"; } ?>>United States</option>
              <option value="Afghanistan"<?php if($_POST['country'] =="Afghanistan") { echo " selected"; } ?>>Afghanistan</option>
              <option value="Albania"<?php if($_POST['country'] =="Albania") { echo " selected"; } ?>>Albania</option>
              <option value="Algeria"<?php if($_POST['country'] =="Algeria") { echo " selected"; } ?>>Algeria</option>
              <option value="American Samoa"<?php if($_POST['country'] =="American Samoa") { echo " selected"; } ?>>American Samoa</option>
              <option value="Andorra"<?php if($_POST['country'] =="Andorra") { echo " selected"; } ?>>Andorra</option>
              <option value="Angola"<?php if($_POST['country'] =="Angola") { echo " selected"; } ?>>Angola</option>
              <option value="Anguilla"<?php if($_POST['country'] =="Anguilla") { echo " selected"; } ?>>Anguilla</option>
              <option value="Antarctica"<?php if($_POST['country'] =="Antarctica") { echo " selected"; } ?>>Antarctica</option>
              <option value="Antigua and Barbuda"<?php if($_POST['country'] =="Antigua and Barbuda") { echo " selected"; } ?>>Antigua and Barbuda</option>
              <option value="Argentina"<?php if($_POST['country'] =="Argentina") { echo " selected"; } ?>>Argentina</option>
              <option value="Armenia"<?php if($_POST['country'] =="Armenia") { echo " selected"; } ?>>Armenia</option>
              <option value="Aruba"<?php if($_POST['country'] =="Aruba") { echo " selected"; } ?>>Aruba</option>
              <option value="Australia"<?php if($_POST['country'] =="Australia") { echo " selected"; } ?>>Australia</option>
              <option value="Austria"<?php if($_POST['country'] =="Austria") { echo " selected"; } ?>>Austria</option>
              <option value="Azerbaijan"<?php if($_POST['country'] =="Azerbaijan") { echo " selected"; } ?>>Azerbaijan</option>
              <option value="Bahamas"<?php if($_POST['country'] =="Bahamas") { echo " selected"; } ?>>Bahamas</option>
              <option value="Bahrain"<?php if($_POST['country'] =="Bahrain") { echo " selected"; } ?>>Bahrain</option>
              <option value="Bangladesh"<?php if($_POST['country'] =="Bangladesh") { echo " selected"; } ?>>Bangladesh</option>
              <option value="Barbados"<?php if($_POST['country'] =="Barbados") { echo " selected"; } ?>>Barbados</option>
              <option value="Belarus"<?php if($_POST['country'] =="Belarus") { echo " selected"; } ?>>Belarus</option>
              <option value="Belgium"<?php if($_POST['country'] =="Belgium") { echo " selected"; } ?>>Belgium</option>
              <option value="Belize"<?php if($_POST['country'] =="Belize") { echo " selected"; } ?>>Belize</option>
              <option value="Benin"<?php if($_POST['country'] =="Benin") { echo " selected"; } ?>>Benin</option>
              <option value="Bermuda"<?php if($_POST['country'] =="Bermuda") { echo " selected"; } ?>>Bermuda</option>
              <option value="Bhutan"<?php if($_POST['country'] =="Bhutan") { echo " selected"; } ?>>Bhutan</option>
              <option value="Bolivia"<?php if($_POST['country'] =="Bolivia") { echo " selected"; } ?>>Bolivia</option>
              <option value="Bosnia and Herzegovina"<?php if($_POST['country'] =="Bosnia and Herzegovina") { echo " selected"; } ?>>Bosnia and Herzegovina</option>
              <option value="Botswana"<?php if($_POST['country'] =="Botswana") { echo " selected"; } ?>>Botswana</option>
              <option value="Bouvet Island"<?php if($_POST['country'] =="Bouvet Island") { echo " selected"; } ?>>Bouvet Island</option>
              <option value="Brazil"<?php if($_POST['country'] =="Brazil") { echo " selected"; } ?>>Brazil</option>
              <option value="British Indian Ocean Territory"<?php if($_POST['country'] =="British Indian Ocean Territory") { echo " selected"; } ?>>British Indian Ocean Territory</option>
              <option value="Brunei Darussalam"<?php if($_POST['country'] =="Brunei Darussalam") { echo " selected"; } ?>>Brunei Darussalam</option>
              <option value="Bulgaria"<?php if($_POST['country'] =="Bulgaria") { echo " selected"; } ?>>Bulgaria</option>
              <option value="Burkina Faso"<?php if($_POST['country'] =="Burkina Faso") { echo " selected"; } ?>>Burkina Faso</option>
              <option value="Burundi"<?php if($_POST['country'] =="Burundi") { echo " selected"; } ?>>Burundi</option>
              <option value="Cambodia"<?php if($_POST['country'] =="Cambodia") { echo " selected"; } ?>>Cambodia</option>
              <option value="Cameroon"<?php if($_POST['country'] =="Cameroon") { echo " selected"; } ?>>Cameroon</option>
              <option value="Canada"<?php if($_POST['country'] =="Canada") { echo " selected"; } ?>>Canada</option>
              <option value="Cape Verde"<?php if($_POST['country'] =="Cape Verde") { echo " selected"; } ?>>Cape Verde</option>
              <option value="Cayman Islands"<?php if($_POST['country'] =="Cayman Islands") { echo " selected"; } ?>>Cayman Islands</option>
              <option value="Central African Republic"<?php if($_POST['country'] =="Central African Republic") { echo " selected"; } ?>>Central African Republic</option>
              <option value="Chad"<?php if($_POST['country'] =="Chad") { echo " selected"; } ?>>Chad</option>
              <option value="Chile"<?php if($_POST['country'] =="Chile") { echo " selected"; } ?>>Chile</option>
              <option value="China"<?php if($_POST['country'] =="China") { echo " selected"; } ?>>China</option>
              <option value="Christmas Island"<?php if($_POST['country'] =="Christmas Island") { echo " selected"; } ?>>Christmas Island</option>
              <option value="Cocos (Keeling) Islands"<?php if($_POST['country'] =="Cocos (Keeling) Islands") { echo " selected"; } ?>>Cocos (Keeling) Islands</option>
              <option value="Colombia"<?php if($_POST['country'] =="Colombia") { echo " selected"; } ?>>Colombia</option>
              <option value="Comoros"<?php if($_POST['country'] =="Comoros") { echo " selected"; } ?>>Comoros</option>
              <option value="Congo"<?php if($_POST['country'] =="Congo") { echo " selected"; } ?>>Congo</option>
              <option value="Congo, The Democratic Republic of The"<?php if($_POST['country'] =="Congo, The Democratic Republic of The") { echo " selected"; } ?>>Congo, The Democratic Republic of The</option>
              <option value="Cook Islands"<?php if($_POST['country'] =="Cook Islands") { echo " selected"; } ?>>Cook Islands</option>
              <option value="Costa Rica"<?php if($_POST['country'] =="Costa Rica") { echo " selected"; } ?>>Costa Rica</option>
              <option value="Cote D'ivoire"<?php if($_POST['country'] =="Cote D'ivoire") { echo " selected"; } ?>>Cote D'ivoire</option>
              <option value="Croatia"<?php if($_POST['country'] =="Croatia") { echo " selected"; } ?>>Croatia</option>
              <option value="Cuba"<?php if($_POST['country'] =="Cuba") { echo " selected"; } ?>>Cuba</option>
              <option value="Cyprus"<?php if($_POST['country'] =="Cyprus") { echo " selected"; } ?>>Cyprus</option>
              <option value="Czech Republic"<?php if($_POST['country'] =="Czech Republic") { echo " selected"; } ?>>Czech Republic</option>
              <option value="Denmark"<?php if($_POST['country'] =="Denmark") { echo " selected"; } ?>>Denmark</option>
              <option value="Djibouti"<?php if($_POST['country'] =="Djibouti") { echo " selected"; } ?>>Djibouti</option>
              <option value="Dominica"<?php if($_POST['country'] =="Dominica") { echo " selected"; } ?>>Dominica</option>
              <option value="Dominican Republic"<?php if($_POST['country'] =="Dominican Republic") { echo " selected"; } ?>>Dominican Republic</option>
              <option value="Ecuador"<?php if($_POST['country'] =="Ecuador") { echo " selected"; } ?>>Ecuador</option>
              <option value="Egypt"<?php if($_POST['country'] =="Egypt") { echo " selected"; } ?>>Egypt</option>
              <option value="El Salvador"<?php if($_POST['country'] =="El Salvador") { echo " selected"; } ?>>El Salvador</option>
              <option value="Equatorial Guinea"<?php if($_POST['country'] =="Equatorial Guinea") { echo " selected"; } ?>>Equatorial Guinea</option>
              <option value="Eritrea"<?php if($_POST['country'] =="Eritrea") { echo " selected"; } ?>>Eritrea</option>
              <option value="Estonia"<?php if($_POST['country'] =="Estonia") { echo " selected"; } ?>>Estonia</option>
              <option value="Ethiopia"<?php if($_POST['country'] =="Ethiopia") { echo " selected"; } ?>>Ethiopia</option>
              <option value="Falkland Islands (Malvinas)"<?php if($_POST['country'] =="Falkland Islands (Malvinas)") { echo " selected"; } ?>>Falkland Islands (Malvinas)</option>
              <option value="Faroe Islands"<?php if($_POST['country'] =="Faroe Islands") { echo " selected"; } ?>>Faroe Islands</option>
              <option value="Fiji"<?php if($_POST['country'] =="Fiji") { echo " selected"; } ?>>Fiji</option>
              <option value="Finland"<?php if($_POST['country'] =="Finland") { echo " selected"; } ?>>Finland</option>
              <option value="France"<?php if($_POST['country'] =="France") { echo " selected"; } ?>>France</option>
              <option value="French Guiana"<?php if($_POST['country'] =="French Guiana") { echo " selected"; } ?>>French Guiana</option>
              <option value="French Polynesia"<?php if($_POST['country'] =="French Polynesia") { echo " selected"; } ?>>French Polynesia</option>
              <option value="French Southern Territories"<?php if($_POST['country'] =="French Southern Territories") { echo " selected"; } ?>>French Southern Territories</option>
              <option value="Gabon"<?php if($_POST['country'] =="Gabon") { echo " selected"; } ?>>Gabon</option>
              <option value="Gambia"<?php if($_POST['country'] =="Gambia") { echo " selected"; } ?>>Gambia</option>
              <option value="Georgia"<?php if($_POST['country'] =="Georgia") { echo " selected"; } ?>>Georgia</option>
              <option value="Germany"<?php if($_POST['country'] =="Germany") { echo " selected"; } ?>>Germany</option>
              <option value="Ghana"<?php if($_POST['country'] =="Ghana") { echo " selected"; } ?>>Ghana</option>
              <option value="Gibraltar"<?php if($_POST['country'] =="Gibraltar") { echo " selected"; } ?>>Gibraltar</option>
              <option value="Greece"<?php if($_POST['country'] =="Greece") { echo " selected"; } ?>>Greece</option>
              <option value="Greenland"<?php if($_POST['country'] =="Greenland") { echo " selected"; } ?>>Greenland</option>
              <option value="Grenada"<?php if($_POST['country'] =="Grenada") { echo " selected"; } ?>>Grenada</option>
              <option value="Guadeloupe"<?php if($_POST['country'] =="Guadeloupe") { echo " selected"; } ?>>Guadeloupe</option>
              <option value="Guam"<?php if($_POST['country'] =="Guam") { echo " selected"; } ?>>Guam</option>
              <option value="Guatemala"<?php if($_POST['country'] =="Guatemala") { echo " selected"; } ?>>Guatemala</option>
              <option value="Guinea"<?php if($_POST['country'] =="Guinea") { echo " selected"; } ?>>Guinea</option>
              <option value="Guinea-bissau"<?php if($_POST['country'] =="Guinea-bissau") { echo " selected"; } ?>>Guinea-bissau</option>
              <option value="Guyana"<?php if($_POST['country'] =="Guyana") { echo " selected"; } ?>>Guyana</option>
              <option value="Haiti"<?php if($_POST['country'] =="Haiti") { echo " selected"; } ?>>Haiti</option>
              <option value="Heard Island and Mcdonald Islands"<?php if($_POST['country'] =="Heard Island and Mcdonald Islands") { echo " selected"; } ?>>Heard Island and Mcdonald Islands</option>
              <option value="Holy See (Vatican City State)"<?php if($_POST['country'] =="Holy See (Vatican City State)") { echo " selected"; } ?>>Holy See (Vatican City State)</option>
              <option value="Honduras"<?php if($_POST['country'] =="Honduras") { echo " selected"; } ?>>Honduras</option>
              <option value="Hong Kong"<?php if($_POST['country'] =="Hong Kong") { echo " selected"; } ?>>Hong Kong</option>
              <option value="Hungary"<?php if($_POST['country'] =="Hungary") { echo " selected"; } ?>>Hungary</option>
              <option value="Iceland"<?php if($_POST['country'] =="Iceland") { echo " selected"; } ?>>Iceland</option>
              <option value="India"<?php if($_POST['country'] =="India") { echo " selected"; } ?>>India</option>
              <option value="Indonesia"<?php if($_POST['country'] =="Indonesia") { echo " selected"; } ?>>Indonesia</option>
              <option value="Iran, Islamic Republic of"<?php if($_POST['country'] =="Iran, Islamic Republic of") { echo " selected"; } ?>>Iran, Islamic Republic of</option>
              <option value="Iraq"<?php if($_POST['country'] =="Iraq") { echo " selected"; } ?>>Iraq</option>
              <option value="Ireland"<?php if($_POST['country'] =="Ireland") { echo " selected"; } ?>>Ireland</option>
              <option value="Israel"<?php if($_POST['country'] =="Israel") { echo " selected"; } ?>>Israel</option>
              <option value="Italy"<?php if($_POST['country'] =="Italy") { echo " selected"; } ?>>Italy</option>
              <option value="Jamaica"<?php if($_POST['country'] =="Jamaica") { echo " selected"; } ?>>Jamaica</option>
              <option value="Japan"<?php if($_POST['country'] =="Japan") { echo " selected"; } ?>>Japan</option>
              <option value="Jordan"<?php if($_POST['country'] =="Jordan") { echo " selected"; } ?>>Jordan</option>
              <option value="Kazakhstan"<?php if($_POST['country'] =="Kazakhstan") { echo " selected"; } ?>>Kazakhstan</option>
              <option value="Kenya"<?php if($_POST['country'] =="Kenya") { echo " selected"; } ?>>Kenya</option>
              <option value="Kiribati"<?php if($_POST['country'] =="Kiribati") { echo " selected"; } ?>>Kiribati</option>
              <option value="Korea, Democratic People's Republic of"<?php if($_POST['country'] =="Korea, Democratic People's Republic of") { echo " selected"; } ?>>Korea, Democratic People's Republic of</option>
              <option value="Korea, Republic of"<?php if($_POST['country'] =="Korea, Republic of") { echo " selected"; } ?>>Korea, Republic of</option>
              <option value="Kuwait"<?php if($_POST['country'] =="Kuwait") { echo " selected"; } ?>>Kuwait</option>
              <option value="Kyrgyzstan"<?php if($_POST['country'] =="Kyrgyzstan") { echo " selected"; } ?>>Kyrgyzstan</option>
              <option value="Lao People's Democratic Republic"<?php if($_POST['country'] =="Lao People's Democratic Republic") { echo " selected"; } ?>>Lao People's Democratic Republic</option>
              <option value="Latvia"<?php if($_POST['country'] =="Latvia") { echo " selected"; } ?>>Latvia</option>
              <option value="Lebanon"<?php if($_POST['country'] =="Lebanon") { echo " selected"; } ?>>Lebanon</option>
              <option value="Lesotho"<?php if($_POST['country'] =="Lesotho") { echo " selected"; } ?>>Lesotho</option>
              <option value="Liberia"<?php if($_POST['country'] =="Liberia") { echo " selected"; } ?>>Liberia</option>
              <option value="Libyan Arab Jamahiriya"<?php if($_POST['country'] =="Libyan Arab Jamahiriya") { echo " selected"; } ?>>Libyan Arab Jamahiriya</option>
              <option value="Liechtenstein"<?php if($_POST['country'] =="Liechtenstein") { echo " selected"; } ?>>Liechtenstein</option>
              <option value="Lithuania"<?php if($_POST['country'] =="Lithuania") { echo " selected"; } ?>>Lithuania</option>
              <option value="Luxembourg"<?php if($_POST['country'] =="Luxembourg") { echo " selected"; } ?>>Luxembourg</option>
              <option value="Macao"<?php if($_POST['country'] =="Macao") { echo " selected"; } ?>>Macao</option>
              <option value="Macedonia, The Former Yugoslav Republic of"<?php if($_POST['country'] =="Macedonia, The Former Yugoslav Republic of") { echo " selected"; } ?>>Macedonia, The Former Yugoslav Republic of</option>
              <option value="Madagascar"<?php if($_POST['country'] =="Madagascar") { echo " selected"; } ?>>Madagascar</option>
              <option value="Malawi"<?php if($_POST['country'] =="Malawi") { echo " selected"; } ?>>Malawi</option>
              <option value="Malaysia"<?php if($_POST['country'] =="Malaysia") { echo " selected"; } ?>>Malaysia</option>
              <option value="Maldives"<?php if($_POST['country'] =="Maldives") { echo " selected"; } ?>>Maldives</option>
              <option value="Mali"<?php if($_POST['country'] =="Mali") { echo " selected"; } ?>>Mali</option>
              <option value="Malta"<?php if($_POST['country'] =="Malta") { echo " selected"; } ?>>Malta</option>
              <option value="Marshall Islands"<?php if($_POST['country'] =="Marshall Islands") { echo " selected"; } ?>>Marshall Islands</option>
              <option value="Martinique"<?php if($_POST['country'] =="Martinique") { echo " selected"; } ?>>Martinique</option>
              <option value="Mauritania"<?php if($_POST['country'] =="Mauritania") { echo " selected"; } ?>>Mauritania</option>
              <option value="Mauritius"<?php if($_POST['country'] =="Mauritius") { echo " selected"; } ?>>Mauritius</option>
              <option value="Mayotte"<?php if($_POST['country'] =="Mayotte") { echo " selected"; } ?>>Mayotte</option>
              <option value="Mexico"<?php if($_POST['country'] =="Mexico") { echo " selected"; } ?>>Mexico</option>
              <option value="Micronesia, Federated States of"<?php if($_POST['country'] =="Micronesia, Federated States of") { echo " selected"; } ?>>Micronesia, Federated States of</option>
              <option value="Moldova, Republic of"<?php if($_POST['country'] =="Moldova, Republic of") { echo " selected"; } ?>>Moldova, Republic of</option>
              <option value="Monaco"<?php if($_POST['country'] =="Monaco") { echo " selected"; } ?>>Monaco</option>
              <option value="Mongolia"<?php if($_POST['country'] =="Mongolia") { echo " selected"; } ?>>Mongolia</option>
              <option value="Montserrat"<?php if($_POST['country'] =="Montserrat") { echo " selected"; } ?>>Montserrat</option>
              <option value="Morocco"<?php if($_POST['country'] =="Morocco") { echo " selected"; } ?>>Morocco</option>
              <option value="Mozambique"<?php if($_POST['country'] =="Mozambique") { echo " selected"; } ?>>Mozambique</option>
              <option value="Myanmar"<?php if($_POST['country'] =="Myanmar") { echo " selected"; } ?>>Myanmar</option>
              <option value="Namibia"<?php if($_POST['country'] =="Namibia") { echo " selected"; } ?>>Namibia</option>
              <option value="Nauru"<?php if($_POST['country'] =="Nauru") { echo " selected"; } ?>>Nauru</option>
              <option value="Nepal"<?php if($_POST['country'] =="Nepal") { echo " selected"; } ?>>Nepal</option>
              <option value="Netherlands"<?php if($_POST['country'] =="Netherlands") { echo " selected"; } ?>>Netherlands</option>
              <option value="Netherlands Antilles"<?php if($_POST['country'] =="Netherlands Antilles") { echo " selected"; } ?>>Netherlands Antilles</option>
              <option value="New Caledonia"<?php if($_POST['country'] =="New Caledonia") { echo " selected"; } ?>>New Caledonia</option>
              <option value="New Zealand"<?php if($_POST['country'] =="New Zealand") { echo " selected"; } ?>>New Zealand</option>
              <option value="Nicaragua"<?php if($_POST['country'] =="Nicaragua") { echo " selected"; } ?>>Nicaragua</option>
              <option value="Niger"<?php if($_POST['country'] =="Niger") { echo " selected"; } ?>>Niger</option>
              <option value="Nigeria"<?php if($_POST['country'] =="Nigeria") { echo " selected"; } ?>>Nigeria</option>
              <option value="Niue"<?php if($_POST['country'] =="Niue") { echo " selected"; } ?>>Niue</option>
              <option value="Norfolk Island"<?php if($_POST['country'] =="Norfolk Island") { echo " selected"; } ?>>Norfolk Island</option>
              <option value="Northern Mariana Islands"<?php if($_POST['country'] =="Northern Mariana Islands") { echo " selected"; } ?>>Northern Mariana Islands</option>
              <option value="Norway"<?php if($_POST['country'] =="Norway") { echo " selected"; } ?>>Norway</option>
              <option value="Oman"<?php if($_POST['country'] =="Oman") { echo " selected"; } ?>>Oman</option>
              <option value="Pakistan"<?php if($_POST['country'] =="Pakistan") { echo " selected"; } ?>>Pakistan</option>
              <option value="Palau"<?php if($_POST['country'] =="Palau") { echo " selected"; } ?>>Palau</option>
              <option value="Palestinian Territory, Occupied"<?php if($_POST['country'] =="Palestinian Territory, Occupied") { echo " selected"; } ?>>Palestinian Territory, Occupied</option>
              <option value="Panama"<?php if($_POST['country'] =="Panama") { echo " selected"; } ?>>Panama</option>
              <option value="Papua New Guinea"<?php if($_POST['country'] =="Papua New Guinea") { echo " selected"; } ?>>Papua New Guinea</option>
              <option value="Paraguay"<?php if($_POST['country'] =="Paraguay") { echo " selected"; } ?>>Paraguay</option>
              <option value="Peru"<?php if($_POST['country'] =="Peru") { echo " selected"; } ?>>Peru</option>
              <option value="Philippines"<?php if($_POST['country'] =="Philippines") { echo " selected"; } ?>>Philippines</option>
              <option value="Pitcairn"<?php if($_POST['country'] =="Pitcairn") { echo " selected"; } ?>>Pitcairn</option>
              <option value="Poland"<?php if($_POST['country'] =="Poland") { echo " selected"; } ?>>Poland</option>
              <option value="Portugal"<?php if($_POST['country'] =="Portugal") { echo " selected"; } ?>>Portugal</option>
              <option value="Puerto Rico"<?php if($_POST['country'] =="Puerto Rico") { echo " selected"; } ?>>Puerto Rico</option>
              <option value="Qatar"<?php if($_POST['country'] =="Qatar") { echo " selected"; } ?>>Qatar</option>
              <option value="Reunion"<?php if($_POST['country'] =="Reunion") { echo " selected"; } ?>>Reunion</option>
              <option value="Romania"<?php if($_POST['country'] =="Romania") { echo " selected"; } ?>>Romania</option>
              <option value="Russian Federation"<?php if($_POST['country'] =="Russian Federation") { echo " selected"; } ?>>Russian Federation</option>
              <option value="Rwanda"<?php if($_POST['country'] =="Rwanda") { echo " selected"; } ?>>Rwanda</option>
              <option value="Saint Helena"<?php if($_POST['country'] =="Saint Helena") { echo " selected"; } ?>>Saint Helena</option>
              <option value="Saint Kitts and Nevis"<?php if($_POST['country'] =="Saint Kitts and Nevis") { echo " selected"; } ?>>Saint Kitts and Nevis</option>
              <option value="Saint Lucia"<?php if($_POST['country'] =="Saint Lucia") { echo " selected"; } ?>>Saint Lucia</option>
              <option value="Saint Pierre and Miquelon"<?php if($_POST['country'] =="Saint Pierre and Miquelon") { echo " selected"; } ?>>Saint Pierre and Miquelon</option>
              <option value="Saint Vincent and The Grenadines"<?php if($_POST['country'] =="Saint Vincent and The Grenadines") { echo " selected"; } ?>>Saint Vincent and The Grenadines</option>
              <option value="Samoa"<?php if($_POST['country'] =="Samoa") { echo " selected"; } ?>>Samoa</option>
              <option value="San Marino"<?php if($_POST['country'] =="San Marino") { echo " selected"; } ?>>San Marino</option>
              <option value="Sao Tome and Principe"<?php if($_POST['country'] =="Sao Tome and Principe") { echo " selected"; } ?>>Sao Tome and Principe</option>
              <option value="Saudi Arabia"<?php if($_POST['country'] =="Saudi Arabia") { echo " selected"; } ?>>Saudi Arabia</option>
              <option value="Senegal"<?php if($_POST['country'] =="Senegal") { echo " selected"; } ?>>Senegal</option>
              <option value="Serbia and Montenegro"<?php if($_POST['country'] =="Serbia and Montenegro") { echo " selected"; } ?>>Serbia and Montenegro</option>
              <option value="Seychelles"<?php if($_POST['country'] =="Seychelles") { echo " selected"; } ?>>Seychelles</option>
              <option value="Sierra Leone"<?php if($_POST['country'] =="Sierra Leone") { echo " selected"; } ?>>Sierra Leone</option>
              <option value="Singapore"<?php if($_POST['country'] =="Singapore") { echo " selected"; } ?>>Singapore</option>
              <option value="Slovakia"<?php if($_POST['country'] =="Slovakia") { echo " selected"; } ?>>Slovakia</option>
              <option value="Slovenia"<?php if($_POST['country'] =="Slovenia") { echo " selected"; } ?>>Slovenia</option>
              <option value="Solomon Islands"<?php if($_POST['country'] =="Solomon Islands") { echo " selected"; } ?>>Solomon Islands</option>
              <option value="Somalia"<?php if($_POST['country'] =="Somalia") { echo " selected"; } ?>>Somalia</option>
              <option value="South Africa"<?php if($_POST['country'] =="South Africa") { echo " selected"; } ?>>South Africa</option>
              <option value="South Georgia and The South Sandwich Islands"<?php if($_POST['country'] =="South Georgia and The South Sandwich Islands") { echo " selected"; } ?>>South Georgia and The South Sandwich Islands</option>
              <option value="Spain"<?php if($_POST['country'] =="Spain") { echo " selected"; } ?>>Spain</option>
              <option value="Sri Lanka"<?php if($_POST['country'] =="Sri Lanka") { echo " selected"; } ?>>Sri Lanka</option>
              <option value="Sudan"<?php if($_POST['country'] =="Sudan") { echo " selected"; } ?>>Sudan</option>
              <option value="Suriname"<?php if($_POST['country'] =="Suriname") { echo " selected"; } ?>>Suriname</option>
              <option value="Svalbard and Jan Mayen"<?php if($_POST['country'] =="Svalbard and Jan Mayen") { echo " selected"; } ?>>Svalbard and Jan Mayen</option>
              <option value="Swaziland"<?php if($_POST['country'] =="Swaziland") { echo " selected"; } ?>>Swaziland</option>
              <option value="Sweden"<?php if($_POST['country'] =="Sweden") { echo " selected"; } ?>>Sweden</option>
              <option value="Switzerland"<?php if($_POST['country'] =="Switzerland") { echo " selected"; } ?>>Switzerland</option>
              <option value="Syrian Arab Republic"<?php if($_POST['country'] =="Syrian Arab Republic") { echo " selected"; } ?>>Syrian Arab Republic</option>
              <option value="Taiwan, Province of China"<?php if($_POST['country'] =="Taiwan, Province of China") { echo " selected"; } ?>>Taiwan, Province of China</option>
              <option value="Tajikistan"<?php if($_POST['country'] =="Tajikistan") { echo " selected"; } ?>>Tajikistan</option>
              <option value="Tanzania, United Republic of"<?php if($_POST['country'] =="Tanzania, United Republic of") { echo " selected"; } ?>>Tanzania, United Republic of</option>
              <option value="Thailand"<?php if($_POST['country'] =="Thailand") { echo " selected"; } ?>>Thailand</option>
              <option value="Timor-leste"<?php if($_POST['country'] =="Timor-leste") { echo " selected"; } ?>>Timor-leste</option>
              <option value="Togo"<?php if($_POST['country'] =="Togo") { echo " selected"; } ?>>Togo</option>
              <option value="Tokelau"<?php if($_POST['country'] =="Tokelau") { echo " selected"; } ?>>Tokelau</option>
              <option value="Tonga"<?php if($_POST['country'] =="Tonga") { echo " selected"; } ?>>Tonga</option>
              <option value="Trinidad and Tobago"<?php if($_POST['country'] =="Trinidad and Tobago") { echo " selected"; } ?>>Trinidad and Tobago</option>
              <option value="Tunisia"<?php if($_POST['country'] =="Tunisia") { echo " selected"; } ?>>Tunisia</option>
              <option value="Turkey"<?php if($_POST['country'] =="Turkey") { echo " selected"; } ?>>Turkey</option>
              <option value="Turkmenistan"<?php if($_POST['country'] =="Turkmenistan") { echo " selected"; } ?>>Turkmenistan</option>
              <option value="Turks and Caicos Islands"<?php if($_POST['country'] =="Turks and Caicos Islands") { echo " selected"; } ?>>Turks and Caicos Islands</option>
              <option value="Tuvalu"<?php if($_POST['country'] =="Tuvalu") { echo " selected"; } ?>>Tuvalu</option>
              <option value="Uganda"<?php if($_POST['country'] =="Uganda") { echo " selected"; } ?>>Uganda</option>
              <option value="Ukraine"<?php if($_POST['country'] =="Ukraine") { echo " selected"; } ?>>Ukraine</option>
              <option value="United Arab Emirates"<?php if($_POST['country'] =="United Arab Emirates") { echo " selected"; } ?>>United Arab Emirates</option>
              <option value="United Kingdom"<?php if($_POST['country'] =="United Kingdom") { echo " selected"; } ?>>United Kingdom</option>
              <option value="United States"<?php if($_POST['country'] =="United States") { echo " selected"; } ?>>United States</option>
              <option value="United States Minor Outlying Islands"<?php if($_POST['country'] =="United States Minor Outlying Islands") { echo " selected"; } ?>>United States Minor Outlying Islands</option>
              <option value="Uruguay"<?php if($_POST['country'] =="Uruguay") { echo " selected"; } ?>>Uruguay</option>
              <option value="Uzbekistan"<?php if($_POST['country'] =="Uzbekistan") { echo " selected"; } ?>>Uzbekistan</option>
              <option value="Vanuatu"<?php if($_POST['country'] =="Vanuatu") { echo " selected"; } ?>>Vanuatu</option>
              <option value="Venezuela"<?php if($_POST['country'] =="Venezuela") { echo " selected"; } ?>>Venezuela</option>
              <option value="Viet Nam"<?php if($_POST['country'] =="Viet Nam") { echo " selected"; } ?>>Viet Nam</option>
              <option value="Virgin Islands, British"<?php if($_POST['country'] =="Virgin Islands, British") { echo " selected"; } ?>>Virgin Islands, British</option>
              <option value="Virgin Islands, U.S."<?php if($_POST['country'] =="Virgin Islands, U.S.") { echo " selected"; } ?>>Virgin Islands, U.S.</option>
              <option value="Wallis and Futuna"<?php if($_POST['country'] =="Wallis and Futuna") { echo " selected"; } ?>>Wallis and Futuna</option>
              <option value="Western Sahara"<?php if($_POST['country'] =="Western Sahara") { echo " selected"; } ?>>Western Sahara</option>
              <option value="Yemen"<?php if($_POST['country'] =="Yemen") { echo " selected"; } ?>>Yemen</option>
              <option value="Zambia"<?php if($_POST['country'] =="Zambia") { echo " selected"; } ?>>Zambia</option>
              <option value="Zimbabwe"<?php if($_POST['country'] =="Zimbabwe") { echo " selected"; } ?>>Zimbabwe</option>
            </select>
      <?php if(isset($_POST['submit']) && isset($error[11])) { echo $error[11]; } ?>
    </td>
  </tr>
  <tr>
    <td><strong>Customer website</strong></td>
    <td colspan="5">
      <input type="text"  size="40" name="website" value="<?php if(!empty($_POST['website'])) { echo $_POST['website']; } ?>" style="width: 300px;" />
      <?php if(isset($_POST['submit']) && isset($error[4])) { echo $error[4]; } ?>
    </td>
  </tr>
  <tr class="odd">
    <td width="120"><strong>Contact first name</strong></td>
    <td colspan="5">
      <input type="text" size="40" name="contactname_first" value="<?php if(!empty($_POST['contactname_first'])) { echo $_POST['contactname_first']; } ?>" style="width: 300px;" />
      
    </td>
  </tr>
    <tr>
      <td><strong>Contact last name</strong></td>
      <td colspan="5"><input type="text" size="40" name="contactname_last" value="<?php if(!empty($_POST['contactname_last'])) { echo $_POST['contactname_last']; } ?>" style="width: 300px;" /></td>
    </tr>
    <tr>
    <td width="120"><strong>Contact function</strong></td>
    <td colspan="5">
      <input type="text" size="40" name="contactfunction" value="<?php if(!empty($_POST['contactfunction'])) { echo $_POST['contactfunction']; } ?>" style="width: 300px;" />
      
    </td>
  </tr>
  <tr class="odd">
    <td><strong>Contact e-mail</strong></td>
    <td colspan="5">
      <input type="text"  size="40" name="contactemail" value="<?php if(!empty($_POST['contactemail'])) { echo $_POST['contactemail']; } ?>" style="width: 300px;" />
      
    </td>
  </tr>
    <tr>
    <td><strong>Expected budget <?php echo date("Y"); ?></strong></td>
    <td colspan="5">
      &euro; (Euro) <input type="text"  size="30" name="budget12" value="<?php if(!empty($_POST['budget12'])) { echo $_POST['budget12']; } ?>" style="width: 250px;" />
      <?php if(isset($_POST['submit']) && isset($error[7])) { echo $error[7]; } ?>
    </td>
  </tr>
   <tr class="odd">
    <td><strong>Expected budget <?php echo date("Y")+1; ?></strong></td>
    <td colspan="5">
      &euro; (Euro) <input type="text"  size="30" name="budget13" value="<?php if(!empty($_POST['budget13'])) { echo $_POST['budget13']; } ?>" style="width: 250px;" />
      <?php if(isset($_POST['submit']) && isset($error[8])) { echo $error[8]; } ?>
    </td>
  </tr>
  <tr>
    <td><strong>Comments</strong></td>
    <td colspan="5">
    <textarea name="comments" cols="35" rows="4"><?php if(!empty($_POST['comments'])) { echo $_POST['comments']; } ?></textarea>
    </td>
  </tr>
  </table>
<input type="hidden" name="action" value="insert"> <input type="submit" name="submit" value="Request CLP ID">
</form>
</div>
</body>
</html>
<?php 
  }
?>