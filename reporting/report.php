<?php
$page="index";
include("config.php");
include("admin/connect.php");
include("includes/header.php");

if(isset($_GET['action'])) $action=$_GET['action'];
if(isset($_POST['action'])) $action=$_POST['action'];

include("includes/variables.php");
include("includes/errorcheck.php");

if(isset($_POST['submit']) && !isset($error)) {
    include("includes/ok.php");
} else {
    ?>

    <div id="report">
        <h3>Partner Report</h3>


        <?php
        if (!isset($_SESSION["partner_email"])){
            echo "<p>You are not authorized to view this page. Please <a href=\"index.php\">login</a> with a valid username and password.</p>";
        }
        else {
            ?>



            <form action="<?php echo $_SERVER['PHP_SELF']; ?>" method="post">

                <table width="500" border="0" cellpadding="0" cellspacing="0" class="report">
                    <tr>
                        <th colspan="6">Report submitted by</th>
                    </tr>
                    <tr class="odd">
                        <td width="120"><strong>First name</strong></td>
                        <td colspan="5">
                            <?php echo $_SESSION['partner_first']; ?>
                        </td>
                    </tr>
                    <tr>
                        <td><strong>Last name</strong></td>
                        <td>

                            <?php echo $_SESSION['partner_last']; ?>
                        </td>
                    </tr>
                    <tr class="odd">
                        <td><strong>E-mail</strong></td>
                        <td colspan="5">
                            <?php echo $_SESSION['partner_email']; ?>
                        </td>
                    </tr>
                </table>

                <table width="500" border="0" cellpadding="0" cellspacing="0" class="report">
                    <tr>
                        <th colspan="6">Reporting for</th>
                    </tr>

                    <tr class="odd">
                        <td width="120"><strong>Company</strong></td>
                        <td colspan="5">
                            <select name="partner_id">
                                <option>- Select -</option>
                                <?php
                                $partner_SQL="SELECT * FROM reporting_partners WHERE active='yes' ORDER BY partner_company";
                                $partner_result=mysql_query($partner_SQL);
                                while($partner=mysql_fetch_array($partner_result)){
                                    ?>
                                    <option value="<?php echo $partner['partner_id'] ?>" <?php if(($_SESSION['partner_company'])==$partner['partner_company']) { echo "selected='selected'";} ?>><?php echo $partner['partner_company'] ?></option>
                                <?php
                                }
                                ?>
                            </select>

                        </td>
                    </tr>
                    <tr>
                        <td><strong>Period</strong></td>
                        <td colspan="5">
                            <?php
                            $current_month = date("F");
                            $current_year = date("Y");
                            $display_year = $current_year;

                            if($current_month == "January") { $display_month = "December"; $display_year = date("Y")-1; }
                            if($current_month == "February") { $display_month = "January"; }
                            if($current_month == "March") { $display_month = "February"; }
                            if($current_month == "April") { $display_month = "March"; }
                            if($current_month == "May") { $display_month = "April"; }
                            if($current_month == "June") { $display_month = "May"; }
                            if($current_month == "July") { $display_month = "June"; }
                            if($current_month == "August") { $display_month = "July"; }
                            if($current_month == "September") { $display_month = "August"; }
                            if($current_month == "October") { $display_month = "September"; }
                            if($current_month == "November") { $display_month = "October"; }
                            if($current_month == "December") { $display_month = "November"; }


                            echo $display_month;
                            echo " ";
                            echo $display_year;
                            ?>
                            <input type="hidden" value="<?php echo $display_month; ?>" name="month" />
                            <input type="hidden" value="<?php echo $display_year; ?>" name="year" />
                        </td>
                    </tr>
                </table>

                <table width="960" border="0" cellpadding="0" cellspacing="0" class="report">
                    <tr>
                        <th colspan="7">Classroom training</th>
                    </tr>
                    <tr class="odd">
                        <td><strong>Title</strong></td>
                        <td><strong>Starting date</strong></td>
                        <td><strong>Duration</strong></td>
                        <td><strong>Participants</strong></td>
                        <?php
                        if(($_SESSION['rev_vis'])=='yes') {
                            ?>
                            <td><strong>Client revenue (Euro)</strong></td>

                        <?php
                        }
                        ?>
                        <td><strong>Type</strong></td>
                        <td><strong>CLP ID</strong></td>
                    </tr>
                    <tr>
                        <td>
                            <select name="course1">
                                <option>- Select -</option>
                                <?php
                                $course_SQL="SELECT * FROM reporting_courses WHERE active = 'yes' ORDER BY course_name ASC";
                                $course_result=mysql_query($course_SQL);
                                while($course=mysql_fetch_array($course_result)){
                                    ?>
                                    <option <?php if(($_POST['course1'])==$course['course_name']) { echo "selected='selected'";} ?>><?php echo $course['course_name'] ?></option>
                                <?php
                                }
                                ?>
                            </select>
                            <input type="hidden" name="course1_id" value="" />
                            <?php if(isset($_POST['submit']) && isset($error[113])) { echo $error[113]; } ?>
                        </td>
                        <td>
                            <input type="date" name="course1_start" id="date" size="8" value="<?php if(!empty($_POST['course1_start'])) { echo $_POST['course1_start']; } ?>">
                            <?php if(isset($_POST['submit']) && isset($error[7])) { echo $error[7]; } ?>
                        </td>
                        <td><input name="course1_dur" type="text" size="1" value="<?php if(!empty($_POST['course1_dur'])) { echo $_POST['course1_dur']; } ?>" > days
                            <?php if(isset($_POST['submit']) && isset($error[8])) { echo $error[8]; } ?>
                        </td>
                        <td><input name="course1_part" type="text" size="3" value="<?php if(!empty($_POST['course1_part'])) { echo $_POST['course1_part']; } ?>" >
                            <?php if(isset($_POST['submit']) && isset($error[9])) { echo $error[9]; } ?>
                        </td>

                        <?php
                        if(($_SESSION['rev_vis'])=='yes') {
                            ?>

                            <td>&euro; <input name="course1_rev" type="text" size="3" value="<?php if(!empty($_POST['course1_rev'])) { echo $_POST['course1_rev']; } ?>" />
                            </td>

                        <?php
                        }
                        ?>
                        <td>
                            <select name="course1_type">
                                <option>- Select -</option>
                                <option <?php if(($_POST['course1_type'])=="Public") { echo "selected='selected'";} ?>>Public</option>
                                <option <?php if(($_POST['course1_type'])=="Corporate") { echo "selected='selected'";} ?>>Corporate</option>
                            </select>
                            <?php if(isset($_POST['submit']) && isset($error[10])) { echo $error[10]; } ?>
                        </td>
                        <td><input name="course1_code" type="text" size="10" value="<?php if(!empty($_POST['course1_code'])) { echo $_POST['course1_code']; } ?>" />
                            <?php if(isset($_POST['submit']) && isset($error[11])) { echo $error[11]; } ?>
                            <?php if(isset($_POST['submit']) && isset($error[57])) { echo $error[57]; } ?>
                            <?php if(isset($_POST['submit']) && isset($error[67])) { echo $error[67]; } ?>
                        </td>
                    </tr>
                    <tr class="odd">
                        <td>
                            <select name="course2">
                                <option>- Select -</option>
                                <?php
                                $course_SQL="SELECT * FROM reporting_courses WHERE active = 'yes' ORDER BY course_name ASC";
                                $course_result=mysql_query($course_SQL);
                                while($course=mysql_fetch_array($course_result)){
                                    ?>
                                    <option <?php if(($_POST['course2'])==$course['course_name']) { echo "selected='selected'";} ?>><?php echo $course['course_name'] ?></option>
                                <?php
                                }
                                ?>
                            </select>
                        </td>
                        <td>
                            <input type="date" name="course2_start" id="date2" size="8" value="<?php if(!empty($_POST['course2_start'])) { echo $_POST['course2_start']; } ?>">
                            <?php if(isset($_POST['submit']) && isset($error[12])) { echo $error[12]; } ?>
                        </td>
                        <td><input name="course2_dur" type="text" size="1" value="<?php if(!empty($_POST['course2_dur'])) { echo $_POST['course2_dur']; } ?>" /> days
                            <?php if(isset($_POST['submit']) && isset($error[13])) { echo $error[13]; } ?>
                        </td>
                        <td><input name="course2_part" type="text" size="3" value="<?php if(!empty($_POST['course2_part'])) { echo $_POST['course2_part']; } ?>" />
                            <?php if(isset($_POST['submit']) && isset($error[14])) { echo $error[14]; } ?>
                        </td>

                        <?php
                        if(($_SESSION['rev_vis'])=='yes') {
                            ?>

                            <td>&euro; <input name="course2_rev" type="text" size="3" value="<?php if(!empty($_POST['course2_rev'])) { echo $_POST['course2_rev']; } ?>" />
                            </td>

                        <?php
                        }
                        ?>
                        <td>
                            <select name="course2_type">
                                <option>- Select -</option>
                                <option <?php if(($_POST['course2_type'])=="Public") { echo "selected='selected'";} ?>>Public</option>
                                <option <?php if(($_POST['course2_type'])=="Corporate") { echo "selected='selected'";} ?>>Corporate</option>
                            </select>
                            <?php if(isset($_POST['submit']) && isset($error[15])) { echo $error[15]; } ?>
                        </td>
                        <td><input name="course2_code" type="text" size="10" value="<?php if(!empty($_POST['course2_code'])) { echo $_POST['course2_code']; } ?>" />
                            <?php if(isset($_POST['submit']) && isset($error[16])) { echo $error[16]; } ?>
                            <?php if(isset($_POST['submit']) && isset($error[58])) { echo $error[58]; } ?>
                            <?php if(isset($_POST['submit']) && isset($error[68])) { echo $error[68]; } ?>
                        </td>
                    </tr>

                    <tr>
                        <td>
                            <select name="course3">
                                <option>- Select -</option>
                                <?php
                                $course_SQL="SELECT * FROM reporting_courses WHERE active = 'yes' ORDER BY course_name ASC";
                                $course_result=mysql_query($course_SQL);
                                while($course=mysql_fetch_array($course_result)){
                                    ?>
                                    <option <?php if(($_POST['course3'])==$course['course_name']) { echo "selected='selected'";} ?>><?php echo $course['course_name'] ?></option>
                                <?php
                                }
                                ?>
                            </select>
                        </td>
                        <td>
                            <input type="date" name="course3_start" id="date3" size="8" value="<?php if(!empty($_POST['course3_start'])) { echo $_POST['course3_start']; } ?>">
                            <?php if(isset($_POST['submit']) && isset($error[17])) { echo $error[17]; } ?>
                        </td>
                        <td><input name="course3_dur" type="text" size="1" value="<?php if(!empty($_POST['course3_dur'])) { echo $_POST['course3_dur']; } ?>" /> days
                            <?php if(isset($_POST['submit']) && isset($error[18])) { echo $error[18]; } ?>
                        </td>
                        <td><input name="course3_part" type="text" size="3" value="<?php if(!empty($_POST['course3_part'])) { echo $_POST['course3_part']; } ?>" />
                            <?php if(isset($_POST['submit']) && isset($error[19])) { echo $error[19]; } ?>
                        </td>

                        <?php
                        if(($_SESSION['rev_vis'])=='yes') {
                            ?>

                            <td>&euro; <input name="course3_rev" type="text" size="3" value="<?php if(!empty($_POST['course3_rev'])) { echo $_POST['course3_rev']; } ?>" />
                            </td>

                        <?php
                        }
                        ?>
                        <td>
                            <select name="course3_type">
                                <option>- Select -</option>
                                <option <?php if(($_POST['course3_type'])=="Public") { echo "selected='selected'";} ?>>Public</option>
                                <option <?php if(($_POST['course3_type'])=="Corporate") { echo "selected='selected'";} ?>>Corporate</option>
                            </select>
                            <?php if(isset($_POST['submit']) && isset($error[20])) { echo $error[20]; } ?>
                        </td>
                        <td><input name="course3_code" type="text" size="10" value="<?php if(!empty($_POST['course3_code'])) { echo $_POST['course3_code']; } ?>" />
                            <?php if(isset($_POST['submit']) && isset($error[21])) { echo $error[21]; } ?>
                            <?php if(isset($_POST['submit']) && isset($error[59])) { echo $error[59]; } ?>
                            <?php if(isset($_POST['submit']) && isset($error[69])) { echo $error[69]; } ?>
                        </td>
                    </tr>

                    <tr class="odd">
                        <td>
                            <select name="course4">
                                <option>- Select -</option>
                                <?php
                                $course_SQL="SELECT * FROM reporting_courses WHERE active = 'yes' ORDER BY course_name ASC";
                                $course_result=mysql_query($course_SQL);
                                while($course=mysql_fetch_array($course_result)){
                                    ?>
                                    <option <?php if(($_POST['course4'])==$course['course_name']) { echo "selected='selected'";} ?>><?php echo $course['course_name'] ?></option>
                                <?php
                                }
                                ?>
                            </select>
                        </td>
                        <td>
                            <input type="date" name="course4_start" id="date4" size="8" value="<?php if(!empty($_POST['course4_start'])) { echo $_POST['course4_start']; } ?>">
                            <?php if(isset($_POST['submit']) && isset($error[22])) { echo $error[22]; } ?>
                        </td>
                        <td><input name="course4_dur" type="text" size="1" value="<?php if(!empty($_POST['course4_dur'])) { echo $_POST['course4_dur']; } ?>" /> days
                            <?php if(isset($_POST['submit']) && isset($error[23])) { echo $error[23]; } ?>
                        </td>
                        <td><input name="course4_part" type="text" size="3" value="<?php if(!empty($_POST['course4_part'])) { echo $_POST['course4_part']; } ?>" />
                            <?php if(isset($_POST['submit']) && isset($error[24])) { echo $error[24]; } ?>
                        </td>

                        <?php
                        if(($_SESSION['rev_vis'])=='yes') {
                            ?>

                            <td>&euro; <input name="course4_rev" type="text" size="3" value="<?php if(!empty($_POST['course4_rev'])) { echo $_POST['course4_rev']; } ?>" />
                            </td>

                        <?php
                        }
                        ?>
                        <td>
                            <select name="course4_type">
                                <option>- Select -</option>
                                <option <?php if(($_POST['course4_type'])=="Public") { echo "selected='selected'";} ?>>Public</option>
                                <option <?php if(($_POST['course4_type'])=="Corporate") { echo "selected='selected'";} ?>>Corporate</option>
                            </select>
                            <?php if(isset($_POST['submit']) && isset($error[25])) { echo $error[25]; } ?>
                        </td>
                        <td><input name="course4_code" type="text" size="10" value="<?php if(!empty($_POST['course4_code'])) { echo $_POST['course4_code']; } ?>" />
                            <?php if(isset($_POST['submit']) && isset($error[26])) { echo $error[26]; } ?>
                            <?php if(isset($_POST['submit']) && isset($error[60])) { echo $error[60]; } ?>
                            <?php if(isset($_POST['submit']) && isset($error[70])) { echo $error[70]; } ?>
                        </td>
                    </tr>

                    <tr>
                        <td>
                            <select name="course5">
                                <option>- Select -</option>
                                <?php
                                $course_SQL="SELECT * FROM reporting_courses WHERE active = 'yes' ORDER BY course_name ASC";
                                $course_result=mysql_query($course_SQL);
                                while($course=mysql_fetch_array($course_result)){
                                    ?>
                                    <option <?php if(($_POST['course5'])==$course['course_name']) { echo "selected='selected'";} ?>><?php echo $course['course_name'] ?></option>
                                <?php
                                }
                                ?>
                            </select>
                        </td>
                        <td>
                            <input type="date" name="course5_start" id="date5" size="8" value="<?php if(!empty($_POST['course5_start'])) { echo $_POST['course5_start']; } ?>">
                            <?php if(isset($_POST['submit']) && isset($error[27])) { echo $error[27]; } ?>
                        </td>
                        <td><input name="course5_dur" type="text" size="1" value="<?php if(!empty($_POST['course5_dur'])) { echo $_POST['course5_dur']; } ?>" /> days
                            <?php if(isset($_POST['submit']) && isset($error[28])) { echo $error[28]; } ?>
                        </td>
                        <td><input name="course5_part" type="text" size="3" value="<?php if(!empty($_POST['course5_part'])) { echo $_POST['course5_part']; } ?>" />
                            <?php if(isset($_POST['submit']) && isset($error[29])) { echo $error[29]; } ?>
                        </td>

                        <?php
                        if(($_SESSION['rev_vis'])=='yes') {
                            ?>

                            <td>&euro; <input name="course5_rev" type="text" size="3" value="<?php if(!empty($_POST['course5_rev'])) { echo $_POST['course5_rev']; } ?>" />
                            </td>

                        <?php
                        }
                        ?>
                        <td>
                            <select name="course5_type">
                                <option>- Select -</option>
                                <option <?php if(($_POST['course5_type'])=="Public") { echo "selected='selected'";} ?>>Public</option>
                                <option <?php if(($_POST['course5_type'])=="Corporate") { echo "selected='selected'";} ?>>Corporate</option>
                            </select>
                            <?php if(isset($_POST['submit']) && isset($error[30])) { echo $error[30]; } ?>
                        </td>
                        <td><input name="course5_code" type="text" size="10" value="<?php if(!empty($_POST['course5_code'])) { echo $_POST['course5_code']; } ?>" />
                            <?php if(isset($_POST['submit']) && isset($error[31])) { echo $error[31]; } ?>
                            <?php if(isset($_POST['submit']) && isset($error[61])) { echo $error[61]; } ?>
                            <?php if(isset($_POST['submit']) && isset($error[71])) { echo $error[71]; } ?>
                        </td>
                    </tr>

                    <tr class="odd">
                        <td>
                            <select name="course6">
                                <option>- Select -</option>
                                <?php
                                $course_SQL="SELECT * FROM reporting_courses WHERE active = 'yes' ORDER BY course_name ASC";
                                $course_result=mysql_query($course_SQL);
                                while($course=mysql_fetch_array($course_result)){
                                    ?>
                                    <option <?php if(($_POST['course6'])==$course['course_name']) { echo "selected='selected'";} ?>><?php echo $course['course_name'] ?></option>
                                <?php
                                }
                                ?>
                            </select>
                        </td>
                        <td>
                            <input type="date" name="course6_start" id="date6" size="8" value="<?php if(!empty($_POST['course6_start'])) { echo $_POST['course6_start']; } ?>">
                            <?php if(isset($_POST['submit']) && isset($error[32])) { echo $error[32]; } ?>
                        </td>
                        <td><input name="course6_dur" type="text" size="1" value="<?php if(!empty($_POST['course6_dur'])) { echo $_POST['course6_dur']; } ?>" /> days
                            <?php if(isset($_POST['submit']) && isset($error[33])) { echo $error[33]; } ?>
                        </td>
                        <td><input name="course6_part" type="text" size="3" value="<?php if(!empty($_POST['course6_part'])) { echo $_POST['course6_part']; } ?>" />
                            <?php if(isset($_POST['submit']) && isset($error[34])) { echo $error[34]; } ?>
                        </td>

                        <?php
                        if(($_SESSION['rev_vis'])=='yes') {
                            ?>

                            <td>&euro; <input name="course6_rev" type="text" size="3" value="<?php if(!empty($_POST['course6_rev'])) { echo $_POST['course6_rev']; } ?>" />
                            </td>

                        <?php
                        }
                        ?>
                        <td>
                            <select name="course6_type">
                                <option>- Select -</option>
                                <option <?php if(($_POST['course6_type'])=="Public") { echo "selected='selected'";} ?>>Public</option>
                                <option <?php if(($_POST['course6_type'])=="Corporate") { echo "selected='selected'";} ?>>Corporate</option>
                            </select>
                            <?php if(isset($_POST['submit']) && isset($error[35])) { echo $error[35]; } ?>
                        </td>
                        <td><input name="course6_code" type="text" size="10" value="<?php if(!empty($_POST['course6_code'])) { echo $_POST['course6_code']; } ?>" />
                            <?php if(isset($_POST['submit']) && isset($error[36])) { echo $error[36]; } ?>
                            <?php if(isset($_POST['submit']) && isset($error[62])) { echo $error[62]; } ?>
                            <?php if(isset($_POST['submit']) && isset($error[72])) { echo $error[72]; } ?>
                        </td>
                    </tr>

                    <tr>
                        <td>
                            <select name="course7">
                                <option>- Select -</option>
                                <?php
                                $course_SQL="SELECT * FROM reporting_courses WHERE active = 'yes' ORDER BY course_name ASC";
                                $course_result=mysql_query($course_SQL);
                                while($course=mysql_fetch_array($course_result)){
                                    ?>
                                    <option <?php if(($_POST['course7'])==$course['course_name']) { echo "selected='selected'";} ?>><?php echo $course['course_name'] ?></option>
                                <?php
                                }
                                ?>
                            </select>
                        </td>
                        <td>
                            <input type="date" name="course7_start" id="date7" size="8" value="<?php if(!empty($_POST['course7_start'])) { echo $_POST['course7_start']; } ?>">
                            <?php if(isset($_POST['submit']) && isset($error[37])) { echo $error[37]; } ?>
                        </td>
                        <td><input name="course7_dur" type="text" size="1" value="<?php if(!empty($_POST['course7_dur'])) { echo $_POST['course7_dur']; } ?>" /> days
                            <?php if(isset($_POST['submit']) && isset($error[38])) { echo $error[38]; } ?>
                        </td>
                        <td><input name="course7_part" type="text" size="3" value="<?php if(!empty($_POST['course7_part'])) { echo $_POST['course7_part']; } ?>" />
                            <?php if(isset($_POST['submit']) && isset($error[39])) { echo $error[39]; } ?>
                        </td>

                        <?php
                        if(($_SESSION['rev_vis'])=='yes') {
                            ?>

                            <td>&euro; <input name="course7_rev" type="text" size="3" value="<?php if(!empty($_POST['course7_rev'])) { echo $_POST['course7_rev']; } ?>" />
                            </td>

                        <?php
                        }
                        ?>
                        <td>
                            <select name="course7_type">
                                <option>- Select -</option>
                                <option <?php if(($_POST['course7_type'])=="Public") { echo "selected='selected'";} ?>>Public</option>
                                <option <?php if(($_POST['course7_type'])=="Corporate") { echo "selected='selected'";} ?>>Corporate</option>
                            </select>
                            <?php if(isset($_POST['submit']) && isset($error[40])) { echo $error[40]; } ?>
                        </td>
                        <td><input name="course7_code" type="text" size="10" value="<?php if(!empty($_POST['course7_code'])) { echo $_POST['course7_code']; } ?>" />
                            <?php if(isset($_POST['submit']) && isset($error[41])) { echo $error[41]; } ?>
                            <?php if(isset($_POST['submit']) && isset($error[63])) { echo $error[63]; } ?>
                            <?php if(isset($_POST['submit']) && isset($error[73])) { echo $error[73]; } ?>
                        </td>
                    </tr>

                    <tr class="odd">
                        <td>
                            <select name="course8">
                                <option>- Select -</option>
                                <?php
                                $course_SQL="SELECT * FROM reporting_courses WHERE active = 'yes' ORDER BY course_name ASC";
                                $course_result=mysql_query($course_SQL);
                                while($course=mysql_fetch_array($course_result)){
                                    ?>
                                    <option <?php if(($_POST['course8'])==$course['course_name']) { echo "selected='selected'";} ?>><?php echo $course['course_name'] ?></option>
                                <?php
                                }
                                ?>
                            </select>
                        </td>
                        <td>
                            <input type="date" name="course8_start" id="date8" size="8" value="<?php if(!empty($_POST['course8_start'])) { echo $_POST['course8_start']; } ?>">
                            <?php if(isset($_POST['submit']) && isset($error[42])) { echo $error[42]; } ?>
                        </td>
                        <td><input name="course8_dur" type="text" size="1" value="<?php if(!empty($_POST['course8_dur'])) { echo $_POST['course8_dur']; } ?>" /> days
                            <?php if(isset($_POST['submit']) && isset($error[43])) { echo $error[43]; } ?>
                        </td>
                        <td><input name="course8_part" type="text" size="3" value="<?php if(!empty($_POST['course8_part'])) { echo $_POST['course8_part']; } ?>" />
                            <?php if(isset($_POST['submit']) && isset($error[44])) { echo $error[44]; } ?>
                        </td>

                        <?php
                        if(($_SESSION['rev_vis'])=='yes') {
                            ?>

                            <td>&euro; <input name="course8_rev" type="text" size="3" value="<?php if(!empty($_POST['course8_rev'])) { echo $_POST['course8_rev']; } ?>" />
                            </td>

                        <?php
                        }
                        ?>
                        <td>
                            <select name="course8_type">
                                <option>- Select -</option>
                                <option <?php if(($_POST['course8_type'])=="Public") { echo "selected='selected'";} ?>>Public</option>
                                <option <?php if(($_POST['course8_type'])=="Corporate") { echo "selected='selected'";} ?>>Corporate</option>
                            </select>
                            <?php if(isset($_POST['submit']) && isset($error[45])) { echo $error[45]; } ?>
                        </td>
                        <td><input name="course8_code" type="text" size="10" value="<?php if(!empty($_POST['course8_code'])) { echo $_POST['course8_code']; } ?>" />
                            <?php if(isset($_POST['submit']) && isset($error[46])) { echo $error[46]; } ?>
                            <?php if(isset($_POST['submit']) && isset($error[64])) { echo $error[64]; } ?>
                            <?php if(isset($_POST['submit']) && isset($error[74])) { echo $error[74]; } ?>
                        </td>
                    </tr>

                    <tr>
                        <td>
                            <select name="course9">
                                <option>- Select -</option>
                                <?php
                                $course_SQL="SELECT * FROM reporting_courses WHERE active = 'yes' ORDER BY course_name ASC";
                                $course_result=mysql_query($course_SQL);
                                while($course=mysql_fetch_array($course_result)){
                                    ?>
                                    <option <?php if(($_POST['course9'])==$course['course_name']) { echo "selected='selected'";} ?>><?php echo $course['course_name'] ?></option>
                                <?php
                                }
                                ?>
                            </select>
                        </td>
                        <td>
                            <input type="date" name="course9_start" id="date9" size="8" value="<?php if(!empty($_POST['course9_start'])) { echo $_POST['course9_start']; } ?>">
                            <?php if(isset($_POST['submit']) && isset($error[47])) { echo $error[47]; } ?>
                        </td>
                        <td><input name="course9_dur" type="text" size="1" value="<?php if(!empty($_POST['course9_dur'])) { echo $_POST['course9_dur']; } ?>" /> days
                            <?php if(isset($_POST['submit']) && isset($error[48])) { echo $error[48]; } ?>
                        </td>
                        <td><input name="course9_part" type="text" size="3" value="<?php if(!empty($_POST['course9_part'])) { echo $_POST['course9_part']; } ?>" />
                            <?php if(isset($_POST['submit']) && isset($error[49])) { echo $error[49]; } ?>
                        </td>

                        <?php
                        if(($_SESSION['rev_vis'])=='yes') {
                            ?>

                            <td>&euro; <input name="course9_rev" type="text" size="3" value="<?php if(!empty($_POST['course9_rev'])) { echo $_POST['course9_rev']; } ?>" />
                            </td>

                        <?php
                        }
                        ?>
                        <td>
                            <select name="course9_type">
                                <option>- Select -</option>
                                <option <?php if(($_POST['course9_type'])=="Public") { echo "selected='selected'";} ?>>Public</option>
                                <option <?php if(($_POST['course9_type'])=="Corporate") { echo "selected='selected'";} ?>>Corporate</option>
                            </select>
                            <?php if(isset($_POST['submit']) && isset($error[50])) { echo $error[50]; } ?>
                        </td>
                        <td><input name="course9_code" type="text" size="10" value="<?php if(!empty($_POST['course9_code'])) { echo $_POST['course9_code']; } ?>" />
                            <?php if(isset($_POST['submit']) && isset($error[51])) { echo $error[51]; } ?>
                            <?php if(isset($_POST['submit']) && isset($error[65])) { echo $error[65]; } ?>
                            <?php if(isset($_POST['submit']) && isset($error[75])) { echo $error[75]; } ?>
                        </td>
                    </tr>

                    <tr class="odd">
                        <td>
                            <select name="course10">
                                <option>- Select -</option>
                                <?php
                                $course_SQL="SELECT * FROM reporting_courses WHERE active = 'yes' ORDER BY course_name ASC";
                                $course_result=mysql_query($course_SQL);
                                while($course=mysql_fetch_array($course_result)){
                                    ?>
                                    <option <?php if(($_POST['course10'])==$course['course_name']) { echo "selected='selected'";} ?>><?php echo $course['course_name'] ?></option>
                                <?php
                                }
                                ?>
                            </select>
                        </td>
                        <td>
                            <input type="date" name="course10_start" id="date10" size="8" value="<?php if(!empty($_POST['course10_start'])) { echo $_POST['course10_start']; } ?>">
                            <?php if(isset($_POST['submit']) && isset($error[52])) { echo $error[52]; } ?>
                        </td>
                        <td><input name="course10_dur" type="text" size="1" value="<?php if(!empty($_POST['course10_dur'])) { echo $_POST['course10_dur']; } ?>" /> days
                            <?php if(isset($_POST['submit']) && isset($error[53])) { echo $error[53]; } ?>
                        </td>
                        <td><input name="course10_part" type="text" size="3" value="<?php if(!empty($_POST['course10_part'])) { echo $_POST['course10_part']; } ?>" />
                            <?php if(isset($_POST['submit']) && isset($error[54])) { echo $error[54]; } ?>
                        </td>

                        <?php
                        if(($_SESSION['rev_vis'])=='yes') {
                            ?>

                            <td>&euro; <input name="course10_rev" type="text" size="3" value="<?php if(!empty($_POST['course10_rev'])) { echo $_POST['course10_rev']; } ?>" />
                            </td>
                        <?php
                        }
                        ?>
                        <td>
                            <select name="course10_type">
                                <option>- Select -</option>
                                <option <?php if(($_POST['course10_type'])=="Public") { echo "selected='selected'";} ?>>Public</option>
                                <option <?php if(($_POST['course10_type'])=="Corporate") { echo "selected='selected'";} ?>>Corporate</option>
                            </select>
                            <?php if(isset($_POST['submit']) && isset($error[55])) { echo $error[55]; } ?>
                        </td>
                        <td><input name="course10_code" type="text" size="10" value="<?php if(!empty($_POST['course10_code'])) { echo $_POST['course10_code']; } ?>" />
                            <?php if(isset($_POST['submit']) && isset($error[56])) { echo $error[56]; } ?>
                            <?php if(isset($_POST['submit']) && isset($error[66])) { echo $error[66]; } ?>
                            <?php if(isset($_POST['submit']) && isset($error[76])) { echo $error[76]; } ?>
                        </td>
                    </tr>

                    <tr>
                        <td>
                            <select name="course11">
                                <option>- Select -</option>
                                <?php
                                $course_SQL="SELECT * FROM reporting_courses WHERE active = 'yes' ORDER BY course_name ASC";
                                $course_result=mysql_query($course_SQL);
                                while($course=mysql_fetch_array($course_result)){
                                    ?>
                                    <option <?php if(($_POST['course11'])==$course['course_name']) { echo "selected='selected'";} ?>><?php echo $course['course_name'] ?></option>
                                <?php
                                }
                                ?>
                            </select>
                        </td>
                        <td>
                            <input type="date" name="course11_start" id="date11" size="8" value="<?php if(!empty($_POST['course11_start'])) { echo $_POST['course11_start']; } ?>">
                            <?php if(isset($_POST['submit']) && isset($error[78])) { echo $error[78]; } ?>
                        </td>
                        <td><input name="course11_dur" type="text" size="1" value="<?php if(!empty($_POST['course11_dur'])) { echo $_POST['course11_dur']; } ?>" /> days
                            <?php if(isset($_POST['submit']) && isset($error[79])) { echo $error[79]; } ?>
                        </td>
                        <td><input name="course11_part" type="text" size="3" value="<?php if(!empty($_POST['course11_part'])) { echo $_POST['course11_part']; } ?>" />
                            <?php if(isset($_POST['submit']) && isset($error[80])) { echo $error[80]; } ?>
                        </td>

                        <?php
                        if(($_SESSION['rev_vis'])=='yes') {
                            ?>

                            <td>&euro; <input name="course11_rev" type="text" size="3" value="<?php if(!empty($_POST['course11_rev'])) { echo $_POST['course11_rev']; } ?>" />
                            </td>
                        <?php
                        }
                        ?>
                        <td>
                            <select name="course11_type">
                                <option>- Select -</option>
                                <option <?php if(($_POST['course11_type'])=="Public") { echo "selected='selected'";} ?>>Public</option>
                                <option <?php if(($_POST['course11_type'])=="Corporate") { echo "selected='selected'";} ?>>Corporate</option>
                            </select>
                            <?php if(isset($_POST['submit']) && isset($error[81])) { echo $error[81]; } ?>
                        </td>
                        <td><input name="course11_code" type="text" size="10" value="<?php if(!empty($_POST['course11_code'])) { echo $_POST['course11_code']; } ?>" />
                            <?php if(isset($_POST['submit']) && isset($error[82])) { echo $error[82]; } ?>
                            <?php if(isset($_POST['submit']) && isset($error[83])) { echo $error[83]; } ?>
                            <?php if(isset($_POST['submit']) && isset($error[108])) { echo $error[108]; } ?>
                        </td>
                    </tr>

                    <tr class="odd">
                        <td>
                            <select name="course12">
                                <option>- Select -</option>
                                <?php
                                $course_SQL="SELECT * FROM reporting_courses WHERE active = 'yes' ORDER BY course_name ASC";
                                $course_result=mysql_query($course_SQL);
                                while($course=mysql_fetch_array($course_result)){
                                    ?>
                                    <option <?php if(($_POST['course12'])==$course['course_name']) { echo "selected='selected'";} ?>><?php echo $course['course_name'] ?></option>
                                <?php
                                }
                                ?>
                            </select>
                        </td>
                        <td>
                            <input type="date" name="course12_start" id="date12" size="8" value="<?php if(!empty($_POST['course12_start'])) { echo $_POST['course12_start']; } ?>">
                            <?php if(isset($_POST['submit']) && isset($error[84])) { echo $error[84]; } ?>
                        </td>
                        <td><input name="course12_dur" type="text" size="1" value="<?php if(!empty($_POST['course12_dur'])) { echo $_POST['course12_dur']; } ?>" /> days
                            <?php if(isset($_POST['submit']) && isset($error[85])) { echo $error[85]; } ?>
                        </td>
                        <td><input name="course12_part" type="text" size="3" value="<?php if(!empty($_POST['course12_part'])) { echo $_POST['course12_part']; } ?>" />
                            <?php if(isset($_POST['submit']) && isset($error[86])) { echo $error[86]; } ?>
                        </td>

                        <?php
                        if(($_SESSION['rev_vis'])=='yes') {
                            ?>

                            <td>&euro; <input name="course12_rev" type="text" size="3" value="<?php if(!empty($_POST['course12_rev'])) { echo $_POST['course12_rev']; } ?>" />
                            </td>
                        <?php
                        }
                        ?>
                        <td>
                            <select name="course12_type">
                                <option>- Select -</option>
                                <option <?php if(($_POST['course12_type'])=="Public") { echo "selected='selected'";} ?>>Public</option>
                                <option <?php if(($_POST['course12_type'])=="Corporate") { echo "selected='selected'";} ?>>Corporate</option>
                            </select>
                            <?php if(isset($_POST['submit']) && isset($error[87])) { echo $error[87]; } ?>
                        </td>
                        <td><input name="course12_code" type="text" size="10" value="<?php if(!empty($_POST['course12_code'])) { echo $_POST['course12_code']; } ?>" />
                            <?php if(isset($_POST['submit']) && isset($error[88])) { echo $error[88]; } ?>
                            <?php if(isset($_POST['submit']) && isset($error[89])) { echo $error[89]; } ?>
                            <?php if(isset($_POST['submit']) && isset($error[109])) { echo $error[109]; } ?>
                        </td>
                    </tr>

                    <tr>
                        <td>
                            <select name="course13">
                                <option>- Select -</option>
                                <?php
                                $course_SQL="SELECT * FROM reporting_courses WHERE active = 'yes' ORDER BY course_name ASC";
                                $course_result=mysql_query($course_SQL);
                                while($course=mysql_fetch_array($course_result)){
                                    ?>
                                    <option <?php if(($_POST['course13'])==$course['course_name']) { echo "selected='selected'";} ?>><?php echo $course['course_name'] ?></option>
                                <?php
                                }
                                ?>
                            </select>
                        </td>
                        <td>
                            <input type="date" name="course13_start" id="date13" size="8" value="<?php if(!empty($_POST['course13_start'])) { echo $_POST['course13_start']; } ?>">
                            <?php if(isset($_POST['submit']) && isset($error[90])) { echo $error[90]; } ?>
                        </td>
                        <td><input name="course13_dur" type="text" size="1" value="<?php if(!empty($_POST['course13_dur'])) { echo $_POST['course13_dur']; } ?>" /> days
                            <?php if(isset($_POST['submit']) && isset($error[91])) { echo $error[91]; } ?>
                        </td>
                        <td><input name="course13_part" type="text" size="3" value="<?php if(!empty($_POST['course13_part'])) { echo $_POST['course13_part']; } ?>" />
                            <?php if(isset($_POST['submit']) && isset($error[92])) { echo $error[92]; } ?>
                        </td>

                        <?php
                        if(($_SESSION['rev_vis'])=='yes') {
                            ?>

                            <td>&euro; <input name="course13_rev" type="text" size="3" value="<?php if(!empty($_POST['course13_rev'])) { echo $_POST['course13_rev']; } ?>" />
                            </td>
                        <?php
                        }
                        ?>
                        <td>
                            <select name="course13_type">
                                <option>- Select -</option>
                                <option <?php if(($_POST['course13_type'])=="Public") { echo "selected='selected'";} ?>>Public</option>
                                <option <?php if(($_POST['course13_type'])=="Corporate") { echo "selected='selected'";} ?>>Corporate</option>
                            </select>
                            <?php if(isset($_POST['submit']) && isset($error[93])) { echo $error[93]; } ?>
                        </td>
                        <td><input name="course13_code" type="text" size="10" value="<?php if(!empty($_POST['course13_code'])) { echo $_POST['course13_code']; } ?>" />
                            <?php if(isset($_POST['submit']) && isset($error[94])) { echo $error[94]; } ?>
                            <?php if(isset($_POST['submit']) && isset($error[95])) { echo $error[95]; } ?>
                            <?php if(isset($_POST['submit']) && isset($error[110])) { echo $error[110]; } ?>
                        </td>
                    </tr>

                    <tr class="odd">
                        <td>
                            <select name="course14">
                                <option>- Select -</option>
                                <?php
                                $course_SQL="SELECT * FROM reporting_courses WHERE active = 'yes' ORDER BY course_name ASC";
                                $course_result=mysql_query($course_SQL);
                                while($course=mysql_fetch_array($course_result)){
                                    ?>
                                    <option <?php if(($_POST['course14'])==$course['course_name']) { echo "selected='selected'";} ?>><?php echo $course['course_name'] ?></option>
                                <?php
                                }
                                ?>
                            </select>
                        </td>
                        <td>
                                  <input type="date" name="course14_start" id="date14" size="8" value="<?php if(!empty($_POST['course14_start'])) { echo $_POST['course14_start']; } ?>">
                            <?php if(isset($_POST['submit']) && isset($error[96])) { echo $error[96]; } ?>
                        </td>
                        <td><input name="course14_dur" type="text" size="1" value="<?php if(!empty($_POST['course14_dur'])) { echo $_POST['course14_dur']; } ?>" /> days
                            <?php if(isset($_POST['submit']) && isset($error[97])) { echo $error[97]; } ?>
                        </td>
                        <td><input name="course14_part" type="text" size="3" value="<?php if(!empty($_POST['course14_part'])) { echo $_POST['course14_part']; } ?>" />
                            <?php if(isset($_POST['submit']) && isset($error[98])) { echo $error[98]; } ?>
                        </td>

                        <?php
                        if(($_SESSION['rev_vis'])=='yes') {
                            ?>

                            <td>&euro; <input name="course14_rev" type="text" size="3" value="<?php if(!empty($_POST['course14_rev'])) { echo $_POST['course14_rev']; } ?>" />
                            </td>
                        <?php
                        }
                        ?>
                        <td>
                            <select name="course14_type">
                                <option>- Select -</option>
                                <option <?php if(($_POST['course14_type'])=="Public") { echo "selected='selected'";} ?>>Public</option>
                                <option <?php if(($_POST['course14_type'])=="Corporate") { echo "selected='selected'";} ?>>Corporate</option>
                            </select>
                            <?php if(isset($_POST['submit']) && isset($error[99])) { echo $error[99]; } ?>
                        </td>
                        <td><input name="course14_code" type="text" size="10" value="<?php if(!empty($_POST['course14_code'])) { echo $_POST['course14_code']; } ?>" />
                            <?php if(isset($_POST['submit']) && isset($error[100])) { echo $error[100]; } ?>
                            <?php if(isset($_POST['submit']) && isset($error[101])) { echo $error[101]; } ?>
                            <?php if(isset($_POST['submit']) && isset($error[111])) { echo $error[111]; } ?>
                        </td>
                    </tr>

                    <tr>
                        <td>
                            <select name="course15">
                                <option>- Select -</option>
                                <?php
                                $course_SQL="SELECT * FROM reporting_courses WHERE active = 'yes' ORDER BY course_name ASC";
                                $course_result=mysql_query($course_SQL);
                                while($course=mysql_fetch_array($course_result)){
                                    ?>
                                    <option <?php if(($_POST['course15'])==$course['course_name']) { echo "selected='selected'";} ?>><?php echo $course['course_name'] ?></option>
                                <?php
                                }
                                ?>
                            </select>
                        </td>
                        <td>
                               <input type="date" name="course15_start" id="date15" size="8" value="<?php if(!empty($_POST['course15_start'])) { echo $_POST['course15_start']; } ?>">
                            <?php if(isset($_POST['submit']) && isset($error[102])) { echo $error[102]; } ?>
                        </td>
                        <td><input name="course15_dur" type="text" size="1" value="<?php if(!empty($_POST['course15_dur'])) { echo $_POST['course15_dur']; } ?>" /> days
                            <?php if(isset($_POST['submit']) && isset($error[103])) { echo $error[103]; } ?>
                        </td>
                        <td><input name="course15_part" type="text" size="3" value="<?php if(!empty($_POST['course15_part'])) { echo $_POST['course15_part']; } ?>" />
                            <?php if(isset($_POST['submit']) && isset($error[104])) { echo $error[104]; } ?>
                        </td>

                        <?php
                        if(($_SESSION['rev_vis'])=='yes') {
                            ?>

                            <td>&euro; <input name="course15_rev" type="text" size="3" value="<?php if(!empty($_POST['course15_rev'])) { echo $_POST['course15_rev']; } ?>" />
                            </td>
                        <?php
                        }
                        ?>
                        <td>
                            <select name="course15_type">
                                <option>- Select -</option>
                                <option <?php if(($_POST['course15_type'])=="Public") { echo "selected='selected'";} ?>>Public</option>
                                <option <?php if(($_POST['course15_type'])=="Corporate") { echo "selected='selected'";} ?>>Corporate</option>
                            </select>
                            <?php if(isset($_POST['submit']) && isset($error[105])) { echo $error[105]; } ?>
                        </td>
                        <td><input name="course15_code" type="text" size="10" value="<?php if(!empty($_POST['course15_code'])) { echo $_POST['course15_code']; } ?>" />
                            <?php if(isset($_POST['submit']) && isset($error[106])) { echo $error[106]; } ?>
                            <?php if(isset($_POST['submit']) && isset($error[107])) { echo $error[107]; } ?>
                            <?php if(isset($_POST['submit']) && isset($error[112])) { echo $error[112]; } ?>
                        </td>
                    </tr>
                </table>
                <?php
                if(($_SESSION['serv_vis'])=='yes') {
                    ?>
                    <table width="500" border="0" cellpadding="0" cellspacing="0" class="report">
                        <tr>
                            <th colspan="2">Consultancy and writing services (Euro)</th>
                        </tr>
                        <tr class="odd">
                            <td colspan="2" class="small">
                                <em>Consultancy and writing services</em> means any customer-oriented activities in connection with the creation of communication, documentation, learning material, training material or reference material, based on paper, online or other media performed by the Partner, where the use of the Information Mapping&reg; methodology or the Information Mapping&reg; products and services by the Distributor was prominent in the discussion, proposal, sales or delivery cycle.
                                <br />
                                <strong>By sending the report, you certify that all services falling under this definition have been reported.</strong></td>
                        </tr>
                        <tr>
                            <td width="120"><strong>Consultancy</strong></td>
                            <td>&euro; <input type="text"  size="5" name="consult" value="<?php if(!empty($_POST['consult'])) { echo $_POST['consult']; } ?>" /></td>
                        </tr>
                        <tr class="odd">
                            <td><strong>Writing services</strong></td>
                            <td>&euro; <input type="text" size="5" name="writing" value="<?php if(!empty($_POST['writing'])) { echo $_POST['writing']; } ?>" /></td>
                        </tr>
                    </table>
                <?php
                }

                if(($_SESSION['cf_vis'])=='yes') {
                    ?>
                    <table width="500" border="0" cellpadding="0" cellspacing="0" class="report">
                        <tr>
                            <th colspan="2">Commitment fee (Euro)</th>
                        </tr>
                        <tr>
                            <td width="150"><strong>Training (quarterly)</strong></td>
                            <td>&euro; <?php echo $_SESSION['cftraining']; ?><!--<input type="text"  size="5" name="cftraining" value="<?php if(empty($_POST['cftraining'])) { echo $_SESSION['cftraining']; } if(!empty($_POST['cftraining'])) { echo $_POST['cftraining']; } ?>" />--></td>
                        </tr>
                        <tr class="odd">
                            <td><strong>Services (monthly)</strong></td>
                            <td>&euro; <?php echo $_SESSION['cfservices']; ?><!--<input type="text" size="5" name="cfservices" value="<?php if(empty($_POST['cfservices'])) { echo $_SESSION['cfservices']; } if(!empty($_POST['cfservices'])) { echo $_POST['cfservices']; } ?>" />--></td>
                        </tr>
                    </table>

                <?php
                }
                if(($_SESSION['publimap_vis'])=='yes') {
                    ?>

                    <table width="500" border="0" cellpadding="0" cellspacing="0" class="report">
                        <tr>
                            <th colspan="2">Publimap revenue (Euro)</th>
                        </tr>
                        <tr>
                            <td width="150"><strong>Revenue</strong></td>
                            <td>&euro; <input type="text"  size="5" name="publimap_rev" value="<?php if(!empty($_POST['publimap_rev'])) { echo $_POST['publimap_rev']; } ?>" /></td>
                        </tr>
                    </table>
                <?php
                }



                if(($_SESSION['imapper_vis'])=='yes') {
                    ?>

                    <table width="500" border="0" cellpadding="0" cellspacing="0" class="report">
                        <tr>
                            <th colspan="2">iMapper revenue (Euro)</th>
                        </tr>
                        <tr>
                            <td width="150"><strong>Revenue</strong></td>
                            <td>&euro; <input type="text"  size="5" name="imapper_rev" value="<?php if(!empty($_POST['imapper_rev'])) { echo $_POST['imapper_rev']; } ?>" /></td>
                        </tr>
                    </table>
                <?php
                }
                ?>

                <table width="500" border="0" cellpadding="0" cellspacing="0" class="report">
                    <tr>
                        <th colspan="2">Additional comments</th>
                    </tr>
                    <tr>
                        <td>
                            <textarea name="comments" cols="55" rows="10"><?php if(!empty($_POST['comments'])) { echo $_POST['comments']; } ?></textarea>
                        </td>
                    </tr>
                </table>
                <input type="hidden" name="action" value="insert"> <input type="submit" name="submit" value="Send report">
            </form>
<?php
// einde: niet ingelogd
        }
        ?>

    </div>

<?php
}
?>

</body>
</html>